<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>deshaw.petal</groupId>
    <artifactId>petal-parent</artifactId>
    <packaging>pom</packaging>
    <version>20171111-RC1</version>
    <name>Petal - Web Commons</name>

    <properties>

    </properties>

    <repositories>
        <repository>
            <snapshots>
                <updatePolicy>always</updatePolicy>
                <enabled>true</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
            <id>desco-maven</id>
            <name>main repository</name>
            <url>http://maven/deshaw</url>
        </repository>
    </repositories>

    <modules>
        <module>core-api</module>
        <module>core-web</module>
    </modules>

    <dependencyManagement>
        <dependencies>
            <!-- Project dependencies -->
            <dependency>
                <groupId>deshaw.petal</groupId>
                <artifactId>petal</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>deshaw.petal</groupId>
                <artifactId>web</artifactId>
                <type>war</type>
                <version>${project.version}</version>
            </dependency>

            <!-- Internal (deshaw.*) dependencies -->
            <dependency>
                <groupId>deshaw.desflow</groupId>
                <artifactId>desflow-commons</artifactId>
                <version>6.4-20110310</version>
                <exclusions>
                    <exclusion>
                        <groupId>ESAPI</groupId>
                        <artifactId>ESAPI</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.owasp</groupId>
                        <artifactId>antisamy</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>net.sourceforge.nekohtml</groupId>
                        <artifactId>nekohtml</artifactId>
                    </exclusion>
                    <exclusion>
                         <groupId>opensymphony</groupId>
                         <artifactId>xwork</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>deshaw</groupId>
                <artifactId>common</artifactId>
               <version>rel20120316_20120316.201719</version>
            </dependency>

            <dependency>
                <groupId>deshaw</groupId>
                <artifactId>webcommons</artifactId>
               <version>rel20120316_20120316.201719</version>
            </dependency>

            <dependency>
                <groupId>deshaw</groupId>
                <artifactId>sonar</artifactId>
                <version>rel20110121_20110121.202752</version>
            </dependency>

            <dependency>
                <groupId>deshaw.moss</groupId>
                <artifactId>moss-infra-services</artifactId>
                <version>20171111-RC1</version>
            </dependency>

            <!-- Other dependencies -->
            <dependency>
                <groupId>javax.servlet</groupId>
                <artifactId>servlet-api</artifactId>
                <version>2.4</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>org.samba.jcifs</groupId>
                <artifactId>jcifs</artifactId>
                <version>1.3.14-cname</version>
            </dependency>

            <dependency>
                <groupId>commons-collections</groupId>
                <artifactId>commons-collections</artifactId>
                <version>3.2.1</version>
            </dependency>

            <dependency>
              <groupId>commons-io</groupId>
              <artifactId>commons-io</artifactId>
              <version>2.4</version>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>3.1</version>
            </dependency>

            <dependency>
              <groupId>commons-logging</groupId>
              <artifactId>commons-logging</artifactId>
              <version>1.1.2</version>
            </dependency>

            <dependency>
              <groupId>commons-beanutils</groupId>
              <artifactId>commons-beanutils</artifactId>
              <version>1.8.3</version>
            </dependency>

            <dependency>
                <groupId>commons-codec</groupId>
                <artifactId>commons-codec</artifactId>
                <version>1.8</version>
            </dependency>

            <dependency>
                <groupId>org.apache.httpcomponents</groupId>
                <artifactId>httpclient</artifactId>
                <version>4.2.5</version>
            </dependency>

            <dependency>
                <groupId>org.apache.httpcomponents</groupId>
                <artifactId>httpcore</artifactId>
                <version>4.2.4</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>14.0.1</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-aop</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-aspects</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-beans</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context-support</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-core</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-jdbc</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-jms</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-orm</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-tx</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-asm</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-expression</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-test</artifactId>
                <version>3.0.5.RELEASE</version>
            </dependency>

            <dependency>
                <groupId>org.apache.struts</groupId>
                <artifactId>struts2-core</artifactId>
                <version>2.3.34</version>
            </dependency>
            <dependency>
                <groupId>commons-lang</groupId>
                <artifactId>commons-lang</artifactId>
                <version>2.4</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-war-plugin</artifactId>
                    <version>2.1.1</version>
                </plugin>

                <plugin>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>2.0</version>
                    <configuration>
                        <autoVersionSubmodules>true</autoVersionSubmodules>
                        <addSchema>false</addSchema>
                    </configuration>
                </plugin>

                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <configuration>
                        <skipTests>true</skipTests>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>properties-maven-plugin</artifactId>
                    <version>1.0-alpha-1</version>
                </plugin>

                <plugin>
                    <artifactId>maven-source-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>attach-sources</id>
                            <phase>deploy</phase>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>attach-javadocs</id>
                            <phase>deploy</phase>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <!--
                            explicitly define maven-deploy-plugin after other to force exec order
                    -->
                    <artifactId>maven-deploy-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>deploy</id>
                            <phase>deploy</phase>
                            <goals>
                                <goal>deploy</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <profile>
            <id>runTests</id>
            <build>
                <pluginManagement>
                    <plugins>
                        <plugin>
                            <artifactId>maven-surefire-plugin</artifactId>
                            <configuration>
                                <skipTests>false</skipTests>
                                <reportsDirectory>${project.parent.build.directory}/surefire-reports</reportsDirectory>
                            </configuration>
                        </plugin>
                    </plugins>
                </pluginManagement>
            </build>
        </profile>
    </profiles>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
                <version>2.1</version>
                <configuration>
                    <!--
                        We will aggregate this once we figure out and aggregate pmd and
                        checkstyle reports <aggregate>true</aggregate>
                    -->
                    <linkJavadoc>true</linkJavadoc>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.6.1</version>
                <configuration>
                    <aggregate>true</aggregate>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>2.1.2</version>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>taglist-maven-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <!--
                        We will aggregate this once we figure out and aggregate pmd and
                        checkstyle reports <aggregate>true</aggregate>
                    -->
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
                <version>1.1</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-eclipse-plugin</artifactId>
                <version>2.8</version>
                <configuration>
                    <downloadSources>true</downloadSources>
                    <downloadJavadocs>true</downloadJavadocs>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <version>2.1</version>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>findbugs-maven-plugin</artifactId>
                <version>2.0.1</version>
                <configuration>
                    <effort>Max</effort>
                    <threshold>Low</threshold>
                    <xmlOutput>true</xmlOutput>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.0</version>
                <configuration>
                    <reportPlugins>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-project-info-reports-plugin</artifactId>
                            <version>2.4</version>
                        </plugin>

                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-jxr-plugin</artifactId>
                            <version>2.3</version>
                            <configuration>
                                <aggregate>true</aggregate>
                                <linkJavadoc>true</linkJavadoc>
                            </configuration>
                        </plugin>

                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-javadoc-plugin</artifactId>
                            <version>2.8</version>
                            <configuration>
                                <aggregate>true</aggregate>
                            </configuration>
                        </plugin>

                        <plugin>
                            <groupId>org.codehaus.mojo</groupId>
                            <artifactId>cobertura-maven-plugin</artifactId>
                            <version>2.4</version>
                            <configuration>
                                <formats>
                                    <format>xml</format>
                                    <format>html</format>
                                </formats>
                                <check>
                                    <haltOnFailure>false</haltOnFailure>
                                </check>
                            </configuration>
                        </plugin>

                        <plugin>
                            <groupId>org.codehaus.mojo</groupId>
                            <artifactId>taglist-maven-plugin</artifactId>
                            <version>2.4</version>
                            <configuration>
                                <aggregate>true</aggregate>
                            </configuration>
                        </plugin>

                        <plugin>
                            <groupId>org.codehaus.mojo</groupId>
                            <artifactId>findbugs-maven-plugin</artifactId>
                            <version>2.4.0</version>
                            <configuration>
                                <effort>Max</effort>
                                <threshold>Low</threshold>
                                <xmlOutput>true</xmlOutput>
                            </configuration>
                        </plugin>

                        <plugin>
                            <groupId>org.codehaus.mojo</groupId>
                            <artifactId>versions-maven-plugin</artifactId>
                            <version>1.2</version>
                        </plugin>
                    </reportPlugins>
                </configuration>
            </plugin>
        </plugins>
    </reporting>

    <ciManagement>
        <system>Hudson</system>
        <url>http://hudson/hudson</url>
    </ciManagement>

    <distributionManagement>
        <snapshotRepository>
            <id>snapshots</id>
            <name>desco-snapshots</name>
            <url> http://maven/repositories/snapshots</url>
        </snapshotRepository>
        <site>
            <id>petal site</id>
            <name>Petal Maven Site</name>
            <url>file:///u/${env.USER}/.www/petal/site</url>
        </site>
    </distributionManagement>

    <scm>
        <url>http://gitweb.nyc.deshaw.com/?p=infra/petal.git;a=summary</url>
        <connection>scm:git:ssh://gitserve/infra/petal.git</connection>
    </scm>

</project>
