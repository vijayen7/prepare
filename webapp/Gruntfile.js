module.exports = function(grunt) {

   grunt.initConfig({
        'copy' :
            {  main: {
                  files: [
                    { expand: true, cwd: 'node_modules/arc-grid/dist', src: '**/*', dest: 'js/arc-grid' }
                  ]
                }
            }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('build', ['copy']);
};