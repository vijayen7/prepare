import Date from './app/commons/components/Date';
import AgmtSummaryVsReconcilerSummaryTab from './app/Lcm/AgreementSummary/components/AgmtSummaryVsReconcilerSummaryTab';
import { PostCollateralPopup } from './app/Lcm/AgreementSummary/components/PostCollateralPopup';
import PhysicalCollateralInventory from './app/Lcm/AgreementSummary/components/PhysicalCollateralInventoryGrid/PhysicalCollateralInventory';
import { AgreementWorkflowComponents as AgreementWorkflowReactComponents } from './app/Lcm/AgreementWorkflow/components';

export const DatePicker = Date;
export const AgmtSummaryVsReconcilerSummaryDetails = AgmtSummaryVsReconcilerSummaryTab;
export const PostCollateralPopupComponent = PostCollateralPopup;
export const PhysicalCollateralInventoryGrid = PhysicalCollateralInventory;
export const AgreementWorkflowComponents = AgreementWorkflowReactComponents;
