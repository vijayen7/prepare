const { createWebpackConfig } = require("arc-react-lib-tools");
const webpack = require('webpack');

module.exports = createWebpackConfig((config) => ({
  ...config,
  entry: { "treasury-components": "./index.jsp.react.components.js" },
  output: {
    ...config.output,
    library: "TreasuryComponents",
    publicPath: "/treasury/components/",
  },
  externals: {
    react: "React",
    "react-dom": "ReactDOM",
  },
  module: {
    rules: [
      ...config.module.rules,
      {
        test: /\.worker\.(js|ts)$/,
        use: require.resolve("worker-loader"),
      },
    ],
  },
  resolve: {
    ...config.resolve,
    extensions: [".js", ".jsx", ".ts", ".tsx"],
    modules: ["app", "node_modules"],
  },
  plugins: [
    ...config.plugins,
    new webpack.DefinePlugin({
      "process.env": {
        url: '""',
        moss_url: '"/moss"'
      }
    })
  ]
}));
