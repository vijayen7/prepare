"use strict";

window.ArcLoading = (function(){

var owner = (document._currentScript || document.currentScript).ownerDocument;

var customLoadingElement = Object.create(HTMLElement.prototype);

customLoadingElement.createdCallback = function() {

    function _initialize(self) {
        var template = owner.getElementById("loadingTemplate");
        var templateClone = document.importNode(template.content, true);

        self.appendChild(templateClone);
    }


    this.show = function() {
        this.style.display = "block";
    };

    this.hide = function() {
        this.style.display = "none";
    };

    _initialize(this);
};

return document.registerElement("arc-loading", {prototype : customLoadingElement});
})();
