/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 *   @author chawlasi
 */

import lodash from 'lodash';

global._ = lodash;
global.CODEX_PROPERTIES = {};

// For onMouseLeave,onMouseEnter in ApplicationMenu (jsdom doesnot have this)
document.createRange = () => ({
  setStart: () => {},
  setEnd: () => {},
  commonAncestorContainer: {
    nodeName: 'BODY',
    ownerDocument: document,
  },
});

//Required for Copy Search, (jsdom doesnt provide this command)
Object.defineProperty(global.document, 'execCommand', {
  value: jest.fn().mockImplementation(() => true),
});

function propagateToGlobal(window) {
  for (const key in window) {
    if (!window.hasOwnProperty(key)) continue;
    if (key in global) continue;
    global[key] = window[key];
  }
}

propagateToGlobal(window);
