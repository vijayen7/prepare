/**
 * DEVELOPMENT WEBPACK CONFIGURATION
 */
const cssVars = require("../../app/css_vars");
const devConfig = require("arc-react-tools/internals/webpack/webpack.dev.babel");
const path = require("path");
const codex = require("../codex");

module.exports = devConfig({
  output: {
    publicPath: "/treasury"
  },
  resolve: {
    extensions: ["", ".js", ".jsx"],
    root: [path.resolve("./app"), path.resolve("./node_modules")]
  },
  codexResource: codex.resource,
  codexEnvironment: codex.environment,
  codexCategories: codex.categories,
  codexProperties: codex.properties,
  enableGrid: true,
  enableJqueryUI: true,
  disableCleanBuild: true,
  cssVars,
  "process.env": {
    url: '"http://treasurydev.deshaw.com"',
    moss_url: '"http://mossqa.deshaw.com"'
  }
});
