/**
 * PRODUCTION WEBPACK CONFIGURATION
 */
const cssVars = require("../../app/css_vars");
const path = require("path");
const codex = require("../codex");

module.exports = require("arc-react-tools/internals/webpack/webpack.prod.babel")(
  {
    output: {
      path: path.resolve(process.cwd(), "../"),
      publicPath: "/treasury/"
    },
    resolve: {
      extensions: ["", ".js", ".jsx", ".ts", ".tsx"],
      root: [path.resolve("./app"), path.resolve("./node_modules")]
    },
    codexResource: codex.resource,
    codexEnvironment: codex.environment,
    codexCategories: codex.categories,
    codexProperties: codex.properties,
    enableGrid: true,
    enableJqueryUI: true,
    disableCleanBuild: true,
    cssVars,
    "process.env": {
      url: '""',
      moss_url: '"/moss"'
    }
  }
);
