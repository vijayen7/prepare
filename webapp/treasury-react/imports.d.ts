// Don't error on imports without types (such as 3rd party libraries without @types definition installed).
// https://stackoverflow.com/a/50516783
declare module '*';
