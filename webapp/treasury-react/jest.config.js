/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 *   @author chawlasi
 */

const config = require('arc-react-tools/internals/testing/jest.config');
const _ = require('lodash');

module.exports = _.merge(
  {},
  config,
  {
    coverageThreshold: {
      global: {
        statements: 0,
        branches: 0,
        functions: 0,
        lines: 0,
      },
    },
  },
  {
    setupFiles: ['./setup-jest.js'],
    preset: 'ts-jest',
    transform: {
      '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest',
    },
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    moduleDirectories: ['node_modules', 'app'],
    transformIgnorePatterns: ['!node_modules/arc-'],
    testEnvironment: 'jsdom',
    modulePathIgnorePatterns: ['build', 'dist'],
    collectCoverageFrom: [
      'app/**/*.{js,jsx,ts,tsx}',
      '!app/**/*.test.{js,jsx,ts,tsx}',
      '!app/tests/**/*.*',
      '!app/*/RbGenerated*/*.{js,jsx,ts,tsx}',
      '!app/app.js',
      '!app/utils/*.*',
    ],
  }
);
