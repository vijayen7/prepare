import {
  FETCH_CPE_GROUPS,
  FETCH_CPE_GROUP_BY_ID,
  FETCH_CPE_LIST,
  HIDE_POPUP,
  RESET_SELECTED_CPE_DATA,
  SET_SELECTED_CPE_DATA,
  SHOW_POPUP,
  SAVE_CPE_GROUP,
  DELETE_CPE_GROUP,
  SET_CPE_DELETE_TOGGLE,
  RESET_CPE_DELETE_TOGGLE,
  RESET_CPE_GROUP_DATA,
  RESET_UNDERLYING_CPE_TOGGLE,
  SET_UNDERLYING_CPE_TOGGLE,
  ADD_CPE_GROUP,
  SET_CPE_EDIT_TOGGLE,
  RESET_CPE_EDIT_TOGGLE,
  RESET_ACTION_MESSAGE
} from "./constants";
import {RESIZE_CANVAS} from "commons/constants";

export function fetchCpeGroups(payload) {
  return {
    type: FETCH_CPE_GROUPS,
    payload
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

export function fetchCpeGroupById(payload, displayName) {
  return {
    type: FETCH_CPE_GROUP_BY_ID,
    payload,
    displayName
  };
}

export function fetchCpeList() {
  return{
    type: FETCH_CPE_LIST
  };
}

export function showPopUp() {
  return {
    type: SHOW_POPUP
  }
}

export function hidePopUp() {
  return {
    type: HIDE_POPUP
  };
}

export function setSelectedCpeData(payload) {
  return {
    type: SET_SELECTED_CPE_DATA,
    payload
  };
}

export function resetSelectedCpeData() {
  return {
    type: RESET_SELECTED_CPE_DATA
  };
}

export function updateCpeGroup(payload) {
  return {
    type: SAVE_CPE_GROUP,
    payload
  }
}

export function deleteCpeGroup(payload) {
  return {
    type: DELETE_CPE_GROUP,
    payload
  }
}

export function setDeleteToggle(){
  return {
    type: SET_CPE_DELETE_TOGGLE
  };
}

export function resetDeleteToggle() {
  return {
    type: RESET_CPE_DELETE_TOGGLE
  };
}

export function resetCpeGroupData() {
  return {
    type: RESET_CPE_GROUP_DATA
  };
}

export function setUnderlyingCpeToggle() {
  return {
    type: SET_UNDERLYING_CPE_TOGGLE
  };
}

export function resetUnderlyingCpeToggle() {
  return {
    type: RESET_UNDERLYING_CPE_TOGGLE
  };
}

export function addCpeGroup(payload) {
  return {
    type: ADD_CPE_GROUP,
    payload
  }
}

export function setEditToggle(){
  return {
    type: SET_CPE_EDIT_TOGGLE
  };
}

export function resetEditToggle() {
  return {
    type: RESET_CPE_EDIT_TOGGLE
  };
}

export function resetActionMessage() {
  return {
    type:RESET_ACTION_MESSAGE
  }
}
