import React, { Component } from 'react';
import _ from 'lodash';
import { Layout, Card} from 'arc-react-components';
import CpeListPopUpGrid from 'commons/components/GridWithCheckbox';
import InputFilter from 'commons/components/InputFilter';
import Message from 'commons/components/Message';
import FilterButton from 'commons/components/FilterButton';
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";

import {
  getAllCpeListColumns,
  getExistingCpeListColumns
} from '../grid/columnConfig';
import { getGridOptionsForPopUp } from '../grid/gridOptions';
import { getFirstDateOfCurrentMonth, getCommaSeparatedValuesOrNull } from 'commons/util';

export default class CpeFilterPopUp extends Component {
  constructor(props) {
    super(props);
    this.renderData = this.renderData.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleRightClick = this.handleRightClick.bind(this);
    this.handleLeftClick = this.handleLeftClick.bind(this);
    this.onDblClickHandler = this.onDblClickHandler.bind(this);
    this.renderAllCpeList = this.renderAllCpeList.bind(this);
    this.renderExistingCpeList = this.renderExistingCpeList.bind(this);
    this.renderButton = this.renderButton.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = this.getDefaultState();
  }

  componentDidMount() {
    this.props.fetchCpeList();
  }

  componentDidUpdate(prevProps) {
    if (_.isEmpty(prevProps.cpeListData) && !_.isEmpty(this.props.cpeListData)) {
      this.setState({ allCpeListGridData: this.props.cpeListData });
    }
    if (!_.isEqual(prevProps.cpeGroupById, this.props.cpeGroupById)) {
      let cpeListData = this.props.cpeGroupById.cpeFamilyList;
      this.setState({ allCpeListGridSelectedRows: cpeListData });
      this.setState({ updateGridForEdit: true });
    }
    if (this.state.updateGridForEdit && !_.isEmpty(this.state.allCpeListGridData)) {
      this.setState({ updateGridForEdit: false });
      this.handleRightClick();
    }
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }


  onCellClickHandler = (args) => { };

  onDblClickHandler = (args) => { };


  getDefaultState() {
    const allCpeListGridData = !_.isEmpty(this.props.cpeListData) ? this.props.cpeListData : [];
    return {
      name: [],
      description: [],
      selectedDate: getFirstDateOfCurrentMonth(),
      selectedAgreementTypes: "",
      allCpeListGridData: allCpeListGridData,
      existingCpeListGridData: [],
      allCpeListGridSelectedRows: [],
      existingCpeListGridSelectedRows: [],
      selectedRowData: [],
      isSubmitClicked: false,
      updateGridForEdit: false,
      showCpeGroupErrorDialog: false,
      error: {
        name: false,
        description: false
      },
      isAddActive: false
    };
  }

  handleRightClick() {

    // if (selectedRows.length === 0) return;
    let updatedDataInCurrentGrid = _.cloneDeep(this.state.allCpeListGridData);
    let updatedDataInExistingGrid = _.cloneDeep(this.state.existingCpeListGridData);

    const allCpeListMap = updatedDataInCurrentGrid.resultList.reduce((map, obj) => {
      map[obj.id] = obj;
      return map;
    }, {});

    const selectedRows = this.state.allCpeListGridSelectedRows.reduce((list, obj) => {
      list.push(obj.id);
      return list;
    }, []);

    for (let i in selectedRows) {
      updatedDataInExistingGrid.push(allCpeListMap[selectedRows[i]]);
      let index = updatedDataInCurrentGrid.resultList.indexOf(allCpeListMap[selectedRows[i]]);
      updatedDataInCurrentGrid.resultList.splice(index, 1);
    }

    this.setState({
      allCpeListGridData: _.cloneDeep(updatedDataInCurrentGrid),
    });
    this.setState({ existingCpeListGridData: updatedDataInExistingGrid });
    this.setState({
      allCpeListGridSelectedRows: [],
      existingCpeListGridSelectedRows: []
    });
  }

  handleLeftClick() {

    // if (selectedRows.length === 0) return;
    let updatedDataInCurrentGrid = _.cloneDeep(this.state.allCpeListGridData);
    let updatedDataInExistingGrid = _.cloneDeep(this.state.existingCpeListGridData);

    const existingCpeListMap = updatedDataInExistingGrid.reduce((map, obj) => {
      map[obj.id] = obj;
      return map;
    }, {});

    const selectedRows = this.state.existingCpeListGridSelectedRows.reduce((list, obj) => {
      list.push(obj.id);
      return list;
    }, []);

    for (let i in selectedRows) {
      updatedDataInCurrentGrid.resultList.push(existingCpeListMap[selectedRows[i]]);
      let index = updatedDataInExistingGrid.indexOf(existingCpeListMap[selectedRows[i]]);
      updatedDataInExistingGrid.splice(index, 1);
    }

    this.setState({
      allCpeListGridData: updatedDataInCurrentGrid,
      existingCpeListGridData: updatedDataInExistingGrid,
      allCpeListGridSelectedRows: [],
      existingCpeListGridSelectedRows: []
    });
  }

  renderButton() {
    if (this.props.isDeleteClicked) {
      return (<FilterButton onClick={this.handleDelete} label="Delete" />);
    } else if (this.props.isEditClicked) {
      return (<FilterButton onClick={this.handleSubmit} reset={false} label="Submit" disabled={_.isEmpty(this.state.existingCpeListGridData)} />);
    }
    else {
      return (<FilterButton onClick={this.handleAdd} reset={false} label="Add" disabled={_.isEmpty(this.state.existingCpeListGridData)} />);
    }
  }


  handleSubmit() {
    let selectedIds = [];
    let parameters = {};
    for (var i in this.state.existingCpeListGridData) {
      selectedIds.push(this.state.existingCpeListGridData[i].id);
    }
    parameters = {
      selectedCpeIds: selectedIds.toString(),
      selectedCpeGroupId: this.props.selectedCpeList.groupId.toString()
    };
    this.props.saveCpeGroup(parameters);
    this.setState(this.getDefaultState());

    this.props.handleClosePopUp();
  }

  handleDelete() {

    const payload = {
      groupIdToDelete: this.props.cpeGroupById.groupId.toString()
    };
    this.props.deleteCpeGroup(payload);
    this.props.handleClosePopUp();
  }

  handleAdd() {
    if (!this.props.isEditClicked) {
      let error = { name: false };
      error.name = this.state.name.length === 0;
      this.setState({ error });
      if (error.name) {
        return;
      }
    }
    let selectedIds = [];
    let parameters = {};
    for (var i in this.state.existingCpeListGridData) {
      selectedIds.push(this.state.existingCpeListGridData[i].id);
    }
    parameters = {
      groupName: this.state.name,
      agreementTypeId : getCommaSeparatedValuesOrNull([this.state.selectedAgreementTypes]),
      cpeFamilyIds: selectedIds.toString(),
    };
    this.props.addCpeGroup(parameters);
    this.setState(this.getDefaultState());
    this.props.handleClosePopUp();
  }

  renderAllCpeList() {
    var dataList = (this.state.allCpeListGridData) ? (this.state.allCpeListGridData.resultList) : [];
    return (
      <CpeListPopUpGrid
        data={dataList}
        gridId="allCpeList"
        gridColumns={getAllCpeListColumns()}
        gridOptions={getGridOptionsForPopUp()}
        label="All Counterparty Family(s)"
        onSelect={this.onSelect}
        preSelectAllRows={false}
        selectedRowDataKey="allCpeListGridSelectedRows"
        onDblClick={this.onDblClickHandler}
        onCellClick={this.onCellClickHandler}
      />

    );
  }

  renderExistingCpeList() {
    var dataList = (this.state.existingCpeListGridData) ? (this.state.existingCpeListGridData) : [];
    return (
      <CpeListPopUpGrid
        data={dataList}
        gridId="existingCpeList"
        gridColumns={getExistingCpeListColumns()}
        gridOptions={getGridOptionsForPopUp()}
        label="Existing/Added Counterparty Family(s)"
        onSelect={this.onSelect}
        preSelectAllRows={false}
        selectedRowDataKey="existingCpeListGridSelectedRows"
        onDblClick={this.onDblClickHandler}
        onCellClick={this.onCellClickHandler}
      />

    );
  }

  renderAddPopUp() {
    return (
      <Layout isRowType>
        <Layout.Child size={1}>
          {this.renderAddPopupLabels()}
        </Layout.Child>
        <Layout.Child size={1}>
          <br />
        </Layout.Child>
        <Layout.Child>
          {this.renderEditPopUp()}
        </Layout.Child>
      </Layout>
    );
  }

  renderAddPopupLabels() {
    return (
      <Card>
      <Layout isColumnType>
          <Layout.Child size='25%'>
            <InputFilter
              onSelect={this.onSelect}
              data={this.state.name}
              stateKey="name"
              label="Name"
              isError={this.state.error.name}
            />
          </Layout.Child>
          <Layout.Child size='5%'>
            <React.Fragment/>
          </Layout.Child>
          <Layout.Child size='35%'>
            <AgreementTypeFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedAgreementTypes}
              multiSelect={false}
              horizontalLayout={true}
            />
          </Layout.Child>
        </Layout>
        </Card>
    );
  }

  renderEditPopUp() {
    return (
      <Layout isColumnType>
        <Layout.Child size={4}>
          <div className="padding--left">{this.renderAllCpeList()}</div>
        </Layout.Child>
        <Layout.Child size={0.25} />
        <Layout.Child size={1.5}>
          <FilterButton
            onClick={this.handleRightClick}
            reset={false}
            label=">"
            style={{ width: '70%', height: '40px' }}
          />
          <br />
          <br />
          <FilterButton
            onClick={this.handleLeftClick}
            reset={false}
            label="<"
            style={{ width: '70%', height: '40px' }}
          />
        </Layout.Child>
        <Layout.Child size={0.5} />
        <Layout.Child size={4}>
          <div className="padding--left">{this.renderExistingCpeList()}</div>
        </Layout.Child>
      </Layout>
    );

  }

  renderDeletePopUP() {

    let messageData = "Do you want to delete Counterparty family group (" + this.props.cpeGroupById.groupName + " )?";
    return (
      <Layout isColumnType>
        <Layout.Child size={4}>
          <Message
            messageData={messageData} />
        </Layout.Child>
      </Layout>
    );
  }

  renderData() {
    let gridData;
    if (this.props.isEditClicked) {
      gridData = this.renderEditPopUp();
    }

    else if (this.props.isDeleteClicked) {
      gridData = this.renderDeletePopUP();
    }
    else {
      gridData = this.renderAddPopUp();
    }
    return (
      <React.Fragment>
        <Layout isRowType>
          <Layout.Child>
            {gridData}
          </Layout.Child>
          <Layout.Child>
            <br />
            {this.renderButton()}
          </Layout.Child>
        </Layout>
      </React.Fragment>

    );
  }

  render() {
    return this.renderData();
  }
}
