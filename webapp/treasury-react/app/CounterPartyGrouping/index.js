import React, { Component } from "react";
import { hot } from "react-hot-loader/root";
import Loader from "commons/container/Loader";
import SearchPanel from "./container/SearchPanel";
import Grid from "./container/Grid";
import { Layout } from "arc-react-components";

class CounterPartyGrouping extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getBreadCrumbs = this.getBreadCrumbs.bind(this);
  }

  getBreadCrumbs() {
    return [
      {
        name: 'Treasury-Financing',
        link: '#'
      },
      {
        name: 'Counterparty Report',
       link: './counterparty-reporting.html'
      },
      {
        name: 'Groups',
       link: './counterparty-grouping.html'
      }
    ];
  }

  componentDidMount() {
      var header = this.header;
      var breadcrumbs = this.getBreadCrumbs();

      if (header.ready) {
        header.setBreadcrumb(breadcrumbs);
      } else {
        header.addEventListener('ready', header.setBreadcrumb(breadcrumbs));
     }
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <Layout isRowType >
        <Layout.Child size={1.5}>
            <arc-header
              ref={header => {
                this.header = header;
              }}
              className="size--content"
              user={USER}
              modern-themes-enabled
            />
          </Layout.Child>
          <Layout.Child size={12}>
            <Layout isColumnType>
              <Layout.Child size={2}>
                <SearchPanel />
              </Layout.Child>
              <Layout.Divider />
              <Layout.Child size={0.1}>
                <React.Fragment/>
              </Layout.Child>
              <Layout.Child size={12}>

                <Grid />
              </Layout.Child>
            </Layout>
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );
  }
}

export default hot(CounterPartyGrouping);
