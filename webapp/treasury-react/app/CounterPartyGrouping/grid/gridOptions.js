import { url } from "../api";

export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function () {
        return window.location.origin + url;
      }
    },
    autoHorizontalScrollBar: true,
    nestedTable: false,
    sortList: [
      {
        columnId: "groupId",
        sortAsc: true
      }

    ],
    onRowClick: function () {// highlight on row works if this callback is defined
    },
    highlightRowOnClick: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    exportToExcel: true
  };

  return options;
}

export function getGridOptionsForPopUp() {
  return {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    highlightRowOnClick: true,
    sortList: [
      {
        columnId: "id",
        sortAsc: true
      }
    ],
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    exportToExcel: true,
	checkboxHeader: [ {
	controlChildSelection: false,
  isSelectAllCheckboxRequired: true
	}
	],
	addCheckboxHeaderAsLastColumn :false
  };
}
