import { linkFormatter } from "../../commons/grid/formatters"

  export function getCpeGroupSearchColumns() {
    return [{
      id: 'groupId',
      type: 'text',
      name: 'Counterparty Group',
      field: 'groupName',
      formatter: function(row, cell, value, columnDef, dataContext) {
        return linkFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext
        );
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'agreementTypeId',
      type: 'text',
      name: 'Agreement Type',
      field: 'agreementType',
      formatter: function(row, cell, value, columnDef, dataContext) {
        return (
          value != undefined ? value['abbrev'] : value
        );
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return (
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        );
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    }
    ];
  }

  export function getAllCpeListColumns() {
    return [{
      id: 'id',
      type: 'text',
      name: 'All CPE Family(s)',
      field: 'cpe_family_name',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }];
  }

  export function getExistingCpeListColumns() {
    return [{
      id: 'id',
      type: 'text',
      name: 'Existing CPE Family(s)',
      field: 'cpe_family_name',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }];
  }


