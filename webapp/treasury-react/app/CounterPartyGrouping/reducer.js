import {
  FETCH_CPE_GROUPS,
  FETCH_CPE_GROUP_BY_ID,
  FETCH_CPE_LIST,
  SHOW_POPUP,
  HIDE_POPUP,
  RESET_SELECTED_CPE_DATA,
  SET_SELECTED_CPE_DATA,
  SAVE_CPE_GROUP,
  DELETE_CPE_GROUP,
  RESET_CPE_DELETE_TOGGLE,
  SET_CPE_DELETE_TOGGLE,
  RESET_CPE_GROUP_DATA,
  SET_UNDERLYING_CPE_TOGGLE,
  RESET_UNDERLYING_CPE_TOGGLE,
  ADD_CPE_GROUP,
  SET_CPE_EDIT_TOGGLE,
  RESET_CPE_EDIT_TOGGLE,
  RESET_ACTION_MESSAGE
} from "./constants";

import {RESIZE_CANVAS} from "commons/constants";
import { combineReducers } from "redux";

function cpeGroupSearchReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_CPE_GROUPS}_SUCCESS`:
      if (action.data.length <= 0) {
        action.data.message = 'No data found';
      }
      return action.data || [];
    case RESET_CPE_GROUP_DATA:
      return [];
    default:
      return state;
  }
}

function cpeGroupSearchByIdReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_CPE_GROUP_BY_ID}_SUCCESS`:
      return action.data || [];
  }
  return state;
}

function cpeListSearchReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_CPE_LIST}_SUCCESS`:
      return action.data || [];
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

const closePopUP = { showCpeList: false };

function openOrClosePopUpReducer(state = closePopUP, action) {
  switch (action.type) {
    case SHOW_POPUP:
      return { ...state, showCpeList: true };
    case HIDE_POPUP:
      return { ...state, showCpeList: false };
  }
  return state;
}

function setSelectedRowDataReducer(state = {}, action) {
  switch (action.type) {
    case SET_SELECTED_CPE_DATA:
      return action.payload || {};
    case RESET_SELECTED_CPE_DATA:
      return {};
    default:
      return state;
  }
}

function saveCpeGroupReducer(state = 0, action) {
  switch (action.type) {
    case `${SAVE_CPE_GROUP}_SUCCESS`:
      return 1;
    case `${SAVE_CPE_GROUP}_FAILURE`:
      return -1;
    case `${RESET_ACTION_MESSAGE}`:
      return 0;
    default:
      return state;
  }
}

function deleteCpeGroupReducer(state = 0, action) {
  switch (action.type) {
    case `${DELETE_CPE_GROUP}_SUCCESS`:
      return 1;
    case `${DELETE_CPE_GROUP}_FAILURE`:
      return -1;
    case `${RESET_ACTION_MESSAGE}`:
      return 0;
    default:
      return state;
  }
}

function deleteCpeToggleReducer(state = false, action) {
  switch (action.type) {
    case SET_CPE_DELETE_TOGGLE:
      return true;
    case RESET_CPE_DELETE_TOGGLE:
      return false;
    default:
      return state;
  }
}

function editCpeToggleReducer(state = false, action) {
  switch (action.type) {
    case SET_CPE_EDIT_TOGGLE:
      return true;
    case RESET_CPE_EDIT_TOGGLE:
      return false;
    default:
      return state;
  }
}

function underlyingCpeToggleReducer(state = false, action) {
  switch (action.type) {
    case SET_UNDERLYING_CPE_TOGGLE:
      return true;
    case RESET_UNDERLYING_CPE_TOGGLE:
      return false;
    default:
      return state;
  }
}

function addCpeGroupReducer(state = 0, action) {
  switch (action.type) {
    case `${ADD_CPE_GROUP}_SUCCESS`:
      return 1;
    case `${ADD_CPE_GROUP}_FAILURE`:
      return -1;
    case `${RESET_ACTION_MESSAGE}`:
      return 0;
    default:
      return state;
  }
}


const CpeGroupReducer = combineReducers({
  cpeGroup: cpeGroupSearchReducer,
  cpeGroupById: cpeGroupSearchByIdReducer,
  cpeList: cpeListSearchReducer,
  resizeCanvas: resizeCanvasReducer,
  isShowPopUp: openOrClosePopUpReducer,
  selectedCpeList: setSelectedRowDataReducer,
  saveCpeGroup: saveCpeGroupReducer,
  isDeleteClicked: deleteCpeToggleReducer,
  isEditClicked: editCpeToggleReducer,
  deleteCpeGroup: deleteCpeGroupReducer,
  showUnderlyingCpe: underlyingCpeToggleReducer,
  addCpeGroup: addCpeGroupReducer
});

export default CpeGroupReducer;
