import {BASE_URL} from "commons/constants";
import { fetchURL, fetchPostURL } from "commons/util";

const queryString = require("query-string");

export function getCpeGroups(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/counterpartyReportingGroupingService/getAllGroups?${paramString}&format=JSON`;
  return fetch(url, {
      credentials: "include"
  }).then(data => data.json())
}

export function getCpeGroupById(payload) {

  console.log(queryString.stringify(payload));
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/counterpartyReportingGroupingService/getGroupById?${paramString}&format=JSON`;
  return fetchURL(url);
}


export function updateCounterpartyReportingGroup(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/counterpartyReportingGroupingService/updateCounterpartyReportingGroup?${paramString}&format=JSON`
  return fetch(url, {
    credentials: "include"
}).then(data => data.json())
}

export function deleteCounterpartyReportingGroup(payload) {
  const paramString = queryString.stringify(payload);
  url= `${BASE_URL}service/counterpartyReportingGroupingService/deleteCounterpartyReportingGroup?${paramString}&format=JSON`
  return fetchURL(url);
}

export function addCounterpartyReportingGroup(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/counterpartyReportingGroupingService/addCounterpartyReportingGroup?${paramString}&format=JSON`
  return fetchURL(url);
}


export function getCpeList() {
  url = `${BASE_URL}data/load-cpe-family-list`
  return fetchURL(url);
}
