import React, { Component } from "react";
import _ from "lodash";
import Dialog from "commons/components/Dialog";
import Message from "commons/components/Message";
import MessageDialog from "commons/components/MessageDialog";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchCpeList,
  updateCpeGroup,
  deleteCpeGroup,
  resetDeleteToggle,
  fetchCpeGroups,
  addCpeGroup,
  resetSelectedCpeData,
  resetCpeGroupData,
  resetUnderlyingCpeToggle,
  hidePopUp,
  resizeCanvas
} from "../actions";
import CpeFilterPopUp from "../components/CpeFilterPopUp";

class CpePopUp extends Component {
  constructor(props) {
    super(props);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
  }

  handleClosePopUp() {
    this.props.hidePopUp();
    this.props.resetSelectedCpeData();
    this.props.resetDeleteToggle();
    this.props.resetUnderlyingCpeToggle();
  }

  renderPopUp() {
    let popUp = null;
    let message = null;
    let title = this.props.isEditClicked
      ? "EDIT Counterparty Family GROUP (" +
      this.props.selectedCpeList.groupName +
      ")"
      : "ADD Counterparty Family GROUP ";
    popUp = (
      <React.Fragment>
        <Dialog
          isOpen={
            (this.props.isPopUpOpen|| !_.isEmpty(this.props.selectedCpeList))
          }
          title={title}
          onClose={this.handleClosePopUp}
          style={{
            width: "880px"
          }}
        >
          <div style={{ width: "860px" }}>
            <CpeFilterPopUp
              isEditClicked={this.props.isEditClicked}
              isDeleteClicked={this.props.isDeleteClicked}
              fetchCpeList={this.props.fetchCpeList}
              cpeListData={this.props.cpeList}
              cpeGroupById={this.props.cpeGroupById}
              addCpeGroup={this.props.addCpeGroup}
              saveCpeGroup={this.props.updateCpeGroup}
              deleteCpeGroup={this.props.deleteCpeGroup}
              handleClosePopUp={this.handleClosePopUp}
              selectedCpeList={this.props.selectedCpeList}
            />
          </div>
        </Dialog>
      </React.Fragment>
    );

    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCpeList,
      updateCpeGroup,
      deleteCpeGroup,
      fetchCpeGroups,
      resetSelectedCpeData,
      resetDeleteToggle,
      resetCpeGroupData,
      resetUnderlyingCpeToggle,
      addCpeGroup,
      hidePopUp,
      resizeCanvas
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.cpeGroupData.isShowPopUp.showCpeList,
    cpeList: state.cpeGroupData.cpeList,
    cpeGroupById: state.cpeGroupData.cpeGroupById,
    selectedCpeList: state.cpeGroupData.selectedCpeList,
    saveCpeGroup: state.cpeGroupData.saveCpeGroup,
    isDeleteClicked: state.cpeGroupData.isDeleteClicked,
    deleteCpe: state.cpeGroupData.deleteCpeGroup,
    isShowUnderlyingCpe: state.cpeGroupData.showUnderlyingCpe,
    isAddCpeGroup: state.cpeGroupData.addCpeGroup,
    isEditClicked: state.cpeGroupData.isEditClicked
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CpePopUp);
