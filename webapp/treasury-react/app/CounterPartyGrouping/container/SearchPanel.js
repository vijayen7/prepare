import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import {getCommaSeparatedValuesOrNullForAll} from "commons/util";
import {
  fetchCpeGroups,
  resizeCanvas
} from "../actions";
import {
  getPreviousBusinessDay
} from "commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }


  getDefaultFilters() {
    return {
      securityFilter: {
        selectedSpns: "",
        selectedSecuritySearchType: { key: "TICKER", value: "TICKER" },
        selectedTextSearchType: { key: "EXACT_MATCH", value: "EXACT_MATCH" },
        securitySearchString: "",
        isAdvancedSearch: false
      },
      selectedDate: getPreviousBusinessDay(),
      toggleSidebar: false,
      showInvalidSPNDailog: false,
      actualImpactThreshold: "0.00",
      impactFeeRateThreshold: "0.0",
      selectedAgreementTypes: []

    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
    this.props.fetchCpeGroups({agreementTypeIds:'__null__'});
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
   let payload = {agreementTypeIds: getCommaSeparatedValuesOrNullForAll(this.state.selectedAgreementTypes)}
   this.props.fetchCpeGroups(payload);
   this.props.resizeCanvas();
  }

render() {
  let sidebar = (<React.Fragment>
  <Sidebar>
    <AgreementTypeFilter
      onSelect={this.onSelect}
      selectedData={this.state.selectedAgreementTypes}
      multiSelect={true}
    />
    <ColumnLayout>
      <FilterButton
        onClick={this.handleClick}
        reset={false}
        label="Search"
      />
      <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
    </ColumnLayout>
  </Sidebar>
  </React.Fragment>);
  return sidebar;
}
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCpeGroups,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(SideBar);
