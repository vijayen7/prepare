import { connect } from "react-redux";
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { getCpeGroupSearchColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { Layout, Card } from "arc-react-components";
import GridContainer from 'commons/components/GridWithCellClick';
import Message from "commons/components/Message";
import Table from "commons/components/Table";
import FilterButton from 'commons/components/FilterButton';
import Dialog from "commons/components/Dialog";
import _ from "lodash";
import {
  fetchCpeGroups,
  fetchCpeGroupById,
  fetchCpeList,
  resizeCanvas,
  setSelectedCpeData,
  showPopUp,
  deleteCpeGroup,
  setDeleteToggle,
  setUnderlyingCpeToggle,
  resetUnderlyingCpeToggle,
  resetSelectedCpeData,
  setEditToggle,
  resetEditToggle,
  resetActionMessage
} from "../actions";
import CpePopUp from "./cpePopUp";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.addCpeGroup = this.addCpeGroup.bind(this);
    this.loadActionMessageDialogs = this.loadActionMessageDialogs.bind(this);
    this.closeMessageDialog = this.closeMessageDialog.bind(this);
    this.state = {
      cpeGroupName: ""
    };

  }

  componentDidUpdate(prevProps) {
    if (prevProps.data.cpeGroup !== this.props.data.cpeGroup) {
      this.props.resetUnderlyingCpeToggle();
    }
  }

  onCellClickHandler = args => {

    let ele = args.event.target;
    let { role } = ele.dataset;

    if (role === 'edit' || role === 'delete') {
      this.props.setSelectedCpeData(args.item);
      this.props.showPopUp();
    }

    if (args.colId === 'groupId' || role === 'edit' || role === 'delete') {
      const parameters = {
        clickedCpeGroupId: args.item.groupId.toString()
      };
      this.props.fetchCpeGroupById(parameters, args.item.groupName);
    }

    if (role === 'delete') {
      this.props.resetEditToggle();
      this.props.setDeleteToggle();
      this.props.resetUnderlyingCpeToggle();
    }

    if (role === 'edit') {
      this.props.setEditToggle();
    }

    if (args.colId === 'groupId' || role === 'edit') {
      this.props.setUnderlyingCpeToggle();
      this.setState({
        cpeGroupName: args.item.groupName
      });
    }

  }


  addCpeGroup = args => {
    this.props.resetUnderlyingCpeToggle();
    this.props.resetSelectedCpeData();
    this.props.resetEditToggle();
    this.props.showPopUp();
  }

  loadActionMessageDialogs() {
    if ((this.props.deleteStatus === 0) && (this.props.editStatus === 0) && (this.props.addStatus === 0)) {
      return (
        <React.Fragment />
      );
    }
    var status = false;
    var message = "";
    if (this.props.deleteStatus === 1 || this.props.editStatus === 1 || this.props.addStatus === 1) { status = true; }
    if (this.props.deleteStatus === -1 || this.props.editStatus === -1 || this.props.addStatus === -1) { status = false; }
    if (status) {
      if (this.props.deleteStatus === 1) { message = "CPE Group deleted successfully."; }
      else if (this.props.editStatus === 1) { message = "CPE Group edited successfully."; }
      else if (this.props.addStatus === 1) { message = "CPE Group added successfully" }
    } else {
      if (this.props.deleteStatus === -1) { message = "Unable to delete CPE Group."; }
      else if (this.props.editStatus === -1) { message = "Unable to edit CPE Group."; }
      else if (this.props.addStatus === -1) { message = "Cannot add CPE Group" }
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={this.props.deleteStatus !== 0 || this.props.editStatus !== 0 || this.props.addStatus !== 0}
          title={status ? "SUCCESS" : "FAILURE"}
          onClose={this.closeMessageDialog}
        >
          <Message error={!status} success={status} messageData={message} />
          <FilterButton
            onClick={this.closeMessageDialog}
            reset={false}
            label="OK"
          />
        </Dialog>
      </React.Fragment>
    );
  }

  closeMessageDialog() {
  this.props.resetActionMessage();
  let payload = {agreementTypeIds: '__null__'}
  this.props.fetchCpeGroups(payload);
  this.props.resizeCanvas();
}


  renderGridData() {
    return (
      <GridContainer
        data={this.props.data}
        gridId="CpeGroupData"
        gridColumns={getCpeGroupSearchColumns()}
        gridOptions={gridOptions()}
        onCellClick={this.onCellClickHandler}
        resizeCanvas={this.props.isResizeCanvas}
        fill={true}
      />
    )
  }

  renderCpeTables(dataObject) {
    if (_.isEmpty(dataObject) || (!this.props.isShowUnderlyingCpe)) {
      return <React.Fragment />;
    }

    let message = "";
    const dataList = dataObject.cpeFamilyList;
    const displayName = this.state.cpeGroupName;

    if (dataList.length <= 0) {
      message = `No Counterparty Family Found for ${displayName}`;
      return (
        <Layout.Child>
          <Message messageData={message} />
        </Layout.Child>
      );
    }
    message = `Counterparty Family(s) for ${displayName}`;
    let tables;
    tables = (
      <React.Fragment>
        <Message messageData={message} />
        <hr />
        <Layout isRowType>
          <Layout.Child size="content">
            <div style={{ paddingBottom: "15px" }}>
              <Layout isColumnType>
                <Layout.Child size={10}>
                  <div className="size--content"
                    style={{ width: "90%", display: "block" }}>
                    <Table
                      labels={["Counterparty Family(s)"]}
                      content={dataList}
                      dataKeys={["name"]}
                      style={{ width: "25%", display: "table" }}
                    />
                  </div>

                </Layout.Child>

              </Layout>
            </div>
          </Layout.Child>
        </Layout>
      </React.Fragment>

    );
    return tables;

  }

  renderPopUp() {
    return (
      <React.Fragment>
        <CpePopUp />
      </React.Fragment>
    );
  }

  renderMainContainer() {
    if ('message' in this.props.data) {
      return <Message className="margin--top" messageData={this.props.data.message} />;
    }

    if (this.props.data.length <= 0) {
      return <Message className="margin--top" messageData="Search to load data" />;
    }
    return (
      <React.Fragment>
        <Layout isColumnType>
          <Layout.Child size='49%'>
            <Card title="Counterparty Family Group">
              {this.renderGridData()}
            </Card>
          </Layout.Child>
          <Layout.Child size='2%'>
            <React.Fragment />
          </Layout.Child >
          <Layout.Child size='49%'>
            <Card>
              {this.renderCpeTables(this.props.dataById)}
            </Card>
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <Layout.Child>
            <Layout>
              <Layout.Child size={1}>
                <FilterButton
                  onClick={this.addCpeGroup}
                  reset={false}
                  label="Add New Counterparty Family Group"
                />
              </Layout.Child>
              <Layout.Child size={12}>
                {this.renderMainContainer()}
              </Layout.Child>
            </Layout>

            {this.renderPopUp()}

          </Layout.Child>
          <Layout.Child>
            {this.loadActionMessageDialogs()}
          </Layout.Child>

        </Layout>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCpeGroups,
      fetchCpeGroupById,
      fetchCpeList,
      resizeCanvas,
      setSelectedCpeData,
      deleteCpeGroup,
      setDeleteToggle,
      setUnderlyingCpeToggle,
      resetUnderlyingCpeToggle,
      resetSelectedCpeData,
      showPopUp,
      setEditToggle,
      resetEditToggle,
      resetActionMessage
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    data: state.cpeGroupData.cpeGroup,
    dataById: state.cpeGroupData.cpeGroupById,
    cpeList: state.cpeGroupData.cpeList,
    isShowUnderlyingCpe: state.cpeGroupData.showUnderlyingCpe,
    selectedCpeList: state.cpeGroupData.selectedCpeList,
    isEditClicked: state.cpeGroupData.isEditClicked,
    deleteStatus: state.cpeGroupData.deleteCpeGroup,
    editStatus: state.cpeGroupData.saveCpeGroup,
    addStatus: state.cpeGroupData.addCpeGroup,
    isResizeCanvas: state.cpeGroupData.resizeCanvas
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
