import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  FETCH_CPE_GROUPS,
  FETCH_CPE_GROUP_BY_ID,
  FETCH_CPE_LIST,
  SAVE_CPE_GROUP,
  DELETE_CPE_GROUP,
  ADD_CPE_GROUP
} from "./constants";
import {
  RESIZE_CANVAS,
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from "commons/constants";
import {
  getCpeGroups,
  getCpeGroupById,
  getCpeList,
  updateCounterpartyReportingGroup,
  deleteCounterpartyReportingGroup,
  addCounterpartyReportingGroup
} from "./api"


function* fetchCpeGroups(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCpeGroups, action.payload);
    yield put({ type: `${FETCH_CPE_GROUPS}_SUCCESS`, data });
    yield put({ type: RESIZE_CANVAS})
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CPE_GROUPS}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchCpeGroupsById(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCpeGroupById, action.payload);
    yield put({ type: `${FETCH_CPE_GROUP_BY_ID}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CPE_GROUP_BY_ID}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveCpeGroup(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(updateCounterpartyReportingGroup, action.payload);
    yield put({ type: `${SAVE_CPE_GROUP}_SUCCESS`, data });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_CPE_GROUP}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchCpeList(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCpeList, action.payload);
    yield put({ type: `${FETCH_CPE_LIST}_SUCCESS`, data });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CPE_LIST}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deleteCpeGroup(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(deleteCounterpartyReportingGroup, action.payload);
    yield put({ type: `${DELETE_CPE_GROUP}_SUCCESS`, data });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_CPE_GROUP}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* addCpeGroup(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(addCounterpartyReportingGroup, action.payload);
    yield put({ type: `${ADD_CPE_GROUP}_SUCCESS`, data });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${ADD_CPE_GROUP}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* cpeGropuSaga() {
  yield [
    takeEvery(FETCH_CPE_GROUPS, fetchCpeGroups)
  ];
}

export function* cpeGroupByIdSaga() {
  yield [
    takeEvery(FETCH_CPE_GROUP_BY_ID, fetchCpeGroupsById)
  ];
}

export function* cpeListSaga() {
  yield [takeEvery(FETCH_CPE_LIST, fetchCpeList)];
}

export function* updateCpeSaga() {
  yield [takeEvery(SAVE_CPE_GROUP, saveCpeGroup)];
}


export function* deleteCpeGroupSaga() {
  yield [takeEvery(DELETE_CPE_GROUP, deleteCpeGroup)];
}

export function* addCpeGroupSaga() {
  yield [takeEvery(ADD_CPE_GROUP, addCpeGroup)];
}


function* conterPartyGroupSaga() {
  yield all([cpeGropuSaga(), cpeGroupByIdSaga(), cpeListSaga(), updateCpeSaga(), deleteCpeGroupSaga(), addCpeGroupSaga()]);
}

export default conterPartyGroupSaga;
