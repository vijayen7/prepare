import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
import { fetchURL } from "commons/util";
export let url = "";

export function getEntityLiquidityData(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/entityLiquidityDataService/getEntityLiquidityData?${paramString}&format=JSON`;
  return fetchURL(url);
}
