import React, { useEffect } from "react";
import moment from "moment"
import DateFilter from "commons/container/DateFilter";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import EntityLiquidityDataStore from '../EntityLiquidityDataStore';
import SaveSettingsManager from "../../commons/components/SaveSettingsManager";
import queryString from "query-string";

const SideBar = inject("entityLiquidityDataStore")(observer((props) => {
  store = props.entityLiquidityDataStore;

  useEffect(() => {
    const values = queryString.parse(props.location.search);
    if (values.date ) {
      let filters = {
        selectedDate: moment(values.date, 'YYYYMMDD').format('YYYY-MM-DD')
      };
      store.setFilters(filters);
      handleClick();
    }
  }, []);

  const onSelect = (params) => {
    store.onSelect(params);
  };

  const handleClick = () => {
    store.loadEntityLiquidityData();
  };

  const getSelectedFiltersForSaveSettings = () => {
    let {selectedDate, ...searchFilters} = store.filters;
    return searchFilters;
  }

  const applySavedFilters = (savedFilters) => {
    store.setFilters(savedFilters);
  };

  const handleCopySearch = () => {
      store.copySearchUrl(window.location.origin);
  };

  return (
    <Sidebar>
      <div style={{'display':'none'}}>
        <SaveSettingsManager
         selectedFilters={getSelectedFiltersForSaveSettings()}
         applySavedFilters={applySavedFilters}
         applicationName="entityLiquidityFilter"
       />
      </div>
      <DateFilter
        onSelect={onSelect}
        stateKey="selectedDate"
        data={store.filters.selectedDate}
        label="Date"
      />
      <ColumnLayout>
        <FilterButton className="button--tertiary margin--horizontal--double" onClick={handleCopySearch} label="Copy Search URL" />
        <FilterButton onClick={handleClick} label="Search" />
      </ColumnLayout>
    </Sidebar>
  );
}));

SideBar.propTypes = {
  entityLiquidityDataStore: PropTypes.instanceOf(EntityLiquidityDataStore).isRequired
};

export default SideBar;
