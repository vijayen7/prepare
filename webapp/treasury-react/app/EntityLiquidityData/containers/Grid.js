import React, { Component } from "react";
import { Layout } from 'arc-react-components';
import { gridColumns } from "../grid/columnConfig";
import { gridConfig } from "../grid/gridConfig";
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import EntityLiquidityDataStore from '../EntityLiquidityDataStore';
import ArcDataGrid from "arc-data-grid";
import Message from "commons/components/Message";

const Grid = inject("entityLiquidityDataStore")(observer((props) => {
 let entityLiquidityData = {};
 let displayMessage = false;
 const loadGridData = () => {
    const data = props.entityLiquidityDataStore.entityLiquidityData;
    displayMessage = true;
    if (props.entityLiquidityDataStore.entityLiquidityDataStatus.errorOccurred) {
      return errorMessage();
    } else if (props.entityLiquidityDataStore.entityLiquidityDataStatus.noDataFound ||
      data == null) {
      return noDataFound();
    } else if (Object.keys(data).length === 0) {
      return initialMessage();
    } else {
      displayMessage = false;
      entityLiquidityData = data;
    }
  }

  const renderGridData = () => {
    const gridMessage = loadGridData();
    if (displayMessage) {
      return gridMessage;
    } else {
      return (
        <Layout>
          <Layout.Child childId="gridHeader"  className="padding">
            <div className="float--right" >
              <button className="margin--right" onClick={props.entityLiquidityDataStore.createReport}>
                Create Report
              </button>
            </div>
          </Layout.Child>
          <Layout.Child childId="mainGrid">
            <ArcDataGrid
              rows={entityLiquidityData.data}
              columns={gridColumns(entityLiquidityData.entities)}
              getRowId={(row, _) => row.id}
              configurations={gridConfig()}
            />
          </Layout.Child>
        </Layout>
       );
    }
  }

  return (
    <div className="padding">
      {renderGridData()}
    </div>
  );
}));

Grid.propTypes = {
  entityLiquidityDataStore: PropTypes.instanceOf(EntityLiquidityDataStore).isRequired
};

const initialMessage = () => {
  return (
    <div>
      <Message messageData="Search to load data" />
    </div>
  );
};

const noDataFound = () => {
  return (
    <div>
      <Message messageData="No Data Found" />
    </div>
  );
};

const errorMessage = () => {
  return (
    <div>
      <Message messageData="Failed to load data" />
    </div>
  );
};

export default Grid;
