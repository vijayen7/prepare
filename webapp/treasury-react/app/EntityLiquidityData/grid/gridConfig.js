export function gridConfig() {
  var config = {
     clickableRows: true,
     getExportFileName: ({ fileType, all }) => {
       return 'Entity Liquidity Data';
     }
  };
  return config;
}
