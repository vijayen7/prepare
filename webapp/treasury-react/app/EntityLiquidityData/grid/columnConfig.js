import React from 'react';
import { getFormattedFloatValue } from '../../commons/util';

export function gridColumns(ownershipEntityArr) {
  var columns = [];

  for (var i = 0; i < ownershipEntityArr.length; i++) {
    columns.push(
      {
        identity: ownershipEntityArr[i],
        header: ownershipEntityArr[i],
        Cell: (value) => { return FloatCell(value) },
        width: 150,
      }
    );
  }

  columns.unshift({
    identity: "name",
    header: "",
    Cell: (value) => { return StringCell(value) },
    width: 150,
  });

  return columns;
}

function StringCell(value) {
  return <div>{value ? value : ''}</div>;
}

function FloatCell(value) {
  return <div>{getFormattedFloatValue(value)}</div>;
}
