import { action, flow, observable } from 'mobx';
import * as EntityLiquidityDataAPI from './api';
import { getPreviousBusinessDay, openReportManager, showToastService } from 'commons/util';
const copy = require('clipboard-copy')

export const INITIAL_FILTERS = {
  selectedDate: getPreviousBusinessDay()
};

const DATA_STATUS = {
  searchInProgress: false,
  errorOccurred: false,
  noDataFound: false,
};

const INITIAL_ENTITY_LIQUIDITY_DATA = {};

const idToName = {
  "mmfCash" : "MMF/Cash",
  "ustGovtBonds" : "US Govt Bonds",
  "nonUstGovtBonds" : "Non US Govt Bonds",
  "brokerCash" : "Broker Cash",
  "totalCash" : "Total Cash",
  "totalMarginUsage" : "Total Margin Usage",
  "netAssetValue" : "Net Asset Value",
  "percentageCash" : "% Cash",
  "percentageCashExcludingUsts" : "% Cash Excluding UST/JGB"
};

const entityIdToName = {
  1765148 : "ORNTPORT",
  1765253 : "ORNTHOLD",
  1769190 : "BXS",
  1769614 : "BXU"
}

function transformData(responseData){
    if(responseData == null || responseData.length === 0){
      return null;
    }
    var data= [];
    var entities = new Set();
    responseData.forEach(function(dataObj){
      entities.add(entityIdToName[dataObj['entityId']]);
    });
    Object.keys(idToName).forEach(function(id) {
        var obj={};
        obj['id']=id;
        obj['name']=idToName[id];
        responseData.forEach(function(dataObj){
            obj[entityIdToName[dataObj['entityId']]]=dataObj[id].toString();
        })
        obj['entities']=[...entities];
        data.push(obj);
    })
    const transformedData = {'data':data,'entities':[...entities]};
    return transformedData;
 }

export default class EntityLiquidityDataStore {

  @observable
  toggleSidebar = false;

  @observable
  entityLiquidityData = INITIAL_ENTITY_LIQUIDITY_DATA;

  @observable
  filters = INITIAL_FILTERS;

  @observable
  entityLiquidityDataStatus = DATA_STATUS;

  @action.bound
  setToggleSidebar = (value) => {
    this.toggleSidebar = value;
  };

  @action
  setFilters(newFilters) {
    this.filters = newFilters;
  }

  copySearchUrl(location) {
    let url = location + `/treasury/lcm/entityLiquidityData.html?date=${this.filters.selectedDate}`;
    copy(url);
    showToastService("Search URL copied to clipboard");
  }

  createReport = () => {
    let url = `/treasury/service/entityLiquidityDataService/getEntityLiquidityData?
    date=${this.filters.selectedDate}&inputFormat=properties&format=json`;
    openReportManager(url, null);
  };

  onSelect = (params) => {
      const { key, value } = params;
      const selectedFilters = { ...this.filters };
      selectedFilters[key] = value;
      this.setFilters(selectedFilters);
  }

   loadEntityLiquidityData = flow(
      function* loadEntityLiquidityData() {
        this.entityLiquidityDataStatus.searchInProgress = true;

        try {
          var payload = {
            date: this.filters.selectedDate
          };

          const responseData = yield EntityLiquidityDataAPI.getEntityLiquidityData(payload);
          const entityLiquidityData = transformData(responseData);
          this.entityLiquidityData = entityLiquidityData;
          this.entityLiquidityDataStatus = {
            searchInProgress: false,
            errorOccurred: false,
            noDataFound: (entityLiquidityData == null || Object.keys(entityLiquidityData).length === 0) ? true : false
          };
        } catch (e) {
          this.entityLiquidityDataStatus = {
            searchInProgress: false,
            errorOccurred: true
          };
        }
      }.bind(this)
    );
}
