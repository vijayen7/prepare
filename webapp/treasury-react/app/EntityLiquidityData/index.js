import React from "react";
import AppHeader from "arc-header";
import { Layout } from 'arc-react-components';
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import PropTypes from 'prop-types';
import { inject, observer } from "mobx-react";
import { ReactLoader } from "../commons/components/ReactLoader";
import EntityLiquidityDataStore from './EntityLiquidityDataStore';

const EntityLiquidityData  = inject("entityLiquidityDataStore")(observer((props) => {
  store = props.entityLiquidityDataStore;
  return (
    <React.Fragment>
      <ReactLoader
        inProgress={
          store.entityLiquidityDataStatus.searchInProgress
        }
      />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} />
        <Layout
          isColumnType={true}
          className="border"
          style={{ height: "100%" }}
        >
          <Layout.Child
            childId="sideBar"
            size={2}
            title="Search Filter"
            collapsible
            collapsed={EntityLiquidityDataStore.collapseSideBar}
            onChange={EntityLiquidityDataStore.setCollapsed}
          >
            <SideBar location={props.location}/>
          </Layout.Child>
          <Layout.Divider childId="sideBarDivider" isResizable />
          <Layout.Child childId="dataDetail" size={9} >
            <Grid />
          </Layout.Child>
        </Layout>
      </div>
    </React.Fragment>
  );
  }
));

EntityLiquidityData.propTypes = {
  entityLiquidityDataStore: PropTypes.object.isRequired
};

export default EntityLiquidityData ;
