import React, { Component } from "react";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";
import Loader from "../commons/container/Loader";
import { inject, observer } from "mobx-react";
import { ReactLoader } from "../commons/components/ReactLoader";
import PropTypes from "prop-types";
import queryString from "query-string";
import { getFilterListFromCommaSeparatedString } from "../commons/util";

@inject("collateralTermStore")
@observer
class CollateralTerm extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired
  };

  store = this.props.collateralTermStore;

  componentDidMount() {
    const values = queryString.parse(this.props.location.search);
    let filters = {
      selectedLegalEntities: getFilterListFromCommaSeparatedString(values.legalEntityIds),
      selectedCpes: getFilterListFromCommaSeparatedString(values.cpeIds),
      selectedAgreementTypes: getFilterListFromCommaSeparatedString(values.agreementTypeIds),
      selectedAccountTypes: getFilterListFromCommaSeparatedString(values.accountTypeIds),
    };
    if ((filters.selectedLegalEntities && filters.selectedLegalEntities.length) ||
      (filters.selectedCpes && filters.selectedCpes.length) ||
      (filters.selectedAgreementTypes && filters.selectedAgreementTypes.length) ||
      (filters.selectedAccountTypes && filters.selectedAccountTypes.length)) {
        this.store.setFilters(filters);
        this.store.toggleToggleSidebarFlag();
        this.store.getCollateralTerms();
    }
  }

  render() {
    return (
      <div>
        <Loader />
        <ReactLoader
          inProgress={
            this.store.searchStatus.inProgress ||
            this.store.editDialogStore.inProgress ||
            this.store.editDialogStore.saveInProgress ||
            this.store.editDialogStore.validationInProgress
          }
        />
        <div className="layout--flex--row">
          <arc-header
            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          ></arc-header>
          <div className="layout--flex--row">
            <div className="layout--flex padding--top">
              <div className="size--content padding--right">
                <SideBar location={this.props.location}  />
              </div>
              <Grid />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CollateralTerm;
