import { action, flow, observable } from "mobx";
import { fetchCollateralTerms } from "./api";
import { getSelectedIdList, getCommaSeparatedKeysFromFilterList } from "../commons/util";
import EditDialogStore from "./container/EditDialog/EditDialogStore";
import { ACCOUNT_TYPES, ACCOUNT_TYPE_IDS } from "./constants";
import { YES, NO, NOT_AVAILABLE, FETCH_INTERACTION_SUMMARY_DATA } from "../commons/constants";

const copy = require('clipboard-copy')

const INITIAL_FILTERS = {
  selectedLegalEntities: [],
  selectedCpes: [],
  selectedAgreementTypes: [],
  selectedAccountTypes: []
};

const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  error: false
};

const ACCOUNT_TYPE_DATA = [
  { key: 1, value: ACCOUNT_TYPES.VM_PLUS_IM },
  { key: 2, value: ACCOUNT_TYPES.IM },
  { key: 3, value: ACCOUNT_TYPES.VM },
  { key: 4, value: ACCOUNT_TYPES.SIMM_LE },
  { key: 5, value: ACCOUNT_TYPES.SIMM_CPE }
];

export default class CollateralTermStore {
  constructor() {
    this.editDialogStore = new EditDialogStore(this);
  }

  @observable
  filters = INITIAL_FILTERS;

  @observable
  searchStatus = INITIAL_SEARCH_STATUS;

  @observable
  resizeCanvasFlag = false;

  @observable
  toggleSidebarFlag = false;

  @observable
  collateralTerms = [];

  @observable
  collateralTermsOnAccountType = [];

  @observable
  accountTypeData = ACCOUNT_TYPE_DATA;

  @observable
  showPopUp = false;

  copySearchUrl(location) {
    let url = location + `/treasury/comet/collateralTerms.html?
    legalEntityIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedLegalEntities)}
    &cpeIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedCpes)}
    &agreementTypeIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedAgreementTypes)}
    &accountTypeIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedAccountTypes)}`;
    copy(url);
  }

  onSelect = (params) => {
    const { key, value } = params;
    const newFilters = { ...this.filters };
    newFilters[key] = value;
    this.setFilters(newFilters);
  }

  @action
  setFilters(newFilters) {
    this.filters = newFilters;
  }

  @action
  resetFilters() {
    this.filters = INITIAL_FILTERS;
  }

  @action.bound
  resizeCanvas() {
    setTimeout(() => {
      this.resizeCanvasFlag = !this.resizeCanvasFlag;
    }, 0);
  }

  @action.bound
  toggleShowPopUp() {
    if (this.editDialogStore.readOnly) {
      this.editDialogStore.toggleReadOnly();
    }
    this.showPopUp = !this.showPopUp;
  }

  @action.bound
  toggleToggleSidebarFlag() {
    this.toggleSidebarFlag = !this.toggleSidebarFlag;
  }

  @action
  getFlattenedData(data) {
    let flatennedData = [];
    if (data.length !== 0) {
      data.forEach(item => {
        let collateralTerm = {};
        collateralTerm = {
          agreementId: item.agreement.agreementId,
          legalEntityId: item.agreement.dESCOEntityId,
          cpeId: item.agreement.cpeId,
          agreementTypeId: item.agreement.agreementTypeId,
          accountType: item.accountType,
          agreementName: item.agreementName,
          legalEntity: item.legalEntity,
          counterPartyEntity: item.counterPartyEntity,
          agreementType: item.agreementType,
          mnaLegalEntity: item.mnaLegalEntity ? item.mnaLegalEntity : NOT_AVAILABLE,
          mnaCpe: item.mnaCpe ? item.mnaCpe : NOT_AVAILABLE,
          comment: item.comment,
          disclaimer: item.disclaimer ? item.disclaimer.displayName : null,
          reportingCurrency: item.reportingCurrency.abbreviation,
          roundingConvention: item.roundingConvention,
          legalEntityRoundingAmount:
            item.cashCollateralTerm.legalEntityRoundingAmount,
          cpeRoundingAmount: item.cashCollateralTerm.cpeRoundingAmount,
          legalEntityMinTransferAmount:
            item.cashCollateralTerm.legalEntityMinTransferAmount,
          cpeMinTransferAmount: item.cashCollateralTerm.cpeMinTransferAmount,
          legalEntityThreshold: item.cashCollateralTerm?.legalEntityThreshold,
          cpeThreshold: item.cashCollateralTerm?.cpeThreshold,
          legalEntityContactDetail: item.legalEntityContactDetail
            ? item.legalEntityContactDetail.emailId
            : null,
          cpeContactDetail: item.cpeContactDetail
            ? item.cpeContactDetail.emailId
            : null,
          legalEntityWireAccounts:
            item.cashCollateralTerm.leWireInstructions &&
              item.cashCollateralTerm.leWireInstructions.length
              ? item.cashCollateralTerm.leWireInstructions
                .map(x => x.displayName)
                .join(";")
              : "",
          cpeWireAccounts:
            item.cashCollateralTerm.cpeWireInstructions &&
              item.cashCollateralTerm.cpeWireInstructions.length
              ? item.cashCollateralTerm.cpeWireInstructions
                .map(x => x.displayName)
                .join(";")
              : "",
          agreementLevelThreshold: item.cashCollateralTerm.agreementLevelThreshold
            ? item.cashCollateralTerm.agreementLevelThreshold : 0,
          sourceCollateralCustodianAccounts:
            item.physicalCollateralTerm &&
            item.physicalCollateralTerm.sourceCollateralCustodianAccounts
              ? item.physicalCollateralTerm.sourceCollateralCustodianAccounts
              : NOT_AVAILABLE,
          destinationCollateralCustodianAccounts:
            item.physicalCollateralTerm &&
            item.physicalCollateralTerm.destinationCollateralCustodianAccounts
              ? item.physicalCollateralTerm
                .destinationCollateralCustodianAccounts
              : NOT_AVAILABLE,
          portfolioEvaluators:
              item.physicalCollateralTerm &&
              item.physicalCollateralTerm.portfolioEvaluators &&
              item.physicalCollateralTerm.portfolioEvaluators.length
                ? item.physicalCollateralTerm.portfolioEvaluators
                    .map((x) => x.portfolioEvaluatorName)
                    .join(";")
                : "",
          positionEvaluators:
              item.physicalCollateralTerm &&
              item.physicalCollateralTerm.positionEvaluators &&
              item.physicalCollateralTerm.positionEvaluators.length
                ? item.physicalCollateralTerm.positionEvaluators
                    .map((x) => x.positionEvaluatorName)
                    .join(";")
                : "",
          applicableRegimes:
              item.applicableRegimes &&
              item.applicableRegimes.length
                ? item.applicableRegimes
                    .map((x) => x.regimeName)
                    .join(";")
                :"",
          isSegregated : (item.accountTypeId == ACCOUNT_TYPE_IDS.VM_ID  || item.accountTypeId == ACCOUNT_TYPE_IDS.IM_ID) ? YES : NO,
          canPostSecuritiesAsCollateral : item.physicalCollateralTerm ? YES : NO,
          useSegMethodology : item.segMethodologyApplicable ? YES : NO,
          triPartyCustodianAccount :
              item.tripartyCustodianAccountName
                ? item.tripartyCustodianAccountName
                :"",

          isTripartyArrangement: item.isTripartyArrangement ? YES : NO,
          tripartyAgreementType: item.tripartyAgreementType
            ? item.tripartyAgreementType
            : NOT_AVAILABLE,
          tripartyLegalEntity: item.tripartyLegalEntity
            ? item.tripartyLegalEntity
            : NOT_AVAILABLE,
          tripartyCPE: item.tripartyCPE ? item.tripartyCPE : NOT_AVAILABLE,
          rehypothecation: item.physicalCollateralTerm?.rehypothecation ? YES : NO,
          populateAsm: item.physicalCollateralTerm?.populateAsm ? YES: NO,
          marginMethodology: item.marginMethodology,
          securityPledged: item.physicalCollateralTerm?.securityPledged ? YES : NO,
          ratingsComparisonEnabled: item.physicalCollateralTerm?.ratingsComparisonEnabled ? YES : NO,
          id: item.id
        };
        flatennedData.push(collateralTerm);
      });
    }
    return flatennedData;
  }

  getCollateralTerms = flow(
    function* getCollateralTerms() {
      this.searchStatus = INITIAL_SEARCH_STATUS;
      this.searchStatus.inProgress = true;

      if (this.filters.selectedAccountTypes.length === 0) {
        this.filters.selectedAccountTypes = ACCOUNT_TYPE_DATA;
      }

      try {
        let input = {
          legalEntityIds: getSelectedIdList(this.filters.selectedLegalEntities),
          cpeIds: getSelectedIdList(this.filters.selectedCpes),
          agreementTypeIds: getSelectedIdList(
            this.filters.selectedAgreementTypes
          ),
          enrichWithRefData: true
        };
        input["@CLASS"] =
          "deshaw.treasury.common.model.comet.LcmPositionDataParam";

        const collateralTerms = yield fetchCollateralTerms(input);
        collateralTerms.forEach(item => (item.id = item.collateralTermId));
        this.collateralTerms = collateralTerms;

        let accountTypeIds = this.filters.selectedAccountTypes.map(x => x.key);

        if (accountTypeIds.length !== 0) {
          this.collateralTermsOnAccountType = collateralTerms.filter(item =>
            accountTypeIds.includes(item.accountTypeId)
          );
        } else {
          this.collateralTermsOnAccountType = collateralTerms;
        }

        this.searchStatus.inProgress = false;
      } catch (e) {
        this.searchStatus = {
          inProgress: false,
          error: true
        };
      }
    }.bind(this)
  );
}
