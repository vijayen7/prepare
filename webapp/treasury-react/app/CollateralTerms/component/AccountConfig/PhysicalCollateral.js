import React from "react";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import InputFilter from "../../../commons/components/InputFilter";
import Column from "../../../commons/components/Column";
import { BASE_URL } from "../../../commons/constants";
import { PhysicalCollateralEvaluators } from "../../component/PhysicalCollateralEvaluators";

export const PhysicalCollateral = (props) => {
  return (
    <React.Fragment>
      <SingleSelectFilter
        onSelect={props.onSelect}
        selectedData={props.filters.selectedSourceCollateralCAs}
        stateKey="selectedSourceCollateralCAs"
        data={props.sourceCollateralCustodianAccounts}
        label="Source Collateral Custodian Accounts*"
        horizontalLayout
      />
      <SingleSelectFilter
        onSelect={props.onSelect}
        selectedData={props.filters.selectedDestinationCollateralCAs}
        stateKey="selectedDestinationCollateralCAs"
        data={props.destinationCollateralCustodianAccounts}
        label="Destination Collateral Custodian Accounts*"
        horizontalLayout
      />
      <PhysicalCollateralEvaluators
        onSelect={props.onSelect}
        filters={props.filters}
        portfolioEvaluators={props.portfolioEvaluators}
        positionEvaluators={props.positionEvaluators}
      />
      <Column>
        {props.rehypothecationApplicable && (
          <CheckboxFilter
            onSelect={props.onSelect}
            label="Rehypothecation"
            defaultChecked={props.filters.isRehypothecation}
            stateKey="isRehypothecation"
            style="left"
          />
        )}
        <div>
          <label>Eligible Collateral Configuration</label>
          <br />
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={`${BASE_URL}lcm/PhysicalCollateralRules.html?legalEntityId=${props.agreementDetailIds.legalEntityId}&cpeId=${props.agreementDetailIds.cpeId}&agreementTypeId=${props.agreementDetailIds.agreementTypeId}`}
          >
            Link
          </a>
        </div>
      </Column>
      <Column>
        <CheckboxFilter
          onSelect={props.onSelect}
          label="Populate ASM"
          defaultChecked={props.filters.isPopulateAsm}
          stateKey="isPopulateAsm"
          style="left"
        />
         <CheckboxFilter
          onSelect={props.onSelect}
          label="Securities Pledged"
          defaultChecked={props.filters.isSecurityPledged}
          stateKey="isSecurityPledged"
          style="left"
        />
        <CheckboxFilter
          onSelect={props.onSelect}
          label="Ratings Comparison"
          defaultChecked={props.filters.isRatingsComparisonEnabled}
          stateKey="isRatingsComparisonEnabled"
          onHoverDisplay="Compare position/issuer ratings and honor rule corresponding to the minimum rating value"
          style="left"
        />
      </Column>
    </React.Fragment>
  );
};
