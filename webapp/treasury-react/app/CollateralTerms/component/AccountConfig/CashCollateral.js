import React from "react";
import MultiSelectFilter from "../../../commons/filters/components/MultiSelectFilter";

export const CashCollateral = (props) => {
  return (
    <React.Fragment>
      <MultiSelectFilter
        onSelect={props.onSelect}
        selectedData={props.filters.selectedLEWireAccounts}
        stateKey="selectedLEWireAccounts"
        data={props.legalEntityWireAccounts}
        label="LE Wire Accounts"
        readonly={props.readonly ? props.readonly : null}
        disabled={props.readonly ? props.readonly : null}
        horizontalLayout
      />
      <MultiSelectFilter
        onSelect={props.onSelect}
        selectedData={props.filters.selectedCPEWireAccounts}
        stateKey="selectedCPEWireAccounts"
        data={props.cpeWireAccounts}
        label="CPE Wire Accounts"
        readonly={props.readonly ? props.readonly : null}
        disabled={props.readonly ? props.readonly : null}
        horizontalLayout
      />
    </React.Fragment>
  );
};
