import React, { Component } from "react";
import { Dialog } from "arc-react-components";
import Message from "../../commons/components/Message";

class PopUpMessage extends Component {
  render() {
    return (
      <Dialog
        isOpen={this.props.isOpen}
        onClose={this.props.onClose}
        title={this.props.title}
        footer={
          <React.Fragment>
            <button onClick={this.props.onClose}>OK</button>
          </React.Fragment>
        }
      >
        <Message messageData={this.props.message} />
      </Dialog>
    );
  }
}

export default PopUpMessage;
