import React from "react";
import MultiSelectFilter from "../../commons/filters/components/MultiSelectFilter";

export const PhysicalCollateralEvaluators = (props) => {
  return (
    <React.Fragment>
      <MultiSelectFilter
        onSelect={props.onSelect}
        selectedData={props.filters.selectedPortfolioEvaluatorsTypes}
        stateKey="selectedPortfolioEvaluatorsTypes"
        data={props.portfolioEvaluators}
        label="Portfolio Evaluators"
        horizontalLayout
      />
      <MultiSelectFilter
        onSelect={props.onSelect}
        selectedData={props.filters.selectedPositionEvaluatorsTypes}
        stateKey="selectedPositionEvaluatorsTypes"
        data={props.positionEvaluators}
        label="Position Evaluators"
        horizontalLayout
      />
    </React.Fragment>
  );
};
