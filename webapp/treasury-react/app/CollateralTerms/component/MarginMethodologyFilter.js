import React from "react";
import SingleSelectFilter from "../../commons/filters/components/SingleSelectFilter";

export const MarginMethodologyFilter = (props) => {
  return (
    <SingleSelectFilter
      stateKey={props.stateKey ? props.stateKey : "selectedMarginMethodology"}
      label={props.label ? props.label : "Margin Methodology"}
      selectedData={props.selectedData}
      data={props.data ? props.data : []}
      onSelect={props.onSelect}
      horizontalLayout
    />
  );
};
