import { parseDJSrvString } from "../../commons/util";
import { linkFormatter } from "../../commons/grid/formatters";
import { numberFormatterDefaultZero } from "../../commons/grid/formatters";
import { decamelize } from "../../commons/util";

export const gridColumns = () => {
  const actionColumns = [
    {
      id: "edit",
      name: "",
      field: "edit",
      toolTip: "Actions",
      type: "text",
      formatter(row, cell, value, columnDef, dataContext) {
        return linkFormatter(
          row,
          cell,
          "<i class='icon-edit--block' />",
          columnDef,
          dataContext
        );
      },
      headerCssClass: "b",
      autoWidth: true
    },
    {
      id: "physicalCollateralRules",
      name: "Eligible Collateral Configuration",
      field: "physicalCollateralRules",
      toolTip: "Actions",
      type: "text",
      formatter(row, cell, value, columnDef, dataContext) {
        return linkFormatter(row, cell, "Link", columnDef, dataContext);
      },
      headerCssClass: "b",
      autoWidth: true
    }
  ];

  const columnIds = [
    "agreementId",
    "accountType",
    "agreementName",
    "legalEntity",
    "counterPartyEntity",
    "agreementType",
    "mnaLegalEntity",
    "mnaCpe",
    "comment",
    "disclaimer",
    "reportingCurrency",
    "agreementLevelThreshold",
    "roundingConvention",
    "legalEntityRoundingAmount",
    "cpeRoundingAmount",
    "legalEntityMinTransferAmount",
    "cpeMinTransferAmount",
    "legalEntityContactDetail",
    "cpeContactDetail",
    "cpeThreshold",
    "legalEntityThreshold",
    "legalEntityWireAccounts",
    "cpeWireAccounts",
    "sourceCollateralCustodianAccounts",
    "destinationCollateralCustodianAccounts",
    "isTripartyArrangement",
    "tripartyAgreementType",
    "tripartyLegalEntity",
    "tripartyCPE",
    "rehypothecation",
    "populateAsm",
    "securityPledged",
    "ratingsComparisonEnabled",
    "marginMethodology",
    "portfolioEvaluators",
    "positionEvaluators",
    "applicableRegimes",
    "isSegregated",
    "triPartyCustodianAccount",
    "canPostSecuritiesAsCollateral",
    "useSegMethodology"
  ];

  const commonColumns = [];

  columnIds.forEach(col => {
    if (col === "isTripartyArrangement") {
      let physicalCollateralRuleColumn = actionColumns.find(
        element => element.id === "physicalCollateralRules"
      );
      if (physicalCollateralRuleColumn) {
        commonColumns.push(physicalCollateralRuleColumn);
      }
    }
    const columnConfig = {
      id: col,
      name: decamelize(col, " "),
      field: col,
      toolTip: col,
      sortable: true,
      type: "text",
      autoWidth: true
    };
    if (col === "agreementId" || col.toLowerCase().includes("amount")) {
      columnConfig.type = "number";
      if (col.toLowerCase().includes("amount")) {
        columnConfig.formatter = numberFormatterDefaultZero;
      }
    }
    if (col.toLowerCase().includes("date")) {
      columnConfig.formatter = (x, y, value) => {
        const date = parseDJSrvString(value);
        if (date) {
          return date.format("YYYY-MM-DD");
        }
        return "";
      };
    }

    if(col === "securityPledged"){
      columnConfig.name = "Securities Pledged";
    }
    commonColumns.push(columnConfig);
  });

  let columns = [actionColumns.find(element => element.id === "edit")];
  columns = columns.concat(commonColumns);
  return columns;
};
