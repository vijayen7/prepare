export function getGridOptions() {
  return {
    autoHorizontalScrollBar: true,
    highlightRowOnClick: true,
    exportToExcel: true,
    asyncEditorLoading: false,
    autoEdit: false,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    configureColumns: true,
    frozenColumn: 2,
    sheetName: "Collateral Terms",
    sortList: [
      {
        columnId: "agreementId",
        sortAsc: true
      }
    ],
    cellRangeSelection: {
      showAggregations: true
    }
  };
}
