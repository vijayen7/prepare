export const ACCOUNT_TYPES = {
  VM_PLUS_IM: "VM + IM",
  IM: "IM",
  VM: "VM",
  SIMM_LE: "SIMM ( LE )",
  SIMM_CPE: "SIMM ( CPE )"
};
export const ACCOUNT_TYPE_IDS = {
  VM_PLUS_IM_ID: 1,
  IM_ID: 2,
  VM_ID: 3,
  SIMM_LE_ID: 4,
  SIMM_CPE_ID: 5
};

export const ISDA_AGREEMENT_TYPE_ID = 6;
export const MNA_AGREEMENT_TYPE_ID = 20;
export const COMMON = "COMMON";
export const ADD = "ADD";
export const ROUNDING_CONVENTIONS = {
  UP_DOWN: "UP:DOWN",
  DOWN_UP: "DOWN:UP",
  UP_UP: "UP:UP",
  DOWN_DOWN: "DOWN:DOWN"
};
export const PACKAGES = {
  AGREEMENT: "deshaw.moss.model.goldensource.Agreement",
  CONTACT_DETAIL:
    "com.arcesium.treasury.model.lcm.collateralterm.ContactDetail",
  CURRENCY: "deshaw.moss.model.Currency",
  DISCLAIMER: "com.arcesium.treasury.model.lcm.collateralterm.Disclaimer",
  WIRE_INSTRUCTIONS: "deshaw.moss.model.goldensource.WireInstructions",
  CASH_COLLATERAL:
    "com.arcesium.treasury.model.lcm.collateralterm.CashCollateralTerm",
  PHYSICAL_COLLATERAL_CUSTODIAN_ACCOUNT:
    "com.arcesium.treasury.model.lcm.collateralterm.PhysicalCollateralCustodianAccount",
  PHYSICAL_COLLATERAL:
    "com.arcesium.treasury.model.lcm.collateralterm.PhysicalCollateralTerm",
  COLLATERAL_TERM:
    "com.arcesium.treasury.model.lcm.collateralterm.CollateralTerm",
  POSITION_EVALUATOR:
    "com.arcesium.treasury.model.lcm.collateralterm.PositionEvaluator",
  PORTFOLIO_EVALUATOR:
    "com.arcesium.treasury.model.lcm.collateralterm.PortfolioEvaluator",
  REGIME:
    "com.arcesium.treasury.model.lcm.collateralterm.Regime"
};
