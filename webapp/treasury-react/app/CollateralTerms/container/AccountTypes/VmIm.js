import React, { Component } from "react";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { ACCOUNT_TYPES } from "../../constants";
import { CashCollateral } from "../../component/AccountConfig/CashCollateral";
import { PhysicalCollateral } from "../../component/AccountConfig/PhysicalCollateral";

@inject("collateralTermStore")
@observer
class VmIm extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired,
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelectVmIm = (params) => {
    const { key, value } = params;
    const newFilters = { ...this.store.vmImAccountFilters };
    newFilters[key] = value;
    this.store.setFilters(ACCOUNT_TYPES.VM_PLUS_IM, newFilters);
  };

  render() {
    return (
      <React.Fragment>
        <legend style={{ fontWeight: "bold" }}>Cash</legend>
        <br />
        <CashCollateral
          onSelect={this.onSelectVmIm}
          filters={this.store.vmImAccountFilters}
          legalEntityWireAccounts={this.store.legalEntityWireAccounts}
          cpeWireAccounts={this.store.cpeWireAccounts}
          readonly={this.store.readOnly}
        />

        <SingleSelectFilter
          onSelect={this.onSelectVmIm}
          selectedData={this.store.vmImAccountFilters.selectedCustodianAccount}
          stateKey="selectedCustodianAccount"
          data={this.store.tripartyCollateralCustodianAccounts}
          label="TriParty Custodian Account"
          readonly={this.store.readOnly}
          disabled={this.store.readOnly}
          horizontalLayout
        />
        <br />
        <CheckboxFilter
          label="Can Post Securities as Collateral"
          defaultChecked={
            this.store.vmImAccountFilters.canPostSecuritiesAsCollateral
          }
          simmImLeAccountFilters
          onSelect={this.onSelectVmIm}
          stateKey="canPostSecuritiesAsCollateral"
          disabled={this.store.isMNAAgreement || this.store.readOnly}
          style="left"
        />
        <br />
        {this.store.vmImAccountFilters.canPostSecuritiesAsCollateral && (
          <React.Fragment>
            <legend style={{ fontWeight: "bold" }}>Physical Collateral</legend>
            <br />
            <PhysicalCollateral
              onSelect={this.onSelectVmIm}
              filters={this.store.vmImAccountFilters}
              rehypothecationApplicable={true}
              sourceCollateralCustodianAccounts={
                this.store.sourceCollateralCustodianAccounts
              }
              destinationCollateralCustodianAccounts={
                this.store.destinationCollateralCustodianAccounts
              }
              roundingConventionDropDownData={
                this.store.roundingConventionDropDownData
              }
              agreementDetailIds={this.store.agreementDetailIds}
              portfolioEvaluators={this.store.portfolioEvaluators}
              positionEvaluators={this.store.positionEvaluators}
            />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default VmIm;
