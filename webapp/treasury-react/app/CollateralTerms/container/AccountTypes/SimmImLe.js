import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { ACCOUNT_TYPES } from "../../constants";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import { CashCollateral } from "../../component/AccountConfig/CashCollateral";
import { PhysicalCollateral } from "../../component/AccountConfig/PhysicalCollateral";
import InputFilter from "../../../commons/components/InputFilter";
import { MarginMethodologyFilter } from "../../component/MarginMethodologyFilter";
import MultiSelectFilter from "../../../commons/components/MultiSelectFilter";

@inject("collateralTermStore")
@observer
class SimmImLe extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelectSimmImLe = ({ key, value }) => {
    const newFilters = { ...this.store.simmImLeAccountFilters };
    newFilters[key] = value;
    this.store.setFilters(ACCOUNT_TYPES.SIMM_LE, newFilters);
  };

  render() {
    return (
      <React.Fragment>
        <MarginMethodologyFilter
          stateKey={'selectedMarginMethodology'}
          label={'Margin Methodology*'}
          selectedData={this.store.simmImLeAccountFilters.selectedMarginMethodology}
          data={this.store.marginMethodologies}
          onSelect={this.onSelectSimmImLe}
        />
        <InputFilter
          onSelect={this.onSelectSimmImLe}
          stateKey="selectedAgreementLevelThreshold"
          type="number"
          data={this.store.simmImLeAccountFilters.selectedAgreementLevelThreshold}
          label="Agreement Level Threshold (USD)"
        />
        <MultiSelectFilter
          onSelect={this.onSelectSimmImLe}
          selectedData={this.store.simmImLeAccountFilters.selectedApplicableRegimesTypes}
          stateKey="selectedApplicableRegimesTypes"
          data={this.store.applicableRegimes}
          label="Applicable Regimes"
          horizontalLayout
        />
        <br />
        <legend style={{ fontWeight: "bold" }}>Cash</legend>
        <br />
        <CashCollateral
          onSelect={this.onSelectSimmImLe}
          filters={this.store.simmImLeAccountFilters}
          legalEntityWireAccounts={this.store.legalEntityWireAccounts}
          cpeWireAccounts={this.store.cpeWireAccounts}
        />
        <br />
        <CheckboxFilter
          label="Can Post Securities as Collateral"
          defaultChecked={
            this.store.simmImLeAccountFilters.canPostSecuritiesAsCollateral
          }
          simmImLeAccountFilters
          onSelect={this.onSelectSimmImLe}
          stateKey="canPostSecuritiesAsCollateral"
          style="left"
        />
        <br />
        {this.store.simmImLeAccountFilters.canPostSecuritiesAsCollateral && (
          <React.Fragment>
            <legend style={{ fontWeight: "bold" }}>Physical Collateral</legend>
            <br />
            <PhysicalCollateral
              onSelect={this.onSelectSimmImLe}
              filters={this.store.simmImLeAccountFilters}
              rehypothecationApplicable={false}
              sourceCollateralCustodianAccounts={
                this.store.sourceCollateralCustodianAccounts
              }
              destinationCollateralCustodianAccounts={
                this.store.destinationCollateralCustodianAccounts
              }
              roundingConventionDropDownData={
                this.store.roundingConventionDropDownData
              }
              agreementDetailIds={this.store.agreementDetailIds}


              portfolioEvaluators = {
                this.store.portfolioEvaluators
              }

              positionEvaluators = {
                this.store.positionEvaluators
              }
            />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default SimmImLe;
