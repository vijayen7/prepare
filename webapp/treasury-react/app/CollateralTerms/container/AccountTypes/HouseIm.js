import React, { Component } from "react";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { ACCOUNT_TYPES } from "../../constants";
import { CashCollateral } from "../../component/AccountConfig/CashCollateral";
import { PhysicalCollateral } from "../../component/AccountConfig/PhysicalCollateral";

@inject("collateralTermStore")
@observer
class HouseIm extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired,
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelectHouseIm = (params) => {
    const { key, value } = params;
    const newFilters = { ...this.store.houseImAccountFilters };
    newFilters[key] = value;
    this.store.setFilters(ACCOUNT_TYPES.IM, newFilters);
  };

  render() {
    return (
      <React.Fragment>
        <legend style={{ fontWeight: "bold" }}>Cash</legend>
        <br />
        <CashCollateral
          onSelect={this.onSelectHouseIm}
          filters={this.store.houseImAccountFilters}
          legalEntityWireAccounts={this.store.legalEntityWireAccounts}
          cpeWireAccounts={this.store.houseImCpeWireAccounts}
        />

        <SingleSelectFilter
          onSelect={this.onSelectHouseIm}
          selectedData={
            this.store.houseImAccountFilters.selectedCustodianAccount
          }
          stateKey="selectedCustodianAccount"
          data={this.store.tripartyCollateralCustodianAccounts}
          label="Seg Custodian Account*"
          horizontalLayout
        />
        <br />
        <CheckboxFilter
          label="Use Segregation methodology"
          defaultChecked={this.store.houseImAccountFilters.useSegMethodology}
          onSelect={this.onSelectHouseIm}
          stateKey="useSegMethodology"
          style="left"
        />
        <br />
        <CheckboxFilter
          label="Can Post Securities as Collateral"
          defaultChecked={
            this.store.houseImAccountFilters.canPostSecuritiesAsCollateral
          }
          onSelect={this.onSelectHouseIm}
          stateKey="canPostSecuritiesAsCollateral"
          style="left"
        />
        <br />
        {this.store.houseImAccountFilters.canPostSecuritiesAsCollateral && (
          <React.Fragment>
            <legend style={{ fontWeight: "bold" }}>Physical Collateral</legend>
            <br />

            <PhysicalCollateral
              onSelect={this.onSelectHouseIm}
              filters={this.store.houseImAccountFilters}
              rehypothecationApplicable={true}
              sourceCollateralCustodianAccounts={
                this.store.sourceCollateralCustodianAccounts
              }
              destinationCollateralCustodianAccounts={
                this.store.destinationCollateralCustodianAccounts
              }
              roundingConventionDropDownData={
                this.store.roundingConventionDropDownData
              }
              agreementDetailIds={this.store.agreementDetailIds}
              portfolioEvaluators={this.store.portfolioEvaluators}
              positionEvaluators={this.store.positionEvaluators}
            />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default HouseIm;
