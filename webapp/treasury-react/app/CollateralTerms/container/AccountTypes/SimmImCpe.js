import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { ACCOUNT_TYPES } from "../../constants";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import { CashCollateral } from "../../component/AccountConfig/CashCollateral";
import { PhysicalCollateral } from "../../component/AccountConfig/PhysicalCollateral";
import InputFilter from "../../../commons/components/InputFilter";
import { MarginMethodologyFilter } from "../../component/MarginMethodologyFilter";
import MultiSelectFilter from "../../../commons/components/MultiSelectFilter";

@inject("collateralTermStore")
@observer
class SimmImCpe extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelectSimmImCpe = params => {
    const { key, value } = params;
    const newFilters = { ...this.store.simmImCpeAccountFilters };
    newFilters[key] = value;
    this.store.setFilters(ACCOUNT_TYPES.SIMM_CPE, newFilters);
  };
  render() {
    return (
      <React.Fragment>
        <MarginMethodologyFilter
          stateKey={'selectedMarginMethodology'}
          label={'Margin Methodology*'}
          selectedData={this.store.simmImCpeAccountFilters.selectedMarginMethodology}
          data={this.store.marginMethodologies}
          onSelect={this.onSelectSimmImCpe}
        />
        <InputFilter
          onSelect={this.onSelectSimmImCpe}
          stateKey="selectedAgreementLevelThreshold"
          type="number"
          data={this.store.simmImCpeAccountFilters.selectedAgreementLevelThreshold}
          label="Agreement Level Threshold (USD)"
        />
        <MultiSelectFilter
          onSelect={this.onSelectSimmImCpe}
          selectedData={this.store.simmImCpeAccountFilters.selectedApplicableRegimesTypes}
          stateKey="selectedApplicableRegimesTypes"
          data={this.store.applicableRegimes}
          label="Applicable Regimes"
          horizontalLayout
        />
        <br />
        <legend style={{ fontWeight: "bold" }}>Cash</legend>
        <br />
        <CashCollateral
          onSelect={this.onSelectSimmImCpe}
          filters={this.store.simmImCpeAccountFilters}
          legalEntityWireAccounts={this.store.legalEntityWireAccounts}
          cpeWireAccounts={this.store.cpeWireAccounts}
        />
        <br />
        <CheckboxFilter
          label="Can Post Securities as Collateral"
          defaultChecked={
            this.store.simmImCpeAccountFilters.canPostSecuritiesAsCollateral
          }
          onSelect={this.onSelectSimmImCpe}
          stateKey="canPostSecuritiesAsCollateral"
          style="left"
        />
        <br />
        {this.store.simmImCpeAccountFilters.canPostSecuritiesAsCollateral && (
          <React.Fragment>
            <legend style={{ fontWeight: "bold" }}>Physical Collateral</legend>
            <br />
            <PhysicalCollateral
              onSelect={this.onSelectSimmImCpe}
              filters={this.store.simmImCpeAccountFilters}
              rehypothecationApplicable={false}
              sourceCollateralCustodianAccounts={
                this.store.sourceCollateralCustodianAccounts
              }
              destinationCollateralCustodianAccounts={
                this.store.destinationCollateralCustodianAccounts
              }
              roundingConventionDropDownData={
                this.store.roundingConventionDropDownData
              }
              agreementDetailIds={this.store.agreementDetailIds}

              portfolioEvaluators = {
                this.store.portfolioEvaluators
              }

              positionEvaluators = {
                this.store.positionEvaluators
              }
            />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default SimmImCpe;
