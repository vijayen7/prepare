import React, { Component } from "react";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { ACCOUNT_TYPES } from "../../constants";
import { CashCollateral } from "../../component/AccountConfig/CashCollateral";
import { PhysicalCollateral } from "../../component/AccountConfig/PhysicalCollateral";

@inject("collateralTermStore")
@observer
class Vm extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelectVm = params => {
    const { key, value } = params;
    const newFilters = { ...this.store.vmAccountFilters };
    newFilters[key] = value;
    this.store.setFilters(ACCOUNT_TYPES.VM, newFilters);
  };

  render() {
    return (
      <React.Fragment>
        <legend style={{ fontWeight: "bold" }}>Cash</legend>
        <br />
        <CashCollateral
          onSelect={this.onSelectVm}
          filters={this.store.vmAccountFilters}
          legalEntityWireAccounts={this.store.legalEntityWireAccounts}
          cpeWireAccounts={this.store.cpeWireAccounts}
        />
        <br />
        <CheckboxFilter
          label="Can Post Securities as Collateral"
          defaultChecked={
            this.store.vmAccountFilters.canPostSecuritiesAsCollateral
          }
          simmImLeAccountFilters
          onSelect={this.onSelectVm}
          stateKey="canPostSecuritiesAsCollateral"
          style="left"
        />
        <br />
        {this.store.vmAccountFilters.canPostSecuritiesAsCollateral && (
          <React.Fragment>
            <legend style={{ fontWeight: "bold" }}>Physical Collateral</legend>
            <br />
            <PhysicalCollateral
              onSelect={this.onSelectVm}
              filters={this.store.vmAccountFilters}
              rehypothecationApplicable={true}
              sourceCollateralCustodianAccounts={
                this.store.sourceCollateralCustodianAccounts
              }
              destinationCollateralCustodianAccounts={
                this.store.destinationCollateralCustodianAccounts
              }
              roundingConventionDropDownData={
                this.store.roundingConventionDropDownData
              }
              agreementDetailIds={this.store.agreementDetailIds}


              portfolioEvaluators = {
                this.store.portfolioEvaluators
              }

              positionEvaluators = {
                this.store.positionEvaluators
              }
            />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default Vm;
