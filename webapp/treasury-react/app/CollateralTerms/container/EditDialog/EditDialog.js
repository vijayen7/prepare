import React, { Component } from "react";
import { Layout } from "arc-react-components";
import { Dialog } from "arc-react-components";
import { inject, observer } from "mobx-react";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import PropTypes from "prop-types";
import CurrencyFilter from "../../../commons/container/CurrencyFilter";
import InputFilter from "../../../commons/components/InputFilter";
import AgreementDetailsOnAdd from "../Agreement/AgreementDetailsOnAdd";
import AgreementDetailsOnEdit from "../Agreement/AgreementDetailsOnEdit";
import RoundingConventions from "../Agreement/RoundingConventions";
import ContactDetails from "../Agreement/ContactDetails";
import AgreementStructure from "../Agreement/AgreementStructure";
import SimmImCpe from "../AccountTypes/SimmImCpe";
import SimmImLe from "../AccountTypes/SimmImLe";
import Vm from "../AccountTypes/Vm";
import VmIm from "../AccountTypes/VmIm";
import HouseIm from "../AccountTypes/HouseIm";
import { COMMON, ADD } from "../../constants";
import PopUpMessage from "../../component/PopUpMessage";
import Message from "../../../commons/components/Message";
import StatusComponent from "../../../commons/components/StatusComponent";

@inject("collateralTermStore")
@observer
class EditDialog extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired,
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelectAdd = (params) => {
    const { key, value } = params;
    const newFilters = { ...this.store.filtersOnAdd };
    newFilters[key] = value;
    this.store.setFilters(ADD, newFilters);
  };

  onSelect = (params) => {
    const { key, value } = params;
    const newFilters = { ...this.store.filters };
    newFilters[key] = value;
    this.store.setFilters(COMMON, newFilters);
  };

  showVmPlusIm = () => {
    return this.store.filters.isSegregated ? false : true;
  };

  showHouseIm = () => {
    return this.store.filters.isSegregated ? true : false;
  };

  showVm = () => {
    return this.store.filters.isSegregated ? true : false;
  };

  showSimmImLe = () => {
    return this.store.filters.isSimApplicable ? true : false;
  };

  showSimmImCpe = () => {
    return this.store.filters.isSimApplicable ? true : false;
  };

  render() {
    if (this.store.error) {
      return (
        <PopUpMessage
          isOpen={this.props.isShowCollateralTermsPopUp}
          onClose={this.props.onHideCollateralTermsPopUp}
          message="Unable to load data."
          title="Error Occured"
        />
      );
    }
    return (
      <React.Fragment>
        <Layout>
          <Layout.Child childId="child1">
            <Dialog
              isOpen={this.props.isShowCollateralTermsPopUp}
              onClose={this.props.onHideCollateralTermsPopUp}
              title={
                this.store.isEdit
                  ? "Edit Collateral Terms"
                  : "Add Collateral Terms"
              }
              style={{
                width: "33%",
              }}
              footer={
                <Layout>
                  {this.store.showInvalidInputMessage && (
                    <Layout.Child childId="child1">
                      <Message
                        messageData={this.store.invalidInputMessage}
                        error
                      />
                    </Layout.Child>
                  )}
                  <Layout.Child childId="child2">
                    <button onClick={this.store.createCollateralTerms}>
                      Save
                    </button>
                    <button onClick={this.store.resetFiltersData}>Reset</button>
                    <button onClick={this.props.onHideCollateralTermsPopUp}>
                      Cancel
                    </button>
                  </Layout.Child>
                </Layout>
              }
            >
              <Layout>
                <Layout.Child
                  title="Agreement Details"
                  showHeader={true}
                  className="form padding--double margin--left"
                  childId="child1"
                >
                  {!this.store.isEdit && <AgreementDetailsOnAdd />}
                  {this.store.isEdit && <AgreementDetailsOnEdit />}
                  <CurrencyFilter
                    stateKey="selectedCurrency"
                    onSelect={this.onSelect}
                    label="Reporting Currency*"
                    multiSelect={false}
                    horizontalLayout
                    selectedData={this.store.filters.selectedCurrency}
                    readonly={this.store.readOnly}
                  />
                  <SingleSelectFilter
                    onSelect={this.onSelect}
                    selectedData={this.store.filters.selectedDisclaimer}
                    horizontalLayout
                    stateKey="selectedDisclaimer"
                    data={this.store.disclaimerList}
                    label="Disclaimer*"
                    readonly={this.store.readOnly}
                    disabled={this.store.readOnly}
                  />
                  <InputFilter
                    onSelect={this.onSelect}
                    stateKey="selectedComments"
                    data={this.store.filters.selectedComments}
                    label="Comments*"
                    disabled={this.store.readOnly}
                  />
                </Layout.Child>
                <Layout.Divider childId='Divider1'/>
                <Layout.Child
                  title="Contact Details"
                  showHeader={true}
                  className="form padding--double margin--left"
                  childId="child2"
                  collapsible
                  collapsed={false}
                >
                  <ContactDetails />
                </Layout.Child>
                <Layout.Divider childId='Divider2'/>
                <Layout.Child
                  title="Agreement Rounding Conventions"
                  showHeader={true}
                  className="form padding--double margin--left"
                  childId="child3"
                  collapsible
                  collapsed={false}
                >
                  <RoundingConventions />
                </Layout.Child>
                <Layout.Divider childId='Divider3'/>
                <Layout.Child
                  title="Agreement Structure"
                  showHeader={true}
                  className="form padding--double margin--left"
                  childId="child4"
                >
                  <AgreementStructure />
                </Layout.Child>

                {this.showVmPlusIm() && (<Layout.Divider childId='Divider4' prevChildId="child4" nextChildId="child5"/>)}
                {this.showVmPlusIm() && (
                  <Layout.Child
                    title="Account Type : VM+IM"
                    showHeader={true}
                    className="form padding--double margin--left"
                    childId="child5"
                    collapsible
                    collapsed={false}
                  >
                    <VmIm />
                  </Layout.Child>
                )}

                {this.showVm() && (<Layout.Divider childId='Divider5' prevChildId="child4" nextChildId="child6"/> )}
                {this.showVm() && (
                  <Layout.Child
                    title="Account Type : VM"
                    showHeader={true}
                    className="form padding--double margin--left"
                    childId="child6"
                    collapsible
                    collapsed={false}
                  >
                    <Vm />
                  </Layout.Child>
                )}

                {this.showHouseIm() && (<Layout.Divider childId='Divider6' prevChildId="child6" nextChildId="child7"/> )}
                {this.showHouseIm() && (
                  <Layout.Child
                    title="Account Type : HOUSE IM"
                    showHeader={true}
                    className="form padding--double margin--left"
                    childId="child7"
                    collapsible
                    collapsed={false}
                  >
                    <HouseIm />
                  </Layout.Child>
                )}

                {this.showHouseIm() && this.showSimmImLe() && (<Layout.Divider childId='Divider7' prevChildId="child7" nextChildId="child8"/> )}
                {!this.showHouseIm() && this.showSimmImLe() && (<Layout.Divider childId='Divider8' prevChildId="child5" nextChildId="child8"/> )}
                {this.showSimmImLe() && (
                  <Layout.Child
                    title="Account Type : SIMM IM(LE)"
                    showHeader={true}
                    className="form padding--double margin--left"
                    childId="child8"
                    collapsible
                    collapsed={false}
                  >
                    <SimmImLe />
                  </Layout.Child>
                )}

                {this.showSimmImCpe() && (<Layout.Divider childId='Divider9' prevChildId="child8" nextChildId="child9"/> )}
                {this.showSimmImCpe() && (
                  <Layout.Child
                    title="Account Type : SIMM IM(CPE)"
                    showHeader={true}
                    className="form padding--double margin--left"
                    childId="child9"
                    collapsible
                    collapsed={false}
                  >
                    <SimmImCpe />
                  </Layout.Child>
                )}
              </Layout>
            </Dialog>
            <StatusComponent
              isOpen={this.store.showMessageDialog}
              onClose={this.store.toggleShowMessageDialog}
              status={this.store.status}
              title={this.store.dialogTitle}
            />
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );
  }
}

export default EditDialog;
