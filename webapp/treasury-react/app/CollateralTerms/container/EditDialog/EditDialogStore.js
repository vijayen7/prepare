import { action, flow, observable } from "mobx";
import {
  getReferenceData,
  validateAgreement,
  saveCollateralTerm
} from "../../api";
import {
  ACCOUNT_TYPES,
  ACCOUNT_TYPE_IDS,
  COMMON,
  ADD,
  ROUNDING_CONVENTIONS,
  PACKAGES,
  ISDA_AGREEMENT_TYPE_ID,
  MNA_AGREEMENT_TYPE_ID
} from "../../constants";
import {
  SUCCESS,
  FAILURE
} from "../../../commons/constants";

const ROUNDING_CONVENTION_DATA = [
  { key: 1, value: ROUNDING_CONVENTIONS.UP_DOWN },
  { key: 2, value: ROUNDING_CONVENTIONS.DOWN_UP },
  { key: 3, value: ROUNDING_CONVENTIONS.UP_UP },
  { key: 4, value: ROUNDING_CONVENTIONS.DOWN_DOWN }
];

const INITIAL_ACCOUNT_CONFIG_FILTERS = {
  selectedLEWireAccounts: [],
  selectedCPEWireAccounts: [],
  selectedSourceCollateralCAs: null,
  selectedDestinationCollateralCAs: null,
  selectedCustodianAccount: null,
  isRehypothecation: false,
  isPopulateAsm: false,
  isSecurityPledged: false,
  isRatingsComparisonEnabled: false,
  selectedAcmRoundingConvention: ROUNDING_CONVENTION_DATA[0],
  selectedAcmRoundingAmount: 0,
  selectedCollateralQuantityRounding: ROUNDING_CONVENTION_DATA[0],
  selectedCollateralQuantityValue: 0,
  selectedAgreementLevelThreshold: 0,
  canPostSecuritiesAsCollateral: false,
  useSegMethodology: false,
  selectedMarginMethodology: null,
  selectedPortfolioEvaluatorsTypes: [],
  selectedPositionEvaluatorsTypes: [],
  selectedApplicableRegimesTypes: []
};

const INITIAL_FILTERS = {
  selectedCurrency: null,
  selectedDisclaimer: null,
  selectedComments: "",
  isSimApplicable: false,
  isSegregated: false,
  selectedLEContact: null,
  selectedCpeContact: null,
  selectedRoundingConvention: null,
  selectedLERoundingAmount: 0,
  selectedCPERoundingAmount: 0,
  selectedLegalEntityThreshold: 0,
  selectedCpeThreshold: 0,
  selectedLEMTA: 0,
  selectedCPEMTA: 0
};

const INITIAL_FILTERS_ON_ADD = {
  selectedLegalEntities: null,
  selectedCpes: null,
  selectedAgreementTypes: null
};

const INITIAL_AGREEMENT_DETAIL_IDS = {
  legalEntityId: 0,
  cpeId: 0,
  agreementTypeId: 0
};

const INITIAL_STATUS = {
  status: FAILURE,
  message: "Error Occured"
};

export default class EditDialogStore {
  constructor(collateraltermStore) {
    this.collateraltermStore = collateraltermStore;
  }

  @observable
  vmImAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;

  @observable
  vmAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;

  @observable
  houseImAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;

  @observable
  simmImLeAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;

  @observable
  simmImCpeAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;

  @observable
  filters = INITIAL_FILTERS;

  @observable
  filtersOnAdd = INITIAL_FILTERS_ON_ADD;

  @observable
  isIsdaAgreement = false;

  @observable
  isMNAAgreement = false;

  @observable
  data = {};

  @observable
  readOnlyAgreementFiltersOnAdd = false;

  @observable
  isEdit = false;

  @observable
  inProgress = false;

  @observable
  validationInProgress = false;

  @observable
  saveInProgress = false;

  @observable
  error = false;

  @observable
  readOnly = false;

  @observable
  termsDataForAllAccountTypes = [];

  @observable
  sourceCollateralCustodianAccounts = [];

  @observable
  tripartyCollateralCustodianAccounts = [];

  @observable
  destinationCollateralCustodianAccounts = [];

  @observable
  legalEntityWireAccounts = [];

  @observable
  marginMethodologies = [];

  @observable
  cpeWireAccounts = [];

  @observable
  houseImCpeWireAccounts = [];

  @observable
  contactDetailsList = [];

  @observable
  disclaimerList = [];

  @observable
  roundingConventionDropDownData = ROUNDING_CONVENTION_DATA;

  @observable
  agreementDetailIds = INITIAL_AGREEMENT_DETAIL_IDS;

  @observable
  dialogTitle = "";

  @observable
  status = INITIAL_STATUS;

  @observable
  showInvalidInputMessage = false;

  @observable
  invalidInputMessage = "";

  @observable
  showMessageDialog = false;

  @observable
  portfolioEvaluators = [];

  @observable
  positionEvaluators = [];

  @observable
  applicableRegimes = [];

  @action.bound
  setInvalidInputMessage(invalidInputMessage) {
    this.invalidInputMessage = invalidInputMessage;
    this.showInvalidInputMessage = true;
  }

  @action.bound
  toggleShowMessageDialog() {
    this.showMessageDialog = !this.showMessageDialog;
  }

  @action.bound
  setDialogTitle(dialogTitle) {
    this.dialogTitle = dialogTitle;
  }

  @action.bound
  setStatus(status, message) {
    this.status.status = status;
    this.status.message = message;
  }

  @action.bound
  toggleReadOnly() {
    this.readOnly = !this.readOnly;
  }

  @action.bound
  toggleInProgress() {
    this.inProgress = !this.inProgress;
  }

  @action.bound
  setIsEdit() {
    this.isEdit = true;
  }

  @action.bound
  unSetIsEdit() {
    this.isEdit = false;
  }

  @action.bound
  resetFiltersData() {
    this.filters = INITIAL_FILTERS;
    this.vmImAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;
    this.vmAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;
    this.houseImAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;
    this.simmImLeAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;
    this.simmImCpeAccountFilters = INITIAL_ACCOUNT_CONFIG_FILTERS;
    this.error = false;
    this.showInvalidInputMessage = false;
    this.readOnlyAgreementFiltersOnAdd = false;
  }

  @action.bound
  resetDropDownData() {
    this.filtersOnAdd = INITIAL_FILTERS_ON_ADD;
    this.sourceCollateralCustodianAccounts = [];
    this.tripartyCollateralCustodianAccounts = [];
    this.destinationCollateralCustodianAccounts = [];
    this.termsDataForAllAccountTypes = [];
    this.legalEntityWireAccounts = [];
    this.cpeWireAccounts = [];
    this.houseImCpeWireAccounts = [];
    this.contactDetailsList = [];
    this.portfolioEvaluators = [];
    this.positionEvaluators = [];
    this.applicableRegimes = [];
  }

  @action.bound
  setFilters(type, newFilters) {
    this.showInvalidInputMessage = false;
    switch (type) {
      case ACCOUNT_TYPES.VM_PLUS_IM:
        this.vmImAccountFilters = newFilters;
        break;
      case ACCOUNT_TYPES.VM:
        this.vmAccountFilters = newFilters;
        break;
      case ACCOUNT_TYPES.IM:
        this.houseImAccountFilters = newFilters;
        break;
      case ACCOUNT_TYPES.SIMM_LE:
        this.simmImLeAccountFilters = newFilters;
        break;
      case ACCOUNT_TYPES.SIMM_CPE:
        this.simmImCpeAccountFilters = newFilters;
        break;
      case COMMON:
        this.filters = newFilters;
        break;
      case ADD:
        this.filtersOnAdd = newFilters;
        if (
          this.filtersOnAdd.selectedLegalEntities &&
          this.filtersOnAdd.selectedCpes &&
          this.filtersOnAdd.selectedAgreementTypes
        ) {
          this.validateAgreementAndFetchDropDown();
        }
        break;
      default:
        console.log("Invalid Filter Selected");
        break;
    }
  }

  @action.bound
  isValidAccountInputData(accountFilter) {
    if (
      accountFilter.canPostSecuritiesAsCollateral &&
      (!accountFilter.selectedSourceCollateralCAs ||
        !accountFilter.selectedDestinationCollateralCAs ||
        !accountFilter.selectedAcmRoundingConvention ||
        !accountFilter.selectedCollateralQuantityRounding ||
        isNaN(accountFilter.selectedAcmRoundingAmount) ||
        isNaN(accountFilter.selectedCollateralQuantityValue))
    ) {
      return false;
    }
    return true;
  }

  @action.bound
  isValidAgreementInputData() {
    if (
      (!this.isEdit &&
        (!this.filtersOnAdd.selectedLegalEntities ||
          !this.filtersOnAdd.selectedCpes ||
          !this.filtersOnAdd.selectedAgreementTypes)) ||
      !this.filters.selectedDisclaimer ||
      !this.filters.selectedCurrency ||
      !this.filters.selectedComments ||
      !this.filters.selectedRoundingConvention ||
      (!this.filters.isSegregated &&
        !this.isValidAccountInputData(this.vmImAccountFilters)) ||
      (this.filters.isSegregated &&
        (!this.isValidAccountInputData(this.houseImAccountFilters) ||
          !this.isValidAccountInputData(this.vmAccountFilters) ||
          !this.houseImAccountFilters.selectedCustodianAccount)) ||
      (this.filters.isSimApplicable &&
        (!this.isValidAccountInputData(this.simmImLeAccountFilters) ||
          !this.isValidAccountInputData(this.simmImCpeAccountFilters) ||
          !this.simmImLeAccountFilters.selectedMarginMethodology ||
          !this.simmImCpeAccountFilters.selectedMarginMethodology))
    ) {
      return false;
    }
    return true;
  }

  @action.bound
  isValidAgreementInputDataType() {
    if (isNaN(this.filters.selectedLERoundingAmount) || isNaN(this.filters.selectedCPERoundingAmount) ||
        isNaN(this.filters.selectedLegalEntityThreshold) || isNaN(this.filters.selectedCpeThreshold) ||
        isNaN(this.filters.selectedLEMTA) || isNaN(this.filters.selectedCPEMTA)) {
       return false;
     }
      return true;
  }

  @action.bound
  getCollateralTerm(agreementDetails, accountTypeFilter, accountTypeId) {
    let newCollateralTerm = {
      agreement: {
        dESCOEntityId: agreementDetails.legalEntityId,
        cpeId: agreementDetails.cpeId,
        agreementTypeId: agreementDetails.agreementTypeId,
        agreementId: this.isEdit ? agreementDetails.agreementId : null
      },
      legalEntityContactDetail: {
        contactId: this.filters.selectedLEContact
          ? parseInt(this.filters.selectedLEContact.key)
          : null
      },
      cpeContactDetail: {
        contactId: this.filters.selectedCpeContact
          ? parseInt(this.filters.selectedCpeContact.key)
          : null
      },
      disclaimer: {
        disclaimerId: this.filters.selectedDisclaimer
          ? parseInt(this.filters.selectedDisclaimer.key)
          : null
      },
      reportingCurrency: {
        spn: parseInt(this.filters.selectedCurrency.key)
      },
      accountTypeId: accountTypeId,
      roundingConventionId: parseInt(
        this.filters.selectedRoundingConvention.key
      ),
      comment: this.filters.selectedComments,
      segMethodologyApplicable: accountTypeFilter.useSegMethodology,
      tripartyCustodianAccountId: accountTypeFilter.selectedCustodianAccount
        ? parseInt(accountTypeFilter.selectedCustodianAccount.key)
        : null,
      marginMethodology: accountTypeFilter.selectedMarginMethodology ?
        accountTypeFilter.selectedMarginMethodology.value : null,
      cashCollateralTerm: {
        leWireInstructions: [],
        cpeWireInstructions: [],
        legalEntityRoundingAmount: this.filters.selectedLERoundingAmount
          ? parseInt(this.filters.selectedLERoundingAmount)
          : 0,
        legalEntityThreshold: this.filters.selectedLegalEntityThreshold
          ? parseInt(this.filters.selectedLegalEntityThreshold)
          : 0,
        legalEntityMinTransferAmount: this.filters.selectedLEMTA
          ? parseInt(this.filters.selectedLEMTA)
          : 0,
        cpeRoundingAmount: this.filters.selectedCPERoundingAmount
          ? parseInt(this.filters.selectedCPERoundingAmount)
          : 0,
        cpeThreshold: this.filters.selectedCpeThreshold
          ? parseInt(this.filters.selectedCpeThreshold)
          : 0,
        agreementLevelThreshold: accountTypeFilter.selectedAgreementLevelThreshold
          ? parseInt(accountTypeFilter.selectedAgreementLevelThreshold)
          : 0,
        cpeMinTransferAmount: this.filters.selectedCPEMTA
          ? parseInt(this.filters.selectedCPEMTA)
          : 0
      },
      physicalCollateralTerm: accountTypeFilter.canPostSecuritiesAsCollateral
        ? {
          acmRoundingConventionId: parseInt(
            accountTypeFilter.selectedAcmRoundingConvention.key
          ),
          acmRoundingAmount: accountTypeFilter.selectedAcmRoundingAmount
            ? parseInt(accountTypeFilter.selectedAcmRoundingAmount)
            : 0,
          collateralQuantityRoundingConventionId: parseInt(
            accountTypeFilter.selectedCollateralQuantityRounding.key
          ),
          collateralQuantityValue: accountTypeFilter.selectedCollateralQuantityValue
            ? parseInt(accountTypeFilter.selectedCollateralQuantityValue)
            : 0,
          rehypothecation: accountTypeFilter.isRehypothecation,
          populateAsm: accountTypeFilter.isPopulateAsm,
          securityPledged: accountTypeFilter.isSecurityPledged,
          ratingsComparisonEnabled: accountTypeFilter.isRatingsComparisonEnabled
        }
        : null
    };

    if (accountTypeFilter.selectedLEWireAccounts) {
      accountTypeFilter.selectedLEWireAccounts.forEach(item =>
        newCollateralTerm.cashCollateralTerm.leWireInstructions.push({
          instructionId: parseInt(item.key)
        })
      );
    }
    if (accountTypeFilter.selectedCPEWireAccounts) {
      accountTypeFilter.selectedCPEWireAccounts.forEach(item =>
        newCollateralTerm.cashCollateralTerm.cpeWireInstructions.push({
          instructionId: parseInt(item.key)
        })
      );
    }

    if (accountTypeFilter.selectedApplicableRegimesTypes) {
      newCollateralTerm.applicableRegimes = [];
      accountTypeFilter.selectedApplicableRegimesTypes.forEach((item) =>
        newCollateralTerm.applicableRegimes.push({
          regimeId: parseInt(item.key)
        })
      );
    }

    if (accountTypeFilter.canPostSecuritiesAsCollateral) {
      newCollateralTerm.physicalCollateralTerm.physicalCollateralCustodianAccounts = [];

      let sourceCollateralCa = accountTypeFilter.selectedSourceCollateralCAs;
      newCollateralTerm.physicalCollateralTerm.physicalCollateralCustodianAccounts.push(
        {
          custodianAccountId: parseInt(sourceCollateralCa.key),
          physicalCollateralCAType: 1
        }
      );
      let destCollateralCa = accountTypeFilter.selectedDestinationCollateralCAs;
      newCollateralTerm.physicalCollateralTerm.physicalCollateralCustodianAccounts.push(
        {
          custodianAccountId: parseInt(destCollateralCa.key),
          physicalCollateralCAType: 2
        }
      );

      if (accountTypeFilter.selectedPortfolioEvaluatorsTypes) {
        newCollateralTerm.physicalCollateralTerm.portfolioEvaluators = [];
        accountTypeFilter.selectedPortfolioEvaluatorsTypes.forEach((item) =>
          newCollateralTerm.physicalCollateralTerm.portfolioEvaluators.push({
            portfolioEvaluatorId: parseInt(item.key)
          })
        );
      }

      if (accountTypeFilter.selectedPositionEvaluatorsTypes) {
        newCollateralTerm.physicalCollateralTerm.positionEvaluators = [];
        accountTypeFilter.selectedPositionEvaluatorsTypes.forEach((item) =>
          newCollateralTerm.physicalCollateralTerm.positionEvaluators.push({
            positionEvaluatorId: parseInt(item.key)
          })
        );
      }

  }

    return newCollateralTerm;
  }

  saveCollateralterms = flow(
    function* saveCollateralterms(newCollateralTerms) {
      this.saveInProgress = true;
      try {
        newCollateralTerms.forEach(item => {
          item.agreement["@CLASS"] = PACKAGES.AGREEMENT;
          item.legalEntityContactDetail["@CLASS"] = PACKAGES.CONTACT_DETAIL;
          item.cpeContactDetail["@CLASS"] = PACKAGES.CONTACT_DETAIL;
          item.reportingCurrency["@CLASS"] = PACKAGES.CURRENCY;
          item.disclaimer["@CLASS"] = PACKAGES.DISCLAIMER;
          item.cashCollateralTerm.leWireInstructions.forEach(
            e => (e["@CLASS"] = PACKAGES.WIRE_INSTRUCTIONS)
          );
          item.cashCollateralTerm.cpeWireInstructions.forEach(
            e => (e["@CLASS"] = PACKAGES.WIRE_INSTRUCTIONS)
          );
          item.applicableRegimes.forEach(
            e =>
              (e["@CLASS"] = PACKAGES.REGIME)
          );
          item.cashCollateralTerm["@CLASS"] = PACKAGES.CASH_COLLATERAL;
          if (item.physicalCollateralTerm) {
            item.physicalCollateralTerm.physicalCollateralCustodianAccounts.forEach(
              e =>
                (e["@CLASS"] = PACKAGES.PHYSICAL_COLLATERAL_CUSTODIAN_ACCOUNT)
            );
            item.physicalCollateralTerm.portfolioEvaluators.forEach(
              e =>
                (e["@CLASS"] = PACKAGES.PORTFOLIO_EVALUATOR)
            );
            item.physicalCollateralTerm.positionEvaluators.forEach(
              e =>
                (e["@CLASS"] = PACKAGES.POSITION_EVALUATOR)
            );
            item.physicalCollateralTerm["@CLASS"] =
              PACKAGES.PHYSICAL_COLLATERAL;
          }
          item["@CLASS"] = PACKAGES.COLLATERAL_TERM;
        });
        let apiResponse = yield saveCollateralTerm(newCollateralTerms);
        this.saveInProgress = false;

        if (apiResponse.successStatus) {
          this.setDialogTitle("Save Status");
          this.setStatus(SUCCESS, "Saved Successfully");
          this.toggleShowMessageDialog();
          this.collateraltermStore.toggleShowPopUp();
          this.collateraltermStore.getCollateralTerms();
        } else {
          this.setDialogTitle("Save Status");
          this.setStatus(FAILURE, apiResponse.message);
          this.toggleShowMessageDialog();
        }
      } catch (e) {
        this.saveInProgress = false;
        this.setDialogTitle("Save Status");
        this.setStatus(FAILURE, "Error Occured While Saving, Save was unsuccessful");
        this.toggleShowMessageDialog();
      }
    }.bind(this)
  );

  @action.bound
  isInvalidPercentage(value) {
    return !(value <= 100 && value >= 0);
  }

  @action.bound
  isInvalidAgreementLevelThreshold() {
    return (this.simmImCpeAccountFilters.selectedAgreementLevelThreshold < 0
      || this.simmImCpeAccountFilters.selectedAgreementLevelThreshold > 50000000
      || this.simmImLeAccountFilters.selectedAgreementLevelThreshold < 0
      || this.simmImLeAccountFilters.selectedAgreementLevelThreshold > 50000000)
  }

  @action.bound
  createCollateralTerms() {
    if (!this.isValidAgreementInputData()) {
      this.setInvalidInputMessage("Please fill all the required(*) inputs");
    } else if(!this.isValidAgreementInputDataType()) {
      this.setInvalidInputMessage("Please fill the Numeric values in Numeric fields");
    } else if (
      this.filters.isSimApplicable && this.isInvalidAgreementLevelThreshold()
    ) {
      this.setInvalidInputMessage(
        "Agreement Level Threshold input is invalid. \n Agreement Level Threshold should be between 0 and 50,000,000"
      );
    } else {
      let newCollateralTerms = [];
      let agreementDetails = {};
      if (!this.isEdit) {
        agreementDetails = {
          legalEntityId: this.filtersOnAdd.selectedLegalEntities.key,
          cpeId: this.filtersOnAdd.selectedCpes.key,
          agreementTypeId: this.filtersOnAdd.selectedAgreementTypes.key
        };
      } else {
        let termData = this.termsDataForAllAccountTypes[0];
        agreementDetails = {
          agreementId: termData.agreement.agreementId,
          legalEntityId: termData.agreement.dESCOEntityId,
          cpeId: termData.agreement.cpeId,
          agreementTypeId: termData.agreement.agreementTypeId
        };
      }

      if (!this.filters.isSegregated) {
        newCollateralTerms.push(
          this.getCollateralTerm(agreementDetails, this.vmImAccountFilters, 1)
        );
      } else {
        newCollateralTerms.push(
          this.getCollateralTerm(agreementDetails, this.vmAccountFilters, 3)
        );
        newCollateralTerms.push(
          this.getCollateralTerm(
            agreementDetails,
            this.houseImAccountFilters,
            2
          )
        );
      }
      if (this.filters.isSimApplicable) {
        newCollateralTerms.push(
          this.getCollateralTerm(
            agreementDetails,
            this.simmImLeAccountFilters,
            4
          )
        );
        newCollateralTerms.push(
          this.getCollateralTerm(
            agreementDetails,
            this.simmImCpeAccountFilters,
            5
          )
        );
      }
      this.saveCollateralterms(newCollateralTerms);
    }
  }

  @action.bound
  setAccountFiltersData(accountData, accountFilter, isDropDownFilterData) {
    if (!isDropDownFilterData) {
      if (accountData.physicalCollateralTerm) {
        accountFilter.canPostSecuritiesAsCollateral = true;
        let pcc = accountData.physicalCollateralTerm;

        accountFilter.selectedAcmRoundingConvention = ROUNDING_CONVENTION_DATA.find(
          item => parseInt(item.key) === pcc.acmRoundingConventionId
        );
        accountFilter.selectedAcmRoundingAmount = pcc.acmRoundingAmount;
        accountFilter.selectedCollateralQuantityRounding = ROUNDING_CONVENTION_DATA.find(
          item =>
            parseInt(item.key) === pcc.collateralQuantityRoundingConventionId
        );
        accountFilter.selectedCollateralQuantityValue =
          pcc.collateralQuantityValue;
        accountFilter.isRehypothecation = pcc.rehypothecation;
        accountFilter.isPopulateAsm = pcc.populateAsm;
        accountFilter.isSecurityPledged = pcc.securityPledged;
        accountFilter.isRatingsComparisonEnabled = pcc.ratingsComparisonEnabled;
      }
      if (accountData.segMethodologyApplicable) {
        accountFilter.useSegMethodology = accountData.segMethodologyApplicable;
      }
      if (accountData.marginMethodology) {
        accountFilter.selectedMarginMethodology = { key: accountData.marginMethodology, value: accountData.marginMethodology };
      }
    } else {
      accountFilter.selectedAgreementLevelThreshold = accountData.cashCollateralTerm.agreementLevelThreshold;

      if (accountData.cashCollateralTerm.cpeWireInstructions) {
        let cpeWiresIds = accountData.cashCollateralTerm.cpeWireInstructions.map(
          e => e.instructionId
        );
        if (cpeWiresIds) {
          if (accountData.accountTypeId === ACCOUNT_TYPE_IDS.IM_ID) {
            accountFilter.selectedCPEWireAccounts = this.houseImCpeWireAccounts.filter(
              item => cpeWiresIds.includes(parseInt(item.key))
            );
          } else {
            accountFilter.selectedCPEWireAccounts = this.cpeWireAccounts.filter(
              item => cpeWiresIds.includes(parseInt(item.key))
            );
          }
        }
      }

      if (accountData.cashCollateralTerm.leWireInstructions) {
        let leWireIds = accountData.cashCollateralTerm.leWireInstructions.map(
          e => e.instructionId
        );
        if (leWireIds) {
          accountFilter.selectedLEWireAccounts = this.legalEntityWireAccounts.filter(
            item => leWireIds.includes(parseInt(item.key))
          );
        }
      }

      if (accountData.tripartyCustodianAccountId) {
        accountFilter.selectedCustodianAccount = this.tripartyCollateralCustodianAccounts.find(
          item => parseInt(item.key) === accountData.tripartyCustodianAccountId
        );
      }

      if(accountData.applicableRegimes
        && accountData.applicableRegimes.length){
      let applicableRegimeIds = [];
      accountData.applicableRegimes.forEach(
        item => {
          applicableRegimeIds.push(item.regimeId);
        });
        accountFilter.selectedApplicableRegimesTypes = this.applicableRegimes.filter(
          item => applicableRegimeIds.includes(parseInt(item.key))
        );
      }

      if (accountData.physicalCollateralTerm) {
        let sCCAId = 0;
        let dCCAId = 0;
        accountData.physicalCollateralTerm.physicalCollateralCustodianAccounts.forEach(
          item => {
            if (item.physicalCollateralCAType === 1) {
              sCCAId = item.custodianAccountId;
            } else {
              dCCAId = item.custodianAccountId;
            }
          }
        );
        accountFilter.selectedDestinationCollateralCAs = this.destinationCollateralCustodianAccounts.find(
          item => parseInt(item.key) === dCCAId
        );

        accountFilter.selectedSourceCollateralCAs = this.sourceCollateralCustodianAccounts.find(
          item => parseInt(item.key) === sCCAId
        );

        if(accountData.physicalCollateralTerm.portfolioEvaluators
          && accountData.physicalCollateralTerm.portfolioEvaluators.length){
        let portfolioPCEIds = [];
        accountData.physicalCollateralTerm.portfolioEvaluators.forEach(
          item => {
            portfolioPCEIds.push(item.portfolioEvaluatorId);
          });
          accountFilter.selectedPortfolioEvaluatorsTypes = this.portfolioEvaluators.filter(
            item => portfolioPCEIds.includes(parseInt(item.key))
          );
        }

        if(accountData.physicalCollateralTerm.positionEvaluators
          && accountData.physicalCollateralTerm.positionEvaluators.length){
        let positionPCEIds = [];
        accountData.physicalCollateralTerm.positionEvaluators.forEach(
          item => {
            positionPCEIds.push(item.positionEvaluatorId);
          });
          accountFilter.selectedPositionEvaluatorsTypes = this.positionEvaluators.filter(
            item => positionPCEIds.includes(parseInt(item.key))
          );
        }

      }
    }
  }

  @action.bound
  setAccountsData(isDropDownFilterData) {
    this.termsDataForAllAccountTypes.forEach(item => {
      switch (item.accountTypeId) {
        case ACCOUNT_TYPE_IDS.VM_PLUS_IM_ID:
          this.setAccountFiltersData(
            item,
            this.vmImAccountFilters,
            isDropDownFilterData
          );
          break;
        case ACCOUNT_TYPE_IDS.IM_ID:
          this.setAccountFiltersData(
            item,
            this.houseImAccountFilters,
            isDropDownFilterData
          );
          this.filters.isSegregated = true;
          break;
        case ACCOUNT_TYPE_IDS.VM_ID:
          this.setAccountFiltersData(
            item,
            this.vmAccountFilters,
            isDropDownFilterData
          );
          this.filters.isSegregated = true;
          break;
        case ACCOUNT_TYPE_IDS.SIMM_LE_ID:
          this.setAccountFiltersData(
            item,
            this.simmImLeAccountFilters,
            isDropDownFilterData
          );
          this.filters.isSimApplicable = true;
          break;
        case ACCOUNT_TYPE_IDS.SIMM_CPE_ID:
          this.setAccountFiltersData(
            item,
            this.simmImCpeAccountFilters,
            isDropDownFilterData
          );
          this.filters.isSimApplicable = true;
          break;
      }
    });
  }

  @action.bound
  setContactDetailsData() {
    let agreementDetails = this.termsDataForAllAccountTypes[0];
    if (agreementDetails.legalEntityContactDetail) {
      this.filters.selectedLEContact = this.contactDetailsList.find(
        item =>
          parseInt(item.key) ===
          agreementDetails.legalEntityContactDetail.contactId
      );
    }
    if (agreementDetails.cpeContactDetail) {
      this.filters.selectedCpeContact = this.contactDetailsList.find(
        item =>
          parseInt(item.key) === agreementDetails.cpeContactDetail.contactId
      );
    }
  }

  @action.bound
  setAgreementDetailsData(agreementDetails) {
    this.data = {
      agreementId: agreementDetails.agreement.agreementId,
      agreementName: agreementDetails.agreementName,
      userLogin: agreementDetails.userLogin,
      legalEntity: agreementDetails.legalEntity,
      counterPartyEntity: agreementDetails.counterPartyEntity,
      agreementType: agreementDetails.agreementType,
      comment: agreementDetails.comment,
      disclaimer: agreementDetails.disclaimer
        ? agreementDetails.disclaimer.displayName
        : null,
      legalEntityContact: agreementDetails.legalEntityContactDetail
        ? agreementDetails.legalEntityContactDetail.emailId
        : null,
      cpeContact: agreementDetails.cpeContactDetail
        ? agreementDetails.cpeContactDetail.emailId
        : null
    };
    this.filters = {
      ...this.filters,
      selectedCurrency: {
        key: agreementDetails.reportingCurrency.spn,
        value:
          agreementDetails.reportingCurrency.abbreviation +
          " [" +
          agreementDetails.reportingCurrency.spn +
          "]"
      },
      selectedDisclaimer: agreementDetails.disclaimer
        ? {
          key: agreementDetails.disclaimer.disclaimerId,
          value: agreementDetails.disclaimer.displayName
        }
        : null,
      selectedComments: agreementDetails.comment,
      selectedRoundingConvention: ROUNDING_CONVENTION_DATA.find(
        item => parseInt(item.key) === agreementDetails.roundingConventionId
      ),
      selectedLERoundingAmount:
        agreementDetails.cashCollateralTerm.legalEntityRoundingAmount,
      selectedCPERoundingAmount:
        agreementDetails.cashCollateralTerm.cpeRoundingAmount,
      selectedLEMTA:
        agreementDetails.cashCollateralTerm.legalEntityMinTransferAmount,
      selectedCPEMTA: agreementDetails.cashCollateralTerm.cpeMinTransferAmount,
      selectedLegalEntityThreshold:
        agreementDetails.cashCollateralTerm.legalEntityThreshold,
      selectedCpeThreshold: agreementDetails.cashCollateralTerm.cpeThreshold
    };
  }

  @action.bound
  setAgreementDetailIds(legalEntityId, cpeId, agreementTypeId) {
    this.agreementDetailIds = {
      legalEntityId: legalEntityId,
      cpeId: cpeId,
      agreementTypeId: agreementTypeId
    };
  }

  @action.bound
  setAgreementData(id) {
    this.resetFiltersData();
    function filterOnAgreementId(item) {
      return item.agreement.agreementId === this;
    }
    this.termsDataForAllAccountTypes = this.collateraltermStore.collateralTerms.filter(
      filterOnAgreementId,
      id
    );
    let agreementDetails = this.termsDataForAllAccountTypes[0];
    this.setAgreementDetailIds(
      agreementDetails.agreement.dESCOEntityId,
      agreementDetails.agreement.cpeId,
      agreementDetails.agreement.agreementTypeId
    );
    this.setAgreementDetailsData(agreementDetails);
    this.setAccountsData(false);
  }

  @action.bound
  sortByValue(list) {
    list.sort((a, b) => {
      const valueA = a.value.toLowerCase();
      const valueB = b.value.toLowerCase();
      if (valueA < valueB) return -1;
      if (valueA > valueB) return 1;
      return 0;
    });
    return list;
  }

  @action.bound
  getSortedList(unSortedList) {
    if (unSortedList !== undefined) {
      let list = [];
      _.forEach(unSortedList, (value, key) => {
        value = value + " [" + key.toString() + "]";
        list = list.concat({ key, value });
      });
      this.sortByValue(list);
      return list;
    }
  }

  fetchDropDownData = flow(
    function* fetchDropDownData(legalEntityId, cpeId, agreementTypeId) {
      this.inProgress = true;
      try {
        let collateralTermReferenceData = yield getReferenceData(
          legalEntityId,
          cpeId,
          agreementTypeId
        );
        this.sourceCollateralCustodianAccounts = this.getSortedList(
          collateralTermReferenceData.sourceCustodianAccountIdToCustodianAccountNameMap
        );
        this.tripartyCollateralCustodianAccounts = this.getSortedList(
          collateralTermReferenceData.tripartyCustodianAccountIdToCustodianAccountNameMap
        );
        this.destinationCollateralCustodianAccounts = this.getSortedList(
          collateralTermReferenceData.destinationCustodianAccountIdToCustodianAccountNameMap
        );
        this.disclaimerList = this.getSortedList(
          collateralTermReferenceData.disclaimerIdToDisclaimerNameMap
        );
        this.contactDetailsList = this.getSortedList(
          collateralTermReferenceData.contactIdToContactNameMap
        );
        this.legalEntityWireAccounts = this.getSortedList(
          collateralTermReferenceData.legalEntityWireInstructionIdToWireInstructionNameMap
        );
        this.cpeWireAccounts = this.getSortedList(
          collateralTermReferenceData.cpeWireInstructionIdToWireInstructionNameMap
        );
        this.houseImCpeWireAccounts = this.getSortedList(
          collateralTermReferenceData.cpeWireInstructionIdToWireInstructionNameMapForHouseIm
        );
        this.portfolioEvaluators = this.getSortedList(
          collateralTermReferenceData.idToPortfolioEvaluatorNameMap
        );
        this.positionEvaluators = this.getSortedList(
          collateralTermReferenceData.idToPositionEvaluatorNameMap
        );
        this.applicableRegimes = this.getSortedList(
          collateralTermReferenceData.idToApplicableRegimeNameMap
        );
        this.marginMethodologies = Object.entries(collateralTermReferenceData.idToMarginMethodologyMap)
          .map(item => ({ key: item[0], value: item[1] }));
        this.setIsIsdaOrMNAAgreement(agreementTypeId);
        if (this.isEdit) {
          this.setAccountsData(true);
          this.setContactDetailsData();
        } else {
          if (this.disclaimerList) {
            this.filters.selectedDisclaimer = this.disclaimerList[0];
          }
          this.readOnly = false;
        }
        this.inProgress = false;
      } catch (e) {
        this.inProgress = false;
        this.error = true;
      }
    }.bind(this)
  );

  @action.bound
  setIsIsdaOrMNAAgreement(agreementTypeId) {

    this.isIsdaAgreement = false;
    this.isMNAAgreement = false;

    if (agreementTypeId === ISDA_AGREEMENT_TYPE_ID) {
      this.isIsdaAgreement = true;
    }
    else if (agreementTypeId === MNA_AGREEMENT_TYPE_ID){
      this.isMNAAgreement = true;
    }
  }

  validateAgreementAndFetchDropDown = flow(
    function* validateAgreementAndFetchDropDown() {
      this.validationInProgress = true;
      let leId = this.filtersOnAdd.selectedLegalEntities.key;
      let cpeId = this.filtersOnAdd.selectedCpes.key;
      let agreementTypeId = this.filtersOnAdd.selectedAgreementTypes.key;
      try {
        let apiResponse = yield validateAgreement(leId, cpeId, agreementTypeId);
        if (apiResponse.successStatus) {
          this.resetFiltersData();
          this.setAgreementDetailIds(leId, cpeId, agreementTypeId);
          this.fetchDropDownData(leId, cpeId, agreementTypeId);
          this.readOnlyAgreementFiltersOnAdd = true;
        } else {
          this.setInvalidInputMessage(
            "Invalid Agreement. Please give valid agreement combination."
          );
          this.filtersOnAdd.selectedAgreementTypes = null;
          this.filters = INITIAL_FILTERS;
          this.isIsdaAgreement = false;
          this.readOnly = true;
        }
        this.validationInProgress = false;
      } catch (e) {
        this.validationInProgress = false;
        this.error = true;
      }
    }.bind(this)
  );
}
