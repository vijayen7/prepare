import React, { Component } from "react";
import { ReactArcGrid } from "arc-grid";
import { Layout } from "arc-react-components";
import { gridColumns } from "../Grid/gridColumns";
import { getGridOptions } from "../Grid/gridOptions";
import PropTypes from "prop-types";
import { inject, observer } from "mobx-react";
import Message from "../../commons/components/Message";
import EditDialog from "./EditDialog/EditDialog";
import { BASE_URL } from "../../commons/constants";

@inject("collateralTermStore")
@observer
class Grid extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.editDialogStore = this.props.collateralTermStore.editDialogStore;
    this.collateralTermColumns = gridColumns();
  }

  store = this.props.collateralTermStore;

  onCellClickHandler = args => {
    if (args.colId === "edit") {
      this.editDialogStore.setIsEdit();
      this.editDialogStore.setAgreementData(args.item.agreementId);
      this.editDialogStore.fetchDropDownData(
        args.item.legalEntityId,
        args.item.cpeId,
        args.item.agreementTypeId
      );
      if (!this.store.showPopUp) {
        this.store.toggleShowPopUp();
      }
    }
    if (args.colId === "physicalCollateralRules") {
      let url = `${BASE_URL}lcm/PhysicalCollateralRules.html?legalEntityId=${args.item.legalEntityId}&cpeId=${args.item.cpeId}&agreementTypeId=${args.item.agreementTypeId}`;
      window.open(url, "_blank");
    }
  };

  toggleShowPopUp = () => {
    this.editDialogStore.unSetIsEdit();
    this.editDialogStore.resetFiltersData();
    this.editDialogStore.resetDropDownData();
    this.store.toggleShowPopUp();
    if (!this.editDialogStore.readOnly) {
      this.editDialogStore.toggleReadOnly();
    }
  };

  renderGridData = () => {
    let data = this.store.collateralTermsOnAccountType;
    let flatennedCollateralTerms = this.store.getFlattenedData(data);

    if (this.store.searchStatus.error) {
      return (
        <Message messageData="Unable to load data. Some error occurred." />
      );
    }

    if (!flatennedCollateralTerms || !flatennedCollateralTerms.length) {
      return (
        <Message messageData="No data available. Perform Search to view results." />
      );
    }

    let grid = (
      <ReactArcGrid
        data={flatennedCollateralTerms}
        gridId="CollateralTerms"
        columns={this.collateralTermColumns}
        options={getGridOptions()}
        resizeCanvas={this.store.resizeCanvasFlag}
        onCellClick={this.onCellClickHandler}
      />
    );

    return grid;
  };

  render() {
    return (
      <Layout>
        <Layout.Child childId="child1">
          <div className="margin" align="right">
            <button onClick={this.toggleShowPopUp}>Add Collateral Terms</button>
          </div>
          <EditDialog
            isShowCollateralTermsPopUp={this.store.showPopUp}
            onHideCollateralTermsPopUp={this.store.toggleShowPopUp}
          />
          {this.renderGridData()}
        </Layout.Child>
      </Layout>
    );
  }
}

export default Grid;
