import React, { Component } from "react";
import Sidebar from "../../commons/components/Sidebar";
import LegalEntityFilter from "../../commons/container/LegalEntityFilter";
import CpeFilter from "../../commons/container/CpeFilter";
import AgreementTypeFilter from "../../commons/container/AgreementTypeFilter";
import ColumnLayout from "../../commons/components/ColumnLayout";
import FilterButton from "../../commons/components/FilterButton";
import PropTypes from "prop-types";
import { inject, observer } from "mobx-react";
import SaveSettingsManager from "../../commons/components/SaveSettingsManager";
import MultiSelectFilter from "../../commons/filters/components/MultiSelectFilter";

@inject("collateralTermStore")
@observer
class SideBar extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired,
  };

  store = this.props.collateralTermStore;

  onSelect = (params) => {
    this.store.onSelect(params);
    this.store.resizeCanvas();
  };

  handleCopySearch = () => {
    this.store.copySearchUrl(window.location.origin);
  };

  handleReset = () => {
    this.store.resetFilters();
    this.store.resizeCanvas();
  };

  handleSearch = () => {
    this.store.toggleToggleSidebarFlag();
    this.store.getCollateralTerms();
  };

  applySavedFilters = (savedFilters) => {
    this.store.setFilters(savedFilters);
  };

  render() {
    return (
      <Sidebar
        collapsible
        size="400px"
        resizeCanvas={this.store.resizeCanvas}
        toggleSidebar={this.store.toggleSidebarFlag}
      >
        <SaveSettingsManager
          selectedFilters={this.store.filters}
          applySavedFilters={this.applySavedFilters}
          applicationName="collateralTermFilter"
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedLegalEntities}
          multiSelect
        />
        <CpeFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedCpes}
          multiSelect
        />
        <AgreementTypeFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedAgreementTypes}
          multiSelect
        />
        <MultiSelectFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedAccountTypes}
          stateKey="selectedAccountTypes"
          data={this.store.accountTypeData}
          label="Account Types"
          multiSelect
        />
        <ColumnLayout>
          <FilterButton onClick={this.handleSearch} label="Search" />
          <FilterButton className="button--tertiary" reset={true} onClick={this.handleReset} label="Reset" />
          <FilterButton className="button--tertiary margin--horizontal--double" onClick={this.handleCopySearch} label="Copy Search URL" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

export default SideBar;
