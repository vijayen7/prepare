import React, { Component } from "react";
import PropTypes from "prop-types";
import Column from "../../../commons/components/Column";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import { inject, observer } from "mobx-react";
import { COMMON } from "../../constants";

@inject("collateralTermStore")
@observer
class AgreementStructure extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelect = params => {
    const { key, value } = params;
    const newFilters = { ...this.store.filters };
    newFilters[key] = value;
    this.store.setFilters(COMMON, newFilters);
  };

  render() {
    return (
      <React.Fragment>
        <Column>
          <CheckboxFilter
            label="Is Segregated"
            defaultChecked={this.store.filters.isSegregated}
            onSelect={this.onSelect}
            stateKey="isSegregated"
            style="left"
            disabled={this.store.isMNAAgreement || this.store.readOnly}
          />
          <CheckboxFilter
            label="Is SIMM Applicable"
            defaultChecked={this.store.filters.isSimApplicable}
            onSelect={this.onSelect}
            stateKey="isSimApplicable"
            style="left"
            disabled={!this.store.isIsdaAgreement || this.store.readOnly}
          />
        </Column>
      </React.Fragment>
    );
  }
}

export default AgreementStructure;
