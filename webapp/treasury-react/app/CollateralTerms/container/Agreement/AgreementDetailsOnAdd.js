import React, { Component } from "react";
import PropTypes from "prop-types";
import { inject, observer } from "mobx-react";
import LegalEntityFilter from "../../../commons/container/LegalEntityFilter";
import CpeFilter from "../../../commons/container/CpeFilter";
import AgreementTypeFilter from "../../../commons/container/AgreementTypeFilter";
import { ADD } from "../../constants";

@inject("collateralTermStore")
@observer
class AgreementDetailsOnAdd extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelectAdd = params => {
    const { key, value } = params;
    const newFilters = { ...this.store.filtersOnAdd };
    newFilters[key] = value;
    this.store.setFilters(ADD, newFilters);
  };

  render() {
    return (
      <React.Fragment>
        <LegalEntityFilter
          onSelect={this.onSelectAdd}
          selectedData={this.store.filtersOnAdd.selectedLegalEntities}
          multiSelect={false}
          horizontalLayout
          readonly={this.store.readOnlyAgreementFiltersOnAdd}
        />
        <CpeFilter
          onSelect={this.onSelectAdd}
          selectedData={this.store.filtersOnAdd.selectedCpes}
          multiSelect={false}
          label={"CPE"}
          horizontalLayout
          readonly={this.store.readOnlyAgreementFiltersOnAdd}
        />
        <AgreementTypeFilter
          onSelect={this.onSelectAdd}
          selectedData={this.store.filtersOnAdd.selectedAgreementTypes}
          multiSelect={false}
          horizontalLayout
          readonly={this.store.readOnlyAgreementFiltersOnAdd}
        />
      </React.Fragment>
    );
  }
}

export default AgreementDetailsOnAdd;
