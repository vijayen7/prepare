import React, { Component } from "react";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import InputFilter from "../../../commons/components/InputFilter";
import { COMMON } from "../../constants";

@inject("collateralTermStore")
@observer
class RoundingConventions extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired,
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelect = (params) => {
    const { key, value } = params;
    const newFilters = { ...this.store.filters };
    newFilters[key] = value;
    this.store.setFilters(COMMON, newFilters);
  };

  render() {
    return (
      <React.Fragment>
        <SingleSelectFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedRoundingConvention}
          stateKey="selectedRoundingConvention"
          data={this.store.roundingConventionDropDownData}
          label="Rounding Convention*"
          readonly={this.store.readOnly}
          disabled={this.store.readOnly}
          horizontalLayout
        />
        <InputFilter
          onSelect={this.onSelect}
          stateKey="selectedLERoundingAmount"
          type="number"
          data={this.store.filters.selectedLERoundingAmount}
          label="LE Rounding Amount (RC)"
          disabled={this.store.readOnly}
        />
        <InputFilter
          onSelect={this.onSelect}
          stateKey="selectedCPERoundingAmount"
          type="number"
          data={this.store.filters.selectedCPERoundingAmount}
          label="CPE Rounding Amount (RC)"
          disabled={this.store.readOnly}
        />
        <InputFilter
          onSelect={this.onSelect}
          stateKey="selectedLegalEntityThreshold"
          data={this.store.filters.selectedLegalEntityThreshold}
          label="Legal Entity Threshold (RC)"
          disabled={this.store.readOnly}
        />
        <InputFilter
          onSelect={this.onSelect}
          stateKey="selectedCpeThreshold"
          data={this.store.filters.selectedCpeThreshold}
          label="CPE Threshold (RC)"
          disabled={this.store.readOnly}
        />
        <InputFilter
          onSelect={this.onSelect}
          stateKey="selectedLEMTA"
          data={this.store.filters.selectedLEMTA}
          label="LE MTA (RC)"
          disabled={this.store.readOnly}
        />
        <InputFilter
          onSelect={this.onSelect}
          stateKey="selectedCPEMTA"
          data={this.store.filters.selectedCPEMTA}
          label="CPE MTA (RC)"
          disabled={this.store.readOnly}
        />
      </React.Fragment>
    );
  }
}

export default RoundingConventions;
