import React, { Component } from "react";
import PropTypes from "prop-types";
import { inject, observer } from "mobx-react";
import Column from "../../../commons/components/Column";

@inject("collateralTermStore")
@observer
class AgreementDetailsOnEdit extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired
  };

  store = this.props.collateralTermStore.editDialogStore;

  render() {
    return (
      <React.Fragment>
        <Column>
          <div>
            <label>Agreement ID</label> <br />
            {this.store.data.agreementId}
          </div>
          <div>
            <label>Agreement Name</label> <br />
            {this.store.data.agreementName}
          </div>
        </Column>
        <Column>
          <div>
            <label>Created by</label> <br />
            {this.store.data.userLogin}
          </div>
          <div>
            <label>Legal Entity</label> <br />
            {this.store.data.legalEntity}
          </div>
        </Column>
        <Column>
          <div>
            <label>CounterParty Entity</label> <br />
            {this.store.data.counterPartyEntity}
          </div>
          <div>
            <label>Agreement Type</label> <br />
            {this.store.data.agreementType}
          </div>
        </Column>
      </React.Fragment>
    );
  }
}

export default AgreementDetailsOnEdit;
