import React, { Component } from "react";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { COMMON } from "../../constants";

@inject("collateralTermStore")
@observer
class ContactDetails extends Component {
  static propTypes = {
    collateralTermStore: PropTypes.object.isRequired,
  };

  store = this.props.collateralTermStore.editDialogStore;

  onSelect = (params) => {
    const { key, value } = params;
    const newFilters = { ...this.store.filters };
    newFilters[key] = value;
    this.store.setFilters(COMMON, newFilters);
  };

  render() {
    return (
      <React.Fragment>
        <SingleSelectFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedLEContact}
          horizontalLayout
          stateKey="selectedLEContact"
          data={this.store.contactDetailsList}
          label="Legal Entity Contact"
          readonly={this.store.readOnly}
          disabled={this.store.readOnly}
        />
        <SingleSelectFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedCpeContact}
          horizontalLayout
          stateKey="selectedCpeContact"
          data={this.store.contactDetailsList}
          label="CPE Contact"
          readonly={this.store.readOnly}
          disabled={this.store.readOnly}
        />
      </React.Fragment>
    );
  }
}

export default ContactDetails;
