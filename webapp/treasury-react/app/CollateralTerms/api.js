import { ArcFetch } from "arc-commons";
import { BASE_URL } from "../commons/constants";
import { fetchPostURL } from "../commons/util";

const QUERY_ARGS = {
  credentials: "include",
  method: "GET"
};

export const validateAgreement = (legalEntityId, cpeId, agreementTypeId) => {
  const url = `${BASE_URL}service/collateralTermService/validateAgreement?legalEntityId=${legalEntityId}&cpeId=${cpeId}&agreementTypeId=${agreementTypeId}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const getReferenceData = (legalEntityId, cpeId, agreementTypeId) => {
  const url = `${BASE_URL}service/collateralTermService/getCollateralTermReferenceData?legalEntityId=${legalEntityId}&cpeId=${cpeId}&agreementTypeId=${agreementTypeId}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const fetchCollateralTerms = input => {
  const url = `${BASE_URL}service/collateralTermService/getCollateralTerms?lcmPositionDataParam=${encodeURIComponent(
    JSON.stringify(input)
  )}&inputFormat=json&format=json`;

  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const saveCollateralTerm = input => {
  const url = `${BASE_URL}service/collateralTermService/saveCollateralTerm`;
  let param = encodeURIComponent(JSON.stringify(input));
  let encodedParm =
    "collateralTermList=" + param + "&inputFormat=JSON&format=JSON";
  return fetchPostURL(url, encodedParm);
};
