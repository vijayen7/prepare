import BorrowFinanceStore from "./store";
import React , { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../commons/components/ReactLoader";
import Loader from "../commons/container/Loader";
import AppHeader, { whenHeaderRef } from "arc-header";
import BorrowFinanceGrid from "./container/Grid";
import BorrowFinanceSideBar from "./container/SideBar";
import { Layout , Breadcrumbs  } from "arc-react-components";
import { extendObservable } from "mobx";
import { useLocalStore } from "mobx-react-lite";

const dummyObservable = Symbol("dummy-observable");

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item
        link="/treasury/seclend/bestBorrow.html"
        key="borrowFinancingReport"
      >
        Borrow Financing Report
      </Breadcrumbs.Item>
    </>
  );
};

const BorrowFinance: React.FC = () => {
  useEffect(() => {
    updateBreadCrumbs();
  }, []);
  const borrowFinanceStore = useLocalStore(() =>
    extendObservable(new BorrowFinanceStore(), {
      [dummyObservable]: true,
    })
  );

  return (
    <>
      <Loader />
      <ReactLoader
        inProgress={borrowFinanceStore.sideBarStore.searchStatus.inProgress}
      />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} />
        <Layout isColumnType={true} style={{ height: "100%" }}>
          <Layout.Child
            size={2}
            key="child1"
            title="Search Criteria"
            collapsible
            showHeader
          >
            <BorrowFinanceSideBar store={borrowFinanceStore} />
          </Layout.Child>
          <Layout.Child size={8} key="child3">
            <BorrowFinanceGrid store={borrowFinanceStore} />
          </Layout.Child>
        </Layout>
      </div>
    </>
  );
};

export default observer(BorrowFinance);
