import { BASE_URL } from "commons/constants";
import { ArcFetch } from "arc-commons";
import { getCommaSeparatedValues } from "commons/util";
import { SearchFilter } from "./models/SearchFilter";

const queryString = require("query-string");
export let url = "";

export const getBorrowFinanceDataList = (filters: SearchFilter) =>  {
  let csvLegalEntity = getCommaSeparatedValues(filters.selectedLegalEntities);
  let csvSecurityTypes = getCommaSeparatedValues(filters.selectedSecurityTypes);
  var payload = {
    dateString: filters.date ,
    legalEntityIds: csvLegalEntity == "-1" ? "-1" : csvLegalEntity,
    securityTypeIds : csvSecurityTypes == "-1" ? "-1" : csvSecurityTypes
  };
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}seclend/search-borrow-financing?${paramString}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}




