import { observable, action, flow } from "mobx";
import {
getBorrowFinanceDataList
} from "../api";
import SideBarStore from "./SideBarStore";

export class BorrowFinanceStore {
// Store initializations
sideBarStore: SideBarStore = new SideBarStore();

// Observable Variables
@observable borrowFinanceDataList = [];
@observable counterPartyColumns = [];

@observable isBorrowFinancingDetailShown = false;
@observable resizeCanvas = false;
@observable  titleDrillDown = "";
@observable  contentDrillDown  : any = [];

@action.bound onSelect({ key, value }: any) {
const newFilters = Object.assign({}, this.sideBarStore.filters);
newFilters[key] = value;
this.sideBarStore.setFilters(newFilters);
  }

  @action.bound handleSearch() {
    let searchStatus = { firstSearch: false, inProgress: true, error: false };
    this.isBorrowFinancingDetailShown = false;
    this.sideBarStore.setSearchStatus(searchStatus);
    this.getBorrowFinanceData();
  }

  // Method to fetch data from API
  getBorrowFinanceData = flow(
    function* getBorrowFinanceData(this: BorrowFinanceStore) {
      try {
         let borrowData = yield getBorrowFinanceDataList(
          this.sideBarStore.filters
        );
        this.borrowFinanceDataList = borrowData.resultList;
        this.sideBarStore.searchStatus.inProgress = false;
        this.counterPartyColumns = borrowData.columns;
      } catch (e) {
        alert("Error occurred while getting Data From API");
        this.sideBarStore.searchStatus = {
          firstSearch: false,
          inProgress: false,
          error: true,
        };
      }
    }.bind(this)
  );
}

export default BorrowFinanceStore;
