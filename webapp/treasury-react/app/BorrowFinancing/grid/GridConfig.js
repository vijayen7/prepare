export function getGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    enableCellNavigation : true,
    frozenColumn: 2,
    highlightRowOnClick: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    nestedTable: false,
    expandTillLevel: -1,
    sortList: [
      { columnId: "date", sortAsc: true },
      { columnId: "legalEntityName", sortAsc: true },
    ],
  };
  return options;
}

