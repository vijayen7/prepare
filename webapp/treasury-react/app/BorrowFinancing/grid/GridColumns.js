import {borrowFinancingFormatter , cpeFamilyFormatter} from '../formatters';

export default function gridColumns(counterpartyColumns) {
      var columns  = [
        {
            id : "securityName",
            name : "Security Name",
            field : "securityName",
            toolTip : "Security Name",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true,
            width : 200
         },
         {
             id : "pnlSpn",
             name : "PNL_SPN",
             field : "pnlSpn",
             toolTip : "PnL Spn",
             type : "text",
             filter : true,
             sortable : true,
             headerCssClass : "b",
             autoWidth : true
         },
         {
             id : "weightedAvgRate",
             name : "Weighted Avg Borrow Rate (bps)",
             field : "weightedAvgRate",
             type : "number",
             sortable : true,
             filter : false,
             formatter : borrowFinancingFormatter ,
            excelDataFormatter : function(borrowRate) {
                 return borrowRate != null ? borrowRate * 100 : "";
             },
             headerCssClass : "aln-rt b",
             autoWidth : true
         },
      ];

      var _defaultColumns  = [];
      if (counterpartyColumns) {
        for (var i = 0; i < counterpartyColumns.length; i++) {

            _defaultColumns.push(encodeURIComponent(counterpartyColumns[i]));

            columns.push({
                id : encodeURIComponent(counterpartyColumns[i]),
                name : counterpartyColumns[i] + " Borrow Rate (bps)",
                field :  (counterpartyColumns[i]),
                type : "object",
                filter : false,
                sortable : true,
                headerCssClass : "aln-rt b",
                autoWidth : true,
                comparator : function(a, b) {
                    var borrowRateFirst = (a[sortcol] ? a[sortcol]["borrowRate"] : 999999);
                    var borrowRateSecond = (b[sortcol] ? b[sortcol]["borrowRate"] : 999999);

                    return treasury.comparators.number(borrowRateFirst, borrowRateSecond);
                },
                excelDataFormatter : function(cpeFamilyFinancing) {
                    return cpeFamilyFinancing && cpeFamilyFinancing.borrowRate != null ? cpeFamilyFinancing.borrowRate * 100 : "";
              },
              formatter : cpeFamilyFormatter
            }
          );
      }
  }

  columns.push({
    id : "securityShortDesc",
    name : "Short Description",
    field : "securityShortDesc",
    toolTip : "Short Description",
    type : "text",
    filter : true,
    sortable : true,
    headerCssClass : "b",
    autoWidth : true
  });

  columns.push({
    id : "securityIsin",
    name : "ISIN",
    field : "securityIsin",
    toolTip : "ISIN",
    type : "text",
    filter : true,
    sortable : true,
    headerCssClass : "b",
    autoWidth : true
  });

  columns.push({
   id : "securityCusip",
   name : "CUSIP",
   field : "securityCusip",
   toolTip : "CUSIP",
   type : "text",
   filter : true,
   sortable : true,
   headerCssClass : "b",
   autoWidth : true
  });

  columns.push({
  id : "securitySedol",
  name : "SEDOL",
  field : "securitySedol",
  toolTip : "SEDOL",
  type : "text",
  filter : true,
  sortable : true,
  headerCssClass : "b",
  autoWidth : true
  });

  columns.push({
  id : "securityTicker",
  name : "Ticker",
  field : "securityTicker",
  toolTip : "Ticker",
  type : "text",
  filter : true,
  sortable : true,
  headerCssClass : "b",
  autoWidth : true
});

columns.push({
  id : "securityRic",
  name : "RIC",
  field : "securityRic",
  toolTip : "RIC",
  type : "text",
  filter : true,
  sortable : true,
  headerCssClass : "b",
  autoWidth : true
});

  return columns;
}
