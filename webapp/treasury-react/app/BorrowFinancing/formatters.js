import {
  drillThroughFormatter
} from 'commons/grid/formatters';

function _customRateFormatter(currValue, prevValue)
    {
        //Converts to rate format and adds the indicator
        var rate = (currValue != null ? currValue * 100 : null);
        var inputData = {"currValue" : currValue,
                         "prevValue":prevValue};

        var indicatorHTML = "";
        var showDataInCell = true;

        if(currValue != null && prevValue != null)
        {
            if(Math.round(currValue * 100) < Math.round(prevValue * 100))
            {
                indicatorHTML = "<i title='Rate went down' inputData=" + JSON.stringify(inputData) + " class='icon-arrow--down green gridcell-icon'></i>";
            }
            else if(Math.round(currValue * 100) > Math.round(prevValue * 100))
            {
                indicatorHTML = "<i title='Rate went up' inputData=" + JSON.stringify(inputData) + " class='icon-arrow--up red gridcell-icon'></i>";
            }
        }
        else if(currValue != null)
        {
            indicatorHTML = "<i title='New position' inputData=" + JSON.stringify(inputData) + " class='new-position gridcell-icon'></i>";
        }
        else if(prevValue != null)
        {
            //if currValue is null but prevValue != null, show X icon
            indicatorHTML = "<i title='Closed position' inputData=" + JSON.stringify(inputData) + " class='icon-interface__cross closed-position gridcell-icon'></i>";
            showDataInCell = false;
        }


        if(showDataInCell)
        {
             return "<div class='aln-rt'>" + indicatorHTML + (rate == null ? "<span>-</span>" : Number(rate).toFixed(0)) + "</div>";
        }

        return "<div class='aln-rt'>" + indicatorHTML + "</div>";
    }


export function borrowFinancingFormatter(row, cell, currValue, columnDef, securitySummary) {
  var params = {
      "pnlSpn" : securitySummary.pnlSpn
  };

      return drillThroughFormatter(
             row,
             cell,
             currValue,
             columnDef,
             undefined,
             function() {
                 return  _customRateFormatter(currValue, securitySummary.weightedAvgPrevRate);
             },
             undefined, // predicate
             JSON.stringify(params), // params
             "-borrowFinancingWeightedRate"); // moduleId (key appended to treasury-drill-through-<moduleId> class)
 }

export function cpeFamilyFormatter(row, cell, cpeFamilyFinancingData, columnDef, borrowFinancingSummary) {
  if (cpeFamilyFinancingData) {
      var params = {
          "pnlSpn" : cpeFamilyFinancingData.pnlSpn,
          "cpeFamilyName" : cpeFamilyFinancingData.cpeFamilyName
      };

      return drillThroughFormatter(
              row,
              cell,
              cpeFamilyFinancingData.borrowRate,
              columnDef,
              undefined,
              function() {
                  return  _customRateFormatter(cpeFamilyFinancingData.borrowRate, cpeFamilyFinancingData.prevDayBorrowRate);
              },
              undefined, // predicate
              JSON.stringify(params), // params
              "-borrowFinancing"); // moduleId (key appended to treasury-drill-through-<moduleId> class)
  }
  else {
      return "<div class='aln-rt'><span class='message'>-</span></div>";
  }
}
