import React from "react";
import { Sidebar } from "arc-react-components";
import { observer } from "mobx-react";
import DateFilter from "commons/container/DateFilter";
import ColumnLayout from "../../commons/components/ColumnLayout";
import FilterButton from "../../commons/components/FilterButton";
import { BorrowFinanceProps } from "../models/BorrowFinanceProps";
import SaveSettingsManager from "commons/components/SaveSettingsManager";
import { Card } from "arc-react-components";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import SecurityTypeFilter from "commons/container/SecurityTypeFilter";

const BorrowFinanceSideBar: React.FC<BorrowFinanceProps> = (
  props: BorrowFinanceProps
) => {
  let filters = (
    <Card>
      <DateFilter
        onSelect={props.store.onSelect}
        data={props.store.sideBarStore.filters.date}
        label="Date"
        stateKey="date"
      />
      <br />
      <LegalEntityFilter
        onSelect={props.store.onSelect}
        selectedData={props.store.sideBarStore.filters.selectedLegalEntities}
        multiSelect={true}
      />
      <SecurityTypeFilter
        onSelect={props.store.onSelect}
        selectedData={props.store.sideBarStore.filters.selectedSecurityTypes}
        stateKey={"selectedSecurityTypes"}
        multiSelect={true}
        label={"Security Type"}
      />
    </Card>
  );

  let searchButtons = (
    <ColumnLayout>
      <FilterButton onClick={props.store.handleSearch} label="Search" />
      <FilterButton
        onClick={props.store.sideBarStore.handleReset}
        reset
        label="Reset"
      />
    </ColumnLayout>
  );

  let saveSettings = (
    <Card>
      <SaveSettingsManager
        selectedFilters={props.store.sideBarStore.filters}
        applySavedFilters={props.store.sideBarStore.applySavedFilters}
        applicationName="BorrowFinancing"
      />
    </Card>
  );

  return (
    <Sidebar header={false} footer={searchButtons}>
      {saveSettings}
      {filters}
    </Sidebar>
  );
};

export default observer(BorrowFinanceSideBar);
