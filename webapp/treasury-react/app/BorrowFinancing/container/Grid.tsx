import React from "react";
import { observer } from "mobx-react";
import Message from "../../commons/components/Message";
import GridWithCellClick from "commons/components/GridWithCellClick";
import Table from "commons/components/Table";
import _ from "lodash";
import { BorrowFinanceProps } from "../models/BorrowFinanceProps";
import { getGridOptions } from "../grid/GridConfig";
import gridColumns from "../grid/GridColumns";
import cloneDeep from "lodash/cloneDeep";
import { Panel } from "arc-react-components";


const BorrowFinanceGrid: React.FC<BorrowFinanceProps> = (
  props: BorrowFinanceProps
) => {

  function onCellClickHandler (args) {
    let borrowFinanceData = args.item;
    var counterPartyData :string[] = [];
      for(var i = 0 ; i < props.store.counterPartyColumns.length ; i++)
      {
        let cpe = props.store.counterPartyColumns[i];
        if(borrowFinanceData[cpe] != undefined)
          counterPartyData.push(cpe);
      }

    let colId = decodeURI(args.colId);
    if (colId === "weightedAvgRate") {
      props.store.isBorrowFinancingDetailShown = true;
      props.store.titleDrillDown = "Borrow Financing Detail for " +  borrowFinanceData.securityName;
      var summaryData :any[] = [];
      for(var i = 0 ; i < counterPartyData.length ; i++)
      {
        let counterParty = cloneDeep(borrowFinanceData[counterPartyData[i]].borrowFinancingDetailList);
        for(var j = 0 ; j < counterParty.length ; j++)
        {
          if(counterParty[j].borrowRate !== undefined)
              counterParty[j].borrowRate = Number(counterParty[j].borrowRate).toFixed(2);
          if(counterParty[j].prevDayBorrowRate !== undefined)
            counterParty[j].prevDayBorrowRate = Number(counterParty[j].prevDayBorrowRate).toFixed(2);
          summaryData.push(counterParty[j]);
        }
      }

      props.store.contentDrillDown = summaryData;
      props.store.resizeCanvas = !props.store.resizeCanvas;
    }
    else if(counterPartyData.indexOf(colId) !== -1)
    {
      props.store.isBorrowFinancingDetailShown = true;
      props.store.titleDrillDown = "Borrow Financing Detail for (" +  borrowFinanceData.securityName + ") - (" + colId + ")";
      let counterParty = cloneDeep(borrowFinanceData[colId].borrowFinancingDetailList);
      var summaryData :any[] = [];
      for(var i = 0 ; i < counterParty.length ; i++)
      {
        if(counterParty[i].borrowRate !== undefined)
          counterParty[i].borrowRate = Number(counterParty[i].borrowRate).toFixed(2);
        if(counterParty[i].prevDayBorrowRate !== undefined)
          counterParty[i].prevDayBorrowRate = Number(counterParty[i].prevDayBorrowRate).toFixed(2);
        summaryData.push(counterParty[i]);
      }
      props.store.contentDrillDown = summaryData;
      props.store.resizeCanvas = !props.store.resizeCanvas;
    }
  };

  function loadBorrowFinancingDrillDown() {
    var title = props.store.titleDrillDown;
    var content = props.store.contentDrillDown;
    let drillDown;
    drillDown = (
      <Panel
        className={`position--relative margin--top size--1`}
        dismissible
        onClose={() => {
          props.store.isBorrowFinancingDetailShown =  false;
          props.store.resizeCanvas = !props.store.resizeCanvas;
        }}
        title={title}
      >
        <br />
        <Table
          labels={["Legal Entity" , "Counterparty Entity" , "Custodian Account" , "Borrow Rate (bps)" , "Previous Day's Borrow Rate (bps)" , "Notional (USD)"]}
          content={content}
          dataKeys={["legalEntity", "cpe" , "eca" , "borrowRate" , "prevDayBorrowRate" , "notional"]}
          style={{ width: "25%", display: "table" }}
        />
        </Panel>
    );
    return drillDown;
  }

  const renderGridData = () => {
    let borrowFinanceData = props.store.borrowFinanceDataList;
    if (props.store.sideBarStore.searchStatus.error) {
      return <Message messageData="Error occurred while loading data." />;
    }
    if (!borrowFinanceData.length) {
      let noDataMessage = "Please search to load data";
      if (props.store.sideBarStore.searchStatus.firstSearch == false) {
        noDataMessage = "No data available for search criteria";
      }
      return <Message messageData={noDataMessage} />;
    }
    let grid = (
      <div className="layout--flex--row">
        <div className="size--2">
        <GridWithCellClick
          gridColumns={gridColumns(props.store.counterPartyColumns)}
          onCellClick={onCellClickHandler}
          gridId="borrowFinanceGrid"
          gridOptions={getGridOptions()}
          data={borrowFinanceData}
          fill
          resizeCanvas={props.store.resizeCanvas}
        />
        </div>
        {props.store.isBorrowFinancingDetailShown &&
          loadBorrowFinancingDrillDown()}
      </div>
    );
    return grid;
  };
  return <>{renderGridData()}</>;
};
export default observer(BorrowFinanceGrid);
