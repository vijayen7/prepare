import ReferenceData from "./../../commons/models/ReferenceData";

export interface SearchFilter {
date: string;
selectedLegalEntities?: Array<ReferenceData>;
selectedSecurityTypes?: Array<ReferenceData>;
}

export interface SearchStatus {
firstSearch: boolean;
inProgress: boolean;
error: boolean;
}
