import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { beginWorkflow } from '../../commons/actions/workflow';
import Message from '../../commons/components/Message';
import { destroyDataState, fetchMMFTradesForDate, fetchMMFTradesForTPlusOneDate } from '../actions';
import {
  getBrokerDetailPanelColumns,
  getInternalDetailPanelColumns,
  getCashBalancesDetailColumns,
  getWireDetailPanelColumns,
  getAdjustmentDetailColumns,
  getAgreementParsingStatusDetailPanelColumns,
  getGovtBondsDetailPanelColumns,
  getGovtBondsTradeDetailPanelColumns
} from '../grid/columnConfig';
import { getTopResultOptions, getUnderlyingResultOptions } from '../grid/gridOptions';
import { CashManagementReportGrid } from './../components/CashManagementReportGrid';

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.fetchMMFTradesForDate = this.fetchMMFTradesForDate.bind(this);
    this.fetchMMFTradesForTPlusOneDate = this.fetchMMFTradesForTPlusOneDate.bind(this);
    this.onCellClickHandlerForUnderlying = this.onCellClickHandlerForUnderlying.bind(this);
    this.changeTab = this.changeTab.bind(this);
    this.beginWorkflow = this.beginWorkflow.bind(this);
    this.updateUnderlyingData = this.updateUnderlyingData.bind(this);
    this.getUpdateOptions = this.getUpdateOptions.bind(this);
  }
  componentWillMount() {
    this.setState(this.getDefaultState());
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  onCellClickHandler = (args) => {
    if (args.colId === 'cashBalance') {
      if (args.item.cashBalances && args.item.cashBalances.length) {
        this.updateUnderlyingData(args.item.cashBalances, getCashBalancesDetailColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'wireOutFlowAmount') {
      if (args.item.outflowWires && args.item.outflowWires.length) {
        this.updateUnderlyingData(args.item.outflowWires, getWireDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'wireAmount') {
      if (args.item.wires && args.item.wires.length) {
        this.updateUnderlyingData(args.item.wires, getWireDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'brokerCallUSD') {
      if (args.item.brokerCallAgreementsList && args.item.brokerCallAgreementsList.length) {
        this.updateUnderlyingData(args.item.brokerCallAgreementsList, getBrokerDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'internalCallUSD') {
      if (args.item.internalCallAgreementsList && args.item.internalCallAgreementsList.length) {
        this.updateUnderlyingData(args.item.internalCallAgreementsList, getInternalDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'brokerPullUSD') {
      if (args.item.brokerPullAgreementsList && args.item.brokerPullAgreementsList.length) {
        this.updateUnderlyingData(args.item.brokerPullAgreementsList, getBrokerDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'internalPullUSD') {
      if (args.item.internalPullAgreementsList && args.item.internalPullAgreementsList.length) {
        this.updateUnderlyingData(args.item.internalPullAgreementsList, getInternalDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'adjustmentAmount') {
      if (args.item.adjustments && args.item.adjustments.length) {
        this.updateUnderlyingData(args.item.adjustments, getAdjustmentDetailColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'parsedPercentage') {
      if (args.item.agreements && args.item.agreements.length) {
        this.updateUnderlyingData(args.item.agreements, getAgreementParsingStatusDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.colId === 'beginWorkflow') {
      this.beginWorkflow(args);
    } else if (args.item[args.colId + 'Positions']) {
      if (args.item[args.colId + 'Positions'].length) {
        this.updateUnderlyingData(args.item[args.colId + 'Positions'], getGovtBondsDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    } else if (args.item[args.colId + '_MaturedPositions']) {
      if (args.item[args.colId + '_MaturedPositions'].length) {
        this.updateUnderlyingData(args.item[args.colId + '_MaturedPositions'], getGovtBondsDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    }
    else if (args.item[args.colId + '_Trades']) {
      if (args.item[args.colId + '_Trades'].length) {
        this.updateUnderlyingData(args.item[args.colId + '_Trades'], getGovtBondsTradeDetailPanelColumns());
      } else {
        this.updateUnderlyingData([], []);
      }
    }
  };

  onCellClickHandlerForUnderlying = (args) => {
    if (args.colId === 'wireId') {
      const clientName = this.state.client;
      const stabilityLevel = this.state.stability;

      let url = '';
      if (clientName === 'desco' && (stabilityLevel === 'qa' || stabilityLevel === 'dev')) {
        url = `${'http://wiresqa.nyc.deshaw.com/wrs/WIREID/'}${args.item.wireId}`;
      } else {
        url = `${window.location.protocol}//${window.location.host}/wires/wrs/WIREID/${args.item.wireId}`;
      }
      window.open(url, '_blank');
    }
  };

  getDefaultState() {
    return {
      underlyingData: [],
      selectedTab: 'summary',
      client: CODEX_PROPERTIES['codex.client_name'],
      stability: CODEX_PROPERTIES['codex.stability_level']
    };
  }
  getUpdateOptions() {
    const options = getTopResultOptions();
    options.groupColumns = this.props.cashManagementData.groupColumns;

    return options;
  }
  fetchMMFTradesForDate() {
    const payloadForMMFTradeData = {
      date: this.props.cashManagementData.date
    };
    this.props.fetchMMFTradesForDate(payloadForMMFTradeData);
  }
  fetchMMFTradesForTPlusOneDate() {
    const payloadForMMFTradeData = {
      date: this.props.cashManagementData.date
    };
    this.props.fetchMMFTradesForTPlusOneDate(payloadForMMFTradeData);
  }

  updateUnderlyingData(data, columns) {
    const newState = {
      ...this.state,
      underlyingData: data,
      underlyingDataColumns: columns,
      underlyingDataOptions: getUnderlyingResultOptions(),
      selectedTab: 'underlyingData'
    };
    this.setState(newState);
  }
  beginWorkflow = (args) => {
    const payload = {
      'workflowParam.dateStr': this.props.cashManagementData.date,
      'workflowParam.entityFamilyId': -1,
      'workflowParam.workflowTypeName': 'CASH_MANAGEMENT',
      'workflowParam.workflowUnit': 'EntityDate'
    };
    const params = {
      dateStr: this.props.cashManagementData.date,
      entityFamilyId: -1,
      fundId: args.item.fundId,
      workflowTypeName: 'CASH_MANAGEMENT',
      workflowUnit: 'EntityDate',
      parsedBrokerCallsUSD: args.item.brokerCallUSD,
      unParsedDataInternalCallsUSD: args.item.internalCallUSD,
      lastUpdatedTime: this.props.cashManagementData.lastUpdatedTime
    };

    this.props.beginWorkflow(payload, params);
  };
  changeTab = (args) => {
    this.setState({ selectedTab: args });
  };
  renderGridData() {
    let grid = null;

    if (
      !Object.prototype.hasOwnProperty.call(this.props.cashManagementData, 'fundLevelLiquidityProfiles') ||
      this.props.cashManagementData.fundLevelLiquidityProfiles.length <= 0
    ) {
      return <Message messageData="No data available. Perform Search to view results." />;
    }

    grid = (
      <CashManagementReportGrid
        selectedTab={this.state.selectedTab}
        underlyingData={this.state.underlyingData}
        underlyingDataColumns={this.state.underlyingDataColumns}
        underlyingDataOptions={this.state.underlyingDataOptions}
        cashManagementData={this.props.cashManagementData}
        mmfTradeData={this.props.mmfTradeData}
        tPlusOneMmfTradeData={this.props.tPlusOneMmfTradeData}
        onCellClickHandler={this.onCellClickHandler}
        getUpdateOptions={this.getUpdateOptions}
        changeTab={this.changeTab}
        onCellClickHandlerForUnderlying={this.onCellClickHandlerForUnderlying}
        refreshTradeData={this.fetchMMFTradesForDate}
        refreshTPlusOneTradeData={this.fetchMMFTradesForTPlusOneDate}
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState,
      beginWorkflow,
      fetchMMFTradesForDate,
      fetchMMFTradesForTPlusOneDate
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    cashManagementData: state.cashManagementData.data,
    mmfTradeData: state.cashManagementData.mmfTradeData,
    tPlusOneMmfTradeData: state.cashManagementData.tPlusOneMmfTradeData,
    resizeCanvas: state.cashManagementData.resizeCanvas
  };
}
Grid.propTypes = {
  cashManagementData: PropTypes.object.isRequired,
  destroyDataState: PropTypes.func.isRequired,
  beginWorkflow: PropTypes.func.isRequired
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
