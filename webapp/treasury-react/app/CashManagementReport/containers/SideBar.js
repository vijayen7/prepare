import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCommaSeparatedValues } from '../../commons/util';
import {
  destroyDataState,
  fetchCashManagementData,
  resizeCanvas,
  fetchMMFTradesForDate,
  fetchMMFTradesForTPlusOneDate
} from '../actions';
import { CollapsableSidebar } from './../components/CollapsableSidebar';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
    this.getCurrentFilterState = this.getCurrentFilterState.bind(this);
    this.state = {};
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedLegalEntityFamilies: [],
      toggleSidebar: false,
      selectedDate: '',
      open: false
    };
  }

  getCurrentFilterState() {
    const filterState = {
      selectedLegalEntityFamilies: this.state.selectedLegalEntityFamilies
    };
    return filterState;
  }
  resizeCanvas() {
    this.props.resizeCanvas();
  }
  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  handleReset() {
    this.setState({ ...this.getDefaultFilters(), open: true });
  }

  handleClick() {
    const payloadForCashManagementData = {
      'CashManagementFilter.date': this.state.selectedDate,
      'CashManagementFilter.legalEntityFamilyIds': getCommaSeparatedValues(this.state.selectedLegalEntityFamilies)
    };
    this.props.fetchCashManagementData(payloadForCashManagementData);
    const payloadForMMFTradeData = {
      date: this.state.selectedDate
    };
    this.props.fetchMMFTradesForDate(payloadForMMFTradeData);
    this.props.fetchMMFTradesForTPlusOneDate(payloadForMMFTradeData);
    this.setState({ toggleSidebar: !this.state.toggleSidebar });
  }

  render() {
    return (
      <React.Fragment>
        <CollapsableSidebar
          toggleSidebar={this.state.toggleSidebar}
          selectedDate={this.state.selectedDate}
          selectedLegalEntityFamilies={this.state.selectedLegalEntityFamilies}
          resizeCanvas={this.resizeCanvas}
          getCurrentFilterState={this.getCurrentFilterState}
          applySavedFilters={this.applySavedFilters}
          onSelect={this.onSelect}
          handleClick={this.handleClick}
          handleReset={this.handleReset}
        />
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCashManagementData,
      destroyDataState,
      resizeCanvas,
      fetchMMFTradesForDate,
      fetchMMFTradesForTPlusOneDate
    },
    dispatch
  );
}

SideBar.propTypes = {
  resizeCanvas: PropTypes.func.isRequired,
  fetchCashManagementData: PropTypes.func.isRequired
};
export default connect(
  null,
  mapDispatchToProps
)(SideBar);
