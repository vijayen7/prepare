import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import { plainDecimalsPercentFormatter, plainNumberFormatter } from "../../commons/grid/formatters";
import { FirmLiquidityDetailsPanel } from "./../components/FirmLiquidityDetailsPanel";
const FrimLiquidityDetails = ({ cashManagementData }) => {
  return (
    <FirmLiquidityDetailsPanel
      plainNumberFormatter={plainNumberFormatter}
      plainDecimalsPercentFormatter={plainDecimalsPercentFormatter}
      totalCashBalance={cashManagementData.totalCashBalance}
      rgyxxOwnershipPercntage={cashManagementData.rgyxxOwnershipPercntage}
    />
  );
};
const mapStateToProps = (state, ownProps) => {
  return {
    cashManagementData: state.cashManagementData.data
  };
};

FrimLiquidityDetails.propTypes = {
  cashManagementData: PropTypes.object
};
export default connect(
  mapStateToProps,
  null
)(FrimLiquidityDetails);
