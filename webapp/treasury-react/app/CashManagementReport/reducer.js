import { combineReducers } from 'redux';
import {
  DESTROY_DATA_STATE_FOR_CASH_MANAGEMENT_VIEW,
  FETCH_CASH_MANAGEMENT_DATA,
  FETCH_MMF_TRADES_FOR_DATE,
  FETCH_MMF_TRADES_FOR_T_PLUS_ONE_DATE,
  RESIZE_CANVAS,
  SEG_IA,
  StateStreet,
  HSBC
} from '../commons/constants';
import { clickableAmountFormatterDefaultZero, plainNumberFormatter } from '../commons/grid/formatters';
import { convertJavaNYCDateTime, convertJavaDate, isDateToday } from '../commons/util';

function cashManagementDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_CASH_MANAGEMENT_DATA}_SUCCESS`:
      return reduceData(action) || {};
    case DESTROY_DATA_STATE_FOR_CASH_MANAGEMENT_VIEW:
      return [];
    default:
      return state;
  }
}

function mmfTradeDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_MMF_TRADES_FOR_DATE}_SUCCESS`:
      return reduceMMFTradeData(action) || [];
    default:
      return state;
  }
}
function tPlusOneMmfTradeDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_MMF_TRADES_FOR_T_PLUS_ONE_DATE}_SUCCESS`:
      return reduceMMFTradeData(action) || [];
    default:
      return state;
  }
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return !state;
    default:
      return false;
  }
}

function reduceMMFTradeData(action) {
  const { data } = action;
  for (let i = 0; i < data.length; i++) {
    data[i].primeBrokerName = data[i].primeBroker.name;
    data[i].executingBrokerName = data[i].executingBroker.name;
    data[i].bookName = data[i].book.displayName;
    data[i].tradeId = data[i].tradeKey.contract;
    data[i].date = convertJavaNYCDateTime(data[i].transactionDate);
    data[i].id = data[i].tradeId;
  }
  return data;
}

function reduceData(action) {
  const { data } = action;
  data.date = action.payload['CashManagementFilter.date'];
  data.lastUpdatedTime = new Date();
  let bonds = [];
  data.governmentBondPurchases = [];
  for (let i = 0; i < data.fundLevelLiquidityProfiles.length; i++) {
    const fundLP = data.fundLevelLiquidityProfiles[i];

    fundLP.adjustments = reduceCashManagementAdjustments(fundLP.adjustments);

    fundLP.id = fundLP.fundId;

    fundLP.brokerCallAgreementsList = reduceCashManagementAgreementBrokerData(
      fundLP.brokerFilesStatusTracker.brokerCallData
    );
    fundLP.internalCallAgreementsList = reduceCashManagementAgreementBrokerData(
      fundLP.brokerFilesStatusTracker.internalCallData
    );
    fundLP.brokerPullAgreementsList = reduceCashManagementAgreementBrokerData(
      fundLP.brokerFilesStatusTracker.brokerPullData
    );
    fundLP.internalPullAgreementsList = reduceCashManagementAgreementBrokerData(
      fundLP.brokerFilesStatusTracker.internalPullData
    );
    fundLP.agreements = reduceFundLPAgreements(fundLP);

    fundLP.parsedPercentage = fundLP.brokerFilesStatusTracker.parsedPercentage;
    fundLP.brokerFilesStatusTracker = {};
    fundLP.closingCashBalance = fundLP.cashBalance + fundLP.wireAmount;
    fundLP.totalFundValue = fundLP.closingCashBalance + fundLP.bondValue + fundLP.mmfBalance;

    fundLP.governmentBondPurchases = reduceGovtBondPurchases(fundLP.bondTrades, fundLP.fundName);
    data.governmentBondPurchases = data.governmentBondPurchases.concat(fundLP.governmentBondPurchases);
    updateId(fundLP.wires);
    updateId(fundLP.outflowWires);
    updateId(fundLP.cashBalances);
    let currencies = getBondCurrencies(fundLP.bondHoldings,fundLP.bondTrades);
    for(let key of currencies){
      fundLP[key + 'Positions'] = reduceGovtBondPositions(fundLP.bondHoldings, key, data.date);
      fundLP[key + ' Maturing_MaturedPositions'] = fundLP[key + 'Positions'].filter((x) => x.isMatured);
      fundLP[key +  ' SettlingToday_Trades' ] = getTodaySettlingTrades(key,fundLP.governmentBondPurchases);
      bonds.push(fundLP[key + 'Positions']);
   }
    reduceActionValue(fundLP);
  }

  data.rgyxxOwnershipPercntage = reduceRgyxxFirmOwnershipPercentage(data);
  bonds = bonds.flat();
  let allBonds = {};
  bonds.flatMap((y) => (allBonds[y.spn] = y));
  allBonds = Object.values(allBonds);
  updateId(bonds);
  data.allBonds = bonds.filter((a)=>!a.alreadyMatured);

  const maturityDateSet = new Set();
  bonds.map((x) => maturityDateSet.add(x.maturityDate));
  const maturingBondPositions = [];

  bonds.forEach((b) => {
    if (maturityDateSet.has(b.maturityDate)) {
      maturingBondPositions.push(b);
    }
  });
  data.maturingBondPositions = maturingBondPositions;
  data.mmfSummaryList = reduceMMFSummary(data);
  data.mmfSummaryMap = {};
  const { orderedColumns, newGroups } = reduceOrderedColumnsAndGroupings(data.fundLevelLiquidityProfiles);

  data.columns = orderedColumns;
  data.groupColumns = newGroups;

  return data;
}

export function reduceActionValue(fundLP) {
  let formattedActionAmount = plainNumberFormatter(undefined, undefined, fundLP.actionAmount, undefined, undefined);
  fundLP.actionValue =
    fundLP.action === 'REDEEM' || fundLP.action === 'INVEST'
      ? fundLP.action + ' ' + formattedActionAmount
      : fundLP.action;
}
export function getTodaySettlingTrades(key,governmentBondPurchases){
  let todaySettlingTrades = [];
  for(let trade of governmentBondPurchases){
    if(trade.settleCurrencyIsoCode === key){
      todaySettlingTrades = todaySettlingTrades.concat(trade);
    }
  }
  return todaySettlingTrades;
}
export function reduceGovtBondPositions(bondHoldings, currencyCode, searchDate) {
  let positions = [];

  if (bondHoldings && bondHoldings.length > 0) {
    bondHoldings.forEach((bondHolding) => {
      let bond = bondHolding.bond;
      let refBond = {
        name: bond.displayName,
        spn: bond.spn,
        cusip: bond.cusip,
        currencyCode: bond.currencyCode,
        custodianAccount: bondHolding.custodianAccount.displayName,
        maturityDate: bondHolding.bond.maturityDate ? convertJavaDate(bondHolding.bond.maturityDate) : undefined
      };

      searchDate = searchDate.split("-").join("")
      if (bond.currencyCode === currencyCode && bondHolding.underlyingPosition) {
        positions.push({
          ...refBond,
          marketValueUsd: bondHolding.underlyingPosition.settleDateMarketValue * bondHolding.underlyingPosition.fxRate,
          isMatured: searchDate === refBond.maturityDate,
          alreadyMatured: searchDate >refBond.maturityDate
        });
      }
    });
  }
  updateId(positions);
  return positions;
}

export function reduceFundLPAgreements(fundLP) {
  let agreementsMap = {};
  const PARSED = 'PARSED';
  const UNPARSED = 'UNPARSED';
  agreementsMap = { ...agreementsMap, ...getAgreementMap(fundLP.brokerCallAgreementsList, PARSED) };
  agreementsMap = { ...agreementsMap, ...getAgreementMap(fundLP.internalCallAgreementsList, UNPARSED) };
  agreementsMap = { ...agreementsMap, ...getAgreementMap(fundLP.brokerPullAgreementsList, PARSED) };
  agreementsMap = { ...agreementsMap, ...getAgreementMap(fundLP.internalPullAgreementsList, UNPARSED) };
  let agreementsList = [];
  Object.entries(agreementsMap).forEach(([agId, ag]) => {
    agreementsList.push(ag);
  });
  return agreementsList;
}

export function getAgreementMap(agreementsList, isParsed) {
  let agreementMap = {};
  agreementsList.forEach((ag) => {
    let agreement = agreementMap[ag.agreementId];
    if (agreement === undefined) {
      agreement = {};
      agreement.legalEntity = ag.legalEntity;
      agreement.counterpartyEntity = ag.counterpartyEntity;
      agreement.agreementType = ag.agreementType;
      agreement.agreementId = ag.agreementId;
      agreement.isParsed = isParsed;
      agreement.id = ag.agreementId;
      agreementMap[ag.agreementId] = agreement;
    }
  });
  return agreementMap;
}

export function reduceCashManagementAgreementBrokerData(cashManagementAgreementBrokerDataList) {
  const agreementsList = [];
  if (!cashManagementAgreementBrokerDataList) {
    return agreementsList;
  }
  for (let j = 0; j < cashManagementAgreementBrokerDataList.length; j++) {
    const agreement = {};
    // TODO: cant agreementId be unique id
    agreement.id = j;
    const cashManagementAgreementBrokerData = cashManagementAgreementBrokerDataList[j];
    const { agreementInfo } = cashManagementAgreementBrokerData;
    agreement.legalEntity = agreementInfo.legalEntity.name;
    agreement.counterpartyEntity = agreementInfo.counterpartyEntity.name;
    agreement.agreementType = agreementInfo.agreementType.abbrev;
    agreement.agreementId = agreementInfo.agreementId;
    agreement.ed = cashManagementAgreementBrokerData.ed;
    agreement.exposure = cashManagementAgreementBrokerData.exposure;
    agreement.collateral = cashManagementAgreementBrokerData.collateral;
    agreement.margin = cashManagementAgreementBrokerData.margin;
    agreement.segEd = cashManagementAgreementBrokerData.segEd;
    agreement.segCollateral = cashManagementAgreementBrokerData.segCollateral;
    agreement.segMargin = cashManagementAgreementBrokerData.segMargin;

    agreementsList.push(agreement);
  }
  return agreementsList;
}

function reduceMMFSummary(data) {
  const mmfSummaryList = [];
  let k = 0;
  Object.entries(data.mmfSummaryMap).forEach(([ticker, mmfSummaryEntry]) => {
    const mmfSummaryData = {};
    mmfSummaryData.id = k++;
    mmfSummaryData.ticker = mmfSummaryEntry.mmf.ticker;
    mmfSummaryData.mmfAumUsd = mmfSummaryEntry.mmf.mmfAumUsd;
    mmfSummaryData.yield = mmfSummaryEntry.mmf.yield;
    mmfSummaryData.expenseRatio = mmfSummaryEntry.mmf.expenseRatio;
    mmfSummaryData.wam = mmfSummaryEntry.mmf.wam;
    mmfSummaryData.ownershipAmount = mmfSummaryEntry.firmLevelOwnershipAmount;
    mmfSummaryData.ownershipPercentage = mmfSummaryEntry.firmLevelOwnershipPercentage;
    mmfSummaryList.push(mmfSummaryData);
  });

  return mmfSummaryList;
}

function reduceRgyxxFirmOwnershipPercentage(data) {
  const rgyxxAum = data.mmfSummaryMap.RGYXX !== undefined && data.mmfSummaryMap.RGYXX.mmf.mmfAumUsd;
  const totalMMFAgreementTypeBalance = data.fundLevelLiquidityProfiles
    .flatMap((x) => {
      return x.cashBalances;
    })
    .filter((y) => {
      return y.isBalanceOfMMFAgreementType;
    })
    .map((d) => {
      return d.balanceUsd;
    })
    .reduce((a, b) => a + b, 0);
  let fundOwnershipPercentageInRGYXX = 100 * (totalMMFAgreementTypeBalance / rgyxxAum);
  if (isNaN(fundOwnershipPercentageInRGYXX)) {
    fundOwnershipPercentageInRGYXX = 0;
  }
  return fundOwnershipPercentageInRGYXX;
}

function reduceOrderedColumnsAndGroupings(fundLevelLiquidityProfiles) {
  const data = {};
  data.columns = {};
  const newColumns = {};
  const updatedColumns = [];
  const groupColumns = [];
  let currencies = new Set();
  fundLevelLiquidityProfiles.forEach((fundLp) => {
    const holdigs = fundLp.mmfHoldings.concat(fundLp.segIAMmfHoldings);
    holdigs.forEach((mmfHolding) => {
      if (
        Object.prototype.hasOwnProperty.call(mmfHolding, 'balanceUsd') &&
        mmfHolding.balanceUsd !== 0 &&
        mmfHolding.balanceUsd != null &&
        mmfHolding.balanceUsd !== undefined
      ) {
        const source = mmfHolding.source === 'N/A' ? SEG_IA : mmfHolding.source;
        const custodianAbbrev = mmfHolding.custodian.abbrev;
        const key = `${mmfHolding.mmf.ticker} ${source} ${custodianAbbrev}`;
        let fundProvider = mmfHolding.mmf.fundProvider;
        let fundProviderInfo = fundProvider;

        if (source === SEG_IA) {
          fundProviderInfo = 'Segregated Initial Margin Balances invested in ' + fundProvider + ' through a sweep';
        } else if (source === StateStreet) {
          fundProviderInfo =
            fundProvider + ' investments custodied at ' + mmfHolding.custodian.name + ' (traded through State Street)';
        } else if (source === HSBC) {
          fundProviderInfo = fundProvider + ' investments custodied at ' + mmfHolding.custodian.name;
        }

        const refData = {
          fundProviderName: fundProviderInfo
        };
        if (newColumns[mmfHolding.mmf.ticker] === undefined) {
          newColumns[mmfHolding.mmf.ticker] = {};
        }
        newColumns[mmfHolding.mmf.ticker][key] = '';
        data.columns[key] = refData;
        let balanceSumUsd = mmfHolding.balanceUsd;
        if (!isNaN(fundLp[key])) {
          balanceSumUsd += fundLp[key];
        }
        fundLp[key] = balanceSumUsd;
        fundLp[`${key} FundProvider`] = mmfHolding.fundProviderName;
      }
    });
  });

  // group colums by ticker
  Object.entries(newColumns).forEach((item) => {
    if (Object.keys(item[1]).length > 1) {
      const tmp = [];
      tmp.push(item[0]);
      Object.keys(item[1]).forEach((it) => {
        tmp.push(it);
        updatedColumns.push(it);
      });
      groupColumns.push(tmp);
    } else {
      const tmp = [];
      tmp.push(item[0]);
      tmp.push(Object.keys(item[1])[0]);
      tmp.push(Object.keys(item[1])[0]);
      updatedColumns.push(Object.keys(item[1])[0]);
      groupColumns.push(tmp);
    }
  });

  fundLevelLiquidityProfiles.forEach((fundLp) => {
    if (fundLp.currencyToBondValueMap) {
      Object.entries(fundLp.currencyToBondValueMap).forEach(([key, value]) => {
        fundLp[key] = value;
        if (!currencies.has(key)) {
          currencies.add(key);
          updatedColumns.push(key);
          let fundProviderName = key + ' Government Bond';
          if (key === 'USD') {
            fundProviderName = 'U.S. Treasury Holdings';
          } else if (key === 'JPY') {
            fundProviderName = 'Japanese Government Bond Holdings';
          }
          const refData = {
            fundProviderName: fundProviderName,
            formatter: clickableAmountFormatterDefaultZero
          };
          data.columns[key] = refData;
        }
      });
    }
  });

  groupColumns.sort();
  const newGroups = [];
  // add pre defined columsn to a empty group
  if (currencies.size > 0) {
    let currenciesArray = Array.from(currencies);
    newGroups.push(['Government Bonds', currenciesArray[0], currenciesArray[currencies.size - 1]]);
  }
  newGroups.push(['', 'beginWorkflow', 'expectedCashOutflowAmount']);
  newGroups.push(['Margin Calls', 'brokerCallUSD', 'internalCallUSD']);
  newGroups.push(['Margin Pulls', 'brokerPullUSD', 'internalPullUSD']);
  // HACK: making single element groups work by giveing second elelment same as first element
  groupColumns.forEach((item) => {
    if (item.length > 3) {
      newGroups.push([item[0], item[1], item[item.length - 1]]);
    } else {
      newGroups.push(item);
    }
  });
  // Ordering columns as per the grouping order
  const orderedColumns = [];
  updatedColumns.forEach((item) => {
    const key = item;
    const value = data.columns[item];
    orderedColumns.push({ [`${key}`]: value });
  });
  return { orderedColumns, newGroups };
}

function updateId(data) {
  for (let i = 0; i < data.length; i++) data[i].id = i;
}

function reduceCashManagementAdjustments(adjustments) {
  if (adjustments != undefined && adjustments.length > 0) {
    return adjustments.map((adjustment) => {
      adjustment.startDate = convertJavaNYCDateTime(adjustment.startDate);
      adjustment.endDate = convertJavaNYCDateTime(adjustment.endDate);
      return adjustment;
    });
  }
  return adjustments;
}

function reduceGovtBondPurchases(bondTrades, fundName) {
  let governmentBondPurchases = [];
  bondTrades.forEach((trade) => {
    let bondTradeDetail = {};
    if (trade.tradeSecurity) {
      let tradeSecurity = trade.tradeSecurity;
      bondTradeDetail.spn = tradeSecurity.spn;
      bondTradeDetail.cusip = tradeSecurity.cusip;
      bondTradeDetail.name = tradeSecurity.desname;
      bondTradeDetail.maturityDate = convertJavaDate(tradeSecurity.death);
    }
    if (trade.custodianAccount) {
      bondTradeDetail.custodianAccount = trade.custodianAccount.displayName;
    }
    bondTradeDetail.settleDate = convertJavaDate(trade.settleDate);
    bondTradeDetail.quantity = trade.quantity;
    bondTradeDetail.notionalGrossMoney = trade.notionalGrossMoney;
    bondTradeDetail.settleCurrencyIsoCode = trade.settleCurrencyIsoCode;
    bondTradeDetail.id = trade.tradeId;
    bondTradeDetail.fundName = fundName;
    bondTradeDetail.isSettlingToday = isDateToday(trade.settleDate);
    governmentBondPurchases.push(bondTradeDetail);
  });
  return governmentBondPurchases;
}
function getBondCurrencies(bondHoldings,bondTrades){
  let currencies = new Set();
  for(let bondHolding of bondHoldings){
    let currencyCode = bondHolding.bond.currencyCode;
    currencies.add(currencyCode);
  }
  for(let bondTrade of bondTrades){
    let currencyCode = bondTrade.settleCurrencyIsoCode;
    currencies.add(currencyCode);
  }
  return currencies;
}
function reduceGovtBondHoldings(bondHoldings) {
  let currencyToUnderlyingPositionsMap = new Map();

  if (bondHoldings && bondHoldings.length > 0) {
    bondHoldings.forEach((bondHolding) => {
      let currencyCode = bondHolding.bond.currencyCode;
      if (!currencyToUnderlyingPositionsMap.has(currencyCode)) {
        currencyToUnderlyingPositionsMap.set(currencyCode, []);
      }
      currencyToUnderlyingPositionsMap.get(currencyCode).push(bondHolding.underlyingPosition);
    });
  }

  return currencyToUnderlyingPositionsMap;
}

const rootReducer = combineReducers({
  data: cashManagementDataReducer,
  resizeCanvas: resizeCanvasReducer,
  mmfTradeData: mmfTradeDataReducer,
  tPlusOneMmfTradeData: tPlusOneMmfTradeDataReducer
});

export default rootReducer;
