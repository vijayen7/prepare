import { reduceCashManagementAgreementBrokerData } from '../reducer';
describe('Underlying Data for BrokerData test', () => {
  it('should return a list', () => {
    expect(reduceCashManagementAgreementBrokerData(undefined)).toEqual([]);
    expect(reduceCashManagementAgreementBrokerData([])).toEqual([]);
  });
  it('should reduce agreementBrokerData to desired shape', () => {
    const agreementBrokerDataList = [
      {
        '@CLASS': 'com.arcesium.treasury.lcm.common.cashmanagement.CashManagementAgreementData',
        agreementInfo: {
          '@CLASS': 'com.arcesium.treasury.model.common.AgreementInfo',
          legalEntity: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 1765215,
            name: 'D. E. Shaw Alkali Portfolios II, L.L.C.'
          },
          counterpartyEntity: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 1760946,
            name: 'Citigroup Global Markets Inc'
          },
          agreementType: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 7,
            abbrev: 'MRA'
          },
          agreementId: 6117
        },
        ed: -209359,
        exposure: 6276245.62,
        collateral: -12809,
        margin: 6473942.4
      },
      {
        '@CLASS': 'com.arcesium.treasury.lcm.common.cashmanagement.CashManagementAgreementData',
        agreementInfo: {
          '@CLASS': 'com.arcesium.treasury.model.common.AgreementInfo',
          legalEntity: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 1765215,
            name: 'D. E. Shaw Alkali Portfolios II, L.L.C.'
          },
          counterpartyEntity: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 1759543,
            name: 'Barclays Bank PLC'
          },
          agreementType: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 8,
            abbrev: 'GMRA'
          },
          agreementId: 5852
        },
        ed: -419411,
        exposure: 2766471,
        collateral: -343944,
        margin: 2841938
      },
      {
        '@CLASS': 'com.arcesium.treasury.lcm.common.cashmanagement.CashManagementAgreementData',
        agreementInfo: {
          '@CLASS': 'com.arcesium.treasury.model.common.AgreementInfo',
          legalEntity: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 1765215,
            name: 'D. E. Shaw Alkali Portfolios II, L.L.C.'
          },
          counterpartyEntity: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 1759554,
            name: 'Credit Suisse International'
          },
          agreementType: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 6,
            abbrev: 'ISDA'
          },
          agreementId: 5853
        },
        ed: -124486.8381,
        exposure: -12046105.0871,
        collateral: 14460057.86,
        margin: 2538439.611
      },
      {
        '@CLASS': 'com.arcesium.treasury.lcm.common.cashmanagement.CashManagementAgreementData',
        agreementInfo: {
          '@CLASS': 'com.arcesium.treasury.model.common.AgreementInfo',
          legalEntity: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 1765215,
            name: 'D. E. Shaw Alkali Portfolios II, L.L.C.'
          },
          counterpartyEntity: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 1759543,
            name: 'Barclays Bank PLC'
          },
          agreementType: {
            '@CLASS': 'arcesium.treasury.model.common.ReferenceData',
            id: 6,
            abbrev: 'ISDA'
          },
          agreementId: 6576
        },
        ed: -332703.63,
        exposure: 1319249,
        collateral: -1651952.63,
        margin: 0,
        segEd: -120290.9098,
        segMargin: 120290.9098
      }
    ];
    expect(reduceCashManagementAgreementBrokerData(agreementBrokerDataList)).toMatchSnapshot();
  });
});
