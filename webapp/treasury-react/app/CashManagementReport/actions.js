import {
  DESTROY_DATA_STATE_FOR_CASH_MANAGEMENT_VIEW,
  FETCH_CASH_MANAGEMENT_DATA,
  FETCH_MMF_TRADES_FOR_T_PLUS_ONE_DATE,
  RESIZE_CANVAS,
  FETCH_MMF_TRADES_FOR_DATE
} from "../commons/constants";

export function fetchCashManagementData(payload) {
  return {
    type: FETCH_CASH_MANAGEMENT_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_CASH_MANAGEMENT_VIEW
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

export function fetchMMFTradesForDate(payload) {
  return {
    type: FETCH_MMF_TRADES_FOR_DATE,
    payload
  };
}
export function fetchMMFTradesForTPlusOneDate(payload) {
  return {
    type: FETCH_MMF_TRADES_FOR_T_PLUS_ONE_DATE,
    payload
  };
}
