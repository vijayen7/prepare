/* eslint-disable no-restricted-globals */
import { getSaveSettingsUrl } from "../../commons/util.js";

export function getResultOptions() {
  return {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    expandTillLevel: -1,
    highlightRow: true,
    highlightRowOnClick: true,
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    cellRangeSelection: {
      showAggregations: true,
      multipleRangesEnabled: true
    },
    enableMultilevelGrouping: {
      isAddGroupingColumn: true
    },
    isFilterOnFormattedValue: true,
    customColumnSelection: true,
    configureColumns: true,
    copyCellSelection: true,
    formatAggregateSummary(formattedText, AggregationsObj) {
      return(
        `<div class="size--content layout--flex" style="float: right; width: fit-content;">
          <div class="size--content margin--left--small margin--right--small">Count:`+Math.round(isNaN(AggregationsObj.count) ? 0: AggregationsObj.count)+`</div>
          <div class="size--content margin--left--small margin--right--small">Sum:`+Math.round(isNaN(AggregationsObj.sum) ? 0 : AggregationsObj.sum)+`</div>
          <div class="size--content margin--left--small margin--right--small">Average:`+Math.round(isNaN(AggregationsObj.avg) ? 0 : AggregationsObj.avg)+`</div>
        </div>`
       )
    }
  };
}

export function getTopResultOptions() {
  const options = getResultOptions();
  options.displaySummaryRow = true;
  options.saveSettings = {
    applicationId: 3,
    applicationCategory: "cashManagementTopGrid",
    serviceURL: getSaveSettingsUrl()
  };
  options.frozenColumn = 1;
  options.sortList = [
    {
      columnId: "fundName",
      sortAsc: true
    }
  ];
  options.sheetName = "Cash Management Report";
  return options;
}

export function getSummaryResultOptions() {
  const options = getResultOptions();
  options.saveSettings = {
    applicationId: 3,
    applicationCategory: "cashManagementSummaryGrid",
    serviceURL: getSaveSettingsUrl()
  };
  options.sortList = [
    {
      columnId: "ownershipPercentage",
      sortAsc: false
    }
  ];
  options.sheetName = "Cash Management MMF Summary";
  return options;
}

export function getUnderlyingResultOptions() {
  const options = getResultOptions();
  return options;
}

export function getTradeResultOptions() {
  const options = getUnderlyingResultOptions();
  options.enableMultilevelGrouping = {
    isAddGroupingColumn: true,
    initialGrouping: ["comment"]
  };
  return options;
}
export function getGovtBondResultOptions() {
  const options = getUnderlyingResultOptions();
  options.enableMultilevelGrouping = {
    isAddGroupingColumn: true,
    initialGrouping: ['maturityDate']
  };
  options.sortList = [{ columnId: 'maturityDate', sortAsc: true }];

  return options;
}
export function getGovtBondTradesResultOptions() {
  const options = getUnderlyingResultOptions();
  options.enableMultilevelGrouping = {
    isAddGroupingColumn: true,
    initialGrouping: ["settleDate"]
  };
  return options;
}
