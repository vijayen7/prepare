/* eslint-disable no-undef */
import { SummationAggregator } from '../../commons/grid/aggregators';
import {
  clickableAmountFormatterDefaultZero,
  groupFormatter,
  linkFormatter,
  numberFormatterDefaultZero,
  percentFormatterDefaultZero,
  priceFormatterDefaultZero,
  textFormatter,
  twoDecimalsPercentFormatterDefaultZero
} from '../../commons/grid/formatters';

export function getTopPanelColumns(columnsList) {
  const columns = [
    {
      id: 'beginWorkflow',
      type: 'text',
      formatter(row, cell, value, columnDef, dataContext) {
        // eslint-disable-next-line quotes
        return linkFormatter(row, cell, '<i class="icon-edit--block margin--left"></i>', columnDef, dataContext);
      },
      name: '',
      field: 'beginWorkflow',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'fundName',
      type: 'text',
      formatter: textFormatter,
      toolTip : 'Legal Entity / Legal Entity Family',
      name: 'Entity',
      field: 'fundName',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'cashBalance',
      type: 'number',
      toolTip: 'Start of Day Cash Balance',
      formatter: clickableAmountFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Opening Cash Balance',
      field: 'cashBalance',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'wireAmount',
      type: 'number',
      formatter: clickableAmountFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Net Cash Flows',
      toolTip : 'Net of all inflows and outflows for the entities operating cash account',
      field: 'wireAmount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'closingCashBalance',
      type: 'number',
      toolTip: 'Sum of Opening Cash Balance plus the impact of net cash flows',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Projected Closing Balance',
      field: 'closingCashBalance',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'actionValue',
      type: 'text',
      formatter: textFormatter,
      name: 'Action Required - Cash',
      toolTip : 'Money Market Fund action to be taken to revert operating cash balances to its target',
      field: 'actionValue',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'expectedCashOutflowAmount',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Intraday Funding Requirement (Expected)',
      toolTip : 'Total Intraday Funding Requirements = Margin Calls + Other Cash Outflow + Adjustment Amount',
      field: 'expectedCashOutflowAmount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'brokerCallUSD',
      type: 'number',
      formatter: clickableAmountFormatterDefaultZero,
      toolTip:'Parsed Broker Margin Deficit',
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'External Data',
      field: 'brokerCallUSD',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'internalCallUSD',
      type: 'number',
      formatter: clickableAmountFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Internal (unparsed)',
      toolTip:'Internal data supplementing unparsed Broker Margin deficit',
      field: 'internalCallUSD',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'brokerPullUSD',
      type: 'number',
      formatter: clickableAmountFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'External Data',
      toolTip:'Parsed Broker Margin Excess',
      field: 'brokerPullUSD',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'internalPullUSD',
      type: 'number',
      formatter: clickableAmountFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Internal (unparsed)',
      toolTip:'Internal data supplementing unparsed Broker Margin excess',
      field: 'internalPullUSD',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'parsedPercentage',
      type: 'text',
      formatter: percentFormatterDefaultZero,
      name: 'Files Parsed (%)',
      toolTip : '% of agreements in COMET with parsed broker data',
      field: 'parsedPercentage',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'wireOutFlowAmount',
      type: 'number',
      formatter: clickableAmountFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Other Cash Outflow',
      toolTip : 'Non-Margin Funding Requirements',
      field: 'wireOutFlowAmount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'adjustmentAmount',
      type: 'number',
      formatter: clickableAmountFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Adjustment Amount',
      toolTip : 'Adjustment amount for broker margin parsing related issues and/or any funding requirements not yet booked',
      field: 'adjustmentAmount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }


  ];

  columnsList.forEach((item) => {
    const key = Object.keys(item)[0];
    const refData = item[key];
    columns.push({
      id: key,
      type: 'number',
      toolTip: refData.fundProviderName,
      formatter : refData.formatter ? refData.formatter : numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: key,
      field: key,
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    });
  });

  columns.push({
    id: 'totalFundValue',
    type: 'number',
    formatter: numberFormatterDefaultZero,
    aggregator: dpGrid.Aggregators.sum,
    groupingAggregator: SummationAggregator,
    groupTotalsFormatter: groupFormatter,
    name: 'Liquidity Pool Balance',
    toolTip : 'Total liquidity pool balance taking into account current net cash flows',
    field: 'totalFundValue',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  });

  columns.push({
    id: 'minThreshold',
    type: 'number',
    formatter: numberFormatterDefaultZero,
    aggregator: dpGrid.Aggregators.sum,
    groupingAggregator: SummationAggregator,
    groupTotalsFormatter: groupFormatter,
    name: 'Cash Target Min',
    toolTip : 'Minimum balance below which will trigger a money market redemption to increase operating cash to its target midpoint',
    field: 'minThreshold',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  });

  columns.push({
    id: 'maxThreshold',
    type: 'number',
    formatter: numberFormatterDefaultZero,
    aggregator: dpGrid.Aggregators.sum,
    groupingAggregator: SummationAggregator,
    groupTotalsFormatter: groupFormatter,
    name: 'Cash Target Max',
    toolTip : 'Maximum balance above which will trigger a money market investment to decrease operating cash to its target midpoint',
    field: 'maxThreshold',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  });

  columns.push({
    id: 'midThreshold',
    type: 'number',
    formatter: numberFormatterDefaultZero,
    aggregator: dpGrid.Aggregators.sum,
    groupingAggregator: SummationAggregator,
    groupTotalsFormatter: groupFormatter,
    name: 'Cash Target Mid',
    toolTip : 'Target operating cash balance should the projected operating cash balance breach the minimum target or breach the maximum target',
    field: 'midThreshold',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  });





  return columns;
}

export function getSummaryPanelColumns(fundOwnershipThresholds) {
  const columns = [
    {
      id: 'ticker',
      type: 'text',
      formatter: textFormatter,
      name: 'Ticker',
      field: 'ticker',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'ownershipAmount',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      name: 'Balance USD',
      field: 'ownershipAmount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'mmfAumUsd',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      name: 'MMF AUM USD',
      field: 'mmfAumUsd',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'expenseRatio',
      type: 'number',
      formatter: priceFormatterDefaultZero,
      name: 'Expense Ratio',
      field: 'expenseRatio',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'yield',
      type: 'number',
      formatter: twoDecimalsPercentFormatterDefaultZero,
      name: 'Yield',
      field: 'yield',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'ownershipPercentage',
      type: 'number',
      formatter(row, cell, value, columnDef, dataContext) {
        if (value != undefined && value != null) {
          let className = 'success';
          if (value >= fundOwnershipThresholds.RED) className = 'critical';
          else if (value >= fundOwnershipThresholds.AMBER) className = 'warning';
          return twoDecimalsPercentFormatterDefaultZero(null, null, value, columnDef, null, className);
        }
        return twoDecimalsPercentFormatterDefaultZero(null, null, null, columnDef, null, null);
      },
      name: 'Ownership Percentage',
      field: 'ownershipPercentage',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }
  ];
  return columns;
}

export function getWireDetailPanelColumns() {
  const columns = [
    {
      id: 'wireId',
      type: 'text',
      formatter: linkFormatter,
      name: 'Wire Id',
      field: 'wireId',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'category',
      type: 'text',
      formatter: textFormatter,
      name: 'Category',
      field: 'category',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'wireAmount',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Wire Amount',
      field: 'wireAmount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'sourceWireAccount',
      type: 'text',
      formatter: textFormatter,
      name: 'Source Wire Account',
      field: 'sourceWireAccount',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'destinationWireAccount',
      type: 'text',
      formatter: textFormatter,
      name: 'Destination Wire Account',
      field: 'destinationWireAccount',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'sourceCustodianAccountId',
      type: 'text',
      formatter: textFormatter,
      name: 'Source Custodian Account Id',
      field: 'sourceCustodianAccountId',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'destinationCustodianAccountId',
      type: 'text',
      formatter: textFormatter,
      name: 'Destination Custodian Account Id',
      field: 'destinationCustodianAccountId',
      sortable: true,
      headerCssClass: 'aln-rt b'
    }
  ];
  return columns;
}

export function getAgreementParsingStatusDetailPanelColumns() {
  const columns = [
    {
      id: 'agreementId',
      type: 'text',
      formatter: textFormatter,
      name: 'Agreement Id',
      field: 'agreementId',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'legalEntity',
      type: 'text',
      formatter: textFormatter,
      name: 'Legal Entity',
      field: 'legalEntity',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'counterpartyEntity',
      type: 'text',
      formatter: textFormatter,
      name: 'Counterparty Entity',
      field: 'counterpartyEntity',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'agreementType',
      type: 'text',
      formatter: textFormatter,
      name: 'Agreement Type',
      field: 'agreementType',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'isParsed',
      type: 'text',
      formatter: textFormatter,
      name: 'Parsing Status',
      field: 'isParsed',
      sortable: true,
      headerCssClass: 'aln-rt b'
    }
  ];
  return columns;
}

export function getWorkflowPanelColumns() {
  const columns = [
    {
      id: 'mmf',
      type: 'text',
      formatter: textFormatter,
      name: 'MMF',
      field: 'mmf',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'custodian',
      type: 'text',
      formatter: textFormatter,
      name: 'Custodian',
      field: 'custodian',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'balance',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Current Balance',
      field: 'balance',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'mmfAum',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'MMF AUM',
      field: 'mmfAum',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'entityOwnership',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Entity Ownership',
      field: 'entityOwnership',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'descoOwnership',
      type: 'number',
      formatter: twoDecimalsPercentFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Desco Ownership',
      field: 'descoOwnership',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'yield',
      type: 'number',
      formatter: twoDecimalsPercentFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Yield',
      field: 'yield',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'expenseRatio',
      type: 'number',
      formatter: priceFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Expense Ratio',
      field: 'expenseRatio',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }
  ];
  return columns;
}
export function getInternalDetailPanelColumns() {
  const columns = [
    {
      id: 'agreementId',
      type: 'text',
      formatter: textFormatter,
      name: 'Agreement Id',
      field: 'agreementId',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'legalEntity',
      type: 'text',
      formatter: textFormatter,
      name: 'Legal Entity',
      field: 'legalEntity',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'counterpartyEntity',
      type: 'text',
      formatter: textFormatter,
      name: 'Counterparty Entity',
      field: 'counterpartyEntity',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'agreementType',
      type: 'text',
      formatter: textFormatter,
      name: 'Agreement Type',
      field: 'agreementType',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'exposure',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Exposure',
      field: 'exposure',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'collateral',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Collateral',
      field: 'collateral',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'margin',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Margin',
      field: 'margin',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'ed',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'ED',
      field: 'ed',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'segCollateral',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Seg Collateral',
      field: 'segCollateral',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'segMargin',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Seg Margin',
      field: 'segMargin',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'segEd',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Seg ED',
      field: 'segEd',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }
  ];
  return columns;
}
export function getBrokerDetailPanelColumns() {
  const columns = [
    {
      id: 'agreementId',
      type: 'text',
      formatter: textFormatter,
      name: 'Agreement Id',
      field: 'agreementId',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'legalEntity',
      type: 'text',
      formatter: textFormatter,
      name: 'Legal Entity',
      field: 'legalEntity',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'counterpartyEntity',
      type: 'text',
      formatter: textFormatter,
      name: 'Counterparty Entity',
      field: 'counterpartyEntity',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'agreementType',
      type: 'text',
      formatter: textFormatter,
      name: 'Agreement Type',
      field: 'agreementType',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'exposure',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Exposure',
      field: 'exposure',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'collateral',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Collateral',
      field: 'collateral',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'margin',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Margin',
      field: 'margin',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'ed',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Broker ED',
      field: 'ed',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'segCollateral',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Seg Collateral',
      field: 'segCollateral',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'segMargin',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Seg Margin',
      field: 'segMargin',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'segEd',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Seg Broker ED',
      field: 'segEd',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }
  ];
  return columns;
}

export function getCashBalancesDetailColumns() {
  const columns = [
    {
      id: 'accountNumber',
      type: 'text',
      formatter: textFormatter,
      name: 'Account Number',
      field: 'accountNumber',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'balanceUsd',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: groupFormatter,
      name: 'Balance USD',
      field: 'balanceUsd',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }
  ];
  return columns;
}

export function getAdjustmentDetailColumns() {
  return [
    {
      id: 'startDate',
      name: 'Start Date',
      field: 'startDate',
      toolTip: 'Start Date',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'endDate',
      name: 'End Date',
      field: 'endDate',
      toolTip: 'End Date',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'comment',
      name: 'Comment',
      field: 'comment',
      toolTip: 'Comment',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'login',
      name: 'User',
      field: 'login',
      toolTip: 'User',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'adjustment',
      name: 'Amount',
      field: 'adjustment',
      toolTip: 'Amount',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }
  ];
}

export function getMMFTradeDataPanelColumns() {
  return [
    {
      id: 'tradeId',
      name: 'Trade Id',
      field: 'tradeId',
      toolTip: 'Trade Id',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'date',
      name: 'Date',
      field: 'date',
      toolTip: 'Date',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'executingBrokerName',
      name: 'Executing Broker Name',
      field: 'executingBrokerName',
      toolTip: 'Executing Broker Name',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'primeBrokerName',
      name: 'Prime Broker Name',
      field: 'primeBrokerName',
      toolTip: 'Prime Broker Name',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'quantity',
      name: 'Quantity',
      field: 'quantity',
      toolTip: 'Quantity',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: 'Sum',
      groupTotalsFormatter: groupFormatter,
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'bookName',
      name: 'Book',
      field: 'bookName',
      toolTip: 'Book',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'comment',
      name: 'Description',
      field: 'comment',
      toolTip: 'Description',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    }
  ];
}

export function getGovtBondsDetailPanelColumns() {
  return [

    {
      id: 'cusip',
      name: 'Cusip',
      field: 'cusip',
      toolTip: 'CUSIP of the bond',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'spn',
      name: 'SPN',
      field: 'spn',
      toolTip: 'SPN of the bond',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'name',
      name: 'Display Name',
      field: 'name',
      toolTip: 'Bond Display Name',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'currencyCode',
      name: 'Currency Code',
      field: 'currencyCode',
      toolTip: 'Currency Code',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'custodianAccount',
      name: 'Custodian Account',
      field: 'custodianAccount',
      toolTip: 'custodianAccount',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'marketValueUsd',
      name: 'MarketValue USD',
      field: 'marketValueUsd',
      toolTip: 'Market Value USD',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: 'Sum',
      groupTotalsFormatter: groupFormatter,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'maturityDate',
      name: 'Maturity Date',
      field: 'maturityDate',
      toolTip: 'Bond Maturity Date',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    }
  ];
}
export function getGovtBondsTradeDetailPanelColumns() {
  return [
    {
      id: 'fundName',
      type: 'text',
      formatter: textFormatter,
      toolTip : 'Legal Entity / Legal Entity Family',
      name: 'Entity',
      field: 'fundName',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'cusip',
      name: 'Cusip',
      field: 'cusip',
      toolTip: 'CUSIP of the bond',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'spn',
      name: 'SPN',
      field: 'spn',
      toolTip: 'SPN of the bond',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'name',
      name: 'Display Name',
      field: 'name',
      toolTip: 'Bond Display Name',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'settleCurrencyIsoCode',
      name: 'Settle Currency Code',
      field: 'settleCurrencyIsoCode',
      toolTip: 'Settle Currency Code',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'quantity',
      name: 'Quantity',
      field: 'quantity',
      toolTip: 'Quantity',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'custodianAccount',
      name: 'Custodian Account',
      field: 'custodianAccount',
      toolTip: 'custodianAccount',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'notionalGrossMoney',
      name: 'Notional Gross Money',
      field: 'notionalGrossMoney',
      toolTip: 'Notional Gross Money',
      type: 'number',
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: 'Sum',
      groupTotalsFormatter: groupFormatter,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'maturityDate',
      name: 'Maturity Date',
      field: 'maturityDate',
      toolTip: 'Bond Maturity Date',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    },
    {
      id: 'settleDate',
      name: 'Settle Date',
      field: 'settleDate',
      toolTip: 'Trade Settle Date',
      type: 'text',
      filter: true,
      headerCssClass: 'b'
    }

  ];
}
