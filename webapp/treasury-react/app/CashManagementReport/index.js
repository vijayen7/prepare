import React, { Component } from "react";
import Workflow from "../commons/components/Workflow";
import ExceptionDialog from "../commons/container/ExceptionDialog";
import Loader from "../commons/container/Loader";
import Grid from "./containers/Grid";
import SideBar from "./containers/SideBar";

export default class CashManagementReport extends Component {
  render() {
    return (
      <React.Fragment>
        <Loader />
        <ExceptionDialog />
        <Workflow url="/treasury/lcm/cashManagementWorkflow.html" showResume={false} />
        <div className="layout--flex--row">
          <arc-header

            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div slot="application-menu" className="application-menu-toggle-view">
              <a
                href="/treasury/lcm/cashManagementThresholds.html"
                target="_blank"
              >
                Cash Management Thresholds
              </a>
            </div>
          </arc-header>
          <div className="layout--flex--row">
            <div className="layout--flex padding--top">
              <div className="size--content padding--right">
                <SideBar />
              </div>
              <Grid />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
