import { BASE_URL } from '../commons/constants';
const queryString = require('query-string');
export function getCashManagementData(payload) {
  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}service/cashManagementLiquidityService/getLiquidityProfile?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getMMFTradesForDate(payload) {
  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}service/cashManagementLiquidityService/getMMFTradesBookedForDate?${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}
export function getMMFTradesForTPlusOneDate(payload) {
  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}service/cashManagementLiquidityService/getMMFTradesBookedForTplusOneDate?${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}
