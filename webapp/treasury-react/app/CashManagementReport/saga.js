import { call, put, takeEvery } from 'redux-saga/effects';
import {
  END_LOADING,
  FETCH_CASH_MANAGEMENT_DATA,
  FETCH_MMF_TRADES_FOR_T_PLUS_ONE_DATE,
  HANDLE_EXCEPTION,
  START_LOADING,
  FETCH_MMF_TRADES_FOR_DATE
} from '../commons/constants';
import { getCashManagementData, getMMFTradesForDate, getMMFTradesForTPlusOneDate } from './api';
import { checkForException } from '../commons/util';

function* fetchCashManagementData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCashManagementData, action.payload);
    checkForException(data);
    yield put({ type: `${FETCH_CASH_MANAGEMENT_DATA}_SUCCESS`, data, payload: action.payload });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CASH_MANAGEMENT_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchMMFTradesForDate(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getMMFTradesForDate, action.payload);
    checkForException(data);
    yield put({ type: `${FETCH_MMF_TRADES_FOR_DATE}_SUCCESS`, data, payload: action.payload });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_MMF_TRADES_FOR_DATE}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchMMFTradesForTPlusOneDate(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getMMFTradesForTPlusOneDate, action.payload);
    checkForException(data);
    yield put({ type: `${FETCH_MMF_TRADES_FOR_T_PLUS_ONE_DATE}_SUCCESS`, data, payload: action.payload });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_MMF_TRADES_FOR_T_PLUS_ONE_DATE}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export default function* cashManagementReportRootSaga() {
  yield [
    takeEvery(FETCH_CASH_MANAGEMENT_DATA, fetchCashManagementData),
    takeEvery(FETCH_MMF_TRADES_FOR_DATE, fetchMMFTradesForDate),
    takeEvery(FETCH_MMF_TRADES_FOR_T_PLUS_ONE_DATE, fetchMMFTradesForTPlusOneDate)
  ];
}
