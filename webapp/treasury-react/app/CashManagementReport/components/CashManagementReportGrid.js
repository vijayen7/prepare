import { ReactArcGrid } from 'arc-grid';
import { Layout, TabPanel, Button } from 'arc-react-components';
import PropTypes from 'prop-types';
import React from 'react';
import Message from '../../commons/components/Message';
import FrimLiquidityDetails from '../containers/FirmLiquidityDetails';
import { getSummaryPanelColumns, getTopPanelColumns, getMMFTradeDataPanelColumns, getGovtBondsDetailPanelColumns, getGovtBondsTradeDetailPanelColumns } from '../grid/columnConfig';
import { getSummaryResultOptions, getResultOptions, getTradeResultOptions, getGovtBondResultOptions, getGovtBondTradesResultOptions } from '../grid/gridOptions';
export class CashManagementReportGrid extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Layout>
          <Layout.Child size="2" className="text-align--center" childId="child1">
            <FrimLiquidityDetails />
          </Layout.Child>
          <Layout.Child size="11" childId="child2">
            <ReactArcGrid
              data={this.props.cashManagementData.fundLevelLiquidityProfiles}
              gridId="cashManagementFundData"
              onCellClick={this.props.onCellClickHandler}
              columns={getTopPanelColumns(this.props.cashManagementData.columns)}
              options={this.props.getUpdateOptions()}
            />
          </Layout.Child>
          <Layout.Child size="5" childId="child3">
            <TabPanel selectedTabId={this.props.selectedTab} onSelect={this.props.changeTab} collapsible="true">
              <TabPanel.Tab label="Summary" tabId="summary">
                {Object.prototype.hasOwnProperty.call(this.props.cashManagementData, 'mmfSummaryList') &&
                  this.props.cashManagementData.mmfSummaryList.length > 0 &&
                  this.props.selectedTab === 'summary' && (
                  <ReactArcGrid
                    data={this.props.cashManagementData.mmfSummaryList}
                    gridId="cashManagementSummaryData"
                    columns={getSummaryPanelColumns(this.props.cashManagementData.ownershipThresholds)}
                    options={getSummaryResultOptions()}
                    containerHeight="55%"
                  />
                )}
              </TabPanel.Tab>
              <TabPanel.Tab label="Govt Bonds Maturities" tabId="govtBondsMaturities">
                {Object.prototype.hasOwnProperty.call(this.props.cashManagementData, 'allBonds') &&
                  this.props.cashManagementData.allBonds.length > 0 &&
                  this.props.selectedTab === 'govtBondsMaturities' && (
                  <ReactArcGrid
                    data={this.props.cashManagementData.allBonds}
                    gridId="cashManagementGovtBondData"
                    columns={getGovtBondsDetailPanelColumns()}
                    options={getGovtBondResultOptions()}
                    containerHeight="55%"
                  />
                )}
              </TabPanel.Tab>
              <TabPanel.Tab label="Government Bond Purchases" tabId="governmentBondPurchases">
                {Object.prototype.hasOwnProperty.call(this.props.cashManagementData, 'governmentBondPurchases') &&
                  this.props.cashManagementData.governmentBondPurchases.length > 0 &&
                  this.props.selectedTab === 'governmentBondPurchases' && (
                  <ReactArcGrid
                    data={this.props.cashManagementData.governmentBondPurchases}
                    gridId="cashManagementGovtBondPurchasesData"
                    columns={getGovtBondsTradeDetailPanelColumns()}
                    options={getGovtBondTradesResultOptions()}
                    containerHeight="55%"
                  />
                )}
              </TabPanel.Tab>
              <TabPanel.Tab label="Trades" tabId="trades">
              <React.Fragment>
              <div className="margin" align="right">
                <Button onClick={this.props.refreshTradeData}>Refresh</Button>
                </div>
                {this.props.mmfTradeData.length > 0 &&
                  this.props.selectedTab === 'trades' && (
                  <ReactArcGrid
                    data={this.props.mmfTradeData}
                    gridId="cashManagementMMFTradeData"
                    columns={getMMFTradeDataPanelColumns()}
                    options={getTradeResultOptions()}
                    containerHeight="55%"
                  />
                )}
                {this.props.selectedTab === 'trades' && this.props.mmfTradeData.length <= 0 && (
                  <Message messageData="No trade data available." />
                )}
                </React.Fragment>
              </TabPanel.Tab>
              <TabPanel.Tab label='T+1 Trades' tabId="tPlusOneTrades">
              <React.Fragment>
              <div className="margin" align="right">
                <Button onClick={this.props.refreshTPlusOneTradeData}>Refresh</Button>
                </div>
                {this.props.tPlusOneMmfTradeData.length > 0 &&
                  this.props.selectedTab === 'tPlusOneTrades' && (
                  <ReactArcGrid
                    data={this.props.tPlusOneMmfTradeData}
                    gridId="cashManagementMMFTPlusOneTradeData"
                    columns={getMMFTradeDataPanelColumns()}
                    options={getTradeResultOptions()}
                    containerHeight="55%"
                  />
                )}
                {this.props.selectedTab === 'tPlusOneTrades' && this.props.tPlusOneMmfTradeData.length <= 0 && (
                  <Message messageData="No T+1 trade data available." />
                )}
                </React.Fragment>
              </TabPanel.Tab>
              <TabPanel.Tab label="Underlying Data" tabId="underlyingData">
                {this.props.selectedTab === 'underlyingData' && this.props.underlyingData.length > 0 && (
                  <ReactArcGrid
                    data={this.props.underlyingData}
                    gridId="cashManagementUnderlyingData"
                    columns={this.props.underlyingDataColumns}
                    options={this.props.underlyingDataOptions}
                    onCellClick={this.props.onCellClickHandlerForUnderlying}
                    containerHeight="55%"
                  />
                )}

                {this.props.selectedTab === 'underlyingData' && this.props.underlyingData.length <= 0 && (
                  <Message messageData="No underlying data available." />
                )}
              </TabPanel.Tab>
            </TabPanel>
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );
  }
}

CashManagementReportGrid.propTypes = {
  cashManagementData: PropTypes.object,
  changeTab: PropTypes.any,
  getUpdateOptions: PropTypes.func.isRequired,
  onCellClickHandler: PropTypes.func.isRequired,
  onCellClickHandlerForUnderlying: PropTypes.func.isRequired,
  selectedTab: PropTypes.any,
  underlyingData: PropTypes.any,
  underlyingDataColumns: PropTypes.any,
  underlyingDataOptions: PropTypes.any
};
