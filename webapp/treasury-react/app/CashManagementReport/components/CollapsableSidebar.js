import PropTypes from "prop-types";
import React from "react";
import ColumnLayout from "../../commons/components/ColumnLayout";
import FilterButton from "../../commons/components/FilterButton";
import SaveSettingsManager from "../../commons/components/SaveSettingsManager";
import Sidebar from "../../commons/components/Sidebar";
import DateFilter from "../../commons/container/DateFilter";
import LegalEntityFamilyFilter from "../../commons/container/LegalEntityFamilyFilter";
export class CollapsableSidebar extends React.Component {
  render() {
    return (
      <Sidebar collapsible size="400px" resizeCanvas={this.props.resizeCanvas} toggleSidebar={this.props.toggleSidebar}>
        <SaveSettingsManager
          selectedFilters={this.props.getCurrentFilterState()}
          applySavedFilters={this.props.applySavedFilters}
          applicationName="cashManagementFilter"
        />

        <DateFilter
          onSelect={this.props.onSelect}
          stateKey="selectedDate"
          dateType="tFilterDate"
          data={this.props.selectedDate}
          label="Date"
        />

        <LegalEntityFamilyFilter onSelect={this.props.onSelect} selectedData={this.props.selectedLegalEntityFamilies} />
        <ColumnLayout>
          <FilterButton onClick={this.props.handleClick} reset={false} label="Search" />
          <FilterButton reset onClick={this.props.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

CollapsableSidebar.propTypes = {
  applySavedFilters: PropTypes.func.isRequired,
  getCurrentFilterState: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  handleReset: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  resizeCanvas: PropTypes.func.isRequired,
  selectedDate: PropTypes.any,
  selectedLegalEntityFamilies: PropTypes.array.isRequired,
  toggleSidebar: PropTypes.bool.isRequired
};
