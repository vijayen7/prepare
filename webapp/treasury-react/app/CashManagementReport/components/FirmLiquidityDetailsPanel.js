import { Layout, Panel } from "arc-react-components";
import PropTypes from "prop-types";
import React from "react";
export class FirmLiquidityDetailsPanel extends React.Component {
  render() {
    let clientName = CODEX_PROPERTIES['codex.client_name'];
    let rgyxxName = "RGYXX";
    if(clientName === "demo"){
      rgyxxName = "IJTXX";
    }
    return (
      <Panel title="Firm Liquidity Details" className="fill--width gutter">
        <Layout isColumnType>
          <Layout.Child size={2} childId="child1">
            <table className="table treasury-table--data-summary">
              <tbody>
                <tr>
                  <td>Total Cash USD</td>
                  <td>{this.props.plainNumberFormatter(null, null, this.props.totalCashBalance)}</td>
                </tr>
                <tr>
                  <td>{rgyxxName} ownership percentage: </td>
                  <td>{`${this.props.plainDecimalsPercentFormatter(this.props.rgyxxOwnershipPercntage, 2)}%`}</td>
                </tr>
              </tbody>
            </table>
          </Layout.Child>
        </Layout>
      </Panel>
    );
  }
}
FirmLiquidityDetailsPanel.propTypes = {
  plainNumberFormatter: PropTypes.func.isRequired,
  plainDecimalsPercentFormatter: PropTypes.func.isRequired,
  totalCashBalance: PropTypes.number,
  rgyxxOwnershipPercntage: PropTypes.any
};
