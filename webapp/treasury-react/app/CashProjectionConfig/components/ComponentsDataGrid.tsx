import React from "react";
import { observer } from "mobx-react";
import Message from "../../commons/components/Message";
import ArcDataGrid from "arc-data-grid";
import _ from "lodash";
import { CashProjectionConfigProps } from "../models/CashProjectionConfigProps";
import { Layout } from "arc-react-components";

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

const ComponentsDataGrid: React.FC<CashProjectionConfigProps> = (
  props: CashProjectionConfigProps
) => {
  const renderGridData = () => {
    let data = props.store.cashProjectionConfigDataList;
    let flattenedCashProjectionConfigData = props.store.dataStore.getComponentDataList(
      data
    );
    if (!flattenedCashProjectionConfigData.length) {
      let noDataMessage = "There is no Cash Projection Configuration Data";
      return <Message messageData={noDataMessage} />;
    }
    props.store.gridStore.populateGridColumnData(
      flattenedCashProjectionConfigData
    );

    let booksData = props.store.whitelistedBooksList;
    props.store.gridStore.populateWhitelistedBooksColumnData(booksData);

    let showWhitelistedBooks = CODEX_PROPERTIES["com.arcesium.treasury.cashprojection.showWhitelistedBooks"];

    let whitelistedBooksGrid;
    if (showWhitelistedBooks != undefined && showWhitelistedBooks == "true") {
      whitelistedBooksGrid = (<>
        <Layout.Divider isResizable childId="componentBooksDivider" />
        <Layout.Child
          childId="whitelistedBooksGridChild"
          size={1}
          title="Whitelisted Books"
          showHeader
        >
          <ArcDataGrid
            rows={props.store.gridStore.gridWhitelistedBooksData}
            columns={props.store.gridStore.whitelistedBooksColumns}
          />
        </Layout.Child>
      </>
      )
    }

    let cashProjectionPivotGrid = (
      <Layout
        isColumnType={false}
        className="border"
        style={{ height: "100%" }}
      >
        <Layout.Child
          childId="topGridView"
          collapsible
          size={1}
          left="0"
          style={{
            height: "100%",
            alignSelf: "flex-start",
          }}
        >
          <Layout isColumnType={true} className="border">
            <Layout.Child
              childId="sourceGridView"
              size={1}
              title="Component Details"
              showHeader
            >
              <ArcDataGrid
                rows={props.store.gridStore.gridComponentData}
                columns={props.store.gridStore.gridComponentDataColumns}
              />
            </Layout.Child>
            {whitelistedBooksGrid}
          </Layout>
        </Layout.Child>
        <Layout.Divider isResizable childId="topBottomDivider" />
        <Layout.Child
          childId="configGridView"
          collapsible
          size={1}
          title="SubComponent Config Details"
          showHeader
          style={{
            height: "100%",
            width: "80%",
            alignSelf: "flex-start",
          }}
        >
          <ArcDataGrid
            rows={props.store.gridStore.gridSubComponentData}
            columns={props.store.gridStore.gridSubComponentDataColumns}
            configurations={{ disableResizing: true }}
          />
        </Layout.Child>
      </Layout>
    );

    return cashProjectionPivotGrid;
  };

  return <>{renderGridData()}</>;
};

export default observer(ComponentsDataGrid);
