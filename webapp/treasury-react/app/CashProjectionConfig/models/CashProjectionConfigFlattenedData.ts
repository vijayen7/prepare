import { SubComponentConfigRow } from "./SubComponentConfigRow";
export interface CashProjectionConfigFlattenedData {
  dataId?: number;
  dataSource?: string;
  name: string;
  type: boolean;
  balancesType: string;
  subComponentList?: Array<SubComponentConfigRow>;
}
