export interface WhitelistedBooksRow {
  bookId?: number;
  bookName?: string;
}
