export interface ComponentInfoRow {
  dataId?: number;
  name?: string;
  type?: string;
  dataSource?: string;
  balancesType?: string;
}
