export interface SubComponentConfigRow {
  dataId?: number;
  mainComponent?: string;
  subComponent?: string;
  numberOfDaysOfProjection?: number;
  isInformational?: boolean;
}
