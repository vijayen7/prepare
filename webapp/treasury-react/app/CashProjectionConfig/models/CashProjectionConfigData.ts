import ReferenceData from "../../commons/models/ReferenceData";
import { CashProjectionComponent } from "../../CashProjection/models/CashProjectionData";

// Interface for Cash Projection Data
export interface ComponentConfig {
  "@CLASS": "com.arcesium.treasury.cashprojection.config.model.ComponentConfig";
  component: CashProjectionComponent;
  config?: ComponentConfigData;
  dataSource?: ReferenceData;
  subComponentsConfig?: Array<ComponentConfig>;
}

export interface ComponentConfigData {
  "@CLASS": "com.arcesium.treasury.cashprojection.config.model.ComponentConfigData";
  numberOfDays?: number;
  isLatest?: Boolean;
}

export interface WhitelistedBooks {
  "@CLASS": "com.arcesium.treasury.cashprojection.config.model.WhitelistedBooks";
  book: ReferenceData;
}
