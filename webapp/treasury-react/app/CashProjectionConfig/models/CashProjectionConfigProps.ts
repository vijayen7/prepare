import CashProjectionConfigStore from "../CashProjectionConfigStore";

export interface CashProjectionConfigProps {
  store: CashProjectionConfigStore;
}
