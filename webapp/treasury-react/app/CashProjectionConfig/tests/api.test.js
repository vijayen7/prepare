import { getCashProjectionConfigurationList } from "../api/CashProjectionConfigDataListApi";
import { BASE_URL } from "../../commons/constants";
import { ArcFetch } from "arc-commons";
import SERVICE_URLS from "../../commons/UrlConfigs";

jest.mock("arc-commons");

describe("Get Cash Projection COnfig Data List API", () => {
  test("Testing function getCashProjectionConfigurationList", () => {
    const getJSONSpy = jest.spyOn(ArcFetch, "getJSON");
    getCashProjectionConfigurationList({});
    expect(getJSONSpy).toHaveBeenCalled();
    expect(getJSONSpy.mock.calls[0][0]).toEqual(
      BASE_URL +
        "service/" +
        SERVICE_URLS.cashProjectionConfigService
          .getCashProjectionConfigurationList +
        `?format=json`
    );
  });
});
