import CashProjectionConfigStore from "./CashProjectionConfigStore";
import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../commons/components/ReactLoader";
import Loader from "../commons/container/Loader";
import AppHeader, { whenHeaderRef } from "arc-header";
import ComponentsDataGrid from "./components/ComponentsDataGrid";
import { Layout, Breadcrumbs } from "arc-react-components";
import { extendObservable } from "mobx";
import { useLocalStore } from "mobx-react-lite";

const dummyObservable = Symbol("dummy-observable");

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item
        link="/treasury/cashProjectionConfig.html"
        key="cashProjectionConfig"
      >
        Cash Projection Config
      </Breadcrumbs.Item>
    </>
  );
};

const CashProjectionConfig: React.FC = () => {
  const cashProjectionConfigStore = useLocalStore(() =>
    extendObservable(new CashProjectionConfigStore(), {
      [dummyObservable]: true,
    })
  );
  useEffect(() => {
    updateBreadCrumbs();
    cashProjectionConfigStore.fetchCashProjectionConfigData();
  }, []);

  return (
    <>
      <Loader />
      <ReactLoader />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} />
        <Layout
          isColumnType={false}
          className="border"
          style={{ height: "100%" }}
        >
          <Layout.Child key="child3">
            <ComponentsDataGrid store={cashProjectionConfigStore} />
          </Layout.Child>
        </Layout>
      </div>
    </>
  );
};

export default observer(CashProjectionConfig);
