import { observable, action } from "mobx";
import GridStore from "./GridStore";
import {
  getSubComponentCommonColumns,
  getComponentCommonColumns,
} from "../components/CashProjectionConfigGridColumns";
import {
  ComponentConfig,
} from "../models/CashProjectionConfigData";
import { SubComponentConfigRow } from "../models/SubComponentConfigRow";

export class DataStore {
  @observable gridStore: GridStore;

  constructor(gridStore: GridStore) {
    this.gridStore = gridStore;
  }

  /**
   * getComponentDataList method flattens the cash projection config API data into pivot data
   * @param cashProjectionReportDataList contains Cash Projection Config API data
   */
  @action getComponentDataList(
    CashProjectionConfigDataList: Array<ComponentConfig>
  ) {
    let configDataList: Array<any> = [];
    if (CashProjectionConfigDataList.length === 0) {
      return configDataList;
    }

    let dataId = 0;
    this.updateColumnsToShowInGrid();
    if (CashProjectionConfigDataList.length) {
      configDataList = CashProjectionConfigDataList.flatMap((configData) => {
        let dataSourceName = configData.dataSource?.name
          ? configData.dataSource?.name
          : "n/a";
        let componentName = configData.component.componentName;
        let isMainComponent = configData.subComponentsConfig?.length
          ? true
          : false;
        let balancesType: string = configData.component.componentType;
        return this.getComponentDetailedData(
          dataId,
          dataSourceName,
          configData,
          componentName,
          isMainComponent,
          balancesType
        );
      });
    }
    return configDataList;
  }

  getComponentDetailedData(
    dataId: number,
    dataSource: string,
    componentConfig: ComponentConfig,
    componentName?: string,
    isMainComponent?: boolean,
    balancesType?: string
  ) {
    dataId += 1;
    let subComponentList: Array<SubComponentConfigRow> = [];

    let commonData = {
      mainComponent: componentName ? componentName : "n/a",
    };

    if (
      componentConfig.subComponentsConfig?.length &&
      componentConfig.subComponentsConfig?.length > 0
    ) {
      subComponentList = this.populateSubComponentData(
        commonData,
        componentConfig.subComponentsConfig
      );
    }

    let flattenedData = {
      dataId: dataId,
      dataSource: dataSource,
      name: componentName ? componentName : "n/a",
      type: isMainComponent,
      balancesType: balancesType,
      subComponentList,
    };

    return flattenedData;
  }

  //updates column name of grids and add Data Source row in component grid
  updateColumnsToShowInGrid() {
    this.gridStore.gridComponentDataColumns = [];
    this.gridStore.gridComponentDataColumns.push(
      ...getComponentCommonColumns()
    );
    this.gridStore.gridSubComponentDataColumns = [];
    this.gridStore.gridSubComponentDataColumns.push(
      ...getSubComponentCommonColumns()
    );
  }

  populateSubComponentData(
    commonData: any,
    subComponentDetailsList?: Array<ComponentConfig>
  ) {
    let subComponentList: Array<SubComponentConfigRow> = [];
    let subComponentId = 0;
    if (
      subComponentDetailsList?.length &&
      subComponentDetailsList?.length > 0
    ) {
      for (let subComponentDetail of subComponentDetailsList) {
        let subComponent = subComponentDetail;
        subComponentId += 1;
        let subComponentData = {
          dataId: subComponentId,
          subComponent: subComponent.component.componentName
            ? subComponent.component.componentName
            : "n/a",
          numberOfDaysOfProjection: subComponent.config?.numberOfDays
            ? subComponent.config?.numberOfDays
            : "n/a",
          isInformational: subComponent.component.isInformational,
          ...commonData,
        };
        subComponentList.push(subComponentData);
      }
    }
    return subComponentList;
  }
}

export default DataStore;
