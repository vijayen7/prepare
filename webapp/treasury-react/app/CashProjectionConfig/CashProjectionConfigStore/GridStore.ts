import { observable, action } from "mobx";
import { ComponentInfoRow } from "../models/ComponentInfoRow";
import { SubComponentConfigRow } from "../models/SubComponentConfigRow";
import { WhitelistedBooksRow } from "../models/WhitelistedBooksRow";
import { CashProjectionConfigFlattenedData } from "../models/CashProjectionConfigFlattenedData";
import { MAIN_COMPONENT } from "../constants";
import { WhitelistedBooks } from "../models/CashProjectionConfigData";

export class GridStore {
  // Observable Variables
  @observable gridComponentData: Array<ComponentInfoRow> = [];
  @observable gridComponentDataColumns: Array<string> = [];
  @observable gridSubComponentData: Array<SubComponentConfigRow> = [];
  @observable gridSubComponentDataColumns: Array<string> = [];
  @observable gridWhitelistedBooksData: Array<WhitelistedBooksRow> = [];
  @observable whitelistedBooksColumns: Array<string> = [];

  // Setters Actions

  @action setGridComponentDataColumns(value: any) {
    this.gridComponentDataColumns = value;
  }

  @action setWhitelistedBooksColumns(value: any) {
    this.whitelistedBooksColumns = value;
  }

  @action setGridSubComponentDataColumns(value: any) {
    this.gridSubComponentDataColumns = value;
  }

  // Getters Actions

  @action getGridComponentDataColumns() {
    return this.gridComponentDataColumns;
  }

  @action getWhitelistedBooksColumns() {
    return this.whitelistedBooksColumns;
  }

  @action getGridSubComponentDataColumns() {
    return this.gridSubComponentDataColumns;
  }

  @action getGridComponentData() {
    return this.gridComponentData.toString;
  }

  @action getGridSubComponentData() {
    return this.gridSubComponentData.toString;
  }

  // Method Actions
  @action populateGridColumnData(
    dataList: Array<CashProjectionConfigFlattenedData>
  ) {
    let subComponentDetailList: Array<SubComponentConfigRow> = [];
    let componentDetailList: Array<ComponentInfoRow> = [];

    for (let data of dataList) {
      if (data.type) {
        let componentDetail = {
          dataId: data.dataId,
          name: data.name,
          type: MAIN_COMPONENT,
          dataSource: data.dataSource,
          balancesType: data.balancesType,
        };
        componentDetailList.push(componentDetail);
      }

      data.subComponentList?.forEach((subComponent) => {
        let subComponentDetail = {
          dataId: subComponent.dataId,
          mainComponent: subComponent.mainComponent,
          subComponent: subComponent.subComponent,
          numberOfDaysOfProjection: subComponent.numberOfDaysOfProjection,
          isInformational: subComponent.isInformational
        };
        subComponentDetailList.push(subComponentDetail);
      });
    }
    this.gridSubComponentData = subComponentDetailList;
    this.gridComponentData.push(...componentDetailList);
  }

  @action populateWhitelistedBooksColumnData(
    whitelistedBooksList: Array<WhitelistedBooks>
  ) {
    let whitelistedBookRows: Array<WhitelistedBooksRow> = [];
    this.whitelistedBooksColumns = ["bookName"];
    if (whitelistedBooksList.length) {
      whitelistedBookRows = whitelistedBooksList.flatMap((bookData) => {
        let bookRow: WhitelistedBooksRow = {
          bookId: bookData.book.id,
          bookName: bookData.book.abbrev,
        };
        return bookRow;
      });
    }
    this.gridWhitelistedBooksData = whitelistedBookRows.sort(
      (a: WhitelistedBooksRow, b: WhitelistedBooksRow) =>
        a.bookName != null && b.bookName != null && a.bookName > b.bookName
          ? 1
          : -1
    );
  }
}

export default GridStore;
