import { observable } from "mobx";
import { getCashProjectionConfigurationList } from "../api/CashProjectionConfigDataListApi";
import { getCashProjectionWhitelistedBooks } from "../api/CashProjectionWhitelistedBooksApi";
import {
  ComponentConfig,
  WhitelistedBooks,
} from "../models/CashProjectionConfigData";
import GridStore from "./GridStore";
import DataStore from "./DataStore";

export class CashProjectionConfigStore {
  // Store initializations
  gridStore = new GridStore();
  dataStore = new DataStore(this.gridStore);

  // Observable Variables
  @observable cashProjectionConfigDataList: Array<
    ComponentConfig
  > = [];
  @observable whitelistedBooksList: Array<WhitelistedBooks> = [];

  // Method to fetch data from API
  fetchCashProjectionConfigData = async () => {
    try {
      this.whitelistedBooksList = await getCashProjectionWhitelistedBooks();
      this.cashProjectionConfigDataList = await getCashProjectionConfigurationList();
    } catch (e) {
      alert(
        "Error occurred while getting Cash Projection Config Data From API"
      );
    }
  };
}

export default CashProjectionConfigStore;
