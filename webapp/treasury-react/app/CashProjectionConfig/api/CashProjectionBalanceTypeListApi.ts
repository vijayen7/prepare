import { BASE_URL } from "../../commons/constants";
import { fetchPostURL } from "../../commons/util";
// @ts-ignore
import SERVICE_URLS from "../../commons/UrlConfigs";

export const getCashProjectionBalanceTypeList = () => {
  let inputs: Array<String> = ["inputFormat=PROPERTIES", "format=json"];

  let url = `${BASE_URL}service/${SERVICE_URLS.cashProjectionConfigService.getBalanceTypeList}`;
  let encodedParam = inputs.join("&");

  return fetchPostURL(url, encodedParam);
};
