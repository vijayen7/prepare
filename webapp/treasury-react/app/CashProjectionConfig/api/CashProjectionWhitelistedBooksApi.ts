import { BASE_URL } from "../../commons/constants";
import { fetchPostURL } from "../../commons/util";
// @ts-ignore
import SERVICE_URLS from "../../commons/UrlConfigs";

export const getCashProjectionWhitelistedBooks = () => {
  let inputs: Array<String> = ["inputFormat=PROPERTIES", "format=json"];

  let url = `${BASE_URL}service/${SERVICE_URLS.cashProjectionConfigService.getWhitelistedBooksList}`;
  let encodedParam = inputs.join("&");

  return fetchPostURL(url, encodedParam);
};
