import { combineReducers } from "redux";
import { convertJavaDateDashed } from "./../commons/util";
import _ from "lodash";
import {
  CLOSE_ADJUSTMENT_DIALOG,
  CLOSE_WIRE_DIALOG,
  DISPLAY_ADJUSTMENT_DIALOG,
  DISPLAY_WIRE_DIALOG,
  FETCH_CASH_MANAGEMENT_WORKFLOW_DATA,
  LOGWIRE_BOOKTRADE_SELECTION_DIALOG,
  LOG_WIRE_AND_BOOK_TRADE,
  UPDATE_ADJUSTMENT_AMOUNT,
  UPDATE_WIRE_TRADE_DETAIL,
  SEG_IA,
  GENERIC_MMF_BOOKING_DIALOG,
  OVER_RIDE_INVALID_AMOUNT_CHECK,
  GOVT_BOND,
  FETCH_MMF_CUTOFF_TIME_DATA,
  SHOW_ADD_ADJUSTMENT_ALERT,
  ADD_ADJUSTMENT,
  SHOW_ADD_ADJUSTMENT_DIALOG,
  ADD_ADJUSTMENT_SUCCESS_DIALOG
} from "../commons/constants";
import { reduceActionValue } from "../CashManagementReport/reducer";

function cashManagementWorkflowDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_CASH_MANAGEMENT_WORKFLOW_DATA}_SUCCESS`:
      return reduceData(action) || {};
    case UPDATE_WIRE_TRADE_DETAIL: {
      const data = state;
      const date = convertJavaDateDashed(action.payload.resp.date);
      const isHoldingsPresent =
        data.fundLevelLiquidityProfiles !== undefined &&
        data.fundLevelLiquidityProfiles.length > 0 &&
        data.fundLevelLiquidityProfiles[0].holdings !== undefined;
      if (isHoldingsPresent) {
        data.fundLevelLiquidityProfiles[0].holdings.forEach((holding) => {
          if (checkIfMMFAlreadyPresent(holding, action.payload)) {
            holding.amount = undefined;
          }
        });
      }
      if (
        isHoldingsPresent &&
        action.payload.resp.wireWrapper.valueDateStr ===
        action.payload.workFlowDate &&
        date === action.payload.workFlowDate
      ) {
        let alreadyPresentMMF = false;
        data.mmfUpdate = false;
        data.fundLevelLiquidityProfiles[0].holdings.forEach((holding) => {
          alreadyPresentMMF = checkIfMMFAlreadyPresent(holding, action.payload);

          if (alreadyPresentMMF) {
            data.mmfUpdate = true;
            if (action.payload.wireId !== undefined) {
              holding.wireIds = holding.wireIds.concat(action.payload.wireId);
              holding.wires = holding.wireIds.join(",");
            }
            if (action.payload.tradeId !== undefined) {
              holding.tradeIds = holding.tradeIds.concat(
                action.payload.tradeId
              );
              holding.trades = holding.tradeIds.join(",");
            }
            if (
              action.payload.amount !== undefined &&
              !Number.isNaN(action.payload.amount)
            ) {
              holding.balanceUsd += action.payload.amount;
              holding.investmentAmount += action.payload.amount;
              holding.amount = undefined;
            }
          }
        });
      }

      // to refresh grids for wire and trade ids
      data.update = !data.update;
      return data;
    }
    default:
      return state;
  }
}

function checkIfMMFAlreadyPresent(holding, payload) {
  let alreadyPresentMMF = false;
  if (payload.ticker && payload.source) {
    alreadyPresentMMF =
      holding.ticker === payload.ticker &&
      holding.source === payload.source;
  }
  if (payload.spn && payload.custodianAccountId) {
    alreadyPresentMMF =
      holding.spn === payload.spn &&
      holding.custodianAccount.custodianAccountId ===
      payload.custodianAccountId;
  }

  return alreadyPresentMMF;
}

function adjustmentDialogReducer(state = false, action) {
  switch (action.type) {
    case `${DISPLAY_ADJUSTMENT_DIALOG}`:
      return true;
    case `${CLOSE_ADJUSTMENT_DIALOG}`:
      return false;
    default:
      return state;
  }
}
function logWireBookTradeSelectionDialogReducer(
  state = { display: false },
  action
) {
  switch (action.type) {
    case `${LOGWIRE_BOOKTRADE_SELECTION_DIALOG}_SHOW`:
      return {
        message: action.payload.message,
        display: true
      };
    case `${LOGWIRE_BOOKTRADE_SELECTION_DIALOG}_HIDE`:
      return {
        display: false
      };
    default:
      return state;
  }
}
function genericLogWireBookTradeSelectionDialogReducer(
  state = { display: false },
  action
) {
  switch (action.type) {
    case `${LOGWIRE_BOOKTRADE_SELECTION_DIALOG}_GENERIC_SHOW`:
      return {
        message: action.payload.message,
        display: true
      };
    case `${LOGWIRE_BOOKTRADE_SELECTION_DIALOG}_GENERIC_HIDE`:
      return {
        display: false
      };
    default:
      return state;
  }
}

function wireDialogReducer(state = false, action) {
  switch (action.type) {
    case `${DISPLAY_WIRE_DIALOG}`:
      return true;
    case `${CLOSE_WIRE_DIALOG}`:
      return false;
    default:
      return state;
  }
}

function updateAdjustmentAmountReducer(state = { amount: 0 }, action) {
  switch (action.type) {
    case `${UPDATE_ADJUSTMENT_AMOUNT}`:
      return (
        { amount: Number(state.amount) + Number(action.payload.amount) } || {}
      );
    default:
      return state;
  }
}

function logWireAndBookTradeReducer(state = {}, action) {
  switch (action.type) {
    case `${LOG_WIRE_AND_BOOK_TRADE}_SUCCESS`:
      return action.data || {};
    default:
      return state;
  }
}
function logWireAndBookTradeGenericReducer(state = {}, action) {
  switch (action.type) {
    case `${LOG_WIRE_AND_BOOK_TRADE}_GENERIC_SUCCESS`:
      return action.data || {};
    default:
      return state;
  }
}

function logWireBookTradeAlertReducer(state = false, action) {
  switch (action.type) {
    case `${LOG_WIRE_AND_BOOK_TRADE}_ALERT_SHOW`:
      return true;
    case `${LOG_WIRE_AND_BOOK_TRADE}_ALERT_HIDE`:
      return false;
    default:
      return state;
  }
}
function genericLogWireBookTradeAlertReducer(state = false, action) {
  switch (action.type) {
    case `${LOG_WIRE_AND_BOOK_TRADE}_GENERIC_ALERT_SHOW`:
      return true;
    case `${LOG_WIRE_AND_BOOK_TRADE}_GENERIC_ALERT_HIDE`:
      return false;
    default:
      return state;
  }
}
function overRideInvalidAmountCheck(state = false, action) {
  switch (action.type) {
    case OVER_RIDE_INVALID_AMOUNT_CHECK:
      return true;
    case `DONT_${OVER_RIDE_INVALID_AMOUNT_CHECK}`:
      return false;
    default:
      return state;
  }
}
function showGenericMMFBookingDialogReducer(state = false, action) {
  switch (action.type) {
    case `${GENERIC_MMF_BOOKING_DIALOG}_SHOW`:
      return true;
    case `${GENERIC_MMF_BOOKING_DIALOG}_HIDE`:
      return false;
    default:
      return state;
  }
}
const result = {
  mmfs: [],
  sourceAccounts: [],
  destinationAccounts: [],
  custodianAccounts: [],
  portfolios: [],
  isBuy: true
};
function genericMMFBookingDialogDataReducer(state = result, action) {
  switch (action.type) {
    case `${GENERIC_MMF_BOOKING_DIALOG}_SUCCESS`:
      return reduceMMFBookingDetails(action);
    default:
      return state;
  }
}
function reduceMMFBookingDetails(action) {
  const {
    portfolios,
    sourceWireInstructions,
    destinationWireInstructions,
    custodianAccounts
  } = action.data;
  const { isBuy } = action;
  let mmfs = [];
  _.forEach(action.data.mmfs, (value, key) => {
    mmfs = mmfs.concat({ key, value });
  });

  mmfs.sort((a, b) => {
    const valueA = a.value.toLowerCase();
    const valueB = b.value.toLowerCase();
    if (valueA < valueB) return -1;
    if (valueA > valueB) return 1;
    return 0;
  });

  const sourceAccounts = reduceWireInstructions(
    Object.values(sourceWireInstructions).flatMap(x => {
      return x;
    })
  );
  const destinationAccounts = reduceWireInstructions(
    Object.values(destinationWireInstructions).flatMap(x => {
      return x;
    })
  );
  const portfoliosKeyValueAdded = portfolios.map(data => {
    data.key = data.id;
    data.value = data.name;
    return data;
  });
  const cas = custodianAccounts.map(data => {
    data.key = data.id;
    data.value = data.name;
    return data;
  });
  return {
    mmfs,
    sourceAccounts,
    destinationAccounts,
    custodianAccounts: cas,
    portfolios: portfoliosKeyValueAdded,
    isBuy
  };
}
function getMMFCutoffTimeDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_MMF_CUTOFF_TIME_DATA}_SUCCESS`:
      return reduceMMFCutoffTimeData(action);
    default:
      return state;
  }
}
function reduceMMFCutoffTimeData(action) {
  const mmfCutoffTimeList = action.data;

  let reducedMMFCutoffTimeList = {};
  mmfCutoffTimeList.forEach(mmfCutoffTime => {
    reducedMMFCutoffTimeList[mmfCutoffTime.spn] = {
      businessDayMMFThresholdTime: mmfCutoffTime.businessDayMMFThresholdTime,
      holidayMMFThresholdTime: mmfCutoffTime.holidayMMFThresholdTime
    };
  });
  return reducedMMFCutoffTimeList;
}

function reduceData(action) {
  const { data } = action;
  const fundId = Number(
    action.payload["CashManagementFilter.legalEntityFamilyIds"]
  );
  let fundData = {};
  data.fundLevelLiquidityProfiles.forEach(fundLP => {
    if (fundLP.fundId === fundId) fundData = fundLP;
  });
  const aggregatedSegIAMmfHoldings = {};
  const fundOwnershipThresholds = fundData.ownershipThresholds;

  let fundWireInstructions = [];
  fundData.cashBalances.forEach(cashBalance => {
    fundWireInstructions = fundWireInstructions.concat(
      reduceWireInstructions(cashBalance.wireInstructions)
    );
  });
  fundData.portfolios.forEach(portfolio => {
    setKeyValueFromIdName(portfolio);
  });
  fundData.segIAMmfHoldings.forEach(segIAMmf => {
    if (
      aggregatedSegIAMmfHoldings[segIAMmf.mmf.ticker] !== undefined &&
      aggregatedSegIAMmfHoldings[segIAMmf.mmf.ticker] != null
    ) {
      segIAMmf.balanceUsd +=
        aggregatedSegIAMmfHoldings[segIAMmf.mmf.ticker].balanceUsd;
    }
    aggregatedSegIAMmfHoldings[segIAMmf.mmf.ticker] = segIAMmf;
  });

  Object.keys(aggregatedSegIAMmfHoldings).forEach(ticker => {
    fundData.mmfHoldings.push(aggregatedSegIAMmfHoldings[ticker]);
  });
  let i = 0;
  fundData.mmfHoldings.forEach(mmfHolding => {
    mmfHolding.id = i++;
    mmfHolding.spn = mmfHolding.mmf.spn;
    mmfHolding.ticker = mmfHolding.mmf.ticker;
    mmfHolding.mmfAumUsd =
      data.mmfSummaryMap[mmfHolding.mmf.ticker].mmf.mmfAumUsd;
    mmfHolding.yield = data.mmfSummaryMap[mmfHolding.mmf.ticker].mmf.yield;
    mmfHolding.expenseRatio =
      data.mmfSummaryMap[mmfHolding.mmf.ticker].mmf.expenseRatio;
    mmfHolding.ownershipAmount =
      data.mmfSummaryMap[mmfHolding.mmf.ticker].firmLevelOwnershipAmount;
    mmfHolding.firmOwnershipThresholds = data.ownershipThresholds;
    mmfHolding.fundOwnershipThresholds = fundOwnershipThresholds;
    mmfHolding.investmentAmount = 0;
    if (mmfHolding.source === "N/A") {
      mmfHolding.source = SEG_IA;
      mmfHolding.caName = SEG_IA;
    } else {
      mmfHolding.caName = _.has(mmfHolding.custodianAccount, "displayName")
        ? mmfHolding.custodianAccount.displayName
        : "UNKNOWN";
    }

    if (mmfHolding.wireInstructions !== undefined) {
      mmfHolding.wireInstructions.forEach(wireInstruction => {
        setKeyValueFromIdName(wireInstruction);
      });
    }
    let wires = "";
    if (mmfHolding.wireIds !== undefined) {
      wires = mmfHolding.wireIds.join(",");
    }
    mmfHolding.wires = wires;
    let trades = "";
    if (mmfHolding.tradeIds !== undefined) {
      trades = mmfHolding.tradeIds.join(",");
    }
    mmfHolding.trades = trades;
    mmfHolding.fundWireInstructions = fundWireInstructions;
  });
  fundData.govtBondHoldings = reduceGovtBondHoldings(
    fundData.bondHoldings,
    fundWireInstructions,
    fundData.ownershipThresholds,
    data.ownershipThresholds
  );
  fundData.holdings = fundData.mmfHoldings.concat(fundData.govtBondHoldings);
  reduceActionValue(fundData);
  data.fundLevelLiquidityProfiles = [fundData];
  data.workflowTime = new Date();
  return data;
}

function reduceGovtBondHoldings(bondHoldings, fundWireInstructions, fundOwnershipThresholds, firmOwnershipThresholds) {
  let bondHoldingsList = [];

  if (bondHoldings && bondHoldings.length > 0) {
    let i = -1;
    bondHoldings.forEach(caBondHolding => {
      let bondHolding = {};
      bondHolding.id = i--;
      bondHolding.ticker =
        caBondHolding.bond.displayName + " " + caBondHolding.bond.currencyCode;
      bondHolding.spn = caBondHolding.bond.spn;
      bondHolding.balanceUsd = caBondHolding.valueUsd;
      bondHolding.custodianAccount = caBondHolding.custodianAccount;
      bondHolding.source = GOVT_BOND;
      bondHolding.caName = _.has(caBondHolding.custodianAccount, "displayName")
        ? caBondHolding.custodianAccount.displayName
        : "UNKNOWN";
      bondHolding.wireInstructions = caBondHolding.wireInstructions;
      if (bondHolding.wireInstructions !== undefined) {
        bondHolding.wireInstructions.forEach(wireInstruction => {
          setKeyValueFromIdName(wireInstruction);
        });
      }
      let wires = "";
      let trades = "";
      if (caBondHolding.wireIds !== undefined) {
        wires = caBondHolding.wireIds.join(",");
      }
      if (caBondHolding.tradeIds !== undefined) {
        trades = caBondHolding.tradeIds.join(",");
      }
      bondHolding.wireIds = caBondHolding.wireIds;
      bondHolding.tradeIds = caBondHolding.tradeIds;
      bondHolding.trades = trades;
      bondHolding.wires = wires;
      bondHolding.fundWireInstructions = fundWireInstructions;
      bondHolding.firmOwnershipThresholds = firmOwnershipThresholds;
      bondHolding.fundOwnershipThresholds = fundOwnershipThresholds;
      bondHoldingsList.push(bondHolding);
    });
  }

  return bondHoldingsList;
}

export function reduceWireInstructions(wireInstructions) {
  const fundWireInstructions = [];
  wireInstructions
    .filter(item => {
      return item.id !== undefined && item.name !== undefined;
    })
    .forEach(wireInstruction => {
      setKeyValueFromIdName(wireInstruction);
      fundWireInstructions.push(wireInstruction);
    });
  return fundWireInstructions;
}

function showAddAdjustmentDialogReducer(state = false, action) {
  switch (action.type) {
    case `${SHOW_ADD_ADJUSTMENT_DIALOG}_OPEN`:
      return true;
    case `${SHOW_ADD_ADJUSTMENT_DIALOG}_CLOSE`:
      return false;
    default:
      return state;
  }
}

function showAddAdjustmentSuccessDialogReducer(state = { show: false, message: '' }, action) {
  switch (action.type) {
    case `${ADD_ADJUSTMENT_SUCCESS_DIALOG}_SHOW`:
      return { show: true, message: action.payload };
    case `${ADD_ADJUSTMENT_SUCCESS_DIALOG}_HIDE`:
      return { show: false, message: '' };
    default:
      return state;
  }
}

function showAddAdjustmentAlertReducer(state = false, action) {
  switch (action.type) {
    case `${SHOW_ADD_ADJUSTMENT_ALERT}_OPEN`:
      return true;
    case `${SHOW_ADD_ADJUSTMENT_ALERT}_CLOSE`:
      return false;
    default:
      return state;
  }
}

function addedAdjustmentsDataReducer(state = [], action) {
  switch (action.type) {
    case `${ADD_ADJUSTMENT}_SUCCESS`:
      return action.data;
    case `${ADD_ADJUSTMENT}_CLEAR_MESSAGE`:
      return [];
    default:
      return state;
  }
}

function setKeyValueFromIdName(data) {
  data.key = data.id;
  data.value = data.name;
}

const rootReducer = combineReducers({
  data: cashManagementWorkflowDataReducer,
  showAdjustmentDialog: adjustmentDialogReducer,
  showWireDialog: wireDialogReducer,
  adjustmentAmount: updateAdjustmentAmountReducer,
  logWireStatus: logWireAndBookTradeReducer,
  genericLogWireStatus: logWireAndBookTradeGenericReducer,
  logWireBookTradeAlert: logWireBookTradeAlertReducer,
  genericLogWireBookTradeAlert: genericLogWireBookTradeAlertReducer,
  logWireBookTradeSelectionStatus: logWireBookTradeSelectionDialogReducer,
  genericLogWireBookTradeSelectionStatus: genericLogWireBookTradeSelectionDialogReducer,
  showGenericMMFBookingDialog: showGenericMMFBookingDialogReducer,
  genericMMFBookingDialogData: genericMMFBookingDialogDataReducer,
  isOverRideInvalidAmountCheck: overRideInvalidAmountCheck,
  mmfCutoffTimeData: getMMFCutoffTimeDataReducer,
  showAddAdjustmentDialog: showAddAdjustmentDialogReducer,
  addAdjustmentSuccessDialog: showAddAdjustmentSuccessDialogReducer,
  showAddAdjustmentAlert: showAddAdjustmentAlertReducer,
  addedAdjustmentData: addedAdjustmentsDataReducer
});

export default rootReducer;
