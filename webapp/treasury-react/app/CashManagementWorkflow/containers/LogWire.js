/* eslint-disable no-restricted-globals */
import { Dialog } from "arc-react-components";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CheckboxFilter from "../../commons/components/CheckboxFilter";
import Column from "../../commons/components/Column";
import InputFilter from "../../commons/components/InputFilter";
import Panel from "../../commons/components/Panel";
import SingleSelectFilter from "../../commons/components/SingleSelectFilter";
import { SEG_IA } from "../../commons/constants";
import DateFilter from "../../commons/container/DateFilter";
import { getCommaSeparatedNumber, getHHmmTime } from "../../commons/util";
import SelectWithLabel from "../components/SelectWithLabel";
import {
  closeWireDialog,
  dontOverRideInvalidAmountCheck,
  hideLogWireAndBookTradeAlert,
  hideLogWireBookTradeSelectionDialog,
  logWireAndBookTrade,
  overRideInvalidAmountCheck,
  showLogWireBookTradeSelectionDialog,
  updateWireTradeDetail
} from "../actions";
import {
  logWireAndBookTrade as createLogWireAndBookTrade,
  isCurrentBookingTimeValid
} from "../Utils";
import { BookFilter } from "./../components/BookFilter";
import { BundleFilter } from "./../components/BundleFilter";
import { LogWireBookTradeDialog } from "./../components/LogWireBookTradeDialog";
import { LogWireBookTradeSelectionConfirmationDialog } from "./../components/LogWireBookTradeSelectionConfirmationDialog";
import { RagStatus } from "./../components/RagStatus";
import { Portfolio } from "../components/Portfolio";

class LogWire extends Component {
  constructor(props) {
    super(props);
    this.handleSave = this.handleSave.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.closeWireDialog = this.closeWireDialog.bind(this);
    this.overRideCheck = this.overRideCheck.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSelectionDialogClose = this.handleSelectionDialogClose.bind(
      this
    );
    this.state = this.getDefaultFilters();
  }

  onSelect(params) {
    const { key, value } = params;
    this.setState(previousState => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }
  getDefaultFilters() {
    let clientName = CODEX_PROPERTIES["codex.client_name"];
    clientName = clientName.toUpperCase();
    return {
      selectedSubject: `${clientName} Treasury Cash Management`,
      selectedSourceAccount: [],
      selectedDestinationAccount: [],
      selectedSourcePortfolio: [],
      selectedDestinationPortfolio: [],
      selectedTradeBook: [],
      selectedBundles: [],
      showWireDialog: false,
      selectedWireDate: "",
      selectedValueDate: "",
      selectedAmount: "",
      selectedComment: "",
      selectedCC: "",
      selectedSubscribers: "",
      logWire: true,
      bookTrade: true,
      isTrackingWire: false,
      defaultBundle: this.props.selectedRowItem.defaultBundle,
      defaultBook: this.props.selectedRowItem.defaultBook,
      defaultSourcePortfolioBook: this.props.selectedRowItem
        .defaultSourcePortfolioBook,
      defaultDestinationPortfolioBook: this.props.selectedRowItem
        .defaultDestinationPortfolioBook,
      isDefaultBundleOverride: false,
      isDefaultBookOverride: false,
      isDefaultSourcePortfolioBookOverride: false,
      isDefaultDestinationPortfolioBookOverride: false
    };
  }
  handleReset() {
    this.setState(this.getDefaultFilters);
  }

  closeWireDialog() {
    this.setState(this.getDefaultFilters());
    this.props.closeWireDialog();
    this.props.dontOverRideInvalidAmountCheck();
  }
  overRideCheck() {
    this.props.overRideInvalidAmountCheck();
  }
  handleSave() {
    let message = "";
    const {
      logWire,
      bookTrade,
      isDefaultBookOverride,
      isDefaultBundleOverride,
      isDefaultSourcePortfolioBookOverride,
      isDefaultDestinationPortfolioBookOverride
    } = this.state;
    if (!logWire && !bookTrade) {
      message = "please select either logwire or booktrade";
    } else if (!logWire) {
      message = "You have only selected BookTrade";
    } else if (!bookTrade) {
      message = "You have only selected LogWire";
    }
    if (message !== "") {
      this.props.showLogWireBookTradeSelectionDialog({
        message: message + ". Do you want to continue?"
      });
      return;
    }
    if (!this.isMMFBoookingTimeValid()) {
      return;
    }
    let overrideMessage = "";
    if (isDefaultBookOverride && this.props.selectedRowItem.defaultBook) {
      overrideMessage =
        "You are trying to override the default Book " +
        this.props.selectedRowItem.defaultBook.name;
    }

    if (isDefaultBundleOverride && this.props.selectedRowItem.defaultBundle) {
      if (overrideMessage === "") {
        overrideMessage =
          "You are trying to override the default Bundle " +
          this.props.selectedRowItem.defaultBundle.name;
      } else {
        overrideMessage =
          overrideMessage +
          " and default Bundle " +
          this.props.selectedRowItem.defaultBundle.name;
      }
    }
    if (overrideMessage !== "") {
      this.props.showLogWireBookTradeSelectionDialog({
        message: overrideMessage + ". Do you want to continue?"
      });
    }
    if (
      isDefaultSourcePortfolioBookOverride &&
      this.props.selectedRowItem.defaultSourcePortfolioBook
    ) {
      if (overrideMessage === "") {
        overrideMessage =
          "You are trying to override the default Source Portfolio " +
          this.props.selectedRowItem.defaultSourcePortfolioBook.name;
      } else {
        overrideMessage =
          overrideMessage +
          " and default Source Portfolio " +
          this.props.selectedRowItem.defaultSourcePortfolioBook.name;
      }
    }
    if (
      isDefaultDestinationPortfolioBookOverride &&
      this.props.selectedRowItem.defaultDestinationPortfolioBook
    ) {
      if (overrideMessage === "") {
        overrideMessage =
          "You are trying to override the default Destination Portfolio " +
          this.props.selectedRowItem.defaultDestinationPortfolioBook.name;
      } else {
        overrideMessage =
          overrideMessage +
          " and default Destination Portfolio " +
          this.props.selectedRowItem.defaultDestinationPortfolioBook.name;
      }
    }
    if (overrideMessage !== "") {
      this.props.showLogWireBookTradeSelectionDialog({
        message: overrideMessage + ". Do you want to continue?"
      });
      return;
    }
    const logWireBookTradeFilter = createLogWireAndBookTrade(
      this.props.selectedRowItem.amount,
      this.props.selectedRowItem.spn,
      this.props.params.dateStr,
      this.props.params.workflowId,
      this.props.params.fundId,
      this.props.selectedRowItem.custodianAccount.custodianAccountId,
      this.state,
      this.props.selectedRowItem.fundWireInstructions,
      this.props.selectedRowItem.wireInstructions,
      this.props.portfolios,
      this.props.selectedRowItem.defaultBook,
      this.props.selectedRowItem.defaultBundle,
      this.props.selectedRowItem.defaultSourcePortfolioBook,
      this.props.selectedRowItem.defaultDestinationPortfolioBook
    );
    this.props.logWireAndBookTrade(logWireBookTradeFilter);
  }

  isMMFBoookingTimeValid() {
    let message = "";
    if (this.props.selectedRowItem.mmf.cutoffTime) {
      let cutoffTime = this.props.selectedRowItem.mmf.cutoffTime
        .businessDayMMFThresholdTime;
      if (this.props.isTomorrowHoliday) {
        cutoffTime = this.props.selectedRowItem.mmf.cutoffTime
          .holidayMMFThresholdTime;
      }

      if (
        typeof cutoffTime == "number" &&
        !isCurrentBookingTimeValid(cutoffTime)
      ) {
        message = this.props.isTomorrowHoliday
          ? "Tomorrow is not a Business Day. Today's MMF Booking Time Limit Exceeded."
          : "Business Day MMF Booking Time Limit Exceeded.";
        message = message.concat(
          " Cutoff Time is " + getHHmmTime(cutoffTime) + " HRS"
        );
      }
    } else {
      message = "No MMF CutoffTime Configured";
    }
    if (message !== "") {
      this.props.showLogWireBookTradeSelectionDialog({
        message: message + ". Do you want to continue?"
      });
      return false;
    }
    return true;
  }
  handleSelectionDialogClose() {
    this.props.hideLogWireBookTradeSelectionDialog();
    const logWireBookTradeFilter = createLogWireAndBookTrade(
      this.props.selectedRowItem.amount,
      this.props.selectedRowItem.spn,
      this.props.params.dateStr,
      this.props.params.workflowId,
      this.props.params.fundId,
      this.props.selectedRowItem.custodianAccount.custodianAccountId,
      this.state,
      this.props.selectedRowItem.fundWireInstructions,
      this.props.selectedRowItem.wireInstructions,
      this.props.portfolios,
      this.props.selectedRowItem.defaultBook,
      this.props.selectedRowItem.defaultBundle,
      this.props.selectedRowItem.defaultSourcePortfolioBook,
      this.props.selectedRowItem.defaultDestinationPortfolioBook
    );
    this.props.logWireAndBookTrade(logWireBookTradeFilter);
  }
  handleClose() {
    if (this.props.logWireStatus.successStatus) {
      if (this.props.selectedRowItem.mmf || this.props.selectedRowItem.spn) {
        const { ticker } = this.props.selectedRowItem;
        const { source } = this.props.selectedRowItem;
        const { wireId, tradeId, amount } = this.props.logWireStatus.response;
        this.props.updateWireTradeDetail({
          wireId,
          tradeId,
          source,
          ticker,
          amount,
          workFlowDate: this.props.params.dateStr,
          resp: this.props.logWireStatus.response
        });
      }

      this.closeWireDialog();
    }
    this.props.hideLogWireAndBookTradeAlert();
  }
  render() {
    let sourceAccounts = [];
    let destinationAccounts = [];
    if (this.props.selectedRowItem.amount > 0) {
      sourceAccounts = this.props.selectedRowItem.fundWireInstructions;
      destinationAccounts = this.props.selectedRowItem.wireInstructions;
    } else {
      destinationAccounts = this.props.selectedRowItem.fundWireInstructions;
      sourceAccounts = this.props.selectedRowItem.wireInstructions;
    }
    const isSellAmountGreaterThanBalance =
      this.props.selectedRowItem.amount < 0 &&
      Math.abs(this.props.selectedRowItem.amount) >
        this.props.selectedRowItem.balanceUsd;
    const isInvalidAmount =
      (this.props.selectedRowItem.amount === "" ||
        isNaN(this.props.selectedRowItem.amount) ||
        isSellAmountGreaterThanBalance) &&
      this.props.showWireDialog &&
      this.props.selectedRowItem !== undefined &&
      this.props.selectedRowItem.source !== SEG_IA &&
      (this.props.selectedRowItem.mmf || this.props.selectedRowItem.spn);
    const { isOverRideInvalidAmountCheck } = this.props;
    return (
      <React.Fragment>
        {this.props.logWireBookTradeSelectionStatus.display && (
          <LogWireBookTradeSelectionConfirmationDialog
            message={this.props.logWireBookTradeSelectionStatus.message}
            display={this.props.logWireBookTradeSelectionStatus.display}
            hideLogWireBookTradeSelectionDialog={
              this.props.hideLogWireBookTradeSelectionDialog
            }
            handleSelectionDialogClose={this.handleSelectionDialogClose}
          />
        )}
        {this.props.logWireBookTradeAlert && (
          <LogWireBookTradeDialog
            logWireBookTradeAlert={this.props.logWireBookTradeAlert}
            message={this.props.logWireStatus.message}
            handleClose={this.handleClose}
          />
        )}
        {isInvalidAmount && this.props.selectedRowItem.spn && (
          <Dialog
            isOpen={this.props.showWireDialog}
            title="Wire Amount Error"
            onClose={this.closeWireDialog}
          >
            {isSellAmountGreaterThanBalance ? (
              <React.Fragment>
                <div style={{ "text-align": "center" }}>
                  <span>
                    Amount is greater than available balance, Do you want to
                    continue ?
                  </span>
                </div>
                <div
                  style={{
                    display: "flex",
                    "align-items": "center",
                    "justify-content": "center"
                  }}
                >
                  <button
                    className="padding--right margin"
                    onClick={this.overRideCheck}
                  >
                    Yes
                  </button>
                  <button onClick={this.closeWireDialog}>No</button>
                </div>
              </React.Fragment>
            ) : (
              <span>Please Enter Valid Amount</span>
            )}
          </Dialog>
        )}
        {(isOverRideInvalidAmountCheck || !isInvalidAmount) &&
          !isNaN(this.props.selectedRowItem.amount) &&
          this.props.showWireDialog &&
          this.props.selectedRowItem !== undefined &&
          this.props.selectedRowItem.source !== SEG_IA &&
          this.props.selectedRowItem.spn && (
            <Dialog
              isOpen={this.props.showWireDialog}
              title="Log Wire"
              onClose={this.closeWireDialog}
              footer={
                <React.Fragment>
                  <button onClick={this.handleSave}>Save</button>
                  <button onClick={this.handleReset}>Reset</button>
                </React.Fragment>
              }
            >
              <InputFilter
                onSelect={this.onSelect}
                stateKey="selectedSubject"
                data={this.state.selectedSubject}
                label="Subject"
              />
              <InputFilter
                onSelect={this.onSelect}
                stateKey="selectedSubscribers"
                data={this.state.selectedSubscribers}
                label="Subscribers"
              />
              <InputFilter
                onSelect={this.onSelect}
                stateKey="selectedCC"
                data={this.state.selectedCC}
                label="CC"
              />
              {sourceAccounts.length !== 1 && (
                <SingleSelectFilter
                  onSelect={this.onSelect}
                  selectedData={this.state.selectedSourceAccount}
                  stateKey="selectedSourceAccount"
                  data={sourceAccounts}
                  label="Source Account"
                />
              )}
              {sourceAccounts.length === 1 && (
                <SelectWithLabel
                  label="Source Account"
                  options={sourceAccounts}
                  value={sourceAccounts[0]}
                  readonly
                />
              )}
              {destinationAccounts.length !== 1 && (
                <SingleSelectFilter
                  onSelect={this.onSelect}
                  selectedData={this.state.selectedDestinationAccount}
                  stateKey="selectedDestinationAccount"
                  data={destinationAccounts}
                  label="Destination Account"
                />
              )}
              {destinationAccounts.length === 1 && (
                <SelectWithLabel
                  label="Destination Account"
                  options={destinationAccounts}
                  value={destinationAccounts[0]}
                  readonly
                />
              )}
              <Portfolio
                isDefaultPortfolioBookOverride={
                  this.state.isDefaultSourcePortfolioBookOverride
                }
                defaultPortfolioBook={this.state.defaultSourcePortfolioBook}
                selectedPortfolio={this.state.selectedSourcePortfolio}
                portfolios={this.props.portfolios}
                onSelect={this.onSelect}
                stateKey="selectedSourcePortfolio"
                defaultStateKey="defaultSourcePortfolioBook"
                label="Source Portfolio"
                checkBoxStateKey="isDefaultSourcePortfolioBookOverride"
              />
              <Portfolio
                isDefaultPortfolioBookOverride={
                  this.state.isDefaultDestinationPortfolioBookOverride
                }
                defaultPortfolioBook={
                  this.state.defaultDestinationPortfolioBook
                }
                selectedPortfolio={this.state.selectedDestinationPortfolio}
                portfolios={this.props.portfolios}
                onSelect={this.onSelect}
                stateKey="selectedDestinationPortfolio"
                defaultStateKey="defaultDestinationPortfolioBook"
                label="Destination Portfolio"
                checkBoxStateKey="isDefaultDestinationPortfolioBookOverride"
              />
              <BookFilter
                isDefaultBookOverride={this.state.isDefaultBookOverride}
                selectedTradeBook={this.state.selectedTradeBook}
                portfolios={this.props.portfolios}
                onSelect={this.onSelect}
                selectedRowItem={this.props.selectedRowItem}
              />
              <BundleFilter
                isDefaultBundleOverride={this.state.isDefaultBundleOverride}
                selectedBundles={this.state.selectedBundles}
                onSelect={this.onSelect}
                selectedRowItem={this.props.selectedRowItem}
              />

              <DateFilter
                onSelect={this.onSelect}
                data={this.state.selectedWireDate}
                stateKey="selectedWireDate"
                dateType="tFilterDate"
                label="Wire Date"
              />
              <DateFilter
                onSelect={this.onSelect}
                data={this.state.selectedValueDate}
                stateKey="selectedValueDate"
                dateType="tFilterDate"
                label="Value Date"
              />
              <InputFilter
                onSelect={this.onSelect}
                stateKey="selectedAmount"
                disabled
                data={getCommaSeparatedNumber(
                  this.props.selectedRowItem.amount
                )}
                label="Amount"
              />
              {this.props.selectedRowItem.mmf && (
                <RagStatus
                  fundOwnershipClassName={
                    this.props.selectedRowItem.fundOwnershipClassName
                  }
                  entityOwnershipClassName={
                    this.props.selectedRowItem.entityOwnershipClassName
                  }
                />
              )}
              <InputFilter
                onSelect={this.onSelect}
                stateKey="selectedComment"
                data={this.state.selectedComment}
                label="Comment"
              />
              <Panel>
                <Column>
                  <CheckboxFilter
                    defaultChecked={this.state.logWire}
                    onSelect={this.onSelect}
                    stateKey="logWire"
                    label="Log Wire"
                  />
                  <CheckboxFilter
                    defaultChecked={this.state.bookTrade}
                    onSelect={this.onSelect}
                    stateKey="bookTrade"
                    label="Book Trade"
                  />
                  <CheckboxFilter
                    defaultChecked={this.state.isTrackingWire}
                    onSelect={this.onSelect}
                    stateKey="isTrackingWire"
                    label="Is Tracking Wire"
                  />
                </Column>
              </Panel>
            </Dialog>
          )}
      </React.Fragment>
    );
  }
}

LogWire.propTypes = {
  closeWireDialog: PropTypes.any,
  hideLogWireAndBookTradeAlert: PropTypes.any,
  hideLogWireBookTradeSelectionDialog: PropTypes.any,
  logWireBookTradeAlert: PropTypes.any,
  logWireBookTradeSelectionStatus: PropTypes.any,
  logWireStatus: PropTypes.any,
  params: PropTypes.any,
  portfolios: PropTypes.any,
  selectedRowItem: PropTypes.any,
  showLogWireBookTradeSelectionDialog: PropTypes.any,
  showWireDialog: PropTypes.any,
  updateWireTradeDetail: PropTypes.any,
  logWireAndBookTrade: PropTypes.func,
  isOverRideInvalidAmountCheck: PropTypes.bool,
  overRideInvalidAmountCheck: PropTypes.func,
  dontOverRideInvalidAmountCheck: PropTypes.func,
  isTomorrowHoliday: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    showWireDialog: state.cashManagementWorkflowData.showWireDialog,
    logWireStatus: state.cashManagementWorkflowData.logWireStatus,
    logWireBookTradeAlert:
      state.cashManagementWorkflowData.logWireBookTradeAlert,
    logWireBookTradeSelectionStatus:
      state.cashManagementWorkflowData.logWireBookTradeSelectionStatus,
    isOverRideInvalidAmountCheck:
      state.cashManagementWorkflowData.isOverRideInvalidAmountCheck,
    isTomorrowHoliday: state.cashManagementWorkflowData.isTomorrowHoliday
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      closeWireDialog,
      logWireAndBookTrade,
      hideLogWireAndBookTradeAlert,
      updateWireTradeDetail,
      showLogWireBookTradeSelectionDialog,
      hideLogWireBookTradeSelectionDialog,
      overRideInvalidAmountCheck,
      dontOverRideInvalidAmountCheck
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(LogWire);
