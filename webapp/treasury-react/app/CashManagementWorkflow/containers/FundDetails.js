import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { executeWorkflow } from '../../commons/actions/workflow';
import { displayAdjustmentDialog, updateAdjustmentAmount, hideAddAdjustmentAlert } from '../actions';
import { FundDetailsFragment } from './../components/FundDetailsFragment';

class FundDetails extends Component {
  constructor(props) {
    super(props);
    this.displayAdjustmentDialog = this.displayAdjustmentDialog.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePublish = this.handlePublish.bind(this);
  }

  displayAdjustmentDialog() {
    this.props.displayAdjustmentDialog();
  }

  handleClose() {
    if (this.props.addAdjustmentStatus[0].status === 'success') {
      const adjustedAmount = this.props.addAdjustmentStatus.amount;
      this.props.updateAdjustmentAmount({ amount: adjustedAmount });
    }
    this.props.hideAddAdjustmentAlert();
  }

  handlePublish() {
    const payload = {
      'workflowParam.workflowId': this.props.params.workflowId,
      status: 'PUBLISHED'
    };

    this.props.executeWorkflow(payload);
  }

  render() {
    if (
      this.props.publishStatus !== undefined &&
      this.props.publishStatus.resultList !== undefined &&
      this.props.publishStatus.resultList.length > 0 &&
      this.props.publishStatus.resultList[0].workflowId !== undefined &&
      this.props.publishStatus.resultList[0].workflowId === this.props.params.workflowId
    ) {
      window.close();
    }
    if (!Object.prototype.hasOwnProperty.call(this.props.cashManagementWorkflowData, 'fundLevelLiquidityProfiles')) {
      return '';
    }
    const amount =
      Number(this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0].expectedCashOutflowAmount) +
      Number(this.props.adjustmentAmount.amount);

    return (
      <FundDetailsFragment
        amount={amount}
        isAlert={this.props.isAlert}
        addAdjustmentStatus={this.props.addAdjustmentStatus}
        params={this.props.params}
        cashManagementWorkflowData={this.props.cashManagementWorkflowData}
        handleClose={this.handleClose}
        handlePublish={this.handlePublish}
        displayAdjustmentDialog={this.displayAdjustmentDialog}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    cashManagementWorkflowData: state.cashManagementWorkflowData.data,
    isAlert: state.cashManagementWorkflowData.showAddAdjustmentAlert,
    addAdjustmentStatus: state.cashManagementWorkflowData.addedAdjustmentData,
    adjustmentAmount: state.cashManagementWorkflowData.adjustmentAmount,
    publishStatus: state.workflow.workflowStatus
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      displayAdjustmentDialog,
      hideAddAdjustmentAlert,
      updateAdjustmentAmount,
      executeWorkflow
    },
    dispatch
  );
}
FundDetails.propTypes = {
  displayAdjustmentDialog: PropTypes.func,
  addAdjustmentStatus: PropTypes.array,
  updateAdjustmentAmount: PropTypes.func,
  hideAddAdjustmentAlert: PropTypes.func,
  executeWorkflow: PropTypes.func,
  params: PropTypes.object,
  publishStatus: PropTypes.array,
  adjustmentAmount: PropTypes.object,
  isAlert: PropTypes.bool,
  cashManagementWorkflowData: PropTypes.object
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FundDetails);
