import { Dialog } from "arc-react-components";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import CheckboxFilter from "../../commons/components/CheckboxFilter";
import Column from "../../commons/components/Column";
import InputFilter from "../../commons/components/InputFilter";
import Panel from "../../commons/components/Panel";
import SingleSelectFilter from "../../commons/components/SingleSelectFilter";
import DateFilter from "../../commons/container/DateFilter";
import {
  getCommaSeparatedNegativeOnlyNumber,
  getCommaSeparatedNumber,
  getHHmmTime
} from "../../commons/util";
import SelectWithLabel from "../components/SelectWithLabel";
import {
  fetchCashManagementWorkflowData,
  hideGenericLogWireAndBookTradeAlert,
  hideGenericLogWireBookTradeSelectionDialog,
  hideGenericMMFBookingDialog,
  logWireAndBookTradeGeneric,
  showGenericLogWireBookTradeSelectionDialog,
  updateWireTradeDetail
} from "../actions";
import { LogWireBookTradeDialog } from "../components/LogWireBookTradeDialog";
import { LogWireBookTradeSelectionConfirmationDialog } from "../components/LogWireBookTradeSelectionConfirmationDialog";
import {
  logWireAndBookTrade as createLogWireAndBookTrade,
  isCurrentBookingTimeValid
} from "../Utils";
class GenericMMFBookingDialog extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleSelectionDialogClose = this.handleSelectionDialogClose.bind(
      this
    );
    this.handleClose = this.handleClose.bind(this);
    this.closeMMFBookingDialog = this.closeMMFBookingDialog.bind(this);

    this.state = this.getDefaultFilters();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.mmfUpdate && this.props.mmfUpdate !== undefined) {
      const payload = {
        "CashManagementFilter.date": this.props.params.dateStr,
        "CashManagementFilter.legalEntityFamilyIds": this.props.params.fundId,
        "CashManagementFilter.enrichWorkflowData": true,
        "CashManagementFilter.workflowId": this.props.params.workflowId
      };

      this.props.fetchCashManagementWorkflowData(payload);
    }
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }
  getDefaultFilters() {
    let clientName = CODEX_PROPERTIES["codex.client_name"];
    clientName = clientName.toUpperCase();
    return {
      // eslint-disable-next-line no-undef
      selectedSubject: `${clientName} Treasury Cash Management`,
      selectedSourceAccount: [],
      selectedDestinationAccount: [],
      selectedSourcePortfolio: [],
      selectedDestinationPortfolio: [],
      selectedCustodianAccount: [],
      selectedTradeBook: [],
      showWireDialog: false,
      selectedWireDate: "",
      selectedValueDate: "",
      selectedAmount: "",
      selectedComment: "",
      selectedCC: "",
      selectedSubscribers: "",
      selectedMMFSpn: "",
      logWire: true,
      bookTrade: true,
      isTrackingWire: false
    };
  }
  handleClose() {
    if (this.props.logWireStatus.successStatus) {
      const {
        wireId,
        tradeId,
        amount,
        custodianAccountId,
        spn
      } = this.props.logWireStatus.response;
      this.props.updateWireTradeDetail({
        wireId,
        tradeId,
        custodianAccountId,
        spn,
        amount,
        resp: this.props.logWireStatus.response,
        workFlowDate: this.props.params.dateStr
      });
      this.closeMMFBookingDialog();
    }

    this.props.hideGenericLogWireAndBookTradeAlert();
  }
  closeMMFBookingDialog() {
    this.setState(this.getDefaultFilters());
    this.props.hideGenericMMFBookingDialog();
  }
  handleSelectionDialogClose() {
    this.props.hideGenericLogWireBookTradeSelectionDialog();
    this.logWireAndBookTrade();
  }
  handleSave() {
    let message = "";
    const { logWire, bookTrade } = this.state;
    if (!logWire && !bookTrade) {
      message = "please select either logwire or booktrade";
    } else if (!logWire) {
      message = "You have only selected BookTrade";
    } else if (!bookTrade) {
      message = "You have only selected LogWire";
    }
    if (message !== "") {
      this.props.showGenericLogWireBookTradeSelectionDialog({
        message: message + ". Do you want to continue?"
      });
      return;
    }
    this.validateMMFBoookingTime();
    this.logWireAndBookTrade();
  }

  validateMMFBoookingTime() {
    let message = "";
    const selectedMMFSpnKey = this.state.selectedMMFSpn.key;
    if (this.props.mmfCutoffTimeData[selectedMMFSpnKey] !== undefined) {
      const cutoffTime = this.props.mmfCutoffTimeData[selectedMMFSpnKey]
        .businessDayMMFThresholdTime;

      if (this.props.isTomorrowHoliday) {
        cutoffTime = this.props.mmfCutoffTimeData[selectedMMFSpnKey]
          .holidayMMFThresholdTime;
      }

      if (
        typeof cutoffTime == "number" &&
        !isCurrentBookingTimeValid(cutoffTime)
      ) {
        message = this.props.isTomorrowHoliday
          ? "Tomorrow is not a Business Day. Today's MMF Booking Time Limit Exceeded."
          : "Business Day MMF Booking Time Limit Exceeded.";
        message = message.concat(
          " Cutoff Time is " + getHHmmTime(cutoffTime) + "HRS"
        );
      }
    }

    if (message !== "") {
      this.props.showGenericLogWireBookTradeSelectionDialog({
        message: message + ". Do you want to continue?"
      });
      return;
    }
  }

  logWireAndBookTrade() {
    const {
      selectedAmount,
      selectedMMFSpn,
      selectedCustodianAccount
    } = this.state;
    const custodianAccountId =
      selectedCustodianAccount.key !== undefined ||
      this.props.genericMMFBookingDialogData.custodianAccounts.length !== 1
        ? selectedCustodianAccount.key
        : this.props.genericMMFBookingDialogData.custodianAccounts[0].id;
    const amount = selectedAmount
      ? selectedAmount.toString().replace(/,/g, "")
      : "0";
    const logWireBookTradeFilter = createLogWireAndBookTrade(
      amount,
      selectedMMFSpn.key,
      this.props.params.dateStr,
      this.props.params.workflowId,
      this.props.params.fundId,
      custodianAccountId,
      this.state,
      this.props.genericMMFBookingDialogData.sourceAccounts,
      this.props.genericMMFBookingDialogData.destinationAccounts,
      this.props.genericMMFBookingDialogData.portfolios
    );
    this.props.logWireAndBookTradeGeneric(logWireBookTradeFilter);
  }

  handleReset() {
    this.setState(this.getDefaultFilters);
  }
  render() {
    const {
      mmfs,
      sourceAccounts,
      destinationAccounts,
      custodianAccounts,
      portfolios,
      isBuy
    } = this.props.genericMMFBookingDialogData;
    return (
      <React.Fragment>
        {this.props.logWireBookTradeSelectionStatus.display && (
          <LogWireBookTradeSelectionConfirmationDialog
            message={this.props.logWireBookTradeSelectionStatus.message}
            display={this.props.logWireBookTradeSelectionStatus.display}
            hideLogWireBookTradeSelectionDialog={
              this.props.hideGenericLogWireBookTradeSelectionDialog
            }
            handleSelectionDialogClose={this.handleSelectionDialogClose}
          />
        )}
        {this.props.logWireBookTradeAlert && (
          <LogWireBookTradeDialog
            logWireBookTradeAlert={this.props.logWireBookTradeAlert}
            message={this.props.logWireStatus.message}
            handleClose={this.handleClose}
          />
        )}
        <Dialog
          isOpen={this.props.showGenericMMFBookingDialog}
          onClose={this.props.hideGenericMMFBookingDialog}
          title="Book MMF"
          footer={
            <React.Fragment>
              <button onClick={this.handleSave}>Save</button>
              <button onClick={this.handleReset}>Reset</button>
            </React.Fragment>
          }
        >
          <SingleSelectFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedMMFSpn}
            stateKey="selectedMMFSpn"
            data={mmfs}
            label="MMF Spn"
          />
          <InputFilter
            onSelect={this.onSelect}
            stateKey="selectedSubject"
            data={this.state.selectedSubject}
            label="Subject"
          />
          <InputFilter
            onSelect={this.onSelect}
            stateKey="selectedSubscribers"
            data={this.state.selectedSubscribers}
            label="Subscribers"
          />
          <InputFilter
            onSelect={this.onSelect}
            stateKey="selectedCC"
            data={this.state.selectedCC}
            label="CC"
          />
          {sourceAccounts.length !== 1 && (
            <SingleSelectFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedSourceAccount}
              stateKey="selectedSourceAccount"
              data={isBuy ? sourceAccounts : destinationAccounts}
              label="Source Account"
            />
          )}
          {sourceAccounts.length === 1 && (
            <SelectWithLabel
              label="Source Account"
              options={isBuy ? sourceAccounts : destinationAccounts}
              value={isBuy ? sourceAccounts[0] : destinationAccounts[0]}
              readonly
            />
          )}
          {destinationAccounts.length !== 1 && (
            <SingleSelectFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedDestinationAccount}
              stateKey="selectedDestinationAccount"
              data={isBuy ? destinationAccounts : sourceAccounts}
              label="Destination Account"
            />
          )}
          {destinationAccounts.length === 1 && (
            <SelectWithLabel
              label="Destination Account"
              options={isBuy ? sourceAccounts : destinationAccounts}
              value={isBuy ? sourceAccounts[0] : destinationAccounts[0]}
              readonly
            />
          )}
          {portfolios.length !== 1 && (
            <SingleSelectFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedSourcePortfolio}
              stateKey="selectedSourcePortfolio"
              data={portfolios}
              label="Source Portfolio"
            />
          )}
          {portfolios.length !== 1 && (
            <SingleSelectFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedDestinationPortfolio}
              stateKey="selectedDestinationPortfolio"
              data={portfolios}
              label="Destination Portfolio"
            />
          )}
          {portfolios.length === 1 && (
            <SelectWithLabel
              label="Source Portfolio"
              options={portfolios}
              value={portfolios[0]}
              readonly
            />
          )}
          {portfolios.length === 1 && (
            <SelectWithLabel
              label="Destination Portfolio"
              options={portfolios}
              value={portfolios[0]}
              readonly
            />
          )}
          {custodianAccounts.length !== 1 && (
            <SingleSelectFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedCustodianAccount}
              stateKey="selectedCustodianAccount"
              data={custodianAccounts}
              label="Custodian Account"
            />
          )}
          {custodianAccounts.length === 1 && (
            <SelectWithLabel
              label="Custodian Account"
              options={custodianAccounts}
              value={custodianAccounts[0]}
              readonly
            />
          )}
          {portfolios.length !== 1 && (
            <SingleSelectFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedTradeBook}
              stateKey="selectedTradeBook"
              data={portfolios}
              label="Trade Book"
            />
          )}
          {portfolios.length === 1 && (
            <SelectWithLabel
              label="Trade Book"
              options={portfolios}
              value={portfolios[0]}
              readonly
            />
          )}
          <DateFilter
            onSelect={this.onSelect}
            data={this.state.selectedWireDate}
            dateType="tFilterDate"
            stateKey="selectedWireDate"
            label="Wire Date"
          />
          <DateFilter
            onSelect={this.onSelect}
            data={this.state.selectedValueDate}
            dateType="tFilterDate"
            stateKey="selectedValueDate"
            label="Value Date"
          />
          <InputFilter
            onSelect={this.onSelect}
            stateKey="selectedAmount"
            data={
              isBuy
                ? getCommaSeparatedNumber(this.state.selectedAmount)
                : getCommaSeparatedNegativeOnlyNumber(this.state.selectedAmount)
            }
            label="Amount"
          />
          <InputFilter
            onSelect={this.onSelect}
            stateKey="selectedComment"
            data={this.state.selectedComment}
            label="Comment"
          />
          <Panel>
            <Column>
              <CheckboxFilter
                defaultChecked={this.state.logWire}
                onSelect={this.onSelect}
                stateKey="logWire"
                label="Log Wire"
              />
              <CheckboxFilter
                defaultChecked={this.state.bookTrade}
                onSelect={this.onSelect}
                stateKey="bookTrade"
                label="Book Trade"
              />
              <CheckboxFilter
                defaultChecked={this.state.isTrackingWire}
                onSelect={this.onSelect}
                stateKey="isTrackingWire"
                label="Is Tracking Wire"
              />
            </Column>
          </Panel>
        </Dialog>
      </React.Fragment>
    );
  }
}

GenericMMFBookingDialog.propTypes = {
  genericMMFBookingDialogData: PropTypes.object,
  hideGenericMMFBookingDialog: PropTypes.func,
  hideGenericLogWireBookTradeSelectionDialog: PropTypes.any,
  logWireAndBookTradeGeneric: PropTypes.func,
  logWireBookTradeAlert: PropTypes.any,
  logWireBookTradeSelectionStatus: PropTypes.any,
  logWireStatus: PropTypes.any,
  params: PropTypes.any,
  showGenericLogWireBookTradeSelectionDialog: PropTypes.func,
  showGenericMMFBookingDialog: PropTypes.bool,
  hideGenericLogWireAndBookTradeAlert: PropTypes.func,
  mmfUpdate: PropTypes.bool,
  fetchCashManagementWorkflowData: PropTypes.func,
  updateWireTradeDetail: PropTypes.func,
  mmfCutoffTimeData: PropTypes.array,
  isTomorrowHoliday: PropTypes.bool
};
const mapStateToProps = (state, ownProps) => {
  return {
    genericMMFBookingDialogData:
      state.cashManagementWorkflowData.genericMMFBookingDialogData,
    showGenericMMFBookingDialog:
      state.cashManagementWorkflowData.showGenericMMFBookingDialog,
    logWireStatus: state.cashManagementWorkflowData.genericLogWireStatus,
    logWireBookTradeAlert:
      state.cashManagementWorkflowData.genericLogWireBookTradeAlert,
    logWireBookTradeSelectionStatus:
      state.cashManagementWorkflowData.genericLogWireBookTradeSelectionStatus,
    mmfUpdate: state.cashManagementWorkflowData.data.mmfUpdate,
    mmfCutoffTimeData: state.cashManagementWorkflowData.mmfCutoffTimeData,
    isTomorrowHoliday: state.cashManagementWorkflowData.isTomorrowHoliday
  };
};
const mapDispatchToProps = {
  hideGenericMMFBookingDialog,
  logWireAndBookTradeGeneric,
  showGenericLogWireBookTradeSelectionDialog,
  hideGenericLogWireBookTradeSelectionDialog,
  hideGenericLogWireAndBookTradeAlert,
  updateWireTradeDetail,
  fetchCashManagementWorkflowData
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GenericMMFBookingDialog);
