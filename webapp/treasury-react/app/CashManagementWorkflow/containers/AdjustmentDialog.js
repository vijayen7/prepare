import { Dialog } from 'arc-react-components';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import InputFilter from '../../commons/components/InputFilter';
import AdjustmentTypeFilter from '../../commons/container/AdjustmentTypeFilter';
import DateFilter from '../../commons/container/DateFilter';
import ReasonCodeFilter from '../../commons/container/ReasonCodeFilter';
import { closeAdjustmentDialog, addAdjustments } from '../actions';
import { getCommaSeparatedNumber } from '../../commons/util';

class AdjustmentDialog extends Component {
  constructor(props) {
    super(props);
    this.handleSave = this.handleSave.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.closeAdjustmentDialog = this.closeAdjustmentDialog.bind(this);
    this.state = {};
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }
  getDefaultFilters() {
    return {
      selectedReasonCodes: { key: 10, value: 'Parsing Issue/Limitation' },
      showAdjustmentDialog: false,
      selectedStartDate: this.props.params.dateStr,
      selectedEndDate: this.props.params.dateStr,
      selectedAmount: '',
      selectedComment: '',
      selectedAdjustmentTypes: { key: 5, value: 'Cash Outflow' }
    };
  }
  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  closeAdjustmentDialog() {
    this.props.closeAdjustmentDialog();
  }

  handleSave() {
    this.closeAdjustmentDialog();
    const payload = {
      'Adjustment.adjustmentTypeId': this.state.selectedAdjustmentTypes.key,
      'Adjustment.entityFamilyId': this.props.params.fundId,
      'Adjustment.startDate': this.state.selectedStartDate,
      'Adjustment.endDate': this.state.selectedEndDate,
      'Adjustment.reasonCodeId': this.state.selectedReasonCodes.key,
      'Adjustment.adjustmentAmountInRptCcy': this.state.selectedAmount
        ? this.state.selectedAmount.toString().replace(/,/g, '')
        : 0,
      'Adjustment.comment': this.state.selectedComment,
      'Adjustment.workflowInstanceId': this.props.params.workflowId
    };

    this.props.addAdjustments(payload);
  }

  render() {
    return (
      <Dialog
        isOpen={this.props.showAdjustmentDialog}
        title="Add Adjustment"
        onClose={this.closeAdjustmentDialog}
        footer={
          <React.Fragment>
            <button onClick={this.handleSave}>Save</button>
            <button onClick={this.handleReset}>Reset</button>
          </React.Fragment>
        }
      >
        <AdjustmentTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedAdjustmentTypes}
          multiSelect={false}
          horizontalLayout
          adjustmentType="Cash Outflow"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedStartDate"
          data={this.state.selectedStartDate}
          label="Start Date"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedEndDate"
          data={this.state.selectedEndDate}
          label="End Date"
        />
        <InputFilter
          onSelect={this.onSelect}
          stateKey="selectedAmount"
          data={getCommaSeparatedNumber(this.state.selectedAmount)}
          label="Amount"
        />
        <ReasonCodeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedReasonCodes}
          multiSelect={false}
          horizontalLayout
          stateKey="selectedReasonCodes"
          adjustmentType="CASH_OUTFLOW"
        />
        <InputFilter
          onSelect={this.onSelect}
          stateKey="selectedComment"
          data={this.state.selectedComment}
          label="Comment"
        />
      </Dialog>
    );
  }
}

function mapStateToProps(state) {
  return {
    showAdjustmentDialog: state.cashManagementWorkflowData.showAdjustmentDialog
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      closeAdjustmentDialog,
      addAdjustments
    },
    dispatch
  );
}
AdjustmentDialog.propTypes = {
  params: PropTypes.object,
  closeAdjustmentDialog: PropTypes.func,
  addAdjustments: PropTypes.func,
  showAdjustmentDialog: PropTypes.bool
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdjustmentDialog);
