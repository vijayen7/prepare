import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  displayGenericMMFBookingDialog,
  displayWireDialog,
  fetchCashManagementWorkflowData,
  getMMFCutoffTimeData
} from "../actions";
import { CashManagementWorkflowGrid } from "./../components/CashManagementWorkflowGrid";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.onCellChangeHandler = this.onCellChangeHandler.bind(this);
    this.state = { selectedRowItem: {} };
  }

  componentWillMount() {
    const payload = {
      "CashManagementFilter.date": this.props.params.dateStr,
      "CashManagementFilter.legalEntityFamilyIds": this.props.params.fundId,
      "CashManagementFilter.enrichWorkflowData": true,
      "CashManagementFilter.workflowId": this.props.params.workflowId,
      "CashManagementFilter.parsedBrokerCallsUSD": this.props.params
        .parsedBrokerCallsUSD,
      "CashManagementFilter.unParsedDataInternalCallsUSD": this.props.params
        .unParsedDataInternalCallsUSD
    };

    this.props.fetchCashManagementWorkflowData(payload);
  }

  onCellChangeHandler = (agreementRow, rowObject, isNotToggle) => {
    const gridRef = this.getGridReference();
    gridRef.refreshGrid([]);
  };

  onCellClickHandler = args => {
    if (args.colId === "logWire") {
      this.setState({ selectedRowItem: args.item });
      this.props.displayWireDialog();
    }
  };

  renderGridData() {
    let grid = null;
    if (
      !Object.prototype.hasOwnProperty.call(
        this.props.cashManagementWorkflowData,
        "fundLevelLiquidityProfiles"
      )
    ) {
      return "";
    }

    grid = (
      <CashManagementWorkflowGrid
        selectedRowItem={this.state.selectedRowItem}
        params={this.props.params}
        cashManagementWorkflowData={this.props.cashManagementWorkflowData}
        getGridReference={getGridReference => {
          this.getGridReference = getGridReference;
        }}
        onCellClickHandler={this.onCellClickHandler}
        onCellChangeHandler={this.onCellChangeHandler}
        displayGenericMMFBookingDialog={
          this.props.displayGenericMMFBookingDialog
        }
        getMMFCutoffTimeData={this.props.getMMFCutoffTimeData}
        showWireDialog={this.props.showWireDialog}
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCashManagementWorkflowData,
      displayWireDialog,
      displayGenericMMFBookingDialog,
      getMMFCutoffTimeData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    cashManagementWorkflowData: state.cashManagementWorkflowData.data,
    update: state.cashManagementWorkflowData.data.update,
    showWireDialog: state.cashManagementWorkflowData.showWireDialog
  };
}
Grid.propTypes = {
  displayWireDialog: PropTypes.func,
  fetchCashManagementWorkflowData: PropTypes.func,
  cashManagementWorkflowData: PropTypes.object,
  params: PropTypes.object,
  displayGenericMMFBookingDialog: PropTypes.func,
  getMMFCutoffTimeData: PropTypes.func
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
