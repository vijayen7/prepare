import React, { Component } from 'react';
import CheckboxComponent from '../../commons/components/Checkbox';

export default class Checkbox extends Component {
  render() {
    return (
      <div className="form-field--split">
        <label title={this.props.label}>
          {this.props.label}
        </label>
        <CheckboxComponent checked={this.props.defaultChecked} onSelect={this.props.onSelect} selectKey={this.props.stateKey} />
      </div>
    );
  }
}
