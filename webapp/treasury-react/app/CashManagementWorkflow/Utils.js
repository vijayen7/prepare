import moment from "moment";

export function logWireAndBookTrade(
  amount,
  spn,
  dateStr,
  workflowId,
  fundId,
  custodianAccountId,
  state,
  fundWireInstructions,
  wireInstructions,
  portfolios,
  defaultBook,
  defaultBundle,
  defaultSourcePortfolioBook,
  defaultDestinationPortfolioBook
) {
  let sourceAccounts = [];
  let destinationAccounts = [];
  if (Number(amount) > 0) {
    sourceAccounts = fundWireInstructions;
    destinationAccounts = wireInstructions;
  } else {
    destinationAccounts = fundWireInstructions;
    sourceAccounts = wireInstructions;
  }
  const wireWrapper = {
    "@CLASS": "com.arcesium.treasury.model.common.WireWrapper",
    wire: {
      "@CLASS": "deshaw.wires.common.wrs.domain.Wire",
      sourceAccount: {
        "@CLASS": "deshaw.wires.common.wrs.domain.Account",
        accountId:
          state.selectedSourceAccount.id !== undefined ||
            sourceAccounts.length !== 1
            ? state.selectedSourceAccount.id
            : sourceAccounts[0].id
      },
      destinationAccount: {
        "@CLASS": "deshaw.wires.common.wrs.domain.Account",
        accountId:
          state.selectedDestinationAccount.id !== undefined ||
            destinationAccounts.length !== 1
            ? state.selectedDestinationAccount.id
            : destinationAccounts[0].id
      },
      amount: Math.abs(Number(amount)),
      sourcePortfolio: {
        "@CLASS": "deshaw.wires.common.wrs.domain.Portfolio",
        bookId: getSelectedPortfolioBookId(state.selectedSourcePortfolio, defaultSourcePortfolioBook, state.isDefaultSourcePortfolioBookOverride, portfolios)
      },
      destinationPortfolio: {
        "@CLASS": "deshaw.wires.common.wrs.domain.Portfolio",
        bookId: getSelectedPortfolioBookId(state.selectedDestinationPortfolio, defaultDestinationPortfolioBook, state.isDefaultDestinationPortfolioBookOverride, portfolios)
      }
    },
    wireDateStr: state.selectedWireDate,
    valueDateStr: state.selectedValueDate,
    isTrackingWire: state.isTrackingWire
  };

  function getSelectedPortfolioBookId(selectedPortfolioBook, defaultPortfolioBook, isDefaultPortfolioBookOverride, allPortfolioBooks) {
    if (selectedPortfolioBook.id && allPortfolioBooks.length !== 1) {
      return selectedPortfolioBook.id;
    } else if (defaultPortfolioBook && !isDefaultPortfolioBookOverride) {
      return defaultPortfolioBook.id;
    } else {
      return allPortfolioBooks[0].id;
    }
  }

  const wireRequest = {
    "@CLASS": "deshaw.wires.common.wrs.domain.WireRequest",
    subscribers: state.selectedSubscribers,
    cc: state.selectedCC,
    subject: state.selectedSubject,
    comments: state.selectedComment
  };
  let selectedBundleId;
  if (defaultBundle) {
    selectedBundleId = defaultBundle.id;
  }
  if (
    (!defaultBundle || state.isDefaultBundleOverride) &&
    state.selectedBundles
  ) {
    selectedBundleId = state.selectedBundles.key;
  }

  let selectedBookId;
  if (defaultBook) {
    selectedBookId = defaultBook.id;
  }
  if (
    (!defaultBook || state.isDefaultBookOverride) &&
    state.selectedTradeBook
  ) {
    selectedBookId =
      state.selectedTradeBook.id !== undefined || portfolios.length !== 1
        ? state.selectedTradeBook.id
        : portfolios[0].id;
  }

  const logWireAndBookTradeFilter = {
    "@CLASS": "com.arcesium.treasury.model.common.LogWireAndBookTradeDetail",
    wireRequest,
    wireWrapper,
    bookTrade: state.bookTrade,
    logWire: state.logWire,
    date: state.selectedValueDate,
    workflowId,
    fundId,
    amount,
    spn,
    custodianAccountId,
    bookId: selectedBookId,
    bundleId: selectedBundleId,
    comment: state.selectedComment
  };
  return logWireAndBookTradeFilter;

  function getSelectedPortfolioId(defaultPortfolioBook, isDefaultPortfolioBookOverride, selectedPortfolio) {
    let selectedSourcePortfolioBookId;
    if (defaultPortfolioBook) {
      selectedSourcePortfolioBookId = defaultPortfolioBook.id;
    }
    if ((!defaultPortfolioBook || isDefaultPortfolioBookOverride) && selectedPortfolio) {
      selectedSourcePortfolioBookId =
        selectedPortfolio.id !== undefined || portfolios.length !== 1
          ? selectedPortfolio.id
          : portfolios[0].id;
    }
    return selectedSourcePortfolioBookId;
  }
}

export const isCurrentBookingTimeValid = cutoffTimeInMinutes => {
  const currentTimeInMinutes = moment().hours() * 60 + moment().minutes();
  if (
    typeof cutoffTimeInMinutes == "number" &&
    currentTimeInMinutes < cutoffTimeInMinutes
  ) {
    return true;
  }
  return false;
};
