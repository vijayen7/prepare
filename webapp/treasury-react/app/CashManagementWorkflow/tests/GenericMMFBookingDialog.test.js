import { shallow } from 'enzyme';
import React from 'react';
import GenericMMFBookingDialog from '../components/GenericMMFBookingDialog';

describe('EmptyContentMessageTests', () => {
  const wrapper = shallow(<GenericMMFBookingDialog />);
  it('should render correctly with no props', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
