import React from 'react';
import SingleSelectFilter from '../../commons/components/SingleSelectFilter';
import SelectWithLabel from './SelectWithLabel';
import InputFilter from '../../commons/components/InputFilter';
import Checkbox from '../containers/Checkbox';
export function Portfolio({
  onSelect,
  portfolios,
  selectedPortfolio,
  isDefaultPortfolioBookOverride,
  defaultPortfolioBook,
  stateKey,
  defaultStateKey,
  label,
  checkBoxStateKey
}) {
  return (
    <React.Fragment>
      {defaultPortfolioBook && (
        <InputFilter
          onSelect={onSelect}
          stateKey={defaultStateKey}
          disabled
          data={defaultPortfolioBook ? defaultPortfolioBook.name : ''}
          label={'Default ' + label}
        />
      )}

      {defaultPortfolioBook && (
        <Checkbox
          defaultChecked={isDefaultPortfolioBookOverride}
          onSelect={onSelect}
          stateKey={checkBoxStateKey}
          label={'Override Default ' + label}
        />
      )}

      {(!defaultPortfolioBook || isDefaultPortfolioBookOverride) && (
        <React.Fragment>
          {portfolios.length !== 1 && (
            <SingleSelectFilter
              onSelect={onSelect}
              selectedData={selectedPortfolio}
              stateKey={stateKey}
              data={portfolios}
              label={label}
            />
          )}
          {portfolios.length === 1 && (
            <SelectWithLabel label={label} options={portfolios} value={portfolios[0]} readonly />
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  );
}
