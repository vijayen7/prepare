import React from 'react';
import InputFilter from '../../commons/components/InputFilter';
import SingleSelectFilter from '../../commons/components/SingleSelectFilter';
import SelectWithLabel from './SelectWithLabel';
import Checkbox from '../containers/Checkbox';
export function BookFilter({ onSelect, portfolios, isDefaultBookOverride, selectedTradeBook, selectedRowItem }) {
  return (
    <React.Fragment>
      {selectedRowItem.defaultBook && (
        <InputFilter
          onSelect={onSelect}
          stateKey="defaultBook"
          disabled
          data={selectedRowItem.defaultBook ? selectedRowItem.defaultBook.name : ''}
          label="Default Book"
        />
      )}

      {selectedRowItem.defaultBook && (
        <Checkbox
          defaultChecked={isDefaultBookOverride}
          onSelect={onSelect}
          stateKey="isDefaultBookOverride"
          label="Override Default Book"
        />
      )}

      {(!selectedRowItem.defaultBook || isDefaultBookOverride) && (
        <React.Fragment>
          {portfolios.length !== 1 && (
            <SingleSelectFilter
              onSelect={onSelect}
              selectedData={selectedTradeBook}
              stateKey="selectedTradeBook"
              data={portfolios}
              label="Trade Book"
            />
          )}
          {portfolios.length === 1 && (
            <SelectWithLabel label="Trade Book" options={portfolios} value={portfolios[0]} readonly />
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  );
}
