import { Dialog, Layout, Panel } from 'arc-react-components';
import PropTypes from 'prop-types';
import React from 'react';
import AdjustmentDialog from '../../CashManagementWorkflow/containers/AdjustmentDialog';
import { plainNumberFormatter } from '../../commons/grid/formatters';
import EditIcon from '../components/EditIcon';
import Message from '../../commons/components/Message';
export class FundDetailsFragment extends React.Component {
  render() {
    const { amount } = this.props;
    let message = 'Report data was generated ';
    let lastUpdatedTime = new Date(this.props.params.lastUpdatedTime);
    let diffTime = (this.props.cashManagementWorkflowData.workflowTime.getTime() - lastUpdatedTime.getTime()) / 60000;
    let diffHours = Math.floor(diffTime / 60);
    let diffMins = Math.round(diffTime % 60);

    message = message + diffHours + ' hours ' + diffMins + ' minutes ago';
    return (
      <React.Fragment>
        {this.props.isAlert && (
          <Dialog isOpen={this.props.isAlert} title="Adjustment Status" onClose={this.props.handleClose}>
            {this.props.addAdjustmentStatus[0].message}
          </Dialog>
        )}
        <div className="margin" align="right">
          <button onClick={this.props.handlePublish}>Save</button></div>

        <div><Message messageData={message} /></div>
        <div >
        <Panel title="Fund Details" className="fill--width margin--top">


          <AdjustmentDialog params={this.props.params} />


           <Layout isColumnType>

           <Layout.Child childId="child1">
              <table className="table treasury-table--data-summary ">
                <tbody>
                  <tr>
                    <td>Date</td>
                    <td>{this.props.params.dateStr}</td>
                  </tr>
                  <tr>
                    <td>Fund Name</td>
                    <td>{this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0].fundName}</td>
                  </tr>
                  <tr>
                    <td>Opening Cash balance</td>
                    <td>
                      {plainNumberFormatter(
                        null,
                        null,
                        this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0].cashBalance
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>Intra Day Funding Requirement</td>
                    <td>
                      <EditIcon amount={amount} displayAdjustmentDialog={this.props.displayAdjustmentDialog} />
                    </td>
                  </tr>
                  <tr>
                    <td>Action</td>
                    <td>
                    {this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0].actionValue}
                    </td>
                  </tr>
                </tbody>
              </table>
              </Layout.Child>
              </Layout>

        </Panel>
        </div>
      </React.Fragment>
    );
  }
}

FundDetailsFragment.propTypes = {
  addAdjustmentStatus: PropTypes.array,
  amount: PropTypes.number,
  cashManagementWorkflowData: PropTypes.object,
  displayAdjustmentDialog: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  handlePublish: PropTypes.func.isRequired,
  isAlert: PropTypes.bool,
  params: PropTypes.object
};
