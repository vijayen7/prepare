import { Select } from 'arc-react-components';
import PropTypes from 'prop-types';
import React from 'react';
const SelectWithLabel = (props) => {
  return (
    <div className="form-field--split" disabled={props.disabled}>
      <label disabled={props.disabled}>{props.label}</label>
      <Select
        key={props.label}
        options={props.options}
        value={props.value}
        disabled={props.disabled}
        readonly={props.readonly}
      />
    </div>
  );
};

SelectWithLabel.propTypes = {
  label: PropTypes.any,
  options: PropTypes.any,
  value: PropTypes.any,
  disabled: PropTypes.bool,
  readonly: PropTypes.bool
};
export default SelectWithLabel;
