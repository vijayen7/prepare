import React from 'react';
import PropTypes from 'prop-types';
export const RagStatus = (props) => {
  return (
    <React.Fragment>
      {props.entityOwnershipClassName !== undefined && (
        <div className={`${props.entityOwnershipClassName} margin--top margin--bottom aln-cntr`}>
          Entity Ownership Status
        </div>
      )}
      {props.entityOwnershipClassName === undefined && (
        <div className="margin--top margin--bottom aln-cntr">Entity Ownership Status Not Available</div>
      )}
      {props.fundOwnershipClassName !== undefined && (
        <div className={`${props.fundOwnershipClassName} margin--top margin--bottom aln-cntr`}>
          Fund Ownership Status
        </div>
      )}
      {props.fundOwnershipClassName === undefined && (
        <div className={`${props.fundOwnershipClassName} margin--top margin--bottom aln-cntr`}>
          Fund Ownership Status Not Available
        </div>
      )}
    </React.Fragment>
  );
};

RagStatus.propTypes = {
  fundOwnershipClassName: PropTypes.any,
  entityOwnershipClassName: PropTypes.any
};
