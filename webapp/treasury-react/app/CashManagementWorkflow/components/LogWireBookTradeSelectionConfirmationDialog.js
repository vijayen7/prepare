import React from 'react';
import PropTypes from 'prop-types';
import { Dialog } from 'arc-react-components';
export const LogWireBookTradeSelectionConfirmationDialog = (props) => {
  return (
    <Dialog
      isOpen={props.display}
      title="Log Wire And Book Trade Selection Confirmation"
      onClose={props.hideLogWireBookTradeSelectionDialog}
      footer={
        <React.Fragment>
          <button onClick={props.handleSelectionDialogClose}>Yes</button>
          <button onClick={props.hideLogWireBookTradeSelectionDialog}>No</button>
        </React.Fragment>
      }
    >
      {props.message}
    </Dialog>
  );
};

LogWireBookTradeSelectionConfirmationDialog.propTypes = {
  handleSelectionDialogClose: PropTypes.func,
  hideLogWireBookTradeSelectionDialog: PropTypes.func,
  message: PropTypes.string,
  display: PropTypes.bool
};
