import React from 'react';
import InputFilter from '../../commons/components/InputFilter';
import BundleFilterComponent from '../../commons/container/BundleFilter';
import Checkbox from '../containers/Checkbox';
export function BundleFilter({ onSelect, isDefaultBundleOverride, selectedBundles, selectedRowItem }) {
  return (
    <React.Fragment>
      {selectedRowItem.defaultBundle && (
        <InputFilter
          onSelect={onSelect}
          stateKey="defaultBundle"
          disabled
          data={selectedRowItem.defaultBundle ? selectedRowItem.defaultBundle.name : ''}
          label="Default Bundle"
        />
      )}

      {selectedRowItem.defaultBundle && (
        <Checkbox
          defaultChecked={isDefaultBundleOverride}
          onSelect={onSelect}
          stateKey="isDefaultBundleOverride"
          label="Override Default Bundle"
        />
      )}
      {(!selectedRowItem.defaultBundle || isDefaultBundleOverride) && (
        <BundleFilterComponent
          onSelect={onSelect}
          selectedData={selectedBundles}
          multiSelect={false}
          horizontalLayout
        />
      )}
    </React.Fragment>
  );
}
