import PropTypes from "prop-types";
import React, { Component } from "react";
import { plainNumberFormatter } from "../../commons/grid/formatters";

export default class EditIcon extends Component {
  render() {
    return (
      <React.Fragment>
        <span className="size--content red">{plainNumberFormatter(null, null, this.props.amount, null, null)}</span>
        <button
          className="tertiary button--tertiary margin--left--small"
          title="Add adjustment"
          onClick={this.props.displayAdjustmentDialog}
        >
          <i className="icon-edit" />
        </button>
      </React.Fragment>
    );
  }
}
EditIcon.propTypes = {
  amount: PropTypes.number.isRequired,
  displayAdjustmentDialog: PropTypes.func.isRequired
};
