import React from 'react';
import PropTypes from 'prop-types';
import { Dialog } from 'arc-react-components';

export const LogWireBookTradeDialog = (props) => {
  return (
    <Dialog
      isOpen={props.logWireBookTradeAlert}
      title="Log Wire And Book Trade Status"
      onClose={props.handleClose}
      footer={
        <React.Fragment>
          <button onClick={props.handleClose}>Close</button>
        </React.Fragment>
      }
    >
      {props.message}
    </Dialog>
  );
};

LogWireBookTradeDialog.propTypes = {
  handleClose: PropTypes.func,
  logWireBookTradeAlert: PropTypes.bool.isRequired,
  message: PropTypes.string
};
