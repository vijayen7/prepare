import { ReactArcGrid } from "arc-grid";
import { Button, Panel } from "arc-react-components";
import PropTypes from "prop-types";
import React from "react";
import LogWire from "../../CashManagementWorkflow/containers/LogWire";
import GenericMMFBookingDialog from "../containers/GenericMMFBookingDialog";
import { getWorkflowColumns } from "../grid/columnConfig";
import { getResultOptions } from "../grid/gridOptions";
export class CashManagementWorkflowGrid extends React.Component {
  constructor(props) {
    super(props);
    this.handleNewGenericMMBooking = this.handleNewGenericMMBooking.bind(this);
    this.handleSellGenericMMBooking = this.handleSellGenericMMBooking.bind(
      this
    );
  }

  handleNewGenericMMBooking() {
    this.props.displayGenericMMFBookingDialog(
      this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0]
        .fundId,
      true
    );
    this.props.getMMFCutoffTimeData([-1]);
  }
  handleSellGenericMMBooking() {
    this.props.displayGenericMMFBookingDialog(
      this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0]
        .fundId,
      false
    );
    this.props.getMMFCutoffTimeData([-1]);
  }
  render() {
    let mmfData;
    if (
      Object.prototype.hasOwnProperty.call(
        this.props.cashManagementWorkflowData,
        "fundLevelLiquidityProfiles"
      ) &&
      this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles.length >
        0 &&
      Object.prototype.hasOwnProperty.call(
        this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0],
        "holdings"
      ) &&
      this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0]
        .holdings.length > 0
    ) {
      mmfData = this.props.cashManagementWorkflowData
        .fundLevelLiquidityProfiles[0].holdings[0];
    }
    let entityOwnershipToolTip = "";
    let fundOwnershipToolTip = "";
    if (mmfData !== undefined) {
      entityOwnershipToolTip = ` RED: >= ${mmfData.fundOwnershipThresholds.RED}%, AMBER: >= ${mmfData.fundOwnershipThresholds.AMBER}%, GREEN: <= ${mmfData.fundOwnershipThresholds.GREEN}%.`;
      fundOwnershipToolTip = ` RED: >= ${mmfData.firmOwnershipThresholds.RED}%, AMBER: >= ${mmfData.firmOwnershipThresholds.AMBER}%, GREEN: <= ${mmfData.firmOwnershipThresholds.GREEN}%.`;
    }
    return (
      <React.Fragment>
      {this.props.showWireDialog && (
        <LogWire
          params={this.props.params}
          selectedRowItem={this.props.selectedRowItem}
          portfolios={
            this.props.cashManagementWorkflowData.fundLevelLiquidityProfiles[0]
              .portfolios
          }
        />)}

        {this.handleNewGenericMMBooking && (
          <GenericMMFBookingDialog params={this.props.params} />
        )}

        <div align="left">
          <Button className="margin" onClick={this.handleNewGenericMMBooking}>
            Buy Other MMF
          </Button>
        </div>

        <div align="left">
          <Button className="margin" onClick={this.handleSellGenericMMBooking}>
            Sell Other MMF
          </Button>
        </div>
        <div>
          <Panel title="Investment Details" className="fill--width">
            {mmfData !== undefined && (
              <ReactArcGrid
                data={
                  this.props.cashManagementWorkflowData
                    .fundLevelLiquidityProfiles[0].holdings
                }
                gridId="cashManagementWorkflowData"
                getGridReference={getGridReference => {
                  this.props.getGridReference(getGridReference);
                }}
                columns={getWorkflowColumns(
                  entityOwnershipToolTip,
                  fundOwnershipToolTip
                )}
                options={getResultOptions()}
                onCellClick={this.props.onCellClickHandler}
                onCellChange={this.props.onCellChangeHandler}
              />
            )}
          </Panel>
        </div>
      </React.Fragment>
    );
  }
}

CashManagementWorkflowGrid.propTypes = {
  cashManagementWorkflowData: PropTypes.object,
  getGridReference: PropTypes.any,
  onCellChangeHandler: PropTypes.func.isRequired,
  onCellClickHandler: PropTypes.func.isRequired,
  params: PropTypes.object,
  selectedRowItem: PropTypes.object,
  displayGenericMMFBookingDialog: PropTypes.func,
  getMMFCutoffTimeData: PropTypes.func
};
