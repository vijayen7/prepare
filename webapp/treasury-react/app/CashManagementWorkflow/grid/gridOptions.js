/* eslint-disable no-restricted-globals */
export function getResultOptions() {
  return {
    saveSettings: {
      applicationId: 3,
      applicationCategory: "cashManagementWorkflowGrid",
      serviceURL: `${host}/service/SettingsService`
    },
    autoHorizontalScrollBar: true,
    nestedTable: false,
    highlightRow: true,
    highlightRowOnClick: true,
    editable: true,
    sortList: [
      {
        columnId: "ticker",
        sortAsc: true
      },
      {
        columnId: "custodian",
        sortAsc: true
      }
    ],
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    displaySummaryRow: true,
    sheetName: "Cash Management Workflow Report",
    cellRangeSelection: {
      showAggregations: true,
      multipleRangesEnabled: true
    },
    enableMultilevelGrouping: {
      isAddGroupingColumn: true,
      initialGrouping: ["ticker"]
    },
    isFilterOnFormattedValue: true,
    customColumnSelection: true,
    configureColumns: true,
    copyCellSelection: true,
    autoHeight: true,
    formatAggregateSummary(formattedText, AggregationsObj) {
      return(
        `<div class="size--content layout--flex" style="float: right; width: fit-content;">
          <div class="size--content margin--left--small margin--right--small">Count:`+Math.round(isNaN(AggregationsObj.count) ? 0: AggregationsObj.count)+`</div>
          <div class="size--content margin--left--small margin--right--small">Sum:`+Math.round(isNaN(AggregationsObj.sum) ? 0 : AggregationsObj.sum)+`</div>
          <div class="size--content margin--left--small margin--right--small">Average:`+Math.round(isNaN(AggregationsObj.avg) ? 0 : AggregationsObj.avg)+`</div>
        </div>`
    )}
  };
}
