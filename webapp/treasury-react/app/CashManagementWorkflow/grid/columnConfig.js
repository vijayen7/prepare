/* eslint-disable no-undef */
/* eslint-disable func-names */
/* eslint-disable object-shorthand */
/* eslint-disable no-restricted-globals */

import {
  textFormatter,
  groupFormatter,
  linkFormatter,
  numberFormatterDefaultZero,
  twoDecimalsPercentFormatterDefaultZero,
  priceFormatterDefaultZero,
  decimalToPercentTwoFormatterDefaultZero
} from "../../commons/grid/formatters";
import { SEG_IA } from "../../commons/constants";

export function getWorkflowColumns(
  entityOwnershipToolTip,
  fundOwnershipToolTip
) {
  const columns = [
    {
      id: "logWire",
      type: "text",
      // eslint-disable-next-line func-names
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (
          dataContext.source !== SEG_IA &&
          dataContext.spn &&
          dataContext.custodianAccount
        ) {
          // eslint-disable-next-line quotes
          return linkFormatter(
            row,
            cell,
            '<i class="icon-transfer margin--left"></i>',
            columnDef,
            dataContext
          );
        }
        return "";
      },
      name: "",
      field: "logWire",
      sortable: true,
      headerCssClass: "aln-rt b"
    },
    {
      id: "ticker",
      type: "text",
      formatter: textFormatter,
      name: "Investment",
      field: "ticker",
      sortable: true,
      headerCssClass: "aln-rt b"
    },
    {
      id: "caName",
      type: "text",
      formatter: textFormatter,
      name: "Custodian Name",
      field: "caName",
      sortable: true,
      headerCssClass: "aln-rt b"
    },
    {
      id: "balanceUsd",
      type: "number",
      formatter: numberFormatterDefaultZero,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFormatter,
      name: "Balance USD",
      field: "balanceUsd",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "amount",
      type: "textbox",
      isEditable: true,
      editor: dpGrid.Editors.Text,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (
          (value === undefined || value === "") &&
          !(dataContext.id === undefined || dataContext.id == null) &&
          dataContext.source !== SEG_IA
        ) {
          // eslint-disable-next-line quotes
          return '<div style="background-color:white;text-align:right">Enter Amount</div>';
        } else if (dataContext.source !== SEG_IA) {
          return `<div style="background-color:white">${numberFormatterDefaultZero(
            row,
            cell,
            value,
            columnDef,
            dataContext
          )}</div>`;
        }
        return null;
      },
      name: "Amount",
      field: "amount",
      aggregator: function(items, field) {
        let agg = 0;
        for (let i = 0; i < items.length; i++) {
          if (
            items[i].gridParent === undefined ||
            items[i].gridParent === "root"
          ) {
            const val = items[i][field];
            if (
              val !== null &&
              val !== "" &&
              !isNaN(val) &&
              val !== "-" &&
              items[i].source !== SEG_IA
            ) {
              agg += parseFloat(val);
            }
          }
        }

        return agg;
      },
      groupingAggregator: "Sum",
      groupTotalsFormatter: function(totals, columnDef, item, isExportToExcel) {
        let value = 0;
        if (
          item !== undefined &&
          item.rows !== undefined &&
          item.rows.length > 0
        ) {
          item.rows.forEach(row => {
            if (
              row.source !== undefined &&
              row.source !== SEG_IA &&
              !isNaN(row.amount)
            ) {
              value += Number(row.amount);
            }
          });
        }
        return numberFormatterDefaultZero(null, null, value, null, null);
      },
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "mmfAumUsd",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return "";
      },
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: function(totals, columnDef, item, isExportToExcel) {
        if (
          item !== undefined &&
          item.rows !== undefined &&
          item.rows.length > 0
        ) {
          return numberFormatterDefaultZero(
            null,
            null,
            item.rows[0].mmfAumUsd,
            null,
            null
          );
        }
        return numberFormatterDefaultZero(null, null, null, null, null);
      },
      name: "AUM USD",
      field: "mmfAumUsd",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "entityOwnership",
      type: "number",
      toolTip: `Calculated as Entity's Balance / MMF AUM. ${entityOwnershipToolTip}`,
      formatter: function(row, cell, value, columnDef, dataContext) {},
      groupTotalsFormatter: function(totals, columnDef, item, isExportToExcel) {
        if (
          !isNaN(totals.sum.balanceUsd) &&
          item !== undefined &&
          item.rows !== undefined &&
          item.rows.length > 0 &&
          !isNaN(item.rows[0].mmfAumUsd) &&
          item.rows[0].mmfAumUsd !== 0
        ) {
          let total = 0;
          if (!isNaN(totals.sum.balanceUsd))
            total += Number(totals.sum.balanceUsd);
          if (item.rows !== undefined && item.rows.length > 0) {
            item.rows.forEach(row => {
              if (
                row.source !== undefined &&
                row.source !== SEG_IA &&
                !isNaN(row.amount)
              ) {
                total += Number(row.amount);
              }
            });
          }
          const percentage = (total / item.rows[0].mmfAumUsd) * 100;
          let className = "success";
          if (percentage >= item.rows[0].fundOwnershipThresholds.RED)
            className = "critical";
          else if (percentage >= item.rows[0].fundOwnershipThresholds.AMBER)
            className = "warning";
          if (item.rows !== undefined && item.rows.length > 0) {
            item.rows.forEach(row => {
              row.entityOwnershipClassName = className;
            });
          }
          return decimalToPercentTwoFormatterDefaultZero(
            null,
            null,
            total / item.rows[0].mmfAumUsd,
            columnDef,
            null,
            className
          );
        }
        return decimalToPercentTwoFormatterDefaultZero(
          null,
          null,
          0,
          columnDef,
          null,
          "success"
        );
      },
      name: "Entity Ownership",
      field: "entityOwnership",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "fundOwnership",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {},
      groupingAggregator: "Sum",
      toolTip: `Calculated as Firm's Total Balance / MMF AUM. ${fundOwnershipToolTip}`,
      groupTotalsFormatter: function(totals, columnDef, item, isExportToExcel) {
        if (
          !isNaN(totals.sum.balanceUsd) &&
          item !== undefined &&
          item.rows !== undefined &&
          item.rows.length > 0 &&
          !isNaN(item.rows[0].mmfAumUsd) &&
          item.rows[0].mmfAumUsd !== 0
        ) {
          let total = 0;
          if (!isNaN(item.rows[0].ownershipAmount))
            total += Number(item.rows[0].ownershipAmount);
          if (item.rows !== undefined && item.rows.length > 0) {
            item.rows.forEach(row => {
              if (
                row.source !== undefined &&
                row.source !== SEG_IA &&
                !isNaN(row.amount)
              ) {
                total += Number(row.amount);
              }
              if (
                row.source !== undefined &&
                row.source !== SEG_IA &&
                !isNaN(row.investmentAmount)
              ) {
                total += Number(row.investmentAmount);
              }
            });
          }

          const percentage = (total / item.rows[0].mmfAumUsd) * 100;
          let className = "success";
          if (percentage >= item.rows[0].firmOwnershipThresholds.RED)
            className = "critical";
          else if (percentage >= item.rows[0].firmOwnershipThresholds.AMBER)
            className = "warning";
          if (item.rows !== undefined && item.rows.length > 0) {
            item.rows.forEach(row => {
              row.fundOwnershipClassName = className;
            });
          }
          return decimalToPercentTwoFormatterDefaultZero(
            null,
            null,
            total / item.rows[0].mmfAumUsd,
            columnDef,
            null,
            className
          );
        }
        return decimalToPercentTwoFormatterDefaultZero(
          null,
          null,
          0,
          columnDef,
          null,
          "success"
        );
      },
      name: "Fund Ownership",
      field: "fundOwnership",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "yield",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return "";
      },
      name: "Yield",
      field: "yield",
      sortable: true,
      groupTotalsFormatter: function(totals, columnDef, item, isExportToExcel) {
        if (
          item !== undefined &&
          item.rows !== undefined &&
          item.rows.length > 0
        ) {
          return twoDecimalsPercentFormatterDefaultZero(
            null,
            null,
            item.rows[0].yield,
            null,
            null
          );
        }
        return twoDecimalsPercentFormatterDefaultZero(
          null,
          null,
          null,
          null,
          null
        );
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "expenseRatio",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return "";
      },
      name: "Expense Ratio",
      field: "expenseRatio",
      sortable: true,
      groupTotalsFormatter: function(totals, columnDef, item, isExportToExcel) {
        if (
          item !== undefined &&
          item.rows !== undefined &&
          item.rows.length > 0
        ) {
          return priceFormatterDefaultZero(
            null,
            null,
            item.rows[0].expenseRatio,
            null,
            null
          );
        }
        return priceFormatterDefaultZero(null, null, null, null, null);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "wires",
      type: "text",
      formatter: textFormatter,
      name: "Wires",
      field: "wires",
      sortable: true,
      headerCssClass: "aln-rt b"
    },
    {
      id: "trades",
      type: "text",
      formatter: textFormatter,
      name: "Trades",
      field: "trades",
      sortable: true,
      headerCssClass: "aln-rt b"
    }
  ];
  return columns;
}
