import { call, put, takeEvery } from 'redux-saga/effects';
import {
  END_LOADING,
  FETCH_CASH_MANAGEMENT_WORKFLOW_DATA,
  GENERIC_MMF_BOOKING_DIALOG,
  HANDLE_EXCEPTION,
  LOG_WIRE_AND_BOOK_TRADE,
  START_LOADING,
  FETCH_MMF_CUTOFF_TIME_DATA,
  ADD_ADJUSTMENT,
  SHOW_ADD_ADJUSTMENT_ALERT
} from '../commons/constants';
import { checkForException } from '../commons/util';
import {
  hideGenericMMFBookingDialog,
  showGenericMMFBookingDialog,
  showLogWireAndBookTradeAlert,
  showLogWireAndBookTradeAlertGeneric,
  hideAddAdjustmentDialog,
  showAddAdjustmentSuccessDialog,
  clearAddAdjustmentStatus
} from './actions';
import { getCashManagementWorkflowData, getGenericMMFBookingDialogData, logWireBookTrade, getMMFCutoffTimeData, addAdjustment } from './api';

function* fetchCashManagementWorkflowData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCashManagementWorkflowData, action.payload);
    checkForException(data);
    yield put({
      type: `${FETCH_CASH_MANAGEMENT_WORKFLOW_DATA}_SUCCESS`,
      data,
      payload: action.payload
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CASH_MANAGEMENT_WORKFLOW_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}
function* logWireAndBookTradeGeneric(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(logWireBookTrade, action.payload);
    checkForException(data);
    yield put({
      type: `${LOG_WIRE_AND_BOOK_TRADE}_GENERIC_SUCCESS`,
      data,
      payload: action.payload
    });
    yield put(showLogWireAndBookTradeAlertGeneric());
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${LOG_WIRE_AND_BOOK_TRADE}_GENERIC_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}
function* logWireAndBookTrade(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(logWireBookTrade, action.payload);
    checkForException(data);
    yield put({
      type: `${LOG_WIRE_AND_BOOK_TRADE}_SUCCESS`,
      data,
      payload: action.payload
    });
    yield put(showLogWireAndBookTradeAlert());
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${LOG_WIRE_AND_BOOK_TRADE}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}
function* getGenericMMFBookingDialogDataSaga(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getGenericMMFBookingDialogData, action.payload);
    checkForException(data);
    const { isBuy } = action;
    yield put({
      type: `${GENERIC_MMF_BOOKING_DIALOG}_SUCCESS`,
      data,
      isBuy
    });
    yield put(showGenericMMFBookingDialog());
  } catch (e) {
    yield put(hideGenericMMFBookingDialog());
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${LOG_WIRE_AND_BOOK_TRADE}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* getMMFCutoffTimeDataSaga(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getMMFCutoffTimeData, action.payload);
    checkForException(data);
    yield put({
      type: `${FETCH_MMF_CUTOFF_TIME_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_MMF_CUTOFF_TIME_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* addedAdjustment(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(addAdjustment, action.payload);
    data.amount = action.payload['Adjustment.adjustmentAmountInRptCcy'];
    checkForException(data);
    if (data && _.isArray(data) && data.length > 0 && data[0].status === 'success') {
      yield put(hideAddAdjustmentDialog());
      yield put(showAddAdjustmentSuccessDialog(data[0].message));
      yield put(clearAddAdjustmentStatus());
    }
    yield put({ type: `${ADD_ADJUSTMENT}_SUCCESS`, data });
    yield put({ type: `${SHOW_ADD_ADJUSTMENT_ALERT}_OPEN` });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${ADD_ADJUSTMENT}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export default function* cashManagementWorkflowRootSaga() {
  yield [
    takeEvery(FETCH_CASH_MANAGEMENT_WORKFLOW_DATA, fetchCashManagementWorkflowData),
    takeEvery(LOG_WIRE_AND_BOOK_TRADE, logWireAndBookTrade),
    takeEvery(GENERIC_MMF_BOOKING_DIALOG, getGenericMMFBookingDialogDataSaga),
    takeEvery(`${LOG_WIRE_AND_BOOK_TRADE}_GENERIC`, logWireAndBookTradeGeneric),
    takeEvery(FETCH_MMF_CUTOFF_TIME_DATA, getMMFCutoffTimeDataSaga),
    takeEvery(ADD_ADJUSTMENT, addedAdjustment)
  ];
}
