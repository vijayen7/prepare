import queryString from 'query-string';
import {  Layout} from 'arc-react-components';
import React, { Component } from 'react';
import ExceptionDialog from '../commons/container/ExceptionDialog';
import Loader from '../commons/container/Loader';
import FundDetails from './containers/FundDetails';
import Grid from './containers/Grid';

export default class CashManagementReport extends Component {
  render() {
    const params = queryString.parse(this.props.location.search);
    return (
      <React.Fragment>
        <Loader />
        <ExceptionDialog />
        <div className="layout--flex--row">
          <arc-header

            ref={(header) => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div slot="application-menu" className="application-menu-toggle-view" />
          </arc-header>

          <Layout>
            <Layout.Child size="content" childId="child1"><FundDetails params={params} /></Layout.Child>

            <Layout.Child childId="child2"><Grid params={params} /></Layout.Child>
          </Layout>

        </div>
      </React.Fragment>
    );
  }
}
