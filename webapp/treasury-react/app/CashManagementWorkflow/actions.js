import {
  FETCH_CASH_MANAGEMENT_WORKFLOW_DATA,
  DISPLAY_ADJUSTMENT_DIALOG,
  CLOSE_ADJUSTMENT_DIALOG,
  DISPLAY_WIRE_DIALOG,
  CLOSE_WIRE_DIALOG,
  UPDATE_ADJUSTMENT_AMOUNT,
  UPDATE_WIRE_TRADE_DETAIL,
  LOG_WIRE_AND_BOOK_TRADE,
  LOGWIRE_BOOKTRADE_SELECTION_DIALOG,
  GET_ALL_MMF_SPNS,
  GENERIC_MMF_BOOKING_DIALOG,
  OVER_RIDE_INVALID_AMOUNT_CHECK,
  FETCH_MMF_CUTOFF_TIME_DATA,
  SHOW_ADD_ADJUSTMENT_DIALOG,
  ADD_ADJUSTMENT_SUCCESS_DIALOG,
  ADD_ADJUSTMENT,
  SHOW_ADD_ADJUSTMENT_ALERT
} from "../commons/constants";

export function fetchCashManagementWorkflowData(payload) {
  return {
    type: FETCH_CASH_MANAGEMENT_WORKFLOW_DATA,
    payload
  };
}

export function displayAdjustmentDialog() {
  return {
    type: DISPLAY_ADJUSTMENT_DIALOG
  };
}

export function addAdjustments(payload) {
  return {
    type: ADD_ADJUSTMENT,
    payload
  };
}

export function closeAdjustmentDialog() {
  return {
    type: CLOSE_ADJUSTMENT_DIALOG
  };
}

export function displayWireDialog() {
  return {
    type: DISPLAY_WIRE_DIALOG
  };
}
export function displayGenericMMFBookingDialog(payload, isBuy) {
  return {
    type: GENERIC_MMF_BOOKING_DIALOG,
    payload,
    isBuy
  };
}

export function getMMFCutoffTimeData(payload) {
  return {
    type: FETCH_MMF_CUTOFF_TIME_DATA,
    payload
  };
}

export function closeWireDialog() {
  return {
    type: CLOSE_WIRE_DIALOG
  };
}

export function updateAdjustmentAmount(payload) {
  return {
    type: UPDATE_ADJUSTMENT_AMOUNT,
    payload
  };
}

export function logWireAndBookTrade(payload) {
  return {
    type: LOG_WIRE_AND_BOOK_TRADE,
    payload
  };
}
export function logWireAndBookTradeGeneric(payload) {
  return {
    type: `${LOG_WIRE_AND_BOOK_TRADE}_GENERIC`,
    payload
  };
}

export function showLogWireAndBookTradeAlert() {
  return {
    type: `${LOG_WIRE_AND_BOOK_TRADE}_ALERT_SHOW`
  };
}
export function showLogWireAndBookTradeAlertGeneric() {
  return {
    type: `${LOG_WIRE_AND_BOOK_TRADE}_GENERIC_ALERT_SHOW`
  };
}
export function hideGenericLogWireAndBookTradeAlert() {
  return {
    type: `${LOG_WIRE_AND_BOOK_TRADE}_GENERIC_ALERT_HIDE`
  };
}
export function showGenericMMFBookingDialog() {
  return {
    type: `${GENERIC_MMF_BOOKING_DIALOG}_SHOW`
  };
}
export function hideGenericMMFBookingDialog() {
  return {
    type: `${GENERIC_MMF_BOOKING_DIALOG}_HIDE`
  };
}

export function hideLogWireAndBookTradeAlert() {
  return {
    type: `${LOG_WIRE_AND_BOOK_TRADE}_ALERT_HIDE`
  };
}

export function updateWireTradeDetail(payload) {
  return {
    type: UPDATE_WIRE_TRADE_DETAIL,
    payload
  };
}
export function showLogWireBookTradeSelectionDialog(payload) {
  return {
    type: `${LOGWIRE_BOOKTRADE_SELECTION_DIALOG}_SHOW`,
    payload
  };
}
export function hideLogWireBookTradeSelectionDialog(payload) {
  return {
    type: `${LOGWIRE_BOOKTRADE_SELECTION_DIALOG}_HIDE`,
    payload
  };
}
export function showGenericLogWireBookTradeSelectionDialog(payload) {
  return {
    type: `${LOGWIRE_BOOKTRADE_SELECTION_DIALOG}_GENERIC_SHOW`,
    payload
  };
}
export function hideGenericLogWireBookTradeSelectionDialog(payload) {
  return {
    type: `${LOGWIRE_BOOKTRADE_SELECTION_DIALOG}_GENERIC_HIDE`,
    payload
  };
}
export const getAllMMFSpns = () => {
  return {
    type: `${GET_ALL_MMF_SPNS}`
  };
};

export function overRideInvalidAmountCheck() {
  return {
    type: OVER_RIDE_INVALID_AMOUNT_CHECK
  };
}

export function dontOverRideInvalidAmountCheck() {
  return {
    type: `DONT_${OVER_RIDE_INVALID_AMOUNT_CHECK}`
  };
}

export function hideAddAdjustmentDialog() {
  return {
    type: `${SHOW_ADD_ADJUSTMENT_DIALOG}_CLOSE`
  };
}

export function showAddAdjustmentSuccessDialog(payload) {
  return {
    type: `${ADD_ADJUSTMENT_SUCCESS_DIALOG}_SHOW`,
    payload
  };
}

export function clearAddAdjustmentStatus() {
  return {
    type: `${ADD_ADJUSTMENT}_CLEAR_MESSAGE`
  };
}

export function hideAddAdjustmentAlert() {
  return {
    type: `${SHOW_ADD_ADJUSTMENT_ALERT}_CLOSE`
  };
}
