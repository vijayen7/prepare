import { BASE_URL } from '../commons/constants';
const queryString = require('query-string');
export function getCashManagementWorkflowData(payload) {
  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}service/cashManagementLiquidityService/getLiquidityProfile?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function logWireBookTrade(payload) {
  const paramString = encodeURI(JSON.stringify(payload));
  const url = `${BASE_URL}service/cashManagementLiquidityService/logWireAndBookTrade?inputFormat=JSON&format=JSON&logWireAndBookTradeFilter=${paramString}`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getGenericMMFBookingDialogData(payload) {
  const url = `${BASE_URL}service/cashManagementLiquidityService/getMMFBookingDetailsForEntityFamily?entityFamilyId=${payload}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getMMFCutoffTimeData(payload) {
  const url = `${BASE_URL}service/cashManagementLiquidityService/getMMFCutoffTimeList?mmfSpnList=${payload}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function addAdjustment(payload) {
  const paramString = queryString.stringify(payload);
  const saveAdjustmentsExt = 'service/lcmAdjustmentService/saveAdjustments?inputFormat=PROPERTIES&';
  const url = `${BASE_URL}${saveAdjustmentsExt}${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}
