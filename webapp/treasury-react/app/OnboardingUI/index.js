import React, { Component } from "react";
import Loader from "commons/container/Loader";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";
import { hot } from "react-hot-loader/root";
import Summary from "./Summary";
import Widget from "./Summary/Widget";
import { connect } from "react-redux";
import { resetOnboardingUISearchMessage, setView } from "./actions";
import { bindActionCreators } from "redux";
import { onboardingScreen } from "./util";
import { Layout } from "arc-react-components";

class OnboardingUI extends Component {
  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = { view: onboardingScreen.CALCULATOR };
  }

  componentDidMount() {
    this.calculatorGroup.classList.add("active");
  }

  handleToggle(view) {
    this.calculatorGroup.classList.remove("active");
    this.financingAgreementTerms.classList.remove("active");
    this.assetClassGroup.classList.remove("active");
    this.reportingCurrency.classList.remove("active");
    this.accrualPosting.classList.remove("active");
    this.nettingGroupMapping.classList.remove("active");
    this.buBundleMapping.classList.remove("active");
    this.genevaBookMapping.classList.remove("active");
    this.summary.classList.remove("active");
    this.widget.classList.remove("active");
    this.adjustment.classList.remove("active");
    this.reconciliationThresholds.classList.remove('active');

    switch (view) {
      case onboardingScreen.CALCULATOR:
        this.calculatorGroup.classList.add("active");
        break;
      case onboardingScreen.FINANCING:
        this.financingAgreementTerms.classList.add("active");
        break;
      case onboardingScreen.ASSET:
        this.assetClassGroup.classList.add("active");
        break;
      case onboardingScreen.REPORTING:
        this.reportingCurrency.classList.add("active");
        break;
      case onboardingScreen.ACCRUAL:
        this.accrualPosting.classList.add("active");
        break;
      case onboardingScreen.NETTING:
        this.nettingGroupMapping.classList.add("active");
        break;
      case onboardingScreen.BUBUNDLE:
        this.buBundleMapping.classList.add("active");
        break;
      case onboardingScreen.GENEVABOOK:
        this.genevaBookMapping.classList.add("active");
        break;
      case onboardingScreen.SUMMARY:
        this.summary.classList.add("active");
        break;
      case onboardingScreen.WIDGET:
        this.widget.classList.add("active");
        break;
      case onboardingScreen.ADJUSTMENT:
        this.adjustment.classList.add("active");
        break;
      case onboardingScreen.RECONCILIATION_THRESHOLDS:
        this.reconciliationThresholds.classList.add('active');
        break;
      default:
        this.calculatorGroup.classList.add("active");
    }
    this.setState({ view });
    this.props.setView(view);
    this.props.resetOnboardingUISearchMessage();
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div
              slot="application-menu"
              className="application-menu-toggle-view"
            >

              <a
                ref={calculatorGroup => {
                  this.calculatorGroup = calculatorGroup;
                }}
                onClick={() => this.handleToggle(onboardingScreen.CALCULATOR)}
              >
                Calculator Group
              </a>
              <a
                ref={financingAgreementTerms => {
                  this.financingAgreementTerms = financingAgreementTerms;
                }}
                onClick={() => this.handleToggle(onboardingScreen.FINANCING)}
              >
                Financing Agreement Terms
              </a>
              <a
                ref={assetClassGroup => {
                  this.assetClassGroup = assetClassGroup;
                }}
                onClick={() => this.handleToggle(onboardingScreen.ASSET)}
              >
                Asset Class Group
              </a>
              <a
                ref={reportingCurrency => {
                  this.reportingCurrency = reportingCurrency;
                }}
                onClick={() => this.handleToggle(onboardingScreen.REPORTING)}
              >
                Accrual Posting Currency
              </a>
              <a
                ref={accrualPosting => {
                  this.accrualPosting = accrualPosting;
                }}
                onClick={() => this.handleToggle(onboardingScreen.ACCRUAL)}
              >
                Accrual Posting
              </a>
              <a
                ref={buBundleMapping => {
                  this.buBundleMapping = buBundleMapping;
                }}
                onClick={() => this.handleToggle(onboardingScreen.BUBUNDLE)}
              >
                BU Bundle Mapping
              </a>
              <a
                ref={genevaBookMapping => {
                  this.genevaBookMapping = genevaBookMapping;
                }}
                onClick={() => this.handleToggle(onboardingScreen.GENEVABOOK)}
              >
                Accounting Book Mapping
              </a>
              <a
                ref={nettingGroupMapping => {
                  this.nettingGroupMapping = nettingGroupMapping;
                }}
                onClick={() => this.handleToggle(onboardingScreen.NETTING)}
              >
                Netting Group Mapping
              </a>
              <a
                ref={widget => {
                  this.widget = widget;
                }}
                onClick={() => this.handleToggle(onboardingScreen.WIDGET)}
              >
                Widget
              </a>
              <a
                ref={summary => {
                  this.summary = summary;
                }}
                onClick={() => this.handleToggle(onboardingScreen.SUMMARY)}
              >
                Summary
              </a>
              <a
                ref={adjustment => {
                  this.adjustment = adjustment;
                }}
                onClick={() => this.handleToggle(onboardingScreen.ADJUSTMENT)}
              >
                Adjustment Allocation
              </a>
              <a
                ref={reconciliationThresholds => {
                  this.reconciliationThresholds = reconciliationThresholds;
                }}
                onClick={() => this.handleToggle(onboardingScreen.RECONCILIATION_THRESHOLDS)}
              >
                Reconciliation Thresholds
              </a>
            </div>
          </arc-header>
          {this.state.view == onboardingScreen.SUMMARY ? (
            <Summary />
          ) : (this.state.view == onboardingScreen.WIDGET ? <Widget /> :
            <Layout isColumnType>
              <Layout.Child childId="OnboardingUIIndex1" size={2}>
                <SideBar view={this.state.view} />
              </Layout.Child>
              <Layout.Divider childId="OnboardingUIDivider1" />
              <Layout.Child childId="OnboardingUIIndex2" size={0.1} />
              <Layout.Child childId="OnboardingUIIndex3" size={12}>
                <Grid
                  view={this.state.view}
                  handleViewToggle={this.handleToggle}
                />
              </Layout.Child>
            </Layout>
            )}
        </div>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetOnboardingUISearchMessage,
      setView
    },
    dispatch
  );
}

export default hot(
  connect(
    null,
    mapDispatchToProps
  )(OnboardingUI)
);
