import {
  getCalculatorGroupColumns,
  getCalculatorGroupSimulatedColumns
} from "./ColumnConfig/calculatorGroupColumnConfig";
import {
  getFinancingAgreementTermsColumns,
  getFinancingCommonColumns
} from "./ColumnConfig/financingAgreementTermsColumnConfig";
import {
  getAssetClassSearchColumns,
  getAllAssetClassColumns,
  getExistingAssetClassColumns,
  getAssetClassConditionColumns
} from "./ColumnConfig/assetClassGroupColumnConfig";
import {
  getReportingCurrencyColumns,
  getReportingCurrencySimulatedColumns
} from "./ColumnConfig/reportingCurrencyColumnConfig";
import {
  getAccrualPostingColumns,
  getCommonColumns
} from "./ColumnConfig/accrualPostingColumnConfig";
import {
  getNettingGroupMappingColumns,
  getNettingGroupSimulatedColumns
} from "./ColumnConfig/nettingGroupMappingColumnConfig";
import {
  getBuBundleMappingColumns,
  getBuBundleMappingSimulatedColumns
} from "./ColumnConfig/buBundleMappingColumnConfig";
import {
  getGenevaBookMappingColumns,
  getGenevaBookMappingSimulatedColumns
} from "./ColumnConfig/genevaBookMappingColumnConfig";
import {
  getAdjustmentAllocationRuleColumns,
  getAdjustmentAllocationRuleSimulatedColumns
} from "./ColumnConfig/adjustmentAllocationRuleColumnConfig";
import {
  getReconciliationThresholdsColumns
} from "./ColumnConfig/reconciliationThresholdsColumnConfig";

import { onboardingScreen } from "../util";

export function getColumnConfig(view, isUserAuthorizedForInterestEdit) {
  let columnConfig = [];
  switch (view) {
    case onboardingScreen.CALCULATOR:
      columnConfig = getCalculatorGroupColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.FINANCING:
      columnConfig = getFinancingAgreementTermsColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.ASSET:
      columnConfig = getAssetClassSearchColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.REPORTING:
      columnConfig = getReportingCurrencyColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.ACCRUAL:
      columnConfig = getAccrualPostingColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.NETTING:
      columnConfig = getNettingGroupMappingColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.BUBUNDLE:
      columnConfig = getBuBundleMappingColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.GENEVABOOK:
      columnConfig = getGenevaBookMappingColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.ADJUSTMENT:
      columnConfig = getAdjustmentAllocationRuleColumns(isUserAuthorizedForInterestEdit);
      break;
    case onboardingScreen.RECONCILIATION_THRESHOLDS:
      columnConfig = getReconciliationThresholdsColumns(isUserAuthorizedForInterestEdit);
      break;
    default:
      break;
  }
  return columnConfig;
}

export function getSimulatedColumnConfig(view) {
  let columnConfig = [];
  switch (view) {
    case onboardingScreen.CALCULATOR:
      columnConfig = getCalculatorGroupSimulatedColumns();
      break;
    case onboardingScreen.FINANCING:
      columnConfig = getFinancingCommonColumns();
      break;
    case onboardingScreen.REPORTING:
      columnConfig = getReportingCurrencySimulatedColumns();
      break;
    case onboardingScreen.ACCRUAL:
      columnConfig = getCommonColumns();
      break;
    case onboardingScreen.NETTING:
      columnConfig = getNettingGroupSimulatedColumns();
      break;
    case onboardingScreen.BUBUNDLE:
      columnConfig = getBuBundleMappingSimulatedColumns();
      break;
    case onboardingScreen.GENEVABOOK:
      columnConfig = getGenevaBookMappingSimulatedColumns();
      break;
    case onboardingScreen.ADJUSTMENT:
      columnConfig = getAdjustmentAllocationRuleSimulatedColumns();
      break;
    default:
      break;
  }
  return columnConfig;
}

export {
  getAllAssetClassColumns,
  getExistingAssetClassColumns,
  getAssetClassConditionColumns
};
