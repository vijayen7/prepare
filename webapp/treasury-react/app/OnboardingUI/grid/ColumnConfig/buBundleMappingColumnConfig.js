import { linkFormatter } from "commons/grid/formatters";

export function getBuBundleMappingColumns(isUserAuthorizedForInterestEdit) {
  let columns = actionColumns(isUserAuthorizedForInterestEdit);
  columns = columns.concat(commonColumns());
  return columns;
}

export function getBuBundleMappingSimulatedColumns() {
  const columns = commonColumns();
  return columns;
}

function actionColumns(isUserAuthorizedForInterestEdit) {
  const columns = [
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return isUserAuthorizedForInterestEdit?(
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        ):null;
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    }
  ];

  return columns;
}

function commonColumns() {
  const columns = [
    {
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreementType",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 100,
      excelFormatter: "#,##0"
    },
    {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legalEntity",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 100,
      excelFormatter: "#,##0"
    },
    {
      id: "counterPartyEntity",
      type: "text",
      name: "Counterparty",
      field: "cpe",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 100,
      excelFormatter: "#,##0"
    },
    {
      id: "currency",
      type: "text",
      name: "Currency",
      field: "ccy",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 100,
      excelFormatter: "#,##0"
    },
    {
      id: "nettingGroup",
      type: "text",
      name: "Netting Group",
      field: "nettingGroup",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "businessUnit",
      type: "text",
      name: "Business Unit",
      field: "businessUnit",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "bundle",
      type: "text",
      name: "Bundle",
      field: "bundle",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "validFrom",
      type: "text",
      name: "Valid From",
      field: "effective_start_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 90,
      excelFormatter: "#,##0"
    },
    {
      id: "validTill",
      type: "text",
      name: "Valid Till",
      field: "effective_end_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter(row, cell, value, columnDef, dataContext) {
        if (value === '20380101') {
          return '-';
        }
        return value;
      },
      width: 70,
      excelFormatter: "#,##0"
    }
  ];

  return columns;
}
