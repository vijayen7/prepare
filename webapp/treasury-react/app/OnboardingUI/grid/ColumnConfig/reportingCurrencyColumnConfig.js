
function getActionColumn(isUserAuthorizedForInterestEdit) {
  return [
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return isUserAuthorizedForInterestEdit?(
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        ):null;
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    }
  ];
}

function getCommonColumns() {
  return [{
    id: 'agreementType',
    type: 'text',
    name: 'Agreement Type',
    field: 'agreementType',
    sortable: true,
    headerCssClass: 'aln-rt b',
    width: 80,
    excelFormatter: '#,##0'
  }, {
    id: 'legalEntity',
    type: 'text',
    name: 'Legal Entity',
    field: 'legalEntity',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  }, {
    id: 'counterPartyEntity',
    type: 'text',
    name: 'Counterparty',
    field: 'cpe',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  }, {
    id: 'currency',
    type: 'text',
    name: 'Currency',
    field: 'ccy',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  }, {
    id: 'nettingGroup',
    type: 'text',
    name: 'Netting Group',
    field: 'nettingGroup',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  }, {
    id: 'reportingCurrency',
    type: 'text',
    name: 'Accrual Posting Currency',
    field: 'reportingCurrency',
    sortable: true,
    headerCssClass: 'aln-rt b',
    width: 120,
    excelFormatter: '#,##0'
  }, {
    id: 'validFrom',
    type: 'text',
    name: 'Valid From',
    field: 'effective_start_date',
    sortable: true,
    headerCssClass: 'aln-rt b',
    width: 80,
    excelFormatter: '#,##0'
  }, {
    id: 'validTill',
    type: 'text',
    name: 'Valid Till',
    field: 'effective_end_date',
    sortable: true,
    headerCssClass: 'aln-rt b',
    formatter(row, cell, value, columnDef, dataContext) {
      if (value === '20380101') {
        return '-';
      }
      return value;
    },
    width: 60,
    excelFormatter: '#,##0'
  }];
}


export function getReportingCurrencyColumns(isUserAuthorizedForInterestEdit) {
  let columns = getActionColumn(isUserAuthorizedForInterestEdit);
  columns = columns.concat(getCommonColumns());
  return columns;
}

export function getReportingCurrencySimulatedColumns() {
  const columns = getCommonColumns();
  return columns;
}
