import { numberComparator, textComparator } from 'commons/grid/comparators';
import { numberFormat } from 'commons/grid/formatters';
import { getColourCode } from './../../util';

export function getFinancingCommonColumns() {
  const columns = [
    {
      id: 'baseRateCurrency',
      type: 'text',
      name: 'Base Rate Currency',
      field: 'financingTerm',
      filter: {
        isFilterOnFormattedValue: true
      },
      formatter: function(row, cell, value, columnDef, dataContext) {
        var returnValue = '';
        if (typeof value !== 'undefined' && typeof value.baseRateCurrencyName !== 'undefined') {
          returnValue = value.baseRateCurrencyName;
        }
        return returnValue;
      },
      comparator: function(a, b) {
        var aValue = '';
        var bValue = '';
        if (
          typeof a.financingTerm !== 'undefined' &&
          typeof a.financingTerm.baseRateCurrencyName !== 'undefined'
        ) {
          aValue = a.financingTerm.baseRateCurrencyName.myAbbreviation;
        }
        if (
          typeof b.financingTerm !== 'undefined' &&
          typeof b.financingTerm.baseRateCurrencyName !== 'undefined'
        ) {
          bValue = b.financingTerm.baseRateCurrencyName.myAbbreviation;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter: function(row, cell) {
        return cell.id;
      },
      excelDataFormatter: function(value) {
        var returnValue = '';
        if (typeof value !== 'undefined' && typeof value.baseRateCurrencyName !== 'undefined') {
          returnValue = value.baseRateCurrencyName;
        }
        return returnValue;
      },
      width: 60,
      excelFormatter: '#,##0'
    },
    {
      id: 'financingAgreementTermType',
      type: 'text',
      name: 'Financing Term Type',
      field: 'financingAgreementTermType',
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value !== 'undefined' && value !== null) {
          returnValue = getColourCode(value, 150, 250);
        }
        return returnValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value !== 'undefined' && value !== null) {
          returnValue = value;
        }
        return returnValue;
      },
      minWidth: 150,
      maxWidth: 250
    },
    {
      id: 'creditBaseRate',
      type: 'text',
      name: 'Credit Base Rate',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value.creditBaseRateTs !== 'undefined' && value.creditBaseRateTs !== null) {
          returnValue = value.creditBaseRateTs.myName;
        }
        return returnValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (
          typeof a.financingTerm !== 'undefined' &&
          typeof a.financingTerm.creditBaseRateTs !== 'undefined'
        ) {
          aValue = a.financingTerm.creditBaseRateTs.myName;
        }
        if (
          typeof b.financingTerm !== 'undefined' &&
          typeof b.financingTerm.creditBaseRateTs !== 'undefined'
        ) {
          bValue = b.financingTerm.creditBaseRateTs.myName;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value.creditBaseRateTs !== 'undefined' && value.creditBaseRateTs !== null) {
          returnValue = value.creditBaseRateTs.myName;
        }
        return returnValue;
      },
      excelFormatter: '#,##0'
    },
    {
      id: 'debitBaseRate',
      type: 'text',
      name: 'Debit Base Rate',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value.debitBaseRateTs !== 'undefined' && value.debitBaseRateTs !== null) {
          returnValue = value.debitBaseRateTs.myName;
        }
        return returnValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (
          typeof a.financingTerm !== 'undefined' &&
          typeof a.financingTerm.debitBaseRateTs !== 'undefined'
        ) {
          aValue = a.financingTerm.debitBaseRateTs.myName;
        }
        if (
          typeof b.financingTerm !== 'undefined' &&
          typeof b.financingTerm.debitBaseRateTs !== 'undefined'
        ) {
          bValue = b.financingTerm.debitBaseRateTs.myName;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value.debitBaseRateTs !== 'undefined' && value.debitBaseRateTs !== null) {
          returnValue = value.debitBaseRateTs.myName;
        }
        return returnValue;
      },
      excelFormatter: '#,##0'
    },
    {
      id: 'rebateBaseRate',
      type: 'text',
      name: 'Rebate Base Rate',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value.rebateBaseRateTs !== 'undefined' && value.rebateBaseRateTs !== null) {
          returnValue = value.rebateBaseRateTs.myName;
        }
        return returnValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (
          typeof a.financingTerm !== 'undefined' &&
          typeof a.financingTerm.rebateBaseRateTs !== 'undefined' &&
          a.financingTerm.rebateBaseRateTs !== null
        ) {
          aValue = a.financingTerm.rebateBaseRateTs.myName;
        }
        if (
          typeof b.financingTerm !== 'undefined' &&
          typeof b.financingTerm.rebateBaseRateTs !== 'undefined' &&
          b.financingTerm.rebateBaseRateTs !== null
        ) {
          bValue = b.financingTerm.rebateBaseRateTs.myName;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value.rebateBaseRateTs !== 'undefined' && value.rebateBaseRateTs !== null) {
          returnValue = value.rebateBaseRateTs.myName;
        }
        return returnValue;
      },
      excelFormatter: '#,##0'
    },
    {
      id: 'marginFactor',
      type: 'text',
      name: 'Margin Factor',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Margin Factor') {
              attributeValue = entry.rvalue;
            }
          });
        }
        return attributeValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
	   let aValue,bValue;
	   if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
       else{
         aValue = _getAttr(a, 'Margin Factor', 'rvalue') * 100;
         bValue = _getAttr(b, 'Margin Factor', 'rvalue') * 100;
		 }
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Margin Factor') {
              attributeValue = entry.rvalue;
            }
          });
        }
        return attributeValue;
      },
      width: 100
    },
    {
      id: 'creditSpread',
      type: 'text',
      name: 'Credit Spread(bps)',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        return numberFormat(value.creditSpread * 100,3);
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (
          typeof a.financingTerm !== 'undefined' &&
          typeof a.financingTerm.creditSpread !== 'undefined'
        ) {
          aValue = a.financingTerm.creditSpread * 100;
        }
        if (
          typeof b.financingTerm !== 'undefined' &&
          typeof b.financingTerm.creditSpread !== 'undefined'
        ) {
          bValue = b.financingTerm.creditSpread * 100;
        }
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return numberFormat(value.creditSpread,3);
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      excelFormatter: '#,##0',
      width: 100
    },
    {
      id: 'rebateSpread',
      type: 'text',
      name: 'Rebate Spread(bps)',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let rebateSpread = _getAttrFormatter(value, 'Rebate Spread', 'rvalue', 0) * 100
        return numberFormat(rebateSpread, 3)
      },
      comparator(a, b) {
        let aValue = _getAttr(a, 'Rebate Spread', 'rvalue') * 100;
        let bValue = _getAttr(b, 'Rebate Spread', 'rvalue') * 100;
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return _getAttrFormatter(value, 'Rebate Spread', 'rvalue', 0) * 100;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      excelFormatter: '#,##0',
      width: 100
    },
    {
      id: 'debitSpread',
      type: 'text',
      name: 'Debit Spread(bps)',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        return numberFormat(value.debitSpread * 100, 3);
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (
          typeof a.financingTerm !== 'undefined' &&
          typeof a.financingTerm.debitSpread !== 'undefined'
        ) {
          aValue = a.financingTerm.debitSpread * 100;
        }
        if (
          typeof b.financingTerm !== 'undefined' &&
          typeof b.financingTerm.debitSpread !== 'undefined'
        ) {
          bValue = b.financingTerm.debitSpread * 100;
        }
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return numberFormat(value.debitSpread,3);
      },
      width: 100
    },
    {
      id: 'bandwidthGroup',
      type: 'text',
      name: 'Bandwidth Group',
      field: 'bandwidthGroup',
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value !== 'undefined' && value !== null) {
          returnValue = value.displayName;
        }
        return returnValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.bandwidthGroup !== 'undefined' && a.bandwidthGroup !== null) {
          aValue = a.bandwidthGroup.displayName;
        }
        if (typeof b.bandwidthGroup !== 'undefined' && b.bandwidthGroup !== null) {
          bValue = b.bandwidthGroup.displayName;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value !== 'undefined' && value !== null) {
          returnValue = value.displayName;
        }
        return returnValue;
      },
      excelFormatter: '#,##0'
    },
    {
      id: 'gcRate',
      type: 'text',
      name: 'GC Rate(bps)',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        return value.gcRate * 100;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.financingTerm !== 'undefined') {
          aValue = a.financingTerm.gcRate * 100;
        }
        if (typeof b.financingTerm !== 'undefined') {
          bValue = b.financingTerm.gcRate * 100;
        }
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return value.gcRate;
      },
      width: 100
    },
    {
      id: 'creditAccrualConvention',
      type: 'text',
      name: 'Credit Accrual Convention',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        return value.creditAccrualConvention;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.financingTerm !== 'undefined') {
          aValue = a.financingTerm.creditAccrualConvention;
        }
        if (typeof b.financingTerm !== 'undefined') {
          bValue = b.financingTerm.creditAccrualConvention;
        }
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return value.creditAccrualConvention;
      },
      width: 100
    },
    {
      id: 'debitAccrualConvention',
      type: 'text',
      name: 'Debit Accrual Convention',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        return value.debitAccrualConvention;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.financingTerm !== 'undefined') {
          aValue = a.financingTerm.debitAccrualConvention;
        }
        if (typeof b.financingTerm !== 'undefined') {
          bValue = b.financingTerm.debitAccrualConvention;
        }
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return value.debitAccrualConvention;
      },
      width: 100
    },
    {
      id: 'ApplicableNegativeCreditIrate',
      type: 'text',
      name: 'Applicable Negative Credit Irate',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Applicable Negative Credit Irate') {
              attributeValue = entry.attributeValueName;
            }
          });
        }
        return attributeValue;
      },
      comparator(a, b) {
	    let aValue,bValue;
	   if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
       else{
         aValue = _getAttr(a, 'Applicable Negative Credit Irate', 'attributeValueName');
         bValue = _getAttr(b, 'Applicable Negative Credit Irate', 'attributeValueName');
		 }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      filter: {
        isFilterOnFormattedValue: true
      },
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Applicable Negative Credit Irate') {
              attributeValue = entry.attributeValueName;
            }
          });
        }
        return attributeValue;
      },
      width: 160
    },
    {
      id: 'SmvConversionFactor',
      type: 'text',
      name: 'SMV Conversion Factor',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'SMV Conversion Factor') {
              attributeValue = entry.rvalue;
            }
          });
        }
        return attributeValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
	    let aValue,bValue;
	   if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
       else{
         aValue = _getAttr(a, 'SMV Conversion Factor', 'rvalue');
         bValue = _getAttr(b, 'SMV Conversion Factor', 'rvalue');
		}
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'SMV Conversion Factor') {
              attributeValue = entry.rvalue;
            }
          });
        }
        return attributeValue;
      },
      width: 100
    },
    {
      id: 'ApplicableNegativeDebitIrate',
      type: 'text',
      name: 'Applicable Negative Debit Irate',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Applicable Negative Debit Irate') {
              attributeValue = entry.attributeValueName;
            }
          });
        }
        return attributeValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
	    let aValue,bValue;
	   if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
       else{
         aValue = _getAttr(a, 'Applicable Negative Debit Irate', 'attributeValueName');
         bValue = _getAttr(b, 'Applicable Negative Debit Irate', 'attributeValueName');
		}
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Applicable Negative Debit Irate') {
              attributeValue = entry.attributeValueName;
            }
          });
        }
        return attributeValue;
      },
      width: 160
    },
    {
      id: 'ApplicableNegativeRateSfic',
      type: 'text',
      name: 'Applicable Negative Irate Sfic',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        return _getAttrFormatter(value, 'Applicable Negative Irate Sfic', 'attributeValueName', "")
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
	    let aValue,bValue;
	   if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
       else{
         aValue = _getAttr(a, 'Applicable Negative Irate Sfic', 'attributeValueName');
         bValue = _getAttr(b, 'Applicable Negative Irate Sfic', 'attributeValueName');
		}
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return _getAttrFormatter(value, 'Applicable Negative Irate Sfic', 'attributeValueName', "");
      },
      width: 100
    },
    {
      id: 'Haircut',
      type: 'text',
      name: 'Haircut',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Haircut') {
              attributeValue = entry.rvalue;
            }
          });
        }
        return attributeValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
	    let aValue,bValue;
	   if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
       else{
         aValue = _getAttr(a, 'Haircut', 'rvalue');
         bValue = _getAttr(b, 'Haircut', 'rvalue');
		}
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Haircut') {
              attributeValue = entry.rvalue;
            }
          });
        }
        return attributeValue;
      }
    },
    {
      id: 'financingStyle',
      type: 'text',
      name: 'Financing Style',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Financing Style Id') {
              attributeValue = entry.attributeValueName;
            }
          });
        }
        return attributeValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
	    let aValue,bValue;
	   if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
       else{
         aValue = _getAttr(a, 'Financing Style Id', 'attributeValueName');
         bValue = _getAttr(b, 'Financing Style Id', 'attributeValueName');
		 }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'Financing Style Id') {
              attributeValue = entry.attributeValueName;
            }
          });
        }
        return attributeValue;
      }
    },
    {
      id: 'GCShortSpread',
      type: 'text',
      name: 'GC Short Spread(bps)',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'GC Short Spread') {
              attributeValue = entry.rvalue * 100;
            }
          });
        }
        return attributeValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
	   let aValue,bValue;
	   if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
       else{
         aValue = _getAttr(a, 'GC Short Spread', 'rvalue') * 100;
         bValue = _getAttr(b, 'GC Short Spread', 'rvalue') * 100;
		 }
        return numberComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let attributeValue = '';
        if (typeof value.attributeList !== 'undefined' && value.attributeList !== null) {
          value.attributeList.forEach((entry) => {
            if (entry.name === 'GC Short Spread') {
              attributeValue = entry.rvalue;
            }
          });
        }
        return attributeValue;
      },
      width: 100
    },{
      id: 'leanSurchargeModel',
      type: 'text',
      name: 'Lean Surcharge Model',
      field: 'financingTerm',
      formatter(row, cell, value, columnDef, dataContext) {
        let formattedValue = _getAttrFormatter(value, 'Lean Surcharge Model Id', 'attributeValueName', "")
        return (typeof formattedValue !== "undefined" && formattedValue !== null)
        ?  `<a>${formattedValue}</a>` : formattedValue;
      },
      comparator(a, b) {
        let aValue = a
        let bValue = b
        if(typeof a !== "string" && typeof b !== "string") {
          aValue = _getAttr(a, 'Lean Surcharge Model Id', 'attributeValueName');
          bValue = _getAttr(b, 'Lean Surcharge Model Id', 'attributeValueName');
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return _getAttrFormatter(value, 'Lean Surcharge Model Id', 'attributeValueName', "");
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      excelFormatter: '#,##0',
      width: 100
    }
  ];

  return columns;
}

function getKeyColumns(isUserAuthorizedForInterestEdit) {
  return [
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return isUserAuthorizedForInterestEdit?(
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        ):null;
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    },
    {
      id: 'agreementType',
      type: 'text',
      name: 'Agreement Type',
      field: 'financingMethodology',
      filter: {
        isFilterOnFormattedValue: true
      },
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        returnValue = value.agreementType;
        return returnValue;
      },
      sortable: true,
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.financingMethodology !== 'undefined') {
          aValue = a.financingMethodology.agreementType;
        }
        if (typeof b.financingMethodology !== 'undefined') {
          bValue = b.financingMethodology.agreementType;
        }
        return textComparator(aValue, bValue);
      },
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return value.agreementType;
      },
      excelFormatter: '#,##0',
      width: 165
    },
    {
      id: 'legalEntity',
      type: 'text',
      name: 'Legal Entity',
      field: 'financingMethodology',
      filter: {
        isFilterOnFormattedValue: true
      },
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        returnValue = value.legalEntity;
        return returnValue;
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.financingMethodology !== 'undefined') {
          aValue = a.financingMethodology.legalEntity;
        }
        if (typeof b.financingMethodology !== 'undefined') {
          bValue = b.financingMethodology.legalEntity;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return value.legalEntity;
      },
      width: 225,
      excelFormatter: '#,##0'
    },
    {
      id: 'cpe',
      type: 'text',
      name: 'Counter Party Entity',
      field: 'financingMethodology',
      filter: {
        isFilterOnFormattedValue: true
      },
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        returnValue = value.cpe;
        return returnValue;
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.financingMethodology !== 'undefined') {
          aValue = a.financingMethodology.cpe;
        }
        if (typeof b.financingMethodology !== 'undefined') {
          bValue = b.financingMethodology.cpe;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        return value.cpe;
      },
      width: 175,
      excelFormatter: '#,##0'
    },
    {
      id: 'currency',
      type: 'text',
      name: 'Currency',
      field: 'financingMethodology',
      filter: {
        isFilterOnFormattedValue: true
      },
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value !== 'undefined' && typeof value.currency !== 'undefined') {
          returnValue = value.currency.myAbbreviation;
        }
        return returnValue;
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (
          typeof a.financingMethodology !== 'undefined' &&
          typeof a.financingMethodology.currency !== 'undefined'
        ) {
          aValue = a.financingMethodology.currency.myAbbreviation;
        }
        if (
          typeof b.financingMethodology !== 'undefined' &&
          typeof b.financingMethodology.currency !== 'undefined'
        ) {
          bValue = b.financingMethodology.currency.myAbbreviation;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      width: 55,
      headerCssClass: 'aln-rt b',

      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value !== 'undefined' && typeof value.currency !== 'undefined') {
          returnValue = value.currency.myAbbreviation;
        }
        return returnValue;
      },
      excelFormatter: '#,##0'
    },
    {
      id: 'nettingGroup',
      type: 'text',
      name: 'Netting Group',
      field: 'financingMethodology',
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value !== 'undefined' && typeof value.nettingGroup !== 'undefined') {
          returnValue = value.nettingGroup.myDescription;
        }
        return returnValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (
          typeof a.financingMethodology !== 'undefined' &&
          typeof a.financingMethodology.nettingGroup !== 'undefined'
        ) {
          aValue = a.financingMethodology.nettingGroup.myDescription;
        }
        if (
          typeof b.financingMethodology !== 'undefined' &&
          typeof b.financingMethodology.nettingGroup !== 'undefined'
        ) {
          bValue = b.financingMethodology.nettingGroup.myDescription;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value !== 'undefined' && typeof value.nettingGroup !== 'undefined') {
          returnValue = value.nettingGroup.myDescription;
        }
        return returnValue;
      },
      width: 250
    },
    {
      id: 'assetClassGroup',
      type: 'text',
      name: 'Asset Class Group',
      field: 'assetClassGroup',
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value !== 'undefined' && value !== null) {
          returnValue = value.displayName;
        }
        return returnValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.assetClassGroup !== 'undefined' && a.assetClassGroup !== null) {
          aValue = a.assetClassGroup.displayName;
        }
        if (typeof b.assetClassGroup !== 'undefined' && b.assetClassGroup !== null) {
          bValue = b.assetClassGroup.displayName;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value !== 'undefined' && value !== null) {
          returnValue = value.displayName;
        }
        return returnValue;
      },
      excelFormatter: '#,##0'
    }
  ];
}

function getEffectiveDateColumns() {
  const columns = [
    {
      id: 'validFrom',
      type: 'text',
      name: 'Valid Start',
      field: 'validStart',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      }
    },
    {
      id: 'validEnd',
      type: 'text',
      name: 'Valid End',
      field: 'validEnd',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      }
    }
  ];

  return columns;
}

export function getAccountLevelColumns() {
  const columns = [
    {
      id: 'custodianAccount',
      type: 'text',
      name: 'Custodian Account',
      field: 'custodianAccountName',
      formatter(row, cell, value, columnDef, dataContext) {
        let returnValue = '';
        if (typeof value !== 'undefined' && value !== null) {
          returnValue = value;
        }
        return returnValue;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator(a, b) {
        let aValue = '';
        let bValue = '';
        if (typeof a.custodianAccountName !== 'undefined' && a.custodianAccountName !== null) {
          aValue = a.custodianAccountName;
        }
        if (typeof b.custodianAccountName !== 'undefined' && b.custodianAccountName !== null) {
          bValue = b.custodianAccountName;
        }
        return textComparator(aValue, bValue);
      },
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelHeaderFormatter(row, cell) {
        return cell.id;
      },
      excelDataFormatter(value) {
        let returnValue = '';
        if (typeof value !== 'undefined' && value !== null) {
          returnValue = value;
        }
        return returnValue;
      },
      excelFormatter: '#,##0'
    }
  ];

  return columns;
}

export function getFinancingAgreementTermsColumns(isUserAuthorizedForInterestEdit) {
  let columns = getKeyColumns(isUserAuthorizedForInterestEdit);
  if (CODEX_PROPERTIES["com.arcesium.treasury.interest.areAccountLevelCalculationsEnabled"] === 'true'
    || CODEX_PROPERTIES["com.arcesium.treasury.interest.areAccountLevelCalculationsEnabled"] === true) {
    columns = columns.concat(getAccountLevelColumns());
  }
  columns = columns.concat(getFinancingCommonColumns());
  columns = columns.concat(getEffectiveDateColumns());
  return columns;
}

function _getAttr(obj, attrName, fieldName) {
  let value = '';
  if (
    typeof obj.financingTerm.attributeList !== 'undefined' &&
    obj.financingTerm.attributeList !== null
  ) {
    obj.financingTerm.attributeList.forEach((entry) => {
      if (entry.name === attrName) {
        if (fieldName === 'ivalue') {
          value = entry.ivalue;
        } else if (fieldName === 'rvalue') {
          value = entry.rvalue;
        } else {
          value = entry.attributeValueName;
        }
      }
    });
  }
  return value;
}

function _getAttrFormatter(financingTerm, attributeName, fieldName, defaultValue) {
  let attributeValue = defaultValue;
  if (typeof financingTerm.attributeList !== 'undefined' && financingTerm.attributeList !== null) {
    financingTerm.attributeList.forEach((entry) => {
      if (entry.name === attributeName) {
        if (fieldName === 'ivalue') {
          attributeValue = entry.ivalue;
        } else if (fieldName === 'rvalue') {
          attributeValue = entry.rvalue;
        } else if (fieldName === 'attributeValueName') {
          attributeValue = entry.attributeValueName;
        }
      }
    });
  }
  return attributeValue;
}
