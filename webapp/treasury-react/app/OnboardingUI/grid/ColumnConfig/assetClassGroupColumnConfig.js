import {
  linkFormatter
} from 'commons/grid/formatters';

export function getAllAssetClassColumns() {
  return [{
    id: 'assetClassId',
    type: 'text',
    name: 'All Asset Classes',
    field: 'asset_class_name',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  }];
}

export function getExistingAssetClassColumns() {
  return [{
    id: 'assetClassId',
    type: 'text',
    name: 'Existing Asset Classes',
    field: 'asset_class_name',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  }];
}

export function getAssetClassSearchColumns() {
  return [{
    id: 'assetClassGroup',
    type: 'text',
    name: 'Asset Class Group',
    field: 'displayName',
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  },
  {
    id: 'actions',
    name: '',
    exportToExcel: false,
    sortable: false,
    formatter: function(row, cell, value, columnDef, dataContext) {
      return (
        "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  "
      );
    },
    width: 30,
    minWidth: 30,
    maxWidth: 30,
    fixed: true
  }
  ];
}

export function getAssetClassConditionColumns(isUserAuthorizedForInterestEdit) {
  const columns = [

      {
        id: 'delete',
        name: 'Delete',
        exportToExcel: false,
        sortable: false,
        formatter: function(row, cell, value, columnDef, dataContext) {
            
          return isUserAuthorizedForInterestEdit? (
            " <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>   "
          ):null;
        },
        width: 60,
        minWidth: 60,
        maxWidth: 60,
        fixed: true
      },
    {
      id: 'sourceType',
      type: 'text',
      name: 'Attribute Source Type',
      field: 'sourceTypeDisplay',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'attrName',
      type: 'text',
      name: 'Attribute Name',
      field: 'attrNameDisplay',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'attrValue',
      type: 'text',
      name: 'Attribute Value',
      field: 'attrValueDisplay',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }];
    return columns;
}
