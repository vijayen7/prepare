import { linkFormatter } from 'commons/grid/formatters';

export function getCommonColumns() {
  return [
    {
      id: 'agreementType',
      type: 'text',
      name: 'Agreement Type',
      field: 'agreementType',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 80,
      excelFormatter: '#,##0'
    },
    {
      id: 'legalEntity',
      type: 'text',
      name: 'Legal Entity',
      field: 'legalEntity',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100,
      excelFormatter: '#,##0'
    },
    {
      id: 'counterPartyEntity',
      type: 'text',
      name: 'Counterparty',
      field: 'cpe',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 90,
      excelFormatter: '#,##0'
    },
    {
      id: 'currency',
      type: 'text',
      name: 'Currency',
      field: 'ccy',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 70,
      excelFormatter: '#,##0'
    },
    {
      id: 'nettingGroup',
      type: 'text',
      name: 'Netting Group',
      field: 'nettingGroup',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 150,
      excelFormatter: '#,##0'
    },
    {
      id: 'chargeType',
      type: 'text',
      name: 'Charge Type',
      field: 'chargeType',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'region',
      type: 'text',
      name: 'Region',
      field: 'region',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'country',
      type: 'text',
      name: 'Country',
      field: 'country',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'bundle',
      type: 'text',
      name: 'Bundle',
      field: 'bundle',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'business_unit',
      type: 'text',
      name: 'Business Unit',
      field: 'business_unit',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'custodian_account',
      type: 'text',
      name: 'Custodian Account',
      field: 'custodian_account',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'validFrom',
      type: 'text',
      name: 'Valid From',
      field: 'effective_start_date',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 80,
      excelFormatter: '#,##0'
    },
    {
      id: 'validTill',
      type: 'text',
      name: 'Valid Till',
      field: 'effective_end_date',
      sortable: true,
      headerCssClass: 'aln-rt b',
      formatter(row, cell, value, columnDef, dataContext) {
        if (value === '20380101') {
          return '-';
        }
        return value;
      },
      width: 60,
      excelFormatter: '#,##0'
    }
  ];
}

function getActionColumn(isUserAuthorizedForInterestEdit) {
  return [
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return isUserAuthorizedForInterestEdit? (
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        ):null;
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    }
  ];
}

export function getAccrualPostingColumns(isUserAuthorizedForInterestEdit) {
  let columns = getActionColumn(isUserAuthorizedForInterestEdit);
  columns = columns.concat(getCommonColumns());
  return columns;
}
