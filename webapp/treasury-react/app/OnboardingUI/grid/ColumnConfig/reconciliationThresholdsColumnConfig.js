
import {convertJavaNYCDashedDate} from 'commons/util';

export function getReconciliationThresholdsColumns(isUserAuthorizedForInterestEdit) {
  return actionColumns(isUserAuthorizedForInterestEdit).concat(commonColumns()).concat(effectiveDateColumns());
}

function commonColumns() {
  return [
    {
      id: 'agreementTypeName',
      type: 'text',
      name: 'Agreement Type',
      field: 'agreementTypeName',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100,
      excelFormatter: '#,##0'
    },
    {
      id: "inFavorPercentageThreshold",
      name: "In Favor Percentage Threshold",
      field: "inFavorPercentageThreshold",
      toolTip: "In Favor Percentage Threshold",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      width: 120
    },
    {
      id: "inFavorAbsoluteThreshold",
      name: "In Favor Absolute Threshold",
      field: "inFavorAbsoluteThreshold",
      toolTip: "In Favor Absolute Threshold",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 120
    },
    {
      id: "againstFavorPercentageThreshold",
      name: "Against Favor Percentage Threshold",
      field: "againstFavorPercentageThreshold",
      toolTip: "Against Favor Percentage Threshold",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 120
    },
    {
      id: "againstFavorAbsoluteThreshold",
      name: "Against Favor Percentage Threshold",
      field: "againstFavorAbsoluteThreshold",
      toolTip: "Against Favor Percentage Threshold",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 120
    },
    {
      id: "comment",
      name: "Comment",
      field: "comment",
      toolTip: "Comment",
      type: "text",
      filter: true,
      headerCssClass: "aln-rt b",
      width: 70
    },
    {
      id: "userLogin",
      name: "User",
      field: "userLogin",
      toolTip: "User",
      type: "text",
      filter: true,
      headerCssClass: "aln-rt b",
      width: 100
    },
  ]
}

function effectiveDateColumns() {
  return [
    {
      id: 'validFrom',
      type: 'text',
      name: 'Valid Start',
      field: 'effectiveBeginDate',
      sortable: true,
      headerCssClass: 'aln-rt b',
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDashedDate(value);
      },
    },
    {
      id: 'validEnd',
      type: 'text',
      name: 'Valid End',
      field: 'effectiveEndDate',
      sortable: true,
      headerCssClass: 'aln-rt b',
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDashedDate(value);
      },
    }
  ];
  return columns;
}

function actionColumns(isUserAuthorizedForInterestEdit) {
  return [
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return isUserAuthorizedForInterestEdit ?(
          "<a title='Delete'><i data-role='delete' class='icon-delete' /></a>  "
        ): null;
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    }
  ];

  return columns;
}
