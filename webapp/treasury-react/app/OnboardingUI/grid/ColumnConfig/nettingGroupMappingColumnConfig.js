
function getActionColumn(isUserAuthorizedForInterestEdit) {
  return [
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return isUserAuthorizedForInterestEdit?(
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  "
        ):null;
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    }
  ];
}

function getCommonColumns() {
  return [
    {
      id: 'custodianAccountName',
      type: 'text',
      name: 'Custodian Account',
      field: 'custodianAccountName',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'nettingGroupName',
      type: 'text',
      name: 'Netting Group',
      field: 'nettingGroupName',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      formatter(row, cell, value, columnDef, dataContext) {
        return `<a>${value}</a>`;
      }
    }
  ];
}

export function getNettingGroupMappingColumns(isUserAuthorizedForInterestEdit) {
  let columns = getActionColumn(isUserAuthorizedForInterestEdit);
  columns = columns.concat(getCommonColumns());
  return columns;
}

export function getNettingGroupSimulatedColumns() {
  const columns = getCommonColumns();
  return columns;
}
