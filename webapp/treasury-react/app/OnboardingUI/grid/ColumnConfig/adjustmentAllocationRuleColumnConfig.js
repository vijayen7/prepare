export function getCommonColumns() {
  return [
    {
      id: 'agreementType',
      type: 'text',
      name: 'Agreement Type',
      field: 'agreementType',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 80,
      excelFormatter: '#,##0'
    },
    {
      id: 'legalEntity',
      type: 'text',
      name: 'Legal Entity',
      field: 'legalEntity',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100,
      excelFormatter: '#,##0'
    },
    {
      id: 'cpe',
      type: 'text',
      name: 'Counterparty',
      field: 'cpe',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 90,
      excelFormatter: '#,##0'
    },
    {
      id: 'ccy',
      type: 'text',
      name: 'Currency',
      field: 'ccy',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 70,
      excelFormatter: '#,##0'
    },
    {
      id: 'chargeType',
      type: 'text',
      name: 'Charge Type',
      field: 'chargeType',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'allocationStrategy',
      type: 'text',
      name: 'Allocation Strategy',
      field: 'allocationStrategy',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }
  ];
}

function getActionColumn(isUserAuthorizedForInterestEdit) {
  return [
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return isUserAuthorizedForInterestEdit? (
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        ):null;
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    }
  ];
}

export function getAdjustmentAllocationRuleColumns(isUserAuthorizedForInterestEdit) {
  let columns = getActionColumn(isUserAuthorizedForInterestEdit);
  columns = columns.concat(getCommonColumns());
  return columns;
}

export function getAdjustmentAllocationRuleSimulatedColumns() {
  const columns = getCommonColumns();
  return columns;
}
