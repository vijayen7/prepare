function commonCalculatorGroupColumns() {
  const columns = [
    {
      id: 'agreementType',
      type: 'text',
      name: 'Agreement Type',
      field: 'agreementType',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100,
      excelFormatter: '#,##0'
    },
    {
      id: 'legalEntity',
      type: 'text',
      name: 'Legal Entity',
      field: 'legalEntity',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100,
      excelFormatter: '#,##0'
    },
    {
      id: 'counterPartyEntity',
      type: 'text',
      name: 'Counterparty',
      field: 'cpe',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100,
      excelFormatter: '#,##0'
    },
    {
      id: 'currency',
      type: 'text',
      name: 'Currency',
      field: 'ccy',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 80,
      excelFormatter: '#,##0'
    },
    {
      id: 'nettingGroup',
      type: 'text',
      name: 'Netting Group',
      field: 'nettingGroup',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 180,
      excelFormatter: '#,##0'
    },
    {
      id: 'calculatorGroup',
      type: 'text',
      name: 'Calculator Group',
      field: 'calculatorGroup',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      formatter(row, cell, value, columnDef, dataContext) {
        return `<a>${value}</a>`;
      }
    },
    {
      id: 'validFrom',
      type: 'text',
      name: 'Valid From',
      field: 'effective_start_date',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 80,
      excelFormatter: '#,##0'
    },
    {
      id: 'validTill',
      type: 'text',
      name: 'Valid Till',
      field: 'effective_end_date',
      sortable: true,
      headerCssClass: 'aln-rt b',
      formatter(row, cell, value, columnDef, dataContext) {
        if (value === '20380101') {
          return '-';
        }
        return value;
      },
      width: 60,
      excelFormatter: '#,##0'
    }
  ];

  return columns;
}

function actionColumns(isUserAuthorizedForInterestEdit) {
  const columns = [
    {
      id: 'actions',
      name: 'Actions',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return isUserAuthorizedForInterestEdit ? (
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a> " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        ): null;
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    }
  ];

  return columns;
}

export function getCalculatorGroupColumns(isUserAuthorizedForInterestEdit) {
  let columns = actionColumns(isUserAuthorizedForInterestEdit);
  columns = columns.concat(commonCalculatorGroupColumns());
  return columns;
}

export function getCalculatorGroupSimulatedColumns() {
  const columns = commonCalculatorGroupColumns();
  return columns;
}
