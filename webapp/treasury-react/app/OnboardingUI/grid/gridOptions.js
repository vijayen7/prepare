import { url } from "../api";
import { getSaveSettingsUrl } from "commons/util";
import { onboardingScreen } from "../util";

export function getGridOptionsForLandingGrid(view) {
  let gridOptions = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    },
    autoHorizontalScrollBar: true,
    nestedTable: false,
    highlightRowOnClick: true,
    sortList: [
      {
        columnId: "agreementType",
        sortAsc: true
      }
    ],
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    frozenColumn: 0,
    exportToExcel: true,
    saveSettings: {
      applicationId: 3,
      serviceURL: getSaveSettingsUrl(),
      onBeforeLoadedCallback: function(settingsData) {},
      loadedCallback: function(gridObject) {}
    }
  };
  switch (view) {
    case onboardingScreen.CALCULATOR:
      gridOptions.saveSettings.applicationCategory = "CalculatorGroup";
      break;
    case onboardingScreen.FINANCING:
      gridOptions.saveSettings.applicationCategory = "FinancingAgreementTerm";
      gridOptions.frozenColumn = 5;
      break;
    case onboardingScreen.ASSET:
      gridOptions.saveSettings.applicationCategory = "AssetClassGroup";
      break;
    case onboardingScreen.REPORTING:
      gridOptions.saveSettings.applicationCategory = "ReportingCurrency";
      break;
    case onboardingScreen.ACCRUAL:
      gridOptions.saveSettings.applicationCategory = "AccrualPosting";
      break;
    case onboardingScreen.NETTING:
      gridOptions.saveSettings.applicationCategory = "NettingGroup";
      break;
    case onboardingScreen.BUBUNDLE:
      gridOptions.saveSettings.applicationCategory = "BuBundleMapping";
      break;
    case onboardingScreen.GENEVABOOK:
      gridOptions.saveSettings.applicationCategory = "GenevaBookMapping";
      break;
    case onboardingScreen.ADJUSTMENT:
      gridOptions.saveSettings.applicationCategory = "Adjustment";
      break;
    case onboardingScreen.RECONCILIATION_THRESHOLDS:
      gridOptions.saveSettings.applicationCategory = "ReconciliationThresholds";
      break;
    default:
      break;
  }
  return gridOptions;
}

export function getGridOptions() {
  return {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    highlightRowOnClick: true,
    sortList: [
      {
        columnId: "agreementType",
        sortAsc: true
      }
    ],
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    exportToExcel: true
  };
}

export function getSimulatedGridOptions() {
  const gridOptions = getGridOptions();
  gridOptions.checkboxHeader = [
    {
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    }
  ];
  gridOptions.addCheckboxHeaderAsLastColumn = false;
  return gridOptions;
}
