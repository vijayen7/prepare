import { connect } from "react-redux";
import OnboardingUIGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import Table from "commons/components/Table";
import _ from "lodash";
import React, { Component } from "react";
import FilterButton from "commons/components/FilterButton";
import { bindActionCreators } from "redux";
import { getColumnConfig } from "../grid/columnConfig";
import { getGridOptionsForLandingGrid } from "../grid/gridOptions";
import CalculatorGroupPopUp from "../popup/components/CalculatorGroupPopUp";
import NettingGroupPopUp from "../popup/container/NettingGroupPopUp";
import FinancingTermsPopUp from "../popup/components/FinancingTermPopUp";
import ReportingCurrencyPopUp from "../popup/components/ReportingCurrencyPopUp";
import AccrualPostingPopUp from "../popup/components/AccrualPostingPopUp";
import AssetClassPopUp from "../popup/container/AssetClassPopUp";
import BuBundleMappingPopUp from "../popup/components/BuBundleMappingPopUp";
import GenevaBookMappingPopUp from "../popup/components/GenevaBookMappingPopUp";
import BandwidthGroupDialog from "../popup/container/BandwidthGroupDialog";
import AddNettingGroupDialog from "../popup/container/AddNettingGroupDialog";
import AdjustmentAllocationPopUp from "../popup/components/AdjustmentAllocationPopUp";
import ReconciliationThresholdsPopUp from "../popup/container/ReconciliationThresholdsPopUp";
import LeanSurchargeModelPopup from "../popup/container/LeanSurchargeModelPopup";
import { Layout, Card, Panel } from "arc-react-components";
import { onboardingScreen } from "../util";
import {
  setDeleteToggle,
  showPrimaryPopUp,
  loadCalculatorSidebar,
  setAddBandwidthToggle,
  saveBandWidthGroup,
  destroyAddBandwidthToggle,
  resetBandwidthGroupData,
  displayAddLeanSurchargePopup,
  displayViewLeanSurchargePopup,
  destroyLeanSurchargePopup,
  fetchLeanSurchargeModelById,
  saveLeanSurchargeModel,
  resetSaveLeanSurchargeModel,
  updateLeanSurchargeGridData,
  fetchAssetClassGroupById,
  resetAssetClassGroupByIdData,
  fetchCalculatorsForCalculatorGroup,
  resetAddNettingGroupResult,
  saveNettingGroup,
  setSelectedRowData,
  fetchNettingGroupDetail,
  isUserAuthorizedForInterestEdit,
isUserAuthorizedForInterestPowerEdit
} from "../actions";


class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.renderPopUps = this.renderPopUps.bind(this);
    this.renderButtons = this.renderButtons.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.handleAddNettingGroup = this.handleAddNettingGroup.bind(this);
    this.openPrimaryPopUp = this.openPrimaryPopUp.bind(this);
    this.closeNettingGroupPopup = this.closeNettingGroupPopup.bind(this);
    this.loadCalculatorDrillDown = this.loadCalculatorDrillDown.bind(this);
    this.getGridData = this.getGridData.bind(this);
    this.state = {
      addNettingGroupDialog: false,
      isCalculatorGroupShown: false,
      calculatorGroupName: "",
      resizeCanvas: false,
      isNettingGroupDetailShown: false,
      nettingGroupName: "",
      selectedLeanSurchargeModel: ""
    };
  }

  componentDidMount() {
    this.props.isUserAuthorizedForInterestEdit();
    this.props.isUserAuthorizedForInterestPowerEdit();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.calculatorGroupData !== this.props.calculatorGroupData) {
      this.setState({ isCalculatorGroupShown: false });
    }
    if (prevProps.nettingGroupMappingData !== this.props.nettingGroupMappingData) {
      this.setState({ isNettingGroupDetailShown: false });
    }
  }

  onCellClickHandler = args => {
    let ele = args.event.target;
    let { role } = ele.dataset;
    if (role === 'edit' || role === 'delete') {
      this.props.setSelectedRowData(args.item);
      this.openPrimaryPopUp();
      if (role === 'delete') {
        this.props.setDeleteToggle();
      }
    }
    if (
      args.colId === "assetClassGroup" ||
      (this.props.view === onboardingScreen.ASSET && role === "edit")
    ) {
      this.props.resetAssetClassGroupByIdData();
      const parameters = {
        clickedAssetClassGroupId: args.item.assetClassGroupId.toString(),
        selectedDate: this.props.selectedDate.toString()
      };
      this.props.fetchAssetClassGroupById(parameters, args.item.displayName);
    }
    if (args.colId === "calculatorGroup") {
      const parameters = {
        calculatorGroupId: args.item.calculator_group
      };
      this.props.fetchCalculatorsForCalculatorGroup(parameters);
      this.setState({
        isCalculatorGroupShown: true,
        resizeCanvas: !this.state.resizeCanvas,
        calculatorGroupName: args.item.calculatorGroup
      });
    }
    if (args.colId === "nettingGroupName") {
      const parameters = {
        nettingGroupId: parseInt(args.item.nettingGroupId)
      };
      this.props.fetchNettingGroupDetail(parameters);
      this.setState({
        isNettingGroupDetailShown: true,
        resizeCanvas: !this.state.resizeCanvas,
        nettingGroupName: args.item.nettingGroupName
      });
    }
    if( args.colId === "leanSurchargeModel" && typeof args.item.financingTerm !== "undefined"
        && typeof args.item.financingTerm.attributeList[9] !== "undefined" &&
        args.item.financingTerm.attributeList[9].ivalue !== null) {
      this.props.displayViewLeanSurchargePopup();
      this.setState({selectedLeanSurchargeModel: args.item.financingTerm.attributeList[9].attributeValueName});
      this.props.fetchLeanSurchargeModelById({id: args.item.financingTerm.attributeList[9].ivalue});
    }
  };

  resizeCanvas() { }

  handleBulkInsertAgreementTerms() {
    const agreementTermsShovelUrl = "/shovel/template/Agreement Terms";
    window.open(agreementTermsShovelUrl);
  }

  handleBulkUpdateMapping() {
    const bulkUpdateNettingGroupMappingShovelUrl =
      "/shovel/template/Custodian Account by Netting Group";
    window.open(bulkUpdateNettingGroupMappingShovelUrl);
  }

  handleBulkInsertNettingGroup() {
    const bulkInsertNettingGroupShovelUrl = "/shovel/template/Netting Group";
    window.open(bulkInsertNettingGroupShovelUrl);
  }

  handleBulkInsert = () => {
    switch(this.props.view) {
      case onboardingScreen.CALCULATOR:
        window.open("/cocoa/#/?parserNames=treasury_interest_calculator_group_rule_update");
        break;
      case onboardingScreen.ACCRUAL:
        window.open("/cocoa/#/?parserNames=treasury_interest_accrual_posting_rule_update");
        break;
      case onboardingScreen.GENEVABOOK:
         window.open("/cocoa/#/?parserNames=Treasury_Interst_account_book_mapping_rule_update");
         break;
       default:
          break;
    }
  }

  handleAddNettingGroup() {
    this.setState({ addNettingGroupDialog: true });
  }

  openPrimaryPopUp() {
    this.props.showPrimaryPopUp(this.props.view);
  }

  closeNettingGroupPopup() {
    this.setState({ addNettingGroupDialog: false });
    this.props.resetAddNettingGroupResult();
  }

  renderAssetTables(dataObject) {
    if (_.isEmpty(dataObject)) {
      return <React.Fragment />;
    }
    let message = "";
    const displayName = dataObject.name;
    const dataList = dataObject.data.resultList;
    if (dataList.length <= 0) {
      message = `No Asset Classes Found for ${displayName}`;
      return (
        <React.Fragment>
          <Message messageData={message} />
        </React.Fragment>
      );
    }
    const labels = ["Source Type", "Attribute", "Value", "Underlying Check"];
    const dataKeys = [
      "source_type_id",
      "attr_name",
      "value",
      "underlying_check"
    ];
    const tables = dataList.map(data => {
      const assetClassName = data.asset_class_name;
      return (
        <Layout.Child childId="assetClassGrid1" size="content" key={assetClassName}>
          <div style={{ paddingBottom: "15px" }}>
            <Layout isColumnType>
              <Layout.Child childId="assetClassGrid2" size={3}>
                <div size="1">{assetClassName}</div>
              </Layout.Child>
              <Layout.Child childId="assetClassGrid3" size={10}>
                <div
                  className="size--content"
                  style={{ width: "90%", display: "block" }}
                >
                  <Table
                    labels={labels}
                    content={data.attributes}
                    dataKeys={dataKeys}
                    width={"25%"}
                  />
                </div>
              </Layout.Child>
            </Layout>
          </div>
        </Layout.Child>
      );
    });
    message = `Asset Classes for ${displayName}`;
    return (
      <Card>
        <Message messageData={message} />
        <hr />
        <Layout childId="assetClassMessage1" isRowType>{tables}</Layout>
      </Card>
    );
  }

  renderGridData() {
    let data = this.getGridData(this.props.view);
    let grid = null;
    if (data.length <= 0) {
      return (
        <React.Fragment />
      );
    }
    let authLevel = this.props.isUserAuthorizedForInterestEdit;
    this.props.view == onboardingScreen.RECONCILIATION_THRESHOLDS ?
        authLevel = this.props.isUserAuthorizedForInterestPowerEdit: null;
    grid = (
      <OnboardingUIGrid
        data={data}
        gridId="onboardingUI"
        gridColumns={getColumnConfig(this.props.view, authLevel)}
        gridOptions={getGridOptionsForLandingGrid(this.props.view)}
        label=""
        onCellClick={this.onCellClickHandler}
        resizeCanvas={this.state.resizeCanvas}
      />
    );

    if (this.props.view === onboardingScreen.ASSET) {
      return (
        <Layout isColumnType>
          <Layout.Child childId="assetClassGrid4" size={1}>
            <div>{grid}</div>
          </Layout.Child>
          <Layout.Child childId="assetClassGrid5" size={0.02}>
            <React.Fragment />
          </Layout.Child>
          <Layout.Child childId="assetClassGrid6" size={1}>
            {this.renderAssetTables(this.props.assetClassGroupDataById)}
          </Layout.Child>
        </Layout>
      );
    }
    return grid;
  }

  getGridData(view) {
    let data = [];
    switch (view) {
      case onboardingScreen.CALCULATOR:
        data = this.props.calculatorGroupData;
        break;
      case onboardingScreen.FINANCING:
        data = this.props.financingAgreementTermsData;
        break;
      case onboardingScreen.ASSET:
        data = this.props.assetClassGroupData;
        break;
      case onboardingScreen.REPORTING:
        data = this.props.reportingCurrencyData;
        break;
      case onboardingScreen.ACCRUAL:
        data = this.props.accrualPostingData;
        break;
      case onboardingScreen.NETTING:
        data = this.props.nettingGroupMappingData;
        break;
      case onboardingScreen.BUBUNDLE:
        data = this.props.buBundleMappingData;
        break;
      case onboardingScreen.GENEVABOOK:
        data = this.props.genevaBookMappingData;
        break;
      case onboardingScreen.ADJUSTMENT:
        data = this.props.adjustmentAllocationRuleData;
        break;
      case onboardingScreen.RECONCILIATION_THRESHOLDS:
        data = this.props.reconciliationThresholdsData;
        break;
      default:
        break;
    }
    return data;
  }

  renderButtons() {
    let buttonName = "";
    let data = [];
    switch (this.props.view) {
      case onboardingScreen.CALCULATOR:
        buttonName = "Calculator Group";
        data = this.props.calculatorGroupData;
        break;
      case onboardingScreen.FINANCING:
        buttonName = "Financing Agreement Terms";
        data = this.props.financingAgreementTermsData;
        break;
      case onboardingScreen.ASSET:
        buttonName = "Asset Class Group";
        data = this.props.assetClassGroupData;
        break;
      case onboardingScreen.REPORTING:
        buttonName = "Accrual Posting Currency";
        data = this.props.reportingCurrencyData;
        break;
      case onboardingScreen.ACCRUAL:
        buttonName = "Accrual Posting";
        data = this.props.accrualPostingData;
        break;
      case onboardingScreen.NETTING:
        buttonName = "Netting Group Mapping";
        data = this.props.nettingGroupMappingData;
        break;
      case onboardingScreen.BUBUNDLE:
        buttonName = "BU Bundle Mapping";
        data = this.props.buBundleMappingData;
        break;
      case onboardingScreen.GENEVABOOK:
        buttonName = "Accounting Book Mapping";
        data = this.props.genevaBookMappingData;
        break;
      case onboardingScreen.ADJUSTMENT:
        buttonName = "Adjustment Allocation";
        data = this.props.adjustmentAllocationRuleData;
        break;
      case onboardingScreen.RECONCILIATION_THRESHOLDS:
        buttonName = "Threshold";
        data = this.props.reconciliationThresholdsData;
        break;
      default:
        break;
    }
    let primary = "button--primary size--content float--right margin--small";
    let secondary = "button--secondary size--content float--right margin--small";

    if (data.length <= 0) {
      primary = "button--success button--medium size--content margin--double";
      secondary = "button--secondary size--content  margin--double";
    }

    let buttons = (
      <React.Fragment>
        <FilterButton onClick={this.openPrimaryPopUp} className={primary} label={`Add ${buttonName}`} />
        {this.props.view === onboardingScreen.FINANCING ? (
          <React.Fragment>
            <FilterButton
              onClick={this.props.setAddBandwidthToggle}
              className={secondary}
              label="Add Bandwidth Group"
            />
            <FilterButton
              onClick={this.props.displayViewLeanSurchargePopup}
              className={secondary}
              label="View Lean Surcharge Model"
            />
            <FilterButton
              onClick={this.props.displayAddLeanSurchargePopup}
              className={secondary}
              label="Add Lean Surcharge Model"
            />
            <FilterButton
              onClick={this.handleBulkInsertAgreementTerms}
              className={secondary}
              label="Bulk Insert Agreement Terms"
            />
          </React.Fragment>
        ) : (
            <React.Fragment />
          )}
        {this.props.view === onboardingScreen.NETTING ? (
          <React.Fragment>
            <FilterButton
              onClick={this.handleBulkUpdateMapping}
              className={secondary}
              label="Bulk Update Mapping"
            />
            <FilterButton
              className={secondary}
              label="Add New Netting Group"
              onClick={this.handleAddNettingGroup}
            />
            <FilterButton
              onClick={this.handleBulkInsertNettingGroup}
              className={secondary}
              label="Bulk Insert Netting Group"
            />
          </React.Fragment>
        ) : (
            <React.Fragment />
          )}
          {(this.props.view == onboardingScreen.ACCRUAL
            || this.props.view == onboardingScreen.GENEVABOOK
            || this.props.view == onboardingScreen.CALCULATOR) ? (
              <FilterButton
                onClick={this.handleBulkInsert}
                className={secondary}
                label={`Bulk Insert ${buttonName}`}
              />
            ): (
              <React.Fragment />
            )

          }
      </React.Fragment>
    );

    if (data.length <= 0) {
      return (
        <div
          className="text-align--center"
          style={{ width: "300px", height: "170px", margin: "auto" }}
        >
          {buttons}
        </div>
      );
    }

    return <Layout.Child childId="GridButtons1" size={1}>{buttons}</Layout.Child>;
  }

  renderPopUps() {
    return (
      <React.Fragment>
        <CalculatorGroupPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.calculator} />
        <FinancingTermsPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.financing} />
        <AssetClassPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.asset} />
        <ReportingCurrencyPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.reporting} />
        <AccrualPostingPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.accrual} />
        <NettingGroupPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.netting} />
        <BuBundleMappingPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.buBundle} />
        <GenevaBookMappingPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.genevaBook} />
        <AdjustmentAllocationPopUp view={this.props.view} isPopUpOpen={this.props.isPopUpOpen.adjustment} />
        <LeanSurchargeModelPopup
          isLeanSurchargeModelPopupOpen={this.props.isLeanSurchargeModelPopupOpen}
          isLeanSurchargeModelViewMode={this.props.isLeanSurchargeModelViewMode}
          leanSurchargeModelData={this.props.leanSurchargeModelData}
          leanSurchargeModels={this.props.leanSurchargeModels}
          selectedLeanSurchargeModel={this.state.selectedLeanSurchargeModel}
          isSavedLeanSurchargeModel={this.props.isSavedLeanSurchargeModel}

          displayAddLeanSurchargePopup={this.props.displayAddLeanSurchargePopup}
          displayViewLeanSurchargePopup={this.props.displayAddLeanSurchargePopup}
          destroyLeanSurchargePopup={this.props.destroyLeanSurchargePopup}
          fetchLeanSurchargeModelById={this.props.fetchLeanSurchargeModelById}
          saveLeanSurchargeModel={this.props.saveLeanSurchargeModel}
          resetSaveLeanSurchargeModel={this.props.resetSaveLeanSurchargeModel}
          updateLeanSurchargeGridData={this.props.updateLeanSurchargeGridData}
        />
        <BandwidthGroupDialog
          isAddBandwidthClicked={this.props.isAddBandwidthClicked}
          destroyAddBandwidthToggle={this.props.destroyAddBandwidthToggle}
          bandwidthGroupData={this.props.bandwidthGroupData}
          saveBandWidthGroup={this.props.saveBandWidthGroup}
          resetBandwidthGroupData={this.props.resetBandwidthGroupData}
        />
        <AddNettingGroupDialog
          isDialogOpen={this.state.addNettingGroupDialog}
          handleClosePopup={this.closeNettingGroupPopup}
          addNettingGroupResult={this.props.addNettingGroupResult}
          resetAddNettingGroupResult={this.props.resetAddNettingGroupResult}
          saveNettingGroup={this.props.saveNettingGroup}
        />
      <ReconciliationThresholdsPopUp isPopUpOpen={this.props.isPopUpOpen.reconciliationThresholds}/>
      </React.Fragment>
    );
  }

  loadCalculatorDrillDown() {
    if (!this.state.isCalculatorGroupShown)
      return <React.Fragment />
    let drillDown;
    drillDown = (
      <Panel
        dismissible
        onClose={() => {
          this.setState({
            resizeCanvas: !this.state.resizeCanvas,
            isCalculatorGroupShown: false
          });
        }}
        title={this.state.calculatorGroupName}>
        <Table
          labels={["Name", "Level"]}
          content={this.props.calculatorsForCalculatorGroup}
          dataKeys={["name", "objectType"]}
          style={{ width: "25%", display: "table" }}
        />
      </Panel>
    );
    if (
      this.props.view === onboardingScreen.CALCULATOR &&
      this.props.calculatorsForCalculatorGroup.length !== 0
    ) {
      return drillDown;
    }
    return <React.Fragment />;
  }

  loadNettingGroupDrillDown = () => {
    if (!this.state.isNettingGroupDetailShown)
      return <React.Fragment />
    let drillDown;
    drillDown = (
      <Panel
        dismissible
        onClose={() => {
          this.setState({
            resizeCanvas: !this.state.resizeCanvas,
            isNettingGroupDetailShown: false
          });
        }}
        title={this.state.nettingGroupName}
        className="margin--vertical">
        <Table
          labels={["Name", "Agreement Type", "Financing Type", "Financing Style", "Agreement"]}
          content={[this.props.nettingGroupDetail]}
          dataKeys={["name", "agreementType", "financingType", "financingStyle", "agreement"]}
          style={{ width: "25%", display: "table" }}
          className="margin--vertical"
        />
      </Panel>
    );
    if (
      this.props.view === onboardingScreen.NETTING &&
      this.props.nettingGroupDetail.length !== 0
    ) {
      return drillDown;
    }
    return <React.Fragment />;
  }

  render() {
    let data = this.getGridData(this.props.view);
    if (data.length <= 0) {
      return (
        <React.Fragment>
          <div style={{ width: "450px", margin: "auto" }}>
            <br />
            <Message messageData={this.props.onboardingUISearchMessage} />
            <br />
          </div>
          {this.props.view == onboardingScreen.RECONCILIATION_THRESHOLDS ?
            this.props.isUserAuthorizedForInterestPowerEdit && this.renderButtons():
            this.props.isUserAuthorizedForInterestEdit && this.renderButtons() }
          {this.renderPopUps()}
        </React.Fragment>
      );
    }
    return (
      <Layout>
        {this.props.view == onboardingScreen.RECONCILIATION_THRESHOLDS ?
            this.props.isUserAuthorizedForInterestPowerEdit && this.renderButtons():
            this.props.isUserAuthorizedForInterestEdit && this.renderButtons() }
        <Layout.Child size={25} childId="onboardingGrid1">
          <Layout>
            <Layout.Child childId="onboardingGrid2">{this.renderGridData()}</Layout.Child>
            <Layout.Child size="Fit" childId="calculatorDrillDown">{this.loadCalculatorDrillDown()}</Layout.Child>
            <Layout.Child size="Fit" childId="NettingGroupDrillDown">{this.loadNettingGroupDrillDown()}</Layout.Child>
          </Layout>
          {this.renderPopUps()}
        </Layout.Child>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    calculatorGroupData: state.financingOnboarding.calculatorGroupData,
    financingAgreementTermsData:
      state.financingOnboarding.financingAgreementTermsData,
    assetClassGroupData: state.financingOnboarding.assetClassGroupData,
    assetClassGroupDataById: state.financingOnboarding.assetClassGroupDataById,
    reportingCurrencyData: state.financingOnboarding.reportingCurrencyData,
    accrualPostingData: state.financingOnboarding.accrualPostingData,
    nettingGroupMappingData: state.financingOnboarding.nettingGroupMappingData,
    onboardingUISearchMessage:
      state.financingOnboarding.onboardingUISearchMessage,
    isAddBandwidthClicked: state.financingOnboarding.isAddBandwidthClicked,
    isLeanSurchargeModelPopupOpen: state.financingOnboarding.isLeanSurchargeModelPopupOpen,
    isLeanSurchargeModelViewMode: state.financingOnboarding.isLeanSurchargeModelViewMode,
    leanSurchargeModelData: state.financingOnboarding.leanSurchargeModelData,
    leanSurchargeModels: state.filters.leanSurchargeModels,
    isSavedLeanSurchargeModel: state.financingOnboarding.isSavedLeanSurchargeModel,
    bandwidthGroupData: state.financingOnboarding.bandwidthGroupData,
    addNettingGroupResult: state.financingOnboarding.addNettingGroupResult,
    selectedDate: state.financingOnboarding.selectedDate,
    calculatorsForCalculatorGroup:
      state.financingOnboarding.calculatorsForCalculatorGroup,
    buBundleMappingData: state.financingOnboarding.buBundleMappingData,
    genevaBookMappingData: state.financingOnboarding.genevaBookMappingData,
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen,
    nettingGroupDetail: state.financingOnboarding.nettingGroupDetail,
    adjustmentAllocationRuleData: state.financingOnboarding.adjustmentAllocationRuleData,
    reconciliationThresholdsData: state.financingOnboarding.reconciliationThresholdsData,
    isUserAuthorizedForInterestPowerEdit: state.financingOnboarding.isUserAuthorizedForInterestPowerEdit,
    isUserAuthorizedForInterestEdit: state.financingOnboarding.isUserAuthorizedForInterestEdit
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      showPrimaryPopUp,
      setDeleteToggle,
      setAddBandwidthToggle,
      saveBandWidthGroup,
      saveNettingGroup,
      destroyAddBandwidthToggle,
      resetBandwidthGroupData,
      resetAddNettingGroupResult,
      loadCalculatorSidebar,
      fetchAssetClassGroupById,
      resetAssetClassGroupByIdData,
      fetchCalculatorsForCalculatorGroup,
      setSelectedRowData,
      fetchNettingGroupDetail,
      isUserAuthorizedForInterestEdit,
      isUserAuthorizedForInterestPowerEdit,
      displayAddLeanSurchargePopup,
      displayViewLeanSurchargePopup,
      destroyLeanSurchargePopup,
      fetchLeanSurchargeModelById,
      saveLeanSurchargeModel,
      resetSaveLeanSurchargeModel,
      updateLeanSurchargeGridData,
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
