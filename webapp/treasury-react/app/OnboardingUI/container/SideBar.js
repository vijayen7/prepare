import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import MonthFilter from "commons/components/MonthFilter";
import { getCommaSeparatedValuesOrUndefined } from "../util";
import { getCommaSeparatedValues } from "commons/util";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import NettingGroupFilter from "commons/container/NettingGroupFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import FilterButton from "commons/components/FilterButton";
import CheckboxFilter from "commons/components/CheckboxFilter";
import { fetchSearchData, setSelectedDate } from "../actions";
import { onboardingScreen, getMonthStartDate } from "../util";
import { getCurrentDate, getCurrentMonth, getCommaSeparatedValuesOrNull } from "../../commons/util";
import { Card } from "arc-react-components";
import ChargeTypeFilter from '../../commons/container/ChargeTypeFilter';

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.state = this.getDefaultFilters();
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
    this.props.setSelectedDate(this.state.date);
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    const defaultDate = getCurrentDate();
    this.props.setSelectedDate(defaultDate);
    return {
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedAgreementTypes: [],
      selectedNettingGroups: [],
      selectedCurrencies: [],
      date: defaultDate,
      selectedChargeTypes: [],
      includeAllAssetClass: false,
      month:getCurrentMonth()
    };
  }

  handleClick() {
    const payload = (this.props.view == onboardingScreen.ADJUSTMENT) ? this.getAdjustmentAllocationRulesPayload() : this.getPayload();
    this.props.fetchSearchData(payload, this.props.view);
  }

  getAdjustmentAllocationRulesPayload = () => {
    let payload = null;

    payload = {
      "ruleSystemFilter.legalEntityIds": getCommaSeparatedValuesOrNull(
        this.state.selectedLegalEntities
      ),
      "ruleSystemFilter.cpeIds": getCommaSeparatedValuesOrNull(this.state.selectedCpes),
      "ruleSystemFilter.agreementTypeIds": getCommaSeparatedValuesOrNull(
        this.state.selectedAgreementTypes
      ),
      "ruleSystemFilter.chargeTypeIds": getCommaSeparatedValuesOrNull(
        this.state.selectedChargeTypes
      ),
      "ruleSystemFilter.currencyIds": getCommaSeparatedValuesOrNull(
        this.state.selectedCurrencies
      )
    };

    return payload;
  }

  getPayload = () => {
    let payload = null;
    if (
      this.props.view === onboardingScreen.ASSET ||
      this.props.view === onboardingScreen.FINANCING ||
      this.props.view === onboardingScreen.NETTING
    ) {
      payload = {
        legalEntityFilterIds: getCommaSeparatedValues(this.state.selectedLegalEntities),
        cpeFilterIds: getCommaSeparatedValues(this.state.selectedCpes),
        agreementTypeFilterIds: getCommaSeparatedValues(this.state.selectedAgreementTypes),
        nettingGroupFilterIds: getCommaSeparatedValues(this.state.selectedNettingGroups),
        currencyFilterIds: getCommaSeparatedValues(this.state.selectedCurrencies),
        dateString: this.state.date
      };
      if (this.props.view === onboardingScreen.ASSET) {
        payload.allAssetClassGroups = this.state.includeAllAssetClass;
      }
    } else if(this.props.view === onboardingScreen.RECONCILIATION_THRESHOLDS) {
        payload = {
          date: getMonthStartDate(this.state.month).toJSONString().substr(0,10).replaceAll('/','-')
        }
    } else {
      payload = {
        "ruleSystemFilter.legalEntityIds": getCommaSeparatedValuesOrUndefined(
          this.state.selectedLegalEntities
        ),
        "ruleSystemFilter.cpeIds": getCommaSeparatedValuesOrUndefined(this.state.selectedCpes),
        "ruleSystemFilter.agreementTypeIds": getCommaSeparatedValuesOrUndefined(
          this.state.selectedAgreementTypes
        ),
        "ruleSystemFilter.nettingGroupIds": getCommaSeparatedValuesOrUndefined(
          this.state.selectedNettingGroups
        ),
        "ruleSystemFilter.currencyIds": getCommaSeparatedValuesOrUndefined(
          this.state.selectedCurrencies
        ),
        "ruleSystemFilter.startDate": this.state.date
      };
    }

    return payload;
  };

  getButtons = () => {
    return (
      <ColumnLayout>
        <FilterButton onClick={this.handleClick} reset={false} label="Search" />
        <FilterButton reset={true} onClick={this.handleReset} label="Reset"  className ="button--tertiary size--content float--left margin--small"/>
      </ColumnLayout>
    )
  }

  render() {
    if(this.props.view == onboardingScreen.RECONCILIATION_THRESHOLDS){
      return (
        <Sidebar header="Search Criteria">
          <Card>
            <MonthFilter
              layout="standard"
              onSelect={this.onSelect}
              stateKey="month"
              data={this.state.month}
              label="Month"
             />
          </Card>
          {this.getButtons()}
        </Sidebar>
      )
    }
    return (
      <Sidebar header="Search Criteria">
        <Card>
          <DateFilter
            layout="standard"
            onSelect={this.onSelect}
            stateKey="date"
            data={this.state.date}
            label="Date"
          />
        </Card>
        <Card>
          <LegalEntityFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntities}
          />
          <CpeFilter onSelect={this.onSelect} selectedData={this.state.selectedCpes} />
          <AgreementTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedAgreementTypes}
          />
          <CurrencyFilter onSelect={this.onSelect} selectedData={this.state.selectedCurrencies} />
          {this.props.view === onboardingScreen.ADJUSTMENT ? (
            <ChargeTypeFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedChargeTypes}
            />
          ) : (
            <NettingGroupFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedNettingGroups}
            />
          )}

          <br />
          {this.props.view === onboardingScreen.ASSET ? (
            <CheckboxFilter
              onSelect={this.onSelect}
              stateKey="includeAllAssetClass"
              label="All Asset Class Groups"
              defaultChecked={this.state.includeAllAssetClass}
              style="left"
            />
          ) : (
            <React.Fragment />
          )}
        </Card>
        {this.getButtons()}
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchSearchData,
      setSelectedDate
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(SideBar);
