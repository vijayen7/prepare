import { START_LOADING, END_LOADING } from "commons/constants";
import {
  RESET_ONBOARDING_UI_SEARCH_MESSAGE,
  SET_BANDWIDTH_TOGGLE,
  DESTROY_BANDWIDTH_TOGGLE,
  LOAD_CALCULATOR_SIDEBAR,
  FETCH_SEARCH_DATA,
  FETCH_SIMULATE_DATA,
  SAVE_SIMULATE_DATA,
  SAVE_BANDWIDTH_GROUP,
  RESET_BANDWIDTH_GROUP_DATA,
  FETCH_ASSET_CLASS_GROUP_BY_ID,
  SET_SELECTED_DATE,
  RESET_SAVE_DATA,
  SET_VIEW,
  SAVE_NETTING_GROUP,
  RESET_NETTING_GROUP_RESULT,
  FETCH_CALCULATORS_FOR_CALCULATOR_GROUP,
  DELETE_FINANCING_TERM,
  DELETE_ACCRUAL_POSTING,
  DELETE_CALCULATOR_GROUP,
  DELETE_REPORTING_CURRENCY,
  DELETE_BU_BUNDLE_MAPPING,
  RESET_DELETE_ACCRUAL_DATA,
  RESET_DELETE_CALCULATOR_DATA,
  RESET_DELETE_REPORTING_DATA,
  RESET_DELETE_BU_BUNDLE_DATA,
  RESET_DELETE_TERM_DATA,
  SAVE_ASSET_CLASS,
  FETCH_AGREEMENTS_AFFECTED_FOR_ASSET,
  RESET_SAVE_ASSET_CLASS,
  SET_DELETE_TOGGLE,
  DESTROY_DELETE_TOGGLE,
  RESET_SIMULATE_DATA,
  SET_SELECTED_ROW_DATA,
  RESET_SELECTED_ROW_DATA,
  SHOW_PRIMARY_POPUP,
  HIDE_PRIMARY_POPUP,
  FETCH_AGREEMENT_ID,
  FETCH_SUMMARY_DATA,
  FETCH_VALID_METHODOLOGIES,
  FETCH_AGREEMENT_TERMS,
  VALIDATE_CALCULATOR_GROUP,
  VALIDATE_AGREEMENT_TERMS,
  VALIDATE_ACCRUAL_POSTING,
  VALIDATE_ACCOUNTING_BOOK,
  SAVE_METHODOLOGY_KEY,
  FETCH_FINANCING_TERM_SUGGESTIONS,
  FETCH_ACCRUAL_POSTING_SUGGESTIONS,
  VALIDATE_WIDGET,
  CLEAR_WIDGET,
  RUN_SIMULATION,
  RESET_ASSET_CLASS_GROUP_BY_ID_DATA,
  FETCH_NETTING_GROUP_DETAIL,
  SAVE_RECONCILIATION_THRESHOLDS_DATA,
  DELETE_RECONCILIATION_THRESHOLDS_DATA,
  RESET_DELETE_RECONCILIATION_THRESHOLDS_DATA,
  RESET_SAVE_RECONCILIATION_THRESHOLDS_DATA,
  DELETE_ADJUSTMENT_ALLOCATION_RULE,
  RESET_ADJUSTMENT_ALLOCATION_RULES,
  RESET_DELETE_ADJUSTMENT_ALLOCATION_RULE,
  IS_USER_AUTHORIZED_FOR_INTEREST_EDIT,
  IS_USER_AUTHORIZED_FOR_INTEREST_POWER_EDIT,
  DISPLAY_ADD_LEAN_SURCHARGE_POPUP,
  DISPLAY_VIEW_LEAN_SURCHARGE_POPUP,
  DESTROY_LEAN_SURCHARGE_POPUP,
  FETCH_LEAN_SURCHARGE_MODEL_BY_ID,
  SAVE_LEAN_SURCHARGE_MODEL,
  RESET_SAVE_LEAN_SURCHARGE_MODEL,
  UPDATE_LEAN_SURCHARGE_MODEL_GRID_DATA
} from "./constants";

export function resetOnboardingUISearchMessage() {
  return {
    type: RESET_ONBOARDING_UI_SEARCH_MESSAGE
  };
}

export function fetchSearchData(payload, view) {
  return {
    type: FETCH_SEARCH_DATA,
    payload,
    view
  };
}

export function fetchSimulateData(payload, view) {
  return {
    type: FETCH_SIMULATE_DATA,
    payload,
    view
  };
}

export function saveSimulateData(payload, view) {
  return {
    type: SAVE_SIMULATE_DATA,
    payload,
    view
  };
}

export function fetchAssetClassGroupById(payload, displayName) {
  return {
    type: FETCH_ASSET_CLASS_GROUP_BY_ID,
    payload,
    displayName
  };
}

export function resetAssetClassGroupByIdData() {
  return {
    type: RESET_ASSET_CLASS_GROUP_BY_ID_DATA
  };
}

export function fetchCalculatorsForCalculatorGroup(payload) {
  return {
    type: FETCH_CALCULATORS_FOR_CALCULATOR_GROUP,
    payload
  };
}

export function fetchAgreementsAffectedForAsset(payload) {
  return {
    type: FETCH_AGREEMENTS_AFFECTED_FOR_ASSET,
    payload
  };
}

export function saveBandWidthGroup(payload) {
  return {
    type: SAVE_BANDWIDTH_GROUP,
    payload
  };
}

export function setSelectedDate(payload) {
  return {
    type: SET_SELECTED_DATE,
    payload
  };
}

export function setView(payload) {
  return {
    type: SET_VIEW,
    payload
  };
}

export function startLoading() {
  return {
    type: START_LOADING
  };
}

export function endLoading() {
  return {
    type: END_LOADING
  };
}

export function resetSaveData() {
  return {
    type: RESET_SAVE_DATA
  };
}

export function resetSimulateData() {
  return {
    type: RESET_SIMULATE_DATA
  };
}

export function resetSaveAssetClassData() {
  return {
    type: RESET_SAVE_ASSET_CLASS
  };
}

export function deleteFinancingTerm(payload) {
  return {
    type: DELETE_FINANCING_TERM,
    payload
  };
}

export function setSelectedRowData(payload) {
  return {
    type: SET_SELECTED_ROW_DATA,
    payload
  };
}

export function resetSelectedRowData() {
  return {
    type: RESET_SELECTED_ROW_DATA
  };
}

export function deleteAccrualPosting(payload) {
  return {
    type: DELETE_ACCRUAL_POSTING,
    payload
  };
}

export function deleteCalculatorGroup(payload) {
  return {
    type: DELETE_CALCULATOR_GROUP,
    payload
  };
}

export function deleteReportingCurrency(payload) {
  return {
    type: DELETE_REPORTING_CURRENCY,
    payload
  };
}

export function deleteBuBundleMapping(payload) {
  return {
    type: DELETE_BU_BUNDLE_MAPPING,
    payload
  };
}

export function resetDeleteTermData() {
  return {
    type: RESET_DELETE_TERM_DATA
  };
}

export function resetAdjustmentAllocationRules() {
  return {
    type: RESET_ADJUSTMENT_ALLOCATION_RULES
  };
}

export function deleteAdjustmentAllocationRule(payload) {
  return {
    type: DELETE_ADJUSTMENT_ALLOCATION_RULE,
    payload
  };
}


export function showPrimaryPopUp(payload) {
  return {
    type: SHOW_PRIMARY_POPUP,
    payload
  };
}

export function hidePrimaryPopUp() {
  return {
    type: HIDE_PRIMARY_POPUP
  };
}

export function resetDeleteAccrualData() {
  return {
    type: RESET_DELETE_ACCRUAL_DATA
  };
}

export function resetDeleteCalculatorData() {
   return {
     type: RESET_DELETE_CALCULATOR_DATA
   };
 }

export function resetDeleteReportingData() {
   return {
     type: RESET_DELETE_REPORTING_DATA
   };
 }

export function resetDeleteBuBundleData() {
   return {
     type: RESET_DELETE_BU_BUNDLE_DATA
   };
 }

export function resetDeleteAdjustmentAllocationRule() {
  return {
    type: RESET_DELETE_ADJUSTMENT_ALLOCATION_RULE
  };
}

export function setAddBandwidthToggle() {
  return {
    type: SET_BANDWIDTH_TOGGLE
  };
}

export function destroyAddBandwidthToggle() {
  return {
    type: DESTROY_BANDWIDTH_TOGGLE
  };
}

export function displayAddLeanSurchargePopup() {
  return {
    type: DISPLAY_ADD_LEAN_SURCHARGE_POPUP
  };
}

export function displayViewLeanSurchargePopup() {
  return {
    type: DISPLAY_VIEW_LEAN_SURCHARGE_POPUP
  };
}

export function destroyLeanSurchargePopup() {
  return {
    type: DESTROY_LEAN_SURCHARGE_POPUP
  };
}

export function saveNettingGroup(payload) {
  return {
    type: SAVE_NETTING_GROUP,
    payload
  };
}

export function saveAssetClass(payload) {
  return {
    type: SAVE_ASSET_CLASS,
    payload
  };
}

export function resetAddNettingGroupResult() {
  return {
    type: RESET_NETTING_GROUP_RESULT
  };
}

export function resetBandwidthGroupData() {
  return {
    type: RESET_BANDWIDTH_GROUP_DATA
  };
}

export function setDeleteToggle() {
  return {
    type: SET_DELETE_TOGGLE
  };
}

export function destroyDeleteToggle() {
  return {
    type: DESTROY_DELETE_TOGGLE
  };
}

export function loadCalculatorSidebar(payload) {
  return {
    type: LOAD_CALCULATOR_SIDEBAR,
    payload
  };
}

export function fetchAgreementId(payload) {
  return {
    type: FETCH_AGREEMENT_ID,
    payload
  };
}

export function fetchSummaryData(date) {
  return {
    type: FETCH_SUMMARY_DATA,
    date
  };
}

export function fetchValidMethodologies(date) {
  return {
    type: FETCH_VALID_METHODOLOGIES,
    date
  };
}

export function fetchAgreementTerms(params) {
  return {
    type: FETCH_AGREEMENT_TERMS,
    params
  };
}

export function validateCalculatorGroup(agreementTerms, calculatorGroupId) {
  let payload = {
    agreementTerms: agreementTerms,
    calculatorGroupId: calculatorGroupId
  };
  return {
    type: VALIDATE_CALCULATOR_GROUP,
    payload
  };
}

export function validateAgreementTerms(agreementTerms, methodologyDataKey, newAgreementTerms, calculatorGroupId) {
  let payload = {
    agreementTerms: agreementTerms,
    newAgreementTerms: newAgreementTerms,
    calculatorGroupId: calculatorGroupId,
    methodologyDataKey: methodologyDataKey
  };
  return {
    type: VALIDATE_AGREEMENT_TERMS,
    payload
  };
}

export function validateAccrualPosting(methodologyDataKey, calculatorGroupId, newRuleMap) {
  let payload = {
    methodologyDataKey: methodologyDataKey,
    calculatorGroupId: calculatorGroupId,
    newAccrualPostingRuleMap: newRuleMap
  };
  return {
    type: VALIDATE_ACCRUAL_POSTING,
    payload
  };
}

export function validateAccountingBook(methodologyDataKey, accountingBookRule) {
  let payload = {
    methodologyDataKey: methodologyDataKey,
    accountingBookRule: accountingBookRule
  };
  return {
    type: VALIDATE_ACCOUNTING_BOOK,
    payload
  };
}

export function validateWidget(methodologyDataKey, agreementTerms, newAgreementTerms, calculatorGroupId,
  accountingBookRule, newAccrualPostingRuleMap) {
  let payload = {
    methodologyDataKey: methodologyDataKey,
    agreementTerms: agreementTerms,
    newAgreementTerms: newAgreementTerms,
    calculatorGroupId: calculatorGroupId,
    accountingBookRule: accountingBookRule,
    newAccrualPostingRuleMap: newAccrualPostingRuleMap
  }
  return {
    type: VALIDATE_WIDGET,
    payload
  };

}

export function saveMethodologyKey(payload) {
  let methodologyDataKey = {
    '@CLASS': 'com.arcesium.treasury.financing.builder.key.MethodologyDataKey',
    'date': payload["validFrom"],
    'methodologyKey': {
      '@CLASS': 'com.arcesium.treasury.financing.builder.key.MethodologyKey',
      'legalEntityId': payload["legalEntityIds"][0],
      'cpeId': payload["cpeIds"][0],
      'agreementTypeId': payload["fundingTypeIds"][0],
      'currencyId': payload["currencyIds"][0],
      'nettingGroupId': payload["nettingGroupIds"][0],
    }
  }
  return {
    type: SAVE_METHODOLOGY_KEY,
    methodologyDataKey
  };

}

export function fetchFinancingTermsSuggestions(calculatorGroupId) {
  return {
    type: FETCH_FINANCING_TERM_SUGGESTIONS,
    calculatorGroupId
  };
}

export function fetchAccrualPostingSuggestions(calculatorGroupId) {
  return {
    type: FETCH_ACCRUAL_POSTING_SUGGESTIONS,
    calculatorGroupId
  };
}

export function clearPopUp() {
  return {
    type: CLEAR_WIDGET
  };
}

export function runSimulation(date) {
  return {
    type: RUN_SIMULATION,
    date
  };
}

export function fetchNettingGroupDetail(payload) {
  return {
    type: FETCH_NETTING_GROUP_DETAIL,
    payload
  };
}

export function saveReconciliationThresholdsData(payload) {
  return {
    type: SAVE_RECONCILIATION_THRESHOLDS_DATA,
    payload
  };
}


export function deleteReconciliationThresholdsData(payload) {
  return {
    type: DELETE_RECONCILIATION_THRESHOLDS_DATA,
    payload
  };
}

export function resetSaveReconciliationThresholdsData() {
  return {
    type: RESET_SAVE_RECONCILIATION_THRESHOLDS_DATA
  };
}


export function resetDeleteReconciliationThresholdsData() {
  return {
    type: RESET_DELETE_RECONCILIATION_THRESHOLDS_DATA
  };
}

export function  isUserAuthorizedForInterestEdit() {
  return {
    type: IS_USER_AUTHORIZED_FOR_INTEREST_EDIT
  };
}

export function isUserAuthorizedForInterestPowerEdit() {
  return {
    type: IS_USER_AUTHORIZED_FOR_INTEREST_POWER_EDIT
  };
}

export function fetchLeanSurchargeModelById(payload) {
  return {
    type: FETCH_LEAN_SURCHARGE_MODEL_BY_ID,
    payload
  };
}

export function updateLeanSurchargeGridData(payload) {
  return {
    type: UPDATE_LEAN_SURCHARGE_MODEL_GRID_DATA,
    payload
  };
}

export function saveLeanSurchargeModel(payload) {
  return {
    type: SAVE_LEAN_SURCHARGE_MODEL,
    payload
  };
}

export function resetSaveLeanSurchargeModel() {
  return {
    type: RESET_SAVE_LEAN_SURCHARGE_MODEL
  };
}
