import React from "react";
import _ from "lodash";
import { convertJavaDate } from "commons/util";

const screen = {
  CALCULATOR: "CALCULATOR_GROUP",
  FINANCING: "FINANCING_AGREEMENT_TERMS",
  ASSET: "ASSET_CLASS_GROUP",
  REPORTING: "REPORTING_CURRENCY",
  ACCRUAL: "ACCRUAL_POSTING",
  NETTING: "NETTING_GROUP_MAPPING",
  BUBUNDLE: "BU_BUNDLE_MAPPING",
  GENEVABOOK: "GENEVA_BOOK_MAPPING",
  SUMMARY: "SUMMARY",
  WIDGET: "WIDGET",
  ADJUSTMENT: "ADJUSTMENT_ALLOCATION_RULES",
  RECONCILIATION_THRESHOLDS: "RECONCILIATION_THRESHOLDS"
};

export const onboardingScreen = Object.freeze(screen);

const financingTermType = {
  AGREEMENT_LEVEL: "Methodology Term",
  ASSET_CLASS_LEVEL: "Methodology Asset Class Group Term",
  ACCOUNT_LEVEL: "Custodian Account Term",
  ACCOUNT_ASSET_CLASS_LEVEL: "Custodian Account Asset Class Group Term"
}

export const financingAgreementTermType = Object.freeze(financingTermType);

// Fetch Key from Single Select Data Object
export function getValueForSingleSelect(data) {
  if (typeof data === "undefined" || data == null || data.length === 0 || data.key === -1)
    return "-1";
  const returnValue = data.key;
  return returnValue;
}

// Fetch Value from Single Select Data Object
export function getDataValueForSingleSelect(data) {
  if (typeof data === "undefined" || data == null || data.length === 0 || data.value === -1)
    return "";
  const returnValue = data.value;
  return returnValue;
}

export function isFinancingTermTypeAgreement(data) {
  return (typeof data !== "undefined" && data !== null && data === financingAgreementTermType.AGREEMENT_LEVEL)
}


export function isFinancingTermTypeAsset(data) {
  return (typeof data !== "undefined" && data !== null &&
    (data === financingAgreementTermType.ASSET_CLASS_LEVEL || data === financingAgreementTermType.ACCOUNT_ASSET_CLASS_LEVEL))
}

export function isFinancingTermTypeAccount(data) {
  return (typeof data !== "undefined" && data !== null &&
    (data === financingAgreementTermType.ACCOUNT_LEVEL || data === financingAgreementTermType.ACCOUNT_ASSET_CLASS_LEVEL))
}

export function getValueForSingleSelectOrNull(data) {
  if (typeof data === "undefined" || data == null || data.length === 0 || data.key === -1)
    return "__null__";
  const returnValue = data.key;
  return returnValue;
}

export function getZeroForDefault(data) {
  if (typeof data !== "undefined" && data !== null && data.length === 0)
    return "0";
  return data;
}

export function getCommaSeparatedValuesOrUndefined(data) {
  let returnValue = _.join(
    _.map(data, e => e.key),
    ","
  );
  if (returnValue === "") return undefined;
  return returnValue;
}

export function getValueForSingleSelectOrUndefined(data) {
  if (typeof data === "undefined" || data === null
        || data.length === 0 || data.key === -1)
    return undefined;
  const returnValue = data.key;
  return returnValue;
}

export function getErrorNote(error,message){
  if(error)
   return (
    <div class="text-align--right">
      <span class="red margin--small text-align--right">{message}</span>
    </div>
   )
  else
   return;
}

export function getDisabledFilter(label, value) {
  return (
    <div className="form-field--split">
      <div className="padding--right--large">{label}</div>
      <div class="text-align--left">{value}</div>
    </div>
  );
}

export function getColourCode(value, minWidth, maxWidth) {
  if (value === financingAgreementTermType.AGREEMENT_LEVEL) {
    return '<span class="token blue4 size--small" style="max-width:'+maxWidth+'px; min-width:'+minWidth+'px; text-align: left;" title='+'"'+value+'"'+'>' + value + '</span>';
  } else if (value === financingAgreementTermType.ASSET_CLASS_LEVEL) {
    return '<span class="token blue8 size--small" style="max-width:'+maxWidth+'px; min-width:'+minWidth+'px; text-align: left;" title='+'"'+value+'"'+'>' + value + '</span>';
  } else if (value === financingAgreementTermType.ACCOUNT_LEVEL) {
    return '<span class="token yellow4 size--small" style="max-width:'+maxWidth+'px; min-width:'+minWidth+'px; text-align: left;" title='+'"'+value+'"'+'>' + value + '</span>';
  }  else {
    return '<span class="token yellow8 size--small" style="max-width:'+maxWidth+'px; min-width:'+minWidth+'px; text-align: left;" title='+'"'+value+'"'+'>' + value + '</span>';
  }
}

export const ErrorMapping = {
  "[INVALID_METHODOLOGY]": screen["GENEVABOOK"],
  "[INCORRECT_CALCULATOR_GROUP]": screen["CALCULATOR"],
  "[INVALID_AGREEMENT_TERMS_MISSING_TSIDS]": screen["FINANCING"],
  "[ACCOUNT_LEVEL_TERMS_NOT_DEFINED]": screen["FINANCING"],
  "[MISSING_ACCOUNT_LEVEL_TERMS]": screen["FINANCING"],
  "[MISSING_DEFAULT_AGREEMENT_TERM]": screen["FINANCING"],
  "[INCORRECT_ACCRUAL_POSTING_RULE]": screen["ACCRUAL"],
  "[MISSING_AGREEMENT_TERMS_FIELDS]": screen["FINANCING"],
  "[NO_ASSET_CLASS_GROUP_FOR_PNLSPN]": screen["FINANCING"],
  "[AGREEMENT_TERMS_NOT_DEFINED]": screen["FINANCING"],
  "[GC_RATE_NOT_DEFINED]": screen["FINANCING"],
  "[GENEVA_BOOK_NOT_DEFINED]": screen["GENEVABOOK"],
  "[CALCULATOR_GROUP_NOT_DEFINED]": screen["CALCULATOR"],
  "[FINANCING_STYLE_NOT_DEFINED]": screen["NETTING"],
  "[AGREEMENT_TYPE_INVALID]": screen["NETTING"],
  "[CA_NETTING_GROUP_NOT_DEFINED]": screen["NETTING"],
  "[DEFAULT_BUNDLE_ID_NOT_DEFINED]": screen["ACCRUAL"]
};

export function formatData(data) {
  let counter = 1;
  data = _.forEach(data, dataObj => {
    dataObj["id"] = counter++;
    if (dataObj["date"] != null && dataObj["date"] != undefined)
      dataObj["date"] = convertJavaDate(dataObj["date"]);
    else dataObj["date"] = null;
  });
  let mappedData = _.chain(data)
    .filter(filterBy => filterBy.errorType !== "[INVALID_AGREEMENT]")
    .groupBy(dataObj => ErrorMapping[dataObj.errorType])
    .value();
  return mappedData;
}

export function formatStatsData(validMethodologies, summaryData) {
  _.forEach(
    validMethodologies,
    methodology =>
      (methodology[
        "key"
      ] = `${methodology.agreement.agreementTypeId.toString() +
      methodology.agreement.dESCOEntityId.toString() +
      methodology.agreement.cpeId.toString() +
      methodology.nettingGroupId$delegate.value.toString() +
      methodology.currencyId$delegate.value.toString()}`)
  );

  _.forEach(
    summaryData,
    methodology =>
      (methodology["key"] = `${methodology.agreementTypeId.toString() +
        methodology.legalEntityId.toString() +
        methodology.cpeId.toString() +
        methodology.nettingGroupId.toString() +
        methodology.currencyId.toString()}`)
  );

  let groupedSummary = _.groupBy(
    summaryData,
    methodology => ErrorMapping[methodology.errorType]
  );

  let statsData = {};
  statsData[screen["CALCULATOR"]] =
    groupedSummary[screen["CALCULATOR"]] !== undefined &&
      groupedSummary[screen["CALCULATOR"]].length > 0
      ? groupedSummary[screen["CALCULATOR"]].length
      : 0;

  statsData[screen["FINANCING"]] =
    groupedSummary[screen["FINANCING"]] !== undefined &&
      groupedSummary[screen["FINANCING"]].length > 0
      ? groupedSummary[screen["FINANCING"]].length
      : 0;

  statsData[screen["REPORTING"]] =
    groupedSummary[screen["REPORTING"]] !== undefined &&
      groupedSummary[screen["REPORTING"]].length > 0
      ? groupedSummary[screen["REPORTING"]].length
      : 0;

  statsData[screen["ACCRUAL"]] =
    groupedSummary[screen["ACCRUAL"]] !== undefined &&
      groupedSummary[screen["ACCRUAL"]].length > 0
      ? groupedSummary[screen["ACCRUAL"]].length
      : 0;

  statsData[screen["NETTING"]] =
    groupedSummary[screen["NETTING"]] !== undefined &&
      groupedSummary[screen["NETTING"]].length > 0
      ? groupedSummary[screen["NETTING"]].length
      : 0;

  statsData[screen["GENEVABOOK"]] =
    groupedSummary[screen["GENEVABOOK"]] !== undefined &&
      groupedSummary[screen["GENEVABOOK"]].length > 0
      ? groupedSummary[screen["GENEVABOOK"]].length
      : 0;
  let groupedSummaryByKey = _.groupBy(
    summaryData,
    methodology => methodology.key
  );
  let groupedMethodologiesByKey = _.groupBy(
    validMethodologies,
    methodology => methodology.key
  );
  let methodolgiesWithErrors =
    Object.keys(groupedSummaryByKey) !== undefined &&
      Object.keys(groupedSummaryByKey).length > 0
      ? Object.keys(groupedSummaryByKey).length
      : 0;

  let methodolgies =
    Object.keys(groupedMethodologiesByKey) !== undefined &&
      Object.keys(groupedMethodologiesByKey).length > 0
      ? Object.keys(groupedMethodologiesByKey).length
      : 0;
  statsData[screen["SUMMARY"]] = methodolgiesWithErrors;
  return { statsData, totalMethodologies: methodolgies };
}

export function getMonthEndDate(monthStr) {
  var month = monthStr.split('/')[0];
  var year = monthStr.split('/')[1];
  return new Date(year,month,0);
}

export function getMonthStartDate(monthStr) {
  var month = monthStr.split('/')[0];
  var year = monthStr.split('/')[1];
  return new Date(year,month-1,1);
}
