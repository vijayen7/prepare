import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from "commons/constants";
import {
  UPDATE_ONBOARDING_UI_SEARCH_MESSAGE,
  FETCH_SEARCH_DATA,
  FETCH_SIMULATE_DATA,
  SAVE_SIMULATE_DATA,
  SAVE_BANDWIDTH_GROUP,
  FETCH_LEAN_SURCHARGE_MODEL_BY_ID,
  SAVE_LEAN_SURCHARGE_MODEL,
  FETCH_ASSET_CLASS_GROUP_BY_ID,
  SAVE_NETTING_GROUP,
  FETCH_CALCULATORS_FOR_CALCULATOR_GROUP,
  DELETE_FINANCING_TERM,
  DELETE_ACCRUAL_POSTING,
  DELETE_CALCULATOR_GROUP,
  DELETE_BU_BUNDLE_MAPPING,
  DELETE_REPORTING_CURRENCY,
  SAVE_ASSET_CLASS,
  FETCH_AGREEMENTS_AFFECTED_FOR_ASSET,
  FETCH_AGREEMENT_ID,
  FETCH_SUMMARY_DATA,
  FETCH_VALID_METHODOLOGIES,
  FETCH_AGREEMENT_TERMS,
  VALIDATE_CALCULATOR_GROUP,
  VALIDATE_AGREEMENT_TERMS,
  VALIDATE_ACCRUAL_POSTING,
  VALIDATE_ACCOUNTING_BOOK,
  FETCH_FINANCING_TERM_SUGGESTIONS,
  FETCH_ACCRUAL_POSTING_SUGGESTIONS,
  VALIDATION_ERROR,
  VALIDATE_WIDGET,
  RUN_SIMULATION,
  FETCH_NETTING_GROUP_DETAIL,
  FETCH_ADJUSTMENT_ALLOCATION_RULES,
  ADD_ADJUSTMENT_ALLOCATION_RULES,
  UPDATE_ADJUSTMENT_ALLOCATION_RULES,
  DELETE_ADJUSTMENT_ALLOCATION_RULE,
  SAVE_RECONCILIATION_THRESHOLDS_DATA,
  DELETE_RECONCILIATION_THRESHOLDS_DATA,
  IS_USER_AUTHORIZED_FOR_INTEREST_EDIT,
  IS_USER_AUTHORIZED_FOR_INTEREST_POWER_EDIT
} from "./constants";
import {
  getSearchData,
  getSimulateData,
  fetchSaveSimulateData,
  saveBandWidthGroup,
  saveLeanSurchargeModel,
  fetchLeanSurchargeModelById,
  getAssetClassGroupById,
  saveNettingGroupApi,
  getCalculatorsForCalculatorGroupId,
  deleteFinancingTerm,
  deleteAccrualPosting,
  deleteCalculatorGroup,
  deleteReportingCurrency,
  deleteBuBundleMapping,
  saveAssetClassApi,
  getAgreementsAffectedForAssetId,
  getAgreementIdApi,
  getSummaryData,
  getValidMethodologies,
  getAgreementTerms,
  getValidateCalculatorGroupApi,
  getValidateAgreementTermsApi,
  getValidateAccrualPostingApi,
  getValidateAccountingBookApi,
  getFinancingTermSuggestionsApi,
  getAccrualPostingSuggestionsApi,
  getvalidateWidgetApi,
  getValidateAssetClassApi,
  getRunSimulationApi,
  getNettingGroupDetailApi,
  deleteAdjustmentAllocationRuleApi,
  saveReconciliationThresholdsApi,
  deleteReconciliationThresholdsApi,
  interestEditUserAuth,
  interestPowerEditUserAuth
} from "./api";
import { ToastService } from "arc-react-components";

import { onboardingScreen } from "./util";

function* fetchSearchData(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(getSearchData, action.payload, action.view);
    yield put({
      type: `FETCH_${action.view}_DATA_SUCCESS`,
      data
    });
    yield put({ type: UPDATE_ONBOARDING_UI_SEARCH_MESSAGE });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `FETCH_${action.view}_DATA_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSimulateData(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(getSimulateData, action.payload, action.view);
    if (action.view === onboardingScreen.ASSET) data.ruleList = data.resultList;
    yield put({
      type: `${FETCH_SIMULATE_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SIMULATE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveSimulateData(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(fetchSaveSimulateData, action.payload, action.view);
    yield put({
      type: `${SAVE_SIMULATE_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_SIMULATE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveBandwidthGroup(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(saveBandWidthGroup, action.payload);
    yield put({
      type: `${SAVE_BANDWIDTH_GROUP}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_BANDWIDTH_GROUP}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveLeanSurchargeModelSaga(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(saveLeanSurchargeModel, action.payload);
    yield put({
      type: `${SAVE_LEAN_SURCHARGE_MODEL}_SUCCESS`,
      data
    });
    ToastService.append({
      content: data,
      type: data.includes("success") ? ToastService.ToastType.SUCCESS : ToastService.ToastType.CRITICAL,
      placement: ToastService.Placement.BOTTOM_RIGHT,
      dismissTime: 7000,
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_LEAN_SURCHARGE_MODEL}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchLeanSurchargeModelSaga(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(fetchLeanSurchargeModelById, action.payload);
    yield put({
      type: `${FETCH_LEAN_SURCHARGE_MODEL_BY_ID}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LEAN_SURCHARGE_MODEL_BY_ID}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deleteFinancingTermSaga(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(deleteFinancingTerm, action.payload);
    yield put({
      type: `${DELETE_FINANCING_TERM}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_FINANCING_TERM}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deleteAccrualPostingSaga(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(deleteAccrualPosting, action.payload);
    yield put({
      type: `${DELETE_ACCRUAL_POSTING}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_ACCRUAL_POSTING}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deleteCalculatorGroupSaga(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(deleteCalculatorGroup, action.payload);
    yield put({
      type: `${DELETE_CALCULATOR_GROUP}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_CALCULATOR_GROUP}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deleteReportingCurrencySaga(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(deleteReportingCurrency, action.payload);
    yield put({
      type: `${DELETE_REPORTING_CURRENCY}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_REPORTING_CURRENCY}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deleteBuBundleMappingSaga(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(deleteBuBundleMapping, action.payload);
    yield put({
      type: `${DELETE_BU_BUNDLE_MAPPING}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_BU_BUNDLE_MAPPING}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deleteAdjustmentAllocationRuleSaga(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(deleteAdjustmentAllocationRuleApi, action.payload);
    yield put({
      type: `${DELETE_ADJUSTMENT_ALLOCATION_RULE}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_ADJUSTMENT_ALLOCATION_RULE}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveNettingGroup(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(saveNettingGroupApi, action.payload);
    yield put({
      type: `${SAVE_NETTING_GROUP}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_NETTING_GROUP}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchAssetClassGroupById(action) {
  let data = [];
  const result = {};
  result.name = action.displayName;
  try {
    yield put({ type: START_LOADING });
    data = yield call(getAssetClassGroupById, action.payload);
    result.data = data;
    yield put({
      type: `${FETCH_ASSET_CLASS_GROUP_BY_ID}_SUCCESS`,
      result
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_ASSET_CLASS_GROUP_BY_ID}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* getCalculatorsForCalculatorGroup(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(getCalculatorsForCalculatorGroupId, action.payload);
    yield put({
      type: `${FETCH_CALCULATORS_FOR_CALCULATOR_GROUP}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CALCULATORS_FOR_CALCULATOR_GROUP}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* getNettingGroupDetail(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(getNettingGroupDetailApi, action.payload);
    yield put({
      type: `${FETCH_NETTING_GROUP_DETAIL}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_NETTING_GROUP_DETAIL}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* getAgreementsAffectedForAsset(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(getAgreementsAffectedForAssetId, action.payload);
    yield put({
      type: `${FETCH_AGREEMENTS_AFFECTED_FOR_ASSET}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_AGREEMENTS_AFFECTED_FOR_ASSET}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveAssetClass(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    var payload = action.payload;
    var attrString = "";
    for (var i = 0; i < payload.attributes.length; i++) {
      attrString =
        attrString +
        `sourceType:${payload.attributes[i].sourceType},attrName:${payload.attributes[i].attrName},attrValue:${payload.attributes[i].attrValue};`;
    }
    payload.attributes = attrString;
    data = yield call(saveAssetClassApi, payload);
    yield put({
      type: `${SAVE_ASSET_CLASS}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_ASSET_CLASS}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveReconciliationThresholds(action) {
  let data = [];
  try {
    yield put({type: START_LOADING});
    data = yield call(saveReconciliationThresholdsApi, action.payload);
    yield put({type: `${SAVE_RECONCILIATION_THRESHOLDS_DATA}_SUCCESS`,data});

  } catch(e) {
    yield put({type: HANDLE_EXCEPTION});
    yield put({type: `${SAVE_RECONCILIATION_THRESHOLDS_DATA}_FAILURE`});
  } finally {
    yield put({type: END_LOADING});
  }
}


function* deleteReconciliationThresholds(action) {
  let data = [];
  try {
    yield put({type: START_LOADING});
    data = yield call(deleteReconciliationThresholdsApi, action.payload);
    yield put({type: `${DELETE_RECONCILIATION_THRESHOLDS_DATA}_SUCCESS`,data});
  } catch(e) {
    yield put({type: HANDLE_EXCEPTION});
    yield put({type: `${DELETE_RECONCILIATION_THRESHOLDS_DATA}_FAILURE`});
  } finally {
    yield put({type: END_LOADING});
  }
}

function* getAgreementId(action) {
  try {
    yield put({ type: START_LOADING });
    let data = yield call(getAgreementIdApi, action.payload);
    yield put({
      type: `${FETCH_AGREEMENT_ID}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_AGREEMENT_ID}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* getValidateCalculatorGroup(action) {
  try {
    let data = yield call(getValidateCalculatorGroupApi, action.payload);
    yield put({
      type: VALIDATION_ERROR,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${VALIDATE_CALCULATOR_GROUP}_FAILURE` });
  }
}

function* getvalidateAgreementTerms(action) {
  try {
    yield put({ type: START_LOADING });
    let agreementTermsdata = yield call(getValidateAgreementTermsApi, action.payload);
    let assetClassData = yield call(getValidateAssetClassApi, action.payload);
    let data = null;
    if (agreementTermsdata != null && assetClassData != null)
      data = [agreementTermsdata, assetClassData];
    else if (agreementTermsdata != null)
      data = agreementTermsdata;
    else if (assetClassData)
      data = assetClassData
    else
      data = null;
    yield put({
      type: VALIDATION_ERROR,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${VALIDATE_AGREEMENT_TERMS}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* getvalidateAccrualPosting(action) {
  try {
    let data = yield call(getValidateAccrualPostingApi, action.payload);
    yield put({
      type: VALIDATION_ERROR,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${VALIDATE_ACCRUAL_POSTING}_FAILURE` });
  }
}

function* getvalidateAccountingBook(action) {
  try {
    let data = yield call(getValidateAccountingBookApi, action.payload);
    yield put({
      type: VALIDATION_ERROR,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${VALIDATE_ACCOUNTING_BOOK}_FAILURE` });
  }
}

function* getvalidateWidget(action) {
  try {
    yield put({ type: START_LOADING });
    let data = yield call(getvalidateWidgetApi, action.payload);
    yield put({
      type: VALIDATION_ERROR,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${VALIDATE_WIDGET}_FAILURE` });
  }
  finally {
    yield put({ type: END_LOADING });
  }
}

function* getSummaryDataList(action) {
  try {
    let data = yield call(getSummaryData, action.date);
    yield put({
      type: `${FETCH_SUMMARY_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SUMMARY_DATA}_FAILURE` });
  }
}

function* getValidMethodologiesList(action) {
  try {
    let data = yield call(getValidMethodologies, action.date);
    yield put({
      type: `${FETCH_VALID_METHODOLOGIES}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_VALID_METHODOLOGIES}_FAILURE` });
  }
}

function* getAgreementTermsList(action) {
  try {
    let data = yield call(getAgreementTerms, action.params);
    yield put({
      type: `${FETCH_AGREEMENT_TERMS}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_AGREEMENT_TERMS}_FAILURE` });
  }
}

function* getFinancingTermsSuggestions(action) {
  try {
    let data = yield call(getFinancingTermSuggestionsApi, action.calculatorGroupId);
    yield put({
      type: `${FETCH_FINANCING_TERM_SUGGESTIONS}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_FINANCING_TERM_SUGGESTIONS}_FAILURE` });
  }
}

function* getAccrualPostingSuggestions(action) {
  try {
    let data = yield call(getAccrualPostingSuggestionsApi, action.calculatorGroupId);
    yield put({
      type: `${FETCH_ACCRUAL_POSTING_SUGGESTIONS}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_ACCRUAL_POSTING_SUGGESTIONS}_FAILURE` });
  }
}

function* runSimulationApi(action) {
  try {
    let data = yield call(getRunSimulationApi, action.date);
    ToastService.append({
      content: data,
      type: data.includes("success") ? ToastService.ToastType.SUCCESS : ToastService.ToastType.CRITICAL,
      placement: ToastService.Placement.BOTTOM_RIGHT,
      dismissTime: 60000,
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${RUN_SIMULATION}_FAILURE` });
  }
}

function* getInterestEditUserAuth(action) {
  try {
    let data = yield call(interestEditUserAuth);
    yield put({
      type: `${IS_USER_AUTHORIZED_FOR_INTEREST_EDIT}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${IS_USER_AUTHORIZED_FOR_INTEREST_EDIT}_FAILURE` });
  }
}

function* getInterestPowerEditUserAuth(action) {
  try {
    let data = yield call(interestPowerEditUserAuth);
    yield put({
      type: `${IS_USER_AUTHORIZED_FOR_INTEREST_POWER_EDIT}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${IS_USER_AUTHORIZED_FOR_INTEREST_POWER_EDIT}_FAILURE` });
  }
}

export function* saveAssetClassData() {
  yield [takeEvery(SAVE_ASSET_CLASS, saveAssetClass)];
}

export function* saveReconciliationThresholdsData() {
  yield [takeEvery(SAVE_RECONCILIATION_THRESHOLDS_DATA, saveReconciliationThresholds)];
}

export function* deleteReconciliationThresholdsData() {
  yield [takeEvery(DELETE_RECONCILIATION_THRESHOLDS_DATA, deleteReconciliationThresholds)];
}

export function* searchData() {
  yield [takeEvery(FETCH_SEARCH_DATA, fetchSearchData)];
}

export function* saveBandwidthGroupData() {
  yield [takeEvery(SAVE_BANDWIDTH_GROUP, saveBandwidthGroup)];
}

export function* saveLeanSurchargeModelData() {
  yield [takeEvery(SAVE_LEAN_SURCHARGE_MODEL, saveLeanSurchargeModelSaga)];
}

export function* fetchLeanSurchargeModelByIdData() {
  yield [takeEvery(FETCH_LEAN_SURCHARGE_MODEL_BY_ID, fetchLeanSurchargeModelSaga)];
}
export function* saveNettingGroupData() {
  yield [takeEvery(SAVE_NETTING_GROUP, saveNettingGroup)];
}

export function* deleteFinancingTermData() {
  yield [takeEvery(DELETE_FINANCING_TERM, deleteFinancingTermSaga)];
}

export function* deleteAccrualPostingData() {
  yield [takeEvery(DELETE_ACCRUAL_POSTING, deleteAccrualPostingSaga)];
}

export function* deleteCalculatorGroupData() {
  yield [takeEvery(DELETE_CALCULATOR_GROUP, deleteCalculatorGroupSaga)];
}

export function* deleteReportingCurrencyData() {
  yield [takeEvery(DELETE_REPORTING_CURRENCY, deleteReportingCurrencySaga)];
}

export function* deleteBuBundleMappingData() {
  yield [takeEvery(DELETE_BU_BUNDLE_MAPPING, deleteBuBundleMappingSaga)];
}

export function* deleteAdjustmentAllocationRule() {
  yield [takeEvery(DELETE_ADJUSTMENT_ALLOCATION_RULE, deleteAdjustmentAllocationRuleSaga)];
}

export function* simulateData() {
  yield [takeEvery(FETCH_SIMULATE_DATA, fetchSimulateData)];
}

export function* saveData() {
  yield [takeEvery(SAVE_SIMULATE_DATA, saveSimulateData)];
}

export function* fetchAssetClassGroupDataById() {
  yield [takeEvery(FETCH_ASSET_CLASS_GROUP_BY_ID, fetchAssetClassGroupById)];
}

export function* fetchCalculatorsForCalculatorGroup() {
  yield [
    takeEvery(
      FETCH_CALCULATORS_FOR_CALCULATOR_GROUP,
      getCalculatorsForCalculatorGroup
    )
  ];
}

export function* fetchAgreementsAffectedForAsset() {
  yield [
    takeEvery(
      FETCH_AGREEMENTS_AFFECTED_FOR_ASSET,
      getAgreementsAffectedForAsset
    )
  ];
}

export function* fetchAgreementId() {
  yield [takeEvery(FETCH_AGREEMENT_ID, getAgreementId)];
}

export function* validateCalculatorGroup() {
  yield [takeEvery(VALIDATE_CALCULATOR_GROUP, getValidateCalculatorGroup)];
}

export function* validateAgreementTerms() {
  yield [takeEvery(VALIDATE_AGREEMENT_TERMS, getvalidateAgreementTerms)];
}

export function* validateAccrualPosting() {
  yield [takeEvery(VALIDATE_ACCRUAL_POSTING, getvalidateAccrualPosting)];
}

export function* validateAccountingBook() {
  yield [takeEvery(VALIDATE_ACCOUNTING_BOOK, getvalidateAccountingBook)];
}

export function* validateAccountingWidget() {
  yield [takeEvery(VALIDATE_WIDGET, getvalidateWidget)];
}

export function* fetchSummaryData() {
  yield [takeEvery(FETCH_SUMMARY_DATA, getSummaryDataList)];
}

export function* fetchValidMethodologies() {
  yield [takeEvery(FETCH_VALID_METHODOLOGIES, getValidMethodologiesList)];
}

export function* fetchAgreementTerms() {
  yield [takeEvery(FETCH_AGREEMENT_TERMS, getAgreementTermsList)];
}

export function* fetchFinancingTermsSuggestions() {
  yield [takeEvery(FETCH_FINANCING_TERM_SUGGESTIONS, getFinancingTermsSuggestions)];
}

export function* fetchAccrualPostingSuggestions() {
  yield [takeEvery(FETCH_ACCRUAL_POSTING_SUGGESTIONS, getAccrualPostingSuggestions)];
}

export function* runSimulation() {
  yield [takeEvery(RUN_SIMULATION, runSimulationApi)];
}

export function* fetchInterestEditUserAuth() {
  yield [takeEvery(  IS_USER_AUTHORIZED_FOR_INTEREST_EDIT, getInterestEditUserAuth)];
}

export function* fetchInterestPowerEditUserAuth() {
  yield [takeEvery(  IS_USER_AUTHORIZED_FOR_INTEREST_POWER_EDIT,getInterestPowerEditUserAuth)];
}


export function* fetchNettingGroupDetail() {
  yield [
    takeEvery(
      FETCH_NETTING_GROUP_DETAIL,
      getNettingGroupDetail
    )
  ];
}


function* onboardingUISaga() {
  yield all([
    searchData(),
    simulateData(),
    saveData(),
    saveBandwidthGroupData(),
    saveLeanSurchargeModelData(),
    fetchLeanSurchargeModelByIdData(),
    saveNettingGroupData(),
    fetchAssetClassGroupDataById(),
    fetchCalculatorsForCalculatorGroup(),
    deleteFinancingTermData(),
    deleteAccrualPostingData(),
    deleteCalculatorGroupData(),
    deleteReportingCurrencyData(),
    deleteBuBundleMappingData(),
    saveAssetClassData(),
    saveReconciliationThresholdsData(),
    deleteReconciliationThresholdsData(),
    fetchAgreementsAffectedForAsset(),
    fetchAgreementId(),
    validateCalculatorGroup(),
    fetchSummaryData(),
    fetchValidMethodologies(),
    fetchAgreementTerms(),
    validateAgreementTerms(),
    validateAccrualPosting(),
    validateAccountingBook(),
    fetchFinancingTermsSuggestions(),
    fetchAccrualPostingSuggestions(),
    validateAccountingWidget(),
    runSimulation(),
    fetchNettingGroupDetail(),
    deleteAdjustmentAllocationRule(),
    fetchInterestEditUserAuth(),
    fetchInterestPowerEditUserAuth()
  ]);
}

export default onboardingUISaga;
