import React from "react";
import Dialog from "commons/components/Dialog";
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { onboardingScreen } from "../../util";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CalculatorGroupPopUpContent from "../container/CalculatorGroupPopUpContent";
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData,
  resetDeleteCalculatorData
} from '../../actions';

class CalculatorGroupPopUp extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClosePopUp = () => {

    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();

  }

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
    this.props.resetDeleteCalculatorData();

  }

  render() {
    let message = null;
    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = (
            <Message messageData="Error occurred while saving rules" error />
          ));
    } else if (!_.isEmpty(this.props.deleteCalculatorGroupData)) {
      !this.props.deleteCalculatorGroupData.errorMessage
        ? (message = <Message messageData="Calculator Group Deleted" success />)
        : (message = (
            <Message messageData="Error deleting Calculator Group" error />
          ));
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.CALCULATOR &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          onClose={this.handleClosePopUp}
          style={{
            width: "1080px"
          }}
        >
          <CalculatorGroupPopUpContent />
        </Dialog>
        <MessageDialog
          isOpen={
            this.props.view === onboardingScreen.CALCULATOR &&
            (!_.isEmpty(this.props.saveSimulatedData) ||
             !_.isEmpty(this.props.deleteCalculatorGroupData))
          }
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData,
      resetDeleteCalculatorData,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    deleteCalculatorGroupData: state.financingOnboarding.deleteCalculatorGroupData,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CalculatorGroupPopUp);
