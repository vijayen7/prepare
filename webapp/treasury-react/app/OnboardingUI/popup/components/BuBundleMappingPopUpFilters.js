import React from 'react';
import { Layout } from 'arc-react-components';
import CpeFilter from 'commons/container/CpeFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import LegalEntityFilter from 'commons/container/LegalEntityFilter';
import DateFilter from 'commons/container/DateFilter';
import NettingGroupFilter from 'commons/container/NettingGroupFilter';
import BusinessUnitFilter from 'commons/container/BusinessUnitFilter';
import BundleFilter from 'commons/container/BundleFilter';
import { getDisabledFilter, getErrorNote as ErroNote } from '../../util';

const BuBundleMappingPopUpFilters = (props) => {
  return (
    <Layout isRowType>
      <Layout.Child childId="buBundleMappingPopup1">
        {props.isEditClicked ? (
          getDisabledFilter('Legal Entity:', props.state.selectedLegalEntities.value)
        ) : (
          <LegalEntityFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedLegalEntities}
            multiSelect={false}
            errorDiv={props.state.error.selectedLegalEntities}
            horizontalLayout
          />
        )}
        {ErroNote(props.state.error.selectedLegalEntities, "Please select a legal entity")}
        {props.isEditClicked ? (
          getDisabledFilter('CPEs:', props.state.selectedCpes.value)
        ) : (
          <CpeFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCpes}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Agreement Type:', props.state.selectedAgreementTypes.value)
        ) : (
          <AgreementTypeFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedAgreementTypes}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Netting Groups:', props.state.selectedNettingGroups.value)
        ) : (
          <NettingGroupFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedNettingGroups}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Currencies:', props.state.selectedCurrencies.value)
        ) : (
          <CurrencyFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCurrencies}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Business Units:', props.state.selectedBusinessUnits.value)
        ) : (
          <BusinessUnitFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedBusinessUnits}
            multiSelect={false}
            errorDiv={props.state.error.selectedBusinessUnits}
            horizontalLayout
          />
        )}
        {ErroNote(props.state.error.selectedBusinessUnits, "Please select a business unit")}
        {props.isDeleteClicked  ? (
          getDisabledFilter('selectedBundles', props.state.selectedBundles.value)
        ) : (
          <BundleFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedBundles}
            stateKey="selectedBundles"
            multiSelect={false}
            errorDiv={props.state.error.selectedBundles}
            horizontalLayout
          />
        )}
        {ErroNote(props.state.error.selectedBundles, "Please select a bundle")}
        <DateFilter
          onSelect={props.onSelect}
          stateKey="selectedDate"
          data={props.state.selectedDate}
          label="Date"
          isError={props.state.error.selectedDate}
        />
      </Layout.Child>
    </Layout>
  );
};

export default BuBundleMappingPopUpFilters;
