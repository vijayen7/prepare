import React from 'react';
import { Layout } from 'arc-react-components';
import CpeFilter from 'commons/container/CpeFilter';
import AgreementTypeSpecificNettingGroupFilter from 'commons/container/AgreementTypeSpecificNettingGroupFilter';
import CalculatorGroupFilter from 'commons/container/CalculatorGroupFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import LegalEntityFilter from 'commons/container/LegalEntityFilter';
import DateFilter from 'commons/container/DateFilter';

import { getValueForSingleSelect, getDisabledFilter, getErrorNote as ErroNote } from '../../util';

const CalculatorGroupPopUpFilters = (props) => {
  const selectedAgreementTypeIds = getValueForSingleSelect(props.state.selectedAgreementTypes);
  const selectedNettingGroupIds = getValueForSingleSelect(props.state.selectedNettingGroups);

  return (
    <Layout isRowType>
      <Layout.Child childId="CalculatorGroupPopup1">
        {props.isEditClicked ? (
          getDisabledFilter('Legal Entity:', props.state.selectedLegalEntities.value)
        ) : (
            <LegalEntityFilter
              onSelect={props.onSelect}
              multiSelect={false}
              selectedData={props.state.selectedLegalEntities}
              horizontalLayout
            />
          )}
        {props.isEditClicked ? (
          getDisabledFilter('CPEs:', props.state.selectedCpes.value)
        ) : (
            <CpeFilter
              onSelect={props.onSelect}
              multiSelect={false}
              selectedData={props.state.selectedCpes}
              horizontalLayout
            />
          )}
        {props.isEditClicked ? (
          getDisabledFilter('Agreement Type:', props.state.selectedAgreementTypes.value)
        ) : (
            <AgreementTypeFilter
              onSelect={props.onSelect}
              selectedData={props.state.selectedAgreementTypes}
              multiSelect={false}
              errorDiv={props.state.error.selectedAgreementTypes}
              horizontalLayout
            />
          )}
        {ErroNote(props.state.error.selectedAgreementTypes, "Please select an Agreement")}
        {props.isEditClicked ? (
          getDisabledFilter('Netting Group:', props.state.selectedNettingGroups.value)
        ) : (
            <AgreementTypeSpecificNettingGroupFilter
              onSelect={props.onSelect}
              selectedData={props.state.selectedNettingGroups}
              agreementTypeFilterIds={selectedAgreementTypeIds}
              multiSelect={false}
              errorDiv={props.state.error.selectedNettingGroups}
              horizontalLayout
            />
          )}
        {ErroNote(props.state.error.selectedNettingGroups, "Please select a netting group")}
        {props.isEditClicked ? (
          getDisabledFilter('Currency:', props.state.selectedCurrencies.value)
        ) : (
            <CurrencyFilter
              onSelect={props.onSelect}
              multiSelect={false}
              selectedData={props.state.selectedCurrencies}
              label="Currency"
              horizontalLayout
            />
          )}
        <DateFilter
          onSelect={props.onSelect}
          stateKey="selectedDate"
          data={props.state.selectedDate}
          label="Date"
        />
        {props.isDeleteClicked ? (
          getDisabledFilter('Calculator Groups' , props.state.selectedCalculatorGroups.value)
        ) :(
          <CalculatorGroupFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCalculatorGroups}
            agreementTypeFilterIds={selectedAgreementTypeIds}
            nettingGroupFilterIds={selectedNettingGroupIds}
            errorDiv={props.state.error.selectedCalculatorGroups}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {ErroNote(props.state.error.selectedCalculatorGroups, "Please select a calculator group")}

      </Layout.Child>
    </Layout>
  );
};

export default CalculatorGroupPopUpFilters;
