import React from "react";
import { getFilterContainer } from "../../../commons/container/FilterWrapper";
import { FINANCING_AGREEMENT_TERM_TYPE_FILTER } from '../../../commons/constants';

const FinancingAgreementTermTypeFilter = props => {
  var label = (typeof props.label !== "undefined") ? props.label : "Financing Term Type";
  var stateKey = (typeof props.stateKey !== "undefined") ? props.stateKey : "selectedFinancingAgreementTermTypes";
  var FilterComponent = getFilterContainer(
    FINANCING_AGREEMENT_TERM_TYPE_FILTER,
    "financingAgreementTermTypes",
    stateKey,
    label
  );

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={false}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default FinancingAgreementTermTypeFilter;
