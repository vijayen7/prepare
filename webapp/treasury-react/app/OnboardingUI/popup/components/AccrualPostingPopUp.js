import React from "react";
import Dialog from "commons/components/Dialog";
import Message from "commons/components/Message";
import MessageDialog from "commons/components/MessageDialog";
import { onboardingScreen } from "../../util";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AccrualPostingPopUpContent from "../container/AccrualPostingPopUpContent";
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData,
  resetDeleteAccrualData
} from "../../actions";

class AccrualPostingPopUp extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClosePopUp = () => {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  };

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
    this.props.resetDeleteAccrualData();
  };

  render() {
    console.log("herer");
    let message = null;
    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = (
            <Message messageData="Error occurred while saving rules" error />
          ));
    } else if (!_.isEmpty(this.props.deleteAccrualPostingData)) {
      !this.props.deleteAccrualPostingData.errorMessage
        ? (message = <Message messageData="Accrual Posting Deleted" success />)
        : (message = (
            <Message messageData="Error deleting Accrual Posting" error />
          ));
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.ACCRUAL &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          onClose={this.handleClosePopUp}
          style={{
            width: "1080px",
          }}
        >
          <AccrualPostingPopUpContent />
        </Dialog>
        <MessageDialog
          isOpen={
            this.props.view === onboardingScreen.ACCRUAL &&
            (!_.isEmpty(this.props.saveSimulatedData) ||
              !_.isEmpty(this.props.deleteAccrualPostingData))
          }
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData,
      resetDeleteAccrualData,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    deleteAccrualPostingData:
      state.financingOnboarding.deleteAccrualPostingData,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccrualPostingPopUp);
