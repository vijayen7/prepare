import React from 'react';
import { Layout } from 'arc-react-components';
import CpeFilter from 'commons/container/CpeFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import LegalEntityFilter from 'commons/container/LegalEntityFilter';
import DateFilter from 'commons/container/DateFilter';
import NettingGroupFilter from 'commons/container/NettingGroupFilter';
import { getDisabledFilter, getErrorNote as ErroNote } from '../../util';

const ReportingCurrencyPopUpFilters = (props) => {
  return (
    <Layout isRowType>
      <Layout.Child childId="reportingCurrencyPopup1">
        {props.isEditClicked ? (
          getDisabledFilter('Legal Entity:', props.state.selectedLegalEntities.value)
        ) : (
          <LegalEntityFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedLegalEntities}
            multiSelect={false}
            errorDiv={props.state.error.selectedLegalEntities}
            horizontalLayout
          />
        )}
        {ErroNote(props.state.error.selectedLegalEntities, "Please select legal entity")}
        {props.isEditClicked ? (
          getDisabledFilter('CPEs:', props.state.selectedCpes.value)
        ) : (
          <CpeFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCpes}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Agreement Type:', props.state.selectedAgreementTypes.value)
        ) : (
          <AgreementTypeFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedAgreementTypes}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Netting Groups:', props.state.selectedNettingGroups.value)
        ) : (
          <NettingGroupFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedNettingGroups}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Currencies:', props.state.selectedCurrencies.value)
        ) : (
          <CurrencyFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCurrencies}
            multiSelect={false}
            horizontalLayout
          />
        )}
        <DateFilter
          onSelect={props.onSelect}
          stateKey="selectedDate"
          data={props.state.selectedDate}
          label="Date"
          isError={props.state.error.selectedDate}
        />
        {props.isDeleteClicked ? (
          getDisabledFilter('Reporting Currencies:',props.state.selectedReportingCurrencies.value)
        ) : (
        <CurrencyFilter
          onSelect={props.onSelect}
          selectedData={props.state.selectedReportingCurrencies}
          label="Reporting Currencies"
          stateKey="selectedReportingCurrencies"
          multiSelect={false}
          errorDiv={props.state.error.selectedReportingCurrencies}
          horizontalLayout
        />
        )}
        {ErroNote(props.state.error.selectedReportingCurrencies, "Please select currency")}
      </Layout.Child>
    </Layout>
  );
};

export default ReportingCurrencyPopUpFilters;
