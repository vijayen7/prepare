import React from 'react';
import { Layout } from 'arc-react-components';
import CpeFilter from 'commons/container/CpeFilter';
import NettingGroupFilter from 'commons/container/NettingGroupFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import LegalEntityFilter from 'commons/container/LegalEntityFilter';
import DateFilter from 'commons/container/DateFilter';
import ChargeTypeFilter from 'commons/container/ChargeTypeFilter';
import AccrualPostingBusinessUnitFilter from 'commons/container/AccrualPostingBusinessUnitFilter';
import AccrualPostingCustodianAccountFilter from 'commons/container/AccrualPostingCustodianAccountFilter';
import AccrualPostingBundleFilter from 'commons/container/AccrualPostingBundleFilter';
import RegionSpecificCountryFilter from 'commons/container/RegionSpecificCountryFilter';
import RegionFilter from 'commons/container/RegionFilter';

import { getValueForSingleSelect, getDisabledFilter } from '../../util';

const AccrualPostingPopUpFilters = (props) => {
  const selectedAgreementTypeIds = getValueForSingleSelect(props.state.selectedAgreementTypes);
  const chargeTypeFilterIds = getValueForSingleSelect(props.state.selectedChargeTypes);
  const businessUnitFilterIds = getValueForSingleSelect(props.state.selectedBusinessUnits);
  const bundleFilterIds = getValueForSingleSelect(props.state.selectedBundles);
  let selectedRegionIds = getValueForSingleSelect(props.state.selectedRegions);
  let isOutPerformance = chargeTypeFilterIds === '8' || chargeTypeFilterIds === '9' || chargeTypeFilterIds === '10' ;
  let isDividendEnhancementChargeType =  chargeTypeFilterIds === '11';
  let isCountryAndRegionLevelChargeType = isOutPerformance || isDividendEnhancementChargeType;

  const parameters = { chargeTypeFilterIds, businessUnitFilterIds, bundleFilterIds };
  return (
    <Layout isRowType>
      <Layout.Child childId="accrualPostingPopup1">
        {props.isEditClicked ? (
            getDisabledFilter('Charge Types:', props.state.selectedChargeTypes.value)
          ) : (
            <ChargeTypeFilter
              onSelect={props.onSelect}
              selectedData={props.state.selectedChargeTypes}
              multiSelect={false}
              horizontalLayout
            />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Legal Entity:', props.state.selectedLegalEntities.value)
        ) : (
          <LegalEntityFilter
            readonly={isCountryAndRegionLevelChargeType}
            onSelect={props.onSelect}
            selectedData={props.state.selectedLegalEntities}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('CPEs:', props.state.selectedCpes.value)
        ) : (
          <CpeFilter
            readonly={isCountryAndRegionLevelChargeType}
            onSelect={props.onSelect}
            selectedData={props.state.selectedCpes}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Agreement Type:', props.state.selectedAgreementTypes.value)
        ) : (
          <AgreementTypeFilter
            readonly={isCountryAndRegionLevelChargeType}
            onSelect={props.onSelect}
            selectedData={props.state.selectedAgreementTypes}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Netting Groups:', props.state.selectedNettingGroups.value)
        ) : (
          <NettingGroupFilter
            readonly={isCountryAndRegionLevelChargeType}
            onSelect={props.onSelect}
            selectedData={props.state.selectedNettingGroups}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Currencies:', props.state.selectedCurrencies.value)
        ) : (
          <CurrencyFilter
            readonly={isCountryAndRegionLevelChargeType}
            onSelect={props.onSelect}
            selectedData={props.state.selectedCurrencies}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {isCountryAndRegionLevelChargeType ? (
            <RegionFilter
              readonly={props.isEditClicked}
              onSelect={props.onSelect}
              selectedData={props.state.selectedRegions}
              multiSelect={false}
              horizontalLayout
            />
        ) : (
          <React.Fragment />
        )}
        {isCountryAndRegionLevelChargeType ? (
            <RegionSpecificCountryFilter
              readonly={props.isEditClicked}
              onSelect={props.onSelect}
              selectedData={props.state.selectedCountries}
              multiSelect={false}
              regionId={selectedRegionIds}
              horizontalLayout
            />
        ) : (
          <React.Fragment />
        )}
        {props.isDeleteClicked ? (
          getDisabledFilter('Business Unit:', props.state.selectedBusinessUnits.value)
        ) : (
          <AccrualPostingBusinessUnitFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedBusinessUnits}
            multiSelect={false}
            parameters={parameters}
            errorDiv={props.state.error.selectedBusinessUnits}
            horizontalLayout
          />
        )}
        {props.isDeleteClicked ? (
          getDisabledFilter('Bundle:', props.state.selectedBundles.value)
        ) : (
          <AccrualPostingBundleFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedBundles}
            multiSelect={false}
            parameters={parameters}
            errorDiv={props.state.error.selectedBundles}
            horizontalLayout
          />
        )}
        {props.isDeleteClicked ? (
          getDisabledFilter('Custodian Account:', props.state.selectedCustodianAccounts.value)
        ) : (
          <AccrualPostingCustodianAccountFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCustodianAccounts}
            multiSelect={false}
            parameters={parameters}
            errorDiv={props.state.error.selectedCustodianAccounts}
            horizontalLayout
          />
        )}
        <DateFilter
          onSelect={props.onSelect}
          stateKey="selectedDate"
          data={props.state.selectedDate}
          label={props.isDeleteClicked ? 'Effective Till' : 'Date'}
          isError={props.state.error.selectedDate}
        />
      </Layout.Child>
    </Layout>
  );
};

export default AccrualPostingPopUpFilters;
