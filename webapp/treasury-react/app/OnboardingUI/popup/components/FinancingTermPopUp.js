import React from "react";
import Dialog from "commons/components/Dialog";
import Message from "commons/components/Message";
import MessageDialog from "commons/components/MessageDialog";
import { onboardingScreen } from "../../util";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FinancingTermsPopUpContent from "../container/FinancingTermsPopUpContent";
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData,
  resetDeleteTermData
} from "../../actions";

class FinancingTermPopUp extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClosePopUp = () => {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  };

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
    this.props.resetDeleteTermData();
  };

  render() {
    let message = null;
    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = (
            <Message messageData="Error occurred while saving rules" error />
          ));
    } else if (!_.isEmpty(this.props.deleteFinancingTermData)) {
      !this.props.deleteFinancingTermData.errorMessage
        ? (message = <Message messageData="Agreement Term Deleted" success />)
        : (message = (
            <Message messageData="Error deleting agreement term" error />
          ));
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.FINANCING &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          onClose={this.handleClosePopUp}
        >
          <FinancingTermsPopUpContent />
        </Dialog>
        <MessageDialog
          isOpen={
            this.props.view === onboardingScreen.FINANCING &&
            (!_.isEmpty(this.props.saveSimulatedData) ||
              !_.isEmpty(this.props.deleteFinancingTermData))
          }
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData,
      resetDeleteTermData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
     deleteFinancingTermData: state.financingOnboarding.deleteFinancingTermData,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FinancingTermPopUp);
