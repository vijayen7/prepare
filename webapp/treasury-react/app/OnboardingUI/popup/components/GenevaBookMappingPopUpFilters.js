import React from 'react';
import { Layout } from 'arc-react-components';
import LegalEntityFilter from 'commons/container/LegalEntityFilter';
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import NettingGroupFilter from "commons/container/NettingGroupFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import DateFilter from 'commons/container/DateFilter';
import BookFilter from 'commons/container/BookFilterWithSpecialAccountingBooks';
import { getDisabledFilter, getErrorNote as ErroNote } from '../../util';
import { PropTypes } from 'mobx-react';


const GenevaBookMappingPopUpFilters = (props) => {
  return (
    <Layout isRowType>
      <Layout.Child childId="genevaBookMapping1">
        {props.isEditClicked ? (
          getDisabledFilter('Legal Entity:', props.state.selectedLegalEntities.value)) :
          (<LegalEntityFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedLegalEntities}
            multiSelect={false}
            errorDiv={props.state.error.selectedLegalEntities}
            horizontalLayout />
          )}
        {ErroNote(props.state.error.selectedLegalEntities,
        "Please select legal entity")}
        {props.isEditClicked ? (
          getDisabledFilter('Currencies:', props.state.selectedCurrencies.value)) :
          <CurrencyFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCurrencies}
            multiSelect={false}
            horizontalLayout />
        }
        {props.isEditClicked ? (getDisabledFilter('CPEs:', props.state.selectedCpes.value)) :
          <CpeFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCpes}
            multiSelect={false}
            horizontalLayout
          />
        }
        {props.isEditClicked ? (getDisabledFilter('Netting Groups:', props.state.selectedNettingGroups.value)) :
          <NettingGroupFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedNettingGroups}
            multiSelect={false}
            horizontalLayout />
        }
        {props.isEditClicked ? (getDisabledFilter('Agreement Type:', props.state.selectedAgreementTypes.value)) :
          <AgreementTypeFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedAgreementTypes}
            multiSelect={false}
            horizontalLayout />
        }
        <BookFilter
          onSelect={props.onSelect}
          selectedData={props.state.selectedBooks}
          stateKey="selectedBooks"
          multiSelect={false}
          errorDiv={props.state.error.selectedBooks}
          horizontalLayout
        />
        {ErroNote(props.state.error.selectedBooks,"Please select book")}
        <DateFilter
          onSelect={props.onSelect}
          stateKey="selectedDate"
          data={props.state.selectedDate}
          label="Start Date"
          isError={props.state.error.selectedDate}
        />
        <div>*Should be start of month</div>
      </Layout.Child>
    </Layout>
  );
};

export default GenevaBookMappingPopUpFilters;
