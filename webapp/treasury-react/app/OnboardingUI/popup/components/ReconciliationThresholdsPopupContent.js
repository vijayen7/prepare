import React from "react";
import Dialog from "commons/components/Dialog";
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { onboardingScreen } from "../../util";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import InputFilter from 'commons/components/InputFilter';
import InputNumberFilter from 'commons/components/InputNumberFilter';
import { getDisabledFilter, getErrorNote as ErroNote, getMonthEndDate, getMonthStartDate } from '../../util';
import {getDateDifference} from "commons/util";
import { Layout } from 'arc-react-components';
import MonthFilter from 'commons/components/MonthFilter';
import FilterButton from 'commons/components/FilterButton';
import { Card } from 'arc-react-components';
import {
  saveReconciliationThresholdsData,
  resetSaveReconciliationThresholdsData,
  resetSelectedRowData,
  deleteReconciliationThresholdsData,
  hidePrimaryPopUp,
  fetchSearchData
} from '../../actions';


class ReconciliationThresholdsPopupContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.isValid = this.isValid.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = this.getDefaultState();
  }
  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }
  handleClosePopUp() {
    this.setState(this.getDefaultState());
    this.props.hidePrimaryPopUp();
    this.props.resetSelectedRowData();
  }

  handleSubmit() {
    if(this.props.isDeleteClicked) {
      this.handleDelete();
    }else {
      this.handleSave();
    }
  }
  handleDelete() {
    var payload = {
      "agreementTypeId": this.props.selectedRowProps.agreementTypeId
    }
    this.props.deleteReconciliationThresholdsData(payload);
    this.handleClosePopUp();

  }
  isValid() {
      var isValid = true;
      var error = {};
      if(!this.state.selectedAgreementTypes || !this.state.selectedAgreementTypes['key']){
        error.agreementTypeId = "Please select Agreement Type";
        isValid = false;
      }
      if(!this.state.effectiveBeginDate || !this.state.effectiveEndDate ||
        (!this.state.inFavorAbsoluteThreshold && !this.state.inFavorPercentageThreshold) ||
        (!this.state.againstFavorAbsoluteThreshold && !this.state.againstFavorPercentageThreshold)){
        isValid = false;
        error.common = "Please enter valid data";
      } else if(getDateDifference(getMonthStartDate(this.state.effectiveBeginDate),getMonthEndDate(this.state.effectiveEndDate)) < 0){
        error.common = "Effective end date must be less than effective begin date";
        isValid = false;
      }
      if(this.state.inFavorPercentageThreshold && parseInt(this.state.inFavorPercentageThreshold) > 100) {
        error.inFavorPercentageThreshold = true;
        error.common = "Percentage must be below 100";
        isValid = false;
      }
      if(this.state.againstFavorPercentageThreshold && parseInt(this.state.againstFavorPercentageThreshold) > 100) {
        error.againstFavorPercentageThreshold = true;
        error.common = "Percentage must be below 100"
        isValid = false;
      }
      this.setState({...this.state,error:error});
      return isValid;
  }
  handleSave() {
    if(this.isValid()){
      var payload = {
        "autoApprovalThreshold.agreementTypeId": this.state.selectedAgreementTypes['key'],
        "autoApprovalThreshold.effectiveBeginDate": getMonthStartDate(this.state.effectiveBeginDate).toJSONString().substr(0,10).replaceAll('/','-'),
        "autoApprovalThreshold.effectiveEndDate": getMonthEndDate(this.state.effectiveEndDate).toJSONString().substr(0,10).replaceAll('/','-'),
        "autoApprovalThreshold.inFavorAbsoluteThreshold": this.state.inFavorAbsoluteThreshold || 0,
        "autoApprovalThreshold.inFavorPercentageThreshold": this.state.inFavorPercentageThreshold || 0,
        "autoApprovalThreshold.againstFavorAbsoluteThreshold": this.state.againstFavorAbsoluteThreshold || 0,
        "autoApprovalThreshold.againstFavorPercentageThreshold": this.state.againstFavorPercentageThreshold || 0,
        "autoApprovalThreshold.comment": this.state.comment,
      };
      this.props.saveReconciliationThresholdsData(payload);
      this.handleClosePopUp();
    }
  }
  getDefaultState() {
    return {
      selectedAgreementTypes: null,
      effectiveBeginDate: null,
      effectiveEndDate: null,
      inFavorPercentageThreshold: null,
      inFavorAbsoluteThreshold: null,
      againstFavorPercentageThreshold: null,
      againstFavorAbsoluteThreshold: null,
      comment:null,
      error: {
        agreementTypeId:null,
        effectiveBeginDate: null,
        effectiveEndDate: null,
        inFavorPercentageThreshold: null,
        inFavorAbsoluteThreshold: null,
        againstFavorPercentageThreshold: null,
        againstFavorAbsoluteThreshold: null,
        common: null
      }
    };
  }
  render() {
    return (
      <Layout isRowType>
        <Layout.Child>
          <Card>
            {(this.props.isDeleteClicked)?(
               <p>Are you sure you want to delete Reconciliation Thresholds for {this.props.selectedRowProps.agreementTypeName}?</p>
            ):(
              <React.Fragment>
                <AgreementTypeFilter
                  onSelect={this.onSelect}
                  selectedData={this.state.selectedAgreementTypes}
                  multiSelect={false}
                  isError={this.state.error.agreementType}
                  stateKey="agreementTypeId"
                  horizontalLayout
                />
                {ErroNote(this.state.error.agreementTypeId, "Please select Agreement Type")}
                <MonthFilter
                  onSelect={this.onSelect}
                  stateKey="effectiveBeginDate"
                  data={this.state.effectiveBeginDate}
                  label="Effective Begin Date"
                  isError={this.state.error.effectiveBeginDate}
                />
                <MonthFilter
                  onSelect={this.onSelect}
                  stateKey="effectiveEndDate"
                  data={this.state.effectiveEndDate}
                  label="Effective End Date"
                  isError={this.state.error.effectiveEndDate}
                />
                <InputNumberFilter
                  data={this.state.inFavorPercentageThreshold}
                  onSelect={this.onSelect}
                  stateKey="inFavorPercentageThreshold"
                  label="In Favor Percentage Threshold"
                  isError={this.state.error.inFavorPercentageThreshold}
                />
                <InputNumberFilter
                  data={this.state.inFavorAbsoluteThreshold}
                  onSelect={this.onSelect}
                  stateKey="inFavorAbsoluteThreshold"
                  label="In Favor Absolute Threshold"
                  isError={this.state.error.inFavorAbsoluteThreshold}
                />
                <InputNumberFilter
                  data={this.state.againstFavorPercentageThreshold}
                  onSelect={this.onSelect}
                  stateKey="againstFavorPercentageThreshold"
                  label="Against Favor Percentage Threshold"
                  isError={this.state.error.againstFavorPercentageThreshold}
                />
                <InputNumberFilter
                  data={this.state.againstFavorAbsoluteThreshold}
                  onSelect={this.onSelect}
                  stateKey="againstFavorAbsoluteThreshold"
                  label="Against Favor Absolute Threshold"
                  isError={this.state.error.againstFavorAbsoluteThreshold}
                />
                <InputFilter
                  data={this.state.comment}
                  onSelect={this.onSelect}
                  stateKey="comment"
                  label="Comment"
                />
                {ErroNote(this.state.error.common, this.state.error.common)}
              </React.Fragment>)}
          </Card>
          <Card>
            <FilterButton
              onClick={this.handleSubmit}
              label={(this.props.isDeleteClicked?"Delete Threshold":"Save ReconciliationThresholds")}
            />
          </Card>
        </Layout.Child>
      </Layout>
    )
  }
}

function mapStateToProps(state) {
  return {
    isDeleteClicked: state.financingOnboarding.isDeleteClicked,
    selectedRowProps: state.financingOnboarding.selectedRowData,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    resetSaveReconciliationThresholdsData,
    deleteReconciliationThresholdsData,
    hidePrimaryPopUp,
    resetSelectedRowData,
    saveReconciliationThresholdsData,
    fetchSearchData
  },dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReconciliationThresholdsPopupContent);
