import React from 'react';
import { Layout } from 'arc-react-components';
import CpeFilter from 'commons/container/CpeFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import LegalEntityFilter from 'commons/container/LegalEntityFilter';
import ChargeTypeFilter from 'commons/container/ChargeTypeFilter';
import { getDisabledFilter, getErrorNote as ErrorNote } from '../../util';
import AdjustmentAllocationStrategyFilter from './../../../commons/container/AdjustmentAllocationStrategyFilter';

const AdjustmentAllocationPopUpFilters = props => {
  return (
    <Layout isRowType>
      <Layout.Child childId="adjustmentPopup1">
        {props.isEditClicked ? (
            getDisabledFilter('Charge Types:', props.state.selectedChargeTypes.value)
          ) : (
            <ChargeTypeFilter
              onSelect={props.onSelect}
              selectedData={props.state.selectedChargeTypes}
              multiSelect={false}
              horizontalLayout
            />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Legal Entity:', props.state.selectedLegalEntities.value)
        ) : (
          <LegalEntityFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedLegalEntities}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('CPEs:', props.state.selectedCpes.value)
        ) : (
          <CpeFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCpes}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Agreement Type:', props.state.selectedAgreementTypes.value)
        ) : (
          <AgreementTypeFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedAgreementTypes}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {props.isEditClicked ? (
          getDisabledFilter('Currencies:', props.state.selectedCurrencies.value)
        ) : (
          <CurrencyFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCurrencies}
            multiSelect={false}
            horizontalLayout
          />
        )}
        {
          <AdjustmentAllocationStrategyFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedAllocationStrategy !== undefined ? props.state.selectedAllocationStrategy.value : ""}
            stateKey="selectedAllocationStrategy"
            multiSelect={false}
            horizontalLayout={true}
          />
        }
        {ErrorNote(props.state.error.selectedAllocationStrategy, "Please enter an allocation strategy")}
      </Layout.Child>
    </Layout>
  );
};

export default AdjustmentAllocationPopUpFilters;
