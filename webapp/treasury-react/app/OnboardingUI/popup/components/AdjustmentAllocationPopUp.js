import React from "react";
import Dialog from "commons/components/Dialog";
import Message from "commons/components/Message";
import MessageDialog from "commons/components/MessageDialog";
import { onboardingScreen } from "../../util";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AdjustmentAllocationPopUpContent from "../container/AdjustmentAllocationPopUpContent";
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData,
  resetDeleteAdjustmentAllocationRule,
} from "../../actions";

class AdjustmentAllocationPopUp extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClosePopUp = () => {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  };

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
    this.props.resetDeleteAdjustmentAllocationRule();
  };

  render() {
    let message = null;
    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = (
            <Message messageData="Error occurred while saving rules" error />
          ));
    } else if (!_.isEmpty(this.props.deleteAdjustmentAllocationRuleData)) {
      !this.props.deleteAdjustmentAllocationRuleData.errorMessage
        ? (message = (
            <Message messageData="Adjustment Allocation Rule Deleted" success />
          ))
        : (message = (
            <Message
              messageData="Error deleting Adjustment Allocation Rule"
              error
            />
          ));
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.ADJUSTMENT &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          onClose={this.handleClosePopUp}
          style={{
            width: "1080px",
          }}
        >
          <AdjustmentAllocationPopUpContent />
        </Dialog>
        <MessageDialog
          isOpen={
            (this.props.view === onboardingScreen.ADJUSTMENT &&
              !_.isEmpty(this.props.saveSimulatedData)) ||
            !_.isEmpty(this.props.deleteAdjustmentAllocationRuleData)
          }
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData,
      resetDeleteAdjustmentAllocationRule,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    deleteAdjustmentAllocationRuleData:
      state.financingOnboarding.deleteAdjustmentAllocationRuleData,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdjustmentAllocationPopUp);
