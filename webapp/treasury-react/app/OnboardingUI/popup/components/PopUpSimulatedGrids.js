import React, { Component } from "react";
import { Layout } from "arc-react-components";
import { Card } from "arc-react-components";
import OnboardingUISimulateGrid from "commons/components/Grid";
import _ from "lodash";
import OnboardingUICheckboxGrid from "commons/components/GridWithCheckbox";
import { getSimulatedColumnConfig } from "../../grid/columnConfig";
import {
  getGridOptions,
  getSimulatedGridOptions
} from "../../grid/gridOptions";

export default class PopUpSimulatedGrids extends Component {
  constructor(props) {
    super(props);
    this.renderData = this.renderData.bind(this);
  }

  renderData() {
    if (!this.props.isSimulate) {
      return null;
    }
    let existingRules = null;
    let simulatedRules = null;
    let existingList = null;
    let simulatedList = null;
    if (this.props.simulatedData !== undefined) {
      existingList = this.props.simulatedData.existingList;
      simulatedList = this.props.simulatedData.simulatedList;
    }
    if (
      typeof existingList === "undefined" ||
      typeof simulatedList === "undefined" ||
      existingList == null ||
      simulatedList == null
    ) {
      return null;
    }
    if (existingList.length > 0) {
      existingRules = (
        <OnboardingUISimulateGrid
          data={existingList}
          gridId="ExistingRules"
          gridColumns={getSimulatedColumnConfig(this.props.view)}
          gridOptions={getGridOptions()}
          label={<div className="text-align--left">Existing Rules</div>}
        />
      );
    }
    if (simulatedList.length > 0) {
      simulatedRules = (
        <OnboardingUICheckboxGrid
          data={simulatedList}
          gridId="SimulatedRules"
          gridColumns={getSimulatedColumnConfig(this.props.view)}
          gridOptions={getSimulatedGridOptions()}
          label={<div className="text-align--left">Simulated Rules : Please select one or more rules to save.</div>}
          onSelect={this.props.onSelect}
          preSelectAllRows={false}
          heightValue={-1}
        />
      );
    }
    return (
      <Card>
        <Layout isRowType>
          <Layout.Child childId="popupSimulatedGrid1">
            {existingRules}
            <hr />
            <br />
          </Layout.Child>
          <Layout.Child childId="popupSimulatedGrid2">
            {simulatedRules}
          </Layout.Child>
        </Layout>
      </Card>
    );
  }

  render() {
    return this.renderData();
  }
}
