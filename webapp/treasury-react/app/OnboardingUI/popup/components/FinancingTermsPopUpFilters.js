import React from 'react';
import { Layout, Message } from 'arc-react-components';
import AgreementSpecificNettingGroupFilter from 'commons/container/AgreementSpecificNettingGroupFilter';
import AgreementFilter from 'commons/container/AgreementFilter';
import AssetClassGroupFilter from 'commons/container/AssetClassGroupFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import DateFilter from 'commons/container/DateFilter';
import CheckboxFilter from 'commons/components/CheckboxFilter';
import AllFinancingStylesFilter from 'commons/container/AllFinancingStylesFilter';
import BandwidthGroupFilter from 'commons/container/BandwidthGroupFilter';
import CurrencySpecificBaseRateFilter from 'commons/container/CurrencySpecificBaseRateFilter';
import NegativeIrateFilter from 'commons/container/NegativeIrateFilter';
import AccrualConventionFilter from 'commons/container/AccrualConventionFilter';
import FinancingAgreementTermTypeFilter from './FinancingAgreementTermTypeFilter';
import AgmtNettingGroupSpecificCustodianAccountsFilter from 'commons/container/AgmtNettingGroupSpecificCustodianAccountsFilter'
import { Card } from 'arc-react-components';
import InputNumberFilter from 'commons/components/InputNumberFilter';
import InputFilter from 'commons/components/InputFilter';
import LeanSurchargeModelFilter from 'commons/container/LeanSurchargeModelFilter'

import { getValueForSingleSelect, getDisabledFilter, isFinancingTermTypeAsset, isFinancingTermTypeAgreement,
  isFinancingTermTypeAccount, getDataValueForSingleSelect, getErrorNote as ErroNote } from '../../util';

const FinancingTermsPopUpFilters = (props) => {
  const selectedAgreementIds = getValueForSingleSelect(props.state.selectedAgreements);
  const currencyId = getValueForSingleSelect(props.state.selectedBaseRateCurrencies);
  const selectedNettingGroupId = getValueForSingleSelect(props.state.selectedNettingGroups);
  const selectedFinancingAgreementTermType = getDataValueForSingleSelect(props.state.selectedFinancingAgreementTermTypes);

  return (
    <Layout isRowType>
      <Layout.Child childId="financingTermsPopup1">
        {props.isDeleteClicked ? (
          <React.Fragment />
        ) : (
          <React.Fragment>
            <Card>
            {props.isEditClicked ? (
                getDisabledFilter('Financing Term Type:', props.state.selectedFinancingAgreementTermTypes.value)
              ) : (
                <FinancingAgreementTermTypeFilter
                  onSelect={props.onSelect}
                  selectedData={props.state.selectedFinancingAgreementTermTypes}
                  stateKey="selectedFinancingAgreementTermTypes"
                  multiSelect={false}
                  horizontalLayout={true}
                />
              )}

            </Card>
            <Card>
              {props.isEditClicked ? (
                getDisabledFilter('Agreements:', props.state.selectedAgreements.value)
              ) : (
                <AgreementFilter
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedAgreements}
                  errorDiv={props.state.error.selectedAgreements}
                  horizontalLayout
                />
              )}
              {ErroNote(props.state.error.selectedAgreements, "Please select an Agreement")}
              {props.isEditClicked ? (
                getDisabledFilter('Netting Groups:', props.state.selectedNettingGroups.value)
              ) : (
                <AgreementSpecificNettingGroupFilter
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedNettingGroups}
                  agreementId={selectedAgreementIds}
                  //errorDiv={props.state.error.selectedNettingGroups}
                  horizontalLayout
                />
              )}
              {ErroNote(props.state.error.selectedNettingGroups, "Please select a Netting group")}
              {props.isEditClicked ? (
                getDisabledFilter('Currencies:', props.state.selectedCurrencies.value)
              ) : (
                <CurrencyFilter
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedCurrencies}
                  errorDiv={props.state.error.selectedCurrencies}
                  horizontalLayout
                />
              )}
              {ErroNote(props.state.error.selectedCurrencies, "Please select a currency")}
              {props.isEditClicked ? (
                getDisabledFilter('Asset Class Groups:', props.state.selectedAssetClassGroups.value)
              ) : isFinancingTermTypeAsset(selectedFinancingAgreementTermType) ? (
                <AssetClassGroupFilter
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedAssetClassGroups}
                  errorDiv={props.state.error.selectedAssetClassGroups}
                  horizontalLayout
                />
              ) : null}
              {isFinancingTermTypeAsset(selectedFinancingAgreementTermType) ?
                ErroNote(props.state.error.selectedAssetClassGroups, "Please select a Asset Class") : null}
              {props.isEditClicked ? (
                getDisabledFilter('Custodian Account:', props.state.selectedCustodianAccounts.value)
              ) : isFinancingTermTypeAccount(selectedFinancingAgreementTermType) ? (
                <AgmtNettingGroupSpecificCustodianAccountsFilter
                  onSelect={props.onSelect}
                  selectedData={props.state.selectedCustodianAccounts}
                  nettingGroupIds={selectedNettingGroupId}
                  agreementIds={selectedAgreementIds}
                  multiSelect={false}
                  horizontalLayout
                />
              ) : null}
              {isFinancingTermTypeAccount(selectedFinancingAgreementTermType) ?
                ErroNote(props.state.error.selectedCustodianAccounts, "Please select a Custodian Account") : null}
              {props.isEditClicked ? (
                getDisabledFilter('Bandwidth Group:', props.state.selectedBandwidthGroups.value)
              ) : (
                <BandwidthGroupFilter
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedBandwidthGroups}
                  horizontalLayout
                />
              )}
            </Card>
            <br />
            <Card>
              <CurrencyFilter
                onSelect={props.onSelect}
                selectedData={props.state.selectedBaseRateCurrencies}
                label="Base Rate Currency"
                stateKey="selectedBaseRateCurrencies"
                multiSelect={false}
                readonly={props.isDeleteClicked}
                horizontalLayout
              />
              <hr />
              <CheckboxFilter
                onSelect={props.onSelect}
                stateKey="differentRates"
                label=" Different Rates"
                defaultChecked={props.state.differentRates}
              />
              {!props.state.differentRates ? (
                <CurrencySpecificBaseRateFilter
                  onSelect={props.onSelect}
                  selectedData={props.state.selectedBaseRates}
                  currencyId={currencyId}
                  multiSelect={false}
                  // errorDiv={props.state.error.selectedBaseRates}
                  readonly={props.isDeleteClicked}
                  horizontalLayout
                />
              ) : (
                <React.Fragment />
              )}
              {!props.state.differentRates ?
              ErroNote(props.state.error.selectedBaseRates, "Please select base rate")
              : null}

              {props.state.differentRates ? (
                <CurrencySpecificBaseRateFilter
                  onSelect={props.onSelect}
                  selectedData={props.state.selectedCreditBaseRates}
                  currencyId={currencyId}
                  multiSelect={false}
                  label="Credit Base Rate"
                  stateKey="selectedCreditBaseRates"
                  // errorDiv={props.state.error.selectedCreditBaseRates}
                  readonly={props.isDeleteClicked}
                  horizontalLayout
                />
              ) : (
                <React.Fragment />
              )}
              {props.state.differentRates ?
              ErroNote(props.state.error.selectedCreditBaseRates, "Please select credit base rate")
              : null}
              {props.state.differentRates ? (
                <CurrencySpecificBaseRateFilter
                  onSelect={props.onSelect}
                  selectedData={props.state.selectedDebitBaseRates}
                  currencyId={currencyId}
                  multiSelect={false}
                  label="Debit Base Rate"
                  stateKey="selectedDebitBaseRates"
                  // errorDiv={props.state.error.selectedDebitBaseRates}
                  readonly={props.isDeleteClicked}
                  horizontalLayout
                />
              ) : (
                <React.Fragment />
              )}
              {props.state.differentRates ?
              ErroNote(props.state.error.selectedDebitBaseRates, "Please select debit base rate")
              : null}
              {props.state.differentRates ? (
                <CurrencySpecificBaseRateFilter
                  onSelect={props.onSelect}
                  selectedData={props.state.selectedRebateBaseRates}
                  currencyId={currencyId}
                  multiSelect={false}
                  label="Rebate Base Rate"
                  stateKey="selectedRebateBaseRates"
                  readonly={props.isDeleteClicked}
                  horizontalLayout
                />
              ) : (
                <div style={{ height: '30px' }} />
              )}
              <hr />
              <CheckboxFilter
                onSelect={props.onSelect}
                stateKey="differentConvention"
                label=" Different Convention"
                defaultChecked={props.state.differentConvention}
              />
              {!props.state.differentConvention ? (
                <AccrualConventionFilter
                  readonly={props.isDeleteClicked}
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedAccrualConventions}
                  errorDiv={props.state.error.selectedAccrualConventions}
                  horizontalLayout
                />
              ) : (
                <React.Fragment />
              )}
              {!props.state.differentConvention ?
              ErroNote(props.state.error.selectedAccrualConventions, "Please select accrual convention")
              : null}
              {props.state.differentConvention ? (
                <AccrualConventionFilter
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedCreditAccrualConventions}
                  label="Credit AC"
                  stateKey="selectedCreditAccrualConventions"
                  errorDiv={props.state.error.selectedCreditAccrualConventions}
                  readonly={props.isDeleteClicked}
                  horizontalLayout
                />
              ) : (
                <React.Fragment />
              )}
              {props.state.differentConvention ?
              ErroNote(props.state.error.selectedCreditAccrualConventions, "Please select accrual convention")
              : null}
              {props.state.differentConvention ? (
                <AccrualConventionFilter
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedDebitAccrualConventions}
                  label="Debit AC"
                  stateKey="selectedDebitAccrualConventions"
                  errorDiv={props.state.error.selectedDebitAccrualConventions}
                  readonly={props.isDeleteClicked}
                  horizontalLayout
                />
              ) : (
                <div style={{ height: '30px' }} />
              )}
              {props.state.differentConvention ?
              ErroNote(props.state.error.selectedDebitAccrualConventions, "Please select accrual convention")
              : null}
              <hr />
              <NegativeIrateFilter
                onSelect={props.onSelect}
                selectedData={props.state.selectedApplyNegativeCreditIRates}
                multiSelect={false}
                label="Apply Negative Credit IRate on"
                stateKey="selectedApplyNegativeCreditIRates"
                readonly={props.isDeleteClicked}
                horizontalLayout
              />
              <NegativeIrateFilter
                onSelect={props.onSelect}
                selectedData={props.state.selectedApplyNegativeDebitIRates}
                multiSelect={false}
                label="Apply Negative Debit IRate on"
                stateKey="selectedApplyNegativeDebitIRates"
                readonly={props.isDeleteClicked}
                horizontalLayout
              />
              <NegativeIrateFilter
                onSelect={props.onSelect}
                selectedData={props.state.selectedApplyNegativeRebateIRates}
                multiSelect={false}
                label="Apply Negative IRate for SFIC on"
                stateKey="selectedApplyNegativeRebateIRates"
                readonly={props.isDeleteClicked}
                horizontalLayout
              />
              <AllFinancingStylesFilter
                onSelect={props.onSelect}
                multiSelect={false}
                selectedData={props.state.selectedFinancingStyles}
                readonly={props.isDeleteClicked}
                horizontalLayout
              />
              { isFinancingTermTypeAgreement(selectedFinancingAgreementTermType) ?
                <LeanSurchargeModelFilter
                  onSelect={props.onSelect}
                  multiSelect={false}
                  selectedData={props.state.selectedLeanSurchargeModel}
                  readonly={props.isDeleteClicked}
                  payload={true}
                  horizontalLayout
                /> : null
              }
              <br />
              <InputNumberFilter
                onSelect={props.onSelect}
                data={props.state.creditSpread}
                label="Credit Spread (bps)"
                stateKey="creditSpread"
                readonly={props.isDeleteClicked}
              />
              <InputNumberFilter
                onSelect={props.onSelect}
                data={props.state.debitSpread}
                label="Debit Spread (bps)"
                stateKey="debitSpread"
                readonly={props.isDeleteClicked}
              />
              <InputNumberFilter
                onSelect={props.onSelect}
                data={props.state.rebateSpread}
                label="Rebate Spread (bps)"
                stateKey="rebateSpread"
                readonly={props.isDeleteClicked}
              />
              <InputNumberFilter
                onSelect={props.onSelect}
                data={props.state.gcRate}
                label="GC Rate (bps)"
                stateKey="gcRate"
                isError={props.state.error.gcRate}
                readonly={props.isDeleteClicked}
              />
              <InputNumberFilter
                onSelect={props.onSelect}
                data={props.state.gcShortSpread}
                label="GC Short Spread (bps)"
                stateKey="gcShortSpread"
                readonly={props.isDeleteClicked}
              />
              <InputNumberFilter
                onSelect={props.onSelect}
                data={props.state.smvConversionFactor}
                label="SMV Conversion Factor  "
                stateKey="smvConversionFactor"
                readonly={props.isDeleteClicked}
              />
              <InputNumberFilter
                onSelect={props.onSelect}
                data={props.state.haircut}
                label="Haircut  "
                stateKey="haircut"
                readonly={props.isDeleteClicked}
              />
              <InputNumberFilter
                  onSelect={props.onSelect}
                  data={props.state.marginFactor}
                  label="Margin Factor "
                  stateKey="marginFactor"
                  readonly={props.isDeleteClicked}
                />
            </Card>
          </React.Fragment>
        )}
        <br />
        <DateFilter
          onSelect={props.onSelect}
          stateKey="selectedDate"
          data={props.state.selectedDate}
          label={props.isDeleteClicked ? 'Effective Till' : 'Effective From'}
        />
        <br />
        <InputFilter
          onSelect={props.onSelect}
          data={props.state.comment}
          stateKey="comment"
          label="Comment"
        />
        Message
        { props.state.error.selectedAgreements || props.state.error.selectedNettingGroups ||
          props.state.error.selectedCurrencies || props.state.error.selectedCreditBaseRates ||
          props.state.error.selectedDebitBaseRates || props.state.error.selectedBaseRates ||
          props.state.error.selectedDebitAccrualConventions || props.state.error.selectedCreditAccrualConventions ||
          props.state.error.selectedAccrualConventions || props.state.error.selectedBaseRateCurrencies ||
          props.state.error.selectedCustodianAccounts || props.state.error.selectedAssetClassGroups
          ? <Message title="Please review the errors in search criteria and try again" type="critical"/>
          : null
        }
        <React.Fragment />
      </Layout.Child>
    </Layout>
  );
};

export default FinancingTermsPopUpFilters;
