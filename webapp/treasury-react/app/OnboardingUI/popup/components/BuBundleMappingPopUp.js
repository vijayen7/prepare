import React from "react";
import Dialog from "commons/components/Dialog";
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { onboardingScreen } from "../../util";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BuBundleMappingPopUpContent from "../container/BuBundleMappingPopUpContent";
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData,
  resetDeleteBuBundleData
} from '../../actions';

class BuBundleMappingPopUp extends React.Component {
  constructor(props) {
    super(props);
  }
  handleClosePopUp = () => {

    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();

  }

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
    this.props.resetDeleteBuBundleData();
  }

  render() {
    let message = null;
    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = <Message messageData="Error occurred while saving rules" error />);
    }else if(!_.isEmpty(this.props.deleteBuBundleMappingData)){
      !this.props.deleteBuBundleMappingData.errorMessage
        ? (message = <Message messageData="BU Bundle Mapping Deleted" success />)
        : (message = (
            <Message messageData="Error deleting Bu Bundle Mapping" error />
        ));
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.BUBUNDLE &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          onClose={this.handleClosePopUp}
          style={{
            width: "1080px"
          }}
        >
          <BuBundleMappingPopUpContent />
        </Dialog>
        <MessageDialog
          isOpen={
            this.props.view === onboardingScreen.BUBUNDLE &&
            (!_.isEmpty(this.props.saveSimulatedData) ||
             !_.isEmpty(this.props.deleteBuBundleMappingData))
          }
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData,
      resetDeleteBuBundleData,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    deleteBuBundleMappingData: state.financingOnboarding.deleteBuBundleMappingData
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuBundleMappingPopUp);
