import React from "react";
import Dialog from "commons/components/Dialog";
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { onboardingScreen } from "../../util";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import GenevaBookMappingPopUpContent from "../container/GenevaBookMappingPopUpContent";
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData
} from '../../actions';
class GenevaBookMappingPopUp extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClosePopUp = () => {

    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();

  }

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
  }

  render() {
    let message = null;
    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = <Message messageData="Error occurred while saving rules" error />);
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.GENEVABOOK &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          onClose={this.handleClosePopUp}
          style={{
            width: "1080px"
          }}
        >
          <GenevaBookMappingPopUpContent />
        </Dialog>
        <MessageDialog
          isOpen={this.props.view === onboardingScreen.GENEVABOOK
            && !_.isEmpty(this.props.saveSimulatedData)}
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    saveSimulatedData: state.financingOnboarding.saveSimulatedData
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GenevaBookMappingPopUp);
