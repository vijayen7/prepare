import React from "react";
import Dialog from "commons/components/Dialog";
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { onboardingScreen } from "../../util";
import ReportingCurrencyPopUpContent from "../container/ReportingCurrencyPopUpContent";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData,
  resetDeleteReportingData
} from '../../actions';


class ReportingCurrencyPopUp extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClosePopUp = () => {

    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();

  }

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
    this.props.resetDeleteReportingData();
  }

  render() {
    let message = null;
    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = <Message messageData="Error occurred while saving rules" error />);
    } else if(!_.isEmpty(this.props.deleteReportingCurrencyData)){
      !this.props.deleteReportingCurrencyData.errorMessage
        ? (message = <Message messageData="Accrual Posting Currency Deleted" success />)
        : (message = (
            <Message messageData="Error deleting Accrual Posting Currency Posting" error />
          ));
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.REPORTING &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          onClose={this.handleClosePopUp}
          style={{
            width: "1080px"
          }}
        >
          <ReportingCurrencyPopUpContent />
        </Dialog>
        <MessageDialog
          isOpen={
            this.props.view === onboardingScreen.REPORTING &&
            (!_.isEmpty(this.props.saveSimulatedData) ||
             !_.isEmpty(this.props.deleteReportingCurrencyData))
          }
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData,
      resetDeleteReportingData,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    deleteReportingCurrencyData: state.financingOnboarding.deleteReportingCurrencyData,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportingCurrencyPopUp);
