import React from 'react';
import { Layout } from 'arc-react-components';
import CustodianAccountFilter from 'commons/container/CustodianAccountFilter';
import CustodianAccountSpecificNettingGroupsFilter from 'commons/container/CustodianAccountSpecificNettingGroupsFilter';
import { getDisabledFilter } from '../../util';

import { getValueForSingleSelect } from '../../util';
import { NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER } from 'commons/constants';

const NettingGroupPopUpFilters = (props) => {
  const selectedCustodianAccountIds = getValueForSingleSelect(
    props.state.selectedCustodianAccounts
  );
  return (
    <Layout isRowType>
      <Layout.Child>
        {props.isEditClicked ? (
          getDisabledFilter('Custodian Account:', props.state.selectedCustodianAccounts.value)
        ) : (
          <CustodianAccountFilter
            onSelect={props.onSelect}
            selectedData={props.state.selectedCustodianAccounts}
            multiSelect={false}
            name={NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER}
            stateName="nettingGroupSpecificCustodianAccounts"
            errorDiv={props.state.error.selectedCustodianAccounts}
            horizontalLayout
          />
        )}
        <CustodianAccountSpecificNettingGroupsFilter
          onSelect={props.onSelect}
          selectedData={props.state.selectedNettingGroups}
          custodianAccountIds={selectedCustodianAccountIds}
          multiSelect={false}
          errorDiv={props.state.error.selectedNettingGroups}
          horizontalLayout
        />
      </Layout.Child>
    </Layout>
  );
};

export default NettingGroupPopUpFilters;
