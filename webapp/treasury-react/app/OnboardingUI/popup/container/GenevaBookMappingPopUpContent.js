import React, { Component } from "react";
import _ from "lodash";
import Dialog from "commons/components/Dialog";
import { Layout, Card } from "arc-react-components";
import Message from "commons/components/Message";
import FilterButton from 'commons/components/FilterButton';
import MessageDialog from "commons/components/MessageDialog";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getValueForSingleSelectOrUndefined } from "../../util";
import {
  destroyDeleteToggle,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData,
  hidePrimaryPopUp
} from '../../actions';
import { getFirstDateOfCurrentMonth, getFirstDateOfMonthForGivenDate } from 'commons/util';
import PopUpSimulatedGrids from '../components/PopUpSimulatedGrids';
import { onboardingScreen } from '../../util';
import GenevaBookMappingPopUpFilters from '../components/GenevaBookMappingPopUpFilters';

class GenevaBookMappingPopUpContent extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.populateValuesForEdit = this.populateValuesForEdit.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleSimulate = this.handleSimulate.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCloseMessagePopup = this.handleCloseMessagePopup.bind(this);
    this.validateSimulateParameters = this.validateSimulateParameters.bind(this);
    this.renderSimulateButton = this.renderSimulateButton.bind(this);
    this.state = this.getDefaultState();
  }

  componentDidMount() {
    if (this.props.widgetView) {
      this.setState(
        {
          selectedLegalEntities: this.props.selectedData.selectedLegalEntities,
          selectedCurrencies: this.props.selectedData.selectedCurrencies,
          selectedCpes: this.props.selectedData.selectedCpes,
          selectedNettingGroups: this.props.selectedData.selectedNettingGroups,
          selectedAgreementTypes: this.props.selectedData.selectedAgreementTypes
        },
        () => {
          this.handleSimulate(true);
        }
      );
    }
    else {
      if (
        this.props.view === onboardingScreen.GENEVABOOK &&
        !_.isEmpty(this.props.selectedRowProps)
      ) {
        var populateValuesForEdit = this.populateValuesForEdit();
        this.setState(populateValuesForEdit);
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.view === onboardingScreen.GENEVABOOK &&
      _.isEmpty(prevProps.selectedRowProps) &&
      !_.isEmpty(this.props.selectedRowProps)
    ) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  onSelect(params) {
    const { key, value } = params;
    this.setState({ [key]: value });
  }

  getDefaultState() {
    return {
      selectedLegalEntities: [],
      selectedCurrencies: [],
      selectedCpes: [],
      selectedNettingGroups: [],
      selectedAgreementTypes: [],
      selectedBooks: [],
      selectedDate: getFirstDateOfCurrentMonth(),
      isSimulate: false,
      selectedRowData: [],
      saveSelectMessage: false,
      error: {
        selectedLegalEntities: false,
        selectedBooks: false,
        selectedDate: false
      }
    };
  }

  populateValuesForEdit() {
    const rowData = this.props.selectedRowProps;
    let populateValues = {};

    if (
      typeof rowData.legal_entity_id !== "undefined" &&
      rowData.legal_entity_id !== ""
    ) {
      populateValues.selectedLegalEntities = {
        key: rowData.legal_entity_id,
        value: `${rowData.legalEntity} [${rowData.legal_entity_id}]`
      };
    }

    populateValues.selectedBooks = {
      key: rowData.book_id,
      value: `${rowData.book} [${rowData.book_id}]`
    };
    populateValues.selectedDate = this.state.selectedDate;

    if (
      typeof rowData.agreement_type_id !== "undefined" &&
      rowData.agreement_type_id !== ""
    ) {
    populateValues.selectedAgreementTypes = {
      key: rowData.agreement_type_id,
      value: `${rowData.agreementType} [${rowData.agreement_type_id}]`
    }
  }
  if (
    typeof rowData.currency_id !== "undefined" &&
    rowData.currency_id !== ""
  )  {
    populateValues.selectedCurrencies = {
      key: rowData.currency_id,
      value: `${rowData.ccy} [${rowData.currency_id}]`
    }
  }
  if (
    typeof rowData.netting_group_id !== "undefined" &&
    rowData.netting_group_id !== ""
  )  {
    populateValues.selectedNettingGroups = {
      key: rowData.netting_group_id,
      value: `${rowData.nettingGroup} [${rowData.netting_group_id}]`
    }
  }
  if (
    typeof rowData.cpe_id !== "undefined" &&
    rowData.cpe_id !== ""
  )  {
    populateValues.selectedCpes = {
      key: rowData.cpe_id,
      value: `${rowData.cpe} [${rowData.cpe_id}]`
    }
  }

    return populateValues;
  }

  handleClosePopUp() {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.setState(this.getDefaultState());
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  }

  handleCloseMessagePopup() {
    this.props.resetSaveData();
  }

  handleSimulate(withoutValidation) {
    let payload = {
      "ruleSystemFilter.legalEntityIds": getValueForSingleSelectOrUndefined(
        this.state.selectedLegalEntities
      ),
      "ruleSystemFilter.currencyIds": getValueForSingleSelectOrUndefined(this.state.selectedCurrencies),
      "ruleSystemFilter.cpeIds": getValueForSingleSelectOrUndefined(this.state.selectedCpes),
      "ruleSystemFilter.agreementTypeIds": getValueForSingleSelectOrUndefined(this.state.selectedAgreementTypes),
      "ruleSystemFilter.nettingGroupIds": getValueForSingleSelectOrUndefined(this.state.selectedNettingGroups),

      "ruleSystemFilter.bookId": getValueForSingleSelectOrUndefined(
        this.state.selectedBooks
      ),
      "ruleSystemFilter.startDate": this.state.selectedDate
    };
    if (
      (withoutValidation !== undefined && typeof withoutValidation === 'boolean' && withoutValidation) ||
      this.validateSimulateParameters(payload)
    ) {
      this.props.fetchSimulateData(payload, onboardingScreen.GENEVABOOK);
      this.setState({ isSimulate: true });
    }
  }

  validateSimulateParameters(parameters) {
    let ret = true;
    const error = this.getDefaultState().error;
    if (parameters["ruleSystemFilter.legalEntityIds"] === undefined ||
        parameters["ruleSystemFilter.legalEntityIds"] == "-1") {
      ret = false;
      error.selectedLegalEntities = true;
    }
    if (parameters["ruleSystemFilter.bookId"] === undefined ||
        parameters["ruleSystemFilter.bookId"] == "-1") {
      ret = false;
      error.selectedBooks = true;
    }
    if (parameters["ruleSystemFilter.startDate"] === "") {
      ret = false;
      error.selectedDate = true;
    }
    this.setState({ error });
    return ret;
  }

  handleSave() {
    if (this.state.selectedRowData.length === 0) {
      this.setState({ saveSelectMessage: true });
      return;
    }

    if (this.props.widgetView) {
      this.props.saveAndValidateWidgetView(
        this.state.selectedRowData,
        onboardingScreen.GENEVABOOK,
        true
      );
      this.props.resetSimulateData();
      this.props.resetSelectedRowData();
    } else {
      let payload = this.state.selectedRowData.map(row => row.columnValues);
      this.props.saveSimulateData(payload, onboardingScreen.GENEVABOOK);
      this.handleClosePopUp();
    }
  }
  renderSimulateButton(isSaveDisabled) {
    if (isSaveDisabled) {
      return (
        <div style={{ width: '500px', margin: 'auto' }}>
          <Message
            warning
            messageData={
              <div>
                You need to simulate the rule before you can save it.
                <FilterButton
                  onClick={this.handleSimulate}
                  label={'Run Simulation'}
                  className="margin--small button--primary"
                />
              </div>
            }
          />
        </div>
      );
    } else {
      return (
        <div>
          SIMULATION RESULTS
          <FilterButton
            onClick={this.handleSimulate}
            label={'Re-run Simulation'}
            className="margin--small margin--left button--primary"
          />
        </div>
      );
    }
  }

  renderPopUp() {
    let popUp = null;
    let message = null;
    let title;
    title = _.isEmpty(this.props.selectedRowProps)
      ? "ADD ACCOUNTING BOOK MAPPING"
      : "EDIT ACCOUNTING BOOK MAPPING";

    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = <Message messageData="Error occurred while saving rules" error />);
    }
    let isSaveDisabled =
      !this.state.isSimulate ||
      typeof this.props.simulatedData === 'undefined' ||
      typeof this.props.simulatedData.existingList === 'undefined' ||
      typeof this.props.simulatedData.simulatedList === 'undefined';

    let popupWidth = isSaveDisabled ? '800px' : '1000px';
    popUp = (
      <React.Fragment>
        <Card title="Rule Configuration">
          <hr />
          <div style={{ width: '600px' }} class="text-align--right">
            <GenevaBookMappingPopUpFilters
              onSelect={this.onSelect}
              state={this.state}
              isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
            />
          </div>
        </Card>
        <br />
        {this.renderSimulateButton(isSaveDisabled)}
        <PopUpSimulatedGrids
          isSimulate={this.state.isSimulate}
          simulatedData={this.props.simulatedData}
          view={onboardingScreen.GENEVABOOK}
          saveSelectMessage={this.state.saveSelectMessage}
          onSelect={this.onSelect}
          handleSave={this.handleSave}
        />
        <FilterButton onClick={this.handleSave} disabled={isSaveDisabled || this.state.selectedRowData.length === 0} label="Save" />
        <MessageDialog
          isOpen={!_.isEmpty(this.props.saveSimulatedData)}
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );

    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData,
      hidePrimaryPopUp
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen.genevaBook,
    isDeleteClicked: state.financingOnboarding.isDeleteClicked,
    simulatedData: state.financingOnboarding.simulatedData,
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    selectedRowProps: state.financingOnboarding.selectedRowData,
    view: state.financingOnboarding.view
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GenevaBookMappingPopUpContent);
