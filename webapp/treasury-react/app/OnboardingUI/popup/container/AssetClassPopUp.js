import React, { Component } from "react";
import _ from "lodash";
import Dialog from "commons/components/Dialog";
import Message from "commons/components/Message";
import MessageDialog from "commons/components/MessageDialog";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchSimulateData,
  saveAssetClass,
  saveSimulateData,
  fetchAgreementsAffectedForAsset,
  resetSaveData,
  resetSaveAssetClassData,
  resetSelectedRowData,
  hidePrimaryPopUp
} from "../../actions";
import AssetClassPopUpFilters from "./AssetClassPopUpFilters";
import { onboardingScreen } from "../../util";

class AssetClassPopUp extends Component {
  constructor(props) {
    super(props);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.handleCloseMessagePopup = this.handleCloseMessagePopup.bind(this);
  }

  handleClosePopUp() {
    this.props.hidePrimaryPopUp();
    this.props.resetSelectedRowData();
  }

  handleCloseMessagePopup() {
    this.props.resetSaveData();
    this.props.resetSaveAssetClassData();
  }

  renderPopUp() {
    let popUp = null;
    let message = null;
    let title = _.isEmpty(this.props.selectedRowProps)
      ? "ADD ASSET CLASS GROUP"
      : "EDIT ASSET CLASS GROUP (" +
        this.props.selectedRowProps.displayName +
        ")";

    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = (
            <Message
              messageData="Asset Class Group added successfully"
              success
            />
          ))
        : (message = (
            <Message messageData="Asset Class Group could not be added" error />
          ));
    } else if (!_.isEmpty(this.props.saveAssetClassData)) {
      !this.props.saveAssetClassData.errorMessage
        ? (message = (
            <Message messageData="Asset Class added successfully" success />
          ))
        : (message = (this.props.saveAssetClassData.errorMessage.includes(" already exists"))
        ?(<Message messageData="Asset Class with this name or properties already exists" error />):
        (<Message messageData="Asset Class Group could not be added" error />) );
    }
    popUp = (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.ASSET &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          title={title}
          onClose={this.handleClosePopUp}
          style={{
            width: "880px"
          }}
        >
          <div style={{ width: "860px" }}>
            <AssetClassPopUpFilters
              isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
              fetchSimulateData={this.props.fetchSimulateData}
              assetClassData={this.props.simulatedData}
              assetClassGroupDataById={this.props.assetClassGroupDataById}
              saveAssetClassGroup={this.props.saveSimulateData}
              saveAssetClass={this.props.saveAssetClass}
              fetchAgreementsAffectedForAsset={
                this.props.fetchAgreementsAffectedForAsset
              }
              handleClosePopUp={this.handleClosePopUp}
              selectedRowProps={this.props.selectedRowProps}
              agreementsAffectedForAsset={this.props.agreementsAffectedForAsset}
            />
          </div>
        </Dialog>
        <MessageDialog
          isOpen={
            this.props.view === onboardingScreen.ASSET &&
            (!_.isEmpty(this.props.saveSimulatedData) ||
            !_.isEmpty(this.props.saveAssetClassData))
          }
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );

    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchSimulateData,
      saveAssetClass,
      saveSimulateData,
      fetchAgreementsAffectedForAsset,
      resetSaveData,
      resetSaveAssetClassData,
      resetSelectedRowData,
      hidePrimaryPopUp
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen.asset,
    simulatedData: state.financingOnboarding.simulatedData.ruleList,
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    saveAssetClassData: state.financingOnboarding.saveAssetClassData,
    assetClassGroupDataById: state.financingOnboarding.assetClassGroupDataById,
    agreementsAffectedForAsset:
      state.financingOnboarding.agreementsAffectedForAsset,
    selectedRowProps: state.financingOnboarding.selectedRowData,
    view: state.financingOnboarding.view
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AssetClassPopUp);
