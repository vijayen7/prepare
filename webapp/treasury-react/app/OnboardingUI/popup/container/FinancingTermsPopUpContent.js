import React, { Component } from "react";
import _ from "lodash";
import Message from "commons/components/Message";
import MessageDialog from "commons/components/MessageDialog";
import FilterButton from "commons/components/FilterButton";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getValueForSingleSelect, getZeroForDefault, getDataValueForSingleSelect,
  isFinancingTermTypeAsset, isFinancingTermTypeAccount, financingAgreementTermType,
  isFinancingTermTypeAgreement} from "../../util";
import {
  destroyDeleteToggle,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  deleteFinancingTerm,
  resetDeleteTermData,
  resetSimulateData,
  resetSelectedRowData,
  hidePrimaryPopUp,
} from "../../actions";
import { getFirstDateOfCurrentMonth } from "commons/util";
import FinancingTermsPopUpFilters from "../components/FinancingTermsPopUpFilters";
import PopUpSimulatedGrids from "../components/PopUpSimulatedGrids";
import { onboardingScreen } from "../../util";
import { Layout } from "arc-react-components";
import Panel from "commons/components/Panel";

class FinancingTermsPopUpContent extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.populateValuesForEdit = this.populateValuesForEdit.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleSimulate = this.handleSimulate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCloseMessagePopup = this.handleCloseMessagePopup.bind(this);
    this.renderSimulateButton = this.renderSimulateButton.bind(this);
    this.validateSimulateParameters = this.validateSimulateParameters.bind(
      this
    );
    this.resetDependentFilters = this.resetDependentFilters.bind(this);
    this.state = this.getDefaultState();
  }

  componentDidMount() {
    if (this.props.widgetView) {
      this.setState(
        {
          selectedLegalEntities: this.props.selectedData.selectedLegalEntities,
          selectedCpes: this.props.selectedData.selectedCpes,
          selectedAgreementTypes: this.props.selectedData
            .selectedAgreementTypes,
          selectedNettingGroups: this.props.selectedData.selectedNettingGroups,
          selectedCurrencies: this.props.selectedData.selectedCurrencies,
          selectedAgreements: this.props.selectedData.selectedAgreements,
        },
        () => {
          this.handleSimulate(true);
        }
      );
    } else {
      if (
        this.props.view === onboardingScreen.FINANCING &&
        !_.isEmpty(this.props.selectedRowProps)
      ) {
        var populateValuesForEdit = this.populateValuesForEdit();
        this.setState(populateValuesForEdit);
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.view === onboardingScreen.FINANCING &&
      _.isEmpty(prevProps.selectedRowProps) &&
      !_.isEmpty(this.props.selectedRowProps)
    ) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  onSelect(params) {
    const { key, value } = params;
    if (key === "differentRates") {
      if (!value) {
        this.setState({
          selectedCreditBaseRates: [],
          selectedDebitBaseRates: [],
          selectedRebateBaseRates: [],
        });
      } else {
        this.setState({ selectedBaseRates: [] });
      }
    }
    if (key === "differentConvention") {
      if (!value) {
        this.setState({
          selectedCreditAccrualConventions: [],
          selectedDebitAccrualConventions: [],
        });
      } else {
        this.setState({ selectedAccrualConventions: [] });
      }
    }
    this.resetDependentFilters(key, value);
    let valueToSet = value;
    if (value === null) {
      const defaultState = this.getDefaultState();
      valueToSet = defaultState[key];
    }
    if (key === "selectedCurrencies") {
      this.setState({
        selectedBaseRateCurrencies: valueToSet,
      });
    }
    this.setState({ [key]: valueToSet });
  }

  resetDependentFilters(key, value) {
    if (key === "selectedAgreements") {
      this.setState({ selectedNettingGroups: [] });
      this.setState({selectedCustodianAccounts: []});
    } else if (key === "selectedBaseRateCurrencies") {
      this.setState({
        selectedBaseRates: [],
        selectedCreditBaseRates: [],
        selectedDebitBaseRates: [],
        selectedRebateBaseRates: [],
      });
    } else if (key === "selectedNettingGroups") {
      this.setState({selectedCustodianAccounts: []});
    }
  }

  getDefaultState() {
    return {
      selectedAgreements: [],
      selectedNettingGroups: [],
      selectedCurrencies: [],
      selectedBaseRateCurrencies: [],
      selectedBaseRates: [],
      selectedCreditBaseRates: [],
      selectedDebitBaseRates: [],
      selectedRebateBaseRates: [],
      selectedDate: getFirstDateOfCurrentMonth(),
      selectedBandwidthGroups: [],
      selectedCustodianAccounts: [],
      selectedFinancingAgreementTermTypes:   { key: "0", value: financingAgreementTermType.AGREEMENT_LEVEL },
      selectedFinancingStyles: [],
      selectedLeanSurchargeModel: [],
      selectedAssetClassGroups: [],
      selectedAccrualConventions: [],
      selectedCreditAccrualConventions: [],
      selectedDebitAccrualConventions: [],
      selectedApplyNegativeCreditIRates: [],
      selectedApplyNegativeDebitIRates: [],
      selectedApplyNegativeRebateIRates: [],
      creditSpread: [],
      rebateSpread: [],
      debitSpread: [],
      gcRate: [],
      gcShortSpread: [],
      smvConversionFactor: [],
      haircut: [],
      marginFactor: [],
      differentConvention: false,
      differentRates: false,
      isSimulate: false,
      comment: "",
      selectedRowData: [],
      saveSelectMessage: false,
      error: {
        selectedNettingGroups: false,
        selectedCustodianAccounts: false,
        selectedAssetClassGroups:false,
        selectedAgreements: false,
        selectedCurrencies: false,
        selectedAccrualConventions: false,
        selectedDebitAccrualConventions: false,
        selectedCreditAccrualConventions: false,
        selectedBaseRates: false,
        selectedCreditBaseRates: false,
        selectedDebitBaseRates: false,
        gcRate: false,
        selectedDate: false,
      },
    };
  }

  populateValuesForEdit() {
    const rowData = this.props.selectedRowProps;
    let populateValues = {};

    if (
      typeof rowData.financingMethodology !== "undefined" &&
      typeof rowData.financingMethodology.agreementId !== "undefined"
    ) {
      populateValues.selectedAgreements = {
        key: rowData.financingMethodology.agreementId,
        value: `${rowData.financingMethodology.legalEntity}-${rowData.financingMethodology.cpe}-${rowData.financingMethodology.agreementType}`,
      };
    }
    if (
      typeof rowData.financingMethodology !== "undefined" &&
      typeof rowData.financingMethodology.nettingGroup !== "undefined"
    ) {
      populateValues.selectedNettingGroups = {
        key: rowData.financingMethodology.nettingGroup.myNettingGroupId,
        value: `${rowData.financingMethodology.nettingGroup.myDescription} [${rowData.financingMethodology.nettingGroup.myNettingGroupId}]`,
      };
    }
    if (
      typeof rowData.financingMethodology !== "undefined" &&
      typeof rowData.financingMethodology.currency !== "undefined"
    ) {
      populateValues.selectedCurrencies = {
        key: rowData.financingMethodology.currency.mySpn,
        value: `${rowData.financingMethodology.currency.myAbbreviation} [${rowData.financingMethodology.currency.mySpn}]`,
      };
    }
    if (
      typeof rowData.assetClassGroup !== "undefined" &&
      rowData.assetClassGroup !== null
    ) {
      populateValues.selectedAssetClassGroups = {
        key: rowData.assetClassGroup.assetClassGroupId,
        value: `${rowData.assetClassGroup.abbreviation} [${rowData.assetClassGroup.assetClassGroupId}]`,
      };
    }
    if (
      typeof rowData.bandwidthGroup !== "undefined" &&
      rowData.bandwidthGroup !== null
    ) {
      populateValues.selectedBandwidthGroups = {
        key: rowData.bandwidthGroup.bandwidthGroupId,
        value: `${rowData.bandwidthGroup.displayName} [${rowData.bandwidthGroup.bandwidthGroupId}]`,
        value: `${rowData.bandwidthGroup.displayName} [${
          rowData.bandwidthGroup.bandwidthGroupId
          }]`
      };
    }
    if (
      typeof rowData.custodianAccountId !== "undefined" &&
      rowData.custodianAccountId !== null
    ) {
      populateValues.selectedCustodianAccounts = {
        key: rowData.custodianAccountId,
        value: `${rowData.custodianAccountName} [${
          rowData.custodianAccountId
          }]`
      };
    }
    if (
      typeof rowData.financingAgreementTermType !== "undefined" &&
      rowData.financingAgreementTermType !== null
    ) {
      populateValues.selectedFinancingAgreementTermTypes = {
        value: `${rowData.financingAgreementTermType}`
      };
    }
    if (typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[7] !== "undefined" &&
      rowData.financingTerm.attributeList[7].rvalue !== null
    ) {
      populateValues.marginFactor =
        rowData.financingTerm.attributeList[7].rvalue;
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[8] !== "undefined" &&
      rowData.financingTerm.attributeList[8].rvalue !== null
    ) {
      populateValues.rebateSpread =
        rowData.financingTerm.attributeList[8].rvalue * 100;
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[9] !== "undefined" &&
      rowData.financingTerm.attributeList[9].ivalue !== null
    ) {
      populateValues.selectedLeanSurchargeModel = {
        key: rowData.financingTerm.attributeList[9].ivalue,
        value: rowData.financingTerm.attributeList[9].attributeValueName
      }
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.creditSpread !== "undefined" &&
      rowData.financingTerm.creditSpread !== null
    ) {
      populateValues.creditSpread = rowData.financingTerm.creditSpread * 100;
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.debitSpread !== "undefined" &&
      rowData.financingTerm.debitSpread !== null
    ) {
      populateValues.debitSpread = rowData.financingTerm.debitSpread * 100;
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.gcRate !== "undefined" &&
      rowData.financingTerm.gcRate !== null
    ) {
      populateValues.gcRate = rowData.financingTerm.gcRate * 100;
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[6] !== "undefined" &&
      rowData.financingTerm.attributeList[6].rvalue !== null
    ) {
      populateValues.gcShortSpread =
        rowData.financingTerm.attributeList[6].rvalue;
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[4] !== "undefined" &&
      rowData.financingTerm.attributeList[4].rvalue !== null
    ) {
      populateValues.haircut = rowData.financingTerm.attributeList[4].rvalue;
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[5] !== "undefined" &&
      rowData.financingTerm.attributeList[5].attributeValueName !== null
    ) {
      populateValues.selectedFinancingStyles = {
        key: rowData.financingTerm.attributeList[5].ivalue,
        value: `${rowData.financingTerm.attributeList[5].attributeValueName}`,
      };
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[1] !== "undefined" &&
      rowData.financingTerm.attributeList[1].rvalue !== null
    ) {
      populateValues.smvConversionFactor =
        rowData.financingTerm.attributeList[1].rvalue;
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.baseRateCurrency !== "undefined" &&
      rowData.financingTerm.baseRateCurrency != null
    ) {
      var currencyMap = this.props.currencies.reduce(function (map, obj) {
        map[obj.key] = obj.value;
        return map;
      }, {});
      populateValues.selectedBaseRateCurrencies = {
        key: rowData.financingTerm.baseRateCurrency,
        value: currencyMap[rowData.financingTerm.baseRateCurrency],
      };
    }
    if (typeof rowData.financingTerm !== "undefined") {
      var creditValue = rowData.financingTerm.creditAccrualConvention;
      var debitValue = rowData.financingTerm.debitAccrualConvention;
      if (
        typeof creditValue !== "undefined" ||
        typeof debitValue !== "undefined"
      ) {
        if (creditValue === debitValue) {
          populateValues.differentConvention = false;
          populateValues.selectedAccrualConventions = {
            key: creditValue,
            value: `${creditValue}`,
          };
        } else {
          populateValues.differentConvention = true;
          if (typeof creditValue !== "undefined") {
            populateValues.selectedCreditAccrualConventions = {
              key: creditValue,
              value: `${creditValue}`,
            };
          }
          if (typeof debitvalue !== "undefined") {
            populateValues.selectedDebitAccrualConventions = {
              key: debitValue,
              value: `${debitValue}`,
            };
          }
        }
      }
      var creditRate = rowData.financingTerm.creditBaseRateTs;
      var debitRate = rowData.financingTerm.debitBaseRateTs;
      var rebateRate = rowData.financingTerm.rebateBaseRateTs;
      if (
        typeof creditRate !== "undefined" &&
        typeof debitRate !== "undefined" &&
        typeof rebateRate !== "undefined"
      ) {
        if (
          creditRate !== null &&
          debitRate !== null &&
          rebateRate !== null &&
          creditRate.myName === debitRate.myName &&
          debitRate.myName === rebateRate.myName
        ) {
          populateValues.differentRates = false;
          populateValues.selectedBaseRates = {
            key: creditRate.myTypeId,
            value: `${creditRate.myName}`,
          };
        } else {
          populateValues.differentRates = true;
          if (typeof creditRate !== "undefined" && creditRate !== null) {
            populateValues.selectedCreditBaseRates = {
              key: creditRate.myTypeId,
              value: `${creditRate.myName}`,
            };
          }
          if (typeof debitRate !== "undefined" && debitRate !== null) {
            populateValues.selectedDebitBaseRates = {
              key: debitRate.myTypeId,
              value: `${debitRate.myName}`,
            };
          }
          if (typeof rebateRate !== "undefined" && rebateRate !== null) {
            populateValues.selectedRebateBaseRates = {
              key: rebateRate.myTypeId,
              value: `${rebateRate.myName}`,
            };
          }
        }
      } else {
        populateValues.differentRates = true;
        if (typeof creditRate !== "undefined" && creditRate !== null) {
          populateValues.selectedCreditBaseRates = {
            key: creditRate.myTypeId,
            value: `${creditRate.myName}`,
          };
        }
        if (typeof debitRate !== "undefined" && debitRate !== null) {
          populateValues.selectedDebitBaseRates = {
            key: debitRate.myTypeId,
            value: `${debitRate.myName}`,
          };
        }
        if (typeof rebateRate !== "undefined" && rebateRate !== null) {
          populateValues.selectedRebateBaseRates = {
            key: rebateRate.myTypeId,
            value: `${rebateRate.myName}`,
          };
        }
      }
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[0] !== "undefined"
    ) {
      populateValues.selectedApplyNegativeCreditIRates = {
        key: rowData.financingTerm.attributeList[0].ivalue,
        value: `${rowData.financingTerm.attributeList[0].attributeValueName}`,
      };
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[2] !== "undefined"
    ) {
      populateValues.selectedApplyNegativeDebitIRates = {
        key: rowData.financingTerm.attributeList[2].ivalue,
        value: `${rowData.financingTerm.attributeList[2].attributeValueName}`,
      };
    }
    if (
      typeof rowData.financingTerm !== "undefined" &&
      typeof rowData.financingTerm.attributeList[3] !== "undefined"
    ) {
      populateValues.selectedApplyNegativeRebateIRates = {
        key: rowData.financingTerm.attributeList[3].ivalue,
        value: `${rowData.financingTerm.attributeList[3].attributeValueName}`,
      };
    }
    populateValues.selectedDate = this.state.selectedDate;

    return populateValues;
  }

  handleClosePopUp() {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.setState(this.getDefaultState());
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  }

  handleCloseMessagePopup() {
    this.props.resetSaveData();
    this.props.resetDeleteTermData();
  }

  handleSimulate(withoutValidation) {
    let termType = getDataValueForSingleSelect(this.state.selectedFinancingAgreementTermTypes);
    let payload;

    payload = {
      agreementFilterIds: getValueForSingleSelect(
        this.state.selectedAgreements
      ),
      currencyFilterIds: getValueForSingleSelect(this.state.selectedCurrencies),
      baseRateCurrencyFilterIds: getValueForSingleSelect(
        this.state.selectedBaseRateCurrencies
      ),
      assetClassGroupFilterId: getValueForSingleSelect(
        this.state.selectedAssetClassGroups
      ),
      bandwidthGroupFilterId: getValueForSingleSelect(
        this.state.selectedBandwidthGroups
      ),
      accrualConventionInput: getValueForSingleSelect(
        this.state.selectedAccrualConventions
      ),
      debitAccrualConvention: getValueForSingleSelect(
        this.state.selectedDebitAccrualConventions
      ),
      creditAccrualConvention: getValueForSingleSelect(
        this.state.selectedCreditAccrualConventions
      ),
      financingStyleFilterId: getValueForSingleSelect(
        this.state.selectedFinancingStyles
      ),
      debitIRateFilterId: getValueForSingleSelect(
        this.state.selectedApplyNegativeDebitIRates
      ),
      creditIRateFilterId: getValueForSingleSelect(
        this.state.selectedApplyNegativeCreditIRates
      ),
      nettingGroupFilterIds: getValueForSingleSelect(
        this.state.selectedNettingGroups
      ),
      custodianAccountFilterIds: getValueForSingleSelect(
        this.state.selectedCustodianAccounts
      ),
      baseRateFilterIds: getValueForSingleSelect(this.state.selectedBaseRates),
      creditBaseRateFilterIds: getValueForSingleSelect(
        this.state.selectedCreditBaseRates
      ),
      debitBaseRateFilterIds: getValueForSingleSelect(
        this.state.selectedDebitBaseRates
      ),
      rebateBaseRateFilterIds: getValueForSingleSelect(
        this.state.selectedRebateBaseRates
      ),
      dateString: this.state.selectedDate,
      creditSpread: getZeroForDefault(this.state.creditSpread) / 100,
      debitSpread: getZeroForDefault(this.state.debitSpread) / 100,
      rebateSpread:
        this.state.rebateSpread.length === 0
          ? ""
          : this.state.rebateSpread / 100,
      gcRate: getZeroForDefault(this.state.gcRate) / 100,
      ApplicableNegativeCreditIrate: getValueForSingleSelect(
        this.state.selectedApplyNegativeCreditIRates
      ),
      ApplicableNegativeDebitIrate: getValueForSingleSelect(
        this.state.selectedApplyNegativeDebitIRates
      ),
      ApplicableNegativeIrateSfic: getValueForSingleSelect(
        this.state.selectedApplyNegativeRebateIRates
      ),
      FinancingStyleId: getValueForSingleSelect(
        this.state.selectedFinancingStyles
      ),
      GcShortSpread:
        this.state.gcShortSpread.length === 0
          ? "null"
          : this.state.gcShortSpread / 100,
      Haircut: this.state.haircut.length === 0 ? "null" : this.state.haircut,
      marginFactor:
        this.state.marginFactor.length === 0 ? "" : this.state.marginFactor,
      SmvConversionFactor:
        this.state.smvConversionFactor.length === 0
          ? "null"
          : this.state.smvConversionFactor,
      comment: this.state.comment,
      validFrom: this.state.selectedDate,
      leanSurchargeModelId: isFinancingTermTypeAgreement(termType)
      ? getValueForSingleSelect(this.state.selectedLeanSurchargeModel) : "null"
    };
    if (!this.state.differentConvention) {
      payload.creditAccrualConvention = payload.accrualConventionInput;
      payload.debitAccrualConvention = payload.accrualConventionInput;
    }
    if (!this.state.differentRates) {
      if (payload.baseRateFilterIds === "-1") {
        payload.baseRateFilterIds = 0;
      }
      payload.creditBaseRateFilterIds = payload.baseRateFilterIds;
      payload.debitBaseRateFilterIds = payload.baseRateFilterIds;
      payload.rebateBaseRateFilterIds = payload.baseRateFilterIds;
    } else {
      if (payload.creditBaseRateFilterIds === "-1") {
        payload.creditBaseRateFilterIds = 0;
      }
      if (payload.debitBaseRateFilterIds === "-1") {
        payload.debitBaseRateFilterIds = 0;
      }
      if (payload.rebateBaseRateFilterIds === "-1") {
        payload.rebateBaseRateFilterIds = 0;
      }
    }
    if (payload.financingStyleFilterId === "-1") {
      payload.FinancingStyleId = "null";
    }
    if (!withoutValidation) {
      if (this.validateSimulateParameters(payload)) {
        this.props.fetchSimulateData(payload, onboardingScreen.FINANCING);
        this.setState({ isSimulate: true });
      }
    } else {
      this.props.fetchSimulateData(payload, onboardingScreen.FINANCING);
      this.setState({ isSimulate: true });
    }
  }

  handleDelete() {
    const payload = {
      comment: this.state.comment,
      dateString: this.state.selectedDate,
      rulesToSave: "[" + JSON.stringify(this.props.selectedRowProps) + "]",
    };
    this.props.deleteFinancingTerm(payload);
    this.handleClosePopUp();
  }

  validateSimulateParameters(parameters) {
    let ret = true;
    const error = this.getDefaultState().error;

    if (parameters.agreementFilterIds == "-1") {
      ret = false;
      error.selectedAgreements = true;
    }
    if (parameters.nettingGroupFilterIds === "-1") {
      ret = false;
      error.selectedNettingGroups = true;
    }
    if (parameters.currencyFilterIds === "-1") {
      ret = false;
      error.selectedCurrencies = true;
    }
    if (this.state.differentConvention == false) {
      if (parameters.accrualConventionInput == "-1") {
        ret = false;
        error.selectedAccrualConventions = true;
      }
    } else {
      if (parameters.creditAccrualConvention == "-1") {
        ret = false;
        error.selectedCreditAccrualConventions = true;
      }
      if (parameters.debitAccrualConvention == "-1") {
        ret = false;
        error.selectedDebitAccrualConventions = true;
      }
    }
    if (this.state.differentRates == false) {
      if (
        parameters.baseRateFilterIds == "-1" ||
        parameters.baseRateFilterIds === 0
      ) {
        ret = false;
        error.selectedBaseRates = true;
      }
    } else {
      if (
        parameters.creditBaseRateFilterIds == "-1" ||
        parameters.creditBaseRateFilterIds === 0
      ) {
        ret = false;
        error.selectedCreditBaseRates = true;
      }
      if (
        parameters.debitBaseRateFilterIds == "-1" ||
        parameters.debitBaseRateFilterIds === 0
      ) {
        ret = false;
        error.selectedDebitBaseRates = true;
      }
    }
    if (
      this.state.selectedAgreements.value &&
      this.state.selectedAgreements.value.split("-")[2] === "PB" &&
      parameters.gcRate === "0"
    ) {
      ret = false;
      error.gcRate = true;
    }
    if (isFinancingTermTypeAsset(getDataValueForSingleSelect(this.state.selectedFinancingAgreementTermTypes)) &&
      parameters.assetClassGroupFilterId === "-1") {
      ret = false;
      error.selectedAssetClassGroups = true;
    }
    if (isFinancingTermTypeAccount(getDataValueForSingleSelect(this.state.selectedFinancingAgreementTermTypes)) &&
      parameters.custodianAccountFilterIds === "-1") {
      ret = false;
      error.selectedCustodianAccounts = true;
    }

    this.setState({ error });
    return ret;
  }

  handleSave() {
    if (this.state.selectedRowData.length === 0) {
      this.setState({ saveSelectMessage: true });
      return;
    }
    let payload = this.state.selectedRowData;
    if (this.props.widgetView) {
      this.props.saveAndValidateWidgetView(
        payload,
        onboardingScreen.FINANCING,
        true
      );
      this.props.resetSimulateData();
      this.props.resetSelectedRowData();
    } else {
      this.props.saveSimulateData(payload, onboardingScreen.FINANCING);
      this.handleClosePopUp();
    }
  }

  renderSimulateButton(isSaveDisabled) {
    if (this.props.isDeleteClicked) {
      return <FilterButton onClick={this.handleDelete} label="Delete" />;
    }
    if (isSaveDisabled) {
      return (
        <div style={{ width: "500px", margin: "auto" }}>
          <Message
            warning
            messageData={
              <div>
                You need to simulate the rule before you can save it.
                <FilterButton
                  onClick={() => this.handleSimulate(false)}
                  label={"Run Simulation"}
                  className="margin--small button--primary"
                />
              </div>
            }
          />
        </div>
      );
    } else {
      return (
        <div>
          SIMULATION RESULTS
          <FilterButton
            onClick={() => this.handleSimulate(false)}
            label={"Re-run Simulation"}
            className="margin--small margin--left button--primary"
          />
        </div>
      );
    }
  }

  renderPopUp() {
    let popUp = null;
    let message = null;
    let title;
    title = _.isEmpty(this.props.selectedRowProps)
      ? "ADD FINANCING AGREEMENT TERMS"
      : "EDIT FINANCING AGREEMENT TERMS";
    let isSaveDisabled =
      !this.state.isSimulate ||
      typeof this.props.simulatedData === "undefined" ||
      typeof this.props.simulatedData.existingList === "undefined" ||
      typeof this.props.simulatedData.simulatedList === "undefined";

    let popupWidth = isSaveDisabled ? "640px" : "1100px";
    let filterHeight = isSaveDisabled ? "600px" : "400px";
    popUp = (
      <React.Fragment>
        <Layout isColumnType>
          <Layout.Child childId="financingTermsPopupContent1">
            <div style={{ width: "600px", margin: "auto" }}>
              <Panel>
                <FinancingTermsPopUpFilters
                  onSelect={this.onSelect}
                  state={this.state}
                  isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
                  isDeleteClicked={this.props.isDeleteClicked}
                  widgetView={this.props.widgetView}
                />
              </Panel>
            </div>
          </Layout.Child>
        </Layout>
        <br />
        {this.renderSimulateButton(isSaveDisabled)}
        <PopUpSimulatedGrids
          isSimulate={this.state.isSimulate}
          simulatedData={this.props.simulatedData}
          view={onboardingScreen.FINANCING}
          saveSelectMessage={this.state.saveSelectMessage}
          onSelect={this.onSelect}
          handleSave={this.handleSave}
        />
        <FilterButton
          onClick={this.handleSave}
          disabled={isSaveDisabled || this.state.selectedRowData.length === 0}
          label="Save"
        />
      </React.Fragment>
    );

    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      deleteFinancingTerm,
      resetDeleteTermData,
      resetSimulateData,
      resetSelectedRowData,
      hidePrimaryPopUp,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen.financing,
    isDeleteClicked: state.financingOnboarding.isDeleteClicked,
    simulatedData: state.financingOnboarding.simulatedData.ruleList,
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    deleteFinancingTermData: state.financingOnboarding.deleteFinancingTermData,
    currencies: state.filters.currencies,
    selectedRowProps: state.financingOnboarding.selectedRowData,
    view: state.financingOnboarding.view,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FinancingTermsPopUpContent);
