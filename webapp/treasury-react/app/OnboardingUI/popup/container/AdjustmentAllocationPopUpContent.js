import React, { Component } from "react";
import _ from "lodash";
import Message from "commons/components/Message";
import MessageDialog from "commons/components/MessageDialog";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FilterButton from "commons/components/FilterButton";
import { getValueForSingleSelectOrNull } from "../../util";
import { Card } from "arc-react-components";
import {
  destroyDeleteToggle,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetAdjustmentAllocationRules,
  resetSimulateData,
  resetSelectedRowData,
  hidePrimaryPopUp,
  deleteAdjustmentAllocationRule,
  resetDeleteAdjustmentAllocationRule,
  setDeleteToggle,
} from "../../actions";
import AdjustmentAllocationPopUpFilters from "../components/AdjustmentAllocationPopUpFilters";
import PopUpSimulatedGrids from "../components/PopUpSimulatedGrids";
import { onboardingScreen } from "../../util";

class AdjustmentAllocationPopUpContent extends Component {
  constructor(props) {
    super(props);
    this.state = this.getDefaultState();
  }

  componentDidMount() {
    if (
      this.props.view === onboardingScreen.ADJUSTMENT &&
      !_.isEmpty(this.props.selectedRowProps)
    ) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.view === onboardingScreen.ADJUSTMENT &&
      _.isEmpty(prevProps.selectedRowProps) &&
      !_.isEmpty(this.props.selectedRowProps)
    ) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  onSelect = (params) => {
    const { key, value } = params;
    this.setState({ [key]: value });
  };

  onTextSelect = (params) => {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = { value: value };
    this.setState(newState);
  };

  getDefaultState = () => {
    return {
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedAgreementTypes: [],
      selectedCurrencies: [],
      selectedChargeTypes: [],
      selectedAllocationStrategy: "",
      isSimulate: false,
      selectedRowData: [],
      saveSelectMessage: false,
      error: {
        selectedLegalEntities: false,
        selectedCpes: false,
        selectedAgreementTypes: false,
        selectedCurrencies: false,
        selectedChargeTypes: false,
        selectedAllocationStrategy: false,
      },
    };
  };

  populateValuesForEdit = () => {
    const rowData = this.props.selectedRowProps;
    let populateValues = {};
    if (
      typeof rowData.legal_entity_id !== "undefined" &&
      rowData.legal_entity_id !== ""
    ) {
      populateValues.selectedLegalEntities = {
        key: rowData.legal_entity_id,
        value: `${rowData.legalEntity} [${rowData.legal_entity_id}]`,
      };
    }
    if (typeof rowData.cpe_id !== "undefined" && rowData.cpe_id !== "") {
      populateValues.selectedCpes = {
        key: rowData.cpe_id,
        value: `${rowData.cpe} [${rowData.cpe_id}]`,
      };
    }
    if (
      typeof rowData.agreement_type_id !== "undefined" &&
      rowData.agreement_type_id !== ""
    ) {
      populateValues.selectedAgreementTypes = {
        key: rowData.agreement_type_id,
        value: `${rowData.agreementType} [${rowData.agreement_type_id}]`,
      };
    }
    if (
      typeof rowData.currency_id !== "undefined" &&
      rowData.currency_id !== ""
    ) {
      populateValues.selectedCurrencies = {
        key: rowData.currency_id,
        value: `${rowData.ccy} [${rowData.currency_id}]`,
      };
    }
    if (
      typeof rowData.charge_type_id !== "undefined" &&
      rowData.charge_type_id !== ""
    ) {
      populateValues.selectedChargeTypes = {
        key: rowData.charge_type_id,
        value: `${rowData.chargeType} [${rowData.charge_type_id}]`,
      };
    }
    if (
      typeof rowData.allocationStrategy !== "undefined" &&
      rowData.allocationStrategy !== ""
    ) {
      populateValues.selectedAllocationStrategy = {
        key: rowData.allocationStrategy,
        value: `${rowData.allocationStrategy}`,
      };
    }
    return populateValues;
  };

  handleClosePopUp = () => {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.setState(this.getDefaultState());
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  };

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
    this.props.resetDeleteAdjustmentAllocationRule();
  };

  getPayload = () => {
    let payload = {
      "ruleSystemFilter.legalEntityIds": getValueForSingleSelectOrNull(
        this.state.selectedLegalEntities
      ),
      "ruleSystemFilter.cpeIds": getValueForSingleSelectOrNull(
        this.state.selectedCpes
      ),
      "ruleSystemFilter.agreementTypeIds": getValueForSingleSelectOrNull(
        this.state.selectedAgreementTypes
      ),
      "ruleSystemFilter.currencyIds": getValueForSingleSelectOrNull(
        this.state.selectedCurrencies
      ),
      "ruleSystemFilter.chargeTypeIds": getValueForSingleSelectOrNull(
        this.state.selectedChargeTypes
      ),
      "ruleSystemFilter.allocationStrategy":
        this.state.selectedAllocationStrategy !== undefined
          ? this.state.selectedAllocationStrategy.value
          : "",
    };
    return payload;
  };

  handleSimulate = (withoutValidation) => {
    let payload = this.getPayload();
    if (
      (withoutValidation !== undefined &&
        typeof withoutValidation === "boolean" &&
        withoutValidation) ||
      this.validateSimulateParameters(payload)
    ) {
      this.props.fetchSimulateData(payload, onboardingScreen.ADJUSTMENT);
      this.setState({ isSimulate: true });
    }
  };

  handleDelete = () => {
    let payload = this.getPayload();
    this.props.deleteAdjustmentAllocationRule(payload);
    this.handleClosePopUp();
  };

  validateSimulateParameters = (parameters) => {
    let ret = true;
    const error = this.getDefaultState().error;
    if (parameters["ruleSystemFilter.legalEntityIds"] === undefined) {
      ret = false;
      error.selectedLegalEntities = true;
    }
    if (parameters["ruleSystemFilter.cpeIds"] === undefined) {
      ret = false;
      error.selectedCpes = true;
    }
    if (parameters["ruleSystemFilter.agreementTypeIds"] === undefined) {
      ret = false;
      error.selectedAgreementTypes = true;
    }
    if (parameters["ruleSystemFilter.currencyIds"] === undefined) {
      ret = false;
      error.selectedCurrencies = true;
    }
    if (parameters["ruleSystemFilter.chargeTypeIds"] === undefined) {
      ret = false;
      error.selectedChargeTypes = true;
    }
    if (parameters["ruleSystemFilter.allocationStrategy"] === undefined) {
      ret = false;
      error.selectedAllocationStrategy = true;
    }
    this.setState({ error });
    return ret;
  };

  handleSave = () => {
    if (this.state.selectedRowData.length === 0) {
      this.setState({ saveSelectMessage: true });
      return;
    }
    let payload = this.state.selectedRowData.map((row) => row.columnValues);
    this.props.saveSimulateData(payload, onboardingScreen.ADJUSTMENT);
    this.handleClosePopUp();
  };

  renderSimulateButton = (isSaveDisabled) => {
    if (this.props.isDeleteClicked) {
      return <FilterButton onClick={this.handleDelete} label="Delete" />;
    }
    if (isSaveDisabled) {
      return (
        <div style={{ width: "500px", margin: "auto" }}>
          <Message
            warning
            messageData={
              <div>
                You need to simulate the rule before you can save it.
                <FilterButton
                  onClick={this.handleSimulate}
                  label={"Run Simulation"}
                  className="margin--small button--primary"
                />
              </div>
            }
          />
        </div>
      );
    } else {
      return (
        <div>
          SIMULATION RESULTS
          <FilterButton
            onClick={this.handleSimulate}
            label={"Re-run Simulation"}
            className="margin--small margin--left button--primary"
          />
        </div>
      );
    }
  };

  renderPopUp = () => {
    let popUp = null;
    let message = null;
    let title;
    title = _.isEmpty(this.props.selectedRowProps)
      ? "ADD ADJUSTMENT ALLOCATION RULES"
      : "EDIT ADJUSTMENT ALLOCATION RULES";

    let isSaveDisabled =
      !this.state.isSimulate ||
      typeof this.props.simulatedData === "undefined" ||
      typeof this.props.simulatedData.existingList === "undefined" ||
      typeof this.props.simulatedData.simulatedList === "undefined";

    popUp = (
      <React.Fragment>
        <Card title={title}>
          <hr />
          <div style={{ width: "600px" }} class="text-align--right">
            <AdjustmentAllocationPopUpFilters
              onSelect={this.onSelect}
              onTextSelect={this.onTextSelect}
              state={this.state}
              isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
              isDeleteClicked={this.props.isDeleteClicked}
            />
          </div>
        </Card>
        <br />
        {this.renderSimulateButton(isSaveDisabled)}
        <PopUpSimulatedGrids
          isSimulate={this.state.isSimulate}
          simulatedData={this.props.simulatedData}
          view={onboardingScreen.ADJUSTMENT}
          saveSelectMessage={this.state.saveSelectMessage}
          onSelect={this.onSelect}
          handleSave={this.handleSave}
        />
        <FilterButton
          onClick={this.handleSave}
          disabled={isSaveDisabled || this.state.selectedRowData.length === 0}
          label="Save Adjustment Allocation Rule"
        />
      </React.Fragment>
    );
    return popUp;
  };

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetAdjustmentAllocationRules,
      resetSimulateData,
      resetSelectedRowData,
      hidePrimaryPopUp,
      deleteAdjustmentAllocationRule,
      resetDeleteAdjustmentAllocationRule,
      setDeleteToggle,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen.adjustment,
    isDeleteClicked: state.financingOnboarding.isDeleteClicked,
    simulatedData: state.financingOnboarding.simulatedData,
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    deleteAdjustmentAllocationRuleData:
      state.financingOnboarding.deleteAdjustmentAllocationRuleData,
    selectedRowProps: state.financingOnboarding.selectedRowData,
    view: state.financingOnboarding.view,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdjustmentAllocationPopUpContent);
