import React, { Component } from 'react';
import Dialog from 'commons/components/Dialog';
import InputFilter from 'commons/components/InputFilter';
import Message from 'commons/components/Message';
import { Layout, Message as ArcMessage } from 'arc-react-components';
import AgreementFilter from 'commons/container/AgreementFilter';
import AgreementTypeSpecificFinancingStyleFilter from 'commons/container/AgreementTypeSpecificFinancingStyleFilter';
import FinancingTypeFilter from 'commons/container/FinancingTypeFilter';
import FilterButton from 'commons/components/FilterButton';
import { getValueForSingleSelect } from '../../util';
import { Card } from 'arc-react-components';

export default class AddNettingGroupDialog extends Component {
  constructor(props) {
    super(props);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.onSelect = this.onSelect.bind(this);
    // this.getMessageComponent = this.getMessageComponent.bind(this);
    this.state = this.getDefaultState();
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultState() {
    return {
      name: '',
      selectedAgreements: [],
      selectedFinancingTypes: [],
      selectedFinancingStyles: [],
      comment: '',
      error: {
        name: false,
        selectedAgreements: false,
        selectedFinancingTypes: false
      }
    };
  }

  handleClosePopUp() {
    this.props.resetAddNettingGroupResult();
    this.setState(this.getDefaultState());
    this.props.handleClosePopup();
  }

  handleSave() {
    this.props.resetAddNettingGroupResult();
    const payload = {
      agreementId: getValueForSingleSelect(this.state.selectedAgreements),
      financingTypeId: getValueForSingleSelect(this.state.selectedFinancingTypes),
      financingStyleId: getValueForSingleSelect(this.state.selectedFinancingStyles),
      nettingGroupDescription: this.state.name,
      nettingGroupComment: this.state.comment
    };
    if (this.validateSaveNettingGroupParameters(payload)) {
      this.setState({msg:this.state.name});
      this.props.saveNettingGroup(payload);
      this.setState(this.getDefaultState());
    }
  }

  validateSaveNettingGroupParameters(parameters) {
    let ret = true;
    const error = {
      name: false,
      selectedAgreements: false,
      selectedFinancingTypes: false
    };
    if (parameters.financingTypeId === '-1') {
      ret = false;
      error.selectedFinancingTypes = true;
    }
    if (parameters.nettingGroupDescription.length === 0) {
      ret = false;
      error.name = true;
    }
    if (parameters.agreementId === '-1') {
      ret = false;
      error.selectedAgreements = true;
    }
    this.setState({ error });
    return ret;
  }

  render() {
    const agreementId = getValueForSingleSelect(this.state.selectedAgreements);
    const financingTypeId = getValueForSingleSelect(this.state.selectedFinancingTypes);
    const financingStyleId = getValueForSingleSelect(this.state.selectedFinancingStyles);
    let message = <div />;
    let messageData="";
    if (this.props.addNettingGroupResult && this.props.addNettingGroupResult.errorMessage) {
      if(this.props.addNettingGroupResult.errorMessage.includes("duplicate key value")) {
        messageData="Netting Group for "+ this.state.msg+" already exists";
        message=<Message messageData={messageData} error/>
      } else {
      message = <Message messageData="Error occured while saving rules" error />;
      }
    } else if (this.props.addNettingGroupResult && this.props.addNettingGroupResult.success) {
      message = <Message messageData="Netting Group has been saved" success />;
    }
    return (
      <React.Fragment>
        <Dialog
          title="Add Netting Group"
          isOpen={this.props.isDialogOpen}
          style={{ width: '620px' }}
          onClose={this.handleClosePopUp}
          footer={
            <React.Fragment>
              <FilterButton onClick={this.handleSave} reset={false} label="Save" />
            </React.Fragment>
          }
        >
          <div style={{ width: '600px' }} >
            {message}
            <Card>
              <InputFilter
                data={this.state.name}
                label="Name"
                onSelect={this.onSelect}
                stateKey="name"
                inputSize="41"
                isError={this.state.error.name}
              />
              <AgreementFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedAgreements}
                multiSelect={false}
                errorDiv={this.state.error.selectedAgreements}
                horizontalLayout
              />
              <FinancingTypeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedFinancingTypes}
                multiSelect={false}
                errorDiv={this.state.error.selectedFinancingTypes}
                horizontalLayout
              />
              <AgreementTypeSpecificFinancingStyleFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedFinancingStyles}
                agreementId={agreementId}
                financingTypeId={financingTypeId}
                financingStyleId={financingStyleId}
                multiSelect={false}
                horizontalLayout
              />
              <InputFilter
                data={this.state.comment}
                label="Comment"
                inputSize="93"
                onSelect={this.onSelect}
                stateKey="comment"
              />
              { this.state.error.selectedAgreements || this.state.error.selectedFinancingTypes
              ? <ArcMessage title="Please select Agreement and Financing Type" type="critical" />
              : null }
            </Card>
          </div>
        </Dialog>
      </React.Fragment>
    );
  }
}
