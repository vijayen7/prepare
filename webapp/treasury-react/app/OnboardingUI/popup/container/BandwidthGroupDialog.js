import React, { Component } from 'react';
import Dialog from 'commons/components/Dialog';
import InputFilter from 'commons/components/InputFilter';
import InputNumberFilter from 'commons/components/InputNumberFilter';
import Message from 'commons/components/Message';
import FilterButton from 'commons/components/FilterButton';
import { Card } from 'arc-react-components';

export default class BandwidthGroupDialog extends Component {
  constructor(props) {
    super(props);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.getMessageComponent = this.getMessageComponent.bind(this);
    this.validateParameters = this.validateParameters.bind(this);
    this.state = this.getDefaultState();
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultState() {
    return {
      name: '',
      startingThreshold: [],
      endingThreshold: [],
      abbreviation: '',
      error: {
        name: false,
        startingThreshold: false,
        endingThreshold: false,
        abbreviation: false
      }
    };
  }

  getMessageComponent() {
    const data = this.props.bandwidthGroupData;
    if (data && data.bandwidthGroupId) {
      return <Message messageData="Bandwidth Group was added." success />;
    } else if (data && data.errorMessage) {
      return <Message messageData="Error in saving bandwidth group." error />;
    }
    return null;
  }

  handleClosePopUp() {
    this.props.destroyAddBandwidthToggle();
    this.props.resetBandwidthGroupData();
    this.setState(this.getDefaultState());
  }

  handleSubmit() {
    this.props.resetBandwidthGroupData();
    const parameters = {};
    const { name, startingThreshold, endingThreshold, abbreviation } = this.state;

    parameters.bandwidthGroupName = name;
    parameters.startingThreshold = startingThreshold;
    parameters.endingThreshold = endingThreshold;
    parameters.bandwidthGroupAbbrev = abbreviation;
    if (this.validateParameters(parameters)) {
      this.props.saveBandWidthGroup(parameters);
      this.setState(this.getDefaultState());
    }
  }

  validateParameters(parameters) {
    let ret = true;
    const error = {
      name: false,
      startingThreshold: false,
      endingThreshold: false,
      abbreviation: false
    };
    if (parameters.bandwidthGroupName.length === 0) {
      ret = false;
      error.name = true;
    }
    if (parameters.startingThreshold.length === 0) {
      ret = false;
      error.startingThreshold = true;
    }
    if (parameters.endingThreshold.length === 0) {
      ret = false;
      error.endingThreshold = true;
    }
    if (parameters.bandwidthGroupAbbrev.length === 0) {
      ret = false;
      error.abbreviation = true;
    }
    this.setState({ error });
    return ret;
  }

  render() {
    return (
      <React.Fragment>
        <Dialog
          title="Add Bandwidth Group"
          isOpen={this.props.isAddBandwidthClicked}
          style={{ width: '550px' }}
          onClose={this.handleClosePopUp}
          footer={
            <React.Fragment>
              <FilterButton onClick={this.handleSubmit} label="Submit" />
            </React.Fragment>
          }
        >
          <div style={{ width: '500px' }}>
            {this.getMessageComponent()}
            <div>
              <Card>
                <InputFilter
                  data={this.state.name}
                  label="Name"
                  onSelect={this.onSelect}
                  stateKey="name"
                  isError={this.state.error.name}
                />
                <InputNumberFilter
                  data={this.state.startingThreshold}
                  label="Starting Threshold"
                  onSelect={this.onSelect}
                  stateKey="startingThreshold"
                  isError={this.state.error.startingThreshold}
                />
                <InputNumberFilter
                  data={this.state.endingThreshold}
                  label="Ending Threshold"
                  onSelect={this.onSelect}
                  stateKey="endingThreshold"
                  isError={this.state.error.endingThreshold}
                />
                <InputFilter
                  data={this.state.abbreviation}
                  label="Abbreviation"
                  onSelect={this.onSelect}
                  stateKey="abbreviation"
                  isError={this.state.error.abbreviation}
                />
              </Card>
            </div>
          </div>
        </Dialog>
      </React.Fragment>
    );
  }
}
