import React, { Component } from 'react';
import _ from 'lodash';
import Dialog from 'commons/components/Dialog';
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import FilterButton from 'commons/components/FilterButton';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getValueForSingleSelect } from '../../util';
import { Card } from 'arc-react-components';
import {
  destroyDeleteToggle,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetSimulateData,
  resetSelectedRowData,
  hidePrimaryPopUp
} from '../../actions';
import NettingGroupPopUpFilters from '../components/NettingGroupPopUpFilters';
import PopUpSimulatedGrids from '../components/PopUpSimulatedGrids';
import { onboardingScreen } from '../../util';

class NettingGroupPopUp extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.populateValuesForEdit = this.populateValuesForEdit.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleSimulate = this.handleSimulate.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCloseMessagePopup = this.handleCloseMessagePopup.bind(this);
    this.validateSimulateParameters = this.validateSimulateParameters.bind(this);
    this.renderSimulateButton = this.renderSimulateButton.bind(this);
    this.resetDependentFilters = this.resetDependentFilters.bind(this);
    this.state = this.getDefaultState();
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.view === onboardingScreen.NETTING &&
      _.isEmpty(prevProps.selectedRowProps) &&
      !_.isEmpty(this.props.selectedRowProps)
    ) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  onSelect(params) {
    const { key, value } = params;
    this.resetDependentFilters(key, value);
    this.setState({ [key]: value });
  }

  resetDependentFilters(key) {
    if (key === 'selectedCustodianAccounts') {
      this.setState({ selectedNettingGroups: [] });
    }
  }

  getDefaultState() {
    return {
      selectedNettingGroups: [],
      selectedCustodianAccounts: [],
      isSimulate: false,
      selectedRowData: [],
      saveSelectMessage: false,
      error: {
        selectedNettingGroups: false,
        selectedCustodianAccounts: false
      }
    };
  }

  populateValuesForEdit() {
    const rowData = this.props.selectedRowProps;
    let populateValues = {};

    populateValues = {
      selectedCustodianAccounts: {
        key: rowData.custodianAccountId,
        value: `${rowData.custodianAccountName} [${rowData.custodianAccountId}]`
      },
      selectedNettingGroups: {
        key: rowData.nettingGroupId,
        value: `${rowData.nettingGroupName} [${rowData.nettingGroupId}]`
      }
    };
    return populateValues;
  }

  handleClosePopUp() {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.setState(this.getDefaultState());
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  }

  handleCloseMessagePopup() {
    this.props.resetSaveData();
  }

  handleSimulate() {
    let payload;

    payload = {
      custodianAccountFilterIds: getValueForSingleSelect(this.state.selectedCustodianAccounts),
      nettingGroupFilterIds: getValueForSingleSelect(this.state.selectedNettingGroups)
    };
    if (this.validateSimulateParameters(payload)) {
      this.props.fetchSimulateData(payload, onboardingScreen.NETTING);
      this.setState({ isSimulate: true });
    }
  }

  validateSimulateParameters(parameters) {
    let ret = true;
    const error = this.getDefaultState().error;

    if (parameters.custodianAccountFilterIds == '-1') {
      ret = false;
      error.selectedCustodianAccounts = true;
    }
    if (parameters.nettingGroupFilterIds == '-1') {
      ret = false;
      error.selectedNettingGroups = true;
    }

    this.setState({ error });
    return ret;
  }

  handleSave() {
    if (this.state.selectedRowData.length === 0) {
      this.setState({ saveSelectMessage: true });
      return;
    }
    this.props.saveSimulateData(this.state.selectedRowData, onboardingScreen.NETTING);
    this.handleClosePopUp();
  }

  renderSimulateButton(isSaveDisabled) {
    if (isSaveDisabled) {
      return (
        <div style={{ width: '500px', margin: 'auto' }}>
          <Message
            warning
            messageData={
              <div>
                You need to simulate the rule before you can save it.
                <FilterButton
                  onClick={this.handleSimulate}
                  label={'Run Simulation'}
                  className="margin--small button--primary"
                />
              </div>
            }
          />
        </div>
      );
    } else {
      return (
        <div>
          SIMULATION RESULTS
          <FilterButton
            onClick={this.handleSimulate}
            label={'Re-run Simulation'}
            className="margin--small margin--left button--primary"
          />
        </div>
      );
    }
  }

  renderPopUp() {
    let popUp = null;
    let message = null;
    let title;
    title = _.isEmpty(this.props.selectedRowProps)
      ? 'ADD NETTING GROUP RULES'
      : 'EDIT NETTING GROUP RULES';

    if (!_.isEmpty(this.props.saveSimulatedData)) {
      !this.props.saveSimulatedData.errorMessage
        ? (message = <Message messageData="Rules have been saved" success />)
        : (message = <Message messageData="Error occurred while saving rules" error />);
    }
    let isSaveDisabled =
      !this.state.isSimulate ||
      typeof this.props.simulatedData === 'undefined' ||
      typeof this.props.simulatedData.existingList === 'undefined' ||
      typeof this.props.simulatedData.simulatedList === 'undefined';

    let popupWidth = isSaveDisabled ? '800px' : '1000px';
    popUp = (
      <React.Fragment>
        <Dialog
          isOpen={
            this.props.view === onboardingScreen.NETTING &&
            (this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))
          }
          title={title}
          onClose={this.handleClosePopUp}
          footer={
            <React.Fragment>
              <FilterButton
                onClick={this.handleSave}
                disabled={isSaveDisabled || this.state.selectedRowData.length === 0}
                label="Save Netting Group Rule"
              />
            </React.Fragment>
          }
          style={{
            width: popupWidth
          }}
        >
          <Card title="Rule Configuration">
            <hr />
            <div style={{ width: '600px' }} class="text-align--right">
              <NettingGroupPopUpFilters
                onSelect={this.onSelect}
                state={this.state}
                isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
                handleSimulate={this.handleSimulate}
              />
            </div>
          </Card>
          <br />
          {this.renderSimulateButton(isSaveDisabled)}
          <PopUpSimulatedGrids
            isSimulate={this.state.isSimulate}
            simulatedData={this.props.simulatedData}
            view={onboardingScreen.NETTING}
            saveSelectMessage={this.state.saveSelectMessage}
            onSelect={this.onSelect}
            handleSave={this.handleSave}
          />
        </Dialog>
        <MessageDialog
          isOpen={this.props.view === onboardingScreen.NETTING
            && !_.isEmpty(this.props.saveSimulatedData)}
          onClose={this.handleCloseMessagePopup}
          content={message}
        />
      </React.Fragment>
    );

    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetSimulateData,
      resetSelectedRowData,
      hidePrimaryPopUp
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen.netting,
    isDeleteClicked: state.financingOnboarding.isDeleteClicked,
    simulatedData: state.financingOnboarding.simulatedData.ruleList,
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    selectedRowProps: state.financingOnboarding.selectedRowData,
    view: state.financingOnboarding.view
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NettingGroupPopUp);
