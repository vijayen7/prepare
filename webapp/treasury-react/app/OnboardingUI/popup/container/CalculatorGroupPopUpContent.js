import React, { Component } from 'react';
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getValueForSingleSelectOrUndefined } from '../../util';
import FilterButton from 'commons/components/FilterButton';
import { Card, Layout } from 'arc-react-components';
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  deleteCalculatorGroup,
  resetDeleteCalculatorData,
  resetSimulateData,
  resetSelectedRowData
} from '../../actions';
import { getFirstDateOfCurrentMonth } from 'commons/util';
import CalculatorGroupPopUpFilters from '../components/CalculatorGroupPopUpFilters';
import PopUpSimulatedGrids from '../components/PopUpSimulatedGrids';
import { onboardingScreen } from '../../util';

class CalculatorGroupPopUpContent extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.populateValuesForEdit = this.populateValuesForEdit.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleSimulate = this.handleSimulate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCloseMessagePopup = this.handleCloseMessagePopup.bind(this);
    this.validateSimulateParameters = this.validateSimulateParameters.bind(this);
    this.renderSimulateButton = this.renderSimulateButton.bind(this);
    this.resetDependentFilters = this.resetDependentFilters.bind(this);
    this.state = this.getDefaultState();
  }

  componentDidMount() {
    if (this.props.widgetView) {
      this.setState(
        {
          selectedLegalEntities: this.props.selectedData.selectedLegalEntities,
          selectedCpes: this.props.selectedData.selectedCpes,
          selectedAgreementTypes: this.props.selectedData
            .selectedAgreementTypes,
          selectedNettingGroups: this.props.selectedData.selectedNettingGroups,
          selectedCurrencies: this.props.selectedData.selectedCurrencies
        },
        () => {
          this.handleSimulate(true);
        }
      );
    }
    else {
      if (
        this.props.view === onboardingScreen.CALCULATOR &&
        !_.isEmpty(this.props.selectedRowProps)
      ) {
        var populateValuesForEdit = this.populateValuesForEdit();
        this.setState(populateValuesForEdit);
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.view === onboardingScreen.CALCULATOR &&
      _.isEmpty(prevProps.selectedRowProps) &&
      !_.isEmpty(this.props.selectedRowProps)
    ) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  onSelect(params) {
    const { key, value } = params;
    this.resetDependentFilters(key, value);
    this.setState({ [key]: value });
  }

  resetDependentFilters(key) {
    if (key === "selectedAgreementTypes") {
      this.setState({
        selectedCalculatorGroups: [],
        selectedNettingGroups: []
      });
    } else if (key === "selectedNettingGroups") {
      this.setState({ selectedCalculatorGroups: [] });
    }
  }

  getDefaultState() {
    return {
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedAgreementTypes: [],
      selectedNettingGroups: [],
      selectedCurrencies: [],
      selectedCalculatorGroups: [],
      selectedDate: getFirstDateOfCurrentMonth(),
      isSimulate: false,
      selectedRowData: [],
      saveSelectMessage: false,
      error: {
        selectedAgreementTypes: false,
        selectedCalculatorGroups: false
      }
    };
  }

  populateValuesForEdit() {
    const rowData = this.props.selectedRowProps;
    let populateValues = {};
    if (
      typeof rowData.shaw_entity_id !== "undefined" &&
      rowData.shaw_entity_id !== ""
    ) {
      populateValues.selectedLegalEntities = {
        key: rowData.shaw_entity_id,
        value: `${rowData.legalEntity} [${rowData.shaw_entity_id}]`
      };
    }
    if (typeof rowData.cpe_id !== "undefined" && rowData.cpe_id !== "") {
      populateValues.selectedCpes = {
        key: rowData.cpe_id,
        value: `${rowData.cpe} [${rowData.cpe_id}]`
      };
    }
    if (
      typeof rowData.funding_type_id !== "undefined" &&
      rowData.funding_type_id !== ""
    ) {
      populateValues.selectedAgreementTypes = {
        key: rowData.funding_type_id,
        value: `${rowData.agreementType} [${rowData.funding_type_id}]`
      };
    }
    if (
      typeof rowData.netting_group_id !== "undefined" &&
      rowData.netting_group_id !== ""
    ) {
      populateValues.selectedNettingGroups = {
        key: rowData.netting_group_id,
        value: `${rowData.nettingGroup} [${rowData.netting_group_id}]`
      };
    }
    if (typeof rowData.ccy_spn !== "undefined" && rowData.ccy_spn !== "") {
      populateValues.selectedCurrencies = {
        key: rowData.ccy_spn,
        value: `${rowData.ccy} [${rowData.ccy_spn}]`
      };
    }
    populateValues.selectedCalculatorGroups = {
      key: rowData.calculator_group,
      value: `${rowData.calculatorGroup} [${rowData.calculator_group}]`
    };
    populateValues.selectedDate = this.state.selectedDate;

    return populateValues;
  }

  handleClosePopUp() {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.setState(this.getDefaultState());
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  }

  handleCloseMessagePopup() {
    this.props.resetSaveData();
    this.props.resetDeleteCalculatorData();
  }

  handleSimulate(withoutValidation) {
    let payload = {
      "ruleSystemFilter.legalEntityIds": getValueForSingleSelectOrUndefined(
        this.state.selectedLegalEntities
      ),
      "ruleSystemFilter.cpeIds": getValueForSingleSelectOrUndefined(
        this.state.selectedCpes
      ),
      "ruleSystemFilter.agreementTypeIds": getValueForSingleSelectOrUndefined(
        this.state.selectedAgreementTypes
      ),
      "ruleSystemFilter.nettingGroupIds": getValueForSingleSelectOrUndefined(
        this.state.selectedNettingGroups
      ),
      "ruleSystemFilter.currencyIds": getValueForSingleSelectOrUndefined(
        this.state.selectedCurrencies
      ),
      "ruleSystemFilter.calculatorGroupId": getValueForSingleSelectOrUndefined(
        this.state.selectedCalculatorGroups
      ),
      "ruleSystemFilter.startDate": this.state.selectedDate
    };
    if (
      (withoutValidation !== undefined && typeof withoutValidation === 'boolean' && withoutValidation) ||
      this.validateSimulateParameters(payload)
    ) {
      this.props.fetchSimulateData(payload, onboardingScreen.CALCULATOR);
      this.setState({ isSimulate: true });
    }
  }

  handleDelete(){
    let columnValues = this.props.selectedRowProps.columnValues;
    let dateArray = columnValues[0].split("-");
    dateArray[1] = this.state.selectedDate.replace(/-/g, "");
    columnValues[0] = dateArray.join("-");

    this.props.deleteCalculatorGroup([columnValues]);
    this.handleClosePopUp();
  }
  validateSimulateParameters(parameters) {
    let ret = true;
    const error = this.getDefaultState().error;
    if (
      parameters["ruleSystemFilter.agreementTypeIds"] === undefined &&
      parameters["ruleSystemFilter.nettingGroupIds"] === undefined
    ) {
      ret = false;
      error.selectedAgreementTypes = true;
    }
    if (
      parameters["ruleSystemFilter.agreementTypeIds"] == "20" &&
      parameters["ruleSystemFilter.nettingGroupIds"] === undefined
    ) {
      ret = false;
      error.selectedNettingGroups = true;
    }
    if (parameters["ruleSystemFilter.calculatorGroupId"] === undefined) {
      ret = false;
      error.selectedCalculatorGroups = true;
    }
    if (parameters["ruleSystemFilter.startDate"] === "") {
      ret = false;
      error.selectedDate = true;
    }

    this.setState({ error });
    return ret;
  }

  handleSave() {
    if (this.state.selectedRowData.length === 0) {
      this.setState({ saveSelectMessage: true });
      return;
    }

    if (this.props.widgetView) {
      this.props.saveAndValidateWidgetView(
        this.state.selectedRowData,
        onboardingScreen.CALCULATOR,
        true
      );
      this.props.resetSimulateData();
      this.props.resetSelectedRowData();
    } else {
      let payload = this.state.selectedRowData.map(row => row.columnValues);
      this.props.saveSimulateData(payload, onboardingScreen.CALCULATOR);
      this.handleClosePopUp();
    }
  }

  renderSimulateButton(isSaveDisabled) {
    if(this.props.isDeleteClicked){
      return <FilterButton onClick = {this.handleDelete} label = "Delete"/>
    }
    if (isSaveDisabled) {
      return (
        <div style={{ width: '500px', margin: 'auto' }}>
          <Message
            warning
            messageData={
              <div>
                You need to simulate the rule before you can save it.
                <FilterButton
                  onClick={this.handleSimulate}
                  label={'Run Simulation'}
                  className="margin--small button--primary"
                />
              </div>
            }
          />
        </div>
      );
    } else {
      return (
        <div>
          SIMULATION RESULTS
          <FilterButton
            onClick={this.handleSimulate}
            label={'Re-run Simulation'}
            className="margin--small margin--left button--primary"
          />
        </div>
      );
    }
  }

  renderPopUp() {
    console.log(this.props.saveSimulatedData);
    let popUp = null;
    let title;
    title = _.isEmpty(this.props.selectedRowProps)
      ? 'ADD CALCULATOR GROUP RULES'
      : 'EDIT CALCULATOR GROUP RULES';

    let isSaveDisabled =
      !this.state.isSimulate ||
      typeof this.props.simulatedData === 'undefined' ||
      typeof this.props.simulatedData.existingList === 'undefined' ||
      typeof this.props.simulatedData.simulatedList === 'undefined';

    let popupWidth = isSaveDisabled ? '800px' : '1000px';
    popUp = (
      <React.Fragment>
        <Card title="Rule Configuration">
          <hr />
          <div style={{ width: '600px' }} class="text-align--right">
            <CalculatorGroupPopUpFilters
              onSelect={this.onSelect}
              state={this.state}
              isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
              isDeleteClicked = {this.props.isDeleteClicked}
            />
          </div>
        </Card>
        <br />
        {this.renderSimulateButton(isSaveDisabled)}
        <PopUpSimulatedGrids
          isSimulate={this.state.isSimulate}
          simulatedData={this.props.simulatedData}
          view={onboardingScreen.CALCULATOR}
          saveSelectMessage={this.state.saveSelectMessage}
          onSelect={this.onSelect}
          handleSave={this.handleSave}
        />
        <FilterButton
          onClick={this.handleSave}
          disabled={isSaveDisabled || this.state.selectedRowData.length === 0}
          label="Save Calculator Group Rule"
        />
      </React.Fragment>
    );
    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      deleteCalculatorGroup,
      resetDeleteCalculatorData,
      resetSimulateData,
      resetSelectedRowData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isAddClicked: state.financingOnboarding.isAddClicked,
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen.calculator,
    isDeleteClicked: state.financingOnboarding.isDeleteClicked,
    simulatedData: state.financingOnboarding.simulatedData,
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    selectedRowProps: state.financingOnboarding.selectedRowData,
    view: state.financingOnboarding.view
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CalculatorGroupPopUpContent);
