import React from "react";
import Dialog from "commons/components/Dialog";
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { onboardingScreen } from "../../util";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReconciliationThresholdsPopupContent from "../components/ReconciliationThresholdsPopupContent";
import {
  destroyDeleteToggle,
  hidePrimaryPopUp,
  saveReconciliationThresholdsData,
  resetSaveReconciliationThresholdsData,
  resetDeleteReconciliationThresholdsData,
  resetSelectedRowData,
  fetchSearchData
} from '../../actions';

class ReconciliationThresholdsPopUp extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
      if (this.props.saveReconciliationThresholdsData === true || this.props.deleteReconciliationThresholdsData === true) {
        let payload = {date: new Date().toJSONString().substr(0,10).replaceAll('/','-')}
        this.props.fetchSearchData(payload,onboardingScreen.RECONCILIATION_THRESHOLDS );
      }
  }
  handleClosePopUp = () => {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.resetSelectedRowData();
  }

  handleCloseMessagePopup = () => {
    this.props.resetSaveReconciliationThresholdsData();
    this.props.resetDeleteReconciliationThresholdsData();
    this.props.destroyDeleteToggle();
  }

  render() {
    let message,title,messageBoxTitle;
    if(this.props.isDeleteClicked) {
      message = (
        <Message messageData={(this.props.deleteReconciliationThresholdsData === true) ? "Reconciliation Thresholds has been deleted":"Reconciliation Thresholds could not be deleted."}
                success={this.props.deleteReconciliationThresholdsData === true}
                error={this.props.deleteReconciliationThresholdsData === false}/>);
      title = "Delete Reconciliation Threshold";
      messageBoxTitle = (this.props.deleteReconciliationThresholdsData === true)?"Success":"Error";
    } else {
      message = (
        <Message messageData={(this.props.saveReconciliationThresholdsData === true) ? "Reconciliation Thresholds has been saved":"Reconciliation Thresholds could not be added."}
                success={this.props.saveReconciliationThresholdsData === true}
                error={this.props.saveReconciliationThresholdsData === false}/>);
      title = "Add Reconciliation Threshold";
      messageBoxTitle = (this.props.saveReconciliationThresholdsData === true)?"Success":"Error";
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={this.props.isPopUpOpen}
          onClose={this.handleClosePopUp}
          style={{width:'600px'}}
          title={title}
        >
          <ReconciliationThresholdsPopupContent/>
        </Dialog>
        <MessageDialog isOpen={this.props.saveReconciliationThresholdsData != null || this.props.deleteReconciliationThresholdsData != null}
          content={message}
          onClose={this.handleCloseMessagePopup}
          title={messageBoxTitle}></MessageDialog>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      hidePrimaryPopUp,
      resetSelectedRowData,
      resetSaveReconciliationThresholdsData,
      resetDeleteReconciliationThresholdsData,
      fetchSearchData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    deleteReconciliationThresholdsData: state.financingOnboarding.deleteReconciliationThresholdsData,
    saveReconciliationThresholdsData: state.financingOnboarding.saveReconciliationThresholdsData,
    isDeleteClicked: state.financingOnboarding.isDeleteClicked
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReconciliationThresholdsPopUp);
