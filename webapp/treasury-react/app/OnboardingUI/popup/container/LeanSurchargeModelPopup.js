import React, { Component } from 'react';
import Dialog from 'commons/components/Dialog';
import FilterButton from 'commons/components/FilterButton';
import InputFilter from 'commons/components/InputFilter';
import InputNumberFilter from 'commons/components/InputNumberFilter';
import LeanSurchargeModelFilter from 'commons/container/LeanSurchargeModelFilter'
import { Card } from 'arc-react-components';
import { getErrorNote as ErrorNote } from '../../util';
import Table from "commons/components/Table";

export default class LeanSurchargeModelPopup extends Component  {
  constructor(props) {
    super(props);
    this.handleAddSlab = this.handleAddSlab.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleReset = this.handleReset.bind(this);

    this.onSelect = this.onSelect.bind(this);
    this.resetGridData = this.resetGridData.bind(this);
    this.renderCreateNewFields = this.renderCreateNewFields.bind(this);
    this.validateParameters = this.validateParameters.bind(this);
    this.validateModelName = this.validateModelName.bind(this);
    this.convertTableData = this.convertTableData.bind(this);

    this.state = this.getDefaultState();
  }

  getDefaultState() {
    return {
      name: '',
      newSlabs: [],
      startingThreshold: 0,
      endingThreshold: 0,
      leanSurchargeValue: 0,
      overallStartingThreshold: 0,
      overallEndingThreshold: 0,
      tempId: 1,
      selectedLeanSurchargeModel: [],
      refreshLeanSurchargeFilter: 0,
      disableAddButton: false,
      error: {
        name: false,
        startingThreshold: false,
        leanSurchargeValue: false,
        duplicateName: false,
        invalidStartingThreshold: false,
        invalidEndingThreshold: false
      },
      gridLabels: ["Starting Threshold (%)", "Ending Threshold (%)", "Lean Surcharge Rate (bps)"],
      gridColumns: ["startingThreshold", "endingThreshold", "value"]
    };
  }

  resetGridData() {
    this.props.updateLeanSurchargeGridData([]);
  }

  handleAddSlab() {
    let slabs = this.state.newSlabs;
    let newId = this.state.tempId +1;
    let newStartingThreshold = this.state.endingThreshold;
    let slab = {
      id: newId,
      startingThreshold: this.state.startingThreshold,
      endingThreshold: this.state.endingThreshold,
      value: this.state.leanSurchargeValue/100
     };
     if(this.validateParameters()){
      slabs.push(slab);
      let newSlabs = [...slabs];
      this.props.updateLeanSurchargeGridData(newSlabs);
      this.setState({tempId: newId,
        startingThreshold: newStartingThreshold,
        endingThreshold: '', leanSurchargeValue: '',
        overallStartingThreshold: Math.min(this.state.overallStartingThreshold, this.state.startingThreshold),
        overallEndingThreshold: Math.max(this.state.overallEndingThreshold, this.state.endingThreshold),
        disableAddButton: newStartingThreshold === ''
      });
     }
  }

  handleClosePopUp() {
    this.resetGridData();
    this.setState(this.getDefaultState());
    this.props.resetSaveLeanSurchargeModel();
    this.props.destroyLeanSurchargePopup();
  }

  handleReset() {
    this.resetGridData();
    this.props.resetSaveLeanSurchargeModel();
    this.setState(this.getDefaultState());
  }

  handleSave() {
    let refresh = this.state.refreshLeanSurchargeFilter;
    refresh++;
    this.setState({ refreshLeanSurchargeFilter: refresh });
    if(this.validateModelName()) {
      let payload = {name : this.state.name,
        leanSurchargeSlabs : _.map(this.state.newSlabs,
           row => _.pick(row, ["startingThreshold", "endingThreshold", "value"])) }
      this.props.saveLeanSurchargeModel(payload);
      this.handleClosePopUp();
    }
  }

  onSelect(params) {
    const { key, value } = params;
    if(key === "selectedLeanSurchargeModel" && value !== null) {
      let payload = {id: value.key};
      this.props.fetchLeanSurchargeModelById(payload);
    } else if (key === "selectedLeanSurchargeModel" && value === null) {
      this.resetGridData();
    }
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  validateModelName() {
    let error = this.state.error;
    let isNameError = true;
    if (this.state.name.length === 0) {
      error.name = true;
      isNameError = false;
    } else if(_.map(this.props.leanSurchargeModels, "value").includes(this.state.name)) {
      error.duplicateName = true;
      isNameError = false;
    }
    this.setState({ error });
    return isNameError;
  }

  validateParameters() {
    let ret = true;
    const error = {
      name: false,
      startingThreshold: false,
      leanSurchargeValue: false,
      duplicateName: false
    };
    if (this.state.name.length === 0) {
      ret = false;
      error.name = true;
    }
    if (this.state.startingThreshold.length === 0) {
      ret = false;
      error.startingThreshold = true;
    }
    if (this.state.leanSurchargeValue.length === 0) {
      ret = false;
      error.leanSurchargeValue = true;
    }
    if (this.state.startingThreshold > this.state.overallStartingThreshold &&
      this.state.startingThreshold < this.state.overallEndingThreshold) {
        ret = false;
        error.invalidStartingThreshold = true;
    }
    if (this.state.endingThreshold > this.state.overallStartingThreshold &&
      this.state.endingThreshold < this.state.overallEndingThreshold) {
        ret = false;
        error.invalidEndingThreshold = true;
    }
    this.setState({ error });
    return ret;
  }

  convertTableData() {
    return _.map(this.props.leanSurchargeModelData, row => _.update(Object.assign({},row), "value", val => val*100 ));
  }

  renderCreateNewFields() {
    return (<React.Fragment>
      <Card>
        <InputFilter
          data={this.state.name}
          label="Lean Surcharge Model Name"
          onSelect={this.onSelect}
          stateKey="name"
        />
        {ErrorNote(this.state.error.name, "Please select a Name")}
        {ErrorNote(this.state.error.duplicateName, "Model Exists. Please select a different Name")}
        <InputNumberFilter
          data={this.state.startingThreshold}
          label="Starting Threshold (%)"
          onSelect={this.onSelect}
          stateKey="startingThreshold"
          isError={this.state.error.startingThreshold}
        />
        {ErrorNote(this.state.error.invalidStartingThreshold, "Invalid Starting Threshold. Values overlapping")}
        <InputNumberFilter
          data={this.state.endingThreshold}
          label="Ending Threshold (%)"
          onSelect={this.onSelect}
          stateKey="endingThreshold"
        />
        {ErrorNote(this.state.error.invalidEndingThreshold, "Invalid Ending Threshold. Values overlapping")}
        <InputNumberFilter
          data={this.state.leanSurchargeValue}
          label="Lean Surcharge Rate (bps)"
          onSelect={this.onSelect}
          stateKey="leanSurchargeValue"
          isError={this.state.error.leanSurchargeValue}
        />
        <FilterButton onClick={this.handleAddSlab} disabled={this.state.disableAddButton} label="Add"/>
      </Card>
    </React.Fragment>);
  }

  render() {
    let title = this.props.isLeanSurchargeModelViewMode ?
      "View Lean Surcharge Model" : "Add Lean Surcharge Model";
    let cardTitle = !_.isEmpty(this.state.selectedLeanSurchargeModel) ?  this.state.selectedLeanSurchargeModel.value : this.props.selectedLeanSurchargeModel
    return (
      <React.Fragment>
        <Dialog
          title={title}
          isOpen={this.props.isLeanSurchargeModelPopupOpen}
          style={{ width: '550px' }}
          onClose={this.handleClosePopUp}
          footer={
            this.props.isLeanSurchargeModelViewMode ?  <></> :
              <React.Fragment>
                <FilterButton onClick={this.handleSave} disabled={this.state.name.length === 0} label="Save" />
                <FilterButton onClick={this.handleReset} label="Reset" />
              </React.Fragment>
            }
        >
          {this.props.isLeanSurchargeModelViewMode ?
          <Card>
            <LeanSurchargeModelFilter
              horizontalLayout={true}
              multiSelect={false}
              selectedData={this.state.selectedLeanSurchargeModel}
              onSelect={this.onSelect}
              payload={this.state.refreshLeanSurchargeFilter}
            />
          </Card>
          : this.renderCreateNewFields()
          }
          <br/>
          <Card>
          <div class="legend">
                <span class="margin--small text-align--left">
                  <b>Note:</b> Given slab is considered exclusive of starting threshold
                </span>
                <span class="margin--small text-align--left">
                  Lean percentage is calculated as a ratio of net debit to smv
                </span>
              <br/>
            </div>
            </Card>
          <Card title={cardTitle}>
            <Table
              labels={this.state.gridLabels}
              dataKeys={this.state.gridColumns}
              content={this.convertTableData()}
            />
          </Card>
          <br/>
        </Dialog>
      </React.Fragment>);
  }
}
