import React, { Component } from "react";
import _ from "lodash";
import Dialog from "commons/components/Dialog";
import { Layout, Card } from "arc-react-components";
import Message from "commons/components/Message";
import MessageDialog from "commons/components/MessageDialog";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getValueForSingleSelectOrUndefined } from "../../util";
import FilterButton from 'commons/components/FilterButton';
import {
  destroyDeleteToggle,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  deleteReportingCurrency,
  resetDeleteReportingData,
  resetSimulateData,
  resetSelectedRowData,
  hidePrimaryPopUp
} from '../../actions';
import { getFirstDateOfCurrentMonth } from 'commons/util';
import ReportingCurrencyPopUpFilters from '../components/ReportingCurrencyPopUpFilters';
import PopUpSimulatedGrids from '../components/PopUpSimulatedGrids';
import { onboardingScreen } from '../../util';

class ReportingCurrencyPopUpContent extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.populateValuesForEdit = this.populateValuesForEdit.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleSimulate = this.handleSimulate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCloseMessagePopup = this.handleCloseMessagePopup.bind(this);
    this.renderSimulateButton = this.renderSimulateButton.bind(this);
    this.validateSimulateParameters = this.validateSimulateParameters.bind(this);
    this.state = this.getDefaultState();
  }

  componentDidMount() {
    if (this.props.widgetView) {
      this.setState(
        {
          selectedLegalEntities: this.props.selectedData.selectedLegalEntities,
          selectedCpes: this.props.selectedData.selectedCpes,
          selectedAgreementTypes: this.props.selectedData
            .selectedAgreementTypes,
          selectedNettingGroups: this.props.selectedData.selectedNettingGroups,
          selectedCurrencies: this.props.selectedData.selectedCurrencies
        },
        () => {
          this.handleSimulate(true);
        }
      );
    }
    else {
      if (
        this.props.view === onboardingScreen.REPORTING &&
        !_.isEmpty(this.props.selectedRowProps)
      ) {
        var populateValuesForEdit = this.populateValuesForEdit();
        this.setState(populateValuesForEdit);
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.view === onboardingScreen.REPORTING &&
      _.isEmpty(prevProps.selectedRowProps) &&
      !_.isEmpty(this.props.selectedRowProps) &&
      this.props.widgetView === undefined
    ) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  onSelect(params) {
    const { key, value } = params;
    this.setState({ [key]: value });
  }

  getDefaultState() {
    return {
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedAgreementTypes: [],
      selectedNettingGroups: [],
      selectedCurrencies: [],
      selectedReportingCurrencies: [],
      selectedDate: getFirstDateOfCurrentMonth(),
      isSimulate: false,
      selectedRowData: [],
      saveSelectMessage: false,
      error: {
        selectedLegalEntities: false,
        selectedReportingCurrencies: false,
        selectedDate: false
      }
    };
  }

  populateValuesForEdit() {
    const rowData = this.props.selectedRowProps;
    let populateValues = {};

    if (
      typeof rowData.legal_entity_id !== "undefined" &&
      rowData.legal_entity_id !== ""
    ) {
      populateValues.selectedLegalEntities = {
        key: rowData.legal_entity_id,
        value: `${rowData.legalEntity} [${rowData.legal_entity_id}]`
      };
    }
    if (typeof rowData.cpe_id !== "undefined" && rowData.cpe_id !== "") {
      populateValues.selectedCpes = {
        key: rowData.cpe_id,
        value: `${rowData.cpe} [${rowData.cpe_id}]`
      };
    }
    if (
      typeof rowData.agreement_type_id !== "undefined" &&
      rowData.agreement_type_id !== ""
    ) {
      populateValues.selectedAgreementTypes = {
        key: rowData.agreement_type_id,
        value: `${rowData.agreementType} [${rowData.agreement_type_id}]`
      };
    }
    if (
      typeof rowData.netting_group_id !== "undefined" &&
      rowData.netting_group_id !== ""
    ) {
      populateValues.selectedNettingGroups = {
        key: rowData.netting_group_id,
        value: `${rowData.nettingGroup} [${rowData.netting_group_id}]`
      };
    }
    if (
      typeof rowData.currency_id !== "undefined" &&
      rowData.currency_id !== ""
    ) {
      populateValues.selectedCurrencies = {
        key: rowData.currency_id,
        value: `${rowData.ccy} [${rowData.currency_id}]`
      };
    }
    populateValues.selectedReportingCurrencies = {
      key: rowData.reporting_currency_id,
      value: `${rowData.reportingCurrency} [${rowData.reporting_currency_id}]`
    };
    populateValues.selectedDate = this.state.selectedDate;

    return populateValues;
  }

  handleClosePopUp() {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.setState(this.getDefaultState());
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  }

  handleCloseMessagePopup() {
    this.props.resetSaveData();
    this.props.resetDeleteReportingData();
  }

  handleSimulate(withoutValidation) {
    let payload = {
      "ruleSystemFilter.legalEntityIds": getValueForSingleSelectOrUndefined(
        this.state.selectedLegalEntities
      ),
      "ruleSystemFilter.cpeIds": getValueForSingleSelectOrUndefined(
        this.state.selectedCpes
      ),
      "ruleSystemFilter.agreementTypeIds": getValueForSingleSelectOrUndefined(
        this.state.selectedAgreementTypes
      ),
      "ruleSystemFilter.nettingGroupIds": getValueForSingleSelectOrUndefined(
        this.state.selectedNettingGroups
      ),
      "ruleSystemFilter.currencyIds": getValueForSingleSelectOrUndefined(
        this.state.selectedCurrencies
      ),
      "ruleSystemFilter.reportingCurrencyId": getValueForSingleSelectOrUndefined(
        this.state.selectedReportingCurrencies
      ),
      "ruleSystemFilter.startDate": this.state.selectedDate
    };
    if (
      (withoutValidation !== undefined && typeof withoutValidation === 'boolean' && withoutValidation) ||
      this.validateSimulateParameters(payload)
    ) {
      this.props.fetchSimulateData(payload, onboardingScreen.REPORTING);
      this.setState({ isSimulate: true });
    }
  }

  handleDelete(){
    let columnValues = this.props.selectedRowProps.columnValues;
    let dateArray = columnValues[0].split("-");
    dateArray[1] = this.state.selectedDate.replace(/-/g, "");
    columnValues[0] = dateArray.join("-");

    this.props.deleteReportingCurrency([columnValues]);
    this.handleClosePopUp();
  }

  validateSimulateParameters(parameters) {
    let ret = true;
    const error = this.getDefaultState().error;
    if (parameters["ruleSystemFilter.legalEntityIds"] === undefined ||
        parameters["ruleSystemFilter.legalEntityIds"] == "-1") {
      ret = false;
      error.selectedLegalEntities = true;
    }
    if (parameters["ruleSystemFilter.reportingCurrencyId"] === undefined ||
        parameters["ruleSystemFilter.reportingCurrencyId"] == "-1") {
      ret = false;
      error.selectedReportingCurrencies = true;
    }
    if (parameters["ruleSystemFilter.startDate"] === "") {
      ret = false;
      error.selectedDate = true;
    }
    this.setState({ error });
    return ret;
  }

  handleSave() {
    if (this.state.selectedRowData.length === 0) {
      this.setState({ saveSelectMessage: true });
      return;
    }

    if (this.props.widgetView) {
      this.props.saveAndValidateWidgetView(this.state.selectedRowData, onboardingScreen.REPORTING,
        true);
      this.props.resetSimulateData();
      this.props.resetSelectedRowData();
    } else {
      let payload = this.state.selectedRowData.map(row => row.columnValues);
      this.props.saveSimulateData(payload, onboardingScreen.REPORTING);
      this.handleClosePopUp();
    }
  }

  renderSimulateButton(isSaveDisabled) {
    if(this.props.isDeleteClicked){
      return <FilterButton onClick={this.handleDelete} label = "Delete"/>
    }
    if (isSaveDisabled) {
      return (
        <div style={{ width: '500px', margin: 'auto' }}>
          <Message
            warning
            messageData={
              <div>
                You need to simulate the rule before you can save it.
                <FilterButton
                  onClick={this.handleSimulate}
                  label={'Run Simulation'}
                  className="margin--small button--primary"
                />
              </div>
            }
          />
        </div>
      );
    } else {
      return (
        <div>
          SIMULATION RESULTS
          <FilterButton
            onClick={this.handleSimulate}
            label={'Re-run Simulation'}
            className="margin--small margin--left button--primary"
          />
        </div>
      );
    }
  }

  renderPopUp() {
    let popUp = null;
    let title;
    title = _.isEmpty(this.props.selectedRowProps)
      ? 'ADD ACCRUAL POSTING CURRENCY RULES'
      : 'EDIT ACCRUAL POSTING CURRENCY RULES';

    let isSaveDisabled =
      !this.state.isSimulate ||
      typeof this.props.simulatedData === 'undefined' ||
      typeof this.props.simulatedData.existingList === 'undefined' ||
      typeof this.props.simulatedData.simulatedList === 'undefined';

    let popupWidth = isSaveDisabled ? '800px' : '1000px';
    let filterMargin = isSaveDisabled ? '0px' : '50px';
    popUp = (
      <React.Fragment>
        <Card title="Rule Configuration">
          <hr />
          <div style={{ width: '600px', marginLeft: filterMargin }} class="text-align--right">
            <ReportingCurrencyPopUpFilters
              onSelect={this.onSelect}
              state={this.state}
              isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
              isDeleteClicked={this.props.isDeleteClicked}
            />
          </div>
        </Card>
        <br />
        {this.renderSimulateButton(isSaveDisabled)}
        <PopUpSimulatedGrids
          isSimulate={this.state.isSimulate}
          simulatedData={this.props.simulatedData}
          view={onboardingScreen.REPORTING}
          saveSelectMessage={this.state.saveSelectMessage}
          onSelect={this.onSelect}
          handleSave={this.handleSave}
        />
        <FilterButton
          onClick={this.handleSave}
          disabled={isSaveDisabled || this.state.selectedRowData.length === 0}
          label="Save Accrual Posting Currency Rule"
        />
      </React.Fragment>
    );
    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      deleteReportingCurrency,
      resetDeleteReportingData,
      resetSimulateData,
      resetSelectedRowData,
      hidePrimaryPopUp
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen.reporting,
    isDeleteClicked: state.financingOnboarding.isDeleteClicked,
    simulatedData: state.financingOnboarding.simulatedData,
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    selectedRowProps: state.financingOnboarding.selectedRowData,
    view: state.financingOnboarding.view
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportingCurrencyPopUpContent);
