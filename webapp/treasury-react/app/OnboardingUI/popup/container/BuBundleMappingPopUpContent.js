import React, { Component } from 'react';
import _ from 'lodash';
import Dialog from 'commons/components/Dialog';
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FilterButton from 'commons/components/FilterButton';
import { getValueForSingleSelectOrUndefined } from '../../util';
import { Card, Layout } from 'arc-react-components';
import {
  destroyDeleteToggle,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  deleteBuBundleMapping,
  resetDeleteBuBundleData,
  resetSimulateData,
  resetSelectedRowData,
  hidePrimaryPopUp
} from '../../actions';
import { getFirstDateOfCurrentMonth } from 'commons/util';
import BuBundleMappingPopUpFilters from '../components/BuBundleMappingPopUpFilters';
import PopUpSimulatedGrids from '../components/PopUpSimulatedGrids';
import { onboardingScreen } from '../../util';

class BuBundleMappingPopUpContent extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.populateValuesForEdit = this.populateValuesForEdit.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleSimulate = this.handleSimulate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCloseMessagePopup = this.handleCloseMessagePopup.bind(this);
    this.renderSimulateButton = this.renderSimulateButton.bind(this);
    this.validateSimulateParameters = this.validateSimulateParameters.bind(this);
    this.state = this.getDefaultState();
  }

  componentDidMount() {
    if (this.props.widgetView) {
      this.setState(
        {
          selectedLegalEntities: this.props.selectedData.selectedLegalEntities,
          selectedCpes: this.props.selectedData.selectedCpes,
          selectedAgreementTypes: this.props.selectedData
            .selectedAgreementTypes,
          selectedNettingGroups: this.props.selectedData.selectedNettingGroups,
          selectedCurrencies: this.props.selectedData.selectedCurrencies
        },
        () => {
          this.handleSimulate(true);
        }
      );
    }
    else {
      if (
        this.props.view === onboardingScreen.BUBUNDLE &&
        !_.isEmpty(this.props.selectedRowProps)
      ) {
        var populateValuesForEdit = this.populateValuesForEdit();
        this.setState(populateValuesForEdit);
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.view === onboardingScreen.BUBUNDLE &&
      _.isEmpty(prevProps.selectedRowProps) &&
      !_.isEmpty(this.props.selectedRowProps)
    ) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  onSelect(params) {
    const { key, value } = params;
    this.setState({ [key]: value });
  }

  getDefaultState() {
    return {
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedAgreementTypes: [],
      selectedNettingGroups: [],
      selectedCurrencies: [],
      selectedBusinessUnits: [],
      selectedBundles: [],
      selectedDate: getFirstDateOfCurrentMonth(),
      isSimulate: false,
      selectedRowData: [],
      saveSelectMessage: false,
      error: {
        selectedBusinessUnits: false,
        selectedBundles: false,
        selectedDate: false
      }
    };
  }

  populateValuesForEdit() {
    const rowData = this.props.selectedRowProps;
    let populateValues = {};

    if (
      typeof rowData.legal_entity_id !== "undefined" &&
      rowData.legal_entity_id !== ""
    ) {
      populateValues.selectedLegalEntities = {
        key: rowData.legal_entity_id,
        value: `${rowData.legalEntity} [${rowData.legal_entity_id}]`
      };
    }
    if (typeof rowData.cpe_id !== "undefined" && rowData.cpe_id !== "") {
      populateValues.selectedCpes = {
        key: rowData.cpe_id,
        value: `${rowData.cpe} [${rowData.cpe_id}]`
      };
    }
    if (
      typeof rowData.funding_type_id !== "undefined" &&
      rowData.funding_type_id !== ""
    ) {
      populateValues.selectedAgreementTypes = {
        key: rowData.funding_type_id,
        value: `${rowData.agreementType} [${rowData.funding_type_id}]`
      };
    }
    if (
      typeof rowData.netting_group_id !== "undefined" &&
      rowData.netting_group_id !== ""
    ) {
      populateValues.selectedNettingGroups = {
        key: rowData.netting_group_id,
        value: `${rowData.nettingGroup} [${rowData.netting_group_id}]`
      };
    }
    if (typeof rowData.ccy_spn !== "undefined" && rowData.ccy_spn !== "") {
      populateValues.selectedCurrencies = {
        key: rowData.ccy_spn,
        value: `${rowData.ccy} [${rowData.ccy_spn}]`
      };
    }

    if (
      typeof rowData.business_unit_id !== "undefined" &&
      rowData.business_unit_id !== ""
    ) {
      populateValues.selectedBusinessUnits = {
        key: rowData.business_unit_id,
        value: `${rowData.businessUnit} [${rowData.business_unit_id}]`
      };
    }

    populateValues.selectedBundles = {
      key: rowData.bundle_id,
      value: `${rowData.bundle} [${rowData.bundle_id}]`
    };
    populateValues.selectedDate = this.state.selectedDate;

    return populateValues;
  }

  handleClosePopUp() {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.setState(this.getDefaultState());
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
  }

  handleCloseMessagePopup() {
    this.props.resetSaveData();
    this.props.resetDeleteBuBundleData();
  }

  handleSimulate(withoutValidation) {
    let payload = {
      "ruleSystemFilter.legalEntityIds": getValueForSingleSelectOrUndefined(
        this.state.selectedLegalEntities
      ),
      "ruleSystemFilter.cpeIds": getValueForSingleSelectOrUndefined(
        this.state.selectedCpes
      ),
      "ruleSystemFilter.agreementTypeIds": getValueForSingleSelectOrUndefined(
        this.state.selectedAgreementTypes
      ),
      "ruleSystemFilter.nettingGroupIds": getValueForSingleSelectOrUndefined(
        this.state.selectedNettingGroups
      ),
      "ruleSystemFilter.currencyIds": getValueForSingleSelectOrUndefined(
        this.state.selectedCurrencies
      ),
      "ruleSystemFilter.inputBusinessUnitIds": getValueForSingleSelectOrUndefined(
        this.state.selectedBusinessUnits
      ),
      "ruleSystemFilter.bundleId": getValueForSingleSelectOrUndefined(
        this.state.selectedBundles
      ),
      "ruleSystemFilter.startDate": this.state.selectedDate
    };

    if (
      (withoutValidation !== undefined && typeof withoutValidation === 'boolean' && withoutValidation) ||
      this.validateSimulateParameters(payload)
    ) {
      this.props.fetchSimulateData(payload, onboardingScreen.BUBUNDLE);
      this.setState({ isSimulate: true });
    }
  }

  handleDelete(){
    let columnValues = this.props.selectedRowProps.columnValues;
    let dateArray = columnValues[0].split("-");
    dateArray[1] = this.state.selectedDate.replace(/-/g, "");
    columnValues[0] = dateArray.join("-");

    this.props.deleteBuBundleMapping([columnValues]);
    this.handleClosePopUp();
  }
  validateSimulateParameters(parameters) {
    let ret = true;
    const error = this.getDefaultState().error;
    if (parameters["ruleSystemFilter.inputBusinessUnitIds"] === undefined || 
        parameters["ruleSystemFilter.inputBusinessUnitIds"] == "-1") {
      ret = false;
      error.selectedBusinessUnits = true;
    }
    if (parameters["ruleSystemFilter.bundleId"] === undefined || 
        parameters["ruleSystemFilter.bundleId"] == "-1") {
      ret = false;
      error.selectedBundles = true;
    }
    if (parameters["ruleSystemFilter.startDate"] === "") {
      ret = false;
      error.selectedDate = true;
    }
    this.setState({ error });
    return ret;
  }

  handleSave() {
    if (this.state.selectedRowData.length === 0) {
      this.setState({ saveSelectMessage: true });
      return;
    }

    if (this.props.widgetView) {
      this.props.saveAndValidateWidgetView(this.state.selectedRowData, onboardingScreen.BUBUNDLE,
        true);
      this.props.resetSimulateData();
      this.props.resetSelectedRowData();
    } else {
      let payload = this.state.selectedRowData.map(row => row.columnValues);
      this.props.saveSimulateData(payload, onboardingScreen.BUBUNDLE);
      this.handleClosePopUp();
    }
  }

  renderSimulateButton(isSaveDisabled) {
    if(this.props.isDeleteClicked){
      return <FilterButton onClick={this.handleDelete} label="Delete" />
    }
    if (isSaveDisabled) {
      return (
        <div style={{ width: '500px', margin: 'auto' }}>
          <Message
            warning
            messageData={
              <div>
                You need to simulate the rule before you can save it.
                <FilterButton
                  onClick={this.handleSimulate}
                  label={'Run Simulation'}
                  className="margin--small button--primary"
                />
              </div>
            }
          />
        </div>
      );
    } else {
      return (
        <div>
          SIMULATION RESULTS
          <FilterButton
            onClick={this.handleSimulate}
            label={'Re-run Simulation'}
            className="margin--small margin--left button--primary"
          />
        </div>
      );
    }
  }

  renderPopUp() {
    let popUp = null;
    let title;
    title = _.isEmpty(this.props.selectedRowProps)
      ? 'ADD BU BUNDLE MAPPING'
      : 'EDIT BU BUNDLE MAPPING';

    let isSaveDisabled =
      !this.state.isSimulate ||
      typeof this.props.simulatedData === 'undefined' ||
      typeof this.props.simulatedData.existingList === 'undefined' ||
      typeof this.props.simulatedData.simulatedList === 'undefined';

    let popupWidth = isSaveDisabled ? '800px' : '1100px';
    let filterMargin = isSaveDisabled ? '0px' : '100px';
    popUp = (
      <React.Fragment>
        <Card title="Rule Configuration">
          <hr />
          <div style={{ width: '600px', marginLeft: filterMargin }} class="text-align--right">
            <BuBundleMappingPopUpFilters
              onSelect={this.onSelect}
              state={this.state}
              isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
              isDeleteClicked={this.props.isDeleteClicked}
            />
          </div>
        </Card>
        <br />
        {this.renderSimulateButton(isSaveDisabled)}
        <PopUpSimulatedGrids
          isSimulate={this.state.isSimulate}
          simulatedData={this.props.simulatedData}
          view={onboardingScreen.BUBUNDLE}
          saveSelectMessage={this.state.saveSelectMessage}
          onSelect={this.onSelect}
          handleSave={this.handleSave}
        />
        <FilterButton onClick={this.handleSave} disabled={isSaveDisabled || this.state.selectedRowData.length === 0} label="Save" />
      </React.Fragment>
    );

    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      deleteBuBundleMapping,
      resetDeleteBuBundleData,
      resetSimulateData,
      resetSelectedRowData,
      hidePrimaryPopUp
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.financingOnboarding.isPrimaryPopUpOpen.buBundle,
    isDeleteClicked: state.financingOnboarding.isDeleteClicked,
    simulatedData: state.financingOnboarding.simulatedData,
    saveSimulatedData: state.financingOnboarding.saveSimulatedData,
    selectedRowProps: state.financingOnboarding.selectedRowData,
    view: state.financingOnboarding.view
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuBundleMappingPopUpContent);
