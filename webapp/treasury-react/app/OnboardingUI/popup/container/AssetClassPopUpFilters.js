import React, { Component } from 'react';
import _ from 'lodash';
import { Layout } from 'arc-react-components';
import AssetClassPopupGrid from 'commons/components/GridWithCheckbox';
import InputFilter from 'commons/components/InputFilter';
import InputNumberFilter from 'commons/components/InputNumberFilter';
import DateFilter from 'commons/container/DateFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import GradingFilter from 'commons/container/GradingFilter';
import IndexFilter from 'commons/container/IndexFilter';
import CountryFilter from 'commons/container/CountryFilter';
import CountryCodeFilter from 'commons/container/CountryCodeFilter';
import FOTypeFilter from 'commons/container/FOTypeFilter';
import SubTypeFilter from 'commons/container/SubTypeFilter';
import BicsIndustrySubgroupFilter from 'commons/container/BicsIndustrySubgroupFilter';
import Message from 'commons/components/Message';
import FilterButton from 'commons/components/FilterButton';
import Table from 'commons/components/Table';
import Dialog from 'commons/components/Dialog';
import AttributeSourceTypeFilter from 'commons/container/AttributeSourceTypeFilter';
import AttributeSourceTypeSpecificAttributeNameFilter from 'commons/container/AttributeSourceTypeSpecificAttributeNameFilter';
import CheckboxGrid from 'commons/components/GridWithCheckbox';

import {
  getAllAssetClassColumns,
  getExistingAssetClassColumns,
  getAssetClassConditionColumns
} from '../../grid/columnConfig';
import { getSimulatedGridOptions } from '../../grid/gridOptions';
import { getFirstDateOfCurrentMonth } from 'commons/util';

import { onboardingScreen } from '../../util';

export default class AssetClassPopUp extends Component {
  constructor(props) {
    super(props);
    this.renderData = this.renderData.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleRightClick = this.handleRightClick.bind(this);
    this.handleLeftClick = this.handleLeftClick.bind(this);
    this.onDblClickHandler = this.onDblClickHandler.bind(this);
    this.handleAddNewAssetClass = this.handleAddNewAssetClass.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmitForAddAssetClass = this.handleSubmitForAddAssetClass.bind(this);
    this.handleCloseAgreementsPopup = this.handleCloseAgreementsPopup.bind(this);
    this.handleCloseAssetClassGroupErrorDialog = this.handleCloseAssetClassGroupErrorDialog.bind(this);
    this.resetDependentFilters = this.resetDependentFilters.bind(this);
    this.state = this.getDefaultState();
    this.onCellClickHandler=this.onCellClickHandler.bind(this);
  }

  componentDidMount() {
    this.props.fetchSimulateData({}, onboardingScreen.ASSET);
  }

  componentDidUpdate(prevProps) {
    if (_.isEmpty(prevProps.assetClassData) && !_.isEmpty(this.props.assetClassData)) {
      this.setState({ allAssetClassGridData: this.props.assetClassData });
    }
    if (!_.isEqual(prevProps.assetClassGroupDataById, this.props.assetClassGroupDataById)) {
      if (typeof this.props.assetClassGroupDataById.data !== 'undefined' && typeof this.props.assetClassGroupDataById.data.resultList !== 'undefined' ) {
        let assetClassGroupData = this.props.assetClassGroupDataById.data.resultList;
        this.setState({ allAssetClassGridSelectedRows: assetClassGroupData, updateGridForEdit: true });
      }
    }
    if (this.state.updateGridForEdit && !_.isEmpty(this.state.allAssetClassGridData)) {
      this.setState({ updateGridForEdit: false });
      this.handleRightClick();
    }
  }

  onSelect(params) {
    const { key, value } = params;
    this.resetDependentFilters(key, value);
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    newState['isAddDisabled'] = false;
    this.setState(newState);
    if(this.state.selectedAttributes.includes(this.state.selectedAttributeName.key)) {
      this.setState({isAddDisabled:true});
    }
  }

    onCellClickHandler = args => {
      if (args.colId === "delete") {
        var updatedSelectedAttributes = this.state.selectedAttributes.filter(data => data != args.item.attrName );
        var  updatedSelectedConditions = this.state.selectedConditions.filter(data => data.attrName !== args.item.attrName);
        if(args.item.attrName === 'type_id') {
          updatedSelectedConditions = updatedSelectedConditions.filter(data => data.attrName !== 'subtype_id');
          updatedSelectedAttributes = updatedSelectedAttributes.filter(data=> data != 'subtype_id');
        }
        if(args.item.attrName === 'subtype_id') {
          updatedSelectedConditions = updatedSelectedConditions.filter(data => data.attrName !== 'type_id');
          updatedSelectedAttributes = updatedSelectedAttributes.filter(data=> data != 'type_id');
        }
        this.setState({
          selectedConditions:updatedSelectedConditions , selectedAttributes:updatedSelectedAttributes
        });
        if(!this.state.selectedAttributes.includes(this.state.selectedAttributeName.key)){
          this.setState({isAddDisabled:false});
        }
      }
    }



  resetDependentFilters(key) {
    if (key === "selectedAttributeSourceTypes") {
      this.setState({ selectedAttributeName: [], selectedAttributeValue: [], selectedFOTypes: []});
    } else if (key === "selectedAttributeName") {
      this.setState({ selectedAttributeValue: [], selectedFOTypes: [] });
    } else if (key === "selectedFOTypes") {
      this.setState({ selectedAttributeValue: [] });
    }
  }


  onDblClickHandler = (args) => {
    this.setState({ attributeData: args.attributes });
  };

  getDefaultState() {
    const allAssetClassGridData = !_.isEmpty(this.props.assetClassData) ? this.props.assetClassData : [];
    return {
      name: [],
      description: [],
      selectedDate: getFirstDateOfCurrentMonth(),
      allAssetClassGridData,
      existingAssetClassGridData: [],
      allAssetClassGridSelectedRows: [],
      existingAssetClassGridSelectedRows: [],
      attributeData: [],
      addNewAssetClass: false,
      selectedAttributeSourceTypes: [],
      selectedAttributeName: [],
      selectedAttributeValue: [],
      selectedFOTypes: [],
      selectedConditions: [],
      selectedRowData: [],
      selectedAttributes:[],
      replaceAttributes:false,
      isSubmitClicked: false,
      isAddDisabled: false,
      updateGridForEdit: false,
      showAffectedAgreements: false,
      showAssetClassGroupErrorDialog: false,
      error: {
        name: false,
        description: false
      }
    };
  }

  handleAddNewAssetClass() {
    this.setState({ addNewAssetClass: true });
  }

  handleCloseAgreementsPopup() {
    this.setState({ showAffectedAgreements: false });
  }

  handleCloseAssetClassGroupErrorDialog() {
    this.setState({ showAssetClassGroupErrorDialog: false });
  }

  handleRightClick() {
    const allAssetClassesMap = this.state.allAssetClassGridData.reduce((map, obj) => {
      map[obj.id] = obj;
      return map;
    }, {});
    const selectedRows = this.state.allAssetClassGridSelectedRows.reduce((list, obj) => {
      list.push(obj.id);
      return list;
    }, []);
    if (selectedRows.length === 0) return;
    let updatedDataInCurrentGrid = _.clone(this.state.allAssetClassGridData);
    let updatedDataInExistingGrid = _.clone(this.state.existingAssetClassGridData);

    for (let i in selectedRows) {
      updatedDataInExistingGrid.push(allAssetClassesMap[selectedRows[i]]);
      let index = updatedDataInCurrentGrid.indexOf(allAssetClassesMap[selectedRows[i]]);
      updatedDataInCurrentGrid.splice(index, 1);
    }

    this.setState({
      allAssetClassGridData: updatedDataInCurrentGrid,
      existingAssetClassGridData: updatedDataInExistingGrid,
      allAssetClassGridSelectedRows: [],
      existingAssetClassGridSelectedRows: []
    });
  }

  handleLeftClick() {
    const existingAssetClassesMap = this.state.existingAssetClassGridData.reduce((map, obj) => {
      map[obj.id] = obj;
      return map;
    }, {});
    const selectedRows = this.state.existingAssetClassGridSelectedRows.reduce((list, obj) => {
      list.push(obj.id);
      return list;
    }, []);
    if (selectedRows.length === 0) return;
    let updatedDataInCurrentGrid = _.clone(this.state.allAssetClassGridData);
    let updatedDataInExistingGrid = _.clone(this.state.existingAssetClassGridData);

    for (let i in selectedRows) {
      updatedDataInCurrentGrid.push(existingAssetClassesMap[selectedRows[i]]);
      let index = updatedDataInExistingGrid.indexOf(existingAssetClassesMap[selectedRows[i]]);
      updatedDataInExistingGrid.splice(index, 1);
    }

    this.setState({
      allAssetClassGridData: updatedDataInCurrentGrid,
      existingAssetClassGridData: updatedDataInExistingGrid,
      allAssetClassGridSelectedRows: [],
      existingAssetClassGridSelectedRows: []
    });
  }

  handleAdd() {
    var conditions = _.cloneDeep(this.state.selectedConditions);
    var selectedAttributesClone = _.cloneDeep(this.state.selectedAttributes);
    var id = (conditions.length == 0)? conditions.length : (conditions.slice(-1)[0].id+1);
    var condition = {
      sourceTypeDisplay: this.state.selectedAttributeSourceTypes.value,
      attrNameDisplay: this.state.selectedAttributeName.value,
      sourceType: this.state.selectedAttributeSourceTypes.key,
      attrName: this.state.selectedAttributeName.key,
      id: id++
    };
    selectedAttributesClone.push(condition.attrName);
    if (this.state.selectedAttributeName.value === 'Subtype') {
      condition.attrValueDisplay = this.state.selectedAttributeValue.value;
      condition.attrValue = this.state.selectedAttributeValue.key;
      conditions.push(condition);
      var typeCondition = _.cloneDeep(condition);
      typeCondition.attrNameDisplay = 'FOType';
      typeCondition.attrName = 'type_id';
      typeCondition.attrValueDisplay = this.state.selectedFOTypes.value;
      typeCondition.attrValue = this.state.selectedFOTypes.key;
      typeCondition.id = id++;
      conditions.push(typeCondition);
      selectedAttributesClone.push('type_id');
    } else if (this.state.selectedAttributeName.value === 'Instrument Type') {
      condition.attrValueDisplay = this.state.selectedFOTypes.value;
      condition.attrValue = this.state.selectedFOTypes.key;
      conditions.push(condition);
      var typeCondition = _.cloneDeep(condition);
      typeCondition.attrNameDisplay = 'Subtype';
      typeCondition.attrName = 'subtype_id';
      typeCondition.attrValueDisplay = this.state.selectedAttributeValue.value;
      typeCondition.attrValue = this.state.selectedAttributeValue.key;
       typeCondition.id = id++;
      conditions.push(typeCondition);
      selectedAttributesClone.push('subtype_id');
    } else {
      condition.attrValueDisplay = this.state.selectedAttributeValue;
      condition.attrValue = this.state.selectedAttributeValue;
      if (
        this.state.selectedAttributeName.value !== 'ISIN' &&
        this.state.selectedAttributeName.value !== 'PNL SPN' &&
        this.state.selectedAttributeName.value !== 'Index'
      ) {
        condition.attrValueDisplay = this.state.selectedAttributeValue.value;
        condition.attrValue = this.state.selectedAttributeValue.key;
      }
      if (this.state.selectedAttributeName.value === 'Grading') {
        condition.attrValue = this.state.selectedAttributeValue.value;
      }
      conditions.push(condition);
    }
    this.setState({ selectedConditions: conditions, selectedAttributes: selectedAttributesClone, isAddDisabled: true });
  }

  ErrorNote(error, message) {
    if (error)
      return <span className="red margin--small">{message}</span>;
    else
      return;
  }

  loadAttributeValueFilterConditional() {
    let filter;
    if (typeof this.state.selectedAttributeName !== 'undefined' && this.state.selectedAttributeName != null) {
      switch (this.state.selectedAttributeName.value) {
        case 'ISIN':
          filter = (
            <InputFilter
              onSelect={this.onSelect}
              data={this.state.selectedAttributeValue}
              stateKey="selectedAttributeValue"
              label="AttributeValue"
            />
          );
          break;
        case 'PNL SPN':
        case 'Index':
          filter = (
            <InputNumberFilter
              onSelect={this.onSelect}
              data={this.state.selectedAttributeValue}
              stateKey="selectedAttributeValue"
              label="AttributeValue"
              layoutStyle="standard"
            />
          );
          break;
        case 'Currency':
          filter = (
            <CurrencyFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedAttributeValue}
              stateKey={'selectedAttributeValue'}
              multiSelect={false}
              horizontalLayout={true}
              label={'Currency'}
            />
          );
          break;
        case 'Country':
          filter = (
            <CountryFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedAttributeValue}
              stateKey={'selectedAttributeValue'}
              multiSelect={false}
              horizontalLayout={true}
              label={'Country'}
            />
          );
          break;
        case 'Bics Industry Subgroup':
          filter = (
            <BicsIndustrySubgroupFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedAttributeValue}
              stateKey={'selectedAttributeValue'}
              multiSelect={false}
              horizontalLayout={true}
            />
          );
          break;
        case 'Instrument Type':
        case 'Subtype':
          filter = (
            <React.Fragment>
              <FOTypeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedFOTypes}
                stateKey={'selectedFOTypes'}
                multiSelect={false}
                horizontalLayout={true}
                label={'Instrument Type'}
              />
              <br />
              <SubTypeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedAttributeValue}
                selectedFOTypeIds={this.state.selectedFOTypes.key}
                multiSelect={false}
                horizontalLayout={true}
                stateKey={'selectedAttributeValue'}
              />
            </React.Fragment>
          );
          break;
        case 'Grading':
          filter = (
            <GradingFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedAttributeValue}
              stateKey={'selectedAttributeValue'}
              multiSelect={false}
              horizontalLayout={true}
              label={'Grading'}
            />
          );
          break;
        case 'Issuer Country':
          filter = (
            <CountryCodeFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedAttributeValue}
              stateKey={'selectedAttributeValue'}
              multiSelect={false}
              horizontalLayout={true}
              label={'Issuer Country'}
            />
          );
          break;
      }
    }
    return filter;
  }

  handleSubmitForAddAssetClass() {
    if (this.state.selectedRowData.length === 0) { return; }
    var payload = {
      assetClassName: this.state.name,
      attributes: this.state.selectedRowData
    };
    this.setState({ isSubmitClicked: true });
    this.props.saveAssetClass(payload);
    this.props.handleClosePopUp();
  }

  handleSubmit() {
    if (!this.props.isEditClicked) {
      let error = { name: false, description: false };
      error.name = this.state.name.length === 0;
      error.description = this.state.description.length === 0;
      this.setState({ error });
      if (error.name || error.description) {
        return;
      }
    }
    if (this.props.isEditClicked && !this.state.showAffectedAgreements) {
      this.props.fetchAgreementsAffectedForAsset({
        selectedAssetClassGroupId: this.props.selectedRowProps.assetClassGroupId.toString()
      });
      this.setState({ showAffectedAgreements: true });
      return;
    }
    let selectedIds = [];
    let parameters = {};
    for (var i in this.state.existingAssetClassGridData) {
      selectedIds.push(this.state.existingAssetClassGridData[i].id);
    }
    if (!this.props.isEditClicked) {
      parameters = {
        acgName: this.state.name,
        acgDescription: this.state.description,
        selectedAcIds: selectedIds.toString(),
        dateString: this.state.selectedDate,
        isEditClicked: false
      };
    } else {
      parameters = {
        selectedAcIds: selectedIds.toString(),
        selectedAssetClassGroupId: this.props.selectedRowProps.assetClassGroupId.toString(),
        dateString: this.state.selectedDate,
        isEditClicked: true
      };
    }
    if (parameters.selectedAcIds === '' && !this.state.showAssetClassGroupErrorDialog) {
      this.setState({ showAssetClassGroupErrorDialog: true });
      return;
    }
    this.props.saveAssetClassGroup(parameters, onboardingScreen.ASSET);
    this.props.handleClosePopUp();
  }

  renderData() {
    let allAssetClassGrid, existingAssetClassGrid;

    allAssetClassGrid = (
      <AssetClassPopupGrid
        data={this.state.allAssetClassGridData}
        gridId="allAssetClasses"
        gridColumns={getAllAssetClassColumns()}
        gridOptions={getSimulatedGridOptions()}
        label=""
        onSelect={this.onSelect}
        preSelectAllRows={false}
        selectedRowDataKey="allAssetClassGridSelectedRows"
        onDblClick={this.onDblClickHandler}
        onCellClick={this.onCellClickHandler}
      />
    );

    existingAssetClassGrid = (
      <AssetClassPopupGrid
        data={this.state.existingAssetClassGridData}
        gridId="existingAssetClasses"
        gridColumns={getExistingAssetClassColumns()}
        gridOptions={getSimulatedGridOptions()}
        label=""
        onSelect={this.onSelect}
        preSelectAllRows={false}
        selectedRowDataKey="existingAssetClassGridSelectedRows"
        onDblClick={this.onDblClickHandler}
        onCellClick={this.onCellClickHandler}
      />
    );

    const labels = ['Source Type', 'Attribute', 'Value', 'Underlying Check'];
    const dataKeys = ['source_type_id', 'attr_name', 'value', 'underlying_check'];

    return (
      <Layout isRowType>
        {this.props.isEditClicked ? (
          <React.Fragment />
        ) : (
          <Layout.Child childId="assetClassPopup1">
            <Layout isColumnType={true}>
              <Layout.Child childId="assetClassPopup2" size={1}>
                <br />
                <InputFilter
                  onSelect={this.onSelect}
                  data={this.state.name}
                  stateKey="name"
                  label="Name"
                  isError={this.state.error.name}
                />
              </Layout.Child>
              <Layout.Child childId="assetClassPopup3" size={1}>
                <br />
              </Layout.Child>
            </Layout>
          </Layout.Child>
        )}
        {this.props.isEditClicked ? (
          <React.Fragment />
        ) : (
          <Layout.Child childId="assetClassPopup4">
            <Layout isColumnType={true}>
              <Layout.Child size={1} childId="assetClassPopup5">
                <br />
                <InputFilter
                  onSelect={this.onSelect}
                  data={this.state.description}
                  stateKey="description"
                  label="Description"
                  isError={this.state.error.description}
                />
              </Layout.Child>
              <Layout.Child size={1} childId="assetClassPopup6">
                <br />
              </Layout.Child>
            </Layout>
          </Layout.Child>
        )}
        <Layout.Child childId="assetClassPopup29">
          <Layout isColumnType={true}>
            <Layout.Child size={1} childId="assetClassPopup7">
              <br />
              <DateFilter
                onSelect={this.onSelect}
                stateKey="selectedDate"
                data={this.state.selectedDate}
                label="Effective Start Date"
              />
              <br />
            </Layout.Child>
            <Layout.Child size={1} childId="assetClassPopup8">
              <br />
            </Layout.Child>
          </Layout>
          <hr />
          <br />
        </Layout.Child>
        <Layout.Child childId="assetClassPopup9">
          <Layout isColumnType>
            <Layout.Child childId="assetClassPopup10" size={4}>{allAssetClassGrid}</Layout.Child>
            <Layout.Child childId="assetClassPopup11" size={0.25} />
            <Layout.Child childId="assetClassPopup12" size={1.5}>
              <FilterButton
                onClick={this.handleRightClick}
                reset={false}
                label=">"
                style={{ width: '70%', height: '40px' }}
              />
              <br />
              <br />
              <FilterButton
                onClick={this.handleLeftClick}
                reset={false}
                label="<"
                style={{ width: '70%', height: '40px' }}
              />
            </Layout.Child>
            <Layout.Child childId="assetClassPopup13" size={0.5} />
            <Layout.Child childId="assetClassPopup14" size={4}>
              <div className="padding--left">{existingAssetClassGrid}</div>
            </Layout.Child>
          </Layout>
        </Layout.Child>
        <Layout.Child childId="assetClassPopup15">
          <br />
          <Message messageData="Double click on asset class names to get corresponding attribute details." />
        </Layout.Child>
        <Layout.Child childId="assetClassPopup16">
          <br />
          {this.state.attributeData.length !== 0 ? (
            <Table labels={labels} content={this.state.attributeData} dataKeys={dataKeys} />
          ) : (
            <React.Fragment />
          )}
        </Layout.Child>
        <Layout.Child childId="assetClassPopup17">
          <br />
          <FilterButton onClick={this.handleSubmit} reset={false} label="Submit" />
          {this.props.isEditClicked ? null : (
            <FilterButton onClick={this.handleAddNewAssetClass} reset={false} label="Add New Asset Class" />
          )}
        </Layout.Child>
      </Layout>
    );
  }

  renderAddNewAssetClassPopup() {
    return (
      <Layout isRowType>
        <Layout.Child childId="assetClassPopup18">
          <br />
          <InputFilter onSelect={this.onSelect} data={this.state.name} stateKey="name" label="Name" />
        </Layout.Child>
        <Layout.Child childId="assetClassPopup19">
          <br />
          <AttributeSourceTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedAttributeSourceTypes}
            multiSelect={false}
            horizontalLayout={true}
          />
        </Layout.Child>
        <Layout.Child childId="assetClassPopup20">
          <br />
          <AttributeSourceTypeSpecificAttributeNameFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedAttributeName}
            selectedAttributeSourceTypeIds={this.state.selectedAttributeSourceTypes.key}
            multiSelect={false}
            horizontalLayout={true}
          />
        </Layout.Child>
        <Layout.Child childId="assetClassPopup21">
          <br />
          {this.loadAttributeValueFilterConditional()}
          <br />
        </Layout.Child>
        <Layout.Child childId="assetClassPopup22">
         <FilterButton onClick={this.handleAdd} reset={false} label="Add"  disabled={this.state.isAddDisabled} />
        </Layout.Child>
        <Layout.Child childId="assetClassPopup23">
          <br />
          {this.ErrorNote(this.state.isAddDisabled, "Can add only one attribute of each type")}
          <CheckboxGrid
            data={this.state.selectedConditions}
            gridId="Conditions"
            gridColumns={getAssetClassConditionColumns()}
            gridOptions={getSimulatedGridOptions()}
            onCellClick={this.onCellClickHandler}
            onSelect={this.onSelect}
            preSelectAllRows={true}
            heightValue={170}
          />
        </Layout.Child>
        <Layout.Child childId="assetClassPopup24">
          <hr />
          {this.state.selectedConditions.length === 0 ? (
            <React.Fragment />
          ) : (
            <FilterButton onClick={this.handleSubmitForAddAssetClass} reset={false} label="Submit" />
          )}
        </Layout.Child>
      </Layout>
    );
  }

  renderAffectedAgreements() {
    let data = this.props.agreementsAffectedForAsset;
    for (let i in data) {
      data[i].agreement = data[i].agreementType + '-' + data[i].legalEntity + '-' + data[i].cpe;
    }
    return (
      <Dialog
        isOpen
        onClose={this.handleCloseAgreementsPopup}
        title="Confirmation"
        style={{
          width: '500px'
        }}
      >
        <Layout isRowType>
          <Layout.Child childId="assetClassPopup25">
            <Table labels={['Agreements Affected']} content={data} dataKeys={['agreement']} />
          </Layout.Child>
          <Layout.Child childId="assetClassPopup26">
            <hr />
            <FilterButton onClick={this.handleSubmit} reset={false} label="Submit" />
          </Layout.Child>
        </Layout>
      </Dialog>
    );
  }

  renderAssetClassGroupErrorDialog() {
    return (
      <Dialog
        isOpen
        onClose={this.handleCloseAssetClassGroupErrorDialog}
        title="Warning"
        style={{
          width: '400px'
        }}
      >
        <Layout isRowType>
          <Layout.Child childId="assetClassPopup27">
            <div class="arc-message--warning">
              <strong>You are trying to create an asset class group with no asset classes</strong>
              <p>This is expected for unsecured asset class groups.Do you wish to proceed?</p>
            </div>
          </Layout.Child>
          <Layout.Child childId="assetClassPopup28">
            <hr />
            <FilterButton onClick={this.handleCloseAssetClassGroupErrorDialog} reset={false} label="No" />
            <FilterButton onClick={this.handleSubmit} reset={false} label="Yes" />
          </Layout.Child>
        </Layout>
      </Dialog>
    );
  }

  render() {
    if (this.state.showAffectedAgreements) {
      return this.renderAffectedAgreements();
    }
    if (this.state.showAssetClassGroupErrorDialog) {
      return this.renderAssetClassGroupErrorDialog();
    }
    if (this.state.addNewAssetClass) {
      return this.renderAddNewAssetClassPopup();
    }
    return this.renderData();
  }
}
