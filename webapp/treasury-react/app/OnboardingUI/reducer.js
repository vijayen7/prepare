import { combineReducers } from "redux";
import {
  DEFAULT_SEARCH_MESSAGE,
  NO_DATA_SEARCH_MESSAGE,
  UPDATE_ONBOARDING_UI_SEARCH_MESSAGE,
  RESET_ONBOARDING_UI_SEARCH_MESSAGE,
  FETCH_CALCULATOR_GROUP_DATA,
  FETCH_FINANCING_AGREEMENT_TERMS_DATA,
  FETCH_ASSET_CLASS_GROUP_DATA,
  FETCH_REPORTING_CURRENCY_DATA,
  FETCH_ACCRUAL_POSTING_DATA,
  FETCH_NETTING_GROUP_MAPPING_DATA,
  SET_BANDWIDTH_TOGGLE,
  DESTROY_BANDWIDTH_TOGGLE,
  FETCH_SIMULATE_DATA,
  SAVE_SIMULATE_DATA,
  SAVE_BANDWIDTH_GROUP,
  RESET_BANDWIDTH_GROUP_DATA,
  FETCH_ASSET_CLASS_GROUP_BY_ID,
  SET_SELECTED_DATE,
  RESET_SAVE_DATA,
  SAVE_NETTING_GROUP,
  RESET_NETTING_GROUP_RESULT,
  FETCH_CALCULATORS_FOR_CALCULATOR_GROUP,
  DELETE_FINANCING_TERM,
  RESET_DELETE_TERM_DATA,
  DELETE_ACCRUAL_POSTING,
  DELETE_CALCULATOR_GROUP,
  DELETE_REPORTING_CURRENCY,
  DELETE_BU_BUNDLE_MAPPING,
  RESET_DELETE_ACCRUAL_DATA,
  RESET_DELETE_CALCULATOR_DATA,
  RESET_DELETE_REPORTING_DATA,
  RESET_DELETE_BU_BUNDLE_DATA,
  RESET_DELETE_ADJUSTMENT_ALLOCATION_RULE,
  SAVE_ASSET_CLASS,
  FETCH_AGREEMENTS_AFFECTED_FOR_ASSET,
  RESET_SAVE_ASSET_CLASS,
  SET_DELETE_TOGGLE,
  DESTROY_DELETE_TOGGLE,
  RESET_SIMULATE_DATA,
  SET_SELECTED_ROW_DATA,
  RESET_SELECTED_ROW_DATA,
  SHOW_PRIMARY_POPUP,
  HIDE_PRIMARY_POPUP,
  SET_VIEW,
  FETCH_BU_BUNDLE_MAPPING_DATA,
  FETCH_GENEVA_BOOK_MAPPING_DATA,
  FETCH_AGREEMENT_ID,
  FETCH_SUMMARY_DATA,
  FETCH_VALID_METHODOLOGIES,
  FETCH_AGREEMENT_TERMS,
  VALIDATION_ERROR,
  SAVE_METHODOLOGY_KEY,
  FETCH_FINANCING_TERM_SUGGESTIONS,
  FETCH_ACCRUAL_POSTING_SUGGESTIONS,
  CLEAR_WIDGET,
  RESET_ASSET_CLASS_GROUP_BY_ID_DATA,
  FETCH_NETTING_GROUP_DETAIL,
  RESET_ADJUSTMENT_ALLOCATION_RULES,
  DELETE_ADJUSTMENT_ALLOCATION_RULE,
  FETCH_ADJUSTMENT_ALLOCATION_RULES,
  FETCH_RECONCILIATION_THRESHOLDS_DATA,
  SAVE_RECONCILIATION_THRESHOLDS_DATA,
  RESET_SAVE_RECONCILIATION_THRESHOLDS_DATA,
  DELETE_RECONCILIATION_THRESHOLDS_DATA,
  RESET_DELETE_RECONCILIATION_THRESHOLDS_DATA,
  IS_USER_AUTHORIZED_FOR_INTEREST_EDIT,
  IS_USER_AUTHORIZED_FOR_INTEREST_POWER_EDIT,
  DISPLAY_ADD_LEAN_SURCHARGE_POPUP,
  DISPLAY_VIEW_LEAN_SURCHARGE_POPUP,
  DESTROY_LEAN_SURCHARGE_POPUP,
  FETCH_LEAN_SURCHARGE_MODEL_BY_ID,
  SAVE_LEAN_SURCHARGE_MODEL,
  RESET_SAVE_LEAN_SURCHARGE_MODEL,
  UPDATE_LEAN_SURCHARGE_MODEL_GRID_DATA
} from "./constants";
import { onboardingScreen } from "./util";

function calculatorGroupDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_CALCULATOR_GROUP_DATA}_SUCCESS`:
      return action.data.existingList || [];
    default:
      return state;
  }
}

function financingAgreementTermsDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_FINANCING_AGREEMENT_TERMS_DATA}_SUCCESS`:
      return action.data.resultList || [];
    default:
      return state;
  }
}

function assetClassGroupDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_ASSET_CLASS_GROUP_DATA}_SUCCESS`:
      return action.data.resultList || [];
    default:
      return state;
  }
}

function assetClassGroupDataByIdReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_ASSET_CLASS_GROUP_BY_ID}_SUCCESS`:
      return action.result || {};
    case RESET_ASSET_CLASS_GROUP_BY_ID_DATA:
      return {};
    default:
      return state;
  }
}

function calculatorsForCalculatorGroupReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_CALCULATORS_FOR_CALCULATOR_GROUP}_SUCCESS`:
      return action.data || [];
    default:
      return state;
  }
}

function agreementsAffectedForAssetReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_AGREEMENTS_AFFECTED_FOR_ASSET}_SUCCESS`:
      return action.data.resultList || [];
    default:
      return state;
  }
}

function reportingCurrencyDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_REPORTING_CURRENCY_DATA}_SUCCESS`:
      return action.data.existingList || [];
    default:
      return state;
  }
}

function accrualPostingDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_ACCRUAL_POSTING_DATA}_SUCCESS`:
      return action.data.existingList || [];
    default:
      return state;
  }
}

function nettingGroupMappingDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_NETTING_GROUP_MAPPING_DATA}_SUCCESS`:
      return action.data.resultList || [];
    default:
      return state;
  }
}

function simulatedDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SIMULATE_DATA}_SUCCESS`:
      return action.data || [];
    case RESET_SIMULATE_DATA:
      return [];
    default:
      return state;
  }
}

function saveSimulatedDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_SIMULATE_DATA}_SUCCESS`:
      return action.data || {};
    case RESET_SAVE_DATA:
      return {};
    default:
      return state;
  }
}

function setSelectedRowDataReducer(state = {}, action) {
  switch (action.type) {
    case SET_SELECTED_ROW_DATA:
      return action.payload || {};
    case RESET_SELECTED_ROW_DATA:
      return {};
    default:
      return state;
  }
}

function deleteFinancingTermReducer(state = {}, action) {
  switch (action.type) {
    case `${DELETE_FINANCING_TERM}_SUCCESS`:
      return action.data || {};
    case RESET_DELETE_TERM_DATA:
      return {};
    default:
      return state;
  }
}

function deleteAccrualPostingReducer(state = {}, action) {
  switch (action.type) {
    case `${DELETE_ACCRUAL_POSTING}_SUCCESS`:
      return action.data || {};
    case RESET_DELETE_ACCRUAL_DATA:
      return {};
    default:
      return state;
  }
}

function deleteCalculatorGroupReducer(state = {}, action) {
  switch (action.type) {
    case `${DELETE_CALCULATOR_GROUP}_SUCCESS`:
      return action.data || {};
    case RESET_DELETE_CALCULATOR_DATA:
      return {};
    default:
      return state;
  }
}

function deleteReportingCurrencyReducer(state = {}, action) {
  switch (action.type) {
    case `${DELETE_REPORTING_CURRENCY}_SUCCESS`:
      return action.data || {};
    case RESET_DELETE_REPORTING_DATA:
      return {};
    default:
      return state;
  }
}

function deleteBuBundleMappingReducer(state = {}, action) {
  switch (action.type) {
    case `${DELETE_BU_BUNDLE_MAPPING}_SUCCESS`:
      return action.data || {};
    case RESET_DELETE_BU_BUNDLE_DATA:
      return {};
    default:
      return state;
  }
}

function bandwidthGroupDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_BANDWIDTH_GROUP}_SUCCESS`:
      return action.data || {};
    case RESET_BANDWIDTH_GROUP_DATA:
      return {};
    default:
      return state;
  }
}
function saveLeanSurchargeModelReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_LEAN_SURCHARGE_MODEL}_SUCCESS`:
      return action.data;
    case RESET_SAVE_LEAN_SURCHARGE_MODEL:
      return {}
    default:
      return state;
  }
}

function leanSurchargeModelDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LEAN_SURCHARGE_MODEL_BY_ID}_SUCCESS`:
      return action.data.leanSurchargeSlabs || [];
    case UPDATE_LEAN_SURCHARGE_MODEL_GRID_DATA:
      return action.payload;
    default:
      return state;
  }
}

function saveNettingGroupDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_NETTING_GROUP}_SUCCESS`:
      return action.data || {};
    case RESET_NETTING_GROUP_RESULT:
      return {};
    default:
      return state;
  }
}

function saveAssetClassDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_ASSET_CLASS}_SUCCESS`:
      return action.data || {};
    case RESET_SAVE_ASSET_CLASS:
      return {};
    default:
      return state;
  }
}

function onboardingUISearchMessageReducer(
  state = DEFAULT_SEARCH_MESSAGE,
  action
) {
  switch (action.type) {
    case UPDATE_ONBOARDING_UI_SEARCH_MESSAGE:
      return NO_DATA_SEARCH_MESSAGE;
    case RESET_ONBOARDING_UI_SEARCH_MESSAGE:
      return DEFAULT_SEARCH_MESSAGE;
    default:
      return state;
  }
}

function buBundleMappingDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_BU_BUNDLE_MAPPING_DATA}_SUCCESS`:
      return action.data.existingList || [];
    default:
      return state;
  }
}

function genevaBookMappingDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_GENEVA_BOOK_MAPPING_DATA}_SUCCESS`:
      return action.data.existingList || [];
    default:
      return state;
  }
}

function getAdjustmentAllocationRuleReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_ADJUSTMENT_ALLOCATION_RULES}_DATA_SUCCESS`:
      return action.data.existingList || {};
    default:
      return state;
  }
}

const closePopups = {
  calculator: false,
  financing: false,
  asset: false,
  reporting: false,
  accrual: false,
  netting: false,
  buBundle: false,
  genevaBook: false,
  adjustment: false
};

function togglePopUpReducer(state = closePopups, action) {
  switch (action.type) {
    case SHOW_PRIMARY_POPUP:
      switch (action.payload) {
        case onboardingScreen.CALCULATOR:
          return { ...state, calculator: true };
        case onboardingScreen.FINANCING:
          return { ...state, financing: true };
        case onboardingScreen.ASSET:
          return { ...state, asset: true };
        case onboardingScreen.REPORTING:
          return { ...state, reporting: true };
        case onboardingScreen.ACCRUAL:
          return { ...state, accrual: true };
        case onboardingScreen.NETTING:
          return { ...state, netting: true };
        case onboardingScreen.BUBUNDLE:
          return { ...state, buBundle: true };
        case onboardingScreen.GENEVABOOK:
          return { ...state, genevaBook: true };
        case onboardingScreen.ADJUSTMENT:
          return { ...state, adjustment: true };
        case onboardingScreen.RECONCILIATION_THRESHOLDS:
          return { ...state, reconciliationThresholds: true};
      }
      break;
    case HIDE_PRIMARY_POPUP:
      return closePopups;
    default:
      return state;
  }
}

function addBandwidthToggleReducer(state = false, action) {
  switch (action.type) {
    case SET_BANDWIDTH_TOGGLE:
      return true;
    case DESTROY_BANDWIDTH_TOGGLE:
      return false;
    default:
      return state;
  }
}

function deleteToggleReducer(state = false, action) {
  switch (action.type) {
    case SET_DELETE_TOGGLE:
      return true;
    case DESTROY_DELETE_TOGGLE:
      return false;
    default:
      return state;
  }
}

function toggleleanSurchargeModelPopupReducer(state = false, action) {
  switch (action.type) {
    case DISPLAY_ADD_LEAN_SURCHARGE_POPUP:
    case DISPLAY_VIEW_LEAN_SURCHARGE_POPUP:
        return true;
    case DESTROY_LEAN_SURCHARGE_POPUP:
        return false;
    default:
      return state;
  }

}
function isViewModeleanSurchargeModelPopupTypeReducer(state = true, action) {
  switch (action.type) {
    case DISPLAY_VIEW_LEAN_SURCHARGE_POPUP:
        return true;
    case DISPLAY_ADD_LEAN_SURCHARGE_POPUP:
        return false;
    default:
      return state;
  }

}

function selectedDateReducer(state = "", action) {
  switch (action.type) {
    case SET_SELECTED_DATE:
      return action.payload || "";
    default:
      return state;
  }
}

function setViewReducer(state = "CALCULATOR_GROUP", action) {
  switch (action.type) {
    case SET_VIEW:
      return action.payload || "";
    default:
      return state;
  }
}

function fetchAgreementIdReducer(state = null, action) {
  switch (action.type) {
    case `${FETCH_AGREEMENT_ID}_SUCCESS`:
      return action.data || {};
    default:
      return state;
  }
}

function fetchSummaryDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SUMMARY_DATA}_SUCCESS`:
      return action.data || [];
    default:
      return state;
  }
}

function fetchValidMethodologiesReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_VALID_METHODOLOGIES}_SUCCESS`:
      return action.data || [];
    default:
      return state;
  }
}

function fetchAgreementTermsReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_AGREEMENT_TERMS}_SUCCESS`:
      return action.data || [];
    default:
      return state;
  }
}

function saveMethodologyDataKey(state = {}, action) {
  switch (action.type) {
    case SAVE_METHODOLOGY_KEY:
      return action.methodologyDataKey || {};
    default:
      return state;
  }
}
function validateErrorReducer(state = null, action) {
  switch (action.type) {
    case VALIDATION_ERROR:
      return action.data || null;
    default:
      return state;
  }
}

function fetchFinancingTermsSuggestionsReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_FINANCING_TERM_SUGGESTIONS}_SUCCESS`:
      return action.data || [];
    case CLEAR_WIDGET:
      return [];
    default:
      return state;
  }
}

function fetchAccrualPostingSuggestionsReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_ACCRUAL_POSTING_SUGGESTIONS}_SUCCESS`:
      return action.data || {};
    case CLEAR_WIDGET:
      return [];
    default:
      return state;
  }
}

function fetchNettingGroupDetailReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_NETTING_GROUP_DETAIL}_SUCCESS`:
      return action.data.hashMap || {};
    case CLEAR_WIDGET:
      return [];
    default:
      return state;
  }
}

function deleteAdjustmentAllocationRuleReducer(state = {}, action) {
  switch (action.type) {
    case `${DELETE_ADJUSTMENT_ALLOCATION_RULE}_SUCCESS`:
      return action.data || {};
    case RESET_DELETE_ADJUSTMENT_ALLOCATION_RULE:
      return [];
    default:
      return state;
    }
  }

function reconciliationThresholdsReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_RECONCILIATION_THRESHOLDS_DATA}_SUCCESS`:
      return action.data || [];
    default:
      return state;
  }
}


function saveReconciliationThresholdsDataReducer(state = null, action) {
  switch (action.type) {
    case `${SAVE_RECONCILIATION_THRESHOLDS_DATA}_SUCCESS`:
      return true;
    case `${SAVE_RECONCILIATION_THRESHOLDS_DATA}_FAILURE`:
      return false;
    case RESET_SAVE_RECONCILIATION_THRESHOLDS_DATA:
      return null;
    default:
      return state;
  }
}

function deleteReconciliationThresholdsDataReducer(state = null, action) {
  switch (action.type) {
    case `${DELETE_RECONCILIATION_THRESHOLDS_DATA}_SUCCESS`:
      return true;
    case `${DELETE_RECONCILIATION_THRESHOLDS_DATA}_FAILURE`:
      return false;
    case RESET_DELETE_RECONCILIATION_THRESHOLDS_DATA:
      return null;
    default:
      return state;
  }
}

function fetchInterestEditUserAuth(state = true, action) {
  switch (action.type) {
    case `${IS_USER_AUTHORIZED_FOR_INTEREST_EDIT}_SUCCESS`:
      return action.data || true;
    default:
      return state;
  }
}

function fetchInterestPowerEditUserAuth(state = true, action) {
  switch (action.type) {
    case `${IS_USER_AUTHORIZED_FOR_INTEREST_POWER_EDIT}_SUCCESS`:
      return action.data || true;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  calculatorGroupData: calculatorGroupDataReducer,
  financingAgreementTermsData: financingAgreementTermsDataReducer,
  onboardingUISearchMessage: onboardingUISearchMessageReducer,
  assetClassGroupData: assetClassGroupDataReducer,
  assetClassGroupDataById: assetClassGroupDataByIdReducer,
  reportingCurrencyData: reportingCurrencyDataReducer,
  accrualPostingData: accrualPostingDataReducer,
  nettingGroupMappingData: nettingGroupMappingDataReducer,
  isPrimaryPopUpOpen: togglePopUpReducer,
  isAddBandwidthClicked: addBandwidthToggleReducer,
  isLeanSurchargeModelPopupOpen : toggleleanSurchargeModelPopupReducer,
  isLeanSurchargeModelViewMode: isViewModeleanSurchargeModelPopupTypeReducer,
  isDeleteClicked: deleteToggleReducer,
  simulatedData: simulatedDataReducer,
  saveSimulatedData: saveSimulatedDataReducer,
  bandwidthGroupData: bandwidthGroupDataReducer,
  leanSurchargeModelData: leanSurchargeModelDataReducer,
  isSavedLeanSurchargeModel: saveLeanSurchargeModelReducer,
  addNettingGroupResult: saveNettingGroupDataReducer,
  selectedDate: selectedDateReducer,
  calculatorsForCalculatorGroup: calculatorsForCalculatorGroupReducer,
  agreementsAffectedForAsset: agreementsAffectedForAssetReducer,
  deleteFinancingTermData: deleteFinancingTermReducer,
  deleteAccrualPostingData: deleteAccrualPostingReducer,
  deleteCalculatorGroupData: deleteCalculatorGroupReducer,
  deleteReportingCurrencyData: deleteReportingCurrencyReducer,
  deleteBuBundleMappingData: deleteBuBundleMappingReducer,
  saveAssetClassData: saveAssetClassDataReducer,
  selectedRowData: setSelectedRowDataReducer,
  view: setViewReducer,
  buBundleMappingData: buBundleMappingDataReducer,
  genevaBookMappingData: genevaBookMappingDataReducer,
  selectedAgreementId: fetchAgreementIdReducer,
  validationError: validateErrorReducer,
  summaryData: fetchSummaryDataReducer,
  validMethodologies: fetchValidMethodologiesReducer,
  agreementTermsForMethodology: fetchAgreementTermsReducer,
  methodologyDataKey: saveMethodologyDataKey,
  financingTermsSuggestions: fetchFinancingTermsSuggestionsReducer,
  accrualPostingSuggestions: fetchAccrualPostingSuggestionsReducer,
  nettingGroupDetail: fetchNettingGroupDetailReducer,
  reconciliationThresholdsData: reconciliationThresholdsReducer,
  saveReconciliationThresholdsData: saveReconciliationThresholdsDataReducer,
  deleteReconciliationThresholdsData: deleteReconciliationThresholdsDataReducer,
  deleteAdjustmentAllocationRuleData: deleteAdjustmentAllocationRuleReducer,
  adjustmentAllocationRuleData: getAdjustmentAllocationRuleReducer,
  isUserAuthorizedForInterestPowerEdit: fetchInterestPowerEditUserAuth,
  isUserAuthorizedForInterestEdit: fetchInterestEditUserAuth
});

export default rootReducer;
