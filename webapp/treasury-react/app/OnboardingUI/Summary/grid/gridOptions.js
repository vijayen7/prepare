let options = {
  applyFilteringOnGrid: true,
  showHeaderRow: true,
  exportToExcel: true,
  forceFitColumns: true,
  sheetName: "Financing_Summary_View"
};

export default options;
