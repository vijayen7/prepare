let columns = [
  {
    id: "agreementType",
    name: "Agreement Type",
    field: "agreementType",
    toolTip: "Agreement Type",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.00",
    width: 90
  },
  {
    id: "legalEntity",
    name: "Legal Entity",
    field: "legalEntity",
    toolTip: "Legal Entity",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.00",
    width: 90
  },
  {
    id: "cpe",
    name: "CPE",
    field: "cpe",
    toolTip: "CPE",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.00",
    width: 90
  },
  {
    id: "currency",
    name: "Currency",
    field: "currency",
    toolTip: "Currency",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.00",
    width: 90
  },
  {
    id: "nettingGroup",
    name: "Netting Group",
    field: "nettingGroup",
    toolTip: "Netting Group",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.00",
    width: 90
  },
  {
    id: "errorType",
    name: "Error Type",
    field: "errorType",
    toolTip: "Error Type",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.00",
    width: 90
  },
  {
    id: "comment",
    name: "Comment",
    field: "comment",
    toolTip: "Comment",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.00",
    width: 90
  }
];

export default columns;
