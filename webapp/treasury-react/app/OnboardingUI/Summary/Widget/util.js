import ErrorDismissible from "commons/components/ErrorDismissible";
import React, { Component } from "react";

export const stages = [
  "CALCULATOR_GROUP",
  "FINANCING_AGREEMENT_TERMS",
  "BU_BUNDLE_MAPPING",
  "ACCRUAL_POSTING",
  "GENEVA_BOOK_MAPPING",
  "REPORTING_CURRENCY",
  "SUMMARY"
];

export const status = {
  SUCCESS: "icon-success",
  FAIL: "icon-blocked",
  LOADING: "icon-loading"
};

export function createAccountingBookRule(rule) {
  if (rule === undefined)
    return null
  else {
    return {
      '@CLASS': 'com.arcesium.treasury.financing.validator.AccountingBookRule',
      'legalEntityId': rule[0].columnValues[1],
      'currencyId': rule[0].columnValues[2],
      'cpeId':rule[0].columnValues[3],
      'agreementTypeId':rule[0].columnValues[4],
      'nettingGroupId':rule[0].columnValues[5],
      'bookId': rule[0].columnValues[6]
    }
  }
}

export function createAccrualPostingRuleMap(rule) {
  console.log(rule);
  if (rule === undefined)
    return null
  else {
    if (rule[0].columnValues[1] == "") {
      return { 0: rule[0].columnValues[9] };
    }
    else {
      let ruleMap = {};
      ruleMap[rule[0].columnValues[1]] = rule[0].columnValues[9];
      return ruleMap;
    }
  }
}

export function getValidationError(errors) {
  if (Array.isArray(errors)) {
    return errors.map(error => <ErrorDismissible messageData={error.errorMessage + " : " + error.comment} />);
  }
  else {
    return <ErrorDismissible messageData={errors.errorMessage + " : " + errors.comment} />
  }
}
