import React, { Component } from "react";
import { Layout, Card, Dialog as ArcDialog } from "arc-react-components";
import Status from "./components/Status";
import WidgetFooter from "./components/WidgetFooter";
import ErrorDismissible from "commons/components/ErrorDismissible";
import OnboardingStage from "./components/OnboardingStage";
import FinancingTermsSuggestions from "./components/FinancingTermsSuggestions";
import AccrualPostingSuggestions from "./components/AccrualPostingSuggestions";
import { onboardingScreen } from "../../util";
import {
  stages,
  createAccountingBookRule,
  createAccrualPostingRuleMap,
  getValidationError,
} from "./util";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getSuggestionsForFinancingTerms,
  validateCalculatorGroup,
  validateAgreementTerms,
  validateAccrualPosting,
  validateAccountingBook,
  fetchFinancingTermsSuggestions,
  fetchAccrualPostingSuggestions,
  clearPopUp,
} from "../../actions";

class Dialog extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState(this.getDefaultState);
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.simulatedData != prevProps.simulatedData &&
      stages !== undefined &&
      !(stages[this.state.stage] in this.state.selectedStageData) &&
      "existingList" in this.props.simulatedData &&
      this.props.simulatedData["existingList"].length > 0
    ) {
      let ruleList = this.props.simulatedData["existingList"];
      this.saveAndValidateWidgetView(ruleList, stages[this.state.stage], false);
    }
  }

  next = (e) => {
    if (stages[this.state.stage + 1] == onboardingScreen.FINANCING) {
      this.props.fetchFinancingTermsSuggestions(
        this.state.selectedStageData[onboardingScreen.CALCULATOR][0]
          .columnValues[10]
      );
    }

    if (stages[this.state.stage + 1] == onboardingScreen.ACCRUAL) {
      this.props.fetchAccrualPostingSuggestions(
        this.state.selectedStageData[onboardingScreen.CALCULATOR][0]
          .columnValues[10]
      );
    }
    this.setState({ stage: this.state.stage + 1 });
  };

  back = (e) => {
    this.setState({ stage: this.state.stage - 1 });
  };

  getDefaultState = () => {
    return {
      stage: 0,
      selectedStageData: {},
      newStageData: {},
    };
  };

  onClose = () => {
    this.props.closeDialog();
    this.props.clearPopUp();
    this.setState(this.getDefaultState);
  };

  validateWidgetView = (view) => {
    switch (view) {
      case onboardingScreen.CALCULATOR:
        this.props.validateCalculatorGroup(
          this.props.applicableAgreementTerms,
          this.state.selectedStageData[onboardingScreen.CALCULATOR][0]
            .columnValues[10]
        );
        break;
      case onboardingScreen.FINANCING:
        this.props.validateAgreementTerms(
          this.props.applicableAgreementTerms,
          this.props.methodologyDataKey,
          this.state.selectedStageData[onboardingScreen.FINANCING],
          this.state.selectedStageData[onboardingScreen.CALCULATOR][0]
            .columnValues[10]
        );
        break;
      case onboardingScreen.ACCRUAL:
        this.props.validateAccrualPosting(
          this.props.methodologyDataKey,
          this.state.selectedStageData[onboardingScreen.CALCULATOR][0]
            .columnValues[10],
          createAccrualPostingRuleMap(
            this.state.selectedStageData[onboardingScreen.ACCRUAL]
          )
        );
        break;
      case onboardingScreen.GENEVABOOK:
        this.props.validateAccountingBook(
          this.props.methodologyDataKey,
          createAccountingBookRule(
            this.state.selectedStageData[onboardingScreen.GENEVABOOK]
          )
        );
        break;
      case onboardingScreen.SUMMARY:
        break;
      default:
        break;
    }
  };

  fetchSuggestions = (view) => {
    switch (view) {
      case onboardingScreen.FINANCING:
        this.props.fetchFinancingTermsSuggestions(
          this.state.selectedStageData[onboardingScreen.CALCULATOR][0]
            .columnValues[10]
        );
        break;
      case onboardingScreen.ACCRUAL:
        this.props.fetchAccrualPostingSuggestions(
          this.state.selectedStageData[onboardingScreen.CALCULATOR][0]
            .columnValues[10]
        );
        break;
      default:
        break;
    }
  };

  saveWidgetView = (view, selectedData, newData) => {
    const selectedStageData = Object.assign({}, this.state.selectedStageData);
    selectedStageData[view] = selectedData;
    if (!newData) {
      this.setState({ selectedStageData }, () => this.validateWidgetView(view));
    } else {
      let newStageData = Object.assign({}, this.state.newStageData);
      newStageData[view] = selectedData;
      this.setState({ selectedStageData, newStageData }, () =>
        this.validateWidgetView(view)
      );
    }
  };

  saveAndValidateWidgetView = (selectedData, view, newData) => {
    this.saveWidgetView(view, selectedData, newData);
  };

  render() {
    return (
      <ArcDialog
        isOpen={this.props.openWidget}
        title={"Onboarding Widget"}
        onClose={this.onClose}
        style={{
          width: "75%",
          minHeight: "100%",
        }}
        footer={
          <WidgetFooter
            stage={this.state.stage}
            next={this.next}
            back={this.back}
          />
        }
      >
        <Layout isRowType={true}>
          <Layout.Child
            className="padding"
            size={"content"}
            childId="OnboardingDialog1"
          >
            {this.props.validationError == null
              ? null
              : getValidationError(this.props.validationError)}
          </Layout.Child>
          <Layout.Child childId="OnboardingDialog2">
            <Layout isColumnType={true}>
              <Layout.Child
                size={3}
                className="padding--horizontal"
                childId="OnboardingDialog3"
              >
                <Status stage={this.state.stage} />
              </Layout.Child>
              <Layout.Child size={10} childId="OnboardingDialog4">
                <OnboardingStage
                  stage={this.state.stage}
                  selectedData={this.props.selectedData}
                  saveAndValidateWidgetView={this.saveAndValidateWidgetView}
                  savedStageData={this.state.newStageData}
                  selectedStageData={this.state.selectedStageData}
                />
              </Layout.Child>
              <Layout.Child
                childId="OnboardingDialog5"
                size={3}
                className="padding--horizontal"
              >
                <FinancingTermsSuggestions
                  suggestions={this.props.financingTermsSuggestions}
                />
                <AccrualPostingSuggestions
                  suggestions={this.props.accrualPostingSuggestions}
                />
              </Layout.Child>
            </Layout>
          </Layout.Child>
        </Layout>
      </ArcDialog>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getSuggestionsForFinancingTerms,
      validateCalculatorGroup,
      validateAgreementTerms,
      validateAccrualPosting,
      validateAccountingBook,
      fetchFinancingTermsSuggestions,
      fetchAccrualPostingSuggestions,
      clearPopUp,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    validationError: state.financingOnboarding.validationError,
    applicableAgreementTerms:
      state.financingOnboarding.agreementTermsForMethodology,
    simulatedData: state.financingOnboarding.simulatedData,
    methodologyDataKey: state.financingOnboarding.methodologyDataKey,
    financingTermsSuggestions:
      state.financingOnboarding.financingTermsSuggestions,
    accrualPostingSuggestions:
      state.financingOnboarding.accrualPostingSuggestions,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dialog);
