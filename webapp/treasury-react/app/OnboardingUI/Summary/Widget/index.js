import React, { Component } from "react";
import { Layout } from "arc-react-components";
import SearchFilter from "./SearchFilter";
import Dialog from "./Dialog";

class Widget extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openWidget: false,
            selectedData: {}
        };
    }

    openDialog = selectedData => {
        this.setState({
            openWidget: true,
            selectedData
        });
    };

    closeDialog = () => {
        this.setState({
            openWidget: false
        });
    };

    render() {
        return (
            <React.Fragment>
                <Dialog
                    selectedData={this.state.selectedData}
                    openWidget={this.state.openWidget}
                    closeDialog={this.closeDialog}
                />
                <Layout>
                    <Layout.Child childId="OnboardingIndex1" size="Fit">
                        <SearchFilter openDialog={this.openDialog} />
                    </Layout.Child>
                </Layout>
            </React.Fragment>
        );
    }
}

export default Widget;
