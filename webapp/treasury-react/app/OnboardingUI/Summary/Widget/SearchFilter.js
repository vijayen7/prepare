import React, { Component } from "react";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import AgreementTypeSpecificNettingGroupFilter from "commons/container/AgreementTypeSpecificNettingGroupFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import FilterButton from "commons/components/FilterButton";
import ErrorDismissible from "commons/components/ErrorDismissible";
import { Layout } from "arc-react-components";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchFilter } from "../../../commons/actions/filters";
import {
  fetchAgreementId,
  fetchSummaryData,
  fetchAgreementTerms,
  saveMethodologyKey,
  fetchSearchData,

} from "../../actions";
import { AGREEMENT_FILTER } from "../../../commons/constants";
import { getValueForSingleSelect, getValueForSingleSelectOrNull, onboardingScreen } from "../../util";
import { getFirstDateOfCurrentMonth } from "commons/util";

class SearchFilter extends Component {
  constructor(props) {
    super(props);
    this.state = this.getDefaultFilters();
  }

  componentDidMount() {
    this.props.fetchFilter({ key: AGREEMENT_FILTER });
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.agreements.length > 0 &&
      nextProps.selectedAgreement != null &&
      Object.keys(nextProps.selectedAgreement).length !== 0
    ) {
      let selectedAgreementFilter = null;
      for (var agreement of nextProps.agreements) {
        if (agreement.key === nextProps.selectedAgreement.agreementId) {
          selectedAgreementFilter = agreement;
        }
      }
      this.setState({ selectedAgreements: selectedAgreementFilter }, () => {
        if (!this.state.summaryView)
          this.props.openDialog(this.state);
      });
    }
  }

  onSelect = params => {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  };

  getDefaultFilters = () => {
    return {
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedAgreementTypes: [],
      selectedNettingGroups: [],
      selectedCurrencies: [],
      selectedAgreements: [],
      summaryView: true,
    };
  };

  getParamsForAgreementTerms = () => {
    let params = {};
    params["validFrom"] = getFirstDateOfCurrentMonth();
    params["simulation"] = 0;
    params["legalEntityIds"] = [this.state.selectedLegalEntities.key];
    params["fundingTypeIds"] = [this.state.selectedAgreementTypes.key];
    params["cpeIds"] = [this.state.selectedCpes.key];
    params["nettingGroupIds"] = [this.state.selectedNettingGroups.key];
    params["currencyIds"] = [this.state.selectedCurrencies.key];
    return params;
  }

  getParamsForAccualPostingRules = () => {
    let payload = {
      "ruleSystemFilter.legalEntityIds": [this.state.selectedLegalEntities.key],
      "ruleSystemFilter.cpeIds": [this.state.selectedCpes.key],
      "ruleSystemFilter.agreementTypeIds": [this.state.selectedAgreementTypes.key],
      "ruleSystemFilter.nettingGroupIds": [this.state.selectedNettingGroups.key]
      ,
      "ruleSystemFilter.currencyIds": [this.state.selectedCurrencies.key],
      "ruleSystemFilter.startDate": getFirstDateOfCurrentMonth()
    };
    return payload;
  }

  openWidget = () => {
    this.setState({ summaryView: false }, () => {
      let payload = {
        legalEntityId: getValueForSingleSelectOrNull(this.state.selectedLegalEntities),
        cpeId: getValueForSingleSelectOrNull(this.state.selectedCpes),
        agreementTypeId: getValueForSingleSelectOrNull(this.state.selectedAgreementTypes)
      };

      this.props.fetchAgreementTerms(this.getParamsForAgreementTerms());
      this.props.fetchSearchData(this.getParamsForAccualPostingRules(), onboardingScreen.ACCRUAL);
      this.props.fetchAgreementId(payload);
      this.props.saveMethodologyKey(this.getParamsForAgreementTerms());
    });
  }

  render() {
    let selectedAgreementId = getValueForSingleSelect(
      this.state.selectedAgreementTypes
    );
    return (
      <Layout>
        <Layout.Child size="Fit" childId="OnboardingSearchFilter1">
          {this.props.selectedAgreement != null && Object.keys(this.props.selectedAgreement).length == 0 ?
            <ErrorDismissible messageData="Select a valid Agreement" dismissible /> : null}
        </Layout.Child>
        <Layout.Child childId="OnboardingSearchFilter2">
          <Layout isColumnType={true}>
            <Layout.Child childId="OnboardingSearchFilter3">
              <AgreementTypeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedAgreementTypes}
                multiSelect={false}
              />
            </Layout.Child>
            <Layout.Child childId="OnboardingSearchFilter4">
              <LegalEntityFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedLegalEntities}
                multiSelect={false}
              />
            </Layout.Child>
            <Layout.Child childId="OnboardingSearchFilter5">
              <CpeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCpes}
                multiSelect={false}
              />
            </Layout.Child>
            <Layout.Child childId="OnboardingSearchFilter6">
              <AgreementTypeSpecificNettingGroupFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedNettingGroups}
                agreementTypeFilterIds={selectedAgreementId}
                multiSelect={false}
              />
            </Layout.Child>
            <Layout.Child childId="OnboardingSearchFilter7">
              <CurrencyFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCurrencies}
                multiSelect={false}
              />
              <FilterButton
                onClick={this.openWidget}
                reset={false}
                label="Open Widget"
              />
            </Layout.Child>
          </Layout>
        </Layout.Child>
      </Layout>)
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchFilter,
      fetchAgreementId,
      fetchAgreementTerms,
      saveMethodologyKey,
      fetchSummaryData,
      fetchSearchData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    selectedAgreement: state.financingOnboarding.selectedAgreementId,
    agreements: state.filters.agreementIds
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchFilter);
