import React, { Component } from "react";
import CalculatorGroupPopUpContent from "../../../popup/container/CalculatorGroupPopUpContent";
import AccrualPostingPopUpContent from "../../../popup/container/AccrualPostingPopUpContent";
import FinancingTermsPopUpContent from "../../../popup/container/FinancingTermsPopUpContent";
import ReportingCurrencyPopUpContent from "../../../popup/container/ReportingCurrencyPopUpContent";
import BuBundleMappingPopUpContent from "../../../popup/container/BuBundleMappingPopUpContent";
import GenevaBookMappingPopUpContent from "../../../popup/container/GenevaBookMappingPopUpContent";
import SummaryStagePopUpContent from "./SummaryStagePopUpContent";
import { stages } from "../util";

class OnboardingStage extends Component {
  constructor(props) {
    super(props);
  }

  renderStage = () => {
    switch (this.props.stage) {
      case 0:
        return (
          <CalculatorGroupPopUpContent
            widgetView={true}
            selectedData={this.props.selectedData}
            saveAndValidateWidgetView={this.props.saveAndValidateWidgetView}
            savedStageData={this.props.savedStageData[stages[0]]}
          />
        );
      case 1:
        return (
          <FinancingTermsPopUpContent
            widgetView={true}
            selectedData={this.props.selectedData}
            saveAndValidateWidgetView={this.props.saveAndValidateWidgetView}
            savedStageData={this.props.savedStageData[stages[0]]}
          />
        );
      case 2:
        return (
          <BuBundleMappingPopUpContent
            widgetView={true}
            selectedData={this.props.selectedData}
            saveAndValidateWidgetView={this.props.saveAndValidateWidgetView}
            savedStageData={this.props.savedStageData[stages[0]]}
          />
        );
      case 3:
        return (
          <AccrualPostingPopUpContent
            widgetView={true}
            selectedData={this.props.selectedData}
            saveAndValidateWidgetView={this.props.saveAndValidateWidgetView}
            savedStageData={this.props.savedStageData[stages[0]]}
          />
        );
      case 4:
        return (
          <GenevaBookMappingPopUpContent
            widgetView={true}
            selectedData={this.props.selectedData}
            saveAndValidateWidgetView={this.props.saveAndValidateWidgetView}
            savedStageData={this.props.savedStageData[stages[0]]}
          />
        );
      case 5:
        return (
          <ReportingCurrencyPopUpContent
            widgetView={true}
            selectedData={this.props.selectedData}
            saveAndValidateWidgetView={this.props.saveAndValidateWidgetView}
            savedStageData={this.props.savedStageData[stages[0]]}
          />
        );
      case 6:
        return <SummaryStagePopUpContent
          savedStageData={this.props.savedStageData}
          selectedStageData={this.props.selectedStageData} />;
      default:
        return (
          <CalculatorGroupPopUpContent
            widgetView={true}
            selectedData={this.props.selectedData}
            saveAndValidateWidgetView={this.props.saveAndValidateWidgetView}
            savedStageData={this.props.savedStageData[stages[0]]}
          />
        );
    }
  };

  render() {
    return <React.Fragment>{this.renderStage()}</React.Fragment>;
  }
}

export default OnboardingStage;
