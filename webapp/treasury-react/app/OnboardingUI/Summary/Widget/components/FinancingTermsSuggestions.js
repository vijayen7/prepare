import React, { Component } from "react";
import { Card } from "arc-react-components";

class FinancingTermsSuggestions extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      this.props.suggestions !== undefined && this.props.suggestions.length > 0 ?
        <React.Fragment>
          FINANCING TERMS SUGGESTIONS
        {this.props.suggestions.map((suggestion, i) => (
            <Card key={i} className="margin--vertical">
              {suggestion[1]}
            </Card>
          ))}
        </React.Fragment> : null
    );
  }
}

export default FinancingTermsSuggestions;
