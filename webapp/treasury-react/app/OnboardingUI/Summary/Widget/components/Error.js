import React, { Component } from "react";
import { LoadingContent, Card, Icon, Panel } from "arc-react-components";

class Error extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <LoadingContent />;
  }
}

export default Error;
