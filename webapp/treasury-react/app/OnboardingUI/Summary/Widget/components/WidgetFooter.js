import React, { Component } from "react";
import { stages } from "./../util";
class WidgetFooter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let back =
      stages[this.props.stage] !== "CALCULATOR" ? (
        <React.Fragment>
          <i className="icon-go--back" onClick={this.props.back} />
          Back
        </React.Fragment>
      ) : null;

    let next =
      stages[this.props.stage] !== "SUMMARY" ? (
        <React.Fragment>
          Next
          <i className="icon-go--forward" onClick={this.props.next} />
        </React.Fragment>
      ) : null;
    return (
      <React.Fragment>
        <div style={{ float: "left" }}>{back}</div>
        <div style={{ display: "inline-block" }}>{this.props.stage + 1}/7</div>
        <div style={{ float: "right" }}>{next}</div>
      </React.Fragment>
    );
  }
}

export default WidgetFooter;
