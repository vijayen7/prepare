import React, { Component } from 'react';
import Grid from "commons/components/Grid";
import FilterButton from 'commons/components/FilterButton';
import ColumnLayout from 'commons/components/ColumnLayout';
import {
    getGridOptions
} from "../../../grid/gridOptions";
import {
    getSimulatedColumnConfig
} from "../../../grid/columnConfig";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { validateWidget, saveSimulateData } from "../../../actions";
import { onboardingScreen } from "../../../util"
import { createAccountingBookRule, createAccrualPostingRuleMap } from "./../util";

class SummaryStagePopUpContent extends Component {
    constructor(props) {
        super(props);
    }

    handleSave = () => {
        Object.keys(this.props.savedStageData).forEach(
            view => {
                if (view != onboardingScreen.FINANCING) {
                    let payload = this.props.savedStageData[view].map(row => row.columnValues);
                    this.props.saveSimulateData(payload, view);
                }
                else {
                    let payload = this.props.savedStageData[view]
                    this.props.saveSimulateData(payload, view);
                }
            }
        )
    }

    handleValidate = () => {
        this.props.validateWidget(this.props.methodologyDataKey, this.props.applicableAgreementTerms,
            this.props.savedStageData[onboardingScreen.FINANCING] == undefined ?
                null : this.props.savedStageData[onboardingScreen.FINANCING],
            this.props.selectedStageData[onboardingScreen.CALCULATOR][0].columnValues[10],
            createAccountingBookRule(this.props.selectedStageData[onboardingScreen.GENEVABOOK]),
            createAccrualPostingRuleMap(this.props.savedStageData[onboardingScreen.ACCRUAL]));
    }

    render() {
        let views = Object.keys(this.props.savedStageData);
        return <React.Fragment>
            {views.map(view =>
                <Grid
                    data={this.props.savedStageData[view]}
                    gridId={view}
                    gridColumns={getSimulatedColumnConfig(view)}
                    gridOptions={getGridOptions()}
                    label={<div className="text-align--left">{view}</div>}
                />)}
            <ColumnLayout>
                <FilterButton
                    onClick={this.handleValidate}
                    label={'Validate'}
                    className="margin--small margin--right button--primary"
                />
                <FilterButton
                    onClick={this.handleSave}
                    label={'Save'}
                    disabled={this.props.validationError == null ? false : true}
                    className="margin--small margin--right button--primary"
                />
            </ColumnLayout>
        </React.Fragment>
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            validateWidget,
            saveSimulateData
        },
        dispatch
    );
}

function mapStateToProps(state) {
    return {
        validationError: state.financingOnboarding.validationError,
        applicableAgreementTerms: state.financingOnboarding.agreementTermsForMethodology,
        methodologyDataKey: state.financingOnboarding.methodologyDataKey
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SummaryStagePopUpContent);

