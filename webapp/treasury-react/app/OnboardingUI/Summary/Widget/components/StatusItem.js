import React, { Component } from "react";
import { Card } from "arc-react-components";

class StatusItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Card className={this.props.active ? "primary" : null}>
        {this.props.stageName}
      </Card>
    );
  }
}

export default StatusItem;
