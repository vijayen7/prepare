import React, { Component } from "react";
import { Card } from "arc-react-components";
import StatusItem from "./StatusItem";

class Status extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props.status);
    return (
      <Card>
        <StatusItem
          active={this.props.stage == 0 ? true : false}
          stageName="Calculator Group"
        />
        <StatusItem
          active={this.props.stage == 1 ? true : false}
          stageName="Agreement Terms"
        />
        <StatusItem
          active={this.props.stage == 2 ? true : false}
          stageName="Bu Bundle Mapping"
        />
        <StatusItem
          active={this.props.stage == 3 ? true : false}
          stageName="Accrual Posting"
        />
        <StatusItem
          active={this.props.stage == 4 ? true : false}
          stageName="Accounting Book Mapping"
        />
        <StatusItem
          active={this.props.stage == 5 ? true : false}
          stageName="Reporting Currency"
        />
        <StatusItem
          active={this.props.stage == 6 ? true : false}
          stageName="Summary"
        />
      </Card>
    );
  }
}

export default Status;
