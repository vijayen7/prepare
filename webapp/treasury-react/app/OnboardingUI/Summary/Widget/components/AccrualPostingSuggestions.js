import React, { Component } from "react";
import { Card } from "arc-react-components";

class AccrualPostingSuggestions extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            this.props.suggestions !== undefined && Object.keys(this.props.suggestions).length > 0 ?
                <React.Fragment>
                    ACCRUAL POSTING SUGGESTIONS
                {Object.keys(this.props.suggestions).map((key, i) => (
                        <Card key={i} className="margin--vertical">
                            {key}
                            {this.props.suggestions[key].map((suggestion, i) => (<Card key={i} className="margin--vertical">{suggestion}</Card>))}
                        </Card>
                    ))}
                </React.Fragment> : null
        );
    }
}

export default AccrualPostingSuggestions;
