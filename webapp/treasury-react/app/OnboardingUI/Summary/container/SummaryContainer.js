import React, { Component } from "react";
import { connect } from "react-redux";
import Stats from "./../components/Stats";
import { formatStatsData, formatData } from "./../../util";

class SummaryContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let statsData = formatStatsData(
      this.props.validMethodologies,
      this.props.summaryData
    );
    let gridData = {};
    if (
      this.props.summaryData !== undefined &&
      this.props.summaryData.length > 0
    ) {
      gridData = formatData(this.props.summaryData);
    }
    return <Stats statsData={statsData} gridData={gridData} summaryData = {this.props.summaryData} />;
  }
}

function mapStateToProps(state) {
  return {
    summaryData: state.financingOnboarding.summaryData,
    validMethodologies: state.financingOnboarding.validMethodologies
  };
}

export default connect(mapStateToProps, null)(SummaryContainer);
