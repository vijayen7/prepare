import React, { Component } from "react";
import DateFilter from "commons/container/DateFilter";
import FilterButton from "commons/components/FilterButton";
import Message from "commons/components/Message";
import { Layout, Card } from "arc-react-components";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchFilter } from "../../../commons/actions/filters";
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import {
    fetchSummaryData,
    fetchValidMethodologies,
    fetchSearchData,
    runSimulation

} from "../../actions";
import { AGREEMENT_FILTER } from "../../../commons/constants";
import { getPreviousDate } from "commons/util";

class SearchFilter extends Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultFilters();
    }

    componentDidMount() {
        this.props.fetchValidMethodologies(this.state);
        this.props.fetchSummaryData(this.state);
    }

    onSelect = params => {
        const { key, value } = params;
        const newState = Object.assign({}, this.state);
        newState[key] = value;
        this.setState(newState);
    };

    getDefaultFilters = () => {
        return {
            selectedDate: getPreviousDate(),
            selectedAgreementTypes: [],
            summaryView: true,
            simulationMessage: false
        };
    };

    searchSummary = () => {
        this.setState({ summaryView: true }, () => {
            this.props.fetchFilter({ key: AGREEMENT_FILTER });
            this.props.fetchValidMethodologies(this.state);
            this.props.fetchSummaryData(this.state);
        });

    };


    runSimulation = () => {
        this.setState({ simulationMessage: true })
        this.props.runSimulation(this.state);
        setTimeout(() => {
            this.setState({ simulationMessage: false })
        }, 5000)
    }

    render() {
        return (
          <Layout isColumnType>
            <Layout.Child childId="OnboardingSummarySearchFilter1"></Layout.Child>
            <Layout.Child childId="OnboardingSummarySearchFilter2">
              <Card className="margin--top">
                <Layout isColumnType>
                  <Layout.Child childId="OnboardingSummaryDate" className="margin--top">
                    <DateFilter
                      layout="standard"
                      onSelect={this.onSelect}
                      stateKey="selectedDate"
                      data={this.state.selectedDate}
                      label="Date"
                    />
                  </Layout.Child>
                  <Layout.Child childId="OnboardingSummaryAgreement" className="margin--left--large">
                    <AgreementTypeFilter
                      onSelect={this.onSelect}
                      selectedData={this.state.selectedAgreementTypes}
                      multiSelect={false}
                    />
                  </Layout.Child>
                </Layout>
                <FilterButton onClick={this.searchSummary} reset={false} label="Search Summary" />
                <FilterButton onClick={this.runSimulation} reset={false} label="Run Simulation" />
              </Card>
            </Layout.Child>
            <Layout.Child childId="OnboardingSummarySearchFilter3">
              {this.state.simulationMessage ? (
                <Message messageData="Running simulation in the background" />
              ) : null}
            </Layout.Child>
          </Layout>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            fetchSummaryData,
            fetchValidMethodologies,
            fetchSearchData,
            runSimulation,
            fetchFilter
        },
        dispatch
    );
}

export default connect(null, mapDispatchToProps)(SearchFilter);
