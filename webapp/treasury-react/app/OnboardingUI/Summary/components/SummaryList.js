import React from "react";
import { Layout } from "arc-react-components";
import { ReactArcGrid } from "arc-grid";
import options from "./../grid/gridOptions";
import columns from "./../grid/columnConfig";
import { formatData, onboardingScreen } from "./../../util";

const SummaryList = props => {
  if (props.summaryData !== undefined && props.summaryData.length > 0) {
    let formattedData = formatData(props.summaryData);
    return (
      <Layout>
        <Layout.Child childId="OnboardingSummaryList1">
          <Layout isColumnType={true}>
            <Layout.Child className="padding" childId="OnboardingSummaryList2">
              <ReactArcGrid
                gridId="calculator-group"
                data={formattedData[onboardingScreen["CALCULATOR"]]}
                columns={columns}
                options={options}
              />
            </Layout.Child>
            <Layout.Child childId="OnboardingSummaryList3" className="padding">
              <ReactArcGrid
                gridId="agreement-terms"
                data={formattedData[onboardingScreen["FINANCING"]]}
                columns={columns}
                options={options}
              />
            </Layout.Child>
            <Layout.Child childId="OnboardingSummaryList4" className="padding">
              <ReactArcGrid
                gridId="accrual-posting"
                data={formattedData[onboardingScreen["ACCRUAL"]]}
                columns={columns}
                options={options}
              />
            </Layout.Child>
          </Layout>
        </Layout.Child>
        <Layout.Child childId="OnboardingSummaryList5">
          <Layout isColumnType={true}>
            <Layout.Child childId="OnboardingSummaryList6" className="padding">
              <ReactArcGrid
                gridId="netting-group"
                data={formattedData[onboardingScreen["NETTING"]]}
                columns={columns}
                options={options}
              />
            </Layout.Child>
            <Layout.Child childId="OnboardingSummaryList7" className="padding">
              <ReactArcGrid
                gridId="accounting-book"
                data={formattedData[onboardingScreen["GENEVABOOK"]]}
                columns={columns}
                options={options}
              />
            </Layout.Child>
            <Layout.Child childId="OnboardingSummaryList8" className="padding">
              <ReactArcGrid
                gridId="reporting-currency"
                data={[]}
                columns={columns}
                options={options}
              />
            </Layout.Child>
          </Layout>
        </Layout.Child>
      </Layout>
    );
  } else return null;
};

export default SummaryList;
