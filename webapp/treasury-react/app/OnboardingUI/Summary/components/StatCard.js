import React, { useState } from "react";
import { Panel } from "arc-react-components";
import { PieChart, Pie, ResponsiveContainer, Cell, Sector } from "recharts";
import { ReactArcGrid } from "arc-grid";
import options from "./../grid/gridOptions";
import columns from "./../grid/columnConfig";

const StatCard = props => {
  const [pieChart, setPieChart] = useState(true);
  const [activeIndex, setActiveIndex] = useState(null);
  const next = () => {
    setPieChart(false);
  };

  const prev = () => {
    setPieChart(true);
  };

  const onPieEnter = (data, index) => {
    setActiveIndex(index);
  }

  const onPieLeave = (data, index) => {
    setActiveIndex(null);
  }

  const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180;
    const {
      cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
      fill, payload, percent, value,
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
      <g>
        <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
        <Sector
          cx={cx}
          cy={cy}
          startAngle={startAngle}
          endAngle={endAngle}
          innerRadius={outerRadius + 6}
          outerRadius={outerRadius + 10}
          fill={fill}
        />
        <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
        <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
        <text x={ex + (cos >= 0 ? 1 : -1) * 12}
          y={ey} textAnchor={textAnchor} fill="#333">{value}</text>
      </g>
    );
  };

  let data = [
    { name: "Issues", value: props.data },
    { name: "Onboarded", value: props.totalData - props.data }
  ];
  return (
    <Panel title={props.title}>
        <span>
          <i
            className={
              pieChart ? "icon-scroll--left disabled" : "icon-scroll--left"
            }
            title="Previous"
            onClick={prev}
          ></i>
          <i
            className={
              !pieChart ? "icon-scroll--right disabled" : "icon-scroll--right"
            }
            title="Next"
            onClick={next}
          ></i>
        </span>
      {(pieChart ?
        <ResponsiveContainer height={"88%"} width={"100%"}>
          <PieChart
            margin={{ top: 5, right: 20, left: 5, bottom: 5 }}
          >
            <Pie
              activeIndex={activeIndex}
              activeShape={renderActiveShape}
              data={data}
              dataKey="value"
              innerRadius={50}
              outerRadius={70}
              paddingAngle={5}
              fill="#82ca9d"
              onMouseEnter={onPieEnter}
              onMouseLeave={onPieLeave}
            >
              <Cell key={`cell-0`} fill={'#FF8042'} />
              <Cell key={`cell-1`} fill={'#00C49F'} />
            </Pie>
          </PieChart>
        </ResponsiveContainer> :
        props.gridData !== undefined && props.gridData.length > 0 ?
          <div style={{ height: window.innerHeight / 5.5 + "px" }}>
            <ReactArcGrid
              gridId={props.gridId}
              data={props.gridData}
              columns={columns}
              options={options}
            />
          </div> : null
      )}
    </Panel>
  );
};

export default StatCard;
