import React from "react";
import StatCard from "./StatCard";
import { Layout } from "arc-react-components";
import { onboardingScreen } from "./../../util";

const Stats = props => {
  return (
    <Layout>
      <Layout.Child childId="OnboardingStats1" className="padding--double">
        <StatCard
          title="Summary"
          data={props.statsData.statsData[onboardingScreen["SUMMARY"]]}
          totalData={props.statsData.totalMethodologies}
          gridData={props.summaryData}
          gridId={"Summary"}
        />
      </Layout.Child>
      <Layout.Child childId="OnboardingStats2">
        <Layout isColumnType={true}>
          <Layout.Child childId="OnboardingStats3" className="padding--double">
            <StatCard
              title="Calculator Group"
              data={props.statsData.statsData[onboardingScreen["CALCULATOR"]]}
              totalData={props.statsData.totalMethodologies}
              gridData={onboardingScreen["CALCULATOR"] in props.gridData ? props.gridData[onboardingScreen["CALCULATOR"]] : []}
              gridId={"Calculator_Group"}
            />
          </Layout.Child>
          <Layout.Child childId="OnboardingStats4" className="padding--double">
            <StatCard
              title="Agreement Terms"
              data={props.statsData.statsData[onboardingScreen["FINANCING"]]}
              totalData={props.statsData.totalMethodologies}
              gridData={onboardingScreen["FINANCING"] in props.gridData ? props.gridData[onboardingScreen["FINANCING"]] : []}
              gridId={"Agreement_Terms"}
            />
          </Layout.Child>
          <Layout.Child childId="OnboardingStats5" className="padding--double">
            <StatCard
              title="Accrual Posting"
              data={props.statsData.statsData[onboardingScreen["ACCRUAL"]]}
              totalData={props.statsData.totalMethodologies}
              gridData={onboardingScreen["ACCRUAL"] in props.gridData ? props.gridData[onboardingScreen["ACCRUAL"]] : []}
              gridId={"Accrual_Posting"}
            />
          </Layout.Child>
        </Layout>
      </Layout.Child>
      <Layout.Child childId="OnboardingStats6">
        <Layout isColumnType={true}>
          <Layout.Child childId="OnboardingStats7" className="padding--double">
            <StatCard
              title="Netting Group"
              data={props.statsData.statsData[onboardingScreen["NETTING"]]}
              totalData={props.statsData.totalMethodologies}
              gridData={onboardingScreen["NETTING"] in props.gridData ? props.gridData[onboardingScreen["NETTING"]] : []}
              gridId={"Netting_Group"}
            />
          </Layout.Child>
          <Layout.Child childId="OnboardingStats8" className="padding--double">
            <StatCard
              title="Accounting Book Mapping"
              data={props.statsData.statsData[onboardingScreen["GENEVABOOK"]]}
              totalData={props.statsData.totalMethodologies}
              gridData={onboardingScreen["GENEVABOOK"] in props.gridData ? props.gridData[onboardingScreen["GENEVABOOK"]] : []}
              gridId={"Accounting_Book"}
            />
          </Layout.Child>
          <Layout.Child childId="OnboardingStats9" className="padding--double">
            <StatCard
              title="Accrual Posting Currency"
              data={props.statsData.statsData[onboardingScreen["REPORTING"]]}
              totalData={props.statsData.totalMethodologies}
              gridData={onboardingScreen["REPORTING"] in props.gridData ? props.gridData[onboardingScreen["REPORTING"]] : []}
              gridId={"Reporting_Currency"}
            />
          </Layout.Child>
        </Layout>
      </Layout.Child>
    </Layout>
  );
};

export default Stats;
