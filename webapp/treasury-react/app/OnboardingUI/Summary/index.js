import React, { Component } from "react";
import { Layout } from "arc-react-components";
import SearchFilter from "./container/SearchFilter";
import SummaryContainer from "./container/SummaryContainer";

class Summary extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <Layout.Child childId="OnboardingSummaryIndex1" size="Fit">
            <SearchFilter />
          </Layout.Child>
          <Layout.Child childId="OnboardingSummaryIndex2">
            <SummaryContainer />
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );
  }
}

export default Summary;
