import { BASE_URL, BASE_MOSS_SERVICE_URL } from "commons/constants";
import { fetchURL, fetchPostURL } from "commons/util";
import { onboardingScreen } from "./util";
import { getValueForSingleSelect } from './util';
const queryString = require("query-string");
export let url = "";

export function getSearchData(payload, view) {
  if(view != onboardingScreen.ADJUSTMENT) {
    if (
      view === onboardingScreen.ASSET ||
      view === onboardingScreen.FINANCING ||
      view === onboardingScreen.NETTING
    ) {
      payload.dateString = payload.dateString.replace(/-/g, "");
    } else if(view === onboardingScreen.RECONCILIATION_THRESHOLDS) {
      payload.date = payload.date.replace(/-/g,"");
    } else {
      payload["ruleSystemFilter.startDate"] = payload[
        "ruleSystemFilter.startDate"
      ].replace(/-/g, "");
    }
  }
  const paramString = queryString.stringify(payload);
  switch (view) {
    case onboardingScreen.CALCULATOR:
      url = `${BASE_URL}/service/calculatorGroupService/getResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.FINANCING:
      url = `${BASE_URL}/financing/load-financing-agreement-terms-results?${paramString}`;
      break;
    case onboardingScreen.ASSET:
      url = `${BASE_URL}/financing/load-asset-class-group-search-results?${paramString}`;
      break;
    case onboardingScreen.REPORTING:
      url = `${BASE_URL}/service/reportingCurrencyService/getResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.ACCRUAL:
      url = `${BASE_URL}/service/methodologyAccrualPostingService/getResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.NETTING:
      url = `${BASE_URL}/financing/get-netting-group-custodian-account-mappings?${paramString}`;
      break;
    case onboardingScreen.BUBUNDLE:
      url = `${BASE_URL}/service/buBundleMappingService/getResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.GENEVABOOK:
      url = `${BASE_URL}/service/genevaBookMappingService/getResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.ADJUSTMENT:
      url = `${BASE_URL}service/adjustmentAllocationRuleSystemService/getResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.RECONCILIATION_THRESHOLDS:
      url = `${BASE_URL}/service/interestReconciliationAutoApprovalThresholdService/getAutoApprovalThresholds?${paramString}&format=JSON`;
      break;
    default:
      break;
  }
  return fetchURL(url);
}

export function getSimulateData(payload, view) {
  if(view != onboardingScreen.ADJUSTMENT) {
    if (
      view === onboardingScreen.ASSET ||
      view === onboardingScreen.FINANCING ||
      view === onboardingScreen.NETTING
    ) {
      if (typeof payload.dateString !== "undefined") {
        payload.dateString = payload.dateString.replace(/-/g, "");
      }
    } else {
      payload["ruleSystemFilter.startDate"] = payload[
        "ruleSystemFilter.startDate"
      ].replace(/-/g, "");
    }
  }
  const paramString = queryString.stringify(payload);
  switch (view) {
    case onboardingScreen.CALCULATOR:
      url = `${BASE_URL}/service/calculatorGroupService/getSimulatedResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.FINANCING:
      url = `${BASE_URL}/financing/agreement-terms-simulate-rules?${paramString}`;
      break;
    case onboardingScreen.ASSET:
      url = `${BASE_URL}/financing/load-asset-classes?${paramString}`;
      break;
    case onboardingScreen.REPORTING:
      url = `${BASE_URL}/service/reportingCurrencyService/getSimulatedResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.ACCRUAL:
      url = `${BASE_URL}/service/methodologyAccrualPostingService/getSimulatedResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.NETTING:
      url = `${BASE_URL}/financing/netting-group-ca-simulate-rules?${paramString}`;
      break;
    case onboardingScreen.BUBUNDLE:
      url = `${BASE_URL}/service/buBundleMappingService/getSimulatedResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.GENEVABOOK:
      url = `${BASE_URL}/service/genevaBookMappingService/getSimulatedResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.ADJUSTMENT:
      url = `${BASE_URL}service/adjustmentAllocationRuleSystemService/getSimulatedResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    default:
      break;
  }
  return fetchURL(url);
}

export function fetchSaveSimulateData(payload, view) {
  if (
    view === onboardingScreen.ASSET ||
    view === onboardingScreen.FINANCING ||
    view === onboardingScreen.NETTING
  ) {
    if (typeof payload.dateString !== "undefined") {
      payload.dateString = payload.dateString.replace(/-/g, "");
    }
  }
  let parameters = {
    rulesToSave: JSON.stringify(payload)
  };
  if (view === onboardingScreen.ASSET) {
    parameters = _.clone(payload);
    parameters.isEditClicked = undefined;
  }
  const paramString = queryString.stringify(parameters);
  switch (view) {
    case onboardingScreen.CALCULATOR:
      url = `${BASE_URL}/service/calculatorGroupService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.FINANCING:
      url = `${BASE_URL}/financing/update-agreement-terms?${paramString}`;
      break;
    case onboardingScreen.ASSET:
      if (!payload.isEditClicked) {
        url = `${BASE_URL}/financing/save-asset-class-group?${paramString}`;
      } else {
        url = `${BASE_URL}/financing/edit-asset-class-group?${paramString}`;
      }
      break;
    case onboardingScreen.REPORTING:
      url = `${BASE_URL}/service/reportingCurrencyService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.ACCRUAL:
      url = `${BASE_URL}/service/methodologyAccrualPostingService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.NETTING:
      url = `${BASE_URL}/financing/update-netting-group-ca-mapping?${paramString}`;
      break;
    case onboardingScreen.BUBUNDLE:
      url = `${BASE_URL}/service/buBundleMappingService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.GENEVABOOK:
      url = `${BASE_URL}/service/genevaBookMappingService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    case onboardingScreen.ADJUSTMENT:
      url = `${BASE_URL}service/adjustmentAllocationRuleSystemService/saveSimulatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
      break;
    default:
      break;
  }
  return fetchURL(url);
}

export function saveBandWidthGroup(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/financing/save-bandwidth-group?${paramString}`;
  return fetchURL(url);
}

export function saveLeanSurchargeModel(payload) {
  const paramString = encodeURIComponent(JSON.stringify(payload));
  url = `${BASE_URL}/service/leanSurchargeService/saveLeanSurchargeModel?inputFormat=JSON&format=JSON&leanSurchargeModel=${paramString}`;
  return fetchURL(url);
}

export function fetchLeanSurchargeModelById(payload) {
  const modelId = payload.id;
  url = `${BASE_URL}/service/leanSurchargeService/getLeanSurchargeModelById?id=${modelId}&format=JSON`;
  return fetchURL(url);
}

export function saveNettingGroupApi(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/financing/add-new-netting-group?${paramString}`;
  return fetchURL(url);
}

export function getAssetClassGroupById(payload) {
  payload.selectedDate = payload.selectedDate.replace(/-/g, "");
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/financing/load-asset-class-group-by-id?${paramString}`;
  return fetchURL(url);
}

export function getCalculatorsForCalculatorGroupId(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/financing/get-calculators-from-calculator-group?${paramString}`;
  return fetchURL(url);
}

export function getAgreementsAffectedForAssetId(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/financing/get-agreements-for-asset-class-group?${paramString}`;
  return fetchURL(url);
}

export function deleteFinancingTerm(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/financing/delete-agreement-term?${paramString}`;
  return fetchURL(url);
}

export function deleteAccrualPosting(payload) {
  let parameters = {
    rulesToSave: JSON.stringify(payload)
  };
  const paramString = queryString.stringify(parameters);
  url = `${BASE_URL}/service/methodologyAccrualPostingService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetchURL(url);
}

export function deleteCalculatorGroup(payload) {
  let parameters = {
    rulesToSave: JSON.stringify(payload)
  };
  const paramString = queryString.stringify(parameters);
  url = `${BASE_URL}/service/calculatorGroupService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetchURL(url);
}

export function deleteReportingCurrency(payload) {
  let parameters = {
    rulesToSave: JSON.stringify(payload)
  };
  const paramString = queryString.stringify(parameters);
  url = `${BASE_URL}/service/reportingCurrencyService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetchURL(url);
}

export function deleteBuBundleMapping(payload) {
  let parameters = {
    rulesToSave: JSON.stringify(payload)
  };
  const paramString = queryString.stringify(parameters);
  url = `${BASE_URL}/service/buBundleMappingService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetchURL(url);
}

export function deleteAdjustmentAllocationRuleApi(payload) {
  const paramString = queryString.stringify(payload);
  return fetchURL(
    (url = `${BASE_URL}service/adjustmentAllocationRuleSystemService/deleteRule?inputFormat=PROPERTIES&${paramString}&format=JSON`)
  );
}

export function saveAssetClassApi(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/financing/save-asset-class?${paramString}`;
  return fetchURL(url);
}

export function saveReconciliationThresholdsApi(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/service/interestReconciliationAutoApprovalThresholdService/addAutoApprovalThreshold?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetchURL(url);
}

export function deleteReconciliationThresholdsApi(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}/service/interestReconciliationAutoApprovalThresholdService/deleteAutoApprovalThreshold?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetchURL(url);
}

export function getAgreementIdApi(payload) {
  const paramString = queryString.stringify(payload, { sort: false });
  return fetchURL(
    `${BASE_MOSS_SERVICE_URL}/custodianAccountRefService/getAgreement?${paramString}&format=json`
  );
}

export function getValidateCalculatorGroupApi(payload) {
  var encodedParam =
    "applicableAgreementTerms=" +
    encodeURIComponent(JSON.stringify(payload.agreementTerms)) +
    "&calcalculatorGroupId=" +
    encodeURIComponent(payload.calculatorGroupId) +
    "&inputFormat=JSON_WITH_REF&format=JSON";
  url = `${BASE_URL}/service/financingWorkflowValidatorService/runCalculatorGroupValidator`;
  return fetchPostURL(url, encodedParam);
}

export function getValidateAgreementTermsApi(payload) {
  var encodedParam =
    "applicableAgreementTerms=" +
    encodeURIComponent(JSON.stringify(payload.agreementTerms)) +
    "&newAgreementTerms=" + encodeURIComponent(JSON.stringify(payload.newAgreementTerms)) +
    "&calculatorGroupId=" +
    encodeURIComponent(payload.calculatorGroupId) +
    "&inputFormat=JSON_WITH_REF&format=JSON";
  url = `${BASE_URL}/service/financingWorkflowValidatorService/runAgreementTermsValidator`;
  return fetchPostURL(url, encodedParam);
}

export function getValidateAssetClassApi(payload) {
  var encodedParam =
    "methodologyDataKey=" +
    encodeURIComponent(JSON.stringify(payload.methodologyDataKey)) +
    "&calculatorGroupId=" +
    encodeURIComponent(payload.calculatorGroupId) +
    "&applicableAgreementTerms=" +
    encodeURIComponent(JSON.stringify(payload.agreementTerms)) +
    "&newAgreementTerms=" + encodeURIComponent(JSON.stringify(payload.newAgreementTerms)) +
    "&inputFormat=JSON_WITH_REF&format=JSON";
  url = `${BASE_URL}/service/financingWorkflowValidatorService/runAssetClassValidator`;
  return fetchPostURL(url, encodedParam);
}

export function getValidateAccrualPostingApi(payload) {
  console.log(payload);
  var encodedParam =
    "methodologyDataKey=" +
    encodeURIComponent(JSON.stringify(payload.methodologyDataKey)) +
    "&calculatorGroupId=" +
    encodeURIComponent(payload.calculatorGroupId) +
    "&newAccrualPostingRuleMap=" +
    encodeURIComponent(JSON.stringify(payload.newAccrualPostingRuleMap)) +
    "&inputFormat=JSON_WITH_REF&format=JSON";
  url = `${BASE_URL}/service/financingWorkflowValidatorService/runAccrualPostingRuleValidator`;
  return fetchPostURL(url, encodedParam);
}

export function getValidateAccountingBookApi(payload) {
  var encodedParam =
    "methodologyDataKey=" +
    encodeURIComponent(JSON.stringify(payload.methodologyDataKey)) +
    "&accountingBookRule=" +
    encodeURIComponent(JSON.stringify(payload.accountingBookRule)) +
    "&inputFormat=JSON_WITH_REF&format=JSON";
  url = `${BASE_URL}/service/financingWorkflowValidatorService/runAccountingBookValidator`;
  return fetchPostURL(url, encodedParam);
}

export function getvalidateWidgetApi(payload) {
  console.log(payload);
  var encodedParam = "methodologyDataKey=" +
    encodeURIComponent(JSON.stringify(payload.methodologyDataKey)) +
    "&applicableAgreementTerms=" +
    encodeURIComponent(JSON.stringify(payload.agreementTerms)) +
    "&newAgreementTerms=" + encodeURIComponent(JSON.stringify(payload.newAgreementTerms)) +
    "&calculatorGroupId=" +
    encodeURIComponent(payload.calculatorGroupId) +
    "&accountingBookRule=" +
    encodeURIComponent(JSON.stringify(payload.accountingBookRule)) +
    "&newAccrualPostingRuleMap=" +
    encodeURIComponent(JSON.stringify(payload.newAccrualPostingRuleMap)) +
    "&inputFormat=JSON_WITH_REF&format=JSON";
  url = `${BASE_URL}/service/financingWorkflowValidatorService/isValidForWorkflow`;
  return fetchPostURL(url, encodedParam);
}

export function getSummaryData(payload) {
  return fetchURL(
    (url = `${BASE_URL}/service/financingDataValidatorService/getLatestRolledUpValidationErrors?date=${payload.selectedDate}&agreementTypeIds=${getValueForSingleSelect(payload.selectedAgreementTypes)}&format=json`)
  );
}

export function getValidMethodologies(payload) {

  return fetchURL(
    (url = `${BASE_URL}/service/financingDataValidatorService/getValidEnrichedMethodologies?date=${payload.selectedDate}&agreementTypeId=${getValueForSingleSelect(payload.selectedAgreementTypes)}&format=json`)
  );
}

export function getAgreementTerms(payload) {
  const encodedParams = getEncodedParams(
    payload,
    "com.arcesium.treasury.model.financing.agreementTerms.AgreementTermsParameter"
  );
  return fetchURL(
    (url = `${BASE_URL}/service/agmtTermsService/getEnrichedTermsAtAllLevels?inputFormat=json&format=json&params=${encodedParams}`)
  );
}

export function getFinancingTermSuggestionsApi(calculatorGroupId) {
  return fetchURL(
    (url = `${BASE_URL}/service/agreementTermsMandatoryFieldsSuggestion/getSuggestionsForAgreementTerms?format=json&calculatorGroupId=${calculatorGroupId}`)
  );
}

export function getAccrualPostingSuggestionsApi(calculatorGroupId) {
  return fetchURL(
    (url = `${BASE_URL}/service/accrualPostingRuleSuggestion/getAccrualPostingRuleSuggestions?format=json&calculatorGroupId=${calculatorGroupId}`)
  );
}

export function getRunSimulationApi(payload) {

  return fetchURL(
    (url = `${BASE_URL}service/financingDriver/runCompleteValidation?inputFormat=PROPERTIES&financingPortfolioGeneratorInput.date=${payload.selectedDate}&financingPortfolioGeneratorInput.agreementTypeIds=${getValueForSingleSelect(payload.selectedAgreementTypes)}&format=json`)
  );
}

export function getNettingGroupDetailApi(payload) {
  const paramString = queryString.stringify(payload);
  return fetchURL(
    (url = `${BASE_URL}service/nettingGroupMappingService/getNettingGroupDetail?inputFormat=PROPERTIES&${paramString}&format=JSON`)
  );
}

function getEncodedParams(payload, className) {
  payload["@CLASS"] = className;
  const jsonString = JSON.stringify(payload);
  return encodeURI(jsonString);
}

export function interestEditUserAuth(payload) {
  return fetchURL(`${BASE_URL}/financing/reconciliation/user-auth-for-interest-edit`);
}



export function interestPowerEditUserAuth(payload) {
  return fetchURL(`${BASE_URL}/financing/reconciliation/user-auth-for-interest-power-edit`);
}
