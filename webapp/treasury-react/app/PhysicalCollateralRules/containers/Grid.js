import React, { Component } from "react";
import { gridColumns } from "../Grid/gridColumns";
import { Layout } from "arc-react-components";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { ReactArcGrid } from "arc-grid";
import Message from "../../commons/components/Message";
import { getGridOptions } from "../Grid/gridOptions";
import AddRulePopUp from "./PhysicalCollateralRulesDialog/AddRulePopUp";
import EditRulePopUp from "./PhysicalCollateralRulesDialog/EditRulePopUp";
import DeletePhysicalCollateralRuleConfirmationDialog from "./DeletePhysicalCollateralRuleConfirmationDialog";
import ErrorMessageDialog from "./ErrorMessageDialog"

@inject("addPhysicalCollateralRuleStore")
@inject("editPhysicalCollateralRuleStore")
@inject("physicalCollateralStore")
@observer
class Grid extends Component {
  static propTypes = {
    physicalCollateralStore: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.physicalCollateralColumns = gridColumns();
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  physicalCollateralStore = this.props.physicalCollateralStore;
  addPhysicalCollateralRuleStore = this.props.addPhysicalCollateralRuleStore;
  editPhysicalCollateralRuleStore = this.props.editPhysicalCollateralRuleStore;

  onSelectedRowsChanged = (selectedRowIds, selectedRowItems) => {
    if (selectedRowIds !== null && selectedRowIds.length > 0) {
      let ruleIds = [];
      for (let i = 0; i < selectedRowIds.length; i++) {
        ruleIds.push(selectedRowIds[i]);
      }

      this.physicalCollateralStore.filters.isRowClicked = true;
      this.physicalCollateralStore.filters.toBeDeletedRuleIds = ruleIds;
    } else {
      this.physicalCollateralStore.filters.isRowClicked = false;
      this.physicalCollateralStore.filters.toBeDeletedRuleIds = [];
    }
  };

  onCellClickHandler = args => {
    let ele = args.event.target;
    let { role } = ele.dataset;
    if (role === 'editRule') {
      this.editPhysicalCollateralRuleStore.setEditRulePopupItem(args.item);
      this.editPhysicalCollateralRuleStore.togglePopUp();
    }
  }

  renderGridData = () => {
    let data = this.physicalCollateralStore.physicalCollateralRuleList;
    let flattenedPhysicalCollateralRulesData = this.physicalCollateralStore.getFlattenedData(
      data
    );

    if (this.physicalCollateralStore.error) {
      return <Message messageData="Unable to load data." />;
    }

    if (!flattenedPhysicalCollateralRulesData || !flattenedPhysicalCollateralRulesData.length) {
      return <Message messageData="No data available" />;
    }
    let grid = (
      <ReactArcGrid
        data={flattenedPhysicalCollateralRulesData}
        gridId="PhysicalCollateralRules"
        columns={this.physicalCollateralColumns}
        options={getGridOptions(this.onSelectedRowsChanged)}
        onCellClick={this.onCellClickHandler}
        resizeCanvas={this.physicalCollateralStore.resizeCanvasFlag}
      />
    );
    return grid;
  };

  render() {
    return (
      <Layout>
        <Layout.Child childId="child1">
          <div className="margin" align="left">
            <button
              disabled={!this.physicalCollateralStore.filters.isRowClicked}
              onClick={this.physicalCollateralStore.toggleDeletePhysicalCollateralRuleDialog}
            >
              Delete Rules
            </button>
            <button onClick={this.addPhysicalCollateralRuleStore.togglePopUp}>Add Rules</button>
          </div>
          <DeletePhysicalCollateralRuleConfirmationDialog
            showDeletePhysicalCollateralRulesDialog={
              this.physicalCollateralStore.showDeletePhysicalCollateralRuleDialog
            }
            hideDeletePhysicalCollateralRulesDialog={
              this.physicalCollateralStore.toggleDeletePhysicalCollateralRuleDialog
            }
          />
          <AddRulePopUp
            showPhysicalCollateralRulesPopUp={this.addPhysicalCollateralRuleStore.showPopUpDialog}
            hidePhysicalCollateralRulesPopUp={this.addPhysicalCollateralRuleStore.togglePopUp}
          />
          <EditRulePopUp
            showEditPhysicalCollateralRulesPopUp={this.editPhysicalCollateralRuleStore.showPopUpDialog}
            hideEditPhysicalCollateralRulesPopUp={this.editPhysicalCollateralRuleStore.togglePopUp}
          />
          <ErrorMessageDialog
            errorMessage = {this.physicalCollateralStore.errorMessage}
            showErrorMessageDialog={this.physicalCollateralStore.showErrorMessageDialog}
            hideErrorMessageDialog={this.physicalCollateralStore.toggleErrorMessageDialog}
          />
        </Layout.Child>
        <Layout.Child childId="child2">{this.renderGridData()}</Layout.Child>
      </Layout>
    );
  }
}

export default Grid;
