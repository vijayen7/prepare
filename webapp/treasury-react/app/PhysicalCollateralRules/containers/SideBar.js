import React, { Component } from "react";
import Sidebar from "../../commons/components/Sidebar";
import PropTypes from "prop-types";
import { inject, observer } from "mobx-react";
import SaveSettingsManager from "../../commons/components/SaveSettingsManager";
import ArcDateFilter from '../../commons/container/ArcDateFilter';
import ColumnLayout from "../../commons/components/ColumnLayout";
import FilterButton from "../../commons/components/FilterButton";
import LegalEntityFilter from "../../commons/container/LegalEntityFilter";
import CpeFilter from "../../commons/container/CpeFilter";
import AgreementTypeFilter from "../../commons/container/AgreementTypeFilter";

@inject("physicalCollateralStore")
@observer
class SideBar extends Component {
  static propTypes = {
    physicalCollateralStore: PropTypes.object.isRequired
  };

  store = this.props.physicalCollateralStore;

  onSelect = params => {
    const { key, value } = params;
    const newFilters = Object.assign({}, this.store.filters);
    newFilters[key] = value;
    this.store.setFilters(newFilters);
    this.store.resizeCanvas();
  };

  handleSearch = () => {
    this.store.getPhysicalCollateralRules();
  };

  handleReset = () => {
    this.store.resetFilters();
    this.store.resizeCanvas();
  };

  applySavedFilters = savedFilters => {
    this.store.setFilters(savedFilters);
  };

  render() {
    if (this.store.reloadPhysicalCollateralRules) {
      this.handleSearch();
      this.store.toggleReloadPhysicalCollateralRules();
    }
    return (
      <Sidebar
        collapsible
        size="400px"
        resizeCanvas={this.store.resizeCanvas}
        toggleSidebar={this.store.toggleSidebar}
      >
        <SaveSettingsManager
          selectedFilters={this.store.filters}
          applySavedFilters={this.applySavedFilters}
          applicationName="physicalCollateralRule"
        />
        <ArcDateFilter
          layout="standard"
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.store.filters.selectedDate}
          label="Date"
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedLegalEntities}
          multiSelect={true}
        />
        <CpeFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedCpes}
          multiSelect={true}
        />
        <AgreementTypeFilter
          onSelect={this.onSelect}
          selectedData={this.store.filters.selectedAgreementTypes}
          multiSelect={true}
        />

        <ColumnLayout>
          <FilterButton onClick={this.handleSearch} label="Search" />
          <FilterButton onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

export default SideBar;
