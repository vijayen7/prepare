import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { Dialog } from "arc-react-components";
import PropTypes from "prop-types";

@inject("physicalCollateralStore")
@observer
class DeletePhysicalCollateralRuleConfirmationDialog extends Component {
  static propTypes = {
    physicalCollateralStore: PropTypes.object.isRequired
  };

  store = this.props.physicalCollateralStore;

  render() {
    return (
      <React.Fragment>
        <Dialog
          isOpen={this.props.showDeletePhysicalCollateralRulesDialog}
          onClose={this.props.hideDeletePhysicalCollateralRulesDialog}
          title="Delete Confirmation"
          style={{
            width: "30%"
          }}
          footer={
            <React.Fragment>
              <button onClick={this.store.deletePhysicalCollateralRules}>Yes</button>
            </React.Fragment>
          }
        >
          Are you sure you want to delete these rules?
        </Dialog>
      </React.Fragment>
    );
  }
}

export default DeletePhysicalCollateralRuleConfirmationDialog;
