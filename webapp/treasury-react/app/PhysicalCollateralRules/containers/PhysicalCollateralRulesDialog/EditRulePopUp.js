import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Layout } from 'arc-react-components';
import InputNumberFilter from '../../../commons/components/InputNumberFilter';
import { observer, inject } from 'mobx-react';
import { Dialog } from 'arc-react-components';
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import ConflictingRulesGrid from '../ConflictingRulesGrid'

@inject('editPhysicalCollateralRuleStore')
@observer
class EditRulePopUp extends Component {
  static propTypes = {
    editPhysicalCollateralRuleStore: PropTypes.object.isRequired
  };

  store = this.props.editPhysicalCollateralRuleStore;

  onSelect = (params) => {
    this.store.setFilters(params);
  };

  getPhysicalCollateralRuleReferenceData() {
    this.store.loadPhysicalCollateralRuleReferenceData();
  }

  componentDidMount() {
    this.getPhysicalCollateralRuleReferenceData();
  }

  render() {
    return (
      <React.Fragment>
        <Dialog
          isOpen={this.props.showEditPhysicalCollateralRulesPopUp}
          onClose={this.props.hideEditPhysicalCollateralRulesPopUp}
          title="Edit Physical Collateral Rule"
          style={{
            width: '30%'
          }}
          footer={
            <React.Fragment>
              <button onClick={this.store.createPhysicalCollateralRulesOutput}>Save</button>
            </React.Fragment>
          }
        >
          <Layout>
            <Layout.Child
              childId="editDialogueRuleInputs"
              title="Rule Input Fields"
              showHeader={true}
              className="form padding--double container margin--left"
            >
              <div className="layout--flex margin--top">
                <div>
                  <label>Legal Entity</label>
                </div>
                <div>
                  {this.store.physicalCollateralRuleFilters.selectedLegalEntities}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Counterparty Entity</label>
                </div>
                <div>
                  {this.store.physicalCollateralRuleFilters.selectedCpes}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Agreement Type</label>
                </div>
                <div>
                  {this.store.physicalCollateralRuleFilters.selectedAgreementTypes}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Account Type</label>
                </div>
                <div>
                  {this.store.physicalCollateralRuleFilters.selectedAccountType}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>GBO Type</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedGboTypes)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Sub Type</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedSubType)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Index</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedIndex)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Currency</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedCurrencies)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Country</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedCountries)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Spn</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedSpn)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Rating Type</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedRatingType)}
                </div>
              </div>
              {this.store.physicalCollateralRuleFilters.selectedRatingType && (
                <div className="layout--flex margin--top">
                  <div>
                    <label>Rating Source</label>
                  </div>
                  <div>
                    {not_null(this.store.physicalCollateralRuleFilters.selectedRatingSource)}
                  </div>
                </div>)}
              {this.store.physicalCollateralRuleFilters.selectedRatingSource && (
                <div className="layout--flex margin--top">
                  <div>
                    <label>Min Rating</label>
                  </div>
                  <div>
                    {not_null(this.store.physicalCollateralRuleFilters.selectedMinRating)}
                  </div>
                </div>)}
              {this.store.physicalCollateralRuleFilters.selectedRatingSource && (
                <div className="layout--flex margin--top">
                  <div>
                    <label>Max Rating</label>
                  </div>
                  <div>
                    {not_null(this.store.physicalCollateralRuleFilters.selectedMaxRating)}
                  </div>
                </div>)}
              <div className="layout--flex margin--top">
                <div>
                  <label>Sector Source</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedSectorSource)}
                </div>
              </div>
              {this.store.physicalCollateralRuleFilters.selectedSectorSource && (
                <div className="layout--flex margin--top">
                  <div>
                    <label>Sector Code</label>
                  </div>
                  <div>
                    {not_null(this.store.physicalCollateralRuleFilters.selectedSectorCode)}
                  </div>
                </div>)}
              <div className="layout--flex margin--top">
                <div>
                  <label>Maturity Range Type</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedMaturityRangeType)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Min Maturity</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedMinMaturity)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Max Maturity</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedMaxMaturity)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Effective Start Date</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedEffectiveStartDate)}
                </div>
              </div>
              <div className="layout--flex margin--top">
                <div>
                  <label>Effective End Date</label>
                </div>
                <div>
                  {not_null(this.store.physicalCollateralRuleFilters.selectedEffectiveEndDate)}
                </div>
              </div>
            </Layout.Child>
            <Layout.Child
              childId="editDialogueRuleOutputs"
              title="Rule Output Fields"
              showHeader={true}
              className="form padding--double container margin--left"
            >
              <SingleSelectFilter
                onSelect={this.onSelect}
                stateKey="selectedRuleType"
                label="Rule Type *"
                selectedData={this.store.physicalCollateralRuleFilters.selectedRuleType}
                data={this.store.ruleTypeList}
                horizontalLayout={true}
              />
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedRuleRank"
                    type="number"
                    label="Rule Rank *"
                    data={this.store.physicalCollateralRuleFilters.selectedRuleRank}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedHaircut"
                    type="number"
                    label="Haircut (in %) *"
                    data={this.store.physicalCollateralRuleFilters.selectedHaircut}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedConcentrationThreshold"
                    type="number"
                    label="Conc. Threshold (in %) *"
                    data={this.store.physicalCollateralRuleFilters.selectedConcentrationThreshold}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedMaturityDateThreshold"
                    type="number"
                    label="Maturity Date Threshold *"
                    data={this.store.physicalCollateralRuleFilters.selectedMaturityDateThreshold}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <SingleSelectFilter
                    onSelect={this.onSelect
                    }
                    stateKey="selectedCriteria"
                    label="Criteria *"
                    selectedData={this.store.physicalCollateralRuleFilters.selectedCriteria}
                    data={this.store.criteriaList}
                    horizontalLayout={true}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <SingleSelectFilter
                    onSelect={this.onSelect}
                    stateKey="selectedCriteriaOrder"
                    label="Criteria Order *"
                    selectedData={this.store.physicalCollateralRuleFilters.selectedCriteriaOrder}
                    data={this.store.criteriaOrderList}
                    horizontalLayout={true}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <SingleSelectFilter
                    onSelect={this.onSelect}
                    stateKey="selectedRoundingValue"
                    label="Rounding Value *"
                    selectedData={this.store.physicalCollateralRuleFilters.selectedRoundingValue}
                    data={this.store.roundingValueList}
                    horizontalLayout={true}
                  />)}
            </Layout.Child>
            {this.store.conflictingRuleList.length > 0 &&
              <Layout.Child childId="ConflictingRulesPanel"
                title="Conflicting Rules"
                showHeader={true}
                className="form padding--double container margin--left"
                size="fit">
                <ConflictingRulesGrid store={this.store} />
              </Layout.Child>}
          </Layout>
        </Dialog>
      </React.Fragment>
    );
  }
}

export default EditRulePopUp;

function not_null(data) {
  return (data == null) ? "n/a" : data;
}
