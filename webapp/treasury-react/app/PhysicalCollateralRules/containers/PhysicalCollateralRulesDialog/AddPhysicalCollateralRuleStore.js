import { observable, action, flow } from 'mobx';
import { savePhysicalCollateralRules } from '../../api';
import { getPhysicalCollateralRulesReferenceData } from '../../api';
import { MAX_INT_VALUE } from '../../../commons/constants';
import { CRITERIA, CRITERIA_ORDER, RULE_TYPE, ROUNDING_VALUE, EXCLUDE_RULE_OUTPUT } from '../../constants';

const RULE_TYPE_LIST = [
  { key: "INCLUDE", value: RULE_TYPE.INCLUDE },
  { key: "EXCLUDE", value: RULE_TYPE.EXCLUDE }
]

const CRITERIA_LIST = [
  { key: "SETTLE_DATE_MV", value: CRITERIA.SETTLE_DATE_MV },
  { key: "TIME_TO_MATURITY", value: CRITERIA.TIME_TO_MATURITY }
]

const CRITERIA_ORDER_LIST = [
  { key: "HIGH_LOW", value: CRITERIA_ORDER.HIGH_LOW },
  { key: "LOW_HIGH", value: CRITERIA_ORDER.LOW_HIGH }
]

const ROUNDING_VALUE_LIST = [
  { key: "ONE", value: ROUNDING_VALUE.ONE },
  { key: "TEN", value: ROUNDING_VALUE.TEN },
  { key: "HUNDRED", value: ROUNDING_VALUE.HUNDRED },
  { key: "THOUSAND", value: ROUNDING_VALUE.THOUSAND }
]

const INITIAL_RULES_CONFIG_FILTERS = {
  selectedLegalEntities: null,
  selectedCpes: null,
  selectedAgreementTypes: null,
  selectedAccountType: null,
  selectedGboTypes: null,
  selectedSubType: null,
  selectedIndex: '',
  selectedCurrencies: null,
  selectedCountries: null,
  selectedSpn: '',
  selectedRatingSource: null,
  selectedMaturityRangeType: null,
  selectedMinRating: null,
  selectedMaxRating: null,
  selectedSectorSource: null,
  selectedSectorCode: null,
  selectedMinMaturity: '',
  selectedMaxMaturity: '',
  selectedEffectiveStartDate: '1900-01-01',
  selectedEffectiveEndDate: '2038-01-01',
  selectedRuleRank: '',
  selectedRuleType: RULE_TYPE_LIST[0],
  selectedHaircut: 0.0,
  selectedConcentrationThreshold: 100.0,
  selectedMaturityDateThreshold: 0,
  selectedCriteria: CRITERIA_LIST[0],
  selectedCriteriaOrder: CRITERIA_ORDER_LIST[0],
  selectedRoundingValue: ROUNDING_VALUE_LIST[2]
};

export default class PopUpStore {
  @observable
  physicalCollateralRuleFilters = INITIAL_RULES_CONFIG_FILTERS;

  @observable
  showPopUpDialog = false;

  @observable
  ruleTypeList = [];

  @observable
  subTypeList = [];

  @observable
  ratingSourceList = [];

  @observable
  ratingTypeList = [];

  @observable
  minRatingList = [];

  @observable
  maxRatingList = [];

  @observable
  maturityRangeTypeList = [];

  @observable
  sectorSourceList = [];

  @observable
  sectorCodeList = [];

  @observable
  moodyRatingsList = [];

  @observable
  fitchRatingsList = [];

  @observable
  sandPRatingsList = [];

  @observable
  bicsSectorCodeList = [];

  @observable
  gicsSectorCodeList = [];

  @observable
  accountTypeList = [];

  @observable
  criteriaList = [];

  @observable
  criteriaOrderList = [];

  @observable
  roundingValueList = [];

  @observable
  conflictingRuleList = [];

  physicalCollateralStore = null;
  constructor(physicalCollateralStore) {
    this.physicalCollateralStore = physicalCollateralStore;
  }

  @action.bound
  setFilters(params) {
    this.physicalCollateralRuleFilters[params['key']] = params['value'];
  }

  @action.bound
  validatePhysicalCollateralRule() {
    if (this.physicalCollateralRuleFilters.selectedLegalEntities === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Legal Entity is mandatory');
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedCpes === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Counterparty Entity is mandatory');
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedAgreementTypes === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Agreement Type is mandatory');
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedAccountType === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Account type is mandatory');
      return false;
    }

    if ((this.physicalCollateralRuleFilters.selectedIndex !== null && isNaN(this.physicalCollateralRuleFilters.selectedIndex)) ||
      this.physicalCollateralRuleFilters.selectedIndex > MAX_INT_VALUE ||
      this.physicalCollateralRuleFilters.selectedIndex < 0 ||
      (this.physicalCollateralRuleFilters.selectedSpn !== null && isNaN(this.physicalCollateralRuleFilters.selectedSpn) &&
        (this.physicalCollateralRuleFilters.selectedSpn > MAX_INT_VALUE || this.physicalCollateralRuleFilters.selectedSpn < 0))) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid numbers', false);
      return false;
    }

    if (
      (!isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMinMaturity) &&
        isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMaxMaturity)) ||
      (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMinMaturity) &&
        !isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMaxMaturity))
    ) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter both Min and Max Maturity values', false);
      return false;
    }

    if (
      !isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMinMaturity) &&
      parseFloat(this.physicalCollateralRuleFilters.selectedMinMaturity) < 0
    ) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Min maturity should be greater than 0', false);
      return false;
    }

    if (
      !isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMaxMaturity) &&
      parseFloat(this.physicalCollateralRuleFilters.selectedMaxMaturity) < 0
    ) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Max maturity should be greater than 0', false);
      return false;
    }

    if (
      !isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMinMaturity) &&
      !isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMaxMaturity) &&
      parseFloat(this.physicalCollateralRuleFilters.selectedMinMaturity) >
      parseFloat(this.physicalCollateralRuleFilters.selectedMaxMaturity)
    ) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Min maturity is greater that max Maturity', false);
      return false;
    }

    if (
      this.physicalCollateralRuleFilters.selectedMinRating !== null &&
      this.physicalCollateralRuleFilters.selectedMaxRating !== null &&
      parseInt(this.physicalCollateralRuleFilters.selectedMinRating['key']) <
      parseInt(this.physicalCollateralRuleFilters.selectedMaxRating['key'])
    ) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Order of ratings are opposite', false);
      return false;
    }

    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedEffectiveStartDate)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Effective Start Date is mandatory');
      return false;
    }

    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedEffectiveEndDate)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Effective End Date is mandatory');
      return false;
    }

    if (this.physicalCollateralRuleFilters.selectedEffectiveStartDate > this.physicalCollateralRuleFilters.selectedEffectiveEndDate) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Effective Start Date should not be after Effective End Date');
      return false;
    }

    if (this.physicalCollateralRuleFilters.selectedRuleType === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Rule Type');
      return false;
    }
    if (this.getValue(this.physicalCollateralRuleFilters.selectedRuleType) == 'EXCLUDE') {
      return true;
    }
    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedRuleRank)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Rule Rank');
      return false;
    }
    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedHaircut)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Haircut');
      return false;
    }
    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedConcentrationThreshold)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Concentration threshold');
      return false;
    }
    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Maturity date threshold');
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedCriteria === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Criteria', false);
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedCriteriaOrder === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Criteria Order', false);
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedRoundingValue === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Rounding Value', false);
      return false;
    }
    if (
      isNaN(this.physicalCollateralRuleFilters.selectedRuleRank) ||
      isNaN(this.physicalCollateralRuleFilters.selectedHaircut) ||
      isNaN(this.physicalCollateralRuleFilters.selectedConcentrationThreshold) ||
      isNaN(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold)
    ) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid numbers', false);
      return false;
    }

    if (!isInteger(this.physicalCollateralRuleFilters.selectedRuleRank)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter an integer rank', false);
      return false;
    }

    if (!isInteger(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter an integer Maturity date threshold', false);
      return false;
    }

    if (parseInt(this.physicalCollateralRuleFilters.selectedRuleRank) > MAX_INT_VALUE) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid rank (<= ' + MAX_INT_VALUE + ')', false);
      return false;
    }

    if (parseInt(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold) > MAX_INT_VALUE ||
      parseInt(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold) < 0) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Maturity date threshold (0 <= ' + MAX_INT_VALUE + ')', false);
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedHaircut) < 0) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Haircut (>= 0 %)', false);
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedHaircut) > 100) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Haircut (<= 100 %)', false);
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedHaircut).countDecimals() > 3) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter Haircut (in %) upto 3 decimal places', false);
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedConcentrationThreshold) <= 0) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Concentration Threshold (> 0 %)', false);
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedConcentrationThreshold) > 100) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Concentration Threshold (<= 100 %)', false);
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedConcentrationThreshold).countDecimals() > 3) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter Concentration Threshold (in %) upto 3 decimal places', false);
      return false;
    }

    return true;
  }

  @action.bound
  createPhysicalCollateralRule() {
    let isValidRule = this.validatePhysicalCollateralRule();
    if (isValidRule === false) {
      return;
    }

    let ruleOutput = this.getValue(this.physicalCollateralRuleFilters.selectedRuleType) == 'EXCLUDE' ? EXCLUDE_RULE_OUTPUT :
      {
        ruleRank: parseInt(this.physicalCollateralRuleFilters.selectedRuleRank),
        ruleType: this.getValue(this.physicalCollateralRuleFilters.selectedRuleType),
        haircut: !isNaN(this.physicalCollateralRuleFilters.selectedHaircut)
          ? (parseFloat(this.physicalCollateralRuleFilters.selectedHaircut) / 100).toFixed(5)
          : undefined,
        concentrationThreshold: !isNaN(this.physicalCollateralRuleFilters.selectedConcentrationThreshold)
          ? (parseFloat(this.physicalCollateralRuleFilters.selectedConcentrationThreshold) / 100).toFixed(5)
          : undefined,
        maturityDateThreshold: parseInt(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold),
        criteria: this.getKeys(this.physicalCollateralRuleFilters.selectedCriteria),
        criteriaOrder: this.getKeys(this.physicalCollateralRuleFilters.selectedCriteriaOrder),
        roundingValue: this.getKeys(this.physicalCollateralRuleFilters.selectedRoundingValue)
      }

    let newPhysicalCollateralRule = {
      legalEntityId: this.getKeys(this.physicalCollateralRuleFilters.selectedLegalEntities),
      cpeId: this.getKeys(this.physicalCollateralRuleFilters.selectedCpes),
      agreementTypeId: this.getKeys(this.physicalCollateralRuleFilters.selectedAgreementTypes),
      accountType: this.physicalCollateralRuleFilters.selectedAccountType,
      gboTypeId: this.getKeys(this.physicalCollateralRuleFilters.selectedGboTypes),
      subTypeId: this.getKeys(this.physicalCollateralRuleFilters.selectedSubType),
      indexSpn: this.physicalCollateralRuleFilters.selectedIndex,
      currencySpn: this.getKeys(this.physicalCollateralRuleFilters.selectedCurrencies),
      countryId: this.getKeys(this.physicalCollateralRuleFilters.selectedCountries),
      spn: this.physicalCollateralRuleFilters.selectedSpn,
      sectorSource: this.physicalCollateralRuleFilters.selectedSectorSource,
      sectorCodeId: this.getKeys(this.physicalCollateralRuleFilters.selectedSectorCode),
      maturityRangeType: this.physicalCollateralRuleFilters.selectedMaturityRangeType,
      minMaturity: this.physicalCollateralRuleFilters.selectedMinMaturity,
      maxMaturity: this.physicalCollateralRuleFilters.selectedMaxMaturity,
      supportedRatingSource: this.physicalCollateralRuleFilters.selectedRatingSource,
      supportedRatingType: this.physicalCollateralRuleFilters.selectedRatingType,
      minRating: this.getValue(this.physicalCollateralRuleFilters.selectedMinRating),
      maxRating: this.getValue(this.physicalCollateralRuleFilters.selectedMaxRating),
      effectiveStartDate: this.physicalCollateralRuleFilters.selectedEffectiveStartDate,
      effectiveEndDate: this.physicalCollateralRuleFilters.selectedEffectiveEndDate,
      ruleRank: ruleOutput.ruleRank,
      ruleType: ruleOutput.ruleType,
      haircut: ruleOutput.haircut,
      concentrationThreshold: ruleOutput.concentrationThreshold,
      maturityDateThreshold: ruleOutput.maturityDateThreshold,
      criteria: ruleOutput.criteria,
      criteriaOrder: ruleOutput.criteriaOrder,
      roundingValue: ruleOutput.roundingValue
    };
    this.addPhysicalCollateralRules(newPhysicalCollateralRule);
  }

  @action.bound
  getKeys(input) {
    return input === null ? null : input['key'];
  }

  @action.bound
  getValue(input) {
    return input === null ? null : input['value'];
  }

  addPhysicalCollateralRules = flow(function* addPhysicalCollateralRules(
    newPhysicalCollateralRule
  ) {
    try {
      newPhysicalCollateralRule['@CLASS'] =
        'com.arcesium.treasury.model.lcm.physicalcollateral.rule.PhysicalCollateralRule';
      let apiresponse = yield savePhysicalCollateralRules(newPhysicalCollateralRule);
      if (apiresponse.successStatus === true) {
        this.conflictingRuleList = [];
        this.togglePopUp();
        this.physicalCollateralStore.generateSuccessToastMessage(apiresponse.message);
        this.physicalCollateralStore.getPhysicalCollateralRules();
      }
      else if (apiresponse.successStatus === false) {
        this.physicalCollateralStore.toggleErrorMessageDialog(apiresponse.message);
        if ((apiresponse.message.includes("Rules are already present with Rule ID")
          || apiresponse.message.includes("Conflicting rank"))
          && apiresponse.response !== undefined && apiresponse.response[1] !== undefined) {
          this.conflictingRuleList = apiresponse.response[1];
        }
      }
      else {
        throw "Internal error";
      }
    } catch (e) {
      this.physicalCollateralStore.toggleErrorMessageDialog();
    }
  });

  @action.bound
  resetFilters() {
    this.physicalCollateralRuleFilters = INITIAL_RULES_CONFIG_FILTERS;
    this.conflictingRuleList = [];
  }

  @action.bound
  loadPhysicalCollateralRuleReferenceData() {
    this.getPhysicalCollateralRuleReferenceDataList();
  }

  @action.bound
  togglePopUp() {
    this.resetFilters();
    this.showPopUpDialog = !this.showPopUpDialog;
    this.conflictingRuleList = [];
  }

  getPhysicalCollateralRuleReferenceDataList = flow(
    function* getPhysicalCollateralRuleReferenceDataList() {
      try {
        let physicalCollateralRulesReferenceData = yield getPhysicalCollateralRulesReferenceData();
        this.prepareReferenceData(physicalCollateralRulesReferenceData);
      } catch (e) {
        console.log(e);
      }
    }.bind(this)
  );

  @action.bound
  prepareReferenceData(referenceData) {
    this.subTypeList = this.getSortedList(referenceData.subTypeIdToNameMap);
    this.bicsSectorCodeList = this.getSortedList(referenceData.bicsIdToSectorCodeMap);
    this.gicsSectorCodeList = this.getSortedList(referenceData.gicsIdToSectorCodeMap);
    this.moodyRatingsList = this.getSortedList(referenceData.moodyRatingIdToRatingMap);
    this.fitchRatingsList = this.getSortedList(referenceData.fitchRatingIdToRatingMap);
    this.sandPRatingsList = this.getSortedList(referenceData.sandPRatingIdToRatingMap);
    this.ratingSourceList = referenceData.supportedRatingSourceList;
    this.ratingTypeList = referenceData.supportedRatingType
    this.maturityRangeTypeList = referenceData.maturityRangeTypeList;
    this.sectorSourceList = referenceData.sectorSourceList;
    this.accountTypeList = referenceData.accountTypeList;
    this.ruleTypeList = RULE_TYPE_LIST;
    this.criteriaList = CRITERIA_LIST;
    this.criteriaOrderList = CRITERIA_ORDER_LIST;
    this.roundingValueList = ROUNDING_VALUE_LIST;
  }

  @action.bound
  getSortedList(unSortedList) {
    if (unSortedList !== undefined) {
      let list = [];
      _.forEach(unSortedList, (value, key) => {
        list = list.concat({ key, value });
      });
      list.sort((a, b) => {
        const valueA = a.value.toLowerCase();
        const valueB = b.value.toLowerCase();
        if (valueA < valueB) return -1;
        if (valueA > valueB) return 1;
        return 0;
      });
      return list;
    }
  }

  @action.bound
  resetRatingList() {
    this.physicalCollateralRuleFilters.selectedMinRating = null;
    this.physicalCollateralRuleFilters.selectedMaxRating = null;
    this.minRatingList = [];
    this.maxRatingList = [];
  }

  @action.bound
  getRatingsList() {
    this.resetRatingList();
    if (this.physicalCollateralRuleFilters.selectedRatingSource === null) {
      return;
    }
    switch (this.physicalCollateralRuleFilters.selectedRatingSource) {
      case 'MOODY':
        this.minRatingList = this.moodyRatingsList;
        this.maxRatingList = this.moodyRatingsList;
        break;
      case 'FITCH':
        this.minRatingList = this.fitchRatingsList;
        this.maxRatingList = this.fitchRatingsList;
        break;
      case 'SANDP':
        this.minRatingList = this.sandPRatingsList;
        this.maxRatingList = this.sandPRatingsList;
        break;
    }
  }

  @action.bound
  resetSectorCodeList() {
    this.sectorCodeList = [];
    this.physicalCollateralRuleFilters.selectedSectorCode = null;
  }

  @action.bound
  getSectorCodeList() {
    this.resetSectorCodeList();

    if (this.physicalCollateralRuleFilters.selectedSectorSource === null) {
      return;
    }

    switch (this.physicalCollateralRuleFilters.selectedSectorSource) {
      case 'BICS':
        this.sectorCodeList = this.bicsSectorCodeList;
        break;
      case 'GICS':
        this.sectorCodeList = this.gicsSectorCodeList;
        break;
    }
  }
}

function isNullOrEmpty(value) {
  return ((value === null) || (value === ""))
}

Number.prototype.countDecimals = function () {
  if (Math.floor(this.valueOf()) === this.valueOf()) return 0;
  return this.toString().split('.')[1].length || 0;
};

function isInteger(value) {
  if (isNaN(value))
    return false
  return parseInt(value) == parseFloat(value)
}
