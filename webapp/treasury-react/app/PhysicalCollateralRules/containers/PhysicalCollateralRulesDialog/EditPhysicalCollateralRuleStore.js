import { observable, action, flow } from 'mobx';
import { editPhysicalCollateralRuleOutput } from '../../api';
import { parseDJSrvString } from '../../../commons/util';
import { MAX_INT_VALUE } from '../../../commons/constants';
import { CRITERIA, CRITERIA_ORDER, RULE_TYPE, ROUNDING_VALUE, EXCLUDE_RULE_OUTPUT } from '../../constants';

const RULE_TYPE_LIST = [
  { key: "INCLUDE", value: RULE_TYPE.INCLUDE },
  { key: "EXCLUDE", value: RULE_TYPE.EXCLUDE }
]

const CRITERIA_LIST = [
  { key: "SETTLE_DATE_MV", value: CRITERIA.SETTLE_DATE_MV },
  { key: "TIME_TO_MATURITY", value: CRITERIA.TIME_TO_MATURITY }
]

const CRITERIA_ORDER_LIST = [
  { key: "HIGH_LOW", value: CRITERIA_ORDER.HIGH_LOW },
  { key: "LOW_HIGH", value: CRITERIA_ORDER.LOW_HIGH }
]

const ROUNDING_VALUE_LIST = [
  { key: "ONE", value: ROUNDING_VALUE.ONE },
  { key: "TEN", value: ROUNDING_VALUE.TEN },
  { key: "HUNDRED", value: ROUNDING_VALUE.HUNDRED },
  { key: "THOUSAND", value: ROUNDING_VALUE.THOUSAND }
]

export default class PopUpStore {
  @observable
  physicalCollateralRuleFilters = {};

  @observable
  showPopUpDialog = false;

  @observable
  ruleTypeList = [];

  @observable
  subTypeList = [];

  @observable
  ratingTypeList = [];

  @observable
  ratingSourceList = [];

  @observable
  minRatingList = [];

  @observable
  maxRatingList = [];

  @observable
  sectorSourceList = [];

  @observable
  sectorCodeList = [];

  @observable
  moodyRatingsList = [];

  @observable
  fitchRatingsList = [];

  @observable
  sandPRatingsList = [];

  @observable
  maturityRangeTypeList = [];

  @observable
  bicsSectorCodeList = [];

  @observable
  gicsSectorCodeList = [];

  @observable
  accountTypeList = [];

  @observable
  editRulePopupItem = null;

  @observable
  criteriaList = [];

  @observable
  criteriaOrderList = [];

  @observable
  roundingValueList = [];

  @observable
  conflictingRuleList = [];

  physicalCollateralStore = null;
  constructor(physicalCollateralStore) {
    this.physicalCollateralStore = physicalCollateralStore;
  }

  @action.bound
  setFilters(params) {
    this.physicalCollateralRuleFilters[params['key']] = params['value'];
  }

  @action.bound
  setEditRulePopupItem(param) {
    this.physicalCollateralRuleFilters.ruleId = param['id'];
    this.physicalCollateralRuleFilters.selectedLegalEntities = param['legalEntity'];
    this.physicalCollateralRuleFilters.selectedCpes = param['counterpartyEntity'];
    this.physicalCollateralRuleFilters.selectedAgreementTypes = param['agreementType'];
    this.physicalCollateralRuleFilters.selectedAccountType = param['accountType'];
    this.physicalCollateralRuleFilters.selectedGboTypes = param['gboType'];
    this.physicalCollateralRuleFilters.selectedSubType = param['subType'];
    this.physicalCollateralRuleFilters.selectedIndex = param['index'];
    this.physicalCollateralRuleFilters.selectedCurrencies = param['currency'];
    this.physicalCollateralRuleFilters.selectedCountries = param['country'];
    this.physicalCollateralRuleFilters.selectedSpn = param['spn'];
    this.physicalCollateralRuleFilters.selectedRatingSource = param['ratingSource'];
    this.physicalCollateralRuleFilters.selectedRatingType = param['ratingType'];
    this.physicalCollateralRuleFilters.selectedMinRating = param['minRating'];
    this.physicalCollateralRuleFilters.selectedMaxRating = param['maxRating'];
    this.physicalCollateralRuleFilters.selectedSectorSource = param['sectorSource'];
    this.physicalCollateralRuleFilters.selectedSectorCode = param['sectorCode'];
    this.physicalCollateralRuleFilters.selectedMaturityRangeType = param['maturityRangeType'];
    this.physicalCollateralRuleFilters.selectedMinMaturity = param['minMaturity'];
    this.physicalCollateralRuleFilters.selectedMaxMaturity = param['maxMaturity'];
    this.physicalCollateralRuleFilters.selectedRuleType = { key: getKeyByValue(RULE_TYPE, param['ruleType']), value: param['ruleType'] };
    this.physicalCollateralRuleFilters.selectedRuleRank = param['ruleRank'] == 'n/a' ? '' : param['ruleRank'];
    this.physicalCollateralRuleFilters.selectedHaircut = param['haircut'] == 'n/a' ? 0.0 : param['haircut'];
    this.physicalCollateralRuleFilters.selectedConcentrationThreshold = param['concentrationThreshold'] == 'n/a' ? 100.0 : param['concentrationThreshold'];
    this.physicalCollateralRuleFilters.selectedMaturityDateThreshold = param['maturityDateThreshold'] == 'n/a' ? 0 : param['maturityDateThreshold'];
    let criteria = param['criteria'] == 'n/a' ? "Settle date Market Value" : param['criteria'];
    let criteriaOrder = param['criteriaOrder'] == 'n/a' ? "HIGH:LOW" : param['criteriaOrder'];
    let roundingValue = param['roundingValue'] == 'n/a' ? 100 : param['roundingValue'];
    this.physicalCollateralRuleFilters.selectedCriteria = { key: getKeyByValue(CRITERIA, criteria), value: criteria };
    this.physicalCollateralRuleFilters.selectedCriteriaOrder = { key: getKeyByValue(CRITERIA_ORDER, criteriaOrder), value: criteriaOrder };
    this.physicalCollateralRuleFilters.selectedRoundingValue = { key: getKeyByValue(ROUNDING_VALUE, roundingValue), value: roundingValue };
    this.physicalCollateralRuleFilters.selectedEffectiveStartDate = dateFormatter(param['effectiveStartDate']);
    this.physicalCollateralRuleFilters.selectedEffectiveEndDate = dateFormatter(param['effectiveEndDate']);
  }

  @action.bound
  validatePhysicalCollateralRuleOutput() {
    if (this.physicalCollateralRuleFilters.selectedRuleType === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Rule Type');
      return false;
    }
    if (this.getValue(this.physicalCollateralRuleFilters.selectedRuleType) == 'EXCLUDE') {
      return true;
    }
    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedRuleRank)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Rule Rank');
      return false;
    }
    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedHaircut)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Haircut');
      return false;
    }
    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedConcentrationThreshold)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Concentration threshold');
      return false;
    }
    if (isNullOrEmpty(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold)) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Maturity date threshold');
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedCriteria === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Criteria');
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedCriteriaOrder === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Criteria Order');
      return false;
    }
    if (this.physicalCollateralRuleFilters.selectedRoundingValue === null) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please add Rounding Value');
      return false;
    }
    if (
      isNaN(this.physicalCollateralRuleFilters.selectedRuleRank) ||
      isNaN(this.physicalCollateralRuleFilters.selectedHaircut) ||
      isNaN(this.physicalCollateralRuleFilters.selectedConcentrationThreshold ||
        isNaN(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold))
    ) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid numbers');
      return false;
    }

    if (parseInt(this.physicalCollateralRuleFilters.selectedRuleRank) > MAX_INT_VALUE) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid rank (<= ' + MAX_INT_VALUE + ')');
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedHaircut) < 0) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Concentration Threshold (>= 0 %)');
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedHaircut) > 100) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Haircut (<= 100 %)');
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedHaircut).countDecimals() > 3) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter Haircut (in %) upto 3 decimal places');
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedConcentrationThreshold) <= 0) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Concentration Threshold (> 0 %)');
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedConcentrationThreshold) > 100) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Concentration Threshold (<= 100 %)');
      return false;
    }

    if (parseInt(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold) > MAX_INT_VALUE ||
      parseInt(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold) < 0) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter valid Maturity date threshold (0 <= ' + MAX_INT_VALUE + ')', false);
      return false;
    }

    if (parseFloat(this.physicalCollateralRuleFilters.selectedConcentrationThreshold).countDecimals() > 3) {
      this.physicalCollateralStore.toggleErrorMessageDialog('Please enter Concentration Threshold (in %) upto 3 decimal places');
      return false;
    }

    return true;
  }

  @action.bound
  createPhysicalCollateralRulesOutput() {
    let isValidRuleOutput = this.validatePhysicalCollateralRuleOutput();
    if (isValidRuleOutput === false) {
      return;
    }
    let ruleId = this.physicalCollateralRuleFilters.ruleId;
    let physicalCollateralRuleOutput = (this.getValue(this.physicalCollateralRuleFilters.selectedRuleType) == "EXCLUDE") ? EXCLUDE_RULE_OUTPUT :
      {
        ruleRank: parseInt(this.physicalCollateralRuleFilters.selectedRuleRank),
        ruleType: this.getValue(this.physicalCollateralRuleFilters.selectedRuleType),
        haircut: !isNaN(this.physicalCollateralRuleFilters.selectedHaircut)
          ? (parseFloat(this.physicalCollateralRuleFilters.selectedHaircut) / 100).toFixed(5)
          : 0.0,
        concentrationThreshold: !isNaN(this.physicalCollateralRuleFilters.selectedConcentrationThreshold)
          ? (parseFloat(this.physicalCollateralRuleFilters.selectedConcentrationThreshold) / 100).toFixed(5)
          : 1.0,
        maturityDateThreshold: parseInt(this.physicalCollateralRuleFilters.selectedMaturityDateThreshold),
        criteria: this.getKeys(this.physicalCollateralRuleFilters.selectedCriteria),
        criteriaOrder: this.getKeys(this.physicalCollateralRuleFilters.selectedCriteriaOrder),
        roundingValue: this.getKeys(this.physicalCollateralRuleFilters.selectedRoundingValue)
      };
    this.editPhysicalCollateralRules(ruleId, physicalCollateralRuleOutput);
  }

  @action.bound
  getKeys(input) {
    return input === null ? null : input['key'];
  }

  @action.bound
  getValue(input) {
    return input === null ? null : input['value'];
  }

  editPhysicalCollateralRules = flow(function* editPhysicalCollateralRules(ruleId,
    physicalCollateralRuleOutput
  ) {
    let successStatus = false;
    try {
      let apiresponse = yield editPhysicalCollateralRuleOutput(ruleId, physicalCollateralRuleOutput);
      successStatus = apiresponse.successStatus;
      if (successStatus === true) {
        this.conflictingRuleList = [];
        this.togglePopUp();
        this.physicalCollateralStore.generateSuccessToastMessage(apiresponse.message);
        this.physicalCollateralStore.getPhysicalCollateralRules();
      }
      else if (successStatus === false) {
        this.physicalCollateralStore.toggleErrorMessageDialog(apiresponse.message);
        if (apiresponse.message.includes("Conflicting rank")
          && apiresponse.response !== undefined && apiresponse.response[1] !== undefined) {
          this.conflictingRuleList = apiresponse.response[1];
        }
        this.conflictingRuleList = apiresponse.response[1];
      }
      else {
        throw "Internal Error";
      }
    } catch (e) {
      this.physicalCollateralStore.toggleErrorMessageDialog();
    }
  });

  @action.bound
  loadPhysicalCollateralRuleReferenceData() {
    this.ruleTypeList = RULE_TYPE_LIST;
    this.criteriaList = CRITERIA_LIST;
    this.criteriaOrderList = CRITERIA_ORDER_LIST;
    this.roundingValueList = ROUNDING_VALUE_LIST;
  }

  @action.bound
  togglePopUp() {
    this.showPopUpDialog = !this.showPopUpDialog;
    this.conflictingRuleList = [];
  }

}

function isNullOrEmpty(value) {
  return ((value === null) || (value === ""))
}

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

Number.prototype.countDecimals = function () {
  if (Math.floor(this.valueOf()) === this.valueOf()) return 0;
  return this.toString().split('.')[1].length || 0;
};

function dateFormatter(value) {
  const date = parseDJSrvString(value);
  if (date) {
    return date.format('YYYY-MM-DD');
  }
  return null;
}
