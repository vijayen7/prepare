import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Layout } from 'arc-react-components';
import InputNumberFilter from '../../../commons/components/InputNumberFilter';
import { observer, inject } from 'mobx-react';
import { Dialog } from 'arc-react-components';
import GBOTypeFilter from '../../../commons/container/GBOTypeFilter';
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import CurrencyFilter from '../../../commons/container/CurrencyFilter';
import CountryFilter from '../../../commons/container/CountryFilter';
import ArcDateFilter from '../../../commons/container/ArcDateFilter';
import LegalEntityFilter from '../../../commons/container/LegalEntityFilter';
import CpeFilter from '../../../commons/container/CpeFilter';
import AgreementTypeFilter from '../../../commons/container/AgreementTypeFilter';
import ConflictingRulesGrid from '../ConflictingRulesGrid'

@inject('addPhysicalCollateralRuleStore')
@observer
class AddRulePopUp extends Component {
  static propTypes = {
    addPhysicalCollateralRuleStore: PropTypes.object.isRequired
  };

  store = this.props.addPhysicalCollateralRuleStore;

  onSelect = (params) => {
    const { key, value } = params;
    this.store.setFilters(params);
    if (key === 'selectedRatingSource') {
      this.store.getRatingsList();
    }
    if (key === 'selectedSectorSource') {
      this.store.getSectorCodeList();
    }
  };

  getPhysicalCollateralRuleReferenceData() {
    this.store.loadPhysicalCollateralRuleReferenceData();
  }

  componentDidMount() {
    this.getPhysicalCollateralRuleReferenceData();
  }

  render() {
    return (
      <React.Fragment>
        <Dialog
          isOpen={this.props.showPhysicalCollateralRulesPopUp}
          onClose={this.props.hidePhysicalCollateralRulesPopUp}
          title="Add Physical Collateral Rule"
          style={{
            width: '30%'
          }}
          footer={
            <React.Fragment>
              <button onClick={this.store.createPhysicalCollateralRule}>Save</button>
              <button onClick={this.store.resetFilters}>Reset</button>
            </React.Fragment>
          }
        >
          <Layout>
            <Layout.Child
              childId="addDialogueRuleInputs"
              title="Rule Input Fields"
              showHeader={true}
              className="form padding--double container margin--left"
            >
              <LegalEntityFilter
                onSelect={this.onSelect}
                selectedData={this.store.physicalCollateralRuleFilters.selectedLegalEntities}
                multiSelect={false}
                horizontalLayout={true}
                label="Legal Entity *"
              />
              <CpeFilter
                onSelect={this.onSelect}
                selectedData={this.store.physicalCollateralRuleFilters.selectedCpes}
                multiSelect={false}
                horizontalLayout={true}
                label="Counterparty Entity *"
              />
              <AgreementTypeFilter
                onSelect={this.onSelect}
                selectedData={this.store.physicalCollateralRuleFilters.selectedAgreementTypes}
                multiSelect={false}
                horizontalLayout={true}
                label="Agreement Type *"
              />
              <SingleSelectFilter
                onSelect={this.onSelect}
                stateKey="selectedAccountType"
                label="Account Type *"
                selectedData={this.store.physicalCollateralRuleFilters.selectedAccountType}
                data={this.store.accountTypeList}
                horizontalLayout={true}
              />
              <GBOTypeFilter
                onSelect={this.onSelect}
                selectedData={this.store.physicalCollateralRuleFilters.selectedGboTypes}
                multiSelect={false}
                horizontalLayout={true}
              />
              <SingleSelectFilter
                onSelect={this.onSelect}
                stateKey="selectedSubType"
                label="Sub Type"
                selectedData={this.store.physicalCollateralRuleFilters.selectedSubType}
                data={this.store.subTypeList}
                horizontalLayout={true}
              />
              <InputNumberFilter
                onSelect={this.onSelect}
                stateKey="selectedIndex"
                type="number"
                label="Index"
                data={this.store.physicalCollateralRuleFilters.selectedIndex}
              />
              <CurrencyFilter
                onSelect={this.onSelect}
                selectedData={this.store.physicalCollateralRuleFilters.selectedCurrencies}
                multiSelect={false}
                horizontalLayout={true}
              />
              <CountryFilter
                onSelect={this.onSelect}
                selectedData={this.store.physicalCollateralRuleFilters.selectedCountries}
                multiSelect={false}
                horizontalLayout={true}
              />
              <InputNumberFilter
                onSelect={this.onSelect}
                stateKey="selectedSpn"
                type="number"
                label="Spn"
                data={this.store.physicalCollateralRuleFilters.selectedSpn}
              />
              <SingleSelectFilter
                onSelect={this.onSelect}
                stateKey="selectedRatingType"
                label="Rating Type"
                selectedData={this.store.physicalCollateralRuleFilters.selectedRatingType}
                data={this.store.ratingTypeList}
                horizontalLayout={true}
              />
              {this.store.physicalCollateralRuleFilters.selectedRatingType && (
                <SingleSelectFilter
                  onSelect={this.onSelect}
                  stateKey="selectedRatingSource"
                  label="Rating Source"
                  selectedData={this.store.physicalCollateralRuleFilters.selectedRatingSource}
                  data={this.store.ratingSourceList}
                  horizontalLayout={true}
                />
              )}

              {/* Conditional Display */}
              {this.store.physicalCollateralRuleFilters.selectedRatingSource && (
                <SingleSelectFilter
                  onSelect={this.onSelect}
                  stateKey="selectedMinRating"
                  label="Min Rating"
                  selectedData={this.store.physicalCollateralRuleFilters.selectedMinRating}
                  data={this.store.minRatingList}
                  horizontalLayout={true}
                />
              )}

              {this.store.physicalCollateralRuleFilters.selectedRatingSource && (
                <SingleSelectFilter
                  onSelect={this.onSelect}
                  stateKey="selectedMaxRating"
                  label="Max Rating"
                  selectedData={this.store.physicalCollateralRuleFilters.selectedMaxRating}
                  data={this.store.maxRatingList}
                  horizontalLayout={true}
                />
              )}

              <SingleSelectFilter
                onSelect={this.onSelect}
                stateKey="selectedSectorSource"
                label="Sector Source"
                selectedData={this.store.physicalCollateralRuleFilters.selectedSectorSource}
                data={this.store.sectorSourceList}
                horizontalLayout={true}
              />
              {this.store.physicalCollateralRuleFilters.selectedSectorSource && (
                <SingleSelectFilter
                  onSelect={this.onSelect}
                  stateKey="selectedSectorCode"
                  label="Sector Code"
                  selectedData={this.store.physicalCollateralRuleFilters.selectedSectorCode}
                  data={this.store.sectorCodeList}
                  horizontalLayout={true}
                />
              )}
              <SingleSelectFilter
                onSelect={this.onSelect}
                stateKey="selectedMaturityRangeType"
                label="Maturity Range Type"
                selectedData={this.store.physicalCollateralRuleFilters.selectedMaturityRangeType}
                data={this.store.maturityRangeTypeList}
                horizontalLayout={true}
              />
              {this.store.physicalCollateralRuleFilters.selectedMaturityRangeType && (
                <InputNumberFilter
                  onSelect={this.onSelect}
                  stateKey="selectedMinMaturity"
                  type="number"
                  label="Min Maturity"
                  data={this.store.physicalCollateralRuleFilters.selectedMinMaturity}
                />
              )}
              {this.store.physicalCollateralRuleFilters.selectedMaturityRangeType && (
                <InputNumberFilter
                  onSelect={this.onSelect}
                  stateKey="selectedMaxMaturity"
                  type="number"
                  label="Max Maturity"
                  data={this.store.physicalCollateralRuleFilters.selectedMaxMaturity}
                />
              )}
              <ArcDateFilter
                onSelect={this.onSelect}
                stateKey="selectedEffectiveStartDate"
                data={this.store.physicalCollateralRuleFilters.selectedEffectiveStartDate}
                label="Effective Start Date *"
              />
              <ArcDateFilter
                onSelect={this.onSelect}
                stateKey="selectedEffectiveEndDate"
                data={this.store.physicalCollateralRuleFilters.selectedEffectiveEndDate}
                label="Effective End Date *"
              />
            </Layout.Child>
            <Layout.Child
              childId="addDialogueRuleOutputs"
              title="Rule Output Fields"
              showHeader={true}
              className="form padding--double container margin--left"
            >
              <SingleSelectFilter
                onSelect={this.onSelect}
                stateKey="selectedRuleType"
                label="Rule Type *"
                selectedData={this.store.physicalCollateralRuleFilters.selectedRuleType}
                data={this.store.ruleTypeList}
                horizontalLayout={true}
              />
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedRuleRank"
                    type="number"
                    label="Rule Rank *"
                    data={this.store.physicalCollateralRuleFilters.selectedRuleRank}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedHaircut"
                    type="number"
                    label="Haircut (in %) *"
                    data={this.store.physicalCollateralRuleFilters.selectedHaircut}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedConcentrationThreshold"
                    type="number"
                    label="Conc. Threshold (in %) *"
                    data={this.store.physicalCollateralRuleFilters.selectedConcentrationThreshold}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedMaturityDateThreshold"
                    type="number"
                    label="Maturity Date Threshold *"
                    data={this.store.physicalCollateralRuleFilters.selectedMaturityDateThreshold}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <SingleSelectFilter
                    onSelect={this.onSelect}
                    stateKey="selectedCriteria"
                    label="Criteria *"
                    selectedData={this.store.physicalCollateralRuleFilters.selectedCriteria}
                    data={this.store.criteriaList}
                    horizontalLayout={true}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <SingleSelectFilter
                    onSelect={this.onSelect}
                    stateKey="selectedCriteriaOrder"
                    label="Criteria Order *"
                    selectedData={this.store.physicalCollateralRuleFilters.selectedCriteriaOrder}
                    data={this.store.criteriaOrderList}
                    horizontalLayout={true}
                  />)}
              {(this.store.physicalCollateralRuleFilters.selectedRuleType &&
                this.store.physicalCollateralRuleFilters.selectedRuleType['value'] == 'INCLUDE') && (
                  <SingleSelectFilter
                    onSelect={this.onSelect}
                    stateKey="selectedRoundingValue"
                    label="Rounding Value *"
                    selectedData={this.store.physicalCollateralRuleFilters.selectedRoundingValue}
                    data={this.store.roundingValueList}
                    horizontalLayout={true}
                  />)}
            </Layout.Child>
            {this.store.conflictingRuleList.length > 0 &&
              <Layout.Child childId="ConflictingRulesPanel"
                title="Conflicting Rules"
                showHeader={true}
                className="form padding--double container margin--left"
                size="fit">
                <ConflictingRulesGrid store={this.store} />
              </Layout.Child>}
          </Layout>
        </Dialog>
      </React.Fragment>
    );
  }
}

export default AddRulePopUp;
