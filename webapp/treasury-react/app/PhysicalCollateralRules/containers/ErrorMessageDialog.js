import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { Dialog, Message } from "arc-react-components";
import PropTypes from "prop-types";

const DEFAULT_ERROR_MESSAGE = "An error occurred.\nPlease contact help@arcesium.com"

@inject("physicalCollateralStore")
@observer
class ErrorMessageDialog extends Component {
  static propTypes = {
    physicalCollateralStore: PropTypes.object.isRequired
  };

  store = this.props.physicalCollateralStore;

  render() {
    let errorMessage = this.props.errorMessage === "" ? DEFAULT_ERROR_MESSAGE: this.props.errorMessage;
    return (
      <React.Fragment>
        <Dialog
          isOpen={this.props.showErrorMessageDialog}
          onClose={this.props.hideErrorMessageDialog}
          title={'Error'}
          style={{
            "maxWidth": "30%"
          }}
          footer={
            <React.Fragment>
              <button onClick={this.props.hideErrorMessageDialog}>OK</button>
            </React.Fragment>
          }
        >
        <Message type={Message.Type.CRITICAL}>
          <div style={{ marginTop: '5px', marginBottom: '5px' }}>
          {errorMessage.toString().split('\n').map((value)=>
           (<div key={value}>{value}</div>))
          }
          </div>
        </Message>
        </Dialog>
      </React.Fragment>
    );
  }
}

export default ErrorMessageDialog;
