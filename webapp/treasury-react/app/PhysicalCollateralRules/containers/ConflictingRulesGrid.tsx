import React, { Component } from "react";
import { gridColumns } from "../Grid/gridColumns";
import { Layout } from "arc-react-components";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { ReactArcGrid } from "arc-grid";
import Message from "../../commons/components/Message";

type ConflictingRulesGridProps = {
  store: any;
  physicalCollateralStore: any;
}

@inject("physicalCollateralStore")
@observer
class ConflictingRulesGrid extends Component<any, ConflictingRulesGridProps> {
  static propTypes = {
    store: PropTypes.object.isRequired,
    physicalCollateralStore: PropTypes.object.isRequired
  };

  physicalCollateralColumns: Array<any>;
  store: any;
  physicalCollateralStore = this.props.physicalCollateralStore;

  constructor(props) {
    super(props);
    this.physicalCollateralColumns = gridColumns(true);
    this.store = this.props.store;
  }

  options = {
    autoHorizontalScrollBar: true,
    highlightRowOnClick: true,
    exportToExcel: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    configureColumns: true,
  };

  renderGridData = () => {
    let data: any = this.store.conflictingRuleList;

    let flattenedPhysicalCollateralRulesData: any = this.physicalCollateralStore.getFlattenedData(
      data
    );

    if (this.physicalCollateralStore.error) {
      return <Message messageData="Unable to load data." />;
    }

    if (!flattenedPhysicalCollateralRulesData || !flattenedPhysicalCollateralRulesData.length) {
      return <Message messageData="No data available" />;
    }
    let grid = (
      <ReactArcGrid
        data={flattenedPhysicalCollateralRulesData}
        gridId={"ConflictingRulesGrid"}
        columns={this.physicalCollateralColumns}
        options={this.options}
      />
    );
    return grid;
  };

  render() {
    return (
      <Layout>
        <Layout.Child childId="child2">{this.renderGridData()}</Layout.Child>
      </Layout>
    );
  }
}

export default ConflictingRulesGrid;
