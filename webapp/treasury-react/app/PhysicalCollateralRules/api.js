import { ArcFetch } from "arc-commons";
import { fetchPostURL} from "../commons/util";
import { BASE_URL } from "../commons/constants";

const QUERY_ARGS = {
    credentials: "include",
    method: "GET"
};

export const getPhysicalCollateralRulesList = (effectiveDate,legalEntityIds,cpeIds,agmtTypeIds) => {
    const url = `${BASE_URL}service/physicalCollateralRuleService/getAllPhysicalCollateralRules?effectiveDate=${effectiveDate}&legalEntityIds=${legalEntityIds}&cpeIds=${cpeIds}&agmtTypeIds=${agmtTypeIds}&inputFormat=PROPERTIES&format=json`;
    return ArcFetch.getJSON(url,QUERY_ARGS);
};

export const getPhysicalCollateralRulesReferenceData = () => {
    const url = `${BASE_URL}service/physicalCollateralRuleReferenceDataService/getPhysicalCollateralRuleReferenceData?format=JSON`;

    return ArcFetch.getJSON(url,QUERY_ARGS);
};

export const savePhysicalCollateralRules = (input) => {

    const url = `${BASE_URL}service/physicalCollateralRuleService/addRule`;
    let param = encodeURIComponent(
        JSON.stringify(input));
    let encodedParm = 'physicalCollateralRule=' + param + '&inputFormat=JSON&format=JSON';
    return fetchPostURL(url, encodedParm);
};

export const editPhysicalCollateralRuleOutput = (ruleId, physicalCollateralRuleOutput) => {

  let params = 'ruleId=' + ruleId +
  '&physicalCollateralRule.ruleRank=' + physicalCollateralRuleOutput['ruleRank'] +
  '&physicalCollateralRule.ruleType=' + physicalCollateralRuleOutput['ruleType'] +
  '&physicalCollateralRule.haircut=' + physicalCollateralRuleOutput['haircut'] +
  '&physicalCollateralRule.concentrationThreshold=' + physicalCollateralRuleOutput['concentrationThreshold'] +
  '&physicalCollateralRule.maturityDateThreshold=' + physicalCollateralRuleOutput['maturityDateThreshold'] +
  '&physicalCollateralRule.criteria=' + physicalCollateralRuleOutput['criteria'] +
  '&physicalCollateralRule.criteriaOrder=' + physicalCollateralRuleOutput['criteriaOrder'] +
  '&physicalCollateralRule.roundingValue=' + physicalCollateralRuleOutput['roundingValue'] +
  '&inputFormat=PROPERTIES&format=JSON';
  const url = `${BASE_URL}service/physicalCollateralRuleService/editRule` + '?' + params;
  return ArcFetch.getJSON(url,QUERY_ARGS);
};

export const invalidatePhysicalCollateralRules = (input) => {
    const url = `${BASE_URL}service/physicalCollateralRuleService/invalidatePhysicalCollateralRules`;
    let param = encodeURIComponent(
        JSON.stringify(input));
    let encodedParam = 'ruleIds=' + param + '&inputFormat=JSON&format=JSON';
    return fetchPostURL(url,encodedParam);
};



