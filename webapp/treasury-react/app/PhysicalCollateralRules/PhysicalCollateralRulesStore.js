import { observable, action, flow } from 'mobx';
import { getCurrentDate, getFilterKeyList } from '../commons/util';
import { getPhysicalCollateralRulesList, invalidatePhysicalCollateralRules } from './api';
import { CRITERIA, CRITERIA_ORDER, ROUNDING_VALUE } from './constants';
import { ToastService } from 'arc-react-components';

const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  error: false
};

const INITIAL_FILTERS = {
  selectedDate: getCurrentDate(),
  selectedCpes: null,
  selectedLegalEntities: null,
  selectedAgreementTypes: null,
  isRowClicked: false,
  toBeDeletedRuleIds: []
};

export default class PhysicalCollateralRulesStore {
  @observable
  searchStatus = INITIAL_SEARCH_STATUS;

  @observable
  filters = INITIAL_FILTERS;

  @observable
  showDeletePhysicalCollateralRuleDialog = false;

  @observable
  showErrorMessageDialog = false;

  @observable
  errorMessage = "";

  @observable
  physicalCollateralRuleList = [];

  @observable
  reloadPhysicalCollateralRules = false;

  @observable
  toggleSidebar = false;

  @observable
  resizeCanvasFlag = false;

  generateSuccessToastMessage(message) {
    ToastService.append({
      content: message,
      type: ToastService.ToastType.SUCCESS,
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 5000
    });
  }

  @action.bound
  toggleErrorMessageDialog(errorMessage = "") {
    this.errorMessage = errorMessage;
    this.showErrorMessageDialog = !this.showErrorMessageDialog;
  }

  @action.bound
  toggleDeletePhysicalCollateralRuleDialog() {
    this.showDeletePhysicalCollateralRuleDialog = !this.showDeletePhysicalCollateralRuleDialog;
  }

  @action.bound
  toggleReloadPhysicalCollateralRules() {
    this.reloadPhysicalCollateralRules = !this.reloadPhysicalCollateralRules;
  }

  @action
  setToggleSidebar() {
    this.toggleSidebar = true;
  }

  @action
  setFilters(newFilters) {
    this.filters = newFilters;
  }

  @action
  resetFilters() {
    this.filters = INITIAL_FILTERS;
  }

  @action
  getFlattenedData(data) {
    let flatennedData = [];
    if (data.length !== 0) {
      data.forEach((item) => {
        let physicalCollateralRule = {
          id: item.ruleId,
          ruleId: item.ruleId,
          legalEntity: item.legalEntity,
          counterpartyEntity: item.counterpartyEntity,
          agreementType: item.agreementType,
          accountType: item.accountType,
          ruleRank: !isNaN(item.ruleRank) && item.ruleType == "INCLUDE" ? parseInt(item.ruleRank) : 'n/a',
          ruleType: item.ruleType,
          gboType: item.gboTypeName,
          subType: item.subTypeName,
          index: item.indexSpn,
          currency: item.currencyISOCode,
          country: item.country,
          spn: item.spn,
          ratingType: item.supportedRatingType,
          ratingSource: item.supportedRatingSource,
          minRating: item.minRating,
          maxRating: item.maxRating,
          sectorSource: item.sectorSource,
          sectorCode: item.sectorCode,
          maturityRangeType: item.maturityRangeType,
          minMaturity: item.minMaturity,
          maxMaturity: item.maxMaturity,
          effectiveStartDate: item.effectiveStartDate,
          effectiveEndDate: item.effectiveEndDate,
          haircut: !isNaN(item.haircut) && item.ruleType == "INCLUDE" ? (parseFloat(item.haircut) * 100).toFixed(3) : 'n/a',
          concentrationThreshold: !isNaN(item.concentrationThreshold) && item.ruleType == "INCLUDE" ? (parseFloat(item.concentrationThreshold) * 100).toFixed(3) : 'n/a',
          maturityDateThreshold: !isNaN(item.maturityDateThreshold) && item.ruleType == "INCLUDE" ? parseInt(item.maturityDateThreshold) : 'n/a',
          criteria: item.ruleType == "INCLUDE" ? CRITERIA[item.criteria] : 'n/a',
          criteriaOrder: item.ruleType == "INCLUDE" ? CRITERIA_ORDER[item.criteriaOrder] : 'n/a',
          roundingValue: item.ruleType == "INCLUDE" ? ROUNDING_VALUE[item.roundingValue] : 'n/a'
        };
        flatennedData.push(physicalCollateralRule);
      });
    }
    return flatennedData;
  }

  @action.bound
  resizeCanvas() {
    setTimeout(() => {
      this.resizeCanvasFlag = !this.resizeCanvasFlag;
    }, 0);
  }

  deletePhysicalCollateralRules = flow(
    function* deletePhysicalCollateralRules() {
      try {
        let apiResponse = yield invalidatePhysicalCollateralRules(this.filters.toBeDeletedRuleIds);
        if (apiResponse.successStatus === true) {
          this.toggleDeletePhysicalCollateralRuleDialog();
          this.filters.isRowClicked = false;
          this.generateSuccessToastMessage(apiResponse.message)
          this.reloadPhysicalCollateralRules = true;
        }
        else if (apiResponse.successStatus === false) {
          this.toggleErrorMessageDialog(apiResponse.message);
        }
        else {
          throw "Internal Error";
        }
      } catch (e) {
        this.toggleErrorMessageDialog();
      }
    }.bind(this)
  );

  getPhysicalCollateralRules = flow(
    function* getPhysicalCollateralRules() {
      this.searchStatus = INITIAL_SEARCH_STATUS;
      this.searchStatus.inProgress = true;
      try {
        let selectedLegalEntities = getFilterKeyList(this.filters.selectedLegalEntities);
        let selectedCpes = getFilterKeyList(this.filters.selectedCpes);
        let selectedAgreementTypes = getFilterKeyList(this.filters.selectedAgreementTypes);
        this.physicalCollateralRuleList = yield getPhysicalCollateralRulesList(
          this.filters.selectedDate,
          selectedLegalEntities,
          selectedCpes,
          selectedAgreementTypes
        );
        this.searchStatus.inProgress = false;
      } catch (e) {
        this.searchStatus = {
          inProgress: false,
          error: true
        };
        this.toggleErrorMessageDialog();
      }
    }.bind(this)
  );
}
