export const CRITERIA = {
  SETTLE_DATE_MV: "Settle date Market Value",
  TIME_TO_MATURITY: "Time to maturity"
}

export const CRITERIA_ORDER = {
  HIGH_LOW: "HIGH:LOW",
  LOW_HIGH: "LOW:HIGH"
}

export const RULE_TYPE = {
  INCLUDE: "INCLUDE",
  EXCLUDE: "EXCLUDE"
}

export const ROUNDING_VALUE = {
  ONE: 1,
  TEN: 10,
  HUNDRED: 100,
  THOUSAND: 1000
}

export const EXCLUDE_RULE_OUTPUT = {
  ruleRank: 1,
  ruleType: "EXCLUDE",
  haircut: 0.0,
  concentrationThreshold: 1.0,
  maturityDateThreshold: 0,
  criteria: "SETTLE_DATE_MV",
  criteriaOrder: "HIGH_LOW",
  roundingValue: "ONE"
};
