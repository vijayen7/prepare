import React, { Component } from "react";
import Loader from "../commons/container/Loader";
import { ReactLoader } from "../commons/components/ReactLoader";
import { inject, observer } from "mobx-react";
import Grid from "./containers/Grid";
import SideBar from "./containers/SideBar";
import PropTypes from "prop-types";
import { getCurrentDate, getFilterListFromCommaSeparatedString } from "../commons/util";

@inject("physicalCollateralStore")
@observer
class PhysicalCollateralRules extends Component {
  static propTypes = {
    physicalCollateralStore: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props)
    this.store = this.props.physicalCollateralStore;

    this.params = new URLSearchParams(window.location.search);

    let filters = {
      selectedDate: getCurrentDate(),
      selectedCpes: this.params.get("cpeId")
        ? getFilterListFromCommaSeparatedString(this.params.get("cpeId"))
        : null,
      selectedLegalEntities: this.params.get("legalEntityId")
        ? getFilterListFromCommaSeparatedString(this.params.get("legalEntityId"))
        : null,
      selectedAgreementTypes: this.params.get("agreementTypeId")
        ? getFilterListFromCommaSeparatedString(this.params.get("agreementTypeId"))
        : null,
      isRowClicked: false,
      toBeDeletedRuleIds: []
    };

    if (filters.selectedCpes || filters.selectedLegalEntities || filters.selectedAgreementTypes) {
      this.props.physicalCollateralStore.setFilters(filters);
      this.props.physicalCollateralStore.getPhysicalCollateralRules();
      this.store.setToggleSidebar();
    }
  }

  render() {
    return (
      <div>
        <Loader />
        <ReactLoader inProgress={this.store.searchStatus.inProgress} />
        <div className="layout--flex--row">
          <arc-header
            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          ></arc-header>
          <div className="layout--flex--row">
            <div className="layout--flex padding--top">
              <div className="size--content padding--right">
                <SideBar />
              </div>
              <Grid />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PhysicalCollateralRules;
