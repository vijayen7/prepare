export function getGridOptions(onSelectedRowsChanged) {
  var options = {
    autoHorizontalScrollBar: true,
    highlightRowOnClick: true,
    exportToExcel: true,
    asyncEditorLoading: false,
    autoEdit: false,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    configureColumns: true,
    checkboxHeader: {
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    },
    onSelectedRowsChanged: onSelectedRowsChanged
  };

  return options;
}
