import { textFormatter, numberFormat } from '../../commons/grid/formatters';
import { parseDJSrvString, decamelize } from '../../commons/util';

export const gridColumns = (skipActions) => {
  const columnIds = [
    'action',
    'ruleId',
    'legalEntity',
    'counterpartyEntity',
    'agreementType',
    'ruleRank',
    'ruleType',
    'accountType',
    'gboType',
    'subType',
    'index',
    'currency',
    'country',
    'spn',
    'ratingType',
    'ratingSource',
    'minRating',
    'maxRating',
    'sectorSource',
    'sectorCode',
    'maturityRangeType',
    'minMaturity',
    'maxMaturity',
    'haircut',
    'concentrationThreshold',
    'maturityDateThreshold',
    'criteria',
    'criteriaOrder',
    'roundingValue',
    'effectiveStartDate',
    'effectiveEndDate'
  ];

  let columns = [];

  columnIds.forEach((col) => {
    let columnConfig = {
      id: col,
      name: decamelize(col, ' '),
      field: col,
      toolTip: col,
      type: 'text',
      sortable: true,
      formatter: textFormatter,
      headerCssClass: 'aln-rt b',
      cssClass: 'aln-lt',
      autoWidth: true
    };
    if (col === 'action') {
      columnConfig.name = '';
      columnConfig.sortable = false;
      columnConfig.fixed = true;
      columnConfig.formatter = function () {
        return ("<a title='Edit'><i data-role='editRule' class='icon-edit--block' /></a>");
      }
      columnConfig.autoWidth = false;
      columnConfig.width = 24;
    } else if (col === 'ruleRank' || col === 'roundingValue') {
      columnConfig.type = 'number';
    } else if (col === 'ruleId') {
      columnConfig.type = 'number';
      columnConfig.autoWidth = false;
      columnConfig.width = 60;
    } else if (col === 'gboType') {
      columnConfig.name = 'GBO Type';
    } else if (col === 'spn') {
      columnConfig.name = 'SPN';
    } else if (col === 'effectiveStartDate' || col === 'effectiveEndDate') {
      columnConfig.formatter = dateFormatter;
      columnConfig.excelDataFormatter = (value) => dateFormatter(null, null, value);
      columnConfig.filter = {
        isFilterOnFormattedValue: true,
        formattedDateFormat: true
      };
    } else if (col === 'minMaturity' || col === 'maxMaturity') {
      columnConfig.formatter = maturityFormatter;
      columnConfig.type = 'number';
    } else if (col === 'haircut') {
      columnConfig.name = 'Haircut (in %)';
      columnConfig.type = 'number';
    } else if (col === 'concentrationThreshold') {
      columnConfig.name = 'Conc. Threshold (in %)';
      columnConfig.type = 'number';
    }
    if (!(col === 'action' && skipActions)) {
      columns.push(columnConfig);
    }
  });
  return columns;
};

const maturityFormatter = (row, cell, value) => {
  return isNaN(value) ? 'n/a' : numberFormat(value, 3);
};

function dateFormatter(x, y, value) {
  const date = parseDJSrvString(value);
  if (date) {
    return date.format('YYYY-MM-DD');
  }
  return '';
}
