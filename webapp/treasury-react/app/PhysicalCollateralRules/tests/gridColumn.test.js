import { gridColumns } from '../Grid/gridColumns';

describe('gridColumns', () => {
  it('should return static columns', () => {
    const expectedColumn = 'Legal Entity';
    expect(gridColumns()).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: expectedColumn,
          type: 'text'
        })
      ])
    );
  });
});
