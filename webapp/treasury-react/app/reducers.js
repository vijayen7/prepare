import { combineReducers } from "redux";
import { loadingReducer } from "commons/reducers/reducer_loading";
import { exceptionReducer } from "commons/reducers/reducer_exception";
import treasuryRootReducer from "Treasury/reducer";
import marketDataReducer from "MarketData/reducer";
import availabilityDataReducer from "AvailabilityData/reducer";
import onboardingUIReducer from "OnboardingUI/reducer";
import rateNegotiationReducer from "seclend/RateNegotiationDialog/reducer";
import financingOpsReducer from "Financing/OpsDashboard/reducer";
import rateNegotiationVerificationReducer from "RateNegotiationVerification/reducer";
import decisionDrivingDashboardReducer from "DecisionDrivingDashboard/reducer";
import returnOnAssetsDataReducer from "ReturnOnAssets/reducer";
import filterReducer from "commons/reducers/reducer_filter";
import detailDataReducer from "DetailReport/reducer";
import { routerReducer } from "react-router-redux";
import cashManagementReducer from "CashManagementReport/reducer";
import cashManagementWorkflowReducer from "CashManagementWorkflow/reducer";
import workflowReducer from "commons/reducers/reducer_workflow";
import outperformanceReducer from "Outperformance/reducer";
import rateNegotiationSavingsReducer from "RateNegotiationSavings/reducer";
import aanaDataReducer from "Aana/AanaReport/reducer";
import lendReportReducer from "LendReports/reducer";
import ruleSystemDataReducer from "Aana/AanaRuleSystem/reducer";
import cofiRepoFinancingReducer from "CofiRepoFinancing/reducer";
import financingReportingDataReducer from "FinancingReporting/reducer";
import outperformanceFileConfigReducer from "OutperformanceFileConfig/reducer";
import CpeGroupReducer from "CounterPartyGrouping/reducer";
import DividendEnhancementReducer from "DividendEnhancement/reducer";
import lcmRootReducer from "Lcm/reducer";
import workflowStatusReducer from "WorkflowStatus/reducer";

const rootReducer = combineReducers({
  treasury: treasuryRootReducer,
  isLoading: loadingReducer,
  isException: exceptionReducer,
  filters: filterReducer,
  router: routerReducer,
  marketData: marketDataReducer,
  availabilityData: availabilityDataReducer,
  financingOnboarding: onboardingUIReducer,
  rateNegotiation: rateNegotiationReducer,
  financingOps: financingOpsReducer,
  rateNegotiationVerification: rateNegotiationVerificationReducer,
  decisionDrivingDashboard: decisionDrivingDashboardReducer,
  returnOnAssetsData: returnOnAssetsDataReducer,
  detailReport: detailDataReducer,
  cashManagementData: cashManagementReducer,
  cashManagementWorkflowData: cashManagementWorkflowReducer,
  workflow: workflowReducer,
  outperformance: outperformanceReducer,
  rateNegotiationSavings: rateNegotiationSavingsReducer,
  aanaData: aanaDataReducer,
  lendReportData: lendReportReducer,
  aanaRuleSystemData: ruleSystemDataReducer,
  financingReporting: financingReportingDataReducer,
  outperformanceFileConfig: outperformanceFileConfigReducer,
  cofiRepoFinancing: cofiRepoFinancingReducer,
  cpeGroupData: CpeGroupReducer,
  dividendEnhancementDetailData: DividendEnhancementReducer,
  workflowStatus: workflowStatusReducer,
  lcm: lcmRootReducer
});

export default rootReducer;
