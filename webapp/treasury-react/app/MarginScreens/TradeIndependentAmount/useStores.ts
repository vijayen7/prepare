import RootStore from "./stores/RootStore";
import { createContext, useContext } from "react";
const rootStore = new RootStore();

const storesContext = createContext({
  rootStore: rootStore,
});

export const useStores = () => useContext(storesContext);
export default rootStore;
