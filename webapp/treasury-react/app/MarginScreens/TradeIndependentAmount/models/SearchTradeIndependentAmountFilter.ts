import MapData from "../../../commons/models/MapData";

export interface SearchTradeIndependentAmountFilter {
  [key: string]: any;
  selectedStartDate: string;
  selectedEndDate: string;
  selectedLegalEntities: MapData[];
  selectedAgreementTypes: MapData[];
  selectedCpes: MapData[];
  selectedGboTypes: MapData[];
  selectedCurrencies: MapData[];
  selectedBooks: MapData[];
  selectedIndependentAmountStatus: MapData;
  selectedCustodianAccounts: MapData[];
  selectedPnlSpns: string;
  selectedTradeIds: string;
}
