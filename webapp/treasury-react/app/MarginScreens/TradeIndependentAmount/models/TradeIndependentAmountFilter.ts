import { CLASS_NAMESPACE } from "../../../commons/ClassConfigs"
import { INDEPENDENT_AMOUNT_STATUS } from "../constants";

export interface TradeIndependentAmountFilter {
  startDate: string;
  endDate: string;
  legalEntityIds: number[];
  counterPartyIds: number[];
  gboTypeIds: number[];
  currencyIds: number[];
  bookIds: number[];
  independentAmountStatus?: INDEPENDENT_AMOUNT_STATUS;
  custodianAccountsIds: number[];
  pnlSpns: number[];
  tradeIds: string[];
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.margin.model.TradeIndependentAmountFilter;
}
