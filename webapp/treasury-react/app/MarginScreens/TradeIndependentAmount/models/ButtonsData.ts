export interface ButtonsData {
  showReset: boolean;
  showSave: boolean;
}
