import { PopUpSelectedData } from "./PopUpSelectedData";

export interface IndependentAmountFormComponentProps {
  onSelect: Function;
  popUpSelectedData: PopUpSelectedData;
}
