import { CLASS_NAMESPACE } from "../../../commons/ClassConfigs"
export interface ModifyIndependentAmountParam {
  [key: string]: any;
  tradeId: string;
  typeId?: number | null;
  value?: number | null;
  currencySpn?: number | null;
  isNegative?: boolean | null;
  effectiveDate: string;
  comment?: string;
  sourceId?: number | null;
  custodianAccountId: number | undefined;
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.margin.model.ModifyIndependentAmountParam;
}
