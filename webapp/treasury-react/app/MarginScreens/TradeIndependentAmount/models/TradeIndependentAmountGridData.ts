export interface TradeIndependentAmountGridData {
  tradeId: string;
  pnlSpn: number;
  tradeDate?: string;
  tradeTime?: string;
  description?: string;
  legalEntity?: string;
  counterPartyEntity?: string;
  executingBroker?: string;
  primeBroker?: string;
  book?: string;
  bundle?: string;
  gboType?: string;
  currency?: string;
  price?: number;
  quantity?: number;
  independentAmountType?: string;
  independentAmountValue?: number;
  independentAmountCurrency?: string;
  isIndependentAmountNegative: boolean;
  independentAmountEffectiveDate?: string;
  comment?: string;
  independentAmountSource?: string;
  userName?: string;
  independentAmountUnderlying?: number;
  independentAmount?: number;
  trader?: string;
  custodianAccount?: string;
  independentAmountStatus: string;
}
