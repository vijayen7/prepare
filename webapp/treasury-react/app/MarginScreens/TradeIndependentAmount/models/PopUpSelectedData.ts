import MapData from "../../../commons/models/MapData";

export interface PopUpSelectedData {
  [key: string]: any;
  selectedIndependentAmountType: MapData | null;
  selectedIndependentAmountValue: number | null;
  selectedIndependentAmountCurrency: MapData | null;
  selectedIsIndependentAmountNegative: boolean | null;
  selectedIndependentAmountEffectiveDate: string
  selectedComment: string;
  selectedIndependentAmountSource: MapData | null;
}
