import ReferenceData from "../../../commons/models/ReferenceData";
import { CLASS_NAMESPACE } from "../../../commons/ClassConfigs"

export interface TradeIndependentAmountData {
  tradeId: string;
  pnlSpn: number;
  tradeDate?: string;
  tradeTime?: string;
  description?: string;
  legalEntity?: ReferenceData;
  counterPartyEntity?: ReferenceData;
  executingBroker?: ReferenceData;
  primeBroker?: ReferenceData;
  book?: ReferenceData;
  bundle?: ReferenceData;
  gboType?: ReferenceData;
  currency?: ReferenceData;
  price?: number;
  quantity?: number;
  independentAmountType?: string;
  independentAmountValue?: number;
  independentAmountCurrency?: ReferenceData;
  isIndependentAmountNegative: boolean;
  independentAmountEffectiveDate: string;
  comment?: string;
  independentAmountSource?: string;
  userName?: string;
  independentAmountUnderlying?: number;
  independentAmount?: number;
  trader?: ReferenceData;
  custodianAccount?: ReferenceData;
  independentAmountStatus: string;
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.margin.model.TradeIndependentAmountDetail;
}
