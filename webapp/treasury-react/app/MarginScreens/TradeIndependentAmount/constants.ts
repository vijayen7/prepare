import MapData from "../../commons/models/MapData";
import { getPreviousBusinessDay } from "../../commons/util";

export const NOT_APPLICABLE = "n/a";

export const DIALOG_HEADERS = {
  UPDATE_IA_INFORMATION: "Update Independent Amount Information",
  DELETE_IA_INFORMATION: "Delete Independent Amount Information",
  IA_DETAILS_WITH_TRADEID: "Independent Amount Details for TradeID : ",
};

export const STATUS_HEADERS = {
  SAVE_STATUS: "Save Status",
  UPDATE_STATUS: "Update Status",
};

export const IA_STATUS = {
  MAPPED: "MAPPED",
  UNMAPPED: "UNMAPPED",
  CONFLICTING: "CONFLICTING"
};


export const ALL_GRID_COLUMNS: string[] = [
  "actions",
  "tradeDate",
  "tradeId",
  "pnlSpn",
  "description",
  "legalEntity",
  "counterPartyEntity",
  "executingBroker",
  "primeBroker",
  "book",
  "bundle",
  "gboType",
  "currency",
  "price",
  "quantity",
  "independentAmountType",
  "independentAmountValue",
  "independentAmountCurrency",
  "isIndependentAmountNegative",
  "independentAmountEffectiveDate",
  "comment",
  "independentAmountSource",
  "userName",
  "independentAmountUnderlying",
  "independentAmount",
  "trader",
  "custodianAccount",
  "tradeTime",
  "independentAmountStatus",
];

export const INITIAL_DISPLAY_COLUMNS: string[] = ALL_GRID_COLUMNS.filter(
  (columnName) => columnName !== "custodianAccount" && columnName !== "tradeTime"
);

export const DEFAULT_PINNED_COLUMNS = { actions: true };

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const SPN_AND_TRADE_ID_DELIMETER = {
  SEMI_COLON: ";",
  SPACE: " "
};


export const STATUS_TYPE_REF_DATA: MapData[] = [
  { key: -1, value: "ALL" },
  { key: 0, value: "MAPPED" },
  { key: 1, value: "UNMAPPED" },
  { key: 2, value: "CONFLICTING" }
];

export const INDEPENDENT_AMOUNT_TYPE_DATA: MapData[] = [
  { key: 0, value: "UNKNOWN_IA_TYPE" },
  { key: 1, value: "PERCENT_MARKET_VALUE" },
  { key: 2, value: "PERCENT_NOTIONAL" },
  { key: 3, value: "CONST_VEGA_MULTIPLIER" },
  { key: 4, value: "ABSOLUTE_VALUE" },
  { key: 5, value: "CONST_VOL_MULTIPLIER" },
  { key: 6, value: "PORTFOLIO_MARGIN" }
]

export const INDEPENDENT_AMOUNT_SOURCE_DATA: MapData[] = [
  { key: 0, value: "UNKNOWN_IA_SOURCE" },
  { key: 1, value: "IA_RULE_SYSTEM" },
  { key: 2, value: "TRADE_IA_INFO_INTERNAL_DATA" },
  { key: 3, value: "TRADE_IA_INFO_BROKER_FILE" },
  { key: 4, value: "IVY_TRADE_EDITOR" },
  { key: 5, value: "OTS_TRADE_COMMENT" },
  { key: 6, value: "GUAS" },
  { key: 7, value: "TRADE_IA_MAPPER" }
]

export const ISDA_AGREEMENT_TYPE_ID = 6;

export enum INDEPENDENT_AMOUNT_TYPE {
  "UNKNOWN_IA_TYPE",
  "PERCENT_MARKET_VALUE",
  "PERCENT_NOTIONAL",
  "CONST_VEGA_MULTIPLIER",
  "ABSOLUTE_VALUE",
  "CONST_VOL_MULTIPLIER",
  "PORTFOLIO_MARGIN"
}

export enum INDEPENDENT_AMOUNT_STATUS {
  MAPPED,
  UNMAPPED,
  CONFLICTING
}

export enum INDEPENDENT_AMOUNT_SOURCE {
  "UNKNOWN_IA_SOURCE",
  "IA_RULE_SYSTEM",
  "TRADE_IA_INFO_INTERNAL_DATA",
  "TRADE_IA_INFO_BROKER_FILE",
  "IVY_TRADE_EDITOR",
  "OTS_TRADE_COMMENT",
  "GUAS",
  "TRADE_IA_MAPPER"
}

export const MESSAGES = {
  INVALID_AGREEMENT:
    "Please enter a valid Combination of Legal Entity, Exposure Counterparty and Agreement Type",
  INVALID_PNL_SPN_WHILE_ADDING:
    "P&L SPN input should be a valid single integer",
  INVALID_TRADE_ID_WHILE_ADDING:
    "Trade Id input should be a valid single string",
  INVALID_START_DATE_AND_END_DATE:
    "Please provide valid Start Date and End Date for the independent amount trades",
  START_DATE_AND_END_DATE_CONFLICT:
    "Please provide Start Date that occurs before the End Date",
  START_DATE_AND_END_DATE_PNLSPN_TRADEID_CONFLICT:
    "Start Date and End Date should be equal when both tradeIds and pnlSpns fields are empty",
  INITIAL_SEARCH_MESSAGE: "Perform search to view Independent Amount Trades",
  SEARCH_TRADES_FAILURE_MESSAGE: "Some error occurred while searching independent amount trades",
  INVALID_PNL_SPN_MESSAGE:
    "P&L SPN Input is invalid. Please enter semi-colon(;) separated integers",
  INVALID_TRADE_ID_MESSAGE:
    "Trade Id Input is invalid. Trade ID starts with TE and will contain only numbers after that. Please enter semi-colon(;) separated strings",
  INVALID_DATE_INPUT: "Please give a valid date input",
  NO_TRADE_INDEPENDENT_AMOUNT_MESSAGE:
    "No Trade Independent Amount are available for the performed search",
  ERROR_MESSAGE: "Error Occurred while fetching independent amount trades",
  GENERIC_ERROR_MESSAGE: "Some error occurred while fetching the data",
  IA_DETAIL_UPDATED_SUCCESSFULLY: "Independent Amount Detail has been updated successfully",
  IA_DETAIL_INVALIDATED_SUCCESSFULLY: "Independent Amount Detail has been invalidated successfully",
  ERROR_WHILE_UPDATING_TRADE_IA_INFO:
    "Some error occurred while updating the independent amount info",
  COPY_SEARCH_URL_MESSAGE: "Search URL copied to clipboard",
  IA_DETAIL_UPDATION_FAILED_FOR_CERTAIN_TYPES_AND_SOURCES: "Cannot update record with IA Type as UNKNOWN_IA_TYPE or PORTFOLIO_MARGIN or IA Source as IA_RULE_SYSTEM",
  IA_DETAIL_UPDATION_MESSAGE_FOR_CERTAIN_SOURCES: "UNKNOWN_IA_SOURCE is converted to TRADE_IA_MAPPER_SOURCE internally",
  INVALID_AGREEMENT_MESSAGE: "Only ISDA agreements are supported currently",
}
export const INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA = {
  INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_FILTER: {
    selectedStartDate: getPreviousBusinessDay(),
    selectedEndDate: getPreviousBusinessDay(),
    selectedLegalEntities: [],
    selectedAgreementTypes: [],
    selectedCpes: [],
    selectedGboTypes: [],
    selectedCurrencies: [],
    selectedBooks: [],
    selectedIndependentAmountStatus: { key: -1, value: "ALL" },
    selectedCustodianAccounts: [],
    selectedPnlSpns: "",
    selectedTradeIds: "",
  },
  INITIAL_SEARCH_STATUS: {
    status: "",
    message: MESSAGES.INITIAL_SEARCH_MESSAGE,
  },
};

export const INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA = {
  INITIAL_POP_UP_SELECTED_DATA: {
    selectedIndependentAmountType: null,
    selectedIndependentAmountValue: 0,
    selectedIndependentAmountCurrency: null,
    selectedIsIndependentAmountNegative: null,
    selectedIndependentAmountEffectiveDate: getPreviousBusinessDay(),
    selectedComment: "",
    selectedIndependentAmountSource: null
  },
  INITIAL_BUTTONS_DATA: {
    showReset: true,
    showSave: true
  },
  INITIAL_STATUS: {
    status: "",
    message: "",
  },
  INITIAL_API_RESPONSE: {
    successStatus: true,
    message: "",
    response: null,
  },
};
