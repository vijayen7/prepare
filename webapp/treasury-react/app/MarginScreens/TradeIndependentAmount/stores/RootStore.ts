import SearchTradeIndependentAmountStore from "./SearchTradeIndependentAmountStore";
import ModifyTradeIndependentAmountStore from "./ModifyTradeIndependentAmountStore";

export class RootStore {
  searchTradeIndependentAmountStore: SearchTradeIndependentAmountStore;
  modifyTradeIndependentAmountStore: ModifyTradeIndependentAmountStore;

  constructor() {
    this.searchTradeIndependentAmountStore = new SearchTradeIndependentAmountStore(this);
    this.modifyTradeIndependentAmountStore = new ModifyTradeIndependentAmountStore(this);
  }
}

export default RootStore;
