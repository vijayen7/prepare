import { action, observable } from "mobx";
import RootStore from "./RootStore";
import { ToastService } from "arc-react-components";
import { SearchTradeIndependentAmountFilter } from "../models/SearchTradeIndependentAmountFilter"
import { TradeIndependentAmountGridData } from "../models/TradeIndependentAmountGridData"
import MapData from "../../../commons/models/MapData";
import { SUCCESS } from "../../../commons/constants";
import Status from "../../../commons/models/Status";
import { getTradeIndependentAmountDetail } from "../api";
import { TradeIndependentAmountFilter } from "../models/TradeIndependentAmountFilter";
import { TradeIndependentAmountData } from "../models/TradeIndependentAmountData";
import { getParsedDate } from "../../../commons/TreasuryUtils";

import {
  MESSAGES,
  TOAST_TYPE,
  INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA,
  INITIAL_DISPLAY_COLUMNS,
  ALL_GRID_COLUMNS
} from "../constants";

import {
  isValidInputData,
  getTradeIndependentAmountFilter,
  getGridDataFromTradeIndependentAmountData,
  getURLFromSelectedFilters,
} from "../utils/searchTradeIndependentAmountUtil";

import { showToastService, getDefaultDate } from "../utils/commonUtil";
import { getTruncatedMessage } from "../../../commons/util";

const copy = require('clipboard-copy')

export class SearchTradeIndependentAmountStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }

  @observable
  inProgress: boolean = false;

  @observable
  collapseSideBar = false;

  @observable
  searchStatus: Status =
    INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA.INITIAL_SEARCH_STATUS;

  @observable
  displayColumns: string[] = INITIAL_DISPLAY_COLUMNS;

  @observable
  selectedLegalEntityData: MapData[] = [];

  @observable
  selectedBookData: MapData[] = [];

  @observable
  tradeIndependentAmountData: TradeIndependentAmountData[] = [];

  @observable
  toggleBtn = true

  @observable
  tradeIndependentAmountGridData: TradeIndependentAmountGridData[] = [];

  @observable
  searchTradeIndependentAmountFilter: SearchTradeIndependentAmountFilter =
    INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA.INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_FILTER;

  @action.bound
  onDisplayColumnsChange(newDisplayColumns) {
    let newDisplayColumnsArray: any[] = [];
    let newDisplayColumnSet = new Set(newDisplayColumns);
    newDisplayColumnsArray = [...newDisplayColumnSet];
    this.displayColumns = newDisplayColumnsArray;
  }

  @action.bound
  applySavedFilters = (savedFilters) => {
    this.searchTradeIndependentAmountFilter = savedFilters;
    if (savedFilters.selectedPnlSpns != "") {
      this.toggleBtn = true
    }
    else if (savedFilters.selectedTradeIds != "") {
      this.toggleBtn = false
    }
  };

  @action.bound
  handleCopySearch = () => {
    let location = window.location.origin;
    let url = getURLFromSelectedFilters(this.searchTradeIndependentAmountFilter, location);
    copy(url);
    showToastService(MESSAGES.COPY_SEARCH_URL_MESSAGE, ToastService.ToastType.INFO, ToastService.Placement.BOTTOM_LEFT,
      1000);
  }

  @action.bound
  onToggleButtonChange = (toggle: boolean) => {
    this.toggleBtn = toggle
  }

  @action.bound
  setCollapsed = (collapsed: boolean) => {
    this.collapseSideBar = collapsed;
  };

  @action.bound
  setSearchStatus(status: string, message: string) {
    this.searchStatus = {
      status: status,
      message: message,
    };
  }

  @action.bound
  setExpandedGlobalSelections(expandedGlobalSelection: any) {
    var bookDataList = expandedGlobalSelection['books'];
    var legalEntityDataList = expandedGlobalSelection['legalEntities'];
    var bookDataMap: MapData[] = [];
    var legalEntityDataMap: MapData[] = [];

    for (var iter = 0; iter < legalEntityDataList.length; iter++) {
      var legalEntityData = legalEntityDataList[iter];
      var legalEntityId = parseInt(legalEntityData['id']);
      var legalEntityName = legalEntityData['name'].concat("[", legalEntityId, "]");
      legalEntityDataMap.push({ key: legalEntityId, value: legalEntityName });
    }

    for (var iter = 0; iter < bookDataList.length; iter++) {
      var bookData = bookDataList[iter];
      bookDataMap.push({ key: parseInt(bookData['id']), value: bookData['name'] });
    }

    this.selectedLegalEntityData = legalEntityDataMap;
    this.selectedBookData = bookDataMap;
  }


  @action.bound
  handleSearch() {
    this.tradeIndependentAmountGridData = [];
    if (this.searchTradeIndependentAmountFilter.selectedStartDate === "") {
      this.searchTradeIndependentAmountFilter.selectedStartDate = getDefaultDate();
    }
    if (
      isValidInputData(
        this.searchTradeIndependentAmountFilter.selectedEndDate,
        this.searchTradeIndependentAmountFilter.selectedPnlSpns,
        this.searchTradeIndependentAmountFilter.selectedTradeIds,
        this.searchTradeIndependentAmountFilter.selectedAgreementTypes
      )
    ) {
      this.fetchTradeIndependentAmount(getTradeIndependentAmountFilter(this.searchTradeIndependentAmountFilter));
    }
  }

  @action.bound
  handleReset() {
    this.searchTradeIndependentAmountFilter =
      INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA.INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_FILTER;
  }

  @action.bound
  setTradeIndependentAmountGridData() {
    if (!this.tradeIndependentAmountData.length) {
      this.setSearchStatus(SUCCESS, MESSAGES.NO_TRADE_INDEPENDENT_AMOUNT_MESSAGE);
      this.tradeIndependentAmountGridData = [];
    } else {
      this.tradeIndependentAmountGridData = getGridDataFromTradeIndependentAmountData(
        this.tradeIndependentAmountData
      );
    }
  }

  @action.bound
  onSelect({ key, value }: any) {
    if (key === "selectedPnlSpns" || key === "selectedTradeIds") {
      if (value.includes(" ")) {
        let inputData: string[] = value.split(" ");
        value = inputData.join(";");
      }
    }
    this.searchTradeIndependentAmountFilter[key] = value;
  }

  fetchTradeIndependentAmount = async (params: TradeIndependentAmountFilter) => {
    try {
      this.inProgress = true;
      this.tradeIndependentAmountData = await getTradeIndependentAmountDetail(params);
      this.setCollapsed(true);
      this.setTradeIndependentAmountGridData();
      this.inProgress = false;
    } catch (error) {
      const truncatedMessage = getTruncatedMessage(error.message)
      showToastService(truncatedMessage, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
      this.inProgress = false;
    }
  };
}

export default SearchTradeIndependentAmountStore;
