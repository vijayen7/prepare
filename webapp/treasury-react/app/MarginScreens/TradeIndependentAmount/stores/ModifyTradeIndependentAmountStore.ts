import RootStore from "./RootStore";
import { action, observable } from "mobx";
import { ToastService } from "arc-react-components";
import { PopUpSelectedData } from "../models/PopUpSelectedData";
import { ButtonsData } from "../models/ButtonsData";
import Status from "../../../commons/models/Status";
import {
  getDialogDataForIndependentAmount, getIAInfoFromDialogData,
} from "../utils/modifyTradeIndependentAmountUtil";
import {
  DIALOG_HEADERS,
  MESSAGES,
  TOAST_TYPE,
  INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA,
  STATUS_HEADERS,
  INDEPENDENT_AMOUNT_TYPE,
  INDEPENDENT_AMOUNT_SOURCE,
} from "../constants";
import { showToastService } from "../utils/commonUtil";
import SearchTradeIndependentAmountStore from "./SearchTradeIndependentAmountStore";
import { INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA } from "../constants";
import { ModifyIndependentAmountParam } from "../models/ModifyIndependentAmountParam";
import { ApiResponse } from "../../../MarginRules/models/ServiceDataTypes";
import { updateIndependentAmountDetail, invalidateIndependentAmountDetail } from "../api";


export class ModifyTradeIndependentAmountStore {

  rootStore: RootStore;
  searchTradeIndependentAmountStore: SearchTradeIndependentAmountStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    this.searchTradeIndependentAmountStore = this.rootStore.searchTradeIndependentAmountStore;
  }

  @observable
  buttonsData: ButtonsData =
    INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA.INITIAL_BUTTONS_DATA;

  @observable
  isOpenUpdateIADialog: boolean = false;

  @observable
  isOpenDeleteIADialog: boolean = false;

  @observable
  setDialogWidth: boolean = true;

  @observable
  inProgress: boolean = false;

  @observable
  status: Status = INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA.INITIAL_STATUS;

  @observable
  showStatus: boolean = false;

  @observable
  statusTitle: string = STATUS_HEADERS.SAVE_STATUS;

  @observable
  apiResponse: ApiResponse =
    INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA.INITIAL_API_RESPONSE;

  @observable
  popUpSelectedData: PopUpSelectedData = INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA.INITIAL_POP_UP_SELECTED_DATA;

  @observable
  tradeIdKey: string = "";

  @observable
  custodianAccountKey: number | undefined = -1;

  @observable
  dialogTitle: string = DIALOG_HEADERS.UPDATE_IA_INFORMATION;

  @observable
  dialogSubTitle: string = DIALOG_HEADERS.IA_DETAILS_WITH_TRADEID;


  @action.bound
  onUpdateIndependentAmountData(tradeId: string) {
    let independentAmountTrade = this.searchTradeIndependentAmountStore.tradeIndependentAmountData.find(
      (trade) => trade.tradeId === tradeId);

    this.dialogTitle = DIALOG_HEADERS.UPDATE_IA_INFORMATION;
    this.dialogSubTitle = DIALOG_HEADERS.IA_DETAILS_WITH_TRADEID.concat(tradeId);
    if (independentAmountTrade) {
      this.tradeIdKey = tradeId;
      this.custodianAccountKey = independentAmountTrade.custodianAccount?.id;
      this.popUpSelectedData = getDialogDataForIndependentAmount(independentAmountTrade);
      this.isOpenUpdateIADialog = true
    } else {
      showToastService(MESSAGES.GENERIC_ERROR_MESSAGE, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
    }
  }

  @action.bound
  onDeleteIndependentAmountData(tradeId: string) {
    let independentAmountTrade = this.searchTradeIndependentAmountStore.tradeIndependentAmountData.find(
      (trade) => trade.tradeId === tradeId);

    this.dialogTitle = DIALOG_HEADERS.DELETE_IA_INFORMATION
    if (independentAmountTrade) {
      this.tradeIdKey = tradeId;
      this.custodianAccountKey = independentAmountTrade.custodianAccount?.id;
      this.popUpSelectedData = getDialogDataForIndependentAmount(independentAmountTrade);
      this.isOpenDeleteIADialog = true
    } else {
      showToastService(MESSAGES.GENERIC_ERROR_MESSAGE, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
    }
  }

  @action.bound
  onCloseUpdateIADialog() {
    this.isOpenUpdateIADialog = false;
  }

  @action.bound
  onCloseDeleteIADialog() {
    this.isOpenDeleteIADialog = false;
  }

  @action.bound
  onSave() {
    let modifyIndependentAmountParam: ModifyIndependentAmountParam = getIAInfoFromDialogData(
      this.popUpSelectedData, this.tradeIdKey, this.custodianAccountKey
    );

    if (modifyIndependentAmountParam.typeId === INDEPENDENT_AMOUNT_TYPE.UNKNOWN_IA_TYPE ||
      modifyIndependentAmountParam.typeId === INDEPENDENT_AMOUNT_TYPE.PORTFOLIO_MARGIN ||
      modifyIndependentAmountParam.sourceId === INDEPENDENT_AMOUNT_SOURCE.IA_RULE_SYSTEM ) {
      showToastService(MESSAGES.IA_DETAIL_UPDATION_FAILED_FOR_CERTAIN_TYPES_AND_SOURCES,
        TOAST_TYPE.CRITICAL,
        ToastService.Placement.TOP_RIGHT, 9000)
    }
    else {
      if (modifyIndependentAmountParam.sourceId === INDEPENDENT_AMOUNT_SOURCE.UNKNOWN_IA_SOURCE) {
        showToastService(MESSAGES.IA_DETAIL_UPDATION_MESSAGE_FOR_CERTAIN_SOURCES,
          TOAST_TYPE.INFO,
          ToastService.Placement.TOP_RIGHT, 5000)
      }
      this.updateIndependentAmountInfo(modifyIndependentAmountParam);
    }
    this.isOpenUpdateIADialog = false;
  }

  @action.bound
  onConfirmDelete() {
    let modifyIndependentAmountParam: ModifyIndependentAmountParam = getIAInfoFromDialogData(
      this.popUpSelectedData, this.tradeIdKey, this.custodianAccountKey
    );
    this.invalidateIndependentAmountInfo(modifyIndependentAmountParam)
    this.isOpenDeleteIADialog = false;
  }

  @action.bound
  onReset() {
    this.setDialogWidth = true;
    this.popUpSelectedData =
      INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA.INITIAL_POP_UP_SELECTED_DATA;
    this.buttonsData = INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA.INITIAL_BUTTONS_DATA;
  }

  @action.bound
  onResetFilters() {
    this.popUpSelectedData = INITIAL_MODIFY_INDEPENDENT_AMOUNT_DATA.INITIAL_POP_UP_SELECTED_DATA
  }

  @action.bound
  onSelect({ key, value }: any) {
    this.popUpSelectedData[key] = value;
  }

  @action.bound
  clearGridData() {
    this.searchTradeIndependentAmountStore.tradeIndependentAmountData = [];
    this.searchTradeIndependentAmountStore.tradeIndependentAmountGridData = [];
  }

  @action.bound
  setStatus(status: string, message: string, title: string) {
    this.status = {
      status: status,
      message: message,
    };
    this.statusTitle = title;
    this.showStatus = true;
  }

  @action.bound
  postSaveCall() {
    if (this.apiResponse.successStatus) {
      this.isOpenUpdateIADialog = false;
      this.onReset();
      showToastService(
        MESSAGES.IA_DETAIL_UPDATED_SUCCESSFULLY,
        TOAST_TYPE.SUCCESS,
        ToastService.Placement.TOP_RIGHT, 7000
      );
      if (
        this.searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter !=
        INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA.INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_FILTER
      ) {
        this.searchTradeIndependentAmountStore.handleSearch();
      } else {
        this.clearGridData();
      }
    } else {
      showToastService(this.apiResponse.message, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
    }
  }

  @action.bound
  postDeleteCall() {
    if (this.apiResponse.successStatus) {
      this.searchTradeIndependentAmountStore.tradeIndependentAmountData = this.searchTradeIndependentAmountStore.tradeIndependentAmountData.filter(
        (x) =>
          x.tradeId !== this.tradeIdKey || x.custodianAccount?.id !== this.custodianAccountKey
      );
      this.searchTradeIndependentAmountStore.tradeIndependentAmountGridData = this.searchTradeIndependentAmountStore.tradeIndependentAmountGridData.filter(
        (x) =>
          x.tradeId !== this.tradeIdKey
      );
      showToastService(
        MESSAGES.IA_DETAIL_INVALIDATED_SUCCESSFULLY,
        TOAST_TYPE.SUCCESS,
        ToastService.Placement.TOP_RIGHT, 7000
      );
    } else {
      showToastService(this.apiResponse.message, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
    }
  }

  updateIndependentAmountInfo = async (modifyIndependentAmountParam: ModifyIndependentAmountParam) => {
    try {
      this.inProgress = true;
      this.apiResponse = await updateIndependentAmountDetail(modifyIndependentAmountParam);
      this.postSaveCall();
      this.inProgress = false;
    } catch (error) {
      showToastService(error.message, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000)
      this.inProgress = false;
    }
  };

  invalidateIndependentAmountInfo = async (modifyIndependentAmountParam: ModifyIndependentAmountParam) => {
    try {
      this.inProgress = true;
      this.apiResponse = await invalidateIndependentAmountDetail(modifyIndependentAmountParam);
      this.postDeleteCall();
      this.inProgress = false;
    } catch (error) {
      showToastService(error.message, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000)
      this.inProgress = false;
    }
  };
}

export default ModifyTradeIndependentAmountStore;
