import React from "react";
import { useStores } from "../useStores"
import { observer } from "mobx-react-lite";
import IndependentAmountFormComponent from "../component/IndependentAmountFormComponent";
import { Layout, Dialog } from "arc-react-components";

const UpdateIndependentAmountDialog: React.FC = () => {
  const { rootStore } = useStores()
  const { modifyTradeIndependentAmountStore } = rootStore

  const render = () => {
    return (
      <>
        <Dialog
          title={modifyTradeIndependentAmountStore.dialogTitle}
          isOpen={modifyTradeIndependentAmountStore.isOpenUpdateIADialog}
          onClose={modifyTradeIndependentAmountStore.onCloseUpdateIADialog}
          style={{ width: modifyTradeIndependentAmountStore.setDialogWidth ? "30%" : null }}
          footer={
            <Layout>
              <Layout.Child childId="UpdateIndependentAmountDialogChild1">
                {modifyTradeIndependentAmountStore.buttonsData.showSave && (
                  <button onClick={modifyTradeIndependentAmountStore.onSave}>
                    Save
                  </button>
                )}

                {modifyTradeIndependentAmountStore.buttonsData.showReset && (
                  <button onClick={modifyTradeIndependentAmountStore.onResetFilters}>
                    Reset
                  </button>
                )}
                <button onClick={modifyTradeIndependentAmountStore.onCloseUpdateIADialog}>
                  Cancel
                </button>
              </Layout.Child>
            </Layout>
          }
        >
          <Layout>
            <Layout.Child
              childId="UpdateIndependentAmountDialogChild2"
              title={modifyTradeIndependentAmountStore.dialogSubTitle}
              showHeader={true}
              className="form padding--left"
            >
              <IndependentAmountFormComponent
                onSelect={modifyTradeIndependentAmountStore.onSelect}
                popUpSelectedData={modifyTradeIndependentAmountStore.popUpSelectedData}
              />
            </Layout.Child>
          </Layout>
        </Dialog>
      </>
    );
  };

  return <>{render()}</>;
};

export default observer(UpdateIndependentAmountDialog);
