import React from "react";
import { useStores } from "../useStores"
import { observer } from "mobx-react-lite";
import { Dialog } from "arc-react-components";

const InvalidateIndependentAmountDialog: React.FC = () => {
  const { rootStore } = useStores();
  const { modifyTradeIndependentAmountStore } = rootStore;

  return (
    <Dialog
      title={modifyTradeIndependentAmountStore.dialogTitle}
      isOpen={modifyTradeIndependentAmountStore.isOpenDeleteIADialog}
      onClose={modifyTradeIndependentAmountStore.onCloseDeleteIADialog}
      style={{ width: modifyTradeIndependentAmountStore.setDialogWidth ? "15%" : null }}
      footer={
        <>
          <button onClick={modifyTradeIndependentAmountStore.onConfirmDelete}>Delete</button>
          <button onClick={modifyTradeIndependentAmountStore.onCloseDeleteIADialog}>
            Cancel
          </button>
        </>
      }
    >
      <text>Do you really want to delete the row with TradeId:{modifyTradeIndependentAmountStore.tradeIdKey} ?</text>
    </Dialog>
  );
};

export default observer(InvalidateIndependentAmountDialog);
