import React from "react";
import { observer } from "mobx-react-lite";
import { Card, ToggleGroup, DatePicker, DateRangePicker, Sidebar } from "arc-react-components";
import CpeFilter from "../../../commons/container/CpeFilter";
import CurrencyFilter from "../../../commons/container/CurrencyFilter";
import AgreementTypeFilter from "../../../commons/container/AgreementTypeFilter";
import CustodianAccountFilter from "../../../commons/container/CustodianAccountFilter";
import GBOTypeFilter from "../../../commons/container/GBOTypeFilter";
import ArcDateFilter from "../../../commons/container/ArcDateFilter";
import SaveSettingsManager from "../../../commons/components/SaveSettingsManager";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import { useStores } from "../useStores";
import InputFilter from '../../../commons/components/InputFilter';
import {
  STATUS_TYPE_REF_DATA
} from "../constants";
import CustomPnlSpnFilter from "../../../MarginRules/component/CustomPnlSpnFilter";
import FilterButton from "../../../commons/components/FilterButton";
import MultiSelectFilter from "../../../commons/filters/components/MultiSelectFilter";


const TradeIndependentAmountSideBar: React.FC = () => {
  const { rootStore } = useStores();
  const searchTradeIndependentAmountStore = rootStore.searchTradeIndependentAmountStore;

  const searchElement =
    <FilterButton
      onClick={searchTradeIndependentAmountStore.handleSearch}
      label="Search"
    />

  const resetElement =
    <FilterButton
      reset={true}
      onClick={searchTradeIndependentAmountStore.handleReset}
      label="Reset"
    />

  const copySearchElement =
    <FilterButton
      reset={true}
      onClick={searchTradeIndependentAmountStore.handleCopySearch}
      label="Copy Search URL"
      className="float--left button--tertiary margin--small"
    />

  const saveSettingsManagerElement =
    <SaveSettingsManager
      selectedFilters={searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter}
      applySavedFilters={searchTradeIndependentAmountStore.applySavedFilters}
      applicationName="TradeIndependentAmountFilter"
    />

  const startDateElement =
    <ArcDateFilter
      stateKey="selectedStartDate"
      label="Start Date"
      onSelect={searchTradeIndependentAmountStore.onSelect}
      data={
        searchTradeIndependentAmountStore.
          searchTradeIndependentAmountFilter.selectedStartDate
      }
      horizontalLayout
    />

  const endDateElement =
    <ArcDateFilter
      stateKey="selectedEndDate"
      label="End Date"
      onSelect={searchTradeIndependentAmountStore.onSelect}
      data={
        searchTradeIndependentAmountStore.
          searchTradeIndependentAmountFilter.selectedEndDate
      }
      horizontalLayout
    />

  const legalEntityFilterElement =
    <MultiSelectFilter
      data={searchTradeIndependentAmountStore.selectedLegalEntityData}
      onSelect={searchTradeIndependentAmountStore.onSelect}
      label="Legal Entities"
      selectedData={searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter.selectedLegalEntities}
      stateKey="selectedLegalEntities"
      horizontalLayout
      multiSelect
    />

  const agreementTypeElement =
    <AgreementTypeFilter
      onSelect={searchTradeIndependentAmountStore.onSelect}
      label="Agreement Types"
      selectedData={searchTradeIndependentAmountStore.
        searchTradeIndependentAmountFilter.selectedAgreementTypes}
      horizontalLayout
      multiSelect
    />

  const cpeFilterElement =
    <CpeFilter
      label="Exposure Counterparties"
      onSelect={searchTradeIndependentAmountStore.onSelect}
      selectedData={searchTradeIndependentAmountStore.
        searchTradeIndependentAmountFilter.selectedCpes}
      multiSelect
      horizontalLayout
    />

  const bookFilterElement =
    <MultiSelectFilter
      data={searchTradeIndependentAmountStore.selectedBookData}
      onSelect={searchTradeIndependentAmountStore.onSelect}
      label="Books"
      selectedData={searchTradeIndependentAmountStore.
        searchTradeIndependentAmountFilter.selectedBooks}
      stateKey="selectedBooks"
      horizontalLayout
      multiSelect
    />

  const statusTypeFilterElement =
    <SingleSelectFilter
      label="Status Type"
      data={STATUS_TYPE_REF_DATA}
      onSelect={searchTradeIndependentAmountStore.onSelect}
      selectedData={searchTradeIndependentAmountStore.
        searchTradeIndependentAmountFilter.selectedIndependentAmountStatus}
      stateKey="selectedIndependentAmountStatus"
      horizontalLayout
    />

  const gboTypeFilterElement =
    <GBOTypeFilter
      label="GBO Types"
      onSelect={searchTradeIndependentAmountStore.onSelect}
      horizontalLayout={true}
      selectedData={searchTradeIndependentAmountStore.
        searchTradeIndependentAmountFilter.selectedGboTypes}
      multiSelect
    />

  const currencyFilterElement =
    <CurrencyFilter
      onSelect={searchTradeIndependentAmountStore.onSelect}
      label="Currencies"
      selectedData={searchTradeIndependentAmountStore.
        searchTradeIndependentAmountFilter.selectedCurrencies}
      horizontalLayout
      multiSelect
    />

  const custodianAccountFilterElement =
    <CustodianAccountFilter
      onSelect={searchTradeIndependentAmountStore.onSelect}
      label="Custodian Account"
      selectedData={searchTradeIndependentAmountStore.
        searchTradeIndependentAmountFilter.selectedCustodianAccounts}
      horizontalLayout
      multiSelect
    />

  let toggleElement;
  if (searchTradeIndependentAmountStore.toggleBtn) {
    searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter.selectedTradeIds = ""
    toggleElement =
      <CustomPnlSpnFilter
        label="P&L SPNs"
        stateKey="selectedPnlSpns"
        data={searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter.selectedPnlSpns}
        onSelect={searchTradeIndependentAmountStore.onSelect}
      />
  }

  else if (!searchTradeIndependentAmountStore.toggleBtn) {
    searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter.selectedPnlSpns = ""
    toggleElement =
      <InputFilter
        label="Trade IDs"
        stateKey="selectedTradeIds"
        data={searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter.selectedTradeIds}
        onSelect={searchTradeIndependentAmountStore.onSelect}
      />
  }

  return (
    <Sidebar
      header={false}
      footer={
        <>
          {searchElement}
          {resetElement}
          {copySearchElement}
        </>
      }
    >
      <Card>
        {saveSettingsManagerElement}
      </Card>
      <Card>
        {startDateElement}
        {endDateElement}
      </Card>
      <Card>
        {legalEntityFilterElement}
        {agreementTypeElement}
        {cpeFilterElement}
        {statusTypeFilterElement}
        {bookFilterElement}
        {gboTypeFilterElement}
        {currencyFilterElement}
        {custodianAccountFilterElement}
      </Card>
      <Card>
        <label>Search By:</label>
        <br></br>
        <ToggleGroup
          value={searchTradeIndependentAmountStore.toggleBtn}
          onChange={(value: boolean) => {
            searchTradeIndependentAmountStore.onToggleButtonChange(value);
          }}
        >
          <ToggleGroup.Button key="0" data={true}>
            P&L SPNs
          </ToggleGroup.Button>
          <ToggleGroup.Button key="1" data={false}>
            Trade IDs
          </ToggleGroup.Button>
        </ToggleGroup>
        {toggleElement}
      </Card>
    </Sidebar>
  );
};

export default observer(TradeIndependentAmountSideBar
);
