import ArcWatchListDataGrid from "ArcWatchListDataGrid";
import React from "react";
import { observer } from "mobx-react-lite";
import { getGridColumns, getWatchListKeyColumns, getWatchListRowDate } from "../grid/gridColumns";
import { TradeIndependentAmountGridConfig } from "../grid/gridConfig";
import Message from "../../../commons/components/Message";
import { SUCCESS, FAILURE } from "../../../commons/constants";
import { DEFAULT_PINNED_COLUMNS } from "../constants";
import { useStores } from "../useStores";
import { Layout } from "arc-react-components";
import UpdateIndependentAmountDialog from "./UpdateIndependentAmountDialog";
import InvalidateIndependentAmountDialog from "./InvalidateIndependentAmountDialog";

const TradeIndependentAmountGrid: React.FC = () => {
  const { rootStore } = useStores()
  const { searchTradeIndependentAmountStore } = rootStore;

  const renderGridData = () => {
    if (searchTradeIndependentAmountStore.tradeIndependentAmountGridData.length) {
      return (
        <>
          <ArcWatchListDataGrid
            rows={searchTradeIndependentAmountStore.tradeIndependentAmountGridData}
            columns={getGridColumns()}
            displayColumns={searchTradeIndependentAmountStore.displayColumns}
            pinnedColumns={DEFAULT_PINNED_COLUMNS}
            onDisplayColumnsChange={searchTradeIndependentAmountStore.onDisplayColumnsChange}
            configurations={TradeIndependentAmountGridConfig}
            watchListStartDate={searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter.selectedStartDate}
            watchListEndDate={searchTradeIndependentAmountStore.searchTradeIndependentAmountFilter.selectedEndDate}
            watchListType="tradeIndependentAmountGrid"
            watchListKeyMapper={getWatchListKeyColumns}
            getRowDate={getWatchListRowDate}
          />
        </>
      );
    } else {
      return (
        <Message
          messageData={searchTradeIndependentAmountStore.searchStatus.message}
          error={searchTradeIndependentAmountStore.searchStatus.status == FAILURE}
          warning={searchTradeIndependentAmountStore.searchStatus.status == SUCCESS}
        />
      );
    }
  };

  return (
    <>
      <Layout>
        <Layout.Child childId="TradeIndependentAmountGridChild1">{renderGridData()}</Layout.Child>
      </Layout>
      <UpdateIndependentAmountDialog />
      <InvalidateIndependentAmountDialog />
    </>
  );

};

export default observer(TradeIndependentAmountGrid);
