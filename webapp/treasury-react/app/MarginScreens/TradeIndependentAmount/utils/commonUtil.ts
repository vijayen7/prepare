import { ToastService } from "arc-react-components";
import { TOAST_TYPE } from "../constants"

const getToastType = (type: string) => {
  let toastType: any = ToastService.ToastType.SUCCESS;
  switch (type) {
    case TOAST_TYPE.CRITICAL:
      toastType = ToastService.ToastType.CRITICAL;
      break;
    case TOAST_TYPE.SUCCESS:
      toastType = ToastService.ToastType.SUCCESS;
      break;
    case TOAST_TYPE.INFO:
      toastType = ToastService.ToastType.INFO;
      break;
  }
  return toastType;
};

export const showToastService = (content: string, type: string, placement: any, dismissTime: any) => {
  ToastService.append({
    content: content,
    type: getToastType(type),
    placement: placement,
    dismissTime: dismissTime,
  });
};

export const getDefaultDate = () => {
  const defaultStartDate = new Date('1900-01-01');
  let formattedStartDate = defaultStartDate.toISOString().substring(0, 10);
  return formattedStartDate;
}
