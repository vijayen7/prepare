import { PopUpSelectedData } from "../models/PopUpSelectedData";
import { TradeIndependentAmountData } from "../models/TradeIndependentAmountData"
import { getParsedDate } from "../../../commons/TreasuryUtils";
import { INDEPENDENT_AMOUNT_SOURCE, INDEPENDENT_AMOUNT_TYPE } from "../constants";
import { ModifyIndependentAmountParam } from "../models/ModifyIndependentAmountParam";
import { CLASS_NAMESPACE } from "../../../commons/ClassConfigs";
import { getDefaultDate } from "../utils/commonUtil";

export const getDialogDataForIndependentAmount = (trade: TradeIndependentAmountData) => {
  let IACurrency = trade.independentAmountCurrency;
  let IACurrencyId = IACurrency?.id;
  let IACurrencyAbbrev = IACurrency?.abbrev;
  let dialogData: PopUpSelectedData = {
    selectedIndependentAmountType: trade.independentAmountType ?
      { key: INDEPENDENT_AMOUNT_TYPE[trade.independentAmountType], value: trade.independentAmountType } : null,
    selectedIndependentAmountValue: trade.independentAmountValue ? trade.independentAmountValue : 0,
    selectedIndependentAmountCurrency: IACurrency && IACurrencyId && IACurrencyAbbrev ? { key: IACurrencyId, value: IACurrencyAbbrev } : null,
    selectedIsIndependentAmountNegative: trade.isIndependentAmountNegative,
    selectedIndependentAmountEffectiveDate: trade.independentAmountEffectiveDate ? getParsedDate(trade.independentAmountEffectiveDate) : getDefaultDate(),
    selectedComment: trade.comment ? trade.comment : "",
    selectedIndependentAmountSource: trade.independentAmountSource ?
      { key: INDEPENDENT_AMOUNT_SOURCE[trade.independentAmountSource], value: trade.independentAmountSource } : null,
  }
  return dialogData;
}

export const getIAInfoFromDialogData = (dialogData: PopUpSelectedData,
  updateTradeId: string, updateCustodianAccountId: number | undefined) => {
  let modifyIndependentAmountParam: ModifyIndependentAmountParam = {
    tradeId: updateTradeId,
    typeId: dialogData.selectedIndependentAmountType?.key,
    value: dialogData.selectedIndependentAmountValue,
    currencySpn: dialogData.selectedIndependentAmountCurrency?.key,
    isNegative: dialogData.selectedIsIndependentAmountNegative,
    effectiveDate: dialogData.selectedIndependentAmountEffectiveDate,
    comment: dialogData.selectedComment,
    sourceId: dialogData.selectedIndependentAmountSource?.key,
    custodianAccountId: updateCustodianAccountId,
    "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.margin.model.ModifyIndependentAmountParam
  }
  return modifyIndependentAmountParam;
}
