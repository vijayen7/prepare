import {
  getCommaSeparatedNumber, roundWithAtmostDecimal,
  convertJavaNYCDateTime, getSelectedIdListWithNull, getFilterListFromCommaSeparatedString,
  getCommaSeparatedKeysFromFilterList
} from "../../../commons/util";
import { SearchTradeIndependentAmountFilter } from
  "../models/SearchTradeIndependentAmountFilter"
import { TradeIndependentAmountGridData } from "../models/TradeIndependentAmountGridData"
import { ToastService } from "arc-react-components";
import { TradeIndependentAmountFilter } from "../models/TradeIndependentAmountFilter";
import { TradeIndependentAmountData } from "../models/TradeIndependentAmountData";
import { CLASS_NAMESPACE } from "../../../commons/ClassConfigs"
import MapData from "../../../commons/models/MapData";
import queryString from "query-string";


import {
  showToastService
} from "./commonUtil";

import { MESSAGES, INDEPENDENT_AMOUNT_STATUS, TOAST_TYPE, NOT_APPLICABLE, SPN_AND_TRADE_ID_DELIMETER, ISDA_AGREEMENT_TYPE_ID } from "../constants";
import { getAbbrevFromRefData, getNameFromRefData, getParsedDate } from "../../../commons/TreasuryUtils";

export const getParsedPnlSpns = (pnlSpn: string) => {
  let spn_delimiter;
  if (pnlSpn.includes(SPN_AND_TRADE_ID_DELIMETER.SEMI_COLON)) {
    spn_delimiter = SPN_AND_TRADE_ID_DELIMETER.SEMI_COLON
  } else if (pnlSpn.includes(SPN_AND_TRADE_ID_DELIMETER.SPACE)) {
    spn_delimiter = SPN_AND_TRADE_ID_DELIMETER.SPACE
  }
  let pnlSpns: string[] = pnlSpn.split(spn_delimiter);
  let parsedPnlSpns: number[] = [];
  pnlSpns.forEach((pnlSpn) => {
    if (pnlSpn != "") {
      parsedPnlSpns.push(parseInt(pnlSpn.trim()));
    }
  });
  return parsedPnlSpns;
};

export const getParsedTradeIds = (tradeId: string) => {
  let trade_id_delimiter;
  if (tradeId.includes(SPN_AND_TRADE_ID_DELIMETER.SEMI_COLON)) {
    trade_id_delimiter = SPN_AND_TRADE_ID_DELIMETER.SEMI_COLON
  } else if (tradeId.includes(SPN_AND_TRADE_ID_DELIMETER.SPACE)) {
    trade_id_delimiter = SPN_AND_TRADE_ID_DELIMETER.SPACE
  }
  let tradeIds: string[] = tradeId.split(trade_id_delimiter);
  let parsedTradeIds: string[] = [];
  tradeIds.forEach((tradeId) => {
    if (tradeId != "") {
      parsedTradeIds.push(tradeId.trim());
    }
  });
  return parsedTradeIds;
};

export const isValidInputData = (endDate: string, pnlSpn: string, tradeId: string, agreementTypeData: MapData[]) => {
  let spn_delimiter, trade_id_delimiter;
  if (pnlSpn.includes(SPN_AND_TRADE_ID_DELIMETER.SEMI_COLON)) {
    spn_delimiter = SPN_AND_TRADE_ID_DELIMETER.SEMI_COLON
  } else if (pnlSpn.includes(SPN_AND_TRADE_ID_DELIMETER.SPACE)) {
    spn_delimiter = SPN_AND_TRADE_ID_DELIMETER.SPACE
  }
  if (tradeId.includes(SPN_AND_TRADE_ID_DELIMETER.SEMI_COLON)) {
    trade_id_delimiter = SPN_AND_TRADE_ID_DELIMETER.SEMI_COLON
  } else if (tradeId.includes(SPN_AND_TRADE_ID_DELIMETER.SPACE)) {
    trade_id_delimiter = SPN_AND_TRADE_ID_DELIMETER.SPACE
  }
  let pnlSpns: string[] = pnlSpn.split(spn_delimiter);
  let tradeIds: string[] = tradeId.split(trade_id_delimiter);
  let regexp = new RegExp("^TE[0-9]+$")
  let returnVal: boolean = true;
  if (endDate === "") {
    showToastService(MESSAGES.INVALID_DATE_INPUT, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
    returnVal = false;
  }

  if (returnVal) {
    pnlSpns.forEach((spn) => {
      if (spn != "" && isNaN(+(spn.trim()))) {
        returnVal = false;
      }
    });
    if (!returnVal) {
      showToastService(MESSAGES.INVALID_PNL_SPN_MESSAGE, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
    }
  }

  if (returnVal) {
    tradeIds.forEach((tradeId) => {
      if (tradeId != "" && !tradeId.trim().match(regexp)) {
        returnVal = false;
      }
    });

    if (!returnVal) {
      showToastService(MESSAGES.INVALID_TRADE_ID_MESSAGE, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
    }
  }

  if (returnVal) {
    if (agreementTypeData.length !== 0 && !agreementTypeData.some(agreement => agreement.key === ISDA_AGREEMENT_TYPE_ID)) {
      returnVal = false
      showToastService(MESSAGES.INVALID_AGREEMENT_MESSAGE, TOAST_TYPE.CRITICAL, ToastService.Placement.TOP_RIGHT, 7000);
    }
  }
  return returnVal;
};

export const getTradeIndependentAmountFilter = (searchTradeIndependentAmountFilter: SearchTradeIndependentAmountFilter) => {
  let tradeIndependentAmountFilter: TradeIndependentAmountFilter = {
    startDate: searchTradeIndependentAmountFilter.selectedStartDate,
    endDate: searchTradeIndependentAmountFilter.selectedEndDate,
    legalEntityIds: getSelectedIdListWithNull(searchTradeIndependentAmountFilter.selectedLegalEntities),
    counterPartyIds: getSelectedIdListWithNull(searchTradeIndependentAmountFilter.selectedCpes),
    gboTypeIds: getSelectedIdListWithNull(searchTradeIndependentAmountFilter.selectedGboTypes),
    currencyIds: getSelectedIdListWithNull(searchTradeIndependentAmountFilter.selectedCurrencies),
    bookIds: getSelectedIdListWithNull(searchTradeIndependentAmountFilter.selectedBooks),
    independentAmountStatus: INDEPENDENT_AMOUNT_STATUS[searchTradeIndependentAmountFilter.selectedIndependentAmountStatus.value],
    custodianAccountsIds: getSelectedIdListWithNull(
      searchTradeIndependentAmountFilter.selectedCustodianAccounts
    ),
    pnlSpns: [],
    tradeIds: [],
    "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.margin.model.TradeIndependentAmountFilter
  };

  if (searchTradeIndependentAmountFilter.selectedPnlSpns != "") {
    tradeIndependentAmountFilter.pnlSpns = getParsedPnlSpns(searchTradeIndependentAmountFilter.selectedPnlSpns);
  }

  if (searchTradeIndependentAmountFilter.selectedTradeIds != "") {
    tradeIndependentAmountFilter.tradeIds = getParsedTradeIds(searchTradeIndependentAmountFilter.selectedTradeIds);
  }

  return tradeIndependentAmountFilter;
};

export const getGridDataFromTradeIndependentAmountData = (
  tradeIndependentAmountData: TradeIndependentAmountData[]
) => {
  let gridDataList: TradeIndependentAmountGridData[] = [];
  tradeIndependentAmountData.forEach((tradeIndependentAmount) => {
    let gridData: TradeIndependentAmountGridData = {
      tradeId: tradeIndependentAmount.tradeId,
      pnlSpn: tradeIndependentAmount.pnlSpn,
      tradeDate: getParsedDate(tradeIndependentAmount.tradeDate),
      tradeTime: convertJavaNYCDateTime(tradeIndependentAmount.tradeTime),
      description: tradeIndependentAmount.description,
      legalEntity: getNameFromRefData(tradeIndependentAmount.legalEntity),
      counterPartyEntity: getNameFromRefData(tradeIndependentAmount.counterPartyEntity),
      executingBroker: getNameFromRefData(tradeIndependentAmount.executingBroker),
      primeBroker: getNameFromRefData(tradeIndependentAmount.primeBroker),
      book: getAbbrevFromRefData(tradeIndependentAmount.book),
      bundle: getNameFromRefData(tradeIndependentAmount.bundle),
      gboType: getNameFromRefData(tradeIndependentAmount.gboType),
      currency: getAbbrevFromRefData(tradeIndependentAmount.currency),
      price: tradeIndependentAmount.price,
      quantity: tradeIndependentAmount.quantity,
      independentAmountType: tradeIndependentAmount.independentAmountType,
      independentAmountValue: tradeIndependentAmount.independentAmountValue,
      independentAmountCurrency: tradeIndependentAmount.independentAmountCurrency ?
        getAbbrevFromRefData(tradeIndependentAmount.independentAmountCurrency) : NOT_APPLICABLE,
      isIndependentAmountNegative: tradeIndependentAmount.isIndependentAmountNegative,
      independentAmountEffectiveDate: tradeIndependentAmount.independentAmountEffectiveDate ? getParsedDate(
        tradeIndependentAmount.independentAmountEffectiveDate) : NOT_APPLICABLE,
      comment: tradeIndependentAmount.comment ? tradeIndependentAmount.comment : NOT_APPLICABLE,
      independentAmountSource: tradeIndependentAmount.independentAmountSource,
      userName: tradeIndependentAmount.userName ? tradeIndependentAmount.userName : NOT_APPLICABLE,
      independentAmountUnderlying: tradeIndependentAmount.independentAmountUnderlying,
      independentAmount: tradeIndependentAmount.independentAmount,
      trader: getAbbrevFromRefData(tradeIndependentAmount.trader),
      custodianAccount: getNameFromRefData(tradeIndependentAmount.custodianAccount),
      independentAmountStatus: tradeIndependentAmount.independentAmountStatus,
    };
    gridDataList.push(gridData);
  });
  return gridDataList;
};

export const getFiltersFromUrl = (url: any) => {
  const values = queryString.parse(url);
  let filters = {
    selectedStartDate: values.startDate,
    selectedEndDate: values.endDate,
    selectedLegalEntities: getFilterListFromCommaSeparatedString(values.legalEntityIds),
    selectedCpes: getFilterListFromCommaSeparatedString(values.counterPartyIds),
    selectedGboTypes: getFilterListFromCommaSeparatedString(values.gboTypeIds),
    selectedCurrencies: getFilterListFromCommaSeparatedString(values.currencyIds),
    selectedBooks: getFilterListFromCommaSeparatedString(values.bookIds),
    selectedIndependentAmountStatus: values.independentAmountStatus !== "ALL" ?
      { key: INDEPENDENT_AMOUNT_STATUS[values.independentAmountStatus], value: values.independentAmountStatus } :
      { key: -1, value: "ALL" },
    selectedCustodianAccounts: getFilterListFromCommaSeparatedString(values.custodianAccountsIds),
    selectedPnlSpns: values.pnlSpns,
    selectedTradeIds: values.tradeIds,
  }
  return filters;
}

export const getURLFromSelectedFilters = (params: SearchTradeIndependentAmountFilter, location: any) => {
  let url = location + `/treasury/margin/tradeIndependentAmount.html?
    startDate=${params.selectedStartDate}
    &endDate=${params.selectedEndDate}
    &legalEntityIds=${getCommaSeparatedKeysFromFilterList(params.selectedLegalEntities)}
    &counterPartyIds=${getCommaSeparatedKeysFromFilterList(params.selectedCpes)}
    &gboTypeIds=${getCommaSeparatedKeysFromFilterList(params.selectedGboTypes)}
    &currencyIds=${getCommaSeparatedKeysFromFilterList(params.selectedCurrencies)}
    &bookIds=${getCommaSeparatedKeysFromFilterList(params.selectedBooks)}
    &independentAmountStatus=${params.selectedIndependentAmountStatus.value}
    &custodianAccountsIds=${getCommaSeparatedKeysFromFilterList(params.selectedCustodianAccounts)}
    &pnlSpns=${params.selectedPnlSpns}
    &tradeIds=${params.selectedTradeIds}`;

  return url;
}
