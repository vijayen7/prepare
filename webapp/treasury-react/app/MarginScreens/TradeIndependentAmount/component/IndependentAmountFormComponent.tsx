import React from "react";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import InputFilter from "../../../commons/components/InputFilter";
import DateFilter from "../../../commons/filters/components/DateFilter";
import CurrencyFilter from "../../../commons/container/CurrencyFilter";
import {
  INDEPENDENT_AMOUNT_TYPE_DATA,
  INDEPENDENT_AMOUNT_SOURCE_DATA
} from "../constants";
import { useStores } from "../useStores";
import { observer } from "mobx-react-lite";
import { IndependentAmountFormComponentProps } from "../models/IndependentAmountFormComponentProps";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";

const IndependentAmountFormComponent: React.FC<IndependentAmountFormComponentProps> = ({
  onSelect,
  popUpSelectedData,
}) => {

  const { rootStore } = useStores()
  const modifyTradeIndependentAmountStore = rootStore.modifyTradeIndependentAmountStore;

  return (
    <>
      <SingleSelectFilter
        horizontalLayout={true}
        label="IA Type*"
        data={INDEPENDENT_AMOUNT_TYPE_DATA}
        onSelect={onSelect}
        stateKey="selectedIndependentAmountType"
        selectedData={popUpSelectedData.selectedIndependentAmountType}
      />
      <InputFilter
        label="IA Value"
        data={popUpSelectedData.selectedIndependentAmountValue}
        onSelect={onSelect}
        stateKey="selectedIndependentAmountValue"
      />
      <CurrencyFilter
        label="IA Currency*"
        onSelect={onSelect}
        stateKey="selectedIndependentAmountCurrency"
        selectedData={popUpSelectedData.selectedIndependentAmountCurrency}
        horizontalLayout
        multiSelect={false}
      />
      <br></br>
      <CheckboxFilter
        label="Is IA Negative"
        defaultChecked={modifyTradeIndependentAmountStore.popUpSelectedData.selectedIsIndependentAmountNegative}
        onSelect={onSelect}
        stateKey="selectedIsIndependentAmountNegative"
      />
      <DateFilter
        label="IA Effective Date*"
        stateKey="selectedIndependentAmountEffectiveDate"
        data={popUpSelectedData.selectedIndependentAmountEffectiveDate}
        onChange={onSelect}
      />
      <InputFilter
        label="Comment"
        data={popUpSelectedData.selectedComment}
        onSelect={onSelect}
        stateKey="selectedComment"
      />
      <SingleSelectFilter
        horizontalLayout={true}
        label="IA Source*"
        data={INDEPENDENT_AMOUNT_SOURCE_DATA}
        onSelect={onSelect}
        stateKey="selectedIndependentAmountSource"
        selectedData={popUpSelectedData.selectedIndependentAmountSource}
      />
    </>
  );
};

export default observer(IndependentAmountFormComponent);
