import ArcDataGrid from "arc-data-grid";

export const TradeIndependentAmountGridConfig: ArcDataGrid.GridConfiguration = {
  clickableRows: true,
  showColumnAggregationRow : true,
  getExportFileName: ({ fileType, all }) => {
    return `${all ? "Trade-Independent-Amount" : "Trade-Independent-Amount-Current-View"}`;
  },
  timezone: "Asia/Calcutta",
};
