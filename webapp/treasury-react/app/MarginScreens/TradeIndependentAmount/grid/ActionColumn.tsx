import React from "react";
import { observer } from "mobx-react-lite";
import { useStores } from "../useStores";
import { IA_STATUS } from "../constants";
const ActionColumn: React.FC<any> = ({ context }) => {
  const { rootStore } = useStores()
  const modifyTradeIndependentAmountStore = rootStore.modifyTradeIndependentAmountStore;

  const renderActionColumn = () => {
    if (context.row.independentAmountStatus === IA_STATUS.MAPPED || context.row.independentAmountStatus === IA_STATUS.CONFLICTING) {
      return (
        <>
          <i
            title="Edit"
            className="icon-edit--block"
            onClick={(e) => {
              e.stopPropagation();
              modifyTradeIndependentAmountStore.onUpdateIndependentAmountData(
                context.row.tradeId
              );
            }}
          ></i>
          <i
            title="Delete"
            className="icon-delete"
            onClick={(e) => {
              e.stopPropagation();
              modifyTradeIndependentAmountStore.onDeleteIndependentAmountData(
                context.row.tradeId
              );
            }}
          ></i>
        </>
      );
    } else if (context.row.independentAmountStatus === IA_STATUS.UNMAPPED) {
      return (
        <>
          <i
            title="Edit"
            className="icon-edit--block"
            onClick={(e) => {
              e.stopPropagation();
              modifyTradeIndependentAmountStore.onUpdateIndependentAmountData(
                context.row.tradeId
              );
            }}
          ></i>
        </>
      );
    }
    return <></>;
  };
  return <>{renderActionColumn()}</>;
};

export default observer(ActionColumn);
