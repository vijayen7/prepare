import ArcDataGrid from "arc-data-grid";
import { TradeIndependentAmountGridData } from "../models/TradeIndependentAmountGridData";
import ActionColumn from "./ActionColumn";
import React from "react";
import { getFormattedNumericColumn } from "../../../commons/TreasuryUtils";
import { SECURITY_SEARCH_URL, TRADE_BLOTTER_URL } from "../../../commons/constants";

const goToSecuritySearch = (value: number) => {
  let url = SECURITY_SEARCH_URL + value;
  window.open(url, "_blank");
}

const goToTradeBlotter = (value: number) => {
  let url = TRADE_BLOTTER_URL + value;
  window.open(url, "_blank");
}

const TradeIndependentAmountGridcolumns: ArcDataGrid.Columns<TradeIndependentAmountGridData, number> = [
  "tradeDate",
  {
    identity: "tradeId",
    header: "Trade ID",
    Cell: (value, _) => {
      return (
        <a
          onClick={(e) => {
            e.stopPropagation();
            goToTradeBlotter(value);
          }}
        >
          <>
            {value}
          </>
        </a>
      );
    },
  },
  {
    identity: "pnlSpn",
    header: "P&L SPN",
    Cell: (value, _) => {
      return (
        <a
          onClick={(e) => {
            e.stopPropagation();
            goToSecuritySearch(value);
          }}
        >
          <>
            {value}
          </>
        </a>
      );
    },
  },
  "description",
  "legalEntity",
  {
    identity: "counterPartyEntity",
    header: "Exposure Counterparty",
  },
  "executingBroker",
  "primeBroker",
  "book",
  "bundle",
  {
    identity: "gboType",
    header: "GBO Type",
  },
  "currency",
  getFormattedNumericColumn("price", "Price", 100, undefined, 2),
  getFormattedNumericColumn("quantity", "Quantity", 100, undefined, 2),
  {
    identity: "independentAmountType",
    header: "IA Type",
  },
  getFormattedNumericColumn("independentAmountValue", "IA Value", 120, undefined, 2),
  {
    identity: "independentAmountCurrency",
    header: "IA Currency",
  },
  {
    identity: "isIndependentAmountNegative",
    header: "Is IA Negative",
  },
  {
    identity: "independentAmountEffectiveDate",
    header: "IA Effective Date",
  },
  "comment",
  {
    identity: "independentAmountSource",
    header: "IA Source",
  },
  "userName",
  getFormattedNumericColumn("independentAmountUnderlying", "IA underlying", 120, undefined, 2),
  getFormattedNumericColumn("independentAmount", "IndependentAmount", 120, undefined, 2),
  "trader",
  "custodianAccount",
  "tradeTime",
  {
    identity: "independentAmountStatus",
    header: "Status Type",
  },
];

export const getGridColumns = () => {
  return [
    {
      identity: "actions",
      Cell: (value, context) => <ActionColumn context={context} />,
      disableFilters: true,
      disableSearches: true,
      disableSortBy: true,
      disableExport: true,
      disableResizing: false,
      width: 10
    },
    ...TradeIndependentAmountGridcolumns,
  ];
};


export const getWatchListKeyColumns = (row: TradeIndependentAmountGridData) => {
  return [
    {
      key: "tradeId",
      value: String(row["tradeId"])
    },
  ];
}

export const getWatchListRowDate = (row: TradeIndependentAmountGridData): string => {
  return String(row["tradeDate"]);
}

