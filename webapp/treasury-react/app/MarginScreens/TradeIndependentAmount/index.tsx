import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../../commons/components/ReactLoader";
import Loader from "../../commons/container/Loader";
import TradeIndependentAmountSideBar from "../TradeIndependentAmount/container/TradeIndependentAmountSideBar";
import TradeIndependentAmountGrid from "../TradeIndependentAmount/container/TradeIndependentAmountGrid";
import { TradeIndependentAmountProps } from "../TradeIndependentAmount/models/TradeIndependentAmountProps";
import AppHeader, { whenHeaderRef } from "arc-header";
import { Breadcrumbs, Layout } from "arc-react-components";
import { COMPONENT_URLS } from "../../commons/UrlConfigs";
import { useStores } from "./useStores";
import { INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA } from "./constants";
import { getFiltersFromUrl } from "./utils/searchTradeIndependentAmountUtil";

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={COMPONENT_URLS.TRADE_INDEPENDENT_AMOUNT} key="TradeIndependentAmount">
        Trade Independent Amount
      </Breadcrumbs.Item>
    </>
  );
};

const TradeIndependentAmount: React.FC<TradeIndependentAmountProps> = (props: TradeIndependentAmountProps) => {
  const { rootStore } = useStores()
  const { searchTradeIndependentAmountStore } = rootStore;

  useEffect(() => {
    let url = props.location.search;
    updateBreadCrumbs();
    setGlobalFilters();
    extractFiltersFromUrl(url);
  }, []);

  const setGlobalFilters = async () => {
    const headerRef = await whenHeaderRef();
    if (headerRef && headerRef.gf) {
      const options = {
        enabled: {
          strategies: true,
          fundFamilies: true,
          legalEntityFamilies: true
        }
      };
      headerRef.gf.addListener(appHeaderListener);
      headerRef.gf.setOptions(options);
    }
  };

  const extractFiltersFromUrl = async (url: string) => {
    if (url != null && url.length > 0) {
      let filters = getFiltersFromUrl(url);
      if (filters !== INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_STORE_DATA.INITIAL_SEARCH_TRADE_INDEPENDENT_AMOUNT_FILTER) {
        searchTradeIndependentAmountStore.applySavedFilters(filters);
        searchTradeIndependentAmountStore.handleSearch();
      }
    }
  };

  function appHeaderListener({ expandedSelections }) {
    searchTradeIndependentAmountStore.setExpandedGlobalSelections(expandedSelections);
  }

  return (
    <>
      <Loader />
      <ReactLoader
        inProgress={searchTradeIndependentAmountStore.inProgress}
      />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={true} />
        <Layout
          isColumnType={true}
          className="border"
          style={{ height: "100%" }}
        >
          <Layout.Child
            childId="TradeIndependentAmountIndexChild1"
            size={1}
            title="Trade Independent Amount Filter"
            collapsible
            collapsed={searchTradeIndependentAmountStore.collapseSideBar}
            onChange={searchTradeIndependentAmountStore.setCollapsed}
          >
            <TradeIndependentAmountSideBar />
          </Layout.Child>
          <Layout.Divider childId="TradeIndependentAmountIndexChild2" isResizable />
          <Layout.Child childId="TradeIndependentAmountIndexChild3" size={3} >
            <TradeIndependentAmountGrid />
          </Layout.Child>
        </Layout>
      </div>
    </>
  );
};

export default observer(TradeIndependentAmount);
