import { BASE_URL } from "../../commons/constants";
import { ArcFetch } from "arc-commons";
import {
  TradeIndependentAmountFilter
} from "./models/TradeIndependentAmountFilter";
import SERVICE_URLS from "../../commons/UrlConfigs";
import { ModifyIndependentAmountParam } from "./models/ModifyIndependentAmountParam";

const getQueryParams = (params) => ({
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
  credentials: 'include',
  method: 'POST',
  body: params
});

export const getTradeIndependentAmountDetail = (tradeIndependentAmountFilter:
  TradeIndependentAmountFilter) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.tradeIndependentAmountService
    .getTradeIndependentAmountDetail}`

  const params = `tradeIndependentAmountFilter=${encodeURIComponent(
    JSON.stringify(tradeIndependentAmountFilter)
  )}&inputFormat=json&format=json`;

  return ArcFetch.getJSON(url, getQueryParams(params));
};

export const updateIndependentAmountDetail = (modifyIndependentAmountParam: ModifyIndependentAmountParam) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.tradeIndependentAmountService
    .updateIndependentAmountDetail}`

  const params = `modifyIndependentAmountParam=${encodeURIComponent(
    JSON.stringify(modifyIndependentAmountParam)
  )}&inputFormat=json&format=json`;

  return ArcFetch.getJSON(url, getQueryParams(params));
}

export const invalidateIndependentAmountDetail = (modifyIndependentAmountParam: ModifyIndependentAmountParam) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.tradeIndependentAmountService
    .invalidateIndependentAmountDetail}`

  const params = `modifyIndependentAmountParam=${encodeURIComponent(
    JSON.stringify(modifyIndependentAmountParam)
  )}&inputFormat=json&format=json`;

  return ArcFetch.getJSON(url, getQueryParams(params));
}
