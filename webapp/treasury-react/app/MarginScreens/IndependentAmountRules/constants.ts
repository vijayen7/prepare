export const ALL_GRID_COLUMNS: string[] = [
  "strategy",
  "book",
  "frontOfficePrimeBroker",
  "securityType",
  "isSwap",
  "effectiveDate",
  "quantity",
  "currency",
  "underlyingType",
  "underlyingSpn",
  "independentAmountRule"
];

export const DEFAULT_PINNED_COLUMNS = { actions: true };

export const MESSAGES = {
  INITIAL_SEARCH_MESSAGE: "Perform search to view Independent Amount Rules",
  SEARCH_RULES_FAILURE_MESSAGE: "Some error occurred while searching rules",
  NO_INDEPENDENT_AMOUNT_RULES_MESSAGE:
    "No Independent Amount Rules are available for the performed search",
};

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const INITIAL_SEARCH_RULE_STORE_DATA = {
  INITIAL_SEARCH_FILTER: {
    selectedStrategies: [],
    selectedPrimeBrokers: [],
    selectedSecurityTypeSubtypes: [],
    selectedCurrencies: [],
  },
  INITIAL_SEARCH_STATUS: {
    status: "",
    message: MESSAGES.INITIAL_SEARCH_MESSAGE,
  },
};
