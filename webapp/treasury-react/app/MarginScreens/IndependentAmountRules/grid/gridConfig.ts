import ArcDataGrid from "arc-data-grid";

export const IndependentAmountRulesGridconfig: ArcDataGrid.GridConfiguration = {
  clickableRows: true,
  getExportFileName: ({ fileType, all }) => {
    return `${all ? "Independent-Amount-Rules" : "Independent-Amount-Current-View"}`;
  },
  timezone: "Asia/Calcutta",
};
