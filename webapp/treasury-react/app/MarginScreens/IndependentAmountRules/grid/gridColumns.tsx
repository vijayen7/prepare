import React from "react";
import ArcDataGrid from "arc-data-grid";
import { IndependentAmountRuleGridData } from "../models/IndependentAmountRuleGridData";

const IndependentAmountRulesGridcolumns: ArcDataGrid.Columns<IndependentAmountRuleGridData, number> = [
  "strategy",
  "book",
  "frontOfficePrimeBroker",
  "securityType",
  "isSwap",
  "effectiveDate",
  "quantity",
  "currency",
  "underlyingType",
  {
    identity: "underlyingSpn",
    header: "Underlying SPN",
  },
  "independentAmountRule",
];

export const getGridColumns = () => {
  return [...IndependentAmountRulesGridcolumns]
};
