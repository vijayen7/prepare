import ArcDataGrid from "arc-data-grid";
import React from "react";
import { observer } from "mobx-react-lite";
import { getGridColumns } from "../grid/gridColumns";
import { IndependentAmountRulesGridconfig } from "../grid/gridConfig";
import Message from "../../../commons/components/Message";
import { SUCCESS, FAILURE } from "../../../commons/constants";
import { DEFAULT_PINNED_COLUMNS } from "../constants";
import { useStores } from "../useStores";
import { Layout } from "arc-react-components";

const IndependentAmountRulesGrid: React.FC = () => {
  const { rootStore } = useStores();
  const { searchIndependentAmountRulesStore } = rootStore;

  const renderGridData = () => {
    if (searchIndependentAmountRulesStore.independentAmountRulesGridData.length) {
      return (
        <>
          <ArcDataGrid
            rows={searchIndependentAmountRulesStore.independentAmountRulesGridData}
            columns={getGridColumns()}
            displayColumns={searchIndependentAmountRulesStore.displayColumns}
            pinnedColumns={DEFAULT_PINNED_COLUMNS}
            onDisplayColumnsChange={searchIndependentAmountRulesStore.onDisplayColumnsChange}
            configurations={IndependentAmountRulesGridconfig}
          />
        </>
      );
    } else {
      return (
        <Message
          messageData={searchIndependentAmountRulesStore.searchStatus.message}
          error={searchIndependentAmountRulesStore.searchStatus.status == FAILURE}
          warning={searchIndependentAmountRulesStore.searchStatus.status == SUCCESS}
        />
      );
    }
  };

  return (
    <>
      <Layout>
        <Layout.Child childId="IndependentAmountRulesGridChild1">{renderGridData()}</Layout.Child>
      </Layout>
    </>
  );
};

export default observer(IndependentAmountRulesGrid);
