import React from "react";
import { Card } from "arc-react-components";
import { Sidebar } from "arc-react-components";
import { observer } from "mobx-react-lite";
import SaveSettingsManager from "../../../commons/components/SaveSettingsManager";
import { useStores } from "../useStores";
import FrontOfficePrimeBrokerFilter from "../../../commons/container/FrontOfficePrimeBrokerFilter";
import CurrencyFilter from "../../../commons/container/CurrencyFilter";
import SecurityFOTypeSubtypeFilter from "../../../commons/container/SecurityFOTypeSubtypeFilter";
import MultiSelectFilter from "../../../commons/filters/components/MultiSelectFilter";
import FilterButton from "../../../commons/components/FilterButton";

const IndependentAmountRulesSideBar: React.FC = () => {
  const { rootStore } = useStores();
  const searchIndependentAmountRulesStore = rootStore.searchIndependentAmountRulesStore;

  const searchElement =
    <FilterButton
      onClick={searchIndependentAmountRulesStore.handleSearch}
      label="Search"
    />

  const resetElement =
    <FilterButton
      reset={true}
      onClick={searchIndependentAmountRulesStore.handleReset}
      label="Reset"
    />

  const copySearchElement =
    <FilterButton
      reset={true}
      onClick={searchIndependentAmountRulesStore.handleCopySearch}
      label="Copy Search URL"
      className="float--left button--tertiary margin--small"
    />

  const saveSettingsManager = (
    <SaveSettingsManager
      selectedFilters={searchIndependentAmountRulesStore.searchFilter}
      applySavedFilters={searchIndependentAmountRulesStore.applySavedFilters}
      applicationName="IndependentAmountRulesFilter"
    />
  )

  const strategiesFilter = (
    <MultiSelectFilter
      data={searchIndependentAmountRulesStore.selectedStrategyData}
      label="Strategies"
      onSelect={searchIndependentAmountRulesStore.onSelect}
      selectedData={searchIndependentAmountRulesStore.searchFilter.selectedStrategies}
      stateKey="selectedStrategies"
      multiSelect
    />
  )

  const primeBrokerFilter = (
    <FrontOfficePrimeBrokerFilter
      label="Front Office Prime Brokers"
      onSelect={searchIndependentAmountRulesStore.onSelect}
      selectedData={searchIndependentAmountRulesStore.searchFilter.selectedPrimeBrokers}
      multiSelect
    />
  )

  const securityTypeFilter = (
    <SecurityFOTypeSubtypeFilter
      label="Security Types"
      onSelect={searchIndependentAmountRulesStore.onSelect}
      selectedData={searchIndependentAmountRulesStore.searchFilter.selectedSecurityTypeSubtypes}
      multiSelect
    />
  )

  const currencyFilter = (
    <CurrencyFilter
      label="Currencies"
      onSelect={searchIndependentAmountRulesStore.onSelect}
      selectedData={searchIndependentAmountRulesStore.searchFilter.selectedCurrencies}
      multiSelect
    />
  )

  return (
    <>
      <Sidebar
      header={false}
      footer={
        <>
          {searchElement}
          {resetElement}
          {copySearchElement}
        </>
      }
    >
        <Card>
          {saveSettingsManager}
        </Card>
        <Card>
          {strategiesFilter}
          {primeBrokerFilter}
          {securityTypeFilter}
          {currencyFilter}
        </Card>
      </Sidebar>
    </>
  );
};

export default observer(IndependentAmountRulesSideBar);
