import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../../commons/components/ReactLoader";
import Loader from "../../commons/container/Loader";
import IndependentAmountRulesSideBar from "./container/IndependentAmountRulesSideBar";
import IndependentAmountRulesGrid from "./container/IndependentAmountRulesGrid";
import AppHeader, { whenHeaderRef } from "arc-header";
import { Breadcrumbs, Layout } from "arc-react-components";
import { COMPONENT_URLS } from "../../commons/UrlConfigs";
import { useStores } from "./useStores";
import { IndependentAmountRuleProps } from "./models/IndependentAmountRuleProps";
import { INITIAL_SEARCH_RULE_STORE_DATA } from "./constants";
import { getFiltersFromUrl } from "./utils/searchRulesUtil";

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={COMPONENT_URLS.INDEPENDENT_AMOUNT_RULES} key="independentAmountRules">
        Independent Amount Rules
      </Breadcrumbs.Item>
    </>
  );
};

const IndependentAmountRules: React.FC<IndependentAmountRuleProps> = (props : IndependentAmountRuleProps) => {
  const { rootStore } = useStores();
  const { searchIndependentAmountRulesStore } = rootStore;

  useEffect(() => {
    let url = props.location.search;
    updateBreadCrumbs();
    setGlobalFilters();
    extractFiltersFromUrl(url);
  }, []);

  const setGlobalFilters = async () => {
    const headerRef = await whenHeaderRef();
    if (headerRef && headerRef.gf) {
      const options = {
        enabled: {
          strategies: true,
          fundFamilies: true,
          legalEntityFamilies: true
        }
      };
      headerRef.gf.addListener(appHeaderListener);
      headerRef.gf.setOptions(options);
    }
  };

  const extractFiltersFromUrl = async (url: string) => {
    if (url != null && url.length > 0) {
      let filters = getFiltersFromUrl(url);
      if (filters !== INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_SEARCH_FILTER) {
        searchIndependentAmountRulesStore.applySavedFilters(filters);
        searchIndependentAmountRulesStore.handleSearch();
      }
    }
  };

  function appHeaderListener({ userSelections, expandedSelections }) {
    searchIndependentAmountRulesStore.setExpandedGlobalSelections(expandedSelections);
  }

  return (
    <>
      <Loader />
      <ReactLoader
        inProgress={searchIndependentAmountRulesStore.inProgress}
      />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={true} />
        <Layout
          isColumnType={true}
          className="border"
          style={{ height: "100%" }}
        >
          <Layout.Child
            childId="IndependentAmountRulesIndexChild1"
            size={1}
            key="child1"
            title="Independent Amount Rules Filter"
            collapsible
            collapsed={searchIndependentAmountRulesStore.collapseSideBar}
            onChange={searchIndependentAmountRulesStore.setCollapsed}
          >
            <IndependentAmountRulesSideBar />
          </Layout.Child>
          <Layout.Divider childId="IndependentAmountRulesIndexChild2" isResizable key="child2" />
          <Layout.Child childId="IndependentAmountRulesIndexChild3" size={4} key="child3">
            <IndependentAmountRulesGrid />
          </Layout.Child>
        </Layout>
      </div>
    </>
  );
};

export default observer(IndependentAmountRules);
