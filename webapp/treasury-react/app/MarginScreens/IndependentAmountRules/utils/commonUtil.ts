import ReferenceData from "../../../commons/models/ReferenceData";
import { TOAST_TYPE } from "../constants";
import { ToastService } from "arc-react-components";
import { NOT_AVAILABLE } from "../../../commons/constants";
import RefData from "../../../commons/models/RefData";
import MapData from "../../../commons/models/MapData";

export const getNameFromRefData = (refData?: ReferenceData) => {
  return refData?.name ? refData.name : NOT_AVAILABLE;
};

export const getAbbrevFromRefData = (refData?: ReferenceData) => {
  return refData?.abbrev ? refData.abbrev : NOT_AVAILABLE;
};

const getToastType = (type: string) => {
  let toastType: any = ToastService.ToastType.SUCCESS;
  switch (type) {
    case TOAST_TYPE.CRITICAL:
      toastType = ToastService.ToastType.CRITICAL;
      break;
    case TOAST_TYPE.SUCCESS:
      toastType = ToastService.ToastType.SUCCESS;
      break;
    case TOAST_TYPE.INFO:
      toastType = ToastService.ToastType.INFO;
      break;
  }
  return toastType;
};

export const showToastService = (content: string, type: string) => {
  ToastService.append({
    content: content,
    type: getToastType(type),
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: 7000,
  });
};

export const getFilterListFromCommaSeparatedUnderscoreValues = (inputStringObjects : string) : RefData[] => {
  if (inputStringObjects) {
    return inputStringObjects.split(',').map(eachValue => ({
      key: eachValue, value: ""
    }));
  }
  else {
    return [];
  }
};
