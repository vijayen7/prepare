import { getCommaSeparatedKeysFromFilterList, getFilterListFromCommaSeparatedString, getSelectedIdListWithNull } from "../../../commons/util";
import { IndependentAmountRuleFilter } from "../models/IndependentAmountRuleFilter";
import { IndependentAmountRuleData } from "../models/IndependentAmountRuleData";
import { IndependentAmountRuleSearchFilter } from "../models/IndependentAmountRuleSearchFilter";
import { IndependentAmountRuleGridData } from "../models/IndependentAmountRuleGridData";
import { getAbbrevFromRefData, getFilterListFromCommaSeparatedUnderscoreValues, getNameFromRefData } from "./commonUtil";
import { CLASS_NAMESPACE } from "../../../commons/ClassConfigs"
import queryString from "query-string";

export const getIndependentAmountRuleFilter = (searchFilter: IndependentAmountRuleSearchFilter) => {
  let independentAmountRuleFilter: IndependentAmountRuleFilter = {
    primeBrokerIds: getSelectedIdListWithNull(searchFilter.selectedPrimeBrokers),
    currencyIds: getSelectedIdListWithNull(searchFilter.selectedCurrencies),
    strategyIds: getSelectedIdListWithNull(searchFilter.selectedStrategies),
    typeIds: getSelectedIdListWithNull(searchFilter.selectedSecurityTypeSubtypes),
    "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.margin.model.IndependentAmountRuleFilter,
  };
  return independentAmountRuleFilter;
};

export const getGridDataFromIndependentAmountRulesData = (
  independentAmountRulesData: IndependentAmountRuleData[]
) => {
  let gridDataList: IndependentAmountRuleGridData[] = [];
  independentAmountRulesData.forEach((rule) => {
    let gridData: IndependentAmountRuleGridData = {
      strategy: getNameFromRefData(rule.strategy),
      book: getNameFromRefData(rule.book),
      frontOfficePrimeBroker: getNameFromRefData(rule.primeBroker),
      securityType: getNameFromRefData(rule.securityType),
      isSwap: rule.isSwapFlag,
      effectiveDate: rule.effectiveDate,
      quantity: rule.quantity,
      currency: getAbbrevFromRefData(rule.currency),
      underlyingType: getNameFromRefData(rule.underlyingType),
      underlyingSpn: rule.underlyingSpn,
      independentAmountRule: rule.independentAmount
    };
    gridDataList.push(gridData);
  });
  return gridDataList;
};

export const getURLFromSelectedFilters = (params: IndependentAmountRuleSearchFilter, location: any) => {
  let url = location + `/treasury/margin/independentAmountRules.html?
    &strategyIds=${getCommaSeparatedKeysFromFilterList(params.selectedStrategies)}
    &primeBrokerIds=${getCommaSeparatedKeysFromFilterList(params.selectedPrimeBrokers)}
    &typeIds=${getCommaSeparatedKeysFromFilterList(params.selectedSecurityTypeSubtypes)}
    &currencyIds=${getCommaSeparatedKeysFromFilterList(params.selectedCurrencies)}`;
  return url;
}

export const getFiltersFromUrl = (url: any) => {
  const values = queryString.parse(url);
  let filters = {
    selectedStrategies: getFilterListFromCommaSeparatedString(values.strategyIds),
    selectedPrimeBrokers: getFilterListFromCommaSeparatedString(values.primeBrokerIds),
    selectedSecurityTypeSubtypes: getFilterListFromCommaSeparatedUnderscoreValues(values.typeIds as string),
    selectedCurrencies: getFilterListFromCommaSeparatedString(values.currencyIds),
  }
  return filters;
}
