import { action, observable } from "mobx";
import Status from "../../../commons/models/Status";
import { IndependentAmountRuleSearchFilter } from "../models/IndependentAmountRuleSearchFilter";
import { IndependentAmountRuleGridData } from "../models/IndependentAmountRuleGridData";
import MapData from "../../../commons/models/MapData";
import { IndependentAmountRuleFilter } from "../models/IndependentAmountRuleFilter";
import { IndependentAmountRuleData } from "../models/IndependentAmountRuleData";
import { getIndependentAmountRules } from "../api";
import { SUCCESS } from "../../../commons/constants";
import {
  MESSAGES,
  TOAST_TYPE,
  INITIAL_SEARCH_RULE_STORE_DATA,
  ALL_GRID_COLUMNS,
} from "../constants";
import RootStore from "./RootStore";
import {
  getIndependentAmountRuleFilter,
  getGridDataFromIndependentAmountRulesData,
  getURLFromSelectedFilters,
} from "../utils/searchRulesUtil";
import { showToastService } from "../utils/commonUtil";

const copy = require('clipboard-copy')
export class SearchIndependentAmountRulesStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }

  @observable
  inProgress: boolean = false;

  @observable
  collapseSideBar = false;

  @observable
  searchStatus: Status = INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_SEARCH_STATUS;

  @observable
  searchFilter: IndependentAmountRuleSearchFilter =
    INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_SEARCH_FILTER;

  @observable
  selectedStrategyData: MapData[] = [];

  @observable
  independentAmountRulesData: IndependentAmountRuleData[] = [];

  @observable
  independentAmountRulesGridData: IndependentAmountRuleGridData[] = [];

  @observable
  displayColumns: string[] = ALL_GRID_COLUMNS;

  @action.bound
  onDisplayColumnsChange(newDisplayColumns) {
    let newDisplayColumnSet = new Set(newDisplayColumns);
    let newDisplayOrderedColumnSet = ALL_GRID_COLUMNS.filter((e) =>
      newDisplayColumnSet.has(e)
    );
    this.displayColumns = newDisplayOrderedColumnSet;
  }

  @action.bound
  setExpandedGlobalSelections(expandedGlobalSelection: any) {
    var strategyDataList = expandedGlobalSelection['strategies'];
    var strategyDataMap: MapData[] = [];

    strategyDataList.forEach(strategyData => {
      strategyDataMap.push({key: parseInt(strategyData['id']), value: strategyData['name']});
    });

    this.selectedStrategyData = strategyDataMap;
  }

  @action.bound
  applySavedFilters = (savedFilters : IndependentAmountRuleSearchFilter) => {
    this.searchFilter = savedFilters;
  };

  @action.bound
  setCollapsed = (collapsed: boolean) => {
    this.collapseSideBar = collapsed;
  };

  @action.bound
  setSearchStatus(status: string, message: string) {
    this.searchStatus = {
      status: status,
      message: message,
    };
  }

  @action.bound
  onSelect({ key, value }: any) {
    this.searchFilter[key] = value;
  }

  @action.bound
  handleSearch() {
    this.setCollapsed(true);
    this.fetchIndependentAmountRules(getIndependentAmountRuleFilter(this.searchFilter));
  }

  @action.bound
  handleCopySearch = () => {
    let location = window.location.origin;
    let url = getURLFromSelectedFilters(this.searchFilter,location);
    copy(url);
    showToastService("Search URL copied to clipboard.", TOAST_TYPE.INFO);
  }

  @action.bound
  handleReset() {
    this.searchFilter = INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_SEARCH_FILTER;
  }

  @action.bound
  setIndependentAmountRulesGridData() {
    if (!this.independentAmountRulesData.length) {
      this.setSearchStatus(SUCCESS, MESSAGES.NO_INDEPENDENT_AMOUNT_RULES_MESSAGE);
      this.independentAmountRulesGridData = [];
    } else {
      this.independentAmountRulesGridData = getGridDataFromIndependentAmountRulesData(
        this.independentAmountRulesData
      );
    }
  }

  fetchIndependentAmountRules = async (params: IndependentAmountRuleFilter) => {
    try {
      this.inProgress = true;
      this.independentAmountRulesData = await getIndependentAmountRules(params);
      this.setIndependentAmountRulesGridData();
      this.inProgress = false;
    } catch (error) {
      showToastService(
        MESSAGES.SEARCH_RULES_FAILURE_MESSAGE,
        TOAST_TYPE.CRITICAL
      );
      this.inProgress = false;
    }
  }

}

export default SearchIndependentAmountRulesStore;
