import SearchIndependentAmountRulesStore from "./SearchIndependentAmountRulesStore";

export class RootStore {
  searchIndependentAmountRulesStore: SearchIndependentAmountRulesStore;
  constructor() {
    this.searchIndependentAmountRulesStore = new SearchIndependentAmountRulesStore(this);
  }
}

export default RootStore;
