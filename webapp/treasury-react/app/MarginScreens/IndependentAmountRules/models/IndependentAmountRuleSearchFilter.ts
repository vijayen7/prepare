// UI DATA TYPE

import MapData from "../../../commons/models/MapData";
import RefData from "../../../commons/models/RefData";

export interface IndependentAmountRuleSearchFilter {
  selectedStrategies: MapData[];
  selectedPrimeBrokers: MapData[];
  selectedSecurityTypeSubtypes: RefData[];
  selectedCurrencies: MapData[];
}
