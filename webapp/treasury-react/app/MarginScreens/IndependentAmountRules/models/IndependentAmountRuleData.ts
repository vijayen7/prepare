// SERVICE DATA TYPE

import ReferenceData from "../../../commons/models/ReferenceData";
import { CLASS_NAMESPACE } from "../../../commons/ClassConfigs"

export interface IndependentAmountRuleData {
  strategy: ReferenceData,
  book?: ReferenceData,
  primeBroker: ReferenceData,
  securityType: ReferenceData,
  isSwapFlag: boolean,
  effectiveDate: string,
  quantity?: string,
  currency?: ReferenceData,
  underlyingType?: ReferenceData,
  underlyingSpn?: number,
  independentAmount: string
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.margin.model.EnrichedIndependentAmountRule;
}
