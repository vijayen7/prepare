import * as H from 'history';

export interface IndependentAmountRuleProps {
  location: H.Location
};
