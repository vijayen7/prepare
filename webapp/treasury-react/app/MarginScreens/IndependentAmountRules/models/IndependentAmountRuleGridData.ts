// UI DATA TYPE

export interface IndependentAmountRuleGridData {
  strategy: string;
  book?: string;
  frontOfficePrimeBroker: string;
  securityType: string;
  isSwap: boolean;
  effectiveDate: string;
  quantity?: string;
  currency?: string;
  underlyingType?: string;
  underlyingSpn?: number;
  independentAmountRule: string;
}
