// SERVICE DATA TYPE

import { CLASS_NAMESPACE } from "../../../commons/ClassConfigs"

export interface IndependentAmountRuleFilter {
  strategyIds: number[];
  primeBrokerIds: number[];
  typeIds: string[];
  currencyIds: number[];
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.margin.model.IndependentAmountRuleFilter;
}
