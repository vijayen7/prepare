import { ArcFetch } from "arc-commons";
import { BASE_URL } from "../../commons/constants";
import {
  IndependentAmountRuleFilter,
} from "./models/IndependentAmountRuleFilter";
import SERVICE_URLS from "../../commons/UrlConfigs";

const getQueryParams = (params) => ({
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
  credentials: 'include',
  method: 'POST',
  body: params
});

export const getIndependentAmountRules = (independentAmountRuleFilter: IndependentAmountRuleFilter) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.independentAmountRuleService.searchIndependentAmountRules}`
  const params = `independentAmountRuleFilter=${encodeURIComponent(
    JSON.stringify(independentAmountRuleFilter)
    )}&inputFormat=json&format=json`;

  return ArcFetch.getJSON(url, getQueryParams(params));
};
