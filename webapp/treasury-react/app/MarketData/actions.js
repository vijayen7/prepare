import {
  FETCH_POSITION_VIEW_MARKET_DATA,
  FETCH_BUNDLE_POSITION_VIEW_MARKET_DATA,
  FETCH_LOT_VIEW_MARKET_DATA,
  FETCH_SECURITY_HISTORY_DATA,
  FETCH_SECURITY_MARKET_DATA,
  DESTROY_DATA_FOR_SECURITY_MARKET_DATA,
  DESTROY_SECURITY_HISTORY_DATA,
  RESET_MARKET_DATA_SEARCH_MESSAGE,
  RESIZE_CANVAS
} from "commons/constants";

export function fetchPositionViewMarketData(payload) {
  return {
    type: FETCH_POSITION_VIEW_MARKET_DATA,
    payload
  };
}

export function resetMarketDataSearchMessage() {
  return {
    type: RESET_MARKET_DATA_SEARCH_MESSAGE
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

export function fetchBundlePositionViewMarketData(payload) {
  return {
    type: FETCH_BUNDLE_POSITION_VIEW_MARKET_DATA,
    payload
  };
}

export function fetchLotViewMarketData(payload) {
  return {
    type: FETCH_LOT_VIEW_MARKET_DATA,
    payload
  };
}

export function fetchSecurityHistoryData(payload) {
  return {
    type: FETCH_SECURITY_HISTORY_DATA,
    payload
  };
}

export function destroySecurityHistoryData() {
  return {
    type: DESTROY_SECURITY_HISTORY_DATA
  };
}

export function fetchSecurityMarketData(payload) {
  return {
    type: FETCH_SECURITY_MARKET_DATA,
    payload
  };
}

export function destroySecurityMarketData(payload) {
  return {
    type: DESTROY_DATA_FOR_SECURITY_MARKET_DATA,
    payload
  };
}
