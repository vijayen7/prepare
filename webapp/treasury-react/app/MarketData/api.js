import { BASE_URL, INPUT_FORMAT_JSON } from 'commons/constants';
import { getEncodedParams} from 'commons/util';
import { CLASS_NAMESPACE } from "commons/ClassConfigs"
const queryString = require("query-string");
export let url = "";
export function getPositionViewMarketData(payload) {
  delete payload['marketDataFilter.bundlePositionTypes'];
  const encodedParams = getEncodedParams(payload, CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.MarketDataFilter);
  url = `${BASE_URL}service/marketDataService/getPositionViewMarketData?marketDataFilter=${encodedParams}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getBundlePositionViewMarketData(payload) {
  delete payload['marketDataFilter.mappingTags'];
  const encodedParams = getEncodedParams(payload, CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.MarketDataFilter);
  url = `${BASE_URL}service/marketDataService/getBuBundleViewMarketData?marketDataFilter=${encodedParams}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getLotViewMarketData(payload) {
  delete payload['marketDataFilter.bundlePositionTypes'];
  const encodedParams = getEncodedParams(payload, CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.MarketDataFilter);
  url = `${BASE_URL}service/marketDataService/getLotViewMarketData?marketDataFilter=${encodedParams}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getSecurityHistoryData(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/marketDataService/getMarketDataSecurityHistory?positionKeyId=${payload.positionKeyId}&date=${payload.date}&numberOfDays=${payload.numberOfDays}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getSecurityMarketData(payload) {
  url = `${BASE_URL}service/marketDataService/getMarketDataForSecurity?spns=${payload.spn}&cusip=null&sedol=null&date="${payload.date}"${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}
