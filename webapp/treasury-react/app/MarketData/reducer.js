import {
  FETCH_POSITION_VIEW_MARKET_DATA,
  FETCH_BUNDLE_POSITION_VIEW_MARKET_DATA,
  FETCH_LOT_VIEW_MARKET_DATA,
  FETCH_SECURITY_HISTORY_DATA,
  FETCH_SECURITY_MARKET_DATA,
  DESTROY_DATA_STATE_FOR_POSITION_VIEW_MARKET_DATA,
  DESTROY_DATA_STATE_FOR_LOT_VIEW_MARKET_DATA,
  DESTROY_DATA_FOR_SECURITY_MARKET_DATA,
  DESTROY_SECURITY_HISTORY_DATA,
  UPDATE_MARKET_DATA_SEARCH_MESSAGE,
  RESET_MARKET_DATA_SEARCH_MESSAGE,
  RESIZE_CANVAS,
  DEFAULT_SEARCH_MESSAGE,
  NO_DATA_SEARCH_MESSAGE
} from "commons/constants";
import { combineReducers } from "redux";
import { convertJavaNYCDate } from "commons/util";

function marketDataSearchMessageReducer(
  state = DEFAULT_SEARCH_MESSAGE,
  action
) {
  switch (action.type) {
    case UPDATE_MARKET_DATA_SEARCH_MESSAGE:
      return !action.data || action.data.length <= 0 || typeof action.data.message !== "undefined"
        ? NO_DATA_SEARCH_MESSAGE
        : "";
    case RESET_MARKET_DATA_SEARCH_MESSAGE:
      return DEFAULT_SEARCH_MESSAGE;
    default:
      return state;
  }
}

function positionViewMarketDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_POSITION_VIEW_MARKET_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_POSITION_VIEW_MARKET_DATA:
      return [];
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

function bundlePositionViewMarketDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_BUNDLE_POSITION_VIEW_MARKET_DATA}_SUCCESS`:
      return action.data || [];
  }
  return state;
}

function lotViewMarketDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LOT_VIEW_MARKET_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_LOT_VIEW_MARKET_DATA:
      return [];
  }
  return state;
}

function securityHistoryDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SECURITY_HISTORY_DATA}_SUCCESS`:
      // Do it in the service
      action.data.date.forEach(
        (o, i, arr) => (arr[i] = convertJavaNYCDate(arr[i]))
      );
      action.data.rebateRate.forEach(
        (o, i, arr) => (arr[i] = Math.abs(arr[i]))
      );
      return action.data || [];
    case DESTROY_SECURITY_HISTORY_DATA:
      return [];
  }
  return state;
}

function securityMarketDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SECURITY_MARKET_DATA}_SUCCESS`:
      return action.data || [];
    case `${DESTROY_DATA_FOR_SECURITY_MARKET_DATA}_SUCCESS`:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  positionViewData: positionViewMarketDataReducer,
  marketDataSearchMessage: marketDataSearchMessageReducer,
  bundlePositionViewData: bundlePositionViewMarketDataReducer,
  lotViewData: lotViewMarketDataReducer,
  securityHistoryData: securityHistoryDataReducer,
  securityMarketData: securityMarketDataReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
