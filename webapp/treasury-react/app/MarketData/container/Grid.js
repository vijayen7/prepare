import React, { Component } from "react";
import {
  positionViewColumns,
  bundlePositionViewColumns,
  lotViewColumns
} from "../grid/columnConfig";
import { Layout, Panel } from "arc-react-components";
import { gridOptions } from "../grid/gridOptions";
import Resizer from "commons/components/Resizer";
import ResizerContainer from "commons/container/ResizerContainer";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MarketDataGrid from "commons/components/GridWithCellClick";
import PositionDataGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import Dialog from "commons/components/Dialog";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import {
  fetchSecurityHistoryData,
  fetchSecurityMarketData,
  fetchBundlePositionViewMarketData,
  fetchLotViewMarketData,
  destroySecurityMarketData
} from "../actions";
import { loadNegotiationPopUp } from "../../seclend/RateNegotiationDialog/actions";
import { convertJavaNYCDate, convertLocalDatetoDate } from "commons/util";
import MappingTagInfo from "../components/MappingTagInfo";
import Graph from "../components/Graph";
import AvailabilityDataPanel from "../../AvailabilityDataPanel/components/AvailabilityDataPanel";
import NegotiationDialog from "../../seclend/RateNegotiationDialog/RateNegotiationDialog";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.reportingCurrencyMessage = this.reportingCurrencyMessage.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.onNegotiationCellClickHandler = this.onNegotiationCellClickHandler.bind(
      this
    );
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onSelectNumberOfDays = this.onSelectNumberOfDays.bind(this);
    this.state = {
      showModal: false,
      selectedRowProps: {},
      negotiationStartDate: "",
      negotiationEndDate: "",
      selectedNumberOfDays: { key: 30, value: "30" }
    };
  }

  componentWillUnmount() {
    this.props.destroySecurityMarketData();
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({
      showModal: false,
      selectedNumberOfDays: { key: 30, value: "30" }
    });
  }

  onSelectNumberOfDays(params) {
    this.onSelect(params);
    var payload = {
      positionKeyId: this.state.selectedRowProps.positionKeyId,
      date: convertLocalDatetoDate(this.state.selectedRowProps.localDate),
      numberOfDays: this.state.selectedNumberOfDays.key
    };
    this.props.fetchSecurityHistoryData(payload);
  }
  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  onNegotiationCellClickHandler = args => {
    if (
      args.colId === "negotiation" &&
      args.item.borrowType !== "REHYPE" &&
      args.item.pnlSpn !== -1 &&
      args.item.pnlSpn !== -2 &&
      args.item.brokerPositionType !== "BROKER_MISSING" &&
      args.row !== null
    ) {
      args.item.view = this.props.view;
      args.item.view = (this.props.view == "position") ? "longPosition": this.props.view
      args.item.id = args.item.brokerPositionDataId;
      this.props.loadNegotiationPopUp(args.item);
    }
  };

  onCellClickHandler = args => {
    this.onNegotiationCellClickHandler(args);
    if (
      args.colId === "history" &&
      args.item.brokerPositionType !== "INTERNAL_MISSING"
    ) {
      var payload = {
        positionKeyId: args.item.positionKeyId,
        date: convertLocalDatetoDate(args.item.localDate),
        numberOfDays: this.state.selectedNumberOfDays.key
      };
      this.props.fetchSecurityHistoryData(payload);
      this.setState({
        showModal: true,
        selectedRowProps: args.item
      });
    } else if (args.colId === "renegotiate") {
      this.setState({ showModal: true, selectedRowProps: args.item });
    } else if (
      args.colId === "bundleView" &&
      args.item.brokerPositionType !== "INTERNAL_MISSING"
    ) {
      this.props.fetchBundlePositionViewMarketData(
        getMarketDataFilterPayload(args.item, "bundle")
      );
      this.props.handleViewToggle("bundle");
    } else if (
      args.colId === "lotView" &&
      args.item.brokerPositionType !== "BROKER_MISSING" &&
      args.item.hasLotLevelData
    ) {
      this.props.fetchLotViewMarketData(
        getMarketDataFilterPayload(args.item, "lot")
      );
      this.props.handleViewToggle("lot");
    } else if (args.item.brokerPositionType !== "INTERNAL_MISSING") {
      var payload = {
        spn: args.item.spn,
        cusip: "__null__",
        sedol: "__null__",
        date: args.item.localDate
      };
      this.props.fetchSecurityMarketData(payload);
      this.setState({
        selectedCpeIdForDrillDown: args.item.cpeId,
        selectedSecurityDescription:
          args.item.securityDescription +
          " - " +
          args.item.spn +
          " - " +
          args.item.pnlSpn
      });
    } else return;
  };

  reportingCurrencyMessage(ccy) {
    if (ccy == undefined) {
      return (<ul className="bullet">
        <li className="critical">{`Fx Rate unavailable for selected Reporting Currency`}</li>
      </ul>);
    }
    return (
      <small><ul className="bullet">
        <li>{`Reporting Currency (RC) corresponds to ${ccy}`}</li>
      </ul></small>
    );
  }

  renderGridData() {
    let grid = null, RC;
    if (this.props.view === "position") {
      if (typeof this.props.positionViewData.message !== "undefined" || this.props.positionViewData.length <= 0)
        return <Message messageData={this.props.marketDataSearchMessage} />;
      RC = this.props.positionViewData[0].reportingCurrency
      grid = (
        <React.Fragment>
          <Layout>
            <Layout.Child size="Fit" childId="gridChild1">
              <MappingTagInfo view={this.props.view} />
            </Layout.Child>
            <Layout.Child size={8} childId="gridChild2">
              <PositionDataGrid
                data={this.props.positionViewData}
                onCellClick={this.onCellClickHandler}
                gridId="PositionData"
                gridColumns={positionViewColumns()}
                gridOptions={gridOptions("position")}
                resizeCanvas={this.props.resizeCanvas}
                fill={true}
              />
            </Layout.Child>
            <Layout.Child size="Fit" childId="rcGridChild1">
              {this.reportingCurrencyMessage(RC)}
            </Layout.Child>
            {this.props.securityMarketData.length > 0 && (
              <Layout.Child size={4} childId="gridChild3">
                <Panel
                  dismissible
                  onClose={() => {
                    this.props.destroySecurityMarketData();
                  }}
                  title={this.state.selectedSecurityDescription}
                >
                  <AvailabilityDataPanel
                    data={this.props.securityMarketData}
                    selectedCpeIdForDrillDown={
                      this.state.selectedCpeIdForDrillDown
                    }
                  />
                </Panel>
              </Layout.Child>
            )}
          </Layout>
          <Dialog
            isOpen={this.state.showModal}
            title=""
            onClose={this.handleCloseModal}
          >
            <Layout>
              <Layout.Child childId="gridChild4">
                <Layout isColumnType={true}>
                  <Layout.Child childId="gridChild15">
                    <SingleSelectFilter
                      data={[
                        { key: 30, value: "30" },
                        { key: 60, value: "60" },
                        { key: 90, value: "90" }
                      ]}
                      onSelect={this.onSelectNumberOfDays}
                      selectedData={this.state.selectedNumberOfDays}
                      stateKey="selectedNumberOfDays"
                      label="Time Frame(Days)"
                    />
                  </Layout.Child>
                  <Layout.Child childId="gridChild5">
                    <div className="padding">
                      <span className="margin--left--small margin--right--double">
                        <b> Ticker </b>-{" "}
                        {this.state.selectedRowProps.securityLocalName}
                        <b> Spn </b>- {this.state.selectedRowProps.spn}
                        <b> Counterparty </b>-{" "}
                        {this.state.selectedRowProps.cpeName}
                        <b> Book </b>- {this.state.selectedRowProps.bookName}
                      </span>
                    </div>
                  </Layout.Child>
                </Layout>
              </Layout.Child>
              <Layout.Divider />
              <Layout.Child size={9} childId="gridChild6">
                <Graph data={this.props.securityHistoryData} />
              </Layout.Child>
            </Layout>
          </Dialog>
        </React.Fragment>
      );
    } else if (this.props.view === "bundle") {
      if (typeof this.props.bundlePositionViewData.message !== "undefined" || this.props.bundlePositionViewData.length <= 0)
        return <Message messageData={this.props.marketDataSearchMessage} />;
      var bundlePositionGridOptions = gridOptions();
      RC = this.props.bundlePositionViewData[0].reportingCurrency
      grid = (
        <Layout>
          <Layout.Child size="Fit" childId="gridChild7">
            <MappingTagInfo view={this.props.view} />
          </Layout.Child>
          <Layout.Child size={8} childId="gridChild8">
            <MarketDataGrid
              data={this.props.bundlePositionViewData}
              gridId="BundlePositionData"
              gridColumns={bundlePositionViewColumns()}
              gridOptions={gridOptions("bundle")}
              resizeCanvas={this.props.resizeCanvas}
              fill={true}
            />
          </Layout.Child>
          <Layout.Child size="Fit" childId="rcGridChild2">
              {this.reportingCurrencyMessage(RC)}
            </Layout.Child>
        </Layout>
      );
    } else {
      if (typeof this.props.lotViewData.message !== "undefined" || this.props.lotViewData.length <= 0)
        return <Message messageData={this.props.marketDataSearchMessage} />;
      RC = this.props.lotViewData[0].reportingCurrency
      grid = (
        <Layout>
          <Layout.Child size={8} childId="gridChild9">
            <MarketDataGrid
              data={this.props.lotViewData}
              gridId="LotData"
              gridColumns={lotViewColumns()}
              gridOptions={gridOptions("lot")}
              resizeCanvas={this.props.resizeCanvas}
              onCellClick={this.onCellClickHandler}
              fill={true}
            />
          </Layout.Child>
          <Layout.Child size="Fit" childId="rcGridChild3">
              {this.reportingCurrencyMessage(RC)}
          </Layout.Child>
          {this.props.securityMarketData.length > 0 && (
            <Layout.Child size={4} childId="gridChild10">
              <Panel
                dismissible
                onClose={() => {
                  this.props.destroySecurityMarketData();
                }}
                title={this.state.selectedSecurityDescription}
              >
                <AvailabilityDataPanel
                  data={this.props.securityMarketData}
                  selectedCpeIdForDrillDown={
                    this.state.selectedCpeIdForDrillDown
                  }
                />
              </Panel>
            </Layout.Child>
          )}
        </Layout>
      );
    }

    return grid;
  }

  render() {
    return (
      <React.Fragment>
        <NegotiationDialog />
        {this.renderGridData()}
      </React.Fragment>
    );
  }
}

function getMarketDataFilterPayload(arg, view) {
  var payload = {
    "localStartDate": arg.localDate,
    "localEndDate": arg.localDate,
    "pnlSpns": [arg.pnlSpn],
    "bookIds": [arg.bookId],
    "reportingCurrencyId": arg.reportingCurrencyId,
    "custodianAccountIds": [arg.custodianAccountId],
    "borrowTypeIds": [arg.borrowTypeId],
    "includeBorrow":
      arg.positionType === "Cash" ? true : false,
    "includeSwap": arg.positionType === "Swap" ? true : false
  };

  if (view === "lot") {
    payload["onlyLotLevelRecords"] = true;
    payload["enrichWithSlfRebateFinancingFee"] = true;
  }

  return payload;
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroySecurityMarketData,
      fetchSecurityHistoryData,
      fetchSecurityMarketData,
      fetchBundlePositionViewMarketData,
      fetchLotViewMarketData,
      loadNegotiationPopUp
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    positionViewData: state.marketData.positionViewData,
    bundlePositionViewData: state.marketData.bundlePositionViewData,
    lotViewData: state.marketData.lotViewData,
    securityHistoryData: state.marketData.securityHistoryData,
    securityMarketData: state.marketData.securityMarketData,
    marketDataSearchMessage: state.marketData.marketDataSearchMessage,
    resizeCanvas: state.marketData.resizeCanvas
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
