import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Card, Layout, DateRangePicker } from "arc-react-components";
import Warning from "commons/components/Warning";
import CurrencyFilter from "commons/container/CurrencyFilter";
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import Sidebar from "commons/components/Sidebar";
import Label from "commons/components/Label";
import ColumnLayout from "commons/components/ColumnLayout";
import Column from "commons/components/Column";
import CheckboxFilter from "commons/components/CheckboxFilter";
import FilterButton from "commons/components/FilterButton";
import CopySearchUrl from "commons/components/CopySearchUrl";
import GenericSecurityFilter from "commons/components/GenericSecurityFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import BookFilter from "commons/container/BookFilter";
import CpeFilter from "commons/container/CpeFilter";
import BusinessUnitFilter from "commons/container/BusinessUnitFilter";
import BundleFilter from "commons/container/BundleFilter";
import BorrowTypeFilter from "commons/container/BorrowTypeFilter";
import BundlePositionTypeFilter from "commons/container/BundlePositionTypeFilter";
import MappingTagFilter from "commons/container/MappingTagFilter";
import ClassificationFilter from "../components/ClassificationFilter";
import Dialog from "commons/components/Dialog";
import { createAPIPayload } from "../util";
import { CLASS_NAMESPACE } from "commons/ClassConfigs";
import {
  fetchPositionViewMarketData,
  fetchBundlePositionViewMarketData,
  fetchLotViewMarketData,
  destroySecurityMarketData,
  resizeCanvas
} from "../actions";
import {
  getPreviousBusinessDay,
  getCommaSeparatedValues,
  getCommaSeparatedListValue,
  getCommaSeparatedValuesOrNullForSingleSelect,
  isAllKeySelected,
  getDateDifference,
  executeCopySearchUrl,
  convertDatetoLocalDate
} from "commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.state = {
      showInvalidSPNDailog: false
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  resizeCanvas() {
    this.props.resizeCanvas();
  }

  getDefaultFilters() {
    return {
      securityFilter: {
        selectedSpns: "",
        selectedSecuritySearchType: { key: "TICKER", value: "TICKER" },
        selectedTextSearchType: { key: "EXACT_MATCH", value: "EXACT_MATCH" },
        securitySearchString: "",
        isAdvancedSearch: false
      },
      selectedLegalEntities: [],
      selectedCurrencies: [],
      selectedReportingCurrency: { key: 1760000, value: `USD [1760000]`},
      selectedCpes: [],
      selectedStartDate: getPreviousBusinessDay(),
      selectedEndDate: getPreviousBusinessDay(),
      selectedBooks: [],
      selectedBusinessUnits: [],
      selectedBundles: [],
      selectedBorrowType: [{ key: 0, value: "Borrow" }],
      selectedClassification: [],
      selectedMappingTag: [],
      selectedBundlePositionType: [],
      includeCash: true,
      includeWeekendData: false,
      includeSwap: false,
      includeRepo: false,
      includeOnlyExcessBorrow: false,
      includeOnlyLots: false,
      toggleSidebar: false,
      showInvalidSPNDailog: false
    };
  }

  componentDidMount() {
    this.node.addEventListener("keydown", this.enterFunction, false);
    let copySearchUrl = executeCopySearchUrl();
    if (copySearchUrl.isTrue) {
      this.setState(copySearchUrl.copySearchUrlState, () => this.handleClick());
    }
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleCloseDialog() {
    this.setState({
      showInvalidSPNDailog: false
    });
  }

  enterFunction = e => {
    if (e.keyCode == 13) {
      const node = e.path[0];
      if (node.id !== undefined && node.id === "sidebar") {
        this.handleClick();
        this.node.blur();
      } else this.node.focus();
    }
  };


  applySavedFilters = (selectedFilters) => {
    this.setState(selectedFilters);
  }

  handleClick() {
    var payload = {
      "localStartDate": convertDatetoLocalDate(this.state.selectedStartDate),
      "localEndDate": convertDatetoLocalDate(this.state.selectedEndDate),
      ...(this.state.selectedCpes.length > 0 &&
        !isAllKeySelected(this.state.selectedCpes)
        ? {
          "cpeIds":
            this.state.selectedCpes

        }
        : null),
      ...(this.state.selectedLegalEntities.length > 0 &&
        !isAllKeySelected(this.state.selectedLegalEntities)
        ? {
          "legalEntityIds":
            this.state.selectedLegalEntities

        }
        : null),
      ...(this.state.securityFilter.isAdvancedSearch &&
        this.state.securityFilter.securitySearchString.trim() !== ""
        ?{ "securityFilter": {
          "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.model.common.security.SecurityFilter,
          "securitySearchType": this.state
            .securityFilter.selectedSecuritySearchType.value,
          "textSearchType": this.state
            .securityFilter.selectedTextSearchType.value,
          "searchStrings":
            this.state.securityFilter.securitySearchString
          },
          "brokerSecurityFilter": {
            "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.BrokerSecurityFilter,
            "brokerSecuritySearchType": this
              .state.securityFilter.selectedSecuritySearchType.value,
            "textSearchType": this.state
              .securityFilter.selectedTextSearchType.value,
            "searchStrings":
              this.state.securityFilter.securitySearchString

          }
        }
        : {
          ...(this.state.securityFilter.selectedSpns &&
            this.state.securityFilter.selectedSpns.trim() !== ""
            ? {
              "pnlSpns": getCommaSeparatedListValue(
                this.state.securityFilter.selectedSpns
              )
            }
            : null)
        }),
      ...(this.state.selectedCurrencies.length > 0
        ? {
          "currencyIds":
            this.state.selectedCurrencies
        }
        : null),
      ...(!_.isEmpty(this.state.selectedReportingCurrency)
        ? {
          "reportingCurrencyId":
            this.state.selectedReportingCurrency
        }
        : null),
      ...(this.state.selectedBorrowType.length > 0
        ? {
          "borrowTypeIds":
            this.state.selectedBorrowType
        }
        : null),
      ...(this.state.selectedClassification.length > 0
        ? {
          "classifications":
            this.state.selectedClassification
        }
        : null),
      ...(this.state.selectedMappingTag.length > 0
        ? {
          "mappingTags":
            this.state.selectedMappingTag
        }
        : null),
      ...(this.state.selectedBundlePositionType.length > 0
        ? {
          "bundlePositionTypes":
            this.state.selectedBundlePositionType
        }
        : null),
      "includeBorrow": this.state.includeCash,
      "includeSwap": this.state.includeSwap,
      "includeWeekendData": this.state.includeWeekendData,
      "includeOnlyExcessBorrow": this.state
        .includeOnlyExcessBorrow
    };
    if (this.props.view === "position") {
      if (
        this.state.selectedBooks.length > 0 &&
        !isAllKeySelected(this.state.selectedBooks)
      ) {
        payload["bookIds"] =
          this.state.selectedBooks
        ;
      }
      // let dateDiff=getDateDifference(new Date(this.state.selectedStartDate), new Date(this.state.selectedEndDate)) ;
      // let invalidSPNs= payload["marketDataFilter.pnlSpns"] == null || payload["marketDataFilter.pnlSpns"] == undefined || payload["marketDataFilter.pnlSpns"].trim()=="";
      // if(dateDiff>0 && invalidSPNs && !this.state.securityFilter.isAdvancedSearch) {
      //     this.setState({showInvalidSPNDailog:true});
      // }
      // else{
      this.setState({ showInvalidSPNDailog: false });
      this.props.fetchPositionViewMarketData(createAPIPayload(payload));
      this.props.destroySecurityMarketData();
      this.setState({ toggleSidebar: !this.state.toggleSidebar });
      //}
    } else if (this.props.view === "bundle") {
      if (
        this.state.selectedBooks.length > 0 &&
        !isAllKeySelected(this.state.selectedBooks)
      ) {
        payload["bookIds"] =
          this.state.selectedBooks;
      }
      if (
        this.state.selectedBusinessUnits.length > 0 &&
        !isAllKeySelected(this.state.selectedBusinessUnits)
      ) {
        payload["businessUnitIds"] =
          this.state.selectedBusinessUnits;
      }
      if (this.state.selectedBundles.length > 0) {
        payload["bundleIds"] =
          this.state.selectedBundles;
      }
      if (this.state.selectedBundlePositionType.length > 0) {
        payload[
          "bundlePositionTypes"
        ] = this.state.selectedBundlePositionType;
      }

      payload["includeOnlyExcessBorrow"] = false;
      this.props.fetchBundlePositionViewMarketData(createAPIPayload(payload));
      this.props.destroySecurityMarketData();
      this.setState({ toggleSidebar: !this.state.toggleSidebar });
    } else {
      if (
        this.state.selectedBooks.length > 0 &&
        !isAllKeySelected(this.state.selectedBooks)
      ) {
        payload["bookIds"] =
          this.state.selectedBooks;
      }
      payload["onlyLotLevelRecords"] = false;
      payload["enrichWithSlfRebateFinancingFee"] = true;
      this.props.fetchLotViewMarketData(createAPIPayload(payload));
      this.props.destroySecurityMarketData();
      this.setState({ toggleSidebar: !this.state.toggleSidebar });
    }
  }

  onDateChange = date => {
    this.setState({ selectedStartDate: date[0], selectedEndDate: date[1] });
  };

  render() {
    let spnDialog = null;
    let dateDiff = getDateDifference(
      this.state.selectedStartDate,
      this.state.selectedEndDate
    );
    let warnDialog = null;
    if (this.state.showInvalidSPNDailog) {
      spnDialog = (
        <Dialog
          isOpen={this.state.showInvalidSPNDailog}
          title="Enter valid SPN"
          onClose={this.handleCloseDialog}
          style={{
            height: "200px"
          }}
        >
          <h3>SPNs have to be entered for searching over a Date Range</h3>
        </Dialog>
      );
      //this.setState(this.getDefaultFilters());
    }
    if (dateDiff >= 3) {
      warnDialog = (
        <Warning messageData="Search over a large Date Range may affect the performance" />
      );
    }

    return (
      <React.Fragment>
        {spnDialog}
        <div
          id="sidebar"
          ref={node => (this.node = node)}
          tabindex="0"
          style={{ height: "100%" }}
        >
          <Sidebar
            collapsible={true}
            size="200px"
            resizeCanvas={this.resizeCanvas}
          >
            <Card>
              {warnDialog}
              <DateRangePicker
                placeholder="Select Date"
                value={[
                  this.state.selectedStartDate,
                  this.state.selectedEndDate
                ]}
                onChange={this.onDateChange}
              />
              <br />
              <CheckboxFilter
                defaultChecked={this.state.includeWeekendData}
                onSelect={this.onSelect}
                stateKey="includeWeekendData"
                label="Include Weekend Data"
                style="left"
              />
            </Card>
            <Card>
              <GenericSecurityFilter
                onSelect={this.onSelect}
                selectedData={this.state.securityFilter}
                stateKey="securityFilter"
              />
            </Card>
            <Card>
              <LegalEntityFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedLegalEntities}
              />
              <CpeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCpes}
              />
              <BookFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedBooks}
              />
              {this.props.view === "bundle" ? (
                <React.Fragment>
                  <BusinessUnitFilter
                    onSelect={this.onSelect}
                    selectedData={this.state.selectedBusinessUnits}
                  />
                  <BundleFilter
                    onSelect={this.onSelect}
                    selectedData={this.state.selectedBundles}
                  />
                </React.Fragment>
              ) : null}
              <CurrencyFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCurrencies}
              />
            </Card>
            <Card>
              <Label label="Position Type" />
              <Column>
                <CheckboxFilter
                  defaultChecked={this.state.includeCash}
                  onSelect={this.onSelect}
                  stateKey="includeCash"
                  label="Cash"
                  style="left"
                />
                <CheckboxFilter
                  defaultChecked={this.state.includeSwap}
                  onSelect={this.onSelect}
                  stateKey="includeSwap"
                  label="Swap"
                  style="left"
                />
                <CheckboxFilter
                  defaultChecked={this.state.includeRepo}
                  onSelect={this.onSelect}
                  stateKey="includeRepo"
                  label="Repo"
                  style="left"
                />
              </Column>
            </Card>
            <Card>
              <BorrowTypeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedBorrowType}
              />
              <br />
              {(this.props.view === "position" || this.props.view === "lot")? (
                <React.Fragment>
                  <CheckboxFilter
                    defaultChecked={this.state.includeOnlyExcessBorrow}
                    onSelect={this.onSelect}
                    stateKey="includeOnlyExcessBorrow"
                    label="Only Excess Borrows"
                    style="left"
                  />
                </React.Fragment>
              ) : null}
            </Card>
            <Card>
              <ClassificationFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedClassification}
              />
            </Card>
            <Card>
              {this.props.view === "position" ? (
                <React.Fragment>
                  <MappingTagFilter
                    onSelect={this.onSelect}
                    selectedData={this.state.selectedMappingTag}
                  />
                </React.Fragment>
              ) : null}
              {this.props.view === "bundle" ? (
                <React.Fragment>
                  <BundlePositionTypeFilter
                    onSelect={this.onSelect}
                    selectedData={this.state.selectedBundlePositionType}
                  />
                </React.Fragment>
              ) : null}
            </Card>
            <Card>
              <ReportingCurrencyFilter
                label="Reporting Currency"
                onSelect={this.onSelect}
                selectedData={this.state.selectedReportingCurrency}
                multiSelect={false}
                stateKey="selectedReportingCurrency"
              />
            </Card>
            <ColumnLayout>
              <FilterButton
                onClick={this.handleClick}
                reset={false}
                label="Search"
              />
              <FilterButton
                reset={true}
                onClick={this.handleReset}
                label="Reset"
              />
              <CopySearchUrl
                copySearchParams={this.state}
              />
            </ColumnLayout>
          </Sidebar>
        </div>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchPositionViewMarketData,
      fetchBundlePositionViewMarketData,
      fetchLotViewMarketData,
      destroySecurityMarketData,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(SideBar);
