import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  FETCH_POSITION_VIEW_MARKET_DATA,
  FETCH_BUNDLE_POSITION_VIEW_MARKET_DATA,
  FETCH_LOT_VIEW_MARKET_DATA,
  FETCH_SECURITY_HISTORY_DATA,
  FETCH_SECURITY_MARKET_DATA,
  UPDATE_MARKET_DATA_SEARCH_MESSAGE,
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
  RESIZE_CANVAS,
  DESTROY_DATA_FOR_SECURITY_MARKET_DATA
} from "commons/constants";

import {
  getPositionViewMarketData,
  getBundlePositionViewMarketData,
  getLotViewMarketData,
  getSecurityHistoryData,
  getSecurityMarketData
} from "./api";

import { ToastService } from 'arc-react-components';

function* fetchPositionViewMarketData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getPositionViewMarketData, action.payload);
    yield put({ type: `${FETCH_POSITION_VIEW_MARKET_DATA}_SUCCESS`, data });

    if (typeof data.message !== "undefined") {
      getToastErrorMessage(data);
    }

    yield put({ type: UPDATE_MARKET_DATA_SEARCH_MESSAGE, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_POSITION_VIEW_MARKET_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function getToastErrorMessage(data) {
  var content = '';
  if(data.message.includes("At least one of Borrows or Swaps must be selected")) {
    content = 'At least one of Borrows or Swaps must be selected.';
  } else {
    content = 'Error occured while fetching data';
  }

  ToastService.append({
    content: content,
    type: ToastService.ToastType.CRITICAL,
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: 8000
  });
}

function* fetchBundlePositionViewMarketData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getBundlePositionViewMarketData, action.payload);
    yield put({
      type: `${FETCH_BUNDLE_POSITION_VIEW_MARKET_DATA}_SUCCESS`,
      data
    });

    if (typeof data.message !== "undefined") {
      getToastErrorMessage(data);
    }

    yield put({ type: UPDATE_MARKET_DATA_SEARCH_MESSAGE, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_BUNDLE_POSITION_VIEW_MARKET_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchLotViewMarketData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLotViewMarketData, action.payload);
    yield put({ type: `${FETCH_LOT_VIEW_MARKET_DATA}_SUCCESS`, data });

    if (typeof data.message !== "undefined") {
      getToastErrorMessage(data);
    }

    yield put({ type: UPDATE_MARKET_DATA_SEARCH_MESSAGE, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LOT_VIEW_MARKET_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSecurityHistoryData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getSecurityHistoryData, action.payload);
    yield put({ type: `${FETCH_SECURITY_HISTORY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SECURITY_HISTORY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSecurityMarketData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getSecurityMarketData, action.payload);
    yield put({ type: `${FETCH_SECURITY_MARKET_DATA}_SUCCESS`, data });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SECURITY_MARKET_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* destroySecurityMarketData(action) {
  try {
    yield put({ type: `${DESTROY_DATA_FOR_SECURITY_MARKET_DATA}_SUCCESS` });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  }
}

export function* securityHistoryData() {
  yield [takeEvery(FETCH_SECURITY_HISTORY_DATA, fetchSecurityHistoryData)];
}

export function* securityMarketData() {
  yield [
    takeEvery(FETCH_SECURITY_MARKET_DATA, fetchSecurityMarketData),
    takeEvery(DESTROY_DATA_FOR_SECURITY_MARKET_DATA, destroySecurityMarketData)
  ];
}

export function* positionViewMarketData() {
  yield [
    takeEvery(FETCH_POSITION_VIEW_MARKET_DATA, fetchPositionViewMarketData)
  ];
}

export function* bundlePositionViewMarketData() {
  yield [
    takeEvery(
      FETCH_BUNDLE_POSITION_VIEW_MARKET_DATA,
      fetchBundlePositionViewMarketData
    )
  ];
}

export function* lotViewMarketData() {
  yield [takeEvery(FETCH_LOT_VIEW_MARKET_DATA, fetchLotViewMarketData)];
}

function* marketDataSaga() {
  yield all([
    positionViewMarketData(),
    bundlePositionViewMarketData(),
    lotViewMarketData(),
    securityHistoryData(),
    securityMarketData()
  ]);
}

export default marketDataSaga;
