import { hot } from "react-hot-loader/root";
import React, { Component } from "react";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";
import Loader from "commons/container/Loader";
import { resetMarketDataSearchMessage } from "./actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class MarketData extends Component {
  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = [];
  }

  componentDidMount() {
    this.positionView.classList.add("active");
    this.setState({ view: "position" });
  }

  handleToggle(view) {
    this.positionView.classList.remove("active");
    this.bundlePositionView.classList.remove("active");
    this.lotView.classList.remove("active");
    switch (view) {
      case "position":
        this.positionView.classList.add("active");
        break;
      case "bundle":
        this.bundlePositionView.classList.add("active");
        break;
      case "lot":
        this.lotView.classList.add("active");
        break;
    }
    this.setState({ view });
    this.props.resetMarketDataSearchMessage();
  }
  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div
              slot="application-menu"
              className="application-menu-toggle-view"
            >
              <a
                ref={positionView => {
                  this.positionView = positionView;
                }}
                onClick={() => this.handleToggle("position")}
              >
                Position View
              </a>
              <a
                ref={bundlePositionView => {
                  this.bundlePositionView = bundlePositionView;
                }}
                onClick={() => this.handleToggle("bundle")}
              >
                Bundle Position View
              </a>
              <a
                ref={lotView => {
                  this.lotView = lotView;
                }}
                onClick={() => this.handleToggle("lot")}
              >
                Lot View
              </a>
            </div>
          </arc-header>
          <div className="layout--flex padding--top">
            <div className="size--content padding--right">
              <SideBar view={this.state.view} />
            </div>
            <Grid view={this.state.view} handleViewToggle={this.handleToggle} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetMarketDataSearchMessage
    },
    dispatch
  );
}

export default hot(connect(null, mapDispatchToProps)(MarketData));
