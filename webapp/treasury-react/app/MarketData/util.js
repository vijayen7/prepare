import { getSelectedFilterArray, getArrayFromString } from 'commons/util';
export function getIntegerArrayFromObjectArray(data) {
  var filterArray = [];
  data.forEach(item => {
    filterArray.push(item.key);
  });
  return filterArray;
}

export function createAPIPayload(payload) {
  var payload = Object.assign({}, payload);
  Object.keys(payload).forEach(function(key) {
    if (
      key !== "localStartDate" &&
      key !== "localEndDate" &&
      key !== "includeOnlyExcessBorrow" &&
      key !== "includeBorrow" &&
      key !== "includeSwap" &&
      key !== "includeWeekendData"
    ) {
      if (payload[key] === undefined || payload[key] == null || payload[key].length === 0) {
        payload[key] = null;
      } else if (key == "reportingCurrencyId") {
        payload[key] = `${payload[key].key}`
      } else if (key == "pnlSpns") {
        payload[key] = getIntegerArrayFromObjectArray(getSelectedFilterArray(payload[key]));
      } else if (key == "securityFilter" || key == "brokerSecurityFilter") {
        payload[key]["searchStrings"] = getArrayFromString(payload[key]["searchStrings"]);
      } else {
        payload[key] = getIntegerArrayFromObjectArray(payload[key]);
      }
    }
  });

  return payload;
}
