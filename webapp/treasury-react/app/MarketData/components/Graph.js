import React from "react";
import Highcharts from "highcharts";
import {
  HighchartsChart,
  Chart,
  withHighcharts,
  XAxis,
  YAxis,
  Legend,
  LineSeries
} from "react-jsx-highcharts";

const itemStyle = {
  color: "#5e90a7"
};

const labels = {
  style: {
    color: "#5e90a7"
  }
};

const style = {
  color: "#5e90a7"
};

const tooltip = {
  enabled: "true"
};

const colors = [
  "#7cb5ec",
  "#90ed7d",
  "#f7a35c",
  "#8085e9",
  "#f15c80",
  "#e4d354",
  "#2b908f",
  "#f45b5b",
  "#91e8e1"
];

const Graph = props => (
  <div className="chart-container">
    <HighchartsChart tooltip={tooltip} colors={colors}>
      <Chart backgroundColor={null} height={300} />
      <Legend
        itemStyle={itemStyle}
        layout="vertical"
        align="center"
        verticalAlign="top"
      />

      <XAxis categories={props.data.date} labels={labels}>
        <XAxis.Title style={style}>Date</XAxis.Title>
      </XAxis>

      <YAxis id="number" labels={labels}>
        <YAxis.Title style={style}>Rate</YAxis.Title>
        <LineSeries
          id="stockLoanFeeRate"
          name="Stock Loan Fee Rate"
          data={props.data.slfFee}
        />
        <LineSeries
          id="financingFeeRate"
          name="Financing Fee Rate"
          data={props.data.financingFeeRate}
        />
        <LineSeries
          id="rebateRate"
          name="Rebate Rate"
          data={props.data.rebateRate}
        />
      </YAxis>
      <YAxis opposite id="number2" labels={labels}>
        <YAxis.Title style={style}>Quantity</YAxis.Title>
        <LineSeries id="quantity" name="Quantity" data={props.data.quantity} />
      </YAxis>
    </HighchartsChart>
  </div>
);

export default withHighcharts(Graph, Highcharts);
