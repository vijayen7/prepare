import React from "react";
import { Layout } from "arc-react-components";

function renderPositionView() {
  return (
    <div className="legend">
      <Layout isColumnType={true}>
        <Layout.Child childId="mappingChild1">
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              <b>Mapped </b>: Broker position is mapped to an internal position;
              both quantities match. For lend/rehype, Broker position quantity
              is less than internal position
            </span>
          </div>
        </Layout.Child>
        <Layout.Child childId="mappingChild2">
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              <b>Quantity Mismatch </b>: Broker position is mapped to an
              internal position; quantities do not match. For lend/rehype,
              Broker position is more than internal position quantity
            </span>
          </div>
        </Layout.Child>
        <Layout.Child childId="mappingChild3">
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              <b>Internal Missing </b>: Broker position is not mapped to an
              internal position
            </span>
          </div>
        </Layout.Child>
      </Layout>
      <Layout isColumnType={true}>
        <Layout.Child childId="mappingChild4">
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              <b>Broker Missing </b>: There is no corresponding broker position
              for this internal position
            </span>
          </div>
        </Layout.Child>
        <Layout.Child childId="mappingChild5">
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              <b>Date format</b>: <i>YYYYMMDD</i>
            </span>
          </div>
        </Layout.Child>
        <Layout.Child childId="mappingChild6"></Layout.Child>
      </Layout>
    </div>
  );
}

function renderBundlePositionView() {
  return (
    <div className="legend">
      <Layout isColumnType={true}>
        <Layout.Child childId="mappingChild7">
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              <b>Netted </b>: Bundle position is completely netted by other
              bundle positions under the same position; direction will be
              opposite to the position
            </span>
          </div>
        </Layout.Child>
        <Layout.Child childId="mappingChild8">
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              <b>Partially Netted </b>: Bundle position is partially netted by
              other bundle positions under the same position; direction of same
              as the position
            </span>
          </div>
        </Layout.Child>
        <Layout.Child childId="mappingChild9">
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              <b>Short </b>: Bundle position is not netted; direction same as
              the position
            </span>
          </div>
        </Layout.Child>
      </Layout>
    </div>
  );
}
const MappingTagInfo = props => {
  switch (props.view) {
    case "position":
      return renderPositionView();
    case "bundle":
      return renderBundlePositionView();
  }
  return null;
};

export default MappingTagInfo;
