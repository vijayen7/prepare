import { ReactArcGrid } from "arc-grid";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import _ from "lodash";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderLabel = this.renderLabel.bind(this);
    this.renderGrid = this.renderGrid.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.metaData = this.metaData.bind(this);
  }

  renderLabel(props) {
    return props.label !== undefined ? (
      <div className="text-align--center margin--vertical--small">
        {props.label}
      </div>
    ) : null;
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (_.isEqual(this.props.data, nextProps.data)) {
      return false;
    } else {
      return true;
    }
  }

  componentWillReceiveProps(prevProps) {
    var gridRef = this.getArcGridObject();
    gridRef.dataView.oldItemMetadata = gridRef.dataView.getItemMetadata;
    gridRef.dataView.getItemMetadata = this.metaData();
  }

  metaData() {
    return function(row) {
      var dataItem = gridRef.dataView.getItem(row);
      var meta = gridRef.dataView.oldItemMetadata(row) || {};

      return meta;
    };
  }

  gridUpdateCallback = () => {
    return false;
  };

  renderGrid(props) {
    if (props.data !== undefined && props.data.length > 0) {
      return (
        <ReactArcGrid
          gridId={props.gridId}
          data={props.data}
          columns={props.gridColumns}
          options={props.gridOptions}
          shouldGridForceUpdate={this.gridUpdateCallback}
          getGridReference={getArcGridObject => {
            this.getArcGridObject = getArcGridObject;
          }}
        />
      );
    }
    return null;
  }

  getStyle() {
    return this.props.variableHeight ? {} : { height: "400px" };
  }

  render() {
    var style = this.getStyle();
    return (
      <div style={style}>
        {this.renderLabel(this.props)}
        {this.renderGrid(this.props)}
      </div>
    );
  }
}

export default Grid;