import { url } from "../api";
import { getSaveSettingsUrl } from "commons/util";
import {
  getSelectedViewDefaultColumns,
  getSelectedViewTotalColumns
} from "./columnSelector";
export function gridOptions(view) {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    },
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    frozenColumn: 2,
    sortList: [
      {
        columnId: "date",
        sortAsc: true
      },
      {
        columnId: "description",
        sortAsc: true
      }
    ],
    page: true,
    onRowClick: function() {
      // highlight on row works if this callback is defined
    },
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    saveSettings: {
      applicationId: 3,
      applicationCategory: "MarketData_" + view,
      serviceURL: getSaveSettingsUrl(),
      onBeforeLoadedCallback: function(settingsData) {},
      loadedCallback: function(gridObject) {}
    },
    customColumnSelection: {
      defaultcolumns: getSelectedViewDefaultColumns(view),
      totalColumns: getSelectedViewTotalColumns(view)
    }
  };
  if (view === "bundle") {
    options.groupColumns = [
      [
        "Broker Position",
        "brokerPositionQuantity",
        "brokerPositionFinancingFeeRate"
      ],
      ["Internal Position", "internalQuantity", "internalMV"]
    ];
    options.sheetName = "Bundle_Position_View";
    options.saveSettings.applicationCategory = "BundleViewMarketData";
  } else if (view === "position") {
    options.sheetName = "Position_View";
    options.saveSettings.applicationCategory = "PositionViewMarketData";
  } else {
    options.sheetName = "Lot_Level_Position_View";
    options.saveSettings.applicationCategory = "LotViewMarketData";
  }
  return options;
}
