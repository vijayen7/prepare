import {
  linkFormatter,
  numberFormat,
  marketDataPositionTypeFormatter
} from "commons/grid/formatters";
import { textComparator } from "commons/grid/comparators";
import { convertJavaNYCDate } from "commons/util";
var columns = [];

export function feeColumns() {
  var feeColumns = [
    {
      id: "rebateFeeRC",
      name: "Rebate Fee RC",
      field: "rebateFeeRC",
      toolTip: "Rebate Fee RC",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      width: 90
    },
    {
      id: "slfRC",
      name: "SL Fee RC",
      field: "slfFeeRC",
      toolTip: "SL Fee RC",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      width: 70
    },
    {
      id: "financingFeeRC",
      name: "Financing Fee RC",
      field: "financingFeeRC",
      toolTip: "Financing Fee RC",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      width: 90
    },
    {
      id: "classification",
      name: "Classification",
      field: "classificationName",
      toolTip: "Classification",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50
    }
  ];

  return feeColumns;
}

export function excessBorrowRateAndFeeColumns(){
  var excessBorrowRateAndFeeColumns = [

    {
      id: "excessBorrowRebateRate",
      name: "Excess Borrow Rebate Rate",
      field: "excessBorrowRebateRate",
      toolTip: "Excess Borrow Rebate Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "excessBorrowSlfRate",
      name: "Excess Borrow SLF Rate",
      field: "excessBorrowStockLoanFeeRate",
      toolTip: "Excess Borrow SLF Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberFormat(value, 4);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "excessBorrowFinancingFeeRate",
      name: "Excess Borrow  Financing Fee Rate",
      field: "excessBorrowFinancingFeeRate",
      toolTip: "Excess Borrow Financing Fee Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "brokerExcessBorrowMarketValueRC",
      name: "Broker Excess Borrow MV (RC)",
      field: "brokerExcessBorrowMarketValueRC",
      toolTip: "Broker Excess Borrow MV (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      width: 90
    },
    {
      id: "excessBorrowSlfFeeRC",
      name: "Excess Borrow SL Fee RC",
      field: "excessBorrowSlfFeeRC",
      toolTip: "Excess Borrow SL Fee RC",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      width: 70
    }

  ];

  return excessBorrowRateAndFeeColumns;
}

export function rateColumns() {
  var rateColumns = [
    {
      id: "rebateRate",
      name: "Rebate Rate",
      field: "rebateRate",
      toolTip: "Rebate Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberFormat(value, 4);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "slfRate",
      name: "SLF Rate",
      field: "stockLoanFeeRate",
      toolTip: "SLF Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberFormat(value, 4);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "financingFeeRate",
      name: "Financing Fee Rate",
      field: "financingFeeRate",
      toolTip: "Financing Fee Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberFormat(value, 4);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    }
  ];

  return rateColumns;
}

export function otherColumns() {
  var otherColumns = [
    {
      id: "gboType",
      name: "GBO Type",
      field: "gboType",
      toolTip: "GBO Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "spn",
      name: "SPN",
      field: "spn",
      toolTip: "SPN",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 65
    },
    {
      id: "pnlSpn",
      name: "PNL SPN",
      field: "pnlSpn",
      toolTip: "PNL SPN",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 65
    },
    {
      id: "country",
      name: "Country",
      field: "country",
      toolTip: "Country",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "market",
      name: "Market",
      field: "market",
      toolTip: "Market",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 220
    },
    {
      id: "brokerPositionPriceRC",
      name: "Broker Price (RC)",
      field: "brokerPriceRC",
      toolTip: "Broker Price (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60
    },
    {
      id: "baseRate",
      name: "Base Rate",
      field: "baseRate",
      toolTip: "Base Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
       return numberFormat(value, 4);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "debitSpread",
      name: "Debit Spread",
      field: "debitSpread",
      toolTip: "Debit Spread",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberFormat(value, 4);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "shortDescription",
      name: "Short Description",
      field: "shortDescription",
      toolTip: "shortDescription",
      type: "text",
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div>" +
          (value == null ? "n/a" : value) +
          "</div>"
      },
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 200
    },
    {
      id: "brokerISIN",
      name: "E-ISIN",
      field: "brokerISIN",
      toolTip: "Broker ISIN",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "brokerCUSIP",
      name: "E-CUSIP",
      field: "brokerCUSIP",
      toolTip: "Broker CUSIP",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "brokerSedol",
      name: "E-Sedol",
      field: "brokerSedol",
      toolTip: "Broker Sedol",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "brokerTicker",
      name: "E-Ticker",
      field: "brokerTicker",
      toolTip: "Broker Ticker",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "brokerSecurityName",
      name: "E-Description",
      field: "brokerSecurityName",
      toolTip: "Broker Description",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "brokerCurrency",
      name: "E-Ccy",
      field: "brokerCurrency",
      toolTip: "Broker Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    }
  ];
  return otherColumns;
}
export function commonColumns(isPositionView) {
  var columns = [
    {
      id: "description",
      name: "I-Description",
      field: "securityDescription",
      toolTip: "Description",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 200
    },
    {
      id: "sedol",
      name: "I-Sedol",
      field: "sedol",
      toolTip: "sedol",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 60
    },
    {
      id: "ticker",
      name: "I-Ticker",
      field: "securityLocalName",
      toolTip: "ticker",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50
    },
    {
      id: "cusip",
      name: "I-CUSIP",
      field: "cusip",
      toolTip: "cusip",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 60
    },
    {
      id: "isin",
      name: "I-ISIN",
      field: "isin",
      toolTip: "isin",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 70
    },
    {
      id: "currency",
      name: "I-Ccy",
      field: "currencyName",
      toolTip: "Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50
    },
    {
      id: "sourceDate",
      name: "Source Date",
      field: "localSourceDate",
      toolTip: "Source Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return value;
      },
      excelDataFormatter: function(value) {
        return value;
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "date",
      name: "Date",
      field: "localDate",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return value;
      },
      excelDataFormatter: function(value) {
        return value;
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "legalEntity",
      name: "Legal Entity",
      field: "legalEntityName",
      toolTip: "Legal Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "custodianAccount",
      name: "Custodian Account",
      field: "custodianAccDisplayName",
      toolTip: "Custodian Account",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 150
    },

    {
      id: "positionType",
      name: "C/S/R",
      field: "positionType",
      toolTip: "Cash/Swap/Repo",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50
    }
  ];
  return columns;
}

export function negotiationColumns() {
  var columns = [
    {
      id: "negotiation",
      name: "Negotiation",
      field: "negotiation",
      toolTip: "Negotiation",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (
          (dataContext.borrowType === "BORROW" ||
            dataContext.borrowType === "LEND") &&
          (dataContext.pnlSpn !== -1 && dataContext.pnlSpn !== -2) &&
          dataContext.brokerPositionType !== "BROKER_MISSING"
        )
          return linkFormatter(
            row,
            cell,
            "<i class='icon-link' />",
            columnDef,
            dataContext
          );
        else return null;
      },
      headerCssClass: "b",
      width: 85
    }
  ];
  return columns;
}

export function positionViewColumns() {
  var columns = commonColumns(true);
  var book = {
    id: "book",
    name: "Book",
    field: "bookName",
    toolTip: "Book",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b",
    width: 80
  };
  columns.splice(8, 0, book);

  var mappingTag = {
    id: "mappingTag",
    name: "Mapping Tag",
    field: "brokerPositionType",
    toolTip: "Mapping Tag",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.0",
    width: 120
  };
  columns.splice(9, 0, mappingTag);

  var history = {
    id: "history",
    name: "History",
    field: "history",
    toolTip: "History",
    type: "text",
    filter: true,
    sortable: true,
    formatter: function(row, cell, value, columnDef, dataContext) {
      if (
        dataContext.brokerPositionType !== "INTERNAL_MISSING" ||
        dataContext.brokerPositionType === "QUANTITY_MISMATCH"
      )
        return linkFormatter(
          row,
          cell,
          "<i class='icon-line-chart' />",
          columnDef,
          dataContext
        );
      else return null;
    },
    headerCssClass: "b",
    width: 50
  };
  columns.splice(10, 0, history);

  columns = columns.concat(negotiationColumns());

  var otherDetails = [
    {
      id: "bundleView",
      name: "Bundle View",
      field: "bundleView",
      toolTip: "Bundle View",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (
          dataContext.brokerPositionType !== "INTERNAL_MISSING" &&
          (dataContext.brokerPositionType !== "ROLLED_OVER" ||
            dataContext.internalQuantity)
        )
          return linkFormatter(
            row,
            cell,
            "<i class='icon-link' />",
            columnDef,
            dataContext
          );
        else return null;
      },
      headerCssClass: "b",
      width: 85
    },
    {
      id: "lotView",
      name: "Lot View",
      field: "lotLevelIdentifier",
      toolTip: "Lot View",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (dataContext.hasLotLevelData)
          return linkFormatter(
            row,
            cell,
            "<i class='icon-link' />",
            columnDef,
            dataContext
          );
        else return null;
      },
      headerCssClass: "b",
      width: 85
    },
    {
      id: "tradeDateQuantity",
      name: "Trade Date Quantity",
      field: "tradeDateQuantity",
      toolTip: "Trade Date Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 85
    },
    {
      id: "internalQuantity",
      name: "Settle Date Quantity",
      field: "internalQuantity",
      toolTip: "Settle Date Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 85
    },
    {
      id: "quantity",
      name: "Broker Shorted Quantity",
      field: "brokerQuantity",
      toolTip: "Broker Shorted Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "excessBorrowQuantity",
      name: "Excess Borrow Quantity",
      field: "brokerExcessBorrowQuantity",
      toolTip: "Excess Borrow Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "totalQuantity",
      name: "Total Quantity",
      field: "totalQuantity",
      toolTip: "Total Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "quantityDifference",
      name: "Quantity Difference",
      field: "quantityDifference",
      toolTip: "Quantity Difference",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },

    {
      id: "internalPriceRC",
      name: "Internal Price (RC)",
      field: "internalPriceRC",
      toolTip: "Internal Price (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 50
    },
    {
      id: "priceRC",
      name: "Broker Price (RC)",
      field: "brokerPriceRC",
      toolTip: "Price (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 50
    },
    {
      id: "internalMVRC",
      name: "Internal MV (RC)",
      field: "internalMarketValueRC",
      toolTip: "Internal MV (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 90
    },
    {
      id: "mvRC",
      name: "Broker MV (RC)",
      field: "brokerMarketValueRC",
      toolTip: "MV (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 90
    }
  ];
  columns = columns.concat(otherDetails);

  columns = columns.concat(rateColumns());

  columns = columns.concat(feeColumns());

  columns = columns.concat(excessBorrowRateAndFeeColumns());

  var borrowType = {
    id: "borrowType",
    name: "B/L/R",
    field: "borrowType",
    toolTip: "Borrow/Lend/Rehype",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b",
    width: 70
  };

  columns.push(borrowType);

  columns = columns.concat(otherColumns());
  return columns;
}

export function bundlePositionViewColumns() {
  var columns = commonColumns(false);

  var buBundle = [
    {
      id: "businessUnit",
      name: "Business Unit",
      field: "businessUnitName",
      toolTip: "Business Unit",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "bundle",
      name: "Bundle",
      field: "bundleName",
      toolTip: "Bundle",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "book",
      name: "Book",
      field: "bookName",
      toolTip: "Book",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80
    }
  ];
  columns = columns.concat(buBundle);

  var otherDetails = [
    {
      id: "bundlePositionType",
      name: "Bundle Position Type",
      field: "bundlePositionType",
      toolTip: "Bundle Position Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "internalQuantity",
      name: "Quantity",
      field: "settleDateQuantity",
      toolTip: "Internal Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      width: 70
    },
    {
      id: "internalPriceRC",
      name: "Price (RC)",
      field: "priceRC",
      toolTip: "Internal Price (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60
    },
    {
      id: "internalMVRC",
      name: "MV (RC)",
      field: "settleDateMarketValueRC",
      toolTip: "Internal MV (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      width: 80
    },
    {
      id: "brokerPositionQuantity",
      name: "Broker Quantity",
      field: "brokerQuantity",
      toolTip: "Broker Position Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 70
    },
    {
      id: "brokerPositionPriceRC",
      name: "Price (RC)",
      field: "brokerPriceRC",
      toolTip: "Broker Position Price (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60
    },
    {
      id: "brokerPositionMvRC",
      name: "MV (RC)",
      field: "brokerMarketValueRC",
      toolTip: "Broker Position MV (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "brokerPositionSlfRate",
      name: "SLF Rate",
      field: "stockLoanFeeRate",
      toolTip: "Broker Position  SLF Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberFormat(value, 4);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "brokerPositionFinancingFeeRate",
      name: "Financing Fee Rate",
      field: "financingFeeRate",
      toolTip: "Broker Position  Financing Fee Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberFormat(value, 4);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      width: 50
    },
    {
      id: "rateSource",
      name: "Rate Source",
      field: "rateSource",
      toolTip: "Rate Source",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    }
  ];
  columns = columns.concat(otherDetails);
  columns = columns.concat(otherColumns());

  var mappingTag = {
    id: "mappingTag",
    name: "Mapping Tag",
    field: "mappingTag",
    toolTip: "Mapping Tag",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.0",
    width: 120
  };
  // columns.push(mappingTag);

  return columns;
}

export function lotViewColumns() {
  var columns = commonColumns(true);
  var book = {
    id: "book",
    name: "Book",
    field: "bookName",
    toolTip: "Book",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b",
    width: 80
  };
  columns.splice(8, 0, book);

  columns = columns.concat(negotiationColumns());

  var otherDetails = [
    {
      id: "quantity",
      name: "Broker Quantity",
      field: "brokerQuantity",
      toolTip: "Broker Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "mvRC",
      name: "Broker MV (RC)",
      field: "brokerMarketValueRC",
      toolTip: "Broker Position MV (RC)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 90
    }
  ];
  columns = columns.concat(otherDetails);

  columns = columns.concat(rateColumns());

  columns = columns.concat(feeColumns());

  var borrowType = {
    id: "borrowType",
    name: "B/L/R",
    field: "borrowType",
    toolTip: "Borrow/Lend/Rehype",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b",
    width: 70
  };

  columns.push(borrowType);

  columns = columns.concat(otherColumns());
  return columns;
}
