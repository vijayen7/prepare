import {
  positionViewColumns,
  bundlePositionViewColumns,
  lotViewColumns
} from "./columnConfig";

export function getSelectedViewDefaultColumns(view) {
  var commonColumns = [
    "description",
    "sedol",
    "ticker",
    "currency",
    "sourceDate",
    "date",
    "legalEntity",
    "counterpartyEntity",
    "positionType"
  ];

  switch (view) {
    case "position":
      var positionColumns = commonColumns.concat([
        "book",
        "mappingTag",
        "history",
        "bundleView",
        "lotView",
        "negotiation",
        "tradeDateQuantity",
        "internalQuantity",
        "quantity",
        "excessBorrowQuantity",
        "quantityDifference",
        "internalPriceRC",
        "priceRC",
        "internalMVRC",
        "mvRC",
        "rebateRate",
        "slfRate",
        "financingFeeRate",
        "rebateFeeRC",
        "slfRC",
        "financingFeeRC",
        "classification",
        "borrowType",
        "spn",
        "pnlSpn",
        "custodianAccount"
      ]);
      return positionColumns;
    case "bundle":
      var bundleColumns = commonColumns.concat([
        "book",
        "businessUnit",
        "bundle",
        "bundlePositionType",
        "internalQuantity",
        "internalPriceRC",
        "internalMVRC",
        "brokerPositionQuantity",
        "brokerPositionPriceRC",
        "brokerPositionMvRC",
        "brokerPositionSlfRate",
        "rateSource",
        "brokerPositionFinancingFeeRate",
        "classification",
        "spn",
        "pnlSpn",
        "custodianAccount"
      ]);
      return bundleColumns;
    case "lot":
      var lotColumns = commonColumns.concat([
        "book",
        "quantity",
        "mvRC",
        "rebateRate",
        "slfRate",
        "financingFeeRate",
        "rebateFeeRC",
        "slfRC",
        "financingFeeRC",
        "negotiation",
        "classification",
        "borrowType",
        "spn",
        "pnlSpn",
        "custodianAccount"
      ]);
      return lotColumns;
    default:
      return commonColumns;
  }
}

export function getSelectedViewTotalColumns(view) {
  switch (view) {
    case "position":
      return positionViewColumns().map(column => [column.id, column.name]);
    case "bundle":
      return bundlePositionViewColumns().map(column => [
        column.id,
        column.name
      ]);
    case "lot":
      return lotViewColumns().map(column => [column.id, column.name]);
  }
}
