import {
  FETCH_BROKER_POSITION_DATA_FOR_ID,
  FETCH_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION,
  DESTROY_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION,
  FETCH_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION,
  DESTROY_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION,
  DESTROY_BROKER_POSITION_DATA_FOR_ID,
  OPEN_NEGOTIATION_POPUP,
  CLOSE_NEGOTIATION_POPUP,
  FETCH_NEGOTIATION_BASE_DATA,
  DESTROY_NEGOTIATION_BASE_DATA,
  SAVE_NEGOTIATION_DATA,
  DESTROY_STATE_FOR_SAVE_NEGOTIATION_DATA_RESULT,
  FETCH_POSITION_VIEW_BASE_DATA,
  FETCH_LONG_POSITION_VIEW_BASE_DATA,
  DESTROY_DATA_STATE_FOR_POSITION_VIEW_BASE_DATA,
  FETCH_LOT_VIEW_BASE_DATA,
  DESTROY_DATA_STATE_FOR_LOT_VIEW_BASE_DATA
} from "./constants";
import { RESIZE_CANVAS } from "commons/constants"
import { combineReducers } from "redux";

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

function positionViewBaseDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_POSITION_VIEW_BASE_DATA}_SUCCESS`:
      action.data.forEach(
        (o, i, arr) => (arr[i].id = arr[i].uniqueId)
      );
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_POSITION_VIEW_BASE_DATA:
      return [];
  }
  return state;
}

function longPositionViewBaseDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LONG_POSITION_VIEW_BASE_DATA}_SUCCESS`:
      action.data.forEach(
        (o, i, arr) => (arr[i].id = arr[i].uniqueId)
      );
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_POSITION_VIEW_BASE_DATA:
      return [];
  }
  return state;
}

function lotViewBaseDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LOT_VIEW_BASE_DATA}_SUCCESS`:
      action.data.forEach(
        (o, i, arr) => (arr[i].id = arr[i].uniqueId)
      );
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_LOT_VIEW_BASE_DATA:
      return [];
  }
  return state;
}

function brokerPositionDataForIdReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_BROKER_POSITION_DATA_FOR_ID}_SUCCESS`:
      return action.data || [];
    case DESTROY_BROKER_POSITION_DATA_FOR_ID:
      return [];
  }
  return state;
}

function saveRateNegotiationDataReducer(state = 0, action) {
  switch (action.type) {
    case `${SAVE_NEGOTIATION_DATA}_SUCCESS`:
      return 1;
    case `${SAVE_NEGOTIATION_DATA}_FAILURE`:
      return -1;
    case DESTROY_STATE_FOR_SAVE_NEGOTIATION_DATA_RESULT:
      return 0;
  }
  return state;
}

function simulatedDataForPositionViewNegotiationReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION}_SUCCESS`:
    var sum = 0;
      action.data.forEach(
        (o, i, arr) => (arr[i].id = arr[i].uniqueId)
      );
      action.data.forEach(
        (o,i,arr) => (sum += arr[i].impactUSD)
      );
      action.data.impactUSDSum = sum;
      return action.data || [];
    case DESTROY_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION:
      return [];
  }
  return state;
}

function simulatedDataForLotViewNegotiationReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION}_SUCCESS`:
    var sum = 0;
      action.data.id = action.data.uniqueId;
      var obj = action.data;
      action.data = [];
      action.data.push(obj);
      return action.data || [];
    case DESTROY_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION:
      return [];
  }
  return state;
}

function negotiationToggleReducer(state = false, action) {
  switch (action.type) {
    case OPEN_NEGOTIATION_POPUP:
      return true;
    case `${CLOSE_NEGOTIATION_POPUP}_SUCCESS`:
      return false;
    default:
      return state;
  }
}

function negotiationLevelToggleReducer(state = true, action) {
  switch (action.type) {
    case `${FETCH_NEGOTIATION_BASE_DATA}_SUCCESS`:
      if(action.data.view === "position" || action.data.view === "longPosition") {
        return true;
      } else if (action.data.view === "lot") {
        return false;
      }
    default:
      return state;
  }
}

function longPositionToggleReducer(state = true, action) {
  switch (action.type) {
    case `${FETCH_NEGOTIATION_BASE_DATA}_SUCCESS`:
      if(action.data.view === "longPosition") {
        return true;
      } else if (action.data.view === "lot" || action.data.view ==="position") {
        return false;
      }
    default:
      return state;
  }
}

function negotiationBaseDataReducer(state = [], action) {
  switch(action.type) {
    case `${FETCH_NEGOTIATION_BASE_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_NEGOTIATION_BASE_DATA:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  brokerPositionDataForId: brokerPositionDataForIdReducer,
  simulatedDataForPositionViewNegotiation: simulatedDataForPositionViewNegotiationReducer,
  simulatedDataForLotViewNegotiation: simulatedDataForLotViewNegotiationReducer,
  negotiationBaseData: negotiationBaseDataReducer,
  positionViewBaseData: positionViewBaseDataReducer,
  longPositionViewBaseData: longPositionViewBaseDataReducer,
  lotViewBaseData: lotViewBaseDataReducer,
  saveResult: saveRateNegotiationDataReducer,
  isOpen: negotiationToggleReducer,
  isPositionView: negotiationLevelToggleReducer,
  isLongPositionView: longPositionToggleReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
