import {
  FETCH_BROKER_POSITION_DATA_FOR_ID,
  FETCH_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION,
  DESTROY_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION,
  FETCH_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION,
  DESTROY_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION,
  DESTROY_BROKER_POSITION_DATA_FOR_ID,
  CLOSE_NEGOTIATION_POPUP,
  FETCH_NEGOTIATION_BASE_DATA,
  DESTROY_NEGOTIATION_BASE_DATA,
  SAVE_NEGOTIATION_DATA,
  DESTROY_STATE_FOR_SAVE_NEGOTIATION_DATA_RESULT,
  FETCH_POSITION_VIEW_BASE_DATA,
  FETCH_LOT_VIEW_BASE_DATA,
  FETCH_LONG_POSITION_VIEW_BASE_DATA
} from "./constants";
import { RESIZE_CANVAS } from "commons/constants";

export function fetchPositionViewBaseData(payload) {
  return {
    type: FETCH_POSITION_VIEW_BASE_DATA,
    payload
  };
}

export function fetchLongPositionViewBaseData(payload) {
  return {
    type: FETCH_LONG_POSITION_VIEW_BASE_DATA,
    payload
  };
}

export function fetchLotViewBaseData(payload) {
  return {
    type: FETCH_LOT_VIEW_BASE_DATA,
    payload
  };
}

export function saveRateNegotiationData(payload) {
  return {
    type: SAVE_NEGOTIATION_DATA,
    payload
  };
}

export function destroySaveNegotiationState(payload) {
  return {
    type: DESTROY_STATE_FOR_SAVE_NEGOTIATION_DATA_RESULT
  };
}

export function fetchBrokerPositionDataForId(payload) {
  return {
    type: FETCH_BROKER_POSITION_DATA_FOR_ID,
    payload
  };
}

export function destroyBrokerPositionDataForId() {
  return {
    type: DESTROY_BROKER_POSITION_DATA_FOR_ID
  };
}

export function fetchSimulatedDataForPositionViewNegotiation(payload) {
  return {
    type: FETCH_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION,
    payload
  };
}

export function destroySimulatedDataForPositionViewNegotiation() {
  return {
    type: DESTROY_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION
  };
}

export function fetchSimulatedDataForLotViewNegotiation(payload) {
  return {
    type: FETCH_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION,
    payload
  };
}

export function destroySimulatedDataForLotViewNegotiation() {
  return {
    type: DESTROY_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

export function closeNegotiationPopUp() {
  return {
    type: CLOSE_NEGOTIATION_POPUP
  };
}

export function loadNegotiationPopUp(payload) {
  return {
    type: FETCH_NEGOTIATION_BASE_DATA,
    data: payload
  };
}

export function destroyNegotiationBaseData() {
  return {
    type: DESTROY_NEGOTIATION_BASE_DATA
  };
}

