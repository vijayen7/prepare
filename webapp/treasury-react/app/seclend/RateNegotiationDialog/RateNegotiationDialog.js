import React, { Component } from "react";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import SpnFilterAutocomplete from "commons/components/SpnFilterAutocomplete";
import InputFilter from "commons/components/InputFilter";
import InputNumberFilter from "commons/components/InputNumberFilter";
import Message from "commons/components/Message";
import DateFilter from "commons/container/DateFilter";
import { getNextBusinessDay } from "commons/util";
import FilterButton from "commons/components/FilterButton";
import Dialog from "commons/components/Dialog";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import MultiSelectFilter from "commons/components/MultiSelectFilter";
import { Layout } from "arc-react-components";
import NegotiationDataGrid from "commons/components/GridWithCellChange";
import LotLevelBaseDataLegend from "./components/LotLevelBaseDataLegend"
import { Panel } from 'arc-react-components';
import { CLASS_NAMESPACE } from "commons/ClassConfigs";
import _ from 'lodash';
import {
  dataGridColumns,
  dataGridColumnsforLongView
} from "./grid/columnConfig";
import {
  negotiationPopUpOptions
} from "./grid/gridOptions";
import { getCurrentDate } from "commons/util";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  convertJavaDate,
  convertJavaDateDashed,
  getCommaSeparatedValues,
  getArrayFromString,
  isAllKeySelected
} from "commons/util";
import {
  fetchBrokerPositionDataForId,
  fetchSimulatedDataForPositionViewNegotiation,
  destroySimulatedDataForPositionViewNegotiation,
  fetchSimulatedDataForLotViewNegotiation,
  destroySimulatedDataForLotViewNegotiation,
  closeNegotiationPopUp,
  destroyNegotiationBaseData,
  saveRateNegotiationData,
  destroySaveNegotiationState,
  fetchPositionViewBaseData,
  fetchLongPositionViewBaseData,
  fetchLotViewBaseData,
  resizeCanvas
} from "./actions";
import { select } from "redux-saga/effects";

class RateNegotiationDialog extends Component {

  constructor(props) {
    super(props);
    this.handleSwitch = this.handleSwitch.bind(this);
    this.handleCloseNegotiation = this.handleCloseNegotiation.bind(this);
    this.handleCloseMessageDialog = this.handleCloseMessageDialog.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onCellChange = this.onCellChange.bind(this);
    this.onBeforeEditCell = this.onBeforeEditCell.bind(this);
    this.handleGet = this.handleGet.bind(this);
    this.getInitialPayload = this.getInitialPayload.bind(this);
    this.state = {
      negotiatedRate: "",
      selectedStartDatePopUp: getNextBusinessDay(),
      selectedEndDatePopUp: getNextBusinessDay(),
      currentDate: getCurrentDate(),
      selectedLegalEntities: [{ key: -1, value: "-1" }],
      comment: "Changes being done by " + USER,
      isSimulate: true,
      impactSum: 0,
      positionViewBaseData: { impactUSDSum: 0, data: [] },
      longPositionViewBaseData: { impactUSDSum: 0, data: [] },
      isCellChange: false,
      isError: false,
      quantityErrorList: [],
      isQuantityError: false,

    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isOpen === false && this.props.isOpen === true) {
      if (prevProps.isPositionView) {
        this.setState({ isSimulate: false });
        this.setState({ selectedLegalEntities: [{ key: this.props.negotiationBaseData.legalEntityId, value: "" + this.props.negotiationBaseData.legalEntityId }] });
        var payload = this.getInitialPayload();
        if (!prevProps.isLongPositionView) {
          prevProps.fetchPositionViewBaseData(payload);
        }
        else {
          prevProps.fetchLongPositionViewBaseData(payload);
        }
      }
      else {
        this.setState({ isSimulate: false });
        payload = {
          "lotPositionRateNegotiation":{
            "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.negotiation.LotPositionRateNegotiation,
            "cpeId": prevProps.negotiationBaseData.cpeId,
            ...(this.state.selectedLegalEntities.length > 0 && !isAllKeySelected(this.state.selectedLegalEntities)
              ? {
                "legalEntityId": prevProps.negotiationBaseData.legalEntityId
              }
              : null),
            ...({
              "pnlSpn": prevProps.negotiationBaseData.pnlSpn
            }),
            ...({
              "borrowType": prevProps.negotiationBaseData.borrowType
            }),
            "isBorrow": (prevProps.negotiationBaseData.positionType === "Cash"),
            "brokerPositionLotLevelDataId": prevProps.negotiationBaseData.brokerPositionLotLevelDataId,
            "bookId": (prevProps.negotiationBaseData.bookId),
            "custodianAccountId": (prevProps.negotiationBaseData.custodianAccountId)
          },
          "date": convertJavaDateDashed(prevProps.negotiationBaseData.date),
        };
        prevProps.fetchLotViewBaseData(payload);
      }
    }
    if (prevProps.isPositionView === false && this.props.isPositionView === true) {
      this.setState({ isSimulate: false });
      this.setState({ selectedLegalEntities: [{ key: this.props.negotiationBaseData.legalEntityId, value: "" + this.props.negotiationBaseData.legalEntityId }] });
      var payload = this.getInitialPayload();
      if (!prevProps.isLongPositionView) {
        prevProps.fetchPositionViewBaseData(payload);
      } else {
        prevProps.fetchLongPositionViewBaseData(payload);
      }
    }
  }

  handleCloseNegotiation() {
    this.setState({
      isSimulate: false,
      negotiatedRate: "",
      selectedStartDatePopUp: getNextBusinessDay(),
      selectedEndDatePopUp: getNextBusinessDay(),
      isCellChange: false
    });
    {
      this.props.closeNegotiationPopUp();
    }
  }

  handleCloseMessageDialog() {
    { this.props.destroySaveNegotiationState(); this.handleCloseNegotiation(); }
  }

  handleSwitch() {
    var payload = {
      id: this.props.negotiationBaseData.brokerPositionDataId,
      isBorrow: this.props.negotiationBaseData.positionType === "Cash" ? true : false
    };
    this.props.fetchBrokerPositionDataForId(payload);
  }

  handleSave() {
    var selectedData = this.state.selectedRowData;
    if (selectedData.length > 0) {
      selectedData.forEach(
        (o, i, arr) => (arr[i].verificationStartDate = this.state.selectedStartDatePopUp, arr[i].verificationEndDate = this.state.selectedEndDatePopUp, arr[i].knowledgeStartDate = getCurrentDate(), arr[i].username = USER, arr[i].comment = this.state.comment)
      );
      this.props.saveRateNegotiationData(selectedData);
    }
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getInitialPayload() {
    var payload = {
      positionRateNegotiation: {
        "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.negotiation.PositionRateNegotiation,
        "cpeId": this.props.negotiationBaseData.cpeId,
        ...({
          "legalEntityIds": [this.props.negotiationBaseData.legalEntityId]
        }),
        ...({
          "pnlSpn": this.props.negotiationBaseData.pnlSpn
        }),
        ...((this.props.negotiationBaseData.borrowType != undefined && typeof this.props.negotiationBaseData.borrowType === 'string') ? {
          "borrowType": this.props.negotiationBaseData.borrowType
        } : {"borrowType": this.props.negotiationBaseData.borrowType[0]}),
        "isBorrow": (this.props.negotiationBaseData.positionType === "Cash")
      },
      "date": convertJavaDateDashed(this.props.negotiationBaseData.date)
    };
    return payload;
  }

  handleGet() {
    this.setState({ isSimulate: false, isCellChange: false, isQuantityError: false, isError: false });
    var payload = {
      positionRateNegotiation: {
        "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.negotiation.PositionRateNegotiation,
        ".cpeId": this.props.negotiationBaseData.cpeId,
        ...(this.state.selectedLegalEntities.length > 0 && !isAllKeySelected(this.state.selectedLegalEntities)
          ? {
            "legalEntityIds": getArrayFromString(getCommaSeparatedValues(
              this.state.selectedLegalEntities)
            )
          }
          : null),
        ...({
          "pnlSpn": this.props.negotiationBaseData.pnlSpn
        }),
        ...((this.props.negotiationBaseData.borrowType != undefined && typeof this.props.negotiationBaseData.borrowType === 'string') ? {
          "borrowType": this.props.negotiationBaseData.borrowType
        } : {"borrowType": this.props.negotiationBaseData.borrowType[0]}),
        "isBorrow": (this.props.negotiationBaseData.positionType === "Cash")
      },
      "date": convertJavaDateDashed(this.props.negotiationBaseData.date),
    };
    if (!this.props.isLongPositionView) {
      this.props.fetchPositionViewBaseData(payload);
    } else {
      this.props.fetchLongPositionViewBaseData(payload);
    }
  }

  loadNegotiationData() {
    var title = this.props.isPositionView ? "Affected Positions" : "Affected Lots";
    var selectedRowId;
    if (typeof this.props.lotViewBaseData !== "undefined") {
      if (this.props.lotViewBaseData.length === 1) {
        selectedRowId = this.props.lotViewBaseData[0].id;
      } else {
        var brokerPositionLotLevelDataId = this.props.negotiationBaseData.brokerPositionLotLevelDataId;
        this.props.lotViewBaseData.forEach(function (element) {
          if (element.brokerPositionLotLevelDataId === brokerPositionLotLevelDataId) { selectedRowId = element.id; }
        });
      }
      console.log(this.props.simulatedDataForLotViewNegotiation);
    }

    return (
      <Panel title={title} collapsible={true}>
        <hr />
        <Layout>
          <Layout.Child size={1}>
            <div className="padding">
              <b>Expected Impact as of {this.state.currentDate.toString() + " (USD) : "}</b>
              {this.state.isCellChange ? (this.props.isPositionView ? (this.props.isLongPositionView ? this.state.longPositionViewBaseData.impactUSDSum.toLocaleString() :
                this.state.positionViewBaseData.impactUSDSum.toLocaleString()) : this.getImpactForLots()) : "0.0"}<br /><br />
              {this.ErrorNote(this.state.isQuantityError, "Negotiated quantity cannot be greater than available Quantity")}

              <NegotiationDataGrid
                data={this.getBaseData()}
                gridId="NegotiationData"
                gridColumns={this.props.isLongPositionView ? dataGridColumnsforLongView(selectedRowId) : dataGridColumns(selectedRowId)}
                gridOptions={negotiationPopUpOptions()}
                heightValue={200}
                resizeCanvas={this.props.resizeCanvas}
                onSelect={this.onSelect}
                onCellChange={this.onCellChange}
                onBeforeEditCell={this.onBeforeEditCell}
              /><br />
            </div>
          </Layout.Child>
          <Layout.Child>
            {this.loadLegend()}
          </Layout.Child>
          <Layout.Child>
            {this.loadGridLegend()}
          </Layout.Child>
        </Layout>
        <Layout isColumnType={true}>
          <Layout.Child size={1}>
            <InputFilter
              data={this.state.comment}
              onSelect={this.onSelect}
              stateKey="comment"
              label="Comment (*) : "
            />
            <p>(*) indicates mandatory fields</p>
          </Layout.Child>
          <Layout.Child size={0.75}>
            <FilterButton
              onClick={this.handleSave}
              reset={false}
              label="Save"
              disabled={(this.state.comment !== "undefined" && this.state.comment.trim() !== "" && (!this.state.isError)) ? false : true}
            /><br />
          </Layout.Child>
        </Layout>
      </Panel>
    );
  }

  getImpactForLots() {
    if (this.props.simulatedDataForLotViewNegotiation.length !== 0) {
      return this.props.simulatedDataForLotViewNegotiation[0].impactUSD.toLocaleString();
    }
    return "0.0";
  }

  getBaseData() {
    if (!this.state.isCellChange) {
      return (this.props.isPositionView ? (this.props.isLongPositionView ? this.props.longPositionViewBaseData : this.props.positionViewBaseData) : this.props.lotViewBaseData);
    } else {
      return (this.props.isPositionView ? (this.props.isLongPositionView ? this.state.longPositionViewBaseData : this.state.positionViewBaseData) : this.props.simulatedDataForLotViewNegotiation);
    }
  }

  loadMessageDialogConditional() {
    var status = false;
    if (this.props.saveResult === 1) { status = true; }
    if (this.props.saveResult === -1) { status = false; }
    var message = status ? "Rate Negotiations Saved Successfully" : "Unable to save Rate Negotiations. Please try again";
    if (this.props.saveResult !== 0) {
      return (
        <Dialog
          isOpen={this.props.saveResult !== 0}
          title={status ? "SUCCESS" : "FAILURE"}
          onClose={this.handleCloseMessageDialog}
        >
          <Message error={!status} success={status} messageData={message} />
          <FilterButton
            onClick={this.handleCloseMessageDialog}
            reset={false}
            label="OK"
          />
        </Dialog>
      );
    }
  }

  onBeforeEditCell = (e, args) => {
    if (args.item.rateNegotiation.borrowType == "LEND" && args.column.id == "negotiatedQuantity") {
      return false;
    }
    return true;
  }

  onCellChange = (e) => {
    console.log(e);
    if (this.props.isPositionView) {
      this.setState({ negotiatedRate: e.item.negotiatedWslfRate });
      var rate = (typeof this.state.negotiatedRate !== "undefined") ? this.state.negotiatedRate : 0;
      var sum = 0;
      if (!this.state.isCellChange) {
        this.setState({ positionViewBaseData: this.props.positionViewBaseData, longPositionViewBaseData: this.props.longPositionViewBaseData });
      }

      var positionViewBaseDataCopy = _.cloneDeep(this.state.positionViewBaseData);
      if (this.props.isLongPositionView) {
        positionViewBaseDataCopy = _.cloneDeep(this.state.longPositionViewBaseData);
        var slfRate = (typeof this.state.longPositionViewBaseData[e.row].slfRate == "undefined") ? 0 : this.state.longPositionViewBaseData[e.row].slfRate;
      } else {
        slfRate = this.state.positionViewBaseData[e.row].slfRate
      }
      console.log(positionViewBaseDataCopy.length);
      console.log(positionViewBaseDataCopy[0]);
      var tempArray = [...this.state.quantityErrorList];
      if (positionViewBaseDataCopy[e.row].rateNegotiation.borrowType == "LONG" &&positionViewBaseDataCopy[e.row].negotiatedQuantity > positionViewBaseDataCopy[e.row].quantity) {
        if (tempArray.indexOf(e.item.id) == -1) {
          this.setState({ quantityErrorList: [...this.state.quantityErrorList, e.item.id], isError: true, isQuantityError: true })
        } else {
          this.setState({ isError: true, isQuantityError: true })
        }
      }
      else {
        if (positionViewBaseDataCopy[e.row].rateNegotiation.borrowType === "LONG") {
          var tempNegotiatedQuantity = (positionViewBaseDataCopy[e.row].negotiatedQuantity > 0) ? positionViewBaseDataCopy[e.row].negotiatedQuantity : 0.0
          positionViewBaseDataCopy[e.row].marketValueUSD = positionViewBaseDataCopy[e.row].priceUSD * tempNegotiatedQuantity;
          positionViewBaseDataCopy[e.row].negotiatedQuantity = tempNegotiatedQuantity;
        }
        var numerator = ((slfRate - rate) * positionViewBaseDataCopy[e.row].marketValueUSD);
        if (numerator === 0) {
          positionViewBaseDataCopy[e.row].impactUSD = 0;
        } else {
          positionViewBaseDataCopy[e.row].impactUSD = ((slfRate - rate) * positionViewBaseDataCopy[e.row].marketValueUSD) / 36000
        }
        positionViewBaseDataCopy[e.row].negotiatedWslfRate = this.state.negotiatedRate, positionViewBaseDataCopy[e.row].verificationStartDate = this.state.selectedStartDatePopUp,
        positionViewBaseDataCopy[e.row].verificationEndDate = this.state.selectedEndDatePopUp, positionViewBaseDataCopy[e.row].username = USER,
        positionViewBaseDataCopy[e.row].comment = this.state.comment
        if(positionViewBaseDataCopy[e.row].rateNegotiation.borrowType  === "LONG" || positionViewBaseDataCopy[e.row].rateNegotiation.borrowType  === "LEND") {
            positionViewBaseDataCopy[e.row].impactUSD = positionViewBaseDataCopy[e.row].impactUSD * -1;
        }
        positionViewBaseDataCopy.forEach((o, i, arr) => sum += arr[i].impactUSD);
        positionViewBaseDataCopy.impactUSDSum = sum;

        if (this.props.isLongPositionView) {
          this.setState({ longPositionViewBaseData: positionViewBaseDataCopy });
        }
        this.setState({ positionViewBaseData: positionViewBaseDataCopy, isCellChange: true });
        if (tempArray.length !== 0 && (tempArray.indexOf(e.item.id) !== -1)) {
          tempArray.splice(tempArray.indexOf(e.item.id), 1);
          this.setState({ quantityErrorList: tempArray });
          this.setState({ isQuantityError: ((tempArray.length == 0) ? false : true) });
          this.setState({ isError: (this.state.isQuantityError) });
        }
      }
    } else {
      this.setState({ isSimulate: true, negotiatedRate: e.item.negotiatedWslfRate });
      this.props.lotViewBaseData.forEach(
        (o, i, arr) => (arr[i].verificationStartDate = this.state.selectedStartDatePopUp, arr[i].verificationEndDate = this.state.selectedEndDatePopUp, arr[i].username = USER, arr[i].comment = this.state.comment)
      );
      var selectedLotLevelId = this.props.negotiationBaseData.brokerPositionLotLevelDataId;
      var selectedRate = this.state.negotiatedRate;
      if (this.props.lotViewBaseData.length === 1) { this.props.lotViewBaseData[0].negotiatedWslfRate = this.state.negotiatedRate; }
      else {
        this.props.lotViewBaseData.forEach(function (element) {
          if (element.brokerPositionLotLevelDataId === selectedLotLevelId) { element.negotiatedWslfRate = selectedRate; }
        });
      }
      this.props.fetchSimulatedDataForLotViewNegotiation(this.props.lotViewBaseData);
      this.setState({ isCellChange: true });
    }
  }

  loadNegotiationBaseData() {

    if ((this.props.isPositionView && (typeof this.props.positionViewBaseData !== "undefined" && this.props.positionViewBaseData.length > 0) ||
      (this.props.isLongPositionView && typeof this.props.longPositionViewBaseData !== "undefinend" && this.props.longPositionViewBaseData.length > 0)) ||
      (!this.props.isPositionView && typeof this.props.lotViewBaseData !== "undefined" && this.props.lotViewBaseData.length > 0)) {
      return (this.loadNegotiationData());
    }
    else {
      var dataUnit = this.props.isPositionView ? "positions" : "lots";
      var dataType = this.props.negotiationBaseData.borrowType;
      var data = "No " + dataType + " " + dataUnit + " available for given search criteria.";

      if (this.props.isLongPositionView) {
        data = "Sufficient LONG quantity not available for negotiation"
      }
      return (
        <Layout>
          <Layout.Child><br />
            <Message messageData={data} />
          </Layout.Child>
        </Layout>
      );
    }
  }

  ErrorNote(error, message) {
    if (error)
      return <span className="red margin--small">{message}</span>;
    else
      return;
  }

  loadGridLegend() {
    if (!this.props.isPositionView) {
      return (
        <LotLevelBaseDataLegend />
      );
    }
  }

  loadLegend() {
    if (this.props.isPositionView && this.props.negotiationBaseData.borrowType !== "BORROW") {
      return (
        <Layout>
        <Layout.Child>
        <div className="legend__item">
          <span className="margin--left--small margin--right--double">
            Available quantity for
            <span style={{ color: "#8FBC8F"}}>
              {" "}
              lend position
              {" "}
            </span>
            is zero
          </span>
        </div>
      </Layout.Child>
      </Layout>
      );
    }
  }

  render() {
    if (typeof this.props.negotiationBaseData === "undefined") return null;
    var doesBaseDataExist = false;
    if (this.props.isPositionView && (typeof this.props.positionViewBaseData !== "undefined" && this.props.positionViewBaseData.length > 0) ||
      (this.props.isLongPositionView && typeof this.props.longPositionViewBaseData !== "undefined" && this.props.longPositionViewBaseData.length > 0)) { doesBaseDataExist = true; }
    else if (!this.props.isPositionView && typeof this.props.lotViewBaseData !== "undefined" && this.props.lotViewBaseData.length > 0) { doesBaseDataExist = true; }
    return (
      <React.Fragment>
        <Dialog
          isOpen={this.props.isOpen}
          title={this.props.negotiationBaseData.borrowType + " NEGOTIATION (" + (this.props.isPositionView ? "POSITION" : "LOT") + " LEVEL)"}
          onClose={this.handleCloseNegotiation}
          style={{ width: "800px" }}
        >
          <Layout isColumnType={true}>
            <Layout.Child size={1}>
              <div className="padding">
                <DateFilter
                  onSelect={this.onSelect}
                  stateKey="date"
                  data={convertJavaDateDashed(this.props.negotiationBaseData.date)}
                  label="Date"
                  disable={true}
                  layout="standard"
                />
              </div>
            </Layout.Child>
            <Layout.Child size={1}>
              <div className="padding">
                <SpnFilterAutocomplete
                  data={this.props.negotiationBaseData.pnlSpn}
                  label="SPN"
                  readonly={true}
                  onSelect={this.onSelect}
                  layout="standard"
                />
              </div>
            </Layout.Child>
            <Layout.Child size={1}>
              <CpeFilter
                onSelect={this.onSelect}
                readonly={true}
                selectedData={[{ key: this.props.negotiationBaseData.cpeId, value: "" + this.props.negotiationBaseData.cpeId }]}
                stateKey="selectedCounterpartyEntities"
                label="Counterparty Entity"
              />
            </Layout.Child>
            <Layout.Child size={1}>
              <LegalEntityFilter
                onSelect={this.onSelect}
                readonly={this.props.isPositionView ? false : true}
                selectedData={this.props.isPositionView ? this.state.selectedLegalEntities : [{ key: this.props.negotiationBaseData.legalEntityId, value: "" + this.props.negotiationBaseData.legalEntityId }]}
                stateKey="selectedLegalEntities"
              />
            </Layout.Child>
          </Layout>
          <Layout>
            <Layout.Child>
              <div className="padding">
                <b>Security Name : </b>
                {this.props.negotiationBaseData.securityDescription}{" | "}
                <b> Ticker : </b>
                {this.props.negotiationBaseData.securityLocalName}{" | "}
                <b> SEDOL : </b>
                {this.props.negotiationBaseData.sedol}
              </div>
            </Layout.Child>
          </Layout>
          <Layout isColumnType={true}>
            <Layout.Child size={3}>
            </Layout.Child>
            <Layout.Child size={1.25}>
              <FilterButton
                onClick={this.handleSwitch}
                reset={false}
                label="Switch to Position View"
                disabled={this.props.isPositionView ? true : false}
              />
            </Layout.Child>
            <Layout.Child size={1.1}>
              <FilterButton
                onClick={this.handleGet}
                reset={false}
                label={this.props.isPositionView ? "Get Affected Positions" : "Get Affected Lots"}
                disabled={typeof this.state.selectedLegalEntities !== "undefined" && this.state.selectedLegalEntities.length > 0 && this.props.isPositionView ? false : true}
              />
            </Layout.Child>
          </Layout><br />
          {this.loadNegotiationBaseData()}
          <hr />
          <Layout size={0.5} isColumnType={true}>
            <Layout.Child size={1}>
              <div className="padding">
                <DateFilter
                  onSelect={this.onSelect}
                  stateKey="selectedStartDatePopUp"
                  data={this.state.selectedStartDatePopUp}
                  label="Effective Start Date : "
                />
                <DateFilter
                  onSelect={this.onSelect}
                  stateKey="selectedEndDatePopUp"
                  data={this.state.selectedEndDatePopUp}
                  label="Effective End Date : "
                /><br />
              </div>
              <br /><br />
            </Layout.Child>
            <Layout.Child size={0.75}>
            </Layout.Child>
          </Layout>
        </Dialog>
        {this.loadMessageDialogConditional()}
      </React.Fragment>
    );
  }

}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchBrokerPositionDataForId,
      fetchSimulatedDataForPositionViewNegotiation,
      destroySimulatedDataForPositionViewNegotiation,
      fetchSimulatedDataForLotViewNegotiation,
      destroySimulatedDataForLotViewNegotiation,
      closeNegotiationPopUp,
      destroyNegotiationBaseData,
      saveRateNegotiationData,
      destroySaveNegotiationState,
      fetchPositionViewBaseData,
      fetchLongPositionViewBaseData,
      fetchLotViewBaseData,
      resizeCanvas
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    simulatedDataForPositionViewNegotiation: state.rateNegotiation.simulatedDataForPositionViewNegotiation,
    simulatedDataForLotViewNegotiation: state.rateNegotiation.simulatedDataForLotViewNegotiation,
    brokerPositionDataForId: state.rateNegotiation.brokerPositionDataForId,
    negotiationBaseData: state.rateNegotiation.negotiationBaseData,
    positionViewBaseData: state.rateNegotiation.positionViewBaseData,
    longPositionViewBaseData: state.rateNegotiation.longPositionViewBaseData,
    lotViewBaseData: state.rateNegotiation.lotViewBaseData,
    saveResult: state.rateNegotiation.saveResult,
    isOpen: state.rateNegotiation.isOpen,
    isPositionView: state.rateNegotiation.isPositionView,
    isLongPositionView: state.rateNegotiation.isLongPositionView
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RateNegotiationDialog);
