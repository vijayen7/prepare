import {
  BASE_URL, INPUT_FORMAT_JSON
} from "commons/constants";
import { fetchPostURL } from "commons/util"
const queryString = require("query-string");
export let url = "";

export function getBrokerPositionDataForId(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/marketDataService/getBrokerPositionDataForId?id=${payload.id}&isBorrow=${payload.isBorrow}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getSimulatedDataForPositionViewNegotiation(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/rateNegotiationSimulationService/simulate?inputFormat=PROPERTIES&positionRateNegotiation.cpeId=${payload.cpeId}&positionRateNegotiation.legalEntityIds=${payload.legalEntityIds}&positionRateNegotiation.pnlSpn=${payload.pnlSpn}&positionRateNegotiation.negotiatedRate=${payload.negotiatedRate}&positionRateNegotiation.borrowType=${payload.borrowType}&positionRateNegotiation.borrow=${payload.borrow}&date=${payload.startDate}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getSimulatedDataForLotViewNegotiation(payload) {
  var encodedParam = "lotRateNegotiationDataList=" + encodeURIComponent(JSON.stringify(payload)) + "&inputFormat=JSON_WITH_REF&format=JSON"
  console.log(encodedParam);
  url = `${BASE_URL}service/rateNegotiationSimulationService/simulate`;
  return fetchPostURL(url,encodedParam);
}

export function saveNegotiationData(payload) {
  var encodedParam = "rateNegotiationData=" + encodeURIComponent(JSON.stringify(payload)) + "&inputFormat=JSON_WITH_REF&format=JSON"
  url = `${BASE_URL}service/rateNegotiationDataService/save`;
  return fetchPostURL(url,encodedParam);
}

export function getPositionViewBaseData(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/rateNegotiationSimulationService/getRateNegotiationDataWithoutImpact?positionRateNegotiation=`+encodeURIComponent(JSON.stringify(payload.positionRateNegotiation))+`&date="${payload.date}"${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getLongPositionViewBaseData(payload) {
  url = `${BASE_URL}service/rateNegotiationSimulationService/getRateNegotiationDataWithoutImpactForLongPositions?positionRateNegotiation=`+encodeURIComponent(JSON.stringify(payload.positionRateNegotiation))+`&date="${payload.date}"${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getLotViewBaseData(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/rateNegotiationSimulationService/getLotRateNegotiationDataWithoutImpact?lotPositionRateNegotiation=`+encodeURIComponent(JSON.stringify(payload.lotPositionRateNegotiation))+`&date="${payload.date}"${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}


