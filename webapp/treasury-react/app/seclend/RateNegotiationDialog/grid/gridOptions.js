export function negotiationPopUpOptions() {
  var options = {
    width: {
      autoHorizontalScrollBar: true
    },
    applyFilteringOnGrid: true,
    useAvailableScreenSpace: true,
    showHeaderRow: true,
    exportToExcel: true,
    editable: {
      changeCellStyleOnEdit: true,
      editedCellStyle: "edited"
    },
    displayNumRows: 5,
    checkboxHeader: {
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    },
    sortList: [
      {columnId: "book", sortAsc: true},
      {columnId: "custodianAccount", sortAsc: true}
    ],
    page: true
  };
  return options;
}

export function dataGridOptions() {
  var options = {
        width: {
          autoHorizontalScrollBar: true,
        },
    applyFilteringOnGrid: true,
    editable: true,
    autoEdit: false,
    showHeaderRow: true,
    useAvailableScreenSpace: true,
    displayNumRows: 5,
    sortList: [
      {
        columnId: "description",
        sortAsc: true
      }
    ]
  };
  return options;
}
