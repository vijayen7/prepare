import {
  numberFormat
} from "commons/grid/formatters";
export function negotiationPopUpColumns(selectedRowId, isPositionView) {
  var negotiationPopUpColumns = commonNegotiationColumns(selectedRowId)
  negotiationPopUpColumns = negotiationPopUpColumns.concat(quantityColumn(selectedRowId));
  negotiationPopUpColumns = negotiationPopUpColumns.concat(commonNegotiationColumnsAfterQuantity(selectedRowId));
  return negotiationPopUpColumns;
}
export function commonNegotiationColumns(selectedRowId) {
  var negotiationPopUpColumns = [{
    id: "legalEntity",
    name: "Legal Entity",
    field: "legalEntityAbbrev",
    toolTip: "Legal Entity",
    type: "text",
    filter: true,
    sortable: true,
    formatter: function(row, cell, value, columnDef, dataContext) {
      return customFormatter(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        selectedRowId
      );
    },
    headerCssClass: "b",
    width: 140
  },
  {
    id: "book",
    name: "Book",
    field: "bookName",
    toolTip: "Book",
    type: "text",
    filter: true,
    sortable: true,
    formatter: function(row, cell, value, columnDef, dataContext) {
      return customFormatter(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        selectedRowId
      );
    },
    headerCssClass: "b",
    width: 110
  },
  {
    id: "custodianAccount",
    name: "Custodian Account",
    field: "custodianAccountName",
    toolTip: "Custodian Account",
    type: "text",
    filter: true,
    sortable: true,
    formatter: function(row, cell, value, columnDef, dataContext) {
      return customFormatter(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        selectedRowId
      );
    },
    headerCssClass: "b",
    width: 175
  }
  ];
  return negotiationPopUpColumns;

}
export function quantityColumn(selectedRowId) {
var quantityColumn = [ {
  id: "quantity",
  name: "Quantity",
  field: "negotiatedQuantity",
  toolTip: "Quantity",
  type: "number",
  filter: true,
  sortable: true,
  formatter: dpGrid.Formatters.Number,
  headerCssClass: "aln-rt b",
  excelFormatter: "#,##0.0",
  width: 120

 }];
 return quantityColumn;

}

export function commonNegotiationColumnsAfterQuantity(selectedRowId) {

  var columns = [   {
    id: "marketValueUSD",
    name: "MarketValueUSD",
    field: "marketValueUSD",
    toolTip: "MarketValueUSD",
    type: "number",
    filter: true,
    sortable: true,
    formatter: function(row, cell, value, columnDef, dataContext) {
      return customFormatter(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        selectedRowId,
        "float"
      );
    },
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.0",
    width: 125
  },
  {
    id: "stockLoanFeeRate",
    name: "SLF Rate(%)",
    field: "slfRate",
    toolTip: "SLF Rate",
    type: "number",
    filter: true,
    sortable: true,
    formatter: function(row, cell, value, columnDef, dataContext) {
      return customFormatter(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        selectedRowId,
        "float"
      );
    },
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.0",
    width: 105
  },
  {
    id: "negotiatedWeightedStockLoanFeeRate",
    name: "Negotiated WSLF Rate(%)",
    field: "negotiatedWslfRate",
    toolTip: "Negotiated WSLF Rate(%)",
    type: "textbox",
    isEditable: true,
    editor: dpGrid.Editors.Text,
    filter: true,
    sortable: true,
    formatter: function(row, cell, value, columnDef, dataContext) {
      return customFormatter(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        selectedRowId,
        "float"
      );
    },
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.0",
    width: 105
  },
  {
        id: "impact",
        name: "Impact (USD)",
        field: "impactUSD",
        toolTip: "Impact",
        type: "number",
        filter: true,
        sortable: true,
        formatter: function(row, cell, value, columnDef, dataContext) {
          return customFormatter(
            row,
            cell,
            value,
            columnDef,
            dataContext,
            selectedRowId,
            "float"
          );
        },
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0.0",
        width: 150
      }];


      return columns;
}

export function longNegotiationPopUpColumns(selectedRowId, isPositionView){

  var columns = commonNegotiationColumns(selectedRowId)

  columns.push(
    {
      id: "availableQuantity",
      name: "Available Quantity",
      field: "quantity",
      toolTip: "AvailableQuantity",
        type: "number",
        filter: true,
        sortable: true,
        formatter: dpGrid.Formatters.Number,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0.0",
        width: 120
      },
    {
      id: "negotiatedQuantity",
      name: "Negotiated Quantity",
      field: "negotiatedQuantity",
      toolTip: "Negotiated Quantity",
      type: "number",
      filter: true,
      sortable: true,
      type: "textbox",
      isEditable: true,
      editor: dpGrid.Editors.Text,
      editorOptions: {
        showEditIcon: true
      },
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 120
    },
  );
  columns = columns.concat(commonNegotiationColumnsAfterQuantity(selectedRowId));
  return columns;
}

export function dataGridColumns(selectedRowId) {
  var columns = negotiationPopUpColumns(selectedRowId);
  return columns;
}

export function dataGridColumnsforLongView(selectedRowId) {
  var columns = longNegotiationPopUpColumns(selectedRowId);
  return columns;
}

function customFormatter(row, cell, value, columnDef, dataContext, selectedRowId, dataTypeFormatter) {
  function formattedValue() {
    if (dataTypeFormatter !== "undefined") {
      if (dataTypeFormatter === "float")
        return numberFormat(value,4);
      else if (dataTypeFormatter === "number")
        return dpGrid.Formatters.Number(row, cell, value, columnDef, dataContext);
      else return value;
    }
    return value;
  }
  if (typeof selectedRowId !== "undefined" && dataContext.id === selectedRowId) {
    if (value === null || value === undefined)
      return "<div style=\"font-style:italic;color:#20B2AA\">n/a</div>";
    else {
      if((dataContext.rateNegotiation.borrowType === "LONG") && (dataContext.negotiatedQuantity > dataContext.quantity) ){
        return "<div  class=\"highlight-row--critical\" style=\"font-style:italic;color:#20B2AA;background-color:#8E4C4C\"><span>" + formattedValue() + "</span></div>";
      }
      return "<div   class= \"highlight-row--clear\" style=\"font-style:italic;color:#20B2AA\"><span>" + formattedValue() + "</span></div>";
  }
  } else {
    if (value === null || value === undefined)
      return "<div><span>n/a</span></div>";
    else {
        if(dataContext.negotiatedQuantity > dataContext.totalQuantity){
        return "<div  class=\"highlight-row--critical\" style=\"font-style:italic;color:#20B2AA;background-color:#8E4C4C\"><span>" + formattedValue() + "</span></div>";
      }
      if(dataContext.rateNegotiation.borrowType === "LEND") {
        return "<div   class= \"highlight-row--clear\" style=\"color:#8FBC8F\"><span>" + formattedValue() + "</span></div>";
      }

    }
      return "<div><span>" + formattedValue() + "</span></div>";
    }


}
