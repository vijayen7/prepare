import React from "react";
import { Layout } from "arc-react-components";

export default class LotLevelBaseDataLegend extends React.Component {
  render() {
    return (
          <Layout>
          <Layout.Child>
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              The row shown in
              <span style={{ color: "#20B2AA", fontStyle: "italic" }}>
                {" "}
                green italics{" "}
              </span>
              indicates the lot corresponding to the selected row in the market data grid
            </span>
          </div>
          </Layout.Child>
          </Layout>
    );
  }
}
