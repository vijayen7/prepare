import { put, takeEvery, call, all } from 'redux-saga/effects';
import {
  FETCH_BROKER_POSITION_DATA_FOR_ID,
  FETCH_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION,
  FETCH_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION,
  FETCH_NEGOTIATION_BASE_DATA,
  SAVE_NEGOTIATION_DATA,
  OPEN_NEGOTIATION_POPUP,
  CLOSE_NEGOTIATION_POPUP,
  DESTROY_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION,
  DESTROY_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION,
  DESTROY_NEGOTIATION_BASE_DATA,
  FETCH_POSITION_VIEW_BASE_DATA,
  FETCH_LOT_VIEW_BASE_DATA,
  FETCH_LONG_POSITION_VIEW_BASE_DATA
} from './constants';

import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from 'commons/constants';

import {
  getBrokerPositionDataForId,
  getSimulatedDataForPositionViewNegotiation,
  getSimulatedDataForLotViewNegotiation,
  saveNegotiationData,
  getPositionViewBaseData,
  getLongPositionViewBaseData,
  getLotViewBaseData
} from './api';

function* fetchBrokerPositionDataForId(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getBrokerPositionDataForId, action.payload);
    yield put({ type: `${FETCH_BROKER_POSITION_DATA_FOR_ID}_SUCCESS`, data });
    data.view = 'position';
    yield put({ type: FETCH_NEGOTIATION_BASE_DATA, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_BROKER_POSITION_DATA_FOR_ID}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSimulatedDataForPositionViewNegotiation(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getSimulatedDataForPositionViewNegotiation, action.payload);
    yield put({ type: `${FETCH_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSimulatedDataForLotViewNegotiation(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getSimulatedDataForLotViewNegotiation, action.payload);
    yield put({ type: `${FETCH_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveNegotiationDataAfterSimulation(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(saveNegotiationData, action.payload);
    if (data === true) { yield put({ type: `${SAVE_NEGOTIATION_DATA}_SUCCESS`, data }); } else { yield put({ type: `${SAVE_NEGOTIATION_DATA}_FAILURE`, data }); }
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_NEGOTIATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchNegotiationBaseDataAndOpenPopUp(action) {
  yield put({ type: START_LOADING });
  const data = action.data;
  yield put({ type: `${FETCH_NEGOTIATION_BASE_DATA}_SUCCESS`, data });
  yield put({ type: OPEN_NEGOTIATION_POPUP });
  yield put({ type: END_LOADING });
}

function* destroyNegotiationPopup() {
  yield put({ type: DESTROY_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION });
  yield put({ type: DESTROY_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION });
  yield put({ type: DESTROY_NEGOTIATION_BASE_DATA });
  yield put({ type: `${CLOSE_NEGOTIATION_POPUP}_SUCCESS` });
}

function* fetchPositionViewBaseData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getPositionViewBaseData, action.payload);
    yield put({ type: `${FETCH_POSITION_VIEW_BASE_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_POSITION_VIEW_BASE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchLongPositionViewBaseData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLongPositionViewBaseData, action.payload);
    yield put({ type: `${FETCH_LONG_POSITION_VIEW_BASE_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LONG_POSITION_VIEW_BASE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchLotViewBaseData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLotViewBaseData, action.payload);
    yield put({ type: `${FETCH_LOT_VIEW_BASE_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LOT_VIEW_BASE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* lotViewBaseData() {
  yield [
    takeEvery(FETCH_LOT_VIEW_BASE_DATA, fetchLotViewBaseData)
  ];
}

export function* positionViewBaseData() {
  yield [
    takeEvery(FETCH_POSITION_VIEW_BASE_DATA, fetchPositionViewBaseData)
  ];
}


export function* longPositionViewBaseData() {
  yield [
    takeEvery(FETCH_LONG_POSITION_VIEW_BASE_DATA, fetchLongPositionViewBaseData)
  ];
}

export function* closeNegotiationPopUp() {
  yield [
    takeEvery(CLOSE_NEGOTIATION_POPUP, destroyNegotiationPopup)
  ];
}

export function* negotiatedBaseDataAndPopUpToggle() {
  yield [
    takeEvery(FETCH_NEGOTIATION_BASE_DATA, fetchNegotiationBaseDataAndOpenPopUp)
  ];
}


export function* saveRateNegotiationData() {
  yield [
    takeEvery(SAVE_NEGOTIATION_DATA, saveNegotiationDataAfterSimulation)
  ];
}

export function* simulatedDataForPositionViewNegotiation() {
  yield [
    takeEvery(FETCH_SIMULATED_DATA_FOR_POSITION_VIEW_NEGOTIATION, fetchSimulatedDataForPositionViewNegotiation)
  ];
}

export function* simulatedDataForLotViewNegotiation() {
  yield [
    takeEvery(FETCH_SIMULATED_DATA_FOR_LOT_VIEW_NEGOTIATION, fetchSimulatedDataForLotViewNegotiation)
  ];
}

export function* brokerPositionDataForId() {
  yield [
    takeEvery(FETCH_BROKER_POSITION_DATA_FOR_ID, fetchBrokerPositionDataForId)
  ];
}

function* marketDataSaga() {
  yield all([brokerPositionDataForId(), simulatedDataForPositionViewNegotiation(), simulatedDataForLotViewNegotiation(), saveRateNegotiationData(), negotiatedBaseDataAndPopUpToggle(), closeNegotiationPopUp(), positionViewBaseData(), lotViewBaseData(), longPositionViewBaseData()]);
}

export default marketDataSaga;
