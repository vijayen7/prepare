import React from "react";
import SpnFilterAutocomplete from "../../commons/components/SpnFilterAutocomplete";
import { CustomPnlSpnFilterProps } from "../models/Props";

const CustomPnlSpnFilter: React.FC<CustomPnlSpnFilterProps> = (
  props: CustomPnlSpnFilterProps
) => {
  const onSelect = ({ key, value }: any) => {
    let returnVal = {
      key: props.stateKey,
      value: value,
    };
    props.onSelect(returnVal);
  };

  return (
    <>
      <SpnFilterAutocomplete
        data={props.data}
        label={props.label}
        onSelect={onSelect}
      />
    </>
  );
};

export default CustomPnlSpnFilter;
