import React from "react";
import { CustomInputFilterProps } from "../models/Props";

const CustomInputFilter: React.FC<CustomInputFilterProps> = (
  props: CustomInputFilterProps
) => {
  const onChange = (e: any) => {
    let returnVal = {
      key: props.stateKey,
      value: e.target.value,
    };
    props.onChange(returnVal);
  };

  return (
    <>
      <input type="text" value={props.data} onChange={onChange} />
    </>
  );
};

export default CustomInputFilter;
