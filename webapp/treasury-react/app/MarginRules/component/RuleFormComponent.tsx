import React from "react";
import CpeFilter from "../../commons/container/CpeFilter";
import AgreementTypeFilter from "../../commons/container/AgreementTypeFilter";
import GBOTypeFilter from "../../commons/container/GBOTypeFilter";
import CustodianAccountFilter from "../../commons/container/CustodianAccountFilter";
import CurrencyFilter from "../../commons/container/CurrencyFilter";
import SingleSelectFilter from "../../commons/filters/components/SingleSelectFilter";
import InputFilter from "../../commons/components/InputFilter";
import DateFilter from "../../commons/filters/components/DateFilter";
import MarginAllocatorFilter from "../../commons/filters/containers/MarginAllocatorFilter";
import CountryFilter from "../../commons/container/CountryFilter";
import MarketFilter from "../../commons/container/MarketFilter";
import CustomPnlSpnFilter from "../component/CustomPnlSpnFilter";
import {
  CALCULATOR_TYPES_REF_DATA,
  RULE_TYPES_REF_DATA,
  USAGE_TYPES_REF_DATA,
  FIELD_TYPES,
  DIRECTION,
  POSITION_RULE_TYPE
} from "../constants";
import { observer } from "mobx-react-lite";
import { RuleFormComponentProps } from "../models/Props";
import { useStores } from "../useStores";

const RuleFormComponent: React.FC<RuleFormComponentProps> = ({
  onSelect,
  popUpSelectedData,
  showPositionLevelDetails,
  isReadOnly,
  selectedRow
}) => {

  const { rootStore } = useStores();
  const searchRulesStore = rootStore.searchRulesStore;
  const modifyRulesStore = rootStore.modifyRulesStore;

  return (
    <>
      <DateFilter
        label="Effective Start Date"
        stateKey="selectedStartDate"
        data={popUpSelectedData.selectedStartDate}
        onChange={onSelect}
      />
      <DateFilter
        label="Effective End Date"
        stateKey="selectedEndDate"
        data={popUpSelectedData.selectedEndDate}
        onChange={onSelect}
      />
      <SingleSelectFilter
        horizontalLayout={true}
        label="Margin Type*"
        data={CALCULATOR_TYPES_REF_DATA}
        onSelect={onSelect}
        stateKey="selectedMarginTypes"
        selectedData={popUpSelectedData.selectedMarginTypes}
      />
      <SingleSelectFilter
          data={searchRulesStore.selectedLegalEntityData}
          onSelect={onSelect}
          selectedData={popUpSelectedData.selectedLegalEntities}
          label="Legal Entity*"
          stateKey="selectedLegalEntities"
          horizontalLayout
          multiSelect={false}
          disabled={isReadOnly}
      />
      <CpeFilter
        label="Exposure Counterparty*"
        onSelect={onSelect}
        selectedData={popUpSelectedData.selectedCpes}
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />
      <AgreementTypeFilter
        label="Agreement Type*"
        onSelect={onSelect}
        selectedData={popUpSelectedData.selectedAgreementTypes}
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />
      <SingleSelectFilter
        horizontalLayout={true}
        label="Rule Type*"
        data={modifyRulesStore.getRuleTypeDropDownData()}
        onSelect={onSelect}
        stateKey="selectedRuleTypes"
        selectedData={popUpSelectedData.selectedRuleTypes}
        disabled={isReadOnly}
      />
      <SingleSelectFilter
        data={searchRulesStore.selectedBookData}
        onSelect={onSelect}
        selectedData={popUpSelectedData.selectedBooks}
        label={popUpSelectedData.selectedRuleTypes?.value.toLowerCase()
          === POSITION_RULE_TYPE.toLowerCase() ? "Book*" : "Book"}
        stateKey="selectedBooks"
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />
      {!isReadOnly && <CountryFilter
        label="Country"
        onSelect={onSelect}
        selectedData={popUpSelectedData.selectedCountries}
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />}
      {!isReadOnly && <MarketFilter
        label="Market"
        onSelect={onSelect}
        stateKey="selectedMarkets"
        selectedData={popUpSelectedData.selectedMarkets}
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />}
      <GBOTypeFilter
        label="GBO Type"
        onSelect={onSelect}
        selectedData={selectedRow ?
          {key : selectedRow.gboTypeId, value: selectedRow.gboTypeName} : popUpSelectedData.selectedGboTypes}
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />
      {!isReadOnly && <GBOTypeFilter
        label="Underlying GBO Type"
        onSelect={onSelect}
        stateKey="selectedUnderlyingGboTypes"
        selectedData={popUpSelectedData.selectedUnderlyingGboTypes}
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />}
      <CustodianAccountFilter
        label={popUpSelectedData.selectedRuleTypes?.value.toLowerCase() === POSITION_RULE_TYPE.toLowerCase()
          ? "Custodian Account*" : "Custodian Account"}
        onSelect={onSelect}
        selectedData={popUpSelectedData.selectedCustodianAccounts}
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />
      <CurrencyFilter
        label="Currency"
        onSelect={onSelect}
        selectedData={selectedRow ?
          {key : selectedRow.currencyId, value: selectedRow.currencyIsoCode} : popUpSelectedData.selectedCurrencies}
        horizontalLayout
        multiSelect={false}
        disabled={isReadOnly}
      />
      {!isReadOnly && <SingleSelectFilter
        horizontalLayout={true}
        label="Direction"
        data={DIRECTION}
        onSelect={onSelect}
        stateKey="selectedDirections"
        selectedData={popUpSelectedData.selectedDirections}
      />}
      <SingleSelectFilter
        horizontalLayout={true}
        label={popUpSelectedData.selectedRuleTypes?.value.toLowerCase() === POSITION_RULE_TYPE.toLowerCase()
          ? "Field Type*" : "Field Type"}
        data={FIELD_TYPES}
        onSelect={onSelect}
        stateKey="selectedFieldTypes"
        selectedData={popUpSelectedData.selectedFieldTypes}
        disabled={isReadOnly}
      />
      <SingleSelectFilter
        label="Margin Calculator*"
        onSelect={onSelect}
        selectedData={popUpSelectedData.selectedMarginCalculators}
        horizontalLayout
        stateKey="selectedMarginCalculators"
        data={modifyRulesStore.calculatorData}
        disabled={isReadOnly}
      />
      <SingleSelectFilter
        label="Allocator*"
        onSelect={onSelect}
        selectedData={popUpSelectedData.selectedAllocators}
        horizontalLayout
        stateKey="selectedAllocators"
        data={modifyRulesStore.marginAllocatorData}
        disabled={isReadOnly}
      />
      {showPositionLevelDetails && (
        <>
          <br />
          <legend style={{ fontWeight: "bold" }}>Position Level Details</legend>
          {isReadOnly &&
            <InputFilter
              label="P&L SPN*"
              stateKey="selectedPnlSpn"
              data={popUpSelectedData.selectedPnlSpn}
              onSelect={onSelect}
              disabled={isReadOnly}
            />
          }
          {!isReadOnly && <CustomPnlSpnFilter
            label="P&L SPN*"
            stateKey="selectedPnlSpn"
            data={popUpSelectedData.selectedPnlSpn}
            onSelect={onSelect}
          />}
          <SingleSelectFilter
            label="Usage Type*"
            data={USAGE_TYPES_REF_DATA}
            horizontalLayout={true}
            onSelect={onSelect}
            stateKey="selectedUsageTypes"
            selectedData={popUpSelectedData.selectedUsageTypes}
          />
          <CurrencyFilter
            label="Usage Currency*"
            onSelect={onSelect}
            stateKey="selectedUsageCurrency"
            selectedData={popUpSelectedData.selectedUsageCurrency}
            horizontalLayout
            multiSelect={false}
          />
          <InputFilter
            label="Usage Haircut*"
            data={popUpSelectedData.selectedUsageHaircut}
            onSelect={onSelect}
            stateKey="selectedUsageHaircut"
            title="Enter value in fraction, for ex, enter 0.05 for 5% haircut.For ABSOLUTE_VALUE, enter the value as it is."
          />
        </>
      )}
      <InputFilter
        label="Comments*"
        data={popUpSelectedData.selectedComments}
        onSelect={onSelect}
        stateKey="selectedComments"
      />
      { !isReadOnly && popUpSelectedData.selectedUsername !== "" && popUpSelectedData.selectedUsername !== undefined && <InputFilter
        label="User Name"
        data={popUpSelectedData.selectedUsername}
        onSelect={onSelect}
        stateKey="selectedUsername"
        disabled={ true }
      />}
    </>
  );
};

export default observer(RuleFormComponent);
