import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../commons/components/ReactLoader";
import Loader from "../commons/container/Loader";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";
import AppHeader, { whenHeaderRef } from "arc-header";
import { Breadcrumbs, Layout } from "arc-react-components";
import { URLS } from "./constants";
import { useStores } from "./useStores";
import { GlobalFilters } from 'arc-header';

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={URLS.MARGIN_RULES_URL} key="marginRules">
        Margin Rules
      </Breadcrumbs.Item>
    </>
  );
};

const MarginRules: React.FC = () => {
  useEffect(() => {
    updateBreadCrumbs();
    modifyRulesStore.prefetchRefData();
    setGlobalFilters();
  }, []);

  const setGlobalFilters = async () => {
    const headerRef = await whenHeaderRef();
    if (headerRef && headerRef.gf) {
      const options = {
        enabled: {
          strategies: true,
          fundFamilies: true,
          legalEntityFamilies: true
        }
      };
      headerRef.gf.addListener(appHeaderListener);
      headerRef.gf.setOptions(options);
    }
  };

  const { rootStore } = useStores();
  const { searchRulesStore, modifyRulesStore } = rootStore;

  function appHeaderListener({ userSelections, expandedSelections }) {
    searchRulesStore.setExpandedGlobalSelections(expandedSelections);
  }

  return (
    <>
      <Loader />
      <ReactLoader
        inProgress={searchRulesStore.inProgress || modifyRulesStore.inProgress}
      />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={true} />
        <Layout
          isColumnType={true}
          className="border"
          style={{ height: "100%" }}
        >
          <Layout.Child
            childId="MarginRulesIndexChild1"
            size={1}
            key="child1"
            title="Margin Rules Filter"
            collapsible
            collapsed={searchRulesStore.collapseSideBar}
            onChange={searchRulesStore.setCollapsed}
            showHeader
          >
            <SideBar />
          </Layout.Child>
          <Layout.Divider childId="MarginRulesIndexChild2" isResizable key="child2" />
          <Layout.Child childId="MarginRulesIndexChild3" size={3} key="child3">
            <Grid />
          </Layout.Child>
        </Layout>
      </div>
    </>
  );
};

export default observer(MarginRules);
