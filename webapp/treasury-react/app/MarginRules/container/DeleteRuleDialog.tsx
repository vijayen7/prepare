import React from "react";
import { useStores } from "../useStores";
import { observer } from "mobx-react-lite";
import { Layout, Dialog } from "arc-react-components";
import InputFilter from "../../commons/components/InputFilter";

const DeleteRuleDialog: React.FC = () => {
  const { rootStore } = useStores();
  const { searchRulesStore } = rootStore;

  return (
    <Dialog
      title="Delete Rule"
      isOpen={searchRulesStore.isOpenDeleteRuleDialog}
      onClose={searchRulesStore.onCloseDeleteRuleDialog}
      footer={
        <>
          <button onClick={searchRulesStore.onConfirmDelete}>Delete</button>
          <button onClick={searchRulesStore.onCloseDeleteRuleDialog}>
            Cancel
          </button>
        </>
      }
    >
      <InputFilter
        label="Comments*"
        data={searchRulesStore.deleteRuleComment}
        onSelect={searchRulesStore.onChangeDeleteRuleComment}
        stateKey="deleteRuleComment"
      />
    </Dialog>
  );
};

export default observer(DeleteRuleDialog);
