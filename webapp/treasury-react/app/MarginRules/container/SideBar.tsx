import React from "react";
import { Sidebar } from "arc-react-components";
import { observer } from "mobx-react-lite";
import MarginCalculatorFilter from "../../commons/container/MarginCalculatorFilter";
import FilterButton from "../../commons/components/FilterButton";
import CpeFilter from "../../commons/container/CpeFilter";
import AgreementTypeFilter from "../../commons/container/AgreementTypeFilter";
import GBOTypeFilter from "../../commons/container/GBOTypeFilter";
import DateFilter from "../../commons/filters/components/DateFilter";
import SaveSettingsManager from "../../commons/components/SaveSettingsManager";
import CheckboxFilter from "../../commons/components/CheckboxFilter";
import MultiSelectFilter from "../../commons/filters/components/MultiSelectFilter";
import CustomPnlSpnFilter from "../component/CustomPnlSpnFilter";
import {
  CALCULATOR_TYPES_REF_DATA,
  RULE_TYPES_REF_DATA,
  USAGE_TYPES_REF_DATA,
} from "../constants";
import { useStores } from "../useStores";

const SideBar: React.FC = () => {
  const { rootStore } = useStores();
  const searchRulesStore = rootStore.searchRulesStore;
  const modifyRulesStore = rootStore.modifyRulesStore;

  return (
    <>
      <Sidebar
        header={false}
        footer={
          <>
            <FilterButton
              onClick={searchRulesStore.handleSearch}
              label="Search"
            />
            <FilterButton
              reset={true}
              onClick={searchRulesStore.handleReset}
              label="Reset"
            />
          </>
        }
      >
        <SaveSettingsManager
          selectedFilters={searchRulesStore.searchFilter}
          applySavedFilters={searchRulesStore.applySavedFilters}
          applicationName="MarginRulesFilter"
        />
        <DateFilter
          label="Applicable On"
          stateKey="selectedDate"
          data={searchRulesStore.searchFilter.selectedDate}
          onChange={searchRulesStore.onSelect}
        />
        <MultiSelectFilter
          data={searchRulesStore.selectedLegalEntityData}
          onSelect={searchRulesStore.onSelect}
          label="Legal Entities"
          selectedData={searchRulesStore.searchFilter.selectedLegalEntities}
          stateKey="selectedLegalEntities"
          horizontalLayout
        />
        <CpeFilter
          label="Exposure Counterparties"
          onSelect={searchRulesStore.onSelect}
          selectedData={searchRulesStore.searchFilter.selectedCpes}
          horizontalLayout
          multiSelect
        />
        <AgreementTypeFilter
          onSelect={searchRulesStore.onSelect}
          selectedData={searchRulesStore.searchFilter.selectedAgreementTypes}
          horizontalLayout
          multiSelect
        />
        <MultiSelectFilter
          data={CALCULATOR_TYPES_REF_DATA}
          onSelect={searchRulesStore.onSelect}
          label="Margin Types"
          selectedData={searchRulesStore.searchFilter.selectedCalculatorTypes}
          stateKey="selectedCalculatorTypes"
          horizontalLayout
        />
        <MultiSelectFilter
          onSelect={searchRulesStore.onSelect}
          data={modifyRulesStore.calculatorData}
          label="Margin Calculators"
          stateKey="selectedMarginCalculators"
          selectedData={searchRulesStore.searchFilter.selectedMarginCalculators}
          horizontalLayout
        />
        <MultiSelectFilter
          data={RULE_TYPES_REF_DATA}
          onSelect={searchRulesStore.onSelect}
          label="Rule Types"
          selectedData={searchRulesStore.searchFilter.selectedRuleTypes}
          stateKey="selectedRuleTypes"
          horizontalLayout
        />
        <MultiSelectFilter
          data={USAGE_TYPES_REF_DATA}
          onSelect={searchRulesStore.onSelect}
          label="Usage Types"
          selectedData={searchRulesStore.searchFilter.selectedUsageTypes}
          stateKey="selectedUsageTypes"
          horizontalLayout
        />
        <GBOTypeFilter
          label="GBO Types"
          onSelect={searchRulesStore.onSelect}
          horizontalLayout={true}
          selectedData={searchRulesStore.searchFilter.selectedGboTypes}
          multiSelect
        />
        <GBOTypeFilter
          label="Underlying GBO Types"
          onSelect={searchRulesStore.onSelect}
          stateKey="selectedUnderlyingGboTypes"
          horizontalLayout={true}
          selectedData={
            searchRulesStore.searchFilter.selectedUnderlyingGboTypes
          }
          multiSelect
        />
        <CustomPnlSpnFilter
          label="P&L SPN"
          stateKey="selectedPnlSpns"
          data={searchRulesStore.searchFilter.selectedPnlSpns}
          onSelect={searchRulesStore.onSelect}
        />
        <br />
        <CheckboxFilter
          label="Include Invalid Rules"
          defaultChecked={searchRulesStore.searchFilter.showInvalidRules}
          onSelect={searchRulesStore.onSelect}
          stateKey="showInvalidRules"
          style="left"
        />
      </Sidebar>
    </>
  );
};

export default observer(SideBar);
