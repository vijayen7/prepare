import ArcDataGrid from "arc-data-grid";
import React from "react";
import { observer } from "mobx-react-lite";
import { getGridColumns } from "../grid/gridColumns";
import { MarginRulesGridconfig } from "../grid/gridConfig";
import Message from "../../commons/components/Message";
import { SUCCESS, FAILURE } from "../../commons/constants";
import { DEFAULT_PINNED_COLUMNS } from "../constants";
import { useStores } from "../useStores";
import ModifyRuleDialog from "./ModifyRuleDialog";
import DeleteRuleDialog from "./DeleteRuleDialog";
import AddRuleButtons from "./AddRuleButtons";
import { Layout } from "arc-react-components";

const Grid: React.FC = () => {
  const { rootStore } = useStores();
  const { searchRulesStore } = rootStore;

  const renderGridData = () => {
    if (searchRulesStore.marginRulesGridData.length) {
      return (
        <>
          <ArcDataGrid
            rows={searchRulesStore.marginRulesGridData}
            columns={getGridColumns()}
            displayColumns={searchRulesStore.displayColumns}
            pinnedColumns={DEFAULT_PINNED_COLUMNS}
            onDisplayColumnsChange={searchRulesStore.onDisplayColumnsChange}
            configurations={MarginRulesGridconfig}
          />
        </>
      );
    } else {
      return (
        <Message
          messageData={searchRulesStore.searchStatus.message}
          error={searchRulesStore.searchStatus.status == FAILURE}
          warning={searchRulesStore.searchStatus.status == SUCCESS}
        />
      );
    }
  };

  return (
    <>
      <Layout>
        <Layout.Child childId="MarginRulesGridChild1" size="fit">
          <AddRuleButtons />
        </Layout.Child>
        <Layout.Child childId="MarginRulesGridChild2">{renderGridData()}</Layout.Child>
      </Layout>
      <ModifyRuleDialog />
      <DeleteRuleDialog />
    </>
  );
};

export default observer(Grid);
