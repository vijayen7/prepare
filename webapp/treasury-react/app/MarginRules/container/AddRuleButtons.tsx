import React from "react";
import { observer } from "mobx-react-lite";
import { useStores } from "../useStores";
import { DIALOG_HEADERS, URLS } from "../constants";

const AddRuleButtons: React.FC = () => {
  const { rootStore } = useStores();
  const { modifyRulesStore, searchRulesStore } = rootStore;

  const onAddRule: any = () => {
    modifyRulesStore.toggleShowDialog(DIALOG_HEADERS.ADD_RULE);
  };

  const onBulkInsert: any = () => {
    let url = URLS.BULK_INSERT_URL;
    window.open(url, "_blank");
  };

  const onBulkUpdate: any = () => {
    let url = URLS.BULK_UPDATE_URL;
    window.open(url, "_blank");
  };

  return (
    <div
      style={{
        textAlign: "right",
      }}
    >
      <button onClick={onAddRule}>Add New Rule</button>
      &nbsp;&nbsp;
      <button onClick={searchRulesStore.generateReportFromAPI}>Create Report</button>
      &nbsp;&nbsp;
      <a onClick={onBulkInsert}>Add Bulk Rules</a>
      &nbsp;&nbsp;
      <a onClick={onBulkUpdate}>Bulk Update Rules</a>
    </div>
  );
};

export default observer(AddRuleButtons);
