import React from "react";
import { useStores } from "../useStores";
import { observer } from "mobx-react-lite";
import RuleFormComponent from "../component/RuleFormComponent";
import { POSITION_RULE_TYPE } from "../constants";
import { Layout, Dialog, Message } from "arc-react-components";
import StatusComponent from "../../commons/components/StatusComponent";
import ConfirmationDialog from "../../commons/components/ConfirmationDialog";
import { MESSAGES } from "../constants";
import ConflictingRulesTable from "./ConflictingRulesTable";
import { ModifyRuleDialogProps } from "../models/Props";

const ModifyRuleDialog: React.FC<ModifyRuleDialogProps> = (props:ModifyRuleDialogProps) => {
  const { rootStore } = useStores();
  const { modifyRulesStore } = rootStore;

  const render = () => {
    let isPositionRuleType: boolean =
      modifyRulesStore.popUpSelectedData.selectedRuleTypes?.value.toLowerCase() ==
      POSITION_RULE_TYPE.toLowerCase();
    return (
      <>
        <Dialog
          title={modifyRulesStore.dialogTitle}
          isOpen={modifyRulesStore.showModifyRuleDialog}
          onClose={modifyRulesStore.toggleShowDialog}
          style={{ width: modifyRulesStore.setDialogWidth ? "33%" : null }}
          footer={
            <Layout>
              <Layout.Child childId="MarginRulesModifyRuleDialogChild1">
                {!props.isReadOnly && modifyRulesStore.buttonsData.showValidate && (
                  <button onClick={modifyRulesStore.onValidate}>
                    Validate
                  </button>
                )}
                {!props.isReadOnly && !modifyRulesStore.buttonsData.showSave &&
                !modifyRulesStore.buttonsData.showUpdateConflicts
                && !modifyRulesStore.buttonsData.showValidate &&(
                  <button onClick={modifyRulesStore.onUpdateConflicts}
                  disabled={!modifyRulesStore.buttonsData.showUpdateConflicts}
                  >
                    Update Conflict(s)
                  </button>
                )}
                {modifyRulesStore.buttonsData.showUpdateConflicts && (
                  <button onClick={modifyRulesStore.onUpdateConflicts}>
                    Update Conflict(s)
                  </button>
                )}
                <button
                  onClick={modifyRulesStore.onSave}
                  disabled={props.isReadOnly ? false : !modifyRulesStore.buttonsData.showSave}
                >
                  Save Rule
                </button>
                {!props.isReadOnly && modifyRulesStore.buttonsData.showReset && (
                  <button onClick={modifyRulesStore.onResetFilters}>
                    Reset
                  </button>
                )}
                {!props.isReadOnly && modifyRulesStore.buttonsData.showBack && (
                  <button onClick={modifyRulesStore.onBack}>Back</button>
                )}
              </Layout.Child>
            </Layout>
          }
        >
          {!modifyRulesStore.showConflictingRules && (
            <Layout>
              <Layout.Child
                childId="MarginRulesModifyRuleDialogChild2"
                title="Margin Rule Details"
                showHeader={true}
                className="form padding--left"
              >
                {props.isPositionRuleAvailable === false && <Message>
                  Position Rule doesn't exist for selected position and margin type
                </Message>
                }
                <RuleFormComponent
                  onSelect= {props.onSelect ? props.onSelect : modifyRulesStore.onSelect}
                  popUpSelectedData={modifyRulesStore.popUpSelectedData}
                  showPositionLevelDetails={isPositionRuleType}
                  isReadOnly= {props.isReadOnly}
                  selectedRow= {props.selectedRow}
                />
              </Layout.Child>
            </Layout>
          )}
          {modifyRulesStore.showConflictingRules && <ConflictingRulesTable />}
        </Dialog>
        <ConfirmationDialog
          title="Confirmation Dialog"
          isOpen={modifyRulesStore.isOpenConfirmationDialog}
          onClose={modifyRulesStore.onCloseConfirmationDialog}
          onConfirm={modifyRulesStore.onBackConfirmed}
          message={MESSAGES.BACK_TO_MODIFY_RULE_CONFIRMATION_MESSAGE}
        />
        <StatusComponent
          status={modifyRulesStore.status}
          isOpen={modifyRulesStore.showStatus}
          onClose={modifyRulesStore.onCloseStatus}
          title={modifyRulesStore.statusTitle}
        />
      </>
    );
  };

  return <>{render()}</>;
};

export default observer(ModifyRuleDialog);
