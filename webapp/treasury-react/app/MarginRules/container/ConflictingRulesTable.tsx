import React from "react";
import { useStores } from "../useStores";
import { observer } from "mobx-react-lite";
import { POSITION_RULE_TYPE } from "../constants";
import { decamelize } from "../../commons/util";
import CustomInputFilter from "../component/CustomInputFilter";
import DateFilter from "../../commons/filters/components/DateFilter";

const ConflictingRulesTable: React.FC = () => {
  const { rootStore } = useStores();
  const { modifyRulesStore } = rootStore;

  const getHeaderRow = () => {
    let columns: any = [];
    for (let i = 0; i < modifyRulesStore.conflictingRulesData.length; i++) {
      columns.push(<th>{"Conflicting Rule " + (i + 1).toString()}</th>);
    }
    return (
      <tr>
        <th>{"Details"}</th>
        <th>{"New Rule"}</th>
        {columns}
      </tr>
    );
  };

  const getDataRow = (key: string) => {
    let conflictingRulesRowData: any = [];
    modifyRulesStore.flatConflictingRulesData.forEach((e) => {
      conflictingRulesRowData.push(<td>{e[key]}</td>);
    });
    let detailColumnVal = decamelize(key, " ");
    if (key == "pnlSpn") {
      detailColumnVal = "P&L SPN";
    }
    return (
      <tr>
        <th>{detailColumnVal}</th>
        <td>{modifyRulesStore.flatMarginRuleData[key]}</td>
        {conflictingRulesRowData}
      </tr>
    );
  };

  const getPositionLevelHeaderRow = () => {
    let positionLevelHeaderRow: any = [];
    modifyRulesStore.flatConflictingRulesData.forEach((e) => {
      positionLevelHeaderRow.push(<th>{"Position Level Details"}</th>);
    });
    return (
      <tr>
        <th>{""}</th>
        <th>{"Position Level Details"}</th>
        {positionLevelHeaderRow}
      </tr>
    );
  };

  const getDataRowForEndDate = () => {
    let columns: any = [];
    for (let i = 0; i < modifyRulesStore.conflictingRulesData.length; i++) {
      columns.push(
        <td>
          {
            <DateFilter
              stateKey={i}
              data={modifyRulesStore.conflictingRulesData[i].effectiveEndDate}
              onChange={modifyRulesStore.onChangeConflictingRulesDate}
            />
          }
        </td>
      );
    }
    return (
      <tr>
        <th>{"Effective End Date"}</th>
        <td>{modifyRulesStore.flatMarginRuleData.effectiveEndDate}</td>
        {columns}
      </tr>
    );
  };

  const getDataRowForComment = () => {
    let columns: any = [];

    for (let rulesIter = 0; rulesIter < modifyRulesStore.conflictingRulesData.length; rulesIter++) {
      if(modifyRulesStore.populateComment == false){
        modifyRulesStore.conflictingRulesData[rulesIter].userComments = "";
      }
    }
    for (let i = 0; i < modifyRulesStore.conflictingRulesData.length; i++) {
      columns.push(
        <td>
          {
            <CustomInputFilter
              stateKey={i}
              data={modifyRulesStore.conflictingRulesData[i].userComments}
              onChange={modifyRulesStore.onChangeConflictingRulesComments}
            />
          }
        </td>
      );
    }
    modifyRulesStore.populateComment = true;
    return (
      <tr>
        <th>{"Comments"}</th>
        <td>
          {
            <CustomInputFilter
              stateKey="selectedComments"
              data={modifyRulesStore.popUpSelectedData.selectedComments}
              onChange={modifyRulesStore.onSelect}
            />
          }
        </td>
        {columns}
      </tr>
    );
  };

  const isPositionRuleType: boolean =
    modifyRulesStore.popUpSelectedData.selectedRuleTypes?.value.toLowerCase() ==
    POSITION_RULE_TYPE.toLowerCase();

  return (
    <>
      <table className="table table--fill--width">
        <tbody>
          {getHeaderRow()}
          {getDataRow("effectiveStartDate")}
          {getDataRowForEndDate()}
          {getDataRow("legalEntity")}
          {getDataRow("exposureCounterparty")}
          {getDataRow("agreementType")}
          {getDataRow("gboType")}
          {getDataRow("underlyingGboType")}
          {getDataRow("custodianAccount")}
          {getDataRow("currency")}
          {getDataRow("direction")}
          {getDataRow("ruleType")}
          {getDataRow("fieldType")}
          {getDataRow("marginType")}
          {getDataRow("marginCalculator")}
          {getDataRow("allocator")}
          {getDataRow("book")}
          {getDataRow("country")}
          {getDataRow("market")}
          {isPositionRuleType && getPositionLevelHeaderRow()}
          {isPositionRuleType && getDataRow("pnlSpn")}
          {isPositionRuleType && getDataRow("usageType")}
          {isPositionRuleType && getDataRow("usageCurrency")}
          {isPositionRuleType && getDataRow("usageHaircut")}
          {getDataRowForComment()}
        </tbody>
      </table>
    </>
  );
};

export default observer(ConflictingRulesTable);
