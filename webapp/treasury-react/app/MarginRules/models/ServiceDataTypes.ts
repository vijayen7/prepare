import ReferenceData from "../../commons/models/ReferenceData";

export interface MarginRuleFilter {
  applicableOn: string;
  legalEntityIds: number[];
  counterPartyIds: number[];
  agreementTypeIds: number[];
  marginTypes: number[];
  calculatorIds: number[];
  ruleTypes: number[];
  usageTypes: number[];
  gboTypeIds: number[];
  underlyingGboTypeIds: number[];
  pnlSpns: number[];
  showInvalidRules: boolean;
  "@CLASS": "arcesium.treasury.margin.common.model.MarginRuleFilter";
}

export interface MarginRuleData {
  ruleId?: number;
  legalEntity: ReferenceData;
  counterPartyEntity: ReferenceData;
  agreementType: ReferenceData;
  marginType: ReferenceData;
  ruleType: ReferenceData;
  calculator: ReferenceData;
  pnlSpn?: number;
  custodianAccount?: ReferenceData;
  book?: ReferenceData;
  currency?: ReferenceData;
  market?: ReferenceData;
  country?: ReferenceData;
  fieldType?: ReferenceData;
  gboType?: ReferenceData;
  underlyingGboType?: ReferenceData;
  direction?: ReferenceData;
  allocator?: ReferenceData;
  effectiveStartDate: string;
  effectiveEndDate: string;
  usageType?: ReferenceData;
  usageHaircut?: number;
  usageCurrency?: ReferenceData;
  user?: string;
  userComments: string;
  isValid?: boolean;
  "@CLASS": "arcesium.treasury.margin.common.model.EnrichedMarginRule";
}

export interface ApiResponse {
  successStatus: boolean;
  message: string;
  response: any;
  "@CLASS"?: string;
}

export interface AgreementData {
  agreementId:number;
  cpeId:number;
  dESCOEntityId:number;
  agreementTypeId:number;
  "@CLASS": "deshaw.moss.model.goldensource.Agreement";
}

export interface MarginRuleKey {
  ruleId?: number;
  marginType?: number;
  "@CLASS": "arcesium.treasury.margin.common.model.MarginRuleKey";
}
