import MapData from "../../commons/models/MapData";

export interface SearchFilter {
  [key: string]: any;
  selectedDate: string;
  selectedLegalEntities: MapData[];
  selectedCpes: MapData[];
  selectedAgreementTypes: MapData[];
  selectedCalculatorTypes: MapData[];
  selectedMarginCalculators: MapData[];
  selectedRuleTypes: MapData[];
  selectedUsageTypes: MapData[];
  selectedGboTypes: MapData[];
  selectedUnderlyingGboTypes: MapData[];
  selectedPnlSpns: string;
  showInvalidRules: boolean;
}

export interface PopUpSelectedData {
  [key: string]: any;
  selectedStartDate: string;
  selectedEndDate: string;
  selectedLegalEntities: MapData | null;
  selectedCpes: MapData | null;
  selectedAgreementTypes: MapData | null;
  selectedGboTypes?: MapData | null;
  selectedUnderlyingGboTypes?: MapData | null;
  selectedCustodianAccounts: MapData | null;
  selectedCurrencies?: MapData | null;
  selectedDirections?: MapData | null;
  selectedRuleTypes: MapData | null;
  selectedFieldTypes: MapData | null;
  selectedMarginTypes: MapData | null;
  selectedMarginCalculators: MapData | null;
  selectedAllocators: MapData | null;
  selectedBooks: MapData | null;
  selectedCountries?: MapData | null;
  selectedMarkets?: MapData | null;
  selectedPnlSpn: string;
  selectedUsageTypes: MapData | null;
  selectedUsageCurrency: MapData | null;
  selectedUsageHaircut: string;
  selectedComments: string;
  selectedUsername?: string;
  selectedRuleId?: number | null;
}

export interface FlatRuleData {
  [key: string]: any;
  effectiveStartDate: string;
  effectiveEndDate: string;
  legalEntity: string;
  exposureCounterparty: string;
  agreementType: string;
  gboType: string;
  underlyingGboType: string;
  custodianAccount: string;
  currency: string | undefined;
  direction: string;
  ruleType: string;
  fieldType: string;
  marginType: string;
  marginCalculator: string;
  allocator: string;
  book: string;
  country: string;
  market: string;
  pnlSpn: string | number;
  usageType: string;
  usageCurrency: string;
  usageHaircut: string | number;
  comments: string;
}

export interface GridData {
  marginRuleId?: number;
  marginTypeId?: number;
  legalEntity: string;
  counterPartyEntity: string;
  agreementType: string;
  marginType: string;
  ruleType: string;
  calculator: string;
  pnlSpn: number | string;
  custodianAccount: string;
  book: string;
  currency: string;
  market: string;
  country: string;
  fieldType: string;
  gboType: string;
  underlyingGboType: string;
  direction: string;
  allocator: string;
  usageType: string;
  usageHaircut: number | string;
  usageCurrency: string;
  effectiveStartDate: string;
  effectiveEndDate: string;
  user: string;
  userComments: string;
  isValid?: string;
}

export interface ButtonsData {
  showReset: boolean;
  showBack: boolean;
  showSave: boolean;
  showValidate: boolean;
  showUpdateConflicts: boolean;
}
