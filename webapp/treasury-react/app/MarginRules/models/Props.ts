import { PopUpSelectedData } from "../models/UIDataTypes";

export interface RuleFormComponentProps {
  onSelect: Function;
  popUpSelectedData: PopUpSelectedData;
  showPositionLevelDetails: boolean;
  isReadOnly?: boolean;
  selectedRow?: any;
  isPositionRuleAvailable?: boolean;
}

export interface CustomPnlSpnFilterProps {
  label: string;
  stateKey: string;
  data: string;
  onSelect: Function;
}

export interface CustomInputFilterProps {
  stateKey: number | string;
  data: string;
  onChange: Function;
}

export interface ModifyRuleDialogProps {
  onSelect?: Function;
  isReadOnly?: boolean;
  selectedRow?: any;
  isPositionRuleAvailable?: boolean;
}
