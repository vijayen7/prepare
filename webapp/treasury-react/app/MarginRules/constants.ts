import MapData from "../commons/models/MapData";
import { getPreviousBusinessDay } from "../commons/util";

export const CALCULATOR_TYPES_REF_DATA: MapData[] = [
  { key: 1, value: "House" },
  { key: 4, value: "Regulatory" },
  { key: 3, value: "Exchange" },
  { key: 5, value: "Segregated IA" },
  { key: 6, value: "Regulatory CPE" },
];

export const RULE_TYPES_REF_DATA: MapData[] = [
  { key: 1, value: "Portfolio" },
  { key: 2, value: "Position" },
];

export const POSITION_RULE_TYPES_REF_DATA: MapData[] = [
  { key: 2, value: "Position" },
];

export const ALL_GRID_COLUMNS: string[] = [
  "actions",
  "marginRuleId",
  "legalEntity",
  "counterPartyEntity",
  "agreementType",
  "marginType",
  "ruleType",
  "calculator",
  "pnlSpn",
  "custodianAccount",
  "book",
  "currency",
  "market",
  "country",
  "fieldType",
  "gboType",
  "underlyingGboType",
  "direction",
  "allocator",
  "usageType",
  "usageHaircut",
  "usageCurrency",
  "effectiveStartDate",
  "effectiveEndDate",
  "user",
  "userComments",
  "isValid",
];

export const INITIAL_DISPLAY_COLUMNS: string[] = ALL_GRID_COLUMNS.filter(
  (e) => e != "marginRuleId"
);

export const DEFAULT_PINNED_COLUMNS = { actions: true };

export const POSITION_RULE_TYPE: string = "Position";

export const PORTFOLIO_RULE_TYPE: string = "Portfolio";

const DEFAULT_RULE_END_DATE: string = "2038-01-01";

export const POSITION_MASTER_CALCULATOR = "PositionMaster ";

export const DEFAULT_MARGIN_ALLOCATOR = "DirectionalityBasedMarginAllocator";

export const USAGE_TYPES_REF_DATA: MapData[] = [
  { key: 1, value: "PERCENT_MARKET_VALUE" },
  { key: 2, value: "PERCENT_NOTIONAL" },
  { key: 3, value: "CONST_VEGA_MULTIPLIER" },
  { key: 4, value: "ABSOLUTE_VALUE" },
  { key: 5, value: "CONST_VOL_MULTIPLIER" },
  { key: 6, value: "PORTFOLIO_MARGIN" },
];

export const FIELD_TYPES: MapData[] = [
  { key: 1, value: "Accurued Interest" },
  { key: 14, value: "Real" },
  { key: 17, value: "Swap Finance Charge" },
  { key: 22, value: "Bad Debt" },
];

export const DIRECTION: MapData[] = [
  { key: 1, value: "SHORT" },
  { key: 2, value: "LONG" },
];

export const URLS = {
  MARGIN_RULES_URL: "/treasury/margin/marginRules.html",
  BULK_INSERT_URL: "/cocoa/#/?parserNames=Margin_Rule_Insert",
  BULK_UPDATE_URL: "/cocoa/#/?parserNames=Update_Margin_Rules",
};

export const MESSAGES = {
  FILL_REQUIRED_INPUT_MESSAGE: "Please fill all the required inputs(*)",
  INVALID_AGREEMENT:
    "Please enter a valid Combination of Legal Entity, Exposure Counterparty and Agreement Type",
  INVALID_PNL_SPN_WHILE_ADDING:
    "P&L SPN input should be a valid single integer",
  INVALID_USAGE_HARICUT: "Please enter a valid numeric input for Usage Haircut",
  INVALID_START_DATE_AND_END_DATE:
    "Please provide valid Start Date and End Date for the rule",
  START_DATE_AND_END_DATE_CONFLICT:
    "Please provide Start Date that occurs before the End Date",
  FILL_END_DATES_FOR_CONFLICTING_RULES:
    "Please fill end dates for all the conflicting rules",
  FILL_COMMENTS_FOR_CONFLICTING_RULES:
    "Please fill the comments for all the conflicting rules",
  INITIAL_SEARCH_MESSAGE: "Perform search to view Margin Rules",
  SEARCH_RULES_FAILURE_MESSAGE: "Some error occurred while searching rules",
  INVALID_PNL_SPN_MESSAGE:
    "P&L SPN Input is invalid. Please enter semi-colon(;) separated integers",
  INVALID_DATE_INPUT: "Please give a valid date input",
  NO_MARGIN_RULES_MESSAGE:
    "No Margin Rules are available for the performed search",
  ERROR_MESSAGE: "Error Occurred while fetching margin rules",
  VALIDATE_SUCCESS_MESSAGE:
    "No conflicting rules found. The rule is ready to be saved",
  UPDATE_CONFLICTS_MESSAGE:
    "Please update the conflicting rules in order to save the new rule",
  DELETE_RULE_CONFIRMATION_MESSAGE: "Are you sure you want to delete the rule?",
  DELETE_RULE_FAILURE_MESSAGE:
    "Some error occurred while trying to delete the rule",
  DELETE_RULE_SUCCESS_MESSAGE: "Margin rule has been deleted succesfully",
  BACK_TO_MODIFY_RULE_CONFIRMATION_MESSAGE:
    "Are you sure you want to go back to modify rule?",
  RULE_ADDED_SUCCESSFULLY: "New Rule has been added successfully",
  ERROR_WHILE_FECTHING_CONFLICTING_RULES:
    "Some error has occurred while fetching conflicting rules",
  ERROR_WHILE_FECTHING_AGREEMENT_DATA:
    "Some error has occurred while fetching agreement data",
  ERROR_WHILE_SAVING_RULE: "Some error has occurred while saving the rule",
  ERROR_WHILE_CLONING_RULE: "Some error has occurred while cloning the rule",
  RULE_SAVED_SUCCESSFULLY: "Margin Rule has been saved successfully",
  ERROR_WHILE_UPDATING_RULE:
    "Some error occurred while updating the margin rule",
  ERROR_WHILE_UPDATING_CONFLICTS:
    "Some error occurred while updating the conflicting rules",
  GENERIC_ERROR_MESSAGE: "Some error occurred while fetching the data",
  UPDATE_CONFLICTS_SUCCESS: "Conflicting rules have been updated succesfully",
  DELETE_RULE_COMMENT: "Please provide the comment for deleting the rule",
};

export const OPERATIONS = {
  ADD_RULE: "ADD_MARGIN_RULE",
  UPDATE_RULE: "UPDATE_MARGIN_RULE",
  CLONE_RULE: "CLONE_MARGIN_RULE",
};

export const DIALOG_HEADERS = {
  ADD_RULE: "Add Margin Rule",
  UPDATE_RULE: "Update Margin Rule",
  CLONE_RULE: "Clone Margin Rule",
};

export const STATUS_HEADERS = {
  SAVE_STATUS: "Save Status",
  VALIDATE_STATUS: "Validation Status",
  UPDATE_STATUS: "Update Status",
  UPDATE_CONFLICTS_STATUS: "Update Conflicts Status",
  REF_DATA_STATUS: "Reference Data Status",
};

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const INITIAL_SEARCH_RULE_STORE_DATA = {
  INITIAL_SEARCH_FILTER: {
    selectedDate: getPreviousBusinessDay(),
    selectedLegalEntities: [],
    selectedCpes: [],
    selectedAgreementTypes: [],
    selectedCalculatorTypes: [],
    selectedMarginCalculators: [],
    selectedRuleTypes: [],
    selectedUsageTypes: [],
    selectedGboTypes: [],
    selectedUnderlyingGboTypes: [],
    selectedPnlSpns: "",
    showInvalidRules: false,
  },
  INITIAL_SEARCH_STATUS: {
    status: "",
    message: MESSAGES.INITIAL_SEARCH_MESSAGE,
  },
  INITIAL_API_RESPONSE: {
    successStatus: true,
    message: "",
    response: null,
  },
};

export const INITIAL_MODIFY_RULE_STORE_DATA = {
  INITIAL_POP_UP_SELECTED_DATA: {
    selectedStartDate: getPreviousBusinessDay(),
    selectedEndDate: DEFAULT_RULE_END_DATE,
    selectedLegalEntities: null,
    selectedCpes: null,
    selectedAgreementTypes: null,
    selectedGboTypes: null,
    selectedUnderlyingGboTypes: null,
    selectedCustodianAccounts: null,
    selectedCurrencies: null,
    selectedDirections: null,
    selectedRuleTypes: null,
    selectedFieldTypes: null,
    selectedMarginTypes: null,
    selectedMarginCalculators: null,
    selectedAllocators: null,
    selectedBooks: null,
    selectedCountries: null,
    selectedMarkets: null,
    selectedPnlSpn: "",
    selectedUsageTypes: null,
    selectedUsageCurrency: null,
    selectedUsageHaircut: "",
    selectedComments: "",
    selectedUsername: ""
  },
  INITIAL_FLAT_CONFLICTING_RULE: {
    effectiveStartDate: "",
    effectiveEndDate: "",
    legalEntity: "",
    exposureCounterparty: "",
    agreementType: "",
    gboType: "",
    underlyingGboType: "",
    custodianAccount: "",
    currency: "",
    direction: "",
    ruleType: "",
    fieldType: "",
    marginType: "",
    marginCalculator: "",
    allocator: "",
    book: "",
    country: "",
    market: "",
    pnlSpn: "",
    usageType: "",
    usageCurrency: "",
    usageHaircut: "",
    comments: "",
  },
  INITIAL_STATUS: {
    status: "",
    message: "",
  },
  INITIAL_BUTTONS_DATA: {
    showBack: false,
    showReset: true,
    showSave: false,
    showValidate: true,
    showUpdateConflicts: false,
  },
  INITIAL_API_RESPONSE: {
    successStatus: true,
    message: "",
    response: null,
  },
};
