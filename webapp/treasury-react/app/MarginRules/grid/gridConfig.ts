import ArcDataGrid from "arc-data-grid";

export const MarginRulesGridconfig: ArcDataGrid.GridConfiguration = {
  clickableRows: true,
  getExportFileName: ({ fileType, all }) => {
    return `${all ? "Margin-Rules" : "Margin-Rules-Current-View"}`;
  },
  timezone: "Asia/Calcutta",
};
