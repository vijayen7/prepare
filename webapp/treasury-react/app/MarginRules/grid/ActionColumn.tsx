import React from "react";
import { observer } from "mobx-react-lite";
import { useStores } from "../useStores";
import { OPERATIONS } from "../constants";

const ActionColumn: React.FC<any> = ({ context }) => {
  const { rootStore } = useStores();
  const { searchRulesStore, modifyRulesStore } = rootStore;

  const renderActionColumn = () => {
    if (context.row.isValid == "Yes") {
      return (
        <>
          <i
            title="Edit"
            className="icon-edit--block"
            onClick={(e) => {
              e.stopPropagation();
              modifyRulesStore.operateOnRule(
                OPERATIONS.UPDATE_RULE,
                context.row.marginRuleId,
                context.row.marginTypeId
              );
            }}
          ></i>
          <i
            title="Clone"
            className="icon-copy"
            onClick={(e) => {
              e.stopPropagation();
              modifyRulesStore.operateOnRule(
                OPERATIONS.CLONE_RULE,
                context.row.marginRuleId,
                context.row.marginTypeId
              );
            }}
          ></i>
          <i
            title="Delete"
            className="icon-delete"
            onClick={(e) => {
              e.stopPropagation();
              searchRulesStore.onDeleteRule(
                context.row.marginRuleId,
                context.row.marginTypeId
              );
            }}
          ></i>
        </>
      );
    }
    return <></>;
  };

  return <>{renderActionColumn()}</>;
};

export default observer(ActionColumn);
