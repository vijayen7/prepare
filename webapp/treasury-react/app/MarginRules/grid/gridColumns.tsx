import React from "react";
import ArcDataGrid from "arc-data-grid";
import ActionColumn from "./ActionColumn";
import { GridData } from "../models/UIDataTypes";

const MarginRulesGridcolumns: ArcDataGrid.Columns<GridData, number> = [
  "marginRuleId",
  "legalEntity",
  {
    identity: "counterPartyEntity",
    header: "Exposure Counterparty",
  },
  "agreementType",
  "marginType",
  "ruleType",
  "calculator",
  {
    identity: "pnlSpn",
    header: "P&L SPN",
  },
  "custodianAccount",
  "book",
  "currency",
  "market",
  "country",
  "fieldType",
  {
    identity: "gboType",
    header: " GBO Type",
  },
  {
    identity: "underlyingGboType",
    header: "Underlying GBO Type",
  },
  "direction",
  "allocator",
  "usageType",
  "usageHaircut",
  "usageCurrency",
  "effectiveStartDate",
  "effectiveEndDate",
  "user",
  "userComments",
  "isValid",
];

export const getGridColumns = () => {
  return [
    {
      identity: "actions",
      Cell: (value, context) => <ActionColumn context={context} />,
      disableFilters: true,
      disableSearches: true,
      disableSortBy: true,
      disableExport: true,
      disableResizing: true,
      width: 90,
    },
    ...MarginRulesGridcolumns,
  ];
};
