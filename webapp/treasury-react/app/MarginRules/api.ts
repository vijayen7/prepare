import { ArcFetch } from "arc-commons";
import { fetchPostURL, fetchURL } from "../commons/util";
import { BASE_URL } from "../commons/constants";
import {
  MarginRuleFilter,
  MarginRuleData,
  MarginRuleKey,
} from "./models/ServiceDataTypes";
import SERVICE_URLS from "../commons/UrlConfigs";

const QUERY_ARGS = {
  credentials: "include",
  method: "GET",
};

const getPostQueryParams = (params) => ({
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
  credentials: 'include',
  method: 'POST',
  body: params
});

export const getMarginRules = (marginRuleFilter: MarginRuleFilter) => {
  const url = `${BASE_URL}service/${
    SERVICE_URLS.marginRuleService.searchMarginRules}`

  const params = `marginRuleFilter=${encodeURIComponent(
    JSON.stringify(marginRuleFilter)
  )}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, getPostQueryParams(params));
};

export const getConflictingRules = (marginRule: MarginRuleData) => {
  const url = `${BASE_URL}service/${
    SERVICE_URLS.marginRuleService.getConflictingRules
  }?enrichedMarginRule=${encodeURIComponent(
    JSON.stringify(marginRule)
  )}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const getAllAgreementData = () => {
  const url = `${BASE_URL}service/${SERVICE_URLS.marginRuleService.getAllAgreementData}?format=json`;
  return ArcFetch.getJSON(url);
};

export const bulkUpdateMarginRules = (
  marginRuleKeyList: MarginRuleKey[],
  marginRulesList: MarginRuleData[]
) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.marginRuleService.bulkUpdateMarginRule}`;
  let param1 = encodeURIComponent(JSON.stringify(marginRuleKeyList));
  let param2 = encodeURIComponent(JSON.stringify(marginRulesList));
  let encodedParm =
    "marginRuleKeyList=" +
    param1 +
    "&enrichedMarginRuleList=" +
    param2 +
    "&inputFormat=JSON&format=JSON";
  return fetchPostURL(url, encodedParm);
};

export const saveMarginRule = (marginRule: MarginRuleData) => {
  const url = `${BASE_URL}service/${
    SERVICE_URLS.marginRuleService.addMarginRule
  }?enrichedMarginRule=${encodeURIComponent(
    JSON.stringify(marginRule)
  )}&opCode=I&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const updateMarginRule = (
  marginRuleKey: MarginRuleKey,
  marginRule: MarginRuleData
) => {
  const url = `${BASE_URL}service/${
    SERVICE_URLS.marginRuleService.updateMarginRule
  }?marginRuleKey=${encodeURIComponent(
    JSON.stringify(marginRuleKey)
  )}&enrichedMarginRule=${encodeURIComponent(
    JSON.stringify(marginRule)
  )}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const deleteMarginRule = (
  marginRuleKey: MarginRuleKey,
  userComments: string
) => {
  const url = `${BASE_URL}service/${
    SERVICE_URLS.marginRuleService.invalidateMarginRule
  }?marginRuleKey=${encodeURIComponent(
    JSON.stringify(marginRuleKey)
  )}&userComments=${userComments}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const getMarginCalculatorData = () => {
  const url = `${BASE_URL}service/${SERVICE_URLS.marginFilterService.getMarginCalculators}?&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const getAllocatorData = () => {
  const url = `${BASE_URL}service/${SERVICE_URLS.marginFilterService.getAllocators}?&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};
