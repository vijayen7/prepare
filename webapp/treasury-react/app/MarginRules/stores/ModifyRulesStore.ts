import { action, observable } from "mobx";
import { PopUpSelectedData, ButtonsData } from "../models/UIDataTypes";
import Status from "../../commons/models/Status";
import { SUCCESS, FAILURE, BASE_URL } from "../../commons/constants";
import MapData from "../../commons/models/MapData";
import {
  MarginRuleData,
  MarginRuleKey,
  ApiResponse,
  AgreementData,
} from "../models/ServiceDataTypes";
import { FlatRuleData } from "../models/UIDataTypes";
import {
  getConflictingRules,
  saveMarginRule,
  updateMarginRule,
  bulkUpdateMarginRules,
  getAllAgreementData,
  getMarginCalculatorData,
  getAllocatorData
} from "../api";
import RootStore from "./RootStore";
import {
  getDialogDataForMarginRule,
  isValidInputData,
  getMarginRuleFromDialogData,
  getFlatConflictingRules,
  getFlatMarginRuleData,
  isValidConflictsInputData,
  resetPositionDetailData,
} from "../utils/modifyRulesUtil";
import {
  OPERATIONS,
  DIALOG_HEADERS,
  MESSAGES,
  STATUS_HEADERS,
  TOAST_TYPE,
  INITIAL_MODIFY_RULE_STORE_DATA,
  INITIAL_SEARCH_RULE_STORE_DATA,
  POSITION_RULE_TYPE,
  PORTFOLIO_RULE_TYPE,
  POSITION_MASTER_CALCULATOR,
  DEFAULT_MARGIN_ALLOCATOR,
  USAGE_TYPES_REF_DATA,
  RULE_TYPES_REF_DATA,
  POSITION_RULE_TYPES_REF_DATA,
} from "../constants";
import { getParsedDate } from "../utils/commonUtil";
import { getMarginRuleKey, showToastService } from "../utils/commonUtil";
import SearchRulesStore from "./SearchRulesStore";

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};
let clientName = CODEX_PROPERTIES["codex.client_name"];
export class ModifyRulesStore {
  rootStore: RootStore;
  searchRulesStore: SearchRulesStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    this.searchRulesStore = this.rootStore.searchRulesStore;
  }

  @observable
  isOpenConfirmationDialog: boolean = false;

  @observable
  buttonsData: ButtonsData =
    INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_BUTTONS_DATA;

  @observable
  setDialogWidth: boolean = true;

  @observable
  dialogTitle: string = DIALOG_HEADERS.ADD_RULE;

  @observable
  showModifyRuleDialog: boolean = false;

  @observable
  showConflictingRules: boolean = false;

  @observable
  status: Status = INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_STATUS;

  @observable
  statusTitle: string = STATUS_HEADERS.SAVE_STATUS;

  @observable
  showStatus: boolean = false;

  @observable
  inProgress: boolean = false;

  @observable
  populateComment: boolean = false;

  @observable
  flatMarginRuleData: FlatRuleData =
    INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_FLAT_CONFLICTING_RULE;

  @observable
  conflictingRulesData: MarginRuleData[] = [];

  @observable
  updatedConflictingRuleIndexes: number[] = [];

  @observable
  agreementData: AgreementData[] = [];

  @observable
  marginAllocatorData: MapData[] = [];

  @observable
  flatConflictingRulesData: FlatRuleData[] = [];

  @observable
  popUpSelectedData: PopUpSelectedData =
    INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_POP_UP_SELECTED_DATA;

  @observable
  allocatorRefData: MapData[] = [];

  @observable
  apiResponse: ApiResponse =
    INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_API_RESPONSE;

  @observable
  operation: string = OPERATIONS.ADD_RULE;

  @observable
  updateRuleId: number = -1;

  @observable
  positionMaster: MapData | undefined = { key: 0, value: "" };

  @observable
  defaultAllocator: MapData | undefined = { key: 0, value: "" }

  @observable
  marginRuleKey: MarginRuleKey = {
    "@CLASS": "arcesium.treasury.margin.common.model.MarginRuleKey",
  };

  @observable
  calculatorData: MapData[] = [];

  @observable
  disablePortfolioLevelRule: boolean = false;

  @action.bound
  onReset() {
    this.isOpenConfirmationDialog = false;
    this.showConflictingRules = false;
    this.setDialogWidth = true;
    this.conflictingRulesData = [];
    this.updatedConflictingRuleIndexes = [];
    this.flatConflictingRulesData = [];
    this.popUpSelectedData =
      INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_POP_UP_SELECTED_DATA;
    this.buttonsData = INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_BUTTONS_DATA;
    this.operation = OPERATIONS.ADD_RULE;
    this.updateRuleId = -1;
  }

  @action.bound
  prefetchRefData() {
    if (this.agreementData.length == 0 || this.calculatorData.length == 0) {
      this.getReferenceData();
    }
  }

  @action.bound
  onResetFilters() {
    this.popUpSelectedData =
      INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_POP_UP_SELECTED_DATA;
  }

  @action.bound
  onSelect({ key, value }: any) {
    if (key === "selectedRuleTypes" && value && value.value) {
      this.changePopUpSelectedDataOnRuleType(key, value.value);
    }

    this.popUpSelectedData[key] = value;
  }

  @action.bound
  changePopUpSelectedDataOnRuleType(key: string, value: string) {
    let prevSelectedRuleType = this.popUpSelectedData[key] ? this.popUpSelectedData[key].value.toLowerCase() : "";
    let currSelectedRuleType = value.toLowerCase();

    if ((prevSelectedRuleType === PORTFOLIO_RULE_TYPE.toLowerCase() || prevSelectedRuleType === "")
      && currSelectedRuleType === POSITION_RULE_TYPE.toLowerCase()) {
      if ((clientName !== 'desco' && clientName !== 'deshaw') || this.popUpSelectedData["selectedAllocators"] === null) {
        this.popUpSelectedData["selectedAllocators"] = this.defaultAllocator ? this.defaultAllocator : null;
      }
      this.popUpSelectedData["selectedMarginCalculators"] = this.positionMaster ? this.positionMaster : null;
      this.popUpSelectedData["selectedUsageTypes"] = null;
      resetPositionDetailData(this.popUpSelectedData);
    } else if ((prevSelectedRuleType === POSITION_RULE_TYPE.toLowerCase() || prevSelectedRuleType === "")
      && currSelectedRuleType === PORTFOLIO_RULE_TYPE.toLowerCase()) {
      this.popUpSelectedData["selectedMarginCalculators"] = null;
      resetPositionDetailData(this.popUpSelectedData);
    }
  }

  @action.bound
  onChangeConflictingRulesDate({ key, value }: any) {
    this.updatedConflictingRuleIndexes.push(key);
    this.conflictingRulesData[key].effectiveEndDate = value;
    let isAllRulesUpdated = this.checkChangeInConflictingRules(this.updatedConflictingRuleIndexes, this.conflictingRulesData)
    let haveConflicts = true;
    if (isAllRulesUpdated == true) {
      this.setButtonsData(true, false, !haveConflicts, false, haveConflicts);
    }
  }

  checkChangeInConflictingRules = (updatedConflictingRuleIndexes: number[], conflictingRulesData: MarginRuleData[]) => {

    for (let rulesIter = 0; rulesIter < conflictingRulesData.length; rulesIter++) {
      let ruleChanged = false;
      for (let updatedRulesIter = 0; updatedRulesIter < updatedConflictingRuleIndexes.length && ruleChanged == false; updatedRulesIter++) {

        if (updatedConflictingRuleIndexes[updatedRulesIter] == rulesIter) {
          ruleChanged = true;
        }

      }
      if (ruleChanged == false) {
        return false;
      }
    }
    return true;
  };

  @action.bound
  onChangeConflictingRulesComments({ key, value }: any) {
    this.conflictingRulesData[key].userComments = value;
  }

  @action.bound
  operateOnRule(operation: string, ruleId: number, marginTypeId: number) {
    let marginRule = this.searchRulesStore.marginRulesData.find(
      (rule) => rule.ruleId == ruleId && rule.marginType.id == marginTypeId
    );
    let dialogTitle: string;
    if (operation == OPERATIONS.UPDATE_RULE) {
      this.operation = OPERATIONS.UPDATE_RULE;
      dialogTitle = DIALOG_HEADERS.UPDATE_RULE;
      if (marginRule) {
        this.updateRuleId = ruleId;
        this.marginRuleKey = getMarginRuleKey(marginRule);
      }
    } else {
      this.operation = OPERATIONS.CLONE_RULE;
      dialogTitle = DIALOG_HEADERS.CLONE_RULE;
    }
    if (marginRule) {
      this.popUpSelectedData = getDialogDataForMarginRule(marginRule);
      this.toggleShowDialog(dialogTitle);
    } else {
      showToastService(MESSAGES.GENERIC_ERROR_MESSAGE, TOAST_TYPE.CRITICAL);
    }
  }

  @action.bound
  toggleShowDialog(dialogTitle?: string) {
    if (this.showModifyRuleDialog) {
      this.onReset();
    }
    if (dialogTitle) {
      this.dialogTitle = dialogTitle;
    }
    this.showModifyRuleDialog = !this.showModifyRuleDialog;
  }

  @action.bound
  setButtonsData(
    showBack: boolean,
    showReset: boolean,
    showSave: boolean,
    showValidate: boolean,
    showUpdateConflicts: boolean
  ) {
    this.buttonsData = {
      showBack: showBack,
      showReset: showReset,
      showSave: showSave,
      showValidate: showValidate,
      showUpdateConflicts: showUpdateConflicts,
    };
  }

  @action.bound
  onValidate() {
    if (isValidInputData(this.popUpSelectedData, this.agreementData)) {
      if (
        this.popUpSelectedData.selectedRuleTypes?.value.toLowerCase() != POSITION_RULE_TYPE.toLowerCase()
      ) {
        resetPositionDetailData(this.popUpSelectedData);
      }
      let marginRule: MarginRuleData = getMarginRuleFromDialogData(
        this.popUpSelectedData
      );
      if (this.operation == OPERATIONS.UPDATE_RULE) {
        marginRule.ruleId = this.updateRuleId;
      }
      this.fetchConflictingRules(marginRule);
      this.flatMarginRuleData = getFlatMarginRuleData(this.popUpSelectedData);
    }
  }

  @action.bound
  toggleDataOnValidate() {
    this.showConflictingRules = true;
    this.populateComment = false;
    this.setDialogWidth = false;
    this.updatedConflictingRuleIndexes = [];
    let haveConflicts: boolean = this.conflictingRulesData.length
      ? true
      : false;
    this.setButtonsData(true, false, !haveConflicts, false, false);
    if (!haveConflicts) {
      showToastService(MESSAGES.VALIDATE_SUCCESS_MESSAGE, TOAST_TYPE.SUCCESS);
    } else {
      showToastService(MESSAGES.UPDATE_CONFLICTS_MESSAGE, TOAST_TYPE.INFO);
    }
  }

  @action.bound
  onBack() {
    this.isOpenConfirmationDialog = true;
  }

  @action.bound
  onCloseConfirmationDialog() {
    this.isOpenConfirmationDialog = false;
  }

  @action.bound
  onBackConfirmed() {
    this.isOpenConfirmationDialog = false;
    this.showConflictingRules = false;
    this.setDialogWidth = true;
    this.conflictingRulesData = [];
    this.flatConflictingRulesData = [];
    this.setButtonsData(false, true, false, true, false);
  }

  @action.bound
  onSave() {
    if(isValidInputData(this.popUpSelectedData, this.agreementData)) {
      let marginRule: MarginRuleData = getMarginRuleFromDialogData(
        this.popUpSelectedData
      );
      if (this.operation == OPERATIONS.UPDATE_RULE) {
        this.modifyMarginRule(marginRule);
      } else {
        this.addMarginRule(marginRule);
      }
    }
  }

  @action.bound
  postSaveCall() {
    if (this.apiResponse.successStatus == true) {
      this.showModifyRuleDialog = false;
      this.onReset();
      this.setStatus(
        SUCCESS,
        MESSAGES.RULE_SAVED_SUCCESSFULLY,
        STATUS_HEADERS.SAVE_STATUS
      );
      if (
        this.searchRulesStore.searchFilter !=
        INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_SEARCH_FILTER
      ) {
        this.searchRulesStore.handleSearch();
      } else {
        this.searchRulesStore.clearGridData();
      }
    } else {
      if (this.operation == OPERATIONS.UPDATE_RULE) {
        this.setStatus(
          FAILURE,
          MESSAGES.ERROR_WHILE_UPDATING_RULE,
          STATUS_HEADERS.SAVE_STATUS
        );
      } else if (this.operation == OPERATIONS.ADD_RULE) {
        this.setStatus(
          FAILURE,
          MESSAGES.ERROR_WHILE_SAVING_RULE,
          STATUS_HEADERS.SAVE_STATUS
        );
      } else if (this.operation == OPERATIONS.CLONE_RULE) {
        this.setStatus(
          FAILURE,
          MESSAGES.ERROR_WHILE_CLONING_RULE,
          STATUS_HEADERS.SAVE_STATUS
        );
      }
    }
  }

  @action.bound
  onUpdateConflicts() {
    if (isValidConflictsInputData(this.conflictingRulesData)) {
      let marginRuleKeyList: MarginRuleKey[] = [];
      let marginRulesList: MarginRuleData[] = [];
      let areAllDatesSequenced: boolean = true;
      this.conflictingRulesData.forEach((rule) => {
        let ruleStartDate = Date.parse(rule['effectiveStartDate']);
        let ruleEndDate = Date.parse(rule['effectiveEndDate']);
        if (ruleStartDate && ruleEndDate && ruleStartDate > ruleEndDate) {
          showToastService(
            MESSAGES.START_DATE_AND_END_DATE_CONFLICT,
            TOAST_TYPE.CRITICAL
          );
          areAllDatesSequenced = false;
          return;
        }
        marginRuleKeyList.push(getMarginRuleKey(rule));
        marginRulesList.push(rule);
      });

      if (areAllDatesSequenced == true) {
        this.updateConflicts(marginRuleKeyList, marginRulesList);
      }
    }
  }

  @action.bound
  postUpdateConflictsCall() {
    if (this.apiResponse.successStatus) {
      this.flatConflictingRulesData = [];
      this.conflictingRulesData = [];
      this.setButtonsData(true, false, false, true, false);
      showToastService(MESSAGES.UPDATE_CONFLICTS_SUCCESS, TOAST_TYPE.SUCCESS);
      this.searchRulesStore.clearGridData();

    } else {
      showToastService(this.apiResponse.message, TOAST_TYPE.CRITICAL);
    }
  }

  @action.bound
  setStatus(status: string, message: string, title: string) {
    this.status = {
      status: status,
      message: message,
    };
    this.statusTitle = title;
    this.showStatus = true;
  }

  @action.bound
  onCloseStatus() {
    this.showStatus = false;
    this.status = INITIAL_MODIFY_RULE_STORE_DATA.INITIAL_STATUS;
  }

  @action.bound
  formatDatesForConflictingRules() {
    this.conflictingRulesData.forEach((rule) => {
      rule.effectiveStartDate = getParsedDate(rule.effectiveStartDate);
      rule.effectiveEndDate = getParsedDate(rule.effectiveEndDate);
    });
  }

  getRuleTypeDropDownData = () => {
    if (!this.disablePortfolioLevelRule) {
      return RULE_TYPES_REF_DATA;
    } else {
      return POSITION_RULE_TYPES_REF_DATA;
    }
  }

  getReferenceData = async () => {
    try {
      this.inProgress = true;
      this.agreementData = await getAllAgreementData();
      this.calculatorData = await getMarginCalculatorData();
      this.marginAllocatorData = await getAllocatorData();
      let positionMaster = this.calculatorData.find(data => data.value.startsWith(POSITION_MASTER_CALCULATOR));
      this.defaultAllocator = this.marginAllocatorData.find(data => data.value.startsWith(DEFAULT_MARGIN_ALLOCATOR));

      // positionMaster object would be like : {key: 46, value: "PositionMaster [46]"}
      this.positionMaster = positionMaster;

      this.inProgress = false;
    } catch (error) {
      this.inProgress = false;
      this.setStatus(
        FAILURE,
        MESSAGES.ERROR_WHILE_FECTHING_AGREEMENT_DATA,
        STATUS_HEADERS.REF_DATA_STATUS
      );
    }
  };

  fetchConflictingRules = async (marginRule: MarginRuleData) => {
    try {
      this.inProgress = true;
      this.conflictingRulesData = await getConflictingRules(marginRule);
      this.formatDatesForConflictingRules();
      this.flatConflictingRulesData = getFlatConflictingRules(
        this.conflictingRulesData
      );
      this.toggleDataOnValidate();
      this.inProgress = false;
    } catch (error) {
      this.inProgress = false;
      this.setStatus(
        FAILURE,
        MESSAGES.ERROR_WHILE_FECTHING_CONFLICTING_RULES,
        STATUS_HEADERS.VALIDATE_STATUS
      );
    }
  };

  addMarginRule = async (marginRule: MarginRuleData) => {
    try {
      this.inProgress = true;
      this.apiResponse = await saveMarginRule(marginRule);
      this.postSaveCall();
      this.inProgress = false;
    } catch (error) {
      this.inProgress = false;
      this.setStatus(
        FAILURE,
        MESSAGES.ERROR_WHILE_SAVING_RULE,
        STATUS_HEADERS.SAVE_STATUS
      );
    }
  };

  modifyMarginRule = async (marginRule: MarginRuleData) => {
    try {
      this.inProgress = true;
      this.apiResponse = await updateMarginRule(this.marginRuleKey, marginRule);
      this.postSaveCall();
      this.inProgress = false;
    } catch (error) {
      this.inProgress = false;
      this.setStatus(
        FAILURE,
        MESSAGES.ERROR_WHILE_UPDATING_RULE,
        STATUS_HEADERS.UPDATE_STATUS
      );
    }
  };

  updateConflicts = async (
    marginRuleKeyList: MarginRuleKey[],
    marginRulesList: MarginRuleData[]
  ) => {
    try {
      this.inProgress = true;
      this.apiResponse = await bulkUpdateMarginRules(
        marginRuleKeyList,
        marginRulesList
      );
      this.postUpdateConflictsCall();
      this.inProgress = false;
    } catch (error) {
      this.inProgress = false;
      this.setStatus(
        FAILURE,
        MESSAGES.ERROR_WHILE_UPDATING_CONFLICTS,
        STATUS_HEADERS.UPDATE_CONFLICTS_STATUS
      );
    }
  };
}

export default ModifyRulesStore;
