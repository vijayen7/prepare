import SearchRulesStore from "./SearchRulesStore";
import ModifyRulesStore from "./ModifyRulesStore";

export class RootStore {
  searchRulesStore: SearchRulesStore;
  modifyRulesStore: ModifyRulesStore;
  constructor() {
    this.searchRulesStore = new SearchRulesStore(this);
    this.modifyRulesStore = new ModifyRulesStore(this);
  }
}

export default RootStore;
