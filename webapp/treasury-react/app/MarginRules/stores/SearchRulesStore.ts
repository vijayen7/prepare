import { action, observable } from "mobx";
import Status from "../../commons/models/Status";
import { SearchFilter, GridData } from "../models/UIDataTypes";
import MapData from "../../commons/models/MapData";
import { generateRulesReportFromAPI } from "../utils/commonUtil"

import {
  MarginRuleFilter,
  MarginRuleData,
  MarginRuleKey,
  ApiResponse,
} from "../models/ServiceDataTypes";
import { getMarginRules, deleteMarginRule } from "../api";
import { SUCCESS } from "../../commons/constants";
import {
  MESSAGES,
  TOAST_TYPE,
  INITIAL_SEARCH_RULE_STORE_DATA,
  ALL_GRID_COLUMNS,
  INITIAL_DISPLAY_COLUMNS,
} from "../constants";
import RootStore from "./RootStore";
import {
  isValidInputData,
  getMarginRuleFilter,
  getGridDataFromMarginRulesData,
} from "../utils/searchRulesUtil";
import { getMarginRuleKey, showToastService } from "../utils/commonUtil";

export class SearchRulesStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }

  @observable
  isOpenDeleteRuleDialog: boolean = false;

  @observable
  deleteRuleId: number = -1;

  @observable
  marginTypeId: number = -1;

  @observable
  inProgress: boolean = false;

  @observable
  collapseSideBar = false;

  @observable
  searchStatus: Status = INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_SEARCH_STATUS;

  @observable
  searchFilter: SearchFilter =
    INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_SEARCH_FILTER;

  @observable
  marginRulesData: MarginRuleData[] = [];

  @observable
  marginRulesGridData: GridData[] = [];

  @observable
  selectedLegalEntityData: MapData[] = [];

  @observable
  selectedBookData: MapData[] = [];

  @observable
  deleteRuleComment: string = "";

  @observable
  marginRuleKey: MarginRuleKey = {
    "@CLASS": "arcesium.treasury.margin.common.model.MarginRuleKey",
  };

  @observable
  apiResponse: ApiResponse =
    INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_API_RESPONSE;

  @observable
  displayColumns: string[] = INITIAL_DISPLAY_COLUMNS;

  @action.bound
  onDisplayColumnsChange(newDisplayColumns) {
    let newDisplayColumnSet = new Set(newDisplayColumns);
    let newDisplayOrderedColumnSet = ALL_GRID_COLUMNS.filter((e) =>
      newDisplayColumnSet.has(e)
    );
    this.displayColumns = newDisplayOrderedColumnSet;
  }

  @action.bound
  onCloseDeleteRuleDialog() {
    this.isOpenDeleteRuleDialog = false;
    this.deleteRuleComment = "";
  }

  @action.bound
  setExpandedGlobalSelections(expandedGlobalSelection: any) {
    var bookDataList = expandedGlobalSelection['books'];
    var legalEntityDataList = expandedGlobalSelection['legalEntities'];
    var bookDataMap: MapData[] = [];
    var legalEntityDataMap: MapData[] = [];

    for(var iter = 0; iter < legalEntityDataList.length; iter++) {
      var legalEntityData = legalEntityDataList[iter];
      var legalEntityId = legalEntityData['id'];
      var legalEntityName = legalEntityData['name'].concat("[", legalEntityId, "]");
      legalEntityDataMap.push({key: legalEntityId, value: legalEntityName});
    }

    for(var iter = 0; iter < bookDataList.length; iter++) {
      var bookData = bookDataList[iter];
      bookDataMap.push({key: bookData['id'], value: bookData['name']});
    }

    this.selectedLegalEntityData = legalEntityDataMap;
    this.selectedBookData = bookDataMap;
    this.searchFilter.selectedLegalEntities = [];
  }

  @action.bound
  applySavedFilters = (savedFilters: SearchFilter) => {
    this.searchFilter = savedFilters;
  };

  @action.bound
  setCollapsed = (collapsed: boolean) => {
    this.collapseSideBar = collapsed;
  };

  @action.bound
  setSearchStatus(status: string, message: string) {
    this.searchStatus = {
      status: status,
      message: message,
    };
  }

  @action.bound
  onSelect({ key, value }: any) {
    this.searchFilter[key] = value;
  }

  @action.bound
  onChangeDeleteRuleComment({ key, value }: any) {
    this.deleteRuleComment = value;
  }

  @action.bound
  clearGridData() {
    this.marginRulesData = [];
    this.marginRulesGridData = [];
  }

  @action.bound
  handleSearch() {
    if (this.searchFilter.selectedLegalEntities.length === 0) {
      this.searchFilter.selectedLegalEntities = this.selectedLegalEntityData;
    }
    if (
      isValidInputData(
        this.searchFilter.selectedPnlSpns,
        this.searchFilter.selectedDate
      )
    ) {
      this.clearGridData();
      this.setCollapsed(true);
      this.fetchMarginRules(getMarginRuleFilter(this.searchFilter));
    }
  }

  @action.bound
  handleReset() {
    this.searchFilter = INITIAL_SEARCH_RULE_STORE_DATA.INITIAL_SEARCH_FILTER;
  }

  @action.bound
  setMarginRulesGridData() {
    if (!this.marginRulesData.length) {
      this.setSearchStatus(SUCCESS, MESSAGES.NO_MARGIN_RULES_MESSAGE);
      this.marginRulesGridData = [];
    } else {
      this.marginRulesGridData = getGridDataFromMarginRulesData(
        this.marginRulesData
      );
    }
  }

  @action.bound
  onDeleteRule(ruleId: number, marginTypeId: number) {
    this.deleteRuleId = ruleId;
    this.marginTypeId = marginTypeId;
    this.isOpenDeleteRuleDialog = true;
  }

  @action.bound
  onConfirmDelete() {
    if (this.deleteRuleComment == "") {
      showToastService(MESSAGES.DELETE_RULE_COMMENT, TOAST_TYPE.INFO);
      return;
    }
    let marginRule = this.marginRulesData.find(
      (rule) =>
        rule.ruleId == this.deleteRuleId &&
        rule.marginType.id == this.marginTypeId
    );
    if (marginRule) {
      this.marginRuleKey = getMarginRuleKey(marginRule);
      this.removeMarginRule();
      this.isOpenDeleteRuleDialog = false;
      this.deleteRuleComment = "";
    } else {
      showToastService(MESSAGES.GENERIC_ERROR_MESSAGE, TOAST_TYPE.CRITICAL);
    }
  }

  @action.bound
  postDeleteCall() {
    if (this.apiResponse.successStatus) {
      this.marginRulesData = this.marginRulesData.filter(
        (x) =>
          x.ruleId != this.deleteRuleId || x.marginType.id != this.marginTypeId
      );
      this.marginRulesGridData = this.marginRulesGridData.filter(
        (x) =>
          x.marginRuleId != this.deleteRuleId ||
          x.marginTypeId != this.marginTypeId
      );
      showToastService(
        MESSAGES.DELETE_RULE_SUCCESS_MESSAGE,
        TOAST_TYPE.SUCCESS
      );
    } else {
      showToastService(this.apiResponse.message, TOAST_TYPE.CRITICAL);
    }
  }

  @action.bound
  generateReportFromAPI() {
    generateRulesReportFromAPI(this.searchFilter);
  }

  fetchMarginRules = async (params: MarginRuleFilter) => {
    try {
      this.inProgress = true;
      this.marginRulesData = await getMarginRules(params);
      this.setMarginRulesGridData();
      this.inProgress = false;
    } catch (error) {
      showToastService(
        MESSAGES.SEARCH_RULES_FAILURE_MESSAGE,
        TOAST_TYPE.CRITICAL
      );
      this.inProgress = false;
    }
  };

  removeMarginRule = async () => {
    try {
      this.inProgress = true;
      this.apiResponse = await deleteMarginRule(
        this.marginRuleKey,
        this.deleteRuleComment
      );
      this.postDeleteCall();
      this.inProgress = false;
    } catch (error) {
      showToastService(
        MESSAGES.DELETE_RULE_FAILURE_MESSAGE,
        TOAST_TYPE.CRITICAL
      );
      this.inProgress = false;
    }
  };
}

export default SearchRulesStore;
