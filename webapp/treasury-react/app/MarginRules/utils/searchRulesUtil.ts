import { getSelectedIdList } from "../../commons/util";
import { MarginRuleFilter, MarginRuleData } from "../models/ServiceDataTypes";
import { SearchFilter, GridData } from "../models/UIDataTypes";
import { MESSAGES, TOAST_TYPE } from "../constants";
import {
  showToastService,
  getAbbrevFromRefData,
  getNameFromRefData,
  getParsedDate,
} from "./commonUtil";

const getParsedPnlSpns = (pnlSpn: string) => {
  let pnlSpns: string[] = pnlSpn.split(";");
  let parsedPnlSpns: number[] = [];
  pnlSpns.forEach((pnlSpn) => {
    if (pnlSpn != "") {
      parsedPnlSpns.push(parseInt(pnlSpn));
    }
  });
  return parsedPnlSpns;
};

export const isValidInputData = (pnlSpn: string, date: string) => {
  let pnlSpns: string[] = pnlSpn.split(";");
  let returnVal: boolean = true;
  if (date == "") {
    showToastService(MESSAGES.INVALID_DATE_INPUT, TOAST_TYPE.CRITICAL);
    returnVal = false;
  }
  pnlSpns.forEach((spn) => {
    if (spn != "" && isNaN(+spn)) {
      returnVal = false;
    }
  });
  if (!returnVal) {
    showToastService(MESSAGES.INVALID_PNL_SPN_MESSAGE, TOAST_TYPE.CRITICAL);
  }
  return returnVal;
};

export const getMarginRuleFilter = (searchFilter: SearchFilter) => {
  let marginRuleFilter: MarginRuleFilter = {
    applicableOn: searchFilter.selectedDate,
    legalEntityIds: getSelectedIdList(searchFilter.selectedLegalEntities),
    counterPartyIds: getSelectedIdList(searchFilter.selectedCpes),
    agreementTypeIds: getSelectedIdList(searchFilter.selectedAgreementTypes),
    marginTypes: getSelectedIdList(searchFilter.selectedCalculatorTypes),
    calculatorIds: getSelectedIdList(searchFilter.selectedMarginCalculators),
    ruleTypes: getSelectedIdList(searchFilter.selectedRuleTypes),
    usageTypes: getSelectedIdList(searchFilter.selectedUsageTypes),
    gboTypeIds: getSelectedIdList(searchFilter.selectedGboTypes),
    underlyingGboTypeIds: getSelectedIdList(
      searchFilter.selectedUnderlyingGboTypes
    ),
    pnlSpns: [],
    showInvalidRules: searchFilter.showInvalidRules,
    "@CLASS": "arcesium.treasury.margin.common.model.MarginRuleFilter",
  };
  if (searchFilter.selectedPnlSpns != "") {
    marginRuleFilter.pnlSpns = getParsedPnlSpns(searchFilter.selectedPnlSpns);
  }
  return marginRuleFilter;
};

export const getGridDataFromMarginRulesData = (
  marginRulesData: MarginRuleData[]
) => {
  let gridDataList: GridData[] = [];
  marginRulesData.forEach((rule) => {
    let gridData: GridData = {
      marginRuleId: rule.ruleId,
      marginTypeId: rule.marginType.id,
      legalEntity: getNameFromRefData(rule.legalEntity),
      counterPartyEntity: getNameFromRefData(rule.counterPartyEntity),
      agreementType: getNameFromRefData(rule.agreementType),
      marginType: getNameFromRefData(rule.marginType),
      ruleType: getNameFromRefData(rule.ruleType),
      calculator: getNameFromRefData(rule.calculator),
      pnlSpn: rule?.pnlSpn ? rule.pnlSpn : "n/a",
      custodianAccount: getNameFromRefData(rule.custodianAccount),
      book: getNameFromRefData(rule.book),
      currency: getAbbrevFromRefData(rule.currency),
      market: getNameFromRefData(rule.market),
      country: getNameFromRefData(rule.country),
      fieldType: getNameFromRefData(rule.fieldType),
      gboType: getNameFromRefData(rule.gboType),
      underlyingGboType: getNameFromRefData(rule.underlyingGboType),
      direction: getNameFromRefData(rule.direction),
      allocator: getNameFromRefData(rule.allocator),
      effectiveStartDate: rule.effectiveStartDate
        ? getParsedDate(rule.effectiveStartDate)
        : "n/a",
      effectiveEndDate: rule.effectiveEndDate
        ? getParsedDate(rule.effectiveEndDate)
        : "n/a",
      usageType: getNameFromRefData(rule.usageType),
      usageHaircut: rule?.usageHaircut != undefined ? rule.usageHaircut : "n/a",
      usageCurrency: getAbbrevFromRefData(rule.usageCurrency),
      user: rule?.user ? rule.user : "n/a",
      userComments: rule.userComments,
      isValid: rule.isValid == true ? "Yes" : "No"
    };
    gridDataList.push(gridData);
  });
  return gridDataList;
};
