import ReferenceData from "../../commons/models/ReferenceData";
import { MarginRuleData, AgreementData } from "../models/ServiceDataTypes";
import MapData from "../../commons/models/MapData";
import { PopUpSelectedData, FlatRuleData } from "../models/UIDataTypes";
import { MESSAGES, TOAST_TYPE, POSITION_RULE_TYPE } from "../constants";
import {
  showToastService,
  getNameFromRefData,
  getAbbrevFromRefData,
  getParsedDate,
} from "./commonUtil";

const getIdFromMapData = (data: MapData | null | undefined) => {
  return data ? data.key : undefined;
};

const getNameFromMapData = (data: MapData | null | undefined) => {
  return data ? data.value : "n/a";
};

const getMapData = (data?: ReferenceData) => {
  return data && data.id != undefined && data.name
    ? { key: data.id, value: data.name }
    : null;
};

const getMapDataWithAbbrev = (data?: ReferenceData) => {
  return data && data.id != undefined && data.abbrev
    ? { key: data.id, value: data.abbrev }
    : null;
};

const getSlicedInput = (input: string) => {
  return input.slice(-1) == ";" ? input.slice(0, -1) : input;
};

const getPnlSpnFromInput = (input: string) => {
  if (input == "") {
    return undefined;
  }
  return parseInt(getSlicedInput(input));
};

const getPnlSpnValForFlatMarginRule = (input: string) => {
  if (input == "") {
    return "n/a";
  }
  return parseInt(getSlicedInput(input));
};

export const getDialogDataForMarginRule = (rule: MarginRuleData) => {
  let dialogData: PopUpSelectedData = {
    selectedStartDate: getParsedDate(rule.effectiveStartDate),
    selectedEndDate: getParsedDate(rule.effectiveEndDate),
    selectedLegalEntities: getMapData(rule.legalEntity),
    selectedCpes: getMapData(rule.counterPartyEntity),
    selectedAgreementTypes: getMapData(rule.agreementType),
    selectedGboTypes: getMapData(rule.gboType),
    selectedUnderlyingGboTypes: getMapData(rule.underlyingGboType),
    selectedCustodianAccounts: getMapData(rule.custodianAccount),
    selectedCurrencies: getMapDataWithAbbrev(rule.currency),
    selectedDirections: getMapData(rule.direction),
    selectedRuleTypes: getMapData(rule.ruleType),
    selectedFieldTypes: getMapData(rule.fieldType),
    selectedMarginTypes: getMapData(rule.marginType),
    selectedMarginCalculators: getMapData(rule.calculator),
    selectedAllocators: getMapData(rule.allocator),
    selectedBooks: getMapData(rule.book),
    selectedCountries: getMapData(rule.country),
    selectedMarkets: getMapData(rule.market),
    selectedPnlSpn: rule.pnlSpn ? rule.pnlSpn.toString() : "",
    selectedUsageTypes: getMapData(rule.usageType),
    selectedUsageCurrency: getMapDataWithAbbrev(rule.usageCurrency),
    selectedUsageHaircut:
      rule.usageHaircut != undefined ? rule.usageHaircut.toString() : "",
    selectedComments: "",
  };
  return dialogData;
};

const isValidAgreement = (agreementDataList: AgreementData[], dialogData: PopUpSelectedData) => {

  let selectedAgreementType = dialogData.selectedAgreementTypes;
  let selectedLegalEntity = dialogData.selectedLegalEntities;
  let selectedCounterPartyEntity = dialogData.selectedCpes;

  for(let index=0 ; index<agreementDataList.length; index++){

    if((agreementDataList[index].dESCOEntityId == selectedLegalEntity?.key) &&
    (agreementDataList[index].cpeId == selectedCounterPartyEntity?.key) &&
    (agreementDataList[index].agreementTypeId == selectedAgreementType?.key)) {
      return true;
    }
  }

  showToastService(MESSAGES.INVALID_AGREEMENT, TOAST_TYPE.CRITICAL);
  return false;
}

export const isValidInputData = (dialogData: PopUpSelectedData, agreementDataList: AgreementData[]) => {
  let returnVal = true;
  if (dialogData.selectedStartDate == "" || dialogData.selectedEndDate == "") {
    showToastService(
      MESSAGES.INVALID_START_DATE_AND_END_DATE,
      TOAST_TYPE.CRITICAL
    );
    return false;
  }
  let selectedStartDate = Date.parse(dialogData.selectedStartDate);
  let selectedEndDate = Date.parse(dialogData.selectedEndDate);

  if (selectedStartDate && selectedEndDate && selectedEndDate < selectedStartDate) {
    showToastService(
      MESSAGES.START_DATE_AND_END_DATE_CONFLICT,
      TOAST_TYPE.CRITICAL
    );
    return false;
  }

  if (
    !dialogData.selectedLegalEntities ||
    !dialogData.selectedCpes ||
    !dialogData.selectedAgreementTypes ||
    !dialogData.selectedRuleTypes ||
    !dialogData.selectedMarginTypes ||
    !dialogData.selectedMarginCalculators ||
    !dialogData.selectedAllocators ||
    dialogData.selectedComments == "" ||
    (dialogData.selectedRuleTypes?.value.toLowerCase() == POSITION_RULE_TYPE.toLowerCase() &&
      (!dialogData.selectedUsageTypes ||
        !dialogData.selectedUsageCurrency ||
        dialogData.selectedUsageHaircut == "" ||
        dialogData.selectedPnlSpn == "" ||
        !dialogData.selectedCustodianAccounts ||
        !dialogData.selectedFieldTypes ||
        !dialogData.selectedBooks))
  ) {
    showToastService(MESSAGES.FILL_REQUIRED_INPUT_MESSAGE, TOAST_TYPE.CRITICAL);
    return false;
  }

  if(!isValidAgreement(agreementDataList, dialogData)) {
    return false
  }
  if (dialogData.selectedRuleTypes?.value.toLowerCase() == POSITION_RULE_TYPE.toLowerCase()) {
    let pnlSpns: string[] = dialogData.selectedPnlSpn.split(";");
    let validPnlSpns: string[] = pnlSpns.filter((pnlSpn) => pnlSpn != "");
    if (validPnlSpns.length != 1 || isNaN(+validPnlSpns[0])) {
      showToastService(
        MESSAGES.INVALID_PNL_SPN_WHILE_ADDING,
        TOAST_TYPE.CRITICAL
      );
      return false;
    } else if (isNaN(+dialogData.selectedUsageHaircut)) {
      showToastService(MESSAGES.INVALID_USAGE_HARICUT, TOAST_TYPE.CRITICAL);
      return false;
    }
  }
  return returnVal;
};

export const resetPositionDetailData = (dialogData: PopUpSelectedData) => {
  dialogData.selectedPnlSpn = "";
  dialogData.selectedUsageTypes = null;
  dialogData.selectedUsageCurrency = null;
  dialogData.selectedUsageHaircut = "";
  dialogData.selectedUsername = "";
};

export const isValidConflictsInputData = (
  conflictingRules: MarginRuleData[]
) => {
  let returnVal = true;
  conflictingRules.forEach((rule) => {
    if (rule.effectiveEndDate == "") {
      showToastService(
        MESSAGES.FILL_END_DATES_FOR_CONFLICTING_RULES,
        TOAST_TYPE.CRITICAL
      );
      returnVal = false;
      return;
    } else if (rule.userComments == "") {
      showToastService(
        MESSAGES.FILL_COMMENTS_FOR_CONFLICTING_RULES,
        TOAST_TYPE.CRITICAL
      );
      returnVal = false;
      return;
    }
  });
  return returnVal;
};

export const getFlatMarginRuleData = (dialogData: PopUpSelectedData) => {
  let flatMarginRuleData: FlatRuleData = {
    effectiveStartDate: dialogData.selectedStartDate,
    effectiveEndDate: dialogData.selectedEndDate,
    legalEntity: getNameFromMapData(dialogData.selectedLegalEntities),
    exposureCounterparty: getNameFromMapData(dialogData.selectedCpes),
    agreementType: getNameFromMapData(dialogData.selectedAgreementTypes),
    gboType: getNameFromMapData(dialogData.selectedGboTypes),
    underlyingGboType: getNameFromMapData(
      dialogData.selectedUnderlyingGboTypes
    ),
    custodianAccount: getNameFromMapData(dialogData.selectedCustodianAccounts),
    currency: getNameFromMapData(dialogData.selectedCurrencies),
    direction: getNameFromMapData(dialogData.selectedDirections),
    ruleType: getNameFromMapData(dialogData.selectedRuleTypes),
    fieldType: getNameFromMapData(dialogData.selectedFieldTypes),
    marginType: getNameFromMapData(dialogData.selectedMarginTypes),
    marginCalculator: getNameFromMapData(dialogData.selectedMarginCalculators),
    book: getNameFromMapData(dialogData.selectedBooks),
    allocator: getNameFromMapData(dialogData.selectedAllocators),
    country: getNameFromMapData(dialogData.selectedCountries),
    market: getNameFromMapData(dialogData.selectedMarkets),
    pnlSpn: getPnlSpnValForFlatMarginRule(dialogData.selectedPnlSpn),
    usageType: getNameFromMapData(dialogData.selectedUsageTypes),
    usageCurrency: getNameFromMapData(dialogData.selectedUsageCurrency),
    usageHaircut: dialogData.selectedUsageHaircut,
    comments: dialogData.selectedComments,
  };
  return flatMarginRuleData;
};

export const getMarginRuleFromDialogData = (dialogData: PopUpSelectedData) => {
  let marginRuleData: MarginRuleData = {
    legalEntity: {
      id: getIdFromMapData(dialogData.selectedLegalEntities),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    counterPartyEntity: {
      id: getIdFromMapData(dialogData.selectedCpes),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    agreementType: {
      id: getIdFromMapData(dialogData.selectedAgreementTypes),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    marginType: {
      id: getIdFromMapData(dialogData.selectedMarginTypes),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    ruleType: {
      id: getIdFromMapData(dialogData.selectedRuleTypes),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    calculator: {
      id: getIdFromMapData(dialogData.selectedMarginCalculators),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    allocator: {
      id: getIdFromMapData(dialogData.selectedAllocators),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    pnlSpn: getPnlSpnFromInput(dialogData.selectedPnlSpn),
    custodianAccount: {
      id: getIdFromMapData(dialogData.selectedCustodianAccounts),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    book: {
      id: getIdFromMapData(dialogData.selectedBooks),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    currency: {
      id: getIdFromMapData(dialogData.selectedCurrencies),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    market: {
      id: getIdFromMapData(dialogData.selectedMarkets),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    country: {
      id: getIdFromMapData(dialogData.selectedCountries),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    fieldType: {
      id: getIdFromMapData(dialogData.selectedFieldTypes),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    gboType: {
      id: getIdFromMapData(dialogData.selectedGboTypes),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    underlyingGboType: {
      id: getIdFromMapData(dialogData.selectedUnderlyingGboTypes),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    direction: {
      id: getIdFromMapData(dialogData.selectedDirections),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    effectiveStartDate: dialogData.selectedStartDate,
    effectiveEndDate: dialogData.selectedEndDate,
    usageType: {
      id: getIdFromMapData(dialogData.selectedUsageTypes),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    usageHaircut:
      dialogData.selectedUsageHaircut != ""
        ? parseFloat(dialogData.selectedUsageHaircut)
        : undefined,
    usageCurrency: {
      id: getIdFromMapData(dialogData.selectedUsageCurrency),
      "@CLASS": "arcesium.treasury.model.common.ReferenceData",
    },
    userComments: dialogData.selectedComments
      ? dialogData.selectedComments
      : "",
    "@CLASS": "arcesium.treasury.margin.common.model.EnrichedMarginRule",
  };
  return marginRuleData;
};

export const getFlatConflictingRules = (rules: MarginRuleData[]) => {
  let flatConflictingRules: FlatRuleData[] = [];
  rules.forEach((rule) => {
    let conflictingRule: FlatRuleData = {
      effectiveStartDate: rule.effectiveStartDate,
      effectiveEndDate: rule.effectiveEndDate,
      legalEntity: getNameFromRefData(rule.legalEntity),
      exposureCounterparty: getNameFromRefData(rule.counterPartyEntity),
      agreementType: getNameFromRefData(rule.agreementType),
      gboType: getNameFromRefData(rule.gboType),
      underlyingGboType: getNameFromRefData(rule.underlyingGboType),
      custodianAccount: getNameFromRefData(rule.custodianAccount),
      currency: getAbbrevFromRefData(rule.currency),
      direction: getNameFromRefData(rule.direction),
      ruleType: getNameFromRefData(rule.ruleType),
      fieldType: getNameFromRefData(rule.fieldType),
      marginType: getNameFromRefData(rule.marginType),
      marginCalculator: getNameFromRefData(rule.calculator),
      allocator: getNameFromRefData(rule.allocator),
      book: getNameFromRefData(rule.book),
      country: getNameFromRefData(rule.country),
      market: getNameFromRefData(rule.market),
      pnlSpn: rule?.pnlSpn ? rule.pnlSpn : "n/a",
      usageType: getNameFromRefData(rule.usageType),
      usageCurrency: getAbbrevFromRefData(rule.usageCurrency),
      usageHaircut: rule.usageHaircut ? rule.usageHaircut : "n/a",
      comments: rule.userComments ? rule.userComments : "n/a",
    };
    flatConflictingRules.push(conflictingRule);
  });
  return flatConflictingRules;
};
