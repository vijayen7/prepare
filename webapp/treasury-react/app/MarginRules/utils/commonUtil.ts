import ReferenceData from "../../commons/models/ReferenceData";
import { MarginRuleData, MarginRuleKey } from "../models/ServiceDataTypes";
import { TOAST_TYPE } from "../constants";
import { SearchFilter } from "../models/UIDataTypes";
import { getMarginRuleFilter } from "../utils/searchRulesUtil";
import { ToastService } from "arc-react-components";
import { parseDJSrvString, openReportManager, getReportingUrl } from "../../commons/util";
import SERVICE_URLS from "../../commons/UrlConfigs";
import { MarginRuleFilter } from "../models/ServiceDataTypes";

export const generateRulesReportFromAPI = (searchFilter: SearchFilter) => {
  let apiUrl = getMarginRulesUrl(getMarginRuleFilter(searchFilter));
  openReportManager(apiUrl, null);
};

const getMarginRulesUrl = (marginRuleFilter: MarginRuleFilter) => {
  let url= getReportingUrl(SERVICE_URLS.marginRuleService.searchMarginRules);
  url += `?marginRuleFilter=${encodeURIComponent(
    JSON.stringify(marginRuleFilter))
  }&inputFormat=json&format=json`;
  return url;
};

export const getNameFromRefData = (refData?: ReferenceData) => {
  return refData?.name ? refData.name : "n/a";
};

export const getAbbrevFromRefData = (refData?: ReferenceData) => {
  return refData?.abbrev ? refData.abbrev : "n/a";
};

export const getParsedDate = (date: string) => {
  let parsedDate = parseDJSrvString(date);
  return parsedDate ? parsedDate.toISOString().substring(0, 10) : "n/a";
};

export const getMarginRuleKey = (rule: MarginRuleData) => {
  let marginRuleKey: MarginRuleKey = {
    ruleId: rule.ruleId,
    marginType: rule.marginType.id,
    "@CLASS": "arcesium.treasury.margin.common.model.MarginRuleKey",
  };
  return marginRuleKey;
};

const getToastType = (type: string) => {
  let toastType: any = ToastService.ToastType.SUCCESS;
  switch (type) {
    case TOAST_TYPE.CRITICAL:
      toastType = ToastService.ToastType.CRITICAL;
      break;
    case TOAST_TYPE.SUCCESS:
      toastType = ToastService.ToastType.SUCCESS;
      break;
    case TOAST_TYPE.INFO:
      toastType = ToastService.ToastType.INFO;
      break;
  }
  return toastType;
};

export const showToastService = (content: string, type: string) => {
  ToastService.append({
    content: content,
    type: getToastType(type),
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: 7000,
  });
};
