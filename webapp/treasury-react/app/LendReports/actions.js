import {
   FETCH_LEND_REPORT_DATA, 
   DESTROY_LEND_REPORT_DATA,
   SAVE_LEND_REPORT_DATA_FILTERS,
   FETCH_SECURITY_MARKET_DATA_PNL,
   DESTROY_DATA_FOR_SECURITY_MARKET_DATA,
   RESIZE_CANVAS
  } from "commons/constants";
  
  export function fetchLendReportData(payload) {
    return {
      type: FETCH_LEND_REPORT_DATA,
      payload
    };
  }
    
  export function resizeCanvas() {
    return {
      type: RESIZE_CANVAS
    };
  }
      
  export function destroyLendReportData() {
    return {
      type: DESTROY_LEND_REPORT_DATA
    };
  }
  
  export function saveLendReportFilters(){
    return {
      type: SAVE_LEND_REPORT_DATA_FILTERS
    };
  }
  
  export function fetchSecurityMarketData(payload) {
    return {
      type: FETCH_SECURITY_MARKET_DATA_PNL,
      payload
    };
  }
  
  export function destroySecurityMarketData(payload) {
    return {
      type: DESTROY_DATA_FOR_SECURITY_MARKET_DATA,
      payload
    };
  }