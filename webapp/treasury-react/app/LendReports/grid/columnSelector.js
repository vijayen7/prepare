import {
    gridColumns
  } from "./columnConfig";
  
  export function getDefaultColumns() {
    var defaultColumns = [
      "date",
      "bookName",
      "legalEntity",
      "counterpartyEntity",
      "custodianAccount",
      "spn",
      "securityDescription",          
      "currencyCode",
      "quantity",
      "priceUsd",
      "marketValueUsd",
      "lentQuantity",
      "lendRate",
      "availableQuantity",
      "bestSLFRate",
      "actualImpact",
      "potentialLendIncome",
      "classification",
      "mktDataFeeRate",
      "availabilityFeeRate",
      "negotiation",
      "totalBorrowQuantity",
      "bookBorrowQuantity",
      "avgBorrowRate",
      "maxBorrowRate",
      "borrowLendSpread"
    ];
  
    return defaultColumns;
  }
  
  export function getTotalColumns() {
        return gridColumns().map(column => [column.id, column.name]);
  }
  