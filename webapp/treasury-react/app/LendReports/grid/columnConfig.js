import { convertJavaNYCDate } from "commons/util";
import {
  percentFormatter,
  priceFormatter,
  drillThroughFormatter,
  numberTotalsFormatter,
  percentTotalsFormatter,
  excelTotalsFormatter,
  marketDataPositionTypeFormatter,
  linkFormatter
} from 'commons/grid/formatters';

import {
  textComparator,
  numberComparator
} from 'commons/grid/comparators';


export function gridColumns(params) {
  return [{
    id: 'date',
    name: 'Date',
    field: 'localDate',
    toolTip: 'Date',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 70,
    formatter: function (
      row,
      cell,
      value,
      columnDef,
      dataContext,
      isExportToExcel
    ) {
     return value;
    },
    excelDataFormatter: function (value) {
      return value;
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'bookName',
    name: 'Book',
    field: 'bookName',
    toolTip: 'Book',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 90
  },
  {
    id: 'legalEntity',
    name: 'Legal Entity',
    field: 'legalEntityName',
    toolTip: 'Legal Entity',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80
  },
  {
    id: 'counterpartyEntity',
    name: 'Counterparty Entity',
    field: 'cpeName',
    toolTip: 'Counterparty Entity',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 100
  },
  {
    id: 'custodianAccount',
    name: 'Custodian Account',
    field: 'custodianAccountName',
    toolTip: 'Custodian Account',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 110
  },
  {
    id: 'spn',
    name: 'SPN',
    field: 'spn',
    toolTip: 'SPN',
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b",
    width: 65
  },

  {
    id: 'pnlSpn',
    name: 'PNL SPN',
    field: 'pnlSpn',
    toolTip: 'PNL SPN',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 65
  },
  {
    id: 'securityDescription',
    name: 'Security Description',
    field: 'securityDescription',
    toolTip: 'Security Description',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 120
  },
  {
    id: 'gboTypeName',
    name: 'GBO Type',
    field: 'gboTypeName',
    toolTip: 'GBO TypeName',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 110
  },
  {
    id: 'sedol',
    name: 'Sedol',
    field: 'sedol',
    toolTip: 'sedol',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60
  },
  {
    id: 'ticker',
    name: 'Ticker',
    field: 'ticker',
    toolTip: 'Ticker',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60
  },
  {
    id: 'currencyCode',
    name: 'Currency',
    field: 'currencyCode',
    toolTip: 'Currency',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80
  },
  {
    id: 'quantity',
    name: 'Settle Date Quantity',
    field: 'quantity',
    toolTip: 'Settle Date Quantity',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80
  },
  {
    id: 'priceUsd',
    name: 'Price (USD)',
    field: 'priceUsd',
    toolTip: 'Price (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 60
  },
  {
    id: 'marketValueUsd',
    name: 'Market Value (USD)',
    field: 'marketValueUsd',
    toolTip: 'Market Value (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'lentQuantity',
    name: 'Quantity Currently Lent',
    field: 'lentQuantity',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80

  },
  {
    id: 'lendRate',
    name: 'Current Lending Rate',
    field: 'lendRate',
    toolTip: 'Current Lending Rate',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80
  },
  {
    id: 'availableQuantity',
    name: 'Quantity Available to Lend',
    field: 'availableQuantity',
    toolTip: 'Quantity Available to Lend',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80
  },
  {
    id: 'bestSLFRate',
    name: 'Impact Fee Rate',
    field: 'bestSLFRate',
    toolTip: 'Best SLF Rate',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 70
  },
  {
    id: 'actualImpact',
    name: 'Actual Impact',
    field: 'actualImpact',
    toolTip: 'Actual Impact',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'potentialLendIncome',
    name: 'Impact(USD)',
    field: 'potentialLendIncome',
    toolTip: 'Impact(USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'classification',
    name: 'Classification',
    field: 'classificationName',
    toolTip: 'Classification',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 50
  },
  {
    id: 'mktDataFeeRate',
    name: 'Market Data Fee Rate',
    field: 'mktDataFeeRate',
    type: 'number',
    filter: true,
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'availabilityFeeRate',
    name: 'Availability Fee Rate',
    field: 'availabilityFeeRate',
    type: 'number',
    filter: true,
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: "negotiation",
    name: "Negotiation",
    field: "negotiation",
    toolTip: "Negotiation",
    type: "text",
    filter: true,
    sortable: true,
    formatter: function (row, cell, value, columnDef, dataContext) {
      if (dataContext.pnlSpn !== -1 && dataContext.pnlSpn !== -2)
        return linkFormatter(
          row,
          cell,
          "<i class='icon-link' />",
          columnDef,
          dataContext
        );
      else return null;
    },
    headerCssClass: "b",
    width: 85
  },
  {
    id: 'totalBorrowQuantity',
    name: 'Total Borrow Quantity',
    field: 'totalBorrowQuantity',
    toolTip: 'Total Borrow Quantity',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80
  },
  {
    id: 'bookBorrowQuantity',
    name: 'Book Borrow Quantity',
    field: 'bookBorrowQuantity',
    toolTip: 'Book Borrow Quantity',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80
  },
  {
    id: 'avgBorrowRate',
    name: 'AVG Borrow Rate',
    field: 'avgBorrowRate',
    type: 'number',
    filter: true,
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'maxBorrowRate',
    name: 'MAX Borrow Rate',
    field: 'maxBorrowRate',
    type: 'number',
    filter: true,
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'borrowLendSpread',
    name: 'B/L Spread',
    field: 'borrowLendSpread',
    type: 'number',
    filter: true,
    sortable: true,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'fxRate',
    name: 'FX-RATE',
    field: 'fxRate',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 60
  },
  {
    id: 'isin',
    name: 'ISIN',
    field: 'isin',
    toolTip: 'ISIN',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60
  },
  {
    id: 'marketValue',
    name: 'Market Value',
    field: 'marketValue',
    toolTip: 'Market Value',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'price',
    name: 'Price (LBC)',
    field: 'price',
    toolTip: 'Price (LBC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 60
  },

  ];
}
