import { url } from "../api";
import { getSaveSettingsUrl } from "commons/util";
import {
  getDefaultColumns,
  getTotalColumns
} from "./columnSelector";

export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function () {
        return window.location.origin + url;
      }
    },
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [
      {
        columnId: "date",
        sortAsc: true
      }

    ],
    page: true,
    customColumnSelection: {
      defaultcolumns: getDefaultColumns(),
      totalColumns: getTotalColumns()
    },
    configureColumns: true,
    onRowClick: function () {// highlight on row works if this callback is defined
    },
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    frozenColumn: 9
  };

  return options;
}
