import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Card, Layout } from "arc-react-components";
import DateFilter from "commons/container/DateFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import SidebarWithCopySearchUrl from "commons/components/SidebarWithCopySearchUrl";
import Label from "commons/components/Label";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import GenericSecurityFilter from "commons/components/GenericSecurityFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import BookFilter from "commons/container/BookFilter";
import CpeFilter from "commons/container/CpeFilter";
import ClassificationFilter from "MarketData/components/ClassificationFilter";
import InputFilter from 'commons/components/InputFilter';
import { CLASS_NAMESPACE } from "commons/ClassConfigs";
import { createAPIPayload } from "../util";
import {
  fetchLendReportData,
  destroyLendReportData,
  destroySecurityMarketData,
  saveLendReportFilters,
  resizeCanvas
} from "../actions";
import {
  getPreviousBusinessDay,
  getCommaSeparatedListValue
} from "commons/util";
import { getCommaSeparatedValuesOrEmpty } from "../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  resizeCanvas() {
    this.props.resizeCanvas();
  }

  getDefaultFilters() {
    return {
      securityFilter: {
        selectedSpns: "",
        selectedSecuritySearchType: { key: "TICKER", value: "TICKER" },
        selectedTextSearchType: { key: "EXACT_MATCH", value: "EXACT_MATCH" },
        securitySearchString: "",
        isAdvancedSearch: false
      },
      selectedDate: getPreviousBusinessDay(),
      toggleSidebar: false,
      showInvalidSPNDailog: false,
      actualImpactThreshold: "0.00",
      impactFeeRateThreshold: "0.0"

    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    var payload = {
      "localDate": this.state.selectedDate,
      "ccyIds": this.state.selectedCurrencies,
      "legalEntityIds": this.state.selectedLegalEntities,
      "cpeIds": this.state.selectedCpes,
      "bookIds": this.state.selectedBooks,
      ...(this.state.securityFilter.selectedSpns &&
        this.state.securityFilter.selectedSpns.trim() !== ""
        ? {
          "spnIds":  getCommaSeparatedListValue(
            this.state.securityFilter.selectedSpns)

        }
        : {
          "spnIds": null
        }

      ),
      ...(this.state.securityFilter.isAdvancedSearch &&
        this.state.securityFilter.securitySearchString.trim() !== ""
        ? { "securityFilter" : {
          "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.model.common.security.SecurityFilter,
          "securitySearchType": this.state
            .securityFilter.selectedSecuritySearchType.value,
          "textSearchType": this.state
            .securityFilter.selectedTextSearchType.value,
          "searchStrings":
            this.state.securityFilter.securitySearchString
        }
        } : {
          "securityFilter":null
        }
      ),
      "classifications": this.state.selectedClassification,
      "actualImpactThreshold": this.state.actualImpactThreshold || 0.0,
      "impactFeeRateThreshold": this.state.impactFeeRateThreshold || 0.0
    };

    this.props.saveLendReportFilters(this.state);
    this.props.fetchLendReportData(createAPIPayload(payload));
    this.props.destroySecurityMarketData();
  }

  setFilters = (filters) => {
    this.setState({...this.state,...filters},()=>{
      (Object.keys(filters).length > 0) ? this.handleClick() : null;
    });
  }

  render() {
    return (
      <React.Fragment>
        <SidebarWithCopySearchUrl
          header={"Search Criteria"}
          collapsible={true}
          size="200px"
          resizeCanvas={this.resizeCanvas}
          toggleSidebar={this.state.toggleSidebar}
          handleSearch={this.handleClick}
          handleReset={this.handleReset}
          copySearchUrl
          setFilters={this.setFilters}
          selectedFilters={this.state}
        >
          <Card>
            <DateFilter
              onSelect={this.onSelect}
              stateKey="selectedDate"
              data={this.state.selectedDate}
              label="Date"
            />
          </Card>
          <Card>
            <GenericSecurityFilter
              onSelect={this.onSelect}
              selectedData={this.state.securityFilter}
              stateKey="securityFilter"
            />
          </Card>
          <Card>
            <LegalEntityFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedLegalEntities}
            />
            <CpeFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedCpes}
            />
            <BookFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedBooks}
            />
            <CurrencyFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedCurrencies}
            />
          </Card>
          <Card>
            <ClassificationFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedClassification}
            />
            <InputFilter
              data={this.state.actualImpactThreshold}
              label="Actual Impact Threshold($)"
              onSelect={this.onSelect}
              stateKey="actualImpactThreshold"
            />
            <InputFilter
              data={this.state.impactFeeRateThreshold}
              label="Impact Fee Rate Threshold (%)"
              onSelect={this.onSelect}
              stateKey="impactFeeRateThreshold"
            />
          </Card>
        </SidebarWithCopySearchUrl>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchLendReportData,
      destroyLendReportData,
      saveLendReportFilters,
      destroySecurityMarketData,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(SideBar);
