import React, { Component } from "react";
import { Layout, Panel } from "arc-react-components";
import { gridOptions } from "../grid/gridOptions";
import { gridColumns } from "../grid/columnConfig";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GridContainer from 'commons/components/GridWithCellClick';
import Message from "commons/components/Message";
import {
    fetchLendReportData,
    destroyLendReportData,
    fetchSecurityMarketData,
    destroySecurityMarketData
} from "../actions";
import { convertJavaNYCDate } from "commons/util";
import AvailabilityDataPanel from "../../AvailabilityDataPanel/components/AvailabilityDataPanel";
import { loadNegotiationPopUp } from "../../seclend/RateNegotiationDialog/actions";
import NegotiationDialog from "../../seclend/RateNegotiationDialog/RateNegotiationDialog";


class Grid extends Component {
    constructor(props) {
        super(props);
        this.onCellClickHandler = this.onCellClickHandler.bind(this);
        this.onSelect = this.onSelect.bind(this);
    }

    componentWillUnmount() {
        this.props.destroyLendReportData;
    }

    onSelect(params) {
        const { key, value } = params;
        let newState = Object.assign({}, this.state);
        newState[key] = value;
        this.setState(newState);
    }

    onCellClickHandler = args => {
        if (args.colId === "negotiation") {
            var positionDataId = args.item.positionDataId;
            args.item.id = positionDataId;
            args.item.spn = args.item.pnlSpn;
            args.item.securityLocalName = args.item.ticker;
            args.item.borrowType = ["LEND"];
            args.item.view = "longPosition";
            this.props.loadNegotiationPopUp(args.item);
        } else {
            var payload = {
                spn: args.item.spn,
                cusip: "__null__",
                sedol: "__null__",
                date: args.item.localDate
            };
            this.props.fetchSecurityMarketData(payload);
            this.setState({
                selectedCpeIdForDrillDown: args.item.cpeId,
                selectedSecurityDescription:
                    args.item.securityDescription +
                    " - " +
                    args.item.spn +
                    " - " +
                    args.item.pnlSpn
            });
        }
    };


    renderGridData() {
        return (
            <GridContainer
                data={this.props.lendReportData.lendReportData}
                gridId="LendReportData"
                gridColumns={gridColumns(this.props.lendReportData)}
                gridOptions={gridOptions()}
                heightValue={600}
                onCellClick={this.onCellClickHandler}
                resizeCanvas={this.props.resizeCanvas}
                fill={true}
            />
        )
    }

    renderMainContainer() {
        if (this.props.lendReportData.lendReportData.length <= 0) {
            return <Message className="margin--top" messageData="No data found." />;
        }

        return (
            <Layout>
                <Layout.Child size={8}>
                    {this.renderGridData()}
                </Layout.Child>
                <br />
                {this.props.securityMarketData.length > 0 && (
                    <Layout.Child size={4}>
                        <Panel
                            dismissible
                            onClose={() => {
                                this.props.destroySecurityMarketData();
                            }}
                            title={this.state.selectedSecurityDescription}
                        >
                            <AvailabilityDataPanel
                                data={this.props.securityMarketData}
                                selectedCpeIdForDrillDown={
                                    this.state.selectedCpeIdForDrillDown
                                }
                            />
                        </Panel>
                    </Layout.Child>
                )}
            </Layout>

        );
    }

    render() {
        return (
            <React.Fragment>
                <NegotiationDialog />
                {this.renderMainContainer()}
            </React.Fragment>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            destroyLendReportData,
            fetchLendReportData,
            destroySecurityMarketData,
            fetchSecurityMarketData,
            loadNegotiationPopUp

        },
        dispatch
    );
}

function mapStateToProps(state) {
    return {
        lendReportData: state.lendReportData,
        securityMarketData: state.lendReportData.securityMarketData,
        resizeCanvas: state.lendReportData.resizeCanvas
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Grid);
