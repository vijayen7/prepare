import {
    FETCH_LEND_REPORT_DATA,
    DESTROY_LEND_REPORT_DATA,
    SAVE_LEND_REPORT_DATA_FILTERS,
    FETCH_SECURITY_MARKET_DATA_PNL,
    DESTROY_DATA_FOR_SECURITY_MARKET_DATA,
    RESIZE_CANVAS
  } from "commons/constants";
  import { combineReducers } from "redux";
  import { convertJavaDate } from "commons/util";


  function lendReportDataReducer(state = [], action) {
    switch (action.type) {
      case `${FETCH_LEND_REPORT_DATA}_SUCCESS`:
        return action.data || [];
      case DESTROY_LEND_REPORT_DATA:
        return [];
    }
    return state;
  }

  function securityMarketDataReducer(state = [], action) {
    switch (action.type) {
      case `${FETCH_SECURITY_MARKET_DATA_PNL}_SUCCESS`:
        return action.data || [];
      case `${DESTROY_DATA_FOR_SECURITY_MARKET_DATA}_SUCCESS`:
        return [];
    }
    return state;
  }

  function resizeCanvasReducer(state, action) {
    switch (action.type) {
      case RESIZE_CANVAS:
        return state == true ? false : true;
      default:
        return false;
    }
  }

  const lendReportReducer = combineReducers({
    lendReportData: lendReportDataReducer,
    securityMarketData: securityMarketDataReducer,
    resizeCanvas: resizeCanvasReducer,
  });

  export default lendReportReducer;
