import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  FETCH_LEND_REPORT_DATA,
  FETCH_SECURITY_MARKET_DATA_PNL,
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
  RESIZE_CANVAS,
  DESTROY_DATA_FOR_SECURITY_MARKET_DATA
} from "commons/constants";

import {
  getLendReportData,
  getSecurityMarketData
} from "./api"


function* fetchLendReportData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLendReportData, action.payload);
    yield put({ type: `${FETCH_LEND_REPORT_DATA}_SUCCESS`, data });

  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LEND_REPORT_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSecurityMarketData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getSecurityMarketData, action.payload);
    yield put({ type: `${FETCH_SECURITY_MARKET_DATA_PNL}_SUCCESS`, data });
    yield put({type:RESIZE_CANVAS});
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SECURITY_MARKET_DATA_PNL}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* destroySecurityMarketData(action) {
  try {
    yield put({ type: `${DESTROY_DATA_FOR_SECURITY_MARKET_DATA}_SUCCESS` });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  }
}

export function* lendReportSaga() {
  yield [
    takeEvery(FETCH_LEND_REPORT_DATA, fetchLendReportData)
  ];
}

export function* securityMarketData() {
  yield [
    takeEvery(FETCH_SECURITY_MARKET_DATA_PNL, fetchSecurityMarketData),
    takeEvery(DESTROY_DATA_FOR_SECURITY_MARKET_DATA, destroySecurityMarketData)
  ];
}

function* lendReportDataSaga() {
  yield all([lendReportSaga(), securityMarketData()]);
}
export default lendReportDataSaga;
