import React, { Component } from "react";
import SideBar from "./container/SideBar";
import LendReportGrid from "./container/Grid";
import Loader from "commons/container/Loader";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FilterButton from 'commons/components/FilterButton';
import { Layout } from 'arc-react-components';

import {
    fetchLendReportData
} from './actions';

 class LendReports extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <Loader />
                <div className="layout--flex--row">
                    <arc-header
                        ref={header => {
                            this.header = header;
                        }}
                        className="size--content"
                        user={USER}
                        modern-themes-enabled
                    />
                    <div className="layout--flex padding--top">
                        <div className="size--content padding--right">
                            <SideBar />

                        </div>
                        <LendReportGrid />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
    {
        fetchLendReportData
    },
      dispatch
    );
}

export default connect(
    null,
    mapDispatchToProps
)(LendReports);