import { BASE_URL, INPUT_FORMAT_JSON } from 'commons/constants';
import { calendarFormat } from "moment";
import { getEncodedParams} from 'commons/util';
import { CLASS_NAMESPACE } from "commons/ClassConfigs"
const queryString = require("query-string");
export let url = "";
var params = "";

export function getLendReportData(payload) {
    const encodedParams = getEncodedParams(payload, CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.LendReportDataFilter);
    url = `${BASE_URL}service/lendReportService/getLendReportData?lendReportDataFilter=${encodedParams}${INPUT_FORMAT_JSON}`;
    return fetch(url, {
        credentials: "include"
    }).then(data => data.json())
}

export function getSecurityMarketData(payload) {
    const paramString = queryString.stringify(payload);
    console.log(paramString);
    url = `${BASE_URL}service/marketDataService/getMarketDataForSecurityPnlSpn?spns=${payload.spn}&cusip=null&sedol=null&date="${payload.date}"${INPUT_FORMAT_JSON}`;
    return fetch(url, {
        credentials: "include"
    }).then(data => data.json())
}
