import { getSelectedFilterArray } from 'commons/util';
export function getIntegerArrayFromObjectArray(data) {
  var filterArray = [];
  data.forEach(item => {
    filterArray.push(item.key);
  });
  return filterArray;
}

export function getArrayFromString(valueString) {
  let filterArray = [];
  valueString.split(",").forEach((value) => {
    filterArray.push(value);
  });

  return filterArray;
}

export function createAPIPayload(payload) {
  var payload = Object.assign({}, payload);
  Object.keys(payload).forEach(function(key) {
    if (
      key !== "date" &&
      key !== "localDate" &&
      key !== "actualImpactThreshold" &&
      key !== "impactFeeRateThreshold"
    ) {
      if (payload[key] === undefined || payload[key] == null || payload[key].length === 0) {
        payload[key] = null;
      } else if (key == "reportingCurrencyId") {
        payload[key] = `${payload[key][0].key}`
      } else if (key == "spnIds") {
        payload[key] = getIntegerArrayFromObjectArray(getSelectedFilterArray(payload[key]));
      } else if (key == "securityFilter") {
        payload[key]["searchStrings"] = getArrayFromString(payload[key]["searchStrings"]);
      } else {
        payload[key] = getIntegerArrayFromObjectArray(payload[key]);
      }
    }
  });

  return payload;
}
