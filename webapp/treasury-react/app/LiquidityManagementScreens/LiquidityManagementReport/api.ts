/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { ArcFetch } from "arc-commons";
import { BASE_URL, QUERY_ARGS } from '../../commons/constants';
import SERVICE_URLS from "../../commons/UrlConfigs";
import { fetchPostURL } from "../../commons/util";
import {
  Adjustment, LiquidityManagementInput,
  MMFDetailsInput, TradeWorkflowInputs
} from "./models";

/**
 * Contains all the API calls definitions
 * @author burri
 */

export const fetchLiquidityReportData = (input: LiquidityManagementInput) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.liquidityManager.getLiquidityReport}`
  let encodedParam = getEncodedInputParamForLiquidityReport(input)

  return fetchPostURL(url, encodedParam);
};

export const fetchLiquidityConfigData = () => {
  let inputs: Array<String> = ["inputFormat=PROPERTIES", "format=json"];
  const url = `${BASE_URL}service/${SERVICE_URLS.liquidityConfigManager.getAllLiquidityConfig}`;
  let encodedParam = inputs.join("&");

  return fetchPostURL(url, encodedParam);
}

export const fetchMMFBookingDetailsForFund = (fundId: number) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.liquidityManager.getMMFBookingDetailsForFund}`
  let fundIdentifier = encodeURIComponent(JSON.stringify(fundId))
  let encodedParam = "fundId=" + fundIdentifier + "&inputFormat=json&format=json"

  return fetchPostURL(url, encodedParam);
};

export const fetchDefaultBookMMFDetailsForFund = (input: MMFDetailsInput) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.liquidityManager.getDefaultMMFBookingDetailsForFund}`
  let inputParam = encodeURIComponent(JSON.stringify(input))
  let encodedParam = "mmfDetailsInput=" + inputParam + "&inputFormat=json&format=json"

  return fetchPostURL(url, encodedParam);
};

export const addAdjustment = (payload: Adjustment) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.lcmAdjustmentService.saveAdjustments}`
  let param = encodeURIComponent(JSON.stringify(payload))
  let encodedParam = "adjustment=" + param + "&inputFormat=json&format=json"
  return fetchPostURL(url, encodedParam);
}

export const initiateTradeBooking = (payload: TradeWorkflowInputs) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.tradeWorkflowManager.initiateTradeBooking}`
  let param = encodeURIComponent(JSON.stringify(payload))
  let encodedParam = "tradeWorkflowInputs=" + param + "&inputFormat=json&format=json"
  return fetchPostURL(url, encodedParam);
}

export const beginWorkflowApi = (paramString: string) => {
  const beginWorkflowUrl = `${BASE_URL}service/${SERVICE_URLS.calmWorkflowManagement.beginWorkflow}?${paramString}`;
  return ArcFetch.getJSON(beginWorkflowUrl, QUERY_ARGS);
};

export const publishWorkflowApi = (paramString: string) => {
  const publishWorkflowUrl = `${BASE_URL}service/${SERVICE_URLS.calmWorkflowManagement.publishWorkflow}?${paramString}&format=JSON`;
  return ArcFetch.getJSON(publishWorkflowUrl, QUERY_ARGS);
};

export const cancelWorkflowApi = (paramString: string) => {
  const cancelWorkflowUrl = `${BASE_URL}service/${SERVICE_URLS.calmWorkflowManagement.cancelWorkflow}?${paramString}&format=JSON`;
  return ArcFetch.getJSON(cancelWorkflowUrl, QUERY_ARGS);
};

export const cancelAndBeginWorkflowApi = (paramString: string) => {
  const cancelAndBeginWorkflowUrl = `${BASE_URL}service/${SERVICE_URLS.calmWorkflowManagement.cancelAndBeginWorkflow}?${paramString}`;
  return ArcFetch.getJSON(cancelAndBeginWorkflowUrl, QUERY_ARGS);
};

export const getWorkflowStatusApi = (paramString: string) => {
  const getWorkflowUrl = `${BASE_URL}service/${SERVICE_URLS.calmWorkflowManagement.getWorkflow}?${paramString}&format=JSON`;
  return ArcFetch.getJSON(getWorkflowUrl, QUERY_ARGS);
};

const getEncodedInputParamForLiquidityReport = (input: LiquidityManagementInput) => {
  let inputParam = encodeURIComponent(JSON.stringify(input))
  let encodedParam = "liquidityManagementInput=" + inputParam + "&inputFormat=json&format=json"
  return encodedParam;
}

export const getReportingUrl = (input: LiquidityManagementInput) => {
  let url = `${BASE_URL}service/${SERVICE_URLS.liquidityManager.getFlattenedLiquidityReport}`
  url += `?`
  url += getEncodedInputParamForLiquidityReport(input)
  return url;
}
