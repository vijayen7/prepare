/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import "./utils/analytics";
import AppHeader, { whenHeaderRef } from "arc-header";
import { Breadcrumbs, Layout } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import { ReactLoader } from "../../commons/components/ReactLoader";
import Loader from "../../commons/container/Loader";
import { URLS, AGREEMENT_LEVEL, INITIAL_FILTERS } from "../commons/constants";
import BottomPanel from "./containers/BottomPanel";
import Grid from "./containers/Grid";
import SideBar from "./containers/LiquidityManagementReportSideBar";
import { LiquidityProps } from "../commons/models";
import { getFiltersFromUrl, getURLFromSelectedFilters } from "../commons/utils";
import { useStores } from "./useStores";
import { URLS as CONFIG_URLS } from "../LiquidityManagementConfig/constants";

/**
 * Rendering the screen with breadcrumbs and sidebar
 * @author burri
 */

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={"/"} key="liquidityManagement">
        Liquidity Management
      </Breadcrumbs.Item>
      <Breadcrumbs.Item
        link={URLS.LIQUIDITY_MANAGEMENT_REPORT}
        key="liquidityManagementReport"
      >
        Liquidity Management Report
      </Breadcrumbs.Item>
    </>
  );
};

const LiquidityManagement: React.FC<LiquidityProps> = (
  props: LiquidityProps
) => {
  useEffect(() => {
    let url = props.location.search;
    document.title = "Liquidity Management Report";
    setGlobalFilters();
    updateBreadCrumbs();
    extractFiltersFromUrl(url);
  }, []);

  const setGlobalFilters = async () => {
    const headerRef = await whenHeaderRef();
    if (headerRef && headerRef.gf) {
      const options = {
        enabled: {
          strategies: true,
          fundFamilies: true,
          legalEntityFamilies: true,
        },
      };
      headerRef.gf.addListener(appHeaderListener);
      headerRef.gf.setOptions(options);
    }
  };

  const { rootStore } = useStores();
  const { liquidityManagementReportStore, liquidityManagementWorkflowStore } =
    rootStore;

  let dataLevel = liquidityManagementReportStore.dataLevel;
  const extractFiltersFromUrl = async (url: string) => {
    if (url != null && url.length > 0) {
      let filters = getFiltersFromUrl(url);
      if (filters !== INITIAL_FILTERS) {
        liquidityManagementReportStore.applySavedFilters(filters);
        liquidityManagementReportStore.handleSearch();
      }
    }
  };

  function appHeaderListener({ userSelections, expandedSelections }) {
    liquidityManagementReportStore.setExpandedGlobalSelections(
      userSelections,
      expandedSelections
    );
  }

  const applicationMenu = (
    <div className="application-menu-toggle-view margin--right">
      <a
        id="workflowStatus"
        onClick={() => {
          window.open(
            URLS.WORKFLOW_STATUS +
              `?date=${liquidityManagementReportStore.searchFilter.selectedDate}`,
            "_blank"
          );
        }}
      >
        Workflow Status&nbsp;
      </a>
      {dataLevel !== AGREEMENT_LEVEL && (
        <a
          id="govtBondBalances"
          onClick={() => {
            window.open(
              URLS.GOVT_BOND_HOLDINGS +
                `?date=${liquidityManagementReportStore.searchFilter.selectedDate}`,
              "_blank"
            );
          }}
        >
          Govt Bond Balances&nbsp;
        </a>
      )}
      <a
        id="configuration"
        onClick={() => {
          window.open(CONFIG_URLS.CONFIGURATION, "_blank");
        }}
      >
        Configuration&nbsp;
      </a>
      <a
        id="summary"
        onClick={() => {
          window.open(
            getURLFromSelectedFilters(
              liquidityManagementReportStore.searchFilter,
              window.location.origin,
              URLS.DASHBOARD
            ),
            "_blank"
          );
        }}
      >
        Summary&nbsp;
      </a>
    </div>
  );

  return (
    <React.Fragment>
      <Loader />
      <ReactLoader
        inProgress={liquidityManagementReportStore.searchStatus.inProgress}
      />
      <div className="layout--flex--row">
        <AppHeader
          className="size--content"
          applicationMenu={applicationMenu}
        />
        <div className="layout--flex--row">
          <div className="layout--flex">
            <Layout isColumnType={true}>
              <Layout.Child
                childId="sideBar"
                size={2}
                collapsible
                title="Search Criteria"
                collapsed={liquidityManagementReportStore.toggleSidebar}
                onChange={liquidityManagementReportStore.setToggleSidebar}
              >
                <SideBar />
              </Layout.Child>
              <Layout.Divider
                childId="sideBarDivider"
                key="sideBarDivider"
                isResizable
              />
              <Layout.Child childId="dataDetail" size={9}>
                <Layout style={{ height: "100%" }}>
                  <Layout.Child childId="grid" collapsible>
                    <Grid />
                  </Layout.Child>
                  <Layout.Divider
                    childId="gridDivider"
                    key="gridDivider"
                    isResizable
                  />
                  <Layout.Child
                    childId="bottomPanel"
                    key="bottomPanel"
                    collapsed={!liquidityManagementReportStore.showBottomPanel}
                  >
                    <BottomPanel />
                  </Layout.Child>
                </Layout>
              </Layout.Child>
            </Layout>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default observer(LiquidityManagement);
