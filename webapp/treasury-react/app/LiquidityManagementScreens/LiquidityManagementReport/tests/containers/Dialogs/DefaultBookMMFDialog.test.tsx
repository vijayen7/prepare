/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import DefaultBookMMFDialog from '../../../components/DefaultBookMMFDialog'
import { shallow } from "enzyme";
import "../../../../../../internals/testing/setupTests";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import React from 'react';

/**
 * Tests the LogWireDialog
 * @author chawlasi
 */

const mockStore = configureMockStore();
const store = mockStore({});

describe('<DefaultBookMMFDialog />', () => {

  it('Rendering', () => {

    const { container } = render(<DefaultBookMMFDialog />);
    expect(container).toMatchSnapshot();
  });

  it("should render non empty DOM element", () => {
    expect(
      shallow(
        <Provider store={store}>
          <DefaultBookMMFDialog />
        </Provider>
      )
    ).not.toBeEmptyDOMElement
  });
});
