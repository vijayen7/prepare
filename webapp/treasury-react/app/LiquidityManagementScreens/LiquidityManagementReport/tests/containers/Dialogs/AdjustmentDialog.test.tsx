/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import AdjustmentDialog from '../../../components/AdjustmentDialog'
import { shallow } from "enzyme";
import "../../../../../../internals/testing/setupTests";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import React from 'react';

/**
 * Tests the AdjustmentDialog
 * @author chawlasi
 */

const mockStore = configureMockStore();
const store = mockStore({});

describe('<AdjustmentDialog />', () => {

  it('Rendering', () => {

    const { container } = render(<AdjustmentDialog />);
    expect(container).toMatchSnapshot();
  });

  it("should render non empty DOM element", () => {
    expect(
      shallow(
        <Provider store={store}>
          <AdjustmentDialog />
        </Provider>
      )
    ).not.toBeEmptyDOMElement
  });
});
