/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import UnderlyingDataGrid from '../../../containers/UnderlyingDataGrid/index';
import { LiquidityManagementReportStore } from '../../../LiquidityManagementReportStore';
import React from 'react';

/**
 * Tests the underlying data grid
 * @author chawlasi
 */

describe('<UnderlyingDataGrid />', () => {
  let columns = ['col_1', 'col_2'];
  let rowData = [
    'row_1_val_1', 'row_1_val_2',
    'row_2_val_1', 'row_2_val_2'
  ]

  it('Rendering', () => {
    const { container } = render(<UnderlyingDataGrid componentFundData={rowData} columns={columns} />);
    expect(container).toMatchSnapshot();
  });
});
