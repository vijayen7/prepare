/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import { TokenCounter } from '../../../containers/GridHeader/TokenCounter';
import React from 'react';

/**
 * Tests the Token Counter of quick filters
 * @author chawlasi
 */

describe('<TokenCounter />', () => {

  it('Rendering', () => {

    const { container } = render(<TokenCounter value="RandomToken" />);
    expect(container).toMatchSnapshot();
  });
});
