/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import GridHeader from '../../../containers/GridHeader/index';
import React from 'react';

/**
 * Tests the Grid Header
 * @author chawlasi
 */

describe('<GridHeader />', () => {

  it('Rendering', () => {

    const { container } = render(<GridHeader />);
    expect(container).toMatchSnapshot();
  });
});
