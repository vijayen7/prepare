/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import Grid from '../../../containers/Grid/index';
import { LiquidityManagementReportStore } from "../../../stores/LiquidityManagementReportStore";
import "../../../../../../internals/testing/setupTests";
import configureMockStore from "redux-mock-store";
import { FlattenedFundData } from "../../../models";
import React from 'react';

/**
 * Tests the main Grid
 * @author chawlasi
 */

const mockStore = configureMockStore();
const dummyStore = mockStore({});

describe('<Grid />', () => {

  it('Rendering', () => {

    let dummyId = -1;
    let dummyName = "dummy";

    const dummyData: FlattenedFundData[] = [{
      counter: 1,
      fund: dummyName,
      fundId: dummyId,
      legalEntity: dummyName,
      legalEntityId: dummyId,
      counterParty: dummyName,
      counterPartyId: dummyId,
      agreementType: dummyName,
      agreementTypeId: dummyId,
      openingCashBalance: dummyId,
      unEncumberedCashBalance: dummyId,
      adjustedEd: dummyId,
      adjustedDebitBalance: dummyId,
      liquidityAction: dummyName,
      liquidityActionAmount: dummyId,
      netCashFlows: dummyId,
      intradayFundingRequirement: dummyId,
      externalCallData: dummyId,
      internalCallData: dummyId,
      externalPullData: dummyId,
      internalPullData: dummyId,
      externalEd: dummyId,
      internalEd: dummyId,
      externalDebitBalance: dummyId,
      internalDebitBalance: dummyId,
      otherCashOutFlows: dummyId,
      cashOutFlows: dummyId,
      adjustmentAmount: dummyId,
      projectedClosingBalance: dummyId,
      mmfBalance: dummyId,
      tDayProjectedDebitBalance: dummyId,
      tPlusOneProjectedDebitBalance: dummyId,
      tPlusTwoProjectedDebitBalance: dummyId
    }];
    let store = new LiquidityManagementReportStore(dummyStore);
    store.liquidityData = dummyData;
    const { container } = render(<Grid liquidityManagementReportStore={store} />);
    expect(container).toMatchSnapshot();
  });
});
