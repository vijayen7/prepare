/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import BottomPanel from '../../../containers/BottomPanel/index';
import React from 'react';

/**
 * Tests the BottomPanel component
 * @author chawlasi
 */

describe('<BottomPanel />', () => {

  it('Rendering', () => {

    const { container } = render(<BottomPanel />);
    expect(container).toMatchSnapshot();
  });
});
