/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import * as myApi from '../api';
import 'whatwg-fetch';
import { CLASSES } from "../../commons/constants"
import { LiquidityManagementInput } from "../models";

/**
 * Tests the api's
 * @author chawlasi
 */

describe('Api', () => {
  const error = { message: 'Testing catch blocksss' };
  const mockResponse = [
    { key: '1', value: 'one' },
    { key: '2', value: 'two' },
    { key: '3', value: 'three' },
  ];
  let payload: LiquidityManagementInput = {
    date: '2021-07-02',
    legalEntityFamilyIds: [-1],
    legalEntityIds: [-1],
    cpeIds: [-1],
    agreementTypeIds: [-1],
    "@CLASS": CLASSES.LIQUIDITY_MANAGEMENT_INPUT
  };
  let fetchSpy = jest.spyOn(window, 'fetch');
  let mockFetchPromise: Promise<any>;

  beforeEach(() => {
    jest.clearAllMocks();
    mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => Promise.resolve(mockResponse),
    });
  });

  it('fetchLiquidityReportData(): success', async () => {
    expect.assertions(2);
    fetchSpy.mockImplementation(() => mockFetchPromise as any);
    const data = await myApi.fetchLiquidityReportData(payload);
    expect(window.fetch).toHaveBeenCalledTimes(1);
    expect(data).toEqual(mockResponse);
  });

  it('fetchLiquidityReportData(): error', async () => {
    expect.assertions(1);
    let mockFetchError = Promise.reject(error);

    fetchSpy.mockImplementation(() => mockFetchError as any);
    try {
      await myApi.fetchLiquidityReportData(payload);
    } catch (e) {
      expect(e).toEqual(error);
    }
  });
});
