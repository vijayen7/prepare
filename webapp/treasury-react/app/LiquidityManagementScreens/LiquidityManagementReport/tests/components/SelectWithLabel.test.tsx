/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import SelectWithLabel from '../../components/SelectWithLabel';
import React from 'react';

/**
 * Tests the SelectWithLabel component
 * @author chawlasi
 */

describe('<SelectWithLabel />', () => {

  it('Rendering', () => {
    const { container } = render(<SelectWithLabel />);
    expect(container).toMatchSnapshot();
  });
});
