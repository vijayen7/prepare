/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import Portfolio from '../../components/Portfolio';
import React from 'react';

/**
 * Tests the Portfolio component
 * @author chawlasi
 */

describe('<Portfolio />', () => {
  const dummyData = [
    { key: '1', value: 'one' },
    { key: '2', value: 'two' },
    { key: '3', value: 'three' },
  ];
  const dummyDefaultValue = { key: '1', value: 'a' };
  const dummyDefaultLabel = "dummyString"
  it('Rendering', () => {
    const { container } = render(<Portfolio data={dummyData} defaultValue={dummyDefaultValue} label={dummyDefaultLabel} isDefaultPortfolioBookOverride={true} />);
    expect(container).toMatchSnapshot();
  });
});
