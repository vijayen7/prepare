/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { shallow } from "enzyme";
import "../../../../../internals/testing/setupTests";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import Bundle from '../../components/Bundle';

/**
 * Tests the bundle component
 * @author chawlasi
 */

const mockStore = configureMockStore();
const store = mockStore({});

describe("Bundle Component", () => {
  it("should render without throwing an error", () => {
    expect(
      shallow(
        <Provider store={store}>
          <Bundle />
        </Provider>
      )
    );
  });
});
