/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import cssVars from '../../../css_vars';
const lightVars = Object.keys(cssVars['light-theme']);
const darkVars = Object.keys(cssVars['dark-theme']);

/**
 * Tests the css
 * @author chawlasi
 */

describe('CSS Vars', () => {
  it('should have same size of dark and light theme variables', () => {
    expect(lightVars.length === darkVars.length).toBeTruthy();
  });
  it('should have same keys in both dark and light objects', () => {
    const keyFlag = lightVars.some((cssVar) => !darkVars.includes(cssVar));
    expect(keyFlag).toBeFalsy();
  });
});
