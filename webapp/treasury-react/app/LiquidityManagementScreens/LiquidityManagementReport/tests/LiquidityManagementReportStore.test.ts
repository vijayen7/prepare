/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */


import { CLASSES } from "../../commons/constants";
import { LiquidityManagementInput, FlattenedFundData, ApiResponse } from "../models";
import RootStore from "../useStores";
import 'whatwg-fetch';

/**
 * Tests the store component
 * @author chawlasi
 */

describe("LiquidityManagementReportStore", () => {

  it("store liquidityData", async () => {

    const { liquidityManagementReportStore } = RootStore

    let dummyId = -1;
    let dummyName = "dummy";

    let payload: LiquidityManagementInput = {
      date: '2021-07-02',
      legalEntityFamilyIds: [-1],
      legalEntityIds: [-1],
      cpeIds: [-1],
      agreementTypeIds: [-1],
      "@CLASS": CLASSES.LIQUIDITY_MANAGEMENT_INPUT
    };

    const mockResponse: FlattenedFundData[] = [{
      counter: 1,
      fund: dummyName,
      fundId: dummyId,
      legalEntity: dummyName,
      legalEntityId: dummyId,
      counterParty: dummyName,
      counterPartyId: dummyId,
      agreementType: dummyName,
      agreementTypeId: dummyId,
      openingCashBalance: dummyId,
      unEncumberedCashBalance: dummyId,
      adjustedEd: dummyId,
      adjustedDebitBalance: dummyId,
      liquidityAction: dummyName,
      liquidityActionAmount: dummyId,
      netCashFlows: dummyId,
      intradayFundingRequirement: dummyId,
      externalCallData: dummyId,
      internalCallData: dummyId,
      externalPullData: dummyId,
      internalPullData: dummyId,
      externalEd: dummyId,
      internalEd: dummyId,
      externalDebitBalance: dummyId,
      internalDebitBalance: dummyId,
      otherCashOutFlows: dummyId,
      cashOutFlows: dummyId,
      adjustmentAmount: dummyId,
      projectedClosingBalance: dummyId,
      mmfBalance: dummyId,
      tDayProjectedDebitBalance: dummyId,
      tPlusOneProjectedDebitBalance: dummyId,
      tPlusTwoProjectedDebitBalance: dummyId
    }];

    liquidityManagementReportStore.getLiquidityReportPayload = () => payload;
    liquidityManagementReportStore.liquidityData = mockResponse
    liquidityManagementReportStore.getLiquidityReportData();
    expect(liquidityManagementReportStore.liquidityData).toHaveLength(1)
  });

  it("store filter", async () => {

    const { liquidityManagementReportStore } = RootStore

    let filter = {
      selectedDate: '2021-07-02',
      selectedLegalEntityFamilies: [],
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedAgreementTypes: [],
      "@CLASS": CLASSES.LIQUIDITY_MANAGEMENT_INPUT,
    };
    liquidityManagementReportStore.setFilters(filter);
    expect(liquidityManagementReportStore.searchFilter).toStrictEqual(filter);
  });

  it("store clickedRowId", async () => {

    const { liquidityManagementReportStore, liquidityManagementWorkflowStore } = RootStore

    let dummyId = -1;
    let dummyName = "dummy";

    liquidityManagementWorkflowStore.setClickedRow(dummyId);
    expect(liquidityManagementReportStore.clickedRowId).toBe(dummyId);
  });

  it("store mmfBookingDetails", async () => {

    const { liquidityManagementWorkflowStore } = RootStore

    let dummyId = -1;
    let dummyName = "dummy";

    const mockResponse: ApiResponse = {
      successStatus: true,
      message: "dummyMessage",
      response: [
        { key: '1', value: 'one' },
        { key: '2', value: 'two' },
        { key: '3', value: 'three' },
      ]
    };

    liquidityManagementWorkflowStore.mmfBookingDetailsForFund = mockResponse;
    liquidityManagementWorkflowStore.getMMFBookingDetailsForFund(dummyId, dummyName);
    expect(liquidityManagementWorkflowStore.mmfBookingDetailsForFund.response).toStrictEqual(mockResponse.response);
  });

});
