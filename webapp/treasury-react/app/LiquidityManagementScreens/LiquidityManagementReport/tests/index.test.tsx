/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

jest.mock('arc-header', () => ({
  __esModule: true,
  whenHeaderRef: jest.fn().mockImplementation(() =>
    Promise.resolve({
      setBreadcrumbs: jest.fn(),
    })
  ),
  default: () => {
    return <div data-testid="AppHeader">AppHeader</div>;
  },
}));

jest.mock('../containers/LiquidityManagementReportSideBar', () => ({
  __esModule: true,
  default: () => {
    return <div data-testid="SideBar">SideBar</div>;
  },
}));
jest.mock('../containers/Grid/index', () => ({
  __esModule: true,
  default: () => {
    return <div data-testid="Grid">Grid</div>;
  },
}));

jest.mock('../containers/BottomPanel/index', () => ({
  __esModule: true,
  default: () => {
    return <div data-testid="BottomPanel">BottomPanel</div>;
  },
}));

import { render } from '@testing-library/react';
import { whenHeaderRef } from 'arc-header';
import LiquidityManagement from '../../LiquidityManagementReport/index';
import "../../../../internals/testing/setupTests";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import React from 'react';

/**
 * Tests the main screen
 * @author chawlasi
 */

const mockStore = configureMockStore();
const store = mockStore({});

describe('<LiquidityManagement />', () => {
  it('Rendering', async () => {
    const { queryByTestId } = render(
      <Provider store={store}>
        <LiquidityManagement location={window.location.origin} />
      </Provider>
    );
    expect(whenHeaderRef).toHaveBeenCalledTimes(2);
    expect(queryByTestId('AppHeader')).not.toBeNull();
    expect(queryByTestId('SideBar')).not.toBeNull();
    expect(queryByTestId('Grid')).not.toBeNull();
    expect(queryByTestId('BottomPanel')).not.toBeNull();
  });
});
