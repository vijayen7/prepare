/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Card, Sidebar as ArcSideBar } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import FilterButton from "../../../commons/components/FilterButton";
import SaveSettingsManager from "../../../commons/components/SaveSettingsManager";
import ArcDateFilter from "../../../commons/container/ArcDateFilter";
import MultiSelectFilter from "../../../commons/filters/components/MultiSelectFilter";
import CpeFilter from "../../../commons/container/CpeFilter";
import AgreementTypeFilter from "../../../commons/container/AgreementTypeFilter";
import { ENTITY_LEVEL, AGREEMENT_LEVEL } from "../../commons/constants";
import { useStores } from "../useStores";

/**
 * Component for rendering the Sidebar filters
 * @author burri
 */

const SideBar: React.FC = () => {
  const { rootStore } = useStores();
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  let dataLevel = liquidityManagementReportStore.dataLevel;
  return (
    <ArcSideBar
      style={{ minWidth: "225px" }}
      footer={
        <span>
          <FilterButton
            label="Reset"
            onClick={liquidityManagementReportStore.handleReset}
            className="margin--horizontal button--tertiary"
          />
          <FilterButton
            label="Search"
            onClick={liquidityManagementReportStore.handleSearch}
            className="margin--horizontal button--primary float--right"
          />
          <FilterButton
            reset={true}
            label="Copy Search URL"
            onClick={liquidityManagementReportStore.handleCopySearch}
            className="margin--horizontal button--tertiary float--right"
          />
        </span>
      }
      title={"Search Criteria"}
    >
      <Card>
        <div className="margin" style={{ height: "30px" }}>
          <SaveSettingsManager
            selectedFilters={liquidityManagementReportStore.searchFilter}
            applySavedFilters={liquidityManagementReportStore.applySavedFilters}
            applicationName="liquidityManagement"
          />
        </div>
      </Card>
      <Card>
        <ArcDateFilter
          layout="standard"
          stateKey="selectedDate"
          label="Date"
          onSelect={liquidityManagementReportStore.onSelect}
          data={liquidityManagementReportStore.searchFilter.selectedDate}
        />
        {dataLevel === ENTITY_LEVEL && (
          <MultiSelectFilter
            data={liquidityManagementReportStore.legalEntityFamilyDataList}
            onSelect={liquidityManagementReportStore.onSelect}
            label="Legal Entity Families"
            selectedData={
              liquidityManagementReportStore.searchFilter
                .selectedLegalEntityFamilies
            }
            stateKey="selectedLegalEntityFamilies"
            multiSelect
          />
        )}
        {dataLevel === AGREEMENT_LEVEL && (
          <>
            <MultiSelectFilter
              data={liquidityManagementReportStore.legalEntityDataList}
              onSelect={liquidityManagementReportStore.onSelect}
              label="Legal Entities"
              selectedData={
                liquidityManagementReportStore.searchFilter
                  .selectedLegalEntities
              }
              stateKey="selectedLegalEntities"
              multiSelect
            />
            <CpeFilter
              label="Counterparty Entities"
              onSelect={liquidityManagementReportStore.onSelect}
              selectedData={
                liquidityManagementReportStore.searchFilter.selectedCpes
              }
              multiSelect
            />
            <AgreementTypeFilter
              label="Agreement Types"
              onSelect={liquidityManagementReportStore.onSelect}
              selectedData={
                liquidityManagementReportStore.searchFilter
                  .selectedAgreementTypes
              }
              multiSelect
            />
          </>
        )}
      </Card>
    </ArcSideBar>
  );
};

export default observer(SideBar);
