/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";

/**
 * Renders Legend Dot Component
 * @author burri
 */

type Props = {
  color: string,
  height: string
}

const LegendDot: React.FC<Props> = (props: Props) => {
  return (
    <>
      <div
      className={props.color}
      style={{
        height:props.height,
        width:props.height,
        borderRadius: "50%",
        display: "inline-block",
        marginBottom: "-2px"
        }}
      ></div>
    </>
  )
};

export default LegendDot;
