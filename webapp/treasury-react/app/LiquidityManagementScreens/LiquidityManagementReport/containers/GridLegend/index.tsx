/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from 'react';
import LegendDot from './LegendDot';

/**
 * Renders Grid Legend Component
 * @author burri
 */

const gridLegends = [
  { title: 'Stay', icon: <LegendDot height="13px" color="primary" /> },
  { title: 'Redeem', icon: <LegendDot height="13px" color="warning" /> },
  { title: 'Invest', icon: <LegendDot height="13px" color="success" /> },
];

const GridLegend: React.FC<any> = () => {
  return (
    <span>
      {gridLegends.map((icon, index) => (
        <span key={index} className="margin--horizontal">
          <span className="margin--horizontal--small">{icon['icon']}</span>
          <span>{icon['title']}</span>
          &nbsp;&nbsp;
        </span>
      ))}
    </span>
  );
};

export default GridLegend;



