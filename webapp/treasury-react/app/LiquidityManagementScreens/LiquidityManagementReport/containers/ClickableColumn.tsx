/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { observer } from "mobx-react-lite";
import React from "react";
import { plainNumberFormatter } from "../../../commons/grid/formatters";
import { useStores } from "../useStores";

/**
 * Component for rendering the clickable cell values
 * @author burri
 */

const ClickableColumn: React.FC<any> = (props) => {

  const { rootStore } = useStores();
  const liquidityManagementReportStore = rootStore.liquidityManagementReportStore;
  const liquidityManagementWorkflowStore = rootStore.liquidityManagementWorkflowStore;


  const setBottomPanel = (props) => {
    liquidityManagementWorkflowStore.setClickedRow(props.row?.counter);
    liquidityManagementReportStore.setBottomPanelTab(props.panelTab);
    liquidityManagementReportStore.setClickedRowId(props.row?.counter + "_" + props?.panelTab);
    liquidityManagementReportStore.setShowBottomPanel(true);
  };

  function render() {
    if (
      props.value &&
      props.value !== null &&
      props.value !== undefined &&
      props.value !== 0
    ) {
      return (
        <>
          <a
            onClick={(e) => {
              e.stopPropagation();
              setBottomPanel(props);
            }}
          >
            {plainNumberFormatter(undefined, undefined, props.value)}
          </a>
        </>
      );
    } else {
      return (
        <>
          <div style={{ opacity: 0.6 }}>0</div>
        </>
      );
    }
  }
  return <>{render()}</>;
};

export default observer(ClickableColumn);
