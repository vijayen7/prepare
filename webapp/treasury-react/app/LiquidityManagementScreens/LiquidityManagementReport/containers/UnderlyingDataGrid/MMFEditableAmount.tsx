/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { EditableInput, Field } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import { plainNumberFormatter } from "../../../../commons/grid/formatters";
import { SEG_IA } from "../../constants";
import { NOT_APPLICABLE } from "../../../commons/constants";
import { useStores } from "../../useStores";
import { getCommaSeparatedNumber } from "../../../../commons/util";

/**
 * Component for taking the amount from the user dynamically
 * @author chawlasi
 */

const MMFEditableAmount: React.FC<any> = ({ context }) => {
  const { rootStore } = useStores();
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;
  let mmfBal;
  if (context.row.parentInfo === undefined || context.row.parentInfo === null) {
    mmfBal = liquidityManagementWorkflowStore.mmfBalancesList.find(
      (mmfBalance) =>
        mmfBalance.tickerWithCustodianAccount ===
        context.row.tickerWithCustodianAccount
    );
  }
  let commaSeparatedNumber = getCommaSeparatedNumber(context.row.amount);
  const childrenAmountElement = (
    <EditableInput
      value={
        commaSeparatedNumber === null || parseFloat(commaSeparatedNumber) === 0
          ? ""
          : commaSeparatedNumber
      }
      onChange={(val) => {
        liquidityManagementWorkflowStore.onChangeAmount(
          context,
          val.toString().replace(/,/g, "")
        );
      }}
      type="number"
      placeholder="Enter Amount"
      testId="basic-editable-input"
      style={{ float: "right" }}
    />
  );

  if (
    context.row.children !== undefined &&
    context.row.parentInfo === undefined
  ) {
    let result = plainNumberFormatter(undefined, undefined, mmfBal?.amount);
    if (
      result === NOT_APPLICABLE ||
      result === null ||
      result === undefined ||
      result == 0
    )
      return (
        <>
          <div className="text-align--right" style={{ opacity: 0.6 }}>
            {0}
          </div>
        </>
      );
    else
      return (
        <>
          <div className="text-align--right">{result}</div>
        </>
      );
  } else if (
    context.row.children === undefined &&
    context.row.tickerWithCustodianAccount !== SEG_IA
  ) {
    return <>{childrenAmountElement}</>;
  } else {
    return <></>;
  }
};

export default observer(MMFEditableAmount);
