/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import ArcDataGrid from "arc-data-grid";
import { Button, ToggleGroup } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import { MMF_BALANCES_TAB } from "../../constants";
import { useStores } from "../../useStores";
import { BOTTOM_PANEL_DEFAULT_AGGREGATION } from "./grid-columns";

/**
 * Renders the grid on the bottom panel
 * @author burri
 */

const gridConfig = {
  enableTreeView: true,
  showColumnAggregationRow: true,
  getExportFileName: ({ fileType, all }) => {
    return "liquidity-management-bottom-panel-report";
  },
};

const UnderlyingDataGrid: React.FC<any> = (props) => {
  const { rootStore } = useStores();
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;

  function renderGridData() {
    let gridData: any;
    if (props.tabName === MMF_BALANCES_TAB) {
      gridData = liquidityManagementWorkflowStore.mmfBalancesList;
    } else {
      gridData = props.componentFundData;
    }

    let grid = (
      <>
        <ArcDataGrid
          rows={gridData}
          columns={props.columns}
          configurations={gridConfig}
          pinnedColumns={props.pinnedColumns}
          aggregation={BOTTOM_PANEL_DEFAULT_AGGREGATION}
        />
      </>
    );
    let gridWithHeader = (
      <>
        {props.tabName && props.tabName === MMF_BALANCES_TAB && (
          <div className="margin--bottom--double">
            <Button
              className="button--secondary"
              onClick={(e) => {
                e.stopPropagation();
                liquidityManagementWorkflowStore.startWorkflow(
                  liquidityManagementReportStore.selectedFundId,
                  liquidityManagementReportStore.selectedFundName,
                  null,
                  false
                );
              }}
            >
              Buy Other MMF
            </Button>
            <label className="padding--horizontal margin--horizontal--double">
              Show Tickers with Zero balance:
            </label>
            <ToggleGroup
              value={liquidityManagementWorkflowStore.mmfToggleButtonValue}
              onChange={(value) => {
                liquidityManagementWorkflowStore.onMMFToggleButtonValueChanged(
                  value
                );
              }}
            >
              <ToggleGroup.Button key="1" data={true}>
                Yes
              </ToggleGroup.Button>
              <ToggleGroup.Button key="2" data={false}>
                No
              </ToggleGroup.Button>
            </ToggleGroup>
          </div>
        )}
        {grid}
      </>
    );
    return gridWithHeader;
  }

  return <>{renderGridData()}</>;
};

export default observer(UnderlyingDataGrid);
