/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { formatFinancial } from "arc-commons/utils/number";
import React from "react";
import { plainNumberFormatter } from "../../../../commons/grid/formatters";
import {
  RAG_CATEGORY_CODE_MAP,
  SEG_IA,
  SOURCE_CATEGORY_CODE_MAP,
  MESSAGES,
} from "../../constants";
import { URLS } from "../../../commons/constants";
import { useStores } from "../../useStores";
import { getNumericColumn, getValueOrDefault } from "../../utils";
import {
  getFormattedPercentColumn,
  genericFormatter,
} from "../../../../commons/TreasuryUtils";
import MMFEditableAmount from "./MMFEditableAmount";
import { showMessageDialog } from "../../components/GenericDialog";

/**
 * Contains all the columns definition for the bottom panel grids
 * @author burri
 */

export const getMarginCallAndPullDataColumns = () => {
  return [
    {
      identity: "type",
      header: "Direction",
      Cell: (value, _) => {
        return <SourceFlags value={value} />;
      },
    },
    {
      identity: "source",
      header: "Source",
      Cell: (value, _) => {
        return <SourceFlags value={value} />;
      },
    },
    genericFormatter("agreementId", "Agreement ID"),
    genericFormatter("legalEntity", "Legal Entity", 250),
    genericFormatter("counterPartyEntity", "Counterparty Entity", 250),
    genericFormatter("agreementType", "Agreement Type"),
    getNumericColumn("exposure"),
    getNumericColumn("collateral"),
    getNumericColumn("margin"),
    getNumericColumn("brokerED", "ED"),
    getNumericColumn("segCollateral"),
    getNumericColumn("segMargin"),
    getNumericColumn("segED"),
  ];
};

export const getOpeningCashBalanceDataColumns = () => {
  return [
    genericFormatter("accountNumber", "Account Number"),
    genericFormatter("currency", "Currency"),
    getNumericColumn("balance", "Balance"),
  ];
};

export const getMMFBalanceDataColumns = () => {
  return [
    {
      identity: "tickerWithCustodianAccount",
      header: "Ticker > Custodian Account",
      minWidth: 240,
    },
    {
      identity: "actions",
      Cell: (_, context) => <MMFActionPanel context={context} />,
      disableFilters: true,
      disableSearches: true,
      disableSortBy: true,
      disableExport: true,
      disableGroupBy: true,
      disableAggregation: true,
      disablePinning: true,
      width: 90,
    },
    getNumericColumn("balanceUsd", "Opening Balance USD"),
    {
      identity: "amount",
      Cell: (_, context) => <MMFEditableAmount context={context} />,
      disableFilters: true,
      disableSearches: true,
      disableSortBy: true,
      disableExport: true,
      disableGroupBy: true,
      disablePinning: true,
      width: 100,
      AggregatedCell: (value: number | string) => (
        <>
          <div className="text-align--right">
            {plainNumberFormatter(undefined, undefined, value)}
          </div>
        </>
      ),
    },
    getNumericColumn("closingBalanceUsd", "Closing Balance USD"),
    getNumericColumn("aumUsd", "AUM USD", 100, true),
    {
      identity: "entityOwnership",
      header: "Fund Ownership",
      Cell: (value, _) => {
        return <ThresholdFlags value={value} name="entityOwnership" />;
      },
    },
    {
      identity: "fundOwnership",
      header: "Firm Ownership",
      Cell: (value, _) => {
        return <ThresholdFlags value={value} name="fundOwnership" />;
      },
    },
    getFormattedPercentColumn("yield", "Yield"),
    genericFormatter("expenseRatio", "Expense Ratio"),
  ];
};

export const getCashFlowsDataColumns = () => {
  const goToWiresSearch = (value: number) => {
    let url = URLS.WIRES_URL + value;
    window.open(url, "_blank");
  };
  return [
    {
      identity: "type",
      header: "Type",
      Cell: (value, _) => {
        return <SourceFlags value={value} />;
      },
    },
    {
      identity: "direction",
      header: "Direction",
      Cell: (value, _) => {
        return <SourceFlags value={value} />;
      },
    },
    {
      identity: "wireId",
      header: "Wire ID",
      Cell: (value, _) => {
        return (
          <a
            onClick={(e) => {
              e.stopPropagation();
              goToWiresSearch(value);
            }}
          >
            <>
              <div className="text-align--right">{value}</div>
            </>
          </a>
        );
      },
    },
    genericFormatter("category", "Category", 150),
    getNumericColumn("wireAmount", "Wire Amount", 150),
    {
      identity: "wireAccount",
      header: "Wire Account",
      columns: [
        genericFormatter("sourceWireAccount", "Source", 295),
        genericFormatter("destinationWireAccount", "Destination", 335),
      ],
    },
    {
      identity: "custodianAccountId",
      header: "Custodian Account ID",
      columns: [
        genericFormatter("sourceCustodianAccountId", "Source"),
        genericFormatter("destinationCustodianAccountId", "Destination"),
      ],
    },
  ];
};

const MMFActionPanel: React.FC<any> = ({ context }) => {
  const { rootStore } = useStores();
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;

  if (
    context.row.children === undefined &&
    context.row.tickerWithCustodianAccount !== SEG_IA
  ) {
    return (
      <>
        <i
          title="Execute"
          className="icon-transfer padding--horizontal--small"
          onClick={(e) => {
            e.stopPropagation();
            let contextAmount = context.row.amount;
            if (
              Number.isNaN(contextAmount) ||
              contextAmount === undefined ||
              contextAmount === null ||
              parseInt(contextAmount) === 0 ||
              (parseFloat(context.row.amount) < 0 &&
                Math.abs(parseFloat(context.row.amount)) >
                  parseFloat(context.row.balanceUsd))
            ) {
              showMessageDialog(MESSAGES.AMOUNT_ERROR, MESSAGES.AMOUNT_MESSAGE);
            } else {
              liquidityManagementWorkflowStore.startWorkflow(
                liquidityManagementReportStore.selectedFundId,
                "",
                context,
                true
              );
            }
          }}
        />
      </>
    );
  } else {
    return <></>;
  }
};

function SourceFlags({ value }) {
  let source = value === undefined || value === null ? undefined : value;
  let tokenCss = getValueOrDefault(SOURCE_CATEGORY_CODE_MAP[source], "");
  let actionIcon = <></>;

  let token = (
    <span className={"token padding--horizontal--double " + tokenCss}>
      {actionIcon}
      {source}
    </span>
  );
  return (
    <div className="text-align--center" title={source}>
      {token}
    </div>
  );
}

function ThresholdFlags({ value, name }) {
  const { rootStore } = useStores();
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;

  let color: string;
  let source =
    value === undefined || value === null
      ? undefined
      : String(formatFinancial(value)) + "%";
  if (name === "entityOwnership") {
    color = liquidityManagementWorkflowStore.compare(
      liquidityManagementReportStore.fundLevelOwnershipThresholds,
      value
    );
  } else {
    color = liquidityManagementWorkflowStore.compare(
      liquidityManagementReportStore.firmLevelOwnershipThresholds,
      value
    );
  }
  let tokenCss = getValueOrDefault(RAG_CATEGORY_CODE_MAP[color], "");
  let actionIcon = <></>;

  let token = (
    <span className={"token padding--horizontal--double " + tokenCss}>
      {actionIcon}
      {source}
    </span>
  );
  return (
    <div className="text-align--center" title={source}>
      {token}
    </div>
  );
}

export const BOTTOM_PANEL_DEFAULT_AGGREGATION = {
  balanceUsd: "sum",
  closingBalanceUsd: "sum",
  amount: "sum",
  aumUsd: "sum",
};
