/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { plainNumberFormatter } from "../../../../commons/grid/formatters";
import {
  ACTION_CATEGORY_CODE_MAP,
  STAY,
  CALL_PULL_DETAILS_TAB,
  CASH_FLOWS_TAB,
  CASH_BALANCES_TAB,
  MMF_BALANCES_TAB,
} from "../../constants";
import { AGREEMENT_LEVEL } from "../../../commons/constants";
import { useStores } from "../../useStores";
import {
  getNumericColumn,
  getValueOrDefault,
  getClickableColumn,
} from "../../utils";
import { genericFormatter } from "../../../../commons/TreasuryUtils";

/**
 * Contains all the columns definition for the main grid
 * @author burri
 */

export const getColumns = () => {
  const { rootStore } = useStores();
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  let dataLevel = liquidityManagementReportStore.dataLevel;

  return [
    {
      identity: "actions",
      Cell: (_, context) => <ActionPanel context={context} />,
      disableFilters: true,
      disableSearches: true,
      disableSortBy: true,
      disableExport: true,
      disableGroupBy: true,
      disableAggregation: true,
      width: 90,
    },
    ...getFundColumns(dataLevel),
    {
      identity: "liquidityAction",
      header: "Liquidity Action",
      Cell: (_, context) => {
        return <ActionFlags row={context.row} />;
      },
    },
    ...getCashBalancesColumns(dataLevel),
    getClickableColumn("netCashFlows", "Net Cash Flows", CASH_FLOWS_TAB, 120),
    ...getMarginAndDebitBalancesColumns(dataLevel),
    getNumericColumn("adjustmentAmount", "Adjustment Amount", 140),
    getClickableColumn("mmfBalance", "MMF Balance", MMF_BALANCES_TAB, 130),
  ];
};

const ActionPanel: React.FC<any> = ({ context }) => {
  const { rootStore } = useStores();
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;

  return (
    <>
      <i
        title="Book MMF"
        className="icon-transfer padding--horizontal--small"
        onClick={(e) => {
          e.stopPropagation();
          liquidityManagementWorkflowStore.startWorkflow(
            context.row?.fundId,
            context.row?.fund,
            null,
            false
          );
        }}
      />
      <i
        title="Add Adjustment"
        className="icon-add padding--horizontal--small"
        onClick={(e) => {
          e.stopPropagation();
          liquidityManagementWorkflowStore.toggleShowDialog();
          liquidityManagementWorkflowStore.adjustmentRowContext = context.row;
          liquidityManagementWorkflowStore.adjustmentFundName =
            context.row.fund;
        }}
      />
    </>
  );
};

function ActionFlags({ row }) {
  const { rootStore } = useStores();
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  const setBottomPanel = () => {
    liquidityManagementWorkflowStore.setClickedRow(row?.counter);
    liquidityManagementReportStore.setBottomPanelTab(MMF_BALANCES_TAB);
    liquidityManagementReportStore.setClickedRowId(
      row?.counter + "_" + MMF_BALANCES_TAB
    );
    liquidityManagementReportStore.setShowBottomPanel(true);
  };
  let liquidityAction = row?.liquidityAction ? row.liquidityAction : STAY;
  let tokenCss = getValueOrDefault(
    ACTION_CATEGORY_CODE_MAP[liquidityAction],
    ""
  );
  let actionIcon = <></>;
  let displayLiquidityAction = liquidityAction;
  if (liquidityAction !== STAY) {
    displayLiquidityAction = plainNumberFormatter(
      undefined,
      undefined,
      row?.liquidityActionAmount
    );
  }
  let token = (
    <span
      className={"token padding--horizontal--double " + tokenCss}
      onClick={(e) => {
        e.stopPropagation();
        setBottomPanel();
      }}
    >
      {actionIcon}
      {displayLiquidityAction}
    </span>
  );
  return (
    <div className="text-align--center" title={liquidityAction}>
      {token}
    </div>
  );
}

export const DEFAULT_AGGREGATION = {
  openingCashBalance: "sum",
  unEncumberedCashBalance: "sum",
  adjustedEd: "sum",
  adjustedDebitBalance: "sum",
  netCashFlows: "sum",
  intradayFundingRequirement: "sum",
  externalCallData: "sum",
  internalCallData: "sum",
  externalPullData: "sum",
  internalPullData: "sum",
  otherCashOutFlows: "sum",
  cashOutFlows: "sum",
  adjustmentAmount: "sum",
  projectedClosingBalance: "sum",
  externalEd: "sum",
  internalEd: "sum",
  externalDebitBalance: "sum",
  internalDebitBalance: "sum",
  mmfBalance: "sum",
  tDayProjectedDebitBalance: "sum",
  tPlusOneProjectedDebitBalance: "sum",
  tPlusTwoProjectedDebitBalance: "sum",
};

const getFundColumns = (dataLevel: number) => {
  let fundColumns;
  dataLevel === AGREEMENT_LEVEL
    ? (fundColumns = [
      {
        identity: "legalEntity",
        header: "Legal Entity",
        width: 200,
      },
      {
        identity: "counterParty",
        header: "Counter Party",
        width: 140,
      },
      {
        identity: "agreementType",
        header: "Agreement Type",
      },
      genericFormatter("fundId", "Agreement ID"),
    ])
    : (fundColumns = [
      {
        identity: "fund",
        header: "Fund",
        disableAggregation: true,
        width: 200,
      },
    ]);
  return fundColumns;
};

const getCashBalancesColumns = (dataLevel: number) => {
  let cashBalancesColumns;
  dataLevel === AGREEMENT_LEVEL
    ? (cashBalancesColumns = [
      getNumericColumn("unEncumberedCashBalance", "Unencumbered Cash", 180),
      getNumericColumn("adjustedEd", "Actionable E/D", 130),
      getNumericColumn(
        "adjustedDebitBalance",
        "Actionable Debit Balance",
        160
      ),
      getNumericColumn(
        "tDayProjectedDebitBalance",
        "T Projected External Debit Balance",
        240
      ),
      getNumericColumn(
        "tPlusOneProjectedDebitBalance",
        "T+1 Projected External Debit Balance",
        240
      ),
      getNumericColumn(
        "tPlusTwoProjectedDebitBalance",
        "T+2 Projected External Debit Balance",
        240
      ),
    ])
    : (cashBalancesColumns = [
      getClickableColumn(
        "openingCashBalance",
        "Opening Cash Balance",
        CASH_BALANCES_TAB,
        160
      ),
      getNumericColumn(
        "intradayFundingRequirement",
        "Intra Day Funding Requirement",
        200
      ),
      getNumericColumn(
        "projectedClosingBalance",
        "Projected Closing Balance",
        180
      ),
    ]);
  return cashBalancesColumns;
};

const getMarginAndDebitBalancesColumns = (dataLevel: number) => {
  let marginCallAndPullColumns;
  dataLevel === AGREEMENT_LEVEL
    ? (marginCallAndPullColumns = [
      getClickableColumn(
        "externalEd",
        "External E/D",
        CALL_PULL_DETAILS_TAB,
        130
      ),
      getClickableColumn(
        "internalEd",
        "Internal E/D (Unparsed)",
        CALL_PULL_DETAILS_TAB,
        160
      ),
      getNumericColumn("externalDebitBalance", "External Debit Balance", 160),
      getNumericColumn("internalDebitBalance", "Internal Debit Balance", 160),
      getClickableColumn(
        "cashOutFlows",
        "Cash Out Flows",
        CASH_FLOWS_TAB,
        150
      ),
    ])
    : (marginCallAndPullColumns = [
      getClickableColumn(
        "externalCallData",
        "External Call Data",
        CALL_PULL_DETAILS_TAB,
        130
      ),
      getClickableColumn(
        "internalCallData",
        "Internal Call Data",
        CALL_PULL_DETAILS_TAB,
        130
      ),
      getClickableColumn(
        "externalPullData",
        "External Pull Data",
        CALL_PULL_DETAILS_TAB,
        130
      ),
      getClickableColumn(
        "internalPullData",
        "Internal Pull Data",
        CALL_PULL_DETAILS_TAB,
        130
      ),
      getClickableColumn(
        "otherCashOutFlows",
        "Other Cash Out Flows",
        CASH_FLOWS_TAB,
        150
      ),
    ]);
  return marginCallAndPullColumns;
};
