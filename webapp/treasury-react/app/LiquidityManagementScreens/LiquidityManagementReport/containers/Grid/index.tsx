/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import ArcDataGrid from "arc-data-grid";
import { Layout, Message } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import {
  DEFAULT_PINNED_COLUMNS,
  DEFAULT_SORT_BY,
  DEFAULT_SORT_BY_FOR_AGREEMENT_FUND,
} from "../../constants";
import { ENTITY_LEVEL } from "../../../commons/constants";
import { useStores } from "../../useStores";
import AdjustmentDialog from "../../components/AdjustmentDialog";
import BookMMFDialog from "../../components/BookMMFDialog";
import GridHeader from "../GridHeader";
import DefaultBookMMFDialog from "../../components/DefaultBookMMFDialog";
import { DEFAULT_AGGREGATION, getColumns } from "./grid-columns";
import GridLegend from "../GridLegend";

/**
 * Renders the main grid based on the selected filters
 * @author burri
 */

const gridConfig = {
  clickableRows: true,
  showColumnAggregationRow: true,
  getExportFileName: ({ fileType, all }) => {
    return "liquidity-management-report";
  },
};

const Grid: React.FC<any> = () => {
  const { rootStore } = useStores();
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;

  let dataLevel = liquidityManagementReportStore.dataLevel;
  const gridColumns = React.useMemo(getColumns, []);

  const onColumnChange = (displayColumns) => {
    liquidityManagementReportStore.setDisplayColumns(displayColumns);
  };

  const [sortBy, setSortBy] = React.useState(
    dataLevel === ENTITY_LEVEL
      ? DEFAULT_SORT_BY
      : DEFAULT_SORT_BY_FOR_AGREEMENT_FUND
  );

  const onSortByChange = (sortBy) => {
    setSortBy(sortBy);
  };

  function renderGridData() {
    let grid;
    let gridData = liquidityManagementReportStore.liquidityDataToShow;
    let showGrid = liquidityManagementReportStore.liquidityData.length
      ? true
      : false;
    liquidityManagementReportStore.showGrid = showGrid;
    if (showGrid) {
      grid = (
        <>
          <ArcDataGrid
            rows={gridData}
            columns={gridColumns}
            configurations={gridConfig}
            clickedRow={liquidityManagementReportStore.clickedRow}
            onClickedRowChange={(clickedRow) => {
              liquidityManagementWorkflowStore.setClickedRow(clickedRow);
            }}
            displayColumns={liquidityManagementReportStore.displayColumns}
            onDisplayColumnsChange={onColumnChange}
            sortBy={sortBy}
            onSortByChange={onSortByChange}
            pinnedColumns={DEFAULT_PINNED_COLUMNS}
            aggregation={DEFAULT_AGGREGATION}
          />
          <div
            className="container--primary padding--horizontal padding--vertical--small"
            style={{
              position: "absolute",
              bottom: "-12px",
              left: "50%",
              transform: "translate(-50%, -50%)",
              borderRadius: "5px",
            }}
          >
            <GridLegend />
          </div>
        </>
      );
      let gridWithHeaders = (
        <>
          <Layout className="border">
            <Layout.Child childId="quickActionsHeader" size="fit">
              <GridHeader />
            </Layout.Child>
            <Layout.Child childId="liquidityManagementReportGrid">
              {grid}
            </Layout.Child>
          </Layout>
          <AdjustmentDialog />
          <BookMMFDialog />
          <DefaultBookMMFDialog />
        </>
      );
      return gridWithHeaders;
    } else {
      const messageType = liquidityManagementReportStore.searchStatus.error
        ? Message.Type.CRITICAL
        : Message.Type.PRIMARY;
      return (
        !liquidityManagementReportStore.searchStatus.inProgress && (
          <div className="margin--horizontal" style={{ textAlign: "center" }}>
            <Message
              children={liquidityManagementReportStore.searchStatus.message}
              type={messageType}
            />
          </div>
        )
      );
    }
  }

  return <>{renderGridData()}</>;
};

export default observer(Grid);
