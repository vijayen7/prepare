/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Layout, ToggleGroup } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import { INVEST, REDEEM, STAY } from "../../constants";
import { TokenCounter } from "./TokenCounter";
import { useStores } from "../../useStores";
import LegendDot from "../GridLegend/LegendDot";

/**
 * Renders the grid Header with required buttons and filters
 * @author burri
 */

const GridHeader: React.FC = () => {
  const { rootStore } = useStores();
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  return (
    <Layout className="padding--vertical" isColumnType={true}>
      <Layout.Child childId="GridHeaderChild1" size='100%'>
        <button onClick={liquidityManagementReportStore.handleRefresh}>
          <i
            className="icon-refresh margin--right"
            style={{ fontSize: "11px" }}
          />
          Refresh Data
        </button>
        <label className="size--content margin--left--large margin--right">
          Quick Filters:
        </label>
        <ToggleGroup
          multiSelect
          value={liquidityManagementReportStore.selectedQuickFilter}
          onChange={async (value) => {
            liquidityManagementReportStore.selectedQuickFilter = value;
            liquidityManagementReportStore.handleApplyQuickFilter();
          }}
        >
          <ToggleGroup.Button key="quick2" data="STAY">
            <>
              <LegendDot height="12px" color="primary" />
              &nbsp;
            </>
            Stay
            <TokenCounter
              value={liquidityManagementReportStore.quickFilterCounter[STAY]}
            />
          </ToggleGroup.Button>
          <ToggleGroup.Button key="quick3" data="REDEEM">
            <>
              <LegendDot height="12px" color="warning" />
              &nbsp;
            </>
            Redeem
            <TokenCounter
              value={liquidityManagementReportStore.quickFilterCounter[REDEEM]}
            />
          </ToggleGroup.Button>
          <ToggleGroup.Button key="quick4" data="INVEST">
            <>
              <LegendDot height="12px" color="success" />
              &nbsp;
            </>
            Invest
            <TokenCounter
              value={liquidityManagementReportStore.quickFilterCounter[INVEST]}
            />
          </ToggleGroup.Button>
        </ToggleGroup>
        <button
          className="size--content margin--right--small"
          style={{ height: "auto", float: "right" }}
          onClick={liquidityManagementReportStore.generateReportFromAPI}
        >
          Create Report
        </button>
      </Layout.Child>
      <Layout.Child childId="dummyChild"></Layout.Child>
    </Layout>
  );
};

export default observer(GridHeader);
