/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Panel, TabPanel } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import {
  MMF_DEFAULT_PINNED_COLUMNS,
  PERCENT,
  ZERO_PERCENT,
  CALL_PULL_DETAILS_TAB,
  CASH_FLOWS_TAB,
  CASH_BALANCES_TAB,
  MMF_BALANCES_TAB,
} from "../../constants";
import { AGREEMENT_LEVEL } from "../../../commons/constants";
import { useStores } from "../../useStores";
import {
  getCashFlowsWiresData,
  getMarginCallAndPullData,
  getMMFBalanceData,
  getOpeningCashBalanceData,
  getEDDataForAgreementFund,
} from "../../utils";
import UnderlyingDataGrid from "../UnderlyingDataGrid";
import {
  getCashFlowsDataColumns,
  getMarginCallAndPullDataColumns,
  getMMFBalanceDataColumns,
  getOpeningCashBalanceDataColumns,
} from "../UnderlyingDataGrid/grid-columns";

/**
 * Renders the particular bottom grid in the bottom panel based on the tab selected
 * @author burri
 */

const BottomPanel: React.FC<any> = () => {
  const { rootStore } = useStores();
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  return (
    <>
      <Panel
        dismissible
        onClose={() => liquidityManagementReportStore.setShowBottomPanel(false)}
        title={
          <div>
            {liquidityManagementReportStore.selectedFundData?.fundData?.name}{" "}
            Details
          </div>
        }
      >
        <div>
          {
            <UnderlyingFundDetails
              selectedFundData={liquidityManagementReportStore.selectedFundData}
              rowId={liquidityManagementReportStore.clickedRowId}
            />
          }
        </div>
      </Panel>
    </>
  );
};

const UnderlyingFundDetails = (props) => {
  const { rootStore } = useStores();
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;

  let dataLevel = liquidityManagementReportStore.dataLevel;
  const [selectedTabId, onSelectTabChange] = React.useState(CASH_BALANCES_TAB);

  let openingCashBalance = getOpeningCashBalanceData(props.selectedFundData);
  let enrichedMMFData = getMMFBalanceData(
    props.selectedFundData,
    liquidityManagementWorkflowStore
  );
  let marginCallAndPullData =
    dataLevel !== AGREEMENT_LEVEL
      ? getMarginCallAndPullData(props.selectedFundData)
      : getEDDataForAgreementFund(props.selectedFundData);
  let cashFlowWires = getCashFlowsWiresData(props.selectedFundData);
  let filesParsedPercentage = props.selectedFundData?.brokerFilesStatusTracker
    ?.parsedPercentage
    ? props.selectedFundData?.brokerFilesStatusTracker?.parsedPercentage?.toFixed(
      1
    ) + PERCENT
    : ZERO_PERCENT;

  liquidityManagementReportStore.fundLevelOwnershipThresholds =
    props.selectedFundData?.ownershipThresholds;
  liquidityManagementWorkflowStore.mmfBalancesList = [
    ...liquidityManagementWorkflowStore.tickerToNonZeroBalancesMap.values(),
  ];
  React.useEffect(() => {
    onSelectTabChange(liquidityManagementReportStore.bottomPanelTab);
  }, [liquidityManagementReportStore.clickedRowId]);

  return (
    <div className="margin--vertical">
      <TabPanel
        id="fundDetail"
        selectedTabId={selectedTabId}
        onSelect={onSelectTabChange}
      >
        {dataLevel !== AGREEMENT_LEVEL && (
          <TabPanel.Tab
            key={CASH_BALANCES_TAB}
            tabId={CASH_BALANCES_TAB}
            label="Opening Cash Balance"
          >
            <>
              <div
                className="margin--vertical--large margin--horizontal--large text-align--center"
                style={{ width: "550px" }}
              >
                {
                  <UnderlyingDataGrid
                    columns={getOpeningCashBalanceDataColumns()}
                    componentFundData={openingCashBalance}
                  />
                }
              </div>
            </>
          </TabPanel.Tab>
        )}
        <TabPanel.Tab
          key={CASH_FLOWS_TAB}
          tabId={CASH_FLOWS_TAB}
          label="Cash Flows"
        >
          <>
            <div className="margin--vertical--large margin--horizontal--large text-align--center">
              {
                <UnderlyingDataGrid
                  columns={getCashFlowsDataColumns}
                  componentFundData={cashFlowWires}
                />
              }
            </div>
          </>
        </TabPanel.Tab>
        <TabPanel.Tab
          key={CALL_PULL_DETAILS_TAB}
          tabId={CALL_PULL_DETAILS_TAB}
          label="Call & Pull Details"
        >
          <>
            {dataLevel !== AGREEMENT_LEVEL && (
              <div className="message--primary no-icon margin--vertical--large margin--horizontal--large text-align--center">
                <span>Files Parsed : {filesParsedPercentage}</span>
              </div>
            )}
            <div className="margin--vertical--large margin--horizontal--large text-align--center">
              {
                <UnderlyingDataGrid
                  columns={getMarginCallAndPullDataColumns()}
                  componentFundData={marginCallAndPullData}
                />
              }
            </div>
          </>
        </TabPanel.Tab>
        <TabPanel.Tab
          key={MMF_BALANCES_TAB}
          tabId={MMF_BALANCES_TAB}
          label="Entity MMF Balance"
        >
          <>
            <div className="margin--vertical--large margin--horizontal--large">
              {
                <UnderlyingDataGrid
                  columns={getMMFBalanceDataColumns()}
                  componentFundData={enrichedMMFData}
                  pinnedColumns={MMF_DEFAULT_PINNED_COLUMNS}
                  tabName={MMF_BALANCES_TAB}
                />
              }
            </div>
          </>
        </TabPanel.Tab>
      </TabPanel>
    </div>
  );
};

export default observer(BottomPanel);
