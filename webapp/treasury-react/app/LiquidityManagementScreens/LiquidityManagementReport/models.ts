/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import MapData from '../../commons/models/MapData';
import ReferenceData from "../../commons/models/ReferenceData";
import { CLASSES } from "../commons/constants";

/**
 * Contains all the models classes or interfaces
 * @author burri
 */


export interface SearchStatus {
  inProgress: boolean;
  error: boolean;
  message: string;
};

export interface AdjustmentFilter {
  selectedStartDate: string;
  selectedEndDate: string;
  selectedAmount: string;
  selectedComment: string;
  selectedAdjustmentType: MapData;
  selectedReasonCode: MapData;
};

export interface MMFGridData {
  tickerWithCustodianAccount: string;
  balanceUsd: number;
  closingBalanceUsd: number;
  amount?: number | null;
  aumUsd: number;
  entityOwnership: number;
  fundOwnership: number;
  fundOwnershipAmount: number;
  yield: number;
  expenseRatio: number;
  children: any;
}

export interface BookMMFFilter {
  [key: string]: any;
  selectedSubject: string;
  selectedCC: string;
  selectedSubscribers: string;
  selectedMMFSpn: MapData | null;
  selectedSourceAccount: MapData[];
  selectedDestinationAccount: MapData | null;
  isDefaultSourcePortfolioOverride?: boolean;
  selectedSourcePortfolio: MapData[];
  defaultSourcePortfolio?: MapData[];
  isDefaultDestinationPortfolioOverride?: boolean;
  selectedDestinationPortfolio: MapData[];
  defaultDestinationPortfolio?: MapData[];
  isDefaultTradeBookOverride?: boolean;
  defaultTradeBook?: MapData[];
  selectedTradeBook: MapData[];
  isDefaultTradeBundleOverride?: boolean;
  selectedBundles?: MapData[];
  defaultTradeBundle?: MapData[];
  selectedCustodianAccount: MapData | null;
  selectedTradeDate: string;
  selectedValueDate: string;
  selectedAmount: string;
  selectedComment: string;
  logWire: boolean;
  bookInternalTrade: boolean;
  isTrackingWire: boolean;
  bookSSTrade: boolean;
}

export interface TickerChildren {
  tickerWithCustodianAccount: string;
  custodianAccountId?: number | null;
  balanceUsd: number;
  closingBalanceUsd: number;
  amount: number | null;
  parentInfo?: any;
}

export interface LiquidityManagementInput {
  date: string;
  legalEntityFamilyIds?: number[];
  legalEntityIds?: number[];
  cpeIds?: number[];
  agreementTypeIds?: number[];
  "@CLASS": typeof CLASSES.LIQUIDITY_MANAGEMENT_INPUT;
}

export interface MMFDetailsInput {
  date: string;
  mmfSpn: number;
  fundId: number;
  custodianAccountIds: number[];
  mmfCustodianAccountId: number;
  isMMFBuy: boolean;
  "@CLASS": typeof CLASSES.MMF_DETAILS_INPUT;
}

export interface TradeWorkflowInputs {
  spn?: number;
  subject?: string;
  subscriberList?: string[];
  ccList?: string[];
  custodianAccountId: number;
  currencySpn: number;
  transactionType: string;
  bookId?: number;
  amount: number;
  metaData?: Map<string, string | undefined | null>;
  "@CLASS": typeof CLASSES.TRADE_WORKFLOW_INPUTS;
}

export interface Adjustment {
  sourceGridConfig: string;
  adjustmentTypeId: number;
  entityFamilyId?: number | null;
  tradingEntityId?: number | null;
  cpeId?: number | null;
  agreementTypeId?: number | null;
  startDate: string;
  endDate: string;
  reasonCodeId: number;
  adjustmentAmountInRptCcy: string | number;
  comment: string;
  "@CLASS": typeof CLASSES.ADJUSTMENT_INPUT;
}

export interface CutoffTime {
  spn: number;
  ticker: string;
  businessDayMMFThresholdTime: number;
  holidayMMFThresholdTime: number;
  user: string;
}

export interface FlattenedFundData {
  counter: number;
  fund?: string;
  fundId?: number;
  legalEntity?: string;
  legalEntityId?: number;
  counterParty?: string;
  counterPartyId?: number;
  agreementType?: string;
  agreementTypeId?: number;
  openingCashBalance: number;
  unEncumberedCashBalance: number;
  adjustedEd: number;
  adjustedDebitBalance: number;
  liquidityAction: string;
  liquidityActionAmount: number;
  netCashFlows: number;
  intradayFundingRequirement: number;
  externalCallData: number;
  internalCallData: number;
  externalPullData: number;
  internalPullData: number;
  externalEd: number;
  internalEd: number;
  externalDebitBalance: number;
  internalDebitBalance;
  otherCashOutFlows: number;
  cashOutFlows: number;
  adjustmentAmount: number;
  projectedClosingBalance: number;
  mmfBalance: number;
  tDayProjectedDebitBalance: number;
  tPlusOneProjectedDebitBalance: number;
  tPlusTwoProjectedDebitBalance: number;
}

export interface FirmLiquidityData {
  firmData?: ReferenceData;
  fundLiquidityData: FundLiquidityData[];
  mmfSummaryMap: Map<string, any>;
  ownershipThresholds: Map<string, number>;
  totalCashBalance: number;
  isTomorrowHoliday: boolean;
  "@CLASS": typeof CLASSES.FIRM_LIQUIDITY_DATA;
}

export interface FundLiquidityData {
  fundData: ReferenceData;
  ownershipThresholds: Map<string, number>;
  brokerFilesStatusTracker: any;
  underlyingData: any;
  componentDataMap: any;
  metaDataMap: Map<string, any>;
  "@CLASS": typeof CLASSES.FUND_LIQUIDITY_DATA;
}

export interface ApiResponse {
  successStatus: boolean;
  message: string;
  response: any;
  "@CLASS"?: string;
}

export interface WorkflowStatusDetail {
  workflowId: number;
  workflowState: string;
  workflowDate: string;
  subject: string;
  message: string;
  fundId: number;
}

export interface CurrentWorkflowStatus {
  workflowId: number;
  workflowState: string;
  message: string;
}
