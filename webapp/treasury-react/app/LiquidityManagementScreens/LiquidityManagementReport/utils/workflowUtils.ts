/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import {
  beginWorkflowApi,
  cancelAndBeginWorkflowApi,
  cancelWorkflowApi,
  publishWorkflowApi,
  getWorkflowStatusApi
} from "../api";
import {
  MESSAGES,
  WORKFLOW_TYPE
} from "../constants";
import { TOAST_TYPE, INPUT_FORMAT_PROPERTIES } from "../../commons/constants";
import { ApiResponse, WorkflowStatusDetail, CurrentWorkflowStatus } from "../models";
import { showToastService } from "../utils";

/**
 * Begins / Cancels / Publish Workflow operations related to MMF bookings
 * @author venkatsu
 */

export const beginWorkflow = async (
  date: string,
  fundId: number
): Promise<WorkflowStatusDetail | undefined> => {
  let workflowParams = getWorkflowParam(date, fundId);
  try {
    let data: ApiResponse = await beginWorkflowApi(workflowParams);
    if (data.successStatus == true) {
      return {
        workflowId: data.response.workflowId,
        workflowState: data.response.workflowState,
        workflowDate: data.response.workflowDate,
        subject: data.message,
        message: data.response.message,
        fundId: fundId,
      };
    }
  } catch (e) {
    showToastService(MESSAGES.INITIATE_WORKFLOW_FAILURE, TOAST_TYPE.CRITICAL);
  }
};

export const getWorkflowParam = (date: string, fundId: number) => {
  let workflowParam = "calmWorkflowManagementInput.date=" + date;
  workflowParam +=
    "&calmWorkflowManagementInput.workflowType=" + WORKFLOW_TYPE;
  workflowParam += "&calmWorkflowManagementInput.fundId=" + fundId;
  workflowParam += INPUT_FORMAT_PROPERTIES;
  return workflowParam;
};

export const publishWorkflow = async (workflowId: number) => {
  try {
    let wfParams = `workflowId=${workflowId}`;
    let data: ApiResponse = await publishWorkflowApi(wfParams);
  } catch (e) {
    showToastService(MESSAGES.WORKFLOW_PUBLISH_FAILURE, TOAST_TYPE.CRITICAL);
  }
};

export const cancelWorkflow = async (workflowId: number) => {
  try {
    let wfParams = `workflowId=${workflowId}`;
    let data: ApiResponse = await cancelWorkflowApi(wfParams);
  } catch (e) {
    showToastService(MESSAGES.CANCEL_WORKFLOW_FAILURE, TOAST_TYPE.CRITICAL);
  }
};

export const cancelAndBeginWorkflow = async (
  workflowId: number,
  date: string,
  fundId: number
) => {
  try {
    let wfParams = `cancelWorkflowId=${workflowId}&`;
    wfParams += getWorkflowParam(date, fundId);
    let data: ApiResponse = await cancelAndBeginWorkflowApi(wfParams);
    if (data.successStatus == true) {
      return {
        workflowId: data.response.workflowId,
        workflowState: data.response.workflowState,
        workflowDate: data.response.workflowDate,
        subject: data.message,
        message: data.response.message,
        fundId: fundId,
      };
    }
  } catch (e) {
    showToastService(MESSAGES.ERROR_MESSAGE, TOAST_TYPE.CRITICAL);
  }
};

export const getWorkflowStatus = async (
  workflowId: number
): Promise<CurrentWorkflowStatus | undefined> => {
  let workflowParams = `workflowId=${workflowId}`;
  try {
    let data: ApiResponse = await getWorkflowStatusApi(workflowParams);
    if (data.successStatus == true) {
      return {
        workflowId: data.response.workflowId,
        workflowState: data.response.workflowState,
        message: data.response.message,
      };
    }
  } catch (e) {
    showToastService(MESSAGES.INITIATE_WORKFLOW_FAILURE, TOAST_TYPE.CRITICAL);
  }
};
