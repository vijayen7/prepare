/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { action, observable } from "mobx";
import MapData from "../../../commons/models/MapData";
import {
  addAdjustment,
  fetchDefaultBookMMFDetailsForFund,
  fetchMMFBookingDetailsForFund,
  initiateTradeBooking
} from "../api";
import {
  INITIAL_ADJUSTMENT_FILTERS,
  INITIAL_API_RESPONSE,
  INITIAL_BOOK_MMF_FILTERS,
  INITIAL_SEARCH_STATUS,
  INITIAL_WORKFLOW_STATUS_DETAIL,
  MESSAGES,
  RAG_CATEGORY_CODE_MAP,
  RAG_THRESHOLD_DEFAULT_COLOR,
  ERROR_SEARCH_STATUS
} from "../constants";
import {
  CLASSES,
  ENTITY_LEVEL,
  TOAST_TYPE
} from "../../commons/constants";
import {
  Adjustment,
  AdjustmentFilter,
  ApiResponse,
  BookMMFFilter,
  CutoffTime,
  MMFDetailsInput,
  MMFGridData,
  TickerChildren,
  WorkflowStatusDetail
} from "../models";
import {
  getValueOrDefault,
  isMMFBoookingTimeValid,
  DefaultBookMMFOnReset,
  getInitateTradeBookingPayloadForBookMMF,
  getInitateTradeBookingPayloadForDefaultBookMMF,
  getTradeConfirmationMessage,
  validateWireAndTradeFilter,
  showToastService,
} from "../utils";
import {
  beginWorkflow,
  cancelAndBeginWorkflow,
  cancelWorkflow,
  publishWorkflow,
  getWorkflowStatus
} from '../utils/workflowUtils';
import LiquidityManagementReportStore from "./LiquidityManagementReportStore";
import RootStore from "./RootStore";
import {
  showMessageDialog,
  workflowCancelAndBeginDialogue
 } from "../components/GenericDialog";
 import {calculatePercentage} from "../../commons/utils";

/**
 * Contains all the state variables and defines action or computation on them for workflow
 * @author burri
 */

export class LiquidityManagementWorkflowStore {
  rootStore: RootStore;
  liquidityManagementReportStore: LiquidityManagementReportStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    this.liquidityManagementReportStore =
      this.rootStore.liquidityManagementReportStore;
  }

  @observable mmfBookingDetailsForFund: ApiResponse = INITIAL_API_RESPONSE;
  @observable defaultBookMMFDetailsForFund: ApiResponse = INITIAL_API_RESPONSE;
  @observable mmfToggleButtonValue = false;
  @observable mmfSummaryMapData: Map<string, any> = new Map();
  @observable mmfBalancesList: MMFGridData[] = [];
  @observable tickerToZeroBalancesMap: Map<string, any> = new Map();
  @observable tickerToNonZeroBalancesMap: Map<string, any> = new Map();
  @observable tickerToCutoffTimeMap: Map<string, CutoffTime> = new Map();
  @observable fundTokenCss: any;
  @observable firmTokenCss: any;

  //For Adjustments popup
  @observable showAdjustmentDialog: boolean = false;
  @observable adjustmentFilter: AdjustmentFilter = INITIAL_ADJUSTMENT_FILTERS;
  @observable adjustmentRowContext: any;
  @observable addAdjustmentSuccessDialog: boolean = false;
  @observable addAdjustmentMessage: string = "";
  @observable adjustmentFundName: string = "";
  //For Book MMF Popup
  @observable showBookMMFDialog: boolean = false;
  @observable showDefaultBookMMFDialog: boolean = false;
  @observable wireAndTradeMessage = "";
  @observable bookMMFFilter: BookMMFFilter = INITIAL_BOOK_MMF_FILTERS;
  @observable mmfDialogTitle: string = "";
  @observable workflowStatusDetail: WorkflowStatusDetail =
    INITIAL_WORKFLOW_STATUS_DETAIL;
  @observable isDefaultBookMMFDetailsForFund: boolean = false;
  @observable defaultBookMMFDetailsContext: any;
  mmfRowContext: any;

  @action.bound
  onSelectForAdjustment = (params) => {
    const { key, value } = params;
    const selectedFilters = { ...this.adjustmentFilter };
    selectedFilters[key] = value;
    this.adjustmentFilter = selectedFilters;
  };

  @action.bound
  onSelectForBookMMF = (params) => {
    const { key, value } = params;
    const selectedFilters = { ...this.bookMMFFilter };
    selectedFilters[key] = value;
    if (key === "selectedTradeDate") {
      selectedFilters["selectedValueDate"] = value;
    }
    this.bookMMFFilter = selectedFilters;
  };

  @action.bound
  setClickedRow = (value: any) => {
    this.mmfToggleButtonValue = false;
    this.tickerToZeroBalancesMap = new Map();
    this.tickerToNonZeroBalancesMap = new Map();
    this.liquidityManagementReportStore.clickedRow = value;
    this.liquidityManagementReportStore.clickedRowId = value;
  };

  @action.bound
  onMMFToggleButtonValueChanged = (value: boolean) => {
    this.mmfToggleButtonValue = value;
    if (this.mmfToggleButtonValue === true) {
      this.mmfBalancesList = [
        ...this.tickerToNonZeroBalancesMap.values(),
        ...this.tickerToZeroBalancesMap.values(),
      ];
    } else {
      this.mmfBalancesList = [...this.tickerToNonZeroBalancesMap.values()];
    }
  };

  @action.bound
  onChangeAmount = (context: any, value: number) => {
    if (isNaN(value) || (value < 0 &&
      Math.abs(value) > parseFloat(context.row.balanceUsd))) {
      showMessageDialog(MESSAGES.AMOUNT_ERROR, MESSAGES.AMOUNT_MESSAGE);
    } else {
      let totalAmt: number = 0;
      let mmfBal = this.tickerToZeroBalancesMap.has(
        context.row.parentInfo.ticker
      )
        ? this.tickerToZeroBalancesMap.get(context.row.parentInfo.ticker)
        : this.tickerToNonZeroBalancesMap.get(context.row.parentInfo.ticker);

      let childrenData: TickerChildren[] = [];
      mmfBal.children.forEach((child) => {
        if (
          child.tickerWithCustodianAccount ===
          context.row.tickerWithCustodianAccount
        ) {
          child.amount = +value;
        }
        totalAmt += child.amount;
        childrenData.push(child);
      });
      mmfBal.amount = totalAmt;
      mmfBal.children = childrenData;
      mmfBal.entityOwnership = calculatePercentage(mmfBal.amount + mmfBal.closingBalanceUsd, mmfBal.aumUsd);
      mmfBal.fundOwnership = calculatePercentage(mmfBal.amount + mmfBal.fundOwnershipAmount, mmfBal.aumUsd);
      context.row.parentInfo.entityOwnership = mmfBal.entityOwnership;
      context.row.parentInfo.fundOwnership = mmfBal.fundOwnership;
      if (this.tickerToZeroBalancesMap.has(context.row.parentInfo.ticker)) {
        this.tickerToZeroBalancesMap.set(context.row.parentInfo.ticker, mmfBal);
      } else {
        this.tickerToNonZeroBalancesMap.set(
          context.row.parentInfo.ticker,
          mmfBal
        );
      }
      if (this.mmfToggleButtonValue === true) {
        this.mmfBalancesList = [
          ...this.tickerToNonZeroBalancesMap.values(),
          ...this.tickerToZeroBalancesMap.values(),
        ];
      } else {
        this.mmfBalancesList = [...this.tickerToNonZeroBalancesMap.values()];
      }
    }
  };

  @action.bound
  toggleShowDialog = () => {
    if (this.showAdjustmentDialog) {
      this.adjustmentFilter = INITIAL_ADJUSTMENT_FILTERS;
    }
    this.adjustmentFilter.selectedStartDate =
      this.rootStore.liquidityManagementReportStore.searchFilter.selectedDate;
    this.adjustmentFilter.selectedEndDate =
      this.rootStore.liquidityManagementReportStore.searchFilter.selectedDate;
    this.showAdjustmentDialog = !this.showAdjustmentDialog;
  };

  @action.bound
  toggleAddAdjustmentStatusDialog = () => {
    this.addAdjustmentSuccessDialog = !this.addAdjustmentSuccessDialog;
  };

  @action.bound
  toggleShowMMFDialog = () => {
    if (this.showBookMMFDialog) {
      this.bookMMFFilter = INITIAL_BOOK_MMF_FILTERS;
      cancelWorkflow(this.workflowStatusDetail.workflowId);
      this.workflowStatusDetail = INITIAL_WORKFLOW_STATUS_DETAIL;
    }
    this.resetMMFDialogFlags();
    this.showBookMMFDialog = !this.showBookMMFDialog;
  };

  @action.bound
  resetMMFDialogFlags = () => {
    this.bookMMFFilter.logWire = this.liquidityManagementReportStore.enableWireLogging;
    this.bookMMFFilter.bookInternalTrade = this.liquidityManagementReportStore.enableInternalTradeBooking;
    this.bookMMFFilter.selectedTradeDate =
      this.liquidityManagementReportStore.searchFilter.selectedDate;
    this.bookMMFFilter.selectedValueDate =
      this.liquidityManagementReportStore.searchFilter.selectedDate;
  }

  @action.bound
  toggleShowDefaultBookMMFDialog = () => {
    if (this.showDefaultBookMMFDialog) {
      this.bookMMFFilter = INITIAL_BOOK_MMF_FILTERS;
      cancelWorkflow(this.workflowStatusDetail.workflowId);
      this.workflowStatusDetail = INITIAL_WORKFLOW_STATUS_DETAIL;
    }
    this.resetMMFDialogFlags();
    this.showDefaultBookMMFDialog = !this.showDefaultBookMMFDialog;
  };

  @action
  handleAdjustmentSave = async () => {
    try {
      this.showAdjustmentDialog = false;
      this.liquidityManagementReportStore.searchStatus = INITIAL_SEARCH_STATUS;
      this.liquidityManagementReportStore.searchStatus.inProgress = true;
      const payload: Adjustment = {
        sourceGridConfig: "ADJUSTMENT",
        adjustmentTypeId: this.adjustmentFilter.selectedAdjustmentType.key,
        entityFamilyId:
          this.liquidityManagementReportStore.dataLevel === ENTITY_LEVEL
            ? this.adjustmentRowContext.fundId
            : null,
        tradingEntityId: this.adjustmentRowContext.legalEntityId,
        cpeId: this.adjustmentRowContext.counterPartyId,
        agreementTypeId: this.adjustmentRowContext.agreementTypeId,
        startDate: this.adjustmentFilter.selectedStartDate,
        endDate: this.adjustmentFilter.selectedEndDate,
        reasonCodeId: this.adjustmentFilter.selectedReasonCode.key,
        adjustmentAmountInRptCcy: this.adjustmentFilter.selectedAmount
          ? this.adjustmentFilter.selectedAmount.toString().replace(/,/g, "")
          : 0,
        comment: this.adjustmentFilter.selectedComment,
        "@CLASS": CLASSES.ADJUSTMENT_INPUT,
      };
      let adjustmentResponse = await addAdjustment(payload);
      await this.liquidityManagementReportStore.handleRefresh();
      this.addAdjustmentMessage = adjustmentResponse[0].message;
      this.toggleAddAdjustmentStatusDialog();
      this.adjustmentFilter = INITIAL_ADJUSTMENT_FILTERS;
    } catch (e) {
      this.liquidityManagementReportStore.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_ADJUSTMENT_MESSAGE,
      };
      this.addAdjustmentMessage = MESSAGES.ERROR_ADJUSTMENT_MESSAGE;
      this.toggleAddAdjustmentStatusDialog();
    }
  };

  @action
  handleAdjustmentReset = async () => {
    this.adjustmentFilter = INITIAL_ADJUSTMENT_FILTERS;
  };

  @action
  validateAndConfirmMMFInputs = async () => {
    let isFiltersValid = validateWireAndTradeFilter(
      this.bookMMFFilter,
      this.rootStore.liquidityManagementWorkflowStore
    );
    if (isFiltersValid) {
      this.wireAndTradeMessage = getTradeConfirmationMessage(
        this.bookMMFFilter
      );
    } else return;
    this.liquidityManagementReportStore.searchStatus.inProgress = true;
    let selectedTicker = this.bookMMFFilter.selectedMMFSpn?.value;
    if (
      selectedTicker !== undefined &&
      !isMMFBoookingTimeValid(
        this.rootStore.liquidityManagementReportStore,
        this.rootStore.liquidityManagementWorkflowStore,
        this.tickerToCutoffTimeMap.get(selectedTicker)
      )
    ) {
      return;
    }
  };

  @action
  handleBookMMFSave = async () => {
    this.liquidityManagementReportStore.searchStatus.inProgress = true;
    let input = this.showBookMMFDialog
      ? getInitateTradeBookingPayloadForBookMMF(this.bookMMFFilter)
      : getInitateTradeBookingPayloadForDefaultBookMMF(this.bookMMFFilter);
    let currentWorkflowStatus = await getWorkflowStatus(this.workflowStatusDetail.workflowId);
    if (currentWorkflowStatus !== undefined && currentWorkflowStatus.workflowState === MESSAGES.IN_PROGRESS) {
      let response = await initiateTradeBooking(input);
      publishWorkflow(this.workflowStatusDetail.workflowId);
      this.setDefaultWorkflow();
      showMessageDialog(
        MESSAGES.TRADE_AND_WIRE_CONFIRMATION,
        response?.message
      );
      this.onChangeAmount(this.mmfRowContext, 0);
    } else {
      this.setDefaultWorkflow();
      if(currentWorkflowStatus !== undefined ){
        showMessageDialog(
          MESSAGES.INVALID_WORKFLOW,
          currentWorkflowStatus.message
        );
      }
    }
  };

  setDefaultWorkflow = () => {
    this.liquidityManagementReportStore.searchStatus.inProgress = false;
    this.workflowStatusDetail = INITIAL_WORKFLOW_STATUS_DETAIL;
      this.showBookMMFDialog = false;
      this.showDefaultBookMMFDialog = false;
      this.handleBookMMFReset();
  };

  @action.bound
  handleBookMMFReset = () => {
    this.bookMMFFilter = INITIAL_BOOK_MMF_FILTERS;
    this.resetMMFDialogFlags();
    this.bookMMFFilter.selectedMMFSpn = null;
    this.bookMMFFilter.selectedAmount = "";
    this.bookMMFFilter.selectedDestinationAccount = null;
    this.bookMMFFilter.selectedCustodianAccount = null;
  };

  @action.bound
  handleDefaultBookMMFReset = () => {
    let currentDefaultBookMMFFilter = this.bookMMFFilter;
    this.bookMMFFilter = DefaultBookMMFOnReset(currentDefaultBookMMFFilter);
    this.resetMMFDialogFlags();
  };

  @action
  startWorkflow = async (
    fundId: number,
    fundName: string,
    context: any,
    isDefaultBookMMFDetailsForFund: boolean
  ) => {
    try {
      this.liquidityManagementReportStore.searchStatus.inProgress = true;
      let workflowStatusDetail = await beginWorkflow(
        this.liquidityManagementReportStore.searchFilter.selectedDate,
        fundId
      );
      if (
        workflowStatusDetail !== undefined &&
        workflowStatusDetail.workflowState === MESSAGES.BEGIN
      ) {
        this.workflowStatusDetail = workflowStatusDetail;
        if (isDefaultBookMMFDetailsForFund === true) {
          await this.getDefaultBookMMFDetailsForFund(fundId, context);
        } else {
          await this.getMMFBookingDetailsForFund(fundId, fundName);
        }
      } else if (
        workflowStatusDetail !== undefined &&
        (workflowStatusDetail.workflowState === MESSAGES.IN_PROGRESS)
      ) {
        this.defaultBookMMFDetailsContext = context;
        this.isDefaultBookMMFDetailsForFund = isDefaultBookMMFDetailsForFund;
        this.workflowStatusDetail = workflowStatusDetail;
        this.mmfDialogTitle = fundName;
        workflowCancelAndBeginDialogue(MESSAGES.WORKFLOW_CONFLICT, this);
      }
      this.liquidityManagementReportStore.searchStatus.inProgress = false;
    } catch (e) {
      this.liquidityManagementReportStore.searchStatus = ERROR_SEARCH_STATUS;
    }
  };

  @action
  cancelAndBeginNewWorkflow = async () => {
    try {
      this.liquidityManagementReportStore.searchStatus.inProgress = true;
      let workflowStatusDetail = await cancelAndBeginWorkflow(
        this.workflowStatusDetail.workflowId,
        this.liquidityManagementReportStore.searchFilter.selectedDate,
        this.workflowStatusDetail.fundId
      );
      if (
        workflowStatusDetail !== undefined &&
        workflowStatusDetail.workflowState === MESSAGES.BEGIN
      ) {
        this.workflowStatusDetail = workflowStatusDetail;
        if (this.isDefaultBookMMFDetailsForFund === true) {
          await this.getDefaultBookMMFDetailsForFund(
            this.workflowStatusDetail.fundId,
            this.defaultBookMMFDetailsContext
          );
        } else {
          await this.getMMFBookingDetailsForFund(
            this.workflowStatusDetail.fundId,
            this.mmfDialogTitle
          );
        }
      } else {
        showToastService(MESSAGES.ERROR_MESSAGE, TOAST_TYPE.CRITICAL);
      }
      this.liquidityManagementReportStore.searchStatus.inProgress = false;
    } catch (e) {
      this.liquidityManagementReportStore.searchStatus = ERROR_SEARCH_STATUS;
    }
  };

  @action
  getMMFBookingDetailsForFund = async (fundId: number, name: string) => {
    try {
      this.liquidityManagementReportStore.searchStatus = INITIAL_SEARCH_STATUS;
      this.liquidityManagementReportStore.searchStatus.inProgress = true;
      this.mmfBookingDetailsForFund = await fetchMMFBookingDetailsForFund(
        fundId
      );
      this.handleBookMMFReset();
      this.liquidityManagementReportStore.searchStatus.inProgress = false;
      this.mmfDialogTitle = name;
      this.toggleShowMMFDialog();
    } catch (e) {
      this.liquidityManagementReportStore.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  };

  @action
  getDefaultBookMMFDetailsForFund = async (fundId: number, context: any) => {
    try {
      let mmfSpnMapData: MapData = {
        key: context.row.parentInfo.spn,
        value: context.row.parentInfo.ticker,
      };
      let custodianAccountIds: number[] = [];
      let cashBalances =
        this.liquidityManagementReportStore.selectedFundData?.underlyingData?.SOD_CASH_BALANCES;
      if (cashBalances !== undefined && cashBalances !== null) {
        for (let cashBalance of cashBalances) {
          custodianAccountIds.push(cashBalance.custodianAccount?.custodianAccountId);
        }
      }
      let payload: MMFDetailsInput = {
        date: this.liquidityManagementReportStore.searchFilter.selectedDate,
        mmfSpn: context.row.parentInfo.spn,
        fundId: fundId,
        isMMFBuy: parseFloat(context.row.amount) >= 0 ? true : false,
        custodianAccountIds: custodianAccountIds,
        mmfCustodianAccountId: context.row.custodianAccountId,
        "@CLASS": CLASSES.MMF_DETAILS_INPUT,
      };
      this.liquidityManagementReportStore.searchStatus = INITIAL_SEARCH_STATUS;
      this.liquidityManagementReportStore.searchStatus.inProgress = true;
      this.defaultBookMMFDetailsForFund = await fetchDefaultBookMMFDetailsForFund(payload);
      this.liquidityManagementReportStore.searchStatus.inProgress = false;
      let fundOwnershipColor = this.compare(
        this.liquidityManagementReportStore.fundLevelOwnershipThresholds,
        context.row.parentInfo.entityOwnership
      );
      let firmOwnershipColor = this.compare(
        this.liquidityManagementReportStore.firmLevelOwnershipThresholds,
        context.row.parentInfo.fundOwnership
      );
      this.fundTokenCss = getValueOrDefault(
        RAG_CATEGORY_CODE_MAP[fundOwnershipColor],
        ""
      );
      this.firmTokenCss = getValueOrDefault(
        RAG_CATEGORY_CODE_MAP[firmOwnershipColor],
        ""
      );
      this.bookMMFFilter.selectedAmount = context.row.amount;
      this.bookMMFFilter.selectedCustodianAccount = {
        key: context.row.custodianAccountId,
        value: context.row.tickerWithCustodianAccount,
      };
      this.bookMMFFilter.selectedMMFSpn = mmfSpnMapData;
      this.mmfRowContext = context;
      this.toggleShowDefaultBookMMFDialog();
    } catch (e) {
      this.liquidityManagementReportStore.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  };

  compare = (ownershipThresholds: Map<string, number>, targetValue: number) => {
    for (let key in ownershipThresholds) {
      let value = ownershipThresholds[key];
      if (targetValue >= value) {
        return key;
      }
    }
    return RAG_THRESHOLD_DEFAULT_COLOR;
  };
}

export default LiquidityManagementWorkflowStore;
