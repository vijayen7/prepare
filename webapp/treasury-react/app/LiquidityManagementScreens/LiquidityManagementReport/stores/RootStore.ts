/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import LiquidityManagementReportStore from "./LiquidityManagementReportStore";
import LiquidityManagementWorkflowStore from "./LiquidityManagementWorkflowStore";

/**
 * Root store for report and workflow stores
 * @author burri
 */
export class RootStore {
  liquidityManagementReportStore: LiquidityManagementReportStore;
  liquidityManagementWorkflowStore: LiquidityManagementWorkflowStore;
  constructor() {
    this.liquidityManagementReportStore = new LiquidityManagementReportStore(this);
    this.liquidityManagementWorkflowStore = new LiquidityManagementWorkflowStore(this);
  }
}

export default RootStore;
