/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { action, computed, observable } from "mobx";
import MapData from "../../../commons/models/MapData";
import { fetchLiquidityReportData, fetchLiquidityConfigData, getReportingUrl } from "../api";
import {
  DEFAULT_COLUMNS,
  INITIAL_QF_COUNTER,
  INITIAL_SEARCH_STATUS,
  MESSAGES,
  UNMAPPED_FUND_ID,
  DEFAULT_COLUMNS_FOR_AGREEMENT_FUND,
  INITIAL_API_RESPONSE
} from "../constants";
import { CLASSES, TOAST_TYPE, ENTITY_LEVEL, INITIAL_FILTERS, URLS } from "../../commons/constants";
import {
  CONFIG_OBJECTS,
  INTERNAL_TRADE_BOOKING_IN_SCOPE,
  WIRES_LOGGING_IN_SCOPE
} from "../../LiquidityManagementConfig/constants";
import {
  SearchStatus,
  LiquidityManagementInput,
  FlattenedFundData,
  FirmLiquidityData,
} from "../models";
import { SearchFilter } from "../../commons/models";
import { ApiResponse, LiquidityConfigData } from "../../LiquidityManagementConfig/models";
import {
  applyQuickFilter,
  flattenDictToList,
  showToastService,
  addWithNullChecks,
} from "../utils";
import { getExpandedGlobalSelections, getURLFromSelectedFilters } from "../../commons/utils";
import RootStore from "./RootStore";
import LiquidityManagementWorkflowStore from "./LiquidityManagementWorkflowStore";
import { openReportManager } from "../../../commons/util";

/**
 * Contains all the state variables and defines action or computation on them for report
 * @author burri
 */

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

const copy = require("clipboard-copy");
export class LiquidityManagementReportStore {
  rootStore: RootStore;
  liquidityManagementWorkflowStore: LiquidityManagementWorkflowStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    this.liquidityManagementWorkflowStore =
      this.rootStore.liquidityManagementWorkflowStore;
  }
  @observable dataLevel =
    parseInt(CODEX_PROPERTIES["com.arcesium.treasury.calm.dataLevelId"])
  @observable searchFilter: SearchFilter = INITIAL_FILTERS;
  @observable searchStatus: SearchStatus = INITIAL_SEARCH_STATUS;
  @observable displayColumns: string[] =
    this.dataLevel === ENTITY_LEVEL
      ? DEFAULT_COLUMNS
      : DEFAULT_COLUMNS_FOR_AGREEMENT_FUND;
  @observable quickFilterCounter: any = INITIAL_QF_COUNTER;
  @observable selectedQuickFilter: Array<string> = [];

  @observable legalEntityDataList: MapData[] = [];
  @observable legalEntityFamilyDataList: MapData[] = [];
  @observable toggleSidebar: boolean = false;

  @observable.shallow liquidityData: Array<FlattenedFundData> = [];
  @observable.shallow liquidityDataToShow: Array<FlattenedFundData> = [];
  @observable.shallow liquidityConfigData: ApiResponse = INITIAL_API_RESPONSE;

  @observable clickedRowId: number | string | undefined = undefined;
  @observable clickedRow: number | string | undefined = undefined;
  @observable bottomPanelTab: any = undefined;
  @observable showBottomPanel: boolean = false;
  @observable showGrid: boolean = false;

  @observable fundLevelOwnershipThresholds: Map<string, number> = new Map();
  @observable firmLevelOwnershipThresholds: Map<string, number> = new Map();
  @observable isTomorrowHoliday: boolean = false;

  @observable keyToFundLiquidityDataMap = {};

  @action.bound
  setExpandedGlobalSelections(
    userSelections: any,
    expandedGlobalSelection: any
  ) {
    let globalSelection = getExpandedGlobalSelections(
      userSelections,
      expandedGlobalSelection
    );
    this.legalEntityDataList = globalSelection.legalEntityDataList;
    this.legalEntityFamilyDataList = globalSelection.legalEntityFamilyDataList;
  }

  @action.bound
  onSelect = (params) => {
    const { key, value } = params;
    const selectedFilters = { ...this.searchFilter };
    selectedFilters[key] = value;
    this.setFilters(selectedFilters);
  };

  @action.bound
  applySavedFilters = (savedFilters: SearchFilter) => {
    this.setFilters(savedFilters);
  };

  @action.bound
  setDisplayColumns(displayColumns: []) {
    this.displayColumns = displayColumns;
  }

  @action
  setFilters = (newFilter: SearchFilter) => {
    this.searchFilter = newFilter;
  };

  @action.bound
  setToggleSidebar = (value: boolean) => {
    this.toggleSidebar = value;
  };

  @action.bound
  setClickedRowId = (value: any) => {
    this.clickedRowId = value;
  };

  @action.bound
  setBottomPanelTab = (value: any) => {
    this.bottomPanelTab = value;
  };

  @action
  handleApplyQuickFilter = () => {
    this.liquidityDataToShow = applyQuickFilter(
      this.liquidityData,
      this.selectedQuickFilter
    );
  };

  @action
  handleSearch = async () => {
    this.setToggleSidebar(true);
    this.resetLiquidityReportGridData();

    await Promise.all([
      this.getLiquidityReportData(),
      this.getLiquidityConfigData()
    ]);
    this.selectedQuickFilter = [];
    this.handleApplyQuickFilter();
  };

  @action.bound
  handleCopySearch = () => {
    let location = window.location.origin;
    let url = getURLFromSelectedFilters(this.searchFilter, location, URLS.LIQUIDITY_MANAGEMENT_REPORT);
    copy(url);
    showToastService("Search URL copied to clipboard.", TOAST_TYPE.INFO);
  };

  @action
  handleRefresh = async () => {
    this.setToggleSidebar(true);
    this.setShowBottomPanel(false);
    await this.getLiquidityReportData();
    this.selectedQuickFilter = [];
    this.handleApplyQuickFilter();
  };

  @action.bound
  handleReset = () => {
    this.resetFilters();
  };

  @action
  resetFilters = () => {
    this.searchFilter = INITIAL_FILTERS;
    this.searchStatus = INITIAL_SEARCH_STATUS;
  };

  @action
  resetLiquidityReportGridData = () => {
    this.liquidityData = [];
    this.liquidityDataToShow = [];
    this.showBottomPanel = false;
  };

  @action
  setShowBottomPanel = (value) => {
    this.showBottomPanel = value;
  };

  @computed
  get selectedFundData() {
    if (this.clickedRowId !== null && this.clickedRowId !== undefined) {
      let rowId = String(this.clickedRowId).split("_")[0];
      return this.keyToFundLiquidityDataMap[rowId];
    }
    return undefined;
  }

  @computed
  get selectedFundName() {
    if (this.clickedRow !== null && this.clickedRow !== undefined) {
      return this.keyToFundLiquidityDataMap[this.clickedRow]?.fundData?.name;
    }
    return undefined;
  }

  @computed
  get selectedFundId() {
    if (this.clickedRow !== null && this.clickedRow !== undefined) {
      return this.keyToFundLiquidityDataMap[this.clickedRow]?.fundData?.id;
    }
    return undefined;
  }

  @computed
  get enableInternalTradeBooking() {
    let tradeInitiationConfig: LiquidityConfigData[] =
      this.liquidityConfigData?.response?.at(1)?.filter(
        (x) => x?.objectKey?.id === CONFIG_OBJECTS.TRADE_INITIATION
      )[0]?.config;
    let internalTradeBookingInScope =
      tradeInitiationConfig?.at(0)?.attrConfigPairs?.filter(
        (x) => x.key === INTERNAL_TRADE_BOOKING_IN_SCOPE
      )[0]?.value;
    return internalTradeBookingInScope;
  }

  @computed
  get enableWireLogging() {
    let wiresConfig: LiquidityConfigData[] =
      this.liquidityConfigData?.response?.at(1)?.filter(
        (x) => x?.objectKey?.id === CONFIG_OBJECTS.WIRES_CONFIGURATION
      )[0]?.config;
    let wiresLoggingInScope =
      wiresConfig?.at(0)?.attrConfigPairs?.filter(
        (x) => x.key === WIRES_LOGGING_IN_SCOPE
      )[0]?.value;
    return wiresLoggingInScope;
  }

  @action
  getLiquidityConfigData = async () => {
    try {
      this.liquidityConfigData = await fetchLiquidityConfigData();
    } catch (e) {
      this.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  }

  @action
  getLiquidityReportData = async () => {
    this.searchStatus = INITIAL_SEARCH_STATUS;
    this.searchStatus.inProgress = true;

    try {
      let input = this.getLiquidityReportPayload();
      const liquidityReportData = await fetchLiquidityReportData(input);
      this.liquidityData = this.getFlattenedReportData(
        liquidityReportData?.response
      );

      if (
        liquidityReportData?.response === undefined ||
        liquidityReportData?.response?.fundLiquidityData?.length == 0
      ) {
        this.searchStatus = {
          inProgress: false,
          error: false,
          message: MESSAGES.NO_DATA_FOUND_MESSAGE,
        };
      } else {
        this.searchStatus.inProgress = false;
      }
    } catch (e) {
      this.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  };

  @action
  getLiquidityReportPayload = () => {
    let input: LiquidityManagementInput = {
      date: this.searchFilter.selectedDate,
      legalEntityFamilyIds: flattenDictToList(
        this.searchFilter.selectedLegalEntityFamilies
      ),
      legalEntityIds: flattenDictToList(
        this.searchFilter.selectedLegalEntities
      ),
      cpeIds: flattenDictToList(this.searchFilter.selectedCpes),
      agreementTypeIds: flattenDictToList(
        this.searchFilter.selectedAgreementTypes
      ),
      "@CLASS": CLASSES.LIQUIDITY_MANAGEMENT_INPUT,
    };
    return input;
  };

  @action.bound
  getFlattenedReportData = (data: FirmLiquidityData) => {
    this.liquidityManagementWorkflowStore =
      this.rootStore.liquidityManagementWorkflowStore;
    this.quickFilterCounter = INITIAL_QF_COUNTER;
    let flattenedReportData: FlattenedFundData[] = [];
    if (data !== null && data !== undefined) {
      this.liquidityManagementWorkflowStore.mmfSummaryMapData =
        data.mmfSummaryMap;
      this.firmLevelOwnershipThresholds = data.ownershipThresholds;
      this.isTomorrowHoliday = data.isTomorrowHoliday;
      let counter: number = 0;
      data.fundLiquidityData?.forEach((fundLiquidityData) => {
        if (fundLiquidityData?.fundData?.id === UNMAPPED_FUND_ID) return;
        let flattenedFundData: FlattenedFundData = {
          counter: counter,
          fund: fundLiquidityData?.fundData?.name,
          fundId: fundLiquidityData?.fundData?.id,
          legalEntity: fundLiquidityData?.metaDataMap["LEGAL_ENTITY"]?.name,
          legalEntityId: fundLiquidityData?.metaDataMap["LEGAL_ENTITY"]?.id,
          counterParty:
            fundLiquidityData?.metaDataMap["COUNTER_PARTY_ENTITY"]?.name,
          counterPartyId:
            fundLiquidityData?.metaDataMap["COUNTER_PARTY_ENTITY"]?.id,
          agreementType: fundLiquidityData?.metaDataMap["AGREEMENT_TYPE"]?.name,
          agreementTypeId: fundLiquidityData?.metaDataMap["AGREEMENT_TYPE"]?.id,
          openingCashBalance:
            fundLiquidityData?.componentDataMap?.SOD_CASH_BALANCES,
          unEncumberedCashBalance:
            fundLiquidityData?.componentDataMap?.SOD_CASH_BALANCES?.cashBalance,
          adjustedEd:
            fundLiquidityData?.componentDataMap?.SOD_CASH_BALANCES?.adjustedEd,
          adjustedDebitBalance:
            fundLiquidityData?.componentDataMap?.SOD_CASH_BALANCES
              ?.adjustedDebitBalance,
          liquidityAction:
            fundLiquidityData?.componentDataMap?.LIQUIDITY_ACTION?.action,
          liquidityActionAmount:
            fundLiquidityData?.componentDataMap?.LIQUIDITY_ACTION?.actionAmount,
          netCashFlows: fundLiquidityData?.componentDataMap?.NET_CASH_FLOW,
          intradayFundingRequirement:
            fundLiquidityData?.componentDataMap?.INTRA_DAY_FUNDING_REQUIREMENT,
          externalCallData:
            fundLiquidityData?.componentDataMap?.MARGIN_CALL_AND_PULL
              ?.brokerCallUsd,
          internalCallData:
            fundLiquidityData?.componentDataMap?.MARGIN_CALL_AND_PULL
              ?.internalCallUsd,
          externalPullData:
            fundLiquidityData?.componentDataMap?.MARGIN_CALL_AND_PULL
              ?.brokerPullUsd,
          internalPullData:
            fundLiquidityData?.componentDataMap?.MARGIN_CALL_AND_PULL
              ?.internalPullUsd,
          externalEd:
            fundLiquidityData?.componentDataMap?.MARGIN_CALL_AND_PULL
              ?.externalEd,
          internalEd:
            fundLiquidityData?.componentDataMap?.MARGIN_CALL_AND_PULL
              ?.internalEd,
          externalDebitBalance:
            fundLiquidityData?.componentDataMap?.DEBIT_BALANCES
              ?.externalDebitBalance,
          internalDebitBalance:
            fundLiquidityData?.componentDataMap?.DEBIT_BALANCES
              ?.internalDebitBalance,
          tDayProjectedDebitBalance:
            fundLiquidityData?.componentDataMap?.DEBIT_BALANCES
              ?.tPlusOneExternalDebitBalance,
          tPlusOneProjectedDebitBalance:
            fundLiquidityData?.componentDataMap?.DEBIT_BALANCES
              ?.tPlusTwoExternalDebitBalance,
          tPlusTwoProjectedDebitBalance:
            fundLiquidityData?.componentDataMap?.DEBIT_BALANCES
              ?.tPlusThreeExternalDebitBalance,
          otherCashOutFlows:
            fundLiquidityData?.componentDataMap?.OTHER_CASH_FLOW,
          cashOutFlows:
            fundLiquidityData?.componentDataMap?.CASH_OUTFLOWS,
          adjustmentAmount: fundLiquidityData?.componentDataMap?.ADJUSTMENTS,
          projectedClosingBalance:
            fundLiquidityData?.componentDataMap?.PROJECTED_CLOSING_BALANCE,
          mmfBalance: addWithNullChecks(
            fundLiquidityData?.componentDataMap?.MMF_HOLDINGS,
            fundLiquidityData?.componentDataMap?.SEG_IA_MMF_HOLDINGS
          ),
        };
        this.keyToFundLiquidityDataMap[counter++] = fundLiquidityData;
        let mmfList = fundLiquidityData?.underlyingData?.MMF_HOLDINGS;
        mmfList?.forEach((mmfBal) => {
          this.liquidityManagementWorkflowStore.tickerToCutoffTimeMap.set(
            mmfBal?.mmf?.ticker,
            mmfBal?.mmf?.cutoffTime
          );
        });

        if (
          fundLiquidityData?.componentDataMap?.LIQUIDITY_ACTION?.action !==
          null &&
          fundLiquidityData?.componentDataMap?.LIQUIDITY_ACTION?.action !==
          undefined
        ) {
          let curValue =
            this.quickFilterCounter[
            fundLiquidityData?.componentDataMap?.LIQUIDITY_ACTION?.action
            ];
          this.quickFilterCounter[
            fundLiquidityData?.componentDataMap?.LIQUIDITY_ACTION?.action
          ] = curValue + 1;
        }
        flattenedReportData.push(flattenedFundData);
      });
    }
    return flattenedReportData;
  };

  @action.bound
  generateReportFromAPI() {
    let input = this.getLiquidityReportPayload()
    let url = getReportingUrl(input)
    openReportManager(url, null);
  };
}

export default LiquidityManagementReportStore;
