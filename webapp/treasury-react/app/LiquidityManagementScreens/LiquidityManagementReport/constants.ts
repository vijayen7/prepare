/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { getCurrentDate } from "../commons/utils";

/**
 * Defines all the constants
 * @author burri
 */

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};


export const MMF_DEFAULT_PINNED_COLUMNS = {
  actions: true,
};

export const MESSAGES = {
  INITIAL_SEARCH_MESSAGE: "Perform search to view Liquidity Report Data",
  NO_DATA_FOUND_MESSAGE:
    "No Liquidity Report Data found for the performed search",
  ERROR_MESSAGE: "Error Occurred while fetching Liquidity Report Data",
  ERROR_ADJUSTMENT_MESSAGE: "Error Occurred while adding Adjustment",
  ERROR_MMF_SPN: "MMF Spn cannot be empty",
  ERROR_CUSTODIAN_ACCOUNT: "Custodian Account cannot be empty",
  ERROR_AMOUNT: "Amount cannot be empty",
  ERROR_BOOK: "Book cannot be empty",
  INVALID_AMOUNT_ERROR: "Please Enter Valid Amount",
  ERROR_BUNDLE: "Bundle cannot be empty",
  ERROR_COMMENT: "Comment cannot be empty",
  ERROR_SUBJECT: "Subject cannot be empty",
  ERROR_SOURCE_ACCOUNT: "Source Account cannot be empty",
  ERROR_DESTINATION_ACCOUNT: "Destination Account cannot be empty",
  ERROR_SOURCE_PORTFOLIO: "Source Portfolio cannot be empty",
  ERROR_DESTINATION_PORTFOLIO: "Destination Portfolio Cannot be empty",
  ERROR_WIRE_TRADE_FILTER:
    "Please select atleast one among Log Wire, Book Internal Trade and Book SS Trade",
  ERROR_EXTERNAL_TRADE: "External Trade cannot be booked for past date",
  BEGIN: "BEGIN",
  IN_PROGRESS: "IN_PROGRESS",
  INITIATE_WORKFLOW_FAILURE: "Error occurred while initiating workflow",
  CANCEL_WORKFLOW_FAILURE: "Error occurred while cancelling workflow",
  WORKFLOW_PUBLISH_FAILURE: "Workflow published failed",
  AMOUNT_ERROR: "Invalid Amount",
  AMOUNT_MESSAGE: "Please enter a valid amount",
  TRADE_AND_WIRE_CONFIRMATION: "Trade and Wire Confirmation",
  WORKFLOW_CONFLICT: "Workflow Conflict",
  INVALID_WORKFLOW: "Invalid Workflow",
  INVALID_WORKFLOW_MESSAGE: "Current Workflow is not in progress state. Not proceeding further."
};

export const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  error: false,
  message: MESSAGES.INITIAL_SEARCH_MESSAGE,
};

export const ERROR_SEARCH_STATUS = {
  inProgress: false,
  error: true,
  message: MESSAGES.ERROR_MESSAGE,
};

export const STAY = "STAY";
export const REDEEM = "REDEEM";
export const INVEST = "INVEST";
export const INTERNAL = "Internal";
export const EXTERNAL = "External";
export const MARGIN = "Margin";
export const OTHER = "Other";
export const USD = "USD";
export const MARGIN_COLLATERAL = "Margin/Collateral";
export const INWARD = "Inward";
export const OUTWARD = "Outward";
export const CALL = "Call";
export const PULL = "Pull";
export const RAG_THRESHOLD_DEFAULT_COLOR = "GREEN";
export const SEG_IA = "SEG IA ";
export const PERCENT = "%";
export const ZERO_PERCENT = "0%";
export const CALL_PULL_DETAILS_TAB = "callPullDetails";
export const CASH_FLOWS_TAB = "cashFlows";
export const CASH_BALANCES_TAB = "cashBalances";
export const MMF_BALANCES_TAB = "mmfBalances";
export const UNMAPPED_FUND_ID = -2;
export const BUY = "BUY";
export const SELL = "SELL";
export const USD_SPN = 1760000;
export const SOURCE_ACCOUNT_ID = "sourceAccountId";
export const DESTINATION_ACCOUNT_ID = "destinationAccountId";
export const SOURCE_PORTFOLIO_ID = "sourcePortfolioId";
export const DESTINATION_PORTFOLIO_ID = "destinationPortfolioId";
export const BUNDLE_ID = "bundleId";
export const TRADE_DATE = "tradeBookingDate";
export const VALUE_DATE = "valueDate";
export const COMMENT = "comment";
export const IS_TRACKING_WIRE = "bookTrackingWire";
export const BOOK_INTERNAL_TRADE = "bookInternalTrade";
export const BOOK_INTERNAL_WIRE = "logInternalWire";
export const BOOK_EXTERNAL_TRADE = "bookExternalTrade";
export const DEFAULT_COMMENT = "Cash Management Trade & Wire Booking";
export const WORKFLOW_TYPE = "CALM_FUND";

export const INITIAL_QF_COUNTER = {
  STAY: 0,
  REDEEM: 0,
  INVEST: 0,
};

export const ACTION_CATEGORY_CODE_MAP = {
  STAY: "primary",
  INVEST: "green5",
  REDEEM: "orange5",
};

export const SOURCE_CATEGORY_CODE_MAP = {
  External: "purple5",
  Internal: "primary",
  Inward: "green5",
  Outward: "orange5",
  Margin: "primary",
  Other: "purple5",
  Call: "orange5",
  Pull: "green5",
};
export const RAG_CATEGORY_CODE_MAP = {
  RED: "critical",
  AMBER: "warning",
  GREEN: "success",
};

export const DEFAULT_COLUMNS: string[] = [
  "actions",
  "liquidityAction",
  "fund",
  "openingCashBalance",
  "netCashFlows",
  "intradayFundingRequirement",
  "externalCallData",
  "internalCallData",
  "externalPullData",
  "internalPullData",
  "otherCashOutFlows",
  "adjustmentAmount",
  "projectedClosingBalance",
  "mmfBalance",
];

export const DEFAULT_COLUMNS_FOR_AGREEMENT_FUND: string[] = [
  "actions",
  "liquidityAction",
  "legalEntity",
  "counterParty",
  "agreementType",
  "unEncumberedCashBalance",
  "adjustedEd",
  "adjustedDebitBalance",
  "netCashFlows",
  "externalEd",
  "internalEd",
  "externalDebitBalance",
  "internalDebitBalance",
  "tDayProjectedDebitBalance",
  "tPlusOneProjectedDebitBalance",
  "tPlusTwoProjectedDebitBalance",
  "cashOutFlows",
  "adjustmentAmount",
];

export const DEFAULT_SORT_BY = ["fund"];
export const DEFAULT_SORT_BY_FOR_AGREEMENT_FUND = ["legalEntity"];
export const DEFAULT_PINNED_COLUMNS = {
  actions: true,
  fund: true,
  liquidityAction: true,
  legalEntity: true,
  counterParty: true,
  agreementType: true,
};

export const INITIAL_ADJUSTMENT_FILTERS = {
  selectedStartDate: getCurrentDate(),
  selectedEndDate: getCurrentDate(),
  selectedAmount: "",
  selectedComment: "",
  selectedAdjustmentType: { key: 5, value: "CASH OUTFLOW" },
  selectedReasonCode: { key: 10, value: "Parsing Issue/Limitation" },
};

export const INITIAL_BOOK_MMF_FILTERS = {
  selectedMMFSpn: null,
  selectedSubject: `${CODEX_PROPERTIES[
    "codex.client_name"
  ]?.toUpperCase()} Treasury Cash Management`,
  selectedCC: "",
  selectedSubscribers: "",
  selectedSourceAccount: [],
  selectedDestinationAccount: null,
  isDefaultSourcePortfolioOverride: false,
  selectedSourcePortfolio: [],
  defaultSourcePortfolio: [],
  isDefaultDestinationPortfolioOverride: false,
  selectedDestinationPortfolio: [],
  defaultDestinationPortfolio: [],
  isDefaultTradeBookOverride: false,
  defaultTradeBook: [],
  selectedTradeBook: [],
  isDefaultTradeBundleOverride: false,
  selectedBundles: [],
  defaultTradeBundle: [],
  selectedCustodianAccount: null,
  selectedTradeDate: getCurrentDate(),
  selectedValueDate: getCurrentDate(),
  selectedAmount: "",
  selectedComment: "",
  logWire: true,
  bookInternalTrade: true,
  isTrackingWire: false,
  bookSSTrade: true,
};

export const INITIAL_API_RESPONSE = {
  successStatus: true,
  message: "",
  response: null,
};

export const INITIAL_WORKFLOW_STATUS_DETAIL = {
  workflowId: -1,
  workflowState: "",
  workflowDate: "",
  subject: "",
  message: "",
  fundId: -1
};

export const INPUT_FORMAT_PROPERTIES = "&inputFormat=properties&format=json"
