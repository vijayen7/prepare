/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from 'react';
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import InputFilter from "../../../commons/components/InputFilter";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import SelectWithLabel from "./SelectWithLabel";

/**
 * Component for displaying the default portfolio, books  or
 * to show all the available portfolios, books based on the checkbox value
 * @author chawlasi
 */

const Portfolio = (props) => {
  return (
    <>
      {props.defaultValue && (
        <div className="margin--horizontal margin--vertical--double">
          <InputFilter
            data={props.defaultValue ? props.defaultValue.name : ''}
            label={'Default ' + props.label}
            stateKey={props.defaultStateKey}
            disabled
          />
        </div>
      )
      }

      {props.defaultValue && (
        <div className="form">
          <div className="row">
            <div className="form margin--horizontal">
              {'Override Default ' + props.label}
            </div>
            <div className="row">
              <CheckboxFilter
                defaultChecked={props.isDefaultPortfolioBookOverride}
                onSelect={props.onSelect}
                stateKey={props.checkBoxStateKey}
              />
            </div>
          </div>
        </div>
      )}

      {(!props.defaultValue || props.isDefaultPortfolioBookOverride) && (
        <>
          {props.data?.length !== 1 && (
            <div className="margin--horizontal margin--vertical--double">
              <SingleSelectFilter
                onSelect={props.onSelect}
                selectedData={props.selectedData}
                stateKey={props.stateKey}
                data={props.data}
                label={props.label}
                horizontalLayout
              />
            </div>
          )}
          {props.data?.length === 1 && (
            <div className="margin--horizontal margin--vertical--double">
              <SelectWithLabel
                label={props.label}
                stateKey={props.stateKey}
                options={props.data}
                value={props.data[0]}
                readonly
              />
            </div>
          )}
        </>
      )}
    </>
  );
};

export default Portfolio;
