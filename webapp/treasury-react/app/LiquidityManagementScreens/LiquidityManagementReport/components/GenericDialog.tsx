/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { DialogService } from "arc-react-components";
import LiquidityManagementWorkflowStore from "../stores/LiquidityManagementWorkflowStore";

/**
 * Generic component to display Dialogs
 * @author burri
 */

export async function showDialog(title: string, message: any, footer?: any) {
  return await DialogService.open({
    title: title,
    style: { width: "35%" },
    renderBody: () => message,
    renderFooter: ({ submitProps, cancelProps, submit }) =>
      footer ? footer({ submitProps, cancelProps, submit }) : undefined,
  });
}

export async function showTradeAndWireConfirmationDialog(
  title: string,
  liquidityManagementWorkflowStore: LiquidityManagementWorkflowStore
) {
  let footer = ({ submitProps, cancelProps }) => (
    <div>
      <button {...cancelProps}>Go Back</button>
      <button {...submitProps}>Confirm</button>
    </div>
  );
  let message = liquidityManagementWorkflowStore.wireAndTradeMessage;
  const tradeAndWireConfirmation = await showDialog(title, message, footer);

  if (tradeAndWireConfirmation.submitted) {
    await liquidityManagementWorkflowStore.handleBookMMFSave();
  }
}

export async function showMessageDialog(title: string, message: string) {
  let footer = ({ submitProps, _ }) => (
    <div>
      <button {...submitProps}>Close</button>
    </div>
  );
  await showDialog(title, message, footer);
}

export async function workflowCancelAndBeginDialogue(
  title: string,
  liquidityManagementWorkflowStore: LiquidityManagementWorkflowStore
) {
  let footer = ({ submitProps, cancelProps }) => (
    <div>
      <button {...cancelProps}>Go Back</button>
      <button {...submitProps}>Confirm</button>
    </div>
  );
  let message = liquidityManagementWorkflowStore.workflowStatusDetail.message;
  message += ". Please confirm to cancel the existing workflow and create a new workflow.";
  const cancelAndBeginWorkflowConfirmation = await showDialog(title, message, footer);

  if (cancelAndBeginWorkflowConfirmation.submitted) {
    await liquidityManagementWorkflowStore.cancelAndBeginNewWorkflow();
  }
}
