/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Dialog } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import Column from "../../../commons/components/Column";
import InputFilter from "../../../commons/components/InputFilter";
import Panel from "../../../commons/components/Panel";
import ArcDateFilter from "../../../commons/container/ArcDateFilter";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import Bundle from "./Bundle";
import Portfolio from "./Portfolio";
import { reduceDefaultMMFBookingDetails } from "../utils";
import { useStores } from "../useStores";
import SelectWithLabel from "./SelectWithLabel";
import { getCommaSeparatedNumber } from "../../../commons/util";

/**
 * PopUp for Default Book MMF
 * @author chawlasi
 */

const DefaultBookMMFDialog: React.FC<any> = () => {
  const { rootStore } = useStores();
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;

  function render() {
    let defaultBookMMFDetails;
    let reducedBookMMFDetails;
    if (liquidityManagementWorkflowStore.showDefaultBookMMFDialog) {
      defaultBookMMFDetails =
        liquidityManagementWorkflowStore.defaultBookMMFDetailsForFund?.response;
      liquidityManagementWorkflowStore.bookMMFFilter.defaultTradeBook =
        defaultBookMMFDetails?.defaultBook;
      liquidityManagementWorkflowStore.bookMMFFilter.defaultSourcePortfolio =
        defaultBookMMFDetails?.defaultSourcePortfolioBook;
      liquidityManagementWorkflowStore.bookMMFFilter.defaultDestinationPortfolio =
        defaultBookMMFDetails?.defaultDestinationPortfolioBook;
      liquidityManagementWorkflowStore.bookMMFFilter.defaultTradeBundle =
        defaultBookMMFDetails?.defaultBundle;
      reducedBookMMFDetails = reduceDefaultMMFBookingDetails(
        defaultBookMMFDetails
      );
    }
    return (
      <>
        <Dialog
          isOpen={liquidityManagementWorkflowStore.showDefaultBookMMFDialog}
          onClose={
            liquidityManagementWorkflowStore.toggleShowDefaultBookMMFDialog
          }
          title={
            "Book MMF For " + liquidityManagementReportStore.selectedFundName
          }
          footer={
            <React.Fragment>
              <button
                onClick={
                  liquidityManagementWorkflowStore.validateAndConfirmMMFInputs
                }
                className="button--primary"
              >
                Book MMF
              </button>
              <button
                onClick={
                  liquidityManagementWorkflowStore.handleDefaultBookMMFReset
                }
              >
                Reset
              </button>
            </React.Fragment>
          }
          style={{ width: "35%" }}
        >
          <div className="margin--horizontal margin--vertical--double">
            <SingleSelectFilter
              label="MMF Spn"
              onSelect={() => {}}
              selectedData={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedMMFSpn
              }
              horizontalLayout
              readonly
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              label="Subject"
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedSubject"
              data={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedSubject
              }
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedSubscribers"
              data={
                liquidityManagementWorkflowStore.bookMMFFilter
                  .selectedSubscribers
              }
              label="Subscribers"
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedCC"
              data={liquidityManagementWorkflowStore.bookMMFFilter.selectedCC}
              label="CC"
            />
          </div>
          {reducedBookMMFDetails?.sourceAccounts?.length !== 1 && (
            <div className="margin--horizontal margin--vertical--double">
              <SingleSelectFilter
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                selectedData={
                  liquidityManagementWorkflowStore.bookMMFFilter
                    .selectedSourceAccount
                }
                stateKey="selectedSourceAccount"
                data={reducedBookMMFDetails?.sourceAccounts}
                label="Source Account"
                horizontalLayout
              />
            </div>
          )}
          {reducedBookMMFDetails?.sourceAccounts?.length === 1 && (
            <div className="margin--horizontal margin--vertical--double">
              <SelectWithLabel
                label="Source Account"
                stateKey="selectedSourceAccount"
                options={reducedBookMMFDetails?.sourceAccounts}
                value={reducedBookMMFDetails?.sourceAccounts[0]}
                readonly
              />
            </div>
          )}
          {reducedBookMMFDetails?.destinationAccounts?.length !== 1 && (
            <div className="margin--horizontal margin--vertical--double">
              <SingleSelectFilter
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                selectedData={
                  liquidityManagementWorkflowStore.bookMMFFilter
                    .selectedDestinationAccount
                }
                data={reducedBookMMFDetails?.destinationAccounts}
                stateKey="selectedDestinationAccount"
                label="Destination Account"
                horizontalLayout
              />
            </div>
          )}
          {reducedBookMMFDetails?.destinationAccounts?.length === 1 && (
            <div className="margin--horizontal margin--vertical--double">
              <SelectWithLabel
                label="Destination Account"
                stateKey="selectedDestinationAccount"
                options={reducedBookMMFDetails?.destinationAccounts}
                value={reducedBookMMFDetails?.destinationAccounts[0]}
                readonly
              />
            </div>
          )}
          <div className="margin--horizontal margin--vertical--double">
            <SingleSelectFilter
              onSelect={() => {}}
              selectedData={
                liquidityManagementWorkflowStore.bookMMFFilter
                  .selectedCustodianAccount
              }
              stateKey="selectedCustodianAccount"
              label="Custodian Account"
              horizontalLayout
              readonly
            />
          </div>
          <Portfolio
            onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
            selectedData={
              liquidityManagementWorkflowStore.bookMMFFilter
                .selectedSourcePortfolio
            }
            defaultStateKey="defaultSourcePortfolio"
            stateKey="selectedSourcePortfolio"
            data={reducedBookMMFDetails?.portfolios}
            label="Source Portfolio"
            defaultValue={defaultBookMMFDetails?.defaultSourcePortfolioBook}
            isDefaultPortfolioBookOverride={
              liquidityManagementWorkflowStore.bookMMFFilter
                .isDefaultSourcePortfolioOverride
            }
            checkBoxStateKey="isDefaultSourcePortfolioOverride"
          />
          <Portfolio
            onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
            selectedData={
              liquidityManagementWorkflowStore.bookMMFFilter
                .selectedDestinationPortfolio
            }
            defaultStateKey="defaultDestinationPortfolio"
            stateKey="selectedDestinationPortfolio"
            data={reducedBookMMFDetails?.portfolios}
            label="Destination Portfolio"
            defaultValue={
              defaultBookMMFDetails?.defaultDestinationPortfolioBook
            }
            isDefaultPortfolioBookOverride={
              liquidityManagementWorkflowStore.bookMMFFilter
                .isDefaultDestinationPortfolioOverride
            }
            checkBoxStateKey="isDefaultDestinationPortfolioOverride"
          />
          <Portfolio
            onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
            selectedData={
              liquidityManagementWorkflowStore.bookMMFFilter.selectedTradeBook
            }
            defaultStateKey="defaultTradeBook"
            stateKey="selectedTradeBook"
            data={reducedBookMMFDetails?.portfolios}
            label="Trade Book"
            defaultValue={defaultBookMMFDetails?.defaultBook}
            isDefaultPortfolioBookOverride={
              liquidityManagementWorkflowStore.bookMMFFilter
                .isDefaultTradeBookOverride
            }
            checkBoxStateKey="isDefaultTradeBookOverride"
          />
          <Bundle
            onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
            selectedData={
              liquidityManagementWorkflowStore.bookMMFFilter.selectedBundles
            }
            defaultStateKey="defaultTradeBundle"
            stateKey="selectedBundles"
            label="Trade Bundle"
            defaultValue={defaultBookMMFDetails?.defaultBundle}
            isDefaultPortfolioBookOverride={
              liquidityManagementWorkflowStore.bookMMFFilter
                .isDefaultTradeBundleOverride
            }
            checkBoxStateKey="isDefaultTradeBundleOverride"
          />
          <div className="margin--horizontal margin--vertical--double">
            <ArcDateFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              data={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedTradeDate
              }
              dateType="tFilterDate"
              stateKey="selectedTradeDate"
              label="Trade Date"
              horizontalLayout
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <ArcDateFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              data={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedValueDate
              }
              dateType="tFilterDate"
              stateKey="selectedValueDate"
              label="Value Date"
              horizontalLayout
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              data={getCommaSeparatedNumber(
                liquidityManagementWorkflowStore.bookMMFFilter.selectedAmount
              )}
              label="Amount"
              disabled
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedComment"
              data={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedComment
              }
              label="Comment"
            />
          </div>
          <br />
          <div className="text-align--center" title="Fund Ownership Status">
            <span
              style={{
                height: "22px",
                width: "750px",
              }}
              className={
                "token padding--horizontal--double " +
                liquidityManagementWorkflowStore.fundTokenCss
              }
            >
              <>Fund Ownership Status</>
            </span>
          </div>
          <div
            className="text-align--center margin--vertical--small"
            title="Firm Ownership Status"
          >
            <span
              style={{
                height: "22px",
                width: "750px",
              }}
              className={
                "token padding--horizontal--double " +
                liquidityManagementWorkflowStore.firmTokenCss
              }
            >
              <>Firm Ownership Status</>
            </span>
          </div>

          <br />
          <Panel>
            <Column>
              <CheckboxFilter
                defaultChecked={
                  liquidityManagementWorkflowStore.bookMMFFilter.logWire
                }
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                stateKey="logWire"
                label="Log Wire"
                disabled={!liquidityManagementReportStore.enableWireLogging}
              />
              <CheckboxFilter
                defaultChecked={
                  liquidityManagementWorkflowStore.bookMMFFilter.isTrackingWire
                }
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                stateKey="isTrackingWire"
                label="Is Tracking Wire"
                disabled={!liquidityManagementReportStore.enableWireLogging}
              />
            </Column>
            <Column>
              <CheckboxFilter
                defaultChecked={
                  liquidityManagementWorkflowStore.bookMMFFilter
                    .bookInternalTrade
                }
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                stateKey="bookInternalTrade"
                label="Book Internal Trade"
                disabled={
                  !liquidityManagementReportStore.enableInternalTradeBooking
                }
              />
              <CheckboxFilter
                defaultChecked={
                  liquidityManagementWorkflowStore.bookMMFFilter.bookSSTrade
                }
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                stateKey="bookSSTrade"
                label="Book External Trade"
              />
            </Column>
          </Panel>
        </Dialog>
      </>
    );
  }
  return <>{render()}</>;
};

export default observer(DefaultBookMMFDialog);
