/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Dialog } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import Column from "../../../commons/components/Column";
import InputFilter from "../../../commons/components/InputFilter";
import Panel from "../../../commons/components/Panel";
import ArcDateFilter from "../../../commons/container/ArcDateFilter";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import { getCommaSeparatedNumber } from "../../../commons/util";
import SelectWithLabel from "./SelectWithLabel";
import { reduceMMFBookingDetails } from "../utils";
import { useStores } from "../useStores";

/**
 * PopUp for Book MMF
 * @author burri
 */

const BookMMFDialog: React.FC<any> = () => {
  const { rootStore } = useStores();
  const liquidityManagementReportStore =
    rootStore.liquidityManagementReportStore;
  const liquidityManagementWorkflowStore =
    rootStore.liquidityManagementWorkflowStore;

  function render() {
    let mmfBookingDetails;
    if (liquidityManagementWorkflowStore.showBookMMFDialog) {
      mmfBookingDetails = reduceMMFBookingDetails(
        liquidityManagementWorkflowStore.mmfBookingDetailsForFund?.response
      );
    }
    return (
      <>
        <Dialog
          isOpen={liquidityManagementWorkflowStore.showBookMMFDialog}
          onClose={liquidityManagementWorkflowStore.toggleShowMMFDialog}
          title={
            "Book MMF For " + liquidityManagementWorkflowStore.mmfDialogTitle
          }
          footer={
            <React.Fragment>
              <button
                onClick={
                  liquidityManagementWorkflowStore.validateAndConfirmMMFInputs
                }
                className="button--primary"
              >
                Book MMF
              </button>
              <button
                onClick={liquidityManagementWorkflowStore.handleBookMMFReset}
              >
                Reset
              </button>
            </React.Fragment>
          }
          style={{ width: "35%" }}
        >
          <div className="margin--horizontal margin--vertical--double">
            <SingleSelectFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              selectedData={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedMMFSpn
              }
              stateKey="selectedMMFSpn"
              data={mmfBookingDetails?.mmfs}
              label="MMF Spn"
              horizontalLayout
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedSubject"
              data={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedSubject
              }
              label="Subject"
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedSubscribers"
              data={
                liquidityManagementWorkflowStore.bookMMFFilter
                  .selectedSubscribers
              }
              label="Subscribers"
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedCC"
              data={liquidityManagementWorkflowStore.bookMMFFilter.selectedCC}
              label="CC"
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <SingleSelectFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              selectedData={
                liquidityManagementWorkflowStore.bookMMFFilter
                  .selectedSourceAccount
              }
              stateKey="selectedSourceAccount"
              data={mmfBookingDetails?.sourceAccounts}
              label="Source Account"
              horizontalLayout
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <SingleSelectFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              selectedData={
                liquidityManagementWorkflowStore.bookMMFFilter
                  .selectedDestinationAccount
              }
              stateKey="selectedDestinationAccount"
              data={mmfBookingDetails?.destinationAccounts}
              label="Destination Account"
              horizontalLayout
            />
          </div>
          <>
            {mmfBookingDetails?.portfolios.length !== 1 && (
              <div className="margin--horizontal margin--vertical--double">
                <SingleSelectFilter
                  onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                  selectedData={
                    liquidityManagementWorkflowStore.bookMMFFilter
                      .selectedSourcePortfolio
                  }
                  stateKey="selectedSourcePortfolio"
                  data={mmfBookingDetails?.portfolios}
                  label="Source Portfolio"
                  horizontalLayout
                />
              </div>
            )}
            {mmfBookingDetails?.portfolios.length === 1 && (
              <div className="margin--horizontal margin--vertical--double">
                <SelectWithLabel
                  label="Source Portfolio"
                  stateKey="selectedSourcePortfolio"
                  options={mmfBookingDetails?.portfolios}
                  value={mmfBookingDetails?.portfolios[0]}
                  readonly
                />
              </div>
            )}
          </>
          <>
            {mmfBookingDetails?.portfolios.length !== 1 && (
              <div className="margin--horizontal margin--vertical--double">
                <SingleSelectFilter
                  onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                  selectedData={
                    liquidityManagementWorkflowStore.bookMMFFilter
                      .selectedDestinationPortfolio
                  }
                  stateKey="selectedDestinationPortfolio"
                  data={mmfBookingDetails?.portfolios}
                  label="Destination Portfolio"
                  horizontalLayout
                />
              </div>
            )}
            {mmfBookingDetails?.portfolios.length === 1 && (
              <div className="margin--horizontal margin--vertical--double">
                <SelectWithLabel
                  label="Destination Portfolio"
                  stateKey="selectedDestinationPortfolio"
                  options={mmfBookingDetails?.portfolios}
                  value={mmfBookingDetails?.portfolios[0]}
                  readonly
                />
              </div>
            )}
          </>
          <div className="margin--horizontal margin--vertical--double">
            <SingleSelectFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              selectedData={
                liquidityManagementWorkflowStore.bookMMFFilter
                  .selectedCustodianAccount
              }
              stateKey="selectedCustodianAccount"
              data={mmfBookingDetails?.custodianAccounts}
              label="Custodian Account"
              horizontalLayout
            />
          </div>
          <>
            {mmfBookingDetails?.portfolios.length !== 1 && (
              <div className="margin--horizontal margin--vertical--double">
                <SingleSelectFilter
                  onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                  selectedData={
                    liquidityManagementWorkflowStore.bookMMFFilter
                      .selectedTradeBook
                  }
                  stateKey="selectedTradeBook"
                  data={mmfBookingDetails?.portfolios}
                  label="Trade Book"
                  horizontalLayout
                />
              </div>
            )}
            {mmfBookingDetails?.portfolios.length === 1 && (
              <div className="margin--horizontal margin--vertical--double">
                <SelectWithLabel
                  label="Trade Book"
                  stateKey="selectedTradeBook"
                  options={mmfBookingDetails?.portfolios}
                  value={mmfBookingDetails?.portfolios[0]}
                  readonly
                />
              </div>
            )}
          </>
          <div className="margin--horizontal margin--vertical--double">
            <ArcDateFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              data={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedTradeDate
              }
              dateType="tFilterDate"
              stateKey="selectedTradeDate"
              label="Trade Date"
              horizontalLayout
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <ArcDateFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              data={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedValueDate
              }
              dateType="tFilterDate"
              stateKey="selectedValueDate"
              label="Value Date"
              horizontalLayout
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedAmount"
              data={getCommaSeparatedNumber(
                liquidityManagementWorkflowStore.bookMMFFilter.selectedAmount
              )}
              label="Amount"
            />
          </div>
          <div className="margin--horizontal margin--vertical--double">
            <InputFilter
              onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
              stateKey="selectedComment"
              data={
                liquidityManagementWorkflowStore.bookMMFFilter.selectedComment
              }
              label="Comment"
            />
          </div>
          <br />
          <Panel>
            <Column>
              <CheckboxFilter
                defaultChecked={
                  liquidityManagementWorkflowStore.bookMMFFilter.logWire
                }
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                stateKey="logWire"
                label="Log Wire"
                disabled={!liquidityManagementReportStore.enableWireLogging}
              />
              <CheckboxFilter
                defaultChecked={
                  liquidityManagementWorkflowStore.bookMMFFilter.isTrackingWire
                }
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                stateKey="isTrackingWire"
                label="Is Tracking Wire"
                disabled={!liquidityManagementReportStore.enableWireLogging}
              />
            </Column>
            <Column>
              <CheckboxFilter
                defaultChecked={
                  liquidityManagementWorkflowStore.bookMMFFilter
                    .bookInternalTrade
                }
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                stateKey="bookInternalTrade"
                label="Book Internal Trade"
                disabled={
                  !liquidityManagementReportStore.enableInternalTradeBooking
                }
              />
              <CheckboxFilter
                defaultChecked={
                  liquidityManagementWorkflowStore.bookMMFFilter.bookSSTrade
                }
                onSelect={liquidityManagementWorkflowStore.onSelectForBookMMF}
                stateKey="bookSSTrade"
                label="Book External Trade"
              />
            </Column>
          </Panel>
        </Dialog>
      </>
    );
  }
  return <>{render()}</>;
};

export default observer(BookMMFDialog);
