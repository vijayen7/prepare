/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Dialog } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import InputFilter from '../../../commons/components/InputFilter';
import ArcDateFilter from "../../../commons/container/ArcDateFilter";
import ReasonCodeFilter from "../../../commons/container/ReasonCodeFilter";
import SingleSelectFilter from "../../../commons/filters/components/SingleSelectFilter";
import { getCommaSeparatedNumber } from '../../../commons/util';
import { useStores } from "../useStores";


/**
 * PopUp for Add Ajustment
 * @author burri
 */

const AdjustmentDialog: React.FC<any> = () => {
  const { rootStore } = useStores();
  const liquidityManagementWorkflowStore = rootStore.liquidityManagementWorkflowStore;

  function render() {
    return (
      <>
        <Dialog
          isOpen={liquidityManagementWorkflowStore.showAdjustmentDialog}
          title={"Add Adjustment For " + liquidityManagementWorkflowStore.adjustmentFundName}
          onClose={liquidityManagementWorkflowStore.toggleShowDialog}
          footer={
            <React.Fragment>
              <button
                onClick={liquidityManagementWorkflowStore.handleAdjustmentSave}
                className="button--primary"
              >
                Add Adjustment
              </button>
              <button
                onClick={liquidityManagementWorkflowStore.handleAdjustmentReset}
              >
                Reset
              </button>
            </React.Fragment>
          }
        >
          <SingleSelectFilter
            onSelect={liquidityManagementWorkflowStore.onSelectForAdjustment}
            selectedData={
              liquidityManagementWorkflowStore.adjustmentFilter
                .selectedAdjustmentType
            }
            label=" Adjustment Type"
            stateKey="selectedAdjustmentType"
            readonly
            horizontalLayout
          />
          <ArcDateFilter
            stateKey="selectedStartDate"
            label="Start Date"
            onSelect={liquidityManagementWorkflowStore.onSelectForAdjustment}
            data={
              liquidityManagementWorkflowStore.adjustmentFilter.selectedStartDate
            }
            horizontalLayout
          />
          <ArcDateFilter
            onSelect={liquidityManagementWorkflowStore.onSelectForAdjustment}
            stateKey="selectedEndDate"
            data={
              liquidityManagementWorkflowStore.adjustmentFilter.selectedEndDate
            }
            label="End Date"
            horizontalLayout
          />
          <InputFilter
            onSelect={liquidityManagementWorkflowStore.onSelectForAdjustment}
            stateKey="selectedAmount"
            data={getCommaSeparatedNumber(liquidityManagementWorkflowStore
              .adjustmentFilter.selectedAmount)}
            label="Amount"
          />
          <ReasonCodeFilter
            onSelect={liquidityManagementWorkflowStore.onSelectForAdjustment}
            selectedData={
              liquidityManagementWorkflowStore.adjustmentFilter
                .selectedReasonCode
            }
            multiSelect={false}
            stateKey="selectedReasonCode"
            adjustmentType="CASH_OUTFLOW"
            horizontalLayout
          />
          <InputFilter
            onSelect={liquidityManagementWorkflowStore.onSelectForAdjustment}
            stateKey="selectedComment"
            data={liquidityManagementWorkflowStore.adjustmentFilter.selectedComment}
            label="Comment"
          />
        </Dialog>
        <Dialog
          title="Add Adjustment Status"
          isOpen={liquidityManagementWorkflowStore.addAdjustmentSuccessDialog}
          onClose={liquidityManagementWorkflowStore.toggleAddAdjustmentStatusDialog}
        >
          <div>{liquidityManagementWorkflowStore.addAdjustmentMessage}</div>
        </Dialog>
      </>
    );
  }
  return <>{render()}</>;
};

export default observer(AdjustmentDialog);
