/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Select } from "arc-react-components";
import React from "react";
import { useStores } from "../useStores";


/**
 * Component for displaying the single
 * select values of a dropdown with atleast one value selected
 * @author burri
 */

const SelectWithLabel: React.FC<any> = (props) => {
  const { rootStore } = useStores();
  const liquidityManagementWorkflowStore = rootStore.liquidityManagementWorkflowStore;
  function render() {
    liquidityManagementWorkflowStore.bookMMFFilter[props.stateKey] = props.value;
    return (
      <>
        <div className="form-field--split">
          <label>{props.label}</label>
          <Select
            key={props.label}
            options={props.options}
            value={props.value}
            disabled={props.disabled}
            readonly={props.readonly}
          />
        </div>
      </>
    );
  }
  return <>{render()}</>;
};

export default SelectWithLabel;
