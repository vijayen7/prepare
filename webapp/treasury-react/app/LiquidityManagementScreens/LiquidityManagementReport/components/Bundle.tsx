/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from 'react';
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import InputFilter from "../../../commons/components/InputFilter";
import BundleFilter from '../../../commons/container/BundleFilter';

/**
 * Component for displaying the default bundles or
 * to show all the available bundles based on the checkbox value
 * @author chawlasi
 */

const Bundle = (props) => {
  return (
    <>
      {props.defaultValue && (
        <div className="margin--horizontal margin--vertical--double">
          <InputFilter
            data={props.defaultValue ? props.defaultValue.name : ''}
            label={'Default ' + props.label}
            stateKey={props.defaultStateKey}
            disabled
          />
        </div>
      )
      }

      {props.defaultValue && (
        <div className="form">
          <div className="row">
            <div className="form margin--horizontal">
              {'Override Default ' + props.label}
            </div>
            <div className="row">
              <CheckboxFilter
                defaultChecked={props.isDefaultPortfolioBookOverride}
                onSelect={props.onSelect}
                stateKey={props.checkBoxStateKey}
              />
            </div>
          </div>
        </div>
      )}

      {(!props.defaultValue || props.isDefaultPortfolioBookOverride) && (
        <>
          <div className="margin--horizontal margin--vertical--double">
            <BundleFilter
              onSelect={props.onSelect}
              selectedData={props.selectedData}
              multiSelect={false}
              horizontalLayout
            />
          </div>
        </>
      )}
    </>
  );
};

export default Bundle;
