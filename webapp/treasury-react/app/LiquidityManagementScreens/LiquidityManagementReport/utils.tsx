/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import ArcDataGrid from "arc-data-grid";
import { ToastService } from "arc-react-components";
import moment from "moment";
import React from "react";
import { plainNumberFormatter } from "../../commons/grid/formatters";
import MapData from "../../commons/models/MapData";
import {
  getNameFromRefData,
  getIdFromRefData,
} from "../../commons/TreasuryUtils";
import { TOAST_TYPE, CLASSES } from "../commons/constants";
import { calculatePercentage } from "../commons/utils";
import { getCurrentDate } from "../../commons/util";
import {
  CALL,
  EXTERNAL,
  INTERNAL,
  INWARD,
  MARGIN,
  MARGIN_COLLATERAL,
  OTHER,
  OUTWARD,
  PULL,
  SEG_IA,
  USD,
  SOURCE_ACCOUNT_ID,
  DESTINATION_ACCOUNT_ID,
  SOURCE_PORTFOLIO_ID,
  DESTINATION_PORTFOLIO_ID,
  BUNDLE_ID,
  IS_TRACKING_WIRE,
  TRADE_DATE,
  VALUE_DATE,
  BOOK_EXTERNAL_TRADE,
  BOOK_INTERNAL_TRADE,
  BOOK_INTERNAL_WIRE,
  MESSAGES,
  USD_SPN,
  BUY,
  SELL,
  COMMENT,
  INITIAL_BOOK_MMF_FILTERS,
  DEFAULT_COMMENT,
} from "./constants";
import {
  MMFGridData,
  TickerChildren,
  TradeWorkflowInputs,
  CutoffTime,
  FlattenedFundData,
  FundLiquidityData,
} from "./models";
import ClickableColumn from "./containers/ClickableColumn";
import ReferenceData from "../../commons/models/ReferenceData";
import LiquidityManagementWorkflowStore from "./stores/LiquidityManagementWorkflowStore";
import LiquidityManagementReportStore from "./stores/LiquidityManagementReportStore";
import { showTradeAndWireConfirmationDialog } from "./components/GenericDialog";

/**
 * Contains all the utility methods and classes.
 * @author burri
 */

/**
 * Method to get flattened data from list
 */
export function flattenDictToList(inputList): any[] {
  return inputList?.map((s) => s?.key?.toString());
}

export function addWithNullChecks(
  a: number | null | undefined,
  b: number | null | undefined
) {
  if (a === null || a === undefined) a = 0;
  if (b === null || b === undefined) b = 0;
  return a + b;
}

export function subWithNullChecks(
  a: number | null | undefined,
  b: number | null | undefined
) {
  if (a === null || a === undefined) a = 0;
  if (b === null || b === undefined) b = 0;
  return a - b;
}

/**
 * Method to apply quick filters
 */
export function applyQuickFilter(
  reportData: FlattenedFundData[],
  selectedQuickFilters: string[]
) {
  if (selectedQuickFilters.length == 0) return reportData;

  var outputList: FlattenedFundData[] = [];
  for (let data of reportData) {
    let exists = false;
    for (let quickFilter of selectedQuickFilters) {
      if (data["liquidityAction"] === quickFilter) exists = true;
    }
    if (exists) {
      outputList.push(data);
    }
  }
  return outputList;
}

/**
 * Method to get the object or the default value
 */
export const getValueOrDefault = (obj: any, defaultValue = {}) => {
  if (obj) return obj;
  else return defaultValue;
};

export function getMarginCallAndPullData(fundLiquidityData: FundLiquidityData) {
  let marginCallAndPullData: any = [];
  fundLiquidityData?.brokerFilesStatusTracker?.brokerCallData?.forEach(
    (data) => {
      let brokerCallData = {
        type: CALL,
        source: EXTERNAL,
        agreementId: data?.agreementInfo?.agreementId,
        legalEntity: data?.agreementInfo?.legalEntity?.name,
        counterPartyEntity: data?.agreementInfo?.counterpartyEntity?.name,
        agreementType: data?.agreementInfo?.agreementType?.abbrev,
        exposure: data?.exposure,
        collateral: data?.collateral,
        margin: data?.margin,
        brokerED: data?.ed,
        segCollateral: data?.segCollateral,
        segMargin: data?.segMargin,
        segED: data?.segEd,
      };
      marginCallAndPullData.push(brokerCallData);
    }
  );

  fundLiquidityData?.brokerFilesStatusTracker?.internalCallData?.forEach(
    (data) => {
      let internalCallData = {
        type: CALL,
        source: INTERNAL,
        agreementId: data?.agreementInfo?.agreementId,
        legalEntity: data?.agreementInfo?.legalEntity?.name,
        counterPartyEntity: data?.agreementInfo?.counterpartyEntity?.name,
        agreementType: data?.agreementInfo?.agreementType?.abbrev,
        exposure: data?.exposure,
        collateral: data?.collateral,
        margin: data?.margin,
        brokerED: data?.ed,
        segCollateral: data?.segCollateral,
        segMargin: data?.segMargin,
        segED: data?.segEd,
      };
      marginCallAndPullData.push(internalCallData);
    }
  );
  fundLiquidityData?.brokerFilesStatusTracker?.brokerPullData?.forEach(
    (data) => {
      let brokerPullData = {
        type: PULL,
        source: EXTERNAL,
        agreementId: data?.agreementInfo?.agreementId,
        legalEntity: data?.agreementInfo?.legalEntity?.name,
        counterPartyEntity: data?.agreementInfo?.counterpartyEntity?.name,
        agreementType: data?.agreementInfo?.agreementType?.abbrev,
        exposure: data?.exposure,
        collateral: data?.collateral,
        margin: data?.margin,
        brokerED: data?.ed,
        segCollateral: data?.segCollateral,
        segMargin: data?.segMargin,
        segED: data?.segEd,
      };
      marginCallAndPullData.push(brokerPullData);
    }
  );

  fundLiquidityData?.brokerFilesStatusTracker?.internalPullData?.forEach(
    (data) => {
      let internalPullData = {
        type: PULL,
        source: INTERNAL,
        agreementId: data?.agreementInfo?.agreementId,
        legalEntity: data?.agreementInfo?.legalEntity?.name,
        counterPartyEntity: data?.agreementInfo?.counterpartyEntity?.name,
        agreementType: data?.agreementInfo?.agreementType?.abbrev,
        exposure: data?.exposure,
        collateral: data?.collateral,
        margin: data?.margin,
        brokerED: data?.ed,
        segCollateral: data?.segCollateral,
        segMargin: data?.segMargin,
        segED: data?.segEd,
      };
      marginCallAndPullData.push(internalPullData);
    }
  );
  return marginCallAndPullData;
}

export function getEDDataForAgreementFund(
  fundLiquidityData: FundLiquidityData
) {
  let marginCallAndPullData: any = [];
  fundLiquidityData?.underlyingData?.MARGIN_CALL_AND_PULL?.forEach((data) => {
    let edData = {
      type: data?.ed < 0 ? CALL : PULL,
      source: data?.source,
      agreementId: data?.agreementInfo?.agreementId,
      legalEntity: data?.agreementInfo?.legalEntity?.name,
      counterPartyEntity: data?.agreementInfo?.counterpartyEntity?.name,
      agreementType: data?.agreementInfo?.agreementType?.abbrev,
      exposure: data?.exposure,
      collateral: data?.collateral,
      margin: data?.margin,
      brokerED: data?.ed,
      segCollateral: data?.segCollateral,
      segMargin: data?.segMargin,
      segED: data?.segEd,
    };
    marginCallAndPullData.push(edData);
  });
  return marginCallAndPullData;
}

/**
 * Method to get opening cash balances data
 */
export function getOpeningCashBalanceData(
  fundLiquidityData: FundLiquidityData
) {
  let cashBalances: any = [];
  let balancesList = fundLiquidityData?.underlyingData?.SOD_CASH_BALANCES;
  if (balancesList !== undefined && balancesList !== null) {
    balancesList?.forEach((data) => {
      let cashBalance = {
        accountNumber: data?.accountNumber,
        currency: USD,
        balance: data?.sodCashUsd,
      };
      cashBalances.push(cashBalance);
    });
  }
  return cashBalances;
}

/**
 * Method to get mmf Balances data
 */
export function getMMFBalanceData(
  fundLiquidityData: FundLiquidityData,
  liquidityManagementWorkflowStore: LiquidityManagementWorkflowStore
) {
  let mmfSummaryMapData = liquidityManagementWorkflowStore.mmfSummaryMapData;
  let tickerToCustodianAccounts = tickerToCustodianAccountBalancesMap(
    fundLiquidityData,
    mmfSummaryMapData
  );
  let mmfBalancesList: any = [];
  for (let [key, value] of tickerToCustodianAccounts) {
    let totalBalanceUsd = 0.0;
    let totalClosingBalanceUsd = 0.0;
    let totalSegIABalance = 0.0;
    let isSEGIA = 0;
    let newChildrenArray: any = [];
    let aumValue = mmfSummaryMapData[key]?.mmf?.mmfAumUsd;
    value?.forEach((childData) => {
      totalBalanceUsd += childData.balanceUsd;
      totalClosingBalanceUsd += childData.closingBalanceUsd;
      if (childData.tickerWithCustodianAccount === SEG_IA) {
        totalSegIABalance += childData.balanceUsd;
        isSEGIA = 1;
      } else {
        newChildrenArray.push(childData);
      }
    });

    if (isSEGIA === 1) {
      let childrenData: TickerChildren = {
        tickerWithCustodianAccount: SEG_IA,
        custodianAccountId: null,
        balanceUsd: totalSegIABalance,
        closingBalanceUsd: totalSegIABalance,
        amount: null,
        parentInfo: null,
      };
      newChildrenArray.push(childrenData);
    }

    let mmfBalData: MMFGridData = {
      tickerWithCustodianAccount: key,
      balanceUsd: totalBalanceUsd,
      closingBalanceUsd: totalClosingBalanceUsd,
      amount: null,
      aumUsd: aumValue,
      entityOwnership: calculatePercentage(totalBalanceUsd, aumValue),
      fundOwnership: mmfSummaryMapData[key]?.firmLevelOwnershipPercentage,
      fundOwnershipAmount: mmfSummaryMapData[key]?.firmLevelOwnershipAmount,
      yield: mmfSummaryMapData[key]?.mmf?.yield * 100,
      expenseRatio: mmfSummaryMapData[key]?.mmf?.expenseRatio,
      children: newChildrenArray,
    };
    if (Math.round(mmfBalData.balanceUsd) === 0) {
      liquidityManagementWorkflowStore.tickerToZeroBalancesMap.set(
        key,
        mmfBalData
      );
    } else {
      liquidityManagementWorkflowStore.tickerToNonZeroBalancesMap.set(
        key,
        mmfBalData
      );
    }
    mmfBalancesList.push(mmfBalData);
  }
  return mmfBalancesList;
}

function tickerToCustodianAccountBalancesMap(
  fundLiquidityData: FundLiquidityData,
  mmfSummaryMapData: Map<string, any>
) {
  let tickerToCustodianAccounts: Map<any, any> = new Map();
  let childrenList: any = [];
  let mmfHoldingsList = fundLiquidityData?.underlyingData?.MMF_HOLDINGS;
  let segIAMMFHoldingsList =
    fundLiquidityData?.underlyingData?.SEG_IA_MMF_HOLDINGS;
  if (mmfHoldingsList !== undefined && mmfHoldingsList !== null) {
    mmfHoldingsList?.forEach((data) => {
      let key = data?.mmf?.ticker;
      let aumValue = mmfSummaryMapData[key]?.mmf?.mmfAumUsd;
      let parentData = {
        spn: data?.mmf?.spn,
        ticker: key,
        entityOwnership: calculatePercentage(data?.closingBalanceUsd, aumValue),
        fundOwnership: mmfSummaryMapData[key]?.firmLevelOwnershipPercentage,
        cutoffTime: data?.mmf?.cutoffTime,
      };
      let childrenData: TickerChildren = {
        tickerWithCustodianAccount: data?.custodianAccount?.displayName,
        custodianAccountId: data?.custodianAccount?.custodianAccountId,
        balanceUsd: data?.balanceUsd,
        closingBalanceUsd: data?.closingBalanceUsd
          ? data?.closingBalanceUsd
          : 0,
        amount: null,
        parentInfo: parentData,
      };
      if (!tickerToCustodianAccounts.has(key)) {
        childrenList = [];
        childrenList.push(childrenData);
        tickerToCustodianAccounts.set(key, childrenList);
      } else {
        childrenList = tickerToCustodianAccounts.get(key);
        childrenList.push(childrenData);
        tickerToCustodianAccounts.set(key, childrenList);
      }
    });
  }
  if (segIAMMFHoldingsList !== undefined && segIAMMFHoldingsList !== null) {
    segIAMMFHoldingsList?.forEach((data) => {
      let key = data?.mmf?.ticker;
      let aumValue = mmfSummaryMapData[key]?.mmf?.mmfAumUsd;
      let parentData = {
        spn: data?.mmf?.spn,
        ticker: key,
        entityOwnership: calculatePercentage(data?.closingBalanceUsd, aumValue),
        fundOwnership: mmfSummaryMapData[key]?.firmLevelOwnershipPercentage,
        cutoffTime: data?.mmf?.cutoffTime,
      };
      let childrenData: TickerChildren = {
        tickerWithCustodianAccount: SEG_IA,
        custodianAccountId: data?.custodianAccount?.custodianAccountId,
        balanceUsd: data?.balanceUsd,
        closingBalanceUsd: data?.balanceUsd,
        amount: 0,
        parentInfo: parentData,
      };
      if (!tickerToCustodianAccounts.has(key)) {
        childrenList = [];
        childrenList.push(childrenData);
        tickerToCustodianAccounts.set(key, childrenList);
      } else {
        childrenList = tickerToCustodianAccounts.get(key);
        childrenList.push(childrenData);
        tickerToCustodianAccounts.set(key, childrenList);
      }
    });
  }
  return tickerToCustodianAccounts;
}

/**
 * Method to get cash flows wire data
 */
export function getCashFlowsWiresData(fundLiquidityData: FundLiquidityData) {
  let wires: any = [];
  let wiresList = fundLiquidityData?.underlyingData?.WIRES;
  if (wiresList !== undefined && wiresList !== null) {
    wiresList?.forEach((data) => {
      let wire = {
        type: data?.category == MARGIN_COLLATERAL ? MARGIN : OTHER,
        direction: data?.isSourceAccMapped == true ? OUTWARD : INWARD,
        wireId: data?.wireId,
        category: data?.category,
        wireAmount: data?.wireAmount,
        sourceWireAccount: data?.sourceWireAccount,
        destinationWireAccount: data?.destinationWireAccount,
        sourceCustodianAccountId: data?.sourceCustodianAccountId,
        destinationCustodianAccountId: data?.destinationCustodianAccountId,
      };
      wires.push(wire);
    });
  }
  return wires;
}

function reduceWireInstructions(wireInstructions: any): MapData[] {
  const fundWireInstructions: MapData[] = [];
  wireInstructions
    .filter((item) => {
      return item.id !== undefined && item.name !== undefined;
    })
    .forEach((wireInstruction) => {
      let wireInstructionMapData = getMapDataFromReferenceData(wireInstruction);
      fundWireInstructions.push(wireInstructionMapData);
    });
  return fundWireInstructions;
}

export function getMapDataFromReferenceData(data: ReferenceData) {
  let mapData: MapData = {
    key: getIdFromRefData(data),
    value: getNameFromRefData(data),
  };
  return mapData;
}

/**
 * Method to get numeric column
 */
export function getNumericColumn(
  identity: string,
  header?: string,
  minWidth?: number,
  showValueWithUndefined?: boolean
): ArcDataGrid.ColumnProperties<any, any> {
  let formattedCellValue: any = {
    identity: identity,
    Cell: (value) => {
      let formattedCell: any;
      if (showValueWithUndefined) {
        formattedCell = (
          <>
            {value === undefined || value === null ? undefined : (
              <div className="text-align--right">
                {plainNumberFormatter(undefined, undefined, value)}
              </div>
            )}
          </>
        );
      } else {
        let result = plainNumberFormatter(undefined, undefined, value);
        if (
          result === "n/a" ||
          result === null ||
          result === undefined ||
          result == 0
        )
          formattedCell = (
            <>
              <div className="text-align--right" style={{ opacity: 0.6 }}>
                {0}
              </div>
            </>
          );
        else
          formattedCell = (
            <>
              <div className="text-align--right">{result}</div>
            </>
          );
      }
      return formattedCell;
    },
    AggregatedCell: (value: any) => (
      <>
        <div className="text-align--right">
          {plainNumberFormatter(undefined, undefined, value)}
        </div>
      </>
    ),
  };
  if (header) {
    formattedCellValue.header = header;
  }
  if (minWidth) {
    formattedCellValue.minWidth = minWidth;
  }
  return formattedCellValue;
}

/**
 * Method to get clickable column
 */
export const getClickableColumn = (
  identity: string,
  header?: string,
  panelTab?: string,
  minWidth?: number
) => {
  let formattedTextColumn: any = {
    identity: identity,
    Cell: (value, context) => (
      <>
        <div className="text-align--right">
          <ClickableColumn
            row={context.row}
            value={value}
            panelTab={panelTab}
          />
        </div>
      </>
    ),
    AggregatedCell: (value: string | number) => (
      <>
        <div className="text-align--right">
          {plainNumberFormatter(undefined, undefined, value)}
        </div>
      </>
    ),
  };
  if (header) {
    formattedTextColumn.header = header;
  }
  if (minWidth) {
    formattedTextColumn.minWidth = minWidth;
  }
  return formattedTextColumn;
};

/**
 * Method to reduce mmf booking details
 */
export function reduceMMFBookingDetails(data) {
  let mmfs: MapData[] = [];

  for (let key in data?.mmfList) {
    mmfs.push({ key: Number(key), value: data?.mmfList[key] });
  }

  mmfs.sort((a, b) => {
    const valueA = a.value.toLowerCase();
    const valueB = b.value.toLowerCase();
    if (valueA < valueB) return -1;
    if (valueA > valueB) return 1;
    return 0;
  });

  const sourceAccounts = reduceWireInstructions(
    Object.values(data.sourceWireInstructions).flatMap((x) => {
      return x;
    })
  );

  const destinationAccounts = reduceWireInstructions(
    Object.values(data.destinationWireInstructions).flatMap((x) => {
      return x;
    })
  );

  const portfoliosKeyValueAdded = data.portfolioList.map((data) => {
    data.key = data.id;
    data.value = data.name;
    return data;
  });

  const cas = data.custodianAccountList.map((data) => {
    data.key = data.id;
    data.value = data.name;
    return data;
  });

  return {
    mmfs: mmfs,
    sourceAccounts: sourceAccounts,
    destinationAccounts: destinationAccounts,
    portfolios: portfoliosKeyValueAdded,
    custodianAccounts: cas,
  };
}

/**
 * Method to reduce default mmf booking details
 */
export function reduceDefaultMMFBookingDetails(data) {
  const sourceAccounts = data.sourceAccounts.map((data) => {
    data.key = data.id;
    data.value = data.name;
    return data;
  });

  const destinationAccounts = data.destinationAccounts.map((data) => {
    data.key = data.id;
    data.value = data.name;
    return data;
  });

  const portfoliosKeyValueAdded = data.portfolioList.map((data) => {
    data.key = data.id;
    data.value = data.name;
    return data;
  });

  return {
    sourceAccounts: sourceAccounts,
    destinationAccounts: destinationAccounts,
    portfolios: portfoliosKeyValueAdded,
  };
}

/**
 * Method to reset default book mmf dialog values
 */
export const DefaultBookMMFOnReset = (defaultBookMMFFilter) => {
  let filter = INITIAL_BOOK_MMF_FILTERS;
  filter.selectedMMFSpn = defaultBookMMFFilter.selectedMMFSpn;
  filter.selectedAmount = defaultBookMMFFilter.selectedAmount;
  filter.selectedDestinationAccount =
    defaultBookMMFFilter.selectedDestinationAccount;
  filter.selectedCustodianAccount =
    defaultBookMMFFilter.selectedCustodianAccount;
  return filter;
};

const isCurrentBookingTimeValid = (cutoffTimeInMinutes: number) => {
  const currentTimeInMinutes = moment().hours() * 60 + moment().minutes();
  if (
    typeof cutoffTimeInMinutes == "number" &&
    currentTimeInMinutes < cutoffTimeInMinutes
  ) {
    return true;
  }
  return false;
};

const getHHmmTime = (minutes: number) => {
  if (minutes >= 0) {
    let timeString = "";
    timeString = ("0" + Math.floor(minutes / 60).toString())
      .slice(-2)
      .concat(":")
      .concat(("0" + (minutes % 60).toString()).slice(-2));
    return timeString;
  }
  return null;
};

const getToastType = (type: string) => {
  let toastType: any = ToastService.ToastType.SUCCESS;
  switch (type) {
    case TOAST_TYPE.CRITICAL:
      toastType = ToastService.ToastType.CRITICAL;
      break;
    case TOAST_TYPE.SUCCESS:
      toastType = ToastService.ToastType.SUCCESS;
      break;
    case TOAST_TYPE.INFO:
      toastType = ToastService.ToastType.INFO;
      break;
  }
  return toastType;
};

/**
 * Method to show toast service
 */
export const showToastService = (content: string, type: string) => {
  ToastService.append({
    content: content,
    type: getToastType(type),
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: 3000,
  });
};

const getMetaDataMapForTradeBooking = (filter) => {
  let metaDataMap: Map<string, string | null | undefined> = new Map();
  metaDataMap[SOURCE_ACCOUNT_ID] =
    filter.selectedSourceAccount?.key?.toString();
  metaDataMap[DESTINATION_ACCOUNT_ID] =
    filter.selectedDestinationAccount?.key?.toString();
  metaDataMap[SOURCE_PORTFOLIO_ID] =
    filter.selectedSourcePortfolio?.id?.toString();
  metaDataMap[DESTINATION_PORTFOLIO_ID] =
    filter.selectedDestinationPortfolio?.id?.toString();
  if (
    filter.isDefaultSourcePortfolioOverride == false &&
    metaDataMap[SOURCE_PORTFOLIO_ID] == undefined
  ) {
    metaDataMap[SOURCE_PORTFOLIO_ID] = filter.defaultSourcePortfolio?.id;
  }
  if (
    filter.isDefaultSourcePortfolioOverride == false &&
    metaDataMap[DESTINATION_PORTFOLIO_ID] == undefined
  ) {
    metaDataMap[DESTINATION_PORTFOLIO_ID] =
      filter.defaultDestinationPortfolio?.id;
  }
  metaDataMap[BUNDLE_ID] = filter.selectedBundles?.key?.toString();
  if (
    filter.isDefaultTradeBundleOverride == false &&
    metaDataMap[BUNDLE_ID] == undefined
  ) {
    metaDataMap[BUNDLE_ID] = filter.defaultTradeBundle?.id;
  }
  metaDataMap[TRADE_DATE] = filter.selectedTradeDate?.toString();
  metaDataMap[VALUE_DATE] = filter.selectedValueDate?.toString();
  metaDataMap[BOOK_EXTERNAL_TRADE] = filter.bookSSTrade?.toString();
  metaDataMap[BOOK_INTERNAL_TRADE] = filter.bookInternalTrade?.toString();
  metaDataMap[BOOK_INTERNAL_WIRE] = filter.logWire?.toString();
  metaDataMap[IS_TRACKING_WIRE] = filter.isTrackingWire?.toString();
  metaDataMap[COMMENT] =
    filter.selectedComment?.toString()?.length === 0
      ? DEFAULT_COMMENT
      : filter.selectedComment?.toString();
  return metaDataMap;
};

/**
 * Method to get trade confirmation message
 */
export const getTradeConfirmationMessage = (bookMMFFilter) => {
  let message = "";
  if (
    !bookMMFFilter.logWire &&
    !bookMMFFilter.bookSSTrade &&
    !bookMMFFilter.bookInternalTrade
  ) {
    message =
      "Please select atleast one of Log Wire, Book Internal Trade, Book External Trade.";
  } else if (bookMMFFilter.bookSSTrade) {
    message = "You have selected Book External Trade.";
  }
  return message;
};

/**
 * Method to validate wire and trade filter
 */
export const validateWireAndTradeFilter = (
  wireAndTradeFilter,
  liquidityManagementWorkflowStore: LiquidityManagementWorkflowStore
) => {
  if (
    !wireAndTradeFilter.logWire &&
    !wireAndTradeFilter.bookInternalTrade &&
    !wireAndTradeFilter.bookSSTrade
  ) {
    showToastService(MESSAGES.ERROR_WIRE_TRADE_FILTER, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    (wireAndTradeFilter.bookInternalTrade || wireAndTradeFilter.bookSSTrade) &&
    (wireAndTradeFilter.selectedMMFSpn === null ||
      wireAndTradeFilter.selectedMMFSpn === undefined)
  ) {
    showToastService(MESSAGES.ERROR_MMF_SPN, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    (wireAndTradeFilter.bookInternalTrade || wireAndTradeFilter.bookSSTrade) &&
    (wireAndTradeFilter.selectedCustodianAccount === null ||
      wireAndTradeFilter.selectedCustodianAccount?.length === 0)
  ) {
    showToastService(MESSAGES.ERROR_CUSTODIAN_ACCOUNT, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (wireAndTradeFilter.logWire && wireAndTradeFilter.selectedSubject === "") {
    showToastService(MESSAGES.ERROR_SUBJECT, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    wireAndTradeFilter.logWire &&
    (wireAndTradeFilter.selectedSourceAccount === null ||
      wireAndTradeFilter.selectedSourceAccount?.length === 0)
  ) {
    showToastService(MESSAGES.ERROR_SOURCE_ACCOUNT, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    wireAndTradeFilter.logWire &&
    (wireAndTradeFilter.selectedDestinationAccount === null ||
      wireAndTradeFilter.selectedDestinationAccount?.length === 0)
  ) {
    showToastService(MESSAGES.ERROR_DESTINATION_ACCOUNT, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    wireAndTradeFilter.logWire &&
    (wireAndTradeFilter.selectedSourcePortfolio === null ||
      wireAndTradeFilter.selectedSourcePortfolio?.length === 0) &&
    (wireAndTradeFilter.defaultSourcePortfolio === undefined ||
      wireAndTradeFilter.isDefaultSourcePortfolioOverride)
  ) {
    showToastService(MESSAGES.ERROR_SOURCE_PORTFOLIO, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    wireAndTradeFilter.logWire &&
    (wireAndTradeFilter.selectedDestinationPortfolio === null ||
      wireAndTradeFilter.selectedDestinationPortfolio?.length === 0) &&
    (wireAndTradeFilter.defaultDestinationPortfolio === undefined ||
      wireAndTradeFilter.isDefaultDestinationPortfolioOverride)
  ) {
    showToastService(MESSAGES.ERROR_DESTINATION_PORTFOLIO, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    wireAndTradeFilter.bookInternalTrade &&
    (wireAndTradeFilter.selectedTradeBook === null ||
      wireAndTradeFilter.selectedTradeBook?.length === 0) &&
    (wireAndTradeFilter.defaultTradeBook?.length === 0 ||
      wireAndTradeFilter.isDefaultTradeBookOverride)
  ) {
    showToastService(MESSAGES.ERROR_BOOK, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    wireAndTradeFilter.logWire &&
    liquidityManagementWorkflowStore.showDefaultBookMMFDialog &&
    (wireAndTradeFilter.selectedBundles === null ||
      wireAndTradeFilter.selectedBundles?.length === 0) &&
    (wireAndTradeFilter.defaultTradeBundle?.length === 0 ||
      wireAndTradeFilter.isDefaultTradeBundleOverride)
  ) {
    showToastService(MESSAGES.ERROR_BUNDLE, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (wireAndTradeFilter.selectedAmount === "") {
    showToastService(MESSAGES.ERROR_AMOUNT, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    liquidityManagementWorkflowStore.showBookMMFDialog &&
    (isNaN(wireAndTradeFilter.selectedAmount) ||
      wireAndTradeFilter.selectedAmount < 0)
  ) {
    showToastService(MESSAGES.INVALID_AMOUNT_ERROR, TOAST_TYPE.CRITICAL);
    return false;
  }
  if (
    wireAndTradeFilter.bookSSTrade &&
    wireAndTradeFilter.selectedTradeDate < getCurrentDate()
  ) {
    showToastService(MESSAGES.ERROR_EXTERNAL_TRADE, TOAST_TYPE.CRITICAL);
    return false;
  }
  return true;
};

/**
 * Method to validate mmf booking time
 */
export const isMMFBoookingTimeValid = (
  liquidityManagementReportStore: LiquidityManagementReportStore,
  liquidityManagementWorkflowStore: LiquidityManagementWorkflowStore,
  cutOffTime?: CutoffTime
) => {
  let message = "";
  if (cutOffTime) {
    let cutoffTime = cutOffTime.businessDayMMFThresholdTime;
    if (liquidityManagementReportStore.isTomorrowHoliday) {
      cutoffTime = cutOffTime.holidayMMFThresholdTime;
    }

    if (
      typeof cutoffTime == "number" &&
      !isCurrentBookingTimeValid(cutoffTime)
    ) {
      message += liquidityManagementReportStore.isTomorrowHoliday
        ? " Tomorrow is not a Business Day. Today's MMF Booking Time Limit Exceeded."
        : " Business Day MMF Booking Time Limit Exceeded.";
      message += " Cutoff Time is " + getHHmmTime(cutoffTime) + " HRS.";
    }
  } else {
    message += " MMF CutoffTime is not configured for the ticker.";
  }
  liquidityManagementReportStore.searchStatus.inProgress = false;
  liquidityManagementWorkflowStore.wireAndTradeMessage +=
    message + " Do you want to continue?";
  showTradeAndWireConfirmationDialog(
    MESSAGES.TRADE_AND_WIRE_CONFIRMATION,
    liquidityManagementWorkflowStore
  );
  return false;
};

const getStringList = (value: any) => {
  let stringList = value?.split(",");
  let result: string[] = [];
  for (let item of stringList) {
    let trimmedItem = item.trim();
    result.push(trimmedItem);
  }
  return result;
};

/**
 * Method to get initate trade booking payload for book mmf
 */

export const getInitateTradeBookingPayloadForBookMMF = (bookMMFFilter) => {
  let input: TradeWorkflowInputs = {
    spn: bookMMFFilter.selectedMMFSpn?.key,
    subject: bookMMFFilter.selectedSubject,
    subscriberList: getStringList(bookMMFFilter.selectedSubscribers),
    ccList: getStringList(bookMMFFilter.selectedCC),
    custodianAccountId: bookMMFFilter.selectedCustodianAccount?.key,
    currencySpn: USD_SPN,
    transactionType: parseFloat(bookMMFFilter.selectedAmount) >= 0 ? BUY : SELL,
    bookId: bookMMFFilter.selectedTradeBook?.key,
    amount: bookMMFFilter.selectedAmount
      ? Math.abs(
          parseFloat(bookMMFFilter.selectedAmount?.toString().replace(/,/g, ""))
        )
      : 0,
    metaData: getMetaDataMapForTradeBooking(bookMMFFilter),
    "@CLASS": CLASSES.TRADE_WORKFLOW_INPUTS,
  };
  return input;
};

/**
 * Method to get initate trade booking payload for default book mmf
 */
export const getInitateTradeBookingPayloadForDefaultBookMMF = (
  defaultBookMMFFilter
) => {
  let input = getInitateTradeBookingPayloadForBookMMF(defaultBookMMFFilter);
  if (
    defaultBookMMFFilter.isDefaultTradeBookOverride == false &&
    input["bookId"] == undefined
  ) {
    input["bookId"] = defaultBookMMFFilter.defaultTradeBook?.id;
  }
  return input;
};
