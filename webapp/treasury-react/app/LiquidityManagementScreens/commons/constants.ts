/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { getCurrentDate } from "../../commons/util";

/**
 * Defines all the common constants for all Liquidity Management Screens
 * @author burri
 */

 export const COMMON_MESSAGES = {
  COPY_SEARCH_URL: "Search URL copied to clipboard.",
};

export const URLS = {
  LIQUIDITY_MANAGEMENT_REPORT:
    "/treasury/liquidityManagement/liquidityManagementReport.html",
  WIRES_URL: "/wires/wireID/",
  WORKFLOW_STATUS: "/treasury/liquidityManagement/workflowStatus.html",
  GOVT_BOND_HOLDINGS: "/treasury/liquidityManagement/govtBondHoldings.html",
  DASHBOARD: "/treasury/liquidityManagement/summary.html"
};

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const CLASSES = {
  LIQUIDITY_MANAGEMENT_INPUT:
    "com.arcesium.treasury.calm.model.LiquidityManagementInput",
  TRADE_WORKFLOW_INPUTS: "com.arcesium.treasury.calm.model.TradeWorkflowInputs",
  ADJUSTMENT_INPUT: "com.arcesium.treasury.model.lcm.adjustment.Adjustment",
  FIRM_LIQUIDITY_DATA: "com.arcesium.treasury.calm.model.FirmLiquidityData",
  FUND_LIQUIDITY_DATA: "com.arcesium.treasury.calm.model.FundLiquidityData",
  MMF_DETAILS_INPUT: "com.arcesium.treasury.calm.model.MMFDetailsInput",
  BOND_ACTIVITY: "com.arcesium.treasury.calm.model.components.BondActivity"
};

export const INITIAL_FILTERS = {
  selectedDate: getCurrentDate(),
  selectedLegalEntityFamilies: [],
  selectedLegalEntities: [],
  selectedCpes: [],
  selectedAgreementTypes: [],
};

export const NOT_APPLICABLE = "n/a";
export const ENTITY_LEVEL = 0;
export const AGREEMENT_LEVEL = 1;
export const LEGAL_ENTITY = "LEGAL_ENTITY";
export const INPUT_FORMAT_PROPERTIES = "&inputFormat=properties&format=JSON";

