/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import * as H from 'history';
import MapData from '../../commons/models/MapData';

/**
 * Contains all the common models classes or interfaces
 * @author burri
 */

export interface LiquidityProps {
  location: H.Location
};

export interface SearchFilter {
  selectedDate: string;
  selectedLegalEntityFamilies: MapData[];
  selectedLegalEntities: MapData[];
  selectedCpes: MapData[];
  selectedAgreementTypes: MapData[];
};
