/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { formatFinancial } from "arc-commons/utils/number";
import MapData from "../../commons/models/MapData";
import queryString from "query-string";
import { getFilterListFromCommaSeparatedString } from "../../commons/util";
import { getCommaSeparatedKeysFromFilterList } from "../../commons/util";
import { SearchFilter } from "./models";
import moment from "moment";

/**
 * Defines all the common util methods for all Liquidity Management Screens
 * @author burri
 */

/* Method to calculate percentage */
export const calculatePercentage = (
  partialValue: number,
  totalValue: number
): number => {
  if (totalValue === 0) return 0;
  return formatFinancial((100 * partialValue) / totalValue);
};

/**
 * Method to get Legal Entity data based on global filters
 */

export const getExpandedGlobalSelections = (
  userSelections: any,
  expandedGlobalSelection: any
) => {
  let legalEntityDataList = expandedGlobalSelection["legalEntities"];
  let legalEntityFamilyDataList =
    expandedGlobalSelection["legalEntityFamilies"];

  let legalEntityDataMap: MapData[] = [];
  let legalEntityFamilyDataMap: MapData[] = [];

  legalEntityDataList.forEach((legalEntityData) => {
    let legalEntityId = parseInt(legalEntityData["id"]);
    let legalEntityName = legalEntityData["name"].concat(
      "[",
      legalEntityId,
      "]"
    );
    legalEntityDataMap.push({ key: legalEntityId, value: legalEntityName });
  });
  legalEntityFamilyDataList.forEach((legalEntityFamilyData) => {
    let legalEntityFamilyId = legalEntityFamilyData["id"];
    let legalEntityFamilyName = legalEntityFamilyData["name"].concat(
      "[",
      legalEntityFamilyId,
      "]"
    );
    legalEntityFamilyDataMap.push({
      key: legalEntityFamilyId,
      value: legalEntityFamilyName,
    });
  });
  return {
    legalEntityDataList: legalEntityDataMap,
    legalEntityFamilyDataList: legalEntityFamilyDataMap,
  };
};

/**
 * Method to get filters from url
 */
export const getFiltersFromUrl = (url: any) => {
  const values = queryString.parse(url);
  let filters = {
    selectedDate: values.date,
    selectedLegalEntityFamilies: getFilterListFromCommaSeparatedString(
      values.legalEntityFamilyIds
    ),
    selectedLegalEntities: getFilterListFromCommaSeparatedString(
      values.legalEntityIds
    ),
    selectedCpes: getFilterListFromCommaSeparatedString(values.cpeIds),
    selectedAgreementTypes: getFilterListFromCommaSeparatedString(
      values.agreementTypeIds
    ),
  };
  return filters;
};

/**
 * Method to get url from selected filters
 */
export const getURLFromSelectedFilters = (
  params: SearchFilter,
  location: string,
  screen: string
) => {
  let url =
    location +
    screen +
    `?date=${params.selectedDate}` +
    `&legalEntityFamilyIds=${getCommaSeparatedKeysFromFilterList(
      params.selectedLegalEntityFamilies
    )}` +
    `&legalEntityIds=${getCommaSeparatedKeysFromFilterList(
      params.selectedLegalEntities
    )}` +
    `&cpeIds=${getCommaSeparatedKeysFromFilterList(params.selectedCpes)}` +
    `&agreementTypeIds=${getCommaSeparatedKeysFromFilterList(
      params.selectedAgreementTypes
    )}`;

  return url;
};

/**
 * Method to get the current local date
 */
export function getCurrentDate() {
  var date = moment();
  return date.format("YYYY-MM-DD");
}
