/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { BASE_URL } from "../../commons/constants";
import { fetchPostURL } from "../../commons/util";
import SERVICE_URLS from "../../commons/UrlConfigs";

/**
 * Contains all the API calls definitions
 * @author burri
 */

export const fetchBondHoldings = (input) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.liquidityAssetManager.getGovtBondHoldings}`;
  let param = encodeURIComponent(JSON.stringify(input.date));
  let encodedParam = "date=" + param + "&inputFormat=json&format=json";

  return fetchPostURL(url, encodedParam);
};

export const fetchBondActivityList = (input) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.liquidityAssetManager.getTDaySettlingGovtBondHoldings}`;
  let param = encodeURIComponent(JSON.stringify(input.date));
  let encodedParam = `date=${param}&inputFormat=json&format=json`;

  return fetchPostURL(url, encodedParam);
};
