/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import { Card } from "arc-react-components";
import { DatePicker } from "arc-react-components";
import govtBondBalancesStore from "../useStores";
import FilterButton from "../../../commons/components/FilterButton";

/**
 * Component for rendering the Search filter
 * @author burri
 */

const SearchFilter: React.FC = () => {
  return (
    <Card>
      <div className="form">
        <div className="row">
          <div></div>
          <label className="size--content">Date:</label>
          <div className="size--content">
            <DatePicker
              placeholder="Select Date"
              value={govtBondBalancesStore.searchFilter.selectedDate}
              onChange={(date) => {
                govtBondBalancesStore.searchFilter.selectedDate = date;
              }}
              renderInBody
            />
          </div>
          <div className="size--content margin--left--double">
            <FilterButton
              onClick={govtBondBalancesStore.handleSearch}
              label="Search"
              className="button--primary"
            />
          </div>
          <div></div>
        </div>
      </div>
    </Card>
  );
};

export default observer(SearchFilter);
