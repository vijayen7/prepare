/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import govtBondBalancesStore from "../../useStores";
import { Message, Layout } from "arc-react-components";
import ArcDataGrid from "arc-data-grid";
import { getColumns, DEFAULT_AGGREGATION } from "./grid-columns";
import { DEFAULT_SORT_BY } from "../../constants";
import GridHeader from "../GridHeader";

/**
 * Renders the main grid based on the selected filters
 * @author burri
 */

const gridConfig = {
  clickableRows: true,
  showColumnAggregationRow: true,
  getExportFileName: ({ fileType, all }) => {
    return "govt-bond-holdings-report";
  },
};

const Grid: React.FC<any> = () => {
  const gridColumns = React.useMemo(getColumns, []);

  const onColumnChange = (displayColumns) => {
    govtBondBalancesStore.setDisplayColumns(displayColumns);
  };

  function renderGridData() {
    let gridData = govtBondBalancesStore.bondHoldings;

    if (gridData && gridData.length) {
      let grid = (
        <>
          <ArcDataGrid
            rows={gridData}
            columns={gridColumns}
            configurations={gridConfig}
            displayColumns={govtBondBalancesStore.displayColumns}
            onDisplayColumnsChange={onColumnChange}
            sortBy={DEFAULT_SORT_BY}
            aggregation={DEFAULT_AGGREGATION}
          />
        </>
      );
      let gridWithHeaders = (
        <>
          <Layout className="padding--right">
            <Layout.Child
              childId="gridHeader"
              size="fit"
              className="padding--bottom"
            >
              <GridHeader />
            </Layout.Child>
            <Layout.Child childId="govtBondHoldingsGrid">{grid}</Layout.Child>
          </Layout>
        </>
      );
      return gridWithHeaders;
    } else {
      const messageType = govtBondBalancesStore.searchStatus.error
        ? Message.Type.CRITICAL
        : Message.Type.PRIMARY;
      return (
        !govtBondBalancesStore.searchStatus.inProgress && (
          <>
            <div style={{ textAlign: "center" }}>
              <Message
                children={govtBondBalancesStore.searchStatus.message}
                type={messageType}
              />
            </div>
          </>
        )
      );
    }
  }

  return <>{renderGridData()}</>;
};

export default observer(Grid);
