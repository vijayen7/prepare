/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { plainNumberFormatter } from "../../../../commons/grid/formatters";

/**
 * Contains all the columns definition for the main grid
 * @author burri
 */

export function getColumns() {
  return [
    {
      identity: "fund",
      disableAggregation: true,
      width: 200,
    },
    "cusip",
    "spn",
    "bondDisplayName",
    "currencyCode",
    getNumericColumn("quantity", "Trade Date Quantity"),
    "custodianAccount",
    "bondMaturityDate",
    getNumericColumn("settleDateQuantity"),
    getNumericColumn("marketValueUsd", "Market Value USD"),
  ];
}

function getNumericColumn(identity: string, header?: string): any {
  let formattedNumericColumn: any = {
    identity: identity,
    Cell: (value) => {
      let result = plainNumberFormatter(undefined, undefined, value);
      if (
        result === "n/a" ||
        result === null ||
        result === undefined ||
        result === 0
      )
        return (
          <>
            <div className="text-align--right" style={{ opacity: 0.6 }}>
              {0}
            </div>
          </>
        );
      else
        return (
          <>
            <div className="text-align--right">{result}</div>
          </>
        );
    },
    AggregatedCell: (value: any) => (
      <>
        <div className="text-align--right">
          {plainNumberFormatter(undefined, undefined, value)}
        </div>
      </>
    ),
  };
  if (header) {
    formattedNumericColumn.header = header;
  }
  return formattedNumericColumn;
}

export const DEFAULT_AGGREGATION = {
  quantity: "sum",
  settleDateQuantity: "sum",
  marketValueUsd: "sum",
};
