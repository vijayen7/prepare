/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import govtBondBalancesStore from "../../useStores";

/**
 * Renders the grid Header with required buttons and filters
 * @author burri
 */

const GridHeader: React.FC = () => {
  return (
    <>
      <div className="form">
        <div className="row">
          <div></div>
          <div className="size--content">
            <button
              className="size--content margin--left"
              style={{ height: "auto" }}
              onClick={govtBondBalancesStore.generateReportFromAPI}
            >
              Create Report
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default observer(GridHeader);
