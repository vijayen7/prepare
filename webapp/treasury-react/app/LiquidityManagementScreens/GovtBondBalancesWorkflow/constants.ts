/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { getCurrentDate } from "../commons/utils";

/**
 * Defines all the constants
 * @author burri
 */

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const INITIAL_FILTERS = {
  selectedDate: getCurrentDate(),
};

export const MESSAGES = {
  INITIAL_SEARCH_MESSAGE: "Perform search to view Govt Bond Balances Data",
  NO_DATA_FOUND_MESSAGE:
    "No Govt Bond Balances Data found for the performed search",
  ERROR_MESSAGE: "Error Occurred while fetching Govt Bond Balances Data",
};

export const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  error: false,
  message: MESSAGES.INITIAL_SEARCH_MESSAGE,
};

export const URLS = {
  GOVT_BOND_HOLDINGS: "/treasury/liquidityManagement/govtBondHoldings.html",
};

export const DEFAULT_COLUMNS: string[] = [
  "fund",
  "cusip",
  "spn",
  "bondDisplayName",
  "currencyCode",
  "quantity",
  "custodianAccount",
  "bondMaturityDate",
  "settleDateQuantity",
  "marketValueUsd",
];

export const DEFAULT_SORT_BY = ["fund"];
