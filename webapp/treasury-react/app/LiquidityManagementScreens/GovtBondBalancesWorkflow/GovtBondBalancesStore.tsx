/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { action, observable } from "mobx";
import { SearchFilter, SearchStatus, FlattenedBondHolding } from "./models";
import {
  INITIAL_FILTERS,
  INITIAL_SEARCH_STATUS,
  MESSAGES,
  DEFAULT_COLUMNS,
} from "./constants";
import {
  convertJavaNYCDashedDate,
  getReportingUrl,
  openReportManager,
} from "../../commons/util";
import { fetchBondHoldings } from "./api";
import { INPUT_FORMAT_PROPERTIES } from "../commons/constants";
import SERVICE_URLS from "../../commons/UrlConfigs";

/**
 * Contains all the state variables and defines action or computation on them
 * @author burri
 */

export class GovtBondBalancesStore {
  @observable searchFilter: SearchFilter = INITIAL_FILTERS;
  @observable searchStatus: SearchStatus = INITIAL_SEARCH_STATUS;
  @observable displayColumns: string[] = DEFAULT_COLUMNS;

  @observable bondHoldings: FlattenedBondHolding[] = [];

  @action.bound
  onSelect = (params) => {
    const { key, value } = params;
    const selectedFilters = { ...this.searchFilter };
    selectedFilters[key] = value;
    this.setFilters(selectedFilters);
  };

  @action.bound
  applySavedFilters = (savedFilters: SearchFilter) => {
    this.setFilters(savedFilters);
  };

  @action
  setFilters = (newFilter: SearchFilter) => {
    this.searchFilter = newFilter;
  };

  @action.bound
  setDisplayColumns(displayColumns: string[]) {
    this.displayColumns = displayColumns;
  }

  @action.bound
  generateReportFromAPI() {
    let url = getReportingUrl(
      SERVICE_URLS.liquidityAssetManager.getGovtBondHoldings
    );
    url += `?date=${this.searchFilter.selectedDate}`;
    url += INPUT_FORMAT_PROPERTIES;
    openReportManager(url, null);
  }

  @action
  handleSearch = async () => {
    this.resetGovtBondHoldingsGridData();
    await this.getBondHoldingsDetails();
    this.setDisplayColumns(DEFAULT_COLUMNS);
  };

  @action
  resetGovtBondHoldingsGridData = () => {
    this.bondHoldings = [];
  };

  @action
  getBondHoldingsDetails = async () => {
    this.searchStatus = INITIAL_SEARCH_STATUS;
    this.searchStatus.inProgress = true;

    try {
      let input = {
        date: this.searchFilter.selectedDate,
      };
      const bondHoldingsDetails = await fetchBondHoldings(input);
      this.bondHoldings = this.getFlattenedBondHoldings(bondHoldingsDetails);

      if (
        bondHoldingsDetails?.response === null ||
        bondHoldingsDetails?.response[1]?.length === 0
      ) {
        this.searchStatus = {
          inProgress: false,
          error: false,
          message: MESSAGES.NO_DATA_FOUND_MESSAGE,
        };
      } else {
        this.searchStatus.inProgress = false;
      }
    } catch (e) {
      this.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  };

  @action
  getFlattenedBondHoldings = (data) => {
    let flattenedBondHoldings: FlattenedBondHolding[] = [];
    if (data && data.length !== 0) {
      data?.forEach((bondHoldingDetails) => {
        let flattenedBondHolding: FlattenedBondHolding = {
          fund: bondHoldingDetails?.fund?.name,
          bondMaturityDate: convertJavaNYCDashedDate(
            bondHoldingDetails?.maturityDate
          ),
          quantity: bondHoldingDetails?.underlyingPosition?.quantity,
          settleDateQuantity:
            bondHoldingDetails?.underlyingPosition?.settleDateQuantity,
          notional: bondHoldingDetails?.underlyingPosition?.notionalUsd,
          marketValueUsd:
            bondHoldingDetails?.underlyingPosition?.marketValueUsd,
          custodianAccount: bondHoldingDetails?.custodianAccount?.name,
          spn: bondHoldingDetails?.spn,
          bondDisplayName: bondHoldingDetails?.displayName,
          cusip: bondHoldingDetails?.cusip,
          currencyCode: bondHoldingDetails?.currencyCode,
        };
        flattenedBondHoldings.push(flattenedBondHolding);
      });
    }
    return flattenedBondHoldings;
  };
}
