/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

/**
 * Contains all the models classes or interfaces
 * @author burri
 */

export interface SearchFilter {
  selectedDate: string;
}

export interface SearchStatus {
  inProgress: boolean;
  error: boolean;
  message: string;
}

export interface FlattenedBondHolding {
  fund?: string;
  bondMaturityDate?: string;
  quantity?: string;
  settleDateQuantity?: number;
  notional?: number;
  marketValueUsd?: number;
  custodianAccount?: string;
  spn?: number;
  bondDisplayName?: string;
  cusip?: string;
  currencyCode?: string;
}
