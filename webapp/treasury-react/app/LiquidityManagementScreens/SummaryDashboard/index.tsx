/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import AppHeader, { whenHeaderRef } from "arc-header";
import { Breadcrumbs, Layout } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import { ReactLoader } from "../../commons/components/ReactLoader";
import { URLS, INITIAL_FILTERS } from "../commons/constants";
import Grid from "./containers/Grid";
import SideBar from "./containers/LiquiditySummarySideBar";
import { LiquidityProps } from "../commons/models";
import liquiditySummaryStore from "./useStores";
import { getFiltersFromUrl } from "../commons/utils";

/**
 * Rendering the screen with breadcrumbs and sidebar
 * @author burri
 */

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={"/"} key="liquidityManagement">
        Liquidity Management
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={URLS.DASHBOARD} key="liquidityManagementSummary">
        Summary
      </Breadcrumbs.Item>
    </>
  );
};

const LiquidityManagementSummary: React.FC<LiquidityProps> = (
  props: LiquidityProps
) => {
  useEffect(() => {
    let url = props.location.search;
    document.title = "Liquidity Summary";
    setGlobalFilters();
    updateBreadCrumbs();
    extractFiltersFromUrl(url);
  }, []);

  const setGlobalFilters = async () => {
    const headerRef = await whenHeaderRef();
    if (headerRef && headerRef.gf) {
      const options = {
        enabled: {
          strategies: false,
          fundFamilies: true,
          legalEntityFamilies: true,
        },
      };
      headerRef.gf.addListener(appHeaderListener);
      headerRef.gf.setOptions(options);
    }
  };

  const extractFiltersFromUrl = async (url: string) => {
    if (url != null && url.length > 0) {
      let filters = getFiltersFromUrl(url);
      if (filters !== INITIAL_FILTERS) {
        liquiditySummaryStore.applySavedFilters(filters);
        liquiditySummaryStore.handleSearch();
      }
    }
  };

  function appHeaderListener({ userSelections, expandedSelections }) {
    liquiditySummaryStore.setExpandedGlobalSelections(
      userSelections,
      expandedSelections
    );
  }

  return (
    <React.Fragment>
      <ReactLoader inProgress={liquiditySummaryStore.searchStatus.inProgress} />
      <div className="layout--flex--row">
        <AppHeader className="size--content" />
        <div className="layout--flex--row">
          <div className="layout--flex">
            <Layout isColumnType={true}>
              <Layout.Child
                childId="sideBar"
                size={2}
                collapsible
                title="Search Criteria"
                collapsed={liquiditySummaryStore.toggleSidebar}
                onChange={liquiditySummaryStore.setToggleSidebar}
              >
                <SideBar />
              </Layout.Child>
              <Layout.Divider
                childId="sideBarDivider"
                key="sideBarDivider"
                isResizable
              />
              <Layout.Child childId="dataDetail" size={9}>
                <Layout style={{ height: "100%" }}>
                  <Layout.Child childId="grid" collapsible>
                    <Grid />
                  </Layout.Child>
                </Layout>
              </Layout.Child>
            </Layout>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default observer(LiquidityManagementSummary);
