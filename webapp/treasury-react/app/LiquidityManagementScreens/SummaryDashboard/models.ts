/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import * as H from 'history';
import ReferenceData from "../../commons/models/ReferenceData";
import {CLASSES} from "../commons/constants";

/**
 * Contains all the models classes or interfaces
 * @author burri
 */

export interface LevelOneSummaryData {
  fund?: string;
  description: string;
  currency: string;
  counterParty: string;
  openingBalance: number;
  activity: number;
  closingBalance: number;
  fundOwnership?: number;
  children: LevelTwoSummaryData[];
}

export interface LevelTwoSummaryData {
  fund?: string;
  description: string;
  currency: string;
  counterParty?: string;
  openingBalance: number;
  activity: number;
  closingBalance: number;
  fundOwnership?: number;
  firmOwnership?: number;
  aum?: number;
  yield?: number;
  expenseRatio?: number;
}

export interface TreeSummaryData {
  fund?: string;
  description: string;
  currency: string;
  counterParty: string;
  openingBalance?: number;
  activity?: number;
  closingBalance?: number;
  children: LevelOneSummaryData[];
}

export interface BondActivity {
  fund?: ReferenceData;
  legalEntityId: number;
  primeBroker?: string;
  openingBalance: number;
  closingBalance: number;
  activity: number;
  currencyCode: string;
  "@CLASS": typeof CLASSES.BOND_ACTIVITY;
}

export interface PositionData {
  pnlSpn?: number;
  quantity?: number;
  marketValueUsd?: number;
}


