/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { TreeSummaryData } from "./models";
import { FundLiquidityData } from "../LiquidityManagementReport/models";
import { LEGAL_ENTITY, ENTITY_LEVEL, URLS } from "../commons/constants";

/**
 * Contains all the utility methods.
 * @author burri
 */

/**
 * Method to sort the given tree summary data list
 */
export const sortTreeSummaryDataList = (treeSummaryData: TreeSummaryData[]) => {
  treeSummaryData.sort((a, b) => {
    let first = a.fund?.toLowerCase();
    let second = b.fund?.toLowerCase();
    if (first === undefined || second === undefined) return 0;
    if (first < second) {
      return -1;
    }
    if (first > second) {
      return 1;
    }
    return 0;
  });
};

/**
 * Method to fetch fund name from fund liquidity data based on data level
 */
export const getFundNameFromFundLiquidityData = (
  fundLiquidityData: FundLiquidityData,
  dataLevel: number
) => {
  return dataLevel === ENTITY_LEVEL
    ? fundLiquidityData?.fundData?.name
    : fundLiquidityData?.metaDataMap[LEGAL_ENTITY]?.name;
};

/**
 * Method to fetch opening cash balance for MMF holdings
 */
export const getTotalOpeningBalanceForMMFHoldings = (
  data: FundLiquidityData
) => {
  let totalOpeningBalance = 0.0;
  let mmfHoldingsList = data?.underlyingData?.MMF_HOLDINGS;
  if (mmfHoldingsList !== undefined && mmfHoldingsList !== null) {
    mmfHoldingsList?.forEach((data) => {
      totalOpeningBalance += data?.balanceUsd;
    });
  }
  return totalOpeningBalance;
};
