/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

/**
 * Defines all the constants
 * @author burri
 */

export const MESSAGES = {
  INITIAL_SEARCH_MESSAGE: "Perform search to view Liquidity Summary Data",
  NO_DATA_FOUND_MESSAGE:
    "No Liquidity Summary Data found for the performed search",
  ERROR_MESSAGE: "Error occurred while fetching Liquidity Summary Data",
};

export const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  error: false,
  message: MESSAGES.INITIAL_SEARCH_MESSAGE,
};

export const DEFAULT_COLUMNS: string[] = [
  "fund",
  "description",
  "currency",
  "counterParty",
  "openingBalance",
  "activity",
  "closingBalance",
  "fundOwnership",
  "firmOwnership",
  "aum",
  "yield",
  "expenseRatio"
];

export const LIQUIDITY_BALANCE_EXCLUDING_SEG =  "Liquidity Pool Balance (EXCLUDING SEG)";
export const LIQUIDITY_BALANCE_INCLUDING_SEG =  "Liquidity Pool Balance (INCLUDING SEG)";
export const MMF_HOLDINGS = "MMF Holdings";
export const SEG_IA_MMF_HOLDINGS = "Seg IA MMF Holdings";
export const UNENCUMBERED_CASH = "Unencumbered Cash";
export const GOVT_BOND_HOLDINGS = "Govt Bond Holdings";
export const LEGAL_ENTITY = "LEGAL_ENTITY";



