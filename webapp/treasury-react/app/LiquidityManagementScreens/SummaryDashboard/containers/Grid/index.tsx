/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import ArcDataGrid from "arc-data-grid";
import { Layout, Message } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import liquiditySummaryStore from "../../useStores";
import GridHeader from "../GridHeader";
import { getColumns, DEFAULT_AGGREGATION } from "./grid-columns";

/**
 * Renders the main grid based on the selected filters
 * @author burri
 */

const gridConfig = {
  clickableRows: true,
  showColumnAggregationRow: true,
  enableTreeView: true,
  initialExpandLevel: 0,
  useAvailableScreenSpace: true,
  getExportFileName: ({ fileType, all }) => {
    return "liquidity-management-summary";
  },
};

const Grid: React.FC<any> = () => {
  const onColumnChange = (displayColumns) => {
    liquiditySummaryStore.setDisplayColumns(displayColumns);
  };
  const gridColumns = React.useMemo(getColumns, []);

  function renderGridData() {
    let gridData = liquiditySummaryStore.summaryData;

    if (gridData && gridData.length) {
      let grid = (
        <>
          <ArcDataGrid
            rows={gridData}
            columns={gridColumns}
            configurations={gridConfig}
            displayColumns={liquiditySummaryStore.displayColumns}
            onDisplayColumnsChange={onColumnChange}
            aggregation={DEFAULT_AGGREGATION}
          />
        </>
      );
      let gridWithHeaders = (
        <>
          <Layout className="border padding--left--small">
            <Layout.Child childId="quickActionsHeader" size="fit">
              <GridHeader />
            </Layout.Child>
            <Layout.Child childId="liquidityManagementSummaryGrid">
              {grid}
            </Layout.Child>
          </Layout>
        </>
      );
      return gridWithHeaders;
    } else {
      const messageType = liquiditySummaryStore.searchStatus.error
        ? Message.Type.CRITICAL
        : Message.Type.PRIMARY;
      return (
        !liquiditySummaryStore.searchStatus.inProgress && (
          <div className="margin--horizontal" style={{ textAlign: "center" }}>
            <Message
              children={liquiditySummaryStore.searchStatus.message}
              type={messageType}
            />
          </div>
        )
      );
    }
  }

  return <>{renderGridData()}</>;
};

export default observer(Grid);
