/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { plainNumberFormatter } from "../../../../commons/grid/formatters";
import {
  genericFormatter,
  getFormattedNumericColumn,
} from "../../../../commons/TreasuryUtils";
import { formatFinancial } from "arc-commons/utils/number";
import { NOT_AVAILABLE } from "../../../../commons/constants";

/**
 * Contains all the columns definition for the main grid
 * @author burri
 */

export function getColumns() {
  return [
    {
      identity: "fund",
      header: "Fund > Balance",
      disableAggregation: true,
      width: 280,
    },
    genericFormatter("description", "Description", 200),
    genericFormatter("currency"),
    genericFormatter("counterParty"),
    getNumericColumn("openingBalance", "Opening Balance"),
    getNumericColumn("activity", "Activity"),
    getNumericColumn("closingBalance", "Closing Balance"),
    {
      identity: "fundOwnership",
      header: "% of Fund Holdings",
      width: 140,
      Cell: (value, _) => {
        return <TokenFlag value={value} />;
      },
    },
    {
      identity: "firmOwnership",
      header: "% of AUM",
      Cell: (value, _) => {
        return <TokenFlag value={value} />;
      },
    },
    getFormattedNumericColumn("aum", "AUM"),
    {
      identity: "yield",
      header: "Yield",
      Cell: (value, _) => {
        return <TokenFlag value={value} />;
      },
    },
    getFormattedNumericColumn(
      "expenseRatio",
      "Expense Ratio",
      110,
      undefined,
      2
    ),
  ];
}

const getNumericColumn = (identity: string, header?: string) => {
  let formattedNumericColumn: any = {
    identity: identity,
    Cell: (value) => {
      let result = plainNumberFormatter(undefined, undefined, value);
      if (
        result === NOT_AVAILABLE ||
        result === null ||
        result === undefined ||
        parseInt(result) === 0
      )
        return (
          <>
            <div className="text-align--right" style={{ opacity: 0.6 }}>
              {0}
            </div>
          </>
        );
      else
        return (
          <>
            <div className="text-align--right">{result}</div>
          </>
        );
    },
    AggregatedCell: (value: any) => (
      <>
        <div className="text-align--right">
          {plainNumberFormatter(undefined, undefined, value)}
        </div>
      </>
    ),
  };
  if (header) {
    formattedNumericColumn.header = header;
  }
  return formattedNumericColumn;
};

function TokenFlag({ value }) {
  let source =
    value === undefined || value === null
      ? undefined
      : String(formatFinancial(value)) + "%";
  let token = (
    <span
      className={"token primary"}
      style={{
        width: "50%",
      }}
    >
      {source}
    </span>
  );
  return (
    <div className="text-align--center" title={source}>
      {token}
    </div>
  );
}

export const DEFAULT_AGGREGATION = {
  openingBalance: "sum",
  closingBalance: "sum",
  activity: "sum",
};
