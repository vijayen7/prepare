/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Layout } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import liquiditySummaryStore from "../../useStores";

/**
 * Renders the grid Header with required buttons and filters
 * @author burri
 */

const GridHeader: React.FC = () => {
  return (
    <Layout className="padding--vertical" isColumnType={true}>
      <Layout.Child childId="gridHeaderChild1" size="100%">
        <button
          onClick={liquiditySummaryStore.handleSearch}
          className="size--content margin--right--small"
        >
          <i className="icon-refresh margin--right" />
          Refresh Data
        </button>
      </Layout.Child>
    </Layout>
  );
};

export default observer(GridHeader);
