/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { Card, Sidebar as ArcSideBar } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import FilterButton from "../../../commons/components/FilterButton";
import SaveSettingsManager from "../../../commons/components/SaveSettingsManager";
import ArcDateFilter from "../../../commons/container/ArcDateFilter";
import MultiSelectFilter from "../../../commons/filters/components/MultiSelectFilter";
import CpeFilter from "../../../commons/container/CpeFilter";
import AgreementTypeFilter from "../../../commons/container/AgreementTypeFilter";
import { ENTITY_LEVEL, AGREEMENT_LEVEL } from "../../commons/constants";
import liquiditySummaryStore from "../useStores";

/**
 * Component for rendering the Sidebar filters
 * @author burri
 */

const SideBar: React.FC = () => {
  return (
    <ArcSideBar
      style={{ minWidth: "225px" }}
      footer={
        <span>
          <FilterButton
            label="Reset"
            onClick={liquiditySummaryStore.handleReset}
            className="margin--horizontal button--tertiary"
          />
          <FilterButton
            label="Search"
            onClick={liquiditySummaryStore.handleSearch}
            className="margin--horizontal button--primary float--right"
          />
          <FilterButton
            reset={true}
            onClick={liquiditySummaryStore.handleCopySearch}
            label="Copy Search URL"
            className="margin--horizontal button--tertiary float--right"
          />
        </span>
      }
      title={"Search Criteria"}
    >
      <Card>
        <div className="margin" style={{ height: "30px" }}>
          <SaveSettingsManager
            selectedFilters={liquiditySummaryStore.searchFilter}
            applySavedFilters={liquiditySummaryStore.applySavedFilters}
            applicationName="liquiditySummary"
          />
        </div>
      </Card>
      <Card>
        <ArcDateFilter
          layout="standard"
          stateKey="selectedDate"
          label="Date"
          onSelect={liquiditySummaryStore.onSelect}
          data={liquiditySummaryStore.searchFilter.selectedDate}
        />
        {liquiditySummaryStore.dataLevel === ENTITY_LEVEL && (
          <MultiSelectFilter
            data={liquiditySummaryStore.legalEntityFamilyDataList}
            onSelect={liquiditySummaryStore.onSelect}
            label="Legal Entity Families"
            selectedData={
              liquiditySummaryStore.searchFilter.selectedLegalEntityFamilies
            }
            stateKey="selectedLegalEntityFamilies"
            multiSelect
          />
        )}
        {liquiditySummaryStore.dataLevel === AGREEMENT_LEVEL && (
          <>
            <MultiSelectFilter
              data={liquiditySummaryStore.legalEntityDataList}
              onSelect={liquiditySummaryStore.onSelect}
              label="Legal Entities"
              selectedData={
                liquiditySummaryStore.searchFilter.selectedLegalEntities
              }
              stateKey="selectedLegalEntities"
              multiSelect
            />
            <CpeFilter
              label="Counterparty Entities"
              onSelect={liquiditySummaryStore.onSelect}
              selectedData={liquiditySummaryStore.searchFilter.selectedCpes}
              multiSelect
            />
            <AgreementTypeFilter
              label="Agreement Types"
              onSelect={liquiditySummaryStore.onSelect}
              selectedData={
                liquiditySummaryStore.searchFilter.selectedAgreementTypes
              }
              multiSelect
            />
          </>
        )}
      </Card>
    </ArcSideBar>
  );
};

export default observer(SideBar);
