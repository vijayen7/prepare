/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { action, observable } from "mobx";
import {
  SearchStatus,
  LiquidityManagementInput,
  FirmLiquidityData,
  FundLiquidityData,
} from "../LiquidityManagementReport/models";
import {
  TreeSummaryData,
  LevelOneSummaryData,
  LevelTwoSummaryData,
  BondActivity,
} from "./models";
import {
  INITIAL_SEARCH_STATUS,
  DEFAULT_COLUMNS,
  MESSAGES,
  LIQUIDITY_BALANCE_EXCLUDING_SEG,
  LIQUIDITY_BALANCE_INCLUDING_SEG,
  MMF_HOLDINGS,
  SEG_IA_MMF_HOLDINGS,
  UNENCUMBERED_CASH,
  GOVT_BOND_HOLDINGS,
  LEGAL_ENTITY,
} from "./constants";
import { UNMAPPED_FUND_ID, USD } from "../LiquidityManagementReport/constants";
import { fetchLiquidityReportData } from "../LiquidityManagementReport/api";
import { fetchBondActivityList } from "../GovtBondBalancesWorkflow/api";
import {
  flattenDictToList,
  addWithNullChecks,
  subWithNullChecks,
  showToastService,
} from "../LiquidityManagementReport/utils";
import MapData from "../../commons/models/MapData";
import {
  sortTreeSummaryDataList,
  getFundNameFromFundLiquidityData,
  getTotalOpeningBalanceForMMFHoldings,
} from "./utils";
import {
  calculatePercentage,
  getExpandedGlobalSelections,
  getURLFromSelectedFilters,
} from "../commons/utils";
import {
  COMMON_MESSAGES,
  CLASSES,
  TOAST_TYPE,
  ENTITY_LEVEL,
  INITIAL_FILTERS,
  URLS,
} from "../commons/constants";
import { SearchFilter } from "../commons/models";

/**
 * Contains all the state variables and defines action or computation on them
 * @author burri
 */

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

const copy = require("clipboard-copy");
export class LiquiditySummaryStore {
  @observable dataLevel = parseInt(
    CODEX_PROPERTIES["com.arcesium.treasury.calm.dataLevelId"]
  );
  @observable searchStatus: SearchStatus = INITIAL_SEARCH_STATUS;
  @observable searchFilter: SearchFilter = INITIAL_FILTERS;
  @observable toggleSidebar: boolean = false;
  @observable displayColumns: string[] = DEFAULT_COLUMNS;
  @observable legalEntityDataList: MapData[] = [];
  @observable legalEntityFamilyDataList: MapData[] = [];

  @observable.shallow summaryData: TreeSummaryData[] = [];
  @observable mmfSummaryMapData: Map<string, any> = new Map();
  @observable fundIdToGovtBondDataMap: Map<
    number,
    Map<string, BondActivity> | undefined
  > = new Map();
  @observable fundIdToPrimeBrokerMapForGovtBond: Map<
    number,
    string | undefined
  > = new Map();

  @action.bound
  setExpandedGlobalSelections(
    userSelections: any,
    expandedGlobalSelection: any
  ) {
    let globalSelection = getExpandedGlobalSelections(
      userSelections,
      expandedGlobalSelection
    );
    this.legalEntityDataList = globalSelection.legalEntityDataList;
    this.legalEntityFamilyDataList = globalSelection.legalEntityFamilyDataList;
  }

  @action.bound
  onSelect = (params) => {
    const { key, value } = params;
    const selectedFilters = { ...this.searchFilter };
    selectedFilters[key] = value;
    this.setFilters(selectedFilters);
  };

  @action
  setFilters = (newFilter: SearchFilter) => {
    this.searchFilter = newFilter;
  };

  @action.bound
  applySavedFilters = (savedFilters: SearchFilter) => {
    this.setFilters(savedFilters);
  };

  @action.bound
  setToggleSidebar = (value: boolean) => {
    this.toggleSidebar = value;
  };

  @action.bound
  setDisplayColumns(displayColumns: string[]) {
    this.displayColumns = displayColumns;
  }

  @action.bound
  handleCopySearch = () => {
    let location = window.location.origin;
    let url = getURLFromSelectedFilters(
      this.searchFilter,
      location,
      URLS.DASHBOARD
    );
    copy(url);
    showToastService(COMMON_MESSAGES.COPY_SEARCH_URL, TOAST_TYPE.INFO);
  };

  @action
  handleSearch = async () => {
    this.setToggleSidebar(true);
    await this.getLiquiditySummaryData();
  };

  @action.bound
  handleReset = () => {
    this.resetFilters();
  };

  @action
  resetFilters = () => {
    this.searchFilter = INITIAL_FILTERS;
    this.searchStatus = INITIAL_SEARCH_STATUS;
  };

  @action
  getLiquiditySummaryData = async () => {
    this.searchStatus = INITIAL_SEARCH_STATUS;
    this.searchStatus.inProgress = true;

    try {
      let input = this.getLiquiditySummaryPayload();
      const [liquidityReportData, govtBondHoldings] = await Promise.all([
        fetchLiquidityReportData(input),
        fetchBondActivityList(input),
      ]);
      this.summaryData = this.getTreeViewSummaryData(
        liquidityReportData?.response,
        govtBondHoldings?.response[1]
      );
      if (
        liquidityReportData?.response === undefined ||
        liquidityReportData?.response?.fundLiquidityData?.length == 0
      ) {
        this.searchStatus = {
          inProgress: false,
          error: false,
          message: MESSAGES.NO_DATA_FOUND_MESSAGE,
        };
      } else {
        this.searchStatus.inProgress = false;
      }
    } catch (e) {
      this.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  };

  @action
  getLiquiditySummaryPayload = () => {
    let input: LiquidityManagementInput = {
      date: this.searchFilter.selectedDate,
      legalEntityFamilyIds: flattenDictToList(
        this.searchFilter.selectedLegalEntityFamilies
      ),
      legalEntityIds: flattenDictToList(
        this.searchFilter.selectedLegalEntities
      ),
      cpeIds: flattenDictToList(this.searchFilter.selectedCpes),
      agreementTypeIds: flattenDictToList(
        this.searchFilter.selectedAgreementTypes
      ),
      "@CLASS": CLASSES.LIQUIDITY_MANAGEMENT_INPUT,
    };
    return input;
  };

  @action
  getTreeViewSummaryData = (
    firmLiquidityData: FirmLiquidityData,
    govtBondData: BondActivity[] | undefined
  ) => {
    let treeViewSummaryData: TreeSummaryData[] = [];
    if (firmLiquidityData !== null && firmLiquidityData !== undefined) {
      this.mmfSummaryMapData = firmLiquidityData.mmfSummaryMap;
      if (govtBondData && govtBondData?.length !== 0) {
        this.enrichGovtBondDataMap(govtBondData);
      }
      if (this.dataLevel !== ENTITY_LEVEL) {
        this.enrichDistinctEntityLiquidityData(firmLiquidityData);
      }
      firmLiquidityData.fundLiquidityData?.forEach((fundLiquidityData) => {
        if (fundLiquidityData?.fundData?.id === UNMAPPED_FUND_ID) return;
        let { openingBalance, closingBalance } =
          this.getFundOpeningAndClosingBalance(
            fundLiquidityData,
            this.dataLevel
          );
        let treeSummaryData: TreeSummaryData = {
          fund: getFundNameFromFundLiquidityData(
            fundLiquidityData,
            this.dataLevel
          ),
          description: "",
          currency: "",
          counterParty: "",
          openingBalance: openingBalance,
          activity: closingBalance - openingBalance,
          closingBalance: closingBalance,
          children: this.getLevelOneSummaryDataList(fundLiquidityData),
        };
        treeViewSummaryData.push(treeSummaryData);
      });
    }
    sortTreeSummaryDataList(treeViewSummaryData);
    return treeViewSummaryData;
  };

  @action
  getLevelOneSummaryDataList = (fundLiquidityData: FundLiquidityData) => {
    let levelOneSummaryDataList: LevelOneSummaryData[] = [];
    let { openingPoolBalanceWithoutSeg, closingPoolBalanceWithoutSeg } =
      this.getPoolBalanceExcludingSeg(fundLiquidityData);

    this.enrichPoolBalanceWithoutSeg(
      openingPoolBalanceWithoutSeg,
      closingPoolBalanceWithoutSeg,
      levelOneSummaryDataList
    );

    let poolBalanceWithSeg: LevelOneSummaryData = this.getPoolBalanceWithSeg(
      openingPoolBalanceWithoutSeg,
      fundLiquidityData,
      closingPoolBalanceWithoutSeg,
      levelOneSummaryDataList
    );

    this.enrichWithMMFHoldings(
      fundLiquidityData,
      poolBalanceWithSeg,
      levelOneSummaryDataList
    );

    this.enrichWithSegIAMMFHoldings(
      fundLiquidityData,
      poolBalanceWithSeg,
      levelOneSummaryDataList
    );

    this.dataLevel === ENTITY_LEVEL &&
      this.enrichWithUnencumberedCash(
        fundLiquidityData,
        poolBalanceWithSeg,
        levelOneSummaryDataList
      );

    this.enrichWithGovtBondHoldings(
      fundLiquidityData,
      poolBalanceWithSeg,
      levelOneSummaryDataList
    );
    return levelOneSummaryDataList;
  };

  @action
  enrichGovtBondDataMap = (bondHoldingList: BondActivity[]) => {
    bondHoldingList?.forEach((bondHolding) => {
      if (bondHolding?.fund?.id === undefined) return;
      if (!this.fundIdToGovtBondDataMap.has(bondHolding?.fund?.id)) {
        this.fundIdToGovtBondDataMap.set(bondHolding?.fund?.id, new Map());
      }
      let internalDataMap = this.fundIdToGovtBondDataMap.get(
        bondHolding?.fund?.id
      );
      internalDataMap?.set(bondHolding.currencyCode, bondHolding);
      this.fundIdToGovtBondDataMap.set(bondHolding?.fund?.id, internalDataMap);
      this.fundIdToPrimeBrokerMapForGovtBond.set(
        bondHolding?.fund?.id,
        bondHolding?.primeBroker
      );
    });
  };

  private enrichDistinctEntityLiquidityData = (
    firmLiquidityData: FirmLiquidityData
  ) => {
    let processedEntities: Set<number> = new Set();
    let uniqueFundLiquidityData: FundLiquidityData[] = [];
    firmLiquidityData.fundLiquidityData?.forEach((fundLiquidityData) => {
      if (fundLiquidityData?.fundData?.id === UNMAPPED_FUND_ID) return;
      if (
        fundLiquidityData?.fundData?.id &&
        !processedEntities.has(fundLiquidityData?.metaDataMap[LEGAL_ENTITY]?.id)
      ) {
        uniqueFundLiquidityData.push(fundLiquidityData);
        processedEntities.add(fundLiquidityData?.metaDataMap[LEGAL_ENTITY]?.id);
      }
    });
    firmLiquidityData.fundLiquidityData = uniqueFundLiquidityData;
  };

  private getLevelTwoMMFHoldings = (fundLiquidityData: FundLiquidityData) => {
    let levelTwoMMFHoldingsList: LevelTwoSummaryData[] = [];
    let mmfHoldingsList = fundLiquidityData?.underlyingData?.MMF_HOLDINGS;
    let totalPoolBalance = this.getTotalPoolBalance(fundLiquidityData);
    if (mmfHoldingsList !== undefined && mmfHoldingsList !== null) {
      mmfHoldingsList?.forEach((data) => {
        if (data?.balanceUsd === 0 || data?.mmf?.ticker === undefined) return;
        let levelTwoMMFHolding: LevelTwoSummaryData = {
          fund: data?.mmf?.ticker,
          description: data?.mmf?.ticker + " " + data?.custodian?.name,
          currency: USD,
          counterParty: data?.source,
          openingBalance: data?.balanceUsd,
          activity: data?.closingBalanceUsd - data?.balanceUsd,
          closingBalance: data?.closingBalanceUsd,
          fundOwnership: calculatePercentage(
            data?.closingBalanceUsd,
            totalPoolBalance
          ),
          firmOwnership: calculatePercentage(
            data?.closingBalanceUsd,
            this.mmfSummaryMapData[data?.mmf?.ticker]?.mmf?.mmfAumUsd
          ),
          aum: this.mmfSummaryMapData[data?.mmf?.ticker]?.mmf?.mmfAumUsd,
          yield: this.mmfSummaryMapData[data?.mmf?.ticker]?.mmf?.yield * 100,
          expenseRatio:
            this.mmfSummaryMapData[data?.mmf?.ticker]?.mmf?.expenseRatio,
        };
        levelTwoMMFHoldingsList.push(levelTwoMMFHolding);
      });
    }
    return levelTwoMMFHoldingsList;
  };

  private getLevelTwoSegIAMMFHoldings = (
    fundLiquidityData: FundLiquidityData
  ) => {
    let levelTwoMMFHoldingsList: LevelTwoSummaryData[] = [];
    let totalPoolBalance = this.getTotalPoolBalance(fundLiquidityData);
    let mmfHoldingsList =
      fundLiquidityData?.underlyingData?.SEG_IA_MMF_HOLDINGS;
    let tickerToBalanceMap: Map<string, number> = new Map();
    let tickerToPrimeBrokerMap: Map<string, string> = new Map();
    if (mmfHoldingsList !== undefined && mmfHoldingsList !== null) {
      mmfHoldingsList?.forEach((data) => {
        if (data?.balanceUsd === 0) return;
        if (!tickerToBalanceMap.has(data?.mmf?.ticker)) {
          tickerToBalanceMap.set(data?.mmf?.ticker, 0.0);
        }
        let prevBalance = tickerToBalanceMap.get(data?.mmf?.ticker);
        tickerToBalanceMap.set(
          data?.mmf?.ticker,
          addWithNullChecks(prevBalance, parseFloat(data?.balanceUsd))
        );
        tickerToPrimeBrokerMap.set(data?.mmf?.ticker, data?.source);
      });
      tickerToBalanceMap?.forEach((value, key) => {
        let levelTwoMMFHolding: LevelTwoSummaryData = {
          fund: key,
          description: key + " Seg IA",
          currency: USD,
          counterParty: tickerToPrimeBrokerMap.get(key),
          openingBalance: value,
          activity: 0,
          closingBalance: value,
          fundOwnership: calculatePercentage(value, totalPoolBalance),
          firmOwnership: calculatePercentage(
            value,
            this.mmfSummaryMapData[key]?.mmf?.mmfAumUsd
          ),
          aum: this.mmfSummaryMapData[key]?.mmf?.mmfAumUsd,
          yield: this.mmfSummaryMapData[key]?.mmf?.yield * 100,
          expenseRatio: this.mmfSummaryMapData[key]?.mmf?.expenseRatio,
        };
        levelTwoMMFHoldingsList.push(levelTwoMMFHolding);
      });
    }
    return levelTwoMMFHoldingsList;
  };

  private getTotalGovtBondBalanceForFund = (fundId?: number) => {
    if (fundId === undefined) return 0;
    let internalDataMap = this.fundIdToGovtBondDataMap.get(fundId);
    let totalBalance = 0.0;
    internalDataMap?.forEach((value, _) => {
      totalBalance += value?.closingBalance;
    });
    return totalBalance;
  };

  private getTotalOpeningGovtBondBalanceForFund = (fundId?: number) => {
    if (fundId === undefined) return 0;
    let internalDataMap = this.fundIdToGovtBondDataMap.get(fundId);
    let totalBalance = 0.0;
    internalDataMap?.forEach((value, _) => {
      totalBalance += value?.openingBalance;
    });
    return totalBalance;
  };

  private getTotalGovtBondActivityForFund = (fundId?: number) => {
    if (fundId === undefined) return 0;
    let internalDataMap = this.fundIdToGovtBondDataMap.get(fundId);
    let totalBalance = 0.0;
    internalDataMap?.forEach((value, _) => {
      totalBalance += value?.activity;
    });
    return totalBalance;
  };

  private getFundOpeningAndClosingBalance(
    fundLiquidityData: FundLiquidityData,
    dataLevel: number
  ) {
    let sodCashBalance = fundLiquidityData?.componentDataMap?.SOD_CASH_BALANCES;
    let projectedClosingBalance =
      fundLiquidityData?.componentDataMap?.PROJECTED_CLOSING_BALANCE;
    let openingBalance =
      dataLevel === ENTITY_LEVEL
        ? addWithNullChecks(
            addWithNullChecks(
              getTotalOpeningBalanceForMMFHoldings(fundLiquidityData),
              sodCashBalance
            ),
            this.getTotalGovtBondBalanceForFund(fundLiquidityData.fundData.id)
          )
        : addWithNullChecks(
            getTotalOpeningBalanceForMMFHoldings(fundLiquidityData),
            this.getTotalGovtBondBalanceForFund(fundLiquidityData.fundData.id)
          );

    let closingBalance =
      dataLevel === ENTITY_LEVEL
        ? addWithNullChecks(
            addWithNullChecks(
              fundLiquidityData?.componentDataMap?.MMF_HOLDINGS,
              projectedClosingBalance
            ),
            this.getTotalGovtBondBalanceForFund(fundLiquidityData.fundData.id)
          )
        : addWithNullChecks(
            fundLiquidityData?.componentDataMap?.MMF_HOLDINGS,
            this.getTotalGovtBondBalanceForFund(fundLiquidityData.fundData.id)
          );
    return { openingBalance, closingBalance };
  }

  private getPoolBalanceExcludingSeg(fundLiquidityData: FundLiquidityData) {
    let closingPoolBalanceWithoutSeg = addWithNullChecks(
      addWithNullChecks(
        fundLiquidityData?.componentDataMap?.MMF_HOLDINGS,
        fundLiquidityData?.componentDataMap?.PROJECTED_CLOSING_BALANCE
      ),
      this.getTotalGovtBondBalanceForFund(fundLiquidityData?.fundData?.id)
    );
    let openingPoolBalanceWithoutSeg =
      this.dataLevel === ENTITY_LEVEL
        ? addWithNullChecks(
            addWithNullChecks(
              fundLiquidityData?.componentDataMap?.SOD_CASH_BALANCES,
              getTotalOpeningBalanceForMMFHoldings(fundLiquidityData)
            ),
            this.getTotalGovtBondBalanceForFund(fundLiquidityData?.fundData?.id)
          )
        : addWithNullChecks(
            getTotalOpeningBalanceForMMFHoldings(fundLiquidityData),
            this.getTotalGovtBondBalanceForFund(fundLiquidityData?.fundData?.id)
          );
    return { openingPoolBalanceWithoutSeg, closingPoolBalanceWithoutSeg };
  }

  private enrichPoolBalanceWithoutSeg(
    openingPoolBalanceWithoutSeg: number,
    closingPoolBalanceWithoutSeg: number,
    levelOneSummaryDataList: LevelOneSummaryData[]
  ) {
    let poolBalance: LevelOneSummaryData = {
      fund: LIQUIDITY_BALANCE_EXCLUDING_SEG,
      description: "",
      currency: "",
      counterParty: "",
      openingBalance: openingPoolBalanceWithoutSeg,
      activity: closingPoolBalanceWithoutSeg - openingPoolBalanceWithoutSeg,
      closingBalance: closingPoolBalanceWithoutSeg,
      children: [],
    };
    levelOneSummaryDataList.push(poolBalance);
  }

  private getPoolBalanceWithSeg(
    openingPoolBalanceWithoutSeg: number,
    fundLiquidityData: FundLiquidityData,
    closingPoolBalanceWithoutSeg: number,
    levelOneSummaryDataList: LevelOneSummaryData[]
  ) {
    let poolBalanceWithSeg: LevelOneSummaryData = {
      fund: LIQUIDITY_BALANCE_INCLUDING_SEG,
      description: "",
      currency: "",
      counterParty: "",
      openingBalance: addWithNullChecks(
        openingPoolBalanceWithoutSeg,
        fundLiquidityData?.componentDataMap?.SEG_IA_MMF_HOLDINGS
      ),
      activity: closingPoolBalanceWithoutSeg - openingPoolBalanceWithoutSeg,
      closingBalance: addWithNullChecks(
        closingPoolBalanceWithoutSeg,
        fundLiquidityData?.componentDataMap?.SEG_IA_MMF_HOLDINGS
      ),
      children: [],
    };
    levelOneSummaryDataList.push(poolBalanceWithSeg);
    return poolBalanceWithSeg;
  }

  private enrichWithMMFHoldings(
    fundLiquidityData: FundLiquidityData,
    poolBalanceWithSeg: LevelOneSummaryData,
    levelOneSummaryDataList: LevelOneSummaryData[]
  ) {
    let openingBalanceForMMF =
      getTotalOpeningBalanceForMMFHoldings(fundLiquidityData);
    let mmfBalance: LevelOneSummaryData = {
      fund: MMF_HOLDINGS,
      description: "",
      currency: "",
      counterParty: "",
      openingBalance: openingBalanceForMMF,
      activity:
        fundLiquidityData?.componentDataMap?.MMF_HOLDINGS -
        openingBalanceForMMF,
      closingBalance: fundLiquidityData?.componentDataMap?.MMF_HOLDINGS,
      fundOwnership: calculatePercentage(
        fundLiquidityData?.componentDataMap?.MMF_HOLDINGS,
        poolBalanceWithSeg.closingBalance
      ),
      children: this.getLevelTwoMMFHoldings(fundLiquidityData),
    };
    levelOneSummaryDataList.push(mmfBalance);
  }

  private enrichWithSegIAMMFHoldings(
    fundLiquidityData: FundLiquidityData,
    poolBalanceWithSeg: LevelOneSummaryData,
    levelOneSummaryDataList: LevelOneSummaryData[]
  ) {
    let segIAMMFBalance: LevelOneSummaryData = {
      fund: SEG_IA_MMF_HOLDINGS,
      description: "",
      currency: "",
      counterParty: "",
      openingBalance: fundLiquidityData?.componentDataMap?.SEG_IA_MMF_HOLDINGS,
      activity: 0,
      closingBalance: fundLiquidityData?.componentDataMap?.SEG_IA_MMF_HOLDINGS,
      fundOwnership: calculatePercentage(
        fundLiquidityData?.componentDataMap?.SEG_IA_MMF_HOLDINGS,
        poolBalanceWithSeg.closingBalance
      ),
      children: this.getLevelTwoSegIAMMFHoldings(fundLiquidityData),
    };
    levelOneSummaryDataList.push(segIAMMFBalance);
  }

  private enrichWithUnencumberedCash(
    fundLiquidityData: FundLiquidityData,
    poolBalanceWithSeg: LevelOneSummaryData,
    levelOneSummaryDataList: LevelOneSummaryData[]
  ) {
    let activityForCashBalance = subWithNullChecks(
      fundLiquidityData?.componentDataMap?.PROJECTED_CLOSING_BALANCE,
      fundLiquidityData?.componentDataMap?.SOD_CASH_BALANCES
    );
    let cashBalance: LevelOneSummaryData = {
      fund: UNENCUMBERED_CASH,
      description: "",
      currency: "",
      counterParty: "",
      openingBalance: fundLiquidityData?.componentDataMap?.SOD_CASH_BALANCES,
      activity: activityForCashBalance,
      closingBalance:
        fundLiquidityData?.componentDataMap?.PROJECTED_CLOSING_BALANCE,
      fundOwnership: calculatePercentage(
        fundLiquidityData?.componentDataMap?.PROJECTED_CLOSING_BALANCE,
        poolBalanceWithSeg.closingBalance
      ),
      children: [],
    };
    levelOneSummaryDataList.push(cashBalance);
  }

  private enrichWithGovtBondHoldings(
    fundLiquidityData: FundLiquidityData,
    poolBalanceWithSeg: LevelOneSummaryData,
    levelOneSummaryDataList: LevelOneSummaryData[]
  ) {
    let bondHolding: LevelOneSummaryData = {
      fund: GOVT_BOND_HOLDINGS,
      description: "",
      currency: "",
      counterParty: "",
      openingBalance: this.getTotalOpeningGovtBondBalanceForFund(
        fundLiquidityData?.fundData?.id
      ),
      activity: this.getTotalGovtBondActivityForFund(
        fundLiquidityData?.fundData?.id
      ),
      closingBalance: this.getTotalGovtBondBalanceForFund(
        fundLiquidityData?.fundData?.id
      ),
      fundOwnership: calculatePercentage(
        this.getTotalGovtBondBalanceForFund(fundLiquidityData?.fundData?.id),
        poolBalanceWithSeg.closingBalance
      ),
      children: this.getLevelTwoBondHoldings(fundLiquidityData),
    };
    levelOneSummaryDataList.push(bondHolding);
  }

  private getTotalPoolBalance(fundLiquidityData: FundLiquidityData) {
    let totalPoolBalance = addWithNullChecks(
      addWithNullChecks(
        fundLiquidityData?.componentDataMap?.MMF_HOLDINGS,
        fundLiquidityData?.componentDataMap?.PROJECTED_CLOSING_BALANCE
      ),
      addWithNullChecks(
        fundLiquidityData?.componentDataMap?.SEG_IA_MMF_HOLDINGS,
        this.getTotalGovtBondBalanceForFund(fundLiquidityData?.fundData?.id)
      )
    );
    return totalPoolBalance;
  }

  private getLevelTwoBondHoldings = (fundLiquidityData: FundLiquidityData) => {
    let levelTwoBondHoldingsList: LevelTwoSummaryData[] = [];
    let fundId = fundLiquidityData?.fundData?.id;
    if (fundId === undefined || fundId === null)
      return levelTwoBondHoldingsList;

    let totalPoolBalance = this.getTotalPoolBalance(fundLiquidityData);

    this.fundIdToGovtBondDataMap?.get(fundId)?.forEach((value, key) => {
      let levelTwoBondHolding: LevelTwoSummaryData = {
        fund: key,
        description: key + " Govt Bond",
        currency: key,
        counterParty:
          fundId !== undefined
            ? this.fundIdToPrimeBrokerMapForGovtBond?.get(fundId)
            : "",
        openingBalance: value.openingBalance,
        activity: value.activity,
        closingBalance: value.closingBalance,
        fundOwnership: calculatePercentage(
          value.closingBalance,
          totalPoolBalance
        ),
      };
      levelTwoBondHoldingsList.push(levelTwoBondHolding);
    });
    return levelTwoBondHoldingsList;
  };
}
