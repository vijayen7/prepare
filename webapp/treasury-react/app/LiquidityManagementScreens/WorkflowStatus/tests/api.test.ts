/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import * as myApi from '../api';
import 'whatwg-fetch';

/**
 * Tests the api's
 * @author chawlasi
 */

describe('Api', () => {
  const error = { message: 'Testing catch blocksss' };
  const mockResponse = [
    { key: '1', value: 'one' },
    { key: '2', value: 'two' },
    { key: '3', value: 'three' },
  ];
  const payload = {
    selectedDate: '2021-08-31',
  };
  let fetchSpy = jest.spyOn(window, 'fetch');
  let mockFetchPromise: Promise<any>;

  beforeEach(() => {
    jest.clearAllMocks();
    mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => Promise.resolve(mockResponse),
    });
  });

  it('fetchTradeWorkflowDetails(): success', async () => {
    expect.assertions(2);
    fetchSpy.mockImplementation(() => mockFetchPromise as any);
    const data = await myApi.fetchTradeWorkflowDetails(payload);
    expect(window.fetch).toHaveBeenCalledTimes(1);
    expect(data).toEqual(mockResponse);
  });

  it('fetchTradeWorkflowDetails(): error', async () => {
    expect.assertions(1);
    let mockFetchError = Promise.reject(error);

    fetchSpy.mockImplementation(() => mockFetchError as any);
    try {
      await myApi.fetchTradeWorkflowDetails(payload);
    } catch (e) {
      expect(e).toEqual(error);
    }
  });
});
