/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import Grid from '../../../containers/Grid/index';
import { TradeMonitorStore } from '../../../TradeMonitorStore';
import React from 'react';

/**
 * Tests the main Grid
 * @author chawlasi
 */

describe('<Grid />', () => {
  let tradeMonitorStore: any;

  it('Rendering', () => {
    tradeMonitorStore = TradeMonitorStore;

    const { container } = render(<Grid />);
    expect(container).toMatchSnapshot();
  });
});
