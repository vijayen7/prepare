/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

jest.mock('arc-header', () => ({
  __esModule: true,
  whenHeaderRef: jest.fn().mockImplementation(() =>
    Promise.resolve({
      setBreadcrumbs: jest.fn(),
    })
  ),
  default: () => {
    return <div data-testid="AppHeader">AppHeader</div>;
  },
}));

jest.mock('../containers/SearchFilter', () => ({
  __esModule: true,
  default: () => {
    return <div data-testid="SearchFilter">SearchFilter</div>;
  },
}));

jest.mock('../containers/Grid/index', () => ({
  __esModule: true,
  default: () => {
    return <div data-testid="Grid">Grid</div>;
  },
}));

jest.mock('../containers/SideBar', () => ({
  __esModule: true,
  default: () => {
    return <div data-testid="SideBar">SideBar</div>;
  },
}));

import { render } from '@testing-library/react';
import { whenHeaderRef } from 'arc-header';
import LiquidityManagementTradeMonitor from '../../LiquidityManagementTradeMonitor/index';
import React from 'react';

/**
 * Tests the main screen
 * @author chawlasi
 */

describe('<LiquidityManagementTradeMonitor />', () => {
  it('Rendering', async () => {
    const { queryByTestId } = render(<LiquidityManagementTradeMonitor />);
    expect(whenHeaderRef).toHaveBeenCalledTimes(1);
    expect(queryByTestId('AppHeader')).not.toBeNull();
    expect(queryByTestId('SearchFilter')).not.toBeNull();
    expect(queryByTestId('Grid')).not.toBeNull();
    expect(queryByTestId('SideBar')).toBeNull();
  });
});
