/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */


import tradeMonitorStore from "../useStores";
import { TradeWorkflowDetails, TradeWorkflowInput } from "../models";
import { CLASSES } from "../constants";

/**
 * Tests the store component
 * @author chawlasi
 */
describe("TradeMonitorStore", () => {
  it("store trade workflow data", async () => {
    let dummyId = -1;
    let dummyName = "dummy";
    const dummyData: TradeWorkflowDetails[] = [
      {
        tradeBookingId: dummyId,
        transactionType: dummyName,
        initiatorUserLogin: dummyName,
        amount: dummyId,
        "@CLASS": CLASSES.TRADE_WORKFLOW_DETAILS
      }
    ]

    const tradeWorkFlowDetails = tradeMonitorStore.getFlattenedWorkflowData(dummyData);
    tradeMonitorStore.tradeWorkFlowData = tradeWorkFlowDetails;
    expect(tradeMonitorStore.tradeWorkFlowData).toHaveLength(1);
    expect(tradeMonitorStore.tradeWorkFlowData[0].orderAmount).toBe(dummyId);

  });
});
