/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { render } from '@testing-library/react';
import StatusLegend from '../../components/StatusLegend';
import { TradeMonitorStore } from '../../TradeMonitorStore';
import React from 'react';

/**
 * Tests the Status Legend Component
 * @author chawlasi
 */

describe('<StatusLegend />', () => {
  let tradeMonitorStore: any;

  it('Rendering', () => {
    tradeMonitorStore = TradeMonitorStore;

    const { container } = render(<StatusLegend />);
    expect(container).toMatchSnapshot();
  });
});
