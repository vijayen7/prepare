/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from 'react';

/**
 * Component for rendering the token value in the quick filters
 * @author burri
 */

type Props = {
  value: string
};

export const TokenCounter: React.FC<Props> = (props: Props) => {
  return <>
    {props.value !== undefined && props.value !== null &&
      <div
        className="margin--left"
      >
        {"(" + props.value + ")"}
      </div>
    }
  </>
}
