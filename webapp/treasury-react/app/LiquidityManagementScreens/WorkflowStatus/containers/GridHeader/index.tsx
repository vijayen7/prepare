/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { ToggleGroup } from "arc-react-components";
import { observer } from "mobx-react-lite";
import tradeMonitorStore from "../../useStores";
import { TokenCounter } from "./TokenCounter";
import StatusLegend from "../../components/StatusLegend";
import { ORANGE, RED, GREEN, TOKEN_BG_COLOR } from "../../constants";

/**
 * Renders the grid Header with required buttons and filters
 * @author burri
 */

const GridHeader: React.FC = () => {
  return (
    <>
      <div className="form">
        <div className="row">
          <label className="size--content">Quick Filters:</label>
          <div className="size--content">
            <ToggleGroup
              multiSelect
              value={tradeMonitorStore.selectedQuickFilter}
              onChange={async (value) => {
                tradeMonitorStore.setQuickFilters(value);
                tradeMonitorStore.handleApplyQuickFilter();
              }}
            >
              <ToggleGroup.Button key="quick2" data="ORANGE">
                <div className="display--flex">
                  <div
                    style={{
                      backgroundColor: TOKEN_BG_COLOR.ORANGE,
                      height: "1em",
                      width: "1em",
                      borderRadius: "100em",
                      margin: "3px",
                    }}
                  ></div>
                  <TokenCounter value={tradeMonitorStore.quickFilterCounter[ORANGE]} />
                </div>
              </ToggleGroup.Button>
              <ToggleGroup.Button key="quick3" data="RED">
                <div className="display--flex">
                  <div
                    style={{
                      backgroundColor: TOKEN_BG_COLOR.RED,
                      height: "1em",
                      width: "1em",
                      borderRadius: "100em",
                      margin: "3px",
                    }}
                  ></div>
                  <TokenCounter value={tradeMonitorStore.quickFilterCounter[RED]} />
                </div>
              </ToggleGroup.Button>
              <ToggleGroup.Button key="quick4" data="GREEN">
                <div className="display--flex">
                  <div
                    style={{
                      backgroundColor: TOKEN_BG_COLOR.GREEN,
                      height: "1em",
                      width: "1em",
                      borderRadius: "100em",
                      margin: "3px",
                    }}
                  ></div>
                  <TokenCounter value={tradeMonitorStore.quickFilterCounter[GREEN]} />
                </div>
              </ToggleGroup.Button>
            </ToggleGroup>
          </div>
          <div></div>
          <div className="size--content">
            <StatusLegend />
            <button
              className="size--content margin--left"
              style={{ height: "auto" }}
              onClick={tradeMonitorStore.generateReportFromAPI}
            >
              Create Report
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default observer(GridHeader);
