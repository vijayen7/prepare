/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import tradeMonitorStore from "../../useStores";
import { Layout, Message } from "arc-react-components";
import ArcDataGrid from "arc-data-grid";
import GridHeader from "../GridHeader";
import { DEFAULT_PINNED_COLUMNS } from "../../constants";
import { getColumns } from "./grid-columns";

/**
 * Renders the main grid based on the selected filters
 * @author burri
 */

const gridConfig = {
  clickableRows: true,
  getExportFileName: ({ fileType, all }) => {
    return "trade-monitor-report";
  },
};

const Grid: React.FC<any> = () => {
  const gridColumns = React.useMemo(getColumns, []);

  const onColumnChange = (displayColumns) => {
    tradeMonitorStore.setDisplayColumns(displayColumns);
  };
  const [sortBy, setSortBy] = React.useState([
    {
      identity: "arcOrderId",
      desc: true,
    },
  ]);
  const onSortByChange = (sortBy) => {
    setSortBy(sortBy);
  };

  function renderGridData() {
    let showGrid = tradeMonitorStore.tradeWorkFlowData.length ? true : false;
    let gridData = tradeMonitorStore.tradeWorkFlowDataToShow;

    if (showGrid) {
      let grid = (
        <>
          <ArcDataGrid
            rows={gridData}
            columns={gridColumns}
            configurations={gridConfig}
            clickedRow={tradeMonitorStore.clickedRow}
            onClickedRowChange={(clickedRow) => {
              tradeMonitorStore.setClickedRow(clickedRow);
              tradeMonitorStore.setToggleSidebar(false);
            }}
            displayColumns={tradeMonitorStore.displayColumns}
            onDisplayColumnsChange={onColumnChange}
            sortBy={sortBy}
            onSortByChange={onSortByChange}
            pinnedColumns={DEFAULT_PINNED_COLUMNS}
          />
        </>
      );
      let gridWithHeaders = (
        <>
          <Layout className="padding--right">
            <Layout.Child
              childId="quickActionsHeader"
              size="fit"
              className="padding--bottom"
            >
              <GridHeader />
            </Layout.Child>
            <Layout.Child childId="tradeMonitorGrid">{grid}</Layout.Child>
          </Layout>
        </>
      );
      return gridWithHeaders;
    } else {
      const messageType = tradeMonitorStore.searchStatus.error
        ? Message.Type.CRITICAL
        : Message.Type.PRIMARY;
      return (
        !tradeMonitorStore.searchStatus.inProgress && (
          <>
            <div style={{ textAlign: "center" }}>
              <Message
                children={tradeMonitorStore.searchStatus.message}
                type={messageType}
              />
            </div>
          </>
        )
      );
    }
  }

  return <>{renderGridData()}</>;
};

export default observer(Grid);
