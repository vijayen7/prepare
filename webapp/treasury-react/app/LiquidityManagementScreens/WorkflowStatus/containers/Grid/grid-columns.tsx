/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import {
  ACTION_CATEGORY_CODE_MAP,
  STATUS_CODE_MAP,
  UNDEFINED,
  NOT_APPLICABLE,
  AGREEMENT_LEVEL,
} from "../../constants";
import {
  getValueOrDefault,
  goToWiresSearch,
  goToTradeBlotter,
  getNumericColumn,
} from "../../utils/tradeWorkFlowUtils";
import {
  genericFormatter,
  getFormattedNumericColumn,
  genericBooleanFormatter,
} from "../../../../commons/TreasuryUtils";
import tradeMonitorStore from "../../useStores";

/**
 * Contains all the columns definition for the main grid
 * @author burri
 */

export function getColumns() {
  let dataLevel = tradeMonitorStore.dataLevel;
  return [
    ...getFundColumns(dataLevel),
    {
      identity: "externalTradeStatus",
      header: "External Trade Status",
      Cell: (_, context) => {
        return <ActionFlags status={context.row.externalTradeStatus} />;
      },
      width: 150,
    },
    {
      identity: "internalTradeStatus",
      header: "Internal Trade Status",
      Cell: (_, context) => {
        return <ActionFlags status={context.row.internalTradeStatus} />;
      },
      width: 150,
    },
    {
      identity: "wireStatus",
      header: "Wire Status",
      Cell: (_, context) => {
        return <ActionFlags status={context.row.wireStatus} />;
      },
      width: 120,
    },
    getNumericColumn("arcOrderId", "Arc Order ID"),
    {
      identity: "wireId",
      header: "Wire ID",
      Cell: (_, context) => {
        return (
          <>
            {context.row.wireId !== undefined && (
              <div className="text-align--right">
                <a
                  onClick={(e) => {
                    e.stopPropagation();
                    goToWiresSearch(context.row.wireId);
                  }}
                >
                  {context.row.wireId}
                </a>
              </div>
            )}
            {context.row.wireId === undefined && (
              <div className="text-align--right" style={{ opacity: 0.6 }}>
                {NOT_APPLICABLE}
              </div>
            )}
          </>
        );
      },
    },
    {
      identity: "internalTradeId",
      header: "Internal Trade ID",
      Cell: (_, context) => {
        return (
          <>
            {context.row.internalTradeId !== undefined && (
              <div className="text-align--right">
                <a
                  onClick={(e) => {
                    e.stopPropagation();
                    goToTradeBlotter(
                      context.row.internalTradeId,
                      tradeMonitorStore.searchFilter.selectedDate
                    );
                  }}
                >
                  {context.row.internalTradeId}
                </a>
              </div>
            )}
            {context.row.internalTradeId === undefined && (
              <div className="text-align--right" style={{ opacity: 0.6 }}>
                {NOT_APPLICABLE}
              </div>
            )}
          </>
        );
      },
    },
    genericFormatter("book", "Book", 120),
    genericFormatter("account", "Account", 200),
    genericFormatter("identifier", "Identifier Type"),
    genericFormatter("identifierValue", "Identifier"),
    genericFormatter("transactionType"),
    getFormattedNumericColumn("tradeQty", "Trade Quantity", 120, undefined, 2),
    genericFormatter("settlementDate"),
    genericBooleanFormatter("bookInternalTrade", "Book Internal Trade", 140),
    genericBooleanFormatter("bookExternalTrade", "Book External Trade", 140),
    genericBooleanFormatter("logInternalWire"),
    genericBooleanFormatter("isTrackingWire"),
    genericFormatter("orderId"),
    getNumericColumn("orderAmount"),
    getNumericColumn("allotedUnits"),
    getFormattedNumericColumn("price", "Price", 140, undefined, 4),
    getNumericColumn("settlementAmount", "Settlement Amount", 140),
    genericFormatter("settlementCurrency", "Settlement Currency", 140),
    genericFormatter("tradeDate"),
    genericFormatter("orderReason", "Order Reject/Void Reason", 180),
    genericFormatter("initiator", "Created By"),
    genericFormatter("createdOn", "Created On", 150),
    genericFormatter("levelOneApprover", "Level-1 Approver"),
    genericFormatter("levelOneApprovedOn", "Level-1 Updated On"),
    genericFormatter("levelTwoApprover", "Level-2 Approver"),
    genericFormatter("levelTwoApprovedOn", "Level-2 Updated On"),
  ];
}

function ActionFlags({ status }) {
  let tokenColor = getValueOrDefault(
    ACTION_CATEGORY_CODE_MAP[status],
    ACTION_CATEGORY_CODE_MAP[UNDEFINED]
  );
  let statusValue = getValueOrDefault(
    STATUS_CODE_MAP[status],
    STATUS_CODE_MAP[UNDEFINED]
  );

  let token = (
    <div
      className={"token"}
      style={{
        background: "rgb(var(--color-" + tokenColor + "))",
      }}
    >
      {statusValue}
    </div>
  );
  return (
    <div className="text-align--center" title={statusValue}>
      {token}
    </div>
  );
}

const getFundColumns = (dataLevel: number) => {
  let fundColumns;
  dataLevel === AGREEMENT_LEVEL
    ? (fundColumns = [
        {
          identity: "legalEntity",
          header: "Legal Entity",
          width: 200,
        },
        {
          identity: "counterPartyEntity",
          header: "Counter Party Entity",
          width: 140,
        },
        {
          identity: "agreementType",
          header: "Agreement Type",
        },
      ])
    : (fundColumns = [
        {
          identity: "fund",
          header: "Fund",
          disableAggregation: true,
          width: 200,
        },
      ]);
  return fundColumns;
};
