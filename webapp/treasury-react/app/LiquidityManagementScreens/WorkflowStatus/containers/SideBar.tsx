/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import { Card, Panel, Message } from "arc-react-components";
import tradeMonitorStore from "../useStores";
import { STATUS_CODE_MAP, NOT_APPLICABLE } from "../constants";
import { TradeApprovalData } from "../models";
import { convertJavaDateTime } from "../../../commons/util";

/**
 * Component for rendering the Sidebar Data
 * @author burri
 */

const SideBar: React.FC = () => {
  let workflowDetails = tradeMonitorStore.selectedWorkflowData;
  if (workflowDetails) {
    return (
      <>
        <Panel title="External Trade Details">
          <Card>
            <div className="form">
              <div className="row">
                <div className="form">
                  <strong>Order ID:</strong>
                  <StateStreetValue value={workflowDetails.orderId} />
                </div>
                <div className="form">
                  <strong>Arc Order ID:</strong>
                  <StateStreetValue value={workflowDetails.arcOrderId} />
                </div>
              </div>
            </div>
            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <strong>Order Status:</strong>
                  <StateStreetValue
                    value={STATUS_CODE_MAP[workflowDetails.externalTradeStatus]}
                  />
                </div>
                <div className="form">
                  <strong>Account:</strong>
                  <StateStreetValue value={workflowDetails.account} />
                </div>
              </div>
            </div>
            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <strong>Identifier:</strong>
                  <StateStreetValue value={workflowDetails.identifierValue} />
                </div>
                <div className="form">
                  <strong>Identifier Type:</strong>
                  <StateStreetValue value={workflowDetails.identifier} />
                </div>
              </div>
            </div>
            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <strong>Transaction Type:</strong>
                  <StateStreetValue value={workflowDetails.transactionType} />
                </div>
                <div className="form">
                  <strong>Order Amount:</strong>
                  <StateStreetValue value={workflowDetails.tradeQty} />
                </div>
              </div>
            </div>
            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <strong>Alloted Units:</strong>
                  <StateStreetValue value={workflowDetails.allotedUnits} />
                </div>
                <div className="form">
                  <strong>Price:</strong>
                  <StateStreetValue value={workflowDetails.price} />
                </div>
              </div>
            </div>
            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <strong>Settlement Amount:</strong>
                  <StateStreetValue value={workflowDetails.settlementAmount} />
                </div>
                <div className="form">
                  <strong>Settlement Currency:</strong>
                  <StateStreetValue
                    value={workflowDetails.settlementCurrency}
                  />
                </div>
              </div>
            </div>
            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <strong>Trade Date:</strong>
                  <StateStreetValue value={workflowDetails.tradeDate} />
                </div>
                <div className="form">
                  <strong>Settlement Date:</strong>
                  <StateStreetValue value={workflowDetails.settlementDate} />
                </div>
              </div>
            </div>
            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <strong>Order Reject/Void Reason:</strong>
                  <StateStreetValue value={workflowDetails.orderReason} />
                </div>
              </div>
            </div>
          </Card>
          <Card className="margin--top">
            <div className="form">
              <div className="row">
                <div className="form">
                  <strong>Created By:</strong>
                  <StateStreetValue value={workflowDetails.initiator} />
                </div>
                <div className="form">
                  <strong>Created On:</strong>
                  <StateStreetValue value={workflowDetails.createdOn} />
                </div>
              </div>
            </div>
            {workflowDetails?.approvalsData !== undefined &&
              workflowDetails?.approvalsData !== null &&
              workflowDetails?.approvalsData?.map(
                (workflowDetail: TradeApprovalData, index) => {
                  return (
                    <div className="form margin--top--double">
                      <div className="row">
                        <div className="form">
                          <strong>Level-{index + 1} Updated By:</strong>
                          <StateStreetValue
                            value={workflowDetail?.approverLogin}
                          />
                        </div>
                        <div className="form">
                          <strong>Level-{index + 1} Updated On:</strong>
                          <StateStreetValue
                            value={convertJavaDateTime(
                              workflowDetail?.responseDateTime
                            )}
                          />
                        </div>
                      </div>
                    </div>
                  );
                }
              )}
            {(workflowDetails?.approvalsData === undefined ||
              workflowDetails?.approvalsData === null) && (
              <div className="form margin--top--double">
                <div className="row">
                  <div className="form">
                    <strong>Level-{1} Updated By:</strong>
                    <div style={{ opacity: 0.6 }}>{NOT_APPLICABLE}</div>
                  </div>
                  <div className="form">
                    <strong>Level-{1} Updated On:</strong>
                    <div style={{ opacity: 0.6 }}>{NOT_APPLICABLE}</div>
                  </div>
                </div>
              </div>
            )}
          </Card>
        </Panel>
      </>
    );
  } else
    return (
      <>
        <Panel title="State Street Trade Details">
          <div style={{ textAlign: "center" }}>
            <Message
              children="Select a record to view its details."
              type={Message.Type.PRIMARY}
            />
          </div>
        </Panel>
      </>
    );
};

const StateStreetValue: React.FC<any> = ({ value }) => {
  return (
    <div>
      {value ? value : <div style={{ opacity: 0.6 }}>{NOT_APPLICABLE}</div>}
    </div>
  );
};

export default observer(SideBar);
