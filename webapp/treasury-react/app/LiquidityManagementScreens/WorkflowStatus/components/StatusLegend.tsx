/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import { Callout } from "arc-react-components";
import { STATUS_LEGEND } from "../constants";

/**
 * Component which renders status types
 * @author burri
 */

const { css, cx } = require("emotion");

const styles = {
  callout: cx(
    "border",
    "padding",
    css(`
      background-color: rgb(var(--color-body-inverted));
      color: rgb(var(--color-body-inverted-neutral));
    `)
  ),
};

const TableRow: React.FC<any> = ({ title, desc, bgValue }) => {
  return (
    <>
      <tr>
        <td className="padding">
          <div className={"token"} style={{ background: bgValue }}>
            {title}
          </div>
        </td>
        <td className="padding">{desc}</td>
      </tr>
    </>
  );
}

let statusLegendRows: any[] = [];
STATUS_LEGEND.forEach(statusLegendValue => {
  let statusLegendRow = <TableRow title={statusLegendValue.title} desc={statusLegendValue.desc} bgValue={statusLegendValue.bgValue} />
  statusLegendRows.push(statusLegendRow);
})
const StatusLegend: React.FC<any> = () => {
  function render() {
    return (
      <>
        <Callout
          renderCallout={({ orientation, alignment }) => (
            <div className={styles.callout}>
              <table>
                {statusLegendRows}
              </table>
            </div>
          )}
          disableOpenOnFocus
        >
          {({ isOpen, handleOpen, handleClose }) => (
            <button className="button--tertiary" onClick={() => (isOpen ? handleClose : handleOpen)()}>
              <i className="icon-help"></i> Status Legend
            </button>
          )}
        </Callout>
      </>
    );
  }
  return <>{render()}</>;
};

export default observer(StatusLegend);
