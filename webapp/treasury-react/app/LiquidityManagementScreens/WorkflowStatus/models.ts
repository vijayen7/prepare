/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

/**
 * Contains all the models classes or interfaces
 * @author burri
 */

import ReferenceData from "../../commons/models/ReferenceData";
import { CLASSES } from "./constants";
export interface SearchFilter {
  selectedDate: string;
};

export interface SearchStatus {
  inProgress: boolean;
  error: boolean;
  message: string;
};

export interface TradeWorkflowDetails {
  fund?: ReferenceData;
  tradeBookingId: number;
  tradeBookingDate?: string;
  transactionType: string;
  initiatorUserLogin: string;
  tradeSpn?: number;
  custodianAccount?: ReferenceData;
  book?: ReferenceData;
  currency?: ReferenceData;
  legalEntity?: ReferenceData;
  counterpartyEntity?: ReferenceData;
  agreementType?: ReferenceData;
  amount: number;
  externalTradeStatus?: ReferenceData;
  internalTradeStatus?: ReferenceData;
  wireStatus?: ReferenceData;
  approvalsData?: TradeApprovalData[];
  metaData?: any;
  "@CLASS": typeof CLASSES.TRADE_WORKFLOW_DETAILS;
}

export interface FlattenedTradeWorkFlow {
  counter: number;
  externalTradeStatus?: string;
  internalTradeStatus?: string;
  wireStatus?: string;
  arcOrderId?: number;
  orderId?: number;
  tradeQty?: number;
  orderAmount?: number;
  allotedUnits?: number;
  price?: number;
  transactionType?: string;
  account?: string;
  fund?: string;
  book?: string;
  legalEntity?: string;
  counterPartyEntity?: string;
  agreementType?: string;
  tradeDate?: string;
  settlementDate?: string;
  settlementAmount?: number;
  settlementCurrency?: string;
  orderReason?: string;
  identifier?: string;
  identifierValue?: number;
  initiator?: string;
  createdOn?: string;
  levelOneApprover?: string;
  levelTwoApprover?: string;
  levelOneApprovedOn?: string;
  levelTwoApprovedOn?: string;
  wireId?: number;
  internalTradeId?: number;
  bookInternalTrade?: boolean;
  bookExternalTrade?: boolean,
  logInternalWire?: boolean;
  isTrackingWire?: boolean;
  approvalsData?: TradeApprovalData[];
}

export interface TradeWorkflowInput {
  startDate: string;
  endDate: string;
  "@CLASS": typeof CLASSES.TRADE_WORKFLOW_INPUT;
}

export interface TradeApprovalData {
  approvalRequestId: number;
  tradeBookingId: number;
  initiatedDateTime: string;
  approvalStatusId: number;
  approverLogin: string;
  responseDateTime: string;
}
