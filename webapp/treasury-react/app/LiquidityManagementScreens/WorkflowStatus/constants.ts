/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { getCurrentDate } from '../commons/utils';

/**
 * Defines all the constants
 * @author burri
 */

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const INITIAL_FILTERS = {
  selectedDate: getCurrentDate(),
};

export const MESSAGES = {
  INITIAL_SEARCH_MESSAGE: "Perform search to view Trade Workflow Data",
  NO_DATA_FOUND_MESSAGE:
    "No Trade Workflow Data found for the performed search",
  ERROR_MESSAGE: "Error Occurred while fetching Trade Workflow Data",
};

export const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  error: false,
  message: MESSAGES.INITIAL_SEARCH_MESSAGE
};

export const URLS = {
  WORKFLOW_STATUS: "/treasury/liquidityManagement/workflowStatus.html",
};

export const CLASSES = {
  TRADE_WORKFLOW_INPUT: "com.arcesium.treasury.calm.model.TradeWorkflowInputFilter",
  TRADE_WORKFLOW_DETAILS: "com.arcesium.treasury.calm.model.TradeWorkFlowDetails"
}

export const INITIAL_QF_COUNTER = {
  "ORANGE": 0,
  "RED": 0,
  "GREEN": 0,
}

export const TOKEN_BG_COLOR = {
  "ORANGE": "rgb(var(--color-orange5))",
  "RED": "rgb(var(--color-red5))",
  "GREEN": "rgb(var(--color-green5))",
}

export const ORANGE = "ORANGE"
export const RED = "RED"
export const GREEN = "GREEN"
export const UNDEFINED = "UNDEFINED"

export const QUICK_FILTER_COLOR_MAP = {
  "APPROVAL_PENDING": "ORANGE",
  "APPROVAL_REJECTED": "RED",
  "SUBMITTED": "ORANGE",
  "SUBMISSION_FAILED": "RED",
  "SUCCESSFUL": "GREEN",
  "CANCELED": "RED",
  "REJECTED": "RED",
  "TRADE_INITIATED": "ORANGE",
  "IN_PROGRESS": "ORANGE"
}

export const ACTION_CATEGORY_CODE_MAP = {
  "APPROVAL_PENDING": "orange5",
  "APPROVAL_REJECTED": "red5",
  "SUBMITTED": "orange5",
  "SUBMISSION_FAILED": "red5",
  "SUCCESSFUL": "green5",
  "CANCELED": "red5",
  "REJECTED": "red5",
  "TRADE_INITIATED": "orange5",
  "IN_PROGRESS": "orange5",
  "UNDEFINED": "gray2",
}

export const STATUS_CODE_MAP = {
  "APPROVAL_PENDING": "Approval Pending",
  "APPROVAL_REJECTED": "Approval Rejected",
  "SUBMITTED": "Submitted",
  "SUBMISSION_FAILED": "Submission Failed",
  "SUCCESSFUL": "Successful",
  "CANCELED": "Canceled",
  "REJECTED": "Rejected",
  "TRADE_INITIATED": "Initiated",
  "IN_PROGRESS": "In-Progress",
  "UNDEFINED": "Undefined"
}

export const DEFAULT_COLUMNS: string[] = [
  'fund',
  'externalTradeStatus',
  'internalTradeStatus',
  'wireStatus',
  'arcOrderId',
  'wireId',
  'internalTradeId',
  'book',
  'account',
  'identifier',
  'identifierValue',
  'transactionType',
  'tradeQty',
  'settlementDate',
  'bookInternalTrade',
  'bookExternalTrade',
  'logInternalWire',
  'isTrackingWire',
]

export const DEFAULT_COLUMNS_FOR_AGREEMENT_FUND: string[] = [
  'legalEntity',
  'counterPartyEntity',
  'agreementType',
  'externalTradeStatus',
  'internalTradeStatus',
  'wireStatus',
  'arcOrderId',
  'wireId',
  'internalTradeId',
  'book',
  'account',
  'identifier',
  'identifierValue',
  'transactionType',
  'tradeQty',
  'settlementDate',
  'bookInternalTrade',
  'bookExternalTrade',
  'logInternalWire',
  'isTrackingWire',
]

export const STATUS_LEGEND = [
  { title: "Initiated", desc: "Logged workflow is being processed.", bgValue: TOKEN_BG_COLOR.ORANGE },
  { title: "Approval Pending", desc: "Logged trade is pending approval.", bgValue: TOKEN_BG_COLOR.ORANGE },
  { title: "Approval Rejected", desc: "Logged trade is rejected by approver.", bgValue: TOKEN_BG_COLOR.RED },
  { title: "Submitted", desc: "Relay of trade from Arcesium platform to SS successful.", bgValue: TOKEN_BG_COLOR.ORANGE },
  { title: "Submission Failed", desc: "Relay of trade from Arcesium platform to SS failed.", bgValue: TOKEN_BG_COLOR.RED },
  { title: "In-Progress", desc: "Orders has been accepted by target Transfer Agent.", bgValue: TOKEN_BG_COLOR.ORANGE },
  { title: "Rejected", desc: "Order has been rejected by SS or target Transfer Agent.", bgValue: TOKEN_BG_COLOR.RED },
  { title: "Canceled", desc: "Order is canceled by user after initial placement.", bgValue: TOKEN_BG_COLOR.RED },
  { title: "Successful", desc: "Priced confirmation for the placed trade is received from MMFP.", bgValue: TOKEN_BG_COLOR.GREEN }
]

export const DEFAULT_SORT_BY = ["arcOrderId"];
export const DEFAULT_PINNED_COLUMNS = {
  fund: true,
  status: true,
  legalEntity: true,
  counterPartyEntity: true,
  agreementType: true,
};
export const SUPPORTED_IDENTIFIERS = ["CUSIP", "ISIN", "RIC"];
export const NOT_APPLICABLE = "N/A";
export const INPUT_FORMAT = "&inputFormat=json&format=json"
export const INPUT_FORMAT_PROPERTIES = "&inputFormat=properties&format=json"
export const AGREEMENT_LEVEL = 1;


