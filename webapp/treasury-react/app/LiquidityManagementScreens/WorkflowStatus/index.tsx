/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { Breadcrumbs, Layout } from "arc-react-components";
import AppHeader, { whenHeaderRef } from "arc-header";
import { ReactLoader } from "../../commons/components/ReactLoader";
import SearchFilter from "./containers/SearchFilter";
import SideBar from "./containers/SideBar";
import { URLS, INITIAL_FILTERS } from "./constants";
import Grid from "./containers/Grid";
import tradeMonitorStore from "./useStores";
import { getFiltersFromUrl } from "../commons/utils";
import { LiquidityProps } from "../commons/models";

/**
 * Rendering the screen with breadcrumbs and sidebar
 * @author burri
 */

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={"/"} key="liquidityManagement">
        Liquidity Management
      </Breadcrumbs.Item>
      <Breadcrumbs.Item
        link={URLS.WORKFLOW_STATUS}
        key="liquidityManagementWorkflowStatus"
      >
        Workflow Status
      </Breadcrumbs.Item>
    </>
  );
};

const WorkflowStatus = (props: LiquidityProps) => {
  useEffect(() => {
    let url = props.location.search;
    document.title = "Workflow Status";
    updateBreadCrumbs();
    extractFiltersFromUrl(url);
  }, []);

  const extractFiltersFromUrl = async (url: string) => {
    if (url != null && url.length > 0) {
      let filters = getFiltersFromUrl(url);
      if (filters !== INITIAL_FILTERS) {
        tradeMonitorStore.applySavedFilters(filters);
        tradeMonitorStore.handleSearch();
      }
    }
  };

  return (
    <React.Fragment>
      <ReactLoader inProgress={tradeMonitorStore.searchStatus.inProgress} />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} />
        <Layout className="padding">
          <Layout.Child
            childId="searchDetail"
            size="content"
            className="gutter"
          >
            <SearchFilter />
          </Layout.Child>
          <Layout.Child childId="dataDetail" size={1} className="gutter">
            <Layout isColumnType={true}>
              <Layout.Child childId="gridDetail" size={1}>
                <Grid />
              </Layout.Child>
              {tradeMonitorStore.tradeWorkFlowData &&
                tradeMonitorStore.tradeWorkFlowData.length !== 0 && (
                  <>
                    <Layout.Divider
                      childId="sideBarDivider"
                      key="sideBarDivider"
                      isResizable
                    />
                    <Layout.Child
                      childId="sideBar"
                      size="350px"
                      collapsible
                      title="State Street Trade Details"
                      collapsed={tradeMonitorStore.toggleSidebar}
                      onChange={tradeMonitorStore.setToggleSidebar}
                    >
                      <SideBar />
                    </Layout.Child>
                  </>
                )}
            </Layout>
          </Layout.Child>
        </Layout>
      </div>
    </React.Fragment>
  );
};

export default observer(WorkflowStatus);
