/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { action, observable, computed } from "mobx";
import { SearchFilter, SearchStatus, TradeWorkflowInput, TradeWorkflowDetails, FlattenedTradeWorkFlow } from "./models";
import { fetchTradeWorkflowDetails } from "./api";
import {
  applyQuickFilter,
  getApplicableIdentifier,
  getApprovalMetaData,
  getReportManagerUrl,
} from "./utils/tradeWorkFlowUtils";
import { convertJavaNYCDashedDate, convertJavaDateTime, openReportManager } from "../../commons/util";
import {
  INITIAL_FILTERS,
  INITIAL_SEARCH_STATUS,
  MESSAGES,
  QUICK_FILTER_COLOR_MAP,
  CLASSES,
  DEFAULT_COLUMNS,
  INITIAL_QF_COUNTER,
  AGREEMENT_LEVEL,
  DEFAULT_COLUMNS_FOR_AGREEMENT_FUND,
} from "./constants";

/**
 * Contains all the state variables and defines action or computation on them
 * @author burri
 */

 declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

export class TradeMonitorStore {
  @observable dataLevel =
    parseInt(CODEX_PROPERTIES["com.arcesium.treasury.calm.dataLevelId"])
  @observable searchFilter: SearchFilter = INITIAL_FILTERS;
  @observable searchStatus: SearchStatus = INITIAL_SEARCH_STATUS;
  @observable displayColumns: string[] =
    this.dataLevel !== AGREEMENT_LEVEL
    ? DEFAULT_COLUMNS
    : DEFAULT_COLUMNS_FOR_AGREEMENT_FUND;
  @observable quickFilterCounter: any = INITIAL_QF_COUNTER;
  @observable selectedQuickFilter: Array<string> = [];
  @observable clickedRow: number | string | undefined = undefined;
  @observable toggleSidebar: boolean = true;

  @observable tradeWorkFlowData: Array<FlattenedTradeWorkFlow> = [];
  @observable tradeWorkFlowDataToShow: Array<FlattenedTradeWorkFlow> = [];

  keyToTradeWorkflowDataMap = {};

  @computed
  get selectedWorkflowData() {
    if (this.clickedRow !== null && this.clickedRow !== undefined) {
      return this.keyToTradeWorkflowDataMap[this.clickedRow];
    }
  }

  @action.bound
  onSelect = (params) => {
    const { key, value } = params;
    const selectedFilters = { ...this.searchFilter };
    selectedFilters[key] = value;
    this.setFilters(selectedFilters);
  };

  @action
  setFilters = (newFilter: SearchFilter) => {
    this.searchFilter = newFilter;
  };

  @action
  setQuickFilters = (quickFilters) => {
    let filtersApplied: string[] = [];
    quickFilters.forEach((element) => {
      filtersApplied.push(element);
    });
    this.selectedQuickFilter = filtersApplied;
  };

  @action.bound
  setClickedRow = (value: any) => {
    this.clickedRow = value;
  };

  @action.bound
  setDisplayColumns(displayColumns: string[]) {
    this.displayColumns = displayColumns;
  }

  @action.bound
  setToggleSidebar = (value: boolean) => {
    this.toggleSidebar = value;
  };

  @action.bound
  applySavedFilters = (savedFilters: SearchFilter) => {
    this.setFilters(savedFilters);
  };

  @action
  handleSearch = async () => {
    this.setToggleSidebar(true);
    this.resetTradeWorkflowGridData();
    await this.getTradeWorkflowDetails();
    this.dataLevel !== AGREEMENT_LEVEL ?
     this.setDisplayColumns(DEFAULT_COLUMNS) :
    this.setDisplayColumns(DEFAULT_COLUMNS_FOR_AGREEMENT_FUND)
    this.selectedQuickFilter = [];
    this.handleApplyQuickFilter();
  };

  @action
  resetTradeWorkflowGridData = () => {
    this.setClickedRow(null);
    this.tradeWorkFlowData = [];
    this.tradeWorkFlowDataToShow = [];
  };

  @action
  handleApplyQuickFilter = () => {
    this.tradeWorkFlowDataToShow = applyQuickFilter(this.tradeWorkFlowData, this.selectedQuickFilter);
  };

  @action.bound
  generateReportFromAPI() {
    let url = getReportManagerUrl(this.searchFilter);
    openReportManager(url, null);
  }

  @action
  getTradeWorkflowDetails = async () => {
    this.searchStatus = INITIAL_SEARCH_STATUS;
    this.searchStatus.inProgress = true;

    try {
      let input: TradeWorkflowInput = {
        startDate: this.searchFilter.selectedDate,
        endDate: this.searchFilter.selectedDate,
        "@CLASS": CLASSES.TRADE_WORKFLOW_INPUT
      };

      const tradeWorkflowDetails = await fetchTradeWorkflowDetails(input);
      if (tradeWorkflowDetails?.response[1].length === 0) {
        this.searchStatus = {
          inProgress: false,
          error: false,
          message: MESSAGES.NO_DATA_FOUND_MESSAGE,
        };
      } else {
        this.tradeWorkFlowData = this.getFlattenedWorkflowData(tradeWorkflowDetails?.response[1]);
        this.searchStatus.inProgress = false;
      }
    } catch (e) {
      this.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  };

  @action
  getFlattenedWorkflowData = (data: TradeWorkflowDetails[]) => {
    this.quickFilterCounter = INITIAL_QF_COUNTER;
    let flattenedTradeWorkflowDetails: FlattenedTradeWorkFlow[] = [];
    if (data.length !== 0) {
      let counter: number = 0;
      data?.forEach((tradeWorkflowDetails) => {
        let approvalMetaData = getApprovalMetaData(tradeWorkflowDetails?.approvalsData)
        let flattenedTradeWorkFlow: FlattenedTradeWorkFlow = {
          counter: counter,
          externalTradeStatus: tradeWorkflowDetails?.externalTradeStatus?.name,
          internalTradeStatus: tradeWorkflowDetails?.internalTradeStatus?.name,
          wireStatus: tradeWorkflowDetails?.wireStatus?.name,
          arcOrderId: tradeWorkflowDetails?.tradeBookingId,
          orderId: tradeWorkflowDetails?.metaData?.orderId,
          tradeQty: tradeWorkflowDetails?.amount,
          orderAmount: tradeWorkflowDetails?.amount,
          allotedUnits: tradeWorkflowDetails?.metaData?.cashOrderQty,
          price: tradeWorkflowDetails?.metaData?.price,
          transactionType: tradeWorkflowDetails?.transactionType,
          account: tradeWorkflowDetails?.custodianAccount?.name,
          fund: tradeWorkflowDetails?.fund?.name,
          book: tradeWorkflowDetails?.book?.name,
          legalEntity: tradeWorkflowDetails?.legalEntity?.name,
          counterPartyEntity: tradeWorkflowDetails?.counterpartyEntity?.name,
          agreementType: tradeWorkflowDetails?.agreementType?.name,
          tradeDate: convertJavaNYCDashedDate(tradeWorkflowDetails?.tradeBookingDate),
          settlementDate: convertJavaNYCDashedDate(tradeWorkflowDetails?.metaData?.settleDate),
          settlementAmount: tradeWorkflowDetails?.metaData?.settleCurrencyAmount,
          settlementCurrency: tradeWorkflowDetails?.metaData?.settleCurrency,
          orderReason: tradeWorkflowDetails?.metaData?.text,
          identifier: getApplicableIdentifier(tradeWorkflowDetails)?.identifier,
          identifierValue: getApplicableIdentifier(tradeWorkflowDetails)?.identifierValue,
          initiator: tradeWorkflowDetails?.initiatorUserLogin,
          createdOn: convertJavaDateTime(approvalMetaData?.createdOn),
          levelOneApprover: approvalMetaData?.levelOneApprover,
          levelTwoApprover: approvalMetaData?.levelTwoApprover,
          levelOneApprovedOn: convertJavaDateTime(approvalMetaData?.levelOneApprovedOn),
          levelTwoApprovedOn: convertJavaDateTime(approvalMetaData?.levelTwoApprovedOn),
          wireId: tradeWorkflowDetails?.metaData?.seedWireId,
          internalTradeId: tradeWorkflowDetails?.metaData?.seedTradeId,
          bookInternalTrade: tradeWorkflowDetails?.metaData?.bookInternalTrade,
          bookExternalTrade: tradeWorkflowDetails?.metaData?.bookExternalTrade,
          logInternalWire: tradeWorkflowDetails?.metaData?.logInternalWire,
          isTrackingWire: tradeWorkflowDetails?.metaData?.bookTrackingWire,
          approvalsData: approvalMetaData?.approvalData
        };
        this.keyToTradeWorkflowDataMap[counter++] = flattenedTradeWorkFlow;
        if (tradeWorkflowDetails?.externalTradeStatus?.name !== null && tradeWorkflowDetails?.externalTradeStatus?.name !== undefined) {
          let curValue = this.quickFilterCounter[QUICK_FILTER_COLOR_MAP[tradeWorkflowDetails?.externalTradeStatus?.name]];
          this.quickFilterCounter[QUICK_FILTER_COLOR_MAP[tradeWorkflowDetails?.externalTradeStatus?.name]] = curValue + 1;
        }
        flattenedTradeWorkflowDetails.push(flattenedTradeWorkFlow);
      });
    }
    return flattenedTradeWorkflowDetails;
  };
}
