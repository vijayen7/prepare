/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { TradeMonitorStore } from "./TradeMonitorStore";

/**
 * Returns the new instance of a store
 * @author burri
 */

const tradeMonitorStore = new TradeMonitorStore();
export default tradeMonitorStore
