/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { BASE_URL } from "../../commons/constants";
import { fetchPostURL } from "../../commons/util";
import SERVICE_URLS from "../../commons/UrlConfigs";
import { INPUT_FORMAT } from "./constants";

/**
 * Contains all the API calls definitions
 * @author burri
 */

export const fetchTradeWorkflowDetails = (input) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.tradeWorkflowManager.getTradeWorkflowDetails}`;
  let param = encodeURIComponent(JSON.stringify(input));
  let encodedParam = "tradeWorkflowInputFilter=" + param + INPUT_FORMAT;

  return fetchPostURL(url, encodedParam);
};
