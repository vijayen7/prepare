/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { SUPPORTED_IDENTIFIERS, QUICK_FILTER_COLOR_MAP } from "../constants";
import { getReportingUrl } from "../../../commons/util";
import SERVICE_URLS from "../../../commons/UrlConfigs";
import {
  SearchFilter,
  FlattenedTradeWorkFlow,
  TradeWorkflowDetails,
  TradeApprovalData,
} from "../models";
import { TRADE_BLOTTER_URL, WIRES_URL } from "../../../commons/constants";
import { INPUT_FORMAT_PROPERTIES } from "../constants";
import { plainNumberFormatter } from "../../../commons/grid/formatters";
import { NOT_AVAILABLE } from "../../../commons/constants";

/**
 * Contains all the utility methods and classes.
 * @author burri
 */

/**
 * Method to apply quick filters based on toggle button value
 */
export function applyQuickFilter(
  tradeWorkFlowDataList: FlattenedTradeWorkFlow[],
  selectedQuickFilters: string[]
) {
  if (selectedQuickFilters.length === 0) return tradeWorkFlowDataList;

  var outputList: FlattenedTradeWorkFlow[] = [];
  for (let data of tradeWorkFlowDataList) {
    let quickFilterDataExists = false;
    for (let quickFilter of selectedQuickFilters) {
      if (
        data?.externalTradeStatus !== undefined &&
        QUICK_FILTER_COLOR_MAP[data?.externalTradeStatus] === quickFilter
      )
        quickFilterDataExists = true;
    }
    if (quickFilterDataExists) {
      outputList.push(data);
    }
  }
  return outputList;
}

/**
 * Method to fetch the object or the default value
 */
export const getValueOrDefault = (obj: any, defaultValue = {}) => {
  if (obj) return obj;
  else return defaultValue;
};

/**
 * Method to fetch the applicable identifier
 */
export const getApplicableIdentifier = (
  tradeWorkFlowData?: TradeWorkflowDetails
) => {
  let identifier, identifierValue;
  for (let key in tradeWorkFlowData?.metaData) {
    if (
      key &&
      key !== null &&
      SUPPORTED_IDENTIFIERS.includes(key.toUpperCase())
    ) {
      identifier = key;
      identifierValue = tradeWorkFlowData?.metaData[key];
    }
  }
  return {
    identifier: identifier?.toUpperCase(),
    identifierValue: identifierValue,
  };
};

/**
 * Method to fetch the approval meta data
 */
export const getApprovalMetaData = (approvalData?: TradeApprovalData[]) => {
  if (
    approvalData === null ||
    approvalData === undefined ||
    approvalData.length === 0
  )
    return;
  approvalData.sort((approvalData1, approvalData2) =>
    approvalData1.approvalRequestId > approvalData2.approvalRequestId ? 1 : -1
  );
  return {
    levelOneApprover: approvalData[0]?.approverLogin,
    levelTwoApprover: approvalData[1]?.approverLogin,
    createdOn: approvalData[0]?.initiatedDateTime,
    levelOneApprovedOn: approvalData[0]?.responseDateTime,
    levelTwoApprovedOn: approvalData[1]?.responseDateTime,
    approvalData: approvalData,
  };
};

/**
 * Method to redirect to wires screen
 */
export const goToWiresSearch = (value: number) => {
  let url = WIRES_URL + value;
  window.open(url, "_blank");
};

/**
 * Method to redirect to trade blotter screen
 */
export const goToTradeBlotter = (value: string, date: string) => {
  let url =
    TRADE_BLOTTER_URL +
    value +
    `&startDate=` +
    date +
    `&endDate=` +
    date +
    `&legacyMode=true&groupingId=0&bookType=1&strDateSearchField=0` +
    `&bookIds=-1&groupingLevel=0&tradeType=0&tradeStatus=1`;
  window.open(url, "_blank");
};

/**
 * Method to fetch the report manager URL for generation of report
 */
export const getReportManagerUrl = (tradeWorkflowInput: SearchFilter) => {
  let url = getReportingUrl(
    SERVICE_URLS.tradeWorkflowManager.getFlattenTradeWorkflowDetails
  );
  url += `?filter.startDate=${tradeWorkflowInput.selectedDate}`;
  url += `&filter.endDate=${tradeWorkflowInput.selectedDate}`;
  url += INPUT_FORMAT_PROPERTIES;
  return url;
};

export function getNumericColumn(
  identity: string,
  header?: string,
  width?: number
): any {
  let formattedNumericColumn: any = {
    identity: identity,
    Cell: (value) => {
      let result = plainNumberFormatter(undefined, undefined, value);
      if (
        result === NOT_AVAILABLE ||
        result === null ||
        result === undefined ||
        result === 0
      )
        return (
          <>
            <div className="text-align--right" style={{ opacity: 0.6 }}>
              {0}
            </div>
          </>
        );
      else
        return (
          <>
            <div className="text-align--right">{result}</div>
          </>
        );
    },
    AggregatedCell: (value: any) => (
      <>
        <div className="text-align--right">
          {plainNumberFormatter(undefined, undefined, value)}
        </div>
      </>
    ),
  };
  if (header) {
    formattedNumericColumn.header = header;
  }
  if (width) {
    formattedNumericColumn.width = width;
  }
  return formattedNumericColumn;
}
