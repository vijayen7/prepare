/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { Breadcrumbs, Layout, SectionPanel } from "arc-react-components";
import AppHeader, { whenHeaderRef } from "arc-header";
import Loader from "../../commons/container/Loader";
import { ReactLoader } from "../../commons/components/ReactLoader";
import liquidityConfigStore from "./useStores";
import { URLS } from "./constants";
import TradeConfiguration from "./containers/TradeConfiguration";
import DataConfiguration from "./containers/DataConfiguration";
import WireConfiguration from "./containers/WireConfiguration";

/**
 * Rendering the screen with breadcrumbs and sidebar
 * @author burri
 */

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={"/"} key="liquidityManagement">
        Liquidity Management
      </Breadcrumbs.Item>
      <Breadcrumbs.Item
        link={URLS.CONFIGURATION}
        key="liquidityManagementConfig"
      >
        Configuration
      </Breadcrumbs.Item>
    </>
  );
};

const LiquidityManagementConfig = () => {
  useEffect(() => {
    document.title = "Configuration";
    updateBreadCrumbs();
    liquidityConfigStore.getLiquidityConfigData();
  }, []);

  const [selectedSectionId, setSelectedSectionId] = React.useState("sec1");
  const handleClick = (id) => {
    setSelectedSectionId(id);
  };

  return (
    <React.Fragment>
      <Loader />
      <ReactLoader inProgress={liquidityConfigStore.searchStatus.inProgress} />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} />
        <Layout isColumnType={true}>
          <Layout.Child
            childId="dataConfigDetail"
            size="1"
            className="padding--double"
          >
            <SectionPanel
              selectedSectionId={selectedSectionId}
              onSelect={handleClick}
            >
              <SectionPanel.Section sectionId="sec1" label="Data Configuration">
                <DataConfiguration />
              </SectionPanel.Section>
              <SectionPanel.Section
                sectionId="sec2"
                label="Trade Configuration"
              >
                <TradeConfiguration />
              </SectionPanel.Section>
              <SectionPanel.Section sectionId="sec3" label="Wire Configuration">
                <WireConfiguration />
              </SectionPanel.Section>
            </SectionPanel>
          </Layout.Child>
        </Layout>
      </div>
    </React.Fragment>
  );
};

export default observer(LiquidityManagementConfig);
