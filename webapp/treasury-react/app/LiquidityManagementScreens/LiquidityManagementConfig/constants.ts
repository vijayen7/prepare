/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */
import {RAGThreshold, GovtBondConfig} from "./models";
/**
 * Defines all the constants
 * @author burri
 */

export const MESSAGES = {
  NO_DATA_FOUND_MESSAGE:
    "No Liquidity Configuration found",
  ERROR_MESSAGE: "Error Occurred while fetching Liquidity Configuration",
};

export const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  error: false,
  message: '',
};

export const INITIAL_RAG_THRESHOLDS : RAGThreshold = {
  clientAmberThreshold: 0,
  clientRedThreshold: 0,
  fundAmberThreshold: 0,
  fundRedThreshold: 0
}

export const INITIAL_GOVT_BOND_DATA_CONFIG: GovtBondConfig = {
  books: [],
  currencies: [],
  accounts: [],
}

export const URLS = {
  CONFIGURATION: "/treasury/liquidityManagement/configuration.html",
};

export const CLASSES = {
  LIQUIDITY_CONFIG: "com.arcesium.treasury.calm.model.LiquidityConfig",
  LIQUIDITY_CONFIG_DATA: "com.arcesium.treasury.calm.model.LiquidityConfigData",
  CONFIG_PAIR: "com.arcesium.treasury.calm.model.ConfigPair",
}

export const TICKER = "ticker";
export const BUSINESS_DAY_THRESHOLD_TIME = "businessDayThresholdTime";
export const HOLIDAY_THRESHOLD_TIME = "holidayThresholdTime";
export const MIN_THRESHOLD = "minThreshold";
export const MAX_THRESHOLD = "maxThreshold";
export const TRADE_BOOKING_APPROVERS = "tradeBookingApprovers";
export const TRADE_BOOKING_APPROVAL_REQUIRED = "tradeBookingApprovalRequired";
export const INTERNAL_TRADE_BOOKING_IN_SCOPE = "internalTradeBookingInScope";
export const WIRES_LOGGING_IN_SCOPE = "wireLoggingInScope";
export const CASH_THRESHOLDS = "Cash Thresholds";
export const MMF_CUTOFF_TIMELINES = "MMF Cutoff Timelines";
export const ENABLE_GOVT_BOND_BALANCES = "enableGovtBondBalances";
export const INCLUDED_REPORTING_AGREEMENT_TYPE_IDS ="includedReportingAgreementTypeIds";
export const REPORTING_CURRENCY = "reportingCurrency";
export const DATA_LEVEL_ID = "dataLevelId";
export const USE_CASH_OUT_FLOWS = "useCashOutflows";
export const UNENCUMBERED_CASH_ACCOUNTS = "unencumberedCashAccounts";
export const SUPPORTED_BOOKS_FOR_GOVT_BONDS = "supportedBooksForGovtBonds";
export const SUPPORTED_CURRENCIES_FOR_GOVT_BONDS = "supportedCurrenciesForGovtBonds";
export const SUPPORTED_ACCOUNTS_FOR_GOVT_BONDS = "supportedAccountsForGovtBonds";


export const CONFIG_OBJECTS = {
  "MMF_CUTOFF_TIME": 1,
  "CASH_THRESHOLDS": 2,
  "CLIENT_RAG_THRESHOLDS": 3,
  "FUND_RAG_THRESHOLDS": 4,
  "TRADE_INITIATION": 6,
  "WIRES_CONFIGURATION": 7,
  "DATA_CONFIGURATION": 8,
}

export const DEFAULT_COLUMNS_FOR_MMF_CUTOFF_TIME: string[] = [
  'ticker',
  'businessThreshold',
  'holidayThreshold',
]

export const DEFAULT_COLUMNS_FOR_CASH_THRESHOLDS: string[] = [
  'fund',
  'minThreshold',
  'maxThreshold',
]

export const DEFAULT_COLUMNS_FOR_EXCLUDED_AGREEMENTS: string[] = [
  'legalEntity',
  'counterPartyEntity',
  'agreementType',
]

export const DEFAULT_COLUMNS_FOR_AGREEMENT_CASH_THRESHOLDS: string[] = [
  'legalEntity',
  'counterParty',
  'agreementType',
  'minThreshold',
  'maxThreshold',
]

export const DEFAULT_SORT_BY = ["fund", "legalEntity", "ticker"];

