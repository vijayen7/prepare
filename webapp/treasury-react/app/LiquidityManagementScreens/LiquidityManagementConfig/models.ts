/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import {CLASSES} from "./constants";
import ReferenceData from "../../commons/models/ReferenceData";
/**
 * Contains all the models classes or interfaces
 * @author burri
 */


export interface SearchStatus {
  inProgress: boolean;
  error: boolean;
  message: string;
}

export interface MMFCutOffTime {
  ticker: string;
  businessThreshold: string | null;
  holidayThreshold: string | null;
}

export interface CashThreshold {
  fund: string | null;
  fundId: number;
  legalEntity: string | null;
  counterParty: string | null;
  agreementType: string | null;
  minThreshold: number;
  maxThreshold: number;
}

export interface RAGThreshold {
  clientAmberThreshold: number;
  clientRedThreshold: number;
  fundAmberThreshold: number;
  fundRedThreshold: number;
}

export interface ConfigPair {
  key: any;
  value : any;
  "@CLASS": typeof CLASSES.CONFIG_PAIR;
}

export interface LiquidityConfigData {
  objectValue: any;
  referenceDataMap: Map<string, string | null | undefined>;
  attrConfigPairs: ConfigPair[];
  "@CLASS": typeof CLASSES.LIQUIDITY_CONFIG_DATA;
}

export interface LiquidityConfig {
  objectKey: ReferenceData;
  config: LiquidityConfigData[];
  "@CLASS": typeof CLASSES.LIQUIDITY_CONFIG;
}

export interface ApiResponse {
  successStatus: boolean;
  message: string;
  response: any;
  "@CLASS"?: string;
}

export interface ExcludedAgreementData {
  legalEntity: string;
  counterPartyEntity: string;
  agreementType: string;
}

export interface UnencumberedCashAccount {
  legalEntity: string;
  counterPartyEntity: string;
  agreementType: string;
  account: string;
}

export interface GovtBondConfig {
  books: string[];
  currencies: string[];
  accounts: string[];
}

export interface GridProps {
  type: string;
  height: string;
  columns: any;
  displayColumns: any;
  gridData: any;
}
