/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import liquidityConfigStore from "../useStores";
import { EditableInput } from "arc-react-components";
import { Select } from "arc-react-components";
import { YES, NO } from "../../../commons/constants";
import Grid from "./Grid";
import { getTokenStringList } from "../utils";
import {
  getMMFCutOffTimeColumns,
  getCashThresholdsColumns,
  getAgreementLevelCashThresholdsColumns,
} from "./Grid/grid-columns";
import { ENTITY_LEVEL } from "../../commons/constants";

/**
 * Component for rendering the Trade Configuration
 * @author burri
 */

const TradeConfiguration: React.FC = () => {
  return (
    <div>
      <div className="container margin--top--double padding--double">
        <div className="form">
          <div className="row">
            <div className="form">
              <div>
                <strong>Internal Trade Booking in Scope:</strong>
                <EditableInput
                  value={
                    liquidityConfigStore.internalTradeBookingInScope ? YES : NO
                  }
                  onChange={(val) => {}}
                  editor={({ value, onChange }) => (
                    <Select
                      options={[YES, NO]}
                      value={value}
                      onChange={onChange}
                    />
                  )}
                  readOnly
                />
              </div>
            </div>
            <div className="form">
              <div>
                <strong>MMF Trade Approval Required:</strong>
                <EditableInput
                  value={
                    liquidityConfigStore.mmfTradeApprovalRequired ? YES : NO
                  }
                  onChange={(val) => {}}
                  editor={({ value, onChange }) => (
                    <Select
                      options={[YES, NO]}
                      value={value}
                      onChange={onChange}
                    />
                  )}
                  readOnly
                />
              </div>
            </div>
            <div className="form">
              <div>
                <strong>Level-1 MMF Trade Approvers:</strong>
                {getTokenStringList(liquidityConfigStore.levelOneApprovers)}
              </div>
            </div>
          </div>
        </div>
        <div className="form margin--top--double">
          <div className="row">
            <div className="form">
              <div>
                <strong>Level-2 MMF Trade Approvers:</strong>
                {getTokenStringList(liquidityConfigStore.levelTwoApprovers)}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="form margin--top--large">
        <div className="row">
          <strong>CASH BALANCE THRESHOLDS</strong>
        </div>
      </div>
      <div className="margin--top--small">
        <Grid
          columns={
            liquidityConfigStore.dataLevelId === ENTITY_LEVEL
              ? getCashThresholdsColumns()
              : getAgreementLevelCashThresholdsColumns()
          }
          displayColumns={liquidityConfigStore.displayColumnsforCashThresholds}
          gridData={liquidityConfigStore.cashThresholdsData}
          type="Cash Thresholds"
          height="400px"
        />
      </div>

      <div className="form margin--top--large">
        <div className="row">
          <div>
            <strong>CLIENT RAG THRESHOLDS</strong>
            <i
              className="icon-info"
              title="% of AUM held in Money Market Fund"
            ></i>
          </div>
        </div>
      </div>

      <div className="container margin--top--small padding--double">
        <div className="form">
          <div className="row">
            <div className="form">
              <div>
                <strong>AMBER (Soft threshold)</strong>
                <EditableInput
                  value={
                    liquidityConfigStore.ragThresholdData.clientAmberThreshold +
                    "%"
                  }
                  onChange={(val) => {}}
                  readOnly
                />
              </div>
            </div>
            <div className="form">
              <div>
                <strong>RED (Hard threshold)</strong>
                <EditableInput
                  value={
                    liquidityConfigStore.ragThresholdData.clientRedThreshold +
                    "%"
                  }
                  onChange={(val) => {}}
                  readOnly
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="form margin--top--large">
        <div className="row">
          <div>
            <strong>FUND RAG THRESHOLDS</strong>
            <i
              className="icon-info"
              title="% of AUM held in Money Market Fund"
            ></i>
          </div>
        </div>
      </div>

      <div className="container margin--top--small padding--double">
        <div className="form">
          <div className="row">
            <div className="form">
              <div>
                <strong>AMBER (Soft threshold)</strong>
                <EditableInput
                  value={
                    liquidityConfigStore.ragThresholdData.fundAmberThreshold +
                    "%"
                  }
                  onChange={(val) => {}}
                  readOnly
                />
              </div>
            </div>
            <div className="form">
              <div>
                <strong>RED (Hard threshold)</strong>
                <EditableInput
                  value={
                    liquidityConfigStore.ragThresholdData.fundRedThreshold + "%"
                  }
                  onChange={(val) => {}}
                  readOnly
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="form margin--top--large">
        <div className="row">
          <strong>MMF CUTOFF TIMELINES</strong>
        </div>
      </div>
      <div className="margin--top--small">
        <Grid
          columns={getMMFCutOffTimeColumns()}
          displayColumns={liquidityConfigStore.displayColumnsforMMFCutoffTime}
          gridData={liquidityConfigStore.mmfCutOffTimeData}
          type="MMF Cutoff Timelines"
          height="250px"
        />
      </div>
    </div>
  );
};

export default observer(TradeConfiguration);
