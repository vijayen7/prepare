/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import liquidityConfigStore from "../useStores";
import { Select, EditableInput } from "arc-react-components";
import { YES, NO } from "../../../commons/constants";

/**
 * Component for rendering the Wire Configuration
 * @author burri
 * @author venkatsu
 */

const WireConfiguration: React.FC = () => {
  return (
    <div>
      <div className="container margin--top--double padding--double">
        <div className="form">
          <div className="row">
            <div className="form">
              <div>
                <strong>Wire Inflow/Outflow Configuration:</strong>
                <EditableInput
                  value={
                    liquidityConfigStore.useCashOutflows
                      ? "Cash Out Flows"
                      : "Net Cash Flows"
                  }
                  onChange={(val) => {}}
                  editor={({ value, onChange }) => (
                    <Select
                      options={["Net Cash Flows", "Cash Out Flows"]}
                      value={value}
                      onChange={onChange}
                    />
                  )}
                  readOnly
                />
              </div>
            </div>
            <div className="form">
              <div>
                <strong>Wire Logging in Scope:</strong>
                <EditableInput
                  value={liquidityConfigStore.wireLoggingInScope ? YES : NO}
                  onChange={(val) => {}}
                  editor={({ value, onChange }) => (
                    <Select
                      options={[YES, NO]}
                      value={value}
                      onChange={onChange}
                    />
                  )}
                  readOnly
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default observer(WireConfiguration);
