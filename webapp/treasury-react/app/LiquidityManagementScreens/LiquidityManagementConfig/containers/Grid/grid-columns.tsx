/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { plainNumberFormatter } from "../../../../commons/grid/formatters";
import { NOT_AVAILABLE } from "../../../../commons/constants";
import { genericFormatter } from "../../../../commons/TreasuryUtils";

/**
 * Contains all the columns definition for the main grid
 * @author burri
 */

export function getMMFCutOffTimeColumns() {
  return [
    "ticker",
    {
      identity: "businessThreshold",
      header: "Business Day Threshold Time",
    },
    {
      identity: "holidayThreshold",
      header: "Holiday Threshold Time",
    },
  ];
}

export function getCashThresholdsColumns() {
  return [
    {
      identity: "fund",
      header: "Fund Name",
    },
    getNumericColumn("minThreshold", "Min Cash Threshold"),
    getNumericColumn("maxThreshold", "Max Cash Threshold"),
  ];
}

export function getExcludedAgreementsColumns() {
  return [
    genericFormatter("legalEntity"),
    genericFormatter("counterPartyEntity"),
    genericFormatter("agreementType"),
  ];
}

export function getAgreementLevelCashThresholdsColumns() {
  return [
    genericFormatter("legalEntity"),
    genericFormatter("counterParty", "Counter Party Entity"),
    genericFormatter("agreementType"),
    getNumericColumn("minThreshold", "Min Cash Threshold"),
    getNumericColumn("maxThreshold", "Max Cash Threshold"),
  ];
}

export function getUnencumberedCashAccountsColumns() {
  return [
    genericFormatter("legalEntity"),
    genericFormatter("counterPartyEntity"),
    genericFormatter("agreementType"),
    genericFormatter("account"),
  ];
}

function getNumericColumn(
  identity: string,
  header?: string,
  width?: number
): any {
  let formattedNumericColumn: any = {
    identity: identity,
    Cell: (value) => {
      let result = plainNumberFormatter(undefined, undefined, value);
      if (
        result === NOT_AVAILABLE ||
        result === null ||
        result === undefined ||
        parseInt(result) === 0
      )
        return (
          <>
            <div className="text-align--right" style={{ opacity: 0.6 }}>
              {0}
            </div>
          </>
        );
      else
        return (
          <>
            <div className="text-align--right">{result}</div>
          </>
        );
    },
  };
  if (header) {
    formattedNumericColumn.header = header;
  }
  if (width) {
    formattedNumericColumn.width = width;
  }
  return formattedNumericColumn;
}
