/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import ArcDataGrid from "arc-data-grid";
import { observer } from "mobx-react-lite";
import React from "react";
import {
  DEFAULT_SORT_BY
} from "../../constants";
import { GridProps } from "../../models";

/**
 * Renders the main grid based on the selected filters
 * @author burri
 */

const gridConfig = {
  clickableRows: true,
  getExportFileName: ({ fileType, all }) => {
    return "liquidity-config-report";
  },
};

const Grid: React.FC<any> = (props: GridProps) => {
  let columns = props.columns;
  let displayColumns = props.displayColumns;
  let gridData = props.gridData;

  function renderGridData() {
    let grid = (
      <>
        <div style={{ height: props.height }}>
          <ArcDataGrid
            rows={gridData}
            columns={columns}
            configurations={gridConfig}
            displayColumns={displayColumns}
            sortBy={DEFAULT_SORT_BY}
          />
        </div>
      </>
    );
    return grid;
  }

  return <>{renderGridData()}</>;
};

export default observer(Grid);
