/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { observer } from "mobx-react-lite";
import liquidityConfigStore from "../useStores";
import { EditableInput } from "arc-react-components";
import { Select } from "arc-react-components";
import { AGREEMENT_LEVEL, ENTITY_LEVEL } from "../../commons/constants";
import { YES, NO } from "../../../commons/constants";
import Grid from "./Grid";
import { getTokenStringList } from "../utils";
import {
  getExcludedAgreementsColumns,
  getUnencumberedCashAccountsColumns,
} from "./Grid/grid-columns";

/**
 * Component for rendering the Data Configuration
 * @author burri
 * @author venkatsu
 */

const DataConfiguration: React.FC = () => {
  return (
    <div>
      <div className="container margin--top--double padding--double">
        <div className="form">
          <div className="row">
            <div className="form">
              <div>
                <strong>Workflow Structure:</strong>
                <EditableInput
                  value={
                    liquidityConfigStore.dataLevelId === 0
                      ? "Entity Level"
                      : "Agreement Level"
                  }
                  onChange={(val) => {}}
                  editor={({ value, onChange }) => (
                    <Select
                      options={["Entity Level", "Agreement Level"]}
                      value={value}
                      onChange={onChange}
                    />
                  )}
                  readOnly
                />
              </div>
            </div>
            <div className="form">
              <div>
                <strong>Currency:</strong>
                <EditableInput
                  value={liquidityConfigStore.reportingCurrency}
                  onChange={(val) => {}}
                  editor={({ value, onChange }) => (
                    <Select
                      options={["USD", "EUR"]}
                      value={value}
                      onChange={onChange}
                    />
                  )}
                  readOnly
                />
              </div>
            </div>
            <div className="form">
              <div>
                <strong>Included Agreement Types:</strong>
                {liquidityConfigStore.dataLevelId === ENTITY_LEVEL ? (
                  <div style={{ opacity: "0.6" }}>N/A</div>
                ) : (
                  getTokenStringList(
                    liquidityConfigStore.includedReportingAgreementTypeIds
                  )
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="form margin--top--double">
          <div className="row">
            <div className="form">
              <div>
                <strong>Government Bond Balances in Scope:</strong>
                <EditableInput
                  value={liquidityConfigStore.enableGovtBondBalances ? YES : NO}
                  onChange={(val) => {}}
                  editor={({ value, onChange }) => (
                    <Select
                      options={[YES, NO]}
                      value={value}
                      onChange={onChange}
                    />
                  )}
                  readOnly
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {liquidityConfigStore.dataLevelId === AGREEMENT_LEVEL && (
        <div className="form margin--top--large">
          <div className="row">
            <strong>EXCLUDED AGREEMENTS</strong>
          </div>
        </div>
      )}
      {liquidityConfigStore.dataLevelId === AGREEMENT_LEVEL && (
        <div className="margin--top--small">
          <Grid
            columns={getExcludedAgreementsColumns()}
            displayColumns={
              liquidityConfigStore.displayColumnsforExcludedAgreements
            }
            gridData={liquidityConfigStore.excludedAgreementsData}
            type="Excluded Agreements"
            height="400px"
          />
        </div>
      )}

      {liquidityConfigStore.dataLevelId !== AGREEMENT_LEVEL && (
        <>
          <div className="form margin--top--large">
            <div className="row">
              <strong>UNENCUMBERED CASH ACCOUNTS</strong>
            </div>
          </div>
          <div className="margin--top--small">
            <Grid
              columns={getUnencumberedCashAccountsColumns()}
              gridData={liquidityConfigStore.unencumberedCashData}
              type="Unencumbered Cash Accounts"
              height="400px"
            />
          </div>
        </>
      )}

      {liquidityConfigStore.dataLevelId !== AGREEMENT_LEVEL && (
        <>
          <div className="form margin--top--large">
            <div className="row">
              <strong>GOVT BONDS CONFIGURATION</strong>
            </div>
          </div>

          <div className="container margin--top--small padding--double">
            <div className="form">
              <div className="row">
                <div className="form">
                  <div>
                    <strong>Supported Books:</strong>
                    {getTokenStringList(
                      liquidityConfigStore.govtBondConfigData.books
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <div>
                    <strong>Supported Currencies:</strong>
                    {getTokenStringList(
                      liquidityConfigStore.govtBondConfigData.currencies
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className="form margin--top--double">
              <div className="row">
                <div className="form">
                  <div>
                    <strong>Supported Accounts:</strong>
                    {getTokenStringList(
                      liquidityConfigStore.govtBondConfigData.accounts
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default observer(DataConfiguration);
