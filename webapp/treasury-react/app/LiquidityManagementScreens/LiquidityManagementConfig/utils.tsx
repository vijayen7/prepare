/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import React from "react";
import { TokenList } from "arc-react-components";

/**
 * Contains all the utility methods and classes.
 * @author burri
 */

/* gets the tokenized form of input string list */
export const getTokenStringList = (data: string[]) => {
  return (
    <>
      {data && data.length !== 0 && <TokenList values={data} dismissDisabled />}
      {(!data || data.length === 0) && (
        <div style={{ opacity: "0.6" }}>N/A</div>
      )}
    </>
  );
};
