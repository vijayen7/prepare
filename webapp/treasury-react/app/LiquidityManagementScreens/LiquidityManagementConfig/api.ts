/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { BASE_URL } from "../../commons/constants";
import { fetchPostURL } from "../../commons/util";
import SERVICE_URLS from "../../commons/UrlConfigs";

/**
 * Contains all the API calls definitions
 * @author burri
 */

export const fetchLiquidityConfigData = () => {
  let inputs: Array<String> = ["inputFormat=PROPERTIES", "format=json"];
  const url = `${BASE_URL}service/${SERVICE_URLS.liquidityConfigManager.getAllLiquidityConfigWithEnrichment}`;
  let encodedParam = inputs.join("&");

  return fetchPostURL(url, encodedParam);
};
