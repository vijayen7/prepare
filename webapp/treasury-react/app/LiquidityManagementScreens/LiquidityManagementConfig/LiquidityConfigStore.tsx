/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { action, observable } from "mobx";
import {
  SearchStatus,
  MMFCutOffTime,
  CashThreshold,
  RAGThreshold,
  LiquidityConfigData,
  ApiResponse,
  ExcludedAgreementData,
  UnencumberedCashAccount,
  GovtBondConfig,
} from "./models";
import { fetchLiquidityConfigData } from "./api";
import {
  INITIAL_SEARCH_STATUS,
  MESSAGES,
  DEFAULT_COLUMNS_FOR_MMF_CUTOFF_TIME,
  DEFAULT_COLUMNS_FOR_CASH_THRESHOLDS,
  DEFAULT_COLUMNS_FOR_AGREEMENT_CASH_THRESHOLDS,
  DEFAULT_COLUMNS_FOR_EXCLUDED_AGREEMENTS,
  INITIAL_RAG_THRESHOLDS,
  TICKER,
  BUSINESS_DAY_THRESHOLD_TIME,
  HOLIDAY_THRESHOLD_TIME,
  CONFIG_OBJECTS,
  MIN_THRESHOLD,
  MAX_THRESHOLD,
  TRADE_BOOKING_APPROVAL_REQUIRED,
  TRADE_BOOKING_APPROVERS,
  INTERNAL_TRADE_BOOKING_IN_SCOPE,
  WIRES_LOGGING_IN_SCOPE,
  ENABLE_GOVT_BOND_BALANCES,
  INCLUDED_REPORTING_AGREEMENT_TYPE_IDS,
  REPORTING_CURRENCY,
  DATA_LEVEL_ID,
  USE_CASH_OUT_FLOWS,
  INITIAL_GOVT_BOND_DATA_CONFIG,
  UNENCUMBERED_CASH_ACCOUNTS,
  SUPPORTED_BOOKS_FOR_GOVT_BONDS,
  SUPPORTED_CURRENCIES_FOR_GOVT_BONDS,
  SUPPORTED_ACCOUNTS_FOR_GOVT_BONDS,
} from "./constants";
import { getHHmmTime } from "../../commons/TreasuryUtils";
import { ENTITY_LEVEL } from "../commons/constants";

/**
 * Contains all the state variables and defines action or computation on them
 * @author burri
 */

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

export class LiquidityConfigStore {
  @observable searchStatus: SearchStatus = INITIAL_SEARCH_STATUS;

  //Trade Configuration
  @observable displayColumnsforMMFCutoffTime: string[] =
    DEFAULT_COLUMNS_FOR_MMF_CUTOFF_TIME;
  @observable displayColumnsforExcludedAgreements: string[] =
    DEFAULT_COLUMNS_FOR_EXCLUDED_AGREEMENTS;
  @observable mmfCutOffTimeData: Array<MMFCutOffTime> = [];
  @observable cashThresholdsData: Array<CashThreshold> = [];
  @observable ragThresholdData: RAGThreshold = INITIAL_RAG_THRESHOLDS;
  @observable levelOneApprovers: string[] = [];
  @observable levelTwoApprovers: string[] = [];
  @observable internalTradeBookingInScope: boolean = false;
  @observable mmfTradeApprovalRequired: boolean = false;
  @observable wireLoggingInScope: boolean = false;
  @observable useCashOutflows: boolean = false;
  @observable dataLevelId: Number = 0;
  @observable reportingCurrency: string = "USD";
  @observable includedReportingAgreementTypeIds: string[] = [];
  @observable enableGovtBondBalances: boolean = false;
  @observable excludedAgreementsData: Array<ExcludedAgreementData> = [];
  @observable unencumberedCashData: Array<UnencumberedCashAccount> = [];
  @observable govtBondConfigData: GovtBondConfig =
    INITIAL_GOVT_BOND_DATA_CONFIG;
  @observable displayColumnsforCashThresholds: string[] =
    this.dataLevelId === ENTITY_LEVEL
      ? DEFAULT_COLUMNS_FOR_CASH_THRESHOLDS
      : DEFAULT_COLUMNS_FOR_AGREEMENT_CASH_THRESHOLDS;

  @action
  getLiquidityConfigData = async () => {
    this.searchStatus = INITIAL_SEARCH_STATUS;
    this.searchStatus.inProgress = true;
    try {
      const liquidityConfig = await fetchLiquidityConfigData();
      this.mmfCutOffTimeData =
        this.getFlattenedMMFCutOffTimeData(liquidityConfig);
      this.populateTradeInitiationConfig(liquidityConfig);
      this.cashThresholdsData = this.getCashThresholdsList(liquidityConfig);
      this.ragThresholdData =
        this.getRagThresholds(liquidityConfig) || INITIAL_RAG_THRESHOLDS;
      this.dataLevelId = this.getDataLevel(liquidityConfig);
      this.reportingCurrency = this.getReportingCurrency(liquidityConfig);
      this.includedReportingAgreementTypeIds =
        this.getIncludedReportingAgreementTypes(liquidityConfig);
      this.enableGovtBondBalances =
        this.getGovtBondBalancesScope(liquidityConfig);
      this.populateWiresConfiguration(liquidityConfig);
      this.excludedAgreementsData =
        this.getExcludedAgreementData(liquidityConfig);
      this.unencumberedCashData = this.getUnencumberedCashData(liquidityConfig);
      this.govtBondConfigData = this.getGovtBondConfigData(liquidityConfig);
      this.populateWiresConfiguration(liquidityConfig);
      this.displayColumnsforCashThresholds =
        this.dataLevelId === ENTITY_LEVEL
          ? DEFAULT_COLUMNS_FOR_CASH_THRESHOLDS
          : DEFAULT_COLUMNS_FOR_AGREEMENT_CASH_THRESHOLDS;
      if (liquidityConfig?.response[1].length === 0) {
        this.searchStatus = {
          inProgress: false,
          error: false,
          message: MESSAGES.NO_DATA_FOUND_MESSAGE,
        };
      } else {
        this.searchStatus.inProgress = false;
      }
    } catch (e) {
      this.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  };

  @action
  getFlattenedMMFCutOffTimeData = (data: ApiResponse) => {
    let flattenedMMFCutOffTimeList: MMFCutOffTime[] = [];
    let mmfCutOffTimeList = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.MMF_CUTOFF_TIME
    )[0]?.config;
    if (mmfCutOffTimeList && mmfCutOffTimeList?.length !== 0) {
      mmfCutOffTimeList?.forEach((mmfCutOffTime: LiquidityConfigData) => {
        let flattenedMMFCutOffTime: MMFCutOffTime = {
          ticker: mmfCutOffTime?.attrConfigPairs?.filter(
            (x) => x.key === TICKER
          )[0]?.value,
          businessThreshold: getHHmmTime(
            parseInt(
              mmfCutOffTime?.attrConfigPairs?.filter(
                (x) => x.key === BUSINESS_DAY_THRESHOLD_TIME
              )[0]?.value
            )
          ),
          holidayThreshold: getHHmmTime(
            parseInt(
              mmfCutOffTime?.attrConfigPairs?.filter(
                (x) => x.key == HOLIDAY_THRESHOLD_TIME
              )[0]?.value
            )
          ),
        };
        flattenedMMFCutOffTimeList.push(flattenedMMFCutOffTime);
      });
    }
    return flattenedMMFCutOffTimeList;
  };

  @action
  getExcludedAgreementData = (data: ApiResponse) => {
    let excludedAgreementList: ExcludedAgreementData[] = [];
    let dataConfigList = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.DATA_CONFIGURATION
    )[0]?.config;
    let excludedReportingAgreementsMap =
      dataConfigList[0]?.referenceDataMap?.excludedReportingAgreementIds;
    for (let agreementId of Object.keys(excludedReportingAgreementsMap)) {
      let excludedAgreementData: ExcludedAgreementData = {
        legalEntity: excludedReportingAgreementsMap[agreementId].LEGAL_ENTITY,
        counterPartyEntity:
          excludedReportingAgreementsMap[agreementId].COUNTER_PARTY_ENTITY,
        agreementType:
          excludedReportingAgreementsMap[agreementId].AGREEMENT_TYPE,
      };
      excludedAgreementList.push(excludedAgreementData);
    }
    return excludedAgreementList;
  };

  @action
  getUnencumberedCashData = (data: ApiResponse) => {
    let unencumberedCashDataList: UnencumberedCashAccount[] = [];
    if (data?.response && data?.response?.length !== 0) {
      let dataConfigList = data?.response[1]?.filter(
        (x) => x?.objectKey?.id === CONFIG_OBJECTS.DATA_CONFIGURATION
      )[0]?.config;
      let encumberedCashDataMap =
        dataConfigList[0]?.referenceDataMap[UNENCUMBERED_CASH_ACCOUNTS];
      if (!encumberedCashDataMap) return unencumberedCashDataList;
      for (let account of Object.keys(encumberedCashDataMap)) {
        let unencumberedCashAccountData: UnencumberedCashAccount = {
          legalEntity: encumberedCashDataMap[account].LEGAL_ENTITY,
          counterPartyEntity:
            encumberedCashDataMap[account].COUNTER_PARTY_ENTITY,
          agreementType: encumberedCashDataMap[account].AGREEMENT_TYPE,
          account: account,
        };
        unencumberedCashDataList.push(unencumberedCashAccountData);
      }
    }
    return unencumberedCashDataList;
  };

  @action
  getGovtBondConfigData = (data: ApiResponse) => {
    if (data?.response && data?.response?.length !== 0) {
      let dataConfigList = data?.response[1]?.filter(
        (x) => x?.objectKey?.id === CONFIG_OBJECTS.DATA_CONFIGURATION
      )[0]?.config;
      let books =
        dataConfigList[0]?.referenceDataMap[SUPPORTED_BOOKS_FOR_GOVT_BONDS];
      let currencies =
        dataConfigList[0]?.referenceDataMap[
          SUPPORTED_CURRENCIES_FOR_GOVT_BONDS
        ];
      let accounts =
        dataConfigList[0]?.referenceDataMap[SUPPORTED_ACCOUNTS_FOR_GOVT_BONDS];

      let govtBondConfig: GovtBondConfig = {
        books: books,
        currencies: currencies,
        accounts: accounts,
      };
      return govtBondConfig;
    } else return INITIAL_GOVT_BOND_DATA_CONFIG;
  };

  @action
  populateTradeInitiationConfig = (data: ApiResponse) => {
    let tradeInitiationConfig: LiquidityConfigData[] =
      data?.response[1]?.filter(
        (x) => x?.objectKey?.id === CONFIG_OBJECTS.TRADE_INITIATION
      )[0]?.config;
    let tradeApprovers = tradeInitiationConfig[0]?.attrConfigPairs
      ?.filter((x) => x.key === TRADE_BOOKING_APPROVERS)[0]
      ?.value.split(",");
    this.mmfTradeApprovalRequired =
      tradeInitiationConfig[0]?.attrConfigPairs?.filter(
        (x) => x.key === TRADE_BOOKING_APPROVAL_REQUIRED
      )[0]?.value;
    this.internalTradeBookingInScope =
      tradeInitiationConfig[0]?.attrConfigPairs?.filter(
        (x) => x.key === INTERNAL_TRADE_BOOKING_IN_SCOPE
      )[0]?.value;
    let approvers: string[] = [];
    if (tradeApprovers[0] !== null && tradeApprovers[0] !== undefined) {
      for (let approver of tradeApprovers[0].split("|")) {
        approvers.push(approver.trim());
      }
      this.levelOneApprovers = approvers;
    }
    approvers = [];
    if (tradeApprovers[1] !== null && tradeApprovers[1] !== undefined) {
      for (let approver of tradeApprovers[1].split("|")) {
        approvers.push(approver.trim());
      }
      this.levelTwoApprovers = approvers;
    }
  };

  @action
  getCashThresholdsList = (data: ApiResponse) => {
    let flattenedCashThresholdList: CashThreshold[] = [];
    let cashThresholds = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.CASH_THRESHOLDS
    )[0]?.config;
    if (cashThresholds && cashThresholds?.length !== 0) {
      cashThresholds?.forEach((cashThreshold) => {
        let flattenedCashThreshold: CashThreshold = {
          minThreshold: cashThreshold?.attrConfigPairs?.filter(
            (x) => x.key === MIN_THRESHOLD
          )[0]?.value,
          maxThreshold: cashThreshold?.attrConfigPairs?.filter(
            (x) => x.key === MAX_THRESHOLD
          )[0]?.value,
          fundId: cashThreshold?.referenceDataMap?.FUND_ID,
          fund: cashThreshold?.referenceDataMap?.FUND_NAME,
          legalEntity: cashThreshold?.referenceDataMap?.LEGAL_ENTITY,
          counterParty: cashThreshold?.referenceDataMap?.COUNTER_PARTY_ENTITY,
          agreementType: cashThreshold?.referenceDataMap?.AGREEMENT_TYPE,
        };
        flattenedCashThresholdList.push(flattenedCashThreshold);
      });
    }
    return flattenedCashThresholdList;
  };

  @action
  getRagThresholds = (data: ApiResponse) => {
    let clientRagThreshold: LiquidityConfigData[] = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.CLIENT_RAG_THRESHOLDS
    )[0]?.config;
    let fundRagThreshold: LiquidityConfigData[] = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.FUND_RAG_THRESHOLDS
    )[0]?.config;

    if (
      clientRagThreshold !== null &&
      clientRagThreshold !== undefined &&
      fundRagThreshold !== null &&
      fundRagThreshold !== undefined
    ) {
      let ragThresholdData: RAGThreshold = {
        clientRedThreshold: clientRagThreshold[0]?.attrConfigPairs?.filter(
          (x) => x.key === MIN_THRESHOLD
        )[0]?.value,
        clientAmberThreshold: clientRagThreshold[1]?.attrConfigPairs?.filter(
          (x) => x.key === MIN_THRESHOLD
        )[0]?.value,
        fundRedThreshold: fundRagThreshold[0]?.attrConfigPairs?.filter(
          (x) => x.key === MIN_THRESHOLD
        )[0]?.value,
        fundAmberThreshold: fundRagThreshold[1]?.attrConfigPairs?.filter(
          (x) => x.key === MIN_THRESHOLD
        )[0]?.value,
      };
      return ragThresholdData;
    }
    return;
  };

  @action
  populateWiresConfiguration = (data: ApiResponse) => {
    let wireConfigList: LiquidityConfigData[] = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.WIRES_CONFIGURATION
    )[0]?.config;
    let wireLoggingScope = wireConfigList[0]?.attrConfigPairs?.filter(
      (x) => x.key === WIRES_LOGGING_IN_SCOPE
    )[0]?.value;
    this.wireLoggingInScope = wireLoggingScope;
    let cashOutFlowStatus = wireConfigList[0]?.attrConfigPairs?.filter(
      (x) => x.key === USE_CASH_OUT_FLOWS
    )[0]?.value;
    this.useCashOutflows = cashOutFlowStatus;
  };

  @action
  getDataLevel = (data) => {
    let dataConfigurationList = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.DATA_CONFIGURATION
    )[0]?.config;
    let dataLevelId = dataConfigurationList[0]?.attrConfigPairs?.filter(
      (x) => x.key === DATA_LEVEL_ID
    )[0]?.value;
    return dataLevelId;
  };

  @action
  getReportingCurrency = (data) => {
    let dataConfigurationList = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.DATA_CONFIGURATION
    )[0]?.config;
    let reportingCurrency =
      dataConfigurationList[0]?.referenceDataMap?.[REPORTING_CURRENCY];
    return reportingCurrency;
  };

  @action
  getIncludedReportingAgreementTypes = (data) => {
    let dataConfigurationList = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.DATA_CONFIGURATION
    )[0]?.config;
    let includedAgreementTypes =
      dataConfigurationList[0]?.referenceDataMap?.[
        INCLUDED_REPORTING_AGREEMENT_TYPE_IDS
      ]?.split(",");
    let agreementTypes: string[] = [];
    for (let agreementType of includedAgreementTypes) {
      agreementTypes.push(agreementType.trim());
    }
    return agreementTypes;
  };

  @action
  getGovtBondBalancesScope = (data) => {
    let dataConfigurationList = data?.response[1]?.filter(
      (x) => x?.objectKey?.id === CONFIG_OBJECTS.DATA_CONFIGURATION
    )[0]?.config;
    let govtBalancesStatus = dataConfigurationList[0]?.attrConfigPairs?.filter(
      (x) => x.key === ENABLE_GOVT_BOND_BALANCES
    )[0]?.value;

    return govtBalancesStatus;
  };
}
