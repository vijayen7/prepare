import { put, takeEvery, call, all } from "redux-saga/effects";
import treasuryRootSaga from "Treasury/saga";
import decisionDrivingDashboardRootSaga from "DecisionDrivingDashboard/saga";
import marketDataSaga from "MarketData/saga";
import availabilityDataSaga from "AvailabilityData/saga";
import onboardingUISaga from "OnboardingUI/saga";
import rateNegotiationSaga from "seclend/RateNegotiationDialog/saga";
import rateNegotiationVerificationSaga from "RateNegotiationVerification/saga";
import financingOpsDataSaga from "Financing/OpsDashboard/saga";
import returnOnAssetsSaga from "ReturnOnAssets/saga";
import commonRootSaga from "commons/sagas";
import detailReportRootSaga from "DetailReport/saga";
import cashManagementReportRootSaga from "CashManagementReport/saga";
import cashManagementWorkflowRootSaga from "CashManagementWorkflow/saga";
import outperformanceSaga from "Outperformance/saga";
import rateNegotiationSavingsSaga from "RateNegotiationSavings/saga";
import aanaReportSaga from "Aana/AanaReport/saga";
import lendReportDataSaga from "LendReports/saga";
import aanaRuleSystemSaga from "Aana/AanaRuleSystem/saga";
import cofiRepoFinancingSaga from "CofiRepoFinancing/saga";
import financingReportingSaga from "FinancingReporting/saga";
import outperformanceFileConfigSaga from "OutperformanceFileConfig/saga";
import conterPartyGroupSaga from "CounterPartyGrouping/saga";
import dividendEnhancementDetailDataSaga from "DividendEnhancement/saga";
import workflowStatusSaga from "WorkflowStatus/saga";
import lcmRootSaga from "Lcm/saga";

function* rootSaga() {
  yield all([
    commonRootSaga(),
    treasuryRootSaga(),
    marketDataSaga(),
    availabilityDataSaga(),
    onboardingUISaga(),
    rateNegotiationSaga(),
    rateNegotiationVerificationSaga(),
    financingOpsDataSaga(),
    decisionDrivingDashboardRootSaga(),
    returnOnAssetsSaga(),
    detailReportRootSaga(),
    cashManagementReportRootSaga(),
    cashManagementWorkflowRootSaga(),
    outperformanceSaga(),
    rateNegotiationSavingsSaga(),
    aanaReportSaga(),
    lendReportDataSaga(),
    aanaRuleSystemSaga(),
    cofiRepoFinancingSaga(),
    financingReportingSaga(),
    outperformanceFileConfigSaga(),
    conterPartyGroupSaga(),
    dividendEnhancementDetailDataSaga(),
    workflowStatusSaga(),
    lcmRootSaga()
  ]);
}
export default rootSaga;
