import { BASE_URL } from "./../../commons/constants";
import { getCommaSeparatedValues, fetchPostURL } from "./../../commons/util";
import { SearchFilter } from "./../models/SearchFilter";
import {
  CASH_PROJECTION_INPUTS,
  KEY_INPUTS,
  CASH_PROJECTION_KEY_TYPE,
  API_BOOK_CA_CCY,
  KEY_BOOK_CA_CCY,
} from "./../constants";
// @ts-ignore
import SERVICE_URLS from "./../../commons/UrlConfigs";

export const getCashProjectionDataList = (filters: SearchFilter) => {
  let url = getUrl(
    SERVICE_URLS.cashProjectionService.getCashProjectionReportData
  );
  let encodedParam = getEncodedParam(filters);
  return fetchPostURL(url, encodedParam);
};

export const getCashProjectionUrl = (filters: SearchFilter) => {
  let url = getUrl(
    SERVICE_URLS.cashProjectionService.getCashProjectionReportFlattenedData
  );
  let encodedParam = getEncodedParam(filters);
  return url + "?" + encodedParam;
};

function getUrl(url: string) {
  return `${BASE_URL}service/${url}`;
}

function getEncodedParam(filters: SearchFilter) {
  let date = `${CASH_PROJECTION_INPUTS}date=${filters.selectedDate}`;
  let keyType = `${CASH_PROJECTION_KEY_TYPE}keyType=${filters.keyType}`;
  let balanceTypeKeyList = getBalanceTypeIds(filters);
  let balanceType = `${CASH_PROJECTION_INPUTS}balanceTypes=${balanceTypeKeyList}`;
  let keyInputs = getKeyInputs(filters);
  let inputs: Array<String> = [
    date,
    keyType,
    keyInputs,
    balanceType,
    "inputFormat=PROPERTIES",
    "format=json",
  ];

  return inputs.join("&");
}

function getBalanceTypeIds(filters: SearchFilter) {
  if (filters.selectedBalanceTypes.length < 1) {
    return null;
  }
  let keyList: Array<number> = [];
  filters.selectedBalanceTypes.forEach((balanceType) => {
    keyList.push(balanceType.key);
  });
  return keyList;
}

function getKeyInputs(filters: SearchFilter) {
  let keyInputs: String = "";

  if (filters.keyType === KEY_BOOK_CA_CCY) {
    keyInputs = getBookCaCcyInputs(filters);
  }

  return keyInputs;
}

function getBookCaCcyInputs(filters: SearchFilter) {
  let bookIds = getCommaSeparatedValues(filters.selectedBooks);
  let books = `${KEY_INPUTS}${API_BOOK_CA_CCY}books=${bookIds}`;

  let caIds = getCommaSeparatedValues(filters.selectedCustodianAccounts);
  let custodianAccounts = `${KEY_INPUTS}${API_BOOK_CA_CCY}custodianAccounts=${caIds}`;

  let ccyIds = getCommaSeparatedValues(filters.selectedCurrencies);
  let currency = `${KEY_INPUTS}${API_BOOK_CA_CCY}currencies=${ccyIds}`;

  let bookCaCcyKeyInputs = [books, custodianAccounts, currency].join("&");

  return bookCaCcyKeyInputs;
}
