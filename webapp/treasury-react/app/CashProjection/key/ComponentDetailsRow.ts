export interface ComponentDetailsRow {
  dataId?: number,
  date?: Date;
  knowledgeDate?: Date,
  versionId?: number,
  component?: string;
  subComponent?: string;
  isInformational?: boolean;
  valueLocal?: number;
  valueUsd?: number;
};
