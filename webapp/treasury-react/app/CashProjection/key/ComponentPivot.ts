export interface ComponentPivot {
  dataId?: number;
  subComponentList?: Array<ComponentPivot>;
  componentName?: string;
  date?: string;
  value?: number;
};
