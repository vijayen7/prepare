import { BookCaCcyKey } from "./BookCaCcyKey";
import { COL_BOOK, COL_CUSTODIAN_ACCOUNT, COL_CURRENCY, COL_KNOWLEDGE_DATE, COL_VERSION_ID } from "./../../constants";

export function getBookCaCcyKeyData(cashProjectionKey: BookCaCcyKey) {
  let book = cashProjectionKey.book!!.abbrev;
  let custodianAccount = cashProjectionKey.custodianAccount!!.name;
  let currency = cashProjectionKey.currency!!.abbrev;

  return ({
    book: book,
    custodianAccount: custodianAccount,
    currency: currency
  });
}

export function getBookCaCcyColumns() {
  return ([
    COL_KNOWLEDGE_DATE,
    COL_VERSION_ID,
    COL_BOOK,
    COL_CUSTODIAN_ACCOUNT,
    COL_CURRENCY
  ]);
}
