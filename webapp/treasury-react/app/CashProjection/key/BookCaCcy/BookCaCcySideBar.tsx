import React from "react";
import { observer } from "mobx-react";
import { CashProjectionProps } from "./../../models/CashProjectionProps";
import CustodianAccountFilter from "./../../../commons/container/CustodianAccountFilter";
import BookFilter from "./../../../commons/container/BookFilter";
import CurrencyFilter from "./../../../commons/container/CurrencyFilter";

const BookCaCcySideBar: React.FC<CashProjectionProps> = (props: CashProjectionProps) => {
  let keyFilters = (
    <div>
      <BookFilter
        onSelect={props.store.onSelect}
        selectedData={props.store.sideBarStore.filters.selectedBooks}
        multiSelect={true}
      />
      <CustodianAccountFilter
        onSelect={props.store.onSelect}
        selectedData={props.store.sideBarStore.filters.selectedCustodianAccounts}
        multiSelect={true}
      />
      <CurrencyFilter
        onSelect={props.store.onSelect}
        selectedData={props.store.sideBarStore.filters.selectedCurrencies}
        multiSelect={true}
      />
    </div>
    );

    return keyFilters;
};

export default observer(BookCaCcySideBar);
