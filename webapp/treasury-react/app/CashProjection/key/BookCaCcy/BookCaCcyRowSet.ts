/*
 *  Copyright (c) 2021. Arcesium LLC. All rights reserved.
 *
 *  This software is the confidential and proprietary information of Arcesium LLC. ("Confidential Information"). You
 *  shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *   agreement you entered into with Arcesium LLC.
 *
 */

import { BookCaCcyRow } from "./BookCaCcyRow";


/**
 * BookCaCcyRowSet is the row st for cash projection row
 * @author bhatti
 */
export class BookCaCcyRowSet {
  dataId?: number;
  component?: string;
  book?: string;
  custodianAccount?: string;
  currency?: string;
  date?: string;
  knowledgeDate?: string;
  versionId?: number;
  valueLocal?: number;
  valueUsd?: number;
  subComponentList: Array<BookCaCcyRow>;
}
