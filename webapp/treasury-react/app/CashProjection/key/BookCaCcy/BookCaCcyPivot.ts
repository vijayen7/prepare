import {ComponentPivot} from "./../ComponentPivot";

export class BookCaCcyPivot implements ComponentPivot {
  dataId?: number;
  subComponentList?: Array<ComponentPivot>;
  componentName?: string;
  date?: string;
  value?: number;
  book?: string;
  custodianAccount?: string;
  currency?: string;
};
