import ReferenceData from "./../../../commons/models/ReferenceData";
import { CashProjectionKey } from "./../CashProjectionKey";

export class BookCaCcyKey implements CashProjectionKey {
  book?: ReferenceData;
  custodianAccount?: ReferenceData;
  currency?: ReferenceData;
}
