export class BookCaCcyRow {
  dataId?: number;
  book?: string;
  custodianAccount?: string;
  currency?: string;
  date?: Date;
  knowledgeDate?: Date,
  versionId?: number,
  component?: string;
  subComponent?: string;
  isInformational?: boolean;
  valueLocal?: string;
  valueUsd?: string;
};
