import ArcDataGrid from "arc-data-grid";

export const CashProjectionGridConfig: ArcDataGrid.GridConfiguration = {
  clickableRows: true,
  getExportFileName: ({ fileType, all }) => {
    return `${all ? "Cash-Projecton" : "Cash-Projection-Current-View"}`;
  },
  timezone: "Asia/Calcutta",
};
