import React from "react";
import { Sidebar, DatePicker } from "arc-react-components";
import { observer } from "mobx-react";
import ColumnLayout from "../../commons/components/ColumnLayout";
import FilterButton from "../../commons/components/FilterButton";
import BookCaCcySideBar from "./../key/BookCaCcy/BookCaCcySideBar";
import { CashProjectionProps } from "../models/CashProjectionProps";
import SaveSettingsManager from "./../../commons/components/SaveSettingsManager";
import { KEY_BOOK_CA_CCY } from "./../constants";
import { Card } from "arc-react-components";
import MultiSelectFilter from "./../../commons/filters/components/MultiSelectFilter";

const CashProjectionSideBar: React.FC<CashProjectionProps> = (
  props: CashProjectionProps
) => {
  let keyFilters = {};
  if (props.store.getCashProjectionKey() == KEY_BOOK_CA_CCY) {
    keyFilters = <BookCaCcySideBar store={props.store} />;
  }

  let balancesType = (
    <div>
      <MultiSelectFilter
        horizontalLayout={true}
        label="Balance Type"
        data={props.store.cashProjectionBalanceTypeList}
        onSelect={props.store.onSelect}
        stateKey="selectedBalanceTypes"
        selectedData={props.store.sideBarStore.filters.selectedBalanceTypes}
      />
    </div>
  );

  let datePicker = (
    <div className="layout--flex gutter" style={{ height: "25px" }}>
      <div>Cash Projection Date</div>
      <DatePicker
        placeholder="Select Date"
        value={props.store.sideBarStore.filters.selectedDate}
        onChange={(date) => {
          props.store.sideBarStore.filters.selectedDate = date;
        }}
        renderInBody
      />
    </div>
  );

  let searchButtons = (
    <ColumnLayout>
      <FilterButton onClick={props.store.handleSearch} label="Search" />
      <FilterButton
        onClick={props.store.sideBarStore.handleReset}
        reset
        label="Reset"
      />
    </ColumnLayout>
  );

  let saveSettings = (
    <SaveSettingsManager
      selectedFilters={props.store.sideBarStore.filters}
      applySavedFilters={props.store.sideBarStore.applySavedFilters}
      applicationName="CashProjection"
    />
  );

  return (
    <Sidebar header={false} footer={searchButtons}>
      <Card>{saveSettings}</Card>
      <Card>
        {datePicker}
        {keyFilters}
      </Card>
      <Card>{balancesType}</Card>
    </Sidebar>
  );
};

export default observer(CashProjectionSideBar);
