import {
  KEY_BOOK_CA_CCY,
  COL_COMPONENT,
  COL_SUB_COMPONENT,
  COL_DATE,
  COL_IS_INFORMATIONAL,
  COL_VALUE_LOCAL,
  COL_VALUE_USD
} from "./../constants";
import {
  getFormattedNumericColumn,
} from "./../../commons/TreasuryUtils";
import { getBookCaCcyColumns } from "./../key/BookCaCcy";

export function getDateColumn() {
  return ([
    COL_DATE
  ]);
}

export function getCommonColumns() {
  return ([
    COL_COMPONENT,
    COL_SUB_COMPONENT,
    COL_IS_INFORMATIONAL,
    getFormattedNumericColumn(COL_VALUE_LOCAL, undefined, undefined, undefined, 2),
    getFormattedNumericColumn(COL_VALUE_USD, undefined, undefined, undefined, 2),
  ]);
}

export function getKeyColumns(keyType: string) {
  let keyColumns: Array<string> = [];
  if (keyType === KEY_BOOK_CA_CCY) {
    keyColumns = getBookCaCcyColumns();
  }

  return keyColumns;
}
