import React from "react";
import { observer } from "mobx-react";
import Message from "../../commons/components/Message";
import ArcDataPivot from 'arc-data-pivot';
import ArcDataGrid from 'arc-data-grid';
import _ from 'lodash';
import { CashProjectionProps } from "../models/CashProjectionProps";
import { Layout } from 'arc-react-components';
import { formatFinancial } from 'arc-commons/utils/number';
import { css, cx } from 'emotion';
import { CashProjectionGridConfig } from "./CashProjectionGridConfig"
import { Panel } from 'arc-react-components'

const CashProjectionGrid: React.FC<CashProjectionProps> = (props: CashProjectionProps) => {

  const renderGridData = () => {
    let data = props.store.cashProjectionReportDataList;
    let flattenedCashProjectionData = props.store.dataStore.pivotDataList;

    if (props.store.sideBarStore.searchStatus.error) {
      return <Message messageData="Error occurred while loading data." />;
    }

    if (!flattenedCashProjectionData.length) {
      let noDataMessage = "Please search Cash Projection Data";
      if (props.store.sideBarStore.searchStatus.firstSearch == false) {
        const date = props.store.sideBarStore.getCurrentDate();
        noDataMessage = "No Cash Projection Data available for Date " + date;
      }
      return <Message messageData={noDataMessage} />;
    }

    const cellStyle = css`text-align: right;`;

    let cashProjectionPivotGrid = (
      <Layout isColumnType={false} className="border" style={{ height: '100%' }}>
        <Layout.Child size={1} title="Cash Projection" childId="gridChild1" collapsible>
          <Panel title="Cash Projection">
            <ArcDataPivot
              ref={props.store.gridStore.setGridRef}
              memoizedRows={flattenedCashProjectionData}
              memoizedContext={null}
              settings={{
                minColumnWidth: 120,
                maxColumnWidth: 170,
                disableAllColumn: true,
                allRowColumnLabel: 'Grand Total'
              }}
              initialState={{
                rowDimensionIdentities: props.store.gridStore.pivotRowIdentities,
                columnDimensionIdentities: props.store.gridStore.pivotColumnIdentities,
                selectedCellPath: [], // This enables cell selection
              }}
              metrics={[
                {
                  metricIdentity: 'Cash Projection Components',
                  getAggregate: (cell, context) => props.store.gridStore.getCumulativeValue(cell.rowSet),
                  getCellSortValue: (aggregate) => aggregate,
                  getCellCsvValue: (aggregate) => aggregate,
                  renderCell: (elementProps, aggregate) => (
                    <div {...elementProps} className={cx(elementProps.className, cellStyle)}>
                      {formatFinancial(aggregate)}
                    </div>
                  )
                }
              ]}
            />
          </Panel>
        </Layout.Child>
        <Layout.Divider isResizable childId="gridChild2" />
        <Layout.Child size={1} title="Cash Projection Component Details" childId="gridChild3" collapsible>
          <Panel title="Cash Projection Component Details">
            <ArcDataGrid
              rows={props.store.gridStore.selectedSubRows}
              columns={props.store.gridStore.getGridColumns()}
              configurations={CashProjectionGridConfig}
            />
          </Panel>
        </Layout.Child>
      </Layout >

    );

    return cashProjectionPivotGrid;
  };

  return <>{renderGridData()}</>;
}

export default observer(CashProjectionGrid);
