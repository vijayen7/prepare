import React from "react";
import { observer } from "mobx-react-lite";
import { CashProjectionProps } from "../models/CashProjectionProps";

const CashProjectionButtons: React.FC<CashProjectionProps> = (
  props: CashProjectionProps
) => {
  const openReportManager = () => {
    let url = props.store.generateReportFromAPI();
    window.open(url);
  };

  return (
    <div
      style={{
        textAlign: "right",
      }}
    >
      <button onClick={openReportManager}>Create Report</button>
    </div>
  );
};

export default observer(CashProjectionButtons);
