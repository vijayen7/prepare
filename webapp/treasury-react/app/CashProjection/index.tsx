import CashProjectionStore from "./CashProjectionStore";
import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../commons/components/ReactLoader";
import Loader from "../commons/container/Loader";
import AppHeader, { whenHeaderRef } from "arc-header";
import CashProjectionGrid from "./components/CashProjectionGrid";
import CashProjectionSideBar from "./components/CashProjectionSideBar";
import { Layout, Breadcrumbs } from "arc-react-components";
import { extendObservable } from "mobx";
import { useLocalStore } from "mobx-react-lite";
import CashProjectionButtons from "./components/CashProjectionButtons";
import { URL_CONFIG_UI } from "./constants";

const dummyObservable = Symbol("dummy-observable");

const applicationMenu = (
  <a id="configManager" href={URL_CONFIG_UI} target="_blank">
    Config Manager
  </a>
);

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item
        link="/treasury/cashProjection.html"
        key="cashProjection"
      >
        Cash Projection
      </Breadcrumbs.Item>
    </>
  );
};

const CashProjection: React.FC = () => {
  const cashProjectionStore = useLocalStore(() =>
    extendObservable(new CashProjectionStore(), {
      [dummyObservable]: true,
    })
  );
  useEffect(() => {
    cashProjectionStore.fetchCashProjectionBalanceTypeList();
    updateBreadCrumbs();
  }, []);

  return (
    <>
      <Loader />
      <ReactLoader
        inProgress={cashProjectionStore.sideBarStore.searchStatus.inProgress}
      />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} applicationMenu={applicationMenu}/>
        <Layout
          isColumnType={true}
          style={{ height: "100%" }}
        >
          <Layout.Child
            size={2}
            childId="child1"
            title="Search Criteria"
            collapsible
            showHeader
          >
            <CashProjectionSideBar store={cashProjectionStore} />
          </Layout.Child>
          <Layout.Divider isResizable childId="child2" />
          <Layout.Child size={8} childId="child3">
            <Layout>
              <Layout.Child size="fit" childId="child4">
                <CashProjectionButtons store={cashProjectionStore} />
              </Layout.Child>
              <Layout.Child childId="child5">
                <CashProjectionGrid store={cashProjectionStore} />
              </Layout.Child>
            </Layout>
          </Layout.Child>
        </Layout>
      </div>
    </>
  );
};

export default observer(CashProjection);
