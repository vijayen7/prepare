import { observable, action } from "mobx";
import { SearchFilter, SearchStatus } from "../models/SearchFilter";
import { getCurrentDate } from "../../commons/util";
import { KEY_BOOK_CA_CCY } from "../constants";
import MapData from "./../../commons/models/MapData";

export const INITIAL_SEARCH_STATUS = {
  firstSearch: true,
  inProgress: false,
  error: false,
};

export const INITIAL_FILTERS = {
  selectedDate: getCurrentDate(),
  selectedBooks: [],
  selectedCustodianAccounts: [],
  selectedCurrencies: [],
  keyType: KEY_BOOK_CA_CCY,
  selectedBalanceTypes: [],
};

export class SideBarStore {
  // Observable Variables
  @observable toggleSidebar: boolean = false;
  @observable searchStatus: SearchStatus = INITIAL_SEARCH_STATUS;
  @observable filters: SearchFilter = INITIAL_FILTERS;

  // Action Setters

  @action setSelectedBalanceTypes(balanceTypeList: MapData[]) {
    this.filters.selectedBalanceTypes = balanceTypeList;
  }

  @action setFilters(newSearchFilters: SearchFilter) {
    this.filters = newSearchFilters;
  }

  @action setSearchStatus(searchStatus: SearchStatus) {
    this.searchStatus = searchStatus;
  }

  @action setToggleSidebar() {
    this.toggleSidebar = true;
  }

  // Action Methods

  @action.bound handleReset() {
    this.searchStatus.firstSearch = true;
    this.resetFilters();
    this.resetSearchStatus();
  }

  @action resetFilters() {
    this.filters = INITIAL_FILTERS;
  }

  @action resetSearchStatus() {
    this.searchStatus = INITIAL_SEARCH_STATUS;
  }

  @action.bound applySavedFilters(savedFilters: SearchFilter) {
    this.filters = savedFilters;
  }

  @action getCurrentDate() {
    return this.filters.selectedDate;
  }
}

export default SideBarStore;
