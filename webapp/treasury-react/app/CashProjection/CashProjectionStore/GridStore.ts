import { observable, action, computed } from "mobx";
import { ComponentDetailsRow } from "../key/ComponentDetailsRow";
import { BookCaCcyRowSet } from "./../key/BookCaCcy/BookCaCcyRowSet";
import { BookCaCcyRow } from "./../key/BookCaCcy/BookCaCcyRow";

import ArcDataGrid from "arc-data-grid";
import ArcDataPivot from "arc-data-pivot";

export class GridStore {
  // Observable Variables
  @observable selectedCellPath: Array<string> = [];
  @observable selectedRowSet: Array<string> = [];
  @observable pivotRowIdentities: Array<string> = [];
  @observable pivotColumnIdentities: Array<string> = [];
  @observable gridColumns: ArcDataGrid.Columns<ComponentDetailsRow, number> = [];
  @observable gridSubComponentData: Array<ComponentDetailsRow> = [];

  // Setters Actions

  @action setSelectedCellPath(value: Array<string>) {
    this.selectedCellPath = value;
  }

  @action setSelectedRowSet(value: Array<string>) {
    this.selectedRowSet = value;
  }

  @action setGridSubComponentData(gridSubComponentData: any) {
    this.gridSubComponentData = gridSubComponentData;
  }

  // Getters Actions

  @action getPivotRowIdentities() {
    return this.pivotRowIdentities;
  }

  @action getPivotColumnIdentities() {
    return this.pivotColumnIdentities;
  }

  @action getGridColumns() {
    return this.gridColumns;
  }

  @action getGridHiddenColumns() {
    return ["dataId"];
  }

  // Method Actions

  @action onPivotCellClick(
    rowDimensionPath: Array<string>,
    columnDimensionPath: Array<string>,
    rowSet: any
  ) {
    this.setSelectedCellPath([...rowDimensionPath, ...columnDimensionPath]);
    this.setSelectedRowSet(rowSet);
    this.extractSubComponentData();
  }

  @action getCumulativeValue(rowSet: Set<BookCaCcyRowSet>) {
    return Array.from(rowSet).reduce(function (a: number, b: BookCaCcyRowSet) {
      let sum: number = 0;
      for (let subComponent of b.subComponentList) {
        if (
          subComponent.isInformational == false &&
          subComponent.valueUsd !== undefined
        ) {
          //Remove commas from string and convert to float value
          sum += parseFloat(subComponent.valueUsd);
        }
      }
      return a + sum;
    }, 0);
  }

  pivotInstanceStore = new ArcDataPivot.InstanceStore<any>();

  @action
  setGridRef = (pivotInstance: ArcDataPivot.Instance<any> | null) => {
    this.pivotInstanceStore.setInstance(pivotInstance);
  };

  @computed
  get selectedSubRows() {
    return this.getSubComponentList(this.pivotInstanceStore.selectedRows);
  }

  @action extractSubComponentData() {
    let selectedRows: Array<string> = [];
    this.selectedRowSet.forEach((selectedRow: string) =>
      selectedRows.push(selectedRow)
    );
    this.gridSubComponentData = this.getSubComponentList(selectedRows);
  }

  @action getSubComponentList(selectedRows: any) {
    let subComponentList: Array<any> = [];
    for (let row of selectedRows) {
      if (row.subComponentList.length > 0) {
        for (let subComponent of row.subComponentList) {
          subComponentList.push(subComponent);
        }
      } else {
        subComponentList.push(row);
      }
    }
    return subComponentList;
  }
}

export default GridStore;
