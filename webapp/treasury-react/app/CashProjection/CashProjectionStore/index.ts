import { observable, action, flow, computed } from "mobx";
import {
  getCashProjectionDataList,
  getCashProjectionUrl,
} from "../api/CashProjectionDataListApi";
import {
  getCashProjectionBalanceTypeList,
} from "../../CashProjectionConfig/api/CashProjectionBalanceTypeListApi";
import { CashProjectionData } from "../models/CashProjectionData";
import SideBarStore, { INITIAL_FILTERS } from "./SideBarStore";
import GridStore from "./GridStore";
import DataStore from "./DataStore";
import { REPORT_MANAGER_UI } from "./../constants";
import MapData from "../../commons/models/MapData";
import ReferenceData from "../../commons/models/ReferenceData";


export class CashProjectionStore {
  // Store initializations
  sideBarStore: SideBarStore = new SideBarStore();
  gridStore = new GridStore();
  dataStore = new DataStore(this.gridStore, computed(() => this.cashProjectionReportDataList));

  // Observable Variables
  @observable cashProjectionReportDataList: Array<CashProjectionData> = [];
  @observable cashProjectionBalanceTypeList: Array<MapData> = [];

  // Action Methods
  @action.bound onSelect({ key, value }: any) {
    const newFilters = Object.assign({}, this.sideBarStore.filters);
    newFilters[key] = value;
    this.sideBarStore.setFilters(newFilters);
  }

  @action.bound handleSearch() {
    let searchStatus = { firstSearch: false, inProgress: true, error: false };
    this.sideBarStore.setSearchStatus(searchStatus);
    this.cashProjectionReportDataList = [];
    this.getCashProjectionData();
  }

  @action getCashProjectionKey() {
    return INITIAL_FILTERS.keyType;
  }

  @action generateReportFromAPI() {
    let paramsUrlString = "";
    let apiUrl = getCashProjectionUrl(this.sideBarStore.filters);
    let baseUrl = REPORT_MANAGER_UI;
    apiUrl = "https://" + window.location.hostname + apiUrl;
    console.log("API: " + apiUrl);
    if (apiUrl) {
      try {
        paramsUrlString += "?url=" + encodeURIComponent(apiUrl);
      } catch (e) {
        console.log("Error in the configure button url callback");
      }
    } else {
      console.log("apiUrl cannot be empty");
      return;
    }
    baseUrl += paramsUrlString;
    return baseUrl;
  }

  // Method to fetch data from API
  getCashProjectionData = flow(
    function* getCashProjectionData(this: CashProjectionStore) {
      try {
        this.cashProjectionReportDataList = yield getCashProjectionDataList(
          this.sideBarStore.filters
        );
        let flattenedCashProjectionData = this.dataStore.pivotDataList;
        this.gridStore.onPivotCellClick([], [], flattenedCashProjectionData);
        this.sideBarStore.searchStatus.inProgress = false;
      } catch (e) {
        if (e instanceof RangeError) {
          alert("Number of Days should be between 1 and 5");
        } else {
          alert("Error occurred while getting Cash Projection Data From API");
        }
        this.sideBarStore.searchStatus = {
          firstSearch: false,
          inProgress: false,
          error: true,
        };
      }
    }.bind(this)
  );

  // Method to fetch data from API
  fetchCashProjectionBalanceTypeList = async () => {
    try {
      let balanceTypeListRefData: Array<ReferenceData> = await getCashProjectionBalanceTypeList();
      let balanceTypeListMapData: Array<MapData> = [];
      balanceTypeListRefData.forEach(element => {
        if (element.id != null && element.name != null) {
          let elementMapData: MapData = {
            key: element.id,
            value: element.name
          }
          balanceTypeListMapData.push(elementMapData);
        }
      });
      this.cashProjectionBalanceTypeList = balanceTypeListMapData;
      this.sideBarStore.setSelectedBalanceTypes(this.cashProjectionBalanceTypeList);
    } catch (e) {
      alert(
        "Error occurred while getting Cash Projection Balance Type List From API"
      );
    }
  }
}

export default CashProjectionStore;
