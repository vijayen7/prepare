import { observable, computed, IComputedValue, reaction } from "mobx";
import GridStore from "./GridStore";
import { convertJavaDateDashed } from "../../commons/util";
import { NOT_AVAILABLE } from "./../../commons/constants";
import { KEY_BOOK_CA_CCY, COL_DATE, COL_COMPONENT } from "../constants";
import { getCommonColumns, getKeyColumns, getDateColumn } from "../components/CashProjectionGridColumns";
import { getBookCaCcyKeyData as getBookCaCcyKeyData } from "../key/BookCaCcy";
import { CashProjectionKey } from "../key/CashProjectionKey";
import { ComponentPivot } from "../key/ComponentPivot";
import {
  CashProjectionData,
  CashProjectionComponentDetails,
  CashProjectionComponentData,
  ComponentValue,
  CashProjectionKeyData,
} from "../models/CashProjectionData";
import { INITIAL_FILTERS } from "./SideBarStore";
import { formatFinancial } from "arc-commons/utils/number";

export class DataStore {
  // private/public will auto-assign them to the class instance before any other initializer runs.
  constructor(
    private readonly gridStore: GridStore,
    private readonly cashProjectionReportDataListBox: IComputedValue<Array<CashProjectionData>>
  ) {
    reaction(() => this.pivotDataList, () => this.updateColumnsToShowInGrid());
  }

  /**
   * getPivotDataList method flattens the cash projection report API data into pivot data
   * @param cashProjectionReportDataList contains Report API data
   */
  @computed get pivotDataList() {
    const cashProjectionReportDataList = this.cashProjectionReportDataListBox.get();
    let pivotDataList: Array<any> = [];
    if (cashProjectionReportDataList.length === 0) {
      return pivotDataList;
    }

    let dataId = 0;
    if (cashProjectionReportDataList.length) {
      pivotDataList = cashProjectionReportDataList.flatMap((reportData) => {
        let date = convertJavaDateDashed(reportData.date);
        return reportData.cashProjectionKeyData?.flatMap((keyData) => {
          return keyData.cashProjectionDataList?.flatMap((cpData) => {
            let cpKeyData = this.getKeyData(keyData);
            let componentValue = cpData.cashProjectionComponentDetails?.value;
            return this.getPivotData(
              cpData,
              dataId,
              date,
              componentValue,
              cpKeyData
            );
          });
        });
      });
    }
    return pivotDataList;
  }

  getPivotData(
    cashProjectionData: CashProjectionComponentData,
    dataId: number,
    date: string,
    componentValue: ComponentValue,
    keyData: CashProjectionKey
  ) {
    let subComponentList: Array<ComponentPivot> = [];

    let commonDataGrid = {
      component: cashProjectionData.cashProjectionComponentDetails?.component.componentName,
      date: date,
    };

    let commonDataPivot = {
      component: cashProjectionData.cashProjectionComponentDetails?.component.componentName,
      date: date + ' (USD)',
    };

    if (
      cashProjectionData.cashProjectionSubComponentDetailsList?.length &&
      cashProjectionData.cashProjectionSubComponentDetailsList?.length > 0
    ) {
      subComponentList = this.populateSubComponentData(
        commonDataGrid,
        keyData,
        cashProjectionData.cashProjectionSubComponentDetailsList
      );
    }

    dataId += 1;
    let pivotData = {
      dataId: dataId,
      valueLocal: componentValue.valueLocal,
      valueUsd: componentValue.valueUsd,
      subComponentList: subComponentList,
      ...commonDataPivot,
      ...keyData,
    };

    return pivotData;
  }

  // updateColumnsToShowInGrid function decides which columns are to be shown in grid based on Key
  updateColumnsToShowInGrid() {
    this.gridStore.pivotRowIdentities = [];
    this.gridStore.pivotColumnIdentities = [];
    this.gridStore.gridColumns = [];
    this.gridStore.pivotRowIdentities.push(COL_COMPONENT);
    this.gridStore.pivotColumnIdentities.push(COL_DATE);
    let keyColumns = getKeyColumns(KEY_BOOK_CA_CCY);
    this.gridStore.pivotRowIdentities = this.gridStore.pivotRowIdentities.concat(keyColumns);
    this.gridStore.pivotRowIdentities.splice(1,2);
    this.gridStore.gridColumns = this.gridStore.gridColumns.concat(getDateColumn());
    this.gridStore.gridColumns = this.gridStore.gridColumns.concat(keyColumns);
    this.gridStore.gridColumns = this.gridStore.gridColumns.concat(getCommonColumns());
  }

  populateSubComponentData(
    commonData: any,
    keyData: CashProjectionKey,
    subComponentDetailsList?: Array<CashProjectionComponentDetails>
  ) {
    let subComponentList: Array<ComponentPivot> = [];
    let subComponentId = 0;
    if (subComponentDetailsList?.length && subComponentDetailsList?.length > 0) {
      for (let subComponentDetail of subComponentDetailsList) {
        let subComponent = subComponentDetail;
        subComponentId += 1;
        let subComponentData = {
          dataId: subComponentId,
          subComponent: subComponent.component.componentName,
          isInformational: subComponent.component.isInformational,
          valueLocal: subComponent.value.valueLocal,
          valueUsd: (Math.abs(subComponent.value.valueLocal) === 0) ? 0 : subComponent.value.valueUsd,
          ...commonData,
          ...keyData,
        };
        subComponentList.push(subComponentData);
      }
    }
    return subComponentList;
  }

  getKeyData(cashProjectionKeyData: CashProjectionKeyData) {
    let key = {};
    if (INITIAL_FILTERS.keyType === KEY_BOOK_CA_CCY) {
      key = getBookCaCcyKeyData(cashProjectionKeyData.cashProjectionKey);
    }

    let keyData = {
      knowledgeDate: cashProjectionKeyData.knowledgeDate ? convertJavaDateDashed(cashProjectionKeyData.knowledgeDate) : NOT_AVAILABLE,
      versionId: cashProjectionKeyData.version ? cashProjectionKeyData.version : NOT_AVAILABLE,
      ...key
    }
    return keyData;
  }
}

export default DataStore;
