import { getCashProjectionDataList } from '../api/CashProjectionDataListApi';
import { BASE_URL } from "../../commons/constants";
import { ArcFetch } from 'arc-commons';
import SERVICE_URLS from '../../commons/UrlConfigs';

jest.mock('arc-commons');

describe('Get Cash Projection Data List API', () => {

  test('Testing function getCashProjectionDataList', () => {
    const getJSONSpy = jest.spyOn(ArcFetch, 'getJSON');
    getCashProjectionDataList({
      selectedDate: '2020-02-13',
      numberOfDays: 4,
      keyType: 'book_ca_ccy',
      getLatest: false,
      selectedBooks: [-1],
      selectedCustodianAccounts: [-1],
      selectedCurrencies: [-1]
    });
    expect(getJSONSpy).toHaveBeenCalled();
    expect(getJSONSpy.mock.calls[0][0])
      .toEqual(BASE_URL + 'service/'+ SERVICE_URLS.cashProjectionService.getCashProjectionReportData + `?cashProjectionInputs.date=2020-02-13&cashProjectionInputs.numberOfDays=4&cashProjectionInputs.cashProjectionInputs.cashProjectionKeyType.keyType=book_ca_ccy&cashProjectionInputs.getLatest=false&cashProjectionInputs.keyInputs.bookCustodianAccountCurrency.books=-1&cashProjectionInputs.keyInputs.bookCustodianAccountCurrency.custodianAccounts=-1&cashProjectionInputs.keyInputs.bookCustodianAccountCurrency.currencies=-1&inputFormat=PROPERTIES&format=json`);
  });
});
