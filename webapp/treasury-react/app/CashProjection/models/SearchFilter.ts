import ReferenceData from "./../../commons/models/ReferenceData";
import MapData from "./../../commons/models/MapData";

// Interface for Search Filters
export interface SearchFilter {
  selectedDate: string;
  selectedBooks?: Array<ReferenceData>;
  selectedCustodianAccounts?: Array<ReferenceData>;
  selectedCurrencies?: Array<ReferenceData>;
  keyType: string;
  selectedBalanceTypes: Array<MapData>;
}

export interface SearchStatus {
  firstSearch: boolean;
  inProgress: boolean;
  error: boolean;
}
