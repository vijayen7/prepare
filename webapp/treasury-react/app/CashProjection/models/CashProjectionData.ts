// Interface for Cash Projection Data
import { CashProjectionKey } from "./../key/CashProjectionKey";

export interface CashProjectionData {
  "@CLASS": "com.arcesium.treasury.cashprojection.model.CashProjectionData";
  date: string;
  cashProjectionKeyType: CashProjectionKeyType;
  cashProjectionKeyData?: Array<CashProjectionKeyData>;
}

interface CashProjectionKeyType {
  "@CLASS": "com.arcesium.treasury.model.cashprojection.CashProjectionKeyType";
  keyType: string;
  keyTypeId: number;
}

export interface CashProjectionKeyData {
  "@CLASS": "com.arcesium.treasury.cashprojection.model.CashProjectionKeyData";
  cashProjectionKey: CashProjectionKey;
  cashProjectionDataList?: Array<CashProjectionComponentData>;
  version?: number;
  knowledgeDate?: Date;
}

export interface CashProjectionComponentData {
  "@CLASS": "com.arcesium.treasury.cashprojection.model.CashProjectionComponentData";
  cashProjectionComponentDetails: CashProjectionComponentDetails;
  cashProjectionSubComponentDetailsList?: Array<CashProjectionComponentDetails>;
}

export interface CashProjectionComponentDetails {
  "@CLASS": "com.arcesium.treasury.cashprojection.model.CashProjectionComponentDetails";
  component: CashProjectionComponent;
  value: ComponentValue;
}

export interface CashProjectionComponent {
  "@CLASS": "com.arcesium.treasury.model.cashprojection.CashProjectionComponent";
  componentId: number;
  componentName: string;
  componentType: string;
  isInformational: boolean;
}

export interface ComponentValue {
  "@CLASS": "com.arcesium.treasury.model.cashprojection.ComponentValue";
  valueLocal: number;
  valueUsd: number;
}
