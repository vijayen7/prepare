import CLASS_NAMESPACE from "../constants";
import WatchListKey from "./WatchListKey";

export default interface WatchListInput {
  type: string;
  key: WatchListKey;
  startDate: string;
  endDate: string;
  tags?: string[] | null;
  message: string;
  observers?: string[] | null;
  expiryNotificationDays: number;
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.watchlist.model.WatchListInput;
}
