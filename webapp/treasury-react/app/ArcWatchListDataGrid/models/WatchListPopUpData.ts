export default interface WatchListPopUpData {
  [key: string]: any;
  selectedStartDate: string;
  selectedEndDate: string;
  selectedTags: string[];
  selectedMessage: string;
  selectedObservers?: string[];
  selectedExpiryNotificationDate: string;
}
