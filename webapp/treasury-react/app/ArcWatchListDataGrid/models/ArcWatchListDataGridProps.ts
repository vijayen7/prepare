import { ArcDataGridProps } from "arc-data-grid/typings/grid";
import KeyData from "./KeyData";

interface WatchListConfig {
  watchListStartDate: string;
  watchListEndDate: string;
  watchListType: string;
  watchListKeyMapper: (row: any) => KeyData[];
  getRowDate: (row: any) => string;
}

export type ArcWatchListDataGridProps<TRow extends object, TRowId> = ArcDataGridProps<TRow, TRowId> & WatchListConfig
