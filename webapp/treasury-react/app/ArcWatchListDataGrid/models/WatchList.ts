import CLASS_NAMESPACE from "../constants";
import WatchListKey from "./WatchListKey";


export default interface WatchList {
  id: number;
  type: string;
  key: WatchListKey;
  startDate: string;
  endDate: string;
  tags: string[];
  message: string;
  observers: string[];
  expiryNotificationDays: number;
  expiryNotificationDate: string;
  createdByUser: string;
  deletedByUser?: string;
  knowledgeStartTime?: string;
  knowledgeEndTime?: string;
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.watchlist.model.WatchList;
}
