enum WatchListStatus {
  NOT_IN_WATCHLIST,
  IN_WATCHLIST,
  IN_EXPIRING_WATCHLIST
}
export default WatchListStatus;


