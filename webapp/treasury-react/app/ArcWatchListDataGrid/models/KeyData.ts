export default interface KeyData {
  key: string;
  value: string;
}
