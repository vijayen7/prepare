import WatchListPopUpData from "./WatchListPopUpData";


export interface WatchListFormComponentProps {
  onChange: (key: string, value: any) => void
  handleMessageChange: any;
  watchListPopUpData: WatchListPopUpData;
}
