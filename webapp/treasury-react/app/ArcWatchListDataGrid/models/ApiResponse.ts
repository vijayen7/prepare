export default interface ApiResponse {
  successStatus: boolean;
  message: string;
  response: any
  "@CLASS"?: string;
}
