import CLASS_NAMESPACE from "../constants";

export default interface WatchListKey {
  key: string;
  verbose: string;
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.watchlist.model.WatchListKey;
}
