import { observer } from "mobx-react-lite";
import React from "react";
import WatchListStatus from "../models/WatchListStatus";
import WatchListStore from "../store";

const WatchListColumn: React.FC<any> = ({ value, context }) => {

  const renderWatchListColumn = () => {

    const watchListProgressElement =
      <i
        title="Loading..."
        className="icon-loading"
      ></i>

    const watchListedRowElement =
      <i
        title="Watchlisted Row"
        className="icon-view"
        onClick={(e) => {
          e.stopPropagation();
          WatchListStore.operateOnModifyWatchList(context);
        }}
      ></i>


    const nonWatchListedRowElement =
      <i
        title="Add Row To Watchlist"
        className="icon-hide"
        onClick={(e) => {
          e.stopPropagation();
          WatchListStore.operateOnAddWatchList(context);
        }}
      ></i>


    const inExpiringWatchListElement =
      <i
        title="Expiring WatchList"
        className="icon-warning"
      ></i>


    if (WatchListStore.inWatchListProgress) {
      return (
        <>
          {watchListProgressElement}
        </>
      );
    } else {
      let watchListStatus = WatchListStore.getWatchListStatus(context.row, context.rowId);
      switch (watchListStatus) {
        case WatchListStatus.IN_EXPIRING_WATCHLIST:
          return (
            <>
              {watchListedRowElement}
              {inExpiringWatchListElement}
            </>
          );
        case WatchListStatus.IN_WATCHLIST:
          return (
            <>
              {watchListedRowElement}
            </>
          );
        default:
          return (
            <>
              {nonWatchListedRowElement}
            </>
          );
      }
    }
    return <></>;
  };
  return <>{renderWatchListColumn()}</>;
};

export default observer(WatchListColumn);
