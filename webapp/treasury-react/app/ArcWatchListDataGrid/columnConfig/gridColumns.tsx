import React from "react";
import WatchListStatus from "../models/WatchListStatus";
import WatchListColumn from './WatchListColumn';

const getAccessorValue = (searchWatchListStore: any, row: any, rowId: number) => {

  let watchListStatus = searchWatchListStore.getWatchListStatus(row, rowId);
  switch (watchListStatus) {
    case WatchListStatus.IN_EXPIRING_WATCHLIST:
      return "In Expiring WatchList";
    case WatchListStatus.IN_WATCHLIST:
      return "In WatchList";
    default:
      return "Not In WatchList";
  }

}

export const getGridColumns = (searchWatchListStore, gridColumns: any) => {
  let filteredGridColumns = [...gridColumns];
  if (filteredGridColumns[0].identity === 'manage watchlist') {
    filteredGridColumns.shift();
  }
  let watchlistGridColumn =
  {
    header: "Manage WatchList",
    identity: "manage watchlist",
    accessor: (row, rowId) => {
      return getAccessorValue(searchWatchListStore, row, rowId);
    },
    Cell: (value, context) => <WatchListColumn value={value} context={context} />,
    disableSearches: true,
    disableResizing: false,
    disableGroupBy: true,
    disablePinning: false,
    width: 10,
  }
  filteredGridColumns.unshift(watchlistGridColumn);
  return filteredGridColumns;
};

export const getDisplayColumns = (displayColumns: any) => {
  let filteredDisplayColumns = [...displayColumns];
  if (filteredDisplayColumns[0] === 'manage watchlist') {
    filteredDisplayColumns.shift();
  }
  filteredDisplayColumns.unshift("manage watchlist");
  return filteredDisplayColumns;
};

export const getPinnedColumns = (pinnedColumns: any) => {
  return { 'manage watchlist': true, ...pinnedColumns };
}
