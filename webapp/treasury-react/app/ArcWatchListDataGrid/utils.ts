import { ToastService } from "arc-react-components";
import crypto from 'crypto';
import moment from "moment";
import CLASS_NAMESPACE, { dateRegex, TOAST_TYPE } from "./constants";
import WatchList from "./models/WatchList";
import WatchListInput from "./models/WatchListInput";
import WatchListKey from "./models/WatchListKey";
import WatchListPopUpData from "./models/WatchListPopUpData";
import { getDateDifference } from "../commons/util";

export const getDialogDataForWatchList = (watchListItem: WatchList): WatchListPopUpData => {
  return {
    selectedStartDate: getParsedDate(watchListItem.startDate),
    selectedEndDate: getParsedDate(watchListItem.endDate),
    selectedTags: watchListItem.tags,
    selectedMessage: watchListItem.message,
    selectedObservers: watchListItem.observers,
    selectedExpiryNotificationDate: getParsedDate(watchListItem.expiryNotificationDate)
  };
}

export const getWatchListDetailFromDialog = (watchListPopUpData: WatchListPopUpData, hashedKeyColumns: string, keyVerbose: string, watchlistType: string): WatchListInput => {
  let watchListKey: WatchListKey = {
    key: hashedKeyColumns,
    verbose: keyVerbose,
    "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.watchlist.model.WatchListKey,
  }
  return {
    type: watchlistType,
    key: watchListKey,
    startDate: watchListPopUpData.selectedStartDate,
    endDate: watchListPopUpData.selectedEndDate,
    tags: watchListPopUpData.selectedTags,
    message: watchListPopUpData.selectedMessage,
    observers: watchListPopUpData.selectedObservers,
    expiryNotificationDays: getDateDifference(watchListPopUpData.selectedExpiryNotificationDate, watchListPopUpData.selectedEndDate),
    "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.watchlist.model.WatchListInput,
  };
}

export const getHash = (keyColumns: string) => {
  return crypto.createHash('sha256')
    .update(keyColumns)
    .digest('hex');

}
export function parseDJSrvString(input) {
  if (input && input.match(dateRegex)) {
    const matches = input.match(dateRegex);
    const timestamp = parseInt(matches[1], 10);
    return moment.unix(timestamp / 1000);
  }
  return null;
}

// Convert the date in unixTimeStamp to yyyy-mm-dd format
export const getParsedDate = (date?: string) => {
  const parsedDate = parseDJSrvString(date);
  return parsedDate ? parsedDate.toISOString().substring(0, 10) : "n/a";
};

const getToastType = (type: string) => {
  switch (type) {
    case TOAST_TYPE.CRITICAL:
      return ToastService.ToastType.CRITICAL;
    case TOAST_TYPE.SUCCESS:
      return ToastService.ToastType.SUCCESS;
    case TOAST_TYPE.INFO:
      return ToastService.ToastType.INFO;
    default:
      return ToastService.ToastType.SUCCESS;
  }
};

export const showToastService = (content: string, type: string) => {
  ToastService.append({
    content: content,
    type: getToastType(type),
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: 7000,
  });
};

export const isValidWatchListItem = (watchListItem: WatchList | undefined, rowDate: string): Boolean => {
  if (watchListItem !== undefined && rowDate >= getParsedDate(watchListItem.startDate) && rowDate <= getParsedDate(watchListItem.endDate)) {
    return true;
  }
  return false;
}
