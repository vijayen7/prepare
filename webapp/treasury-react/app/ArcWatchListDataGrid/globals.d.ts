import { Component } from 'react';
import { ArcWatchListDataGridProps } from './model/ArcWatchListDataGridProps';

class ArcWatchListDataGrid<T, U> extends Component<ArcWatchListDataGridProps<T, U>, any> { }

namespace ArcWatchListDataGrid {
  export = ArcWatchListDataGridProps;
}
export default ArcWatchListDataGrid;
