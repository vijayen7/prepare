export const dateRegex = /\\\/Date\((-?\d+)\)\\\//;

export const URL = process.env.url;
export const BASE_URL = `${URL}/treasury/`;

export const KEY_SEPARATOR = '_';

export const SERVICE_URLS = {
  watchListService: {
    getWatchLists: "watchListService/getWatchLists",
    addWatchList: "watchListService/addWatchList",
    invalidateWatchList: "watchListService/invalidateWatchList",
    updateWatchList: "watchListService/updateWatchList"
  },
};

const CLASS_NAMESPACE = {
  com: {
    arcesium: {
      treasury: {
        watchlist: {
          model: {
            WatchList: "com.arcesium.treasury.watchlist.model.WatchList",
            WatchListInput: "com.arcesium.treasury.watchlist.model.WatchListInput",
            WatchListKey: "com.arcesium.treasury.watchlist.model.WatchListKey",
          }
        }
      }
    }
  }
} as const;

export default CLASS_NAMESPACE;



export const INITIAL_WATCHLIST_DETAIL_DATA = {
  INITIAL_POP_UP_DATA: {
    selectedType: "",
    selectedStartDate: "",
    selectedEndDate: "",
    selectedTags: [],
    selectedMessage: "",
    selectedObservers: [],
    selectedExpiryNotificationDate: "",
  },
  INITIAL_API_RESPONSE: {
    successStatus: true,
    message: "",
    response: null,
    "@CLASS": "com.arcesium.treasury.watchlist.common.ApiResponse",
  },
}

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const MESSAGES = {
  ERROR_WHILE_ADDING_WATCHLIST_DATA: "Some error occurred while adding watchlist data",
  ERROR_WHILE_UPDATING_WATCHLIST_DATA: "Some error occurred while updating watchlist data",
  ERROR_WHILE_INVALIDATING_WATCHLIST_DATA: "Some error occurred while invalidating watchlist data",
  ERROR_WHILE_FETCHING_WATCHLIST_DATA: "Some error occurred while fetching watchlist data",
};
