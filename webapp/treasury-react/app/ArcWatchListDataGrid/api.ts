import { ArcFetch } from "arc-commons";
import { BASE_URL, SERVICE_URLS } from "./constants";
import WatchListInput from "./models/WatchListInput";

const getQueryParams = (params) => ({
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
  credentials: 'include',
  method: 'POST',
  body: params
});

export const addWatchList = (watchListInput: WatchListInput) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.watchListService
    .addWatchList}`

  const params = `watchListInput=${encodeURIComponent(
    JSON.stringify(watchListInput)
  )}&inputFormat=json&format=json`;

  return ArcFetch.getJSON(url, getQueryParams(params));
}

export const getWatchLists = (startDate: string, endDate: string, type: string) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.watchListService
    .getWatchLists}`

  const params = `startDate=${startDate}&endDate=${endDate}&type=${type}&isActive=${true}&format=json`;

  return ArcFetch.getJSON(url, getQueryParams(params));
}

export const invalidateWatchList = (watchListId: number) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.watchListService
    .invalidateWatchList}`

  const params = `watchListId=${watchListId}&format=json`;

  return ArcFetch.getJSON(url, getQueryParams(params));
}

export const updateWatchList = (watchListId: number, watchListInput: WatchListInput) => {
  const url = `${BASE_URL}service/${SERVICE_URLS.watchListService
    .updateWatchList}`

  const params = `watchListId=${watchListId}&watchListInput=${encodeURIComponent(
    JSON.stringify(watchListInput))}&inputFormat=json&format=json`;

  return ArcFetch.getJSON(url, getQueryParams(params));

}
