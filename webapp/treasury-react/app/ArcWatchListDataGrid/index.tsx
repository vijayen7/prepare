import ArcDataGrid from "arc-data-grid";
import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import { getDisplayColumns, getGridColumns, getPinnedColumns } from "./columnConfig/gridColumns";
import WatchListDetailDialog from "./container/WatchListDetailDialog";
import WatchListModifyDialog from "./container/WatchListModifyDialog";
import { ArcWatchListDataGridProps } from "./models/ArcWatchListDataGridProps";
import WatchListStore from "./store";

const ArcWatchListDataGrid: React.FC<ArcWatchListDataGridProps<any, any>> = (props: ArcWatchListDataGridProps<any, any>) => {

  useEffect(() => {
    WatchListStore.props = props;
    props.rows.forEach(
      (row: any, index: number) => {
        if (props.getRowId) {
          WatchListStore.initStore(row, props.getRowId(row, index))
        } else {
          WatchListStore.initStore(row, index)
        }
      }
    );
    WatchListStore.getWatchList(props.watchListStartDate, props.watchListEndDate, props.watchListType);
  }, [])

  useEffect(() => {
    props.columns = getGridColumns(WatchListStore, props.columns);
  }, [WatchListStore.filterPanelCount])

  if (!WatchListStore.inWatchListProgress) {
    WatchListStore.props = props;
    props.columns = getGridColumns(WatchListStore, props.columns);
    props.displayColumns = getDisplayColumns(props.displayColumns);
    props.pinnedColumns = getPinnedColumns(props.pinnedColumns);
  }

  return (
    <>
      <ArcDataGrid
        {...props}
      />
      <WatchListDetailDialog />
      <WatchListModifyDialog />
    </>
  );
};

export default observer(ArcWatchListDataGrid);
