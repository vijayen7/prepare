import { action, observable } from "mobx";
import { addWatchList, getWatchLists, invalidateWatchList, updateWatchList } from "./api";
import { INITIAL_WATCHLIST_DETAIL_DATA, KEY_SEPARATOR, MESSAGES, TOAST_TYPE } from "./constants";
import ApiResponse from "./models/ApiResponse";
import { ArcWatchListDataGridProps } from "./models/ArcWatchListDataGridProps";
import KeyData from "./models/KeyData";
import WatchList from "./models/WatchList";
import WatchListInput from "./models/WatchListInput";
import WatchListPopUpData from "./models/WatchListPopUpData";
import WatchListStatus from "./models/WatchListStatus";
import { getDialogDataForWatchList, getHash, getParsedDate, getWatchListDetailFromDialog, isValidWatchListItem, showToastService } from "./utils";

export class WatchListStore {

  @observable
  inProgress: boolean = false;

  @observable
  inWatchListProgress: boolean = false;

  @observable
  watchListPopUpData: WatchListPopUpData = INITIAL_WATCHLIST_DETAIL_DATA.INITIAL_POP_UP_DATA;

  @observable
  apiResponse: ApiResponse =
    INITIAL_WATCHLIST_DETAIL_DATA.INITIAL_API_RESPONSE;

  @observable
  watchListDataGridRow: WatchList[] = [];

  @observable
  watchListIds: number[] = [];

  @observable
  showAddWatchListDialog: boolean = false;

  @observable
  showModifyWatchListDialog: boolean = false;

  @observable
  showConfirmationDeleteDialog: boolean = false;

  @observable
  showKeyColumnsDialog: boolean = false;

  @observable
  tagsList: string[] = [];

  @observable
  hashedkeyColumns: string = "";

  @observable
  keyVerbose: string = "";

  @observable
  modifyFormIndex = 0;

  @observable
  rowIdToVerboseStringMap: Map<number | string, string> = new Map();

  @observable
  rowIdToHashKeyMap: Map<number | string, string> = new Map();

  @observable
  hashKeyToWatchListIds: Map<string, number[]> = new Map();

  @observable
  watchListIdToWatchListItem: Map<number, WatchList> = new Map();

  @observable
  watchlistDialogTitle: string = "";

  @observable
  props!: ArcWatchListDataGridProps<any, any>;

  @observable
  modifiedWatchlistId: number = -1;

  @observable
  gridRowDate: string = "";

  @observable
  filterPanelCount: number = 0;

  @action.bound
  operateOnAddWatchList(context: any) {
    this.hashedkeyColumns = String(this.rowIdToHashKeyMap.get(context.rowId));
    this.keyVerbose = String(this.rowIdToVerboseStringMap.get(context.rowId));
    this.watchListPopUpData = INITIAL_WATCHLIST_DETAIL_DATA.INITIAL_POP_UP_DATA;
    if (this.props.watchListStartDate && this.props.watchListEndDate) {
      this.watchListPopUpData.selectedStartDate = this.props.watchListStartDate;
      this.watchListPopUpData.selectedEndDate = this.props.watchListEndDate;
    }
    this.watchlistDialogTitle = "Add WatchList Detail";
    this.showAddWatchListDialog = true;
  };

  @action.bound
  operateOnModifyWatchList(context: any) {
    this.hashedkeyColumns = String(this.rowIdToHashKeyMap.get(context.rowId));
    this.keyVerbose = String(this.rowIdToVerboseStringMap.get(context.rowId));
    let numberArray = this.hashKeyToWatchListIds.get(this.hashedkeyColumns);
    let watchlistIds: number[] = [];
    if (numberArray === undefined) {
      watchlistIds = [];
    } else {
      watchlistIds = numberArray;
    }
    this.watchListIds = watchlistIds;
    this.modifiedWatchlistId = watchlistIds[0];
    let watchListItem = this.watchListIdToWatchListItem.get(this.modifiedWatchlistId);
    this.gridRowDate = this.props.getRowDate(context.row);
    if (watchListItem) {
      this.watchListPopUpData = getDialogDataForWatchList(watchListItem);
      if (this.gridRowDate >= getParsedDate(watchListItem?.expiryNotificationDate)) {
        this.watchlistDialogTitle = "Manage WatchList Detail ".concat(
          "(" + (this.modifyFormIndex + 1) + "/" + this.watchListIds.length + ") Expiring"
        );
      } else {
        this.watchlistDialogTitle = "Manage WatchList Detail ".concat(
          "(" + (this.modifyFormIndex + 1) + "/" + this.watchListIds.length + ")"
        );
      }
      this.showModifyWatchListDialog = true;
    }
  }

  @action.bound
  onChange = (key: string, value: any) => {
    this.watchListPopUpData[key] = value
  }

  @action.bound
  handleMessageChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.watchListPopUpData.selectedMessage = event.target.value;
  }

  @action.bound
  onPrevClick = () => {
    this.modifyFormIndex = this.modifyFormIndex - 1;
    this.modifiedWatchlistId = this.watchListIds[this.modifyFormIndex];
    let watchListItem = this.watchListIdToWatchListItem.get(this.modifiedWatchlistId);

    if (watchListItem) {
      this.watchListPopUpData = getDialogDataForWatchList(watchListItem);
      if (this.gridRowDate >= getParsedDate(watchListItem?.expiryNotificationDate)) {

        this.watchlistDialogTitle = "Manage WatchList Detail ".concat(
          "(" + (this.modifyFormIndex + 1) + "/" + this.watchListIds.length + ") Expiring"
        );
      } else {
        this.watchlistDialogTitle = "Manage WatchList Detail ".concat(
          "(" + (this.modifyFormIndex + 1) + "/" + this.watchListIds.length + ")"
        );
      }
    }
  }

  @action.bound
  onNextClick = () => {
    this.modifyFormIndex = this.modifyFormIndex + 1;
    this.modifiedWatchlistId = this.watchListIds[this.modifyFormIndex];
    let watchListItem = this.watchListIdToWatchListItem.get(this.modifiedWatchlistId);

    if (watchListItem) {
      this.watchListPopUpData = getDialogDataForWatchList(watchListItem);
      if (this.gridRowDate >= getParsedDate(watchListItem?.expiryNotificationDate)) {
        this.watchlistDialogTitle = "Manage WatchList Detail ".concat(
          "(" + (this.modifyFormIndex + 1) + "/" + this.watchListIds.length + ") Expiring"
        );
      } else {
        this.watchlistDialogTitle = "Manage WatchList Detail ".concat(
          "(" + (this.modifyFormIndex + 1) + "/" + this.watchListIds.length + ")"
        );
      }
    }
  }

  @action.bound
  onReset() {
    this.watchListPopUpData = INITIAL_WATCHLIST_DETAIL_DATA.INITIAL_POP_UP_DATA;
    this.apiResponse = INITIAL_WATCHLIST_DETAIL_DATA.INITIAL_API_RESPONSE;
    this.modifiedWatchlistId = -1;
    this.hashedkeyColumns = "";
    this.keyVerbose = "";
    this.watchlistDialogTitle = "";
    this.modifyFormIndex = 0;
  }

  @action.bound
  onCloseAddWatchListDialog() {
    this.showAddWatchListDialog = false;
    this.onReset();
  }

  @action.bound
  onCloseModifyWatchListDialog() {
    this.showModifyWatchListDialog = false;
    this.onReset();
  }

  @action.bound
  onCloseKeyColumnsDialog() {
    this.showKeyColumnsDialog = false;
  }

  @action.bound
  onUpdate() {
    let watchListInput: WatchListInput = getWatchListDetailFromDialog(
      this.watchListPopUpData, this.hashedkeyColumns, this.keyVerbose, this.props.watchListType
    )
    this.updateWatchList(this.modifiedWatchlistId, watchListInput);
  }

  @action.bound
  onDelete() {
    this.showConfirmationDeleteDialog = true;
  }

  @action.bound
  onCloseConfirmationDialog() {
    this.showConfirmationDeleteDialog = false;
  }

  @action.bound
  onConfirmDelete() {
    this.invalidateWatchlist(this.modifiedWatchlistId);
  }

  @action.bound
  onSave() {
    let watchListInput: WatchListInput = getWatchListDetailFromDialog(
      this.watchListPopUpData, this.hashedkeyColumns, this.keyVerbose, this.props.watchListType
    )
    this.addWatchList(watchListInput);
  }

  @action.bound
  postSaveCall() {
    if (this.apiResponse === INITIAL_WATCHLIST_DETAIL_DATA.INITIAL_API_RESPONSE) {
      return;
    }
    if (this.apiResponse.successStatus) {
      this.addWatchListItem(this.apiResponse.response);
    }
    else {
      showToastService(this.apiResponse.message, TOAST_TYPE.CRITICAL);
    }
    this.showAddWatchListDialog = false;
    this.onReset();
  }

  @action.bound
  postDeleteCall() {
    this.showConfirmationDeleteDialog = false;
    if (this.apiResponse === INITIAL_WATCHLIST_DETAIL_DATA.INITIAL_API_RESPONSE) {
      return;
    }
    if (this.apiResponse.successStatus) {
      this.deleteWatchListItem();
    }
    else {
      showToastService(this.apiResponse.message, TOAST_TYPE.CRITICAL);
    }
    this.showModifyWatchListDialog = false;
    this.onReset();
  }

  @action.bound
  postUpdateCall() {
    if (this.apiResponse === INITIAL_WATCHLIST_DETAIL_DATA.INITIAL_API_RESPONSE) {
      return;
    }
    if (this.apiResponse.successStatus) {
      this.addWatchListItem(this.apiResponse.response);
      this.deleteWatchListItem();
    }
    else {
      showToastService(this.apiResponse.message, TOAST_TYPE.CRITICAL);
    }
    this.showModifyWatchListDialog = false;
    this.onReset();
  }

  addWatchListItem = (response: WatchList) => {
    let watchListItem: WatchList = response;
    this.watchListDataGridRow.push(watchListItem);
    this.watchListDataGridRow = this.watchListDataGridRow.slice().sort((a, b) => a.expiryNotificationDays - b.expiryNotificationDays);
    let numberArray: any = this.hashKeyToWatchListIds.get(watchListItem.key.key);
    let watchListIds: number[] = [];
    if (numberArray === undefined) {
      watchListIds = [];
    } else {
      watchListIds = numberArray;
    }
    watchListIds.push(watchListItem.id);
    this.watchListIdToWatchListItem.set(watchListItem.id, watchListItem);
    this.hashKeyToWatchListIds.set(watchListItem.key.key, watchListIds);
    this.tagsList = this.tagsList.concat(watchListItem.tags);
    this.tagsList = [...new Set(this.tagsList)];
    this.filterPanelCount = this.filterPanelCount + 1;
  }

  deleteWatchListItem() {
    let watchListItem: WatchList = {} as WatchList;
    this.watchListDataGridRow.forEach(
      (watchList, index) => {
        if (watchList.id === this.modifiedWatchlistId) {
          watchListItem = watchList;
          this.watchListDataGridRow.splice(index, 1);
        }
      });
    this.watchListDataGridRow = this.watchListDataGridRow.slice().sort((a, b) => a.expiryNotificationDays - b.expiryNotificationDays);
    let watchListIds: any = this.hashKeyToWatchListIds.get(watchListItem.key.key);
    watchListIds.forEach(
      (watchListId, index) => {
        if (watchListId === this.modifiedWatchlistId) {
          watchListIds.splice(index, 1);
        }
      }
    )
    if (watchListIds.length === 0) {
      this.hashKeyToWatchListIds.delete(this.hashedkeyColumns);
    }
    else {
      this.hashKeyToWatchListIds.set(watchListItem.key.key, watchListIds);
    }
    this.watchListIdToWatchListItem.delete(this.modifiedWatchlistId);
    this.filterPanelCount = this.filterPanelCount - 1;
  }

  addWatchList = async (watchListInput: WatchListInput) => {
    try {
      this.inProgress = true;
      this.apiResponse = await addWatchList(watchListInput);
      this.postSaveCall();
    } catch (error) {
      showToastService(MESSAGES.ERROR_WHILE_ADDING_WATCHLIST_DATA, TOAST_TYPE.CRITICAL);
    } finally {
      this.inProgress = false;
    }
  }

  invalidateWatchlist = async (watchListId: number) => {
    try {
      this.inProgress = true;
      this.apiResponse = await invalidateWatchList(watchListId);
      this.postDeleteCall();
    } catch (error) {
      showToastService(MESSAGES.ERROR_WHILE_INVALIDATING_WATCHLIST_DATA, TOAST_TYPE.CRITICAL);
    } finally {
      this.inProgress = false;
    }
  }

  updateWatchList = async (watchListId: number, watchListInput: WatchListInput) => {
    try {
      this.inProgress = true;
      this.apiResponse = await updateWatchList(watchListId, watchListInput);
      this.postUpdateCall();
    } catch (error) {
      showToastService(MESSAGES.ERROR_WHILE_UPDATING_WATCHLIST_DATA, TOAST_TYPE.CRITICAL);
    } finally {
      this.inProgress = false;
    }
  }

  initStore = (row: any, rowId: number | string) => {
    let keyColumns: KeyData[] = this.props.watchListKeyMapper(row)
    let sortedColumnList: KeyData[] = keyColumns.sort((columnNameA, columnNameB) => columnNameA.key.localeCompare(columnNameB.key));
    let keyVerbose: JSON = JSON;
    let concatenatedKeyColumns: string = "";
    sortedColumnList.forEach(
      keyColumn => {
        concatenatedKeyColumns += keyColumn.value + KEY_SEPARATOR;
        keyVerbose[keyColumn.key] = keyColumn.value;
      }
    )
    concatenatedKeyColumns = concatenatedKeyColumns.slice(0, -1);
    let hashedString = getHash(concatenatedKeyColumns);
    let verboseString = JSON.stringify(keyVerbose);
    this.rowIdToVerboseStringMap.set(rowId, verboseString);
    this.rowIdToHashKeyMap.set(rowId, hashedString);
  }

  getWatchList = async (startDate: string, endDate: string, type: string) => {
    try {
      this.inWatchListProgress = true;
      this.watchListDataGridRow = [];
      this.watchListIds = [];
      this.hashKeyToWatchListIds = new Map();
      this.watchListIdToWatchListItem = new Map();
      let apiResponse: ApiResponse = await getWatchLists(startDate, endDate, type);
      if (apiResponse.successStatus) {
        apiResponse.response.forEach((response) =>
          this.addWatchListItem(response)
        )
      }
    } catch (error) {
      showToastService(MESSAGES.ERROR_WHILE_FETCHING_WATCHLIST_DATA, TOAST_TYPE.CRITICAL);
    } finally {
      this.inWatchListProgress = false;
    }
  }

  getWatchListStatus = (row: any, rowId: number) => {
    let hashedKey: string | undefined = this.rowIdToHashKeyMap.get(rowId);
    let rowDate = this.props.getRowDate(row);
    let inWatchList: Boolean = false;
    let inExpiringWatchList: Boolean = false;
    if (hashedKey !== undefined && this.hashKeyToWatchListIds.has(hashedKey)) {
      let watchListIds: number[] | undefined = this.hashKeyToWatchListIds.get(hashedKey);
      watchListIds?.forEach(
        (watchListId) => {
          let watchListItem: WatchList | undefined = this.watchListIdToWatchListItem.get(watchListId);
          inWatchList = isValidWatchListItem(watchListItem, rowDate);
          if (watchListItem !== undefined && rowDate >= getParsedDate(watchListItem.expiryNotificationDate)) {
            inExpiringWatchList = true;
          }
        }
      )
      if (inWatchList && inExpiringWatchList) {
        return WatchListStatus.IN_EXPIRING_WATCHLIST
      }
      else if (inWatchList && !inExpiringWatchList) {
        return WatchListStatus.IN_WATCHLIST
      } else {
        return WatchListStatus.NOT_IN_WATCHLIST
      }
    }
  }

}

export default new WatchListStore();
