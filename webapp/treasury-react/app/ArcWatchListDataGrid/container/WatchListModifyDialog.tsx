import { Dialog, Layout } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import WatchListStore from "../store";
import WatchListForm from "./WatchListForm";

const WatchListModifyDialog: React.FC = () => {

  let watchlistIds: number[] | undefined = WatchListStore.hashKeyToWatchListIds.get(WatchListStore.hashedkeyColumns);
  if (watchlistIds === undefined) {
    watchlistIds = [];
  }
  WatchListStore.watchListIds = watchlistIds;
  let watchListFormDialogs: any[] = [];
  let startIndex = 0;
  let endIndex = watchlistIds.length - 1;

  for (let i = 0; i < watchlistIds.length; i++) {
    let disabledPrev = i == startIndex ? true : false;
    let disabledNext = i == endIndex ? true : false;
    watchListFormDialogs.push(
      <Layout key={i}>
        <Layout.Child
          childId="ModifyWatchListDialogMainPanel"
          className="form padding--left"
        >
          {startIndex !== endIndex && (
            <Layout isColumnType>
              <Layout.Child
                childId="ModifyWatchListDialogPrevNextButtons"
              >
                <>
                  <button className="button--tertiary" style={{ float: 'left', width: '15%', height: '8%' }} onClick={WatchListStore.onPrevClick} disabled={disabledPrev}>&lt; Previous</button>
                  <button className="button--tertiary" style={{ float: 'right', width: '15%', height: '8%' }} onClick={WatchListStore.onNextClick} disabled={disabledNext}>Next &gt;</button>
                </>
              </Layout.Child>
            </Layout>
          )}
          <WatchListForm
            onChange={WatchListStore.onChange}
            handleMessageChange={WatchListStore.handleMessageChange}
            watchListPopUpData={WatchListStore.watchListPopUpData}
          />
        </Layout.Child>
      </Layout>
    )
  }

  const render = () => {
    return (
      <>
        <Dialog
          title={WatchListStore.watchlistDialogTitle}
          isOpen={WatchListStore.showModifyWatchListDialog}
          onClose={WatchListStore.onCloseModifyWatchListDialog}
          style={{ width: '38%' }}
          footer={
            <Layout>
              <Layout.Child childId="ModifyWatchListDialogFooterPanel">
                <button className="button--warning" onClick={WatchListStore.onUpdate}>
                  Update
                </button>
                <button className="button--critical" onClick={WatchListStore.onDelete}>
                  Delete
                </button>
                <button className="button--secondary" onClick={WatchListStore.onCloseModifyWatchListDialog}>
                  Cancel
                </button>
              </Layout.Child>
            </Layout>
          }
        >
          {watchListFormDialogs[WatchListStore.modifyFormIndex]}
        </Dialog>
        <Dialog
          title="Confirmation Delete Dialog"
          isOpen={WatchListStore.showConfirmationDeleteDialog}
          onClose={WatchListStore.onCloseConfirmationDialog}
          footer={
            <Layout>
              <Layout.Child childId="DeleteConfirmationFooterDialog">
                <button onClick={WatchListStore.onConfirmDelete}>
                  Delete
                </button>
                <button onClick={WatchListStore.onCloseConfirmationDialog}>
                  Cancel
                </button>
              </Layout.Child>
            </Layout>
          }
        >
          <h3>Do you really want to delete the watchlist?</h3>
        </Dialog>
      </>
    );
  };

  return <>{render()}</>;
};

export default observer(WatchListModifyDialog);
