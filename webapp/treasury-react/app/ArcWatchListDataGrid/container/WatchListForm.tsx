import { DatePicker, Dialog, Layout, TokenSelect, UserComboBox } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import { WatchListFormComponentProps } from "../models/WatchListFormComponentProps";
import WatchListStore from "../store";

const WatchListFormComponent: React.FC<WatchListFormComponentProps> = ({
  onChange,
  handleMessageChange,
  watchListPopUpData,
}) => {


  const fieldStyle = { marginBottom: '10px' };
  const displayElement = (label: string, value: any) => {
    return (
      <Layout isColumnType>
        <Layout.Child
          childId={`${label}-label`}
          size={1}
          className="fieldalign"
        >
          <span style={{ float: 'right', marginRight: '40px' }}>
            {label}
          </span>
        </Layout.Child>
        <Layout.Child
          childId={`${label}-value`}
        >
          {value}
        </Layout.Child>
      </Layout>
    );
  }
  const startDateElement =
    <DatePicker
      placeholder="Start Date"
      value={watchListPopUpData.selectedStartDate}
      onChange={(value) => onChange("selectedStartDate", value)}
      style={fieldStyle}
      renderInBody
    />

  const endDateElement =
    <DatePicker
      placeholder="End Date"
      value={watchListPopUpData.selectedEndDate}
      onChange={(value) => onChange("selectedEndDate", value)}
      style={fieldStyle}
      renderInBody
    />

  const tagsElement =
    <TokenSelect
      placeholder="Select WatchList Tags"
      values={watchListPopUpData.selectedTags}
      onChange={(values) => onChange("selectedTags", values)}
      onSubmit={(newTag) => onChange("selectedTags", [...watchListPopUpData.selectedTags, newTag])}
      options={WatchListStore.tagsList}
      style={fieldStyle}
    />

  const messageElement =
    <textarea
      placeholder="WatchList message"
      value={watchListPopUpData.selectedMessage}
      onChange={handleMessageChange}
      style={{ marginBottom: '10px', height: '60px', width: '-webkit-fill-available' }}
    />

  const observersElement =
    <UserComboBox
      placeholder="Select WatchList Observers"
      value={watchListPopUpData.selectedObservers}
      onChange={(value) => onChange("selectedObservers", value)}
      style={fieldStyle}
      multiSelect
    />

  const expiryNotificationDateElement =
    <DatePicker
      placeholder="Notification Date"
      value={watchListPopUpData.selectedExpiryNotificationDate}
      onChange={(value) => onChange("selectedExpiryNotificationDate", value)}
      style={fieldStyle}
      renderInBody
    />

  const keyColumnsElement =
    <i
      title="View key columns"
      className="icon-view"
      style={{ textAlign: "center", width: "100%", marginBottom: '10px' }}
      onClick={(e) => {
        e.stopPropagation();
        WatchListStore.showKeyColumnsDialog = true
      }}
    >View Key Columns</i>

  const keyColumnsDialog =
    <Dialog
      title="Key Columns"
      isOpen={WatchListStore.showKeyColumnsDialog}
      onClose={WatchListStore.onCloseKeyColumnsDialog}
      footer={
        <Layout>
          <Layout.Child childId="KeyColumnsDialogFooterPanel">
            <button onClick={WatchListStore.onCloseKeyColumnsDialog}>
              Cancel
            </button>
          </Layout.Child>
        </Layout>
      }
    >
      <Layout>
        <Layout.Child
          childId="KeyColumnsDialogMainPanel"
          className="form padding--left"
        >
          {WatchListStore.keyVerbose}
        </Layout.Child>
      </Layout>
    </Dialog>

  return (
    <>
      {keyColumnsElement}
      {keyColumnsDialog}
      <Layout isRowType>
        <Layout.Child
          childId="FormComponentElements"
        >
          {displayElement('Effective Start Date', startDateElement)}
          {displayElement('Effective End Date', endDateElement)}
          {displayElement('Tags', tagsElement)}
          {displayElement('Observers', observersElement)}
          {displayElement('Expiry Notification Date', expiryNotificationDateElement)}
          {displayElement('Message', messageElement)}
        </Layout.Child>
      </Layout>
    </>
  );
};

export default observer(WatchListFormComponent);
