import { Dialog, Layout } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import WatchListStore from "../store";
import WatchListForm from "./WatchListForm";

const WatchListDetailDialog: React.FC = () => {

  const render = () => {
    return (
      <>
        <Dialog
          title={WatchListStore.watchlistDialogTitle}
          isOpen={WatchListStore.showAddWatchListDialog}
          onClose={WatchListStore.onCloseAddWatchListDialog}
          style={{ width: '38%' }}
          footer={
            <Layout>
              <Layout.Child childId="AddWatchListFooterPanel">
                <button className="button--primary" onClick={WatchListStore.onSave}>
                  Save
                </button>
                <button className="button--secondary" onClick={WatchListStore.onCloseAddWatchListDialog}>
                  Cancel
                </button>
              </Layout.Child>
            </Layout>
          }
        >
          <Layout>
            <Layout.Child
              childId="AddWatchListHeaderPanel"
              className="form padding--left"
            >
              <WatchListForm
                onChange={WatchListStore.onChange}
                handleMessageChange={WatchListStore.handleMessageChange}
                watchListPopUpData={WatchListStore.watchListPopUpData}
              />
            </Layout.Child>
          </Layout>
        </Dialog>
      </>
    );
  };

  return <>{render()}</>;
};

export default observer(WatchListDetailDialog);
