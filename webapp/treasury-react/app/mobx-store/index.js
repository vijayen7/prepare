import { configure } from "mobx";
import CashManagementThresholdsStore from "../CashManagementThresholds/CashManagementThresholdsStore";
import BrokerDataStore from "../BrokerData/BrokerDataStore";
import CollateralTermStore from "../CollateralTerms/CollateralTermStore";
import RagRuleDataStore from "../RagRuleData/RagRuleDataStore";
import PhysicalCollateralStore from "../PhysicalCollateralRules/PhysicalCollateralRulesStore";
import AddPhysicalCollateralStore from "../PhysicalCollateralRules/containers/PhysicalCollateralRulesDialog/AddPhysicalCollateralRuleStore";
import EditPhysicalCollateralStore from "../PhysicalCollateralRules/containers/PhysicalCollateralRulesDialog/EditPhysicalCollateralRuleStore";
import EntityLiquidityDataStore from "../EntityLiquidityData/EntityLiquidityDataStore";

configure({ enforceActions: "never" });
const physicalCollateralStore = new PhysicalCollateralStore();

export default {
  cashManagementThresholdsStore: new CashManagementThresholdsStore(),
  brokerDataStore: new BrokerDataStore(),
  collateralTermStore: new CollateralTermStore(),
  ragRuleDataStore: new RagRuleDataStore(),
  physicalCollateralStore: physicalCollateralStore,
  addPhysicalCollateralRuleStore: new AddPhysicalCollateralStore(physicalCollateralStore),
  editPhysicalCollateralRuleStore: new EditPhysicalCollateralStore(physicalCollateralStore),
  entityLiquidityDataStore: new EntityLiquidityDataStore()
};
