import {
  percentFormatter,
  drillThroughFormatter,
  defaultNumberToZeroFormatter
} from "commons/grid/formatters";
import {
  sumIgnoringUndefinedAggregator
} from "commons/grid/aggregators";
import { numberComparator } from "commons/grid/comparators";

var columns = [];

export function gridColumns(params) {
  var columns = [
    {
      id: "cpeFamily",
      name: "Counterparty Entity Family",
      field: "cpeFamily",
      toolTip: "Counterparty Entity Family",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 100,
      excelDataFormatter: function(data) {
        return data["value"];
      },
      comparator: function(a, b) {
        return numberComparator(a[sortcol]["value"], b[sortcol]["value"]);
      },
      formatter: function(row, cell, value, columnDef, dataContext) {
        return drillThroughFormatter(
          row,
          cell,
          value["value"],
          columnDef,
          dataContext,
          undefined,
          undefined,
          value["params"]
        );
      }
    },
    {
      id: "clearingFeeUSD",
      name: "Clearing Fee",
      field: "clearingFeeUSD",
      toolTip: "Clearing Fee USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "ticketChargesUSD",
      name: "Ticket Charges",
      field: "ticketChargesUSD",
      toolTip: "Ticket Charges USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "explicitCommissionUSD",
      name: "Explicit Commission",
      field: "explicitCommissionUSD",
      toolTip: "Explicit Commission USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "implicitCommissionUSD",
      name: "Implicit Commission",
      field: "implicitCommissionUSD",
      toolTip: "Implicit Commission USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "bankChargeUSD",
      name: "Bank Charge",
      field: "bankChargeUSD",
      toolTip: "Bank Charge USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "custodyFeeUSD",
      name: "Custody Fee",
      field: "custodyFeeUSD",
      toolTip: "Custody Fee USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "totalCommissionUSD",
      name: "Total Commission",
      field: "totalCommissionUSD",
      toolTip: "Total Commission USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      minWidth: 70
    },
    {
      id: "trsFinancingUSD",
      name: "TRS Financing",
      field: "trsFinancingUSD",
      toolTip: "TRS Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "repoFinancingUSD",
      name: "Repo Financing",
      field: "repoFinancingUSD",
      toolTip: "Repo Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "pbFinancingUSD",
      name: "PB Financing",
      field: "pbFinancingUSD",
      toolTip: "PB Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "slaFinancingUSD",
      name: "SLA Financing",
      field: "slaFinancingUSD",
      toolTip: "SLA Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "focFinancingUSD",
      name: "FOC Financing",
      field: "focFinancingUSD",
      toolTip: "FOC Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "totalFinancingUSD",
      name: "Total Financing",
      field: "totalFinancingUSD",
      toolTip: "Total Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "adjustedPbFinancing",
      name: "Adjusted PB Financing (Gross)",
      field: "adjustedPbFinancing",
      toolTip: "Adjusted PB Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "adjustedSlaFinancing",
      name: "Adjusted SLA Financing (Gross)",
      field: "adjustedSlaFinancing",
      toolTip: "Adjusted SLA Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "adjustedFuturesFinancing",
      name: "Adjusted Futures Financing (Gross)",
      field: "adjustedFuturesFinancing",
      toolTip: "Adjusted Futures Financing",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "grossSwapFinancingDTD",
      name: "Swap Financing DTD (Gross)",
      field: "grossSwapFinancingDTD",
      toolTip: "Swap Financing DTD (Gross)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "grossSwapFinancingMTD",
      name: "Swap Financing MTD (Gross)",
      field: "grossSwapFinancingMTD",
      toolTip: "Swap Financing MTD (Gross)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "grossSwapFinancingQTD",
      name: "Swap Financing QTD (Gross)",
      field: "grossSwapFinancingQTD",
      toolTip: "Swap Financing QTD (Gross)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "grossRepoFinancingDTD",
      name: "Repo Financing DTD (Gross)",
      field: "grossRepoFinancingDTD",
      toolTip: "Repo Financing DTD (Gross)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "grossRepoFinancingMTD",
      name: "Repo Financing MTD (Gross)",
      field: "grossRepoFinancingMTD",
      toolTip: "Repo Financing MTD (Gross)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "grossRepoFinancingQTD",
      name: "Repo Financing QTD (Gross)",
      field: "grossRepoFinancingQTD",
      toolTip: "Repo Financing QTD (Gross)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      aggregator: sumIgnoringUndefinedAggregator,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    }
  ];

  return columns;
}
