import { url } from "../api";
export function gridOptions() {
  var clearingFeeUSDColumnId = "clearingFeeUSD";
  var totalCommissionUSDColumnId = "totalCommissionUSD";
  var trsFinancingUSDColumnId = "trsFinancingUSD";
  var totalFinancingUSDColumnId = "totalFinancingUSD";
  var adjustedPbFinancingColumnId = "adjustedPbFinancing";
  var grossRepoFinancingColumnId = "grossRepoFinancingQTD";

  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=brokerRevenueSummaryList";
      }
    },
    forceFitColumns: true,
    applyFilteringOnGrid: true,
    expandTillLevel: 1,
    headerHeight: 100,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    page: true,
    sheetName: "Broker Revenue Summary",
    sortList: [{ columnId: "cpeFamily", sortAsc: true }],
    groupColumns: [
      ["Commission", clearingFeeUSDColumnId, totalCommissionUSDColumnId],
      ["Spread Only Financing (USD)", trsFinancingUSDColumnId, totalFinancingUSDColumnId],
      ["Gross Financing (USD)", adjustedPbFinancingColumnId, grossRepoFinancingColumnId]
    ]
  };
  return options;
}
