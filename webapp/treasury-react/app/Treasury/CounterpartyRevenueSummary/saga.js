import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_COUNTERPARTY_REVENUE_SUMMARY_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";


import { getCounterpartyRevenueSummaryData } from "./api";

function* fetchCounterpartyRevenueSummaryData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCounterpartyRevenueSummaryData, action.payload);
    yield put({ type: `${FETCH_COUNTERPARTY_REVENUE_SUMMARY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_COUNTERPARTY_REVENUE_SUMMARY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* counterpartyRevenueSummaryDataSaga() {
  yield [
    takeEvery(FETCH_COUNTERPARTY_REVENUE_SUMMARY_DATA, fetchCounterpartyRevenueSummaryData)
  ];
}
