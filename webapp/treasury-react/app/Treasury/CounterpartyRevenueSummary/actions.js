import {
  BASE_URL,
  FETCH_COUNTERPARTY_REVENUE_SUMMARY_DATA,
  DESTROY_DATA_STATE_FOR_COUNTERPARTY_REVENUE_SUMMARY_VIEW
} from "commons/constants";

export function fetchCounterpartyRevenueSummary(payload) {
  return {
    type: FETCH_COUNTERPARTY_REVENUE_SUMMARY_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_COUNTERPARTY_REVENUE_SUMMARY_VIEW,
  };
}
