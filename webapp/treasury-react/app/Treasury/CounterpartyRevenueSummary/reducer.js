import {
  FETCH_COUNTERPARTY_REVENUE_SUMMARY_DATA,
  DESTROY_DATA_STATE_FOR_COUNTERPARTY_REVENUE_SUMMARY_VIEW
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function counterpartyRevenueSummaryDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_COUNTERPARTY_REVENUE_SUMMARY_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_COUNTERPARTY_REVENUE_SUMMARY_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: counterpartyRevenueSummaryDataReducer
});

export default rootReducer;
