import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";
import FinancingDetailGridContainer from "../FinancingDetail/containers/Grid";
import CommissionDetailGridContainer from "../CommissionDetail/containers/Grid";

import {
  COUNTERPARTY_REVENUE_SUMMARY
} from "../constants";

const CounterpartyRevenueSummary = (props) =>
 {
  if(props.selectedView != COUNTERPARTY_REVENUE_SUMMARY) {
    props.updateSelectedView(COUNTERPARTY_REVENUE_SUMMARY);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
            <br />
            <FinancingDetailGridContainer drillDownGrid = {true}/>
            <br />
            <CommissionDetailGridContainer drillDownGrid = {true}/>
          </div>
          <br />
          <div>
            <ul className="bullet">
              <li>All figures in USD</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CounterpartyRevenueSummary;
