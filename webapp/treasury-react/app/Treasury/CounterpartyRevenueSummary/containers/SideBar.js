import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import Error from "commons/components/Error";
import FilterButton from "commons/components/FilterButton";
import CheckboxFilter from "commons/components/CheckboxFilter";
import ExecBrokerFamiliesFilter from "commons/container/ExecBrokerFamiliesFilter";
import BULevelFilter from "commons/container/BULevelFilter";
import BundleGroupTypeFilter from "commons/container/BundleGroupTypeFilter";
import BundleGroupFilter from "commons/container/BundleGroupFilter";
import { fetchCounterpartyRevenueSummary } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import { destroyDataState as financingDetailDestroyDataState } from "../../FinancingDetail/actions";
import { destroyDataState as commissionDetailDestroyDataState } from "../../CommissionDetail/actions";
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  getDefaultFilters() {
    return {
      selectedExecBrokerFamilies: [],
      selectedBULevel: { key: "BU", value: "Business Unit" },
      selectedBundleGroupTypes: { key: "-2", value: "" },
      selectedBundleGroups: [],
      selectedStartDate: getCodexFilters("DATE"),
      selectedEndDate: getCodexFilters("DATE"),
      includeDesri: false
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    this.props.financingDetailDestroyDataState();
    this.props.commissionDetailDestroyDataState();
    var payload = {
      startDateString: this.state.selectedStartDate,
      endDateString: this.state.selectedEndDate,
      cpeFamilyIds: getCommaSeparatedValues(this.state.selectedExecBrokerFamilies),
      bundleGroupIds: getCommaSeparatedValues(this.state.selectedBundleGroups),
      buLevel: this.state.selectedBULevel.key,
      bundleGroupTypeId: this.state.selectedBundleGroupTypes.key,
      includeDesri: this.state.includeDesri
    };

    this.props.updateDrillDownSelectedFilters(this.state);
    this.props.fetchCounterpartyRevenueSummary(payload);
  }

  render() {
    const buLevel = this.state.selectedBULevel.key;
    let bundleGroupFilters = null;
    let includeDesriFilter = null;
    if (buLevel === "BUNDLE_GROUP") {
      bundleGroupFilters = (
        <div>
          <BundleGroupTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedBundleGroupTypes}
            multiSelect={false}
          />
          <BundleGroupFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedBundleGroups}
          />
        </div>
      );
    } else {
      let isBankChargeEnabled = JSON.parse(CODEX_PROPERTIES['treasury.portal.financing.enableBankChargeUpload']);
      if(isBankChargeEnabled){
        includeDesriFilter = (
          <CheckboxFilter
            onSelect={this.onSelect}
            stateKey="includeDesri"
            label="Include Desri"
            style="left"
            key="1"
          />
        );
      }
    }

    let dateError = null;
    if (this.state.selectedStartDate > this.state.selectedEndDate) {
      dateError = <Error messageData="Start date is greater than End date" />;
    } else {
      dateError = null;
    }

    return (
      <Sidebar>
        {dateError}
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedStartDate"
          data={this.state.selectedStartDate}
          label="Start Date"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedEndDate"
          data={this.state.selectedEndDate}
          label="End Date"
        />
        <ExecBrokerFamiliesFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedExecBrokerFamilies}
          label="CPE Families"
        />
        <BULevelFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBULevel}
        />
        {bundleGroupFilters}
        {includeDesriFilter}
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            disabled={dateError !== null}
            reset={false}
            label="Search"
          />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCounterpartyRevenueSummary,
      updateDrillDownSelectedFilters,
      financingDetailDestroyDataState,
      commissionDetailDestroyDataState
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
