import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CounterpartyRevenueSummaryGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";
import { fetchFinancingDetail } from "../../FinancingDetail/actions";
import { fetchCommissionDetail } from "../../CommissionDetail/actions";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  onCellClickHandler = args => {
    if (args.colId === "cpeFamily") {
      var cpeFamilyName = args.item.cpeFamily.value;
      var cpeFamilyIds = args.item.cpeFamily.params
        .substring(19, args.item.cpeFamily.params.length - 2)
        .split(",");
      var selectedCpeFamilies = [];
      for (var i in cpeFamilyIds) {
        selectedCpeFamilies.push({
          key: parseInt(cpeFamilyIds[i]),
          value: cpeFamilyName
        });
      }
      var selectedExecBrokerFamilies = selectedCpeFamilies;
      var selectedFilterValues = Object.assign({}, this.props.selectedFilters);
      selectedFilterValues.selectedCpeFamilies = selectedCpeFamilies;
      selectedFilterValues.selectedExecBrokerFamilies = selectedExecBrokerFamilies;

      var financingDetailPayload = {
        startDateString: selectedFilterValues.selectedStartDate,
        endDateString: selectedFilterValues.selectedEndDate,
        cpeFamilyIds: getCommaSeparatedValues(
          selectedFilterValues.selectedCpeFamilies
        ),
        cpeIds: "-1",
        businessUnitIds: "-1",
        agreementTypeIds: "-1",
        descoEntityIds: "-1",
        bundleGroupIds: getCommaSeparatedValues(
          selectedFilterValues.selectedBundleGroups
        ),
        buLevel: selectedFilterValues.selectedBULevel.key,
        bundleGroupTypeId: selectedFilterValues.selectedBundleGroupTypes.key
      };

      var commissionDetailPayload = {
        startDateString: selectedFilterValues.selectedStartDate,
        endDateString: selectedFilterValues.selectedEndDate,
        businessUnitIds: "-1",
        execBrokerFamilyIds: getCommaSeparatedValues(
          selectedFilterValues.selectedExecBrokerFamilies
        ),
        execBrokerIds: "-1",
        gboTypeIds: "-1",
        descoEntityIds: "-1",
        bundleGroupIds: getCommaSeparatedValues(
          selectedFilterValues.selectedBundleGroups
        ),
        buLevel: selectedFilterValues.selectedBULevel.key,
        bundleGroupTypeId: selectedFilterValues.selectedBundleGroupTypes.key
      };

      this.props.fetchFinancingDetail(financingDetailPayload);
      this.props.fetchCommissionDetail(commissionDetailPayload);
    }
  };

  renderGridData() {
    let grid = null;
    if (
      !this.props.counterpartyRevenueSummaryData.hasOwnProperty(
        "brokerRevenueSummaryList"
      ) ||
      this.props.counterpartyRevenueSummaryData.brokerRevenueSummaryList
        .length <= 0
    )
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <CounterpartyRevenueSummaryGrid
        data={
          this.props.counterpartyRevenueSummaryData.brokerRevenueSummaryList
        }
        onCellClick={this.onCellClickHandler}
        gridId="counterpartyRevenueSummaryData"
        gridColumns={gridColumns()}
        gridOptions={gridOptions()}
        label = "Counterparty Revenue Summary"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchFinancingDetail,
      fetchCommissionDetail,
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    counterpartyRevenueSummaryData:
      state.treasury.counterpartyRevenueSummaryView.data,
    selectedFilters: state.treasury.drillDownFilters
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
