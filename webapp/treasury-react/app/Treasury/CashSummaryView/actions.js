import {BASE_URL, FETCH_CASH_SUMMARY_DATA} from 'commons/constants'

export function fetchCashSummaryData(payload) {
  return {
    type: FETCH_CASH_SUMMARY_DATA,
    payload
  };
}
