import React, { Component } from "react";
import SideBar from "./SideBar";
import Message from "commons/components/Message";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { fetchCashSummaryOwnershipViewData } from "../actions";
import { ReactArcGrid } from "arc-grid";
import { connect } from "react-redux";
import Grid from "commons/components/Grid";

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.renderGridData = this.renderGridData.bind(this);
  }

  renderGridData() {
    let grid = null;
    if (this.props.cashSummaryData.length <= 0)
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <Grid
        gridId="cashSummary"
        data={this.props.cashSummaryData}
        gridColumns={gridColumns(this.props.type)}
        gridOptions={gridOptions()}
        label = "Cash Summary"
      />
    );
    return grid;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.type !== this.props.type) nextProps.cashSummaryData = [];
  }

  render() {
    return this.renderGridData();
  }
}

function mapStateToProps(state) {
  return { cashSummaryData: state.treasury.cashSummaryView.data };
}

export default connect(mapStateToProps)(Dashboard);
