import React, { Component } from "react";
import Date from "commons/components/Date";
import { getPreviousDate } from "commons/util";
import { fetchCashSummaryData } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.buttonClickHandler = this.buttonClickHandler.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);

    this.state = {
      selectedDate: ""
    };
  }

  componentDidMount() {
    this.buttonClickHandler();
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  buttonClickHandler() {
    var payload = {
      date: this.state.selectedDate,
      type: this.props.type
    };
    this.props.fetchCashSummaryData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
        />
        <ColumnLayout>
          <FilterButton onClick={this.buttonClickHandler} label="Search" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchCashSummaryData }, dispatch);
}

export default connect(null, mapDispatchToProps)(SideBar);
