import {percentFormatter} from "commons/grid/formatters";

export function gridColumns(type) {
  var columns = [
    {
      id: "liquidityUsd",
      name: "Liquidity (USD)",
      field: "liquidityUsd",
      toolTip: "Liquidity refers to unencumbered cash and cash equivalents.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "liquidityPercentage",
      name: "Liquidity (NAV) (%)",
      field: "liquidityPercentage",
      toolTip:
        "Liquidity (NAV) reflects available liquidity as a percentage of total Net Asset Value of the fund.",
      type: "number",
      sortable: true,
      formatter: percentFormatter,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "illiquidLmvUsd",
      name: "LMV of Illiquids (USD)",
      field: "illiquidLmvUsd",
      toolTip:
        "LMV of Illiquids (USD) represents long market value of all positions that typically have a low liquidity in the market. (Examples include non-G10 bonds, Asset Backed Securities and private common stocks.)",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "crashRisk",
      name: "Crash Risk",
      field: "crashRisk",
      toolTip:
        "Crash Risk refers to exposure to 25% drop in major equity indices with corresponding shock to equity volatility and credit spreads based on in-house models. This data is sourced from Risk.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b"
    },
    {
      id: "valueAtRisk",
      name: "VAR",
      field: "valueAtRisk",
      toolTip:
        "Value at Risk is calculated using Risk Metrics. This number is calculated at the 84% confidence interval using Monte Carlo Simulations. This data is sourced from Risk.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "sortOrder",
      name: "",
      type: "number",
      field: "sortOrder",
      sortable: true,
      width: 1,
      maxWidth: 1,
      minWidth: 1,
      unselectable: true,
      formatter: function(row, cell, val, columnDef, dataContext) {
        "<div style='display: none;'>#{val}</div>";
      }
    }
  ];
  if (type === "OWNERSHIP") {
    columns.unshift({
      id: "entityName",
      name: "Ownership",
      field: "entityName",
      toolTip: "Ownership",
      type: "text",
      sortable: true,
      cssClass: "b",
      headerCssClass: "b"
    });
  } else {
    columns.unshift({
      id: "entityName",
      name: "Trading Entity",
      field: "entityName",
      toolTip: "Trading Entity",
      type: "text",
      sortable: true,
      cssClass: "b",
      headerCssClass: "b"
    });
  }

  return columns;
}
