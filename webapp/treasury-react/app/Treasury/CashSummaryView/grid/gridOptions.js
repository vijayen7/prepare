export function gridOptions() {
  return {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    },
    highlightRowOnClick: true,
    autoHorizontalScrollBar: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    sortList: [{ columnId: "sortOrder", sortAsc: true }],
    exportToExcel: true,
    forceFitColumnsProportionately: true,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    page: true,
    sheetName: "Cash Summary"
  };
}
