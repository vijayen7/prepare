import { put, takeEvery, call } from "redux-saga/effects";
import { FETCH_CASH_SUMMARY_DATA, START_LOADING, END_LOADING } from "commons/constants";

import { getCashSummaryData } from "./api";

function* fetchCashSummaryData(action) {
  try {
    yield put({type : START_LOADING});
    const data = yield call(getCashSummaryData, action.payload);
    yield put({ type: `${FETCH_CASH_SUMMARY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: `${FETCH_CASH_SUMMARY_DATA}_FAILURE` });
  } finally {
    yield put({type : END_LOADING});
  }
}

export function* cashSummaryRootSaga() {
  yield takeEvery(FETCH_CASH_SUMMARY_DATA, fetchCashSummaryData);
}
