import { BASE_URL } from "commons/constants";
export let url = "";
export function getCashSummaryData(payload) {
  url = `${BASE_URL}data/search-cash-summary?dateString=${
    payload.date
  }&entityType=${payload.type}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
