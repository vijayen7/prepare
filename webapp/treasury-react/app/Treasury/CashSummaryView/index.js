import React, { Component } from "react";
import Grid from "./containers/Grid";
import SideBar from "./containers/SideBar";
import Loader from "commons/container/Loader";

import {
  POSITION_DETAIL_VIEW
} from "../constants";

export default class CashSummaryOwnershipView extends Component {

  render() {
    if(this.props.selectedView != POSITION_DETAIL_VIEW) {
      props.updateSelectedView(POSITION_DETAIL_VIEW);
     }

    return (
      <div className="layout--flex">
        <div>
          <SideBar type={this.props.type} applicationName={props.gadgetName} />
        </div>
        <div className="layout--flex--row size--5 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid type={this.props.type} />
          </div>
          <br />
          <ul className="bullet">
            <li>All figures in USD</li>
          </ul>
        </div>
      </div>
    );
  }
}
