import { FETCH_CASH_SUMMARY_DATA } from "commons/constants";
import { combineReducers } from "redux";

function cashSummaryDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_CASH_SUMMARY_DATA}_SUCCESS`:
      return action.data.cashSummaryList || [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: cashSummaryDataReducer
});

export default rootReducer;
