import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
import { fetchURL } from "commons/util";
export let genericBuDataUrl = "";
export function getGenericBuData(payload) {
  const paramString = queryString.stringify(payload);
  genericBuDataUrl = `${BASE_URL}data/search-generic-bu-detail?${paramString}`;
  console.log(genericBuDataUrl);
  return fetchURL(genericBuDataUrl);
}

export let genericBuSummaryDataUrl = "";
export function getGenericBuSummaryData(payload) {
  const paramString = queryString.stringify(payload);
  genericBuSummaryDataUrl = `${BASE_URL}data/search-broker-revenue-ytd-summary?${paramString}`;
  return fetchURL(genericBuSummaryDataUrl);
}
