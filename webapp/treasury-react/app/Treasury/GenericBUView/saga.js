import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  START_LOADING,
  END_LOADING,
  FETCH_GENERIC_BU_DATA
} from "commons/constants";

import { getGenericBuData, getGenericBuSummaryData } from "./api";

function* fetchGenericBuData(action) {
  try {
    yield put({ type: START_LOADING });
    const [detailData, summaryData] = yield all([
      call(getGenericBuData, action.payload),
      call(getGenericBuSummaryData, action.payload)
    ]);
    yield put({
      type: `${FETCH_GENERIC_BU_DATA}_SUCCESS`,
      summaryData,
      detailData
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_GENERIC_BU_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* genericBuRootSaga() {
  yield [takeEvery(FETCH_GENERIC_BU_DATA, fetchGenericBuData)];
}
