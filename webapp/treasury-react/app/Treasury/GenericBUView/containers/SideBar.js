import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LegalEntityFamilyFilter from "commons/container/LegalEntityFamilyFilter";
import CpeFamilyFilter from "commons/container/CpeFamilyFilter";
import CounterpartyRevenueCheckboxFilters from "commons/components/CounterpartyRevenueCheckboxFilters";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import BrokerRevenueYearFilter from "commons/container/BrokerRevenueYearFilter";
import BusinessUnitGroupFilter from "commons/container/BusinessUnitGroupFilter";
import BundleGroupFilter from "commons/container/BundleGroupFilter";
import BundleGroupTypeFilter from "commons/container/BundleGroupTypeFilter";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import BrokerRevenueBusinessUnitFilter from "commons/container/BrokerRevenueBusinessUnitFilter";
import InputFilter from "commons/components/InputFilter";
import { getCommaSeparatedValues } from "commons/util";
import { fetchGenericBuData } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  componentDidMount() {
    if (Object.keys(getCodexFilters("YEAR")[0]).length == 0) {
      if (this.props.years.length > 0) {
        this.setState({
          selectedYear: this.props.years[0]
        });
      }
      if (this.props.businessUnitGroups.length > 0) {
        if(this.props.businessUnitGroups.length == 1){
          this.setState({
            selectedBusinessUnitGroups: this.props.businessUnitGroups[0]
          });
        }else{
          this.setState({
            selectedBusinessUnitGroups: this.props.businessUnitGroups[1]
          });
        }
      }
    }
    if (this.props.bundleGroups.length > 0) {
      this.setState({
        selectedBundleGroups: this.props.bundleGroups[0]
      });
    }
    if (this.props.businessUnits.length > 0) {
      this.setState({
        selectedBrokerRevenueBusinessUnits: this.props.businessUnits[0]
      });
    }

    if (this.props.bundleGroupTypes.length > 0) {
      this.setState({
        selectedBundleGroupTypes: this.props.bundleGroupTypes[0]
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (Object.keys(getCodexFilters("YEAR")[0]).length == 0) {
      if (nextProps.years.length > 0) {
        this.setState({
          selectedYear: nextProps.years[0]
        });
      }

      if (nextProps.businessUnitGroups.length > 0) {
        if(nextProps.businessUnitGroups.length == 1){
          this.setState({
            selectedBusinessUnitGroups: nextProps.businessUnitGroups[0]
          });
        }else{
          this.setState({
            selectedBusinessUnitGroups: nextProps.businessUnitGroups[1]
         });
        }
      }
    }
    if (nextProps.bundleGroups.length > 0) {
      this.setState({
        selectedBundleGroups: nextProps.bundleGroups[0]
      });
    }
    if (nextProps.businessUnits.length > 0) {
      this.setState({
        selectedBrokerRevenueBusinessUnits: nextProps.businessUnits[0]
      });
    }

    if (nextProps.bundleGroupTypes.length > 0) {
      this.setState({
        selectedBundleGroupTypes: nextProps.bundleGroupTypes[0]
      });
    }
  }

  getDefaultFilters() {
    return {
      selectedLegalEntityFamilies: getCodexFilters("LEGAL_ENTITY_FAMILIES"),
      selectedYear: getCodexFilters("YEAR")[0],
      showAnnualized: true,
      explicitCommission: true,
      implicitCommission: true,
      financing: true,
      clearingFee: true,
      ticketCharges: true,
      bankCharge: true,
      custodyFee: true,
      groupFilterData: [
        { key: "BU_GROUP", value: "Business Unit Group" },
        { key: "BU", value: "Business Unit" },
        { key: "BUNDLE_GROUP", value: "Bundle Group" }
      ],
      selectedGroupFilter: { key: "BU_GROUP", value: "Business Unit Group" },
      selectedBusinessUnitGroups: getCodexFilters(
        "BUSINESS_UNIT_GROUP_BROKER_REVENUE"
      )[0],
      selectedBundleGroupTypes: [],
      selectedBundleGroups: [],
      selectedBrokerRevenueBusinessUnits: [],
      topNCpes: "10"
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
    this.setState({
      selectedBrokerRevenueBusinessUnits: this.props.businessUnits[0]
    });
    if (Object.keys(getCodexFilters("YEAR")[0]).length == 0) {
      this.setState({ selectedYear: this.props.years[0] });

      if(this.props.businessUnitGroups.length == 1 ){
        this.setState({
          selectedBusinessUnitGroups: this.props.businessUnitGroups[0]
        });
      }else{
        this.setState({
          selectedBusinessUnitGroups: this.props.businessUnitGroups[1]
        });
      }

    }
    this.setState({ selectedBundleGroups: this.props.bundleGroups[0] });
    this.setState({
      selectedBundleGroupTypes: this.props.bundleGroupTypes[0]
    });
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    var payload = {
      year: this.state.selectedYear.key,
      descoEntityFamilyIds: getCommaSeparatedValues(
        this.state.selectedLegalEntityFamilies
      ),
      showAnnualized: this.state.showAnnualized,
      includeExplicitCommission: this.state.explicitCommission,
      includeImplicitCommission: this.state.implicitCommission,
      includeFinancing: this.state.financing,
      includeClearingFee: this.state.clearingFee,
      includeTicketCharges: this.state.ticketCharges,
      includeBankCharge: this.state.bankCharge,
      includeCustodyFee: this.state.custodyFee,
      entityType: "BU",
      buLevel: this.state.selectedGroupFilter.key,
      buGroupSelectedId: this.state.selectedBusinessUnitGroups.key || -1,
      buSelectedId: this.state.selectedBrokerRevenueBusinessUnits.key || -1,
      bundleGroupSelectedId: this.state.selectedBundleGroups.key || -1,
      topN: this.state.topNCpes,
      bundleGroupTypeId: this.state.selectedBundleGroupTypes.key,
      bundleGroupId: this.state.selectedBundleGroups.key || -1
    };
    if (this.state.selectedGroupFilter.value === "Business Unit Group") {
      payload.dataEntityId = payload.buGroupSelectedId;
      payload.bundleGroupTypeId = -2;
      payload.bundleGroupId = -1;
    }
    if (this.state.selectedGroupFilter.value === "Business Unit") {
      payload.dataEntityId = payload.buSelectedId;
      payload.bundleGroupTypeId = -2;
      payload.bundleGroupId = -1;
    }
    if (this.state.selectedGroupFilter.value === "Bundle Group") {
      payload.dataEntityId = -1;
      payload.bundleGroupId = this.state.selectedBundleGroups.key;
      payload.bundleGroupTypeId = this.state.selectedBundleGroupTypes.key || -2;
    }
    this.props.updateDrillDownSelectedFilters(this.state);
    this.props.fetchGenericBuData(payload);
  }

  loadConditionalFilters() {
    if (this.state.selectedGroupFilter.value === "Business Unit Group") {
      return (
        <BusinessUnitGroupFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnitGroups}
          multiSelect={false}
        />
      );
    } else if (this.state.selectedGroupFilter.value === "Bundle Group") {
      return [
        <BundleGroupTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBundleGroupTypes}
          multiSelect={false}
          key="bundleGroupType"
        />,
        <BundleGroupFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBundleGroups}
          multiSelect={false}
          key="bundleGroup"
        />
      ];
    } else {
      return (
        <BrokerRevenueBusinessUnitFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBrokerRevenueBusinessUnits}
          multiSelect={false}
        />
      );
    }
  }

  render() {
    return (
      <Sidebar>
        <BrokerRevenueYearFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedYear}
        />
        <LegalEntityFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntityFamilies}
        />
        <SingleSelectFilter
          data={this.state.groupFilterData}
          onSelect={this.onSelect}
          selectedData={this.state.selectedGroupFilter}
          stateKey="selectedGroupFilter"
          label="Group"
        />
        {this.loadConditionalFilters()}
        <CounterpartyRevenueCheckboxFilters
          state={this.state}
          onSelect={this.onSelect}
        />
        <InputFilter
          data={this.state.topNCpes}
          onSelect={this.onSelect}
          stateKey="topNCpes"
          label="Top N CPEs"
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />

          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchGenericBuData,
      updateDrillDownSelectedFilters
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    businessUnits: state.filters.brokerRevenueBusinessUnits,
    years: state.filters.brokerRevenueYears,
    businessUnitGroups: state.filters.businessUnitGroups,
    bundleGroups: state.filters.bundleGroups,
    bundleGroupTypes: state.filters.bundleGroupTypes
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
