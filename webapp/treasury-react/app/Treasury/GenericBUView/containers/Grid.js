import React, { Component } from "react";
import { summaryGridColumns } from "../grid/SummaryGridColumns";
import { summaryGridOptions } from "../grid/SummaryGridOptions";
import { detailGridColumns } from "../grid/DetailGridColumns";
import { detailGridOptions } from "../grid/DetailGridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";
import GenericBuGrid from "commons/components/Grid";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.filterTopNCpes = this.filterTopNCpes.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  getYear(date) {
    return date.substring(0, 4);
  }

  filterTopNCpes() {
    if (!this.props.data.detailData.hasOwnProperty("resultList")) {
      return [];
    }
    var resultList = this.props.data.detailData.resultList;
    var columns = [
      "cpeSpendInBUUSD1",
      "cpeSpendInBUUSD2",
      "cpeSpendInBUChange",
      "marketShareInBU1",
      "marketShareInBU2",
      "marketShareInBUChange"
    ];
    var modifiedResultList = [];
    var topN = parseInt(this.props.selectedFilters.topNCpes);
    if (resultList.length > topN) {
      for (var i = 0; i < topN; i++) {
        modifiedResultList.push(resultList[i]);
      }
      var otherRow = {
        cpeFamily: "Other",
        id: "OtherRow"
      };
      console.log(columns[0]);

      for (var j = 0; j < columns.length; j++) {
        otherRow[columns[j]] = 0;
      }
      for (var i = topN; i < resultList.length; i++) {
        for (var j = 0; j < columns.length; j++) {
          var column = columns[j];

          if (resultList[i][column]) {
            otherRow[column] += resultList[i][column];
          }
        }
      }
      modifiedResultList.push(otherRow);
      return modifiedResultList;
    } else return resultList;
  }

  renderGridData() {
    let grid = null;

    if (
      !this.props.data.hasOwnProperty("summaryData") ||
      !this.props.data.summaryData.hasOwnProperty("resultList") ||
      !this.props.data.summaryData.resultList.length > 0 ||
      this.props.data.summaryData.resultList[0].rows <= 0
    ) {
      return (
        <Message messageData="No data available for this search criteria." />
      );
    }
    grid = (
      <div>
        <GenericBuGrid
          data={this.props.data.summaryData.resultList[0].rows}
          gridColumns={summaryGridColumns(
            this.getYear(this.props.data.summaryData.metaData.asOfDate),
            this.props.data.summaryData.resultList[0].summary
          )}
          gridOptions={summaryGridOptions(
            summaryGridColumns(
              this.getYear(this.props.data.summaryData.metaData.asOfDate),
              this.props.data.summaryData.resultList[0].summary
            )
          )}
          gridId="GenericBUSummary"
          label="Generic BU Summary"
        />
        <GenericBuGrid
          data={this.filterTopNCpes()}
          gridColumns={detailGridColumns(
            this.getYear(this.props.data.detailData.metaData.asOfDate)
          )}
          gridOptions={detailGridOptions(
            detailGridColumns(
              this.getYear(this.props.data.detailData.metaData.asOfDate)
            )
          )}
          gridId="GenericBUDetail"
          label="Generic BU Detail"
        />
        <br />
        <div>
          <small>
            <ul className="bullet">
              <li> All figures in $M USD</li>
              <li>
                The numbers are updated and verified up until
                {this.props.data.detailData.metaData.asOfDate}.
              </li>
              <li>
                The numbers are annualized based on year to date data and may be
                materially higher or lower than prior year if there are expense
                fluctuations in the earlier months of the year.
              </li>
            </ul>
          </small>
        </div>
      </div>
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapStateToProps(state) {
  return {
    data: state.treasury.genericBuView.data,
    selectedFilters: state.treasury.drillDownFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
