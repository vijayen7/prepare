import {
  FETCH_GENERIC_BU_DATA,
  DESTROY_DATA_STATE_GENERIC_BU_VIEW
} from "commons/constants";

export function fetchGenericBuData(payload) {
  return {
    type: FETCH_GENERIC_BU_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_GENERIC_BU_VIEW
  };
}
