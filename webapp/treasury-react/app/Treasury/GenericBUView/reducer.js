import {
  FETCH_GENERIC_BU_DATA,
  DESTROY_DATA_STATE_GENERIC_BU_VIEW
} from "commons/constants";
import { combineReducers } from "redux";

function genericBuDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_GENERIC_BU_DATA}_SUCCESS`:
      return (
        { summaryData: action.summaryData, detailData: action.detailData } || {}
      );
    case DESTROY_DATA_STATE_GENERIC_BU_VIEW:
      return {};
  }
  return state;
}

const rootReducer = combineReducers({
  data: genericBuDataReducer
});

export default rootReducer;
