import { genericBuSummaryDataUrl } from "../api";
export function summaryGridOptions(columns) {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + genericBuSummaryDataUrl+ "&_outputKey=resultList";
      }
    },
    summaryRowText: "Total",
    nestedTable: true,
    nestedField: columns[0].field,
    expandTillLevel: -1,
    expandCollapseAll: true,
    isAutoWidth: true,
     
    summaryRow: true,
    displaySummaryRow: true,
    exportToExcel: true,
    headerTextWrap: false,
    page: true,
    sheetName: "Genercic Bu Summary"
  };
  return options;
}
