import {
  percentFormatter,
  millionFormatter,
  toolTipFormatter
} from "commons/grid/formatters";

export function detailGridColumns(year) {
  var year1 = year;
  var year2 = year - 1;
  var columns = [
    {
      id: "cpeFamily",
      name: "CounterParty Entity Family",
      field: "cpeFamily",
      toolTip: "CounterParty Entity Family",
      sortable: true,
      headerCssClass: "b",
      width: 140
    },
    {
      id: "cpeSpendInBUUSD1",
      name: year1 + " Spend",
      field: "cpeSpendInBUUSD1",
      toolTip:
        year1 +
        " Spend refers to total spend at this broker either on a Year to Date or Annualized basis depending on whether the the 'Show Annualized' check box is ticked.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.00"
    },
    {
      id: "cpeSpendInBUUSD2",
      name: year2 + " Spend",
      field: "cpeSpendInBUUSD2",
      toolTip:
        year2 +
        " Spend refers to the total spend at this broker in the previous year.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      autoWidth: true
    },
    {
      id: "cpeSpendInBUChange",
      name: year1 + "/" + year2 + " Spend Change",
      field: "cpeSpendInBUChange",
      toolTip:
        year1 +
        "/" +
        year2 +
        " Spend Change refers to the change in the spend at this broker year over year  (Current year - Previous year).",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.00"
    },
    {
      id: "marketShareInBU1",
      name: year1 + " Market Share (%)",
      field: "marketShareInBU1",
      toolTip:
        year1 +
        " Market Share (%) refers to the current year total spend for the search criteria at this broker as a percentage of the overall spend across brokers for the search criteria.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: percentFormatter,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      autoWidth: true
    },
    {
      id: "marketShareInBU2",
      name: year2 + " Market Share (%)",
      field: "marketShareInBU2",
      toolTip:
        year2 +
        " Market Share (%) refers to the previous year total spend for the search criteria at this broker as a percentage of the overall spend across brokers for the search criteria.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: percentFormatter,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.0"
    },
    {
      id: "marketShareInBUChange",
      name: year1 + "/" + year2 + " Market Share Change (%)",
      field: "marketShareInBUChange",
      toolTip:
        year1 +
        "/" +
        year2 +
        " Market Share Change (%) refers to the change in the market share at this broker compared to the previous year for the search criteria. A positive number indicates that the current year market share is higher than the previous year.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: percentFormatter,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      autoWidth: true
    },
    {
      id: "rankInBU1",
      name: year1 + " Rank",
      field: "rankInBU1",
      toolTip:
        year1 +
        " Rank refers to the current year rank of this broker when compared to total spend at other brokers for the search criteria. ",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "rankInBU2",
      name: year2 + " Rank",
      field: "rankInBU2",
      toolTip:
        year2 +
        " Rank refers to the previous year rank of this broker when compared to total spend at other brokers for the search criteria. ",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "rankInBUChange",
      name: year1 + "/" + year2 + " Rank Change",
      field: "rankInBUChange",
      toolTip:
        year1 +
        "/" +
        year2 +
        " Rank Change refers to the year over year change in the rank of the broker for the search criteria.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      autoWidth: true
    }
  ];

  return columns;
}
