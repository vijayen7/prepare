import {
  percentFormatter,
  millionFormatter,
  toolTipFormatter,
  numberTotalsFormatter,
  percentTotalsFormatter
} from "commons/grid/formatters";

export function summaryGridColumns(year, params) {
  var year1 = year;
  var year2 = year - 1;
  var columns = [{
      id: "revenueType",
      name: "",
      field: "revenueType",
      toolTip: "",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 140,
      excelDataFormatter: function(data) {
        return data["name"];
      },
      formatter: function(row, cell, value, columnDef, dataContext) {
        return toolTipFormatter(
          row,
          cell,
          value["name"],
          columnDef,
          dataContext,
          value["description"]
        );
      }
    },
    {
      id: "cpeSpend1",
      name: year1 + " Spend",
      field: "cpeSpend1",
      toolTip: year1 +
        " Spend refers to total spend for the search criteria either on a Year to Date or Annualized basis depending on whether the the 'Show Annualized' check box is ticked.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.0"
    },
    {
      id: "cpeSpend2",
      name: year2 + " Spend",
      field: "cpeSpend2",
      toolTip: year2 +
        " Spend refers to the total spend for the search criteria in the previous year.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.00"
    },
    {
      id: "cpeSpendChange",
      name: year1 + "/" + year2 + " Spend Change",
      field: "cpeSpendChange",
      toolTip: year1 +
        "/" +
        year2 +
        " Spend Change refers to the change in the spend for the search criteria year over year  (Current year - Previous year).",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      autoWidth: true
    },
    {
      id: "marketShare1",
      name: year1 + " Market Share (%)",
      field: "marketShare1",
      toolTip: year1 +
        " Market Share (%) refers to the current year total spend for the search criteria as a percentage of the overall spend for the selected legal entities. ",
      type: "number",
      filter: true,
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(row, cell, value, columnDef, dataContext, params["marketShare1"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["0"] === "Total")
          return params["marketShare1"];
        else
          return itemValue;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      autoWidth: true
    },
    {
      id: "marketShare2",
      name: year2 + " Market Share (%)",
      field: "marketShare2",
      toolTip: year2 +
        " Market Share (%) refers to the previous year total spend for the search criteria as a percentage of the overall spend for the selected legal entities. ",
      type: "number",
      filter: true,
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(row, cell, value, columnDef, dataContext, params["marketShare2"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["0"] === "Total")
          return params["marketShare2"];
        else
          return itemValue;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      autoWidth: true
    },
    {
      id: "marketShareChange",
      name: year1 + "/" + year2 + " Market Share Change (%)",
      field: "marketShareChange",
      toolTip: year1 +
        "/" +
        year2 +
        " Market Share Change (%) refers to the percentage change in the Market Share year over year. A positive number indicates that the total spend for the search criteria increased compared to the previous year.",
      type: "number",
      filter: true,
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(row, cell, value, columnDef, dataContext, params["marketShareChange"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["0"] === "Total")
          return params["marketShareChange"];
        else
          return itemValue;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      autoWidth: true
    },
    {
      id: "rank1",
      name: year1 + " Rank",
      field: "rank1",
      toolTip: year1 +
        " Rank refers to the current year rank of this broker in terms of total spend for the Business Unit. Rank is calculated as ($ of Spending at broker) / (Total Counterparty Spending) ranked numerically.",
      type: "number",
      filter: true,
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["rank1"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["0"] === "Total")
          return params["rank1"];
        else
          return itemValue;
      },
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "rank2",
      name: year2 + " Rank",
      field: "rank2",
      toolTip: year2 +
        " Rank refers to the previous year rank of this broker in terms of total spend for the Business Unit.  Rank is calculated as  ($ of Spending at broker) / (Total Counterparty Spending) ranked numerically.",
      type: "number",
      filter: true,
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["rank2"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["0"] === "Total")
          return params["rank2"];
        else
          return itemValue;
      },
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "rankChange",
      name: year1 + "/" + year2 + " Rank Change",
      field: "rankChange",
      toolTip: year1 +
        "/" +
        year2 +
        " Rank Change refers to the year over year change in the rank of the broker for the Business Unit.",
      type: "number",
      filter: true,
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["rankChange"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["0"] === "Total")
          return params["rankChange"];
        else
          return itemValue;
      },
      headerCssClass: "aln-rt b",
      autoWidth: true
    }
  ];

  return columns;
}
