import { genericBuDataUrl } from "../api";

export function detailGridOptions(columns) {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + genericBuDataUrl + "&_outputKey=resultList";
      }
    },
    isAutoWidth: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
     
    summaryRow: true,
    headerTextWrap: false,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    page: true,
    sheetName: "Generic BU Detail",
    sortList: [
      { columnId: "rankInBU1", sortAsc: true },
      { columnId: "cpeSpendInBUUSD1", sortAsc: false },
      { columnId: "cpeFamily", sortAsc: true }
    ]
  };
  return options;
}
