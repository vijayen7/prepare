import { put, takeEvery, call, all } from "redux-saga/effects";
import { UPDATE_DRILL_DOWN_SELECTED_FILTERS } from "commons/constants";
import { cashSummaryRootSaga } from "Treasury/CashSummaryView/saga";
import { ownershipBUSummaryRootSaga } from "Treasury/OwnershipBUSummary/saga";
import { positionDetailRootSaga } from "Treasury/PositionDetailView/saga";
import { buCPEFamilyDataSaga } from "Treasury/BUCPEFamily/saga";
import { exposureCoverageRootSaga } from "Treasury/ExposureCoverageView/saga";
import { totalExposureRootSaga } from "Treasury/TotalExposureView/saga";
import { exposureDetailDataSaga } from "Treasury/ExposureDetail/saga";
import { liquidIlliquidDataSaga } from "Treasury/LiquidIlliquid/saga";
import { individualBrokerRootSaga } from "Treasury/IndividualCounterpartyView/saga";
import { instrumentTypeRootSaga } from "Treasury/InstrumentTypeView/saga";
import { genericBuRootSaga } from "Treasury/GenericBUView/saga";
import { counterpartyRevenueSummaryDataSaga } from "Treasury/CounterpartyRevenueSummary/saga";
import { financingDetailDataSaga } from "Treasury/FinancingDetail/saga";
import { commissionDetailDataSaga, uploadBankChargeDataSaga } from "Treasury/CommissionDetail/saga";
import { summaryByCounterpartyDataSaga } from "Treasury/SummaryByCounterparty/saga";
import { interactionSummaryDataSaga } from "Treasury/InteractionSummary/saga";
import { cashExposureReportDataSaga } from "Treasury/CashExposureReport/saga";
import { cashReportDataSaga } from "Treasury/CashReport/saga";
import { counterpartyExposureMonitorDataSaga } from "Treasury/CounterpartyExposureMonitor/saga";

function* updateDrillDownSelectedFilters(action) {
  try {
    const selectedFilters = action.payload;
    yield put({
      type: `${UPDATE_DRILL_DOWN_SELECTED_FILTERS}_SUCCESS`,
      selectedFilters
    });
  } catch (e) {}
}

function* drillDownSelectedFiltersRootSaga() {
  yield [
    takeEvery(
      UPDATE_DRILL_DOWN_SELECTED_FILTERS,
      updateDrillDownSelectedFilters
    )
  ];
}

function* treasuryRootSaga() {
  yield all([
    cashSummaryRootSaga(),
    ownershipBUSummaryRootSaga(),
    positionDetailRootSaga(),
    buCPEFamilyDataSaga(),
    drillDownSelectedFiltersRootSaga(),
    exposureCoverageRootSaga(),
    exposureDetailDataSaga(),
    totalExposureRootSaga(),
    individualBrokerRootSaga(),
    liquidIlliquidDataSaga(),
    instrumentTypeRootSaga(),
    genericBuRootSaga(),
    counterpartyRevenueSummaryDataSaga(),
    financingDetailDataSaga(),
    commissionDetailDataSaga(),
    uploadBankChargeDataSaga(),
    summaryByCounterpartyDataSaga(),
    interactionSummaryDataSaga(),
    cashExposureReportDataSaga(),
    cashReportDataSaga(),
    counterpartyExposureMonitorDataSaga()
  ]);
}

export default treasuryRootSaga;
