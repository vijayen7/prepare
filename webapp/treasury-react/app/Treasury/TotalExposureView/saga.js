import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_TOTAL_EXPOSURE_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getFilter } from "commons/api/filters";

import { getTotalExposureData } from "./api";

function* fetchTotalExposureData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getTotalExposureData, action.payload);
    yield put({ type: `${FETCH_TOTAL_EXPOSURE_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_TOTAL_EXPOSURE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* totalExposureRootSaga() {
  yield [takeEvery(FETCH_TOTAL_EXPOSURE_DATA, fetchTotalExposureData)];
}
