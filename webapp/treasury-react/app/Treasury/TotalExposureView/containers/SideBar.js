import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getPreviousDate } from "commons/util";
import { fetchTotalExposureData } from "../actions";
import DateFilter from "commons/container/DateFilter";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import InputFilter from "commons/components/InputFilter";
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);

    this.state = {
      selectedDate: getCodexFilters("DATE"),
      topNCpes: "5"
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedDate: nextProps.dates["tMinusTwoFilterDate"]
    });

  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    var payload = {
      dateString: this.state.selectedDate,
      topN: this.state.topNCpes
    };

    this.props.fetchTotalExposureData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
          dateType="tMinusTwo"
        />
        <InputFilter
          data={this.state.topNCpes}
          onSelect={this.onSelect}
          stateKey="topNCpes"
          label="Top N CPEs"
        />
        <ColumnLayout>
          <FilterButton onClick={this.handleClick} label="Search" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchTotalExposureData
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
