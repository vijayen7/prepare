import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TotalExposureGrid from "commons/components/Grid";
import Message from "commons/components/Message";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  renderGridData() {
    let grid = null;
    if (
      !this.props.totalExposureData.hasOwnProperty(
        "totalCounterpartyExposure"
      ) ||
      this.props.totalExposureData.totalCounterpartyExposure.length <= 0
    )
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <TotalExposureGrid
        data={this.props.totalExposureData.totalCounterpartyExposure}
        gridId="TotalExposure"
        gridColumns={gridColumns(this.props.totalExposureData.ownershipEntitiesSortedOrder)}
        gridOptions={gridOptions(gridColumns(this.props.totalExposureData.ownershipEntitiesSortedOrder))}
        label = "Total Exposure"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    totalExposureData: state.treasury.totalExposureView.data
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
