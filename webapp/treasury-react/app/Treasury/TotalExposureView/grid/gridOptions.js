import { url } from "../api";
export function gridOptions(columns) {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=totalCounterpartyExposure";
      }
    },
    nestedTable: true,
    nestedField: columns[0].field,
    expandCollapseAll: true,
    autoHorizontalScrollBar: true,
    summaryRowText: "Total",
    expandTillLevel: 1,
     
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    exportToExcel: true,
    page: true,
    sheetName: "Total Exposure",
    groupColumns: getGroupColumns(columns)
  };
  return options;
}

function getGroupColumns(columns) {
  var groupColumns = [];
  for (var i = 1; i < columns.length; i += 2) {
    groupColumns.push([
      columns[i]["entity"],
      columns[i]["field"],
      columns[i + 1]["field"]
    ]);
  }

  return groupColumns;
}
