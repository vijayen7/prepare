import {
  percentFormatter,
  drillThroughFormatter,
  textRightAlignFormatter
} from "commons/grid/formatters";

import { percentComparator } from "commons/grid/comparators";

import { numberComparator } from "commons/grid/comparators";

var columns = [];

export function gridColumns(ownershipEntityArr) {
  var columns = [];

  for (var i = 0; i < ownershipEntityArr.length; i++) {
    columns.push(
      {
        id: ownershipEntityArr[i] + "_equity",
        entity: ownershipEntityArr[i], // Group name should be taken from this.
        name: "Equity",
        headerCssClass: "aln-rt b",
        field: ownershipEntityArr[i] + "_equity",
        toolTip:
          "Equity refers to the firms net exposure to the counterparty. It is a summation of mark to market/ exposure and collateral after adjusting for the impact of collateral in transit.",
        type: "number",
        sortable: true,
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        excelHeaderFormatter: function() {
          return ownershipEntityArr[i] + " Equity";
        },
        excelFormatter: "#,##0",
        comparator: function(a, b) {
          return numberComparator(a[sortcol], b[sortcol]);
        }
      },

      {
        id: ownershipEntityArr[i] + "_percent",
        name: "% of Fund",
        headerCssClass: "aln-rt b",
        field: ownershipEntityArr[i] + "_percent",
        type: "text",
        sortable: true,
        formatter: textRightAlignFormatter,
        excelHeaderFormatter: function() {
          return ownershipEntityArr[i] + " % of Fund";
        },
        comparator: function(a, b) {
          return percentComparator(a[sortcol], b[sortcol]);
        }
      }
    );
  }

  columns.unshift({
    id: "name",
    name: "",
    field: "name",
    type: "text",
    sortable: true,
    minWidth: 200
  });

  return columns;

  return columns;
}
