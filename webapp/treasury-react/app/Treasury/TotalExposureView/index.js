import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Loader from "commons/container/Loader";

const TotalExposureView = (props) =>
 {
  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
          </div>
          <br />
          <div>
            <ul className="bullet">
              <li>All figures in USD</li>
              <li>
                Notes:<ul className="bullet">
                  <li>
                    <small>
                      Total Exposure values are split pro-rata based on the
                      ownership of each trading entity and presented based on
                      the ownership of each trading entity.
                    </small>
                  </li>
                  <li>
                    <small>
                      All values are represented at ownership entity level after
                      applying ownership percentages.
                    </small>
                  </li>
                  <li>
                    <small>
                      Total Exposure is derived from collateral management
                      workflows.
                    </small>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TotalExposureView;
