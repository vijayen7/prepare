import {
  FETCH_TOTAL_EXPOSURE_DATA,
  DESTROY_TOTAL_EXPOSURE_VIEW
} from "commons/constants";

export function fetchTotalExposureData(payload) {
  return {
    type: FETCH_TOTAL_EXPOSURE_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_TOTAL_EXPOSURE_VIEW
  };
}
