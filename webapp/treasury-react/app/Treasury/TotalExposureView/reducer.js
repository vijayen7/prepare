import {
  FETCH_TOTAL_EXPOSURE_DATA,
  DESTROY_TOTAL_EXPOSURE_VIEW
} from "commons/constants";
import { combineReducers } from "redux";

function totalExposureDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_TOTAL_EXPOSURE_DATA}_SUCCESS`:
      return action.data || {};
    case DESTROY_TOTAL_EXPOSURE_VIEW:
      return {};
  }
  return state;
}

const rootReducer = combineReducers({
  data: totalExposureDataReducer
});

export default rootReducer;
