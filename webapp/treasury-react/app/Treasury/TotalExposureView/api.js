import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
import { fetchURL } from "commons/util";
export let url = "";
export function getTotalExposureData(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}data/search-total-counterparty-exposure?${paramString}`;
  return fetchURL(url);
}
