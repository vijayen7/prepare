import {
  percentFormatter,
  drillThroughFormatter,
  textTotalsNumberFormatter
} from "commons/grid/formatters";

import {
  numberComparator
} from "commons/grid/comparators";

var predicateFunction = function(row, cell, value, columnDef, dataContext) {
  return typeof dataContext["id"] !== "undefined";
};

var percentForSummaryRowFormatter = function(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  if (dataContext["isSummary"] != null && dataContext["isSummary"] == true) {
    return '<div class="margin--right--small" style="text-align: right;">' + value + ";</div>";
  } else {
    return dpGrid.Formatters.Number(row, cell, value, columnDef, dataContext);
  }
};

export function gridColumns(topNCpesArr, params) {
  var columns = [{
    id: "displayName",
    name: "Business Unit",
    headerCssStyle: "b",
    field: "displayName",
    minWidth: 125,
    sortable: true
  }];
  var i = 0;
  for (i = 0; i < topNCpesArr.length; i++) {
    columns.push({
      id: topNCpesArr[i],
      name: topNCpesArr[i],
      type: "number",
      headerCssClass: "aln-rt b",
      minWidth: 130,
      field: topNCpesArr[i],
      sortable: true,
      comparator: function(a, b) {
        return numberComparator(a[sortcol]["value"], b[sortcol]["value"]);
      },
      formatter: function(row, cell, value, columnDef, dataContext) {
        return drillThroughFormatter(
          row,
          cell,
          value["value"],
          columnDef,
          dataContext,
          textTotalsNumberFormatter,
          predicateFunction,
          value["params"],
          params[columnDef["name"]].value

        );
      },
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    });
  }

  columns.push({
    id: "buSortOder",
    name: "",
    type: "number",
    field: "buSortOder",
    sortable: true,
    width: 1,
    maxWidth: 1,
    minWidth: 1,
    unselectable: true,
    formatter: function displaySortValue(
      row,
      cell,
      val,
      columnDef,
      dataContext

    ) {
      "<div style='display: none;'>#{val}</div>";
    },
    exportToExcel: false
  });
  return columns;
}
