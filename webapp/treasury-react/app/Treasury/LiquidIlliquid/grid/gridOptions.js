import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=liquidIlliquidData";
      }
    },
    nestedTable: true,
    nestedField: "displayName",
    expandCollapseAll: true,
    expandTillLevel: -1,
    frozenColumn: 1,
    autoHorizontalScrollBar: true,
    summaryRowText: "Total",
     
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    page: true,
    exportToExcel: true,
    sheetName: "Liquid Illiquid data",
    sortList: [{ columnId: "buSortOder", sortAsc: true }]
  };
  return options;
}
