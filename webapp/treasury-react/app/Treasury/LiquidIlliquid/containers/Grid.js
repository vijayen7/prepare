import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LiquidIlliquidGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";
import { fetchPositionDetailData } from "../../PositionDetailView/actions";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  onCellClickHandler = args => {
    if (args.colId !== "displayName") {
      var parameters = args.item[args.colId].params.split("],");
      var selectedCpeFamilies = [];
      var selectedBusinessUnits = [];
      var businessUnitIds = parameters[0]
        .substring(32, parameters[0].length)
        .split(",");

      for (var i in businessUnitIds) {
        selectedBusinessUnits.push({
          key: parseInt(businessUnitIds[i]),
          value: "businessUnitIds"
        });
      }

      if (args.colId !== "Total") {
        var cpeFamilyIds = parameters[1]
          .substring(29, parameters[1].length - 2)
          .split(",");

        for (var i in cpeFamilyIds) {
          selectedCpeFamilies.push({
            key: parseInt(cpeFamilyIds[i]),
            value: "cpeFamilyIds"
          });
        }
      }

      var selectedFilterValues = Object.assign({}, this.props.selectedFilters);
      selectedFilterValues.selectedCpeFamilies = selectedCpeFamilies;
      selectedFilterValues.selectedBusinessUnits = selectedBusinessUnits;

      var payload = {
        dateString: selectedFilterValues.selectedDate,
        cpeIds: "-1",
        cpeFamilyIds: getCommaSeparatedValues(
          selectedFilterValues.selectedCpeFamilies
        ),
        agreementTypeIds: "-1",
        assetLiquidityIds: "2,3",
        positionGranularity: "OWNERSHIP_ENTITY",
        financingStatusTypeIds: "1",
        ownershipEntityIds: getCommaSeparatedValues(
          selectedFilterValues.selectedOwnershipEntities
        ),
        descoEntityIds: getCommaSeparatedValues(
          selectedFilterValues.selectedLegalEntities
        ),
        businessUnitIds: getCommaSeparatedValues(
          selectedFilterValues.selectedBusinessUnits
        )
      };
      this.props.fetchPositionDetailData(payload);
    }
  };

  renderGridData() {
    let grid = null;
    if (
      !this.props.liquidIlliquidData.hasOwnProperty("liquidIlliquidData") ||
      this.props.liquidIlliquidData.liquidIlliquidData.length <= 0
    )
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <LiquidIlliquidGrid
        data={this.props.liquidIlliquidData.liquidIlliquidData}
        onCellClick={this.onCellClickHandler}
        gridId="liquidIlliquidData"
        gridColumns={gridColumns(this.props.liquidIlliquidData.topNCpes,
          this.props.liquidIlliquidData.total[0])}
        gridOptions={gridOptions()}
        label = "Liquid Illiquid"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchPositionDetailData,
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    liquidIlliquidData: state.treasury.liquidIlliquidView.data,
    selectedFilters: state.treasury.drillDownFilters
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
