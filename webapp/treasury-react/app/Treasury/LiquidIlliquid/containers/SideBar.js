import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import OwnershipEntityFilter from "commons/container/OwnershipEntityFilter";
import AssetLiquidityFilter from "commons/container/AssetLiquidityFilter";
import FinancingStatusFilter from "commons/container/FinancingStatusFilter";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import InputFilter from "commons/components/InputFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import { fetchLiquidIlliquidData } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import { destroyDataState as positionDetailDestroyDataState } from "../../PositionDetailView/actions";
import _ from "lodash";
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  getDefaultFilters() {
    return {
      selectedLegalEntities: getCodexFilters("LEGAL_ENTITIES"),
      selectedOwnershipEntities: getCodexFilters("OWNERSHIP_ENTITIES"),
      selectedAssetLiquidity: [
        { key: "2", value: "Liquid" },
        { key: "3", value: "Illiquid" }
      ],
      selectedFinancingStatus: { key: "-1", value: "Total" },
      selectedMarketValue: { key: "GMV", value: "GMV" },
      numberOfCPEFamilies: "5",
      selectedDate: getCodexFilters("DATE")
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    this.props.positionDetailDestroyDataState();
    var payload = {
      dateString: this.state.selectedDate,
      financingStatusTypeIds: this.state.selectedFinancingStatus.key,
      gmvOrNmv: this.state.selectedMarketValue.key,
      noOfCpes: this.state.numberOfCPEFamilies,
      assetLiquidityIds: getCommaSeparatedValues(
        this.state.selectedAssetLiquidity
      ),
      ownershipEntityIds: getCommaSeparatedValues(
        this.state.selectedOwnershipEntities
      ),
      descoEntityIds: getCommaSeparatedValues(this.state.selectedLegalEntities)
    };

    this.props.updateDrillDownSelectedFilters(this.state);
    this.props.fetchLiquidIlliquidData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <OwnershipEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedOwnershipEntities}
        />
        <AssetLiquidityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedAssetLiquidity}
        />
        <FinancingStatusFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedFinancingStatus}
        />
        <SingleSelectFilter
          data={[{ key: "GMV", value: "GMV" }, { key: "NMV", value: "NMV" }]}
          onSelect={this.onSelect}
          selectedData={this.state.selectedMarketValue}
          stateKey="selectedMarketValue"
          label="GMV/NMV"
        />
        <InputFilter
          data={this.state.numberOfCPEFamilies}
          onSelect={this.onSelect}
          stateKey="numberOfCPEFamilies"
          label="Number of CPE Families"
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />

          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchLiquidIlliquidData,
      updateDrillDownSelectedFilters,
      positionDetailDestroyDataState
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
