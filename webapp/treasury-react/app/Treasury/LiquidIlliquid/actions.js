import {
  BASE_URL,
  FETCH_LIQUID_ILLIQUID_DATA,
  DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW
} from "commons/constants";

export function fetchLiquidIlliquidData(payload) {
  return {
    type: FETCH_LIQUID_ILLIQUID_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW
  };
}
