import {
  FETCH_LIQUID_ILLIQUID_DATA,
  DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function liquidIlliquidDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LIQUID_ILLIQUID_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: liquidIlliquidDataReducer
});

export default rootReducer;
