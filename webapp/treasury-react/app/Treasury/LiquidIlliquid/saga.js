import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_LIQUID_ILLIQUID_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";


import {
  getLiquidIlliquidData
} from "./api";

function* fetchLiquidIlliquidData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLiquidIlliquidData, action.payload);
    yield put({ type: `${FETCH_LIQUID_ILLIQUID_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LIQUID_ILLIQUID_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* liquidIlliquidDataSaga() {
  yield [
    takeEvery(FETCH_LIQUID_ILLIQUID_DATA, fetchLiquidIlliquidData)
  ];
}
