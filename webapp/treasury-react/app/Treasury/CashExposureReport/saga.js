import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_CASH_EXPOSURE_REPORT,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getCashExposureReport } from "./api";

function* fetchCashExposureReport(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCashExposureReport, action.payload);
    yield put({ type: `${FETCH_CASH_EXPOSURE_REPORT}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CASH_EXPOSURE_REPORT}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* cashExposureReportDataSaga() {
  yield [takeEvery(FETCH_CASH_EXPOSURE_REPORT, fetchCashExposureReport)];
}
