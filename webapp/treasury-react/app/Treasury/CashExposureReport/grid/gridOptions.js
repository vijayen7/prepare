import { url } from "../api";

function getGroupColumns(columns) {
  var groupColumns = [];
  var i = 0;
  while(i < columns.length) {
      // [GroupName, firstColumn's field, lastColumn's field]
      var j = i;
      while(j < columns.length && columns[i]["groupId"] == columns[j]["groupId"]) {
          j++;
      }
      groupColumns.push([columns[i]["groupName"], columns[i]["field"], columns[j-1]["field"]]);
      i = j;
  }

  return groupColumns;
}
export function gridOptions(columns, includeInstrumentDetails, groupByBu) {

  var initialGroupingColums = [
    "legalEntity",
    "counterpartyEntity",
    "agreementType"
  ];
  if (groupByBu.key === "BU") {
    initialGroupingColums = [
      "legalEntity",
      "businessUnit",
      "counterpartyEntity"
    ];
  }

  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=resultList";
      }
    },
    expandCollapseAll: true,
    groupColumns: getGroupColumns(columns),
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
     
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    page: true,
    sheetName: "Cash Exposure Report",
    sortList: [
      {
        columnId: "legalEntity",
        sortAsc: true
      }
    ],
    enableMultilevelGrouping: {
      showGroupingKeyInColumn: true,
      initialGrouping: initialGroupingColums,
      hideGroupingHeader: true
    }
  };

  if(includeInstrumentDetails) {
    options.nestedTable = true;
    options.nestedField = "gboType";
    options.expandTillLevel = -1;
  }

  return options;
}
