import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CashExposureGrid from "commons/components/Grid";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  renderGridData() {
    let grid = null;

    if (
      !this.props.cashExposureReport.hasOwnProperty("resultList") ||
      this.props.cashExposureReport.resultList.length <= 0
    )
      if (this.props.drillDownGrid) {
        return null;
      } else
        return (
          <Message messageData="No data available for this search criteria." />
        );
    grid = (
      <CashExposureGrid
        data={this.props.cashExposureReport.resultList}
        gridId="CashExposureReport"
        gridColumns={gridColumns(
          this.props.selectedFilters.includeInstrumentDetails
        )}
        gridOptions={gridOptions(
          gridColumns(this.props.selectedFilters.includeInstrumentDetails),
          this.props.selectedFilters.includeInstrumentDetails,
          this.props.selectedFilters.groupByLevel
        )}
        label="Cash Exposure Report"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    cashExposureReport: state.treasury.cashExposureReportView.data,
    selectedFilters: state.treasury.drillDownFilters
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
