import {
  BASE_URL,
  FETCH_CASH_EXPOSURE_REPORT,
  DESTROY_DATA_STATE_FOR_CASH_EXPOSURE_REPORT_VIEW
} from "commons/constants";

export function fetchCashExposureReport(payload) {
  return {
    type: FETCH_CASH_EXPOSURE_REPORT,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_CASH_EXPOSURE_REPORT_VIEW,
  };
}
