import {
  FETCH_CASH_EXPOSURE_REPORT,
  DESTROY_DATA_STATE_FOR_CASH_EXPOSURE_REPORT_VIEW
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function cashExposureReportDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_CASH_EXPOSURE_REPORT}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_CASH_EXPOSURE_REPORT_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: cashExposureReportDataReducer
});

export default rootReducer;
