import { UPDATE_DRILL_DOWN_SELECTED_FILTERS } from "commons/constants";
export function updateDrillDownSelectedFilters(payload) {
  return {
    type: UPDATE_DRILL_DOWN_SELECTED_FILTERS,
    payload
  };
}
