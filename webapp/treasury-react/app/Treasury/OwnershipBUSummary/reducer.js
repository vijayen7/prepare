import {
  FETCH_OWNERSHIP_BU_SUMMARY_DATA,
  DESTROY_DATA_STATE_FOR_OWNERSHIP_BU_SUMMARY_VIEW
} from "commons/constants";
import { combineReducers } from "redux";

function ownershipBUSummaryDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_OWNERSHIP_BU_SUMMARY_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_OWNERSHIP_BU_SUMMARY_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: ownershipBUSummaryDataReducer
});

export default rootReducer;
