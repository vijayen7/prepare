import { BASE_URL } from "commons/constants";
export let url = "";
export function getOwnershipBUSummaryData(payload) {
  url = `${BASE_URL}data/search-ownership-bu-data?date=${
    payload.date
  }&mvRollingDays=${payload.mvRollingDays}&cpeFamilyIds=${
    payload.cpeFamilyIds
  }&ownershipEntityIds=${payload.ownershipEntityIds}&descoEntityIds=${
    payload.descoEntityIds
  }`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
