import {
  BASE_URL,
  FETCH_OWNERSHIP_BU_SUMMARY_DATA,
  DESTROY_DATA_STATE_FOR_OWNERSHIP_BU_SUMMARY_VIEW
} from "commons/constants";

export function fetchOwnershipBUSummaryData(payload) {
  return {
    type: FETCH_OWNERSHIP_BU_SUMMARY_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_OWNERSHIP_BU_SUMMARY_VIEW
  };
}
