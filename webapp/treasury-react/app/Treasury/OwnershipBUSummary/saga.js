import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_OWNERSHIP_BU_SUMMARY_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getOwnershipBUSummaryData } from "./api";

function* fetchOwnershipBUSummaryData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getOwnershipBUSummaryData, action.payload);
    yield put({ type: `${FETCH_OWNERSHIP_BU_SUMMARY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_OWNERSHIP_BU_SUMMARY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* ownershipBUSummaryRootSaga() {
  yield [
    takeEvery(FETCH_OWNERSHIP_BU_SUMMARY_DATA, fetchOwnershipBUSummaryData)
  ];
}
