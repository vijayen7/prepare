import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Message from "commons/components/Message";
import { fetchBUCPEFamilyData } from "Treasury/BUCPEFamily/actions";
import { getCommaSeparatedValues } from "commons/util";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import { destroyDataState } from "../actions";
import OwnershipBuSummaryGrid from "commons/components/GridWithCellClick";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  onCellClickHandler = args => {
    if (args.colId === "businessUnitName") {
      var businessUnitName = args.item.businessUnitName.value;
      var businessUnits = args.item.businessUnitName.params
        .substring(22, args.item.businessUnitName.params.length - 2)
        .split(",");
      var selectedBusinessUnits = [];
      for (var i in businessUnits) {
        selectedBusinessUnits.push({
          key: parseInt(businessUnits[i]),
          value: businessUnitName
        });
      }
      var selectedFilterValues = Object.assign({}, this.props.selectedFilters);
      selectedFilterValues.selectedBusinessUnits = selectedBusinessUnits;
      var payload = {
        date: selectedFilterValues.selectedDate,
        mvRollingDays: selectedFilterValues.selectedMVRolling.value,
        cpeFamilyIds: getCommaSeparatedValues(
          selectedFilterValues.selectedCpeFamilies
        ),
        ownershipEntityIds: getCommaSeparatedValues(
          selectedFilterValues.selectedOwnershipEntities
        ),
        descoEntityIds: getCommaSeparatedValues(
          selectedFilterValues.selectedLegalEntities
        ),
        businessUnitIds: getCommaSeparatedValues(
          selectedFilterValues.selectedBusinessUnits
        )
      };
      this.props.updateDrillDownSelectedFilters(selectedFilterValues);
      this.props.fetchBUCPEFamilyData(payload);
    }
  };

  renderGridData() {
    let grid = null;
    if (
      !this.props.ownershipBUSummaryData.hasOwnProperty("buGroupSummaryList") ||
      this.props.ownershipBUSummaryData.buGroupSummaryList.length <= 0
    )
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <OwnershipBuSummaryGrid
        data={this.props.ownershipBUSummaryData.buGroupSummaryList}
        onCellClick={this.onCellClickHandler}
        gridId="OwnershipBuSummary"
        gridColumns={gridColumns(
          this.props.ownershipBUSummaryData.totalSummary[0]
        )}
        gridOptions={gridOptions()}
        label="Ownership BU Summary"
      />
    );
    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchBUCPEFamilyData,
      updateDrillDownSelectedFilters,
      destroyDataState
    },
    dispatch
  );
}
function mapStateToProps(state) {
  return {
    ownershipBUSummaryData: state.treasury.ownershipView.data,
    selectedFilters: state.treasury.drillDownFilters,
    drillDownBUCPEFamilyViewData:
      state.treasury.BUCPEFamilyView.data
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
