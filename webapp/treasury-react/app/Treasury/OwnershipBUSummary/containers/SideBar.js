import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import DateFilter from "commons/container/DateFilter";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import OwnershipEntityFilter from "commons/container/OwnershipEntityFilter";
import CpeFamilyFilter from "commons/container/CpeFamilyFilter";
import MvRollingFilter from "commons/container/MvRollingFilter";
import { getCodexFilters } from "../../../commons/util";
import { fetchOwnershipBUSummaryData } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import { destroyDataState as buCpeFamilyDestroyDataState } from "../../BUCPEFamily/actions";
import { destroyDataState as positionDetailDestroyDataState } from "../../PositionDetailView/actions";


class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);

    this.state = {
      selectedLegalEntities:getCodexFilters("LEGAL_ENTITIES"),
      selectedOwnershipEntities: getCodexFilters("OWNERSHIP_ENTITIES"),
      selectedMVRolling: getCodexFilters("ROLLING_DAYS")[0],
      selectedCpeFamilies: [],
      selectedDate: getCodexFilters("DATE")
    };
  }

  applySavedFilters(selectedFilters) {
    console.log(selectedFilters);
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    this.props.buCpeFamilyDestroyDataState();
    this.props.positionDetailDestroyDataState();
    var payload = {
      date: this.state.selectedDate,
      mvRollingDays: this.state.selectedMVRolling.value,
      cpeFamilyIds: getCommaSeparatedValues(this.state.selectedCpeFamilies),
      ownershipEntityIds: getCommaSeparatedValues(
        this.state.selectedOwnershipEntities
      ),
      descoEntityIds: getCommaSeparatedValues(this.state.selectedLegalEntities)
    };
    this.props.updateDrillDownSelectedFilters(this.state);
    this.props.fetchOwnershipBUSummaryData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <OwnershipEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedOwnershipEntities}
        />
        <CpeFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpeFamilies}
        />
        <MvRollingFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedMVRolling}
        />
        <ColumnLayout>
          <FilterButton onClick={this.handleClick} label="Search" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchOwnershipBUSummaryData,
      updateDrillDownSelectedFilters,
      buCpeFamilyDestroyDataState,
      positionDetailDestroyDataState
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
