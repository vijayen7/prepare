import {
  percentFormatter,
  drillThroughFormatter,
  percentTotalsFormatter,
  numberTotalsFormatter,
  excelTotalsFormatter
} from "commons/grid/formatters";

export function gridColumns(params) {
  console.log(params);
  var columns = [{
      id: "businessUnitName",
      name: "Summary By BU",
      headerCssClass: "b",
      field: "businessUnitName",
      sortable: true,
      excelDataFormatter: function(data) {
        return data["value"];
      },
      formatter: function(row, cell, value, columnDef, dataContext) {
        return drillThroughFormatter(
          row,
          cell,
          value["value"],
          columnDef,
          dataContext,
          undefined,
          undefined,
          value["params"]
        );
      }
    },
    {
      id: "financedAssetsLMV",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["financedAssetsLMV"]
        );
      },
      name: "LMV",
      field: "financedAssetsLMV",
      sortable: true,
      toolTip: "LMV refers to long market value of financed assets.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "financedAssetsSMV",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["financedAssetsSMV"]
        );
      },
      name: "SMV",
      field: "financedAssetsSMV",
      sortable: true,
      toolTip: "SMV refers to short market value of financed assets.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "marginPercent",
      type: "number",
      name: "Margin (%)",
      field: "marginPercent",
      sortable: true,
      toolTip: "Margin refers to the percentage of financed asset margin as a proportion of financed asset GMV.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["marginPercent"]
        );
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        return excelTotalsFormatter(itemValue, dataItem, params["marginPercent"]);
      }
    },
    {
      id: "financedAssetsMargin",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["financedAssetsMargin"]
        );
      },
      name: "Margin (financed)",
      field: "financedAssetsMargin",
      sortable: true,
      toolTip: "Margin (financed) refers to margin on financed assets.",
      headerCssClass: "text-align--right",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "totalMargin",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["totalMargin"]
        );
      },
      name: "Margin (total)",
      field: "totalMargin",
      toolTip: "Margin (total) refers to margin on all positions (both financed and unfinanced, assets and non-assets).",
      sortable: true,
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "totalUsage",
      type: "number",
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["totalUsage"]
        );
      },
      name: "Usage",
      field: "totalUsage",
      toolTip: "Usage refers to usage across positions (financed and unfinanced, assets and non-assets). Usage is the total amount of capital required to hold a position. In most cases, usage is equivalent to margin, but usage also includes the net option value under Futures & Options agreement, accrued interest and bad debt expense. Loan received under a variable funding note arrangement is shown as a deduction in usage.",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "adjustedUsage",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["adjustedUsage"]
        );
      },
      name: "Adjusted Usage",
      field: "adjustedUsage",
      toolTip: "Adjusted Usage refers to usage, including adjustments from published collateral management workflows.",
      sortable: true,
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "marginChange",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["marginChange"]
        );
      },
      name: "Margin Change",
      field: "marginChange",
      sortable: true,
      toolTip: "Margin Change refers to the change in Total Margin when compared to previous day's total margin.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "wam",
      name: "WAM",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["wam"]
        );
      },
      field: "wam",
      sortable: true,
      toolTip: "WAM refers to gross market value weighted financing lock up represented in days, WAM is calculated as {Sum of Lockup Days * Financed Asset GMV} / {Sum of Financed Asset GMV}",
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b"
    },
    {
      id: "mvRolling",
      name: "MV rolling in selected days",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["mvRolling"]
        );
      },
      field: "mvRolling",
      sortable: true,
      toolTip: "MV rolling in selected days represents the amount of gross market value of financed assets that could become unfinanced in selected days, considering the lock up terms.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "marketShareOfLargestCPEFamily",
      name: "Market share of largest CPE Family",
      type: "number",
      field: "marketShareOfLargestCPEFamily",
      sortable: true,
      toolTip: "Market share of largest CPE Family indicates the largest counterparty family (as a % of GMV) in the Business Unit or Business Unit group.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          params["marketShareOfLargestCPEFamily"]
        );
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        return excelTotalsFormatter(itemValue, dataItem, params["marketShareOfLargestCPEFamily"]);
      }
    },
    {
      id: "buSortOrder",
      name: "",
      type: "number",
      field: "buSortOrder",
      sortable: true,
      width: 1,
      maxWidth: 1,
      minWidth: 1,
      unselectable: true,
      formatter: function(row, cell, val, columnDef, dataContext) {
        "<div style='display: none;'>#{val}</div>";
      },
      exportToExcel: false
    }
  ];

  return columns;
}
