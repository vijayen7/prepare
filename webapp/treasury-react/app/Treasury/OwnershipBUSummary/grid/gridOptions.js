import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=buGroupSummaryList";
      }
    },
    nestedTable: true,
    nestedField: "businessUnitName",
    autoHorizontalScrollBar: true,
    expandTillLevel: -1,
    expandCollapseAll: true,
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    sheetName: "Ownership-BU Summary data",
    page: true,
    sortList: [{ columnId: "buSortOrder", sortAsc: true }]
  };
  return options;
}
