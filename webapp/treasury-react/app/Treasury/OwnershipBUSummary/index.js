import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Loader from "commons/container/Loader";
import BuCPEFamilyGridContainer from "../BUCPEFamily/containers/Grid";
import PositionDetailGridContainer from "../PositionDetailView/containers/Grid";

import {
  OWNERSHIP_BU_SUMMARY
} from "../constants";

const OwnershipBUSummary = props => {
  if(props.selectedView != OWNERSHIP_BU_SUMMARY) {
    props.updateSelectedView(OWNERSHIP_BU_SUMMARY);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName} />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
            <br />
            <BuCPEFamilyGridContainer drillDownGrid={true} />
            <br />
            <PositionDetailGridContainer drillDownGrid={true} />
          </div>
          <div className="padding--vertical--double">
            <ul className="bullet">
              <li>All figures in USD. </li>
              <li>
                Notes:
                <ul className="bullet">
                  <li>
                    <small>
                      Margin and usage numbers are system calculated numbers.
                    </small>
                  </li>
                  <li>
                    <small>
                      Adjusted Usage includes adjustments published from
                      collateral management workflows.
                    </small>
                  </li>
                  <li>
                    <small>
                      Assets refer to 'on-balance sheet' instruments and
                      includes listed equity options and total return swaps
                      whose first derivative is an asset.
                    </small>
                  </li>
                  <li>
                    <small>
                      Financed Assets refer to assets that are explicitly
                      levered and excludes futures and other instrument types
                      not explicitly levered.
                    </small>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OwnershipBUSummary;
