import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  START_LOADING,
  END_LOADING,
  FETCH_INDIVIDUAL_BROKER_DATA
} from "commons/constants";

import { getIndividualBrokerData, getIndividualBrokerSummaryData } from "./api";

function* fetchIndividualBrokerData(action) {
  try {
    yield put({ type: START_LOADING });
    const [detailData, summaryData] = yield all([
      call(getIndividualBrokerData, action.payload),
      call(getIndividualBrokerSummaryData, action.payload)
    ]);
    yield put({
      type: `${FETCH_INDIVIDUAL_BROKER_DATA}_SUCCESS`,
      summaryData,
      detailData
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_INDIVIDUAL_BROKER_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* individualBrokerRootSaga() {
  yield [takeEvery(FETCH_INDIVIDUAL_BROKER_DATA, fetchIndividualBrokerData)];
}
