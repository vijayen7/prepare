import {
  FETCH_INDIVIDUAL_BROKER_DATA,
  DESTROY_DATA_STATE_INDIVIDUAL_BROKER_VIEW
} from "commons/constants";
import { combineReducers } from "redux";

function individualBrokerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_INDIVIDUAL_BROKER_DATA}_SUCCESS`:
      return (
        { summaryData: action.summaryData, detailData: action.detailData } || {}
      );
    case DESTROY_DATA_STATE_INDIVIDUAL_BROKER_VIEW:
      return {};
  }
  return state;
}

const rootReducer = combineReducers({
  data: individualBrokerDataReducer
});

export default rootReducer;
