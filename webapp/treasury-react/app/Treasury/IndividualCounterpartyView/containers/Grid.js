import React, { Component } from "react";
import { summaryGridColumns } from "../grid/SummaryGridColumns";
import { summaryGridOptions } from "../grid/SummaryGridOptions";
import { detailGridColumns } from "../grid/DetailGridColumns";
import { detailGridOptions } from "../grid/DetailGridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";
import IndividualCounterpartyDetailGrid from "commons/components/Grid";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  getYear(date) {
    return date.substring(0, 4);
  }

  renderGridData() {
    let grid = null;
    if (
      !this.props.data.hasOwnProperty("summaryData") ||
      !this.props.data.summaryData.hasOwnProperty("resultList") ||
      !this.props.data.summaryData.resultList.length > 0 ||
      this.props.data.summaryData.resultList[0].rows <= 0
    ) {
      return (
        <Message messageData="No data available for this search criteria." />
      );
    }
    grid = (
      <div>
        <IndividualCounterpartyDetailGrid
          data={this.props.data.summaryData.resultList[0].rows}
          gridColumns={summaryGridColumns(
            this.getYear(this.props.data.summaryData.metaData.asOfDate),
            this.props.data.summaryData.resultList[0].summary
          )}
          gridOptions={summaryGridOptions(
            summaryGridColumns(
              this.getYear(this.props.data.summaryData.metaData.asOfDate)
            )
          )}
          gridId="IndividualCounterpartySummary"
          label="Individual Counterparty Summary"
        />
        <IndividualCounterpartyDetailGrid
          data={this.props.data.detailData.resultList}
          gridColumns={detailGridColumns(
            this.getYear(this.props.data.detailData.metaData.asOfDate)
          )}
          gridOptions={detailGridOptions(
            detailGridColumns(
              this.getYear(this.props.data.detailData.metaData.asOfDate)
            )
          )}
          gridId="IndividualCounterpartyDetail"
          label="Individual Counterparty Detail"
        />
        <br />
        <div>
          <small>
            <ul className="bullet">
              <li> All figures in $M USD</li>
              <li>
                The numbers are updated and verified up until{" "}
                {this.props.data.detailData.metaData.asOfDate}.
              </li>
              <li>
                The numbers are annualized based on year to date data and may be
                materially higher or lower than prior year if there are expense
                fluctuations in the earlier months of the year.
              </li>
            </ul>
          </small>
        </div>
      </div>
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapStateToProps(state) {
  return {
    data: state.treasury.individualBrokerView.data
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
