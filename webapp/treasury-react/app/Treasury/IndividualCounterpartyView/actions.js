import {
  FETCH_INDIVIDUAL_BROKER_DATA,
  FETCH_INDIVIDUAL_BROKER_FILTERS,
  DESTROY_DATA_STATE_INDIVIDUAL_BROKER_VIEW
} from "commons/constants";

export function fetchIndividualBrokerData(payload) {
  return {
    type: FETCH_INDIVIDUAL_BROKER_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_INDIVIDUAL_BROKER_VIEW
  };
}
