import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";

import {
  INDIVIDUAL_COUNTERPARTY_VIEW
} from "../constants";

const IndividualCounterparty = (props) =>
 {
  if(props.selectedView != INDIVIDUAL_COUNTERPARTY_VIEW) {
    props.updateSelectedView(INDIVIDUAL_COUNTERPARTY_VIEW);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
        </div>
        <div className="size--content">
          <Grid />
        </div>
      </div>
    </div>
  );
};

export default IndividualCounterparty;
