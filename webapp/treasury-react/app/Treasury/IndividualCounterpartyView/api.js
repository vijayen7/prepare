import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
import { fetchURL } from "commons/util";
export let brokerDataUrl = "";
export function getIndividualBrokerData(payload) {
  const paramString = queryString.stringify(payload);
  brokerDataUrl = `${BASE_URL}data/search-broker-revenue-detail?${paramString}`;
  return fetchURL(brokerDataUrl);
}
export let brokerSummaryDataUrl = "";
export function getIndividualBrokerSummaryData(payload) {
  const paramString = queryString.stringify(payload);
  brokerSummaryDataUrl = `${BASE_URL}data/search-broker-revenue-ytd-summary?${paramString}`;
  return fetchURL(brokerSummaryDataUrl);
}
