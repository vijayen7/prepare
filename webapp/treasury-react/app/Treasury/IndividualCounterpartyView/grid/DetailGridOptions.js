import { brokerDataUrl } from "../api";
export function detailGridOptions(columns) {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin +brokerDataUrl+ "&_outputKey=resultList";
      }
    },
    isAutoWidth: true,
    nestedTable: true,
    nestedField: columns[0].field,
    expandTillLevel: -1,
    expandCollapseAll: true,
     
    headerTextWrap: false,
    frozenColumn: 0,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    page: true,
    sheetName: "Individual Broker Revenue Detail",
    sortList: [{ columnId: "buSortOrder", sortAsc: true }]
  };
  return options;
}
