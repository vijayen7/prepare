import { brokerSummaryDataUrl } from "../api";
export function summaryGridOptions(columns) {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + brokerSummaryDataUrl+ "&_outputKey=resultList";
      }
    },
    nestedTable: true,
    nestedField: columns[0].field,
    expandTillLevel: -1,
    expandCollapseAll: true,
    isAutoWidth: true,
     
    summaryRow: true,
    displaySummaryRow: true,
    exportToExcel: true,
    headerTextWrap: false,
    page: true,
    sheetName: "Individual Broker YTD"
  };
  return options;
}
