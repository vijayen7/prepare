import {
  percentFormatter,
  millionFormatter,
  toolTipFormatter
} from "commons/grid/formatters";

export function detailGridColumns(year) {
  var year1 = year;
  var year2 = year - 1;
  var columns = [
    {
      id: "businessUnit",
      name: "Business Unit",
      field: "businessUnit",
      toolTip: "Business Unit",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      autoWidth: true
    },
    {
      id: "cpeSpendInBUUSD1",
      name: year1 + " Spend",
      field: "cpeSpendInBUUSD1",
      toolTip:
        year1 +
        " Spend refers to the total spend at this broker either on a Year to Date or Annualized basis depending on whether the the 'Show Annualized' check box is ticked.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.00"
    },
    {
      id: "cpeSpendInBUUSD2",
      name: year2 + " Spend",
      field: "cpeSpendInBUUSD2",
      toolTip:
        year2 +
        " Spend refers to the total spend at this broker in the previous year.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.0"
    },
    {
      id: "cpeSpendInBUChange",
      name: year1 + "/" + year2 + " Spend Change",
      field: "cpeSpendInBUChange",
      toolTip:
        year1 +
        "/" +
        year2 +
        " Spend Change refers to the change in the spend at this broker year over year  (Current year - Previous year).",
      type: "number",
      filter: true,
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      autoWidth: true
    },
    {
      id: "marketShareInBU1",
      name: year1 + " Market Share (%)",
      field: "marketShareInBU1",
      toolTip:
        year1 +
        " Market Share (%) refers to the current year total spend of the Business Unit at this broker as a percentage of the overall spend of the Business Unit across brokers.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: percentFormatter,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.0"
    },
    {
      id: "marketShareInBU2",
      name: year2 + " Market Share (%)",
      field: "marketShareInBU2",
      toolTip:
        year2 +
        " Market Share (%) refers to the previous  year total spend of the Business Unit at this broker as a percentage of the overall spend of the Business Unit across brokers.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: percentFormatter,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.0"
    },
    {
      id: "marketShareInBUChange",
      name: year1 + "/" + year2 + " Market Share Change (%)",
      field: "marketShareInBUChange",
      toolTip:
        year1 +
        "/" +
        year2 +
        " Market Share Change (%) refers to the change in the composition of the Business Unit's spend among different brokers. A positive number indicates that the Business Unit is spending a higher % of its total spend at this broker compared to the previous year.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: percentFormatter,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      excelFormatter: "#,##0.0"
    },
    {
      id: "rankInBU1",
      name: year1 + " Rank",
      field: "rankInBU1",
      toolTip:
        year1 +
        " Rank refers to the current year rank of this broker in terms of total spend for the Business Unit. Rank is calculated as ($ of Spending at broker) / (Total Counterparty Spending) ranked numerically.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "rankInBU2",
      name: year2 + " Rank",
      field: "rankInBU2",
      toolTip:
        year2 +
        " Rank refers to the previous year rank of this broker in terms of total spend for the Business Unit.  Rank is calculated as  ($ of Spending at broker) / (Total Counterparty Spending) ranked numerically.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "rankInBUChange",
      name: year1 + "/" + year2 + " Rank Change",
      field: "rankInBUChange",
      toolTip:
        year1 +
        "/" +
        year2 +
        " Rank Change refers to the year over year change in the rank of the broker for the Business Unit.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "buSortOrder",
      name: "",
      type: "number",
      field: "buSortOrder",
      sortable: true,
      width: 1,
      maxWidth: 1,
      minWidth: 1,
      unselectable: true,
      formatter: function(row, cell, val, columnDef, dataContext) {
        "<div style='display: none;'>#{val}</div>";
      },
      exportToExcel: false
    }
  ];

  return columns;
}
