import {
  FETCH_COMMISSION_DETAIL_DATA,
  DESTROY_DATA_STATE_FOR_COMMISSION_DETAIL_VIEW
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function commissionDetailDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_COMMISSION_DETAIL_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_COMMISSION_DETAIL_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: commissionDetailDataReducer
});

export default rootReducer;
