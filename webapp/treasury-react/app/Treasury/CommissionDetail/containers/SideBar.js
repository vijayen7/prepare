import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import { getCommaSeparatedValues } from "commons/util";
import Sidebar from "commons/components/Sidebar";
import Error from "commons/components/Error";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import BusinessUnitFilter from "commons/container/BusinessUnitFilter";
import ExecBrokerFamiliesFilter from "commons/container/ExecBrokerFamiliesFilter";
import ExecBrokerFilter from "commons/container/ExecBrokerFilter";
import GBOTypeFilter from "commons/container/GBOTypeFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import BULevelFilter from "commons/container/BULevelFilter";
import BundleGroupTypeFilter from "commons/container/BundleGroupTypeFilter";
import BundleGroupFilter from "commons/container/BundleGroupFilter";
import TypeSubtypeFilter from "commons/container/TypeSubtypeFilter";
import { fetchCommissionDetail } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import { getCodexFilters, getKeysAsArray } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

applySavedFilters(selectedFilters) {
  this.setState(selectedFilters);
}

onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
}

  getDefaultFilters() {
    return {
      selectedBusinessUnits: [],
      selectedLegalEntities: getCodexFilters("LEGAL_ENTITIES"),
      selectedExecBrokerFamilies: [],
      selectedExecBrokers: [],
      selectedGboTypes: [],
      selectedTypeSubtype: {
        selectedFoTypes: [],
        selectedSubtypes: [],
      },
      selectedBULevel: { key: "BU", value: "Business Unit" },
      selectedBundleGroupTypes: { key: "-2", value: "" },
      selectedBundleGroups: [],
      selectedStartDate: getCodexFilters("DATE"),
      selectedEndDate: getCodexFilters("DATE")
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    var payload = {
      startDateString: this.state.selectedStartDate,
      endDateString: this.state.selectedEndDate,
      businessUnitIds: getCommaSeparatedValues(
        this.state.selectedBusinessUnits
      ),
      execBrokerFamilyIds: getCommaSeparatedValues(
        this.state.selectedExecBrokerFamilies
      ),
      execBrokerIds: getCommaSeparatedValues(this.state.selectedExecBrokers),
      gboTypeIds: getCommaSeparatedValues(this.state.selectedGboTypes),
      foTypeIds: getCommaSeparatedValues(this.state.selectedTypeSubtype.selectedFoTypes),
      subtypeIds: getCommaSeparatedValues(this.state.selectedTypeSubtype.selectedSubtypes),
      descoEntityIds: getCommaSeparatedValues(this.state.selectedLegalEntities),
      bundleGroupIds: getCommaSeparatedValues(this.state.selectedBundleGroups),
      buLevel: this.state.selectedBULevel.key,
      bundleGroupTypeId: this.state.selectedBundleGroupTypes.key
    };

    this.props.fetchCommissionDetail(payload);
  }

  render() {
    const buLevel = this.state.selectedBULevel.key;
    let bundleGroupFilters = null;
    if (buLevel === "BUNDLE_GROUP") {
      bundleGroupFilters = (
        <div>
          <BundleGroupTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedBundleGroupTypes}
          />
          <BundleGroupFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedBundleGroups}
          />
        </div>
      );
    } else {
      bundleGroupFilters = (
        <BusinessUnitFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnits}
        />
      );
    }
    let dateError = null;
    if (this.state.selectedStartDate > this.state.selectedEndDate) {
      dateError = <Error messageData="Start date is greater than End date" />;
    } else {
      dateError = null;
    }
    return (
      <Sidebar>
        {dateError}
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedStartDate"
          data={this.state.selectedStartDate}
          label="Start Date"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedEndDate"
          data={this.state.selectedEndDate}
          label="End Date"
        />

        <ExecBrokerFamiliesFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedExecBrokerFamilies}
        />
        <ExecBrokerFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedExecBrokers}
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <GBOTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedGboTypes}
        />
        <TypeSubtypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedTypeSubtype}
        />
        <BULevelFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBULevel}
        />
        {bundleGroupFilters}
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            disabled={dateError !== null}
            reset={false}
            label="Search"
          />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCommissionDetail,
      updateDrillDownSelectedFilters
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
