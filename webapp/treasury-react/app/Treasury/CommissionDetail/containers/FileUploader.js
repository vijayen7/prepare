import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ALLOWED_FILE_TYPES } from "./../constants";
import {
  uploadBankChargeData
} from "./../actions";
import { FILE } from "./../constants";
import Dropzone from "./../components/Dropzone";
import TemplateDownloader from "../components/TemplateDownloader";
import { Layout } from "arc-react-components";
import { ToastService } from 'arc-react-components';

class FileUploader extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters = () => {
    return {
      files: { accepted: [], rejected: [], rejectedReasons: new Map() }
    };
  }

  clearFileArray = () => {
    this.setState({
      files: { accepted: [], rejected: [], rejectedReasons: new Map() }
    });
  }

  onFileUpload = files => {
    this.setState({ files: files });
    this.handleUpload(files);
  }

  getUploadedFileName = () => {
    return this.state.files.accepted[this.state.files.accepted.length - 1] ===
      undefined
      ? undefined
      : this.state.files.accepted[this.state.files.accepted.length - 1].name;
  }

  onSelect = (params) => {
    const { key, value } = params;
    let newState = { ...this.state };
    newState[key] = value;
    this.setState(newState);
  }

  handleUpload = files => {
    const uploadData = new FormData();

    if (files.accepted.length > 0) {
      let file = files.accepted[files.accepted.length - 1];
      if (file.name.lastIndexOf(ALLOWED_FILE_TYPES) == -1) {
        ToastService.append({
          content: `Please check if file format and name are proper.`,
          type: ToastService.ToastType.CRITICAL,
          placement: ToastService.Placement.TOP_RIGHT,
          dismissTime: 8000
        });
        return;
      }
      uploadData.append(FILE, file);
      this.props.uploadBankChargeData(
        uploadData
      );
    } else {
      ToastService.append({
        content: `Please check if the format of the file is excel.`,
        type: ToastService.ToastType.CRITICAL,
        placement: ToastService.Placement.TOP_RIGHT,
        dismissTime: 8000
      });
    }
    this.clearFileArray();
  }


  getTemplateDownloader = () => {
    return (
      <Layout.Child className="text-align--center padding--top">
        <TemplateDownloader />
      </Layout.Child>
    );
  }

  getFileUploader = () => {
    let uploadedFileName = this.getUploadedFileName();

    return (
      <Layout.Child className="text-align--center padding--top">
        <Dropzone
          style={{ width: "45%" }}
          fileName={uploadedFileName}
          files={this.state.files}
          allowedFileTypes={ALLOWED_FILE_TYPES}
          onFileUpload={this.onFileUpload}
        />
      </Layout.Child>
    );
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          {this.getTemplateDownloader()}
          {this.getFileUploader()}
        </Layout>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      uploadBankChargeData
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(FileUploader);
