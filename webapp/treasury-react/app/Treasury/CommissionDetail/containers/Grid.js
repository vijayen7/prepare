import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CommissionDetailGrid from "commons/components/Grid";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  renderGridData() {
    let grid = null;

    if (
      !this.props.commissionDetailData.hasOwnProperty(
        "commissionDetailList"
      ) ||
      this.props.commissionDetailData.commissionDetailList
        .length <= 0
    )
      if (this.props.drillDownGrid) {
        return null;
      } else {
        return (
          <Message messageData="No data available for this search criteria." />
        );
      }
    grid = (
      <CommissionDetailGrid
        data={
          this.props.commissionDetailData.commissionDetailList
        }
        gridId="CommissionDetail"
        gridColumns={gridColumns()}
        gridOptions={gridOptions()}
        label = "Commission Detail"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    commissionDetailData: state.treasury.commissionDetailView.data
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
