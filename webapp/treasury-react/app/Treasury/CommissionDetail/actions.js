import {
  BASE_URL,
  FETCH_COMMISSION_DETAIL_DATA,
  DESTROY_DATA_STATE_FOR_COMMISSION_DETAIL_VIEW,
  UPLOAD_BANK_CHARGE_DATA
} from "commons/constants";

export function fetchCommissionDetail(payload) {
  return {
    type: FETCH_COMMISSION_DETAIL_DATA,
    payload
  };
}

export function uploadBankChargeData(payload) {
  return {
    type: UPLOAD_BANK_CHARGE_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_COMMISSION_DETAIL_VIEW,
  };
}
