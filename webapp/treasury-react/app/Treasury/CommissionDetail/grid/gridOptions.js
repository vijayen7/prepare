import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function () {
        return window.location.origin + url + "&_outputKey=commissionDetailList";
      }
    },
    forceFitColumns: true,
    applyFilteringOnGrid: true,

    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    page: true,
    sheetName: "Commission Detail",
    sortList: [
      { columnId: "date", sortAsc: true },
      { columnId: "execBrokerFamily", sortAsc: true },
      { columnId: "execBroker", sortAsc: true },
      { columnId: "descoEntity", sortAsc: true },
      { columnId: "businessUnitName", sortAsc: true },
      { columnId: "gboType", sortAsc: true }
    ]
  };
  return options;
}
