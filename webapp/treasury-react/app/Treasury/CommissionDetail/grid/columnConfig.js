import {
  percentFormatter,
  drillThroughFormatter
} from "commons/grid/formatters";
import { numberComparator } from "commons/grid/comparators";

var columns = [];

export function gridColumns(params) {
  var columns = [
    {
      id: "date",
      name: "Date",
      field: "date",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 80
    },
    {
      id: "execBrokerFamily",
      name: "Exec Broker Family",
      field: "execBrokerFamily",
      toolTip: "Exec Broker Family",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 100
    },
    {
      id: "execBroker",
      name: "Exec Broker",
      field: "execBroker",
      toolTip: "Exec Broker",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 120
    },
    {
      id: "descoEntity",
      name: "Legal Entity",
      field: "descoEntity",
      toolTip: "Legal Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 120
    },
    {
      id: "businessUnitName",
      name: "Business Unit Name",
      field: "businessUnitName",
      toolTip: "Business Unit Name",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 90
    },
    {
      id: "businessUnit",
      name: "Business Unit Description",
      field: "businessUnit",
      toolTip: "Business Unit",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 90
    },
    {
      id: "gboType",
      name: "GBO Type",
      field: "gboType",
      toolTip: "GBO Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 80
    },
    {
      id: "foType",
      name: "FO Type",
      field: "foType",
      toolTip: "FO Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 80
    },
    {
      id: "subtype",
      name: "Subtype",
      field: "subtype",
      toolTip: "Subtype",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 80
    },
    {
      id: "bundleGroup",
      name: "Bundle Group",
      field: "bundleGroupName",
      toolTip: "Bundle Group",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 80
    },
    {
      id: "clearingFeeUSD",
      name: "Clearing Fee",
      field: "clearingFeeUSD",
      toolTip: "Clearing Fee USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "ticketChargesUSD",
      name: "Ticket Charges",
      field: "ticketChargesUSD",
      toolTip: "Ticket Charges USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "explicitCommissionUSD",
      name: "Explicit Commission",
      field: "explicitCommissionUSD",
      toolTip: "Explicit Commission USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "implicitCommissionUSD",
      name: "Implicit Commission",
      field: "implicitCommissionUSD",
      toolTip: "Implicit Commission USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "bankChargeUSD",
      name: "Bank Charge",
      field: "bankChargeUSD",
      toolTip: "Bank Charge USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "custodyFeeUSD",
      name: "Custody Fee",
      field: "custodyFeeUSD",
      toolTip: "Custody Fee USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 70
    },
    {
      id: "source",
      name: "Source",
      field: "source",
      toolTip: "Source",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 70
    },
    {
      id: "totalCommissionUSD",
      name: "Total Commission",
      field: "totalCommissionUSD",
      toolTip: "Total Commission USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      minWidth: 70
    },
    {
      id: "knowledgeStartDate",
      name: "Knowledge Date",
      field: "knowledgeStartDate",
      toolTip: "Knowledge Date",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 120
    }
  ];

  return columns;
}
