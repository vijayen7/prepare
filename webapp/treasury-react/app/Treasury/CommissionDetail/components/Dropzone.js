import React from "react";
import DropzoneInput from "commons/container/DropzoneInput";

const Dropzone = props => {
  return (
    <div className="display--inline-block" style={props.style}>
      <DropzoneInput
        name={props.fileName}
        value={props.files}
        accept={props.allowedFileTypes}
        onChange={props.onFileUpload}
      />
    </div>
  );
};

export default Dropzone;
