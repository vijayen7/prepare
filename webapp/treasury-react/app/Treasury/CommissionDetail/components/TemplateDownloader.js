import React from "react";
import Link from "commons/components/Link";
import { BASE_URL } from "commons/constants";
import { INPUT_TEMPLATE_URL } from "./../constants";

const BankChargeFileTemplateDownloader = props => {
  return (
    <div>
      <Link
        id="downloadTemplate"
        text="Download the file template"
        href={BASE_URL + INPUT_TEMPLATE_URL}
      />
    </div>
  );
};

export default BankChargeFileTemplateDownloader;
