import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import FileUploader from "./containers/FileUploader";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";
import { Panel } from "arc-react-components";

import {
  COMMISSION_DETAIL
} from "../constants";

const CommissionDetail = (props) =>
 {
  if(props.selectedView != COMMISSION_DETAIL) {
    props.updateSelectedView(COMMISSION_DETAIL);
   }
  let bankChargeUploader = null;
  let isBankChargeEnabled = JSON.parse(CODEX_PROPERTIES['treasury.portal.financing.enableBankChargeUpload']);
  if(isBankChargeEnabled){
    bankChargeUploader = (
          <div className="size--1">
            <br />
            <Panel title="Bank Charge File Uploader">
              <FileUploader />
            </Panel>
          </div>
    );
  }
  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName} />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--5">
            <Panel>
                <Grid />
                <br />
                <ul className="bullet">
                  <li>All figures in USD</li>
                  <li>Sign convention is w.r.t. counterparty</li>
                </ul>
            </Panel>
          </div>
          {bankChargeUploader}
        </div>
      </div>
    </div>
  );
};

export default CommissionDetail;
