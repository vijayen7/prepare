export const ALLOWED_FILE_TYPES = ".xlsx";
export const FILE = "file";
export const INPUT_TEMPLATE_URL = "service/bankChargeFileService/downloadBankChargeTemplateFile";
