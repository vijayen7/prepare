import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_COMMISSION_DETAIL_DATA,
  START_LOADING,
  END_LOADING,
  UPLOAD_BANK_CHARGE_DATA
} from "commons/constants";

import { getCommissionDetail, uploadBankChargeFile } from "./api";
import { ToastService } from 'arc-react-components';

function* fetchCommissionDetail(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCommissionDetail, action.payload);
    yield put({ type: `${FETCH_COMMISSION_DETAIL_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_COMMISSION_DETAIL_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* uploadBankChargeData(action) {
  try {
    yield put({ type: START_LOADING });
    const result = yield call(uploadBankChargeFile, action.payload);
    if(result.includes("success")) {
      ToastService.append({
        content: `${result}`,
        type: ToastService.ToastType.SUCCESS,
        placement: ToastService.Placement.TOP_RIGHT,
        dismissTime: 30000
      });
    } else {
      ToastService.append({
        content: `Upload failed: ${result}`,
        type: ToastService.ToastType.CRITICAL,
        placement: ToastService.Placement.TOP_RIGHT,
        dismissTime: 30000
      });
    }
  } catch (e) {
    ToastService.append({
      content: `There was a problem in uploading the file.`,
      type: ToastService.ToastType.CRITICAL,
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 30000
    });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* commissionDetailDataSaga() {
  yield [takeEvery(FETCH_COMMISSION_DETAIL_DATA, fetchCommissionDetail)];
}

export function* uploadBankChargeDataSaga() {
  yield [takeEvery(UPLOAD_BANK_CHARGE_DATA, uploadBankChargeData)];
}
