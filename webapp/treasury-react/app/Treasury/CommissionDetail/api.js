import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
export let url = "";
export function getCommissionDetail(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}data/search-commission-detail?${paramString}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function uploadBankChargeFile(payload) {
  url = `${BASE_URL}service/bankChargeFileService/processBankChargeInputFile`;
  return fetch(url, {
    method: "POST",
    credentials: "include",
    body: payload
  }).then(data => data.json());
}
