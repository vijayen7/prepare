import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";
import ExposureDetailGridContainer from "../ExposureDetail/containers/Grid";

import {
  COUNTERPARTY_EXPOSURE_MONITOR
} from "../constants";

const CounterpartyExposureMonitor = (props) =>
 {
  if(props.selectedView != COUNTERPARTY_EXPOSURE_MONITOR) {
    props.updateSelectedView(COUNTERPARTY_EXPOSURE_MONITOR);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
            <br />
            <ExposureDetailGridContainer drillDownGrid = {true}/>
          </div>
          <br />
          <div>
            <ul className="bullet">
              <li>All figures in USD</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CounterpartyExposureMonitor;
