import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_COUNTERPARTY_EXPOSURE_MONITOR_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";


import { getCounterpartyExposureMonitorData } from "./api";

function* fetchCounterpartyExposureMonitor(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCounterpartyExposureMonitorData, action.payload);
    yield put({ type: `${FETCH_COUNTERPARTY_EXPOSURE_MONITOR_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_COUNTERPARTY_EXPOSURE_MONITOR_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* counterpartyExposureMonitorDataSaga() {
  yield [
    takeEvery(FETCH_COUNTERPARTY_EXPOSURE_MONITOR_DATA, fetchCounterpartyExposureMonitor)
  ];
}
