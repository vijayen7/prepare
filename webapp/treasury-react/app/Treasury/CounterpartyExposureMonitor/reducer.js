import {
  FETCH_COUNTERPARTY_EXPOSURE_MONITOR_DATA,
  DESTROY_DATA_STATE_FOR_COUNTERPARTY_EXPOSURE_MONITOR_VIEW
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function counterpartyExposureMonitorDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_COUNTERPARTY_EXPOSURE_MONITOR_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_COUNTERPARTY_EXPOSURE_MONITOR_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: counterpartyExposureMonitorDataReducer
});

export default rootReducer;
