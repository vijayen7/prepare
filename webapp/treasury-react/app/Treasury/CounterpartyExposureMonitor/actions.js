import {
  BASE_URL,
  FETCH_COUNTERPARTY_EXPOSURE_MONITOR_DATA,
  DESTROY_DATA_STATE_FOR_COUNTERPARTY_EXPOSURE_MONITOR_VIEW
} from "commons/constants";

export function fetchCounterpartyExposureMonitor(payload) {
  return {
    type: FETCH_COUNTERPARTY_EXPOSURE_MONITOR_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_COUNTERPARTY_EXPOSURE_MONITOR_VIEW
  };
}
