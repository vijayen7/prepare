import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import Error from "commons/components/Error";
import FilterButton from "commons/components/FilterButton";
import CpeFamilyFilter from "commons/container/CpeFamilyFilter";
import OwnershipEntityFilter from "commons/container/OwnershipEntityFilter";
import { fetchCounterpartyExposureMonitor } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  getDefaultFilters() {
    return {
      selectedCpeFamilies: [],
      selectedOwnershipEntities: [],
      selectedDate: ""
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

applySavedFilters(selectedFilters) {
  this.setState(selectedFilters);
}

onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    var payload = {
      dateString: this.state.selectedDate,
      cpeFamilyIds: getCommaSeparatedValues(this.state.selectedCpeFamilies),
      ownershipEntityIds: getCommaSeparatedValues(
        this.state.selectedOwnershipEntities
      )
    };

    this.props.updateDrillDownSelectedFilters(this.state);
    this.props.fetchCounterpartyExposureMonitor(payload);
  }

  render() {
    return (
      <Sidebar>

        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
          label="Date"
        />
        <CpeFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpeFamilies}
        />
        <OwnershipEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedOwnershipEntities}
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCounterpartyExposureMonitor,
      updateDrillDownSelectedFilters
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
