import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CounterpartyExposureMonitorGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";
import { fetchExposureDetailData } from "../../ExposureDetail/actions";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  onCellClickHandler = args => {
    if (args.colId === "equity") {
      var cpeFamilyName = "cpeFamilyIds";
      var cpeFamilyIds = JSON.parse(args.item.equity.params).cpeFamilyIds

      var selectedCpeFamilies = [];
      for (var i in cpeFamilyIds) {
        selectedCpeFamilies.push({
          key: cpeFamilyIds[i],
          value: cpeFamilyName
        });
      }

      var payload = {
        dateString: this.props.selectedFilters.selectedDate,
        subtotalField: "1",
        cpeFamilyIds: getCommaSeparatedValues(selectedCpeFamilies),
        ownershipEntityIds: getCommaSeparatedValues(
          this.props.selectedFilters.selectedOwnershipEntities
        ),
        descoEntityIds: "-1",
        agreementTypeIds: "-1"
      };
      this.props.fetchExposureDetailData(payload);
    }
  };

  renderGridData() {
    let grid = null;
    if (
      !this.props.counterpartyExposureMonitorData.hasOwnProperty(
        "resultList"
      ) ||
      this.props.counterpartyExposureMonitorData.resultList.length <= 0
    )
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <CounterpartyExposureMonitorGrid
        data={this.props.counterpartyExposureMonitorData.resultList}
        onCellClick={this.onCellClickHandler}
        gridId="counterpartyExposureMonitor"
        gridColumns={gridColumns(
          this.props.counterpartyExposureMonitorData["metaData"]["columns"]
        )}
        gridOptions={gridOptions(
          gridColumns(
            this.props.counterpartyExposureMonitorData["metaData"]["columns"]
          )
        )}
        label="Counterparty Exposure Monitor"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchExposureDetailData,
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    counterpartyExposureMonitorData:
      state.treasury.counterpartyExposureMonitorView.data,
    selectedFilters: state.treasury.drillDownFilters
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
