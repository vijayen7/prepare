import {
  percentFormatter,
  decimalToPercentFormatter,
  priceFormatter,
  drillThroughFormatter,
  spreadFormatter
} from "commons/grid/formatters";
import {
  linkSumAggregator,
  sumIgnoringUndefinedAggregator
} from "commons/grid/aggregators";
import { numberComparator } from "commons/grid/comparators";

var columns = [];

export function gridColumns(columnHeaders) {
  var columns = [
    {
      id: "cpe",
      name: "Counterparty Family",
      attribute: "",
      headerCssClass: "b",
      field: "cpe",
      sortable: true,
      autoWidth: true,
      minWidth: 100,
      maxWidth: 180
    }
  ];
  var ratingColumns = ["snpRating", "moodysRating", "fitchRating"];

  for (var i = 0; i < columnHeaders.length; i++) {
    if (columnHeaders[i]["id"] === "equity") {
      columns.push({
        id: columnHeaders[i]["id"],
        name: columnHeaders[i]["name"],
        headerCssClass: "aln-cntr b",
        field: columnHeaders[i]["field"],
        attribute: columnHeaders[i]["attribute"],
        type: "number",
        aggregator: linkSumAggregator,
        sortable: true,
        minWidth: 90,
        autoWidth: true,
        filter: false,
        comparator: function(a, b) {
          return numberComparator(a[sortcol]["value"], b[sortcol]["value"]);
        },
        excelDataFormatter: function(equityField) {
          return equityField["value"];
        },
        formatter: function(row, cell, value, columnDef, dataContext) {
          return drillThroughFormatter(
            row,
            cell,
            value["value"],
            columnDef,
            undefined,
            dpGrid.Formatters.Number,
            undefined,
            value["params"]
          );
        }
      });
    } else if (columnHeaders[i]["id"] === "equityNavRatio") {
      columns.push({
        id: columnHeaders[i]["id"],
        name: columnHeaders[i]["name"],
        headerCssClass: "aln-cntr b",
        field: columnHeaders[i]["field"],
        attribute: columnHeaders[i]["attribute"],
        type: "number",
        aggregator: sumIgnoringUndefinedAggregator,
        sortable: true,
        minWidth: 90,
        autoWidth: true,
        formatter: dpGrid.Formatters.Number,
        excelFormatter: "#,##0"
      });
    } else if (ratingColumns.indexOf(columnHeaders[i]["id"]) >= 0) {
      // Rating columns configuration
      columns.push({
        id: columnHeaders[i]["id"],
        name: columnHeaders[i]["name"],
        attribute: columnHeaders[i]["attribute"],
        headerCssClass: "aln-cntr b",
        cssClass: "aln-cntr",
        field: columnHeaders[i]["field"],
        aggregator: undefined,
        sortable: true,
        autoWidth: true,
        minWidth: 90
      });
    } else if (
      columnHeaders[i]["id"] === "cdsFiveYearMid" ||
      columnHeaders[i]["id"] === "52wHighestFiveYearSpread"
    ) {
      columns.push({
        id: columnHeaders[i]["id"],
        name: columnHeaders[i]["name"],
        headerCssClass: "aln-cntr b",
        field: columnHeaders[i]["field"],
        attribute: columnHeaders[i]["attribute"],
        type: "number",
        aggregator: undefined,
        sortable: true,
        minWidth: 90,
        autoWidth: true,
        formatter: spreadFormatter,
        excelFormatter: "#,##0.00"
      });
    } else {
      columns.push({
        id: columnHeaders[i]["id"],
        name: columnHeaders[i]["name"],
        headerCssClass: "aln-cntr b",
        field: columnHeaders[i]["field"],
        attribute: columnHeaders[i]["attribute"],
        type: "number",
        aggregator: undefined,
        sortable: true,
        minWidth: 90,
        autoWidth: true,
        formatter: dpGrid.Formatters.Number,
        excelFormatter: "#,##0.0"
      });
    }
  }

  var percentColumns = [
    "cdsOneMonthChange",
    "equityPriceYearLowVsCurrent",
    "equityPriceOneMonthChange",
    "equityNavRatio"
  ];
  var priceColumns = [
    "equityPriceCurrent",
    "equityPriceMonthAgo",
    "equityPriceOneYearLow"
  ];

  for (var i = 0; i < columns.length; i++) {
    //shortInterestOneMonthChange -> no multiplication required
    if (columns[i].id === "shortInterestOneMonthChange") {
      columns[i].formatter = percentFormatter;
    } else if (percentColumns.indexOf(columns[i].id) >= 0) {
      //equityPriceOneMonthChange
      //equityPriceYearLowVsCurrent
      //cdsOneMonthChange
      //equityNavRatio
      columns[i].formatter = decimalToPercentFormatter;
    } else if (priceColumns.indexOf(columns[i].id) >= 0) {
      //equityPriceCurrent
      //equityPriceMonthAgo
      //equityPriceOneYearLow
      columns[i].formatter = priceFormatter;
    }
  }
  return columns;
}
