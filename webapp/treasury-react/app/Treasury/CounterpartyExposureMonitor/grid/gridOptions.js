import {
  url
} from "../api";
export function gridOptions(columns) {
  var groupColumns = [];
  var i = 0;
  while (i < columns.length) {
    // [GroupName, firstColumn's field, lastColumn's field]
    var j = i;
    while (
      j < columns.length &&
      columns[i]["attribute"] == columns[j]["attribute"]
    ) {
      j++;
    }
    groupColumns.push([
      columns[i]["attribute"],
      columns[i]["field"],
      columns[j - 1]["field"]
    ]);
    i = j;
  }

  var options = {
    summaryRowText: "Total",
    summaryRow: true,
    displaySummaryRow: true,
    exportToExcel: true,
    applyFilteringOnGrid: true,
    page: true,
    sheetName: "Counterparty Family Exposure Summary",
    groupColumns: groupColumns,
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=resultList";
      }
    }
  };
  return options;
}
