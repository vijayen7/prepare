import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";

import {
  FINANCING_DETAIL
} from "../constants";

const FinancingDetail = (props) =>
 {
  if(props.selectedView != FINANCING_DETAIL) {
    props.updateSelectedView(FINANCING_DETAIL);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
          </div>
          <br />
          <div>
            <ul className="bullet">
              <li>All figures in USD</li>
              <li>Sign convention is w.r.t. counterparty</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FinancingDetail;
