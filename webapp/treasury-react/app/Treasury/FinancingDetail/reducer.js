import {
  FETCH_FINANCING_DETAIL_DATA,
  DESTROY_DATA_STATE_FOR_FINANCING_DETAIL_VIEW 
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function financingDetailDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_FINANCING_DETAIL_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_FINANCING_DETAIL_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: financingDetailDataReducer
});

export default rootReducer;
