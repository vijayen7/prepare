import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url+ "&_outputKey=financingDetailList";
      }
    },
    forceFitColumns: true,
    applyFilteringOnGrid: true,
    autoHorizontalScrollBar: true,
    frozenColumn: 4,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    page: true,
    sheetName: "Financing Detail",
    sortList: [
      { columnId: "date", sortAsc: true },
      { columnId: "cpeFamily", sortAsc: true },
      { columnId: "cpe", sortAsc: true },
      { columnId: "descoEntity", sortAsc: true },
      { columnId: "businessUnitName", sortAsc: true },
      { columnId: "agreementType", sortAsc: true }
    ],
    customColumnSelection: {
      defaultcolumns: getDefaultColumns(),
      totalColumns: getTotalColumns()
    },
    groupColumns :[ ['Spread Only Financing (USD)', 'financingUSD', 'total'], ['Gross Financing (USD)', 'grossCashFinancing', 'grossTotalFinancing'] ]
  };
  return options;
}

function getDefaultColumns() {
  let defaultColumns = [
    "date",
    "cpeFamily",
    "cpe",
    "descoEntity",
    "agreementType",
    "businessUnitName",
    "businessUnit",
    "bundleGroup",
    "financingUSD",
    "trsFinancingUSD",
    "repoFinancingUSD",
    "total"
  ];

  let clientName = CODEX_PROPERTIES["codex.client_name"];
  if (clientName !== "brdreach" && clientName !== "tbird") {
    defaultColumns = defaultColumns.concat([
    "grossCashFinancing",
    "adjustedCashFinancing",
    "grossSwapFinancingDTD",
    "grossSwapFinancingMTD",
    "grossSwapFinancingQTD",
    "grossRepoFinancingDTD",
    "grossRepoFinancingMTD",
    "grossRepoFinancingQTD",
    "grossTotalFinancing"
    ])
  }
  return defaultColumns;
}

function getTotalColumns() {
  let totalColumns = [
    "date",
    "cpeFamily",
    "cpe",
    "descoEntity",
    "agreementType",
    "businessUnitName",
    "businessUnit",
    "bundleGroup",
    "financingUSD",
    "pbFinancingUSD",
    "trsFinancingUSD",
    "repoFinancingUSD",
    "slaFinancingUSD",
    "futuresFinancingUSD",
    "total",
    "grossCashFinancing",
    "adjustedCashFinancing",
    "grossPbFinancing",
    "adjustedPbFinancing",
    "grossSlaFinancing",
    "adjustedSlaFinancing",
    "grossFuturesFinancing",
    "adjustedFuturesFinancing",
    "grossSwapFinancingDTD",
    "grossSwapFinancingMTD",
    "grossSwapFinancingQTD",
    "grossRepoFinancingDTD",
    "grossRepoFinancingMTD",
    "grossRepoFinancingQTD",
    "grossTotalFinancing"
  ];
  return totalColumns;
}
