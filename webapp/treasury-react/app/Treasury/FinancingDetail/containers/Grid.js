import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FinancingDetaiGrid from "commons/components/GridWithCellClick";
import { URL } from 'commons/constants';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import { Layout } from "arc-react-components";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  onCellClickHandler = args => {

  }

  renderLinkPanel() {
    return (
      <LinkPanel
        links={[
          <Link
            id="cofi-repo-financing-rule"
            text="View Repo Spread Only Financing Rules"
            href={`${URL}/treasury/financing/cofiRepoFinancing.html`}
          />
        ]}
      />
    );
  }

  renderGridData() {
    let grid = null;

    if (
      !this.props.financingDetailData.hasOwnProperty("financingDetailList") ||
      this.props.financingDetailData.financingDetailList.length <= 0
    )
      if (this.props.drillDownGrid) {
        return null;
      } else
        return (
          <Message messageData="No data available for this search criteria." />
        );
    grid = (
      <React.Fragment>
        <Layout>
          <Layout.Child>
            <FinancingDetaiGrid
              data={this.props.financingDetailData.financingDetailList}
              gridId="FinancingDetail"
              gridColumns={gridColumns()}
              gridOptions={gridOptions()}
              onCellClick={this.onCellClickHandler}
            />
          </Layout.Child>
          <Layout.Child>
            {this.renderLinkPanel()}
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    financingDetailData: state.treasury.financingDetailView.data,
    drillDownFilters: state.treasury.drillDownFilters
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
