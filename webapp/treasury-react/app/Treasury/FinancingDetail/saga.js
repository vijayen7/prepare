import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_FINANCING_DETAIL_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getFinancingDetail } from "./api";

function* fetchFinancingDetail(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getFinancingDetail, action.payload);
    yield put({ type: `${FETCH_FINANCING_DETAIL_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_FINANCING_DETAIL_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* financingDetailDataSaga() {
  yield [takeEvery(FETCH_FINANCING_DETAIL_DATA, fetchFinancingDetail)];
}
