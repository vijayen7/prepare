import {
  BASE_URL,
  FETCH_FINANCING_DETAIL_DATA,
  DESTROY_DATA_STATE_FOR_FINANCING_DETAIL_VIEW
} from "commons/constants";

export function fetchFinancingDetail(payload) {
  return {
    type: FETCH_FINANCING_DETAIL_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_FINANCING_DETAIL_VIEW,
  };
}
