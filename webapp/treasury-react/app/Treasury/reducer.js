import { combineReducers } from "redux";
import { UPDATE_DRILL_DOWN_SELECTED_FILTERS } from "commons/constants";
import ownershipViewReducer from "Treasury/OwnershipBUSummary/reducer";
import cashSummaryViewReducer from "Treasury/CashSummaryView/reducer";
import positionDetailViewReducer from "Treasury/PositionDetailView/reducer";
import BUCPEFamilyViewReducer from "Treasury/BUCPEFamily/reducer";
import exposureCoverageViewReducer from "Treasury/ExposureCoverageView/reducer";
import exposureDetailViewReducer from "Treasury/ExposureDetail/reducer";
import totalExposureViewReducer from "Treasury/TotalExposureView/reducer";
import liquidIlliquidViewReducer from "Treasury/LiquidIlliquid/reducer";
import individualBrokerViewReducer from "Treasury/IndividualCounterpartyView/reducer";
import counterpartyRevenueSummaryDataReducer from "Treasury/CounterpartyRevenueSummary/reducer";
import financingDetailDataReducer from "Treasury/FinancingDetail/reducer";
import commissionDetailDataReducer from "Treasury/CommissionDetail/reducer";
import instrumentTypeViewReducer from "Treasury/InstrumentTypeView/reducer";
import genericBuViewReducer from "Treasury/GenericBUView/reducer";
import summaryByCounterpartyReducer from "Treasury/SummaryByCounterparty/reducer";
import interactionSummaryReducer  from "Treasury/InteractionSummary/reducer";
import cashExposureReportReducer from "Treasury/CashExposureReport/reducer";
import cashReportReducer from "Treasury/CashReport/reducer";
import counterpartyExposureMonitorReducer from "Treasury/CounterpartyExposureMonitor/reducer";

function drillDownSelectedFilterReducer(state = {}, action) {
  switch (action.type) {
    case `${UPDATE_DRILL_DOWN_SELECTED_FILTERS}_SUCCESS`:
      return action.selectedFilters || {};
  }
  return state;
}

const rootReducer = combineReducers({
  ownershipView: ownershipViewReducer,
  cashSummaryView: cashSummaryViewReducer,
  positionDetailView: positionDetailViewReducer,
  BUCPEFamilyView: BUCPEFamilyViewReducer,
  exposureCoverageView: exposureCoverageViewReducer,
  totalExposureView: totalExposureViewReducer,
  exposureDetailView: exposureDetailViewReducer,
  liquidIlliquidView: liquidIlliquidViewReducer,
  individualBrokerView: individualBrokerViewReducer,
  counterpartyRevenueSummaryView: counterpartyRevenueSummaryDataReducer,
  financingDetailView: financingDetailDataReducer,
  commissionDetailView: commissionDetailDataReducer,
  instrumentTypeView: instrumentTypeViewReducer,
  genericBuView: genericBuViewReducer,
  summaryByCounterpartyView: summaryByCounterpartyReducer,
  interactionSummaryView: interactionSummaryReducer,
  cashExposureReportView: cashExposureReportReducer,
  cashReportView: cashReportReducer,
  counterpartyExposureMonitorView: counterpartyExposureMonitorReducer,
  drillDownFilters: drillDownSelectedFilterReducer
});

export default rootReducer;
