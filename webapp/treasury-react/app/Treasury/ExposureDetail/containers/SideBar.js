import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import OwnershipEntityFilter from "commons/container/OwnershipEntityFilter";
import CpeFamilyFilter from "commons/container/CpeFamilyFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import FilterButton from "commons/components/FilterButton";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import { fetchExposureDetailData } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import { destroyDataState as positionDetailDestroyDataState } from "../../PositionDetailView/actions";
import _ from "lodash";
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  getDefaultFilters() {
    return {
      selectedLegalEntities: getCodexFilters("LEGAL_ENTITIES"),
      selectedOwnershipEntities: getCodexFilters("OWNERSHIP_ENTITIES"),
      selectedSubtotal: { key: 1, value: "Legal Entity" },
      selectedCpeFamilies: [],
      selectedAgreementTypes: [],
      selectedDate: getCodexFilters("DATE"),
      subtotal: [
        { key: 1, value: "Legal Entity" },
        { key: 2, value: "Agreement Type" }
      ]
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    this.props.positionDetailDestroyDataState();
    var payload = {
      dateString: this.state.selectedDate,
      subtotalField: this.state.selectedSubtotal.key,
      cpeFamilyIds: getCommaSeparatedValues(this.state.selectedCpeFamilies),
      ownershipEntityIds: getCommaSeparatedValues(
        this.state.selectedOwnershipEntities
      ),
      descoEntityIds: getCommaSeparatedValues(this.state.selectedLegalEntities),
      agreementTypeIds: getCommaSeparatedValues(
        this.state.selectedAgreementTypes
      )
    };

    this.props.updateDrillDownSelectedFilters(this.state);
    this.props.fetchExposureDetailData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
          dateType ="tMinusTwo"
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <OwnershipEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedOwnershipEntities}
        />
        <CpeFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpeFamilies}
        />
        <AgreementTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedAgreementTypes}
        />
        <SingleSelectFilter
          data={this.state.subtotal}
          onSelect={this.onSelect}
          selectedData={this.state.selectedSubtotal}
          stateKey="selectedSubtotal"
          label="Subtotal On"
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />

          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchExposureDetailData,
      updateDrillDownSelectedFilters,
      positionDetailDestroyDataState
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
