import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ExposureDetailGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";
import { fetchPositionDetailData } from "../../PositionDetailView/actions";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.parsingDrillDownParameters = this.parsingDrillDownParameters.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  parsingDrillDownParameters(parameterString, payloadType) {
    let idList = parameterString
      .split(": ")[1]
      .substring(1, parameterString.split(": ")[1].length - 1)
      .split(",");

    let idListPayload = [];
    for (var i in idList) {
      idListPayload.push({
        key: parseInt(idList[i]),
        value: payloadType
      });
    }
    return idListPayload;
  }

  onCellClickHandler = args => {
    if (args.colId === "usage_utp" && !this.props.drillDownGrid) {
      var selectedFilterValues = Object.assign({}, this.props.selectedFilters);
      var drillthroughParameters = args.item.usage_utp.params.substring(
        1,
        args.item.usage_utp.params.length - 1
      );
      var drillthroughParameterString = drillthroughParameters.split(",");

      selectedFilterValues.selectedCpeFamilies = this.parsingDrillDownParameters(
        drillthroughParameterString[0],
        "cpeFamilyIds"
      );

      if (drillthroughParameterString.length > 1) {
        selectedFilterValues.selectedLegalEntities = this.parsingDrillDownParameters(
          drillthroughParameterString[1],
          "descoEntityIds"
        );
      }

      if (drillthroughParameterString.length > 2) {
        selectedFilterValues.selectedAgreementTypes = this.parsingDrillDownParameters(
          drillthroughParameterString[2],
          "agreementTypeIds"
        );
      }

      var payload = {
        dateString: selectedFilterValues.selectedDate,
        cpeIds: "-1",
        cpeFamilyIds: getCommaSeparatedValues(
          selectedFilterValues.selectedCpeFamilies
        ),
        agreementTypeIds: getCommaSeparatedValues(
          selectedFilterValues.selectedAgreementTypes
        ),
        assetLiquidityIds: "2,3",
        positionGranularity: "BUNDLE",
        financingStatusTypeIds: "1",
        ownershipEntityIds: getCommaSeparatedValues(
          selectedFilterValues.selectedOwnershipEntities
        ),
        descoEntityIds: getCommaSeparatedValues(
          selectedFilterValues.selectedLegalEntities
        ),
        businessUnitIds: "-1"
      };

      this.props.fetchPositionDetailData(payload);
    }
  };

  renderGridData() {
    let grid = null;
    if (
      !this.props.exposureDetailData.hasOwnProperty("exposureDetailData") ||
      this.props.exposureDetailData.exposureDetailData.length <= 0
    ) {
      if (this.props.drillDownGrid) {
        return null;
      } else
        return (
          <Message messageData="No data available for this search criteria." />
        );
    }
    grid = (
      <ExposureDetailGrid
        data={this.props.exposureDetailData.exposureDetailData}
        onCellClick={this.onCellClickHandler}
        gridId="exposureDetail"
        gridColumns={gridColumns(this.props)}
        gridOptions={gridOptions()}
        label="Exposure Detail"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchPositionDetailData,
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    exposureDetailData: state.treasury.exposureDetailView.data,
    selectedFilters: state.treasury.drillDownFilters
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
