import { BASE_URL } from "commons/constants";
export let url = "";
export function getPositionDetailData(payload) {
  url = `${BASE_URL}data/search-exposure-detail-data?dateString=${
    payload.dateString
  }&subtotalField=${payload.subtotalField}&cpeFamilyIds=${
    payload.cpeFamilyIds
  }&ownershipEntityIds=${payload.ownershipEntityIds}&descoEntityIds=${
    payload.descoEntityIds
  }&agreementTypeIds=${payload.agreementTypeIds}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
