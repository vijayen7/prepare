import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_EXPOSURE_DETAIL_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getPositionDetailData } from "./api";

function* fetchExposureDetailData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getPositionDetailData, action.payload);
    yield put({ type: `${FETCH_EXPOSURE_DETAIL_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_EXPOSURE_DETAIL_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* exposureDetailDataSaga() {
  yield [
    takeEvery(FETCH_EXPOSURE_DETAIL_DATA, fetchExposureDetailData)
  ];
}
