import {
  FETCH_EXPOSURE_DETAIL_DATA,
  DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW
} from "commons/constants";
import { combineReducers } from "redux";

function exposureDetailDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_EXPOSURE_DETAIL_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW:
      return [];
  }
  return state;
}


const rootReducer = combineReducers({
  data: exposureDetailDataReducer
});

export default rootReducer;
