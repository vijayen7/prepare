import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";
import PositionDetailGridContainer from "../PositionDetailView/containers/Grid";

import {
  EXPOSURE_DETAIL
} from "../constants";

const ExposureDetail = (props) =>
 {
  if(props.selectedView != EXPOSURE_DETAIL) {
    props.updateSelectedView(EXPOSURE_DETAIL);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
        </div>
        <div className="size--content">
          <Grid />
          <br />
          <PositionDetailGridContainer drillDownGrid = {true}/>
        </div>
        <br />
        <div>
          <ul className="bullet">
            <li>All figures in USD</li>
            <li>
              Notes:<ul className="bullet">
                <li>
                  <small>
                    This gadget captures equity and usage numbers at a trading
                    entity level.
                  </small>
                </li>
                <li>
                  <small>
                    Data in this gadget is derived from collateral management
                    workflows except for Usage (unadjusted) which flows in from
                    margin calculations.
                  </small>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ExposureDetail;
