import {
  BASE_URL,
  FETCH_EXPOSURE_DETAIL_DATA,
  DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW
} from "commons/constants";

export function fetchExposureDetailData(payload) {
  return {
    type: FETCH_EXPOSURE_DETAIL_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW
  };
}
