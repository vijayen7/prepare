import {
  percentFormatter,
  drillThroughFormatter
} from "../../../commons/grid/formatters";

import {
  linkSumAggregator
} from "../../../commons/grid/aggregators";

import {
  numberComparator
} from "../../../commons/grid/comparators";

var columns = [];

export function gridColumns(params) {
  var columns = [{
    id: "cpe_family",
    name: "Counterparty Entity Family",
    headerCssClass: "b",
    field: "cpe_family",
    type: "text",
    sortable: true,
    minWidth: 160
  },
  {
    id: "cpe",
    name: "Counterparty Entity",
    field: "cpe",
    type: "text",
    sortable: true,
    headerCssClass: "b",
    minWidth: 130
  },
  {
    id: "agreement_type",
    name: "Agreement Type",
    field: "agreement_type",
    type: "text",
    sortable: true,
    headerCssClass: "b",
    minWidth: 100
  },
  {
    id: "desco_entity",
    name: "Legal Entity",
    field: "desco_entity",
    type: "text",
    sortable: true,
    headerCssClass: "b",
    minWidth: 150
  },
  {
    id: "usage_comet",
    name: "Usage (excl. segregated margin)",
    field: "usage_comet",
    type: "number",
    toolTip: "Usage (excl. segregated margin) refers to the total amount of capital required to hold a position. In most cases, usage is equivalent to margin, but usage also includes the net option value under Futures & Options agreement, accrued interest and bad debt expense. Loan received under a variable funding note arrangement is reflected as a reduction in usage. Segregated margin is excluded from Usage reported here.",
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol], b[sortcol]);
    },
    minWidth: 90
  },
  {
    id: "segregated_margin",
    name: "Segregated Margin",
    field: "segregated_margin",
    type: "number",
    sortable: true,
    toolTip: "Segregated Margin refers to the initial margin requirement housed under segregated ISDA arrangements. The collateral to cover the margin requirement is posted to a custodian bank like BONY or NTRS.",
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol], b[sortcol]);
    },
    minWidth: 90
  },
  {
    id: "total_usage",
    name: "Total Usage",
    field: "total_usage",
    type: "number",
    toolTip: "Total Usage : Usage (excl. segregated margin) + Segregated Margin ",
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol], b[sortcol]);
    },
    minWidth: 90
  },
  {
    id: "usage_utp",
    name: "Total Usage (Unadjusted)",
    field: "usage_utp",
    type: "number",
    toolTip: "Total Usage (Unadjusted) refers to usage data taken directly from the margin calculations, and excludes adjustments published from collateral management workflows.",
    sortable: true,
    formatter: function (row, cell, value, columnDef, dataContext) {
      return drillThroughFormatter(
        row,
        cell,
        value["value"],
        columnDef,
        dataContext,
        dpGrid.Formatters.Number,
        () => !params.drillDownGrid,
        value["params"]
      );
    },
    aggregator: linkSumAggregator,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol]["value"], b[sortcol]["value"]);
    },
    minWidth: 90
  },
  {
    id: "equity",
    name: "Equity",
    field: "equity",
    type: "number",
    sortable: true,
    toolTip: "Equity refers to the firms net exposure to the counterparty. It is a summation of mark to market/ exposure and collateral after adjusting for the impact of collateral in transit.",
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol], b[sortcol]);
    },
    minWidth: 90
  },
  {
    id: "reg_cpe_equity",
    name: "Reg Cpe Equity",
    field: "reg_cpe_equity",
    type: "number",
    sortable: true,
    toolTip: "Reg Cpe Equity",
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol], b[sortcol]);
    },
    minWidth: 90
  },
  {
    id: "reg_cpe_margin",
    name: "Reg Cpe Margin",
    field: "reg_cpe_margin",
    type: "number",
    sortable: true,
    toolTip: "Reg Cpe Margin",
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol], b[sortcol]);
    },
    minWidth: 90
  },
  {
    id: "excess_deficit",
    name: "Excess / Deficit",
    field: "excess_deficit",
    type: "number",
    sortable: true,
    toolTip: "Excess / Deficit refers to internal view of the excess or deficit collateral balance maintained at the counterparty and excludes collateral in transit.",
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol], b[sortcol]);
    },
    minWidth: 90
  },
  {
    id: "valuation_difference",
    name: "Valuation Difference",
    field: "valuation_difference",
    type: "number",
    toolTip: "Valuation Difference captures counterparty level exposure differences between internal and external numbers.",
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    comparator: function (a, b) {
      return numberComparator(a[sortcol], b[sortcol]);
    },
    minWidth: 90
  }
  ];

  return columns;
}
