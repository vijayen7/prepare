import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=exposureDetailData";
      }
    },
    nestedTable: true,
    nestedField: "cpe_family",
    expandCollapseAll: true,
    summaryRowText: "Total",
    expandTillLevel: -1,
     
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    exportToExcel: true,
    page: true,
    sheetName: "Exposure Detail Report"
  };
  return options;
}
