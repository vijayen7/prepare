import {
  BASE_URL,
  FETCH_SUMMARY_BY_COUNTERPARTY_DATA,
  DESTROY_DATA_STATE_FOR_SUMMARY_BY_COUNTERPARTY_VIEW
} from "commons/constants";

export function fetchSummaryByCounterparty(payload) {
  return {
    type: FETCH_SUMMARY_BY_COUNTERPARTY_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_SUMMARY_BY_COUNTERPARTY_VIEW,
  };
}
