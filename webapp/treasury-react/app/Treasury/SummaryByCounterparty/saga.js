import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_SUMMARY_BY_COUNTERPARTY_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getSummaryByCounterparty } from "./api";

function* fetchSummaryByCounterparty(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getSummaryByCounterparty, action.payload);
    yield put({ type: `${FETCH_SUMMARY_BY_COUNTERPARTY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SUMMARY_BY_COUNTERPARTY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* summaryByCounterpartyDataSaga() {
  yield [
    takeEvery(FETCH_SUMMARY_BY_COUNTERPARTY_DATA, fetchSummaryByCounterparty)
  ];
}
