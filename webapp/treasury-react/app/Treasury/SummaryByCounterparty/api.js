import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
export let url = "";
export function getSummaryByCounterparty(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}data/search-summary-by-broker?${paramString}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
