import {
  FETCH_SUMMARY_BY_COUNTERPARTY_DATA,
  DESTROY_DATA_STATE_FOR_SUMMARY_BY_COUNTERPARTY_VIEW
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function summaryByCounterPartyDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SUMMARY_BY_COUNTERPARTY_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_SUMMARY_BY_COUNTERPARTY_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: summaryByCounterPartyDataReducer
});

export default rootReducer;
