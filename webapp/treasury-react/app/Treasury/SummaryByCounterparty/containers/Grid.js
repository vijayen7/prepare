import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SummaryByCounterpartyGrid from "commons/components/Grid";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  renderGridData() {
    let grid = null;
    if (
      !this.props.summaryByCounterpartyData.hasOwnProperty("resultList") ||
      this.props.summaryByCounterpartyData.resultList.length <= 0
    )
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <div>
        <SummaryByCounterpartyGrid
          data={this.props.summaryByCounterpartyData.resultList}
          gridId="summaryByCounterpartyData"
          gridColumns={gridColumns(
            this.props.summaryByCounterpartyData.metaData.columns
          )}
          gridOptions={gridOptions()}
          label = "Summary By Counterparty Data"
        />
        <div>
          <ul className="bullet">
            <li>All figures in USD</li>
            <li>
              The numbers are updated and verified up until {this.props.summaryByCounterpartyData.metaData.asOfDate}
            </li>
            <li>
              The numbers are annualized based on year to date data and may be
              materially higher or lower than prior year if there are expense
              fluctuations in the earlier months of the year.
            </li>
          </ul>
        </div>
      </div>
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    summaryByCounterpartyData: state.treasury.summaryByCounterpartyView.data
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
