import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import moment from "moment";
import MonthFilter from "commons/components/MonthFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import Error from "commons/components/Error";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import InputFilter from "commons/components/InputFilter";
import ExecBrokerFamiliesFilter from "commons/container/ExecBrokerFamiliesFilter";
import CounterpartyRevenueCheckboxFilters from "commons/components/CounterpartyRevenueCheckboxFilters";
import { fetchSummaryByCounterparty } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import LegalEntityFamilyFilter from '../../../commons/container/LegalEntityFamilyFilter';
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  getDefaultFilters() {
    return {
      selectedLegalEntityFamilies: getCodexFilters("LEGAL_ENTITY_FAMILIES"),
      selectedExecBrokerFamilies: [],
      selectedStartMonth: moment().startOf('y').format('MM/YYYY'),
      selectedEndMonth: moment().endOf('y').format('MM/YYYY'),
      showAnnualized: true,
      explicitCommission: true,
      implicitCommission: true,
      financing: true,
      clearingFee: true,
      ticketCharges: true,
      bankCharge: true,
      custodyFee: true,
      topNCpes: "10"
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

applySavedFilters(selectedFilters) {
  this.setState(selectedFilters);
}

onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    var startMonthString = this.state.selectedStartMonth;
    var endMonthString = this.state.selectedEndMonth;
    var payload = {
      startMonth: startMonthString.split("/")[0],
      startYear: startMonthString.split("/")[1],
      endMonth: endMonthString.split("/")[0],
      endYear: endMonthString.split("/")[1],
      descoEntityFamilyIds: getCommaSeparatedValues(
        this.state.selectedLegalEntityFamilies
      ),
      cpeFamilyIds: getCommaSeparatedValues(this.state.selectedExecBrokerFamilies),
      showAnnualized: this.state.showAnnualized,
      includeExplicitCommission: this.state.explicitCommission,
      includeImplicitCommission: this.state.implicitCommission,
      includeFinancing: this.state.financing,
      includeClearingFee: this.state.clearingFee,
      includeTicketCharges: this.state.ticketCharges,
      includeBankCharge: this.state.bankCharge,
      includeCustodyFee: this.state.custodyFee,
      topN: this.state.topNCpes
    };
    this.props.fetchSummaryByCounterparty(payload);
  }

  render() {
    let dateError = null;
    var startMonthString = this.state.selectedStartMonth;
    console.log(startMonthString.split("/")[1]);
    var endMonthString = this.state.selectedEndMonth;
    if (startMonthString.split("/")[1] > endMonthString.split("/")[1]) {
      dateError = <Error messageData="Start date is greater than End date" />;
    } else {
      if (startMonthString.split("/")[0] > endMonthString.split("/")[0] && startMonthString.split("/")[1] == endMonthString.split("/")[1])
        dateError = <Error messageData="Start date is greater than End date" />;
      else dateError = null;
    }

    return (
      <Sidebar>
        {dateError}
        <MonthFilter
          onSelect={this.onSelect}
          stateKey="selectedStartMonth"
          data={this.state.selectedStartMonth}
          label="Start Month"
        />
        <MonthFilter
          onSelect={this.onSelect}
          stateKey="selectedEndMonth"
          data={this.state.selectedEndMonth}
          label="End Month"
        />
        <ExecBrokerFamiliesFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedExecBrokerFamilies}
          label = "CPE Families"
        />
        <LegalEntityFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntityFamilies}
        />
        <CounterpartyRevenueCheckboxFilters
          state={this.state}
          onSelect={this.onSelect}
        />
        <InputFilter
          data={this.state.topNCpes}
          onSelect={this.onSelect}
          stateKey="topNCpes"
          label="Top N CPEs"
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            disabled={dateError !== null}
            label="Search"
          />

          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchSummaryByCounterparty,
      updateDrillDownSelectedFilters
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
