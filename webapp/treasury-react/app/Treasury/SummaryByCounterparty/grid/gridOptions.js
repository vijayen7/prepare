import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=resultList"; 
      }
    },
    frozenColumn: 1,
    autoHorizontalScrollBar: true,
    summaryRowText: "Total",
     
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    exportToExcel: true,
    page: true,
    sheetName: "Summary By Broker"
  };
  return options;
}
