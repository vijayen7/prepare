import {
  millionFormatter,
  drillThroughFormatter
} from "commons/grid/formatters";
import { numberComparator } from "commons/grid/comparators";

var columns = [];

export function gridColumns(columnNames) {
  var columns = [
    {
      id: "cpeFamily",
      name: "Counterparty Entity Family",
      field: "cpeFamily",
      toolTip: "Counterparty Entity Family",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      autoWidth: true,
      minWidth: 150
    }
  ];

  for (var i = 0; i < columnNames.length; i++) {
    columns.push({
      id: columnNames[i],
      name: columnNames[i],
      field: columnNames[i],
      toolTip: columnNames[i],
      type: "number",
      sortable: true,
      formatter: millionFormatter,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      autoWidth: true, // Header word-wrap is achieved by setting autoWidth to true (as per Prop#112790)
      minWidth: 77  // minWidth along with autoWidth (true) sets the width of all columns uniform
    });
  }

  return columns;
}
