import React, { Component } from "react";
import { Route } from "react-router";
import CashSummaryOwnershipView from "Treasury/CashSummaryView";
import Dropdown from "commons/components/Dropdown";
import OwnershipBUSummary from "Treasury/OwnershipBUSummary";
import PositionDetailView from "Treasury/PositionDetailView";
import ExposureCoverageView from "Treasury/ExposureCoverageView";
import BUCPEFamily from "Treasury/BUCPEFamily";
import ExposureDetail from "Treasury/ExposureDetail";
import TotalExposureView from "Treasury/TotalExposureView";
import LiquidIlliquidView from "Treasury/LiquidIlliquid";
import IndividualCounterpartyView from "Treasury/IndividualCounterpartyView";
import InstrumentTypeView from "Treasury/InstrumentTypeView";
import GenericBUView from "Treasury/GenericBUView";
import CounterpartyRevenueSummary from "Treasury/CounterpartyRevenueSummary";
import FinancingDetail from "Treasury/FinancingDetail";
import CommissionDetail from "Treasury/CommissionDetail";
import SummaryByCounterparty from "Treasury/SummaryByCounterparty";
import InteractionSummary from "Treasury/InteractionSummary";
import CashExposureReport from "Treasury/CashExposureReport";
import CashReport from "Treasury/CashReport";
import CounterpartyExposureMonitor from "Treasury/CounterpartyExposureMonitor";
import store from "../store";
import { push } from "react-router-redux";
import ExceptionDialog from "commons/container/ExceptionDialog";

import {
  OWNERSHIP_BU_SUMMARY,
  BU_CPE_FAMILY,
  TOTAL_EXPOSURE_VIEW,
  EXPOSURE_DETAIL,
  EXPOSURE_COVERAGE_VIEW,
  LIQUID_ILLIQUID_VIEW,
  POSITION_DETAIL_VIEW,
  INDIVIDUAL_COUNTERPARTY_VIEW,
  INSTRUMENT_TYPE_VIEW,
  GENERIC_BU_VIEW,
  COUNTERPARTY_REVENUE_SUMMARY,
  FINANCING_DETAIL,
  COMMISSION_DETAIL,
  SUMMARY_BY_COUNTERPARTY,
  INTERACTION_SUMMARY,
  CASH_EXPOSURE_REPORT,
  CASH_REPORT,
  COUNTERPARTY_EXPOSURE_MONITOR
} from "./constants";

const DEFAULT_GROUP = "Summary Reports";
export default class Treasury extends Component {
  constructor(props) {
    super(props);
    this.handleViewChange = this.handleViewChange.bind(this);
    this.getDefaultViewForGroup = this.getDefaultViewForGroup.bind(this);
    this.loadSummaryGadgets - this.loadSummaryGadgets.bind(this);
    this.loadBrokerGadgets - this.loadBrokerGadgets.bind(this);
    this.updateBreadCrumbs = this.updateBreadCrumbs.bind(this);
    this.getBreadCrumbs = this.getBreadCrumbs.bind(this);
    this.updateSelectedView = this.updateSelectedView.bind(this);
    this.getView = this.getView.bind(this);
    this.state = {
      client: "",
      selectedGroup: DEFAULT_GROUP,
      selectedView: this.getDefaultViewForGroup(DEFAULT_GROUP)
    };
  }

  getBreadCrumbs() {
    return [
      {
        name: 'Treasury',
        link: '/treasury'
      },
      {
        name: 'Counterparty Revenue',
        link: '#'
      },
      {
        name: this.state.selectedView,
        link: this.getView(this.state.selectedView)
      }
    ];
  }

  updateBreadCrumbs() {
    var header = this.header;
    var breadcrumbs = this.getBreadCrumbs();

    if (header.ready) {
      header.setBreadcrumb(breadcrumbs);
    } else {
      header.addEventListener('ready', header.setBreadcrumb(breadcrumbs));
    }
  }

  componentDidUpdate() {
    this.updateBreadCrumbs();
  }

  componentDidMount() {
    this.updateBreadCrumbs();
  }

  componentWillMount() {
    this.setState({ client: CODEX_PROPERTIES["codex.client_name"] });
    var path = store.getState().router.location.pathname;
    if (path.indexOf("treasury") === -1 || path.split("/treasury")[1].trim() === "" || path.split("/treasury")[1].trim() === "/")
      store.dispatch(push("/treasury/report/OwnershipBUSummary.html"));
  }

  getDefaultViewForGroup(selectedGroup) {
    return selectedGroup == "Summary Reports"
      ? "Ownership BU Summary"
      : "Individual Counterparty View";
  }

  updateSelectedView(currentView) {
    this.setState({selectedView : currentView});
  }

  getView(selectedView) {
    return "/treasury/report/" + selectedView.replace(/ /g, "") + ".html";
  }

  handleViewChange = function (event) {
    var selectedView = event.currentTarget.textContent;
    var view = this.getView(selectedView);
    store.dispatch(push(view));
    this.updateSelectedView(selectedView);
    this.summaryCallout.hide();
    this.brokerCallout.hide();
  };

  loadSummaryGadgets = () => {
    this.summaryCallout.show(this.summaryTrigger);
  };

  loadBrokerGadgets = () => {
    this.brokerCallout.show(this.brokerTrigger);
  };

  render() {
    return (
      <React.Fragment>
      <ExceptionDialog />
      <div className="layout--flex--row">
        <arc-header

          ref={header => {
            this.header = header;
          }}
          className="size--content"
          user={USER}
          modern-themes-enabled
        >
          <div slot="application-menu">
            <a
              ref={summaryTrigger => {
                this.summaryTrigger = summaryTrigger;
              }}
              href="#"
              onClick={this.loadSummaryGadgets}
            >
              <span className="margin--left--small margin--right--double">
                Summary Reports<i className="icon-caret--down" />
              </span>
            </a>
            <a
              href="#"
              ref={brokerTrigger => {
                this.brokerTrigger = brokerTrigger;
              }}
              onClick={this.loadBrokerGadgets}
            >
              <span className="margin--left--small margin--right--double">
                Broker Revenue Reports<i className="icon-caret--down" />
              </span>
            </a>
            <arc-callout
              ref={summaryCallout => {
                this.summaryCallout = summaryCallout;
              }}
              className="application-menu__callout"
              alignment="bottom-left"
            >
              <ul className="menu">
                <li>
                  <a onClick={this.handleViewChange}>{OWNERSHIP_BU_SUMMARY}</a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{BU_CPE_FAMILY}</a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{TOTAL_EXPOSURE_VIEW}</a>
                </li>

                <li>
                  <a onClick={this.handleViewChange}>{EXPOSURE_DETAIL}</a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{EXPOSURE_COVERAGE_VIEW}</a>
                </li>

                <li>
                  <a onClick={this.handleViewChange}>{LIQUID_ILLIQUID_VIEW}</a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{POSITION_DETAIL_VIEW}</a>
                </li>
              </ul>
            </arc-callout>
            <arc-callout
              ref={brokerCallout => {
                this.brokerCallout = brokerCallout;
              }}
              className="application-menu__callout"
              alignment="bottom-left"
            >
              <ul className="menu">
                <li>
                  <a onClick={this.handleViewChange}>
                    {INDIVIDUAL_COUNTERPARTY_VIEW}
                  </a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{INSTRUMENT_TYPE_VIEW}</a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{GENERIC_BU_VIEW}</a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>
                    {COUNTERPARTY_REVENUE_SUMMARY}
                  </a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{FINANCING_DETAIL}</a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{COMMISSION_DETAIL}</a>
                </li>
                <li>
                  <a onClick={this.handleViewChange}>{SUMMARY_BY_COUNTERPARTY}</a>
                </li>
                <li
                  style={{
                    display: this.state.client === "desco" || this.state.client === "deshaw" ? "block" : "none"
                  }}
                >
                  <a onClick={this.handleViewChange}>{INTERACTION_SUMMARY}</a>
                </li>
                <li
                  style={{
                    display: this.state.client === "baam" ? "block" : "none"
                  }}
                >
                  <a onClick={this.handleViewChange}>{CASH_EXPOSURE_REPORT}</a>
                </li>
                <li
                  style={{
                    display: this.state.client === "baam" || this.state.client === "brdreach" ? "block" : "none"
                  }}
                >
                  <a onClick={this.handleViewChange}>{CASH_REPORT}</a>
                </li>
                <li
                  style={{
                    display: this.state.client === "baam" || this.state.client === "tbird" || this.state.client === "tig" ? "block" : "none"
                  }}
                >
                  <a onClick={this.handleViewChange}>
                    {COUNTERPARTY_EXPOSURE_MONITOR}
                  </a>
                </li>
              </ul>
            </arc-callout>
          </div>

          {
            // <div className="application-menu application-menu-toggle-view">
            //   <a
            //     className={
            //       this.state.selectedGroup === "Summary Reports" ? "active" : ""
            //     }
            //     href="#"
            //     onClick={() => this.handleGroupsChange("Summary Reports")}
            //   >
            //     Summary Reports
            //   </a>
            //   <a
            //     className={
            //       this.state.selectedGroup === "Broker Revenue Reports"
            //         ? "active"
            //         : ""
            //     }
            //     href="#"
            //     onClick={() => this.handleGroupsChange("Broker Revenue Reports")}
            //   >
            //     Broker Revenue Reports
            //   </a>
            // </div>
          }
        </arc-header>
        <div className="layout-flex--row">
          <div className="padding--top--large">
            <Route
              path="/treasury/report/BUCPEFamily.html"
              render={props => (
                <BUCPEFamily {...props} gadgetName="BUCPEFamily"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/CounterpartyExposureMonitor.html"
              render={props => (
                <CounterpartyExposureMonitor
                  {...props}
                  gadgetName="CounterpartyExposureMonitor"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/OwnershipBUSummary.html"
              render={props => (
                <OwnershipBUSummary
                  {...props}
                  gadgetName="OwnershipBUSummary"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/CashReport.html"
              render={props => (
                <CashReport {...props} gadgetName="CashReport"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/CashExposureReport.html"
              render={props => (
                <CashExposureReport
                  {...props}
                  gadgetName="CashExposureReport"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/SummaryByCounterparty.html"
              render={props => (
                <SummaryByCounterparty
                  {...props}
                  gadgetName="SummaryByCounterparty"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/InteractionSummary.html"
              render={props => (
                <InteractionSummary
                  {...props}
                  gadgetName="InteractionSummary"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/CommissionDetail.html"
              render={props => (
                <CommissionDetail {...props} gadgetName="CommissionDetail"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />

            <Route
              path="/treasury/report/FinancingDetail.html"
              render={props => (
                <FinancingDetail {...props} gadgetName="FinancingDetail"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/CounterpartyRevenueSummary.html"
              render={props => (
                <CounterpartyRevenueSummary
                  {...props}
                  gadgetName="CounterpartyRevenueSummary"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/GenericBUView.html"
              render={props => (
                <GenericBUView {...props} gadgetName="GenericBUView"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/InstrumentTypeView.html"
              render={props => (
                <InstrumentTypeView
                  {...props}
                  gadgetName="InstrumentTypeView"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/IndividualCounterpartyView.html"
              render={props => (
                <IndividualCounterpartyView
                  {...props}
                  gadgetName="IndividualCounterpartyView"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/LiquidIlliquidView.html"
              render={props => (
                <LiquidIlliquidView
                  {...props}
                  gadgetName="LiquidIlliquidView"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />

            <Route
              path="/treasury/report/TotalExposureView.html"
              render={props => (
                <TotalExposureView {...props} gadgetName="TotalExposureView"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/ExposureCoverageView.html"
              render={props => (
                <ExposureCoverageView
                  {...props}
                  gadgetName="ExposureCoverageView"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/ExposureDetail.html"
              render={props => (
                <ExposureDetail {...props} gadgetName="ExposureDetail"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
            <Route
              path="/treasury/report/PositionDetailView.html"
              render={props => (
                <PositionDetailView
                  {...props}
                  gadgetName="PositionDetailView"
                  updateSelectedView = {this.updateSelectedView}
                  selectedView = {this.state.selectedView}
                />
              )}
            />
          </div>
        </div>
      </div>
      </React.Fragment>
    );
  }
}
