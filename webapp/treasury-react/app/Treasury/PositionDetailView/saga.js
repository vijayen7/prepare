import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_POSITION_DETAIL_DATA,
  FETCH_POSITION_DETAIL_FILTERS,
  START_LOADING,
  END_LOADING,
  POSITON_DETAIL_VIEW
} from "commons/constants";

import { getFilter } from "commons/api/filters";

import { getPositionDetailData } from "./api";

function* fetchPositionDetailData(action) {
  try {
    yield put({ type: START_LOADING });
    const viewType = action.payload.positionGranularity;
    yield put({ type: POSITON_DETAIL_VIEW, viewType });
    const data = yield call(getPositionDetailData, action.payload);
    yield put({ type: `${FETCH_POSITION_DETAIL_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_POSITION_DETAIL_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}


export function* positionDetailRootSaga() {
  yield [
    takeEvery(FETCH_POSITION_DETAIL_DATA, fetchPositionDetailData)
  ];
}
