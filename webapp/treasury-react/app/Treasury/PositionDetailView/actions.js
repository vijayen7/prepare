import {BASE_URL, FETCH_POSITION_DETAIL_DATA, DESTROY_DATA_STATE_FOR_POSITION_DETAIL_VIEW} from 'commons/constants'

export function fetchPositionDetailData(payload) {
  return {
    type: FETCH_POSITION_DETAIL_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_POSITION_DETAIL_VIEW,
  };
}
