import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Loader from "commons/container/Loader";

import {
  POSITION_DETAIL_VIEW
} from "../constants";

const PositionDetailView = (props) =>
 {
   if(props.selectedView != POSITION_DETAIL_VIEW) {
    props.updateSelectedView(POSITION_DETAIL_VIEW);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
          </div>
        </div>
        <br />
        <ul className="bullet">
          <li>
            All figures in USD. Margin, usage and exposure numbers are system
            calculated numbers.
          </li>
        </ul>
      </div>
    </div>
  );
};

export default PositionDetailView;
