import {
  FETCH_POSITION_DETAIL_DATA,
  POSITON_DETAIL_VIEW,
  FETCH_POSITION_DETAIL_FILTERS,
  DESTROY_DATA_STATE_FOR_POSITION_DETAIL_VIEW
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function positionDetailDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_POSITION_DETAIL_DATA}_SUCCESS`:
      return action.data.positionDetailData || [];
    case `${FETCH_POSITION_DETAIL_DATA}_FAILURE`:
      return [];
    case DESTROY_DATA_STATE_FOR_POSITION_DETAIL_VIEW:
      return [];
  }
  return state;
}

function positionDetailViewTypeReducer(state = "", action) {
  switch (action.type) {
    case POSITON_DETAIL_VIEW:
      return action.viewType;
  }
  return state;
}

function positionDetailRowCountReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_POSITION_DETAIL_DATA}_SUCCESS`:
      return { actualRowCount: action.data.actualRowCount, truncatedRowCount: action.data.truncatedRowCount };
    case `${FETCH_POSITION_DETAIL_DATA}_FAILURE`:
      return {};
    case DESTROY_DATA_STATE_FOR_POSITION_DETAIL_VIEW:
      return {};
  }
  return state;
}

const rootReducer = combineReducers({
  data: positionDetailDataReducer,
  viewType: positionDetailViewTypeReducer,
  rowCount: positionDetailRowCountReducer
});

export default rootReducer;
