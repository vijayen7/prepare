import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import OwnershipEntityFilter from "commons/container/OwnershipEntityFilter";
import CpeFamilyFilter from "commons/container/CpeFamilyFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import AssetLiquidityFilter from "commons/container/AssetLiquidityFilter";
import FinancingStatusFilter from "commons/container/FinancingStatusFilter";
import BusinessUnitFilter from "commons/container/BusinessUnitFilter";
import CpeFilter from "commons/container/CpeFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import { fetchPositionDetailData } from "../actions";
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  getDefaultFilters() {
    return {
      selectedLegalEntities: getCodexFilters("LEGAL_ENTITIES"),
      selectedOwnershipEntities: getCodexFilters("OWNERSHIP_ENTITIES"),
      selectedCpeFamilies: [],
      selectedDate: getCodexFilters("DATE"),
      selectedCpes: [],
      selectedBusinessUnits: [],
      selectedAgreementTypes: [],
      selectedAssetLiquidity: [
        { key: "2", value: "Liquid" },
        { key: "3", value: "Illiquid" }
      ],
      selectedFinancingStatus: { key: "1", value: "Financed" },
      selectedView: { key: "BUNDLE", value: "BUNDLE" },
      viewFilter: [
        { key: "BUNDLE", value: "BUNDLE" },
        { key: "OWNERSHIP_ENTITY", value: "OWNERSHIP" }
      ]
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    var payload = {
      dateString: this.state.selectedDate,
      cpeFamilyIds: getCommaSeparatedValues(this.state.selectedCpeFamilies),
      cpeIds: getCommaSeparatedValues(this.state.selectedCpes),
      businessUnitIds: getCommaSeparatedValues(
        this.state.selectedBusinessUnits
      ),
      agreementTypeIds: getCommaSeparatedValues(
        this.state.selectedAgreementTypes
      ),
      ownershipEntityIds: getCommaSeparatedValues(
        this.state.selectedOwnershipEntities
      ),
      descoEntityIds: getCommaSeparatedValues(this.state.selectedLegalEntities),
      assetLiquidityIds: getCommaSeparatedValues(
        this.state.selectedAssetLiquidity
      ),
      financingStatusTypeIds: this.state.selectedFinancingStatus.key,
      positionGranularity: this.state.selectedView.key
    };

    this.props.fetchPositionDetailData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
        />
        <CpeFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpeFamilies}
        />
        <CpeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpes}
        />
        <BusinessUnitFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnits}
        />
        <AgreementTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedAgreementTypes}
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <OwnershipEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedOwnershipEntities}
        />
        <AssetLiquidityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedAssetLiquidity}
        />
        <FinancingStatusFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedFinancingStatus}
        />
        <SingleSelectFilter
          data={this.state.viewFilter}
          onSelect={this.onSelect}
          selectedData={this.state.selectedView}
          stateKey="selectedView"
          label="View"
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />

          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchPositionDetailData
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
