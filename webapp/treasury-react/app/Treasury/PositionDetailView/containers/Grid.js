import React, { Component } from "react";
import PositionDetailGrid from "commons/components/Grid";
import { connect } from "react-redux";
import Message from "commons/components/Message";
import { bindActionCreators } from "redux";
import { destroyDataState } from "../actions";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  renderGridData() {
    let grid = null;

    if (this.props.positionDetailViewData.length <= 0)
      if (this.props.drillDownGrid) {
        return null;
      } else
        return (
          <Message messageData="No data available for this search criteria." />
        );
    grid = (
      <PositionDetailGrid
        data={this.props.positionDetailViewData}
        gridId="PositionDetail"
        gridColumns={gridColumns(this.props.viewType)}
        gridOptions={gridOptions()}
        label="Position Detail"
        actualRowCount={this.props.actualRowCount}
        truncatedRowCount={this.props.truncatedRowCount}        
      />
    );
    return grid;
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    positionDetailViewData: state.treasury.positionDetailView.data,
    viewType: state.treasury.positionDetailView.viewType,
    actualRowCount: state.treasury.positionDetailView.rowCount.actualRowCount,
    truncatedRowCount: state.treasury.positionDetailView.rowCount.truncatedRowCount
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
