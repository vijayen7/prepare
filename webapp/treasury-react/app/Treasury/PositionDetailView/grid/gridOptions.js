import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=positionDetailData";
      }
    },
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
     
    summaryRow: true,
    displaySummaryRow: true,
    copyCellSelection: true,
    exportToExcel: true,
    sheetName: "Position Detail Report",
    page: true
  };
  return options;
}
