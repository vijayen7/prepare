import { priceFormatter } from "commons/grid/formatters";

import { numberComparator } from "commons/grid/comparators";

export function gridColumns(params) {
  var columns = [
    {
      id: "pnlSpn",
      name: "PNL SPN",
      field: "pnlSpn",
      toolTip: "PNL SPN",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 60
    },
    {
      id: "desname",
      name: "Instrument name",
      field: "desname",
      toolTip: "Instrument name",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 120
    },
    {
      id: "custodianAccount",
      name: "Custodian Account",
      field: "custodianAccount",
      toolTip: "Custodian Account",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 110
    },
    {
      id: "agreement",
      name: "Agreement",
      field: "agreement",
      toolTip: "Agreement",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 150
    },
    {
      id: "bundle",
      name: "Bundle",
      field: "bundle",
      toolTip: "Bundle",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 95
    },
    {
      id: "businessUnit",
      name: "Business Unit",
      field: "businessUnit",
      toolTip: "Business Unit",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 90
    }
  ];
  if (params !== "BUNDLE") {
    columns.push({
      id: "ownershipEntity",
      name: "Ownership Entity",
      field: "ownershipEntity",
      toolTip: "Ownership Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 100
    });
  }

  columns.push(
    {
      id: "gboType",
      name: "GBO Type",
      field: "gboType",
      toolTip: "GBO Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 100
    },
    {
      id: "assetLiquidity",
      name: "Asset Liquidity",
      field: "assetLiquidity",
      toolTip: "Asset Liquidity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 70
    },
    {
      id: "financingStatusType",
      name: "Financing Status",
      field: "financingStatusType",
      toolTip: "Financing Status",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 80
    },
    {
      id: "currency",
      name: "Currency",
      field: "currency",
      toolTip: "Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 60
    },
    {
      id: "price",
      name: "Price",
      field: "price",
      type: "number",
      toolTip: "Price",
      filter: true,
      sortable: true,
      formatter: priceFormatter,
      headerCssClass: "aln-rt b",
      minWidth: 60
    },
    {
      id: "priceTick",
      name: "Price Tick",
      field: "priceTick",
      toolTip: "Price Tick",
      type: "number",
      filter: true,
      sortable: true,
      formatter: priceFormatter,
      headerCssClass: "aln-rt b",
      minWidth: 60
    },
    {
      id: "quantity",
      name: "Quantity",
      field: "quantity",
      toolTip: "Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      minWidth: 80
    },
    {
      id: "marketValue",
      name: "Market Value",
      field: "marketValue",
      toolTip: "Market Value",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      minWidth: 90
    },
    {
      id: "margin",
      name: "Margin",
      field: "margin",
      toolTip: "Margin",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "totalUsage",
      name: "Total Usage",
      field: "totalUsage",
      toolTip: "Total Usage",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "accruedInterestUsage",
      name: "AI Usage",
      field: "accruedInterestUsage",
      toolTip: "Usage on any accrued interest on this position",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "badDebtUsage",
      name: "Bad Debt Usage",
      field: "badDebtUsage",
      toolTip: "Usage on any bedt debt expense on this position",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "lockupDays",
      name: "Lockup Days",
      field: "lockupDays",
      toolTip: "Lockup Days",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      minWidth: 60
    },
    {
      id: "fundingExpirationDate",
      name: "Lockup Date",
      field: "fundingExpirationDate",
      toolTip: "Lockup Date",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "aln-rt b",
      minWidth: 80
    },
    {
      id: "exposure",
      name: "Exposure",
      field: "exposure",
      toolTip: "Exposure",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    }
  );

  return columns;
}
