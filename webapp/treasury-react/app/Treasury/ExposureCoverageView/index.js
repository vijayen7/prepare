import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Loader from "commons/container/Loader";

import {
  EXPOSURE_COVERAGE_VIEW
} from "../constants";

const ExposureCoverageView = (props) =>
 {
  if(props.selectedView != EXPOSURE_COVERAGE_VIEW) {
    props.updateSelectedView(EXPOSURE_COVERAGE_VIEW);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
          </div>
          <br />
          <div>
            <ul className="bullet">
              <li>All figures in USD</li>
              <li>
                Notes:<ul className="bullet">
                  <li>
                    <small>
                      The Exposure Coverage gadget provides a view of the
                      current status of CDS hedge requirement.
                    </small>
                  </li>
                </ul>
              </li>
              <li>
                Source:<ul className="bullet">
                  <li>
                    <small>Collateral management workflows.</small>
                  </li>
                  <li>
                    <small>Actual CDS numbers from PNL.</small>
                  </li>
                  <li>
                    <small>
                      Required CDS is calculated based on usage ( excluding
                      segregated margin) assuming 30% recovery on non-customer
                      accounts, 70% recovery on customer accounts and 30%
                      non-performance on broker CDS.
                    </small>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExposureCoverageView;
