import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import OwnershipEntityFilter from "commons/container/OwnershipEntityFilter";
import InputFilter from "commons/components/InputFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import { fetchExposureCoverageData } from "../actions";
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);

    this.state = {
      selectedLegalEntities: getCodexFilters("LEGAL_ENTITIES"),
      selectedOwnershipEntities: getCodexFilters("OWNERSHIP_ENTITIES"),
      selectedDate: getCodexFilters("DATE"),
      numberOfCPEFamilies: 5
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    var payload = {
      dateString: this.state.selectedDate,
      ownershipEntityIds: getCommaSeparatedValues(
        this.state.selectedOwnershipEntities
      ),
      descoEntityIds: getCommaSeparatedValues(this.state.selectedLegalEntities),
      topNCounterparties: this.state.numberOfCPEFamilies
    };

    this.props.fetchExposureCoverageData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
          dateType ="tMinusTwo"
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <OwnershipEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedOwnershipEntities}
        />
        <InputFilter
          data={this.state.numberOfCPEFamilies}
          onSelect={this.onSelect}
          stateKey="numberOfCPEFamilies"
          label="Number of CPE Families"
        />
        <ColumnLayout>
          <FilterButton onClick={this.handleClick} label="Search" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchExposureCoverageData
    },
    dispatch
  );
}


export default connect(null, mapDispatchToProps)(SideBar);
