import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ExposureCoverageGrid from "commons/components/Grid";
import Message from "commons/components/Message";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  renderGridData() {
    let grid = null;
    if (this.props.exposureCoverageData.length <= 0)
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <ExposureCoverageGrid
        data={this.props.exposureCoverageData}
        gridId="BUCPEFamily"
        gridColumns={gridColumns()}
        gridOptions={gridOptions()}
        label = "Exposure Coverage"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    exposureCoverageData: state.treasury.exposureCoverageView.data
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
