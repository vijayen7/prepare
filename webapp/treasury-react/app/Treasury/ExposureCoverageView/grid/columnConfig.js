import {
  percentFormatter,
  drillThroughFormatter
} from "commons/grid/formatters";

import { numberComparator } from "commons/grid/comparators";

var columns = [];

export function gridColumns(params) {
  var columns = [
    {
      id: "cpe_family",
      name: "Counterparty Entity Family",
      field: "cpe_family",
      toolTip: "Counterparty Entity Family",
      type: "text",
      sortable: true,
      headerCssClass: "b",
      minWidth: 100
    },
    {
      id: "customer_account_margin_usd",
      name: "Customer Account Usage",
      field: "customer_account_margin_usd",
      toolTip:
        "Customer Account Usage refers to usage for positions held in exchange regulated relationships such as Prime Brokerage and Futures & Options accounts.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 90
    },
    {
      id: "non_customer_account_margin_usd",
      name: "Non Customer Account Usage",
      field: "non_customer_account_margin_usd",
      toolTip:
        "Non Customer Account Usage refers to usage for positions held across all non-exchange regulated arrangements such as ISDA, bilateral repo and alternative funding arrangements. Non-Customer Account Usage excludes segregated margin.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 90
    },
    {
      id: "required_cds_usd",
      name: "Required CDS",
      field: "required_cds_usd",
      toolTip:
        "Required CDS represents the Credit Default Swap notional that is required to cover the counterparty exposure, sourced at the counterparty family level. Required CDS levels are computed assuming different recovery rates based on the agreement type.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "actual_cds_usd",
      name: "Actual CDS",
      field: "actual_cds_usd",
      toolTip:
        "Actual CDS represents the Credit Default Swap notional purchased to hedge the counterparty exposure.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "cds_shortfall_usd",
      name: "Current CDS (Shortfall)/Excess",
      field: "cds_shortfall_usd",
      toolTip:
        "Current CDS (Shortfall)/Excess represents the difference between Required CDS and Actual CDS notional.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "cds_shortfall_one_month",
      name: "One Month CDS Shortfall/Excess",
      field: "cds_shortfall_one_month",
      toolTip:
        "One Month CDS Shortfall/Excess represents the difference between Required CDS and Actual CDS notional available after accounting for CDS expiries in the next one month.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "cds_shortfall_three_month",
      name: "Three Month CDS Shortfall/Excess",
      field: "cds_shortfall_three_month",
      toolTip:
        "Three Month CDS Shortfall/Excess represents the difference between Required CDS and Actual CDS notional available after accounting for CDS expiries in the next three months.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "cds_shortfall_six_month",
      name: "Six Month CDS Shortfall/Excess",
      field: "cds_shortfall_six_month",
      toolTip:
        "Six Month CDS Shortfall/Excess represents the difference between Required CDS and Actual CDS notional available after accounting for CDS expiries in the next six months.",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    },
    {
      id: "one_year_spread",
      name: "Current 1 year CDS",
      field: "one_year_spread",
      toolTip: "Current 1yr CDS Spread",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      comparator: function(a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
      minWidth: 70
    }
  ];

  return columns;
}
