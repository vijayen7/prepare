import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=counterpartyExposureCoverageData";
      }
    },
    autoHorizontalScrollBar: true,
     
    summaryRow: true,
    displaySummaryRow: true,
    autoHorizontalScrollBar: true,
    summaryRowText: "Total",
    copyCellSelection: true,
    exportToExcel: true,
    page: true,
    sheetName: "Exposure Coverage Report"
  };
  return options;
}
