import {
  FETCH_EXPOSURE_COVERAGE_DATA,
  DESTROY_EXPOSURE_COVERAGE_VIEW
} from "commons/constants";
import { combineReducers } from "redux";

function exposureCoverageDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_EXPOSURE_COVERAGE_DATA}_SUCCESS`:
      return action.data.counterpartyExposureCoverageData || [];
    case DESTROY_EXPOSURE_COVERAGE_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: exposureCoverageDataReducer
});

export default rootReducer;
