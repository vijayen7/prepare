import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_EXPOSURE_COVERAGE_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getExposureCoverageData } from "./api";

function* fetchExposureCoverageData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getExposureCoverageData, action.payload);
    yield put({ type: `${FETCH_EXPOSURE_COVERAGE_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_EXPOSURE_COVERAGE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* exposureCoverageRootSaga() {
  yield [
    takeEvery(FETCH_EXPOSURE_COVERAGE_DATA, fetchExposureCoverageData)
  ];
}
