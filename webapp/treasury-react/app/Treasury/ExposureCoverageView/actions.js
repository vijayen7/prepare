import {
  BASE_URL,
  FETCH_EXPOSURE_COVERAGE_DATA,
  DESTROY_EXPOSURE_COVERAGE_VIEW
} from "commons/constants";

export function fetchExposureCoverageData(payload) {
  return {
    type: FETCH_EXPOSURE_COVERAGE_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_EXPOSURE_COVERAGE_VIEW,
  };
}
