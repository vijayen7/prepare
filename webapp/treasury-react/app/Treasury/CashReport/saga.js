import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_CASH_REPORT,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getCashReport } from "./api";

function* fetchCashReport(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCashReport, action.payload);
    yield put({ type: `${FETCH_CASH_REPORT}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CASH_REPORT}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* cashReportDataSaga() {
  yield [takeEvery(FETCH_CASH_REPORT, fetchCashReport)];
}
