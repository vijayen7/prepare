import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
export let url = "";
export function getCashReport(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}data/search-cash-data?${paramString}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
