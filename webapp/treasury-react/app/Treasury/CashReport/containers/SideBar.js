import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import Error from "commons/components/Error";
import FilterButton from "commons/components/FilterButton";
import ColumnLayout from "commons/components/ColumnLayout";
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import { fetchCashReport } from "../actions";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  getDefaultFilters() {
    return {
      selectedDate: "",
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedAgreementTypes: []
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

applySavedFilters(selectedFilters) {
  this.setState(selectedFilters);
}

onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    var payload = {
      dateParam: this.state.selectedDate,
      cpeIds: getCommaSeparatedValues(this.state.selectedCpes),
      agreementTypeIds: getCommaSeparatedValues(
        this.state.selectedAgreementTypes
      ),
      legalEntityIds: getCommaSeparatedValues(this.state.selectedLegalEntities)
    };
    this.props.fetchCashReport(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
          label="Date"
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <CpeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpeFamilies}
        />
        <AgreementTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedAgreementTypes}
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCashReport
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
