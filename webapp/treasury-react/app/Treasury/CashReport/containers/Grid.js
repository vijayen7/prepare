import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CashGrid from "commons/components/Grid";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  renderGridData() {
    let grid = null;

    if (
      !this.props.cashReport.hasOwnProperty("resultList") ||
      this.props.cashReport.resultList.length <= 0
    )
      if (this.props.drillDownGrid) {
        return null;
      } else
        return (
          <Message messageData="No data available for this search criteria." />
        );
    grid = (
      <CashGrid
        data={this.props.cashReport.resultList}
        gridId="CashReport"
        gridColumns={gridColumns()}
        gridOptions={gridOptions()}
        label="Cash Report"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    cashReport: state.treasury.cashReportView.data
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
