import { cashReportSumGroupFormatter } from "commons/grid/formatters";
var columns = [];

export function gridColumns() {
  var columns = [
    {
      id: "legalEntity",
      name: "Legal Entity",
      field: "legalEntity",
      toolTip: "Legal Entiy",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 50,
      maxWidth: 350
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "counterpartyEntity",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 50,
      maxWidth: 350
    },
    {
      id: "agreementType",
      name: "Agreement Type",
      field: "agreementType",
      toolTip: "Agreement Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 50
    },
    {
      id: "currency",
      name: "Currency",
      field: "currency",
      toolTip: "Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 50
    },
    {
      id: "cash",
      name: "Cash",
      field: "cash",
      toolTip: "Cash in currency of Currency column",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 50
    },
    {
      id: "cashUsd",
      name: "Cash (USD)",
      field: "cashUsd",
      toolTip: "Cash in USD currency",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: cashReportSumGroupFormatter,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      minWidth: 50
    }
  ];
  return columns;
}
