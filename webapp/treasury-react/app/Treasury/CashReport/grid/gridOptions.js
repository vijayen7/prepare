import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=resultList"; 
      }
    },
    enableMultilevelGrouping: {
      showGroupingKeyInColumn: true,
      initialGrouping: ["legalEntity", "counterpartyEntity", "agreementType"]
    },
    expandTillLevel: -1,
    expandCollapseAll: true,
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
     
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    sheetName: "Cash Report",
    page: true,
    sortList: [{ columnId: "legalEntity", sortAsc: true }]
  };
  return options;
}
