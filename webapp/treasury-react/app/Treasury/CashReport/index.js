import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";

import {
  CASH_REPORT
} from "../constants";

const CashReport = (props) =>
 {
  if(props.selectedView != CASH_REPORT) {
    props.updateSelectedView(CASH_REPORT);
   }
  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
          </div>
          <br />
        </div>
      </div>
    </div>
  );
};

export default CashReport;
