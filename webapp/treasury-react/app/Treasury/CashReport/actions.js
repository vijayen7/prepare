import {
  BASE_URL,
  FETCH_CASH_REPORT,
  DESTROY_DATA_STATE_FOR_CASH_REPORT_VIEW
} from "commons/constants";

export function fetchCashReport(payload) {
  return {
    type: FETCH_CASH_REPORT,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_CASH_REPORT_VIEW,
  };
}
