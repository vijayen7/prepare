import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";

import {
  INSTRUMENT_TYPE_VIEW
} from "../constants";

const InstrumentTypeView = (props) =>
 {
  if(props.selectedView != INSTRUMENT_TYPE_VIEW) {
    props.updateSelectedView(INSTRUMENT_TYPE_VIEW);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
        </div>
        <div className="size--content">
          <Grid />
        </div>
      </div>
    </div>
  );
};

export default InstrumentTypeView;
