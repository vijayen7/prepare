import {
  FETCH_INSTRUMENT_TYPE_DATA,
  DESTROY_DATA_STATE_INSTRUMENT_TYPE_VIEW
} from "commons/constants";

export function fetchInstrumentTypeData(payload) {
  return {
    type: FETCH_INSTRUMENT_TYPE_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_INSTRUMENT_TYPE_VIEW
  };
}
