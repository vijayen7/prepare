import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
import { fetchURL } from "commons/util";
export let url = "";
export function getInstrumentTypeData(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}data/search-instrument-type-detail?${paramString}`;
  return fetchURL(url);
}
