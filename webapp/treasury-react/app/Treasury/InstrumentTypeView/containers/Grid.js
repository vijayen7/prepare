import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Message from "commons/components/Message";
import InstrumentTypeGrid from "commons/components/Grid";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  getYear(date) {
    return date.substring(0, 4);
  }

  renderGridData() {
    let grid = null;
    if (
      !this.props.data.hasOwnProperty("resultList") ||
      Object.keys(this.props.data.resultList).length == 0
    ) {
      return (
        <Message messageData="No data available for this search criteria." />
      );
    }

    var totalObj = this.props.data.resultList.find(function(obj) {
      return obj.sfsType === "Total";
    });
    grid = (
      <div>
        <InstrumentTypeGrid
          data={this.props.data.resultList.slice(0,-1)}
          gridColumns={gridColumns(
            this.getYear(this.props.data.metaData.asOfDate),
            totalObj
          )}
          gridOptions={gridOptions(
            gridColumns(this.getYear(this.props.data.metaData.asOfDate))
          )}
          gridId="InstrumentType"
          label="Instrument Type"
        />
        <br />
        <div>
          <small>
            <ul className="bullet">
              <li> All figures in $M USD</li>
              <li>The numbers are updated and verified up until {this.props.data.metaData.asOfDate}.</li>
              <li>
                The numbers are annualized based on year to date data and may be
                materially higher or lower than prior year if there are expense
                fluctuations in the earlier months of the year.
              </li>
            </ul>
          </small>
        </div>
      </div>
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapStateToProps(state) {
  return {
    data: state.treasury.instrumentTypeView.data
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
