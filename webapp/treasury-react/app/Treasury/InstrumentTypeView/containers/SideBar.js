import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import { getCommaSeparatedValues } from "commons/util";
import { fetchInstrumentTypeData } from "../actions";
import LegalEntityFamilyFilter from "commons/container/LegalEntityFamilyFilter";
import ExecBrokerFamiliesFilter from "commons/container/ExecBrokerFamiliesFilter";
import CounterpartyRevenueCheckboxFilters from "commons/components/CounterpartyRevenueCheckboxFilters";
import BrokerRevenueYearFilter from "commons/container/BrokerRevenueYearFilter";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import BusinessUnitGroupFilter from "commons/container/BusinessUnitGroupFilter";
import BundleGroupFilter from "commons/container/BundleGroupFilter";
import BundleGroupTypeFilter from "commons/container/BundleGroupTypeFilter";
import { getCodexFilters } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.loadConditionalFilters = this.loadConditionalFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  componentDidMount() {
    if ( Object.keys(getCodexFilters("YEAR")[0]).length == 0) {
      if (this.props.years.length > 0 && this.props.execBrokerFamilies.length > 0) {
        this.setState({
          selectedYear: this.props.years[0],
          selectedExecBrokerFamilies: this.props.execBrokerFamilies[0]
        });
      }
    }
    if(this.props.bundleGroupTypes.length >0) {
      this.setState({selectedBundleGroupTypes : this.props.bundleGroupTypes[0]});
    }
  }

  componentWillReceiveProps(nextProps) {
    if ( Object.keys(getCodexFilters("YEAR")[0]).length == 0) {
      if (nextProps.years.length > 0 && nextProps.execBrokerFamilies.length > 0) {
        this.setState({
          selectedYear: nextProps.years[0],
          selectedExecBrokerFamilies: nextProps.execBrokerFamilies[0]
        });
      }
    }
    if(nextProps.bundleGroupTypes.length >0) {
      this.setState({selectedBundleGroupTypes : nextProps.bundleGroupTypes[0]});
    }
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedLegalEntityFamilies: getCodexFilters("LEGAL_ENTITIES"),
      selectedExecBrokerFamilies: getCodexFilters("CPE_FAMILIES_BROKER_REVENUE")[0],
      selectedYear: getCodexFilters("YEAR")[0],
      showAnnualized: true,
      explicitCommission: true,
      implicitCommission: true,
      financing: true,
      clearingFee: true,
      ticketCharges: true,
      bankCharge: true,
      custodyFee: true,
      groupFilterData: [
        { key: "BU_GROUP", value: "Business Unit Group" },
        { key: "BUNDLE_GROUP", value: "Bundle Group" }
      ],
      selectedGroupFilter: { key: "BU_GROUP", value: "Business Unit Group" },
      selectedBusinessUnitGroups: [],
      selectedBundleGroupTypes: [],
      selectedBundleGroups: []
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
    if ( Object.keys(getCodexFilters("YEAR")[0]).length == 0){
      this.setState({
        selectedYear: this.props.years[0],
        selectedExecBrokerFamilies: this.props.execBrokerFamilies[0]
      });
    }
    if(this.props.bundleGroupTypes.length >0) {
      this.setState({selectedBundleGroupTypes : this.props.bundleGroupTypes[0]});
    }
  }

  handleClick() {
    var payload = {
      year: this.state.selectedYear.key,
      descoEntityFamilyIds: getCommaSeparatedValues(
        this.state.selectedLegalEntityFamilies
      ),
      businessUnitGroupIds: getCommaSeparatedValues(
        this.state.selectedBusinessUnitGroups
      ),
      bundleGroupIds: getCommaSeparatedValues(this.state.selectedBundleGroups),
      bundleGroupTypeId: this.state.selectedBundleGroupTypes.hasOwnProperty(
        "key"
      )
        ? this.state.selectedBundleGroupTypes.key
        : -2,
      buLevel: this.state.selectedGroupFilter.key,
      cpeFamilyId: this.state.selectedExecBrokerFamilies.key,
      showAnnualized: this.state.showAnnualized,
      includeExplicitCommission: this.state.explicitCommission,
      includeImplicitCommission: this.state.implicitCommission,
      includeFinancing: this.state.financing,
      includeClearingFee: this.state.clearingFee,
      includeTicketCharges: this.state.ticketCharges,
      includeBankCharge: this.state.bankCharge,
      includeCustodyFee: this.state.custodyFee
    };
    this.props.fetchInstrumentTypeData(payload);
  }

  loadConditionalFilters() {
    if (this.state.selectedGroupFilter.value === "Business Unit Group") {
      return (
        <BusinessUnitGroupFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnitGroups}
        />
      );
    } else {
      return [
        <BundleGroupTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBundleGroupTypes}
          multiSelect={false}
          key="bundleGroupType"
        />,
        <BundleGroupFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBundleGroups}
          key="bundleGroup"
        />
      ];
    }
  }

  render() {
    return (
      <Sidebar>
        <BrokerRevenueYearFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedYear}
        />
        <LegalEntityFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntityFamilies}
        />
        <ExecBrokerFamiliesFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedExecBrokerFamilies}
          label="CPE Families"
          multiSelect={false}
        />
        <SingleSelectFilter
          data={this.state.groupFilterData}
          onSelect={this.onSelect}
          selectedData={this.state.selectedGroupFilter}
          stateKey="selectedGroupFilter"
          label="Group"
        />
        {this.loadConditionalFilters()}
        <CounterpartyRevenueCheckboxFilters
          state={this.state}
          onSelect={this.onSelect}
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />

          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchInstrumentTypeData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    execBrokerFamilies: state.filters.execBrokerFamilies,
    years: state.filters.brokerRevenueYears,
    bundleGroupTypes: state.filters.bundleGroupTypes
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
