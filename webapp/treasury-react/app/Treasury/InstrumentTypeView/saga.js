import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  START_LOADING,
  END_LOADING,
  FETCH_INSTRUMENT_TYPE_DATA
} from "commons/constants";

import { getFilter } from "commons/api/filters";
import { getInstrumentTypeData } from "./api";

function* fetchInstrumentTypeData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getInstrumentTypeData, action.payload);
    yield put({
      type: `${FETCH_INSTRUMENT_TYPE_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_INSTRUMENT_TYPE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* instrumentTypeRootSaga() {
  yield [takeEvery(FETCH_INSTRUMENT_TYPE_DATA, fetchInstrumentTypeData)];
}
