import {
  FETCH_INSTRUMENT_TYPE_DATA,
  DESTROY_DATA_STATE_INSTRUMENT_TYPE_VIEW
} from "commons/constants";
import { combineReducers } from "redux";

function instrumentTypeDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_INSTRUMENT_TYPE_DATA}_SUCCESS`:
      return action.data;
    case DESTROY_DATA_STATE_INSTRUMENT_TYPE_VIEW:
      return {};
  }
  return state;
}

const rootReducer = combineReducers({
  data: instrumentTypeDataReducer
});

export default rootReducer;
