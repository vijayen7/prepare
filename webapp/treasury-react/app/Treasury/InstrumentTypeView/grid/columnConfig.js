import {
  percentFormatter,
  millionFormatter,
  toolTipFormatter,
  percentTotalsFormatter,
  numberTotalsFormatter
} from "commons/grid/formatters";

export function gridColumns(year, totalObj) {
  var year1 = year;
  var year2 = year - 1;
  var columns = [{
      id: "sfsType",
      name: "",
      field: "sfsType",
      sortable: true,
      headerCssClass: "b",
      width: 130,
      formatter: function(row, cell, value, columnDef, dataContext) {
        // For "Financing" and "Total Excluding Financing" rows.
        if (dataContext["id"] < 0) {
          return "<b>" + value + "</b>";
        }
        return value;
      }
    },
    {
      id: "cpeSpendInEntityYear1",
      name: `${year1} Spend`,
      field: "cpeSpendInEntityYear1",
      toolTip: year1 +
        " Spend refers to total spend of the Instrument Type at this broker either on a Year to Date or Annualized basis depending on whether the the 'Show Annualized' check box is ticked.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return millionFormatter(row, cell, value, columnDef, dataContext, totalObj["cpeSpendInEntityYear1"]);
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      autoWidth: true
    },
    {
      id: "cpeSpendInEntityYear2",
      name: `${year2} Spend`,
      field: "cpeSpendInEntityYear2",
      toolTip: year2 +
        " Spend refers to the total spend of the Instrument Type at this broker in the previous year.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return millionFormatter(row, cell, value, columnDef, dataContext, totalObj["cpeSpendInEntityYear2"]);
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      autoWidth: true
    },
    {
      id: "cpeSpendInEntityChange",
      name: `${year1}/${year2} Spend Change`,
      field: "cpeSpendInEntityChange",
      toolTip: year1 +
        "/" +
        year2 +
        " Spend Change refers to the change in the spend of the Instrument Type at this broker year over year  (Current year - Previous year).",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return millionFormatter(row, cell, value, columnDef, dataContext, totalObj["cpeSpendInEntityChange"]);
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      autoWidth: true
    },
    {
      id: "marketShareInEntityYear1",
      name: `${year1} Market Share (%)`,
      field: "marketShareInEntityYear1",
      toolTip: year1 +
        " Market Share (%) refers to the current year total spend of the Instrument Type at this broker as a percentage of the overall spend of the Instrument Type across brokers.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(row, cell, value, columnDef, dataContext, totalObj["marketShareInEntityYear1"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["sfsType"] === "Total")
          return totalObj["marketShareInEntityYear1"];
        else
          return itemValue;
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      autoWidth: true
    },
    {
      id: "marketShareInEntityYear2",
      name: `${year2} Market Share (%)`,
      field: "marketShareInEntityYear2",
      toolTip: year2 +
        " Market Share (%) refers to the previous year total spend of the Instrument Type at this broker as a percentage of the overall spend of the Instrument Type across brokers.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(row, cell, value, columnDef, dataContext, totalObj["marketShareInEntityYear2"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["sfsType"] === "Total")
          return totalObj["marketShareInEntityYear2"];
        else
          return itemValue;
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      autoWidth: true
    },
    {
      id: "marketShareInEntityChange",
      name: `${year1}/${year2} Market Share Change (%)`,
      field: "marketShareInEntityChange",
      toolTip: year1 +
        "/" +
        year2 +
        " Market Share Change (%) refers to the change in the composition of the Instrument Type's spend among different brokers. A positive number indicates that the Instrument Type is spending a higher % of its total spend at this broker compared to the previous year.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(row, cell, value, columnDef, dataContext, totalObj["marketShareInEntityChange"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["sfsType"] === "Total")
          return totalObj["marketShareInEntityChange"];
        else
          return itemValue;
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      autoWidth: true
    },
    {
      id: "cpeRank1",
      name: `${year1} Rank`,
      field: "cpeRank1",
      toolTip: year1 +
        " Rank refers to the current year rank of this broker compared to spending at other brokers for the Instrument Type. ",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, totalObj["cpeRank1"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["sfsType"] === "Total")
          return totalObj["cpeRank1"];
        else
          return itemValue;
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "cpeRank2",
      name: `${year2} Rank`,
      field: "cpeRank2",
      toolTip: year2 +
        " Rank refers to the previous year rank of this broker compared to spending at other brokers for the Instrument Type. ",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, totalObj["cpeRank2"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["sfsType"] === "Total")
          return totalObj["cpeRank2"];
        else
          return itemValue;
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true
    },
    {
      id: "cpeRankChange",
      name: `${year1}/${year2} Rank Change`,
      field: "cpeRankChange",
      toolTip: year1 +
        "/" +
        year2 +
        " Rank Change refers to the year over year change in the rank of the broker for the Instrument Type.",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, totalObj["cpeRankChange"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["sfsType"] === "Total")
          return totalObj["cpeRankChange"];
        else
          return itemValue;
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true
    }
  ];

  return columns;
}
