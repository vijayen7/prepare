import { url } from "../api";
export function gridOptions(columns) {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=resultList";
      }
    },
    isAutoWidth: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    exportToExcel: true,
    headerTextWrap: false,
    page: true,
    sheetName: "Instrument Type View"
  };
  return options;
}
