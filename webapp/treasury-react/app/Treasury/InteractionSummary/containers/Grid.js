import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import InteractionSummaryGrid from "commons/components/Grid";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  renderGridData() {
    let grid = null;
    if (
      !this.props.interactionSummaryData.hasOwnProperty("resultList") ||
      this.props.interactionSummaryData.resultList.length <= 0
    )
      return (
        <Message messageData="No data available for this search criteria." />
      );
    grid = (
      <div>
        <InteractionSummaryGrid
          data={this.props.interactionSummaryData.resultList}
          gridId="interactionSummaryData"
          gridColumns={gridColumns(
            this.props.interactionSummaryData.metaData.columns,
            this.props.selectedFilters.includeCommissions
          )}
          gridOptions={gridOptions()}
          label = "Interaction Summary Data"
        />
      </div>
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    interactionSummaryData: state.treasury.interactionSummaryView.data,
    selectedFilters: state.treasury.drillDownFilters
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
