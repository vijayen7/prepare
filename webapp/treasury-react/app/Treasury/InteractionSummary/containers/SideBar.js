import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MonthFilter from "commons/components/MonthFilter";
import { getCommaSeparatedValues } from "commons/util";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import Error from "commons/components/Error";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import InputFilter from "commons/components/InputFilter";
import BusinessUnitGroupFilter from "commons/container/BusinessUnitGroupFilter";
import BusinessUnitFilter from "commons/container/BusinessUnitFilter";
import ExecBrokerFamiliesFilter from "commons/container/ExecBrokerFamiliesFilter";
import InteractionTypeFilter from "commons/container/InteractionTypeFilter";
import CheckboxFilter from "commons/components/CheckboxFilter";
import { fetchInteractionSummary } from "../actions";
import { updateDrillDownSelectedFilters } from "Treasury/actions";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  getDefaultFilters() {
    var date = new Date();
    var month = (date.getMonth() + 1) + "/" + date.getFullYear();

    return {
      selectedExecBrokerFamilies: [],
      selectedBusinessUnitGroups: [],
      selectedBusinessUnits: [],
      selectedInteractionTypes: [],
      includeCommissions: true,
      selectedStartMonth: month,
      selectedEndMonth: month
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    var startMonthString = this.state.selectedStartMonth;
    var endMonthString = this.state.selectedEndMonth;

    var payload = {
      startMonth: startMonthString.split("/")[0],
      startYear: startMonthString.split("/")[1],
      endMonth: endMonthString.split("/")[0],
      endYear: endMonthString.split("/")[1],
      interactionTypes: getCommaSeparatedValues(this.state.selectedInteractionTypes),
      buGroupIds: getCommaSeparatedValues(this.state.selectedBusinessUnitGroups),
      cpeFamilyIds: getCommaSeparatedValues(this.state.selectedExecBrokerFamilies),
      buIds: getCommaSeparatedValues(this.state.selectedBusinessUnits),
      stitchCommission: this.state.includeCommissions
    };

    this.props.updateDrillDownSelectedFilters(this.state);
    this.props.fetchInteractionSummary(payload);
  }

  render() {
    let dateError = null;
    var startMonthString = this.state.selectedStartMonth;
    var endMonthString = this.state.selectedEndMonth;
    if (startMonthString.split("/")[1] > endMonthString.split("/")[1]) {
      dateError = <Error messageData="Start date is greater than End date" />;
    } else if (startMonthString.split("/")[1] == endMonthString.split("/")[1] && startMonthString.split("/")[0] > endMonthString.split("/")[0]) {
      dateError = <Error messageData="Start date is greater than End date" />;
    }

    return (
      <Sidebar>
        {dateError}
        <MonthFilter
          onSelect={this.onSelect}
          stateKey="selectedStartMonth"
          data={this.state.selectedStartMonth}
          label="Start Month"
        />
        <MonthFilter
          onSelect={this.onSelect}
          stateKey="selectedEndMonth"
          data={this.state.selectedEndMonth}
          label="End Month"
        />
        <BusinessUnitGroupFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnitGroups}
        />
        <BusinessUnitFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnits}
        />
        <ExecBrokerFamiliesFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedExecBrokerFamilies}
          label = "CPE Families"
        />
        <InteractionTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedInteractionTypes}
        />
        <CheckboxFilter
          defaultChecked={this.state.includeCommissions}
          onSelect={this.onSelect}
          stateKey="includeCommissions"
          label="Include Commissions"
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            disabled={dateError !== null}
            label="Search"
          />
          <vr />
          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchInteractionSummary,
      updateDrillDownSelectedFilters
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
