import {
  FETCH_INTERACTION_SUMMARY_DATA,
  DESTROY_DATA_STATE_FOR_INTERACTION_SUMMARY_VIEW
} from "commons/constants";
import { combineReducers } from "redux";
import { convertArrayToFilterObjects } from "commons/util";

function interactionSummaryDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_INTERACTION_SUMMARY_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_INTERACTION_SUMMARY_VIEW:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  data: interactionSummaryDataReducer
});

export default rootReducer;
