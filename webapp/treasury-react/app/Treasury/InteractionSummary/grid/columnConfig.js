import { defaultNumberToZeroFormatter } from "commons/grid/formatters";

var columns = [];

export function gridColumns(columnNames, includeCommissions) {
  var columns = [
    {
      id: "businessUnitGroup",
      name: "Business Unit Group",
      field: "businessUnitGroup",
      toolTip: "Business Unit Group",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      autoWidth: true,
      minWidth: 77
    },
    {
      id: "businessUnit",
      name: "Business Unit",
      field: "businessUnit",
      toolTip: "Business Unit",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      autoWidth: true,
      minWidth: 77
    },
    {
      id: "cpeFamily",
      name: "Counterparty Entity Family",
      field: "cpeFamily",
      toolTip: "Counterparty Entity Family",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      autoWidth: true,
      minWidth: 77
    },
    {
      id: "totalInteractions",
      name: "Total Interactions",
      field: "totalInteractions",
      toolTip: "Total Interactions",
      type: "number",
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      minWidth: 77
    }
  ];

  for (var i = 0; i < columnNames.length; i++) {
    columns.push({
      id: "interaction_type" + i.toString(),
      name: columnNames[i],
      field: columnNames[i],
      toolTip: columnNames[i],
      type: "number",
      sortable: true,
      formatter: defaultNumberToZeroFormatter,
      excelDataFormatter: function(value) {
        return (value == "")? 0 : value;
      },
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      minWidth: 77
    });
  }

  if(includeCommissions) {
    columns.push({
      id: "commission",
      name: "Commission",
      field: "commission",
      toolTip: "Commission",
      type: "float",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      autoWidth: true,
      minWidth: 77
    });
  }

  return columns;
}
