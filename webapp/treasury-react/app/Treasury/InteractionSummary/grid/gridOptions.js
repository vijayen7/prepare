import { url } from "../api";
export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=resultList";
      }
    },
    frozenColumn: 2,
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    summaryRowText: "Total",
    maxHeight: 800,
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    exportToExcel: true,
    page: true,
    sheetName: "Interaction Summary"
  };
  return options;
}
