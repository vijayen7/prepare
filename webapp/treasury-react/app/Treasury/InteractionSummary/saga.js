import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  HANDLE_EXCEPTION,
  FETCH_INTERACTION_SUMMARY_DATA,
  START_LOADING,
  END_LOADING
} from "commons/constants";

import { getInteractionSummary } from "./api";

function* fetchInteractionSummary(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getInteractionSummary, action.payload);
    yield put({ type: `${FETCH_INTERACTION_SUMMARY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_INTERACTION_SUMMARY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* interactionSummaryDataSaga() {
  yield [
    takeEvery(FETCH_INTERACTION_SUMMARY_DATA, fetchInteractionSummary)
  ];
}
