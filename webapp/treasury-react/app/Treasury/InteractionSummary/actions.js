import {
  BASE_URL,
  FETCH_INTERACTION_SUMMARY_DATA,
  DESTROY_DATA_STATE_FOR_INTERACTION_SUMMARY_VIEW
} from "commons/constants";

export function fetchInteractionSummary(payload) {
  return {
    type: FETCH_INTERACTION_SUMMARY_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_INTERACTION_SUMMARY_VIEW,
  };
}
