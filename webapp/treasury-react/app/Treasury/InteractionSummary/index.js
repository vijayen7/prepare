import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";

import {
  INTERACTION_SUMMARY
} from "../constants";

const InteractionSummary = (props) => {
  if(props.selectedView != INTERACTION_SUMMARY) {
    props.updateSelectedView(INTERACTION_SUMMARY);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
        </div>
        <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
          </div>
          <br />
          <div>
            <ul className="bullet">
              <li>Commissions in USD</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InteractionSummary;
