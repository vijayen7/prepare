import React from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";
import PositionDetailGridContainer from "../PositionDetailView/containers/Grid";

import {
  BU_CPE_FAMILY
} from "../constants";

const BUCPEFamily = (props) =>
 {
  if(props.selectedView != BU_CPE_FAMILY) {
    props.updateSelectedView(BU_CPE_FAMILY);
   }

  return (
    <div className="layout--flex">
      <div>
        <SideBar applicationName={props.gadgetName}  />
      </div>
      <div className="size--5 padding--horizontal--double">
        <div className="layout--flex--row size--4 padding--left--double">
          <Loader />
          <div className="size--content">
            <Grid />
            <br />
            <PositionDetailGridContainer drillDownGrid = {true}/>
          </div>
          <br />
          <div>
            <ul className="bullet">
              <li>All figures in USD</li>
              <li>
                Notes:
                <ul className="bullet">
                  <li>
                    <small>
                      Margin and usage numbers are system calculated numbers.
                    </small>
                  </li>
                  <li>
                    <small>
                      Adjusted Usage includes adjustments published from
                      collateral management workflows.
                    </small>
                  </li>
                  <li>
                    <small>
                      Assets refer to 'on-balance sheet' instruments and
                      includes listed equity options and total return swaps
                      whose first derivative is an asset.
                    </small>
                  </li>
                  <li>
                    <small>
                      Financed Assets refer to assets that are explicitly
                      levered and excludes futures and other instrument types
                      not explicitly levered.
                    </small>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BUCPEFamily;
