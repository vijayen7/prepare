import { BASE_URL } from "commons/constants";

export let url = "";
export function getBUCPEFamilyData(payload) {
  url = `${BASE_URL}data/search-bu-cpefamily-data?dateString=${
    payload.date
  }&mvRollingDays=${payload.mvRollingDays}&cpeFamilyIds=${
    payload.cpeFamilyIds
  }&ownershipEntityIds=${payload.ownershipEntityIds}&descoEntityIds=${
    payload.descoEntityIds
  }&businessUnitIds=${payload.businessUnitIds}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
