import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  FETCH_BU_CPE_FAMILY_DATA,
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from "commons/constants";


import { getBUCPEFamilyData } from "./api";

function* fetchBUCPEFamilyData(action) {
  try {
    yield put({ type: START_LOADING });
    
    const data = yield call(getBUCPEFamilyData, action.payload);
    yield put({ type: `${FETCH_BU_CPE_FAMILY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_BU_CPE_FAMILY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* buCPEFamilyDataSaga() {
  yield [
    takeEvery(FETCH_BU_CPE_FAMILY_DATA, fetchBUCPEFamilyData)
  ];
}
