import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import { destroyDataState } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import BuCPEFamilyGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import { getCommaSeparatedValues } from "commons/util";
import { fetchPositionDetailData } from "../../PositionDetailView/actions";
import { url } from "../api";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  componentWillUnmount() {
    this.props.destroyDataState();
  }

  onCellClickHandler = args => {
    if (args.colId === "cpeFamilyName") {
      var cpeFamilyName = args.item.cpeFamilyName.value;
      var cpeFamilyIds = args.item.cpeFamilyName.params
        .substring(19, args.item.cpeFamilyName.params.length - 2)
        .split(",");
      var selectedCpeFamilies = [];
      for (var i in cpeFamilyIds) {
        selectedCpeFamilies.push({
          key: parseInt(cpeFamilyIds[i]),
          value: cpeFamilyName
        });
      }
      var selectedFilterValues = Object.assign({}, this.props.selectedFilters);
      selectedFilterValues.selectedCpeFamilies = selectedCpeFamilies;

      var payload = {
        dateString: selectedFilterValues.selectedDate,
        cpeIds: "-1",
        cpeFamilyIds: getCommaSeparatedValues(
          selectedFilterValues.selectedCpeFamilies
        ),
        agreementTypeIds: "-1",
        assetLiquidityIds: "2,3",
        positionGranularity: "OWNERSHIP_ENTITY",
        financingStatusTypeIds: "1",
        ownershipEntityIds: getCommaSeparatedValues(
          selectedFilterValues.selectedOwnershipEntities
        ),
        descoEntityIds: getCommaSeparatedValues(
          selectedFilterValues.selectedLegalEntities
        ),
        businessUnitIds: getCommaSeparatedValues(
          selectedFilterValues.selectedBusinessUnits
        )
      };
      this.props.fetchPositionDetailData(payload);
    }
  };

  renderGridData() {
    let grid = null;

    if (
      !this.props.buCPEFamilySummaryData.hasOwnProperty(
        "cpeFamilySummaryList"
      ) ||
      this.props.buCPEFamilySummaryData.cpeFamilySummaryList.length <= 0
    ) {
      if (this.props.drillDownGrid) {
        return null;
      } else {
        return (
          <Message messageData="No data available for this search criteria." />
        );
      }
    }
    grid = (
      <BuCPEFamilyGrid
        data={this.props.buCPEFamilySummaryData.cpeFamilySummaryList}
        onCellClick={this.onCellClickHandler}
        gridId="BUCPEFamily"
        gridColumns={gridColumns(
          this.props.buCPEFamilySummaryData.totalSummary[0]
        )}
        gridOptions={gridOptions()}
        label="BU CPE Family"
      />
    );

    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchPositionDetailData,
      destroyDataState
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    buCPEFamilySummaryData: state.treasury.BUCPEFamilyView.data,
    selectedFilters: state.treasury.drillDownFilters
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
