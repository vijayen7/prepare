import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import { getPreviousDate, getCommaSeparatedValues } from "commons/util";
import SaveSetting from "commons/container/SaveSetting";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import OwnershipEntityFilter from "commons/container/OwnershipEntityFilter";
import CpeFamilyFilter from "commons/container/CpeFamilyFilter";
import BusinessUnitFilter from "commons/container/BusinessUnitFilter";
import MvRollingFilter from "commons/container/MvRollingFilter";
import { fetchBUCPEFamilyData } from "../actions";
import { getCodexFilters } from "../../../commons/util";
import { updateDrillDownSelectedFilters } from "Treasury/actions";
import { destroyDataState as positionDetailDestroyDataState } from "../../PositionDetailView/actions";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedLegalEntities: getCodexFilters("LEGAL_ENTITIES"),
      selectedOwnershipEntities: getCodexFilters("OWNERSHIP_ENTITIES"),
      selectedMVRolling: getCodexFilters("ROLLING_DAYS")[0],
      selectedCpeFamilies: [],
      selectedBusinessUnits: [],
      selectedDate: getCodexFilters("DATE")
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    this.props.positionDetailDestroyDataState();
    var payload = {
      date: this.state.selectedDate,
      mvRollingDays: this.state.selectedMVRolling.value,
      cpeFamilyIds: getCommaSeparatedValues(this.state.selectedCpeFamilies),
      ownershipEntityIds: getCommaSeparatedValues(
        this.state.selectedOwnershipEntities
      ),
      descoEntityIds: getCommaSeparatedValues(this.state.selectedLegalEntities),
      businessUnitIds: getCommaSeparatedValues(this.state.selectedBusinessUnits)
    };

    this.props.updateDrillDownSelectedFilters(this.state);
    this.props.fetchBUCPEFamilyData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <OwnershipEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedOwnershipEntities}
        />
        <CpeFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpeFamilies}
        />
        <BusinessUnitFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnits}
        />
        <MvRollingFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedMVRolling}
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />

          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchBUCPEFamilyData,
      updateDrillDownSelectedFilters,
      positionDetailDestroyDataState
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
