import {
  percentFormatter,
  drillThroughFormatter,
  numberTotalsFormatter,
  percentTotalsFormatter,
  excelTotalsFormatter
} from "commons/grid/formatters";

var columns = [];

export function gridColumns(params) {
  var columns = [{
      id: "cpeFamilyName",
      name: "Counterparty Entity Family",
      headerCssClass: "b",
      field: "cpeFamilyName",
      sortable: true,
      excelDataFormatter: function(data) {
        return data["value"];
      },
      formatter: function(row, cell, value, columnDef, dataContext) {
        return drillThroughFormatter(
          row,
          cell,
          value["value"],
          columnDef,
          dataContext,
          undefined,
          undefined,
          value["params"]
        );
      }
    },
    {
      id: "financedAssetsLMV",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["financedAssetsLMV"]);
      },
      aggregator: dpGrid.Aggregators.sum,
      name: "LMV",
      field: "financedAssetsLMV",
      sortable: true,
      toolTip: "LMV refers to long market value of financed assets.",
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },
    {
      id: "financedAssetsSMV",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["financedAssetsSMV"]);
      },
      name: "SMV",
      field: "financedAssetsSMV",
      sortable: true,
      toolTip: "SMV refers to short market value of financed assets.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "marginPercent",
      type: "number",
      name: "Margin (%)",
      field: "marginPercent",
      sortable: true,
      toolTip: "Margin refers to the percentage of financed asset margin as a proportion of financed asset GMV.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(row, cell, value, columnDef, dataContext, params["marginPercent"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["cpeFamilyName"] === "Total")
          return params["marginPercent"];
        else
          return itemValue;
      }
    },
    {
      id: "financedAssetsMargin",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["financedAssetsMargin"]);
      },
      name: "Margin (financed)",
      field: "financedAssetsMargin",
      sortable: true,
      toolTip: "Margin (financed) refers to margin on financed assets.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "totalMargin",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["totalMargin"]);
      },
      name: "Margin (total)",
      field: "totalMargin",
      sortable: true,
      toolTip: "Margin (total) refers to margin on all positions (both financed and unfinanced, assets and non-assets).",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "totalUsage",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["totalUsage"]);
      },
      name: "Usage",
      field: "totalUsage",
      toolTip: "Usage refers to usage across positions (financed and unfinanced, assets and non-assets). Usage is the total amount of capital required to hold a position. In most cases, usage is equivalent to margin, but usage also includes the net option value under Futures & Options agreement, accrued interest and bad debt expense. Loan received under a variable funding note arrangement is shown as a deduction in usage.",
      sortable: true,
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "adjustedUsage",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["adjustedUsage"]);
      },
      name: "Adjusted Usage",
      field: "adjustedUsage",
      toolTip: "Adjusted Usage refers to usage, including adjustments from published collateral management workflows.",
      sortable: true,
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "marginChange",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["marginChange"]);
      },
      name: "Margin change",
      field: "marginChange",
      sortable: true,
      toolTip: "Margin Change refers to the change in Total Margin when compared to previous day's total margin.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "wam",
      name: "WAM",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["wam"]);
      },
      field: "wam",
      sortable: true,
      toolTip: "WAM refers to gross market value weighted financing lock up represented in days, WAM is calculated as {Sum of Lockup Days * Financed Asset GMV} / {Sum of Financed Asset GMV}",
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b"
    },
    {
      id: "mvRolling",
      name: "MV rolling in selected days",
      type: "number",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, params["mvRolling"]);
      },
      field: "mvRolling",
      sortable: true,
      toolTip: "MV rolling in selected days represents the amount of gross market value of financed assets that could become unfinanced in selected days, considering the lock up terms.",
      headerCssClass: "aln-rt b",
      aggregator: dpGrid.Aggregators.sum,
      excelFormatter: "#,##0"
    },
    {
      id: "marketShare",
      name: "Market share of CPE Family",
      type: "text",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return percentTotalsFormatter(row, cell, value, columnDef, dataContext, params["marketShare"]);
      },
      excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
        if (dataItem["cpeFamilyName"] === "Total")
          return params["marketShare"];
        else
          return itemValue;
      },
      field: "marketShare",
      headerCssClass: "aln-rt b",
      sortable: true,
      toolTip: "Market share of largest CPE Family indicates the percentage of ownership entity’s GMV held with the counterparty family.",
      aggregator: dpGrid.Aggregators.sum,
    }
  ];

  return columns;
}
