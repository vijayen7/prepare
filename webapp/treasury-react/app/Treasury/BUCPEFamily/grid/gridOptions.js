import { url } from "../api";

export function gridOptions() {
  var options = {
    // nestedTable: true,
    // nestedField: "businessUnitName",
    // expandTillLevel: -1,
    // expandCollapseAll: true,
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url + "&_outputKey=cpeFamilySummaryList";
      }
    },
    forceFitColumns: true,
    summaryRow: true,
    displaySummaryRow: true,
    autoHorizontalScrollBar: true,
    exportToExcel: true,
    summaryRowText: "Total",
    sheetName: "BU-CPE Summary data",
    page: true,
    sortList: [{ columnId: "cpeFamilyName", sortAsc: true }]
  };
  return options;
}
