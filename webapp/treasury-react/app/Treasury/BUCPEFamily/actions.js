import {
  BASE_URL,
  FETCH_BU_CPE_FAMILY_DATA,
  DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW
} from "commons/constants";

export function fetchBUCPEFamilyData(payload) {
  return {
    type: FETCH_BU_CPE_FAMILY_DATA,
    payload
  };
}

export function destroyDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_BU_CPE_FAMILY_VIEW,
  };
}
