import React from "react";
import { render } from "@testing-library/react";
import LogWireBookTradeDialog from "../component/LogWireBookTradeDialog";

describe("Log Wire Book Trade Dialog Component", () => {
  let wirePopUpSelectedData,
    wirePopUpRefData,
    onSelect,
    onClose,
    onSave,
    errorMessage;
  beforeEach(() => {
    wirePopUpSelectedData = null;
    wirePopUpRefData = null;
    onSelect = jest.fn();
    onClose = jest.fn();
    onSave = jest.fn();
    errorMessage = "";
  });
});

test("Log Wire Book Trade Dialog Component would render even if no data is passed", () => {
  const { container } = render(
    <LogWireBookTradeDialog
      wirePopUpSelectedData={wirePopUpSelectedData}
      wirePopUpRefData={wirePopUpRefData}
      onSelect={onSelect}
      onClose={onClose}
      onSave={onSave}
      errorMessage={errorMessage}
    />
  );
  expect(container.querySelectorAll("div").length).not.toBeNull();
});
