export interface WireRequest {
  subject: string;
  subscribers: string;
  cc: string;
  comments: string;
  "@CLASS": "deshaw.wires.common.wrs.domain.WireRequest";
}

export interface WireWrapper {
  sourceAccountId: number;
  destinationAccountId: number;
  sourcePortfolioId: number;
  destinationPortfolioId: number;
  wireAmount: number;
  reportingCurrencyId: number | null;
  bookMmf: boolean;
  wireDateStr: string;
  valueDateStr: string;
  isTrackingWire: boolean;
  "@CLASS": "com.arcesium.treasury.model.common.WireWrapper";
}

export interface DisplayLogWireParams {
  workflowId: number;
  actionableAgreementId: number;
  custodianAccountId: number | null;
  tripartyAgreementId: number | null;
}

export interface LogWireAndBookTradeDetail {
  workflowId: number;
  agreementId: number;
  wireWrapper: WireWrapper;
  wireRequest: WireRequest;
  "@CLASS": "com.arcesium.treasury.model.common.LogWireAndBookTradeDetail";
}
