import RefData from "../../../commons/models/RefData";

export interface WirePopUpSelectedData {
  [key: string]: any;
  selectedSubject: string;
  selectedSubscribers: string;
  selectedCC: string;
  selectedComment: string;
  selectedSourceAccount: RefData;
  selectedDestinationAccount: RefData;
  selectedSourcePortfolio: RefData;
  selectedDestinationPortfolio: RefData;
  selectedWireDate: string;
  selectedValueDate: string;
  selectedWireAmount: string | number;
  selectedFinalWireAmount: string | number;
  logWire: boolean;
  bookTrade: boolean;
  isTrackingWire: boolean;
}

export interface WirePopUpRefData {
  sourceAccounts: RefData[];
  destinationAccounts: RefData[];
  sourcePortfolios: RefData[];
  destinationPortfolios: RefData[];
  custodianAccounts: RefData[];
}
