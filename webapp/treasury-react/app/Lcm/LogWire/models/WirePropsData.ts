export default interface WirePropsData {
  isOpen: boolean,
  onClose: Function,
  workflowId: number;
  actionableAgreementId: number;
  custodianAccountId: number | null;
  tripartyAgreementId: number | null;
  acmVal: number;
  acmSign: number;
  reportingCurrencySpn: number | null;
  wirePortfolioData: [];
  isPortfolioMandatoryToLogWire: boolean;
  tag?: string;
  page: string;
  title?: string;
  onLoggingWire: Function;
}
