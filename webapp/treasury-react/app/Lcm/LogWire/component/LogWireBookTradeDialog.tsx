import React, { Fragment } from "react";
import { Layout, Dialog } from "arc-react-components";
import { observer } from "mobx-react-lite";
import InputFilter from "../../../commons/components/InputFilter";
import ButtonWithPause from "../../../commons/components/ButtonWithPause";
import TextArea from "../../../commons/components/TextArea";
import SingleSelectFilter from "../../../commons/components/SingleSelectFilter";
import DateFilter from "../../../commons/container/DateFilter";
import { getCommaSeparatedNumber } from "../../../commons/util";
import {
  WirePopUpSelectedData,
  WirePopUpRefData,
} from "../models/WirePopUpData";
import Panel from "../../../commons/components/Panel";
import Column from "../../../commons/components/Column";
import CheckboxFilter from "../../../commons/components/CheckboxFilter";
import Message from "../../../commons/components/Message";
import Status from "../../../commons/models/Status";
import { FAILURE } from "../../../commons/constants";

interface LogWireBookTradeDialogProps {
  wirePopUpSelectedData: WirePopUpSelectedData;
  wirePopUpRefData: WirePopUpRefData;
  title?: string;
  isOpen: boolean;
  onClose: any;
  onSave: (e: MouseEvent) => void;
  onSelect: (any) => void;
  onSaveStatus: Status;
  showOnSaveStatus: boolean;
}

const LogWireBookTradeDialog: React.FC<LogWireBookTradeDialogProps> = ({
  wirePopUpSelectedData,
  wirePopUpRefData,
  ...props
}) => {

  return (
    <div id="logWireDraggable">
      <Dialog
        id="logWireDialog"
        isOpen={props.isOpen}
        onClose={props.onClose}
        draggable={true}
        title={props.title || "Log Wire | Book Trade"}
        style={{
          width: "40%", position: "absolute"
        }}
        footer={
          <Layout>
            {props.showOnSaveStatus && (
              <Layout.Child childId="onSaveStatusMessage">
                <Message
                  messageData={props.onSaveStatus.message}
                  error={props.onSaveStatus.status == FAILURE}
                />
              </Layout.Child>
            )}
            <Layout.Child childId="logWireOrCancel">
              <ButtonWithPause onClick={props.onSave} data={<Fragment>Save</Fragment>} />
              <button onClick={props.onClose}>Cancel</button>
            </Layout.Child>
          </Layout>
        }
      >
        <Panel>
          <TextArea
            onSelect={props.onSelect}
            stateKey="selectedSubject"
            data={wirePopUpSelectedData.selectedSubject}
            label="Subject"
          />
          <InputFilter
            onSelect={props.onSelect}
            stateKey="selectedSubscribers"
            data={wirePopUpSelectedData.selectedSubscribers}
            label="Subscribers"
          />
          <InputFilter
            onSelect={props.onSelect}
            stateKey="selectedCC"
            data={wirePopUpSelectedData.selectedCC}
            label="CC"
          />

          <InputFilter
            onSelect={props.onSelect}
            stateKey="selectedComment"
            data={wirePopUpSelectedData.selectedComment}
            label="Comment"
          />

          <SingleSelectFilter
            onSelect={props.onSelect}
            selectedData={wirePopUpSelectedData.selectedSourceAccount}
            stateKey="selectedSourceAccount"
            data={wirePopUpRefData.sourceAccounts}
            label="Source Account"
          />
          <SingleSelectFilter
            onSelect={props.onSelect}
            selectedData={wirePopUpSelectedData.selectedDestinationAccount}
            stateKey="selectedDestinationAccount"
            data={wirePopUpRefData.destinationAccounts}
            label="Destination Account"
          />

          <SingleSelectFilter
            onSelect={props.onSelect}
            selectedData={wirePopUpSelectedData.selectedSourcePortfolio}
            stateKey="selectedSourcePortfolio"
            data={wirePopUpRefData.sourcePortfolios}
            label="Source Portfolio"
          />
          <SingleSelectFilter
            onSelect={props.onSelect}
            selectedData={wirePopUpSelectedData.selectedDestinationPortfolio}
            stateKey="selectedDestinationPortfolio"
            data={wirePopUpRefData.destinationPortfolios}
            label="Destination Portfolio"
          />

          <DateFilter
            onSelect={props.onSelect}
            data={wirePopUpSelectedData.selectedWireDate}
            stateKey="selectedWireDate"
            dateType="tFilterDate"
            label="Wire Date"
          />
          <DateFilter
            onSelect={props.onSelect}
            data={wirePopUpSelectedData.selectedValueDate}
            stateKey="selectedValueDate"
            dateType="tFilterDate"
            label="Value Date"
          />
          <InputFilter
            onSelect={props.onSelect}
            stateKey="selectedWireAmount"
            disabled={true}
            data={getCommaSeparatedNumber(
              wirePopUpSelectedData.selectedWireAmount
            )}
            label="Wire Amount (RC)"
          />
          <InputFilter
            onSelect={props.onSelect}
            stateKey="selectedFinalWireAmount"
            data={getCommaSeparatedNumber(
              wirePopUpSelectedData.selectedFinalWireAmount
            )}
            label="Final Wire Amount (RC)"
          />
        </Panel>
        <Panel>
          <Column>
            <CheckboxFilter
              defaultChecked={wirePopUpSelectedData.bookTrade}
              onSelect={props.onSelect}
              stateKey="bookTrade"
              label="Book MMF for this amount"
            />
            <CheckboxFilter
              defaultChecked={wirePopUpSelectedData.isTrackingWire}
              onSelect={props.onSelect}
              stateKey="isTrackingWire"
              label="Is Tracking Wire"
            />
          </Column>
        </Panel>
      </Dialog>
    </div>
  );
};

export default observer(LogWireBookTradeDialog);
