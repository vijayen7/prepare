import { action, observable } from "mobx";
import { createContext } from "react";
import {
  WirePopUpSelectedData,
  WirePopUpRefData,
} from "./models/WirePopUpData";
import { getWireData, logWireRequest } from "./api";
import { FAILURE, SUCCESS } from "../../commons/constants";
import RefData from "../../commons/models/RefData";
import Status from "../../commons/models/Status";
import WirePropsData from "./models/WirePropsData";
import {
  WireRequest,
  WireWrapper,
  DisplayLogWireParams,
  LogWireAndBookTradeDetail,
} from "./models/WireServiceInputData";

const INITIAL_REF_DATA: RefData = {
  key: "",
  value: "",
};

const INITIAL_PROPS_DATA: WirePropsData = {
  isOpen: false,
  onClose: () => { },
  workflowId: -1,
  actionableAgreementId: -1,
  custodianAccountId: null,
  tripartyAgreementId: null,
  acmVal: 0,
  acmSign: 1,
  reportingCurrencySpn: null,
  wirePortfolioData: [],
  isPortfolioMandatoryToLogWire: false,
  tag: "",
  page: "",
  title: "",
  onLoggingWire: () => { },
};

const INITIAL_WIRE_POPUP_SELECTED_DATA: WirePopUpSelectedData = {
  selectedSubject: "",
  selectedSubscribers: "",
  selectedCC: "",
  selectedComment: "",
  selectedSourceAccount: INITIAL_REF_DATA,
  selectedDestinationAccount: INITIAL_REF_DATA,
  selectedSourcePortfolio: INITIAL_REF_DATA,
  selectedDestinationPortfolio: INITIAL_REF_DATA,
  selectedWireDate: "",
  selectedValueDate: "",
  selectedWireAmount: 0,
  selectedFinalWireAmount: 0,
  logWire: true,
  bookTrade: false,
  isTrackingWire: false,
};

const INITIAL_WIRE_POPUP_REF_DATA: WirePopUpRefData = {
  sourceAccounts: [],
  destinationAccounts: [],
  sourcePortfolios: [],
  destinationPortfolios: [],
  custodianAccounts: [],
};

const INITIAL_STATUS: Status = {
  status: "",
  message: "",
};

export class WirePopUpStore {

  @observable
  wirePropsData: WirePropsData = INITIAL_PROPS_DATA;

  @observable
  showOnSaveStatus: boolean = false;

  @observable
  showLogWireStatus: boolean = false;

  @observable
  wirePopUpSelectedData: WirePopUpSelectedData = INITIAL_WIRE_POPUP_SELECTED_DATA;

  @observable
  wirePopUpRefData: WirePopUpRefData = INITIAL_WIRE_POPUP_REF_DATA;

  @observable
  inProgress: boolean = false;

  @observable
  isDataReady: boolean = false;

  @observable
  saveStatus: Status = INITIAL_STATUS;

  @observable
  logWireStatus: Status = INITIAL_STATUS;

  @action.bound
  unsetShowLogWireStatus() {
    this.showLogWireStatus = false;
  }

  @action.bound
  onClose() {
    this.wirePropsData.onClose();
    this.wirePopUpSelectedData = INITIAL_WIRE_POPUP_SELECTED_DATA;
    this.wirePopUpRefData = INITIAL_WIRE_POPUP_REF_DATA;
    this.saveStatus = INITIAL_STATUS;
  }

  @action.bound
  onChangeWirePopUpData({ key, value }: RefData) {
    this.wirePopUpSelectedData[key] = value;
    if (this.showOnSaveStatus) {
      this.showOnSaveStatus = false;
    }
  }

  @action.bound
  setOnSaveStatus(status: string, message: string) {
    this.saveStatus = {
      status: status,
      message: message,
    };
    this.showOnSaveStatus = true;
  }

  @action.bound
  setLogWireStatus(status: string, message: string) {
    this.logWireStatus = {
      status: status,
      message: message,
    };
    this.showLogWireStatus = true;
  }

  @action.bound
  setPropsData(props: WirePropsData) {
    this.wirePropsData = props;
    this.wirePropsData.custodianAccountId = props.custodianAccountId
      ? props.custodianAccountId
      : null;
    this.wirePropsData.tripartyAgreementId = props.tripartyAgreementId
      ? props.tripartyAgreementId
      : null;
    this.wirePropsData.title = props.title ? props.title : "Comet - Log Wires";
  }

  @action.bound
  isValidWirePopUpData() {
    if (
      (this.wirePopUpSelectedData.selectedSourcePortfolio.key == "" ||
        this.wirePopUpSelectedData.selectedDestinationPortfolio.key == "") &&
      this.wirePropsData.isPortfolioMandatoryToLogWire
    ) {
      this.setOnSaveStatus(
        FAILURE,
        "Source and Destination Portfolio are mandatory"
      );
      return false;
    }
    if (
      this.wirePopUpSelectedData.selectedSourceAccount.key == "" ||
      this.wirePopUpSelectedData.selectedDestinationAccount.key == ""
    ) {
      this.setOnSaveStatus(
        FAILURE,
        "Source and Destination Account are mandatory"
      );
      return false;
    }
    if (this.wirePopUpSelectedData.selectedFinalWireAmount == "") {
      this.setOnSaveStatus(FAILURE, "Please Specify Final Wire Amount");
      return false;
    }
    if (
      this.wirePropsData.tag &&
      this.wirePropsData.tag == "SEG" &&
      !this.wirePopUpSelectedData.bookTrade
    ) {
      if (
        !confirm(
          "This is a Seg Ia workflow and you have not selected to book mmf. Do you still wish to continue?"
        ) == true
      ) {
        return false;
      }
    }
    if (
      this.wirePropsData.tag &&
      this.wirePropsData.tag != "SEG" &&
      this.wirePopUpSelectedData.bookTrade
    ) {
      if (
        !confirm(
          "This is not a Seg Ia workflow and you have selected to book mmf. MMF will not be booked. " +
          "Do you wish to just log wire ?"
        ) == true
      ) {
        return false;
      } else {
        this.wirePopUpSelectedData.bookTrade = false;
      }
    }
    return true;
  }

  @action.bound
  onSave() {
    if (!this.isValidWirePopUpData()) {
      return;
    }
    let wireWrapper: WireWrapper = {
      sourceAccountId: Number(
        this.wirePopUpSelectedData.selectedSourceAccount.key
      ),
      destinationAccountId: Number(
        this.wirePopUpSelectedData.selectedDestinationAccount.key
      ),
      sourcePortfolioId: Number(
        this.wirePopUpSelectedData.selectedSourcePortfolio.key
      ),
      destinationPortfolioId: Number(
        this.wirePopUpSelectedData.selectedDestinationPortfolio.key
      ),
      wireAmount: Number(this.wirePopUpSelectedData.selectedFinalWireAmount.toString().replaceAll(',', '')),
      reportingCurrencyId: Number(this.wirePropsData.reportingCurrencySpn),
      bookMmf: this.wirePopUpSelectedData.bookTrade,
      wireDateStr: this.wirePopUpSelectedData.selectedWireDate,
      valueDateStr: this.wirePopUpSelectedData.selectedValueDate,
      isTrackingWire: this.wirePopUpSelectedData.isTrackingWire,
      "@CLASS": "com.arcesium.treasury.model.common.WireWrapper",
    };

    let wireRequest: WireRequest = {
      subject: this.wirePopUpSelectedData.selectedSubject,
      subscribers: this.wirePopUpSelectedData.selectedSubscribers,
      cc: this.wirePopUpSelectedData.selectedCC,
      comments: this.wirePopUpSelectedData.selectedComment,
      "@CLASS": "deshaw.wires.common.wrs.domain.WireRequest",
    };

    let input: LogWireAndBookTradeDetail = {
      workflowId: this.wirePropsData.workflowId,
      agreementId: this.wirePropsData.actionableAgreementId,
      wireRequest: wireRequest,
      wireWrapper: wireWrapper,
      "@CLASS": "com.arcesium.treasury.model.common.LogWireAndBookTradeDetail",
    };
    this.logWire(input);
  }

  @action.bound
  setWirePopUpData(response: any) {
    this.wirePopUpSelectedData.selectedWireAmount = Math.abs(
      this.wirePropsData.acmVal
    );
    this.wirePopUpSelectedData.selectedFinalWireAmount = Math.abs(
      this.wirePropsData.acmVal
    );
    let subject: string = response.wireRequest.subject;
    if (this.wirePropsData.acmVal < 0) {
      this.wirePropsData.acmSign = -1;
      subject += " - Margin Pull";
    } else {
      this.wirePropsData.acmSign = 1;
      subject += " - Margin Call";
    }
    this.wirePopUpSelectedData.selectedSubject = subject;
    this.wirePopUpSelectedData.selectedSubscribers =
      response.wireRequest.subscribers;
    this.wirePopUpSelectedData.selectedCC = response.wireRequest.cc;
    this.wirePopUpSelectedData.selectedComment = response.wireRequest.comments;

    let refData: RefData = INITIAL_REF_DATA;
    this.wirePropsData.wirePortfolioData.forEach(
      (e: { myId: string; myAbbreviation: string }) => {
        refData = { key: e.myId, value: e.myAbbreviation };
        this.wirePopUpRefData.sourcePortfolios.push(refData);
        this.wirePopUpRefData.destinationPortfolios.push(refData);
      }
    );

    let cpeWireInstructions = response.cpeWireInstructions;
    let cpeWireInstructionsRefData: RefData[] = [];

    if (cpeWireInstructions != null && cpeWireInstructions.length >= 1) {
      cpeWireInstructions.forEach(
        (e: { instructionId: string; displayName: string }) => {
          refData = { key: e.instructionId, value: e.displayName };
          cpeWireInstructionsRefData.push(refData);
        }
      );
    }

    let legalWireInstructions = response.legalWireInstructions;
    let legalWireInstructionsRefData: RefData[] = [];

    if (legalWireInstructions != null && legalWireInstructions.length >= 1) {
      legalWireInstructions.forEach(
        (e: { instructionId: string; displayName: string }) => {
          refData = { key: e.instructionId, value: e.displayName };
          legalWireInstructionsRefData.push(refData);
        }
      );
    }

    let sourceAccountRefData: RefData[] = legalWireInstructionsRefData;
    let destinationAccountRefData: RefData[] = cpeWireInstructionsRefData;

    if (this.wirePropsData.acmVal < 0) {
      [sourceAccountRefData, destinationAccountRefData] = [
        destinationAccountRefData,
        sourceAccountRefData,
      ];
    }

    this.wirePopUpRefData.sourceAccounts = sourceAccountRefData;
    if (sourceAccountRefData.length == 1) {
      this.wirePopUpSelectedData.selectedSourceAccount = {
        key: sourceAccountRefData[0].key,
        value: sourceAccountRefData[0].value,
      };
    }

    this.wirePopUpRefData.destinationAccounts = destinationAccountRefData;
    if (destinationAccountRefData.length == 1) {
      this.wirePopUpSelectedData.selectedDestinationAccount = {
        key: destinationAccountRefData[0].key,
        value: destinationAccountRefData[0].value,
      };
    }
  }

  @action.bound
  setWireResponseDataAndCallPropAction(apiResponse: any) {
    this.onClose();
    let logMsg = apiResponse.response.logMsg;
    let status = apiResponse.successStatus ? SUCCESS : FAILURE;
    if (logMsg) {
      this.setLogWireStatus(status, logMsg);
    }
    this.wirePropsData.onLoggingWire(
      apiResponse.response,
      this.wirePropsData.acmSign
    );
  }

  fetchWiresData = async (params: DisplayLogWireParams) => {
    try {
      this.inProgress = true;
      const response: any = await getWireData(params);
      this.setWirePopUpData(response);
      this.inProgress = false;
      this.isDataReady = true;
    } catch (error) {
      this.setLogWireStatus(
        FAILURE,
        "Error while fetching Log Wire Popup Data"
      );
      this.inProgress = false;
    }
  };

  logWire = async (params: LogWireAndBookTradeDetail) => {
    try {
      this.inProgress = true;
      const apiResponse: any = await logWireRequest(params);
      this.setWireResponseDataAndCallPropAction(apiResponse);
      this.inProgress = false;
    } catch (error) {
      this.setLogWireStatus(
        FAILURE,
        "Error occurred while logging wire request"
      );
      this.inProgress = false;
    }
  };
}

export default createContext(new WirePopUpStore());
