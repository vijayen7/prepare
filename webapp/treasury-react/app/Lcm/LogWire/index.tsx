import React, { useEffect, useContext } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../../commons/components/ReactLoader";
import { DisplayLogWireParams } from "./models/WireServiceInputData";
import WirePropsData from "./models/WirePropsData";
import WirePopUpStore from "./WirePopUpStore";
import LogWireBookTradeDialog from "./component/LogWireBookTradeDialog";
import StatusComponent from "../../commons/components/StatusComponent";

const LogWirePopUp: React.FC<WirePropsData> = (props: WirePropsData) => {
  const store = useContext(WirePopUpStore);

  useEffect(() => {
    store.setPropsData(props);
    if (props.isOpen) {
      let logWirePopUpParams: DisplayLogWireParams = {
        workflowId: store.wirePropsData.workflowId,
        actionableAgreementId: store.wirePropsData.actionableAgreementId,
        custodianAccountId: store.wirePropsData.custodianAccountId,
        tripartyAgreementId: store.wirePropsData.tripartyAgreementId,
      };
      store.fetchWiresData(logWirePopUpParams);
    }
  }, [props.isOpen]);

  return (
    <React.Fragment>
      <ReactLoader inProgress={store.inProgress} />
      {store.isDataReady && (
        <LogWireBookTradeDialog
          title={store.wirePropsData.title}
          wirePopUpSelectedData={store.wirePopUpSelectedData}
          wirePopUpRefData={store.wirePopUpRefData}
          onSelect={store.onChangeWirePopUpData}
          isOpen={store.wirePropsData.isOpen}
          onSave={store.onSave}
          onClose={store.onClose}
          onSaveStatus={store.saveStatus}
          showOnSaveStatus={store.showOnSaveStatus}
        />
      )}
      <StatusComponent
        status={store.logWireStatus}
        isOpen={store.showLogWireStatus}
        onClose={store.unsetShowLogWireStatus}
        title={"Log Wire Status"}
      />
    </React.Fragment>
  );
};

export default observer(LogWirePopUp);
