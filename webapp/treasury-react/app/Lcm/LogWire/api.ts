import { BASE_URL } from "../../commons/constants";
import {
  DisplayLogWireParams,
  LogWireAndBookTradeDetail,
} from "./models/WireServiceInputData";
import { ArcFetch } from "arc-commons";

const QUERY_ARGS = {
  credentials: "include",
  method: "GET",
};

export const getWireData = (params: DisplayLogWireParams) => {
  const url = `${BASE_URL}service/lcmWiresService/displayLogWirePopup?workflowId=${params.workflowId}&actionableAgreementId=${params.actionableAgreementId}&custodianAccountId=${params.custodianAccountId}&tripartyAgreementId=${params.tripartyAgreementId}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const logWireRequest = (params: LogWireAndBookTradeDetail) => {
  const url = `${BASE_URL}service/lcmWiresService/logWireRequest?logWireAndBookTradeDetail=${encodeURIComponent(
    JSON.stringify(params)
  )}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};
