import React, { Component } from "react";
import { Layout } from "arc-react-components";
import TodaysAgmtSummaryBreakTable from "./TodaysAgmtSummaryBreakTable";
import Table from './Table';
import {
  AGREEMENT_TYPE_TABLES,
  CRIMSON_TABLES,
  INDEX_FOR_HOVERED_COLUMN,
  ROW_OFFSET_FOR_CELL_HIGHLIGHT,
  YELLOW,
  IGNORED_CRIMSON_DIFF_TYPES,
  ACCEPTED_CRIMSON_DIFF_TYPES,
  EXCESS_DEFICIT_DATA_SOURCES
} from '../../../commons/constants';
import { createIdFromParams } from './../../Utils'

export default class AgmtSummaryVsReconcilerSummaryTab extends Component {
  constructor(props) {
    super(props);
    this.handleHoverOn = this.handleHoverOn.bind(this);
    this.handleHoverOff = this.handleHoverOff.bind(this);
    this.onClick = this.onClick.bind(this);
    this.isNull = this.isNull.bind(this);
    this[CRIMSON_TABLES.DAY_ON_DAY_CHANGES] = React.createRef();
    this[CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA] = React.createRef();
    this[CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA] = React.createRef();
    this[CRIMSON_TABLES.BOTTOM_POS_REC_VS_MARGIN_FILE_DATA] = React.createRef();
    this[CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA] = React.createRef();
    this[CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA] = React.createRef();
    this[CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG] = React.createRef();
    this[CRIMSON_TABLES.RECON_STATUS_SUMMARY] = React.createRef();
  }

  //Using this method we are changing Table component css on hover TodaysAgmtSummaryBreakTable
  handleHoverOn(id) {
    id = Number(id);
    let tableName = this.props.data.todaySummaryDetail.tableRows[id][INDEX_FOR_HOVERED_COLUMN].hoverOnTable;
    let diffType = this.props.data.todaySummaryDetail.tableRows[id][INDEX_FOR_HOVERED_COLUMN].hoverOnColumn;
    let elementId = createIdFromParams(tableName, diffType, ROW_OFFSET_FOR_CELL_HIGHLIGHT);
    let elementRef = this[tableName];
    if (!this.isNull(elementRef) && !this.isNull(elementRef.current[elementId]) && !this.isNull(elementRef.current[elementId].style)) {
      elementRef.current[elementId].style.backgroundColor = YELLOW;
    }
  }

  handleHoverOff(id) {
    id = Number(id);
    let tableName = this.props.data.todaySummaryDetail.tableRows[id][INDEX_FOR_HOVERED_COLUMN].hoverOnTable;
    let diffType = this.props.data.todaySummaryDetail.tableRows[id][INDEX_FOR_HOVERED_COLUMN].hoverOnColumn;
    let elementId = createIdFromParams(tableName, diffType, ROW_OFFSET_FOR_CELL_HIGHLIGHT);
    let elementRef = this[tableName];
    if (!this.isNull(elementRef) && !this.isNull(elementRef.current[elementId]) && !this.isNull(elementRef.current[elementId].style)) {
      elementRef.current[elementId].style.backgroundColor = null;
    }
  }

  onClick(id) {
    id = Number(id);
    let excessDeficitDataType = this.props.data.todaySummaryDetail.tableRows[id][INDEX_FOR_HOVERED_COLUMN].hoverOnColumn;
    if (!IGNORED_CRIMSON_DIFF_TYPES.includes(excessDeficitDataType)) {
      let tableName = this.props.data.todaySummaryDetail.tableRows[id][INDEX_FOR_HOVERED_COLUMN].hoverOnTable;
      let excessDeficitDataSources = this.getExcessDeficitDataSources(tableName);
      let showSegDrilldown = tableName === CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG;
      this.props.onClick(excessDeficitDataSources.fromExcessDeficitDataSource, excessDeficitDataSources.toExcessDeficitDataSource,
        _.invert(ACCEPTED_CRIMSON_DIFF_TYPES)[excessDeficitDataType], showSegDrilldown);
    }
  }

  getExcessDeficitDataSources = (crimsonTable) => {
    let excessDeficitDataSources = _.invert(EXCESS_DEFICIT_DATA_SOURCES);
    switch (crimsonTable) {
      case CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG:
      case CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA:
        return this.getExcessDeficitDataSourcesByKey(excessDeficitDataSources,
          EXCESS_DEFICIT_DATA_SOURCES.INTERNAL_DATA, EXCESS_DEFICIT_DATA_SOURCES.MARGIN_FILE);
        break;
      case CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA:
        return this.getExcessDeficitDataSourcesByKey(excessDeficitDataSources,
          EXCESS_DEFICIT_DATA_SOURCES.INTERNAL_DATA, EXCESS_DEFICIT_DATA_SOURCES.POS_REC);
        break;
      case CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA:
              return this.getExcessDeficitDataSourcesByKey(excessDeficitDataSources,
                EXCESS_DEFICIT_DATA_SOURCES.INTERNAL_DATA, EXCESS_DEFICIT_DATA_SOURCES.REPO_REC);
              break;

      case CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA:
      case CRIMSON_TABLES.BOTTOM_POS_REC_VS_MARGIN_FILE_DATA:
        return this.getExcessDeficitDataSourcesByKey(excessDeficitDataSources,
          EXCESS_DEFICIT_DATA_SOURCES.POS_REC, EXCESS_DEFICIT_DATA_SOURCES.MARGIN_FILE);
        break;
    }
  }

  getExcessDeficitDataSourcesByKey = (excessDeficitDataSources, keyA, keyB) => {
    return {
      fromExcessDeficitDataSource: excessDeficitDataSources[keyA],
      toExcessDeficitDataSource: excessDeficitDataSources[keyB]
    }
  }

  isNull(value) {
    if (value) {
      return false;
    }
    return true
  }

  render() {
    let agreementType = this.props.data.agreementType;
    let isTripartyISDA = this.props.data.tag.includes("SEG");
    let tables = AGREEMENT_TYPE_TABLES[agreementType];
    let isInternalVsMarginFile = tables.includes(CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA);
    let isTopPosRecVsMarginFile = tables.includes(CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA);
    let isBottomPosRecVsMarginFile = agreementType == 'MNA' ? true : false;
    isTopPosRecVsMarginFile = isBottomPosRecVsMarginFile ? false : isTopPosRecVsMarginFile;
    let isInternalVsPosRec = tables.includes(CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA);
    let isInternalVsRepoRec = tables.includes(CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA);
    let isInternalVsMarginFileSeg = (isTripartyISDA && tables.includes(CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG));
    let isDayOnDayChanges = tables.includes(CRIMSON_TABLES.DAY_ON_DAY_CHANGES);
    let isReconStatusSummary = tables.includes(CRIMSON_TABLES.RECON_STATUS_SUMMARY);

    return (
      <React.Fragment>
        <div>
          <div>

            <Layout>
              <Layout.Child childId="child1">
                <TodaysAgmtSummaryBreakTable
                  data={
                    this.props.data.todaySummaryDetail
                  }
                  handleHoverOn={this.handleHoverOn}
                  handleHoverOff={this.handleHoverOff}
                  onClick={this.onClick}
                />
              </Layout.Child>
            </Layout>
          </div>

          <div>
            <Layout isColumnType={true}>
              {
                isInternalVsMarginFile ?
                  <Layout.Child className="margin" childId="child1">
                    <Table
                      ref={this[CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA]}
                      data={
                        this.props.data.internalVsMarginFileData
                      }
                    />
                  </Layout.Child>
                  : null
              }
              {
                isTopPosRecVsMarginFile ?
                  <Layout.Child className="margin" childId="child2">
                    <Table
                      ref={this[CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA]}
                      data={
                        this.props.data.posRecVsMarginFileData
                      }
                    />

                  </Layout.Child >
                  : null
              }
              {
                isInternalVsMarginFileSeg ?
                  <Layout.Child className="margin" childId="child3">
                    <Table
                      ref={this[CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG]}
                      data={
                        this.props.data.internalVsMarginFileSegData
                      }
                    />
                  </Layout.Child>
                  : null
              }
              {
                isInternalVsPosRec ?
                  <Layout.Child className="margin" childId="child4">
                    <Table
                      ref={this[CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA]}
                      data={
                        this.props.data.internalVsPosRecData
                      }
                    />
                  </Layout.Child>
                  : null
              }
              {
                isInternalVsRepoRec ?
                  <Layout.Child className="margin" childId="child5">
                    <Table
                      ref={this[CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA]}
                      data={
                        this.props.data.internalVsRepoRecData
                      }
                    />
                  </Layout.Child>
                  : null
              }

            </Layout>
          </div>
          <div>
            <Layout isColumnType={true}>
              {
                isBottomPosRecVsMarginFile ?
                  <Layout.Child className="margin" childId="child1">
                    <Table
                      ref={this[CRIMSON_TABLES.BOTTOM_POS_REC_VS_MARGIN_FILE_DATA]}
                      data={
                        this.props.data.posRecVsMarginFileData
                      }
                    />
                  </Layout.Child >
                  : null
              }
              {
                isDayOnDayChanges ?
                  <Layout.Child className="margin" childId="child2">
                    <Table
                      ref={this[CRIMSON_TABLES.DAY_ON_DAY_CHANGES]}
                      data={
                        this.props.data.dayOnDayChangesData
                      }
                    />
                  </Layout.Child>
                  : null
              }
              {
                isReconStatusSummary ?
                  <Layout.Child className="margin" childId="child3">
                    <Table
                      ref={this[CRIMSON_TABLES.RECON_STATUS_SUMMARY]}
                      data={
                        this.props.data.reconStatusSummaryData
                      }
                    />
                  </Layout.Child>
                  : null
              }
            </Layout>
          </div>
        </div>
      </React.Fragment >
    );
  }
}
