import PropTypes from "prop-types";
import React, { Component } from "react";
import { Panel } from 'arc-react-components';
import { INDEX_FOR_HOVERED_COLUMN } from '../../../commons/constants'


export default class TodaysAgmtSummaryBreakTable extends Component {
  constructor(props) {
    super(props);
    this.hoverOn = this.hoverOn.bind(this);
    this.hoverOff = this.hoverOff.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  hoverOn(e) {
    this.props.handleHoverOn(e.currentTarget.id);
  }

  hoverOff(e) {
    this.props.handleHoverOff(e.currentTarget.id);
  }

  onClick(e) {
    this.props.onClick(e.currentTarget.id);
  }

  render() {
    let data = this.props.data;
    let hoverOn = this.hoverOn;
    let hoverOff = this.hoverOff;
    let onClick = this.onClick;

    if (data === undefined || data == null) {
      return null;
    }
    if (data.tableRows === undefined) {
      return null;
    }

    let cardTitle = "Today's Summary : Excess (Deficit) Diff: " + data.excessDeficitDiff
    return (
      <div className="display--inline-block">
        <Panel title={cardTitle}>
          <table className="table table--no-border">
            <tbody>
              {data.tableRows.length > 0 &&
                data.tableRows.map(function (tableRow, rowIndex) {
                  return (
                    <tr key={rowIndex}>
                      {
                        tableRow.map(function (columnObject, columnIndex) {
                          return <td key={columnIndex}>
                            {columnIndex == INDEX_FOR_HOVERED_COLUMN ?
                              <a id={rowIndex} onMouseEnter={hoverOn} onMouseLeave={hoverOff} onClick={onClick}>
                                {columnObject.value}
                              </a>
                              :
                              columnObject
                            }
                          </td>
                        })
                      }
                    </tr>
                  );

                })
              }
            </tbody>
          </table>
        </Panel>
      </div>
    );
  }
}
