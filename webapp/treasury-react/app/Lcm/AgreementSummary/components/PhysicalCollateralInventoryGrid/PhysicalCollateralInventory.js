import React, { useState, useEffect, Fragment } from "react";
import { IntVsExtTable } from "./IntVsExtTable";
import { SecurityCollateralDetail } from "./SecurityCollateralDetail";
import { Dialog, Layout, Message } from "arc-react-components";
import ArcDataGrid from "arc-data-grid";
import { getGridColumns } from "./grid/gridColumns";
import { GridOptions } from "./grid/gridOptions";
import {
  DIRECTIONS,
  INCOMING,
  INITIAL_PORTFOLIO_VALIDATOR_RESULT,
  MESSAGES,
  INITIAL_DIALOG_STATE,
} from "./constants";
import { SUCCESS, FAILURE } from "./../../../../commons/constants";
import ButtonWithPause from "./../../../../commons/components/ButtonWithPause";
import AutoSelectionDetail from "./AutoSelectionDetail";
import QuantityDialog from "./QuantityDialog";
import {
  validatePortfolio,
  bookTransfers,
  getPhysicalCollateralAgreementDataView,
} from "./api";
import {
  validateAndGetPositionTransferInputList,
  populateValidationResponses,
  updateSelectedRows,
  getSecurityCollateralDetail,
  populatePostHaircutMV,
  populateSimualtedConcentration,
} from "./utils";
import { ReactLoader } from "commons/components/ReactLoader";
import StatusComponent from "commons/components/StatusComponent";

export default function PhysicalCollateralInventory(props) {
  const [forceSelectedRowIds, setForceSelectedRowIds] = useState([]);
  const [inventoryPositions, setInventoryPositions] = useState([]);
  const [isInventoryDialogOpen, setIsInventoryDialogOpen] = useState(true);
  const [showTransferQuantityDialog, setShowTransferQuantityDialog] = useState(
    false
  );
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const [transferQuantity, setTransferQuantity] = useState("");
  const [transferQuantityId, setTransferQuantityId] = useState("");
  const [validatorResult, setValidatorResult] = useState(INITIAL_PORTFOLIO_VALIDATOR_RESULT);
  const [isLoading, setIsLoading] = useState(false);
  const [alertDialogState, setAlertDialogState] = useState(INITIAL_DIALOG_STATE)
  const [securityCollateralDetail, setSecurityCollateralDetail] = useState({})
  const [transferBookingSuccessStatus, setTransferBookingSuccessStatus] = useState(false)
  const [autoSelectionResult, setAutoSelectionResult] = useState(
    props.autoSelectionResult
  );
  const [
    stagedPhysicalCollateralAgreementDataId,
    setStagedPhysicalCollateralAgreementDataId,
  ] = useState(props.stagedPhysicalCollateralAgreementDataId);
  const [
    physicalCollateralPositionData,
    setPhysicalCollateralPositionData,
  ] = useState(props.physicalCollateralPositionData);
  const [actualSecurityMovement, setActualSecurityMovement] = useState(
    props.physicalCollateralInputParam.autoSelectionInputParam
      .actualSecurityMovement
  );
  const [applyOptimizationRules, setApplyOptimizationRules] = useState(
    props.physicalCollateralInputParam.autoSelectionInputParam
      .applyOptimizationRules
  );
  const [inventorySettleDates, setInventorySettleDates] = useState(
    props.inventorySettleDates
  );

  useEffect(() => {
    let mandatoryPullbackPositions = [];
    let sourceAutoSelectedPositions = [];
    let destinationAutoSelectedPositions = [];
    let otherInventoryPositions = [];
    let forceSelectedRowIds = [];
    let selectedRowIds = [];
    if (physicalCollateralPositionData) {
      for (const [index, element] of physicalCollateralPositionData.entries()) {
        let inventoryPosition = {
          dataId: index + 1,
          selectedQuantity: "",
          selectedDirection: null,
          postHaircutMV: "",
          comments: "",
          validationResponse: "",
          ...element,
        };
        if (element.inventorySettleDateQuantities) {
          inventoryPosition = {
            ...inventoryPosition,
            ...element.inventorySettleDateQuantities,
          };
        }

        if (inventoryPosition.transferQuantity) {
          inventoryPosition.isSelected = true;
          inventoryPosition.selectedQuantity =
            inventoryPosition.transferQuantity;
          inventoryPosition.selectedDirection = DIRECTIONS.find(
            (x) => _.upperCase(x.value) == inventoryPosition.transferDirection
          );
          selectedRowIds.push(inventoryPosition.dataId);

          if (inventoryPosition.destinationForceTransferQuantity) {
            forceSelectedRowIds.push(inventoryPosition.dataId);
            mandatoryPullbackPositions.push(inventoryPosition);
          } else {
            if (inventoryPosition.selectedDirection.value === INCOMING) {
              destinationAutoSelectedPositions.push(inventoryPosition);
            } else {
              sourceAutoSelectedPositions.push(inventoryPosition);
            }
          }
        } else {
          otherInventoryPositions.push(inventoryPosition);
        }
      }
    }
    let allInventoryPositions = _.concat(
      mandatoryPullbackPositions,
      destinationAutoSelectedPositions,
      sourceAutoSelectedPositions,
      otherInventoryPositions
    );
    setForceSelectedRowIds(forceSelectedRowIds);
    setSelectedRowIds(selectedRowIds);
    refreshInventoryDialog(allInventoryPositions);
  }, [physicalCollateralPositionData]);

  useEffect(() => {
    if (validatorResult.validationSuccess != undefined) {
      setInventoryPositions(populateValidationResponses(inventoryPositions, validatorResult))
    }
  }, [validatorResult])

  const refreshInventoryDialog = (newInventoryPositions) => {
    populateSimualtedConcentration(newInventoryPositions);
    populatePostHaircutMV(newInventoryPositions);
    setInventoryPositions(newInventoryPositions);
    setSecurityCollateralDetail(getSecurityCollateralDetail(newInventoryPositions, props.cometData.internalED));
    setValidatorResult({ ...validatorResult, validationSuccess: undefined });
  }

  const closeInventoryDialog = () => {
    setIsInventoryDialogOpen(false);
    if (props.setIsInventoryDialogOpen) props.setIsInventoryDialogOpen(false);
  };

  const onChangeDirection = (id, newDirection) => {
    let inventoryPositionsCopy = inventoryPositions;
    let inventoryPosition = inventoryPositionsCopy.find(
      (item) => item.dataId === id
    );
    inventoryPosition.selectedDirection = newDirection;
    refreshInventoryDialog(inventoryPositionsCopy);
  };

  const onEditQuantity = (id, val) => {
    setShowTransferQuantityDialog(true);
    setTransferQuantity(val);
    setTransferQuantityId(id);
  };

  const onChangeQuantity = (id, val) => {
    let inventoryPositionsCopy = inventoryPositions;
    let inventoryPosition = inventoryPositionsCopy.find(
      (item) => item.dataId === id
    );
    inventoryPosition.selectedQuantity = val;
    refreshInventoryDialog(inventoryPositionsCopy);
    setShowTransferQuantityDialog(false);
  };

  const onCloseTransferDialog = () => {
    setShowTransferQuantityDialog(false);
  };

  const onChangeActualSecurityMovement = (e) =>
    setActualSecurityMovement(e.target.value);

  const onChangeApplyOptimizationRules = (e) =>
    setApplyOptimizationRules(e.target.checked);

  const onChangeSelectedRowIds = (ids) => {
    let selectedRowIds = [...new Set([...ids, ...forceSelectedRowIds])];
    setInventoryPositions(updateSelectedRows(inventoryPositions, selectedRowIds))
    setSelectedRowIds(selectedRowIds);
    refreshInventoryDialog(inventoryPositions);
  };

  const getRowId = (row, _) => row.dataId;

  const columnProps = {
    onChangeDirection: onChangeDirection,
    onEditQuantity: onEditQuantity,
    settleDatesList: inventorySettleDates ? inventorySettleDates : [],
  };

  const DEFAULT_PINNED_COLUMNS = { gboType: true, spn: true };

  const gridColumns = React.useMemo(() => getGridColumns(columnProps), [
    columnProps,
  ]);

  const showAlert = (message, status) => {
    setAlertDialogState({ ...alertDialogState, isOpen: true, status: { message: message, status: status } })
  }

  const onClickRefresh = async () => {
    try {
      setIsLoading(true);
      let response = await getPhysicalCollateralAgreementDataView({
        ...props.physicalCollateralInputParam,
        autoSelectionInputParam: {
          "@CLASS":
            "com.arcesium.treasury.model.lcm.physicalcollateral.autochooser.PhysicalCollateralAutoSelectionInputParam",
          actualSecurityMovement: actualSecurityMovement || 0,
          applyOptimizationRules: applyOptimizationRules,
        },
      });
      if (response) {
        setStagedPhysicalCollateralAgreementDataId(
          response.stagedPhysicalCollateralAgreementDataId
        );
        setPhysicalCollateralPositionData(
          response.physicalCollateralPositionDataView
        );
        setAutoSelectionResult(response.autoSelectionResult);
        setInventorySettleDates(response.inventorySettleDates);
      }
    } catch (error) {
      showAlert(MESSAGES.FETCH_INVENTORY_POSITIONS_FAILURE, FAILURE);
    } finally {
      setIsLoading(false);
    }
  };

  const onClickValidateSecurities = async () => {

    if (selectedRowIds.length <= 0) {
      showAlert(MESSAGES.NO_POSITIONS_SELECTED_FOR_TRANSFER, FAILURE)
      return
    }

    let selectedPositionDetail = validateAndGetPositionTransferInputList(inventoryPositions, selectedRowIds)
    if (!selectedPositionDetail) {
      showAlert(MESSAGES.INVALID_TRANSFER_DETAILS, FAILURE)
      return
    }

    try {
      setIsLoading(true);
      let validationResults = await validatePortfolio(stagedPhysicalCollateralAgreementDataId, selectedPositionDetail)
      setValidatorResult(validationResults)
      showAlert(MESSAGES.VALIDATION_COMPLETE, SUCCESS)
    } catch (error) {
      console.log(error)
      showAlert(MESSAGES.VALIDATE_PORTFOLIO_FAILURE, FAILURE)
    } finally {
      setIsLoading(false)
    }
  }

  const onClickBookTransfers = async () => {
    try {
      setIsLoading(true);
      let response = await bookTransfers(stagedPhysicalCollateralAgreementDataId)
      if (response?.message) {
        if (response.successStatus && response.response !== undefined) {
          props.updateBookingReferenceId(response.response);
        }
        showAlert(response.message, response.successStatus ? SUCCESS : FAILURE)
        setTransferBookingSuccessStatus(response.successStatus)
      } else {
        showAlert(MESSAGES.BOOK_TRANSFER_FAILURE, FAILURE)
      }
    } catch (error) {
      console.log(error)
      showAlert(MESSAGES.BOOK_TRANSFER_FAILURE, FAILURE)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <>
      <ReactLoader inProgress={isLoading} />
      <Dialog
        isOpen={isInventoryDialogOpen}
        onClose={closeInventoryDialog}
        title="Inventory Positions"
        style={{
          height: "85%",
          maxWidth: "95%"
        }}
      >
        <Layout
          isColumnType={false}
          className="border"
          style={{ height: "100%" }}
        >
          <Layout.Child key="actionPanel" size="fit" childId="actionPanelChild">
            <div style={{ textAlign: "right" }}>
              <button className="token success" disabled={transferBookingSuccessStatus} onClick={onClickValidateSecurities} >Validate Securities</button>
              &nbsp;&nbsp;
              <ButtonWithPause onClick={onClickBookTransfers} disabled={!validatorResult.validationSuccess || transferBookingSuccessStatus} data={<Fragment>Book Transaction(s)</Fragment>}/>
            </div>
          </Layout.Child>
          <Layout.Child
            key="autoSelectionPanel"
            size="fit"
            childId="autoSelectionPanelChild"
          >
            <AutoSelectionDetail autoSelectionResult={autoSelectionResult} />
          </Layout.Child>
          <Layout.Child key="dataPanel" childId="dataPanelChild">
            <Layout>
              <Layout.Child key="grid" size={3} childId="dataGrid">
                {physicalCollateralPositionData && physicalCollateralPositionData.length > 0 ?
                  inventoryPositions && inventoryPositions.length > 0 ?
                    <ArcDataGrid
                      rows={inventoryPositions}
                      columns={gridColumns}
                      configurations={GridOptions}
                      selectedRowIds={selectedRowIds}
                      onSelectedRowIdsChange={onChangeSelectedRowIds}
                      getRowId={getRowId}
                      pinnedColumns={DEFAULT_PINNED_COLUMNS}
                    /> : <Message type={Message.Type.SECONDARY}>Loading</Message>
                  : <Message type={Message.Type.WARNING}>No Data Found</Message>
                }
              </Layout.Child>
              <Layout.Divider isResizable key="resizer" childId="divider" />
              <Layout.Child
                key="physicalCollateralBottomPanel"
                collapsible
                size="fit"
                childId="physicalCollateralBottomChild"
              >
                <Layout>
                  <Layout.Child
                    key="autoSelectionParamPanel"
                    size="fit"
                    childId="autoSelectionParamChild"
                  >
                    <fieldset className="margin">
                      <legend>Auto Selection Input</legend>
                      <label className="margin">Actual Security Movement</label>
                      <input
                        type="number"
                        className="margin"
                        value={actualSecurityMovement}
                        onChange={onChangeActualSecurityMovement}
                      />
                      <label className="margin">Apply Optimization Rules</label>
                      <input
                        className="margin"
                        type="checkbox"
                        defaultChecked={applyOptimizationRules}
                        onChange={onChangeApplyOptimizationRules}
                      />
                      <button className="margin" onClick={onClickRefresh}>
                        Refresh
                      </button>
                    </fieldset>
                  </Layout.Child>
                  <Layout.Child key="edDetails" size="fit" childId="edData">
                    <div className="layout--flex padding">
                      <IntVsExtTable cometData={props.cometData} />
                      <SecurityCollateralDetail {...securityCollateralDetail} />
                    </div>
                  </Layout.Child>
                </Layout>
              </Layout.Child>
            </Layout>
          </Layout.Child>
        </Layout>
      </Dialog>
      <QuantityDialog
        dataId={transferQuantityId}
        selectedQuantity={transferQuantity}
        isOpen={showTransferQuantityDialog}
        onSave={onChangeQuantity}
        onClose={onCloseTransferDialog}
      />
      <StatusComponent {...alertDialogState}
        title="Alert"
        onClose={() => setAlertDialogState({ ...alertDialogState, isOpen: false })}
      />
    </>
  );
}
