import React from 'react';
import { validateAndGetRoundedValue } from './utils';
export const IntVsExtTable = (props) => {
  return (
    <table className="table treasury-table--data-summary" id="internalVsExternalId">
      <tbody>
        <tr>
          <th colSpan="6">Internal vs External Data</th>
        </tr>
        <tr>
          <th>Source</th>
          <th>Exposure</th>
          <th>Requirement</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>E/D</th>
        </tr>
        <tr>
          <td>Internal</td>
          <td id="internalExposure">
            {validateAndGetRoundedValue(props.cometData.internalExposure)}
          </td>
          <td id="internalRequirement">
            {validateAndGetRoundedValue(props.cometData.internalRequirement)}
          </td>
          <td id="internalCashCollateral">
            {validateAndGetRoundedValue(props.cometData.internalCashCollateral)}
          </td>
          <td id="internalSecuritiesCollateral">
            {validateAndGetRoundedValue(props.cometData.internalSecuritiesCollateral)}
          </td>
          <td id="internalED">
            {validateAndGetRoundedValue(props.cometData.internalED)}
          </td>
        </tr>
        <tr>
          <td>External</td>
          <td id="externalExposure">
            {validateAndGetRoundedValue(props.cometData.externalExposure)}
          </td>
          <td id="externalRequirement">
            {validateAndGetRoundedValue(props.cometData.externalRequirement)}
          </td>
          <td id="externalCashCollateral">
            {validateAndGetRoundedValue(props.cometData.externalCashCollateral)}
          </td>
          <td id="externalSecuritiesCollateral">
            {validateAndGetRoundedValue(props.cometData.externalSecuritiesCollateral)}
          </td>
          <td id="externalED">
            {validateAndGetRoundedValue(props.cometData.externalED)}
          </td>
        </tr>
        <tr>
          <td>Diff</td>
          <td id="diffExposure">
            {validateAndGetRoundedValue(props.cometData.diffExposure)}
          </td>
          <td id="diffRequirement">
            {validateAndGetRoundedValue(props.cometData.diffRequirement)}
          </td>
          <td id="diffCashCollateral">
            {validateAndGetRoundedValue(props.cometData.diffCashCollateral)}
          </td>
          <td id="diffSecuritiesCollateral">
            {validateAndGetRoundedValue(props.cometData.diffSecuritiesCollateral)}
          </td>
          <td id="diffED">
            {validateAndGetRoundedValue(props.cometData.diffED)}
          </td>
        </tr>
      </tbody>
    </table>
  );
};
