import React from 'react';
import { validateAndGetRoundedValue } from './utils';

export const SecurityCollateralDetail = (props: SecurityCollateralDetail) => {
  return (
    <table className="table" id="securityCollateralDetail">
      <tbody>
        <tr>
          <th>Security Collateral Details</th>
          <th>Figures (in USD)</th>
        </tr>
        <tr>
          <td>Available inventory post haircut MV</td>
          <td id="availableMarketValue">{validateAndGetRoundedValue(props.availableMarketValue)}</td>
        </tr>
        <tr>
          <td>Outgoing collateral frozen post haircut MV</td>
          <td id="outgoingFrozenCollateral">{validateAndGetRoundedValue(props.outgoingFrozenCollateral)}</td>
        </tr>
        <tr>
          <td>Incoming collateral frozen post haircut MV</td>
          <td id="incomingFrozenCollateral">{validateAndGetRoundedValue(props.incomingFrozenCollateral)}</td>
        </tr>
        <tr>
          <td>Projected inventory post movement</td>
          <td id="projectedMarketValue">{validateAndGetRoundedValue(props.projectedMarketValue)}</td>
        </tr>
        <tr>
          <td>Projected pledged collateral post movement</td>
          <td id="projectedPledgedCollateral">{validateAndGetRoundedValue(props.projectedPledgedCollateral)}</td>
        </tr>
        <tr>
          <td>Projected E/D post movement</td>
          <td id="projectedED">{validateAndGetRoundedValue(props.projectedED)}</td>
        </tr>
      </tbody>
    </table>
  );
};

interface SecurityCollateralDetail {
  availableMarketValue: number,
  outgoingFrozenCollateral: number,
  incomingFrozenCollateral: number,
  projectedMarketValue: number,
  projectedPledgedCollateral: number,
  projectedED: number
}
