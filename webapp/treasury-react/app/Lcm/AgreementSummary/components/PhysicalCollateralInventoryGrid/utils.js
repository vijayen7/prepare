import { formatNumberUsingCommas } from '../../../../commons/grid/formatters';
import { OUTGOING, INCOMING } from './constants'

export const isValidNumber = (value) => {
  if (_.isUndefined(value)) {
    return false;
  }

  const parsedValue = parseFloat(value);
  return _.isNumber(parsedValue) && !isNaN(parsedValue);
};

export const validateAndGetFormattedText = (value, defaultText = "n/a") => {
  return isValidNumber(value)
    ? formatNumberUsingCommas(parseFloat(value).toFixed(3))
    : defaultText;
};

export const validateAndGetRoundedValue = (value, defaultText = "n/a") => {
  return isValidNumber(value)
    ? formatNumberUsingCommas(Math.round(parseFloat(value)))
    : defaultText;
};

export const validateAndGetPositionTransferInputList = (inventoryPositions, selectedRowIds) => {
  let selectedPositions = inventoryPositions.filter(e => selectedRowIds.includes(e.dataId))
  return validateSelectedPositionData(selectedPositions) ? selectedPositions.map(e => getPositionTransferInput(e)) : false
}

const validateSelectedPositionData = (selectedPositions) => {
  let isTransferDetailInvalid = selectedPositions.map(inventoryPosition =>
    !(inventoryPosition?.selectedQuantity && inventoryPosition?.selectedDirection?.value)
  ).includes(true)
  return !isTransferDetailInvalid
}

const getPositionTransferInput = (inventoryPosition) => {
  return {
    '@CLASS': 'com.arcesium.treasury.model.lcm.physicalcollateral.PhysicalCollateralPositionTransferInput',
    physicalCollateralPositionDataKey: {
      '@CLASS': 'com.arcesium.treasury.model.lcm.physicalcollateral.PhysicalCollateralPositionDataKey',
      spn: inventoryPosition.spn,
      bookId: inventoryPosition.bookId
    },
    physicalCollateralPositionTransferDetail: {
      '@CLASS': 'com.arcesium.treasury.model.lcm.physicalcollateral.PhysicalCollateralPositionTransferDetail',
      transferQuantity: inventoryPosition.selectedQuantity,
      transferDirection: inventoryPosition.selectedDirection.value.toUpperCase()
    }
  }
}

export const populateValidationResponses = (inventoryPositions, validatorResult) => {
  let transferQuantityValidationResultsMap = getValidationResultMap(validatorResult.transferQuantityValidationResults);

  for (const inventoryPosition of inventoryPositions) {
    let validationResponse = undefined
    let physicalCollateralPositionDataKey = getPhysicalCollateralPositionDataKey(inventoryPosition.spn, inventoryPosition.bookId)
    if (transferQuantityValidationResultsMap[physicalCollateralPositionDataKey] || inventoryPosition.isSelected) {
      let transferQuantityValidationResult = getStringValue(transferQuantityValidationResultsMap[physicalCollateralPositionDataKey])
      validationResponse = transferQuantityValidationResult != "" ? "FAILED - " + transferQuantityValidationResult : "SUCCESS - Selected quantity available. Concentration threshold not breached"
    }
    inventoryPosition.validationResponse = validationResponse
  }
  return inventoryPositions
}

const getStringValue = (value) => {
  if (value) {
    return value
  }
  return ""
}

export const updateSelectedRows = (inventoryPositions, selectedRowIds) => {
  for (const inventoryPosition of inventoryPositions) {
    inventoryPosition.isSelected = selectedRowIds.includes(
      inventoryPosition.dataId
    );
  }
  return inventoryPositions;
};

export const populatePostHaircutMV = (inventoryPositions) => {
  inventoryPositions.forEach((inventoryPosition) => {
    if (
      inventoryPosition.isSelected &&
      isValidNumber(inventoryPosition.selectedQuantity) &&
      inventoryPosition.selectedDirection
    ) {
      inventoryPosition.postHaircutMV =
        inventoryPosition.selectedQuantity *
        (isValidNumber(inventoryPosition.dirtyPriceUsd)
          ? inventoryPosition.dirtyPriceUsd
          : 0) *
        (isValidNumber(inventoryPosition.haircut) &&
          inventoryPosition.haircut >= 0 &&
          inventoryPosition.haircut <= 100
          ? 1 - inventoryPosition.haircut / 100
          : 0);
    } else {
      inventoryPosition.postHaircutMV = undefined;
    }
  });
};

export const calculatePostHaircutMVFromPrevDayRule = (inventoryPosition) => {
  if (
    inventoryPosition.isSelected &&
    isValidNumber(inventoryPosition.selectedQuantity) &&
    inventoryPosition.selectedDirection
  ) {
    let prevDayPostHaircutMV =
      inventoryPosition.selectedQuantity *
      (isValidNumber(inventoryPosition.dirtyPriceUsd)
        ? inventoryPosition.dirtyPriceUsd
        : 0) *
      (isValidNumber(inventoryPosition.prevDayHaircut) &&
        inventoryPosition.prevDayHaircut >= 0 &&
        inventoryPosition.prevDayHaircut <= 100
        ? 1 - inventoryPosition.prevDayHaircut / 100
        : 0);
    return prevDayPostHaircutMV;
  } else {
    return 0.0
  }
};



export const populateSimualtedConcentration = (inventoryPositions) => {
  let totalMarketValue = 0;
  inventoryPositions.forEach((inventoryPosition) => {
    const simulatedPostHaircutPledgedMarketValue = getSimulatedPostHaircutPledgedMarketValue(
      inventoryPosition
    );
    inventoryPosition.simulatedPostHaircutPledgedMarketValue = simulatedPostHaircutPledgedMarketValue;
    if (!_.isUndefined(simulatedPostHaircutPledgedMarketValue)) {
      totalMarketValue += simulatedPostHaircutPledgedMarketValue;
    }
  });

  if (totalMarketValue) {
    inventoryPositions.forEach((inventoryPosition) => {
      if (
        isValidNumber(inventoryPosition.simulatedPostHaircutPledgedMarketValue)
      ) {
        inventoryPosition.simulatedConcentration =
          (inventoryPosition.simulatedPostHaircutPledgedMarketValue /
            totalMarketValue) *
          100;
      }
    });
  }
};

const getSimulatedPledgedCollateral = (inventoryPositions) => {
  let totalMarketValue = 0;
  for (const inventoryPosition of inventoryPositions) {
    const simulatedPostHaircutDestinationMarketValue = getSimulatedPostHaircutPledgedMarketValue(
      inventoryPosition
    );
    if (!_.isUndefined(simulatedPostHaircutDestinationMarketValue)) {
      totalMarketValue += simulatedPostHaircutDestinationMarketValue;
    }
  }
  return totalMarketValue;
};

const getSimulatedPostHaircutPledgedMarketValue = (inventoryPosition) => {
  const {
    pledgedQuantity,
    dirtyPriceUsd,
    selectedQuantity,
    selectedDirection,
    isSelected,
    haircut,
  } = inventoryPosition;

  if (
    (isSelected && isValidNumber(selectedQuantity) && selectedDirection) ||
    isValidNumber(pledgedQuantity)
  ) {
    let simulatedPledgedQuantity = isValidNumber(pledgedQuantity)
      ? pledgedQuantity
      : 0.0;

    if (isSelected) {
      simulatedPledgedQuantity += _.isEqual(OUTGOING, selectedDirection?.value)
        ? selectedQuantity
        : -1 * selectedQuantity;
    }

    return (
      simulatedPledgedQuantity *
      (isValidNumber(dirtyPriceUsd) ? dirtyPriceUsd : 0) *
      (isValidNumber(haircut) && haircut >= 0 && haircut <= 100
        ? 1 - haircut / 100
        : 0)
    );
  }

  return undefined;
};

const getValidNumberOrZero = (value) => {
  return getValidNumberOrDefault(value, 0)
}

const getValidNumberOrDefault = (value, defaultValue) => {
  if (!isNaN(value)) {
    return Number(value)
  }
  return defaultValue
}

const getPhysicalCollateralPositionDataKey = (spn, bookId) => `${spn}_${bookId}`

function getValidationResultMap(validatorResults) {
  let validationResultsMap = {};
  if (!validatorResults) return validationResultsMap

  validatorResults.forEach(e => {
    if (e.comment != "") {
      validationResultsMap[getPhysicalCollateralPositionDataKey(e.physicalCollateralPositionDataKey.spn, e.physicalCollateralPositionDataKey.bookId)] = e.comment;
    }
  });
  return validationResultsMap;
}

export const getSecurityCollateralDetail = (inventoryPositions, internalExcessDeficit) => {
  let availableMarketValue = 0;
  let outgoingFrozenCollateral = 0;
  let incomingFrozenCollateral = 0;
  for (const inventoryPosition of inventoryPositions) {
    availableMarketValue += getValidNumberOrZero(inventoryPosition.postHaircutMarketValue)
    if (inventoryPosition.isSelected && inventoryPosition.selectedDirection?.value == OUTGOING) {
      outgoingFrozenCollateral += getValidNumberOrZero(inventoryPosition.postHaircutMV)
    } else if (inventoryPosition.isSelected && inventoryPosition.selectedDirection?.value == INCOMING) {
      incomingFrozenCollateral += getValidNumberOrZero(inventoryPosition.postHaircutMV)
      if (inventoryPosition.prevDayRuleType == "INCLUDE" && (!inventoryPosition.ruleType || inventoryPosition.ruleType == "EXCLUDE")) {
        incomingFrozenCollateral += calculatePostHaircutMVFromPrevDayRule(inventoryPosition)
      }
    }
  }
  return {
    availableMarketValue: availableMarketValue,
    outgoingFrozenCollateral: outgoingFrozenCollateral,
    incomingFrozenCollateral: incomingFrozenCollateral,
    projectedMarketValue: availableMarketValue - outgoingFrozenCollateral + incomingFrozenCollateral,
    projectedPledgedCollateral: getSimulatedPledgedCollateral(inventoryPositions),
    projectedED: getValidNumberOrZero(internalExcessDeficit) + outgoingFrozenCollateral - incomingFrozenCollateral
  }
}
