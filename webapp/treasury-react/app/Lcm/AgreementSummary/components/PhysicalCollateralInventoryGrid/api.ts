import { BASE_URL } from "../../../../commons/constants";
import { ArcFetch } from 'arc-commons';

const getQueryParams = (params) => ({
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
  credentials: 'include',
  method: 'POST',
  body: params
});

export const validatePortfolio = (stagedPhysicalCollateralAgreementDataId: number, physicalCollateralPositionTransferInputList: Array<any>) => {
  const url = `${BASE_URL}service/physicalCollateralWorkflowManager/getPhysicalCollateralPortfolioValidationResult`
  const params = `stagedPhysicalCollateralAgreementDataId=${stagedPhysicalCollateralAgreementDataId
    }&physicalCollateralPositionTransferInputList=${encodeURIComponent(JSON.stringify(physicalCollateralPositionTransferInputList))
    }&format=JSON&inputFormat=JSON`

  return ArcFetch.getJSON(url, getQueryParams(params));
}

export const bookTransfers = (stagedPhysicalCollateralAgreementDataId: number) => {
  const url = `${BASE_URL}service/physicalCollateralWorkflowManager/bookPhysicalCollateralPositionTransfers`
  const params = `stagedPhysicalCollateralAgreementDataId=${stagedPhysicalCollateralAgreementDataId}&format=JSON`

  return ArcFetch.getJSON(url, getQueryParams(params));
}

export const getPhysicalCollateralAgreementDataView = (
  physicalCollateralInputParam: any
) => {
  const url = `${BASE_URL}service/physicalCollateralWorkflowManager/getPhysicalCollateralAgreementDataView`;
  const params = `physicalCollateralInputParam=${encodeURIComponent(
    JSON.stringify(physicalCollateralInputParam)
  )}&format=JSON&inputFormat=JSON`;
  return ArcFetch.getJSON(url, getQueryParams(params));
};
