import React from "react";
import { ComboBox } from "arc-react-components";
import { DIRECTIONS } from "../constants";
import MapData from "../../../../../commons/models/MapData";

const Direction: React.FC<any> = ({ context, onChangeDirection }) => {
  const onChange = (val: MapData) => {
    onChangeDirection(context.row.dataId, val);
  };

  return (
    <ComboBox
      options={DIRECTIONS}
      value={context.row.selectedDirection}
      onChange={onChange}
      readOnly={context.row.destinationForceTransferQuantity ? true : false}
    />
  );
};

export default Direction;
