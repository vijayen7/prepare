import ArcDataGrid from "arc-data-grid";

export const GridOptions: ArcDataGrid.GridConfiguration = {
  clickableRows: true,
  getExportFileName: ({ fileType, all }) => {
    return `${
      all ? "Inventory Positions" : "Inventory Positions Current View"
    }`;
  },
  timezone: "Asia/Calcutta",
  enableCheckboxes: true,
};
