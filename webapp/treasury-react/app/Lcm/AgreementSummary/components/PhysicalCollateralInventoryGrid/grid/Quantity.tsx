import React from "react";
import { validateAndGetFormattedText } from "../utils";

const Quantity: React.FC<any> = ({ context, onEditQuantity }) => {
  const onclick = () => {
    onEditQuantity(context.row.dataId, context.row.selectedQuantity);
  };

  return (
    <>
      <i
        title="Edit"
        className="icon-edit--block"
        onClick={(e) => {
          e.stopPropagation();
          onclick();
        }}
      ></i>
      &nbsp;
      <b>{validateAndGetFormattedText(context.row.selectedQuantity, "")}</b>
    </>
  );
};

export default Quantity;
