import React from "react";
import { roundWithAtmostDecimal } from "commons/util";
import { isValidNumber } from "../utils";
import { RULE_TYPE } from "../constants";
import _ from "lodash";

const Concentration: React.FC<any> = ({ value, context }) => {
  let ruleType = context.row.ruleType;
  let concentrationThreshold = context.row.concentrationThreshold;

  const getFormattedValue = (value) => roundWithAtmostDecimal(value, 3) + "%";

  if (isValidNumber(value)) {
    if (_.isEqual(ruleType, RULE_TYPE.EXCLUDE)) {
      return (
        <span className={`token ${value > 0 ? "critical" : "success"}`}>
          {getFormattedValue(value)}
        </span>
      );
    }

    if (isValidNumber(concentrationThreshold)) {
      return (
        <span
          className={`token ${
            value > concentrationThreshold ? "critical" : "success"
          }`}
        >
          {getFormattedValue(value)}
        </span>
      );
    }

    return <>{getFormattedValue(value)}</>;
  }

  return <></>;
};

export default Concentration;
