import React from "react";
import Direction from "./Direction";
import Quantity from "./Quantity";
import SPN from "./SPN";
import Concentration from "./Concentration";
import { validateAndGetFormattedText, isValidNumber } from "../utils";
import { RULE_TYPE } from "../constants";
import { roundWithAtmostDecimal } from "commons/util";
import _ from "lodash";

export const getGridColumns = (props: any) => {
  const getFormattedColumn = (
    identity: any,
    cell: (value: any, context: any) => JSX.Element,
    header?: string,
    width?: number,
    ...args: any[]
  ) => {
    let formattedColumn: any = {
      identity: identity,
      Cell: cell,
      ...args,
    };
    if (header) {
      formattedColumn.header = header;
    }
    if (width) {
      formattedColumn.width = width;
    }
    return formattedColumn;
  };

  const getFormattedNumericColumn = (
    identity: any,
    header?: string,
    width?: number
  ) =>
    getFormattedColumn(
      identity,
      (value) => (
        <>
          {isValidNumber(value)
            ? validateAndGetFormattedText(value)
            : undefined}
        </>
      ),
      header,
      width
    );

  const getFormattedTextColumn = (
    identity: any,
    header?: string,
    width?: number
  ) =>
    getFormattedColumn(
      identity,
      (value) => <>{value ? value : undefined}</>,
      header,
      width
    );

  const getSettleDateQuantitiesGroupedColumn = () => {
    let settleDateQuantityColumns: any[] = [];
    props.settleDatesList.forEach((e) => {
      settleDateQuantityColumns.push(getFormattedNumericColumn(e, e));
    });
    let settleDateQuantityGroupedColum = {
      identity: "settleDateQuantities",
      header: "Inventory Settle Date Quantity",
      columns: [
        ...settleDateQuantityColumns,
        getFormattedNumericColumn("lentQuantity"),
        getFormattedNumericColumn(
          "minSettleDateQuantity",
          "Minimum Settle Date Quantity"
        ),
        getFormattedNumericColumn("tradeDateQuantity"),
        getFormattedNumericColumn("frozenQuantity"),
        getFormattedNumericColumn("brokerSettleDateQuantity"),
      ],
    };
    return settleDateQuantityGroupedColum;
  };

  const getReferenceDataColumn = () => {
    let referenceDataColumn = {
      identity: "referenceDataDetails",
      header: "Reference Data Details",
      columns: [
        getFormattedTextColumn("sourceCustodianAccount", undefined, 180),
        getFormattedTextColumn("destinationCustodianAccount", undefined, 180),
        getFormattedTextColumn("legalEntity", undefined, 160),
        getFormattedTextColumn("cpeEntity", undefined, 160),
        getFormattedTextColumn("agreementType"),
        getFormattedTextColumn("bookId"),
      ],
    };
    return referenceDataColumn;
  };

  const getInventoryRuleOutputValue = (value, context) => (
    <>{_.isEqual(context.row.ruleType, RULE_TYPE.INCLUDE) ? value : "n/a"}</>
  );

  const getInventoryRuleColumn = () => {
    return {
      identity: "inventoryRuleDetails",
      header: "Inventory Rule Details",
      columns: [
        getFormattedTextColumn("ruleType", "Rule Type"),
        getFormattedColumn("ruleRank", (value, context) =>
          getInventoryRuleOutputValue(value, context)
        ),
        getFormattedColumn(
          "haircut",
          (value, context) =>
            getInventoryRuleOutputValue(
              _.isNumber(value)
                ? roundWithAtmostDecimal(value, 3) + "%"
                : undefined,
              context
            ),
          "Haircut (in %)"
        ),
        getFormattedColumn(
          "concentrationThreshold",
          (value, context) =>
            getInventoryRuleOutputValue(
              _.isNumber(value)
                ? roundWithAtmostDecimal(value, 3) + "%"
                : undefined,
              context
            ),
          "Concentration Threshold (in %)"
        ),
        getFormattedColumn("criteria", (value, context) =>
          getInventoryRuleOutputValue(value, context)
        ),
        getFormattedColumn("criteriaOrder", (value, context) =>
          getInventoryRuleOutputValue(value, context)
        ),
        getFormattedColumn("roundingValue", (value, context) =>
          getInventoryRuleOutputValue(value, context)
        ),
        getFormattedColumn("maturityDateThreshold", (value, context) =>
          getInventoryRuleOutputValue(value, context)
        ),
      ],
    };
  };

  const getSecurityIdentifiersColumn = () => {
    let securityIdentifiersColumn = {
      identity: "securityIdentifiers",
      header: "Security Identifiers",
      columns: [
        getFormattedTextColumn("isin", "ISIN"),
        getFormattedTextColumn("cusip", "CUSIP"),
        getFormattedTextColumn("sedol", "SEDOL"),
      ],
    };
    return securityIdentifiersColumn;
  };

  const getPledgedQuantityColumn = () => {
    let pledgedQuantityColumn = {
      identity: "pledgedQuantityGroup",
      header: "Pledged Quantity",
      columns: [
        getFormattedNumericColumn("pledgedQuantity"),
        getFormattedNumericColumn(
          "pledgedMarketValue",
          "Pledged Market Value (in USD)",
          160
        ),
        getFormattedColumn(
          "concentration",
          (value, context) => <Concentration value={value} context={context} />,
          "Concentration (in %)",
          150
        ),
      ],
    };
    return pledgedQuantityColumn;
  };

  const getSecurityDetails = () => {
    let securityDetailsColumn = {
      identity: "securityDetails",
      header: "Security Details",
      columns: [
        getFormattedTextColumn("gboType", "GBO Type", 160),
        getFormattedColumn(
          "spn",
          (value, context) => <SPN context={context} />,
          "SPN"
        ),
        getFormattedTextColumn("description", undefined, 160),
        getFormattedTextColumn("currency"),
        getFormattedColumn(
          "tMinusOneDirtyPrice",
          (value) => <>{value ? value.toFixed(3) : undefined}</>,
          "EOD Price (T-1)"
        ),
      ],
    };
    return securityDetailsColumn;
  };

  const getSimulateMovements = () => {
    let simulateMovementsColumn = {
      identity: "simulateMovements",
      header: "Simulate Movements",
      columns: [
        {
          identity: "selectedQuantity",
          Cell: (value, context) => (
            <Quantity context={context} onEditQuantity={props.onEditQuantity} />
          ),
          width: 160,
          disableFilters: true,
          disableSearches: true,
          disableSortBy: true,
        },
        {
          identity: "selectedDirection",
          header: "Direction",
          Cell: (value, context) => (
            <Direction
              context={context}
              onChangeDirection={props.onChangeDirection}
            />
          ),
          exportAccessor: (row, _) => {
            return row.selectedDirection ? JSON.parse(row.selectedDirection).value : "";
          },
          width: 160,
          disableFilters: true,
          disableSearches: true,
          disableSortBy: true,
        },
        {
          identity: "postHaircutMV",
          Cell: (value) => (
            <b>
              {isValidNumber(value)
                ? validateAndGetFormattedText(value)
                : undefined}
            </b>
          ),
          disableFilters: true,
          disableSearches: true,
          disableSortBy: true,
        },
        {
          identity: "comment",
          Cell: (value, context) => (
            <>{value && context.row.isSelected ? value : undefined}</>
          ),
          width: 160,
          disableFilters: true,
          disableSearches: true,
          disableSortBy: true,
        },
        {
          identity: "validationResponse",
          Cell: (value) => <>{value ? value : undefined}</>,
          width: 160,
          disableFilters: true,
        },
        getFormattedColumn(
          "simulatedConcentration",
          (value, context) => <Concentration value={value} context={context} />,
          "Simulated Concentration (in %)"
        ),
      ],
    };
    return simulateMovementsColumn;
  };

  const getEvaluatorCommentColumn = () => {
    return {
      identity: "evaluatorComments",
      header: "Evaluator Comments",
      columns: [
        getFormattedTextColumn("sourcePositionEvaluatorComment"),
        getFormattedTextColumn("destinationPositionEvaluatorComment"),
      ],
    };
  };

  return [
    getSecurityDetails(),
    getSimulateMovements(),
    getReferenceDataColumn(),
    getSecurityIdentifiersColumn(),
    getFormattedTextColumn("tickUnit"),
    getPledgedQuantityColumn(),
    getSettleDateQuantitiesGroupedColumn(),
    getFormattedNumericColumn("marketValue", "Market Value (in USD)"),
    getFormattedNumericColumn(
      "postHaircutMarketValue",
      "Post Haircut Market Value (in USD)",
      160
    ),
    getFormattedTextColumn("maturityDate"),
    getFormattedTextColumn("daysToMaturity"),
    getFormattedTextColumn("nextCouponDate"),
    getFormattedNumericColumn("sourceEligibleQuantity"),
    getFormattedNumericColumn("sourceForceTransferQuantity"),
    getFormattedNumericColumn("destinationEligibleQuantity"),
    getFormattedNumericColumn("destinationForceTransferQuantity"),
    getEvaluatorCommentColumn(),
    getFormattedTextColumn("autoSelectionDetail", "Auto Selection Detail"),
    getInventoryRuleColumn(),
  ];
};
