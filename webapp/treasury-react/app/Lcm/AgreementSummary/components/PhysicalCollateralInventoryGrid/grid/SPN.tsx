import React from "react";

const SPN: React.FC<any> = ({ context }) => {
  if (context.row.ragStatus === "RED") {
    return <span className="token critical">{context.row.spn}</span>;
  } else if (context.row.ragStatus === "AMBER") {
    return <span className="token warning">{context.row.spn}</span>;
  }
  return <>{context.row.spn}</>;
};

export default SPN;
