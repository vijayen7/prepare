import React, { Component } from "react";
import { Dialog } from "arc-react-components";

export default class QuantityDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedQuantity: props.selectedQuantity,
      isOpen: props.isOpen,
    };
  }

  onChange = (e) => {
    this.setState({
      ...this.state,
      selectedQuantity: e.target.value,
    });
  };

  onSave = () => {
    this.props.onSave(this.props.dataId, this.state.selectedQuantity);
  };

  render() {
    return (
      <Dialog
        isOpen={this.props.isOpen}
        onClose={this.props.onClose}
        title="Transfer Quantity"
        footer={<button onClick={this.onSave}>Save</button>}
      >
        <input
          type="text"
          value={this.state.selectedQuantity}
          onChange={this.onChange}
        />
      </Dialog>
    );
  }
}
