import MapData from "../../../../commons/models/MapData";

export const INCOMING: string = "Incoming";
export const OUTGOING: string = "Outgoing";

export const RULE_TYPE = {
  INCLUDE: "INCLUDE",
  EXCLUDE: "EXCLUDE",
};

export const DIRECTIONS: MapData[] = [
  { key: 1, value: INCOMING },
  { key: 2, value: OUTGOING },
];

export const INITIAL_PORTFOLIO_VALIDATOR_RESULT = {
  validationSuccess: undefined,
  transferQuantityValidationResults: new Map(),
  frozenQuantityValidationResults: new Map(),
  portfolioEvaluatorResult: new Map<string, Array<string>>()
}

export const INITIAL_DIALOG_STATE = { isOpen: false, status: { status: "", message: "" } }

export const MESSAGES = {
  VALIDATE_PORTFOLIO_FAILURE: "Error occurred while validating portfolio",
  VALIDATION_COMPLETE: "Validation Complete. Validation Responses Populated",
  INVALID_TRANSFER_DETAILS: "Invalid Transfer Quantity/Direction",
  NO_POSITIONS_SELECTED_FOR_TRANSFER: "Please select positions for transfer",
  BOOK_TRANSFER_FAILURE: "Unable to book transfers.",
  FETCH_INVENTORY_POSITIONS_FAILURE: "Error occurred while getting inventory positions",
  POSITION_TRANSFERS_INITIATED: "Position Transfers Initiated."
}
