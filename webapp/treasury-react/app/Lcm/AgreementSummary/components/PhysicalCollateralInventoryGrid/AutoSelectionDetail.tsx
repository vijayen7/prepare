import { Message } from "arc-react-components";
import React from "react";
import _ from "lodash";

interface Props {
  autoSelectionResult?: {
    isSuccess: boolean;
    comment?: string;
  };
}

const AutoSelectionDetail: React.FC<Props> = (props) => {
  return (
    <>
      {props.autoSelectionResult &&
        !_.isEmpty(props.autoSelectionResult!!.comment) && (
          <Message
            title={props.autoSelectionResult!!.comment}
            type={
              props.autoSelectionResult!!.isSuccess
                ? Message.Type.PRIMARY
                : Message.Type.WARNING
            }
            children={null}
          />
        )}
    </>
  );
};

export default AutoSelectionDetail;
