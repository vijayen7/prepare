import React, { Component } from 'react';
import { createIdFromParams } from './../../Utils'

export default class Table extends Component {
    constructor(props) {
        super(props);
        this.createRefFromParams = this.createRefFromParams.bind(this);
    }

    createRefFromParams(table, column, index, ref) {
        this[createIdFromParams(table, column, index)] = ref;
    }


    render() {
        let data = this.props.data;
        let that = this;


        if (data === undefined || data == null) {
            return null;
        }

        if (data.headerRow === undefined || data.numberOfColumns === undefined) {
            return null;
        }
        return (
            <table className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double">
                <tbody>
                    <tr>
                        <th key={data.tableName} colSpan={data.numberOfColumns}>{data.tableName}</th>
                    </tr>
                    <tr>
                        {
                            data.headerRow.map(function (value) {
                                return <th id={value}>{value}</th>
                            })
                        }
                    </tr>
                    {
                        data.tableRows.map(function (tableRow, rowIndex) {
                            return (
                                <tr key={rowIndex}>
                                    {
                                        tableRow.map(function (value, columnIndex) {
                                            return (
                                                <td key={columnIndex}
                                                    id={createIdFromParams(data.tableName, data.headerRow[columnIndex], rowIndex)}
                                                    ref={ref => that.createRefFromParams(data.tableName, data.headerRow[columnIndex], rowIndex, ref)}
                                                >
                                                    {value}
                                                </td>
                                            );
                                        })
                                    }
                                </tr>
                            );

                        })
                    }
                </tbody>
            </table>

        );
    }
}
