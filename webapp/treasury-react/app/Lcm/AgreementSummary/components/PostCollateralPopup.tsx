import React from 'react';
import { Dialog, Button } from 'arc-react-components';

export const PostCollateralPopup: React.FC<PostCollateralPopupProps> = (props) => {
  const [isOpen, setIsOpen] = React.useState(true)
  const closePostCollateralPopup = () => {
    setIsOpen(false)
    if (props.onClose) props.onClose()
  }

  const onClickPostCash = () => {
    closePostCollateralPopup();
    props.onClickPostCash();
  };

  const onClickPostSecurities = () => {
    closePostCollateralPopup();
    props.onClickPostSecurities();
  };

  return (
    <Dialog
      isOpen={isOpen}
      onClose={closePostCollateralPopup}
      footer={
        <React.Fragment>
          <Button onClick={onClickPostSecurities}>Securities Collateral</Button>
          <Button onClick={onClickPostCash}>Cash Collateral</Button>
          <Button onClick={closePostCollateralPopup}>Close</Button>
        </React.Fragment>
      }
    >
      Do you wish to post Securities or Cash Collateral ?
    </Dialog>
  );
}
interface PostCollateralPopupProps {
  isOpen: boolean,
  onClose: Function,
  onClickPostCash: Function,
  onClickPostSecurities: Function
}
