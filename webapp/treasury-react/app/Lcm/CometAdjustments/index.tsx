import React, { useEffect, useContext } from 'react';
import { observer } from 'mobx-react-lite';
import AdjustmentPopupStore from './AdjustmentPopupStore';
import AdjustmentFormComponent from './AdjustmentFormComponent'
import AdjustmentDataFilter from './models/AdjustmnetDataFilter'
import { ReactLoader } from '../../commons/components/ReactLoader';
const AdjustmentPopup: React.FC<any> = (props) => {
  const adjustmentPopupStore = useContext(AdjustmentPopupStore);

  useEffect(() => {
    let payload = prepareAdjustmentDataParam();
    updateStoreFromProps();
    adjustmentPopupStore.fetchAdjustments(payload);
  }, []);

  const prepareAdjustmentDataParam = () => {

    let adjustmentPopupDataInput: AdjustmentDataFilter = {
      sourceGridConfig: props.sourceGridConfig,
      agreementId: props.selectedAgreementId,
      tradingEntityId: props.legalEntityId,
      cpeId: props.counterPartyEntityId,
      agreementTypeId: props.agreementTypeId,
      entityFamilyId: props.entityFamilyId,
      actionableType: props.actionableType,
      businessUnitId: props.businessUnitId,
      bookId: props.bookId,
      spn: props.pnlSpn,
      '@CLASS': 'com.arcesium.treasury.model.lcm.adjustment.Adjustment'
    }
    return adjustmentPopupDataInput;
  }

  const updateStoreFromProps = () => {
    adjustmentPopupStore.updatePopupDataFromProps(props.workflowInstanceId, props.startDate,
      props.caName, props.caId, props.calculator, props.pnlSpn, props.sourceGridConfig, props.tag)
  }

  const onSelect = (data: any, filterName: string) => {
    adjustmentPopupStore.onChange(data, filterName);
  }

  const onReset = () => {
    adjustmentPopupStore.closeDialog();
    props.onClose();
    adjustmentPopupStore.isDataReady = false
  }

  return (
    adjustmentPopupStore.adjustmentPopupData != null ?
      <>
        <ReactLoader inProgress={adjustmentPopupStore.inProgress} />
        {adjustmentPopupStore.isDataReady &&
          <AdjustmentFormComponent
            data={adjustmentPopupStore.adjustmentPopupData}
            onChange={onSelect}
            onSave={adjustmentPopupStore.saveAdjustments}
            onReset={onReset}
            isOpen={props.isOpen}
            addAdjustmentStatus={[adjustmentPopupStore.status]}
            resetAddAdjustmentStatus={adjustmentPopupStore.closeAdjustmentStatusDialog}
          />
        }
      </> : null

  );

};


export default observer(AdjustmentPopup);
