import React from 'react';
import { createContext } from 'react';
import { observable, action } from 'mobx';
import { getAdjustments, addAdjustment } from "./api"
import AdjustmentPopupData from './models/AdjustmentPopupData';
import RefData from '../../commons/models/RefData';
import { prepareAddAdjustmentData } from './Utils'
import {
  IN_PROGRESS, SELECTED_ADJUSTMENT_TYPE, SELECTED_AGREEMENT_TYPE,
  SELECTED_LEGAL_ENTITY, SELECTED_COUNTER_PARTY_ENTITY, SELECTED_BOOK,
  SELECTED_BUSINESS_UNIT, SELECTED_MARGIN_CALL_TYPE,
  SELECTED_REASON_CODE, SUCCESS, FAILURE
} from '../../commons/constants';
import AdjustmentDataFilter from './models/AdjustmnetDataFilter'
import RawAdjustmentData from './models/RawAdjustmentData'
import Status from './models/Status'


const INITIAL_REF_DATA: RefData = {
  key: '',
  value: ''
}

const INITIAL_ADJUSTMENT_DATA: AdjustmentPopupData = {
  sourceGridConfig: '',
  workflowInstanceId: NaN,
  selectedAdjustmentType: INITIAL_REF_DATA,
  adjustmentTypes: [],
  selectedLegalEntity: INITIAL_REF_DATA,
  legalEntities: [],
  selectedCounterParty: INITIAL_REF_DATA,
  counterParties: [],
  selectedAgreementType: INITIAL_REF_DATA,
  agreementTypes: [],
  selectedBook: INITIAL_REF_DATA,
  books: [],
  selectedBusinessUnit: INITIAL_REF_DATA,
  businessUnits: [],
  selectedMarginCallType: INITIAL_REF_DATA,
  marginCallTypes: [],
  selectedCalculator: '',
  selectedCustodianAccount: INITIAL_REF_DATA,
  selectedPnlSpn: '',
  selectedEndDate: '',
  selectedStartDate: '',
  selectedReasonCode: INITIAL_REF_DATA,
  reasonCodes: [],
  startDate: '',
  endDate: '',
  adjustmentAmount: '',
  comment: '',
  tag: ''
}

const INITIAL_STATUS: Status = {
  status: '',
  message: ''
}

export class AdjustmentPopupStore {

  @observable
  inProgress = false

  @observable
  isDataReady = false

  @observable
  adjustmentPopupData: AdjustmentPopupData = INITIAL_ADJUSTMENT_DATA;

  @observable
  status: Status = INITIAL_STATUS

  @action.bound
  onChange(data: RefData, filterName: string) {

    switch (filterName) {
      case SELECTED_ADJUSTMENT_TYPE:
        this.adjustmentPopupData.selectedAdjustmentType = data;
        break;
      case SELECTED_AGREEMENT_TYPE:
        this.adjustmentPopupData.selectedAgreementType = data;
        break;
      case SELECTED_LEGAL_ENTITY:
        this.adjustmentPopupData.selectedLegalEntity = data;
        break;
      case SELECTED_COUNTER_PARTY_ENTITY:
        this.adjustmentPopupData.selectedCounterParty = data;
        break;
      case SELECTED_BOOK:
        this.adjustmentPopupData.selectedBook = data;
        break;
      case SELECTED_BUSINESS_UNIT:
        this.adjustmentPopupData.selectedBusinessUnit = data;
        break;
      case SELECTED_MARGIN_CALL_TYPE:
        this.adjustmentPopupData.selectedMarginCallType = data;
        break;
      case SELECTED_REASON_CODE:
        this.adjustmentPopupData.selectedReasonCode = data;
        break;
      default:
        switch (data.key) {
          case 'amount':
            this.adjustmentPopupData.adjustmentAmount = data.value;
            break;
          case 'selectedStartDate':
            this.adjustmentPopupData.selectedStartDate = data.value;
            break;
          case 'selectedEndDate':
            this.adjustmentPopupData.selectedEndDate = data.value;
            break;
          case 'comment':
            this.adjustmentPopupData.comment = data.value;
            break;
          case 'calculator':
            this.adjustmentPopupData.selectedCalculator = data.value;
            break;
          case 'pnlSpn':
            this.adjustmentPopupData.selectedPnlSpn = data.value;
            break;
          case 'custodianAccount':
            this.adjustmentPopupData.selectedCustodianAccount.value = data.value;

        }

    }
  }

  @action.bound
  closeDialog() {
    this.adjustmentPopupData = INITIAL_ADJUSTMENT_DATA;
    this.status = INITIAL_STATUS
  }

  @action
  closeAdjustmentStatusDialog = () => {
    this.status = INITIAL_STATUS
  }

  @action.bound
  updateAdjustmentPopupData(data: AdjustmentPopupData) {
    this.adjustmentPopupData = data;
  }

  @action.bound
  updatePopupDataFromProps = (workflowInstanceId: number, startDate: string,
    caName: string, caId: string, calculator: string, pnlSpn: string, sourceGridConfig: string, tag: string) => {

    this.adjustmentPopupData.workflowInstanceId = workflowInstanceId;
    this.adjustmentPopupData.startDate = startDate;
    this.adjustmentPopupData.endDate = startDate;
    this.adjustmentPopupData.selectedCustodianAccount.key = caId;
    this.adjustmentPopupData.selectedCustodianAccount.value = caName;
    this.adjustmentPopupData.selectedCalculator = calculator;
    this.adjustmentPopupData.selectedPnlSpn = pnlSpn;
    this.adjustmentPopupData.sourceGridConfig = sourceGridConfig;
    this.adjustmentPopupData.selectedStartDate = startDate
    this.adjustmentPopupData.selectedEndDate = startDate
    this.adjustmentPopupData.tag = tag
  }

  @action.bound
  updateAdjustmentStatus(status: string, message: string) {
    let updatedStatus: Status = {
      status: status,
      message: message
    };
    this.status = updatedStatus;
  }



  fetchAdjustments = async (payload: AdjustmentDataFilter) => {
    try {
      this.inProgress = true
      const response: RawAdjustmentData = await getAdjustments(payload);
      this.processAdjustmentData(response);
      this.removeBUForCollAdjustments()
    } catch (err) {
      console.log(err)
      this.updateAdjustmentStatus(FAILURE, 'Error while fetching Adjustment Popup Data');
    } finally {
      this.inProgress = false
      this.isDataReady = true
    }
  }

  removeBUForCollAdjustments = () => {
    if (
      this.adjustmentPopupData.sourceGridConfig == "COLLATERAL_GRID" ||
      this.adjustmentPopupData.sourceGridConfig == "SECURITIES_COLLATERAL_GRID"
    ) {
      this.adjustmentPopupData.businessUnits = [{ key: "-1", value: "ALL" }];
    }
  };

  saveAdjustments = async () => {
    try {
      this.inProgress = true
      if (!this.validateAdjustmentData(this.adjustmentPopupData)) {
        return
      }
      let adjustmentData: AdjustmentDataFilter = prepareAddAdjustmentData(this.adjustmentPopupData);

      const response = await addAdjustment(adjustmentData);

      if (response.length > 0 && response[0].status == SUCCESS) {
        this.updateAdjustmentStatus(SUCCESS, 'Adjustment added successfully');
        window.location.reload()
      } else {
        let message = 'Unable To Add Adjustment.'
        if (response.length && response[0].message) {
          message += response[0].message
        }
        this.updateAdjustmentStatus(FAILURE, message)
      }
    } catch (err) {
      console.log(err)
      this.updateAdjustmentStatus(FAILURE, 'Error occured while adding adjustment')
    } finally {
      this.inProgress = false
    }
  }

  validateAdjustmentData = (adjustmentData: AdjustmentPopupData) => {
    if (adjustmentData.selectedAdjustmentType.key == '1' && adjustmentData.selectedBusinessUnit.key == '-1') {
      if (!confirm("Adding Usage Adjustment with Business Unit as ALL. Do you wish to proceed?")) {
        return false;
      }
    }
    let amount: any = adjustmentData.adjustmentAmount.replace(/,/g, "");
    amount = amount.match(/-?[0-9.]+/g);
    if (amount === null) {
      this.updateAdjustmentStatus(IN_PROGRESS, 'Please enter a valid amount')
      return false
    } else {
      adjustmentData.adjustmentAmount = amount.join("");
    }
    return true;
  }

  processAdjustmentData = (data: RawAdjustmentData) => {

    if (data == null) {
      return;
    }
    let rawData = data;
    let refData: RefData | null = null;

    rawData.adjustmentTypes.forEach((element: { id: string; name: string; }) => {
      refData = { key: element.id, value: element.name }
      this.adjustmentPopupData.adjustmentTypes.push(refData);
    });
    this.adjustmentPopupData.adjustmentTypes.length != 0 ?
      this.adjustmentPopupData.selectedAdjustmentType = this.adjustmentPopupData.adjustmentTypes[0] :
      null;

    rawData.agreementTypes.forEach((element: { id: string; abbrev: string; }) => {
      refData = { key: element.id, value: element.abbrev }
      this.adjustmentPopupData.agreementTypes.push(refData);
    });
    this.adjustmentPopupData.agreementTypes.length != 0 ?
      this.adjustmentPopupData.selectedAgreementType = this.adjustmentPopupData.agreementTypes[0] :
      null;

    rawData.marginCallTypes.forEach((element: { id: string; abbrev: string; }) => {
      refData = { key: element.id, value: element.abbrev }
      this.adjustmentPopupData.marginCallTypes.push(refData);
    });

    this.adjustmentPopupData.tag != undefined ?
      this.adjustmentPopupData.marginCallTypes = this.adjustmentPopupData.marginCallTypes.filter(
                                                 marginCallType => marginCallType.value == this.adjustmentPopupData.tag) :
                                                 null;


    this.adjustmentPopupData.marginCallTypes.length != 0 ?
      this.adjustmentPopupData.selectedMarginCallType = this.adjustmentPopupData.marginCallTypes[0] :
      null;

    rawData.businessUnits.forEach((element: { id: string; name: string; }) => {
      refData = { key: element.id, value: element.name }
      this.adjustmentPopupData.businessUnits.push(refData);
    });

    this.adjustmentPopupData.businessUnits.length != 0 ?
      this.adjustmentPopupData.selectedBusinessUnit = this.adjustmentPopupData.businessUnits[0] :
      null;

    rawData.books.forEach((element: { id: string; name: string; }) => {
      refData = { key: element.id, value: element.name }
      this.adjustmentPopupData.books.push(refData);
    });

    this.adjustmentPopupData.books.length != 0 ?
      this.adjustmentPopupData.selectedBook = this.adjustmentPopupData.books[0] :
      null;

    rawData.reasons.forEach((element: { id: string; name: string; }) => {
      refData = { key: element.id, value: element.name }
      this.adjustmentPopupData.reasonCodes.push(refData);
    });

    this.adjustmentPopupData.reasonCodes.length != 0 ?
      this.adjustmentPopupData.selectedReasonCode = this.adjustmentPopupData.reasonCodes[0] :
      null;


    rawData.counterParties.forEach((element: { id: string; name: string; }) => {
      refData = { key: element.id, value: element.name }
      this.adjustmentPopupData.counterParties.push(refData);
    });
    this.adjustmentPopupData.counterParties.length != 0 ?
      this.adjustmentPopupData.selectedCounterParty = this.adjustmentPopupData.counterParties[0] :
      null;


    rawData.legalEntities.forEach((element: { id: string; name: string; }) => {
      refData = { key: element.id, value: element.name }
      this.adjustmentPopupData.legalEntities.push(refData);
    });
    this.adjustmentPopupData.legalEntities.length != 0 ?
      this.adjustmentPopupData.selectedLegalEntity = this.adjustmentPopupData.legalEntities[0] :
      null;

    this.updateAdjustmentPopupData(this.adjustmentPopupData);
  }

}

export default createContext(new AdjustmentPopupStore());
