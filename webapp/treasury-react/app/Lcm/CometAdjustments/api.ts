import { BASE_URL } from '../../commons/constants';
import { fetchPostURL } from '../../commons/util';
import AdjustmentDataFilter from './models/AdjustmnetDataFilter';

export function getAdjustments(payload: AdjustmentDataFilter) {
    let paramString = JSON.stringify(payload);
    paramString = encodeURIComponent(paramString);
    let encodedParam = 'adjustment=' + paramString + '&inputFormat=JSON_WITH_REF&format=JSON'
    const adjustmentsUrlExt = 'service/lcmAdjustmentService/getAdjustmentData'
    const url = `${BASE_URL}${adjustmentsUrlExt}`;
    return fetchPostURL(url, encodedParam);
}

export function addAdjustment(payload: AdjustmentDataFilter) {
    let paramString = JSON.stringify(payload);
    paramString = encodeURIComponent(paramString);
    let encodedParam = 'adjustment=' + paramString + '&inputFormat=JSON_WITH_REF&format=JSON'
    const saveAdjustmentsExt = 'service/lcmAdjustmentService/saveAdjustments';
    const url = `${BASE_URL}${saveAdjustmentsExt}`;
    return fetchPostURL(url, encodedParam);
}
