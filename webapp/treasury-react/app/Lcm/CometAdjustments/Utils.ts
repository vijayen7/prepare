import AdjustmentPopupData from './models/AdjustmentPopupData';
import AdjustmentDataFilter from './models/AdjustmnetDataFilter';

export function prepareAddAdjustmentData(data: AdjustmentPopupData) {

    const adjustmentData: AdjustmentDataFilter = {
        adjustmentTypeId: Number(data.selectedAdjustmentType.key),
        tradingEntityId: Number(data.selectedLegalEntity.key),
        cpeId: Number(data.selectedCounterParty.key),
        agreementTypeId: Number(data.selectedAgreementType.key),
        bookId: Number(data.selectedBook.key),
        businessUnitId: Number(data.selectedBusinessUnit.key),
        caId: Number(data.selectedCustodianAccount.key),
        startDate: data.selectedStartDate,
        endDate: data.selectedEndDate,
        reasonCodeId: Number(data.selectedReasonCode.key),
        adjustmentAmountInRptCcy: Number(data.adjustmentAmount),
        calculator: data.selectedCalculator,
        spn: Number(data.selectedPnlSpn),
        comment: data.comment,
        sourceGridConfig: data.sourceGridConfig,
        workflowInstanceId: data.workflowInstanceId,
        marginCallTypeId: Number(data.selectedMarginCallType.key),
        "@CLASS": 'com.arcesium.treasury.model.lcm.adjustment.Adjustment'
    };
    return adjustmentData;

}
