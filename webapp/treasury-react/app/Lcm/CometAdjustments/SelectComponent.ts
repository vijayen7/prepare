import React from 'react';
import { observer } from 'mobx-react-lite';

const SelectComponent: React.FC<any> = (props: any) => {
    return (
        (props.data == null || props.data.length == 0) ?
            props.component2 : props.component1
    );
}

export default observer(SelectComponent);