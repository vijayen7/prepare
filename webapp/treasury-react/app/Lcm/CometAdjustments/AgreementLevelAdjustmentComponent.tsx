import React from 'react';
import { observer } from 'mobx-react-lite';
import InputFilter from "../../commons/components/InputFilter";
import SelectWithLabel from "./SelectWithLabel";
import SelectComponent from './SelectComponent'
import { SELECTED_BUSINESS_UNIT, } from "../../commons/constants"
import CustodianAccountFilter from "../../commons/container/CustodianAccountFilter";
import AdjustmentPopupData from './models/AdjustmentPopupData';

interface AgreementLevelAdjustmentComponentProps {
    data: AdjustmentPopupData,
    onChange: any
}

const AgreementLevelAdjustmentComponent: React.FC<any> = (props: AgreementLevelAdjustmentComponentProps) => {

    return (

        props.data.sourceGridConfig != "Agreement" &&
        (
            <div>
                <InputFilter
                    data={props.data.selectedCalculator}
                    stateKey="calculator"
                    label="Calculator"
                    disabled={true}
                />
                <SelectComponent
                    data={props.data.selectedCustodianAccount}
                    component1={
                        <SelectWithLabel
                            label="Custodian Account"
                            options={[props.data.selectedCustodianAccount]}
                            onChange={props.onChange}
                            filterName={SELECTED_BUSINESS_UNIT}
                        />
                    }
                    component2={
                        <CustodianAccountFilter
                            onSelect={props.onChange}
                            selectedData={props.data.selectedCustodianAccount}
                            multiSelect={false}
                            horizontalLayout
                        />
                    }
                />
                <InputFilter
                    data={props.data.selectedPnlSpn}
                    stateKey="pnlSpn"
                    label="PNL SPN"
                    disabled={true}
                />
            </div>
        )
        || null

    );
}

export default observer(AgreementLevelAdjustmentComponent);
