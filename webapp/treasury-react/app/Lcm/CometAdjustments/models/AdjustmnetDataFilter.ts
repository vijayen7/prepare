
export default interface AdjustmentDataFilter {
    agreementId?: number;
    adjustmentTypeId?: number;
    tradingEntityId: number;
    cpeId: number;
    agreementTypeId: number;
    entityFamilyId?: number;
    actionableType?: string;
    spn?: number;
    caId?: number;
    bookId?: number;
    businessUnitId?: number;
    startDate?: string;
    endDate?: string;
    calculator?: string;
    reasonCodeId?: number;
    adjustmentAmountInRptCcy?: number;
    comment?: string;
    marginCallTypeId?: number;
    sourceGridConfig?: string;
    workflowInstanceId?: number;
    '@CLASS': 'com.arcesium.treasury.model.lcm.adjustment.Adjustment'
}