import RefData from '../../../commons/models/RefData';

export default interface AdjustmentPopupData {
  sourceGridConfig: string | '';
  workflowInstanceId: number;
  selectedAdjustmentType: RefData;
  adjustmentTypes: RefData[];
  selectedLegalEntity: RefData;
  legalEntities: RefData[];
  selectedCounterParty: RefData;
  counterParties: RefData[];
  selectedAgreementType: RefData;
  agreementTypes: RefData[];
  selectedBook: RefData;
  books: RefData[];
  selectedBusinessUnit: RefData;
  businessUnits: RefData[];
  selectedMarginCallType: RefData;
  marginCallTypes: RefData[];
  selectedCalculator: string | ''
  selectedCustodianAccount: RefData;
  selectedPnlSpn: string | '';
  selectedStartDate: string | '';
  startDate: string | '';
  selectedEndDate: string | '';
  endDate: string | '';
  selectedReasonCode: RefData;
  reasonCodes: RefData[];
  adjustmentAmount: string | '';
  comment: string | '';
  tag: string | '';
}

export interface AdjustmentDialogProps {
  startDate: string,
  selectedAgreementId: number,
  legalEntityId: number,
  counterPartyEntityId: number,
  agreementTypeId: number,
  calculator?: string,
  caName?: string,
  caId?: string,
  pnlSpn?: number,
  entityFamilyId?: number,
  actionableType?: string,
  businessUnitId?: number,
  bookId?: number,
  reportingCurrency?: string,
  workflowInstanceId: number,
  sourceGridConfig: string
}
