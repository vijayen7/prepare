export default interface RawAdjustmentData {
    adjustmentTypes: []
    agreementTypes: []
    marginCallTypes: [],
    businessUnits: [],
    books: [],
    reasons: [],
    counterParties: [],
    legalEntities: [],

}