import React, { Fragment } from 'react';
import { observer } from 'mobx-react-lite';
import { Dialog } from "arc-react-components";
import LayoutWithSpace from "../../commons/components/LayoutWithSpace";
import ButtonWithPause from "../../commons/components/ButtonWithPause";
import SelectWithLabel from "./SelectWithLabel";
import InputFilter from "../../commons/components/InputFilter";
import { getCommaSeparatedNumber } from "../../commons/util";
import {
    SELECTED_BUSINESS_UNIT, SELECTED_BOOK, SELECTED_AGREEMENT_TYPE,
    SELECTED_ADJUSTMENT_TYPE, SELECTED_REASON_CODE, SELECTED_LEGAL_ENTITY,
    SELECTED_COUNTER_PARTY_ENTITY, SELECTED_MARGIN_CALL_TYPE
} from "../../commons/constants"
import SelectComponent from './SelectComponent'
import AgreementTypeFilter from "../../commons/container/AgreementTypeFilter";
import BookFilter from "../../commons/container/BookFilter";
import BusinessUnitFilter from "../../commons/container/BusinessUnitFilter";
import CpeFilter from "../../commons/container/CpeFilter";
import LegalEntityFilter from "../../commons/container/LegalEntityFilter";
import ReasonCodeFilter from "../../commons/container/ReasonCodeFilter";
import StatusComponent from "./StatusComponent";
import AdjustmentPopupData from './models/AdjustmentPopupData';
import DateFilter from "../../commons/filters/components/DateFilter";
import Status from './models/Status'
import AgreementLevelAdjustmentComponent from './AgreementLevelAdjustmentComponent';
import SingleSelectFilter from "../../commons/filters/components/SingleSelectFilter";
import {ADJUSTMENT_TYPES_FILTER_DATA} from "../../commons/constants";

interface AdjustmentFormComponentProps {
    data: AdjustmentPopupData,
    onChange: (...args: any[]) => any,
    onSave: any,
    onReset: any,
    isOpen: boolean,
    addAdjustmentStatus: Status[],
    limitedAdjustmentTypes?: [],
    resetAddAdjustmentStatus: Function
}

const AdjustmentFormComponent: React.FC<any> = (props: AdjustmentFormComponentProps) => {

    return (
        <React.Fragment>
            <Dialog
                isOpen={props.isOpen}
                title="Add Adjustment"
                onClose={props.onReset}
                footer={
                    <React.Fragment>
                        <ButtonWithPause onClick={props.onSave} data={<Fragment>Save</Fragment>}/>
                        <button onClick={props.onReset}>Cancel</button>
                    </React.Fragment>
                }
            >
                <LayoutWithSpace>
                    <SelectComponent
                        data={props.data.adjustmentTypes}
                        component1={
                            <SelectWithLabel
                                label="Adjustment Type"
                                options={props.data.adjustmentTypes}
                                onChange={props.onChange}
                                value={props.data.selectedAdjustmentType}
                                filterName={SELECTED_ADJUSTMENT_TYPE}
                            />}
                        component2={
                            <SingleSelectFilter
                              label="Adjustment Type"
                              onSelect={props.onChange}
                              stateKey="selectedAdjustmentType"
                              data={props.limitedAdjustmentTypes?.length ? props.limitedAdjustmentTypes : ADJUSTMENT_TYPES_FILTER_DATA}
                              selectedData={props.data.selectedAdjustmentType}
                              multiSelect={false}
                              horizontalLayout
                          />
                        }
                    />
                    <SelectComponent
                        data={props.data.legalEntities}
                        component1={
                            <SelectWithLabel
                                label="Legal Entity"
                                options={props.data.legalEntities}
                                onChange={props.onChange}
                                filterName={SELECTED_LEGAL_ENTITY}
                            />}
                        component2={
                            <LegalEntityFilter
                                onSelect={props.onChange}
                                selectedData={props.data.selectedLegalEntity}
                                multiSelect={false}
                                horizontalLayout
                            />
                        }
                    />
                    <SelectComponent
                        data={props.data.counterParties}
                        component1={
                            <SelectWithLabel
                                label="Exposure Counterparty"
                                options={props.data.counterParties}
                                onChange={props.onChange}
                                filterName={SELECTED_COUNTER_PARTY_ENTITY}
                            />}
                        component2={
                            <CpeFilter
                                onSelect={props.onChange}
                                selectedData={props.data.selectedCounterParty}
                                multiSelect={false}
                                horizontalLayout
                                label="Exposure Counterparty"
                            />
                        }
                    />
                    <SelectComponent
                        data={props.data.agreementTypes}
                        component1={
                            <SelectWithLabel
                                label="Agreement Type"
                                options={props.data.agreementTypes}
                                onChange={props.onChange}
                                filterName={SELECTED_AGREEMENT_TYPE}

                            />}
                        component2={
                            <AgreementTypeFilter
                                onSelect={props.onChange}
                                selectedData={props.data.selectedAgreementType}
                                multiSelect={false}
                                horizontalLayout
                            />
                        }
                    />
                    <SelectComponent
                        data={props.data.books}
                        component1={
                            <SingleSelectFilter
                              label = "Book"
                              onSelect={(data)=>props.onChange(data.value, SELECTED_BOOK)}
                              stateKey={SELECTED_BOOK}
                              selectedData={props.data.selectedBook}
                              horizontalLayout
                              data = {props.data.books}
                            />
                        }
                        component2={
                            <BookFilter
                                onSelect={props.onChange}
                                selectedData={props.data.selectedBook}
                                multiSelect={false}
                                horizontalLayout
                            />
                        }
                    />
                    <SelectComponent
                        data={props.data.businessUnits}
                        component1={
                            <SingleSelectFilter
                              label = "Business Unit"
                              onSelect={(data)=>props.onChange(data.value, SELECTED_BUSINESS_UNIT)}
                              stateKey={SELECTED_BUSINESS_UNIT}
                              selectedData={props.data.selectedBusinessUnit}
                              horizontalLayout
                              data = {props.data.businessUnits}
                            />
                        }
                        component2={
                            <BusinessUnitFilter
                                onSelect={props.onChange}
                                selectedData={props.data.selectedBusinessUnit}
                                multiSelect={false}
                                horizontalLayout
                            />
                        }
                    />
                    {
                        props.data.marginCallTypes &&
                        (
                            <SelectWithLabel
                                label="Margin Call Type"
                                options={props.data.marginCallTypes}
                                onChange={props.onChange}
                                filterName={SELECTED_MARGIN_CALL_TYPE}
                            />
                        )
                    }
                    <AgreementLevelAdjustmentComponent
                        data={props.data}
                        onChange={props.onChange}
                    />
                    <DateFilter
                        onChange={props.onChange}
                        stateKey="selectedStartDate"
                        data={props.data.selectedStartDate}
                        label="Start Date"
                    />
                    <DateFilter
                        onChange={props.onChange}
                        stateKey="selectedEndDate"
                        data={props.data.selectedEndDate}
                        label="End Date"
                    />
                    <SelectComponent
                        data={props.data.books}
                        component1={
                            <SelectWithLabel
                                label="Reason Code"
                                options={props.data.reasonCodes}
                                onChange={props.onChange}
                                filterName={SELECTED_REASON_CODE}
                            />
                        }
                        component2={
                            <ReasonCodeFilter
                                onSelect={props.onChange}
                                selectedData={props.data.selectedReasonCode}
                                multiSelect={false}
                                horizontalLayout
                            />
                        }
                    />
                    <InputFilter
                        data={getCommaSeparatedNumber(props.data.adjustmentAmount)}
                        onSelect={props.onChange}
                        stateKey="amount"
                        label="Adjustment Amount (USD)"
                    />
                    <InputFilter
                        data={props.data.comment}
                        onSelect={props.onChange}
                        stateKey="comment"
                        label="Comment"
                    />

                </LayoutWithSpace>
            </Dialog>
            <StatusComponent
                addAdjustmentStatus={props.addAdjustmentStatus}
                onReset={props.resetAddAdjustmentStatus}
            />
        </React.Fragment>

    );
};

export default observer(AdjustmentFormComponent);
