import React from 'react';
import AdjustmentFormComponent from '../AdjustmentFormComponent';
import { render } from '@testing-library/react';


describe('Adjustment Form Component', () => {
    let data, onChange, onSave, onReset;
    beforeEach(() => {
        data = null;
        onChange = jest.fn();
        onSave = jest.fn();
        onReset = jest.fn();
    });
});

test('Form Component would render even if no data is passed', () => {
    const { container } = render(
        <AdjustmentFormComponent
            data={data}
            onChange={onChange}
            onSave={onSave}
            onReset={onReset}
            addAdjustmentStatus={[]}
            isOpen={true}
        />
    );
    expect(container.querySelectorAll('div').length).not.toBeNull();
});
