import { Select } from 'arc-react-components';
import PropTypes from 'prop-types';
import React from 'react';
const SelectWithLabel = (props) => {

  const onChange = (data) => {
    props.onChange(data, props.filterName);
  }


  return (
    <div className="form-field--split" disabled={props.disabled}>
      <label disabled={props.disabled}>{props.label}</label>
      <Select
        key={props.label}
        options={props.options}
        value={props.value}
        disabled={props.disabled}
        readonly={props.readonly}
        optionToKey={(val) => (val && val.key) || null}
        optionToDisplay={(val) => (val && val.value) || null}
        onChange={onChange}
      />
    </div>
  );
};

SelectWithLabel.propTypes = {
  label: PropTypes.string,
  filterName: PropTypes.string,
  options: PropTypes.array,
  value: PropTypes.any,
  disabled: PropTypes.bool,
  readonly: PropTypes.bool,
  onChange: PropTypes.func
};
export default SelectWithLabel;
