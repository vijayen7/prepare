import React from 'react';
import { observer } from 'mobx-react-lite';
import { Dialog } from "arc-react-components";

interface StatusProps {
    addAdjustmentStatus: {
        status: string,
        message: string
    }[];
    onReset: any
}

const StatusComponent: React.FC<any> = (props: StatusProps) => {

    return (

        props.addAdjustmentStatus &&
        props.addAdjustmentStatus.length > 0 &&
        props.addAdjustmentStatus[0].status != "" &&
        props.addAdjustmentStatus[0].message !== "" && (
            <Dialog
                title="Add Adjustment Status"
                isOpen={true}
                onClose={props.onReset}
            >
                <div>{props.addAdjustmentStatus[0].message}</div>
            </Dialog>
        ) || null

    );

}

export default observer(StatusComponent);