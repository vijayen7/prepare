export const ADJUSTMENT_TYPES = [
  {key: 1, value:"Usage/Requirement"},
  {key: 2, value:"Rebalancing"},
  {key: 3, value:"Collateral"},
  {key: 4, value:"Exposure"},
  {key: 5, value:"Cash Outflow"}
]
export const PUBLISHED_WORKFLOW_STATUS_ID = 3
