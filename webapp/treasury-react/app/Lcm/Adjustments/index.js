import React, { Component } from 'react';
import ExceptionDialog from '../../commons/container/ExceptionDialog';
import Loader from '../../commons/container/Loader';
import ViewAdjustments from './containers/ViewAdjustments';

export default class Adjustments extends Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (
      <React.Fragment>
        <Loader />
        <ExceptionDialog />
        <div className="layout--flex--row">
          <arc-header

            ref={(header) => {
              this.header = header;
            }}
            className="size--content padding--bottom"
            user={USER}
            modern-themes-enabled
          >
            <div slot="application-menu" className="application-menu-toggle-view">
            </div>
          </arc-header>
          <ViewAdjustments location={this.props.location}></ViewAdjustments>

        </div>
      </React.Fragment>
    );
  }
}

