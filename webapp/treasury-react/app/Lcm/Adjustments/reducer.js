import _ from 'lodash';
import { combineReducers } from 'redux';
import {
  ADD_ADJUSTMENT,
  FETCH_COMET_ADJUSTMENTS,
  SAVE_ADJUSTMENT,
  SHOW_ADD_ADJUSTMENT_ALERT,
  SHOW_ADD_ADJUSTMENT_DIALOG,
  SHOW_COPY_ADJUSTMENT_ALERT,
  SHOW_COPY_ADJUSTMENT_DIALOG,
  COPY_SUCCESS_DIALOG,
  ADD_ADJUSTMENT_SUCCESS_DIALOG,
  RESIZE_CANVAS
} from '../../commons/constants';
import { convertJavaNYCDateTime } from '../../commons/util';

export function adjustmentsDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_COMET_ADJUSTMENTS}_SUCCESS`: {
      return extractAdjustmentData(action) || [];
    }
    default:
      return state;
  }
}
function saveAdjustmentsDataReducer(state = [], action) {
  switch (action.type) {
    case `${SAVE_ADJUSTMENT}_SUCCESS`:
      return action.data;
    case `${SAVE_ADJUSTMENT}_CLEAR_MESSAGE`:
      return [];
    default:
      return state;
  }
}
function showCopyAdjustmentDialogReducer(state = false, action) {
  switch (action.type) {
    case `${SHOW_COPY_ADJUSTMENT_DIALOG}_OPEN`:
      return true;
    case `${SHOW_COPY_ADJUSTMENT_DIALOG}_CLOSE`:
      return false;
    default:
      return state;
  }
}
function showCopySuccessDialogReducer(state = { show: false, message: '' }, action) {
  switch (action.type) {
    case `${COPY_SUCCESS_DIALOG}_SHOW`:
      return { show: true, message: action.payload };
    case `${COPY_SUCCESS_DIALOG}_HIDE`:
      return { show: false, message: '' };
    default:
      return state;
  }
}

function showAddAdjustmentSuccessDialogReducer(state = { show: false, message: '' }, action) {
  switch (action.type) {
    case `${ADD_ADJUSTMENT_SUCCESS_DIALOG}_SHOW`:
      return { show: true, message: action.payload };
    case `${ADD_ADJUSTMENT_SUCCESS_DIALOG}_HIDE`:
      return { show: false, message: '' };
    default:
      return state;
  }
}

function showCopyAdjustmentAlertReducer(state = false, action) {
  switch (action.type) {
    case `${SHOW_COPY_ADJUSTMENT_ALERT}_OPEN`:
      return true;
    case `${SHOW_COPY_ADJUSTMENT_ALERT}_CLOSE`:
      return false;
    default:
      return state;
  }
}

function addedAdjustmentsDataReducer(state = [], action) {
  switch (action.type) {
    case `${ADD_ADJUSTMENT}_SUCCESS`:
      return action.data;
    case `${ADD_ADJUSTMENT}_CLEAR_MESSAGE`:
      return [];
    default:
      return state;
  }
}
function showAddAdjustmentDialogReducer(state = false, action) {
  switch (action.type) {
    case `${SHOW_ADD_ADJUSTMENT_DIALOG}_OPEN`:
      return true;
    case `${SHOW_ADD_ADJUSTMENT_DIALOG}_CLOSE`:
      return false;
    default:
      return state;
  }
}
function showAddAdjustmentAlertReducer(state = false, action) {
  switch (action.type) {
    case `${SHOW_ADD_ADJUSTMENT_ALERT}_OPEN`:
      return true;
    case `${SHOW_ADD_ADJUSTMENT_ALERT}_CLOSE`:
      return false;
    default:
      return state;
  }
}
function extractAdjustmentData(action) {
  if (_.isArray(action.data)) {
    return _.map(action.data, (item) =>
      _.assign(
        { id: item.id },
        { legalEntityFamily: item.entityFamily.abbreviation },
        { legalEntityFamilyId: item.entityFamily.id },
        { legalEntityFamilyDisplayName: item.entityFamily.displayName },
        { legalEntity: item.tradingEntity.abbreviation },
        { legalEntityId: item.tradingEntity.id },
        { legalEntityDisplayName: item.tradingEntity.displayName },
        { exposureCounterParty: item.cpe.abbreviation },
        { exposureCounterPartyId: item.cpe.id },
        { exposureCounterPartyDisplayName: item.cpe.displayName },
        { book: item.book.displayName },
        { bookId: item.bookId },
        { agreementId: item.agreementId },
        { businessUnit: item.businessUnit.displayName ? item.businessUnit.displayName : 'N/A' },
        { businessUnitId: item.businessUnitId },
        { agreementType: item.agreementType.abbrev },
        { agreementTypeId: item.agreementTypeId },
        { adjustmentType: item.adjustmentType.name },
        { marginCallType: item.marginCallType.abbrev },
        { marginCallTypeId: item.marginCallTypeId },
        { custodianAccountId: item.caId },
        {
          custodianAccountDisplayName:
            item.custodianAccount && item.custodianAccount.displayName ? item.custodianAccount.displayName : 'N/A'
        },
        { adjustmentTypeId: item.adjustmentTypeId },
        { description: item.reasonCode.name },
        { reasonCodeId: item.reasonCode.id },
        { amount: item.adjustmentAmountInRptCcy},
        { startDate: convertJavaNYCDateTime(item.startDate).split(' ')[0] },
        { endDate: convertJavaNYCDateTime(item.endDate).split(' ')[0] },
        { source: item.workflowType.name },
        { publishedTime: convertJavaNYCDateTime(item.knowledgeTime) },
        { comment: item.comment },
        { user: item.login }
      )
    );
  }
  return [];
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state != true;
    default:
      return false;
  }
}
const rootReducer = combineReducers({
  data: adjustmentsDataReducer,
  saveAdjustmentData: saveAdjustmentsDataReducer,
  showCopyAdjustmentDialog: showCopyAdjustmentDialogReducer,
  showCopyAdjustmentAlert: showCopyAdjustmentAlertReducer,
  addedAdjustmentData: addedAdjustmentsDataReducer,
  showAddAdjustmentDialog: showAddAdjustmentDialogReducer,
  showAddAdjustmentAlert: showAddAdjustmentAlertReducer,
  showCopySuccessDialog: showCopySuccessDialogReducer,
  addAdjustmentSuccessDialog: showAddAdjustmentSuccessDialogReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
