import { linkFormatter, numberTotalsFormatter } from '../../../commons/grid/formatters';

export function gridColumns() {
  const columns = [
    {
      id: 'copyAdjustment',
      name: 'Copy Adjustment',
      field: 'copyAdjustment',
      toolTip: 'copyAdjustment',
      type: 'text',
      filter: true,
      sortable: true,
      formatter(row, cell, value, columnDef, dataContext) {
        return linkFormatter(row, cell, "<i class='icon-copy' />", columnDef, dataContext);
      },
      headerCssClass: 'b',
      width: 50
    },
    {
      id: 'legalEntityFamilyDisplayName',
      name: 'Legal Entity Family',
      headerCssClass: 'b',
      field: 'legalEntityFamilyDisplayName',
      type: 'text'
    },
    {
      id: 'legalEntity',
      name: 'Legal Entity',
      headerCssClass: 'b',
      field: 'legalEntity',
      type: 'text'
    },
    {
      id: 'exposureCounterParty',
      name: 'Exposure Counterparty',
      headerCssClass: 'b',
      field: 'exposureCounterParty',
      type: 'text'
    },
    {
      id: 'agreementType',
      name: 'Agreement Type',
      headerCssClass: 'b',
      field: 'agreementType',
      type: 'text'
    },
    {
      id: 'book',
      name: 'Book',
      headerCssClass: 'b',
      field: 'book',
      type: 'text'
    },
    {
      id: 'businessUnit',
      name: 'Business Unit',
      headerCssClass: 'b',
      field: 'businessUnit',
      type: 'text'
    },

    {
      id: 'adjustmentType',
      name: 'Adjustment Type',
      headerCssClass: 'b',
      field: 'adjustmentType',
      type: 'text'
    },
    {
      id: 'marginCallType',
      name: 'Margin Call Type',
      headerCssClass: 'b',
      field: 'marginCallType',
      type: 'text'
    },
    {
      id: 'description',
      name: 'Description',
      headerCssClass: 'b',
      field: 'description',
      type: 'text'
    },
    {
      id: 'amount',
      name: 'Amount (in RC)  ',
      field: 'amount',
      formatter(row, cell, value, columnDef, dataContext) {
        return numberTotalsFormatter(row, cell, value, columnDef, dataContext, 'amount');
      },
      headerCssClass: 'aln-rt b',
      type: 'number',
      sortable: true,
      toolTip: 'LMV refers to long market value of financed assets.',
      aggregator: dpGrid.Aggregators.sum
    },
    {
      id: 'startDate',
      name: 'Start Date',
      headerCssClass: 'b',
      field: 'startDate',
      type: 'text'
    },
    {
      id: 'endDate',
      name: 'End Date',
      headerCssClass: 'b',
      field: 'endDate',
      type: 'text'
    },
    {
      id: 'source',
      name: 'Source',
      headerCssClass: 'b',
      field: 'source',
      type: 'text'
    },
    {
      id: 'publishedTime',
      name: 'Published Time',
      headerCssClass: 'b',
      field: 'publishedTime',
      type: 'text'
    },
    {
      id: 'comment',
      name: 'Comment',
      headerCssClass: 'b',
      field: 'comment',
      type: 'text'
    },
    {
      id: 'user',
      name: 'User',
      headerCssClass: 'b',
      field: 'user',
      type: 'text'
    }
  ];

  return columns;
}
