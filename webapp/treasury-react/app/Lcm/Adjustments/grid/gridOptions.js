import { getAdjustmentsUrl } from "../api";
import { getSaveSettingsUrl } from "../../../commons/util.js";
export function gridOptions() {
  const options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + getAdjustmentsUrl;
      }
    },
    autoHorizontalScrollBar: true,
    expandTillLevel: -1,
    highlightRow: true,
    highlightRowOnClick: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    enableMultilevelGrouping: {
      isAddGroupingColumn: true
    },
    isFilterOnFormattedValue: true,
    customColumnSelection: true,
    configureColumns: true,
    page: true,
    saveSettings: {
      applicationId: 3,
      applicationCategory: "Adjustments Grid",
      serviceURL: getSaveSettingsUrl()
    },
    sheetName: "Adjustments data"
  };
  return options;
}
