import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ColumnLayout from '../../../commons/components/ColumnLayout';
import FilterButton from '../../../commons/components/FilterButton';
import SaveSetiing from '../../../commons/components/SaveSettingsManager';
import Sidebar from '../../../commons/components/Sidebar';
import AdjustmentTypeFilter from '../../../commons/container/AdjustmentTypeFilter';
import BusinessUnitFilter from '../../../commons/container/BusinessUnitFilter';
import CpeFilter from '../../../commons/container/CpeFilter';
import DateFilter from '../../../commons/container/DateFilter';
import CopySearchUrl from "commons/components/CopySearchUrl";
import LegalEntityFilter from '../../../commons/container/LegalEntityFilter';
import AgreementTypeFilter from "../../../commons/container/AgreementTypeFilter";
import { getKeysAsArray, getPreviousDate, executeCopySearchUrl} from '../../../commons/util';
import { fetchAdjustments } from '../actions';
import { resizeCanvas } from '../../../commons/actions';
import { PUBLISHED_WORKFLOW_STATUS_ID } from '../constants';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDataFromGlobalFilters = this.getDataFromGlobalFilters.bind(this);
    this.state = {};
  }

  componentDidMount() {
    let path = window.location.search;
    if (path != "" && !path.includes('=')) {
      let copySearchUrl = executeCopySearchUrl();
      if (copySearchUrl.isTrue) {
        this.setState(copySearchUrl.copySearchUrlState, () => this.handleClick());
      }
    }
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  componentDidUpdate(prevProps) {
    let path = window.location.search;
    if (path != "" && !path.includes('=')) {
      return;
    }
    if (prevProps != this.props && this.props.defaultFilterData) {
      let selectedLegalEntites = this.getDataFromGlobalFilters(this.props.defaultFilterData.selectedLegalEntities, 'legalEntities');
      let selectedCpes = this.getDataFromGlobalFilters(this.props.defaultFilterData.selectedCpes, 'cpes');
      let selectedAgreementTypes = this.getDataFromGlobalFilters(this.props.defaultFilterData.selectedAgreementTypes, 'agreementTypes');
      this.setState((previousState) => {
        return {
          ...previousState,
          selectedCpes: selectedCpes,
          selectedLegalEntities: selectedLegalEntites,
          selectedAgreementTypes: selectedAgreementTypes,
          selectedStartDate: this.props.defaultFilterData.date,
          selectedEndDate: this.props.defaultFilterData.date
        }
      })
    }
  }

  getDataFromGlobalFilters(data, key) {
    let filters = this.props.filterData;
    let selectedObjectList = [];
    if (filters[key].length > 0) {
      let objectList = filters[key];
      data.forEach(function (selectedObject) {
        objectList.forEach(function (object) {
          if (object.key == selectedObject) {
            selectedObjectList.push(object);
          }
        });
      });
    }
    return selectedObjectList;
  }


  onSelect(params) {
    const { key, value } = params;
    this.setState((previousState) => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }
  getDefaultFilters() {
    return {
      selectedEndDate: getPreviousDate(),
      selectedAgreementTypes: [],
      selectedBusinessUnits: [],
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedStartDate: getPreviousDate(),
      selectedAdjustmentTypes: [],
      toggleSidebar: false
    };
  }

  handleClick() {
    const payload = {
      '@CLASS': 'com.arcesium.treasury.model.lcm.adjustment.AdjustmentFilter',
      endDate: this.state.selectedEndDate,
      startDate: this.state.selectedStartDate,
      workflowStatusIds: [PUBLISHED_WORKFLOW_STATUS_ID],
      businessUnitIds: getKeysAsArray(this.state.selectedBusinessUnits),
      tradingEntityIds: getKeysAsArray(this.state.selectedLegalEntities),
      cpeIds: getKeysAsArray(this.state.selectedCpes),
      adjustmentTypeIds: getKeysAsArray(this.state.selectedAdjustmentTypes),
      agreementTypeIds: getKeysAsArray(this.state.selectedAgreementTypes),
      enrichLightly: true
    };
    this.props.fetchAdjustments(payload);
  }
  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }
  handleReset() {
    this.setState(this.getDefaultFilters());
  }
  render() {
    return (
      <Sidebar collapsible resizeCanvas={this.props.resizeCanvas} toggleSidebar={this.state.toggleSidebar}>
        <SaveSetiing
          applicationName={this.props.applicationName}
          applySavedFilters={this.applySavedFilters}
          selectedFilters={this.state}
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedStartDate"
          data={this.state.selectedStartDate}
          label="Start Date"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedEndDate"
          data={this.state.selectedEndDate}
          label="End Date"
        />
        <LegalEntityFilter onSelect={this.onSelect} selectedData={this.state.selectedLegalEntities} />
        <CpeFilter onSelect={this.onSelect} selectedData={this.state.selectedCpes} />
        <BusinessUnitFilter onSelect={this.onSelect} selectedData={this.state.selectedBusinessUnits} />
        <AdjustmentTypeFilter onSelect={this.onSelect} selectedData={this.state.selectedAdjustmentTypes} />
        <AgreementTypeFilter onSelect={this.onSelect} selectedData={this.state.selectedAgreementTypes} />
        <ColumnLayout>
          <FilterButton onClick={this.handleClick} reset={false} label="Search" />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
          <CopySearchUrl copySearchParams={this.state}/>
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.propTypes = {
  applicationName: PropTypes.any,
  fetchAdjustments: PropTypes.any,
  resizeCanvas: PropTypes.func
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchAdjustments,
      resizeCanvas
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    filterData: state.filters
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideBar);
