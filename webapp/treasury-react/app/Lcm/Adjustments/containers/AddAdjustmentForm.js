import { Dialog } from "arc-react-components";
import _ from "lodash";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  addAdjustments,
  hideAddAdjustmentDialog,
  showAddAdjustmentDialog,
  showAddAdjustmentSuccessDialog,
  clearAddAdjustmentStatus
} from "../actions";
import {ADJUSTMENT_TYPES} from "../constants"
import { getPreviousBusinessDay } from "../../../commons/util";
import AdjustmentFormComponent from '../../CometAdjustments/AdjustmentFormComponent'


class AddAdjustmentForm extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);

    this.state = this.getDefaultFilters();
  }

  onSelect = (params) => {
    const { key, value } = params;
    this.setState(previousState => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  getDefaultFilters() {
    return {
      ...this.props.data,
      selectedEndDate: getPreviousBusinessDay(),
      selectedBusinessUnits: [],
      selectedLegalEntities: [],
      selectedCpes: [],
      selectedStartDate: getPreviousBusinessDay(),
      selectedAdjustmentType: [],
      selectedAgreementTypes: [],
      selectedReasonCode: [],
      selectedCustodianAccounts: [],
      toggleSidebar: false,
      selectedBooks: [],
      amount: "",
      comment: ""
    };
  }

  getToggleSidebar() {
    return this.state.toggleSidebar;
  }
  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }
  handleClick() {
    const payload = {
      "Adjustment.sourceGridConfig": "ADJUSTMENT"
    };
    if (this.isNotEmpty(this.state.selectedAdjustmentType.key)) {
      payload[
        "Adjustment.adjustmentTypeId"
      ] = this.state.selectedAdjustmentType.key;
    }
    if (this.isNotEmpty(this.state.selectedLegalEntities.key)) {
      payload[
        "Adjustment.tradingEntityId"
      ] = this.state.selectedLegalEntities.key;
    }
    if (this.isNotEmpty(this.state.selectedCpes.key)) {
      payload["Adjustment.cpeId"] = this.state.selectedCpes.key;
    }
    if (this.isNotEmpty(this.state.selectedAgreementTypes.key)) {
      payload[
        "Adjustment.agreementTypeId"
      ] = this.state.selectedAgreementTypes.key;
    }
    const caId = _.isEmpty(this.state.selectedCustodianAccounts)
      ? this.state.custodianAccountId
      : this.state.selectedCustodianAccounts.key;
    if (this.isNotEmpty(caId)) {
      payload["Adjustment.caId"] = caId;
    }

    const bookId = _.isEmpty(this.state.selectedBooks)
      ? this.state.bookId
      : this.state.selectedBooks.key;
    if (this.isNotEmpty(bookId)) {
      payload["Adjustment.bookId"] = bookId;
    }

    const businessUnitId = _.isEmpty(this.state.selectedBusinessUnits)
      ? this.state.businessUnitId
      : this.state.selectedBusinessUnits.key;

    if (this.isNotEmpty(businessUnitId)) {
      payload["Adjustment.businessUnitId"] = businessUnitId;
    }

    if (
      this.isNotEmpty(this.state.selectedStartDate) &&
      this.state.selectedStartDate !== ""
    ) {
      payload["Adjustment.startDate"] = this.state.selectedStartDate;
    }
    if (
      this.isNotEmpty(this.state.selectedEndDate) &&
      this.state.selectedEndDate !== ""
    ) {
      payload["Adjustment.endDate"] = this.state.selectedEndDate;
    }

    const reasonCodeId = _.isEmpty(this.state.selectedReasonCodes)
      ? undefined
      : this.state.selectedReasonCodes.key;
    if (this.isNotEmpty(reasonCodeId)) {
      payload["Adjustment.reasonCodeId"] = reasonCodeId;
    }
    if (this.isNotEmpty(this.state.amount) && this.state.amount !== "") {
      let adjustmentAmount = this.state.amount.replace(/,/g, "");
      payload["Adjustment.adjustmentAmountInRptCcy"] = adjustmentAmount;
    }
    if (this.isNotEmpty(this.state.comment) && this.state.comment !== "") {
      payload["Adjustment.comment"] = this.state.comment;
    }
    this.props.addAdjustments(payload);
  }
  isNotEmpty = str => {
    return str !== undefined && str !== null;
  };
  handleReset = () => {
    this.setState(this.getDefaultFilters());
    this.props.clearAdjustments();
  };

  handleOpenModal() {
    this.props.showAddAdjustmentDialog();
  }
  handleCloseModal() {
    this.props.hideAddAdjustmentDialog();
    this.props.clearAdjustments();
    this.setState(this.getDefaultFilters());
  }

  prepareDataForAdjustmentForm = () => {
    let data = {
      selectedAdjustmentType: this.state.selectedAdjustmentType,
      selectedLegalEntity: this.state.selectedLegalEntities,
      selectedCounterParty: this.state.selectedCpes,
      selectedAgreementType: this.state.selectedAgreementTypes,
      selectedBook: this.state.selectedBooks,
      selectedBusinessUnit: this.state.selectedBusinessUnits,
      selectedCustodianAccount: this.state.selectedCustodianAccounts,
      selectedStartDate: this.state.selectedStartDate,
      selectedReasonCode: this.state.selectedReasonCode,
      selectedEndDate: this.state.selectedEndDate,
      adjustmentAmount: this.state.amount,
      comment: this.state.comment,
      adjustmentData: ADJUSTMENT_TYPES
    }
    return data;
  }

  render() {
    return (
      <AdjustmentFormComponent
        data={this.prepareDataForAdjustmentForm()}
        onChange={this.onSelect}
        onSave={this.handleClick}
        onReset={this.handleCloseModal}
        isOpen={this.props.isDialogOpen}
        addAdjustmentStatus={this.props.addAdjustmentStatus}
        resetAddAdjustmentStatus={this.props.clearAdjustments}
        limitedAdjustmentTypes={ADJUSTMENT_TYPES}
      />
    );
  }
}

AddAdjustmentForm.propTypes = {
  addAdjustmentStatus: PropTypes.any,
  addAdjustments: PropTypes.any,
  data: PropTypes.any,
  isDialogOpen: PropTypes.any,
  showAddAdjustmentDialog: PropTypes.any,
  hideAddAdjustmentDialog: PropTypes.any,
  clearAdjustments: PropTypes.any
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addAdjustments,
      showAddAdjustmentDialog,
      hideAddAdjustmentDialog,
      showAddAdjustmentSuccessDialog,
      clearAdjustments: clearAddAdjustmentStatus
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    addAdjustmentStatus: state.lcm.adjustmentsView.addedAdjustmentData,
    selectedFilters: state.lcm.drillDownFilters
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true }
)(AddAdjustmentForm);
