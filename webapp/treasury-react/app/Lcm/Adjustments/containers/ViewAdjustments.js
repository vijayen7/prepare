import { Dialog } from 'arc-react-components';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showAddAdjustmentDialog, hideAddAdjustmentSuccessDialog, fetchAdjustments } from '../actions';
import AddAdjustmentForm from './AddAdjustmentForm';
import Grid from './Grid';
import SideBar from './SideBar';
import queryString from 'query-string';
import { PUBLISHED_WORKFLOW_STATUS_ID } from '../constants';

export class ViewAdjustments extends Component {
  constructor(props) {
    super(props);
    this.handleOpenAddAdjustmentDialog = this.handleOpenAddAdjustmentDialog.bind(this);
    this.handleAddAdjustmentClose = this.handleAddAdjustmentClose.bind(this);
    this.state = {};
    this.setPayloadFields = this.setPayloadFields.bind(this);
    this.addAdjustmentFormRef = React.createRef();
  }

  componentDidMount() {
    let url = this.props.location.search;
    if (!url || url.length == 0 || !url.includes('=')) {
      return;
    }
    let params = queryString.parse(url);
    const payload = {};
    payload['@CLASS'] = 'com.arcesium.treasury.model.lcm.adjustment.AdjustmentFilter';
    payload['startDate'] = params.dateString;
    payload['endDate'] = params.dateString;
    payload['workflowStatusIds'] = [PUBLISHED_WORKFLOW_STATUS_ID];
    this.setPayloadFields(payload, params, 'tradingEntityIds');
    this.setPayloadFields(payload, params, 'cpeIds');
    this.setPayloadFields(payload, params, 'agreementTypeIds');
    payload.enrichLightly = true
    this.props.fetchAdjustments(payload);
    let defaultFilterData = {
      selectedCpes: payload.cpeIds || [],
      selectedLegalEntities: payload.tradingEntityIds || [],
      selectedAgreementTypes: payload.agreementTypeIds || [],
      date: payload.startDate
    }
    this.setState((previousState) => {
      return {
        ...previousState,
        defaultFilterData: defaultFilterData
      }
    });
  }

  setPayloadFields(payload, params, field) {
    if (!params[field]) {
      return;
    } else {
      payload[field] = params[field].split(',');
    }
  }

  handleOpenAddAdjustmentDialog() {
    this.props.showAddAdjustmentDialog();
  }
  handleAddAdjustmentClose() {
    this.props.hideAddAdjustmentSuccessDialog();
    if (this.addAdjustmentFormRef.current) {
      this.addAdjustmentFormRef.current.getWrappedInstance().handleReset();
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="layout--flex--row">
          <div className="layout--flex padding--top">
            <div className="size--content padding--right">
              <SideBar defaultFilterData={this.state.defaultFilterData} applicationName="Adjustments" />
            </div>
            <Grid handleOpenAddAdjustment={this.handleOpenAddAdjustmentDialog} />
          </div>
        </div>
        <Dialog
          title="Add Adjustment Status"
          isOpen={this.props.addAdjustmentSuccessDialog && this.props.addAdjustmentSuccessDialog.show}
          onClose={this.handleAddAdjustmentClose}
        >
          <div>{this.props.addAdjustmentSuccessDialog.message}</div>
        </Dialog>
        <AddAdjustmentForm isDialogOpen={this.props.isDialogOpen} ref={this.addAdjustmentFormRef} />
      </React.Fragment>
    );
  }
}

ViewAdjustments.propTypes = {
  addAdjustmentSuccessDialog: PropTypes.any,
  hideAddAdjustmentSuccessDialog: PropTypes.any,
  isDialogOpen: PropTypes.any,
  showAddAdjustmentDialog: PropTypes.any
};

function mapStateToProps(state) {
  return {
    isDialogOpen: state.lcm.adjustmentsView.showAddAdjustmentDialog,
    addAdjustmentStatus: state.lcm.adjustmentsView.addedAdjustmentData,
    addAdjustmentSuccessDialog: state.lcm.adjustmentsView.addAdjustmentSuccessDialog
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      showAddAdjustmentDialog,
      hideAddAdjustmentSuccessDialog,
      fetchAdjustments
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewAdjustments);
