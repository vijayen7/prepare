import { Dialog } from "arc-react-components";
import _ from "lodash";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import InputFilter from "../../../commons/components/InputFilter";
import Message from "../../../commons/components/Message";
import BookFilter from "../../../commons/container/BookFilter";
import BusinessUnitFilter from "../../../commons/container/BusinessUnitFilter";
import DateFilter from "../../../commons/container/DateFilter";
import ReasonCodeFilter from "../../../commons/container/ReasonCodeFilter";
import { getCommaSeparatedNumber } from "../../../commons/util";
import {
  hideCopyAdjustmentDialog,
  saveAdjustments,
  showCopyAdjustmentDialog,
  showCopySuccessDialog,
  clearSaveAdjustmentStatus
} from "../actions";
import LayoutWithSpace from "../../../commons/components/LayoutWithSpace";
import SelectWithLabel from "../components/SelectWithLabel";
class CopyAdjustmentFormContainer extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {};
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  onSelect(params) {
    const { key, value } = params;
    this.setState(previousState => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  getDefaultFilters() {
    const adjustMentTypeFromCopy =
      this.props.data.adjustmentTypeId && this.props.data.adjustmentTypeId > 0
        ? {
          key: this.props.data.adjustmentTypeId,
          value: this.props.data.adjustmentType
        }
        : {};

    const reasonCodeFromCopy =
      this.props.data.reasonCodeId && this.props.data.reasonCodeId > 0
        ? {
          key: this.props.data.reasonCodeId,
          value: this.props.data.description
        }
        : {};
    const businessUnitCopy =
      this.props.data.businessUnitId && this.props.data.businessUnitId > 0
        ? [
          {
            key: this.props.data.businessUnitId,
            value: this.props.data.businessUnit
          }
        ]
        : [];

    const bookCopy =
      this.props.data.bookId && this.props.data.bookId > 0
        ? [
          {
            key: this.props.data.bookId,
            value: this.props.data.book
          }
        ]
        : {
          key: -1,
          value: "All"
        };
    return {
      ...this.props.data,
      selectedEndDate: this.props.data.endDate,
      selectedBusinessUnits: businessUnitCopy,
      selectedLegalEntities: "",
      selectedCpes: [],
      selectedStartDate: this.props.data.startDate,
      selectedAdjustmentTypes: adjustMentTypeFromCopy,
      selectedReasonCodes: reasonCodeFromCopy,
      toggleSidebar: false,
      selectedBook: bookCopy,
      message: ""
    };
  }

  getToggleSidebar() {
    return this.state.toggleSidebar;
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }
  handleClick() {
    let businessUnitId;
    if (
      this.state.businessUnitId === undefined ||
      this.state.businessUnitId === -1
    ) {
      if (_.isEmpty(this.state.selectedBusinessUnits)) {
        ({ businessUnitId } = this.state);
      } else {
        businessUnitId = this.state.selectedBusinessUnits.key;
      }
    } else {
      ({ businessUnitId } = this.state);
    }

    let bookId;
    if (this.state.bookId === undefined || this.state.bookId === -1) {
      if (_.isEmpty(this.state.selectedBook)) {
        ({ bookId } = this.state);
      } else {
        bookId = this.state.selectedBook.key;
      }
    } else {
      ({ bookId } = this.state);
    }
    const payload = {
      "Adjustment.adjustmentTypeId": this.state.selectedAdjustmentTypes.key,
      "Adjustment.tradingEntityId": this.state.legalEntityId,
      "Adjustment.cpeId": this.state.exposureCounterPartyId,
      "Adjustment.agreementTypeId": this.state.agreementTypeId,
      "Adjustment.caId": this.state.custodianAccountId,
      "Adjustment.bookId": bookId,
      "Adjustment.businessUnitId": businessUnitId,
      "Adjustment.startDate": this.state.selectedStartDate,
      "Adjustment.endDate": this.state.selectedEndDate,
      "Adjustment.reasonCodeId": this.state.selectedReasonCodes.key,
      "Adjustment.adjustmentAmountInRptCcy": this.state.amountUSD
        ? this.state.amountUSD.toString().replace(/,/g, "")
        : 0,
      "Adjustment.comment": this.state.comment,

      "Adjustment.marginCallTypeId": this.state.marginCallTypeId,
      "Adjustment.agreementId": this.state.agreementId,
      "Adjustment.sourceGridConfig": "ADJUSTMENT"
    };
    this.props.saveAdjustments(payload);
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
    this.props.clearSaveAdjustmentStatus();
  }

  handleClose() {
    this.props.hideCopyAdjustmentDialog();
    this.props.clearSaveAdjustmentStatus();
  }

  render() {
    return (
      <React.Fragment>
        <Dialog
          isOpen={this.props.isDialogOpen}
          title="Add Adjustment"
          onClose={this.handleClose}
          footer={
            <React.Fragment>
              <button onClick={this.handleClick}>Save</button>
              <button onClick={this.handleReset}>Reset</button>
            </React.Fragment>
          }
        >
          {this.props.saveAdjustmentStatus &&
            _.isArray(this.props.saveAdjustmentStatus) &&
            this.props.saveAdjustmentStatus.length > 0 &&
            this.props.saveAdjustmentStatus[0].status === "failure" &&
            this.props.saveAdjustmentStatus[0].message !== "" && (
              <Message
                error
                messageData={this.props.saveAdjustmentStatus[0].message}
              />
            )}
          <LayoutWithSpace>
            <SelectWithLabel
              label="Adjustment Type"
              options={[this.state.adjustmentType]}
              value={this.state.adjustmentType}
              readonly
            />
            <SelectWithLabel
              label="Legal Entity"
              options={[this.state.legalEntityDisplayName]}
              value={this.state.legalEntityDisplayName}
              readonly
            />
            <SelectWithLabel
              label="Exposure Counterparties"
              options={[this.state.exposureCounterPartyDisplayName]}
              value={this.state.exposureCounterPartyDisplayName}
              readonly
            />
            <SelectWithLabel
              label="Agreement Type"
              options={[this.state.agreementType]}
              value={this.state.agreementType}
              readonly
            />
            <SelectWithLabel
              label="Custodian Account"
              options={[this.state.custodianAccountDisplayName]}
              value={this.state.custodianAccountDisplayName}
              readonly
            />
            {this.state.selectedAdjustmentTypes &&
              this.state.selectedAdjustmentTypes.key !== 3 &&
              (this.state.bookId === undefined || this.state.bookId === -1) && (
                <BookFilter
                  onSelect={this.onSelect}
                  selectedData={this.state.selectedBook}
                  multiSelect={false}
                  horizontalLayout
                />
              )}
            {this.state.selectedAdjustmentTypes &&
              this.state.selectedAdjustmentTypes.key !== 3 &&
              !(
                this.state.bookId === undefined || this.state.bookId === -1
              ) && (
                <SelectWithLabel
                  label="Books"
                  options={[this.state.book]}
                  value={this.state.book}
                  readonly
                />
              )}
            {this.state.selectedAdjustmentTypes &&
              this.state.selectedAdjustmentTypes.key !== 3 &&
              (this.state.businessUnitId === undefined ||
                this.state.businessUnitId === -1) && (
                <BusinessUnitFilter
                  onSelect={this.onSelect}
                  selectedData={this.state.selectedBusinessUnits}
                  multiSelect={false}
                  horizontalLayout
                />
              )}
            {this.state.selectedAdjustmentTypes &&
              this.state.selectedAdjustmentTypes.key !== 3 &&
              !(
                this.state.businessUnitId === undefined ||
                this.state.businessUnitId === -1
              ) && (
                <SelectWithLabel
                  label="Business Unit"
                  options={[this.state.businessUnit]}
                  value={this.state.businessUnit}
                  readonly
                />
              )}
            <DateFilter
              onSelect={this.onSelect}
              stateKey="selectedStartDate"
              data={this.state.selectedStartDate}
              label="Start Date"
            />
            <DateFilter
              onSelect={this.onSelect}
              stateKey="selectedEndDate"
              data={this.state.selectedEndDate}
              label="End Date"
            />
            <ReasonCodeFilter
              onSelect={this.onSelect}
              selectedData={this.state.selectedReasonCodes}
              multiSelect={false}
              horizontalLayout
            />
            <InputFilter
              data={getCommaSeparatedNumber(this.state.amountUSD)}
              onSelect={this.onSelect}
              stateKey="amountUSD"
              label="Adjustment Amount (USD)"
            />
            <InputFilter
              data={this.state.comment}
              onSelect={this.onSelect}
              stateKey="comment"
              label="Comment"
            />
          </LayoutWithSpace>
        </Dialog>
      </React.Fragment>
    );
  }
}

CopyAdjustmentFormContainer.propTypes = {
  data: PropTypes.any,
  hideCopyAdjustmentDialog: PropTypes.any,
  isDialogOpen: PropTypes.bool,
  saveAdjustmentStatus: PropTypes.any,
  saveAdjustments: PropTypes.any,
  clearSaveAdjustmentStatus: PropTypes.any
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveAdjustments,
      showCopyAdjustmentDialog,
      hideCopyAdjustmentDialog,
      showCopySuccessDialog,
      clearSaveAdjustmentStatus
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    saveAdjustmentStatus: state.lcm.adjustmentsView.saveAdjustmentData,
    selectedFilters: state.lcm.drillDownFilters
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CopyAdjustmentFormContainer);
