import { ReactArcGrid } from 'arc-grid';
import { Button, Dialog } from 'arc-react-components';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hideCopySuccessDialog, showCopyAdjustmentDialog } from '../actions';
import { gridColumns } from '../grid/columnConfig';
import { gridOptions } from '../grid/gridOptions';
import EmptyContentMessage from './../components/EmptyContentMessage';
import CopyAdjustmentFormContainer from './CopyAdjustmentFormContainer';
export class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.state = {
      selectedRowProps: {}
    };
  }

  onSelect(params) {
    const { key, value } = params;
    this.setState((previousState) => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  onCellClickHandler = (args) => {
    if (args.colId === 'copyAdjustment') {
      this.setState({ ...this.state, selectedRowProps: args.item });
      this.handleOpenModal();
    }
  };
  handleOpenModal() {
    this.props.showCopyAdjustmentDialog();
  }
  renderGridData() {
    let grid = null;
    if (!_.isArray(this.props.adjustmentsData) || this.props.adjustmentsData.length <= 0) {
      return <EmptyContentMessage handleOpenAddAdjustment={this.props.handleOpenAddAdjustment} />;
    }

    grid = (
      <React.Fragment>
        <div className="layout--flex--row">
          <div align="right" className="display--block size--content margin--right margin--bottom">
            <Button className="button.primary" onClick={this.props.handleOpenAddAdjustment}>
              Add Adjustment
            </Button>
          </div>
          <ReactArcGrid
            data={this.props.adjustmentsData}
            gridId="Adjustsments"
            columns={gridColumns(1)}
            options={gridOptions()}
            onCellClick={this.onCellClickHandler}
            label="Adjustments"
            resizeCanvas={this.props.resizeCanvas}
          />
        </div>
        {this.props.isDialogOpen ? (
          <CopyAdjustmentFormContainer isDialogOpen={this.props.isDialogOpen} data={this.state.selectedRowProps} />
        ) : null}
        <Dialog
          title="Add Adjustment Status"
          isOpen={this.props.copySuccessDialog && this.props.copySuccessDialog.show}
          onClose={this.props.hideCopySuccessDialog}
        >
          <div>{this.props.copySuccessDialog.message}</div>
        </Dialog>
      </React.Fragment>
    );
    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

Grid.propTypes = {
  adjustmentsData: PropTypes.array,
  copySuccessDialog: PropTypes.shape({
    show: PropTypes.bool.isRequired,
    message: PropTypes.string
  }),
  handleOpenAddAdjustment: PropTypes.func.isRequired,
  hideCopySuccessDialog: PropTypes.func,
  isDialogOpen: PropTypes.bool,
  showCopyAdjustmentDialog: PropTypes.func,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    adjustmentsData: state.lcm.adjustmentsView.data,
    isDialogOpen: state.lcm.adjustmentsView.showCopyAdjustmentDialog,
    copySuccessDialog: state.lcm.adjustmentsView.showCopySuccessDialog,
    resizeCanvas: state.lcm.adjustmentsView.resizeCanvas
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      showCopyAdjustmentDialog,
      hideCopySuccessDialog
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
