import {
  ADD_ADJUSTMENT,
  FETCH_COMET_ADJUSTMENTS,
  SAVE_ADJUSTMENT,
  SHOW_ADD_ADJUSTMENT_ALERT,
  SHOW_ADD_ADJUSTMENT_DIALOG,
  SHOW_COPY_ADJUSTMENT_ALERT,
  SHOW_COPY_ADJUSTMENT_DIALOG,
  COPY_SUCCESS_DIALOG,
  ADD_ADJUSTMENT_SUCCESS_DIALOG
} from '../../commons/constants';

export function fetchAdjustments(payload) {
  return {
    type: FETCH_COMET_ADJUSTMENTS,
    payload
  };
}

export function saveAdjustments(payload) {
  return {
    type: SAVE_ADJUSTMENT,
    payload
  };
}
export function clearSaveAdjustmentStatus() {
  return {
    type: `${SAVE_ADJUSTMENT}_CLEAR_MESSAGE`
  };
}

export function addAdjustments(payload) {
  return {
    type: ADD_ADJUSTMENT,
    payload
  };
}

export function clearAddAdjustmentStatus() {
  return {
    type: `${ADD_ADJUSTMENT}_CLEAR_MESSAGE`
  };
}
export function showCopyAdjustmentDialog() {
  return {
    type: `${SHOW_COPY_ADJUSTMENT_DIALOG}_OPEN`
  };
}
export function showCopySuccessDialog(payload) {
  return {
    type: `${COPY_SUCCESS_DIALOG}_SHOW`,
    payload
  };
}
export function hideCopySuccessDialog() {
  return {
    type: `${COPY_SUCCESS_DIALOG}_HIDE`
  };
}

export function showAddAdjustmentSuccessDialog(payload) {
  return {
    type: `${ADD_ADJUSTMENT_SUCCESS_DIALOG}_SHOW`,
    payload
  };
}
export function hideAddAdjustmentSuccessDialog() {
  return {
    type: `${ADD_ADJUSTMENT_SUCCESS_DIALOG}_HIDE`
  };
}

export function hideCopyAdjustmentDialog() {
  return {
    type: `${SHOW_COPY_ADJUSTMENT_DIALOG}_CLOSE`
  };
}
export function showCopyAdjustmentAlert() {
  return {
    type: `${SHOW_COPY_ADJUSTMENT_ALERT}_OPEN`
  };
}
export function hideCopyAdjustmentAlert() {
  return {
    type: `${SHOW_COPY_ADJUSTMENT_ALERT}_CLOSE`
  };
}
export function showAddAdjustmentDialog() {
  return {
    type: `${SHOW_ADD_ADJUSTMENT_DIALOG}_OPEN`
  };
}
export function hideAddAdjustmentDialog() {
  return {
    type: `${SHOW_ADD_ADJUSTMENT_DIALOG}_CLOSE`
  };
}
export function showAddAdjustmentAlert() {
  return {
    type: `${SHOW_ADD_ADJUSTMENT_ALERT}_OPEN`
  };
}
export function hideAddAdjustmentAlert() {
  return {
    type: `${SHOW_ADD_ADJUSTMENT_ALERT}_CLOSE`
  };
}
