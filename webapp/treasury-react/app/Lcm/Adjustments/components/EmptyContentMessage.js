import { Button } from 'arc-react-components';
import PropTypes from 'prop-types';
import React from 'react';
const EmptyContentMessage = (props) => {
  return (
    <div>
      <center>
        <div
          className="message"
          style={{
            width: '100%'
          }}
        >
          <p>Search to load data.</p>
          <Button className="button-primary" onClick={props.handleOpenAddAdjustment}>
            Add Adjustment
          </Button>
        </div>
      </center>
    </div>
  );
};

EmptyContentMessage.propTypes = {
  handleOpenAddAdjustment: PropTypes.func.isRequired
};
export default EmptyContentMessage;
