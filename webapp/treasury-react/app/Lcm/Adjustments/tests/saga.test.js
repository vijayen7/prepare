import { runSaga } from 'redux-saga';
import { END_LOADING, FETCH_COMET_ADJUSTMENTS, HANDLE_EXCEPTION, START_LOADING } from '../../../commons/constants';
import * as api from '../api';
import { fetchAdjustments } from '../saga';

describe('run fetch adjustments', () => {
  let getAdjustmentsMock;
  beforeEach(() => {
    getAdjustmentsMock = jest.spyOn(api, 'getAdjustments');
  });

  afterEach(() => {
    getAdjustmentsMock.mockRestore();
  });
  it('should handle success ', async () => {
    getAdjustmentsMock.mockImplementation(() => ({
      '@CLASS': 'com.arcesium.treasury.model.Adjustment',
      marginCallType: 'HOUSE'
    }));

    const dispatchedActions = [];
    const fakeStore = {
      getState: () => ({
        adjustments: ['@CLASS']
      }),
      dispatch: (action) => dispatchedActions.push(action)
    };

    await runSaga(fakeStore, fetchAdjustments, getAdjustmentsMock).done;
    expect(dispatchedActions).toEqual([
      { type: START_LOADING },
      {
        type: `${FETCH_COMET_ADJUSTMENTS}_SUCCESS`,
        data: {
          '@CLASS': 'com.arcesium.treasury.model.Adjustment',
          marginCallType: 'HOUSE'
        }
      },
      { type: END_LOADING }
    ]);
  });
  it('should handle failure ', async () => {
    getAdjustmentsMock.mockImplementation(() => ({
      '@CLASS': 'java.rmi.RemoteException',
      marginCallType: 'HOUSE'
    }));

    const dispatchedActions = [];
    const fakeStore = {
      getState: () => ({
        adjustments: ['@CLASS']
      }),
      dispatch: (action) => dispatchedActions.push(action)
    };

    await runSaga(fakeStore, fetchAdjustments, getAdjustmentsMock).done;

    expect(dispatchedActions).toEqual([
      { type: START_LOADING },
      { type: HANDLE_EXCEPTION },
      { type: `${FETCH_COMET_ADJUSTMENTS}_FAILURE` },
      { type: END_LOADING }
    ]);
  });
});
