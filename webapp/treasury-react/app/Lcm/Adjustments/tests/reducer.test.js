import { adjustmentsDataReducer } from '../reducer';
import { FETCH_COMET_ADJUSTMENTS } from '../../../commons/constants';
import data from './adjustmets';
describe('get adjustments reducer', () => {
  it('should return the initial state', () => {
    expect(adjustmentsDataReducer(undefined, {})).toEqual([]);
  });
  it('should handle FETCH_COMET_ADJUSTMENTS_SUCCESS', () => {
    const test = adjustmentsDataReducer({}, { type: `${FETCH_COMET_ADJUSTMENTS}_SUCCESS`, data });
    expect(test).toMatchSnapshot();
  });
  it('should handle FETCH_COMET_ADJUSTMENTS_FAILURE', () => {
    expect(adjustmentsDataReducer(undefined, { type: `${FETCH_COMET_ADJUSTMENTS}_FAILURE` })).toEqual([]);
  });
});
