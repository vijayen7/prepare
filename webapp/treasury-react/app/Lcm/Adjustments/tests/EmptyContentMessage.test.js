import { shallow } from 'enzyme';
import React from 'react';
import EmptyContentMessage from '../components/EmptyContentMessage';

describe('EmptyContentMessageTests', () => {
  const handleOpenAddAdjustmentMock = jest.fn();
  const wrapper = shallow(<EmptyContentMessage handleOpenAddAdjustment={handleOpenAddAdjustmentMock} />);
  it('should render correctly with no props', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
it('should exist a button', () => {
  const handleOpenAddAdjustmentMock = jest.fn();
  const wrapper = shallow(<EmptyContentMessage handleOpenAddAdjustment={handleOpenAddAdjustmentMock} />);
  expect(wrapper.find('.button-primary').length).toEqual(1);
  expect(wrapper.find('.message').length).toEqual(1);
});
