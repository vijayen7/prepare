import _ from 'lodash';
import { call, put, takeEvery } from 'redux-saga/effects';
import {
  ADD_ADJUSTMENT,
  END_LOADING,
  FETCH_COMET_ADJUSTMENTS,
  HANDLE_EXCEPTION,
  SAVE_ADJUSTMENT,
  SHOW_ADD_ADJUSTMENT_ALERT,
  SHOW_COPY_ADJUSTMENT_ALERT,
  START_LOADING
} from '../../commons/constants';
import { checkForException } from '../../commons/util';
import {
  hideCopyAdjustmentDialog,
  showCopySuccessDialog,
  hideAddAdjustmentDialog,
  showAddAdjustmentSuccessDialog,
  clearAddAdjustmentStatus,
  clearSaveAdjustmentStatus
} from '../Adjustments/actions';
import { addAdjustment, getAdjustments } from './api';

export function* fetchAdjustments(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getAdjustments, action.payload);
    checkForException(data);
    yield put({ type: `${FETCH_COMET_ADJUSTMENTS}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_COMET_ADJUSTMENTS}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveAdjustment(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(addAdjustment, action.payload);
    checkForException(data);
    if (data && _.isArray(data) && data.length > 0 && data[0].status === 'success') {
      yield put(hideCopyAdjustmentDialog());
      yield put(showCopySuccessDialog(data[0].message));
      yield put(clearSaveAdjustmentStatus());
    }
    yield put({ type: `${SAVE_ADJUSTMENT}_SUCCESS`, data });
    yield put({ type: `${SHOW_COPY_ADJUSTMENT_ALERT}_OPEN` });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_ADJUSTMENT}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}
function* addedAdjustment(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(addAdjustment, action.payload);
    data.amount = action.payload['Adjustment.adjustmentAmountInRptCcy'];
    checkForException(data);
    if (data && _.isArray(data) && data.length > 0 && data[0].status === 'success') {
      yield put(hideAddAdjustmentDialog());
      yield put(showAddAdjustmentSuccessDialog(data[0].message));
      yield put(clearAddAdjustmentStatus());
    }
    yield put({ type: `${ADD_ADJUSTMENT}_SUCCESS`, data });
    yield put({ type: `${SHOW_ADD_ADJUSTMENT_ALERT}_OPEN` });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${ADD_ADJUSTMENT}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* adjustmentsRootSaga() {
  yield [
    takeEvery(FETCH_COMET_ADJUSTMENTS, fetchAdjustments),
    takeEvery(SAVE_ADJUSTMENT, saveAdjustment),
    takeEvery(ADD_ADJUSTMENT, addedAdjustment)
  ];
}
