import { BASE_URL } from '../../commons/constants';
const queryString = require('query-string');
export let getAdjustmentsUrl = "";
export function getAdjustments(payload) {
  const paramString = encodeURIComponent(JSON.stringify(payload));
  const getAdjustmentsUrlExt =
    'service/lcmAdjustmentService/getAdjustments?inputFormat=JSON&format=JSON&adjustmentFilter=';
  getAdjustmentsUrl = `${BASE_URL}${getAdjustmentsUrlExt}${paramString}`;
  return fetch(getAdjustmentsUrl, {
    credentials: 'include'
  }).then((data) => data.json());
}
export function addAdjustment(payload) {
  const paramString = queryString.stringify(payload);
  const saveAdjustmentsExt = 'service/lcmAdjustmentService/saveAdjustments?inputFormat=PROPERTIES&';
  const url = `${BASE_URL}${saveAdjustmentsExt}${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}
