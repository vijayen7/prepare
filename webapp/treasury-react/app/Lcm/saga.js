import { all } from 'redux-saga/effects';
import { adjustmentsRootSaga } from '../Lcm/Adjustments/saga';

function* lcmRootSaga() {
  yield all([adjustmentsRootSaga()]);
}

export default lcmRootSaga;
