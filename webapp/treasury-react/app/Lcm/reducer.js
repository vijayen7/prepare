import { combineReducers } from 'redux';
import adjustmentsReducer from '../Lcm/Adjustments/reducer';

const rootReducer = combineReducers({
  adjustmentsView: adjustmentsReducer
});

export default rootReducer;
