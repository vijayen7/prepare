import React from "react"
import { useObserver } from "mobx-react-lite"
import PhysicalCollateralInventory from "../../AgreementSummary/components/PhysicalCollateralInventoryGrid/PhysicalCollateralInventory"
import { PostCollateralPopup } from "../../AgreementSummary/components/PostCollateralPopup"
import LogWire from "../../LogWire"
import AddAdjustmentStore from "../stores/AddAdjustmentStore"
import PostCollateralStore from "../stores/PostCollateralStore"
import AdjustmentPopup from "../../CometAdjustments"
import { BookMmfPopup } from "../components/BookMmfPopup"

const DialogContainer: React.FC<DialogContainerProps> = (props) => {
  const postCollateralStore = props.postCollateralStore
  const addAdjustmentStore = props.addAdjustmentStore
  return useObserver(() => <>
    <LogWire {...postCollateralStore.logWirePopupProps}
      isOpen={postCollateralStore.isLogWirePopupOpen}
      onClose={postCollateralStore.toggleLogWirePopup} />

    {postCollateralStore.isPostCollateralPopupOpen &&
      <PostCollateralPopup
        isOpen={postCollateralStore.isPostCollateralPopupOpen}
        onClose={postCollateralStore.togglePostCollateralPopup}
        onClickPostCash={postCollateralStore.onClickPostCash}
        onClickPostSecurities={postCollateralStore.onClickPostSecurities}
      />}

    {postCollateralStore.isInventoryDialogOpen &&
      <PhysicalCollateralInventory {...postCollateralStore.inventoryPositionsPopupProps}
        isInventoryDialogOpen={postCollateralStore.isInventoryDialogOpen}
        setIsInventoryDialogOpen={postCollateralStore.setIsInventoryDialogOpen}
      />}

      {postCollateralStore.isBookMmfPopupOpen && (
        <BookMmfPopup
          {...postCollateralStore.bookMmfTradePopupProps}
          isOpen={postCollateralStore.isBookMmfPopupOpen}
          onClose={postCollateralStore.toggleBookMmfPopup}
          onClickBookTrade={postCollateralStore.onClickBookTrade}
        />
      )}

    {addAdjustmentStore.isAdjustmentDialogOpen &&
      <AdjustmentPopup {...addAdjustmentStore.adjustmentDialogProps}
        isOpen={addAdjustmentStore.isAdjustmentDialogOpen}
        onClose={addAdjustmentStore.onCloseAdjustmentDialog}
      />}
  </>
  )
}

interface DialogContainerProps {
  postCollateralStore: PostCollateralStore,
  addAdjustmentStore: AddAdjustmentStore
}

export default DialogContainer
