import React from "react"
import { observer } from "mobx-react-lite"
import { Layout } from "arc-react-components";
import WorkflowActionsCard from '../WorkflowActionsCard'
import TopPanelGrid from "../components/TopPanelGrid";
import { AgreementDetailPanel } from "../components/AgreementDetailPanel";
import { DetailInformationPanel } from "./DetailInformationPanel";
import { AdjustmentsPanel } from "../components/AdjustmentsPanel";
import { WORKFLOW_OPERATION, MESSAGES } from "../constants"
import { downloadBrokerFile, downloadCrifFile } from "../utils"
import PostCollateralStore from "../stores/PostCollateralStore";
import AgreementWorkflowStore from "../stores/AgreementWorkflowStore";
import AddAdjustmentStore from "../stores/AddAdjustmentStore";
import showConfirmationDialog from "../components/ConfirmationDialog"

const AgreementWorkflowBaseComponent: React.FC<AgreementWorkflowBaseComponentProps> = (props) => {
  const agreementWorkflowStore = props.agreementWorkflowStore
  const postCollateralStore = props.postCollateralStore
  const addAdjustmentStore = props.addAdjustmentStore

  const onClickCancelWorkflow = React.useCallback(async () => {
    let cancelWorkflow: boolean = await showConfirmationDialog("Do you really want to Cancel the workflow?")
    if (cancelWorkflow) {
      agreementWorkflowStore.workflowExecute(WORKFLOW_OPERATION.CANCEL, MESSAGES.WORKFLOW_CANCELLED)
    }
  }, [])
  const onClickPublish = React.useCallback(() => { agreementWorkflowStore.workflowExecute(WORKFLOW_OPERATION.PUBLISH, MESSAGES.WORKFLOW_PUBLISHED) }, [])
  const onClickSaveAndExit = React.useCallback(() => { agreementWorkflowStore.workflowExecute(WORKFLOW_OPERATION.SAVE, MESSAGES.WORKFLOW_SAVED) }, [])

  const WorkflowActionsCardComponent = observer(() => {
    return (
      <WorkflowActionsCard
        searchDate={agreementWorkflowStore.searchDate}
        onClickCancelWorkflow={onClickCancelWorkflow}
        onClickPublish={onClickPublish}
        onClickSaveAndExit={onClickSaveAndExit} />);
  })

  const TopPanelGridComponent = observer(() => {
    if (agreementWorkflowStore.topPanelGridData.length > 0) {
      return <TopPanelGrid
        topPanelDataList={agreementWorkflowStore.topPanelGridData}
        handleAddAdjustment={addAdjustmentStore.addAdjustmentFromTopPanelGrid}
        downloadBrokerFile={(brokerFileUrl: React.ReactText) => downloadBrokerFile(brokerFileUrl, agreementWorkflowStore.searchDate)}
        downloadCrifFile={(crifFileUrl: string) => downloadCrifFile(crifFileUrl)}
        onClickPostCollateral={postCollateralStore.setAgreementData}
        quickCallReport={agreementWorkflowStore.generateCallReport}
        onclickAgreementName={agreementWorkflowStore.setAllDetail}
        populatePage={agreementWorkflowStore.setAgreementDetailForArcDataGrid}
        setLogCallActions={agreementWorkflowStore.updateAcmAndDisputeAmounts}
      />
    }
    return null;
  })


  const AgreementDetailPanelComponent = observer(() => {
    return (
      <AgreementDetailPanel {...agreementWorkflowStore.agreementDetail} />
    );
  })

  const DetailInformationPanelComponent = observer(() => {
    return (<DetailInformationPanel
      agreementWorkflowStore={agreementWorkflowStore}
      handleAddAdjustment={addAdjustmentStore.displayAdjustmentDialog} />);
  })

  const AdjustmentsPanelComponent = observer(() => {
    return (<AdjustmentsPanel adjustmentDataList={agreementWorkflowStore.adjustmentDataList} />);
  })

  return (
    <>
      <Layout>
        <Layout.Child key="workflowActionsCard" childId="workflowActionsCardChild" size="fit">
          <WorkflowActionsCardComponent />
        </Layout.Child>

        <Layout.Child key="emptySpace1" childId="emptySpaceChild1" size="0.05" />
        <Layout.Child key="topPanelGrid" childId="topPanelGridChild" size="fit">
          <TopPanelGridComponent />
        </Layout.Child>

        <Layout.Child key="emptySpace2" childId="emptySpaceChild2" size="0.05" />
        <Layout.Child key="agreementDetailGrid" childId="agreementDetailGridChild" size="fit">
          <AgreementDetailPanelComponent />
        </Layout.Child>

        <Layout.Child
          key="detailedInformationPanel"
          childId="detailedInformationPanelChild"
          size="0.75"
        >
          <DetailInformationPanelComponent />
        </Layout.Child>

        <Layout.Child key="adjustmentPanel" childId="adjustmentPanelChild" size="0.55" >
          <AdjustmentsPanelComponent />
        </Layout.Child>

      </Layout>
    </>
  );
}
interface AgreementWorkflowBaseComponentProps {
  agreementWorkflowStore: AgreementWorkflowStore,
  postCollateralStore: PostCollateralStore,
  addAdjustmentStore: AddAdjustmentStore
}

export default AgreementWorkflowBaseComponent
