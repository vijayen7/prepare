import React from "react"
import { Panel, TabPanel } from "arc-react-components";
import AgreementWorkflowStore from "../stores/AgreementWorkflowStore";
import CashCollateralGrid from "../components/CashCollateralGrid";
import SecuritiesCollateralGrid from "../components/SecuritiesCollateralGrid";
import UsageGrid from "../components/UsageGrid";
import { useObserver } from "mobx-react-lite"

const ExposureRequirementPositionsGrid = React.lazy(() => import('../components/ExposureRequirementPositionsGrid'));

const CashCollateralGridComponent: React.FC<DetailInformationPanelProps> = (props) => {
  return useObserver(() => {
    if (props.agreementWorkflowStore.agreementWorkflowDataStatus.inProgress) {
      return (
        <div className="loader loader--block margin--top" id="CashCollateralLoader" >Loading Data</div>
      );
    } else {
      return (
        <>
          <div className="message size--content margin--top" hidden={props.agreementWorkflowStore.custodianAccountDataList.length > 0} id="CashCollateralMessage" >No data found</div>
          <div className="layout--flex fill--width gutter">
            {props.agreementWorkflowStore.custodianAccountDataList.length > 0 &&
              <div style={{ height: '95%' }}><CashCollateralGrid
                cashCollateralDataList={props.agreementWorkflowStore.custodianAccountDataList}
                handleAddAdjustment={props.handleAddAdjustment}
                resizeCanvas={props.agreementWorkflowStore.cashCollateralTabResizeCanvasFlag}
                useAvailableSpace={false} /></div>}
          </div>
        </>
      );
    }
  })
}

const SecuritiesCollateralGridComponent: React.FC<DetailInformationPanelProps> = (props) => {
  return useObserver(() => {
    if (props.agreementWorkflowStore.positionDataStatus.inProgress) {
      return (
        <div className="loader loader--block margin--top" id="PhysicalCollateralLoader" >Loading Data</div>
      );
    } else {
      return (
        <>
          <div className="message size--content margin--top" hidden={props.agreementWorkflowStore.physicalCollateralDataList.length > 0} id="PhysicalCollateralMessage" >No data found</div>
          <div className="layout--flex fill--width gutter">
            {props.agreementWorkflowStore.physicalCollateralDataList.length > 0 &&
              <div style={{ height: '95%' }}><SecuritiesCollateralGrid
                securitiesCollateralDataList={props.agreementWorkflowStore.physicalCollateralDataList}
                handleAddAdjustment={props.handleAddAdjustment}
                resizeCanvas={props.agreementWorkflowStore.securitiesCollateralTabResizeCanvasFlag}
                useAvailableSpace={false} /></div>}
          </div>
        </>
      );
    }
  })
}

const ExposureRequirementPositionsGridComponent: React.FC<DetailInformationPanelProps> = (props) => {
  return useObserver(() => {
    if (props.agreementWorkflowStore.positionDataStatus.inProgress) {
      return (
        <div className="loader loader--block margin--top" id="ExposureLoader" >Loading Data</div>
      );
    } else {
      return (
        <>
          <div className="message size--content margin--top" hidden={props.agreementWorkflowStore.exposureRequirementPositionDataList.length > 0} id="ExposureMessage" >No data found</div>
          <div className="layout--flex fill--width gutter">
            <React.Suspense fallback={<div className="loader margin--top"> Loading </div>}>
              {props.agreementWorkflowStore.exposureRequirementPositionDataList.length > 0 &&
                <div style={{ height: '95%' }}><ExposureRequirementPositionsGrid
                  exposureRequirementPositionDataList={props.agreementWorkflowStore.exposureRequirementPositionDataList}
                  handleAddAdjustment={props.handleAddAdjustment}
                  resizeCanvas={props.agreementWorkflowStore.exposureRequirementTabResizeCanvasFlag}
                  useAvailableSpace={false} /></div>}
            </React.Suspense>
          </div>
        </>
      );
    }
  })
}

const UsageGridComponent: React.FC<DetailInformationPanelProps> = (props) => {
  return useObserver(() => {
    if (props.agreementWorkflowStore.bundlePositionDataStatus.inProgress) {
      return (
        <div className="loader loader--block margin--top" id="UsageLoader" >Loading Data</div>
      );
    } else {
      return (
        <>
          <div className="message size--content margin--top" hidden={props.agreementWorkflowStore.bundlePositionDataList.length > 0} id="UsageMessage" >No data found</div>
          <div className="layout--flex fill--width gutter">
            {props.agreementWorkflowStore.bundlePositionDataList.length > 0 &&
              <div style={{ height: '95%' }}><UsageGrid
                bundlePositionDataList={props.agreementWorkflowStore.bundlePositionDataList}
                handleAddAdjustment={props.handleAddAdjustment}
                resizeCanvas={props.agreementWorkflowStore.usageTabResizeCanvasFlag}
                useAvailableSpace={false} /></div>}
          </div>
        </>
      );
    }
  })
}

export const DetailInformationPanel: React.FC<DetailInformationPanelProps> = (props) => {
  const [selectedTab, setSelectedTab] = React.useState("cashCollateralTab");


  return (
    <>
      <Panel title="Detailed Information" collapsible>
        <TabPanel id="positionDataPanelId"
          onSelect={(tabId: string) => { handleSelect(tabId, props.agreementWorkflowStore); setSelectedTab(tabId) }}>
          <TabPanel.Tab key="cashCollateral" tabId="cashCollateralTab" label="Cash Collateral">
            {selectedTab == "cashCollateralTab" ? <CashCollateralGridComponent {...props} /> : undefined}
          </TabPanel.Tab>

          <TabPanel.Tab key="securitiesCollateral" tabId="securitiesCollateralTab" label="Securities Collateral">
            {selectedTab == "securitiesCollateralTab" ? <SecuritiesCollateralGridComponent {...props} /> : undefined}
          </TabPanel.Tab>

          <TabPanel.Tab key="exposureRequirement" tabId="exposureRequirementTab" label="Exposure &amp; Requirement">
            {selectedTab == "exposureRequirementTab" ? <ExposureRequirementPositionsGridComponent {...props} /> : undefined}
          </TabPanel.Tab>
          <TabPanel.Tab key="usage" tabId="usageTab" label="Usage" >
            {selectedTab == "usageTab" ? <UsageGridComponent {...props} /> : undefined}

          </TabPanel.Tab>
        </TabPanel>
      </Panel>
    </>
  )
}

const handleSelect = (tabId: string, agreementWorkflowStore: AgreementWorkflowStore) => {
  if (tabId === 'usageTab' && Object.keys(agreementWorkflowStore.agreementIdToBundlePositionDataMap).length === 0
    && !agreementWorkflowStore.isInternalDataMissing) {
    agreementWorkflowStore.loadBundlePositionData()
  }
  agreementWorkflowStore.resizeCanvas(tabId)
}

interface DetailInformationPanelProps {
  agreementWorkflowStore: AgreementWorkflowStore,
  handleAddAdjustment: Function
}
