import React, { useEffect } from "react"
import AppHeader, { whenHeaderRef } from "arc-header";
import { Breadcrumbs } from "arc-react-components";
import queryString from "query-string";
import AgreementWorkflowStore from "./stores/AgreementWorkflowStore"
import { ReactLoader } from "../../commons/components/ReactLoader";
import { useLocalStore, useObserver } from 'mobx-react-lite';
import moment from "moment"
import { DISPUTE_WORKFLOW } from "./constants";
import PostCollateralStore from "./stores/PostCollateralStore";
import AddAdjustmentStore from "./stores/AddAdjustmentStore";
import AgreementWorkflowBaseComponent from "./containers/AgreementWorkflowBaseComponent";
import DialogContainer from "./containers/DialogContainer";

const AgreementWorkflow: React.FC<AgreementWorkflowProps> = (props) => {

  const agreementWorkflowStore = useLocalStore(() => new AgreementWorkflowStore());
  const postCollateralStore = useLocalStore(() => new PostCollateralStore(agreementWorkflowStore));
  const addAdjustmentStore = useLocalStore(() => new AddAdjustmentStore(agreementWorkflowStore));

  const updateBreadCrumbs = async () => {
    const headerRef = await whenHeaderRef();
    headerRef.setBreadcrumbs(
      <>
        <Breadcrumbs.Item link="/" key="home">
          <i className="icon-home" />
        </Breadcrumbs.Item>
        <Breadcrumbs.Item link="/" key="disputeWorkflow">
          {DISPUTE_WORKFLOW}
        </Breadcrumbs.Item>
      </>
    );
  };

  useEffect(() => {
    updateBreadCrumbs();
    const values = queryString.parse(props.location.search);
    const workflowId = values.workflowId;
    const searchDate = values.date;
    const actionableAgreementId = values.actionableAgreementId;

    agreementWorkflowStore.setWorkflowId(workflowId)
    agreementWorkflowStore.searchDate = moment(searchDate, 'YYYYMMDD').format('YYYY-MM-DD')
    agreementWorkflowStore.actionableAgreementId = actionableAgreementId

    agreementWorkflowStore.loadAgreementWorkflowData()
    agreementWorkflowStore.loadPositionData()
  }, [0])

  return (
    <>
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} pageTitle={DISPUTE_WORKFLOW} />

        <AgreementWorkflowBaseComponent
          agreementWorkflowStore={agreementWorkflowStore}
          postCollateralStore={postCollateralStore}
          addAdjustmentStore={addAdjustmentStore}
        />
      </div>

      <LoaderComponent
        agreementWorkflowStore={agreementWorkflowStore}
        postCollateralStore={postCollateralStore}
      />

      <DialogContainer
        postCollateralStore={postCollateralStore}
        addAdjustmentStore={addAdjustmentStore}
      />
    </>
  )
}

const LoaderComponent: React.FC<LoaderComponentProps> = ({ agreementWorkflowStore, postCollateralStore }) => {
  return useObserver(() =>
    <ReactLoader
      inProgress={agreementWorkflowStore.agreementWorkflowDataStatus.inProgress ||
        agreementWorkflowStore.workflowOperationStatus.inProgress ||
        agreementWorkflowStore.generateCallReportStatus.inProgress ||
        postCollateralStore.collateralTermSearchStatus.inProgress ||
        postCollateralStore.inventoryPositionDataSearchStatus.inProgress ||
        postCollateralStore.logWirePopupDataSearchStatus.inProgress ||
        postCollateralStore.bookMmfStatus.inProgress
      }
    />
  )
}

interface LoaderComponentProps { agreementWorkflowStore: AgreementWorkflowStore, postCollateralStore: PostCollateralStore }

interface AgreementWorkflowProps {
  location: {
    search: String
  }
}

export default React.memo(AgreementWorkflow)
