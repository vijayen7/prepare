import { fetchPostURL } from "../../../commons/util";
import SERVICE_URLS from "../commons/urlConfigs"
import { getBookMmfTradeParams, getDisplayBookMmfPopupParams } from "../utils/postCollateralStoreUtils";

export const getEDSummaryForWire = (workflowId: number, actionableAgreementId: number,
  custodianAccountId: number) => {
  let edSummaryDataParam = "workflowId=" + workflowId;
  edSummaryDataParam += "&actionableAgreementId=" + actionableAgreementId;
  edSummaryDataParam += "&custodianAccountId=" + custodianAccountId;

  return fetchPostURL(SERVICE_URLS.postCollateralService.edSummaryUrl, edSummaryDataParam)
}

export const getCollateralTerms = (agreementId: number) => {
  let collateralTermParam = 'lcmPositionDataParam.agreementIds=' + agreementId;
  collateralTermParam += '&inputFormat=PROPERTIES&format=json';

  return fetchPostURL(SERVICE_URLS.postCollateralService.collateralTermUrl, collateralTermParam)
}

export const getInventoryPositionData = (physicalCollateralInputParam) => {
  let eligibleInventoryPositionParam = 'physicalCollateralInputParam=' + encodeURIComponent(JSON.stringify(physicalCollateralInputParam));
  eligibleInventoryPositionParam += '&inputFormat=json&format=json';

  return fetchPostURL(SERVICE_URLS.postCollateralService.eligibleInventoryPositionsUrl, eligibleInventoryPositionParam)
}

export const getBookMmfTradeDialogData = (params) => {
  let displayBookMmfPopupUrl = SERVICE_URLS.postCollateralService.bookMmfDialogData;
  let displayBookMmfPopupParams = getDisplayBookMmfPopupParams(params)

  return fetchPostURL(displayBookMmfPopupUrl, displayBookMmfPopupParams)
}

export const bookMmfTrade = (bookMmfParams) => {
  let bookMmfTradeUrl = SERVICE_URLS.postCollateralService.bookMmfTrade
  let bookMmfTradeParam = getBookMmfTradeParams(bookMmfParams)

  return fetchPostURL(bookMmfTradeUrl, bookMmfTradeParam)
}
