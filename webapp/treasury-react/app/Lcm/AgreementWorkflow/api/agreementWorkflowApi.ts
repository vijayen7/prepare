import { ExcessDeficitWorkflowFilter } from "../models";
import SERVICE_URLS from "../commons/urlConfigs";
import { ArcFetch } from "arc-commons";
import { fetchJSONPostURL } from "../../../commons/util";

const QUERY_ARGS = {
  credentials: "include",
  method: "GET",
};

export const getAgreementWorkflowData = (params: ExcessDeficitWorkflowFilter) => {
  params["@CLASS"] = "deshaw.treasury.common.model.lcm.ExcessDeficitWorkflowFilter"
  const url = `${SERVICE_URLS.agreementWorkflowService.getAgreementWorkflowData
    }?excessDeficitWorkflowFilter=${encodeURIComponent(
      JSON.stringify(params)
    )}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS)
}

export const quickCallReport = (workflowId: number, agreementId: number, treasuryAgreementId: number) => {
  const saveCallReportUrl = `${SERVICE_URLS.agreementWorkflowService.saveCallReport}?workflowId=${workflowId
  }&actionableAgreementId=${agreementId}&treasuryAgreementId=${treasuryAgreementId}&format=Json&inputFormat=PROPERTIES`;
  return ArcFetch.getJSON(saveCallReportUrl, QUERY_ARGS)
}

export const executeWorkflowOperation = (workflowId: number, workflowOperation: string) => {
  const workflowExecuteUrl = `${SERVICE_URLS.agreementWorkflowService.executeWorkflow}?workflowParam.workflowId=${workflowId}&status=${workflowOperation}`;
  return ArcFetch.getJSON(workflowExecuteUrl, QUERY_ARGS)
}

export const saveWorkflowArtifacts = (params: string) => {
  const saveWorkflowUrl = SERVICE_URLS.agreementWorkflowService.saveWorkflowArtifacts
  return fetchJSONPostURL(saveWorkflowUrl, params)
}
