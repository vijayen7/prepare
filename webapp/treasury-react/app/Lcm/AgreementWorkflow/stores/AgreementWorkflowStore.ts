import { action, observable, computed, toJS } from 'mobx'
import {
  INITIAL_AGREEMENT_DETAIL,
  INITIAL_SEARCH_STATUS,
  MESSAGES,
  TOAST_TYPE,
  WORKFLOW_OPERATION,
  WORKFLOW_STATUS_TYPE,
} from '../constants';
import { showToastService } from '../utils'
import {
  getAgreementWorkflowData,
  quickCallReport,
  saveWorkflowArtifacts,
  executeWorkflowOperation
} from "../api/agreementWorkflowApi";
import {
  getExcessDeficitWorkflowFilter,
  getTopPanelGridData,
  getDetailedInformationDataMaps,
  getAdjustmentDataMap,
  closeWorkflowWindow,
  getAdjustmentDetails,
  getPublishWorkflowParams,
  getAgreementDetails,
  getAgreementDetail,
  getAgreementAccountType
} from '../utils/agreementWorkflowStoreUtils';
import { showValidationDialog } from '../components/ConfirmationDialog';
import { AdjustmentData } from '../components/AgreementAdjustment';
import { SUCCESS, FAILURE } from '../../../commons/constants';
import { AgreementDetail } from '../models';

export default class AgreementWorkflowStore {

  @observable
  workflowId = -1

  @observable
  searchDate = ""

  actionableAgreementId: number = 0

  @observable
  agreementWorkflowDataStatus = INITIAL_SEARCH_STATUS

  @observable
  positionDataStatus = INITIAL_SEARCH_STATUS

  @observable
  bundlePositionDataStatus = INITIAL_SEARCH_STATUS

  @observable
  workflowOperationStatus = INITIAL_SEARCH_STATUS

  @observable
  generateCallReportStatus = INITIAL_SEARCH_STATUS;

  @observable
  topPanelGridData: Array<any> = [];

  @observable
  selectedAgreementId = 0

  @observable
  isInternalDataMissing = false

  @observable
  cashCollateralTabResizeCanvasFlag = true

  @observable
  securitiesCollateralTabResizeCanvasFlag = true

  @observable
  exposureRequirementTabResizeCanvasFlag = true

  @observable
  usageTabResizeCanvasFlag = true

  @observable
  resizeCanvas = (tabId: string) => {
    switch (tabId) {
      case "cashCollateralTab":
        this.cashCollateralTabResizeCanvasFlag = !this.cashCollateralTabResizeCanvasFlag;
        break;
      case "securitiesCollateralTab":
        this.securitiesCollateralTabResizeCanvasFlag = !this.securitiesCollateralTabResizeCanvasFlag;
        break;
      case "exposureRequirementTab":
        this.exposureRequirementTabResizeCanvasFlag = !this.exposureRequirementTabResizeCanvasFlag;
        break;
      case "usageTab":
        this.usageTabResizeCanvasFlag = !this.usageTabResizeCanvasFlag;
        break;
    }
  }

  @observable
  selectedAgreementAccountType: string = '';

  agreementAccountTypeToCashCollateralDataMap = {}

  @computed
  get custodianAccountDataList() {
    let custodianAccountDataList = this.agreementAccountTypeToCashCollateralDataMap[
      this.selectedAgreementAccountType
    ];
    return custodianAccountDataList ? custodianAccountDataList : [];
  }

  agreementAccountTypeToPhysicalCollateralPositionDataMap = {}

  @computed
  get physicalCollateralDataList() {
    let physicalCollateralDataList = this.agreementAccountTypeToPhysicalCollateralPositionDataMap[
      this.selectedAgreementAccountType
    ];
    return physicalCollateralDataList ? physicalCollateralDataList : [];
  }

  agreementIdToExposureRequirementPositionDataMap = {}

  @computed
  get exposureRequirementPositionDataList() {
    if (!this.agreementIdToExposureRequirementPositionDataMap[this.selectedAgreementId]) {
      return []
    }
    return this.agreementIdToExposureRequirementPositionDataMap[this.selectedAgreementId]
  }

  agreementIdToBundlePositionDataMap = {}

  @computed
  get bundlePositionDataList() {
    if (!this.agreementIdToBundlePositionDataMap[this.selectedAgreementId]) {
      return []
    }
    return this.agreementIdToBundlePositionDataMap[this.selectedAgreementId]
  }

  agreementIdToAdjustmentDataMap: Map<number, AdjustmentData[]> = new Map()

  @computed
  get adjustmentDataList() {
    if (!this.agreementIdToAdjustmentDataMap[this.selectedAgreementId]) {
      return []
    }
    return this.agreementIdToAdjustmentDataMap[this.selectedAgreementId]
  }

  @observable
  agreementDetail: AgreementDetail = INITIAL_AGREEMENT_DETAIL

  @action
  setAgreementDetail = (selectedRowData) => {
    this.agreementDetail = getAgreementDetail(selectedRowData);
  }


  @action
  setAllDetail = (selectedRowData) => {
    this.agreementDetail = getAgreementDetail(selectedRowData)
    this.selectedAgreementId = selectedRowData.agreementId;
    this.selectedAgreementAccountType = getAgreementAccountType(
      selectedRowData.agreementId,
      selectedRowData.accountType
    ).toString();
  }


  @action
  setAgreementDetailForArcDataGrid = (rowId: number) => {
    let levels = (rowId.toString()).split(".")
    let data = this.topPanelGridData[levels[0]];
    for (let i = 1; i < levels.length; i++) {
      data = (data["children"])[levels[i]]
    }
    this.setAgreementDetail(data)
  }

  @action
  setTopPanelGrid = (topPanelGridData) => {
    this.topPanelGridData = toJS(topPanelGridData)
  }

  @action
  setWorkflowId = (workflowId: number) => {
    this.workflowId = workflowId
  }

  loadAgreementWorkflowData = async () => {
    try {
      let params = getExcessDeficitWorkflowFilter(this.workflowId, false, false, this.actionableAgreementId);
      this.agreementWorkflowDataStatus.inProgress = true

      let agreementWorkflowData = await getAgreementWorkflowData(params)
      // Each element in agreementWorkflowData is coming as an array of 2 elements where first has the java object class name
      // and second element has data in it. Thus keeping all second elements.
      agreementWorkflowData = agreementWorkflowData.map(e => JSON.parse(e[1]));

      this.topPanelGridData = getTopPanelGridData(agreementWorkflowData, params.workflowId)
      this.agreementAccountTypeToCashCollateralDataMap = getDetailedInformationDataMaps(
        agreementWorkflowData
      ).agreementAccountTypeToCashCollateralDataMap
      this.agreementIdToAdjustmentDataMap = getAdjustmentDataMap(agreementWorkflowData)

      this.setApplicableAgreementDetail(agreementWorkflowData);

      this.isInternalDataMissing = agreementWorkflowData[0].isInternalDataMissing

    } catch (error) {
      console.log(error)
      showToastService(
        MESSAGES.OPEN_WORKFLOW_FAILURE_MESSAGE,
        TOAST_TYPE.CRITICAL
      );
      this.agreementWorkflowDataStatus.error = error;
    } finally {
      this.agreementWorkflowDataStatus.inProgress = false
    }
  }

  loadPositionData = async () => {
    try {
      let params = getExcessDeficitWorkflowFilter(this.workflowId, true, false, this.actionableAgreementId, false, false);

      this.positionDataStatus.inProgress = true

      let agreementWorkflowData = await getAgreementWorkflowData(params)
      agreementWorkflowData = agreementWorkflowData.map(e => JSON.parse(e[1]));

      let detailedInformationDataMaps = getDetailedInformationDataMaps(agreementWorkflowData)
      this.agreementAccountTypeToPhysicalCollateralPositionDataMap =
        detailedInformationDataMaps.agreementAccountTypeToPhysicalCollateralPositionDataMap;
      this.agreementIdToExposureRequirementPositionDataMap = detailedInformationDataMaps.agreementIdToExposureRequirementPositionDataMap

    } catch (error) {
      console.log(error)
      showToastService(
        MESSAGES.LOAD_POSITION_DATA_FAILURE,
        TOAST_TYPE.CRITICAL
      );
      this.positionDataStatus.error = error;
    } finally {
      this.positionDataStatus.inProgress = false
    }
  }

  loadBundlePositionData = async () => {
    try {
      let params = getExcessDeficitWorkflowFilter(this.workflowId, true, true, this.actionableAgreementId, false, false);

      this.bundlePositionDataStatus.inProgress = true

      let agreementWorkflowData = await getAgreementWorkflowData(params)
      agreementWorkflowData = agreementWorkflowData.map(e => JSON.parse(e[1]));

      let detailedInformationDataMaps = getDetailedInformationDataMaps(agreementWorkflowData)
      this.agreementIdToBundlePositionDataMap = detailedInformationDataMaps.agreementIdToBundlePositionDataMap
      // this.setAgreementDetail(agreementWorkflowData[0])

    } catch (error) {
      console.log(error)
      showToastService(
        MESSAGES.LOAD_BUNDLE_POSITION_DATA_FAILURE,
        TOAST_TYPE.CRITICAL
      );
      this.bundlePositionDataStatus.error = error;
    } finally {
      this.bundlePositionDataStatus.inProgress = false
    }
  }

  generateCallReport = async (agreementId: number) => {
    try {
      this.generateCallReportStatus.inProgress = true;
      let apiResponse = await quickCallReport(this.workflowId, agreementId, this.actionableAgreementId)
      if (apiResponse.successStatus === true) {
        this.generateCallReportStatus.inProgress = false;
        await showValidationDialog(SUCCESS, MESSAGES.GENERATE_CALL_REPORT_SUCCESS);
        console.log("Call report generated and emailed successfully.");
      } else {
        showToastService(
          MESSAGES.GENERATE_CALL_REPORT_FAILURE,
          TOAST_TYPE.CRITICAL
        )
      }
    } catch (error) {
      this.generateCallReportStatus.error = error;
      console.log(error)
      showToastService(
        MESSAGES.GENERATE_CALL_REPORT_FAILURE,
        TOAST_TYPE.CRITICAL
      )
    } finally {
      this.generateCallReportStatus.inProgress = false;
    }
  }

  /**
     * This method can be used to CANCEL and SAVE the workflow
     */
  workflowExecute = async (workflowOperation: WORKFLOW_OPERATION, statusMsg: string) => {
    try {
      this.workflowOperationStatus.inProgress = true
      await executeWorkflowOperation(
        this.workflowId,
        workflowOperation === WORKFLOW_OPERATION.PUBLISH
          ? WORKFLOW_STATUS_TYPE.PUBLISHED
          : workflowOperation
      );

      if (workflowOperation === WORKFLOW_OPERATION.CANCEL) {
        closeWorkflowWindow(this.actionableAgreementId, workflowOperation);
      } else if (workflowOperation === WORKFLOW_OPERATION.SAVE || workflowOperation === WORKFLOW_OPERATION.PUBLISH) {
        this.saveAgreementWorkflowArtifacts(workflowOperation, statusMsg, this.workflowId, this.actionableAgreementId);
      }
    } catch (error) {
      console.log(error)
      showToastService(MESSAGES.WORKFLOW_EXECUTE_FAILURE, TOAST_TYPE.CRITICAL)
    }
  }

  /**
     * Used once Workflow artifacts need to be saved. After the Quick Publish,
     * Save and Exit, Publish
     */
  saveAgreementWorkflowArtifacts = async (workflowOperation: string, statusMsg: string, workflowId: number, actionableAgreementId: number,
    agreementId?: number, wireAmount?: string) => {
    try {
      let params = getPublishWorkflowParams(workflowOperation, workflowId, actionableAgreementId, agreementId, wireAmount, '', this.topPanelGridData);

      let data = await saveWorkflowArtifacts(params)
      if (data.resultList[0].status === FAILURE) {
        showToastService(MESSAGES.WORKFLOW_PUBLISH_FAILURE, TOAST_TYPE.CRITICAL)
      } else {
        showToastService(statusMsg, TOAST_TYPE.SUCCESS)
        if (workflowOperation == WORKFLOW_OPERATION.PUBLISH) {
          let adjustmentDetail = getAdjustmentDetails(this.agreementIdToAdjustmentDataMap);
          let checkAndReload = !adjustmentDetail.isAdjustmentAddedInWorkflow;
          closeWorkflowWindow(this.actionableAgreementId, workflowOperation,
            getAgreementDetails(adjustmentDetail.maxKnowledgeTime, this.topPanelGridData, this.actionableAgreementId), checkAndReload);
        } else if (workflowOperation === WORKFLOW_OPERATION.SAVE) {
          closeWorkflowWindow(this.actionableAgreementId, workflowOperation, {}, true);
        }
      }
    } catch (error) {
      if (workflowOperation == WORKFLOW_OPERATION.SAVE) {
        showToastService("Error occurred while saving workflow", TOAST_TYPE.CRITICAL);
      } else if (workflowOperation == WORKFLOW_OPERATION.PUBLISH) {
        showToastService("Error occurred while publishing workflow", TOAST_TYPE.CRITICAL);
      }
      console.log(error);
    } finally {
      this.workflowOperationStatus.inProgress = false
    }
  }

  @action
  updateAcmAndDisputeAmounts = (agreementRow, rerenderGrid: Boolean = false) => {
    let topPanelRowIndex = this.topPanelGridData.findIndex(e => e["id"] === agreementRow.id);
    this.topPanelGridData[topPanelRowIndex]["acmRC"] = agreementRow.acmRC;
    this.topPanelGridData[topPanelRowIndex]["asmRC"] = agreementRow.asmRC;
    this.topPanelGridData[topPanelRowIndex]["disputeAmountRC"] = agreementRow.disputeAmountRC;

    if (rerenderGrid) {
      this.topPanelGridData[topPanelRowIndex] = { ... this.topPanelGridData[topPanelRowIndex] };
    }
  }

  private setApplicableAgreementDetail(agreementWorkflowData: any) {
    let treasuryAgreementData = agreementWorkflowData[0];
    if (treasuryAgreementData.agreementType === 'MNA') {
      this.setAllDetail(treasuryAgreementData);
    } else {
      this.setAllDetail(treasuryAgreementData.children[0]);
    }
  }
}
