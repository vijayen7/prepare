import { action, observable } from 'mobx'
import AgreementWorkflowStore from './AgreementWorkflowStore'
import { AdjustmentDialogProps } from '../../CometAdjustments/models/AdjustmentPopupData'
import { getAdjustmentDialogProps } from '../utils/addAdjustmentStoreUtils'

export default class AddAdjustmentStore {
  agreementWorkflowStore: AgreementWorkflowStore
  constructor(agreementWorkfowStore: AgreementWorkflowStore) {
    this.agreementWorkflowStore = agreementWorkfowStore
  }

  @observable
  isAdjustmentDialogOpen = false

  @action
  onCloseAdjustmentDialog = () => {
    this.isAdjustmentDialogOpen = false
  }

  adjustmentDialogProps?: AdjustmentDialogProps

  @action
  displayAdjustmentDialog = (sourceGridConfig: string, agreementId: number, startDataStr: string, legalEntityId: number,
    counterPartyEntityId: number, agreementTypeId: number, calculator?: string,
    custodianAccount?: string, pnlSpn?: number, custodianAccountId?: string, entityFamilyId?: number,
    actionableType?: string, businessUnitId?: number, bookId?: number, reportingCurrency?: string, tag?: string) => {

    this.adjustmentDialogProps = getAdjustmentDialogProps(this.agreementWorkflowStore.workflowId, sourceGridConfig, agreementId, startDataStr, legalEntityId,
      counterPartyEntityId, agreementTypeId, calculator,
      custodianAccount, pnlSpn, custodianAccountId, entityFamilyId,
      actionableType, businessUnitId, bookId, reportingCurrency, tag)
    this.isAdjustmentDialogOpen = true
  }

  @action
  addAdjustmentFromTopPanelGrid = (sourceConfig: string, agreementId: number, legalEntityId: number, exposureCounterPartyId: number,
    agreementTypeId: number, agreementType: string, reportingCurrency: string) =>
    this.displayAdjustmentDialog(sourceConfig,
      agreementId, this.agreementWorkflowStore.searchDate, legalEntityId, exposureCounterPartyId,
      agreementTypeId, undefined, undefined, undefined, undefined, undefined, agreementType, undefined, undefined, reportingCurrency)
}
