import { action, computed, observable } from 'mobx'
import { showToastService } from '../utils'
import { bookMmfTrade, getBookMmfTradeDialogData, getCollateralTerms, getEDSummaryForWire, getInventoryPositionData } from '../api/postCollateralApi'
import { INITIAL_AGREEMENT_DATA, INITIAL_BOOK_MMF_POPUP_PROPS, INITIAL_SEARCH_STATUS, MESSAGES, TOAST_TYPE } from '../constants'
import { getAgreementData, getInventoryPositionPopupProps, getLogWirePopupProps, getPhysicalCollateralParam, getRelevantCollateralTerm, getUpdatedTopPanelGridData, getUpdatedTopPanelGridDataForPostCollateralSuccess } from '../utils/postCollateralStoreUtils'
import AgreementWorkflowStore from './AgreementWorkflowStore'

export default class PostCollateralStore {

  agreementWorkflowStore: AgreementWorkflowStore
  constructor(agreementWorkfowStore: AgreementWorkflowStore) {
    this.agreementWorkflowStore = agreementWorkfowStore
  }

  @observable
  isPostCollateralPopupOpen = false

  @action
  togglePostCollateralPopup = () => {
    this.isPostCollateralPopupOpen = !this.isPostCollateralPopupOpen
  }

  @observable
  logWirePopupDataSearchStatus = INITIAL_SEARCH_STATUS

  @observable
  isLogWirePopupOpen = false

  @action
  toggleLogWirePopup = () => {
    this.isLogWirePopupOpen = !this.isLogWirePopupOpen
  }

  @observable
  isBookMmfPopupOpen = false

  @action
  toggleBookMmfPopup = () => {
    this.isBookMmfPopupOpen = !this.isBookMmfPopupOpen
  }

  @observable
  bookMmfStatus = INITIAL_SEARCH_STATUS

  @observable
  collateralTermSearchStatus = INITIAL_SEARCH_STATUS

  @observable
  inventoryPositionDataSearchStatus = INITIAL_SEARCH_STATUS

  @observable
  isInventoryDialogOpen = false

  @action
  setIsInventoryDialogOpen = (isInventoryDialogOpen: boolean) => {
    this.isInventoryDialogOpen = isInventoryDialogOpen
  }

  agreementData = INITIAL_AGREEMENT_DATA

  logWirePopupProps: any = {}

  @observable
  bookMmfDialogData: any = INITIAL_BOOK_MMF_POPUP_PROPS;

  @computed
  get bookMmfTradePopupProps() {
    if (this.bookMmfDialogData !== INITIAL_BOOK_MMF_POPUP_PROPS)
      return {
        segCustodianAccountName: this.bookMmfDialogData.segCustodianAccountAbbrev,
        mmfSpn: this.bookMmfDialogData.mmfSpn,
        amount: this.bookMmfDialogData.mmfAmount,
        description: this.bookMmfDialogData.description
      }
  }

  inventoryPositionsPopupProps = {}

  setAgreementData = (item, agreementId: number, page = "AgreementWorkflow", onPostCollateralSuccess = undefined) => {
    this.agreementData = getAgreementData(item, agreementId,
      this.agreementWorkflowStore.searchDate,
      this.agreementWorkflowStore.agreementDetail.internalVsExternalData,
      page, onPostCollateralSuccess)
    this.togglePostCollateralPopup()
  }

  onClickPostCash = async () => {
    try {
      if (this.isPostCollateralPopupOpen) this.togglePostCollateralPopup()
      let workflowId = this.agreementWorkflowStore.workflowId
      this.logWirePopupDataSearchStatus.inProgress = true

      let data = await getEDSummaryForWire(workflowId, this.agreementData.actionableAgreementId, this.agreementData.custodianAccountId)

      let wirePortfolios = data.resultList[0];
      this.agreementData.actionableAgreementId = wirePortfolios.actionableAgreementId
      this.agreementData.custodianAccountId = wirePortfolios.custodianAccountId;

      this.logWirePopupProps = getLogWirePopupProps(workflowId, this.agreementData, wirePortfolios, (response, acmSign) => this.onLoggingWireRequest(response, acmSign))
      this.toggleLogWirePopup();
    } catch (error) {
      showToastService(MESSAGES.SHOW_LOG_WIRE_POPUP_FAILURE, TOAST_TYPE.CRITICAL)
    } finally {
      this.logWirePopupDataSearchStatus.inProgress = false
    }
  }

  @action
  onLoggingWireRequest(response, acmSign) {

    let page = this.agreementData.page;

    if (page && (page == 'AgreementWorkflow' || page == 'AgreementSummary') && response.bookMmf == true) {
      let params = {...response};
      params.wireAmount *= acmSign;
      params.mmfAmount *= acmSign;
      params.reportingCurrencyFxRate = this.agreementData.reportingCurrencyFxRate;
      params.treasuryAgreementId = this.agreementWorkflowStore.actionableAgreementId;
      this.onDisplayBookMmfPopup(params);
    }

    if (page && page == 'AgreementWorkflow' && response.status) {
      let agreementWorkflowData = this.agreementWorkflowStore.topPanelGridData

      let actionableKey = this.agreementData.actionableAgreementId + this.agreementData.tag;
      let wireAmount = response.wireAmount;
      let wireRequestNo = response.wireRequestNo;

      let topPanelGridData = getUpdatedTopPanelGridData(agreementWorkflowData, actionableKey, acmSign, wireAmount, wireRequestNo)
      if (topPanelGridData && topPanelGridData.length) this.agreementWorkflowStore.setTopPanelGrid(topPanelGridData)
    }

    if (page && page == 'AgreementSummary' && response.status && this.agreementData.onPostCollateralSuccess) {
      let params = {...response};
      params.wireAmount *= acmSign;
      this.agreementData.onPostCollateralSuccess(params.wireAmount);
    }
  }

  onDisplayBookMmfPopup = async (params: any) => {
    try {
      this.bookMmfStatus.inProgress = true
      let data = await getBookMmfTradeDialogData(params)
      this.bookMmfDialogData = data.resultList[0]
      this.isBookMmfPopupOpen = true
    } catch (error) {
      showToastService(MESSAGES.DISPLAY_BOOK_MMF_DIALOG_FAILURE, TOAST_TYPE.CRITICAL)
    } finally {
      this.bookMmfStatus.inProgress = false
    }
  }

  onClickBookTrade = async (mmfSpn: number) => {
    try {
      this.bookMmfStatus.inProgress = true;
      let bookMmfDialogData = { ...this.bookMmfDialogData, mmfSpn: mmfSpn };
      let data = await bookMmfTrade(bookMmfDialogData);
      let bookMmfObj = data.resultList[0];
      let bookMmfMsg = bookMmfObj.bookMmfTradeMsg;
      let mmfBookingSuccessFlag = bookMmfObj.mmfBookingSuccessFlag;

      if (bookMmfMsg) showToastService(bookMmfMsg, TOAST_TYPE.INFO)
      if (mmfBookingSuccessFlag) this.toggleBookMmfPopup()
    } catch (error) {
      console.log(error)
      showToastService(MESSAGES.BOOK_MMF_TRADE_FAILURE, TOAST_TYPE.CRITICAL)
    } finally {
      this.bookMmfStatus.inProgress = false
    }
  }

  onClickPostSecurities = async () => {
    try {
      this.togglePostCollateralPopup()
      let agreementId = this.agreementData.actionableAgreementId;
      if (!isNaN(this.agreementData.tripartyAgreementId) && this.agreementData.accountType === 'IM') {
        agreementId = this.agreementData.tripartyAgreementId;
      }
      this.collateralTermSearchStatus.inProgress = true

      let data = await getCollateralTerms(agreementId)

      let collateralTerm = getRelevantCollateralTerm(data, this.agreementData.accountType);
      if (!collateralTerm || !collateralTerm.physicalCollateralTerm) {
        showToastService(MESSAGES.PHYSICAL_COLLATERAL_TERM_MISSING, TOAST_TYPE.INFO)
        return;
      }

      this.fetchInventoryPositionData(agreementId);
    } catch (error) {
      showToastService(MESSAGES.FETCH_COLLATERAL_TERMS_FAILURE, TOAST_TYPE.CRITICAL)
    } finally {
      this.collateralTermSearchStatus.inProgress = false
    }
  }

  fetchInventoryPositionData = async (agreementId: number) => {
    try {
      let intVsExtData = this.agreementData.internalVsExternalData;
      let actionableDate = this.agreementData.actionableDate;
      let physicalCollateralInputParam = getPhysicalCollateralParam(
        agreementId,
        actionableDate,
        this.agreementData.accountType,
        this.agreementData.isPopulateAsm ? this.agreementData.asm : this.agreementData.acm
      );

      this.inventoryPositionDataSearchStatus.inProgress = true;
      let data = await getInventoryPositionData(physicalCollateralInputParam);

      this.inventoryPositionsPopupProps = getInventoryPositionPopupProps(
        data,
        actionableDate,
        intVsExtData,
        physicalCollateralInputParam,
        this.updatePhysicalCollateralSuccessForBookingReferenceId
      );
      this.setIsInventoryDialogOpen(true);
    } catch (error) {
      console.log(error);
      showToastService(
        MESSAGES.FETCH_INVENTORY_POSITIONS_FAILURE,
        TOAST_TYPE.CRITICAL
      );
    } finally {
      this.inventoryPositionDataSearchStatus.inProgress = false;
    }
  };

  @action
  updatePhysicalCollateralSuccessForBookingReferenceId = async (bookingReferenceId) => {
    let page = this.agreementData.page
    if (page && page == 'AgreementWorkflow') {
      let agreementWorkflowData = this.agreementWorkflowStore.topPanelGridData
      let actionableKey = this.agreementData.actionableAgreementId + this.agreementData.tag;
      let topPanelGridData = getUpdatedTopPanelGridDataForPostCollateralSuccess(agreementWorkflowData, actionableKey, bookingReferenceId)
      if (topPanelGridData && topPanelGridData.length) this.agreementWorkflowStore.setTopPanelGrid(topPanelGridData)
    }
    if (page && page == 'AgreementSummary' && this.agreementData.onPostCollateralSuccess) {
      this.agreementData.onPostCollateralSuccess()
    }
  }
}
