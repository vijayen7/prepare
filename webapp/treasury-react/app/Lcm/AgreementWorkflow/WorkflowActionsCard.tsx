import React, { Fragment } from "react"
import ButtonWithPause from "../../commons/components/ButtonWithPause"

const WorkflowActionsCard: React.FC<WorkflowActionProps> = (props) => {
  return (
    <>
      <div className="size--content container--action">
        <div className="form">
          <div className="row">
            <a className="size--content" id="cancelId" onClick={props.onClickCancelWorkflow}>Cancel & Go Back</a>
            <div></div>
            {props.searchDate && (
              <span id="workflowDateInfo" className="size--content margin--right--large">
                Workflow for <strong id="searchDate">{props.searchDate}</strong>
              </span>
            )}
            <ButtonWithPause className="size--content" id="publishId" onClick={props.onClickPublish} data={<Fragment>Publish</Fragment>}/>
            <button className="size--content" id="saveId" onClick={props.onClickSaveAndExit}>
              Save & Exit
            </button>
          </div>
        </div>
      </div>
    </>
  )
}

interface WorkflowActionProps {
  onClickCancelWorkflow: () => void,
  onClickPublish: () => void,
  onClickSaveAndExit: () => void,
  searchDate: String
}

export default React.memo(WorkflowActionsCard)
