import { formatNumberUsingCommas } from '../../commons/grid/formatters'
import { ToastService } from "arc-react-components";
import { TOAST_TYPE } from "./constants";


export function downloadBrokerFile(brokerFileUrl: string | number, date: string) {
  if (typeof brokerFileUrl === "string" && brokerFileUrl.includes('transfers')) {
    window.open('cometFileDownload?fileName=&contentType=&fileUrl=' + brokerFileUrl, '_blank');
  } else {
    window.open('/cocoa/api/file/downloadFile?objectUri=&fileId=' + brokerFileUrl + '&fileDate=' + date);
  }
}

export function downloadCrifFile(crifFileUrl: string) {
  let downloableFileName = 'CRIF_file';
  window.open('downloadSimulationReport?fileName=' + downloableFileName + ".xls&contentType=&fileUrl=" + crifFileUrl, '_blank');
}


export function TextBoxNumberFormatter(row, cell, value, columnDef, dataContext) {
  if (
    value != undefined &&
    value !== "" &&
    (typeof value == "string" || !isNaN(value))
  ) {
    value += "";
    value = value.replace(/,/g, "").replace(/x/g, "");
    if(isNaN(value)){
      return "n/a"
    }
    var num = Math.round(value);
    value = formatNumberUsingCommas(num);
    if (num < 0) {
      return "(" + value.replace(/-/, "") + ")";
    } else {
      return value;
    }
  } else {
    return "n/a";
  }
}

const getToastType = (type: string) => {
  let toastType: any = ToastService.ToastType.SUCCESS;
  switch (type) {
    case TOAST_TYPE.CRITICAL:
      toastType = ToastService.ToastType.CRITICAL;
      break;
    case TOAST_TYPE.SUCCESS:
      toastType = ToastService.ToastType.SUCCESS;
      break;
    case TOAST_TYPE.INFO:
      toastType = ToastService.ToastType.INFO;
      break;
  }
  return toastType;
};

export const showToastService = (content: string, type: string) => {
  ToastService.append({
    content: content,
    type: getToastType(type),
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: 7000,
  });
};
