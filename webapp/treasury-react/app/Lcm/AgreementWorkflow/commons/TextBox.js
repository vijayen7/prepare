import React, { Component } from "react";
import PropTypes from 'prop-types';
import { TextBoxNumberFormatter } from '../utils'

export default class TextBox extends Component {
  constructor(props) {
    super(props);
    this.myInputRef = React.createRef();
    if (props.value === undefined || props.value === "") {
      this.state = {
        backgroundColor: "white",
        formattedValue: props.defaultValue,
        editingMode: false
      };
    } else {
      this.state = {
        backgroundColor: "white",
        formattedValue: TextBoxNumberFormatter(null, null, props.value),
        value: props.value,
        editingMode: false
      };
    }

  }

  onFocusOut = () => {
    if (this.state.value != undefined) {
      this.setState({
        backgroundColor: "white",
        editingMode: false,
        formattedValue: TextBoxNumberFormatter(null, null, this.state.value),
      });
      if (this.props.onFocusOut != undefined) {
        this.props.onFocusOut(this.state.value);
      }
    } else {
      this.setState({
        backgroundColor: "white",
        editingMode: false,
      });
    }
  }

  onChange = (e) => {
    this.setState({
      ...this.state,
      value: e.target.value,
    });
    this.props.onChange(e.target.value)
  };

  handleClick = (event) => {
    this.props.onClick();
  }

  onFocus = (event) => {
    this.setState({
      ...this.state,
      backgroundColor: "black",
      editingMode: true
    }, () => { this.myInputRef.current.select(); });
  }

  render() {
    return (
      <input
        style={{ backgroundColor: this.state.backgroundColor, color: "#b9b9b9" }}
        type="text"
        value={this.state.editingMode ? this.state.value : this.state.formattedValue}
        onChange={this.onChange}
        onBlur={this.onFocusOut}
        onClick={this.handleClick}
        onFocus={this.onFocus}
        ref={this.myInputRef}
      />
    );
  }
}


TextBox.propTypes = {
  value: PropTypes.number.isRequired,
  defaultValue: PropTypes.string.isRequired,
};
