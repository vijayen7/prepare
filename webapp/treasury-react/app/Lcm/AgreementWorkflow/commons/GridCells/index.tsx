import React from 'react';

export function IntCell({ value }: { value: any }) {
  return <div className="text-align--right">{value}</div>;
}

export function FloatCell({ value }: { value: any }) {
  return <div className="text-align--right">{getFormattedFloatValue(value)}</div>;
}

export function StringCell({ value }: { value: any }) {
  return <div>{value ? value : ''}</div>;
}

export function getFormattedFloatValue(value: number) {
  let formattedValue = value % 1 === 0 ? value : Number(value.toFixed(2));
  return value >= 0
    ? formattedValue.toLocaleString()
    : `(${Math.abs(formattedValue).toLocaleString()})`;
}
