import { BASE_URL } from "../../../commons/constants";

const SERVICE_URLS = {
  agreementWorkflowService: {
    getAgreementWorkflowData: `${BASE_URL}service/agreementWorkflowService/getExcessDeficitWorkflowDataList`,
    saveCallReport: `${BASE_URL}service/callReportService/emailCallReport`,
    executeWorkflow: `${BASE_URL}comet/workflowExecute`,
    saveWorkflowArtifacts: `${BASE_URL}comet/saveAgreementWorkflowArtifacts`,
  },
  postCollateralService: {
    edSummaryUrl: `${BASE_URL}comet/getEdSummaryForWire`,
    collateralTermUrl: `${BASE_URL}/service/collateralTermService/getCollateralTerms`,
    eligibleInventoryPositionsUrl: `${BASE_URL}/service/physicalCollateralWorkflowManager/getPhysicalCollateralAgreementDataView`,
    bookMmfTrade: `${BASE_URL}comet/bookMmfTrade`,
    bookMmfDialogData: `${BASE_URL}comet/displayBookMmfPopup`
  }
}

export default SERVICE_URLS;
