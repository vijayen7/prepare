import { Select } from 'arc-react-components';
import PropTypes from 'prop-types';
import React from 'react';
const SelectWithoutLabel = (props) => {

  const onChange = (data) => {
    if(props.onChange != undefined){
    props.onChange(data, props.filterName);
    }
  }


  return (
      <Select
        id={props.id}
        key={props.id}
        options={props.options}
        value={props.value}
        disabled={props.disabled}
        readonly={props.readonly}
        optionToKey={(val) => (val && val.key) || null}
        optionToDisplay={(val) => (val && val.value) || null}
        onChange={onChange}
      />
  );
};

SelectWithoutLabel.propTypes = {
  id: PropTypes.string,
  filterName: PropTypes.string,
  options: PropTypes.array,
  value: PropTypes.any,
  disabled: PropTypes.bool,
  readonly: PropTypes.bool,
  onChange: PropTypes.func
};
export default SelectWithoutLabel;
