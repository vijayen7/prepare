import CashCollateralGrid from './CashCollateralGrid'
import SecuritiesCollateralGrid from './SecuritiesCollateralGrid'
import ExposureRequirementPositionsGrid from './ExposureRequirementPositionsGrid'
import UsageGrid from './UsageGrid'

export const AgreementWorkflowComponents = {
  cashCollateralGridComponent: CashCollateralGrid,
  securitiesCollateralGridComponent: SecuritiesCollateralGrid,
  exposureRequirementPositionsGrid: ExposureRequirementPositionsGrid,
  usageGrid: UsageGrid
}
