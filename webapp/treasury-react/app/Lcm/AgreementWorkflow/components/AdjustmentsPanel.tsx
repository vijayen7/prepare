import React from "react"
import { Panel } from "arc-react-components";
import AgreementAdjusment, { AdjustmentData } from "./AgreementAdjustment";

export const AdjustmentsPanel: React.FC<AdjustmentsPanelProps> = (props) => {
  return (
    <>
      <Panel title="Adjustments Impacting Dispute Workflow" collapsible>
        {props.adjustmentDataList.length > 0 &&
          <AgreementAdjusment
            adjustmentDataList={props.adjustmentDataList} />}
      </Panel>
    </>
  )
}

interface AdjustmentsPanelProps {
  adjustmentDataList: AdjustmentData[]
}
