import React from 'react';
import { ExcessDeficitAdditionalSummary } from './ExcessDeficitAdditionalSummary';
import { ExcessDeficitDataSummary } from './ExcessDeficitDataSummary';
import { Panel, TabPanel } from 'arc-react-components';
import { AgreementDetail } from '../models';

export const AgreementDetailPanel: React.FC<AgreementDetail> = (props) => {
  return (
    <>
      <Panel title="Excess Deficit Summary" collapsible>
        <TabPanel id="excessDeficitSummaryPanelId">
          <TabPanel.Tab key="dataSummary" tabId="dataSummaryTab" label="Data Summary">
            <div className="lauout--flex fill--width gutter">
              <ExcessDeficitDataSummary
                isSimmApplicable={props.isSimmApplicable}
                internalVsExternalData={props.internalVsExternalData}
              />
            </div>
          </TabPanel.Tab>
          <TabPanel.Tab
            key="additionalSummary"
            tabId="additionalSummaryTab"
            label="Additional Summary"
          >
            <div className="layout--flex fill--width gutter">
              <ExcessDeficitAdditionalSummary
                isSimmApplicable={props.isSimmApplicable}
                internalT1VsT2={props.internalT1VsT2}
                externalT1VsT2={props.externalT1VsT2}
                adjustedIntVsInt={props.adjustedIntVsInt}
              />
            </div>
          </TabPanel.Tab>
        </TabPanel>
      </Panel>
    </>
  );
};
