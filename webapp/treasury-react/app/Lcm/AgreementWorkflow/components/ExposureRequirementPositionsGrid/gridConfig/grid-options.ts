import { getNonDashedDate } from "../../utils";

export const options = (handleAddAdjustment: Function, useAvailableScreenSpaceFlag: boolean | undefined) => {
  return {
    editable: true,
    exportToExcel: true,
    highlightRowOnClick: true,
    enableCellNavigation: true,
    autoHorizontalScrollBar: true,
    dynamicSummaryRow: {
      isSummaryRowVisible: true,
    },
    useAvailableScreenSpace: useAvailableScreenSpaceFlag != undefined ? useAvailableScreenSpaceFlag : true,
    customColumnSelection: true,
    asyncEditorLoading: false,
    autoEdit: true,
    configureColumns: true,
    applyFilteringOnGrid: true,
    enableMultilevelGrouping: {
      groupingControls: true,
      showGroupingTotal: true,
      customGroupingRendering: true,
      isAddGroupingColumn: true,
      initialGrouping: ["calculator"]
    },
    onCellClick: function (args: any) {
      if (args.event.target.classList.contains("icon-add")) {
        let agreementId = args.item.agreementId;
        if (agreementId < 0) {
          agreementId = -1 * agreementId;
        }
        args.event.stopPropagation();
        handleAddAdjustment(
          'EXPOSURE_REQ_GRID',
          agreementId,
          getNonDashedDate(args.item.date),
          args.item.legalEntityId,
          args.item.expCounterPartyId,
          args.item.agreementTypeId,
          args.item.calculator,
          args.item.custodianAccountAbbrev,
          args.item.pnlSpn,
          args.item.custodianAccountId,
          null,
          null,
          args.item.businessUnitId,
          args.item.bookId,
          args.item.reportingCurrency);
      }
    }
  }
};
