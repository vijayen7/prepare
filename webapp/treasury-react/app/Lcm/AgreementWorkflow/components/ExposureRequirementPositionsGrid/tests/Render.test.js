import React from 'react';
import ExposureRequirementPositionsGrid from '../';
import { render } from '@testing-library/react';

jest.mock('arc-grid');

describe('Exposure & Requirement', () => {
  let data, handleAddAdjustment;
  beforeEach(() => {
    data = [];
    for (let i = 0; i < 10; ++i) {
      data.push({
        isin: 'isin',
        cusip: 'cusip',
        sedol: 'sedol',
        local: 'local',
        bloomberg: 'bloomberg',
        legalEntity: 'testLE',
        exposureCounterparty: 'testECPE',
        agreementType: 'testAT',
        reportingCurrency: 'testRC',
        price: i,
        mktValueRC: i,
        exposureRC: i,
        exposureAdjustmentRC: i,
        requirementHairCut: i,
        adjustedRequirementRC: i,
        date: 'testDate'
      });
    }
    handleAddAdjustment = jest.fn();
  });

  test('Exposure & Requirement Grid would render if data is passed', () => {
    const { container } = render(
      <ExposureRequirementPositionsGrid
        exposureRequirementPositionDataList={data}
        handleAddAdjustment={handleAddAdjustment}
      />
    );

    expect(container.querySelectorAll('div').length).toBe(0);
  });
});
