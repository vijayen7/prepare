import React from 'react';
import { ReactArcGrid } from 'arc-grid';
import { columns } from './gridConfig/grid-columns';
import { options } from './gridConfig/grid-options';

const ExposureRequirementPositionsGrid: React.FC<any> = (props: ExposureRequirementPositionsGridPropType) => {
  return (
    <React.Fragment>
      <ReactArcGrid
        style={{ height: '80%' }}
        data={props.exposureRequirementPositionDataList}
        gridId="exposureRequirementGrid"
        columns={columns}
        options={options(props.handleAddAdjustment, props.useAvailableSpace)}
        resizeCanvas={props.resizeCanvasFlag}
      />
    </React.Fragment>
  );
};

interface ExposureRequirementPositionsGridPropType {
  exposureRequirementPositionDataList: any;
  handleAddAdjustment: Function;
  resizeCanvasFlag: boolean;
  useAvailableSpace?: boolean;
}

export default ExposureRequirementPositionsGrid;
