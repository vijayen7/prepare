import React from 'react';
import UsageGrid from '../';
import { render } from '@testing-library/react';

jest.mock('arc-grid');

describe('Usage Grid', () => {
  let data, handleAddAdjustment;
  beforeEach(() => {
    data = [];
    for (let i = 0; i < 10; ++i) {
      data.push({
        businessUnitName: 'testBU',
        instrumentName: 'demoInstrument',
        foType: 'testFO',
        subType: 'subType',
        reportingCurrency: 'testCurrency',
        quantity: i,
        priceRC: i,
        marketValueRC: i,
        marginHaircut: i,
        adjustment: i,
        agreementType: 'testAT',
        bookAbbrev: 'testBook',
        fieldType: 'testFieldType',
        bundleAbbrev: 'testBundleAbbrev',
        calculator: 'calc',
        country: 'US',
        currency: 'testAbbrev',
        date: 'testDate',
        marginRC: i,
        adjustedEntityRequirement: i,
        adjustedEntityUsage: i,
        pnlSpn: 'demoPnlSpn',
        usageRC: i,
        exposureCA: 'expCA',
        exposureCounterparty: 'expCPE',
        lmvInternalRC: i,
        legalEntity: 'testLE',
        marginType: 'testMT',
        market: 'testMarket',
        smvInternalRC: i,
        underlyingValueRC: i
      });
    }
    handleAddAdjustment = jest.fn();
  });

  test('Usage Grid would render if data is passed', () => {
    const { container } = render(
      <UsageGrid bundlePositionDataList={data} handleAddAdjustment={handleAddAdjustment} />
    );
    expect(container.querySelectorAll('div').length).toBe(0);
  });
});
