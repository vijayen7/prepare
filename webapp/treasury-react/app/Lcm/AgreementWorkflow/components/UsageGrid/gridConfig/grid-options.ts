import { getNonDashedDate } from "../../utils";

export const options = (handleAddAdjustment: Function, useAvailableScreenSpaceFlag: boolean | undefined) => {
  return {
    editable: true,
    exportToExcel: true,
    highlightRowOnClick: true,
    enableCellNavigation: true,
    autoHorizontalScrollBar: true,
    useAvailableScreenSpace: useAvailableScreenSpaceFlag != undefined ? useAvailableScreenSpaceFlag : true,
    customColumnSelection: true,
    dynamicSummaryRow: {
      isSummaryRowVisible: true,
    },
    asyncEditorLoading: false,
    autoEdit: true,
    configureColumns: true,
    applyFilteringOnGrid: true,
    enableMultilevelGrouping: {
      groupingControls: true,
      showGroupingTotal: true,
      customGroupingRendering: true,
      isAddGroupingColumn: true,
      initialGrouping: ["businessUnitName"]
    },
    onCellClick: function (args: any) {
      if (args.event.target.classList.contains("icon-add")) {
        let agreementId = args.item.agreementId;
        if (agreementId < 0) {
          agreementId = -1 * agreementId;
        }
        args.event.stopPropagation();
        handleAddAdjustment(
          'USAGE_GRID',
          agreementId,
          getNonDashedDate(args.item.date),
          args.item.legalEntityId,
          args.item.expCounterPartyId,
          args.item.agreementTypeId,
          args.item.calculator,
          args.item.custodianAccountAbbrev,
          args.item.pnlSpn,
          args.item.custodianAccountId,
          null,
          null,
          null,
          null,
          args.item.reportingCurrency
        );
      }
    }
  }
};
