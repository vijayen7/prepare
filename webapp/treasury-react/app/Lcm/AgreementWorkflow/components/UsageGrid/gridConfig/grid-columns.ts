
import {
  numberFormatter,
  textFormatter,
  groupFormatter,
  priceFormatter,
  linkExcelFormatter
} from '../../../../../commons/grid/formatters';
import { roaAggregator, dGridsumAggregator } from '../../../../../commons/grid/aggregators';
import { adjustmentFormatter } from './../../utils';

export const columns = [
  {
    id: "businessUnitName",
    name: "Bussiness Unit",
    field: "businessUnitName",
    toolTip: "Bussiness Unit",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "instrumentName",
    name: "Instrument Name",
    field: "instrumentName",
    toolTip: "Instrument Name",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 120
  },{
    id: "shortDescription",
    name: "Short Description",
    field: "shortDescription",
    toolTip: "Short Description",
    formatter: textFormatter,
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 120
  },
  {
    id: "foType",
    name: "FO Type",
    field: "foType",
    toolTip: "FO Type",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 60
  },
  {
    id: "subType",
    name: "Subtype",
    field: "subType",
    toolTip: "Subtype",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 80
  }, {
    id: "reportingCurrency",
    name: "Reporting Currency",
    field: "reportingCurrency",
    toolTip: "Reporting Currency",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 40
  }, {
    id: "quantity",
    name: "Quantity",
    field: "quantity",
    toolTip: "Quantity",
    type: "number",
    formatter: numberFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "price",
    name: "Price",
    field: "priceRC",
    toolTip: "Price",
    type: "number",
    formatter: priceFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "marketValueRC",
    name: "Market Value(RC)",
    field: "marketValueRC",
    toolTip: "Market Value(RC)",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "marginHaircut",
    name: "Requirement Hair Cut",
    field: "marginHaircut",
    toolTip: "Requirement Hair Cut",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 120
  },
  {
    id: "adjustment",
    name: "Adjustment",
    field: "adjustment",
    formatter: adjustmentFormatter,
    toolTip: "Adjustment",
    type: "text",
    filter: true,
    excelDataFormatter: linkExcelFormatter,
    headerCssClass: "b",
    width: 60
  },
  {
    id: "agreementType",
    name: "Agreement Type",
    field: "agreementType",
    type: "text",
    toolTip: "Agreement Type",
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "bookAbbrev",
    name: "Book",
    field: "bookAbbrev",
    type: "text",
    toolTip: "Book",
    filter: true,
    headerCssClass: "b",
    width: 80
  }, {
    id: "fieldType",
    name: "Field Type",
    field: "fieldType",
    toolTip: "Field Type",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "bundleAbbrev",
    name: "Bundle",
    field: "bundleAbbrev",
    type: "text",
    toolTip: "Bundle",
    filter: true,
    headerCssClass: "b",
    width: 80
  }, {
    id: "calculator",
    name: "Calculator",
    field: "calculator",
    type: "text",
    toolTip: "Calculator",
    filter: true,
    headerCssClass: "b",
    width: 120
  }, {
    id: "country",
    name: "Country",
    field: "country",
    type: "text",
    toolTip: "Country",
    filter: true,
    headerCssClass: "b",
    width: 80
  }, {
    id: "currency",
    name: "Currency",
    field: "currency",
    type: "text",
    toolTip: "Currency",
    filter: true,
    headerCssClass: "b",
    width: 30
  }, {
    id: "date",
    name: "Date",
    field: "date",
    type: "text",
    toolTip: "Date",
    filter: true,
    headerCssClass: "b",
    width: 100
  }, {
    id: "marginRC",
    name: "Entity Requirement",
    field: "marginRC",
    toolTip: "Entity Requirement",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 120
  }, {
    id: "adjustedEntityRequirement",
    name: "Adjusted Entity Requirement",
    field: "adjustedEntityRequirement",
    toolTip: "Adjusted Entity Requirement",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 120
  }, {
    id: "adjustedEntityUsage",
    name: "Adjusted Entity Usage",
    field: "adjustedEntityUsage",
    toolTip: "Adjusted Entity Usage",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 120
  }, {
    id: "pnlSpn",
    name: "PNL SPN",
    field: "pnlSpn",
    toolTip: "PNL SPN",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "usageRC",
    name: "Entity Usage",
    field: "usageRC",
    toolTip: "Entity Usage",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "exposureCA",
    name: "Exposure CA",
    field: "exposureCA",
    toolTip: "Exposure CA",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 100
  }, {
    id: "exposureCounterparty",
    name: "Exposure Counterparty",
    field: "exposureCounterparty",
    type: "text",
    toolTip: "Exposure Counterparty",
    filter: true,
    headerCssClass: "b",
    width: 120
  }, {
    id: "lmvInternal",
    name: "LMV",
    field: "lmvInternalRC",
    type: "number",
    toolTip: "LMV",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "legalEntity",
    name: "Legal Entity",
    field: "legalEntity",
    type: "text",
    toolTip: "Legal Entity",
    filter: true,
    headerCssClass: "b",
    width: 120
  }, {
    id: "marginType",
    name: "Margin Type",
    field: "marginType",
    type: "text",
    toolTip: "Margin Type",
    filter: true,
    headerCssClass: "b",
    width: 80
  }, {
    id: "market",
    name: "Market",
    field: "market",
    type: "text",
    toolTip: "Market",
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "smvInternal",
    name: "SMV",
    field: "smvInternalRC",
    type: "number",
    toolTip: "SMV",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "underlyingValue",
    name: "Underlying Value",
    field: "underlyingValueRC",
    type: "number",
    toolTip: "Underlying Value",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }];
