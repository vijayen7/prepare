import React from 'react';
import { ReactArcGrid } from 'arc-grid';
import { columns } from './gridConfig/grid-columns';
import { options } from './gridConfig/grid-options';

const UsageGrid: React.FC<any> = (props: UsageGridPropType) => {
  return (
    <React.Fragment>
      <ReactArcGrid
        style={{ height: '80%' }}
        data={props.bundlePositionDataList}
        gridId="usageGrid"
        columns={columns}
        options={options(props.handleAddAdjustment, props.useAvailableSpace)}
        resizeCanvas={props.resizeCanvasFlag}
      />
    </React.Fragment>
  );
};

interface UsageGridPropType {
  bundlePositionDataList: BundlePositionData[];
  handleAddAdjustment: Function;
  resizeCanvasFlag: boolean;
  useAvailableSpace?: boolean;
}

export interface BundlePositionData {
  businessUnitName?: string;
  instrumentName?: string;
  shortDescription?: string;
  foType?: string;
  subType?: string;
  reportingCurrency?: string;
  quantity?: number;
  priceRC?: number;
  marketValueRC?: number;
  marginHaircut?: number;
  adjustment?: number;
  agreementType?: string;
  bookAbbrev?: string;
  fieldType?: string;
  bundleAbbrev?: string;
  calculator?: string;
  country?: string;
  currency?: string;
  date?: string;
  marginRC?: number;
  adjustedEntityRequirement?: number;
  adjustedEntityUsage?: number;
  pnlSpn: string;
  usageRC?: number;
  exposureCA?: string;
  exposureCounterparty?: string;
  lmvInternalRC?: number;
  legalEntity: string;
  marginType?: string;
  market?: string;
  smvInternalRC?: number;
  underlyingValueRC?: number;
}

export default UsageGrid;
