import React from "react"
import { Dialog, Button } from 'arc-react-components';

export const BookMmfPopup: React.FC<any> = (props) => {
  const [mmfSpn, setMmfSpn] = React.useState(props.mmfSpn);
  return props.segCustodianAccountName && props.description && props.amount ? (
    <Dialog
      isOpen={props.isOpen}
      onClose={props.onClose}
      title={'COMET - Book mmf trades wires'}
      style={{
        width: "40%",
      }}
      footer={
        <React.Fragment>
          <Button onClick={() => props.onClickBookTrade(mmfSpn)}>Book Trades</Button>
          <Button onClick={() => onCancel(props.onClose)}>Cancel</Button>
        </React.Fragment>
      }
    >
      <div className="form-field--split">
        <label>Seg IA Custodian Account</label>
        <div className="form-field__input">
          <input
            type="text"
            readOnly
            id="segCustodianAccountName"
            value={props.segCustodianAccountName}
          />
        </div>
      </div>
      <div className="form-field--split">
        <label>MMF SPN</label>
        <div className="form-field__input">
          <input
            type="text"
            id="mmfSpn"
            onChange={(e) => setMmfSpn(e.target.value)}
            value={mmfSpn}
          />
        </div>
      </div>
      <div className="form-field--split">
        <label>Description</label>
        <div className="form-field__input">
          <input type="text" readOnly id="description" value={props.description} />
        </div>
      </div>
      <div className="form-field--split">
        <label>Amount</label>
        <div className="form-field__input">
          <input type="text" readOnly id="amount" value={props.amount} />
        </div>
      </div>
    </Dialog>
  ) : null;
};

const onCancel = (onClose: Function) => {
  if (confirm("Do you really want to close popup?") == true) {
    onClose()
  }
}
