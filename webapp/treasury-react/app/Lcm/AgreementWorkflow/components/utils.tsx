import React from "react"
import moment from "moment";
import { ExcessDeficitData } from "../models";
import { validateAndGetRoundedValue } from "./TopPanelGrid/utils";

export const adjustmentFormatter = (): string => {
  return '<div align="center"><a title="Add Adjustment"><i class="icon-add"></i></a></div>';
};

export const linkExcelFormatter = (x: unknown, cell: any): void => {
  cell.actions = "";
};

export const getNonDashedDate = (dateString: string) => {
  let date = moment(dateString, "YYYY-MM-DD");
  if (date.isValid()) {
    return date.format("YYYYMMDD");
  }
};

export const getExcessDeficitDataRow = (
  excessDeficitData: ExcessDeficitData,
  rowName: string
): any => {
  return (
    <tr>
      <td style={{ textAlign: "right" }}>{rowName}</td>
      {displayExcessDeficitDataRow(excessDeficitData.exposure)}
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.excessDeficit)}
    </tr>
  );
};

export const displayExcessDeficitDataRow = (
  excessDeficitDataCellValue?: number
): any => {
  return (
    <td style={{ textAlign: "right" }}>
      {validateAndGetRoundedValue(excessDeficitDataCellValue)}
    </td>
  );
};
