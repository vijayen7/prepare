import ArcDataGrid from 'arc-data-grid';
import gridConfig from './Grid/gridConfig'
import React, { useState } from 'react';
import TopPanelGridPropType from './types/TopPanelGridPropType'
import getTopPanelGridColumns from './Grid/gridColumns'


const TopPanelGrid: React.FC<TopPanelGridPropType> = (props: TopPanelGridPropType) => {

  const isPopulateAsm = props.topPanelDataList.find((e) => e.isPopulateAsm === true) != undefined;

  const DEFAULT_PINNED_COLUMNS: any = React.useMemo(() => {
    return { actions: true, tag: true };
  }, []);

  const [sortBy, setSortBy] = React.useState([
    {
      identity: "tag",
      desc: false,
    },
  ]);

  const onSortByChange = (sortBy) => {
    setSortBy(sortBy);
  };

  const [updatedRow, setUpdatedRow] = React.useState();

  const onUpdatedRowChange = (row) => {
    setUpdatedRow(row);
  };


  const [pinnedColumns, setPinnedColumns] = useState(DEFAULT_PINNED_COLUMNS);
  const columns = React.useMemo(() => getTopPanelGridColumns(props, isPopulateAsm, onUpdatedRowChange), [props, isPopulateAsm]);
  const [clickedRow, setClickedRow] = React.useState<null | number>(null);

  return (
    <React.Fragment>
      <div onMouseLeave={() => { updatedRow ? props.setLogCallActions(updatedRow , true) : undefined }}>
        <ArcDataGrid
          rows={props.topPanelDataList}
          columns={columns}
          configurations={gridConfig}
          pinnedColumns={pinnedColumns}
          onPinnedColumnsChange={setPinnedColumns}
          clickedRow={clickedRow}
          sortBy={sortBy}
          onSortByChange={onSortByChange}
          onClickedRowChange={(row) => {
            setClickedRow(row)
            if (row != null) {
              props.populatePage(row);
            }
          }}
        />
      </div>
    </React.Fragment>
  );
};

export default TopPanelGrid;
