import { numberFormatter, formatNumberUsingCommas } from '../../../../commons/grid/formatters'
export function actionsFormatter(row, cell, value, columnnDef, dataContext) {
  let actions = "";
  let isInternalDataMissing = dataContext.isInternalDataMissing

  if (
    dataContext.agreementType != undefined &&
    dataContext.agreementType != "MNA" &&
    dataContext.agreementType != "SEG" && !isInternalDataMissing
  ) {
    actions = actions.concat(
      '<a title="Add Adjustment"   ><i class="icon-add"></i></a>'
    );
  }

  if (dataContext.isActionableAgreement) {
    actions = actions.concat(
      '<a title="Post Collateral"  ><i class="icon-transfer margin--left"></i></a>'
    );
    if (dataContext.agreementType != "SEG" && !isInternalDataMissing) {
      actions = actions.concat(
        ' <a title="Email Call Report"  ><i class="icon-mail margin--left"></i></a>'
      );
    }
  }
  if (dataContext.brokerFileUrlsByLevel != undefined && dataContext.brokerFileUrlsByLevel != '') {
    actions = actions.concat(_brokerFilesFormatter(dataContext));
    actions = actions.concat(' <a title="Download Broker File"  ><i class="icon-download margin--left"></i></a>');
  }
  if (dataContext.crifFileUrl != undefined && dataContext.crifFileUrl != '') {
    actions = actions.concat(' <a href="#" title="Download Crif File"  ><i class="icon-cloud--download margin--left"></i></a>');
  }
  return actions;
}

function _brokerFilesFormatter(dataContext) {
  var options = ""
  for (var i = 0; i < dataContext.brokerFileUrlsByLevel.length; i++) {
    var brokerFile = dataContext.brokerFileUrlsByLevel[i];
    options += "<OPTION value='" + brokerFile[0] + "'>" + brokerFile[1] + "</OPTION>";
  }
  return '<select id="selectBrokerFile' + dataContext.agreementId + '">' +
    options +
    '</select>';
}

export function agreementNameFormatter(
  row,
  cell,
  value,
  columnnDef,
  dataContext
) {
  var agreementName = '<a>' + dataContext.agreementName + "</a>";
  return agreementName;
}

export function applicableColumnIdentifier(
  row,
  cell,
  value,
  columnnDef,
  dataContext
) {
  if (cell == 1) {
    if (dataContext.tag == "REG") {
      return '<span class="token red">' + value + "</span>";
    } else {
      return '<span class="token gray2">' + value + "</span>";
    }
  }
}

export function acmColumnFormatter(row, cell, value, columnnDef, dataContext) {
  if (dataContext.acmRC === undefined) {
    dataContext.acmRC = "";
  }

  if (dataContext.isActionableAgreement) {
    if (value === undefined || value == "") {
      return '<div style="background-color:white">Enter ACM</div>';
    } else {
      return (
        '<div style="background-color:white">' +
        numberFormatter(row, cell, value, columnnDef, dataContext) +
        "</div>"
      );
    }
  } else {
    dataContext.acmRC = "";
  }

}

export function asmColumnFormatter(row, cell, value, columnnDef, dataContext) {
  if (dataContext.asmRC === undefined) {
    dataContext.asmRC = "";
  }

  if (dataContext.isActionableAgreement) {
    if (value === undefined || value == "") {
      return '<div style="background-color:white">Enter ASM</div>';
    } else {
      return (
        '<div style="background-color:white">' +
        numberFormatter(row, cell, value, columnnDef, dataContext) +
        "</div>"
      );
    }
  } else {
    dataContext.asmRC = "";
  }
}

export function disputeAmountColumnFormatter(row, cell, value, columnnDef, dataContext) {
  if (dataContext.isActionableAgreement) {
    if (value === undefined || value == "") {
      return '<div style="background-color:white">Enter dispute amount</div>';
    } else {
      return (
        '<div style="background-color:white">' +
        numberFormatter(row, cell, value, columnnDef, dataContext) +
        "</div>"
      );
    }
  } else {
    dataContext.disputeAmountRC = "";
  }
}

export const validateAndGetRoundedValue = (value, defaultText = "n/a") => {
  if (isValidNumber(value)) {
    value = Math.round(parseFloat(value));
    var str = formatNumberUsingCommas(value);
    if (value < 0) {
      return "(" + str.replace(/-/, "") + ")";
    } else {
      return str;
    }
  }
  return defaultText;
};

export const isValidNumber = (value) => {
  return value !== undefined && !isNaN(parseFloat(value));
};

export function ptgBookingReferenceFormatter(row, cell, value, columnnDef, dataContext) {
  let url = CODEX_PROPERTIES["com.arcesium.treasury.ptg.url"] + "/searchInstructions?instructionGroupId=" + value
  return value? '<a href="' + url + '" target="_blank">' + value + '</a>': '';
}
