
import ArcDataGrid from 'arc-data-grid';
import TopPanelData from '../types/TopPanelData'
import { ToolTipCell, AsmColumnCell, ActionPanel, TagCell, AgreementNameCell, BookingReferenceCell, AcmColumnCell, DisputeAmountCell } from './CustomCells'
import { plainNumberFormatter } from '../../../../../commons/grid/formatters'
import { StringCell } from '../../../commons/GridCells'
import React from 'react';
import { MARGINCALLTYPE_RANK } from '../constants'

export default function getTopPanelGridColumns(props: any, isPopulateAsm: boolean, onUpdatedRowChange: Function): ArcDataGrid.ColumnProperties<TopPanelData, number>[] {


  const asmColumn = {

    identity: 'asmRC',
    header: 'ASM',
    Cell: (value: any, context: any) => {
      return <ToolTipCell toolTip="Actual Securities Movement"
        value={<AsmColumnCell
          context={context}
          value={value}
          props={props}
          onUpdatedRowChange={onUpdatedRowChange} />} />
    },
    width: 60,
    type: 'number',

  }

  const columns: any = [
    {
      identity: 'actions',
      Cell: (_, context) => <ActionPanel context={context} props={props} />,
      disableFilters: true,
      disableSearches: true,
      disableSortBy: true,
      disableExport: true,
      disableGroupBy: true,
      disableAggregation: true,
      width: 150,
    },
    {
      identity: 'tag',
      header: 'Tags',
      Cell: (value: any, context: any) => <TagCell context={context} value={value} />,
      width: 60,
      sortComparator: (a, b) => {
        let rankA = MARGINCALLTYPE_RANK[a];
        let rankB = MARGINCALLTYPE_RANK[b];
        return rankA === rankB ? 0 : rankA > rankB ? 1 : -1;
      },
    },
    {
      identity: 'accountType',
      header: 'Account Type',
      Cell: (value: any) => <StringCell value={value} />,
      width: 60,
    },
    {
      identity: 'agreementName',
      header: 'Agreement Name',
      Cell: (_, context: any) => <AgreementNameCell context={context} onClickCallback={props.onclickAgreementName} />,
      width: 60,
    },
    {
      identity: 'reportingCurrency',
      header: 'Reporting Currency',
      width: 40,
    },
    {
      identity: 'internalEDRC',
      header: 'Internal E/D',
      Cell: (value: any) => <ToolTipCell value={plainNumberFormatter(null, null, value)} toolTip="E/D" />,
      type: 'number',
      width: 60,
    },
    {
      identity: 'brokerExcessDeficitRC',
      header: 'External E/D',
      Cell: (value: any) => <ToolTipCell value={plainNumberFormatter(null, null, value)} toolTip="Ext E/D" />,
      type: 'number',
      width: 60,
    },
    {
      identity: 'ecmRC',
      header: 'ECM',
      Cell: (value: any) => <ToolTipCell value={plainNumberFormatter(null, null, value)} toolTip="Expected Cash Movement" />,
      type: 'number',
      width: 60,
    },
    {
      identity: 'acmRC',
      header: 'ACM',
      Cell: (value: any, context: any) => {
        return <ToolTipCell toolTip="Actual Cash Movement"
          value={
            <AcmColumnCell
              context={context}
              value={value}
              props={props}
              onUpdatedRowChange={onUpdatedRowChange} />} />
      },
      width: 60,
      type: 'number',
    },
    {
      identity: 'disputeAmountRC',
      header: 'Dispute Amount',
      Cell: (value: any, context: any) => {
        return <ToolTipCell toolTip="Dispute Amount"
          value={
            <DisputeAmountCell
              context={context}
              value={value}
              props={props}
              onUpdatedRowChange={onUpdatedRowChange} />} />
      },
      width: 60,
      type: 'number',
    },
    {
      identity: 'wireRequest',
      header: 'Wire Request',
      Cell: (value: any) => <StringCell value={value} />,
      width: 60,
      minWidth: 40,
    },
    {
      identity: 'bookingReference',
      header: 'Booking Reference',
      Cell: (value: any) => <BookingReferenceCell value={value} />,
      width: 60,
      minWidth: 40,
    },
  ]




  if (isPopulateAsm) {
    columns.splice(7, 0, asmColumn);
    return columns;
  }
  return columns;
}


