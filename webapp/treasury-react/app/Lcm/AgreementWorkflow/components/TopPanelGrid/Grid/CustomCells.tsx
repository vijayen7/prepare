import SelectWithoutLabel from '../../../commons/SelectWithoutLabel'
import TopPanelData from '../types/TopPanelData'
import TextBox from "../../../commons/TextBox";
import React from 'react';
import { plainNumberFormatter } from '../../../../../commons/grid/formatters'


export function ActionPanel({ context, props }) {


  function addAdjustmentClickHandler(row) {

    var agreementId = row.agreementId;
    if (agreementId < 0) {
      agreementId = -1 * agreementId;
    }

    props.handleAddAdjustment(
      "Agreement",
      agreementId,
      row.legalEntityId,
      row.exposureCounterPartyId,
      row.agreementTypeId,
      row.agreementType,
      row.reportingCurrency
    );
  }

  function postCollateralClickHandler(row) {
    var agreementId = row.agreementId;
    if (agreementId < 0) {
      agreementId = -1 * agreementId;
    }
    props.onClickPostCollateral(row, agreementId);
  }

  function callReportClickHandler(row) {
    var agreementId = row.agreementId;
    props.quickCallReport(agreementId);
  }


  function downloadBrokerFileClickHandler(row) {
    let brokerFile: any = document.getElementById('selectBrokerFile' + row.agreementId);
    if (brokerFile?.value) {
      let idx = Number(brokerFile.value);
      var bFile = row.brokerFileUrlsByLevel[idx]
      let fileId = bFile[0]
      props.downloadBrokerFile(fileId);
    }
  }


  function brokerFilesSelect(row) {
    let brokerFields: { key: any, value: any }[] = [];
    for (var i = 0; i < row.brokerFileUrlsByLevel.length; i++) {
      var brokerFile = row.brokerFileUrlsByLevel[i];
      brokerFields.push({ key: String(i), value: brokerFile[1] });
    }

    return (<SelectWithoutLabel
      id={`selectBrokerFile${row.agreementId}`}
      options={brokerFields}
      value={brokerFields[0].value}
    />
    );
  }



  let row = context.row;
  let isInternalDataMissing = row.isInternalDataMissing

  let fields: JSX.Element[] = [];

  if (
    row.agreementType != undefined &&
    row.agreementType != "MNA" &&
    row.agreementType != "SEG" && !isInternalDataMissing
  ) {
    fields.push(<a title="Add Adjustment"   ><i className="icon-add" onClick={() => { addAdjustmentClickHandler(row) }}></i></a>)
  }

  if (row.isActionableAgreement) {
    fields.push(<a title="Post Collateral"  ><i className="icon-transfer margin--left" onClick={() => { postCollateralClickHandler(row) }}></i></a>)

    if (row.agreementType != "SEG" && !isInternalDataMissing) {
      fields.push(<a title="Email Call Report"  ><i className="icon-mail margin--left" onClick={() => { callReportClickHandler(row) }}></i></a>)
    }
  }


  if (row.brokerFileUrlsByLevel != undefined && row.brokerFileUrlsByLevel != '') {
    fields.push(brokerFilesSelect(row))
    fields.push(<a title="Download Broker File"  ><i className="icon-download margin--left" onClick={() => { downloadBrokerFileClickHandler(row) }}></i></a>);
  }
  if (row.crifFileUrl != undefined && row.crifFileUrl != '') {
    fields.push(<a href="#" title="Download Crif File"  ><i className="icon-cloud--download margin--left" onClick={() => { props.downloadCrifFile(row.crifFileUrl); }}></i></a>);
  }
  return (
    <>
      {fields}
    </>
  )
}



const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

export function BookingReferenceCell({ value }) {
  let url = CODEX_PROPERTIES["com.arcesium.treasury.ptg.url"] + "/searchInstructions?instructionGroupId=" + value
  return value ? <a href={url} target="_blank"> {value} </a> : null;
}

export function ToolTipCell({ value, toolTip }) {
  return (
    <div style={{ display: 'inline' }} title={toolTip}> {value} </div>
  );
}

export function AsmColumnCell({ context, value, props, onUpdatedRowChange }) {

  if (context.row.asmRC === undefined) {
    context.row.asmRC = "";
  }

  if (context.row.isActionableAgreement && context.row.isPopulateAsm) {
    return (
      <TextBox value={value}
        defaultValue="Enter ASM"
        onChange={(val) => {
          val = val.replace(/\(|\)|,/g, '');
          context.row.asmRC = Number(val);
          onUpdatedRowChange(context.row);
        }}
        onFocusOut={(val) => {
          if (val != context.row.asmRC) {
            val = val.replace(/\(|\)|,/g, '');
            context.row.asmRC = Number(val);
          }
          props.setLogCallActions(context.row);
        }}
        onClick={() => {
          onUpdatedRowChange(context.row);
        }}
      />
    );
  }

  context.row.asmRC = "";
  return (
    null
  );
}

export function AcmColumnCell({ context, value, props, onUpdatedRowChange }) {

  if (context.row.acmRC === undefined) {
    context.row.acmRC = "";
  }

  if (context.row.isActionableAgreement) {
    return (
      <TextBox
        value={value}
        defaultValue="Enter ACM"
        onChange={(val) => {
          val = val.replace(/\(|\)|,/g, '');
          context.row.acmRC = Number(val);
          onUpdatedRowChange(context.row);
        }}
        onFocusOut={(val) => {
          if (val != context.row.acmRC) {
            val = val.replace(/\(|\)|,/g, '');
            context.row.acmRC = Number(val);
          }
          props.setLogCallActions(context.row);
        }}
        onClick={() => {
          onUpdatedRowChange(context.row);
        }}
      />
    );
  }

  context.row.acmRC = "";
  return (
    null
  );
}

export function DisputeAmountCell({ context, value, props, onUpdatedRowChange }) {
  if (context.row.isActionableAgreement) {
    return (
      <TextBox value={value}
        defaultValue="Enter dispute amount"
        onChange={(val) => {
          val = val.replace(/\(|\)|,/g, '');
          context.row.disputeAmountRC = Number(val);
          onUpdatedRowChange(context.row);
        }}
        onFocusOut={(val) => {
          if (val != context.row.disputeAmountRC) {
            val = val.replace(/\(|\)|,/g, '');
            context.row.disputeAmountRC = Number(val);
          }
          props.setLogCallActions(context.row);
        }}
        onClick={() => {
          onUpdatedRowChange(context.row);
        }}
      />
    );
  }
  context.row.disputeAmountRC = "";
  return (
    null
  )
}

export function TagCell({ context, value }) {
  const row: TopPanelData = context.row;
  if (row.tag == "REG") {
    return <span className="token red"> {value} </span>;
  }
  return <span className="token gray2"> {value} </span>;
}

export function AgreementNameCell({ context, onClickCallback }) {
  const row: TopPanelData = context.row;
  return <div style={{ flex: 1 }} onClick={() => { onClickCallback(context.row) }}><a>{row.agreementName}</a></div>;
}
