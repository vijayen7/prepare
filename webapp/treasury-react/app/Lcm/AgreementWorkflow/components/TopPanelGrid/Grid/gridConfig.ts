const gridConfig = {
  initialExpandLevel: 0,
  disableSearches: true,
  disableExport: true,
  clickableRows: true,
  disableGroupBy: true,
  disableAggregation: true,
  enableTreeView: true,

}

export default gridConfig
