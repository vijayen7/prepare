import TopPanelData from './TopPanelData'

export default interface TopPanelGridPropType {
  topPanelDataList: TopPanelData[];
  handleAddAdjustment: Function;
  downloadBrokerFile: Function;
  downloadCrifFile: Function;
  onClickPostCollateral: Function;
  quickCallReport: Function;
  onclickAgreementName: Function;
  populatePage: Function;
  setLogCallActions: Function;
}
