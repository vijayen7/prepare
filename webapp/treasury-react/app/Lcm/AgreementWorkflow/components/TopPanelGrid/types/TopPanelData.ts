export default interface TopPanelData {
  actions?: string;
  tag?: string;
  agreementName?: string;
  reportingCurrency?: string;
  internalED?: number;
  brokerExcessDeficit?: number;
  asm?: number;
  ecm?: number;
  acm?: string;
  disputeAmount?: number;
  wireRequest?: string;
  bookingReference?: string;
  isPopulateAsm?: boolean;
}
