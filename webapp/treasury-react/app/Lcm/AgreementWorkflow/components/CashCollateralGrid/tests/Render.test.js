import React from 'react';
import CashCollateralGrid from '../';
import { render } from '@testing-library/react';

jest.mock('arc-grid');

describe('Cash Collateral Grid', () => {
  let data, handleAddAdjustment;
  beforeEach(() => {
    data = [];
    for (let i = 0; i < 10; ++i) {
      data.push({
        custodianAccountAbbrev: 'testCA',
        legalEntity: 'testLE',
        exposureCounterparty: 'testECPE',
        agreementType: 'testAT',
        reportingCurrency: 'testRC',
        cashCollateralRC: i,
        adjustmentAmountRC: i,
        adjustedEntityCashCollateralRC: i,
        brokerCollateralTreasuryRC: i,
        collateralDifferenceRC: i,
        date: 'testDate'
      });
    }
    handleAddAdjustment = jest.fn();
  });

  test('Cash Collateral Grid would render if data is passed', () => {
    const { container } = render(
      <CashCollateralGrid cashCollateralDataList={data} handleAddAdjustment={handleAddAdjustment} />
    );

    expect(container.querySelectorAll('div').length).toBe(0);
  });
});
