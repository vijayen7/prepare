import { numberFormatter } from '../../../../../commons/grid/formatters';
import { dGridsumAggregator } from '../../../../../commons/grid/aggregators';
import { adjustmentFormatter, linkExcelFormatter } from './../../utils';

export const columns = [
  {
    id: 'custodianAccountAbbrev',
    name: 'Custodian Account',
    field: 'custodianAccountAbbrev',
    toolTip: 'Custodian Account',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    width: 100
  },
  {
    id: 'legalEntity',
    name: 'Legal Entity',
    field: 'legalEntity',
    toolTip: 'Legal Entity',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    width: 120
  },
  {
    id: 'exposureCounterparty',
    name: 'Exposure Counterparty',
    field: 'exposureCounterparty',
    toolTip: 'Exposure Counterparty',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    width: 120
  },
  {
    id: 'agreementType',
    name: 'Agreement Type',
    field: 'agreementType',
    toolTip: 'Agreement Type',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    width: 100
  },
  {
    id: 'reportingCurrency',
    name: 'Reporting Currency',
    field: 'reportingCurrency',
    toolTip: 'Reporting Currency',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    width: 40
  },
  {
    id: 'collateral',
    name: 'Entity Collateral',
    field: 'cashCollateralRC',
    toolTip: 'Entity Collateral',
    type: 'number',
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0',
    width: 100
  },
  {
    id: 'adjustmentAmount',
    name: 'Adjustment Amount',
    field: 'adjustmentAmountRC',
    toolTip: 'Adjustment Amount',
    type: 'number',
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0',
    width: 100
  },
  {
    id: 'adjustedEntityCollateral',
    name: 'Adjusted Entity Collateral',
    field: 'adjustedEntityCashCollateralRC',
    toolTip: 'Adjusted Entity Collateral',
    type: 'number',
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0',
    width: 100
  },
  {
    id: 'brokerCollateralTreasury',
    name: 'Broker Collateral(Treasury)',
    field: 'brokerCollateralTreasuryRC',
    toolTip: 'Broker Collateral(Treasury)',
    type: 'number',
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0',
    width: 120
  },
  {
    id: 'collateralDifference',
    name: 'Collateral Difference',
    field: 'collateralDifferenceRC',
    toolTip: 'Collateral Difference',
    type: 'number',
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0',
    width: 100
  },
  {
    id: 'adjustment',
    name: 'Adjustment',
    field: 'adjustment',
    formatter: adjustmentFormatter,
    toolTip: 'Adjustment',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    excelDataFormatter: linkExcelFormatter,
    width: 60
  },
  {
    id: 'date',
    name: 'Date',
    field: 'date',
    toolTip: 'Date',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    width: 80
  }
];
