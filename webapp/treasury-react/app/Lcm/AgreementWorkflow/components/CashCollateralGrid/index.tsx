import React from 'react';
import { ReactArcGrid } from 'arc-grid';
import { columns } from './gridConfig/grid-columns';
import { options } from './gridConfig/grid-options';

const CashCollateralGrid: React.FC<any> = (props: CashCollateralGridPropType) => {
  return (
    <React.Fragment>
      <ReactArcGrid
        style={{ height: '80%' }}
        data={props.cashCollateralDataList}
        gridId="cashCollateralGrid"
        columns={columns}
        options={options(props.handleAddAdjustment, props.useAvailableSpace)}
        resizeCanvas={props.resizeCanvasFlag}
      />
    </React.Fragment>
  );
};

interface CashCollateralGridPropType {
  cashCollateralDataList: CashCollateralData[];
  handleAddAdjustment: Function;
  resizeCanvasFlag: boolean;
  useAvailableSpace?: boolean;
}

export interface CashCollateralData {
  custodianAccountAbbrev: string;
  legalEntity: string;
  exposureCounterparty: string;
  agreementType: string;
  reportingCurrency?: string;
  cashCollateralRC?: number;
  adjustmentAmountRC?: number;
  adjustedEntityCashCollateralRC?: number;
  brokerCollateralTreasuryRC?: number;
  collateralDifferenceRC?: number;
  date: string;
}

export default CashCollateralGrid;
