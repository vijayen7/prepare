import { numberFormatter, textFormatter, groupFormatter, priceFormatter } from '../../../../../commons/grid/formatters';
import { roaAggregator, dGridsumAggregator } from '../../../../../commons/grid/aggregators';
import { adjustmentFormatter, linkExcelFormatter } from './../../utils';

export const columns = [
  {
    id: "pnlSpn",
    name: "PNL SPN",
    field: "pnlSpn",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "isin",
    name: "ISIN",
    field: "isin",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "cusip",
    name: "Cusip",
    field: "cusip",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "sedol",
    name: "Sedol",
    field: "sedol",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "local",
    name: "Local",
    field: "local",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "legalEntity",
    name: "Legal Entity",
    field: "legalEntity",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 150
  }, {
    id: "exposureCounterparty",
    name: "Exposure Counterparty",
    field: "exposureCounterparty",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 120
  }, {
    id: "agreementType",
    name: "Agreement Type",
    field: "agreementType",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 80
  }, {
    id: "bookAbbrev",
    name: "Book",
    field: "bookAbbrev",
    type: "text",
    toolTip: "Book",
    filter: true,
    headerCssClass: "b",
    width: 80
  }, {
    id: "gboType",
    name: "GBO Type",
    field: "gboType",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 120
  }, {
    id: "currencyAbbrev",
    name: "Currency",
    field: "currencyAbbrev",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 60
  }, {
    id: "custodianAccountAbbrev",
    name: "Custodian Account",
    field: "custodianAccountAbbrev",
    toolTip: "Custodian Account",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 100
  }, {
    id: "exposureCustodianAccountName",
    name: "Exposure Custodian Account",
    field: "exposureCustodianAccountName",
    toolTip: "Exposure Custodian Account",
    type: "text",
    filter: true,
    headerCssClass: "b",
    width: 100
  }, {
    id: "tickUnit",
    name: "Tick Unit",
    field: "tickUnit",
    type: "number",
    toolTip: "tickUnit",
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "haircutPct",
    name: "Haircut (in %)",
    field: "haircutPct",
    type: "number",
    toolTip: "Haircut (in %)",
    filter: true,
    sortable: true,
    formatter: numberFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: '#,##0.0',
    width: 100
  }, {
    id: "priceRC",
    name: "Price(RC)",
    field: "priceRC",
    toolTip: "PriceRC",
    type: "number",
    formatter: priceFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "quantity",
    name: "Quantity",
    field: "quantity",
    type: "number",
    toolTip: "Quantity",
    filter: true,
    sortable: true,
    formatter: numberFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: '#,##0.0',
    width: 100
  }, {
    id: "marketValueRC",
    name: "Market Value(RC)",
    field: "marketValueRC",
    toolTip: "Market Value(RC)",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "securityCollateralRC",
    name: "Security Collateral(RC)",
    field: "securityCollateralRC",
    toolTip: "Security Collateral(RC)",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "securitiesCollateralMarginRC",
    name: "Security Collateral Margin(RC)",
    field: "physicalCollateralMarginRC",
    type: "number",
    toolTip: "Security Collateral Margin(RC)",
    filter: true,
    sortable: true,
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: "aln-rt b",
    excelFormatter: '#,##0',
    width: 150
  }, {
    id: 'adjustment',
    name: 'Adjustment',
    field: 'adjustment',
    formatter: adjustmentFormatter,
    toolTip: 'Adjustment',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    excelDataFormatter: linkExcelFormatter,
    width: 60
  }, {
    id: "securityCollateralAdjustmentRC",
    name: "Security Collateral Adjustment(RC)",
    field: "securityCollateralAdjustmentRC",
    toolTip: "Security Collateral Adjustment(RC)",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "adjustedSecurityCollateralRC",
    name: "Adjusted Security Collateral(RC)",
    field: "adjustedSecurityCollateralRC",
    toolTip: "Adjusted Security Collateral(RC)",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "brokerHaircutPct",
    name: "Broker Haircut (in %)",
    field: "brokerHaircutPct",
    type: "number",
    toolTip: "Broker Haircut (in %)",
    filter: true,
    sortable: true,
    formatter: numberFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: '#,##0.0',
    width: 100
  }, {
    id: "brokerNativePrice",
    name: "Broker Native Price",
    field: "brokerNativePrice",
    toolTip: "Broker Native Price",
    type: "number",
    formatter: priceFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "brokerQuantity",
    name: "Broker Quantity",
    field: "brokerQuantity",
    type: "number",
    toolTip: "Broker Quantity",
    filter: true,
    sortable: true,
    formatter: numberFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: '#,##0.0',
    width: 100
  }, {
    id: "brokerMarketValueRC",
    name: "Broker Market Value(RC)",
    field: "brokerMarketValueRC",
    toolTip: "Broker Market Value(RC)",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "brokerMarketValueUsd",
    name: "Broker Market Value (in USD)",
    field: "brokerMarketValueUsd",
    toolTip: "Broker Market Value (in USD)",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    groupingAggregator: roaAggregator,
    groupTotalsFormatter: groupFormatter,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "brokerSecurityCollateralRC",
    name: "Broker Security Collateral(RC)",
    field: "brokerSecurityCollateralRC",
    toolTip: "Broker Security Collateral(RC)",
    type: "number",
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    width: 100
  }, {
    id: "date",
    name: "Date",
    field: "date",
    toolTip: "Date",
    type: "text",
    formatter: textFormatter,
    filter: true,
    headerCssClass: "b",
    width: 80
  }
];
