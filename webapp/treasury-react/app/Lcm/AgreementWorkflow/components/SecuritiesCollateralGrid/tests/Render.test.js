import React from 'react';
import SecuritiesCollateralGrid from '../';
import { render } from '@testing-library/react';

jest.mock('arc-grid');

describe('Securities Collateral Grid', () => {
  let data;
  beforeEach(() => {
    data = [];
    for (let i = 0; i < 10; ++i) {
      data.push({
        pnlSpn: 'demoPnlSpn',
        isin: 'isin',
        cusip: 'cusip',
        sedol: 'sedol',
        local: 'local',
        legalEntity: 'testLE',
        exposureCounterparty: 'testECPE',
        agreementType: 'testAT',
        reportingCurrency: 'testRC',
        custodianAccountAbbrev: 'testCA',
        exposureCustodianAccountName: 'testECA',
        bookAbbrev: 'demoBook',
        gboType: 'testGbo',
        currencyAbbrev: 'testAbbrev',
        tickUnit: 'testTick',
        haircutPct: i,
        price: i,
        quantity: i,
        marketValueRC: i,
        securityCollateralRC: i,
        brokerHaircutPct: i,
        brokerPrice: i,
        brokerQuantity: i,
        brokerMarketValueRC: i,
        brokerMarketValueUsd: i,
        brokerSecurityCollateralRC: i,
        date: 'testDate',
        securitiesCollateralMarginRC: i
      });
    }
  });

  test('Securities Collateral Grid would render if data is passed', () => {
    const { container } = render(<SecuritiesCollateralGrid securitiesCollateralDataList={data} />);
    expect(container.querySelectorAll('div').length).toBe(0);
  });
});
