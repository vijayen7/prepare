import React from 'react';
import { ReactArcGrid } from 'arc-grid';
import { columns } from './gridConfig/grid-columns';
import { options } from './gridConfig/grid-options';

const SecuritiesCollateralGrid: React.FC<any> = (props: SecuritiesCollateralGridPropType) => {
  return (
    <React.Fragment>
      <ReactArcGrid
        style={{ height: '80%' }}
        data={props.securitiesCollateralDataList}
        gridId="securitiesCollateralGrid"
        columns={columns}
        options={options(props.handleAddAdjustment, props.useAvailableSpace)}
        resizeCanvas={props.resizeCanvasFlag}
      />
    </React.Fragment>
  );
};

interface SecuritiesCollateralGridPropType {
  securitiesCollateralDataList: SecuritesCollateralData[];
  handleAddAdjustment: Function;
  resizeCanvasFlag: boolean;
  useAvailableSpace?: boolean;
}

export interface SecuritesCollateralData {
  pnlSpn: string;
  isin?: string;
  cusip?: string;
  sedol?: string;
  local?: string;
  legalEntity: string;
  exposureCounterparty: string;
  agreementType: string;
  bookAbbrev?: string;
  gboType?: string;
  currencyAbbrev?: string;
  custodianAccountAbbrev?: string;
  exposureCustodianAccountName?: string;
  tickUnit?: string;
  haircutPct?: number;
  price?: number;
  quantity?: number;
  marketValueRC?: number;
  securityCollateralRC?: number;
  securitiesCollateralMarginRC?: number;
  brokerHaircutPct?: number;
  brokerPrice?: number;
  brokerQuantity?: number;
  brokerMarketValueRC?: number;
  brokerMarketValueUsd?: number;
  brokerSecurityCollateralRC?: number;
  date: string;
}

export default SecuritiesCollateralGrid;
