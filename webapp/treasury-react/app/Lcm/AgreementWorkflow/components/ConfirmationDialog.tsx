import React from 'react';
import { DialogService } from 'arc-react-components';

export default async function showConfirmationDialog(message: string) {
  let footer = ({ submitProps, cancelProps }) => (
    <div>
      <button {...submitProps}>Yes</button>
      <button {...cancelProps}>No</button>
    </div>
  );

  const dialog = await showDialog('Confirm', message, footer);
  if (dialog.submitted) {
    return true;
  }
  return false;
}

export async function showValidationDialog(title: string, message: string) {
  let footer = ({ submitProps }) => (
    <div>
      <button {...submitProps}>Ok</button>
    </div>
  );
  const dialog = await showDialog(title, message, footer);
  if (dialog.submitted) {
    return true;
  }
  return false;
}

async function showDialog(title: string, message: any, footer?: any) {
  return await DialogService.open({
    title: title,
    renderBody: () => message,
    renderFooter: ({ submitProps, cancelProps, submit }) =>
      footer ? footer({ submitProps, cancelProps, submit }) : undefined,
  });
}
