import React from "react";

import { getExcessDeficitDataRow } from "../utils";
import { InternalVsExternalData } from "../../models";

interface IntVsExtPropType {
  internalVsExternalData: InternalVsExternalData;
}

export const InternalVsExternalTable = ({
  internalVsExternalData,
}: IntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th colSpan={6}>Internal vs External Data</th>
        </tr>
        <tr>
          <th>Source</th>
          <th>Exposure</th>
          <th>Requirement</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>E/D</th>
        </tr>

        {getExcessDeficitDataRow(
          internalVsExternalData.internalEDData,
          "Internal"
        )}

        {getExcessDeficitDataRow(
          internalVsExternalData.externalEDData,
          "External"
        )}

        {getExcessDeficitDataRow(internalVsExternalData.diffEDData, "Diff")}
      </tbody>
    </table>
  );
};
