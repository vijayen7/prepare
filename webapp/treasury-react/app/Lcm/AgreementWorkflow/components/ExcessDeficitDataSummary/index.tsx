import React from 'react';
import { InternalVsExternalData } from '../../models';
import { InternalVsExternalTable } from '../../../../AgreementSummary/components/ExcessDeficitDataTables/InternalVsExternalTable';

export const ExcessDeficitDataSummary: React.FC<DataSummaryPropType> = (
  props: DataSummaryPropType
) => {
  return (
    <div className="layout--flex">
      <InternalVsExternalTable
        isSimmApplicable={props.isSimmApplicable}
        internalVsExternalData={props.internalVsExternalData}
      />
    </div>
  );
};

interface DataSummaryPropType {
  isSimmApplicable: boolean;
  internalVsExternalData: InternalVsExternalData;
}
