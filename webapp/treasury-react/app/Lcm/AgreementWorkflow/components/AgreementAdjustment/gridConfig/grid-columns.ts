import { numberFormatter } from '../../../../../commons/grid/formatters';
import { dGridsumAggregator } from '../../../../../commons/grid/aggregators';

export const columns = [
  {
    id: 'legalEntityName',
    name: 'Legal Entity',
    field: 'legalEntityName',
    toolTip: 'Legal Entity',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'counterpartyName',
    name: 'Exposure Counter Party',
    field: 'counterpartyName',
    toolTip: 'Exposure Counter Party',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'book',
    name: 'Book',
    field: 'book',
    toolTip: 'Book',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'businessUnit',
    name: 'Business Unit',
    field: 'businessUnit',
    toolTip: 'Business Unit',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'agreementType',
    name: 'Agreement Type',
    field: 'agreementType',
    toolTip: 'Agreement Type',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'adjustmentType',
    name: 'Adjustment Type',
    field: 'adjustmentType',
    toolTip: 'Adjustment Type',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'marginCallType',
    name: 'Margin Call Type',
    field: 'marginCallType',
    toolTip: 'Margin Call Type',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'description',
    name: 'Description',
    field: 'description',
    toolTip: 'Description',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'adjustment',
    name: 'Amount',
    field: 'adjustment',
    toolTip: 'Amount',
    type: 'number',
    formatter: numberFormatter,
    aggregator: dGridsumAggregator,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0'
  },
  {
    id: 'startDate',
    name: 'Start Date',
    field: 'startDate',
    toolTip: 'Start Date',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'endDate',
    name: 'End Date',
    field: 'endDate',
    toolTip: 'End Date',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'source',
    name: 'Source',
    field: 'source',
    toolTip: 'Source',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'publishTime',
    name: 'Publish Time',
    field: 'publishTime',
    toolTip: 'Publish Time',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'comment',
    name: 'Comment',
    field: 'comment',
    toolTip: 'Comment',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  },
  {
    id: 'login',
    name: 'User',
    field: 'login',
    toolTip: 'User',
    type: 'text',
    filter: true,
    headerCssClass: 'b',
    minWidth: 60
  }
];

