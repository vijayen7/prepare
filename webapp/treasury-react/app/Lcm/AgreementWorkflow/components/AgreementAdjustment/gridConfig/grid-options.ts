export const options = {
  editable: true,
  exportToExcel: true,
  highlightRowOnClick: true,
  enableCellNavigation: true,
  autoHorizontalScrollBar: true,
  customColumnSelection: false,
  displaySummaryRow: true,
  asyncEditorLoading: false,
  applyFilteringOnGrid: true,
  autoEdit: true,
  configureColumns: true,
};
