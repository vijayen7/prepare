import React from "react";
import { ReactArcGrid } from "arc-grid";
import { columns } from "./gridConfig/grid-columns";
import { options } from "./gridConfig/grid-options";

const AgreementAdjusment: React.FC<any> = (props: AdjustmentGridPropType) => {
  return (
    <React.Fragment>
      <ReactArcGrid
        style={{ height: "80%" }}
        data={props.adjustmentDataList}
        gridId="adjustmentGrid"
        columns={columns}
        options={options}
      />
    </React.Fragment>
  );
};

interface AdjustmentGridPropType {
  adjustmentDataList: AdjustmentData[];
}

export interface AdjustmentData {
  legalEntityName?: string;
  counterpartyName?: string;
  book?: string;
  businessUnit?: string;
  agreementType?: string;
  adjustmentType?: string;
  marginCallType?: string;
  description?: string;
  adjustment?: number;
  startDate?: string;
  endDate?: string;
  source?: string;
  publishTime?: string;
  comment?: string;
  login?: string;
}

export default AgreementAdjusment;
