import React from 'react';
import { Layout } from 'arc-react-components';
import { InternalT1VsT2, ExternalT1VsT2, AdjustedIntVsInt } from '../../models';
import { InternalT1VsT2Table } from '../../../../AgreementSummary/components/ExcessDeficitDataTables/InternalT1VsT2Table';
import { ExternalT1VsT2Table } from '../../../../AgreementSummary/components/ExcessDeficitDataTables/ExternalT1VsT2Table';
import { AdjustIntVsIntTable } from '../../../../AgreementSummary/components/ExcessDeficitDataTables/AdjustIntVsIntTable';

export const ExcessDeficitAdditionalSummary: React.FC<AdditionalSummaryPropType> = (
  props: AdditionalSummaryPropType
) => {
  return (
    <React.Fragment>
      <Layout isColumnType>
        <Layout.Child childId="internalT1VsT2Table" size={1}>
          <InternalT1VsT2Table
            isSimmApplicable={props.isSimmApplicable}
            internalT1VsT2={props.internalT1VsT2}
          />
        </Layout.Child>
        <Layout.Child childId="externalT1VsT2Table" size={1}>
          <ExternalT1VsT2Table
            isSimmApplicable={props.isSimmApplicable}
            externalT1VsT2={props.externalT1VsT2}
          />
        </Layout.Child>
        <Layout.Child childId="adjustIntVsIntTable" size={1}>
          <AdjustIntVsIntTable
            isSimmApplicable={props.isSimmApplicable}
            adjustedIntVsInt={props.adjustedIntVsInt}
          />
        </Layout.Child>
      </Layout>
    </React.Fragment>
  );
};

export interface AdditionalSummaryPropType {
  isSimmApplicable: boolean;
  internalT1VsT2: InternalT1VsT2;
  externalT1VsT2: ExternalT1VsT2;
  adjustedIntVsInt: AdjustedIntVsInt;
}
