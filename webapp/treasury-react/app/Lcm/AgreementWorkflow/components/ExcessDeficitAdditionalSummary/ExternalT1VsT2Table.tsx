import React from "react";
import { getExcessDeficitDataRow } from "../utils";
import { ExternalT1VsT2 } from "../../models";

interface ExtT1VsT2PropType {
  externalT1VsT2: ExternalT1VsT2;
}

export const ExternalT1VsT2Table = ({ externalT1VsT2 }: ExtT1VsT2PropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="externalT1T2"
    >
      <tbody>
        <tr>
          <th colSpan={6}>External Data: T-1 vs T-2</th>
        </tr>
        <tr>
          <th></th>
          <th>Exposure</th>
          <th>Requirement</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>Excess</th>
        </tr>
        {getExcessDeficitDataRow(externalT1VsT2.todayExternalT1VsT2Data, "Today")}

        {getExcessDeficitDataRow(
          externalT1VsT2.yesterdayExternalT1VsT2Data,
          "Yesterday"
        )}

        {getExcessDeficitDataRow(externalT1VsT2.diffExternalT1VsT2Data, "Diff")}
      </tbody>
    </table>
  );
};
