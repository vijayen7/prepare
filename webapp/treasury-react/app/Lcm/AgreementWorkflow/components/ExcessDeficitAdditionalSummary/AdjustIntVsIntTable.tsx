import React from "react";
import { getExcessDeficitDataRow } from "../utils";
import { AdjustedIntVsInt } from "../../models";

interface AdjustIntVsIntPropType {
  adjustedIntVsInt: AdjustedIntVsInt;
}

export const AdjustIntVsIntTable = ({
  adjustedIntVsInt,
}: AdjustIntVsIntPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsInternalAdj"
    >
      <tbody>
        <tr>
          <th colSpan={6}>Internal Vs Internal Adjusted</th>
        </tr>
        <tr>
          <th></th>
          <th>Exposure</th>
          <th>Requirement</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>Excess</th>
        </tr>
        {getExcessDeficitDataRow(
          adjustedIntVsInt.internalAdjustedData,
          "Adjusted"
        )}

        {getExcessDeficitDataRow(
          adjustedIntVsInt.internalUnadjustedData,
          "Unadjusted"
        )}

        {getExcessDeficitDataRow(
          adjustedIntVsInt.internalAdjustedDiffData,
          "Diff"
        )}
      </tbody>
    </table>
  );
};
