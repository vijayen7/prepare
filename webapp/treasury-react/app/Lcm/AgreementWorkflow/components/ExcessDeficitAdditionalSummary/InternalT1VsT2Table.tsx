import React from "react";
import { getExcessDeficitDataRow } from "../utils";
import { InternalT1VsT2 } from "../../models";

interface IntT1VsT2PropType {
  internalT1VsT2: InternalT1VsT2;
}

export const InternalT1VsT2Table = ({ internalT1VsT2 }: IntT1VsT2PropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalT1T2"
    >
      <tbody>
        <tr>
          <th colSpan={6}>Internal Data: T-1 vs T-2</th>
        </tr>
        <tr>
          <th></th>
          <th>Exposure</th>
          <th>Requirement</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>Excess</th>
        </tr>
        {getExcessDeficitDataRow(internalT1VsT2.todayInternalT1VsT2Data, "Today")}

        {getExcessDeficitDataRow(
          internalT1VsT2.yesterdayInternalT1VsT2Data,
          "Yesterday"
        )}

        {getExcessDeficitDataRow(internalT1VsT2.diffInternalT1VsT2Data, "Diff")}
      </tbody>
    </table>
  );
};
