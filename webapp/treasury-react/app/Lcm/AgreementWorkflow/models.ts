import { CashCollateralData } from "./components/CashCollateralGrid";
import { SecuritesCollateralData } from "./components/SecuritiesCollateralGrid";
import { BundlePositionData } from "./components/UsageGrid";

export interface AgreementDetail {
  isSimmApplicable: boolean,
  internalVsExternalData: InternalVsExternalData,
  internalT1VsT2: InternalT1VsT2,
  externalT1VsT2: ExternalT1VsT2,
  adjustedIntVsInt: AdjustedIntVsInt
}

export interface InternalVsExternalData {
  internalEDData: ExcessDeficitData;
  externalEDData: ExcessDeficitData;
  diffEDData: ExcessDeficitData;
}

export interface InternalT1VsT2 {
  todayInternalT1VsT2Data: ExcessDeficitData;
  yesterdayInternalT1VsT2Data: ExcessDeficitData;
  diffInternalT1VsT2Data: ExcessDeficitData;
}

export interface ExternalT1VsT2 {
  todayExternalT1VsT2Data: ExcessDeficitData;
  yesterdayExternalT1VsT2Data: ExcessDeficitData;
  diffExternalT1VsT2Data: ExcessDeficitData;
}

export interface AdjustedIntVsInt {
  internalAdjustedData: ExcessDeficitData;
  internalUnadjustedData: ExcessDeficitData;
  internalAdjustedDiffData: ExcessDeficitData;
}

export interface ExcessDeficitData {
  exposure?: number;
  requirement?: number;
  cashCollateral?: number;
  securitiesCollateral?: number;
  excessDeficit?: number;
  applicableRequirement?: number;
  applicableExcessDeficit?: number;
}

export interface ExcessDeficitWorkflowFilter {
  workflowId: number,
  includePositions: boolean,
  includeBundlePositions: boolean,
  actionableAgreementId: number,
  includeCustodianAccountData: boolean,
  includePreviousDayData: boolean
}

export interface DetailInformationDataMap {
  agreementAccountTypeToCashCollateralDataMap: {},
  agreementAccountTypeToPhysicalCollateralPositionDataMap: {},
  agreementIdToExposureRequirementPositionDataMap:{},
  agreementIdToBundlePositionDataMap: {}
}

export interface AgreementAccountType {
  agreementId: number,
  accountType: string,
  toString(): string
}

export interface AgreementMarginCallTypeAmountData {
  agreementId: number;
  marginCallType: string;
  amountString: string;
}

export interface PublishWorkflowParams {
  workflowOperation: string;
  workflowId: number;
  actionableAgreementId: number;
  disputeAmounts: Array<AgreementMarginCallTypeAmountData>;
  acms: Array<AgreementMarginCallTypeAmountData>;
  asms: Array<AgreementMarginCallTypeAmountData>;
}
