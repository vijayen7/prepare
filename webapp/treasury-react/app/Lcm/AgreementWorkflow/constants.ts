import { AdjustedIntVsInt, ExcessDeficitData, ExternalT1VsT2, InternalT1VsT2, InternalVsExternalData, AgreementDetail } from './models';
import { SearchStatus } from '../../commons/models/SearchStatus';

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};
export const isPortfolioMandatoryToLogWire = CODEX_PROPERTIES['com.arcesium.treasury.lcm.isPortfolioMandatoryToLogWire']

export const LOGWIRE_POPUP_TITLE = "Comet - Log Wire"

export const DISPUTE_WORKFLOW = "Dispute Workflow"

export const MESSAGES = {
  OPEN_WORKFLOW_FAILURE_MESSAGE: "Error occurred while loading workflow data",
  LOAD_POSITION_DATA_FAILURE: "Error occurred while loading exposure & requirement data",
  LOAD_BUNDLE_POSITION_DATA_FAILURE: "Error occurred while loading bundle position data",
  GENERATE_CALL_REPORT_SUCCESS: "Call report generated and emailed successfully",
  GENERATE_CALL_REPORT_FAILURE: "Error occurred while generating call report",
  WORKFLOW_EXECUTE_FAILURE: "Error occurred while Workflow execution",
  WORKFLOW_CANCELLED: "Workflow has been cancelled",
  WORKFLOW_PUBLISHED: "Workflow has been published",
  WORKFLOW_SAVED: "Workflow has been saved",
  WORKFLOW_PUBLISH_FAILURE: "Error occurred while publishing workflow",
  SHOW_LOG_WIRE_POPUP_FAILURE: "Error occurred while opening Log Wire Popup",
  PHYSICAL_COLLATERAL_TERM_MISSING: "Could not find physical collateral term for selected agreement",
  FETCH_COLLATERAL_TERMS_FAILURE: "Error occurred while fetching collateral terms",
  FETCH_INVENTORY_POSITIONS_FAILURE: "Error occurred while getting inventory positions",
  BOOK_MMF_TRADE_FAILURE: "Error occurred while book mmf",
  DISPLAY_BOOK_MMF_DIALOG_FAILURE: "Error occurred while loading book mmf trade dialog"
}

export enum WORKFLOW_OPERATION {
  PUBLISH = "PUBLISH",
  CANCEL = "CANCELLED",
  SAVE = "SAVED",
  QUICK_PUBLISH = "QUICK_PUBLISH",
  QUICK_WIRE_PUBLISH = "QUICK_WIRE_PUBLISH"
}

export enum WORKFLOW_STATUS_TYPE {
  IN_PROGRESS = "IN_PROGRESS",
  PUBLISHED = "PUBLISHED"
}

export enum TOAST_TYPE {
  INFO = "info",
  SUCCESS = "success",
  CRITICAL = "critical",
}

export const INITIAL_EXCESS_DEFICIT_DATA: ExcessDeficitData = {
  exposure: 0,
  requirement: 0,
  cashCollateral: 0,
  securitiesCollateral: 0,
  excessDeficit: 0
}

export const INITIAL_INTERNAL_VS_EXTERNAL_DATA: InternalVsExternalData = {
  internalEDData: INITIAL_EXCESS_DEFICIT_DATA,
  externalEDData: INITIAL_EXCESS_DEFICIT_DATA,
  diffEDData: INITIAL_EXCESS_DEFICIT_DATA
}

export const INITIAL_INTERNAL_T1_VS_T2_DATA: InternalT1VsT2 = {
  todayInternalT1VsT2Data: INITIAL_EXCESS_DEFICIT_DATA,
  yesterdayInternalT1VsT2Data: INITIAL_EXCESS_DEFICIT_DATA,
  diffInternalT1VsT2Data: INITIAL_EXCESS_DEFICIT_DATA
}

export const INITIAL_EXTERNAL_T1_VS_T2_DATA: ExternalT1VsT2 = {
  todayExternalT1VsT2Data: INITIAL_EXCESS_DEFICIT_DATA,
  yesterdayExternalT1VsT2Data: INITIAL_EXCESS_DEFICIT_DATA,
  diffExternalT1VsT2Data: INITIAL_EXCESS_DEFICIT_DATA
}

export const INITIAL_ADJUSTED_INTERNAL_VS_INTERNAL_DATA: AdjustedIntVsInt = {
  internalAdjustedData: INITIAL_EXCESS_DEFICIT_DATA,
  internalUnadjustedData: INITIAL_EXCESS_DEFICIT_DATA,
  internalAdjustedDiffData: INITIAL_EXCESS_DEFICIT_DATA
}

export const INITIAL_AGREEMENT_DETAIL: AgreementDetail = {
  isSimmApplicable: false,
  internalVsExternalData: INITIAL_INTERNAL_VS_EXTERNAL_DATA,
  internalT1VsT2: INITIAL_INTERNAL_T1_VS_T2_DATA,
  externalT1VsT2: INITIAL_EXTERNAL_T1_VS_T2_DATA,
  adjustedIntVsInt: INITIAL_ADJUSTED_INTERNAL_VS_INTERNAL_DATA
}

export const INITIAL_AGREEMENT_DATA = {
  page: "AgreementWorflow",
  tag: "",
  accountType: "",
  tripartyAgreementId: 0,
  isPopulateAsm: false,
  acm: 0,
  asm: 0,
  actionableAgreementId: 0,
  actionableDate: "",
  custodianAccountId: 0,
  reportingCurrencyIsoCode: "",
  reportingCurrencySpn: 0,
  reportingCurrencyFxRate: 1,
  internalVsExternalData: INITIAL_INTERNAL_VS_EXTERNAL_DATA,
  onPostCollateralSuccess: Function
}

export const INITIAL_SEARCH_STATUS: SearchStatus = {
  firstSearch: true,
  inProgress: false,
  error: false
};

export const ID_TO_ACCOUNT_TYPE_MAP = {
  1: "VM_PLUS_IM",
  2: "IM",
  3: "VM",
  4: "SIMM_LE",
  5: "SIMM_CPE"
}

export const INITIAL_BOOK_MMF_POPUP_PROPS = {
  segCustodianAccountName: undefined,
  mmfSpn: undefined,
  amount: undefined,
  description: undefined,
};

