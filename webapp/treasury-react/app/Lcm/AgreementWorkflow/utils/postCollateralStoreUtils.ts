import _ from "lodash"
import { ID_TO_ACCOUNT_TYPE_MAP, isPortfolioMandatoryToLogWire, LOGWIRE_POPUP_TITLE } from "../constants"
import { InternalVsExternalData } from "../models"

export const getAgreementData = (item: any, agreementId: number, date: string,
  internalVsExternalData, page: string, onPostCollateralSuccess) => {
  return {
    page: page,
    tag: item.tag,
    accountType: item.accountType,
    tripartyAgreementId: item.tripartyAgreementId,
    isPopulateAsm: item.isPopulateAsm,
    acm: item.acmRC,
    asm: item.asmRC,
    actionableAgreementId: agreementId,
    actionableDate: date,
    custodianAccountId: item.custodianAccountId,
    reportingCurrencyIsoCode: item.reportingCurrency,
    reportingCurrencySpn: item.reportingCurrencySpn,
    reportingCurrencyFxRate: item.reportingCurrencyFxRate,
    internalVsExternalData: internalVsExternalData,
    onPostCollateralSuccess: onPostCollateralSuccess
  }
}

export const getPhysicalCollateralParam = (
  agreementId: number,
  actionableDate: string,
  accountType: string,
  actualSecurityMovement: number
) => {
  return {
    "@CLASS":
      "com.arcesium.treasury.model.lcm.physicalcollateral.PhysicalCollateralInputParam",
    date: actionableDate,
    agreementId: agreementId,
    accountType: accountType,
    autoSelectionInputParam: {
      "@CLASS":
        "com.arcesium.treasury.model.lcm.physicalcollateral.autochooser.PhysicalCollateralAutoSelectionInputParam",
      actualSecurityMovement: actualSecurityMovement,
      applyOptimizationRules: false,
    },
  };
};

function validateValue(value) {
  return value === 'n/a' ? null : value;
}

export const getInventoryPositionPopupProps = (
  inventoryPositionData,
  actionableDate: string,
  intVsExtData: InternalVsExternalData,
  physicalCollateralInputParam,
  updatePhysicalCollateralSuccessForBookingReferenceId
) => ({
  stagedPhysicalCollateralAgreementDataId:
    inventoryPositionData.stagedPhysicalCollateralAgreementDataId,
  physicalCollateralPositionData:
    inventoryPositionData.physicalCollateralPositionDataView,
  inventorySettleDates: inventoryPositionData.inventorySettleDates,
  date: actionableDate,
  cometData: formatIntVsExtData(intVsExtData),
  autoSelectionResult: inventoryPositionData.autoSelectionResult,
  physicalCollateralInputParam: physicalCollateralInputParam,
  updateBookingReferenceId: updatePhysicalCollateralSuccessForBookingReferenceId
});

const formatIntVsExtData = (intVsExtData: InternalVsExternalData) => {
  return {
    internalExposure: intVsExtData.internalEDData.exposure,
    internalRequirement: intVsExtData.internalEDData.requirement,
    internalCashCollateral: intVsExtData.internalEDData.cashCollateral,
    internalSecuritiesCollateral: intVsExtData.internalEDData.securitiesCollateral,
    internalED: intVsExtData.internalEDData.excessDeficit,
    externalExposure: intVsExtData.externalEDData.exposure,
    externalRequirement: intVsExtData.externalEDData.requirement,
    externalCashCollateral: intVsExtData.externalEDData.cashCollateral,
    externalSecuritiesCollateral: intVsExtData.externalEDData.securitiesCollateral,
    externalED: intVsExtData.externalEDData.excessDeficit,
    diffExposure: intVsExtData.diffEDData.exposure,
    diffRequirement: intVsExtData.diffEDData.requirement,
    diffCashCollateral: intVsExtData.diffEDData.cashCollateral,
    diffSecuritiesCollateral: intVsExtData.diffEDData.securitiesCollateral,
    diffED: intVsExtData.diffEDData.excessDeficit
  }
}

export const getRelevantCollateralTerm = (collateralTermList: any[], accountType: string) => {
  let accountTypeId = Number(_.invert(ID_TO_ACCOUNT_TYPE_MAP)[accountType])

  return collateralTermList.find(item => item.accountTypeId === accountTypeId);
}

export function getUpdatedTopPanelGridData(agreementWorkflowData: any[], actionableKey: string, acmSign: any, wireAmount: any, wireRequestNo: any) {
  if (agreementWorkflowData != null && agreementWorkflowData.length) {
    for (let i = 0; i < agreementWorkflowData.length; i++) {
      let agId = Math.abs(agreementWorkflowData[i].agreementId)
      let tagVal = agreementWorkflowData[i].tag
      let agKey = agId + tagVal
      if (actionableKey === agKey) {
        agreementWorkflowData[i].acmRC = acmSign * wireAmount
        if (agreementWorkflowData[i].wireRequest == undefined || agreementWorkflowData[i].wireRequest == "") {
          agreementWorkflowData[i].wireRequest = 'WRS#' + wireRequestNo
        } else {
          agreementWorkflowData[i].wireRequest = agreementWorkflowData[i].wireRequest.concat(',WRS#').concat(wireRequestNo)
        }
      }
    }
    // Initiating the Grid
    return agreementWorkflowData
  }
}

export function getUpdatedTopPanelGridDataForPostCollateralSuccess(agreementWorkflowData: any[], actionableKey: string, bookingReferenceId: any) {
  if (agreementWorkflowData != null && agreementWorkflowData.length) {
    for (let i = 0; i < agreementWorkflowData.length; i++) {
      let agId = Math.abs(agreementWorkflowData[i].agreementId)
      let tagVal = agreementWorkflowData[i].tag
      let agKey = agId + tagVal
      if (actionableKey === agKey) {
        if (agreementWorkflowData[i].bookingReference == undefined || agreementWorkflowData[i].bookingReference  == "") {
          agreementWorkflowData[i].bookingReference  = "".concat(bookingReferenceId);
        } else {
          agreementWorkflowData[i].bookingReference = agreementWorkflowData[i].bookingReference.concat(',').concat(bookingReferenceId)
        }
      }
    }
    return agreementWorkflowData
  }
}

export const getLogWirePopupProps = (workflowId: number, agreementData, wirePortfolios, onLoggingWireRequest: Function) => ({
  workflowId: workflowId,
  actionableAgreementId: agreementData.actionableAgreementId,
  custodianAccountId: agreementData.custodianAccountId,
  tripartyAgreementId: agreementData.tripartyAgreementId,
  acmVal: agreementData.acm,
  reportingCurrencySpn: agreementData.reportingCurrencySpn,
  wirePortfolioData: wirePortfolios.wirePortfolios,
  isPortfolioMandatoryToLogWire: isPortfolioMandatoryToLogWire,
  tag: agreementData.tag,
  page: agreementData.page,
  title: LOGWIRE_POPUP_TITLE,
  onLoggingWire: onLoggingWireRequest
})

export const getDisplayBookMmfPopupParams = (params) => {
  let workflowId = params.workflowId;
  let actionableAgreementId = params.actionableAgreementId;
  let treasuryAgreementId = params.treasuryAgreementId;
  let wireRequestNo = params.wireRequestNo;
  let mmfAmount = params.mmfAmount;
  let reportingCurrencyFxRate = params.reportingCurrencyFxRate;

  let displayBookMmfPopupParams = "";

  if (workflowId) {
    displayBookMmfPopupParams += "workflowId=" + workflowId;
  }
  if (treasuryAgreementId) {
    displayBookMmfPopupParams += '&treasuryAgreementId=' + treasuryAgreementId;
  }
  if (actionableAgreementId) {
    displayBookMmfPopupParams += "&actionableAgreementId=" + actionableAgreementId;
  }
  if (wireRequestNo) {
    displayBookMmfPopupParams += "&wireRequestNo=" + wireRequestNo;
  }
  if (mmfAmount) {
    displayBookMmfPopupParams += "&mmfAmount=" + mmfAmount;
  }
  if (reportingCurrencyFxRate) {
    displayBookMmfPopupParams += "&reportingCurrencyFxRate=" + reportingCurrencyFxRate;
  }
  return displayBookMmfPopupParams
}

export const getBookMmfTradeParams = (bookMmfParams) => {
  let bookMmfTradeParam = "";
  let segCustodianAccountAbbrev = bookMmfParams.segCustodianAccountAbbrev;
  let mmfAmount = bookMmfParams.mmfAmount;
  let actionableAgreementId = bookMmfParams.actionableAgreementId;
  let wireRequestNo = bookMmfParams.wireRequestNo;
  let mmfSpn = bookMmfParams.mmfSpn;
  let description = bookMmfParams.description;
  let segCustodianAccountId = bookMmfParams.segCustodianAccountId;
  let reportingCurrencyFxRate = bookMmfParams.reportingCurrencyFxRate;

  if (segCustodianAccountAbbrev != undefined) {
    bookMmfTradeParam += "segCustodianAccountAbbrev=" + segCustodianAccountAbbrev;
  }

  if (mmfAmount != undefined) {
    bookMmfTradeParam += "&mmfAmount=" + mmfAmount;
  }
  if (segCustodianAccountAbbrev != undefined) {
    bookMmfTradeParam += "&actionableAgreementId=" + actionableAgreementId;
  }
  if (segCustodianAccountAbbrev != undefined) {
    bookMmfTradeParam += "&wireRequestNo=" + wireRequestNo;
  }
  if (segCustodianAccountAbbrev != undefined) {
    bookMmfTradeParam += "&mmfSpn=" + mmfSpn;
  }
  if (segCustodianAccountAbbrev != undefined) {
    bookMmfTradeParam += "&description=" + description;
  }
  if (segCustodianAccountAbbrev != undefined) {
    bookMmfTradeParam += "&segCustodianAccountId=" + segCustodianAccountId;
  }
  if (reportingCurrencyFxRate != undefined) {
    bookMmfTradeParam += "&reportingCurrencyFxRate=" + reportingCurrencyFxRate;
  }
  return bookMmfTradeParam
}
