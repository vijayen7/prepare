import { AdjustmentData } from '../components/AgreementAdjustment';
import { WORKFLOW_OPERATION, WORKFLOW_STATUS_TYPE } from '../constants';
import { DetailInformationDataMap, AgreementDetail, ExcessDeficitWorkflowFilter, AgreementAccountType,
  AgreementMarginCallTypeAmountData, PublishWorkflowParams} from '../models';

export const getExcessDeficitWorkflowFilter = (
  workflowId: number,
  includePositions: boolean,
  includeBundlePositions: boolean,
  actionableAgreementId: number,
  includeCustodianAccountData: boolean = true,
  includePreviousDayData: boolean = true
): ExcessDeficitWorkflowFilter => ({
  workflowId: workflowId,
  includePositions: includePositions,
  includeBundlePositions: includeBundlePositions,
  actionableAgreementId: actionableAgreementId,
  includeCustodianAccountData: includeCustodianAccountData,
  includePreviousDayData: includePreviousDayData
});

export const getAgreementDetail = (agreementSummary): AgreementDetail => {
  return {
    isSimmApplicable: agreementSummary.isSimmApplicable,
    internalVsExternalData: getInternalVsExternalData(agreementSummary),
    internalT1VsT2: getInternalT1VsT2Data(agreementSummary),
    externalT1VsT2: getExternalT1VsT2Data(agreementSummary),
    adjustedIntVsInt: getInternalAdjustedVsUnadjustedData(agreementSummary),
  };
};

export function getInternalVsExternalData(agreementSummary) {
  let internalEDData = getExcessDeficitData(
    agreementSummary.adjustedExposureRC,
    agreementSummary.adjustedRequirementRC,
    agreementSummary.adjustedCashCollateralRC,
    agreementSummary.adjustedPhysicalCollateralRC,
    agreementSummary.adjustedExcessDeficitRC,
    agreementSummary.adjustedApplicableRequirementRC,
    agreementSummary.adjustedApplicableExcessDeficitRC
  )

  let externalEDData = getExcessDeficitData(
    agreementSummary.brokerExposureRC,
    agreementSummary.brokerRequirementRC,
    agreementSummary.brokerCashCollateralRC,
    agreementSummary.brokerPhysicalCollateralRC,
    agreementSummary.brokerExcessDeficitRC,
    agreementSummary.brokerApplicableRequirementRC,
    agreementSummary.brokerApplicableExcessDeficitRC
  )

  let diffEDData = getExcessDeficitData(
    subtractWithNullChecksAndGetDisplayText(
      agreementSummary.adjustedExposureRC,
      agreementSummary.brokerExposureRC
    ),
    subtractWithNullChecksAndGetDisplayText(
      agreementSummary.adjustedRequirementRC,
      agreementSummary.brokerRequirementRC
    ),
    subtractWithNullChecksAndGetDisplayText(
      agreementSummary.adjustedCashCollateralRC,
      agreementSummary.brokerCashCollateralRC
    ),
    subtractWithNullChecksAndGetDisplayText(
      agreementSummary.adjustedPhysicalCollateralRC,
      agreementSummary.brokerPhysicalCollateralRC
    ),
    subtractWithNullChecksAndGetDisplayText(
      agreementSummary.adjustedExcessDeficitRC,
      agreementSummary.brokerExcessDeficitRC
    ),
    subtractWithNullChecksAndGetDisplayText(
      agreementSummary.adjustedApplicableRequirementRC,
      agreementSummary.brokerApplicableRequirementRC
    ),
    subtractWithNullChecksAndGetDisplayText(
      internalEDData.applicableExcessDeficit,
      externalEDData.applicableExcessDeficit
    )
  )

  return {
    internalEDData: internalEDData,
    externalEDData: externalEDData,
    diffEDData: diffEDData,
  };
}

export function getInternalT1VsT2Data(agreementSummary) {
  return {
    todayInternalT1VsT2Data: getExcessDeficitData(
      agreementSummary.adjustedExposureRC,
      agreementSummary.adjustedRequirementRC,
      agreementSummary.adjustedCashCollateralRC,
      agreementSummary.adjustedPhysicalCollateralRC,
      agreementSummary.adjustedExcessDeficitRC,
      agreementSummary.adjustedApplicableRequirementRC,
      agreementSummary.adjustedApplicableExcessDeficitRC
    ),

    yesterdayInternalT1VsT2Data: getExcessDeficitData(
      agreementSummary.prevDayAdjustedExposureRC,
      agreementSummary.prevDayAdjustedRequirementRC,
      agreementSummary.prevDayAdjustedCashCollateralRC,
      agreementSummary.prevDayAdjustedPhysicalCollateralRC,
      agreementSummary.prevDayAdjustedExcessDeficitRC,
      agreementSummary.prevDayAdjustedApplicableRequirementRC,
      agreementSummary.prevDayAdjustedApplicableExcessDeficitRC
    ),

    diffInternalT1VsT2Data: getExcessDeficitData(
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedExposureRC,
        agreementSummary.prevDayAdjustedExposureRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedRequirementRC,
        agreementSummary.prevDayAdjustedRequirementRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedCashCollateralRC,
        agreementSummary.prevDayAdjustedCashCollateralRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedPhysicalCollateralRC,
        agreementSummary.prevDayAdjustedPhysicalCollateralRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedExcessDeficitRC,
        agreementSummary.prevDayAdjustedExcessDeficitRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedApplicableRequirementRC,
        agreementSummary.prevDayAdjustedApplicableRequirementRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedApplicableExcessDeficitRC,
        agreementSummary.prevDayAdjustedApplicableExcessDeficitRC
      )
    ),
  };
}

export function getExternalT1VsT2Data(agreementSummary) {
  return {
    todayExternalT1VsT2Data: getExcessDeficitData(
      agreementSummary.brokerExposureRC,
      agreementSummary.brokerRequirementRC,
      agreementSummary.brokerCashCollateralRC,
      agreementSummary.brokerPhysicalCollateralRC,
      agreementSummary.brokerExcessDeficitRC,
      agreementSummary.brokerApplicableRequirementRC,
      agreementSummary.brokerApplicableExcessDeficitRC
    ),

    yesterdayExternalT1VsT2Data: getExcessDeficitData(
      agreementSummary.prevDayBrokerExposureRC,
      agreementSummary.prevDayBrokerRequirementRC,
      agreementSummary.prevDayBrokerCashCollateral,
      agreementSummary.prevDayBrokerPhysicalCollateralRC,
      agreementSummary.prevDayBrokerExcessDeficitRC,
      agreementSummary.prevDayBrokerApplicableRequirementRC,
      agreementSummary.prevDayBrokerApplicableExcessDeficitRC
    ),

    diffExternalT1VsT2Data: getExcessDeficitData(
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.brokerExposureRC,
        agreementSummary.prevDayBrokerExposureRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.brokerRequirementRC,
        agreementSummary.prevDayBrokerRequirementRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.brokerCashCollateralRC,
        agreementSummary.prevDayBrokerCashCollateral
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.brokerPhysicalCollateralRC,
        agreementSummary.prevDayBrokerPhysicalCollateralRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.brokerExcessDeficitRC,
        agreementSummary.prevDayBrokerExcessDeficitRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.brokerApplicableRequirementRC,
        agreementSummary.prevDayBrokerApplicableRequirementRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.brokerApplicableExcessDeficitRC,
        agreementSummary.prevDayBrokerApplicableExcessDeficitRC
      )
    ),
  };
}

export function getInternalAdjustedVsUnadjustedData(agreementSummary) {
  return {
    internalAdjustedData: getExcessDeficitData(
      agreementSummary.adjustedExposureRC,
      agreementSummary.adjustedRequirementRC,
      agreementSummary.adjustedCashCollateralRC,
      agreementSummary.adjustedPhysicalCollateralRC,
      agreementSummary.adjustedExcessDeficitRC,
      agreementSummary.adjustedApplicableRequirementRC,
      agreementSummary.adjustedApplicableExcessDeficitRC
    ),

    internalUnadjustedData: getExcessDeficitData(
      agreementSummary.exposureRC,
      agreementSummary.requirementRC,
      agreementSummary.cashCollateralRC,
      agreementSummary.physicalCollateralRC,
      agreementSummary.excessDeficitRC,
      agreementSummary.applicableRequirementRC,
      agreementSummary.applicableExcessDeficitRC
    ),

    internalAdjustedDiffData: getExcessDeficitData(
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedExposureRC,
        agreementSummary.exposureRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedRequirementRC,
        agreementSummary.requirementRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedCashCollateralRC,
        agreementSummary.cashCollateralRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedPhysicalCollateralRC,
        agreementSummary.physicalCollateralRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedExcessDeficitRC,
        agreementSummary.excessDeficitRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedApplicableRequirementRC,
        agreementSummary.applicableRequirementRC
      ),
      subtractWithNullChecksAndGetDisplayText(
        agreementSummary.adjustedApplicableExcessDeficitRC,
        agreementSummary.applicableExcessDeficitRC
      )
    ),
  };
}

function subtractWithNullChecksAndGetDisplayText(value1, value2) {
  return !checkUndefinedOrEmpty(value1) &&
    !checkUndefinedOrEmpty(value2) &&
    !checkUndefinedOrEmpty(value1 - value2) &&
    !isNaN(value1 - value2)
    ? value1 - value2
    : 'n/a';
}

function validateAndGetDisplayText(value) {
  return !checkUndefinedOrEmpty(value) ? value : 'n/a';
}

function getExcessDeficitData(
  exposure,
  requirement,
  cashCollateral,
  securitiesCollateral,
  excessDeficit,
  applicableRequirement,
  applicableExcessDeficit
) {
  return {
    exposure: validateAndGetDisplayText(exposure),
    requirement: validateAndGetDisplayText(requirement),
    cashCollateral: validateAndGetDisplayText(cashCollateral),
    securitiesCollateral: validateAndGetDisplayText(securitiesCollateral),
    excessDeficit: validateAndGetDisplayText(excessDeficit),
    applicableRequirement: validateAndGetDisplayText(applicableRequirement),
    applicableExcessDeficit: validateAndGetDisplayText(
      getApplicableAdjustedExcessDeficit(excessDeficit, applicableExcessDeficit)
    ),
  };
}

// this is required to handle cases where for a simm applicable agreement, the HOUSE leg is not applicable.
// thus setting the applicable excess/deficit as raw excess/deficit
function getApplicableAdjustedExcessDeficit(excessDeficit, applicableExcessDeficit) {
  return applicableExcessDeficit != undefined && applicableExcessDeficit != 'n/a'
    ? applicableExcessDeficit
    : excessDeficit;
}

function checkUndefinedOrEmpty(value) {
  if (value == undefined || value === '') {
    return true;
  }
  return false;
}

export const getTopPanelGridData = (excessDeficitWorkflowDataList, workflowId: number) => {
  let treasuryAgreementData = excessDeficitWorkflowDataList[0];
  treasuryAgreementData.workflowId = workflowId;

  for (let i = 0; i < excessDeficitWorkflowDataList.length; i++) {
    excessDeficitWorkflowDataList[i].workflowId = workflowId;
  }

  if (treasuryAgreementData.agreementType == 'MNA') {
    return loadMNAAgreement(excessDeficitWorkflowDataList, workflowId);
  } else {
    return loadNonMNAAgreement(treasuryAgreementData);
  }
};

const loadMNAAgreement = (excessDeficitWorkflowDataList, workflowId: number) => {
  let id = 0;
  for (let i = 0; i < excessDeficitWorkflowDataList.length; i++) {
    excessDeficitWorkflowDataList[i].id = id++;

    excessDeficitWorkflowDataList[i].internalED = getApplicableAdjustedExcessDeficit(
      excessDeficitWorkflowDataList[i].adjustedExcessDeficit,
      excessDeficitWorkflowDataList[i].adjustedApplicableExcessDeficit
    );

    excessDeficitWorkflowDataList[i].internalEDRC = getApplicableAdjustedExcessDeficit(
      excessDeficitWorkflowDataList[i].adjustedExcessDeficitRC,
      excessDeficitWorkflowDataList[i].adjustedApplicableExcessDeficitRC
    );

    if (excessDeficitWorkflowDataList[i].children?.length > 0) {
      for (let j = 0; j < excessDeficitWorkflowDataList[i].children.length; j++) {
        excessDeficitWorkflowDataList[i].children[j].id = id++;
        excessDeficitWorkflowDataList[i].children[j].workflowId = workflowId;

        excessDeficitWorkflowDataList[i].children[
          j
        ].internalED = getApplicableAdjustedExcessDeficit(
          excessDeficitWorkflowDataList[i].children[j].adjustedExcessDeficit,
          excessDeficitWorkflowDataList[i].children[j].adjustedApplicableExcessDeficit
        );

        excessDeficitWorkflowDataList[i].children[
          j
        ].internalEDRC = getApplicableAdjustedExcessDeficit(
          excessDeficitWorkflowDataList[i].children[j].adjustedExcessDeficitRC,
          excessDeficitWorkflowDataList[i].children[j].adjustedApplicableExcessDeficitRC
        );
      }
    }
  }

  return excessDeficitWorkflowDataList;
};

const loadNonMNAAgreement = (excessDeficitWorkflowDataList) => {
  excessDeficitWorkflowDataList.internalED = getApplicableAdjustedExcessDeficit(
    excessDeficitWorkflowDataList.adjustedExcessDeficit,
    excessDeficitWorkflowDataList.adjustedApplicableExcessDeficit
  );

  excessDeficitWorkflowDataList.internalEDRC = getApplicableAdjustedExcessDeficit(
    excessDeficitWorkflowDataList.adjustedExcessDeficitRC,
    excessDeficitWorkflowDataList.adjustedApplicableExcessDeficitRC
  );

  if (excessDeficitWorkflowDataList.children?.length > 0) {
    excessDeficitWorkflowDataList.children[0].wireRequest =
      excessDeficitWorkflowDataList.wireRequest;
    excessDeficitWorkflowDataList.children[0].bookingReference =
      excessDeficitWorkflowDataList.bookingReference;
    for (let j = 0; j < excessDeficitWorkflowDataList.children.length; j++) {
      excessDeficitWorkflowDataList.children[j].internalED = getApplicableAdjustedExcessDeficit(
        excessDeficitWorkflowDataList.children[j].adjustedExcessDeficit,
        excessDeficitWorkflowDataList.children[j].adjustedApplicableExcessDeficit
      );

      excessDeficitWorkflowDataList.children[j].internalEDRC = getApplicableAdjustedExcessDeficit(
        excessDeficitWorkflowDataList.children[j].adjustedExcessDeficitRC,
        excessDeficitWorkflowDataList.children[j].adjustedApplicableExcessDeficitRC
      );
      excessDeficitWorkflowDataList.children[j].id = j + 1;
    }
  }

  return excessDeficitWorkflowDataList.children;
};

export function getDetailedInformationDataMaps(
  excessDeficitWorkerDataList: any[]
): DetailInformationDataMap {
  let treasuryAgreementExcessDeficitData = excessDeficitWorkerDataList[0];
  let agreementAccountTypeToCashCollateralDataMap = {};
  let agreementAccountTypeToPhysicalCollateralPositionDataMap = {};
  let agreementIdToExposureRequirementPositionDataMap = {};
  let agreementIdToBundlePositionDataMap = {};

  let childAgreementIds: Array<number> = [];
  let childAgreementAccountTypes: Array<AgreementAccountType> = [];

  if (treasuryAgreementExcessDeficitData && treasuryAgreementExcessDeficitData.children) {
    for (let j = 0; j < treasuryAgreementExcessDeficitData.children.length; j++) {
      let agreementData = treasuryAgreementExcessDeficitData.children[j];
      let agreementAccountTypeData: AgreementAccountType = getAgreementAccountType(agreementData.agreementId, agreementData.accountType);
      let agreementAccountTypeDataString = agreementAccountTypeData.toString();
      if (!childAgreementIds.includes(agreementData.agreementId)) {
        childAgreementIds = childAgreementIds.concat(agreementData.agreementId);
      }
      if (!childAgreementAccountTypes.includes(agreementAccountTypeData)) {
        childAgreementAccountTypes = childAgreementAccountTypes.concat(agreementAccountTypeData);
      }

      setAgreementIdAndAdjustment(agreementData, agreementData.agreementId);

      if (agreementData.positionDataList && agreementData.positionDataList.length) {
        agreementIdToExposureRequirementPositionDataMap[agreementData.agreementId] = clean(
          agreementData.positionDataList,
          undefined
        );
      }

      if (
        agreementData.lcmPhysicalCollateralPositionDataList &&
        agreementData.lcmPhysicalCollateralPositionDataList.length
      ){
        let updatedPhysicalCollateralPositionDataList = clean(
          agreementData.lcmPhysicalCollateralPositionDataList,
          undefined
        );
        updatedPhysicalCollateralPositionDataList.forEach(function (item, index) {
         item.tag = agreementData.tag
        });
        agreementAccountTypeToPhysicalCollateralPositionDataMap[agreementAccountTypeDataString] =  updatedPhysicalCollateralPositionDataList
      }

      if (agreementData.bundlePositionDataList && agreementData.bundlePositionDataList.length) {
        agreementIdToBundlePositionDataMap[agreementData.agreementId] = clean(
          agreementData.bundlePositionDataList,
          undefined
        );
      }

      if (agreementData.custodianAccountDataList && agreementData.custodianAccountDataList.length) {
        agreementAccountTypeToCashCollateralDataMap[agreementAccountTypeDataString] = clean(
          agreementData.custodianAccountDataList,
          undefined
        );
      }
    }
  }

  let detailInformationDataMap: DetailInformationDataMap = {
    agreementAccountTypeToCashCollateralDataMap: agreementAccountTypeToCashCollateralDataMap,
    agreementAccountTypeToPhysicalCollateralPositionDataMap: agreementAccountTypeToPhysicalCollateralPositionDataMap,
    agreementIdToExposureRequirementPositionDataMap: agreementIdToExposureRequirementPositionDataMap,
    agreementIdToBundlePositionDataMap: agreementIdToBundlePositionDataMap,
  };

  if (treasuryAgreementExcessDeficitData.agreementType == 'MNA') {
    updateTreasuryAgreementDetaiInformation(
      treasuryAgreementExcessDeficitData,
      detailInformationDataMap,
      childAgreementIds,
      childAgreementAccountTypes
    );
  }

  handlePositionsForActionableIsda(excessDeficitWorkerDataList, detailInformationDataMap);

  updateIdsFromAgreementId(agreementAccountTypeToCashCollateralDataMap, "cash")
  updateIdsFromAgreementId(agreementAccountTypeToPhysicalCollateralPositionDataMap, "physical")
  updateIdsFromAgreementId(agreementIdToExposureRequirementPositionDataMap, "exposure")
  updateIdsFromAgreementId(agreementIdToBundlePositionDataMap, "usage")

  return detailInformationDataMap;
}

function updateIdsFromAgreementId(map: any, unique_id: string) {
  let rowId = 1;
  for (let key in map) {
    if (map[key].length > 0) {
      for (let i = 0; i < map[key].length; i++) {
        (map[key])[i].id = unique_id + "_" + rowId;
        rowId++;
      }
    }
  }
}



export const getAgreementAccountType = (
  agreementId: number,
  accountType: string
): AgreementAccountType => ({
  agreementId: agreementId,
  accountType: accountType,
  toString() {
    return agreementId + accountType;
  },
});

function updateTreasuryAgreementDetaiInformation(
  treasuryAgreementExcessDeficitData: any,
  detailInformationDataMap: DetailInformationDataMap,
  childAgreementIds: number[],
  childAgreementAccountTypes: AgreementAccountType[]
) {
  let {
    agreementAccountTypeToCashCollateralDataMap,
    agreementAccountTypeToPhysicalCollateralPositionDataMap,
    agreementIdToExposureRequirementPositionDataMap,
    agreementIdToBundlePositionDataMap,
  } = detailInformationDataMap;

  let treasuryAgreementAccountTypeString = getAgreementAccountType(
    treasuryAgreementExcessDeficitData.agreementId,
    treasuryAgreementExcessDeficitData.accountType
  ).toString();

  agreementAccountTypeToCashCollateralDataMap[treasuryAgreementAccountTypeString] = [];
  agreementAccountTypeToPhysicalCollateralPositionDataMap[
    treasuryAgreementAccountTypeString
  ] = [];
  agreementIdToExposureRequirementPositionDataMap[
    treasuryAgreementExcessDeficitData.agreementId
  ] = [];
  agreementIdToBundlePositionDataMap[treasuryAgreementExcessDeficitData.agreementId] = [];

  let cashCollateralSet: Set<any> = new Set();
  let physicalCollateralSet: Set<any> = new Set();

  for (let i = 0; i < childAgreementAccountTypes.length; i++) {
    let childAgreementAccountTypeString = childAgreementAccountTypes[i].toString();
    if (agreementAccountTypeToCashCollateralDataMap[childAgreementAccountTypeString]) {
      cashCollateralSet = new Set([...cashCollateralSet, ...agreementAccountTypeToCashCollateralDataMap[childAgreementAccountTypeString]]);
    }



    if (agreementAccountTypeToPhysicalCollateralPositionDataMap[childAgreementAccountTypeString]) {
      physicalCollateralSet = new Set([...physicalCollateralSet, ...agreementAccountTypeToPhysicalCollateralPositionDataMap[childAgreementAccountTypeString]]);
    }
  }

  agreementAccountTypeToCashCollateralDataMap[
    treasuryAgreementAccountTypeString
  ] = updateIds(
    agreementAccountTypeToCashCollateralDataMap[treasuryAgreementAccountTypeString].concat(
      Array.from(cashCollateralSet)
    )
  );


  agreementAccountTypeToPhysicalCollateralPositionDataMap[
    treasuryAgreementAccountTypeString
  ] = updateIds(
    agreementAccountTypeToPhysicalCollateralPositionDataMap[
      treasuryAgreementAccountTypeString
    ].concat(Array.from(physicalCollateralSet))
  );

  for (let i = 0; i < childAgreementIds.length; i++) {
    if (agreementIdToExposureRequirementPositionDataMap[childAgreementIds[i]]) {
      agreementIdToExposureRequirementPositionDataMap[
        treasuryAgreementExcessDeficitData.agreementId
      ] = updateIds(
        agreementIdToExposureRequirementPositionDataMap[
          treasuryAgreementExcessDeficitData.agreementId
        ].concat(agreementIdToExposureRequirementPositionDataMap[childAgreementIds[i]])
      );
    }

    if (agreementIdToBundlePositionDataMap[childAgreementIds[i]]) {
      agreementIdToBundlePositionDataMap[
        treasuryAgreementExcessDeficitData.agreementId
      ] = updateIds(
        agreementIdToBundlePositionDataMap[treasuryAgreementExcessDeficitData.agreementId].concat(
          agreementIdToBundlePositionDataMap[childAgreementIds[i]]
        )
      );
    }
  }
}

function setAgreementIdAndAdjustment(agreementData, agreementId) {
  if (agreementData.positionDataList && agreementData.positionDataList.length) {
    agreementData.positionDataList.forEach((positionData, index) => {
      setAgreementIdAndAdjustement(positionData, agreementId);
      positionData.id = index;
    });
  }

  if (agreementData.bundlePositionDataList && agreementData.bundlePositionDataList.length) {
    agreementData.bundlePositionDataList.forEach((bundlePositionData, index) => {
      setAgreementIdAndAdjustement(bundlePositionData, agreementId);
      bundlePositionData.id = index;
    });
  }

  if (
    agreementData.lcmPhysicalCollateralPositionDataList &&
    agreementData.lcmPhysicalCollateralPositionDataList.length
  ) {
    agreementData.lcmPhysicalCollateralPositionDataList.forEach(
      (physicalCollateralPositionData, index) => {
        setAgreementIdAndAdjustement(physicalCollateralPositionData, agreementId);
        physicalCollateralPositionData.id = index;
      }
    );
  }

  if (agreementData.custodianAccountDataList && agreementData.custodianAccountDataList.length) {
    agreementData.custodianAccountDataList.forEach((custodianAccountData, index) => {
      setAgreementIdAndAdjustement(custodianAccountData, agreementId);
      custodianAccountData.id = index;
      if (
        custodianAccountData.custodianAccount &&
        custodianAccountData.custodianAccount.displayName
      ) {
        custodianAccountData.custodianAccountAbbrev = custodianAccountData.custodianAccount.displayName.replace(
          /\s/g,
          '&nbsp;'
        );
      }
    });
  }
}

function handlePositionsForActionableIsda(
  excessDeficitWorkerDataList,
  detailInformationDataMap: DetailInformationDataMap
) {
  let {
    agreementAccountTypeToCashCollateralDataMap,
    agreementAccountTypeToPhysicalCollateralPositionDataMap,
    agreementIdToExposureRequirementPositionDataMap,
    agreementIdToBundlePositionDataMap,
  } = detailInformationDataMap;

  for (let i = 1; i < excessDeficitWorkerDataList.length; i++) {
    if (excessDeficitWorkerDataList[i].agreementId < 0) {
      let agreementData = excessDeficitWorkerDataList[i];

      let agreementId = -1 * agreementData.agreementId;

      let agreementAccountTypeData: AgreementAccountType = getAgreementAccountType(
        agreementData.agreementId,
        agreementData.accountType
      );

      let agreementAccountTypeDataString = agreementAccountTypeData.toString();

      setAgreementIdAndAdjustement(agreementData, agreementData.agreementId);





      if (
        agreementData.custodianAccountDataList &&
        agreementAccountTypeToCashCollateralDataMap[agreementAccountTypeDataString] == null
      ) {

        for (let i = 0; i < agreementData.custodianAccountDataList.length; i++) {
          agreementData.custodianAccountDataList[i].agreementId = agreementId;
        }

        agreementAccountTypeToCashCollateralDataMap[agreementAccountTypeDataString] = clean(
          agreementData.custodianAccountDataList,
          undefined
        );
      }

      if (
        agreementData.lcmPhysicalCollateralPositionDataList &&
        agreementAccountTypeToPhysicalCollateralPositionDataMap[agreementAccountTypeDataString] == null
      ) {

        for (let i = 0; i < agreementData.lcmPhysicalCollateralPositionDataList.length; i++) {
          agreementData.lcmPhysicalCollateralPositionDataList[i].agreementId = agreementId;
        }

        agreementAccountTypeToPhysicalCollateralPositionDataMap[agreementAccountTypeDataString] = clean(
          agreementData.lcmPhysicalCollateralPositionDataList,
          undefined
        );
      }

      if (
        agreementData.positionDataList &&
        agreementIdToExposureRequirementPositionDataMap[agreementData.agreementId] == null
      ) {

        for (let i = 0; i < agreementData.positionDataList.length; i++) {
          agreementData.positionDataList[i].agreementId = agreementId;
        }

        agreementIdToExposureRequirementPositionDataMap[agreementData.agreementId] = clean(
          agreementData.positionDataList,
          undefined
        );
      }

      if (
        agreementData.bundlePositionDataList &&
        agreementIdToBundlePositionDataMap[agreementData.agreementId] == null
      ) {

        for (let i = 0; i < agreementData.bundlePositionDataList.length; i++) {
          agreementData.bundlePositionDataList[i].agreementId = agreementId;
        }

        agreementIdToBundlePositionDataMap[agreementData.agreementId] = clean(
          agreementData.bundlePositionDataList,
          undefined
        );
      }
    }
  }
}

function clean(arr, deleteValue) {
  if (arr) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] == deleteValue) {
        arr.splice(i, 1);
        i--;
      }
    }
    return arr;
  }
  return arr;
}

function updateIds(array: Array<any>) {
  array.forEach((element, index) => (element.id = index));
  return array;
}

function setAgreementIdAndAdjustement(element, agreementId) {
  element.agreementId = agreementId;
  element.adjustment = '';
}

export function getAdjustmentDataMap(excessDeficitWorkerData) {
  let treasuryAgreementData = excessDeficitWorkerData[0];
  let agreementIdToAdjustmentMap = new Map();

  if (treasuryAgreementData && treasuryAgreementData.children) {
    let treasuryAgreementAdjustment = [];
    for (let j = 0; j < treasuryAgreementData.children.length; j++) {
      let agreementData = treasuryAgreementData.children[j];

      let agreementAdjustment = [];
      if (agreementData.positionDataList && agreementData.positionDataList.length > 0) {
        for (let i = 0; i < agreementData.positionDataList.length; i++) {
          agreementData.positionDataList[i].id = i;

          addCollateralAdjustmentsPositionDataList(
            i,
            agreementData,
            agreementAdjustment,
            treasuryAgreementAdjustment
          );

          addExposureAdjustmentsPositionDataList(
            i,
            agreementData,
            agreementAdjustment,
            treasuryAgreementAdjustment
          );

          addRequiredAdjustments(
            i,
            agreementData,
            agreementAdjustment,
            treasuryAgreementAdjustment
          );
        }
      } else {
        addCollateralAdjustmentsAgreementData(
          agreementData,
          agreementAdjustment,
          treasuryAgreementAdjustment
        );

        addExposureAdjustmentsAgreementData(
          agreementData,
          agreementAdjustment,
          treasuryAgreementAdjustment
        );

        addRequiredAdjustmentsAgreementData(
          agreementData,
          agreementAdjustment,
          treasuryAgreementAdjustment
        );
      }

      if (agreementData.lcmPhysicalCollateralPositionDataList
        && agreementData.lcmPhysicalCollateralPositionDataList.length > 0) {
        for (let i = 0; i < agreementData.lcmPhysicalCollateralPositionDataList.length; i++) {
          agreementData.lcmPhysicalCollateralPositionDataList[i].id = i;
          addPhysicalCollateralAdjustmentsPositionDataList(
            i,
            agreementData,
            agreementAdjustment,
            treasuryAgreementAdjustment
          );
        }
      }
      else {
        addPhysicalCollateralAdjustmentsAgreementData(
          agreementData,
          agreementAdjustment,
          treasuryAgreementAdjustment
        );
      }

      if (agreementAdjustment) {
        agreementIdToAdjustmentMap[agreementData.agreementId] = agreementAdjustment;
      }
    }
    if (treasuryAgreementAdjustment) {
      agreementIdToAdjustmentMap[treasuryAgreementData.agreementId] = treasuryAgreementAdjustment;
    }
  }

  return agreementIdToAdjustmentMap;
}

function addCollateralAdjustmentsPositionDataList(
  i,
  agreementData,
  agreementAdjustment,
  treasuryAgreementAjustment
) {
  if (
    agreementData.positionDataList[i].collateralAdjustments != null &&
    agreementData.positionDataList[i].collateralAdjustments != undefined
  ) {
    let agreementAdjustmentCount = agreementAdjustment.length;
    let treasuryAgreementAdjustmentCount = treasuryAgreementAjustment.length;
    for (let m = 0; m < agreementData.positionDataList[i].collateralAdjustments.length; m++) {
      agreementAdjustment[agreementAdjustmentCount] =
        agreementData.positionDataList[i].collateralAdjustments[m];
      agreementAdjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
      agreementAdjustmentCount++;
      treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] =
        agreementData.positionDataList[i].collateralAdjustments[m];
      treasuryAgreementAjustment[
        treasuryAgreementAdjustmentCount
      ].id = treasuryAgreementAdjustmentCount;
      treasuryAgreementAdjustmentCount++;
    }
  }
}

function addPhysicalCollateralAdjustmentsPositionDataList(
  i,
  agreementData,
  agreementAdjustment,
  treasuryAgreementAjustment
) {
  if (
    agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments != null &&
    agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments != undefined
  ) {
    let agreementAdjustmentCount = agreementAdjustment.length;
    let treasuryAgreementAdjustmentCount = treasuryAgreementAjustment.length;
    for (let m = 0; m < agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments.length; m++) {
      agreementAdjustment[agreementAdjustmentCount] =
        agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments[m];
      agreementAdjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
      agreementAdjustmentCount++;
      treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] =
        agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments[m];
      treasuryAgreementAjustment[
        treasuryAgreementAdjustmentCount
      ].id = treasuryAgreementAdjustmentCount;
      treasuryAgreementAdjustmentCount++;
    }
  }
}

function addExposureAdjustmentsPositionDataList(
  i,
  agreementData,
  agreementAjustment,
  treasuryAgreementAjustment
) {
  if (
    agreementData.positionDataList[i].exposureAdjustments != null &&
    agreementData.positionDataList[i].exposureAdjustments != undefined
  ) {
    let agreementAdjustmentCount = agreementAjustment.length;
    let treasuryAgreementAdjustmentCount = treasuryAgreementAjustment.length;
    for (let m = 0; m < agreementData.positionDataList[i].exposureAdjustments.length; m++) {
      agreementAjustment[agreementAdjustmentCount] =
        agreementData.positionDataList[i].exposureAdjustments[m];
      agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
      agreementAdjustmentCount++;
      treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] =
        agreementData.positionDataList[i].exposureAdjustments[m];
      treasuryAgreementAjustment[
        treasuryAgreementAdjustmentCount
      ].id = treasuryAgreementAdjustmentCount;
      treasuryAgreementAdjustmentCount++;
    }
  }
}

function addRequiredAdjustments(i, agreementData, agreementAjustment, treasuryAgreementAjustment) {
  if (
    agreementData.positionDataList[i].requirementAdjustments != null &&
    agreementData.positionDataList[i].requirementAdjustments != undefined
  ) {
    let agreementAdjustmentCount = agreementAjustment.length;
    let treasuryAgreementAdjustmentCount = treasuryAgreementAjustment.length;
    for (let m = 0; m < agreementData.positionDataList[i].requirementAdjustments.length; m++) {
      agreementAjustment[agreementAdjustmentCount] =
        agreementData.positionDataList[i].requirementAdjustments[m];
      agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
      agreementAdjustmentCount++;
      treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] =
        agreementData.positionDataList[i].requirementAdjustments[m];
      treasuryAgreementAjustment[
        treasuryAgreementAdjustmentCount
      ].id = treasuryAgreementAdjustmentCount;
      treasuryAgreementAdjustmentCount++;
    }
  }
}

function addCollateralAdjustmentsAgreementData(
  agreementData,
  agreementAjustment,
  treasuryAgreementAjustment
) {
  if (
    agreementData.collateralAdjustments != null &&
    agreementData.collateralAdjustments != undefined
  ) {
    let agreementAdjustmentCount = agreementAjustment.length;
    let treasuryAgreementAdjustmentCount = treasuryAgreementAjustment.length;
    for (let m = 0; m < agreementData.collateralAdjustments.length; m++) {
      agreementAjustment[agreementAdjustmentCount] = agreementData.collateralAdjustments[m];
      agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
      agreementAdjustmentCount++;
      treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] =
        agreementData.collateralAdjustments[m];
      treasuryAgreementAjustment[
        treasuryAgreementAdjustmentCount
      ].id = treasuryAgreementAdjustmentCount;
      treasuryAgreementAdjustmentCount++;
    }
  }
}
function addPhysicalCollateralAdjustmentsAgreementData(
  agreementData,
  agreementAjustment,
  treasuryAgreementAjustment
) {
  if (
    agreementData.physicalCollateralAdjustments != null &&
    agreementData.physicalCollateralAdjustments != undefined
  ) {
    let agreementAdjustmentCount = agreementAjustment.length;
    let treasuryAgreementAdjustmentCount = treasuryAgreementAjustment.length;
    for (let m = 0; m < agreementData.physicalCollateralAdjustments.length; m++) {
      agreementAjustment[agreementAdjustmentCount] = agreementData.physicalCollateralAdjustments[m];
      agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
      agreementAdjustmentCount++;
      treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] =
        agreementData.physicalCollateralAdjustments[m];
      treasuryAgreementAjustment[
        treasuryAgreementAdjustmentCount
      ].id = treasuryAgreementAdjustmentCount;
      treasuryAgreementAdjustmentCount++;
    }
  }
}

function addExposureAdjustmentsAgreementData(
  agreementData,
  agreementAjustment,
  treasuryAgreementAjustment
) {
  if (agreementData.exposureAdjustments != null && agreementData.exposureAdjustments != undefined) {
    let agreementAdjustmentCount = agreementAjustment.length;
    let treasuryAgreementAdjustmentCount = treasuryAgreementAjustment.length;
    for (let m = 0; m < agreementData.exposureAdjustments.length; m++) {
      agreementAjustment[agreementAdjustmentCount] = agreementData.exposureAdjustments[m];
      agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
      agreementAdjustmentCount++;
      treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] =
        agreementData.exposureAdjustments[m];
      treasuryAgreementAjustment[
        treasuryAgreementAdjustmentCount
      ].id = treasuryAgreementAdjustmentCount;
      treasuryAgreementAdjustmentCount++;
    }
  }
}

function addRequiredAdjustmentsAgreementData(
  agreementData,
  agreementAjustment,
  treasuryAgreementAjustment
) {
  if (
    agreementData.requirementAdjustments != null &&
    agreementData.requirementAdjustments != undefined
  ) {
    let agreementAdjustmentCount = agreementAjustment.length;
    let treasuryAgreementAdjustmentCount = treasuryAgreementAjustment.length;
    for (let m = 0; m < agreementData.requirementAdjustments.length; m++) {
      agreementAjustment[agreementAdjustmentCount] = agreementData.requirementAdjustments[m];
      agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
      agreementAdjustmentCount++;
      treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] =
        agreementData.requirementAdjustments[m];
      treasuryAgreementAjustment[
        treasuryAgreementAdjustmentCount
      ].id = treasuryAgreementAdjustmentCount;
      treasuryAgreementAdjustmentCount++;
    }
  }
}

export function closeWorkflowWindow(
  actionableAgreementId: number,
  workflowActionType: WORKFLOW_OPERATION,
  agreementDetails = {},
  checkAndReload?: boolean
) {
  // Close the workflow window
  window.focus();

  if (
    workflowActionType &&
    workflowActionType != WORKFLOW_OPERATION.QUICK_PUBLISH &&
    workflowActionType != WORKFLOW_OPERATION.QUICK_WIRE_PUBLISH
  ) {
    let treasuryAgreementId = actionableAgreementId;

    let agreementDetail = agreementDetails[treasuryAgreementId]
      ? agreementDetails[treasuryAgreementId]
      : {};
    agreementDetails[treasuryAgreementId] = agreementDetail;

    if (workflowActionType === WORKFLOW_OPERATION.CANCEL) {
      checkAndReload = false;
    } else if (workflowActionType == WORKFLOW_OPERATION.SAVE) {
      agreementDetail['workflowStatus'] = WORKFLOW_STATUS_TYPE.IN_PROGRESS;
    } else if (workflowActionType == WORKFLOW_OPERATION.PUBLISH) {
      agreementDetail['workflowStatus'] = WORKFLOW_STATUS_TYPE.PUBLISHED;
    }

    window.opener.treasury.comet.agreementsummary.reloadAgreementSummary(
      checkAndReload,
      agreementDetails
    );

    window.close();
  }
}

export function getAdjustmentDetails(agreementIdToAdjustmentMap: Map<number, AdjustmentData[]>) {
  let isAdjustmentAddedInWorkflow = false;
  let maxKnowledgeTime;
  Object.keys(agreementIdToAdjustmentMap).forEach(function (key) {
    let adjustments = agreementIdToAdjustmentMap[key];
    if (adjustments != undefined && adjustments.length > 0) {
      for (let i = 0; i < adjustments.length; i++) {
        if (adjustments[i].knowledgeTime == undefined) {
          isAdjustmentAddedInWorkflow = true;
          break;
        } else {
          let knowledgeTime = new Date(adjustments[i].knowledgeTime);
          if (maxKnowledgeTime == null || knowledgeTime > maxKnowledgeTime) {
            maxKnowledgeTime = knowledgeTime;
          }
        }
      }
    }
  });

  let adjustmentDetail = {
    isAdjustmentAddedInWorkflow: isAdjustmentAddedInWorkflow,
    maxKnowledgeTime: maxKnowledgeTime,
  };

  return adjustmentDetail;
}

export const getDisputeAmountList = (topPanelGridData) => {
  return getFieldDataList(topPanelGridData, 'disputeAmountRC');
};

export const getACMList = (topPanelGridData) => {
  return getFieldDataList(topPanelGridData, 'acmRC');
};

export const getASMList = (topPanelGridData) => {
  return getFieldDataList(topPanelGridData, "asmRC")
};

const getFieldDataList = (topPanelGridData, fieldKey: string) => {
  let agreementMarginCallTypeFieldDataList = new Array();

  topPanelGridData.forEach((value) => {
    let agreementId = value.agreementId;
    //To handle cases for actionable ISDA
    if (agreementId < 0) {
      agreementId = -1 * agreementId;
    }
    let marginCallType = value.tag;
    //Added for MNA
    if (marginCallType == null || marginCallType == undefined || marginCallType == "") {
      marginCallType = "HOUSE"
    }
    if (value.isActionableAgreement) {
      let disputeAmountAndMovementData : AgreementMarginCallTypeAmountData = {
        agreementId: agreementId,
        marginCallType: marginCallType,
        amountString: getZeroIfUndefined(value[fieldKey]).toString()
      }
      agreementMarginCallTypeFieldDataList.push(disputeAmountAndMovementData);
    }

  });
  return agreementMarginCallTypeFieldDataList
};

export const getPublishWorkflowParams = (workflowOperation: string, workflowId: number, actionableAgreementId: number,
  agreementId?: number, wireAmount?: string, marginCallType?: string, topPanelGridData?) => {

  let disputeAmounts = Array()
  let acms = Array()
  let asms = Array()

  if (workflowOperation === WORKFLOW_OPERATION.QUICK_WIRE_PUBLISH) {
    let acmData : AgreementMarginCallTypeAmountData = {
      agreementId: agreementId!!,
      marginCallType: marginCallType!!,
      amountString: getZeroIfUndefined(wireAmount).toString()
    }
    acms.push(acmData)
  }
  else if (workflowOperation != WORKFLOW_OPERATION.QUICK_PUBLISH) {
    disputeAmounts = getDisputeAmountList(topPanelGridData);
    acms = getACMList(topPanelGridData);
    asms = getASMList(topPanelGridData);

  }

  let publishWorkflowParams : PublishWorkflowParams = {
    workflowOperation: workflowOperation,
    workflowId: workflowId,
    actionableAgreementId: actionableAgreementId,
    disputeAmounts: disputeAmounts,
    acms: acms,
    asms: asms
  };

  return JSON.stringify(publishWorkflowParams)
}

export const getWireIdsForCurrentWorkflow = (topPanelGridData) => {
  let wireIds;
  let wireIdsArray: string[] = [];

  topPanelGridData.forEach((value) => {
    if (value.wireRequest && value.wireRequest != '') {
      wireIdsArray.push(value.wireRequest);
    }
  });

  if (wireIdsArray.length > 0) {
    wireIds = wireIdsArray.join(',');
  }

  return wireIds;
};

export const getBookingIdsForCurrentWorkflow = (topPanelGridData) => {
  let bookingIds;
  let bookingIdsArray: String[] = [];

  topPanelGridData.forEach((value) => {
    if (value.bookingReference && value.bookingReference != '') {
      bookingIdsArray.push(value.bookingReference);
    }
  });

  if (bookingIdsArray.length > 0) {
    bookingIds = bookingIdsArray.join(',');
  }

  return bookingIds;
};

export const getSegACM = (topPanelGridData) => {
  let segACMRC = 0;

  topPanelGridData.forEach((value) => {
    if (value.isActionableAgreement && value.tag == 'SEG') {
      segACMRC += isNaN(value.acmRC) ? 0 : Number(value.acmRC);
    }
  });

  return segACMRC;
};

export const getSegASM = (topPanelGridData) => {

  let segASMRC = 0;

  topPanelGridData.forEach(value => {
    if (value.isActionableAgreement && value.tag == "SEG") {
      segASMRC += isNaN(value.asmRC) ? 0 : Number(value.asmRC);
    }
  })

  return segASMRC;
};

export const getSegDisputeAmount = (topPanelGridData) => {
  let segDisputeAmountRC = 0;

  topPanelGridData.forEach((value) => {
    if (value.isActionableAgreement && value.tag == 'SEG') {
      segDisputeAmountRC += isNaN(value.disputeAmountRC) ? 0 : Number(value.disputeAmountRC);
    }
  });

  return segDisputeAmountRC;
};

export const getAgreementDetails = (
  adjustmentKnowledgeTime,
  topPanelGridData,
  actionableAgreementId
) => {
  let wireIds = getWireIdsForCurrentWorkflow(topPanelGridData);
  let bookingIds = getBookingIdsForCurrentWorkflow(topPanelGridData);
  let agmtIdToACMMap = getACMList(topPanelGridData);
  let agmtIdToASMMap = getASMList(topPanelGridData);
  let agmtIdToDisputeAmountMap = getDisputeAmountList(topPanelGridData);
  let segECMRC = getSegACM(topPanelGridData);
  let segASMRC = getSegASM(topPanelGridData);
  let segDisputeAmountRC = getSegDisputeAmount(topPanelGridData);
  let agreementDetails = {};
  let agreementDetail = {};
  let treasuryAgreementId = actionableAgreementId;
  agreementDetails[treasuryAgreementId] = agreementDetail;
  agreementDetail['wireRequest'] = wireIds;
  agreementDetail['bookingReference'] = bookingIds;
  let ecmRC = agmtIdToACMMap[treasuryAgreementId];
  let asmRC = agmtIdToASMMap[treasuryAgreementId];
  let disputeAmountRC = agmtIdToDisputeAmountMap[treasuryAgreementId];
  agreementDetail['ecmRC'] = ecmRC;
  agreementDetail["asmRC"] = asmRC;
  agreementDetail['disputeAmountRC'] = disputeAmountRC;
  agreementDetail['segECMRC'] = segECMRC;
  agreementDetail["segASMRC"] = segASMRC;
  agreementDetail['segDisputeAmountRC'] = segDisputeAmountRC;
  agreementDetail['adjustmentKnowledgeTime'] = adjustmentKnowledgeTime;
  return agreementDetails;
};

function getZeroIfUndefined(inputVal) {
  return Number((inputVal == null || isNaN(inputVal)) ? 0 : inputVal)
}
