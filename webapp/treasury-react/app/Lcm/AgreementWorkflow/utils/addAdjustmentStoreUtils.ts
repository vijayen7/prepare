export const getAdjustmentDialogProps = (workflowId: number, sourceGridConfig: string, agreementId: number, startDataStr: string, legalEntityId: number,
  counterPartyEntityId: number, agreementTypeId: number, calculator?: string,
  custodianAccount?: string, pnlSpn?: number, custodianAccountId?: string, entityFamilyId?: number,
  actionableType?: string, businessUnitId?: number, bookId?: number, reportingCurrency?: string, tag?: string) => {
  return {
    startDate: startDataStr,
    selectedAgreementId: agreementId,
    legalEntityId: legalEntityId,
    counterPartyEntityId: counterPartyEntityId,
    agreementTypeId: agreementTypeId,
    calculator: calculator,
    caName: custodianAccount,
    caId: custodianAccountId,
    pnlSpn: pnlSpn,
    entityFamilyId: entityFamilyId,
    actionableType: actionableType,
    businessUnitId: businessUnitId,
    bookId: bookId,
    reportingCurrency: reportingCurrency,
    workflowInstanceId: workflowId,
    sourceGridConfig: sourceGridConfig,
    tag: tag
  }
}
