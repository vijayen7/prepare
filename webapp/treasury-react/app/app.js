import "@babel/polyfill";
import DecisionDrivingDashboard from "DecisionDrivingDashboard";
import MarketData from "MarketData";
import AvailabilityData from "AvailabilityData";
import LendReports from "LendReports";
import DetailReport from "DetailReport";
import OpsDashboard from "Financing/OpsDashboard";
import RateNegotiationVerification from "RateNegotiationVerification";
import RateNegotiationSavings from "RateNegotiationSavings";
import Outperformance from "Outperformance";
import { Provider as MobxProvider } from "mobx-react";
import React, { Suspense, lazy } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Route, Switch } from "react-router";
import { ConnectedRouter } from "react-router-redux";
import ReturnOnAssets from "ReturnOnAssets";
import Treasury from "Treasury";
import OnboardingUI from "OnboardingUI";
import history from "./customHistory";
import mobxStore from "./mobx-store";
import store from "./store";
import CashManagementReport from "CashManagementReport";
import CashManagementWorkflow from "CashManagementWorkflow";
import CashManagementThresholds from "CashManagementThresholds";
import AanaReport from "Aana/AanaReport";
import AanaRuleSystem from "Aana/AanaRuleSystem";
import FinancingReporting from "FinancingReporting";
import CofiRepoFinancing from "CofiRepoFinancing";
import CounterPartyGrouping from "CounterPartyGrouping";
import CashProjection from "./CashProjection";
import CashProjectionConfig from "./CashProjectionConfig";
import OutperformanceFileConfig from "OutperformanceFileConfig";
import BrokerCashBalance from "BrokerCashBalance";
import PositionDetailReport from "./PositionDetailReport";
import Indebtedness from "Indebtedness";
import MarginRules from "MarginRules";
import IndependentAmountRules from "./MarginScreens/IndependentAmountRules";
import TradeIndependentAmount from "./MarginScreens/TradeIndependentAmount";
import BestBorrowFinancing from "BorrowFinancing";
import DividendEnhancement from "DividendEnhancement";
import LiquidityManagementReport from "./LiquidityManagementScreens/LiquidityManagementReport"
import LiquidityManagementWorkflowStatus from "./LiquidityManagementScreens/WorkflowStatus";
import GovtBondBalancesWorkflow from "./LiquidityManagementScreens/GovtBondBalancesWorkflow";
import LiquidityManagementConfig from "./LiquidityManagementScreens/LiquidityManagementConfig";
import SummaryDashboard from "./LiquidityManagementScreens/SummaryDashboard";

const AgreementSummary = lazy(() => import('./AgreementSummary/index'));
const AgreementWorkflow = lazy(() => import('./Lcm/AgreementWorkflow/index'));
const PhysicalCollateralRules = lazy(() => import('./PhysicalCollateralRules/index'));
const Adjustments = lazy(() => import('./Lcm/Adjustments/index'));
const WorkflowStatus = lazy(() => import('./WorkflowStatus/index'));
const CollateralTerms = lazy(() => import('./CollateralTerms/index'));
const BrokerData = lazy(() => import('./BrokerData/index'));
const RagRuleData = lazy(() => import('./RagRuleData/index'));
const EntityLiquidityData = lazy(() => import('./EntityLiquidityData/index'));


const render = () => {
  ReactDOM.render(
    <MobxProvider {...mobxStore}>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Suspense fallback={<div>Loading...</div>}>
           <Switch>
              <Route exact path="/" component={Treasury} />
              <Route
                path="/treasury/lcm/workflowStatus.html"
                component={WorkflowStatus}
              />
              <Route
                path="/treasury/comet/brokerdata.html"
                component={BrokerData}
              />
              <Route
                path="/treasury/comet/collateralTerms.html"
                component={CollateralTerms}
              />
              <Route
                path="/treasury/lcm/adjustments.html"
                component={Adjustments}
              />
              <Route
                path="/treasury/comet/ragrules.html"
                component={RagRuleData}
              />
              <Route
                path="/treasury/lcm/physicalCollateralRules.html"
                component={PhysicalCollateralRules}
              />
              <Route
                path="/treasury/lcm/entityLiquidityData.html"
                component={EntityLiquidityData}
              />
              <Route
                path="/treasury/comet/agreementWorkflow.html"
                component={AgreementWorkflow}
              />
              <Route
                path="/treasury/comet/agreementSummary.html"
                component={AgreementSummary}
              />
              <Route
                path="/treasury/seclend/rateNegotiationVerification.html"
                component={RateNegotiationVerification}
              />
              <Route
                path="/treasury/seclend/rateNegotiationSavings.html"
                component={RateNegotiationSavings}
              />
              <Route
                path="/treasury/detailReport.html"
                component={DetailReport}
              />
              <Route
                path="/treasury/cashProjection.html"
                component={CashProjection}
              />
              <Route
                path="/treasury/cashProjectionConfig.html"
                component={CashProjectionConfig}
              />
              <Route
                path="/treasury/financing/opsdashboard.html"
                component={OpsDashboard}
              />
              <Route
                path="/treasury/seclend/marketdata.html"
                component={MarketData}
              />
              <Route
                path="/treasury/seclend/availabilitydata.html"
                component={AvailabilityData}
              />
              <Route
                path="/treasury/seclend/lendReportData.html"
                component={LendReports}
              />
              <Route
                path="/treasury/seclend/outperformance.html"
                component={Outperformance}
              />
              <Route
                path="/treasury/seclend/outperformanceFileConfig.html"
                component={OutperformanceFileConfig}
              />
              <Route
                path="/treasury/financing/cofiRepoFinancing.html"
                component={CofiRepoFinancing}
              />
              <Route
                path="/treasury/financing/onboarding.html"
                component={OnboardingUI}
              />
              <Route
                path="/treasury/financing/currencyBalanceReport.html"
                component={BrokerCashBalance}
              />
              <Route
                path="/treasury/indebtedness.html"
                component={Indebtedness}
              />
              <Route
                path="/treasury/optimization-opportunities-dashboard.html"
                component={DecisionDrivingDashboard}
              />
              <Route
                path="/treasury/counterpartyRelationship/return-on-assets.html"
                component={ReturnOnAssets}
              />
              <Route
                path="/treasury/seclend/bestBorrow.html"
                component={BestBorrowFinancing}
              />
              <Route
                path="/treasury/margin/marginRules.html"
                component={MarginRules}
              />
              <Route
                path="/treasury/margin/independentAmountRules.html"
                component={IndependentAmountRules}
              />
              <Route
                path="/treasury/margin/tradeIndependentAmount.html"
                component={TradeIndependentAmount}
              />
              <Route
                path="/treasury/liquidityManagement/liquidityManagementReport.html"
                component={LiquidityManagementReport}
              />
              <Route
                path="/treasury/liquidityManagement/workflowStatus.html"
                component={LiquidityManagementWorkflowStatus}
              />
              <Route
                path="/treasury/liquidityManagement/govtBondHoldings.html"
                component={GovtBondBalancesWorkflow}
              />
              <Route
                path="/treasury/liquidityManagement/configuration.html"
                component={LiquidityManagementConfig}
              />
              <Route
                path="/treasury/liquidityManagement/summary.html"
                component={SummaryDashboard}
              />
              <Route
                path="/treasury/lcm/cashManagementReport.html"
                component={CashManagementReport}
              />
              <Route
                path="/treasury/lcm/cashManagementWorkflow.html"
                component={CashManagementWorkflow}
              />
              <Route
                path="/treasury/lcm/cashManagementThresholds.html"
                component={CashManagementThresholds}
              />
              <Route
                path="/treasury/aana/aanaReport.html"
                component={AanaReport}
              />
              <Route
                path="/treasury/aana/AanaRuleSystem.html"
                component={AanaRuleSystem}
              />
              <Route
                path="/treasury/comet/positiondetailreport.html"
                component={PositionDetailReport}
              />
              <Route
                path="/treasury/financing/reporting/counterparty-reporting.html"
                component={FinancingReporting}
              />
              <Route
                path="/treasury/financing/reporting/counterparty-grouping.html"
                component={CounterPartyGrouping}
              />
              <Route
                path="/treasury/seclend/dividend-enhancement.html"
                component={DividendEnhancement}
              />
              <Route path="/treasury" component={Treasury} />
            </Switch>
          </Suspense>
        </ConnectedRouter>
      </Provider>
    </MobxProvider>,
    document.getElementById("app")
  );
};

WebComponents.waitFor(() => {
  render();
});
