import {
  marketDataPositionTypeFormatter
} from "commons/grid/formatters";
import { convertJavaNYCDate } from "commons/util";
var columns = [];

export function securityMarketDataColumns(cpeId) {
  var columns = [
    {
      id: "sourceDate",
      name: "Source Date",
      field: "sourceDate",
      toolTip: "Source Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "date",
      name: "Date",
      field: "date",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDate(value);
      },
      headerCssClass: "b",
      width: 70
    },
    {
      id: "positionType",
      name: "Position Type",
      field: "positionType",
      toolTip: "Position Type",
      type: "text",
      filter: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (dataContext.isAvailabilityData) {
          return "<div class='green6' style='width:100%;height:100%'></div>";
        } else {
          return "<div class='red7' style='width:100%;height:100%'></div>";
        }
      },
      excelDataFormatter: function(value, dataContext) {
        if (dataContext.isAvailabilityData) {
          return "Availability";
        } else {
          return "Borrow";
        }
      },
      sortable: true,
      headerCssClass: "b",
      width: 30
    },
    {
      id: "book",
      name: "Book",
      field: "bookName",
      toolTip: "Book",
      type: "text",
      filter: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      sortable: true,
      headerCssClass: "b",
      minWidth: 50
    },
    {
      id: "legalEntity",
      name: "Legal Entity",
      field: "legalEntityName",
      toolTip: "Legal Entity",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      headerCssClass: "b",
      minWidth: 50
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      headerCssClass: "b",
      minWidth: 50
    },
    {
      id: "currency",
      name: "CCY",
      field: "ccyName",
      toolTip: "Currency",
      type: "text",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50
    },
    {
      id: "sedol",
      name: "SEDOL",
      field: "sedol",
      toolTip: "SEDOL",
      type: "text",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 60
    },
    {
      id: "securityName",
      name: "Security Name",
      field: "securityName",
      toolTip: "Security Name",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 50
    },
    {
      id: "quantity",
      name: "Quantity",
      field: "quantity",
      toolTip: "Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId,
          "number"
        );
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 90
    },
    {
      id: "mvUsd",
      name: "Market Value (USD)",
      field: "marketValueUsd",
      toolTip: "MV (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId,
          "number"
        );
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 90
    },
    {
      id: "rebateRate",
      name: "Rebate Rate(%)",
      field: "rebateRate",
      toolTip: "Rebate Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId,
          "float"
        );
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "stockLoanFeeRate",
      name: "SLF Rate(%)",
      field: "stockLoanFeeRate",
      toolTip: "SLF Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId,
          "float"
        );
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "financingFeeRate",
      name: "Financing Fee Rate(%)",
      field: "financingFeeRate",
      toolTip: "Financing Fee Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId,
          "float"
        );
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "spn",
      name: "SPN",
      field: "spn",
      toolTip: "SPN",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      headerCssClass: "b",
      width: 80
    },
    {
      id: "instrumentType",
      name: "C/S/R",
      field: "instrumentType",
      toolTip: "C/S/R",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      headerCssClass: "b",
      width: 60
    },
    {
      id: "classification",
      name: "Class",
      field: "classification",
      toolTip: "Class",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      headerCssClass: "b",
      minWidth: 50
    },
    {
      id: "borrowType",
      name: "B/L/R",
      field: "borrowType",
      toolTip: "B/L/R",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return marketDataPositionTypeFormatter(
          row,
          cell,
          value,
          columnDef,
          dataContext,
          cpeId
        );
      },
      headerCssClass: "b",
      width: 60
    }
  ];

  return columns;
}
