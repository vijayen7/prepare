export function securityMarketDataGridOptions() {
    var options = {
      autoHorizontalScrollBar: true,
      applyFilteringOnGrid: false,
      exportToExcel: true,
      sortList: [
        {
          columnId: "description",
          sortAsc: true
        }
      ],
      onRowClick: function() {
        // highlight on row works if this callback is defined
      },
      highlightRowOnClick: true,
    };
    return options;
  }