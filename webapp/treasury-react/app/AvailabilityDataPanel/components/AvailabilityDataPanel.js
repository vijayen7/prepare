import React, { Component } from "react";
import { TabPanel } from "arc-react-components";
import { securityMarketDataColumns } from "../grid/columnConfig";
import { securityMarketDataGridOptions } from "../grid/gridOptions";
import GridContainer from "commons/components/GridWithCellClick";
import DrillDownLegend from "../components/DrillDownLegend";
import Message from "commons/components/Message";
import { Layout } from "arc-react-components";

export default class AvailabilityDataPanel extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.state = { resizeCanvas: false };
  }

  onSelect() {
    this.setState({ resizeCanvas: !this.state.resizeCanvas });
  }

  render() {
    let clientName = CODEX_PROPERTIES["codex.client_name"];

    if (clientName !== "desco" && clientName !== "deshaw") {
      return getSecurityMarketDataGrid(
        this.props.data,
        this.props.selectedCpeIdForDrillDown,
        "SecurityMarketData",
        this.state.resizeCanvas
      );
    }

    let securityMarketDataByCpeType = _.groupBy(
      this.props.data,
      "isAltFundCpe"
    );

    return (
      <TabPanel onSelect={this.onSelect}>
        <TabPanel.Tab label="PB Counterparties" id="pbCpe">
          {securityMarketDataByCpeType[false] &&
          securityMarketDataByCpeType[false].length > 0 ? (
            getSecurityMarketDataGrid(
              securityMarketDataByCpeType[false],
              this.props.selectedCpeIdForDrillDown,
              "PbCounterpartySecurityMarketData",
              this.state.resizeCanvas
            )
          ) : (
            <Message messageData="Selected position not found." />
          )}
        </TabPanel.Tab>
        <TabPanel.Tab label="AltFund Counterparties" id="altfundCpe">
          {securityMarketDataByCpeType[true] &&
          securityMarketDataByCpeType[true].length > 0 ? (
            getSecurityMarketDataGrid(
              securityMarketDataByCpeType[true],
              this.props.selectedCpeIdForDrillDown,
              "AltFundCounterpartySecurityMarketData",
              this.state.resizeCanvas
            )
          ) : (
            <Message messageData="Selected position not found." />
          )}
        </TabPanel.Tab>
      </TabPanel>
    );
  }
}

function getSecurityMarketDataGrid(
  data,
  selectedCpeIdForDrillDown,
  gridId,
  resizeCanvas
) {
  return (
    <Layout>
      <Layout.Child size={3}>
        <GridContainer
          data={data}
          gridId={gridId}
          gridColumns={securityMarketDataColumns(selectedCpeIdForDrillDown)}
          gridOptions={securityMarketDataGridOptions()}
          resizeCanvas={resizeCanvas}
          fill={true}
        />
      </Layout.Child>
      <Layout.Child size={1}>
        <DrillDownLegend />
      </Layout.Child>
    </Layout>
  );
}
