import React from "react";

export default class DrillDownLegend extends React.Component {
  render() {
    return (
      <div className="padding">
        <div className="legend">
          <div className="legend__item">
            <div className="legend__item__swatch green6" />
            <span className="margin--left--small margin--right--double">
              Availability Data as of T
            </span>
          </div>
          <div className="legend__item">
            <div className="legend__item__swatch red7" />
            <span className="margin--left--small margin--right--double">
              Borrow Data as of T-1
            </span>
          </div>
          <div className="legend__item">
            <span className="margin--left--small margin--right--double">
              The row shown in
              <span style={{ color: "#20B2AA", fontStyle: "italic" }}>
                {" "}
                green italics{" "}
              </span>
              indicates borrow data corresponding to the counterparty entity for
              the selected position in the above grid
            </span>
          </div>
        </div>
      </div>
    );
  }
}
