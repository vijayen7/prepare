import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SideBar from "./containers/SideBar";
import Loader from "commons/container/Loader";
import Grid from "./containers/Grid";

export default class OutperformanceFileConfig extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="layout--flex--row">
        <arc-header
          ref={header => {
            this.header = "";
          }}
          className="size--content"
          user={USER}
          modern-themes-enabled
        />
        <Loader /><br/>
        <div className="layout--flex">
          <div>
            <SideBar />
          </div>
          <div className="size--5 padding--horizontal--double">
            <div className="size--content">
                <Grid />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
