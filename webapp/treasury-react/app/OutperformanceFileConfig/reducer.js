import {
  FETCH_ACTIVE_FILE_PARSERS_DATA,
  DESTROY_ACTIVE_FILE_PARSERS_DATA,
  ADD_FILE_PARSER_DATA,
  INVALIDATE_FILE_PARSER_DATA,
  REMOVE_INVALIDATION_MESSAGE,
  REMOVE_ADD_FILE_ALIAS_MESSAGE
} from "./constants";

import {
  UPDATE_DRILL_DOWN_SELECTED_FILTERS,
  RESIZE_CANVAS,
} from "commons/constants";

import {
  combineReducers
} from "redux";

function activeOutperformanceFilesDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_ACTIVE_FILE_PARSERS_DATA}_SUCCESS`:
      if(action.data.length <= 0) {
        action.message = 'No data available for selected search parameters';
      }
      return action || {};
    case DESTROY_ACTIVE_FILE_PARSERS_DATA:
      return [];
  }
  return state;
}

function drillDownSelectedFilterReducer(state = {}, action) {
  switch (action.type) {
    case `${UPDATE_DRILL_DOWN_SELECTED_FILTERS}_SUCCESS`:
      return action.selectedFilters || {};
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

function addOutperformanceFileAliasReducer(state = [], action) {
  switch(action.type) {
    case `${ADD_FILE_PARSER_DATA}_SUCCESS`:
    var data = {
      'addFileAliasStatus' : action.addFileAliasdata['@CLASS'].includes('Exception') ? false : true
    };
    return data || {};
    case REMOVE_ADD_FILE_ALIAS_MESSAGE:
        return {};
  }
  return state;
}

function invalidateFileAliasReducer(state = [], action) {
  switch(action.type) {
    case `${INVALIDATE_FILE_PARSER_DATA}_SUCCESS`:
    var data = {
      'invalidationStatus' : action.data['@CLASS'].includes('Exception') ? false : true
    };
    return data || {};
    case REMOVE_INVALIDATION_MESSAGE:
        return {};
  }
  return state;
}

const rootReducer = combineReducers({
  activeOutperformanceFilesData: activeOutperformanceFilesDataReducer,
  resizeCanvas: resizeCanvasReducer,
  outperformanceFileAliasData: addOutperformanceFileAliasReducer,
  drillDownFilters: drillDownSelectedFilterReducer,
  invalidatedFileAliasData: invalidateFileAliasReducer
});

export default rootReducer;
