import { BASE_URL, INPUT_FORMAT_JSON } from 'commons/constants'

export let url = "";

export function getActiveOutperformanceFiles(payload) {
  url = `${BASE_URL}service/outperformanceFileAliasService/getActiveOutperformanceFiles?date="${payload.date}"${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function addOutperformanceFileAlias(payload) {
  url = `${BASE_URL}service/outperformanceFileAliasService/addOutperformanceFileAlias?effectiveBeginDate="${payload.effectiveBeginDate}"&fileAlias=${payload.fileAlias}&filePattern=${payload.filePattern}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function removeFileAlias(payload) {
  url = `${BASE_URL}service/outperformanceFileAliasService/invalidateOutperformanceFileAlias?fileAliasId=${payload.fileAliasId}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}
