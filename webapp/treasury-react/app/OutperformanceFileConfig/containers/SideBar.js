import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
import DateFilter from "commons/container/DateFilter";
import { getCommaSeparatedValuesOrNull } from "commons/util";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import {
  fetchActiveFileParsersData,
  destroyActiveFileParsersData,
  updateDrillDownSelectedFilters
} from "../actions";
import {
  getPreviousDate
} from "commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.state = this.getDefaultFilters();
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  componentDidMount() {
    this.setState({
      selectedDate : this.props.selectedFilters.selectedDate
    });
  }
  getDefaultFilters() {
    return {
      selectedDate: getPreviousDate(),
    };
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
   this.props.updateDrillDownSelectedFilters(this.state);
   this.props.destroyActiveFileParsersData();
   var payload = {
     date: this.state.selectedDate
   };
   this.props.fetchActiveFileParsersData(payload);
 }

  render() {
    let sidebar = (<Sidebar>
      <DateFilter
        onSelect={this.onSelect}
        stateKey="selectedDate"
        data={this.state.selectedDate}
      />
      <ColumnLayout>
        <FilterButton
          onClick={this.handleClick}
          reset={false}
          label="Search"
        />
        <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
      </ColumnLayout>
    </Sidebar>);
    return sidebar;
  }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
      {
        fetchActiveFileParsersData,
        destroyActiveFileParsersData,
        updateDrillDownSelectedFilters
      },
      dispatch
    );
}

function mapStateToProps(state) {
  return {
    selectedFilters: state.outperformanceFileConfig.drillDownFilters
  };
}

SideBar.propTypes = {
  fetchActiveFileParsersData: PropTypes.func,
  destroyActiveFileParsersData: PropTypes.func,
  updateDrillDownSelectedFilters: PropTypes.func,
  selectedFilters: PropTypes.array
};

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
