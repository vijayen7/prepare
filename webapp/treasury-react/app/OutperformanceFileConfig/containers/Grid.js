import React, { Component } from "react";
import { Layout } from 'arc-react-components';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Dialog from "commons/components/Dialog";
import OutperformanceFileAliasDataGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import InputFilter from "commons/components/InputFilter";
import DateFilter from "commons/container/DateFilter";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import {
  outperformanceFileAliasGridColumns
} from "../grid/columnConfig";
import {
  outperformanceFileAliasGridOptions
} from "../grid/gridOptions";
import {
  validateInputEmpty,
  getStatusMessage
} from "../../commons/util";
import {
  addOutperformanceFileAliasData,
  destroyActiveFileParsersData,
  fetchActiveFileParsersData,
  invalidateFileAliasData,
  removeInvalidationMessage,
  removeAddFileAliasMessage
} from "../actions";
import {
  getPreviousDate
} from "commons/util";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.onAddFileParserData = this.onAddFileParserData.bind(this);
    this.updateAddFileAliasStatus = this.updateAddFileAliasStatus.bind(this);
    this.loadActionMessageDialogs = this.loadActionMessageDialogs.bind(this);
    this.onDeleteFileAlias = this.onDeleteFileAlias.bind(this);
    this.state = {};
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      showModal: false,
      selectedFileAlias: "",
      selectedFilePattern: "",
      selectedDate: '',
      selectedEffectiveBeginDate: getPreviousDate(),
      outperformanceFileAliasData: {},
      invalidatedFileAliasData: {}
    };
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  onDeleteFileAlias = args => {
    if(args.colId == "actions") {
      var payload = {
        fileAliasId: args.item.id
      };
      this.props.invalidateFileAliasData(payload);
    }
  }

  updateDialogForAddFileAlias = () => {
    var updatedState = {
      showModal: true,
      selectedFileAlias: "",
      selectedFilePattern: "",
      selectedDate: '',
      selectedEffectiveBeginDate: getPreviousDate(),
      outperformanceFileAliasData: {},
      invalidatedFileAliasData: {}
    };
    this.setState(updatedState);
  }

  updateAddFileAliasStatus(addFileAliasMessage, addFileAliasMessageClass, addFileAliasMessageHidden) {
    let newState = Object.assign({}, this.state);
    var data = {
      'addFileAliasMessage' : addFileAliasMessage,
      'addFileAliasMessageClass': addFileAliasMessageClass,
      'addFileAliasMessageHidden': addFileAliasMessageHidden
    };
    newState['outperformanceFileAliasData'] = data;
    this.setState(newState);
  }

  onAddFileParserData() {
    this.updateAddFileAliasStatus('', '', true);

    if(this.state.selectedEffectiveBeginDate === undefined || this.state.selectedEffectiveBeginDate === null || this.state.selectedEffectiveBeginDate == "") {
      this.updateAddFileAliasStatus('Begin Date cannot be empty.', 'message--warning no-icon', false);
      return;
    }

    var fileAliasEmpty = validateInputEmpty(this.state.selectedFileAlias);
    var filePatternEmpty = validateInputEmpty(this.state.selectedFilePattern)
    if(fileAliasEmpty || filePatternEmpty) {
      this.updateAddFileAliasStatus('File Alias/Pattern cannot be empty.', 'message--warning no-icon', false);
      return;
    }

    var payload = {
      date: this.state.selectedDate,
      effectiveBeginDate: this.state.selectedEffectiveBeginDate,
      fileAlias: this.state.selectedFileAlias,
      filePattern: encodeURIComponent(this.state.selectedFilePattern)
    }
    this.updateAddFileAliasStatus('', '', true);
    this.props.addOutperformanceFileAliasData(payload);
  }

  onCloseModal = () => {
    this.props.removeInvalidationMessage({});
    this.props.removeAddFileAliasMessage({});
    this.setState(this.getDefaultFilters());
  }

  loadActionMessageDialogs() {
    var isDialogOpen = false;
    var messageTitle = '';
    var message = '';
    var errorStatus = false;
    var successStatus = true;
    if('addFileAliasStatus' in this.props.outperformanceFileAliasData) {
      isDialogOpen = true;
      if(this.props.outperformanceFileAliasData.addFileAliasStatus !== '' || this.props.outperformanceFileAliasData.addFileAliasStatus !== null) {
        message = 'File alias added successfully. Please trigger search to see updated list of active files.';
        messageTitle = 'SUCCESS';
      } else {
        message = 'Error while adding file alias.';
        messageTitle = 'ERROR';
        errorStatus = true;
        successStatus = false;
      }
    }

    if('invalidationStatus' in this.props.invalidatedFileAliasData) {
      isDialogOpen = true;
      if(this.props.invalidatedFileAliasData.invalidationStatus !== '' || this.props.invalidatedFileAliasData.invalidationStatus !== null) {
        message = 'File alias has been invalidated. Please trigger search to see updated list of active files.';
        messageTitle = 'SUCCESS';
      } else {
        message = 'Error while invalidating.';
        messageTitle = 'ERROR';
        errorStatusror = true;
        successStatus = false;
      }
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={isDialogOpen}
          title={messageTitle}
          onClose={this.onCloseModal} >
          <Message error={errorStatus} success={successStatus}
            messageData={message} />
          <FilterButton onClick={this.onCloseModal} reset={false} label='Ok' />
        </Dialog>
      </React.Fragment>
    );
  }

  render() {
    var mainContainer = '';
    if ('message' in this.props.activeOutperformanceFilesData)
      mainContainer = <Message messageData={this.props.activeOutperformanceFilesData.message} />;

    if(this.props.activeOutperformanceFilesData.length <= 0)
        mainContainer = <Message messageData="Search to load data" />;

    var message = (this.state.outperformanceFileAliasData.addFileAliasMessageHidden) ? '' : (<div className={this.state.outperformanceFileAliasData.addFileAliasMessageClass} >
            {this.state.outperformanceFileAliasData.addFileAliasMessage}  </div>);

    if(mainContainer === '' || this.state.showModal)
      mainContainer = (<React.Fragment><OutperformanceFileAliasDataGrid
                                         data={this.props.activeOutperformanceFilesData.data}
                                         gridId="OutperformanceFileAlias"
                                         onCellClick={this.onDeleteFileAlias}
                                         gridColumns={outperformanceFileAliasGridColumns()}
                                         gridOptions={outperformanceFileAliasGridOptions()}
                                       />
                                       <Dialog isOpen={this.state.showModal} title="Add file alias" onClose={this.onCloseModal}
                                         style={{ width: "80px", height: "400px" }}
                                       >
                                       {message}
                                       <DateFilter
                                         onSelect={this.onSelect}
                                         stateKey="selectedEffectiveBeginDate"
                                         data={this.state.selectedEffectiveBeginDate}
                                         label="Effective Begin Date"
                                       />
                                       <InputFilter
                                         onSelect={this.onSelect}
                                         stateKey="selectedFileAlias"
                                         label="File Alias"
                                         data={this.state.selectedFileAlias}
                                       />
                                       <InputFilter
                                         stateKey="selectedFilePattern"
                                         label="File Pattern"
                                         data={this.state.selectedFilePattern}
                                         onSelect={this.onSelect}
                                       />
                                       <ColumnLayout>
                                         <FilterButton
                                           onClick={this.onAddFileParserData}
                                           reset={false}
                                           label="Save"
                                         />
                                         <FilterButton reset={true} onClick={this.onCloseModal} label="Cancel" />
                                       </ColumnLayout>
                                       </Dialog>
                                       {this.loadActionMessageDialogs()}
                                       </React.Fragment>
                                       );                                       ;

      return  (<React.Fragment>
                <Layout>
                  <Layout.Child>
                    <FilterButton
                      onClick={this.updateDialogForAddFileAlias}
                      reset={false}
                      label="Add new File Alias"
                    />
                   </Layout.Child>
                   <Layout.Child>
                     {mainContainer}
                   </Layout.Child>
                 </Layout>
              </React.Fragment>);
  }
}

Grid.propTypes = {
  activeOutperformanceFilesData: PropTypes.object,
  selectedFilters: PropTypes.object,
  handleViewToggle: PropTypes.func,
  fetchActiveFileParsersData: PropTypes.func,
  destroyActiveFileParsersData: PropTypes.func,
  updateDrillDownSelectedFilters: PropTypes.func,
  addOutperformanceFileAliasData: PropTypes.func,
  outperformanceFileAliasData: PropTypes.array,
  invalidateFileAliasData: PropTypes.func,
  invalidatedFileAliasData: PropTypes.object,
  resetInvalidationMessage: PropTypes.object,
  resetAddFileMessage: PropTypes.object,
  removeInvalidationMessage: PropTypes.func,
  removeAddFileAliasMessage: PropTypes.func
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addOutperformanceFileAliasData,
      fetchActiveFileParsersData,
      destroyActiveFileParsersData,
      invalidateFileAliasData,
      removeInvalidationMessage,
      removeAddFileAliasMessage
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    activeOutperformanceFilesData: state.outperformanceFileConfig.activeOutperformanceFilesData,
    selectedFilters: state.outperformanceFileConfig.drillDownFilters,
    outperformanceFileAliasData: state.outperformanceFileConfig.outperformanceFileAliasData,
    invalidatedFileAliasData: state.outperformanceFileConfig.invalidatedFileAliasData,
    resetAddFileMessage: state.outperformanceFileConfig.resetAddFileMessage,
    resetInvalidationMessage: state.outperformanceFileConfig.resetInvalidationMessage
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
