import {
  RESIZE_CANVAS,
  UPDATE_DRILL_DOWN_SELECTED_FILTERS
} from "commons/constants";

import {
  FETCH_ACTIVE_FILE_PARSERS_DATA,
  DESTROY_ACTIVE_FILE_PARSERS_DATA,
  ADD_FILE_PARSER_DATA,
  INVALIDATE_FILE_PARSER_DATA,
  REMOVE_INVALIDATION_MESSAGE,
  REMOVE_ADD_FILE_ALIAS_MESSAGE
} from "./constants";

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

export function fetchActiveFileParsersData(payload) {
  return {
    type: FETCH_ACTIVE_FILE_PARSERS_DATA,
    payload
  };
}

export function destroyActiveFileParsersData() {
  return {
    type: DESTROY_ACTIVE_FILE_PARSERS_DATA
  };
}

export function addOutperformanceFileAliasData(payload) {
  return {
    type: ADD_FILE_PARSER_DATA,
    payload
  }
}

export function updateDrillDownSelectedFilters(payload) {
  return {
    type: UPDATE_DRILL_DOWN_SELECTED_FILTERS,
    payload
  };
}

export function invalidateFileAliasData(payload) {
  return {
    type: INVALIDATE_FILE_PARSER_DATA,
    payload
  };
}

export function removeInvalidationMessage() {
  return {
    type: REMOVE_INVALIDATION_MESSAGE
  };
}

export function removeAddFileAliasMessage() {
  return {
    type: REMOVE_ADD_FILE_ALIAS_MESSAGE
  };
}
