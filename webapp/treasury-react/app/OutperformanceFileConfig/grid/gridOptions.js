export function outperformanceFileAliasGridOptions(paging) {
    var options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [{ columnId: "fileAlias", sortAsc: true }],
    highlightRowOnClick: true,
    useAvailableScreenSpace : true,
  };
  return options;
}
