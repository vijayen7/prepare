import {
  linkFormatter
} from "commons/grid/formatters";

import { convertJavaNYCDashedDate } from "commons/util";

export function outperformanceFileAliasGridColumns() {
  var columns = [{
    id: "actions",
    name: "Actions",
    field: "actions",
    type: "text",
    filter: true,
    sortable: true,
    formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return linkFormatter(
          row,
          cell,
          "<i class='icon-delete' />",
          columnDef,
          dataContext
        );
    },
    headerCssClass: "b",
    width: 20
    },
    {
      id: "fileAlias",
      name: "File Alias",
      field: "fileAlias",
      toolTip: "File Alias",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 70
    },
    {
      id: "filePattern",
      name: "File Pattern",
      field: "filePattern",
      toolTip: "File Pattern",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "effectiveBeginDate",
      name: "Effective Begin Date",
      field: "effectiveBeginDate",
      toolTip: "Effective Begin Date",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 30,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDashedDate(value);
      }
    },
    {
      id: "effectiveEndDate",
      name: "Effective End Date",
      field: "effectiveEndDate",
      toolTip: "Effective End Date",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 30,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDashedDate(value);
      },
    }
  ];

  return columns;
}
