import { put, takeEvery, call, all } from "redux-saga/effects";

import {
  FETCH_ACTIVE_FILE_PARSERS_DATA,
  DESTROY_ACTIVE_FILE_PARSERS_DATA,
  ADD_FILE_PARSER_DATA,
  INVALIDATE_FILE_PARSER_DATA,
  REMOVE_INVALIDATION_MESSAGE,
  REMOVE_ADD_FILE_ALIAS_MESSAGE
} from "./constants";

import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
  UPDATE_DRILL_DOWN_SELECTED_FILTERS,
} from "commons/constants";

import {
  getActiveOutperformanceFiles,
  addOutperformanceFileAlias,
  removeFileAlias
} from "./api";

function* fetchActiveFileParsersData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getActiveOutperformanceFiles, action.payload);
    yield put({ type: `${FETCH_ACTIVE_FILE_PARSERS_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_ACTIVE_FILE_PARSERS_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* activeOutperformanceFilesData() {
  yield [
    takeEvery(FETCH_ACTIVE_FILE_PARSERS_DATA, fetchActiveFileParsersData)
  ];
}

function* addOutperformanceFileAliasData(action) {
  try {
    yield put({type: START_LOADING });
    const addFileAliasdata = yield call(addOutperformanceFileAlias, action.payload);
    yield put({ type: `${ADD_FILE_PARSER_DATA}_SUCCESS`, addFileAliasdata});
  } catch(e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${ADD_FILE_PARSER_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* outperformanceFileAliasData() {
  yield [
    takeEvery(ADD_FILE_PARSER_DATA, addOutperformanceFileAliasData),
  ];
}

function* invalidateFileAlias(action) {
  try {
    yield put({type: START_LOADING });
    const data = yield call(removeFileAlias, action.payload);
    yield put({ type: `${INVALIDATE_FILE_PARSER_DATA}_SUCCESS`, data});
  } catch(e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${INVALIDATE_FILE_PARSER_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* invalidateFileAliasData() {
  yield [
    takeEvery(INVALIDATE_FILE_PARSER_DATA, invalidateFileAlias)
  ];
}

function* outperformanceFileConfigSaga() {
  yield all([activeOutperformanceFilesData(), outperformanceFileAliasData(), invalidateFileAliasData()]);
}

export default outperformanceFileConfigSaga;
