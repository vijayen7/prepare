import { getNumericColumn, getTextColumn} from "./common/columnHelper"
import getColumnOptions from "./common/commonConfig"
import {
  linkFormatter
} from "commons/grid/formatters";

export default function getBundleColumns() {
  var columns = getColumnOptions();

  columns.push(getTextColumn("businessUnitName", "Business Unit Name", "businessUnitName", "Business Unit Name", 100));

  columns.push(getNumericColumn("netOptionValue", "Net Option Value", "netOptionValue", "Net Option Value"));

  return columns;
}
