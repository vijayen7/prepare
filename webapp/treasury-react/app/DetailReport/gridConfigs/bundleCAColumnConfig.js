import { getNumericColumn, getTextColumn } from "./common/columnHelper";
import getColumnOptions from "./common/commonConfig";
import { convertJavaNYCDashedDate } from "commons/util";
import { linkFormatter, groupFloatFormatter } from "commons/grid/formatters";
export default function getBundleColumns() {
  var columns = getColumnOptions();

  columns.push(
    getNumericColumn(
      "totalDailyFinancingIncomeCostLocal",
      "Total Daily Financing Income Cost (Local)",
      "totalDailyFinancingIncomeCostLBC",
      "Total Daily Financing Income Cost (Local)",
      dpGrid.Aggregators.sum
    )
  );

  columns.push(
    getNumericColumn(
      "totalDailyFinancingIncomeCostRC",
      "Total Daily Financing Income Cost (RC)",
      "totalDailyFinancingIncomeCostRC",
      "Total Daily Financing Income Cost (RC)",
      dpGrid.Aggregators.sum
    )
  );

  columns.push(
    getTextColumn(
      "businessUnitName",
      "Business Unit Name",
      "businessUnitName",
      "Business Unit Name",
      100
    )
  );

  columns.push(
    getTextColumn("bundle", "Bundle", "bundleDisplayName", "Bundle", 100)
  );

  columns.push(
    getTextColumn(
      "custodianAccountName",
      "Custodian Account",
      "custodianAccountName",
      "Custodian Account",
      100,
      250
    )
  );

  columns.push(
    getNumericColumn(
      "netOptionValue",
      "Net Option Value",
      "netOptionValue",
      "Net Option Value"
    )
  );

  columns.push(
    getTextColumn(
      "genevaBundleName",
      "Accounting Bundle Name",
      "genevaBundleDisplayName",
      "Accounting Bundle Name"
    )
  );

  columns.push(
    getTextColumn(
      "chargeType",
      "Charge Type",
      "chargeType",
      "Charge Type"
    )
  );

  columns.push(
    getTextColumn(
      "methodologyName",
      "Methodology",
      "methodologyName",
      "Methodology"
    )
  );

  columns.push(
    getTextColumn(
      "genevaCustodianAccountName",
      "Accounting Custodian Account Name",
      "genevaCustodianAccountName",
      "Accounting CustodianAccount Name"
    )
  );

  columns.push(getNumericColumn("spreadOnlyFinancingRC", "Spread Only Financing (RC)", "spreadOnlyFinancingRC", "Spread Only Financing (RC)", dpGrid.Aggregators.sum));
  columns.push(getNumericColumn("ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)", "ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)"));
  columns.push(getNumericColumn("treasuryNoSpreadRC", "Allocated Financing No Spread (RC)", "treasuryNoSpreadRC", "Allocated Financing No Spread (RC)"));
  columns.push(getNumericColumn("ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)", "ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)"));
  columns.push(getNumericColumn("treasuryNoBorrowRC", "Allocated Financing No Borrow (RC)", "treasuryNoBorrowRC", "Allocated Financing No Borrow (RC)"));

  columns.push({
    id: "drillThrough",
    name: "Drill Through",
    field: "",
    formatter: function(row, cell, value, columnDef, dataContext) {
      return linkFormatter(row, cell, "Bundle Pos", columnDef, dataContext);
    },
    toolTip: "Drill Through to Bundle Position",
    type: "text",
    headerCssClass: "b",
    minWidth: 50
  });

  let nonNumericalColumns = [
    "date",
    "agreementType",
    "legalEntityCodaCompCode",
    "cpeName",
    "nettingGroup",
    "currency",
    "knowledgeDate",
    "interestOnlyCustodianAccountName",
    "bundle",
    "agreementTermDetails",
    "fxRate",
    "isDesim",
    "businessUnitName",
    "netOptionValue",
    "genevaBundleName",
    "genevaCustodianAccountName",
    "drillThrough",
    "custodianAccountName"
  ];

  for (let column of columns) {

    if(column.id == "date") {
      column.groupingFormatter = function(group) {
          if(group != null && group != undefined) {
            return convertJavaNYCDashedDate(group.value || "");
          }
          return "N/A";
        };
    }

  if (!nonNumericalColumns.includes(column.id)) {
        column.groupingAggregator = "Sum";
        column.groupTotalsFormatter = groupFloatFormatter;
      }
  }
  return columns;
}
