import { getNumericColumn, getTextColumn} from "./common/columnHelper"
import getColumnOptions from "./common/commonConfig"
import {
  linkFormatter
} from "commons/grid/formatters";

export default function getBusinessUnitGroupColumns() {
  var columns = getColumnOptions();

  columns.push(getTextColumn("businessUnitGroupTypeName", "Business Unit Group Type Name", "businessUnitGroupTypeName", "Business Unit Group Type Name", 100));
  columns.push(getTextColumn("businessUnitGroupName", "Business Unit Group Name", "businessUnitGroupName", "Business Unit Group Name", 50));
  columns.push(getNumericColumn("ebValue", "EB Value", "ebValue", "EB Value"));
  columns.push(getNumericColumn("etbRate", "ETB Rate", "etbRate", "ETB Rate"));
  columns.push(getNumericColumn("gammaRatio", "Gamma Ratio", "gammaRatio", "Gamma Ratio"));
  columns.push(getNumericColumn("netOptionValue", "Net Option Value", "netOptionValue", "Net Option Value"));
  columns.push(getNumericColumn("spreadOnlyFinancingRC", "Spread Only Financing (RC)", "spreadOnlyFinancingRC", "Spread Only Financing (RC)", dpGrid.Aggregators.sum));
  columns.push(getNumericColumn("ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)", "ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)"));
  columns.push(getNumericColumn("treasuryNoSpreadRC", "Allocated Financing No Spread (RC)", "treasuryNoSpreadRC", "Allocated Financing No Spread (RC)"));
  columns.push(getNumericColumn("ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)", "ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)"));
  columns.push(getNumericColumn("treasuryNoBorrowRC", "Allocated Financing No Borrow (RC)", "treasuryNoBorrowRC", "Allocated Financing No Borrow (RC)"));

  columns.push({
    id: "drillThrough",
    name: "Drill Through",
    field: "",
    formatter: function(row, cell, value, columnDef, dataContext) {
      return linkFormatter(
        row,
        cell,
        "BU",
        columnDef,
        dataContext
      );
    },
    toolTip: "Drill Through to Business Unit",
    type: "text",
    headerCssClass: "b",
    minWidth: 50
  });

  return columns;
}
