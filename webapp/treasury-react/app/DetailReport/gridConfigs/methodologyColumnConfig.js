import {
  getNumericColumn,
  getTextColumn
} from "./common/columnHelper"
import getColumnOptions from "./common/commonConfig"
import {
  linkFormatter
} from "commons/grid/formatters";

export default function getMethodologyColumns() {
  var columns = getColumnOptions();

  columns.push(getNumericColumn("totalDailyFinancingIncomeCostLocal", "Total Daily Financing Income Cost (Local)", "totalDailyFinancingIncomeCostLBC", "Total Daily Financing Income Cost (Local)", dpGrid.Aggregators.sum));

  columns.push(getNumericColumn("totalDailyFinancingIncomeCostRC", "Total Daily Financing Income Cost (RC)", "totalDailyFinancingIncomeCostRC", "Total Daily Financing Income Cost (RC)", dpGrid.Aggregators.sum));

  columns.push(getNumericColumn("dailyFinanceCostLocal", "Daily Finance Cost (Local)", "dailyFinanceCost", "Daily Finance Cost (Local)"));

  columns.push(getNumericColumn("dailyFinanceCostRC", "Daily Finance Cost (RC)", "dailyFinanceCostRC", "Daily Finance Cost (RC)"));

  columns.push(getNumericColumn("spreadOnlyFinancingRC", "Spread Only Financing (RC)", "spreadOnlyFinancingRC", "Spread Only Financing (RC)", dpGrid.Aggregators.sum));

  columns.push(getNumericColumn("ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)", "ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)"));
  columns.push(getNumericColumn("ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)", "ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)"));

  columns.push({
    id: "drillThrough",
    name: "Drill Through",
    field: "",
    formatter: function(row, cell, value, columnDef, dataContext) {
      return (
        "<a title='BUGroup' data-role='BUGroup' style='margin-right: 3px;'>BUGroup</a>  " +
        " <a title='Position' data-role='Position'> Position</a>  "
      );
    },
    type: "text",
    headerCssClass: "b",
    minWidth: 50
  });

  return columns;
}
