import { getNumericColumn, getTextColumn} from "./common/columnHelper"
import getColumnOptions from "./common/commonConfig"
import {
  linkFormatter
} from "commons/grid/formatters";

export default function getBusinessUnitColumns() {
  var columns = getColumnOptions();

  columns.push(getTextColumn("businessUnitName", "Business Unit Name", "businessUnitName", "Business Unit Name", 50));
  columns.push(getTextColumn("ebValue", "EB Value", "ebValue", "EB Value", 50));
  columns.push(getTextColumn("etbRate", "ETB Rate", "etbRate", "ETB Rate", 50));
  columns.push(getTextColumn("gammaRatio", "Gamma Ratio", "gammaRatio", "Gamma Ratio", 50));
  columns.push(getTextColumn("netOptionValue", "Net Option Value", "netOptionValue", "Net Option Value", 50));
  columns.push(getNumericColumn("spreadOnlyFinancingRC", "Spread Only Financing (RC)", "spreadOnlyFinancingRC", "Spread Only Financing (RC)", dpGrid.Aggregators.sum));
  columns.push(getNumericColumn("ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)", "ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)"));
  columns.push(getNumericColumn("treasuryNoSpreadRC", "Allocated Financing No Spread (RC)", "treasuryNoSpreadRC", "Allocated Financing No Spread (RC)"));
  columns.push(getNumericColumn("ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)", "ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)"));
  columns.push(getNumericColumn("treasuryNoBorrowRC", "Allocated Financing No Borrow (RC)", "treasuryNoBorrowRC", "Allocated Financing No Borrow (RC)"));

  columns.push({
    id: "drillThrough",
    name: "Drill Through",
    field: "",
    formatter: function(row, cell, value, columnDef, dataContext) {
      return linkFormatter(
        row,
        cell,
        "Accounting Bundle CA",
        columnDef,
        dataContext
      );
    },
    toolTip: "Drill Through to Accounting Bundle CA",
    type: "text",
    headerCssClass: "b",
    minWidth: 50
  });

  return columns;
}
