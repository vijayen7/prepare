import getCommonTotalColumns from "./commonTotalColumns";

export function getCustodianAccountViewDefaultColumns(agreementType) {
  var additionalColumns;
  switch (agreementType) {
    case 13:
      additionalColumns = [
        "custodianAccountName",
        "cash",
        "cashRC",
        "longMarketValueLocal",
        "shortMarketValueLocal"
      ];
      break;
    case 20:
      additionalColumns = [
        "custodianAccountName",
        "cash",
        "cashRC",
        "oteLocal",
        "oteRC",
        "equityRC",
        "collateralRC",
        "longMarketValueLocal",
        "shortMarketValueLocal"
      ];
      break;
    case 26:
      additionalColumns = [
        "custodianAccountName",
        "cash",
        "cashRC",
        "oteLocal",
        "oteRC",
        "equityLocal",
        "equityRC"
      ];
      break;
    case 2:
      additionalColumns = [
        "custodianAccountName",
        "cash",
        "cashRC",
        "equityLocal",
        "longMarketValueLocal",
        "shortMarketValueLocal"
      ];
      break;
    case 3:
      additionalColumns = [
        "custodianAccountName",
        "cash",
        "cashRC",
        "equity",
        "longMarketValueLocal",
        "shortMarketValueLocal"
      ];
      break;
    case 5:
      additionalColumns = [
        "custodianAccountName",
        "cash",
        "cashRC",
        "oteLocal",
        "oteRC",
        "equity",
        "equityRC"
      ];
      break;
    case 6:
    case 7:
    case 8:
    case 25:
      additionalColumns = [
        "custodianAccountName",
        "independentAmountLocal",
        "collateralLocal",
        "collateralRC",
        "marketToMarketLocal",
        "marketToMarketRC"
      ];
      break;
    case 9:
    case 11:
    case 17:
      additionalColumns = ["custodianAccountName", "cash", "cashRC"];
      break;
    default:
      additionalColumns = [
        "custodianAccountName",
        "cash",
        "cashRC",
        "oteLocal",
        "oteRC",
        "equityRC",
        "collateralRC",
        "longMarketValueLocal",
        "shortMarketValueLocal"
      ];

      break;
  }

  return additionalColumns;
}

export function getCustodianAccountViewTotalColumns() {
  var commonColumns = getCommonTotalColumns();
  var additionalColumns = [
    ["custodianAccountName", "Custodian AccountId Name"]
  ];
  commonColumns = commonColumns.concat(additionalColumns);
  return commonColumns;
}
