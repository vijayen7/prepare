import getCommonTotalColumns from "./commonTotalColumns";

export function getBUGroupViewDefaultColumns(agreementType) {
  var additionalColumns;
  switch (agreementType) {
    case 13:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "etbLocal",
        "htbLocal",
        "directFinancingRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "dailyStockLendIncomeRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 20:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "marginLocal",
        "oteLocal",
        "marketToMarketLocal",
        "independentAmountLocal",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "shortFinanceIncomeCostRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 26:
      additionalColumns = [
        "marginLocal",
        "netOptionValue",
        "equityLocal",
        "equityRC",
        "directFinancingRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 2:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "marginLocal",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "shortFinanceIncomeCostRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 3:
      additionalColumns = [
        "wahtb",
        "etbRate",
        "marginLocal",
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "etbLocal",
        "htbLocal",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "dailyExcessBorrowCostRC",
        "ebValue",
        "waEbRate",
        "waslr",
        "slvLocal",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "directFinancingRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 5:
      additionalColumns = [
        "marginLocal",
        "netOptionValue",
        "oteLocal",
        "equityLocal",
        "directFinancingRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 6:
    case 7:
    case 8:
    case 25:
      additionalColumns = [
        "marketToMarketLocal",
        "independentAmountLocal",
        "directFinancingRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 9:
    case 11:
    case 17:
      additionalColumns = [
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    default:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "marginLocal",
        "oteLocal",
        "marketToMarketLocal",
        "independentAmountLocal",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "shortFinanceIncomeCostRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];

      break;
  }

  return additionalColumns;
}

export function getBUGroupViewTotalColumns() {
  var commonColumns = getCommonTotalColumns();

  var bUGroupViewAdditionalColumns = [
    ["businessUnitGroupTypeName", "Business Unit Group Type Name"],
    ["businessUnitGroupName", "Business Unit Group Name"],
    ["ebValue", "EB Value"],
    ["etbRate", "ETB Rate"],
    ["etbLocal", "ETB Value (Local)"],
    ["etbRC", "ETB Value (RC)"],
    ["htbLocal", "HTB Value (Local)"],
    ["htbRC", "HTB Value (RC)"],
    ["gammaRatio", "Gamma Ratio"],
    ["netOptionValue", "Net Option Value"],
    ["ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)"],
    ["treasuryNoSpreadRC", "Allocated Financing No Spread (RC)"],
    ["ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)"],
    ["treasuryNoBorrowRC", "Allocated Financing No Borrow (RC)"],
    ["spreadOnlyFinancingRC", "Spread Only Financing (RC)"]
  ];
  commonColumns = commonColumns.concat(bUGroupViewAdditionalColumns);
  return commonColumns;
}
