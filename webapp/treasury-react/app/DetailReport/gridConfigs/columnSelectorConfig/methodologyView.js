import getCommonTotalColumns from "./commonTotalColumns";

export function getMethodologyViewDefaultColumns(agreementType) {
  var additionalColumns;
  switch (agreementType) {
    case 13:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "etbLocal",
        "htbLocal",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "dailyStockLendIncomeRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 20:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "cash",
        "oteLocal",
        "equityLocal",
        "collateralLocal",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "dailyExcessBorrowCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "repoFinancingRC",
        "swapFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 26:
      additionalColumns = [
        "equityLocal",
        "equityRC",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "allocatedFinancingRC",
        "totalFinancingRC"
      ];
      break;
    case 2:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "cash",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "dailyExcessBorrowCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 3:
      additionalColumns = [
        "cash",
        "dailyFinanceIncomeCostRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 5:
      additionalColumns = [
        "cash",
        "oteLocal",
        "equityRC",
        "netDebitRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 6:
    case 7:
    case 8:
    case 25:
      additionalColumns = [
        "collateralLocal",
        "repoFinancingRC",
        "swapFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 9:
    case 11:
    case 17:
      additionalColumns = ["cash", "totalFinancingLocal", "totalFinancingRC"];
      break;
    default:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "cash",
        "oteLocal",
        "equityLocal",
        "collateralLocal",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "dailyExcessBorrowCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "totalFinancingRC",
        "totalFinancingRC"
      ];

      break;
  }

  return additionalColumns;
}

export function getMethodologyViewTotalColumns() {
  var commonColumns = getCommonTotalColumns();
  var methodologyViewAdditionalColumns = [
    ["dailyFinanceCostLocal", "Daily Finance Cost (Local)"],
    ["dailyFinanceCostRC", "Daily Finance Cost (RC)"],
    ["drillThrough", "Drill Through"],
    ["etbLocal", "ETB Value (Local)"],
    ["etbRC", "ETB Value (RC)"],
    ["htbLocal", "HTB Value (Local)"],
    ["htbRC", "HTB Value (RC)"],
    ["ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)"],
    ["ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)"],
    ["spreadOnlyFinancingRC", "Spread Only Financing (RC)"]
  ];
  commonColumns = commonColumns.concat(methodologyViewAdditionalColumns);
  return commonColumns;
}
