export default function getCommonDefaultColumns() {
  return [
    "date",
    "agreementType",
    "legalEntityCodaCompCode",
    "cpeName",
    "nettingGroup",
    "currency",
    "knowledgeDate",
    "interestOnlyCustodianAccountName",
    "agreementTermDetails",
    "fxRate",
    "drillThrough"
  ];
}
