import getCommonTotalColumns from "./commonTotalColumns";

export function getInterestBundleCAViewDefaultColumns(agreementType) {
  var additionalColumns;
  switch (agreementType) {
    case 13:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "etbLocal",
        "htbLocal",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "dailyStockLendIncomeRC",
        "directFinancingRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 20:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "oteLocal",
        "initialMargin",
        "marketToMarketLocal",
        "independentAmount",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "shortFinanceIncomeCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "allocatedFinancingRC",
        "repoFinancingRC",
        "swapFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 26:
      additionalColumns = [
        "businessUnitName",
        "initialMargin",
        "netOptionValue",
        "equityLocal",
        "equityRC",
        "directFinancingRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 2:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "initialMargin",
        "netDebitRC",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "shortFinanceIncomeCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "allocatedFinancingRC",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    case 3:
      additionalColumns = [
        "businessUnitName",
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "dailyBorrowCostRC",
        "rebateRC",
        "dailyStockLendIncomeRC",
        "totalFinancing",
        "totalFinancingRC"
      ];
      break;
    case 5:
      additionalColumns = [
        "initialMargin",
        "netOptionValue",
        "oteLocal",
        "equityLocal",
        "directFinancingRC",
        "allocatedFinancingRC",
        "totalFinancing",
        "totalFinancingRC"
      ];
      break;
    case 6:
    case 7:
    case 8:
    case 25:
      additionalColumns = [
        "marketToMarketLocal",
        "independentAmountLocal",
        "directFinancingRC",
        "allocatedFinancingRC",
        "repoFinancingRC",
        "swapFinancingRC",
        "totalFinancing",
        "totalFinancingRC"
      ];
      break;
    case 9:
    case 11:
    case 17:
      additionalColumns = [
        "allocatedFinancingRC",
        "totalFinancing",
        "totalFinancingRC"
      ];
      break;
    default:
      additionalColumns = [
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "initialMargin",
        "oteLocal",
        "marketToMarketLocal",
        "independentAmountLocal",
        "dailyFinanceIncomeCostRC",
        "dailyBorrowCostRC",
        "netDebitRC",
        "shortFinanceIncomeCostRC",
        "allocatedFinancingRC",
        "totalFinancing",
        "totalFinancingRC"
      ];

      break;
  }

  return additionalColumns;
}

export function getInterestBundleCAViewTotalColumns() {
  var commonColumns = getCommonTotalColumns();
  var genevaBundleCAAdditionalColumns = [
    [
      "totalDailyFinancingIncomeCostLocal",
      "Total Daily Financing Income Cost (Local)"
    ],
    [
      "totalDailyFinancingIncomeCostRC",
      "Total Daily Financing Income Cost (RC)"
    ],
    ["businessUnitName", "Business Unit Name"],
    ["netOptionValue", "Net Option Value"],
    ["genevaBundleName", "Accounting Bundle Name"],
    ["etbLocal", "ETB Value (Local)"],
    ["etbRC", "ETB Value (RC)"],
    ["htbLocal", "HTB Value (Local)"],
    ["htbRC", "HTB Value (RC)"],
    ["genevaCustodianAccountName", "Accounting Custodian Account Name"],
    ["ndficNoSpreadRC", "Net Debit Finance Income Cost No Spread (RC)"],
    ["treasuryNoSpreadRC", "Allocated Financing No Spread (RC)"],
    ["ndficNoBorrowRC", "Net Debit Finance Income Cost No Borrow (RC)"],
    ["treasuryNoBorrowRC", "Allocated Financing No Borrow (RC)"],
    ["spreadOnlyFinancingRC", "Spread Only Financing (RC)"]
  ];
  commonColumns = commonColumns.concat(genevaBundleCAAdditionalColumns);
  return commonColumns;
}
