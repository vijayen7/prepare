import getCommonTotalColumns from "./commonTotalColumns";

export function getAssetClassViewDefaultColumns(agreementType) {
  var additionalColumns;
  switch (agreementType) {
    default:
      additionalColumns = [
        "assetCassGroupDisplayName",
        "longMarketValueLocal",
        "shortMarketValueLocal",
        "marginLocal",
        "waterfallCashRC",
        "loanValueRC",
        "waterfallLoanValueRC",
        "initialMargin",
        "shortFinanceIncomeCostRC",
        "directFinancingRC"
      ];

      break;
  }

  return additionalColumns;
}

export function getAssetClassViewTotalColumns() {
  var commonColumns = getCommonTotalColumns();
  var additionalColumns = [
    ["assetCassGroupDisplayName", "Asset Class Group Name"],
    ["loanValueLocal", "Loan Value (Local)"],
    ["loanValueRC", "Loan Value (RC)"],
    ["waterfallCashLocal", "Waterfall Cash (Local)"],
    ["waterfallCashRC", "Waterfall Cash (RC)"],
    ["waterfallLoanValueLocal", "Waterfall Loan Value (Local)"],
    ["waterfallLoanValueRC", "Waterfall Loan Value (RC)"]
  ];
  commonColumns = commonColumns.concat(additionalColumns);
  return commonColumns;
}
