import getCommonTotalColumns from "./commonTotalColumns";

export function getBundlePositionViewDefaultColumns(agreementType) {
  var additionalColumns;
  switch (agreementType) {
    case 13:
      additionalColumns = [
        "pnlSpn",
        "securityName",
        "custodianAccount",
        "bookDisplayName",
        "price",
        "priceTick",
        "quantity",
        "marginLocal",
        "marketValueLocal",
        "marketValueRC",
        "lbType",
        "feeRate",
        "dailyLendBorrowIncomeCost"
      ];
      break;
    case 20:
      additionalColumns = [
        "pnlSpn",
        "securityName",
        "custodianAccount",
        "bookDisplayName",
        "nativeCurrency",
        "price",
        "priceTick",
        "quantity",
        "marginLocal",
        "marketValueLocal",
        "marketValueRC",
        "lbType",
        "feeRate",
        "dailyLendBorrowIncomeCost"
      ];
      break;
    case 26:
      additionalColumns = [
        "pnlSpn",
        "custodianAccount",
        "businessUnitName",
        "price",
        "priceTick",
        "quantity",
        "marketValueLocal",
        "marketValueRC"
      ];
      break;
    case 2:
      additionalColumns = [
        "pnlSpn",
        "securityName",
        "custodianAccount",
        "bookDisplayName",
        "price",
        "priceTick",
        "quantity",
        "marginLocal",
        "marketValueLocal",
        "marketValueRC",
        "lbType",
        "feeRate",
        "dailyLendBorrowIncomeCost"
      ];
      break;
    case 3:
      additionalColumns = [
        "pnlSpn",
        "securityName",
        "custodianAccount",
        "price",
        "priceTick",
        "quantity",
        "marketValueLocal",
        "marketValueRC",
        "lbType",
        "feeRate",
        "dailyLendBorrowIncomeCost"
      ];
      break;
    case 5:
      additionalColumns = [
        "pnlSpn",
        "securityName",
        "custodianAccount",
        "price",
        "priceTick",
        "quantity",
        "marginLocal",
        "oteLocal",
        "oteRC"
      ];
      break;
    case 6:
    case 7:
    case 8:
    case 25:
      additionalColumns = [
        "pnlSpn",
        "securityName",
        "custodianAccount",
        "nativeCurrency",
        "price",
        "priceTick",
        "quantity",
        "marketToMarketLocal",
        "independentAmountLocal"
      ];
      break;
    case 9:
    case 11:
    case 17:
      additionalColumns = [
        "securityName",
        "cash",
        "custodianAccount",
        "totalFinancingLocal",
        "totalFinancingRC"
      ];
      break;
    default:
      additionalColumns = [
        "pnlSpn",
        "securityName",
        "custodianAccount",
        "bookDisplayName",
        "nativeCurrency",
        "price",
        "priceTick",
        "quantity",
        "marginLocal",
        "marketValueLocal",
        "marketValueRC",
        "lbType",
        "feeRate",
        "dailyLendBorrowIncomeCost"
      ];

      break;
  }

  return additionalColumns;
}

export function getBundlePositionViewTotalColumns() {
  var commonColumns = getCommonTotalColumns();
  var additionalColumns = [
    ["custodianAccount", "Custodian Account"],
    ["bookDisplayName", "Book Display Name"],
    ["dailyLendBorrowIncomeCost", "Daily Lend Borrow Income Cost"],
    ["feeRate", "Fee Rate"],
    ["lbType", "Position Type"],
    ["marketValueLocal", "Market Value (Local)"],
    ["marketValueRC", "Market Value (RC)"],
    ["pnlSpn", "PnL SPN"],
    ["price", "Price"],
    ["priceTick", "Price Tick"],
    ["quantity", "Quantity"],
    ["securityName", "Security Name"],
    ["sedol", "Sedol"],
    ["ric", "RIC"],
    ["bloomberg", "Bloomberg"],
    ["cusip", "Cusip"],
    ["isin", "ISIN"],
    ["classification", "Classification"],
    ["borrowRate", "Borrow Rate"],
    ["waborrowRate", "WA Borrow Rate"],
    ["borrowFeeRC", "Borrow Fee (RC)"],
    ["repoFinancingLocal", "Repo Financing (Local)"],
    ["swapFinancingLocal", "Swap Financing (Local)"],
    ["businessUnitName", "Business Unit Name"]
  ];
  commonColumns = commonColumns.concat(additionalColumns);
  return commonColumns;
}
