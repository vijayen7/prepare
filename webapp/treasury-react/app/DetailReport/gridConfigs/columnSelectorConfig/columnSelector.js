import { viewLevels } from "../../util";
import {
  getMethodologyViewDefaultColumns,
  getMethodologyViewTotalColumns
} from "./methodologyView";

import {
  getBUGroupViewDefaultColumns,
  getBUGroupViewTotalColumns
} from "./buGroupView";
import { getBUViewDefaultColumns, getBUViewTotalColumns } from "./buView";
import {
  getAssetClassViewDefaultColumns,
  getAssetClassViewTotalColumns
} from "./assetClassView";
import {
  getBundlePositionViewDefaultColumns,
  getBundlePositionViewTotalColumns
} from "./bundlePositionView";
import {
  getBundleViewDefaultColumns,
  getBundleViewTotalColumns
} from "./bundleView";
import {
  getCustodianAccountViewDefaultColumns,
  getCustodianAccountViewTotalColumns
} from "./custodianAccountView";
import {
  getGenevaBundleCAViewDefaultColumns,
  getGenevaBundleCAViewTotalColumns
} from "./genevaBundleCAView";
import {
  getInterestBundleCAViewDefaultColumns,
  getInterestBundleCAViewTotalColumns
} from "./interestBundleCAView";
import {
  getPositionViewDefaultColumns,
  getPositionViewTotalColumns
} from "./positionView";

import getCommonDefaultColumns from "./commonDefaultColumns";

export function getSelectedViewDefaultColumns(view, agreementType) {
  switch (view) {
    case viewLevels.METHODOLOGY:
      return getCommonDefaultColumns().concat(
        getMethodologyViewDefaultColumns(agreementType)
      );
    case viewLevels.BUSINESS_UNIT_GROUP:
      return getCommonDefaultColumns()
        .concat([
          "businessUnitGroupTypeName",
          "businessUnitGroupName",
          "gammaRatio"
        ])
        .concat(getBUGroupViewDefaultColumns(agreementType));
    case viewLevels.BUSINESS_UNIT:
      return getCommonDefaultColumns()
        .concat(["businessUnitName", "gammaRatio"])
        .concat(getBUViewDefaultColumns(agreementType));
    case viewLevels.BUNDLE:
      return getCommonDefaultColumns().concat(
        getBundleViewDefaultColumns(agreementType)
      );
    case viewLevels.POSITION:
      return getCommonDefaultColumns().concat(
        getPositionViewDefaultColumns(agreementType)
      );
    case viewLevels.BUNDLE_POSITION:
      return getCommonDefaultColumns()
        .concat(["bundle"])
        .concat(getBundlePositionViewDefaultColumns(agreementType));
    case viewLevels.CUSTODIAN_ACCOUNT:
      return getCommonDefaultColumns().concat(
        getCustodianAccountViewDefaultColumns(agreementType)
      );
    case viewLevels.ASSET_CLASS:
      return getCommonDefaultColumns().concat(
        getAssetClassViewDefaultColumns(agreementType)
      );
    case viewLevels.INTEREST_BUNDLE_CUSTODIAN_ACCOUNT:
      return getCommonDefaultColumns()
        .concat(["bundle", "custodianAccountName"])
        .concat(getInterestBundleCAViewDefaultColumns(agreementType));
    case viewLevels.GENEVA_BUNDLE_CUSTODIAN_ACCOUNT:
      return getCommonDefaultColumns()
        .concat(["genevaBundleName", "genevaCustodianAccountName"])
        .concat(getGenevaBundleCAViewDefaultColumns(agreementType));
    default:
      return getCommonDefaultColumns().concat(
        getMethodologyViewDefaultColumns(agreementType)
      );
  }
}

export function getSelectedViewTotalColumns(view) {
  switch (view) {
    case viewLevels.METHODOLOGY:
      return getMethodologyViewTotalColumns();
    case viewLevels.BUSINESS_UNIT_GROUP:
      return getBUGroupViewTotalColumns();
    case viewLevels.BUSINESS_UNIT:
      return getBUViewTotalColumns();
    case viewLevels.BUNDLE:
      return getBundleViewTotalColumns();
    case viewLevels.POSITION:
      return getPositionViewTotalColumns();
    case viewLevels.BUNDLE_POSITION:
      return getBundlePositionViewTotalColumns();
    case viewLevels.CUSTODIAN_ACCOUNT:
      return getCustodianAccountViewTotalColumns();
    case viewLevels.ASSET_CLASS:
      return getAssetClassViewTotalColumns();
    case viewLevels.INTEREST_BUNDLE_CUSTODIAN_ACCOUNT:
      return getInterestBundleCAViewTotalColumns();
    case viewLevels.GENEVA_BUNDLE_CUSTODIAN_ACCOUNT:
      return getGenevaBundleCAViewTotalColumns();
    default:
      return getMethodologyViewTotalColumns();
  }
}
