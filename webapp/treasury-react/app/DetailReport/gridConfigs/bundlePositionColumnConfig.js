import { getNumericColumn, getTextColumn, getNumericSpreadColumn } from "./common/columnHelper";
import getColumnOptions from "./common/commonConfig";

export default function getBundlePositionColumns() {
  var columns = getColumnOptions();

  columns.push(
    getTextColumn("bundle", "Bundle", "bundleDisplayName", "Bundle", 50)
  );
  columns.push(getTextColumn("nativeCurrency", "Native Currency", "nativeCurrencyAbbrev", "Native Currency", 50));
  columns.push(getNumericColumn("nativefxRate", "Native Fx Rate", "nativefxRate", "Native Fx Rate", 50));
  columns.push(
    getTextColumn(
      "custodianAccount",
      "Custodian Account",
      "custodianAccount",
      "Custodian Account",
      50
    )
  );
  columns.push(
    getTextColumn(
      "bookDisplayName",
      "Book Display Name",
      "bookDisplayName",
      "Book Display Name",
      50
    )
  );
  columns.push(
    getNumericColumn(
      "dailyLendBorrowIncomeCost",
      "Daily Lend Borrow Income Cost",
      "dailyLendBorrowIncomeCost",
      "Daily Lend Borrow Income Cost",
      dpGrid.Aggregators.sum
    )
  );
  columns.push(getNumericSpreadColumn("feeRate", "Fee Rate", "feeRate", "Fee Rate"));
  columns.push(
    getTextColumn("lbType", "Position Type", "lbType", "Position Type", 50)
  );
  columns.push(
    getNumericColumn(
      "marketValueLocal",
      "Market Value (Local)",
      "marketValue",
      "Market Value (Local)",
      dpGrid.Aggregators.sum
    )
  );
  columns.push(
    getNumericColumn(
      "marketValueRC",
      "Market Value (RC)",
      "marketValueRC",
      "Market Value (RC)",
      dpGrid.Aggregators.sum
    )
  );
  columns.push(getTextColumn("pnlSpn", "PnL SPN", "pnlSpn", "PnL SPN", 50));
  columns.push(getNumericColumn("price", "Price", "price", "Price"));
  columns.push(
    getNumericColumn("priceTick", "Price Tick", "priceTick", "Price Tick")
  );
  columns.push(
    getNumericColumn(
      "quantity",
      "Quantity",
      "quantity",
      "Quantity",
      dpGrid.Aggregators.sum
    )
  );

  columns.push(
    getTextColumn(
      "securityName",
      "Security Name",
      "securityName",
      "Security Name",
      50
    )
  );
  columns.push(getTextColumn("sedol", "Sedol", "sedol", "sedol", 50));
  columns.push(getTextColumn("ric", "RIC", "ric", "RIC", 50));
  columns.push(
    getTextColumn("bloomberg", "Bloomberg", "bloomberg", "Bloomberg", 50)
  );
  columns.push(getTextColumn("cusip", "Cusip", "cusip", "Cusip", 50));
  columns.push(getTextColumn("isin", "ISIN", "isin", "ISIN", 50));

  columns.push(getTextColumn("classification", "Classification", "classification", "Classification", 50));
  columns.push(getNumericSpreadColumn("borrowRate", "Borrow Rate", "borrowRate", "Borrow Rate"));
  columns.push(getNumericSpreadColumn("waborrowRate", "Weighted Average Borrow Rate", "waborrowRate", "Weighted Average Borrow Rate"));
  columns.push(getNumericColumn("borrowFeeRC", "Borrow Fee (RC)", "borrowFeeRC", "Borrow Fee (RC)", dpGrid.Aggregators.sum));
  columns.push(getTextColumn("financeChargeVersion","Pnl Version (Finance Charge)","financeChargeVersion","Pnl Version (Finance Charge)",50));
  columns.push(
    getTextColumn(
      "businessUnitId",
      "Business Unit Id",
      "businessUnitId",
      "Business Unit Id",
      100
    )
  );

  columns.push(
    getTextColumn(
      "businessUnitName",
      "Business Unit Name",
      "businessUnitName",
      "Business Unit Name",
      100
    )
  );
  columns.push(getTextColumn("foType","FO Type","foType","FO Type",50));
  columns.push(getTextColumn("gboType","GBO Type","gboType","GBO Type",50));
  columns.push(getTextColumn("subtype","Subtype","subtype","Subtype",50));
  columns.push(getTextColumn("shortDescription", "Short Description", "shortDescription", "Short Description", 50));

  return columns;
}
