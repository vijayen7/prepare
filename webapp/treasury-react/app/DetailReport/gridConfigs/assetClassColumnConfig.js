import { getNumericColumn, getTextColumn} from "./common/columnHelper"
import getColumnOptions from "./common/commonConfig"

export default function getAssetClassColumns() {
  var columns = getColumnOptions();

  columns.push(getTextColumn("assetCassGroupDisplayName", "Asset Class Group Name", "assetClassGroupDisplayName", "Asset Class Group Name", 50));
  columns.push(getNumericColumn("loanValueLocal", "Loan Value (Local)", "loanValue", "Loan Value (Local)"));
  columns.push(getNumericColumn("loanValueRC", "Loan Value (RC)", "loanValueRC", "Loan Value (RC)"));
  columns.push(getNumericColumn("waterfallCashLocal", "Waterfall Cash (Local)", "waterfallCash", "Waterfall Cash (Local)"));
  columns.push(getNumericColumn("waterfallCashRC", "Waterfall Cash (RC)", "waterfallCashRC", "Waterfall Cash (RC)"));
  columns.push(getNumericColumn("waterfallLoanValueLocal", "Waterfall Loan Value (Local)", "waterfallLoanValue", "Waterfall Loan Value (Local)"));
  columns.push(getNumericColumn("waterfallLoanValueRC", "Waterfall Loan Value (RC)", "waterfallLoanValueRC", "Waterfall Loan Value (RC)"));

  return columns;
}
