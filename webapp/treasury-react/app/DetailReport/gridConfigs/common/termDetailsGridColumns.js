import {
  formatterFourDecimal
} from "./columnHelper";

export default function termDetailsGridColumns() {
  var columns = [
    {
      id: "creditBaseRate",
      name: "Credit Base Rate",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      type: "text",
      filter: true,
      toolTip: "Credit Base Rate",
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (
          value !== undefined &&
          value !== null &&
          value.creditBaseRateTs !== undefined &&
          value.creditBaseRateTs !== null
        )
          return value.creditBaseRateTs.name;
        else return null;
      },
      excelHeaderFormatter: function(row, cell) {
        return cell.id;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.creditBaseRateTs !== "undefined" &&
          value.creditBaseRateTs !== null
        ) {
          returnValue = value.creditBaseRateTs.name;
        }
        return returnValue;
      },
      excelFormatter: "#,##0",

      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "creditBaseRateValue",
      name: "Credit Base Rate Value",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      toolTip: "Credit Base Rate Value",
      type: "text",
      filter: true,
      sortable: true,
      width: 95,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null) {
          return formatterFourDecimal(
            row,
            cell,
            value.creditBaseRate == null ? null : Number(value.creditBaseRate) * 100,
            columnDef,
            dataContext
          );
        }
        else return null;
      },
      excelHeaderFormatter: function(row, cell) {
        return cell.id;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.creditBaseRate !== "undefined" &&
          value.creditBaseRate !== null
        ) {
          returnValue =  Number(value.creditBaseRate) * 100;
        }
        return returnValue;
      },
      excelFormatter: "#,##0.#####0",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "creditSpread",
      name: "Credit Spread",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      toolTip: "Credit Spread",
      type: "text",
      filter: true,
      sortable: true,
      width: 95,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(
            row,
            cell,
            value.creditSpread,
            columnDef,
            dataContext
          );
        else return null;
      },
      excelHeaderFormatter: function(row, cell) {
        return cell.id;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.creditSpread !== "undefined" &&
          value.creditSpread !== null
        ) {
          returnValue = value.creditSpread;
        }
        return returnValue;
      },
      excelFormatter: "#,##0.#####0",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "creditAccConvention",
      name: "Credit Accrual Convention",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      toolTip: "Credit Accrual Convention",
      type: "text",
      filter: true,
      sortable: true,
      width: 95,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return dpGrid.Formatters.Number(
            row,
            cell,
            value.creditAccrualConvention,
            columnDef,
            dataContext
          );
        else return null;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.creditAccrualConvention !== "undefined" &&
          value.creditAccrualConvention !== null
        ) {
          returnValue = value.creditAccrualConvention;
        }
        return returnValue;
      },
      excelFormatter: "#,##0",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "debitBaseRate",
      name: "Debit Base Rate",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      type: "text",
      filter: true,
      toolTip: "Debit Base Rate",
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (
          value !== undefined &&
          value !== null &&
          value.debitBaseRateTs !== undefined &&
          value.debitBaseRateTs !== null
        )
          return value.debitBaseRateTs.name;
        else return null;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.debitBaseRateTs !== "undefined" &&
          value.debitBaseRateTs !== null
        ) {
          returnValue = value.debitBaseRateTs.name;
        }
        return returnValue;
      },
      excelFormatter: "#,##0",

      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "debitBaseRateValue",
      name: "Debit Base Rate Value",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      toolTip: "Debit Base Rate Value",
      type: "text",
      filter: true,
      sortable: true,
      width: 95,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(
            row,
            cell,
            value.debitBaseRate == null ? null : Number(value.debitBaseRate) * 100,
            columnDef,
            dataContext
          );
        else return null;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.debitBaseRate !== "undefined" &&
          value.debitBaseRate !== null
        ) {
          returnValue = Number(value.debitBaseRate) * 100;
        }
        return returnValue;
      },
      excelFormatter: "#,##0.#####0",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "debitSpread",
      name: "Debit Spread",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      toolTip: "Debit Spread",
      type: "text",
      filter: true,
      sortable: true,
      width: 95,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(
            row,
            cell,
            value.debitSpread,
            columnDef,
            dataContext
          );
        else return null;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.debitSpread !== "undefined" &&
          value.debitSpread !== null
        ) {
          returnValue = value.debitSpread;
        }
        return returnValue;
      },
      excelFormatter: "#,##0.#####0",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "debitAccConvention",
      name: "Debit Accrual Convention",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      toolTip: "Debit Accrual Convention",
      type: "text",
      filter: true,
      sortable: true,
      width: 95,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return dpGrid.Formatters.Number(
            row,
            cell,
            value.debitAccrualConvention,
            columnDef,
            dataContext
          );
        else return null;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.debitAccrualConvention !== "undefined" &&
          value.debitAccrualConvention !== null
        ) {
          returnValue = value.debitAccrualConvention;
        }
        return returnValue;
      },
      excelFormatter: "#,##0",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "rebateBaseRate",
      name: "Rebate Base Rate",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      type: "text",
      filter: true,
      toolTip: "Rebate Base Rate",
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (
          value !== undefined &&
          value !== null &&
          value.rebateBaseRateTs !== undefined &&
          value.rebateBaseRateTs !== null
        )
          return value.rebateBaseRateTs.name;
        else return null;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.rebateBaseRateTs !== "undefined" &&
          value.rebateBaseRateTs !== null
        ) {
          returnValue = value.rebateBaseRateTs.name;
        }
        return returnValue;
      },
      excelFormatter: "#,##0",

      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "rebateBaseRateValue",
      name: "Rebate Base Rate Value",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      toolTip: "Rebate Base Rate Value",
      type: "text",
      filter: true,
      sortable: true,
      width: 95,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(
            row,
            cell,
            value.rebateBaseRate == null ? null : Number(value.rebateBaseRate) * 100,
            columnDef,
            dataContext
          );
        else return null;
      },
      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.rebateBaseRate !== "undefined" &&
          value.rebateBaseRate !== null
        ) {
          returnValue = Number(value.rebateBaseRate) * 100;
        }
        return returnValue;
      },
      excelFormatter: "#,##0.#####0",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "gcRate",
      name: "GC Rate",
      headerCssClass: "b",
      field: "financingTerm",
      sortable: true,
      toolTip: "GC Rate",
      type: "text",
      filter: true,
      sortable: true,
      width: 95,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(
            row,
            cell,
            value.gcRate,
            columnDef,
            dataContext
          );
        else return null;
      },

      excelDataFormatter: function(value) {
        var returnValue = "";
        if (
          typeof value.gcRate !== "undefined" &&
          value.gcRate !== null
        ) {
          returnValue = value.gcRate;
        }
        return returnValue;
      },
      excelFormatter: "#,##0.#####0",

      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "assetClassGroup",
      name: "Asset Class Group",
      headerCssClass: "b",
      field: "assetClassGroup",
      sortable: true,
      toolTip: "Asset Class Group",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null) return value.displayName;
        else return null;
      },
      excelDataFormatter: function(value) {
        if (value !== undefined && value !== null) return value.displayName;
        else return "";
      },
      excelFormatter: "#,##0",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "bandwidthGroup",
      name: "Bandwidth Group",
      headerCssClass: "b",
      field: "bandwidthGroup",
      sortable: true,
      toolTip: "Bandwidth Group",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null) return value.displayName;
        else return null;
      },
      excelDataFormatter: function(value) {
        if (value !== undefined && value !== null) return value.displayName;
        else return "";
      },
       excelFormatter: "#,##0",
      filter: {
        isFilterOnFormattedValue: true
      }
    }
    ,
    {
      id: "custodianAccount",
      name: "Custodian Account",
      headerCssClass: "b",
      field: "custodianAccountName",
      sortable: true,
      toolTip: "Custodian Account",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null) return value;
        else return null;
      },
      excelDataFormatter: function(value) {
        if (value !== undefined && value !== null) return value;
        else return "";
      },
       excelFormatter: "#,##0",
      filter: {
        isFilterOnFormattedValue: true
      }
    }
  ];

  return columns;
}
