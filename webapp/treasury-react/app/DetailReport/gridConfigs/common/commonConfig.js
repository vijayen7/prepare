import {
  getNumericColumn,
  getNumericSpreadColumn,
  getTextColumn,
  getNestedTextColumn,
  getDateColumn,
  getDateTimeColumn
} from "./columnHelper";
import { linkFormatter, spreadFormatter } from "commons/grid/formatters";
export default function getColumnOptions() {
  var columns = [
    getDateColumn("date", "Date", "date", "Date", 50),

    getNestedTextColumn(
      "agreementType",
      "Agreement Type",
      "agreementType",
      "description",
      "Agreement Type",
      50
    ),

    getTextColumn(
      "legalEntityCodaCompCode",
      "Legal Entity",
      "legalEntityCodaCompCode",
      "Legal Entity Coda Comp Code",
      50
    ),

    getTextColumn(
      "cpeName",
      "Counterparty Name",
      "cpeName",
      "Counterparty Name",
      50
    ),

    getNestedTextColumn(
      "nettingGroup",
      "Netting Group",
      "nettingGroup",
      "description",
      "Netting Group",
      50
    ),

    getTextColumn("currency", "Currency", "currencyAbbrev", "Currency", 50),

    getDateTimeColumn(
      "knowledgeDate",
      "Knowledge Date",
      "knowledgeDate",
      "Knowledge Date",
      50
    ),

    getTextColumn(
      "interestOnlyCustodianAccountName",
      "Interest Only Custodian Account",
      "interestOnlyCustodianAccountName",
      "Interest Only Custodian Account",
      50,
      100
    ),

    {
      id: "agreementTermDetails",
      name: "Agreement Term Details",
      field: "",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return linkFormatter(row, cell, "Term Details", columnDef, dataContext);
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      toolTip: "Agreement Term Details",
      type: "text",
      headerCssClass: "b",
      minWidth: 50
    },

    getNumericColumn("fxRate", "FX Rate", "fxRate", "FX Rate"),

    getNumericColumn(
      "collateralLocal",
      "Collateral (Local)",
      "collateral",
      "Collateral (Local)"
    ),

    getNumericColumn(
      "totalFinancingLocal",
      "Total Financing (Local)",
      "totalFinancingLBC",
      "Total Financing (Local)",
      dpGrid.Aggregators.sum
    ),


    getNumericColumn(
      "allocatedFinancingLocal",
      "Allocated Financing (Local)",
      "allocatedFinancingLBC",
      "Allocated Financing (Local)",
      dpGrid.Aggregators.sum
    ),


    getNumericColumn("cash", "Cash", "cash", "Cash"),

    getNumericColumn(
      "dailyBorrowCostLocal",
      "Daily Borrow Cost (Local)",
      "dailyBorrowCostLBC",
      "Daily Borrow Cost (Local)",
      dpGrid.Aggregators.sum
    ),

    getNumericColumn(
      "dailyExcessBorrowCostLocal",
      "Daily Excess Borrow Cost (Local)",
      "dailyExcessBorrowCostLBC",
      "Daily Excess Borrow Cost (Local)",
      dpGrid.Aggregators.sum
    ),

    getNumericColumn(
      "dailyFinanceIncomeCostLocal",
      "Debit Finance Income Cost (Local)",
      "dailyFinanceIncomeCostLBC",
      "Debit Finance Income Cost (Local)",
      dpGrid.Aggregators.sum
    ),

    getNumericColumn(
      "dailyStockLendIncomeLocal",
      "Daily Stock Lend Income (Local)",
      "dailyStockLendIncomeLBC",
      "Daily Stock Lend Income (Local)",
      dpGrid.Aggregators.sum
    ),

    getNumericColumn(
      "etbLocal",
      "ETB Value (Local)",
      "etb",
      "ETB Value (Local)"
    ),

    getNumericSpreadColumn(
      "wahtb",
      "Weighted average HTB Rate",
      "wahtbRate",
      "Weighted average hard to borrow Rate"
    ),

    getNumericSpreadColumn(
      "waetb",
      "Weighted average ETB Rate",
      "waetbRate",
      "Weighted average easy to borrow Rate"
    ),

    getNumericColumn(
      "htbLocal",
      "HTB Value (Local)",
      "htb",
      "HTB Value (Local)"
    ),

    getNumericColumn(
      "equityLocal",
      "Equity (Local)",
      "actualEquity",
      "Equity (Local)"
    ),

    getNumericColumn(
      "excessBorrowValueLocal",
      "Excess Borrow Value (Local)",
      "excessBorrowValue",
      "Excess Borrow Value (Local)"
    ),

    getNumericColumn(
      "excessBorrowValueUSD",
      "Excess Borrow Value (USD)",
      "excessBorrowValueUSD",
      "Excess Borrow Value (USD)"
    ),

    getNumericColumn(
      "independentAmountLocal",
      "Independent Amount (Local)",
      "independentAmount",
      "Independent Amount (Local)"
    ),

    getNumericSpreadColumn(
      "waEbRate",
      "Weighted Average Excess Borrow Rate",
      "waEbRate",
      "Weighted Average Excess Borrow Rate"
    ),

    getNumericSpreadColumn(
      "waslr",
      "Weighted Average Lend Rate",
      "waslr",
      "Weighted Average Lend Rate"
    ),

    getNumericColumn(
      "slvLocal",
      "Stock Lend Value (Local)",
      "slv",
      "Stock Lend Value (Local)"
    ),

    getNumericColumn(
      "slvUSD",
      "Stock Lend Value (USD)",
      "slvUSD",
      "Stock Lend Value (USD)"
    ),

    getNumericColumn(
      "marketToMarketLocal",
      "Market To Market (Local)",
      "marketToMarket",
      "Market to Market (Local)"
    ),

    {
      id: "isDesim",
      name: "Is Desim",
      field: "isIam",
      toolTip: "Is DESIM?",
      type: "bool",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      minWidth: 50
    },

    getNumericColumn("oteLocal", "OTE (Local)", "ote", "OTE (Local)"),

    getNumericColumn(
      "marginLocal",
      "Margin (Local)",
      "initialMargin",
      "Margin (Local)"
    ),

    getNumericColumn(
      "netDebitLocal",
      "Net Debit (Local)",
      "netDebitLBC",
      "Net Debit (Local)",
      dpGrid.Aggregators.sum
    ),

    getNumericColumn(
      "rebateLocal",
      "Rebate (Local)",
      "rebateLBC",
      "Rebate (Local)",
      dpGrid.Aggregators.sum
    ),

    getNumericColumn(
      "shortFinanceIncomeCostLocal",
      "Short Finance Income Cost (Local)",
      "shortFinanceIncomeCostLBC",
      "Short Finance Income Cost (Local)",
      dpGrid.Aggregators.sum
    ),

    getNumericColumn(
      "shortMarketValueLocal",
      "Short Market Value (Local)",
      "smv",
      "Short Market Value (Local)"
    ),

    getNumericColumn(
      "longMarketValueLocal",
      "Long Market Value (Local)",
      "lmv",
      "Long Market Value (Local)"
    ),

    getNumericSpreadColumn(
      "creditBaseRate",
      "Credit Base Rate",
      "creditBaseRate",
      "Credit Base Rate"
    ),
    getNumericSpreadColumn(
      "creditSpread",
      "Credit Spread",
      "creditSpread",
      "Credit Spread"
    ),
    getNumericSpreadColumn(
      "debitBaseRate",
      "Debit Base Rate",
      "debitBaseRate",
      "Debit Base Rate"
    ),
    getNumericSpreadColumn(
      "debitSpread",
      "Debit Spread",
      "debitSpread",
      "Debit Spread"
    ),
    getNumericSpreadColumn(
      "rebateBaseRate",
      "Rebate Base Rate",
      "rebateBaseRate",
      "Rebate Base Rate"
    ),
    getNumericSpreadColumn(
      "gcRate",
      "GC Rate",
      "gcRate",
      "GC Rate"
    ),
    getNumericSpreadColumn(
      "appliedBaseRate",
      "Applied Base Rate",
      "appliedBaseRate",
      "Applied Base Rate"
    ),
    getNumericSpreadColumn(
      "appliedSpread",
      "Applied Spread",
      "appliedSpread",
      "Applied Spread"
    ),
    getNumericColumn("swapFinancing", "Swap Financing DTD (Local)", "swapFinancing", "Swap Financing DTD (Local)", dpGrid.Aggregators.sum),
    getNumericColumn("swapFinancingMTD", "Swap Financing MTD (Local)", "swapFinancingMtd", "Swap Financing MTD (Local)"),
    getNumericColumn("swapFinancingQTD", "Swap Financing QTD (Local)", "swapFinancingQtd", "Swap Financing QTD (Local)"),
    getNumericColumn("swapFinancingYTD", "Swap Financing YTD (Local)", "swapFinancingYtd", "Swap Financing YTD (Local)"),

    getNumericColumn("repoFinancing", "Repo Financing DTD (Local)", "repoFinancing", "Repo Financing DTD (Local)", dpGrid.Aggregators.sum),
    getNumericColumn("repoFinancingMTD", "Repo Financing MTD (Local)", "repoFinancingMtd", "Repo Financing MTD (Local)"),
    getNumericColumn("repoFinancingQTD", "Repo Financing QTD (Local)", "repoFinancingQtd", "Repo Financing QTD (Local)"),
    getNumericColumn("repoFinancingYTD", "Repo Financing YTD (Local)", "repoFinancingYtd", "Repo Financing YTD (Local)"),

    getNumericColumn(
      "collateralRC",
      "Collateral (RC)",
      "collateralRC",
      "Collateral (RC)"
    ),
	getNumericColumn(
      "totalFinancingRC",
      "Total Financing (RC)",
      "totalFinancingRC",
      "Total Financing (RC)",
      dpGrid.Aggregators.sum
    ),
	getNumericColumn(
      "allocatedFinancingRC",
      "Allocated Financing (RC)",
      "allocatedFinancingRC",
      "Allocated Financing (RC)",
      dpGrid.Aggregators.sum
    ),
	    getNumericColumn("cashRC", "Cash (RC)", "cashRC", "Cash (RC)"),
getNumericColumn(
      "dailyBorrowCostRC",
      "Daily Borrow Cost (RC)",
      "dailyBorrowCostRC",
      "Daily Borrow Cost (RC)",
      dpGrid.Aggregators.sum
    ),
	    getNumericColumn(
      "dailyExcessBorrowCostRC",
      "Daily Excess Borrow Cost (RC)",
      "dailyExcessBorrowCostRC",
      "Daily Excess Borrow Cost (RC)",
      dpGrid.Aggregators.sum
    ),
	    getNumericColumn(
      "dailyFinanceIncomeCostRC",
      "Debit Finance Income Cost (RC)",
      "dailyFinanceIncomeCostRC",
      "Debit Finance Income Cost (RC))",
      dpGrid.Aggregators.sum
    ),
	getNumericColumn(
      "dailyStockLendIncomeRC",
      "Daily Stock Lend Income (RC)",
      "dailyStockLendIncomeRC",
      "Daily Stock Lend Income (RC)",
      dpGrid.Aggregators.sum
    ),
	getNumericColumn(
      "directFinancingRC",
      "Direct Financing (RC)",
      "directFinancingRC",
      "Direct Financing (RC)",
      dpGrid.Aggregators.sum
    ),
	    getNumericColumn("swapFinancingDtdRC", "Swap Financing DTD (RC)", "swapFinancingDtdRC", "Swap Financing DTD (RC)", dpGrid.Aggregators.sum),
    getNumericColumn("swapFinancingMtdRC", "Swap Financing MTD (RC)", "swapFinancingMtdRC", "Swap Financing MTD (RC)"),
    getNumericColumn("swapFinancingQtdRC", "Swap Financing QTD (RC)", "swapFinancingQtdRC", "Swap Financing QTD (RC)"),
    getNumericColumn("swapFinancingYtdRC", "Swap Financing YTD (RC)", "swapFinancingYtdRC", "Swap Financing YTD (RC)"),

    getNumericColumn("repoFinancingDtdRC", "Repo Financing DTD (RC)", "repoFinancingDtdRC", "Repo Financing DTD (RC)", dpGrid.Aggregators.sum),
    getNumericColumn("repoFinancingMtdRC", "Repo Financing MTD (RC)", "repoFinancingMtdRC", "Repo Financing MTD (RC)"),
    getNumericColumn("repoFinancingQtdRC", "Repo Financing QTD (RC)", "repoFinancingQtdRC", "Repo Financing QTD (RC)"),
    getNumericColumn("repoFinancingYtdRC", "Repo Financing YTD (RC)", "repoFinancingYtdRC", "Repo Financing YTD (RC)"),

	    getNumericColumn("etbRC", "ETB Value (RC)", "etbRC", "ETB Value (RC)", dpGrid.Aggregators.sum),

    getNumericColumn("etbBorrowFeeRC", "ETB Borrow Fee (RC)", "etbBorrowFeeRC", "ETB Borrow Fee (RC)"),
    getNumericColumn("htbBorrowFeeRC", "HTB Borrow Fee (RC)", "htbBorrowFeeRC", "HTB Borrow Fee (RC)"),
	    getNumericColumn("htbRC", "HTB Value (RC)", "htbRC", "HTB Value (RC)", dpGrid.Aggregators.sum),
 getNumericColumn(
      "equityRC",
      "Equity (RC)",
      "actualEquityRC",
      "Equity (RC)"
    ),
	    getNumericColumn(
      "independentAmountRC",
      "Independent Amount (RC)",
      "independentAmountRC",
      "Independent Amount (RC)"
    ),
	    getNumericColumn(
      "slvRC",
      "Stock Lend Value (RC)",
      "slvRC",
      "Stock Lend Value (RC)"
    ),
	    getNumericColumn(
      "marketToMarketRC",
      "Market To Market (RC)",
      "markToMarketRC",
      "Market to Market (RC)"
    ),
	    getNumericColumn("oteRC", "OTE (RC)", "oteRC", "OTE (RC)"),

    getNumericColumn(
      "marginRC",
      "Margin (RC)",
      "initialMarginRC",
      "initialMargin (RC)"
    ),
	    getNumericColumn(
      "netDebitRC",
      "Net Debit (RC)",
      "netDebitRC",
      "Net Debit (RC)",
      dpGrid.Aggregators.sum
    ),
	    getNumericColumn(
      "rebateRC",
      "Rebate (RC)",
      "rebateRC",
      "Rebate (RC)",
      dpGrid.Aggregators.sum
    ),
	    getNumericColumn(
      "shortFinanceIncomeCostRC",
      "Short Finance Income Cost (RC)",
      "shortFinanceIncomeCostRC",
      "Short Finance Income Cost (RC)",
      dpGrid.Aggregators.sum
    ),
	getNumericColumn(
      "shortMarketValueRC",
      "Short Market Value (RC)",
      "smvRC",
      "Short Market Value (RC)"
    ),
	 getNumericColumn(
      "longMarketValueRC",
      "Long Market Value (RC)",
      "lmvRC",
      "Long Market Value (RC)"
    )
  ];
  return columns;
}
