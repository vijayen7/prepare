import {
  convertJavaNYCDashedDate,
  convertJavaDateTime
} from "commons/util";

export function getNumericColumn(id, name, field, toolTip, aggregator) {
  var column = {
    id: id,
    name: name,
    field: field,
    toolTip: toolTip,
    type: "number",
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.0",
    width: 100
  }
  if (aggregator !== undefined && aggregator != null) {
    if (typeof aggregator !== "function") {
      console.log("NOT AN FUNCTION" + id);
    } else {
      column["aggregator"] = aggregator;
    }
  }
  return column;
}

export function getNumericSpreadColumn(id, name, field, toolTip, aggregator) {
  var column = {
    id: id,
    name: name,
    field: field,
    toolTip: toolTip,
    type: "number",
    filter: true,
    sortable: true,
    formatter: function (row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null)
        return formatterFourDecimal(
          row,
          cell,
          value,
          columnDef,
          dataContext
        );
      else return null;
    },
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0.#####0",
    width: 100
  }
  if (aggregator !== undefined && aggregator != null) {
    if (typeof aggregator !== "function") {
      console.log("NOT AN FUNCTION" + id);
    } else {
      column["aggregator"] = aggregator;
    }
  }
  return column;
}

export function getTextColumn(id, name, field, toolTip, minWidth, maxWidth = 150) {
  var column = {
    id: id,
    name: name,
    field: field,
    toolTip: toolTip,
    type: "text",
    filter: true,
    sortable: true,
    formatter: function (row, cell, value, columnDef, dataContext) {
      return "<div  style=\"text-align: right;\">" +
        (value == null ? "n/a" : value) +
        "</div>"
    },
    excelFormatter: "#,##0",
    filter: {
      isFilterOnFormattedValue: true
    },
    headerCssClass: "b",
    minWidth: minWidth,
    maxWidth: maxWidth
  }
  return column;
}

export function getDateColumn(id, name, field, toolTip, minWidth) {
  var column = {
    id: id,
    name: name,
    field: field,
    toolTip: toolTip,
    type: "text",
    filter: true,
    sortable: true,
    formatter: function (row, cell, value, columnDef, dataContext, isExportToExcel) {
      return convertJavaNYCDashedDate(value);
    },
    excelDataFormatter: function (value) {
      return convertJavaNYCDashedDate(value);
    },
    headerCssClass: "b",
    minWidth: minWidth,
    filter: {
      isFilterOnFormattedValue: true
    }
  }
  return column;
}

export function getDateTimeColumn(id, name, field, toolTip, minWidth) {
  var column = {
    id: id,
    name: name,
    field: field,
    toolTip: toolTip,
    type: "text",
    filter: true,
    sortable: true,
    formatter: function (row, cell, value, columnDef, dataContext, isExportToExcel) {
      return convertJavaDateTime(value);
    },
    excelDataFormatter: function (value) {
      return convertJavaDateTime(value);
    },
    headerCssClass: "b",
    minWidth: minWidth,
    filter: {
      isFilterOnFormattedValue: true
    }
  }
  return column;
}

export function getNestedTextColumn(id, name, field, nestedField, toolTip, minWidth) {
  var column = {
    id: id,
    name: name,
    field: field,
    formatter: function (row, cell, value, columnDef, dataContext) {
      if (value !== undefined) {
        return value[nestedField];
      }
      return null;
    },
    excelDataFormatter: function (value) {
      return value[nestedField]
    },
    toolTip: toolTip,
    type: "text",
    sortable: true,
    headerCssClass: "b",
    minWidth: minWidth,
    filter: {
      isFilterOnFormattedValue: true
    }
  }
  return column;
}

export function formatterFourDecimal(row, cell, value, columnDef, dataContext) {
  return (
    "<div class='aln-rt'>" +
    (value == null
      ? "<span class='message'>n/a</span>"
      : Number(value).toFixed(4)) +
    "</div>"
  );
}
