import {
  getSelectedViewDefaultColumns,
  getSelectedViewTotalColumns
} from "../columnSelectorConfig/columnSelector";
import { detailUrl, batchedDetailUrl } from "../../api";
import { getSaveSettingsUrl } from "commons/util";
import { viewLevels } from "../../util";

export default function gridOptions(view, agreementType) {
  var options = {
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    exportToExcel: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    frozenColumn: 5,
    page: true,
    expandCollapseAll: true,
    onRowClick: function () {
      // highlight on row works if this callback is defined
    },
    highlightRowOnClick: true,
    isAutoWidth: false,
    saveSettings: {
      applicationId: 3,
      applicationCategory: "DetailReport_" + view,
      serviceURL: getSaveSettingsUrl(),
      onBeforeLoadedCallback: function (settingsData) { },
      loadedCallback: function (gridObject) { }
    },
    configureReport: {
      urlCallback: function () {
        return window.location.origin + batchedDetailUrl;
      }
    },
    customColumnSelection: {
      defaultcolumns: getSelectedViewDefaultColumns(view, agreementType),
      totalColumns: getSelectedViewTotalColumns(view)
    }
  };

  if (view == viewLevels.GENEVA_BUNDLE_CUSTODIAN_ACCOUNT) {
    options.enableMultilevelGrouping = {
      hideGroupingHeader: false,
      showGroupingKeyInColumn: false,
      initialGrouping: ["methodologyName", "date", "bundle", "chargeType"]
    };
    options.frozenColumn = 0;
  }

  return options;
}
