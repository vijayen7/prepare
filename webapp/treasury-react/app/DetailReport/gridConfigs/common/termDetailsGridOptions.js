export default function termDetailsGridOptions() {
  return {
    applyFilteringOnGrid : true,
    exportToExcel : true,
    page: true,
    isAutoWidth: false,
    forceFitColumns:true
  };
}
