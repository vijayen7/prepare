import { getNumericColumn, getTextColumn} from "./common/columnHelper"
import getColumnOptions from "./common/commonConfig"

export default function getCustodianAccountColumns() {
  var columns = getColumnOptions();

  columns.push(getTextColumn("custodianAccountName", "Custodian AccountId Name", "custodianAccountName", "Custodian AccountId Name", 100));

  return columns;
}
