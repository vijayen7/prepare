import { put, takeEvery, call, all } from "redux-saga/effects";
import { ToastService } from 'arc-react-components';
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from "commons/constants";
import {
    FETCH_CALCULATION_DATA,
    FETCH_FINANCING_TERM_DETAILS,
    MAX_ROW_SIZE
} from "./constants"


import { getCalculationData, getCalculationFilters, getFinancingTermDetails } from "./api";
import { getPayload } from "./util";

function* fetchData(api, action) {
  try {
    yield put({ type: START_LOADING });

    const data = yield call(api, action.payload);
    yield put({ type: `${action.type}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}


function* fetchCalculationData(action) {
  try {
    yield put({ type: START_LOADING });
    let checkRowLimit = (action.payload.startDate != action.payload.endDate)
    const filters = yield call (getCalculationFilters, action.payload);
    let data = [],filter, payload,result;
    if(filters.message == null){
      for(filter in filters){
        try{
          payload = getPayload(filters[filter])
          result = yield call (getCalculationData, payload)
          data = [...data, ...result];
          if(checkRowLimit && data.length > MAX_ROW_SIZE){
            data = data.slice(0,MAX_ROW_SIZE);
            ToastService.append({
              content: "Displaying only Top " + MAX_ROW_SIZE + " Records. Please search with additional filters or for smaller date range.",
              type: ToastService.ToastType.CRITICAL,
              placement: ToastService.Placement.TOP_RIGHT
            })
            break;
          }
        }
        catch(e){
          yield put({ type: HANDLE_EXCEPTION });
          yield put({ type: `${action.type}_FAILURE` });
          return;
        }
      }
      yield put({ type: `${action.type}_SUCCESS`, data });
    }
    else{
      let error = filters.message
      if(error.search("No data") > -1){
        yield put({ type: `${action.type}_SUCCESS`, data });
        return;
      }
      else {
        let start = error.indexOf(":")+1
        let end = error.indexOf("." , start)+1
        error = error.substring(start,end)
      }
      ToastService.append({
        content: error,
        type: ToastService.ToastType.CRITICAL,
        placement: ToastService.Placement.TOP_RIGHT,
        dismissTime: 20000
      })
    }
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* calculationDataSaga() {
  yield [
    takeEvery(FETCH_CALCULATION_DATA, fetchCalculationData),
    takeEvery(FETCH_FINANCING_TERM_DETAILS, fetchData.bind(null, getFinancingTermDetails)),
  ];
}

export default function* detailReportRootSaga() {
  yield all([calculationDataSaga()]);
}
