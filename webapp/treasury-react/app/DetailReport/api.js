import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
import { getEncodedParams } from "./util";
import { fetchURL } from "../commons/util";

export let detailUrl = "", batchedDetailUrl = "";
export function getCalculationData(payload) {
  const encodedParams = getEncodedParams(payload, "com.arcesium.treasury.model.financing.calculation.filter.FinancingCalculationDataFilter");
  detailUrl = `${BASE_URL}service/financingCalculationDataInternalService/getFinancingCalculationData?inputFormat=json&format=json&financingCalculationDataFilter=${encodedParams}`;
  return fetch(detailUrl, {
    credentials: "include"
  }).then(data => data.json());
}

export function getCalculationFilters(payload){
  const encodedParams = getEncodedParams(payload,"com.arcesium.treasury.model.financing.calculation.filter.FinancingCalculationDataFilter");
  batchedDetailUrl = `${BASE_URL}service/financingCalculationDataInternalService/getFinancingCalculationData?inputFormat=json&format=json&financingCalculationDataFilter=${encodedParams}`;
  let filtersUrl = `${BASE_URL}service/financingCalculationDataInternalService/getFinancingCalculationDataFilters?inputFormat=json&format=json&financingCalculationDataFilter=${encodedParams}`;
  return fetchURL(filtersUrl);
}

export function getFinancingTermDetails(payload) {
  const paramString = queryString.stringify(payload);
  var url = `${BASE_URL}service/financingCalculationDataInternalService/getAgreementTermDetails?format=json&${paramString}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
