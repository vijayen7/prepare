import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SideBar from "./containers/SideBar";
import Footer from "commons/components/Footer";
import Loader from "commons/container/Loader";
import CommonGrid from "./containers/Grid";

export default class DetailReport extends Component {
  constructor(props) {
    super(props);
    this.getBreadCrumbs = this.getBreadCrumbs.bind(this);
  }

  getBreadCrumbs() {
    return [
      {
        name: 'Treasury',
        link: '/treasury'
      },
      {
        name: 'Detail Report',
       link: './detailReport.html'
      }
    ];
  }

  componentDidMount() {
      var header = this.header;
      var breadcrumbs = this.getBreadCrumbs();

      if (header.ready) {
        header.setBreadcrumb(breadcrumbs);
      } else {
        header.addEventListener('ready', header.setBreadcrumb(breadcrumbs));
     }
  }

  render() {
    return (
      <div className="layout--flex--row">
        <arc-header

          ref={header => {
            this.header = header;
          }}
          className="size--content"
          user={USER}
          modern-themes-enabled
        />
        <Loader />
        <div className="layout--flex padding--top">
          <div className="size--content padding--right">
            <SideBar />
          </div>
              <CommonGrid />
        </div>
      </div>
    );
  }
}
