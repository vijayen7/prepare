import {
  FETCH_CALCULATION_DATA,
  DESTROY_DATA_STATE_FOR_AGREEMENT_TERMS,
  UPDATE_SELECTED_VIEW,
  UPDATE_DRILL_DOWN_SELECTED_FILTERS,
  UPDATE_DRILL_THROUGH_SELECTED_FILTERS,
  FETCH_FINANCING_TERM_DETAILS,
  UPDATE_DRILL_THROUGH_DATA,
  REMOVE_DRILL_THROUGH_DATA,
  UPDATE_DRILL_THROUGH_PAYLOAD,
  REMOVE_DRILL_THROUGH_PAYLOAD,
  UPDATE_DRILL_THROUGH_SELECTED_VIEW,
  UPDATE_GRID_DATA,
  RESIZE_CANVAS
} from "./constants";

export function updateDrillDownSelectedFilters(payload) {
  return {
    type: UPDATE_DRILL_DOWN_SELECTED_FILTERS,
    payload
  };
}

export function updateDrillThroughSelectedFilters(payload) {
  return {
    type: UPDATE_DRILL_THROUGH_SELECTED_FILTERS,
    payload
  };
}


export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

export function destroyAgreementTermsDataState() {
  return {
    type: DESTROY_DATA_STATE_FOR_AGREEMENT_TERMS
  };
}

export function updateSelectedView(selectedView) {
  return {
    type: UPDATE_SELECTED_VIEW,
    selectedView
  };
}

export function updateDrillThroughSelectedView(selectedView) {
  return {
    type: UPDATE_DRILL_THROUGH_SELECTED_VIEW,
    selectedView
  };
}

export function updateGridData(gridData) {
  return {
    type: UPDATE_GRID_DATA,
    gridData
  };
}

export function fetchCalculationData(payload) {
  return {
    type: FETCH_CALCULATION_DATA,
    payload
  };
}

export function updateDrillThroughData(payload) {
  return {
    type: UPDATE_DRILL_THROUGH_DATA,
    payload
  }
}

export function removeDrillThroughData(elementLength) {
  return {
    type: REMOVE_DRILL_THROUGH_DATA,
    elementLength
  }
}

export function updateDrillThroughPayload(payload) {
  return {
    type: UPDATE_DRILL_THROUGH_PAYLOAD,
    payload
  }
}

export function removeDrillThroughPayload(elementLength) {
  return {
    type: REMOVE_DRILL_THROUGH_PAYLOAD,
    elementLength
  }
}

export function fetchFinancingTermDetails(payload) {
  return {
    type: FETCH_FINANCING_TERM_DETAILS,
    payload
  };
}
