import { BASE_URL } from "commons/constants";

export function getIntegerArrayFromObjectArray(data) {
  var filterArray = [];
  data.forEach(item => {
    filterArray.push(item.key);
  });
  return filterArray;
}

export function getObjectArrayFromIntegerId(keyId) {
  return [{ key: keyId, value: keyId }];
}

export function createAPIPayload(payload) {
  var payload = Object.assign({}, payload);
  Object.keys(payload).forEach(function(key) {
    if (
      key !== "endDate" &&
      key !== "startDate" &&
      key !== "includeYtdAccrual" &&
      key !== "financingCalculationDataLevel" &&
      key !== "spns"
    ) {
      if (payload[key].length === 0) {
        payload[key] = null;
      } else if (key == "reportingCurrencyId") {
        payload[key] = `${payload[key][0].key}`
      } else {
        payload[key] = getIntegerArrayFromObjectArray(payload[key]);
      }
    }
  });

  return payload;
}

export function getEncodedParams(payload, className) {
  payload["@CLASS"] = className;
  const jsonString = JSON.stringify(payload);
  return encodeURI(jsonString);
}

export function getViewFilterData() {
  return Object.keys(viewLevels).map(key => {
    return { key: key, value: viewLevels[key] };
  });
}

export function getViewFilterDataForView(view) {
  for (let key in viewLevels) {
    if (key == view) return { key: key, value: viewLevels[key] };
  }
}

export function getDataLevelForView(view) {
  switch (view) {
    case viewLevels.METHODOLOGY:
      return 1;
    case viewLevels.BUSINESS_UNIT_GROUP:
      return 8;
    case viewLevels.BUSINESS_UNIT:
      return 2;
    case viewLevels.POSITION:
      return 4;
    case viewLevels.BUNDLE_POSITION:
      return 7;
    case viewLevels.CUSTODIAN_ACCOUNT:
      return 5;
    case viewLevels.ASSET_CLASS:
      return 6;
    case viewLevels.INTEREST_BUNDLE_CUSTODIAN_ACCOUNT:
      return 9;
    case viewLevels.GENEVA_BUNDLE_CUSTODIAN_ACCOUNT:
      return 10;
  }
}

export let viewLevels = {
  METHODOLOGY: "Methodology",
  BUSINESS_UNIT_GROUP: "Business Unit Group",
  BUSINESS_UNIT: "Business Unit",
  POSITION: "Position",
  BUNDLE_POSITION: "Bundle Position",
  CUSTODIAN_ACCOUNT: "CA",
  ASSET_CLASS: "Asset Class",
  INTEREST_BUNDLE_CUSTODIAN_ACCOUNT: "Interest Bundle CA",
  GENEVA_BUNDLE_CUSTODIAN_ACCOUNT: "Accounting Bundle CA"
};

export function getPayload(filter) {
  var payload = {
    startDate: filter.startDate,
    endDate: filter.endDate,
    agreementTypeIds: filter.agreementTypeIds? filter.agreementTypeIds: null,
    cpeIds: filter.cpeIds? filter.cpeIds : null,
    legalEntityIds: filter.legalEntityIds? filter.legalEntityIds : null,
    nettingGroupIds: filter.nettingGroupIds? filter.nettingGroupIds: null,
    ccySpns: filter.ccySpns? filter.ccySpns : null,
    reportingCurrencyId: filter.reportingCurrencyId? filter.reportingCurrencyId : { key: 1760000, value: `USD [1760000]`},
    financingCalculationDataLevel: filter.financingCalculationDataLevel,
    businessUnitGroupTypeIds: filter.businessUnitGroupTypeIds? filter.businessUnitGroupTypeIds : null,
    businessUnitGroupIds: filter.businessUnitGroupIds? filter.businessUnitGroupIds : null,
    businessUnitIds: filter.businessUnitIds? filter.businessUnitIds : null,
    bundleIds: filter.bundleIds? filter.bundleIds : null,
    lbTypes: filter.lbTypes? filter.lbTypes : null,
    termIds: filter.termIds? filter.termIds: null,
    portfolioIds: filter.portfolioIds? filter.portfolioIds : null,
    spns: filter.spns? filter.spns : null,
    includeYtdAccrual: filter.includeYtdAccrual? filter.includeYtdAccrual : null,
  };
  return payload;
}
