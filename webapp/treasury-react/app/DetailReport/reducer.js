import { combineReducers } from "redux";

import {
  FETCH_CALCULATION_DATA,
  FETCH_FINANCING_TERM_DETAILS,
  DESTROY_DATA_STATE_FOR_AGREEMENT_TERMS,
  UPDATE_DRILL_DOWN_SELECTED_FILTERS,
  UPDATE_DRILL_THROUGH_SELECTED_FILTERS,
  UPDATE_DRILL_THROUGH_SELECTED_VIEW,
  UPDATE_SELECTED_VIEW,
  UPDATE_DRILL_THROUGH_DATA,
  REMOVE_DRILL_THROUGH_DATA,
  UPDATE_DRILL_THROUGH_PAYLOAD,
  REMOVE_DRILL_THROUGH_PAYLOAD,
  UPDATE_GRID_DATA,
  RESIZE_CANVAS
} from "./constants";

function drillDownSelectedFilterReducer(state = {}, action) {
  switch (action.type) {
    case `${UPDATE_DRILL_DOWN_SELECTED_FILTERS}_SUCCESS`:
      return action.selectedFilters || {};
  }
  return state;
}

function drillThroughSelectedFilterReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_DRILL_THROUGH_SELECTED_FILTERS:
      return action.payload || {};
  }
  return state;
}

function selectedViewReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_SELECTED_VIEW:
      return action.selectedView || state;
  }
  return state;
}

function selectedDrillThroughViewReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_DRILL_THROUGH_SELECTED_VIEW:
      return action.selectedView || state;
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

function calculationDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_CALCULATION_DATA}_SUCCESS`:
      var data = action.data;
      let id = 1;
      data.map(row => {
        Object.assign(row, row["calculationBaseData"]);
        row["id"] = id++;
        return row;
      });
      return data;

      case UPDATE_GRID_DATA:
        if(action.gridData == undefined)
          return [];
        return action.gridData;
  }
  return state;
}

function financingTermDataReducer(state = [], action) {
  if (action.type == `${FETCH_FINANCING_TERM_DETAILS}_SUCCESS`) {
    return action.data;
  }
  if (action.type == DESTROY_DATA_STATE_FOR_AGREEMENT_TERMS)
  return [];
  return state;
}

function drillThroughDataReducer(state, action) {
  switch (action.type) {
    case UPDATE_DRILL_THROUGH_DATA:
      if(action.payload == undefined) {
        return state || [];
      }
      return [
        ...state,
        action.payload
      ]
    case REMOVE_DRILL_THROUGH_DATA:
      if(action.elementLength == undefined) {
        return [];
      }
      return state.slice(0, action.elementLength).concat(state.slice(action.elementLength + 1, state.length));
  }
  return state || [];
}

function drillThroughPayloadReducer(state = [], action) {
  switch(action.type) {
    case UPDATE_DRILL_THROUGH_PAYLOAD:
      if(action.payload == undefined) {
        return state || [];
      }
      return [
        ...state,
        action.payload
      ]
    case REMOVE_DRILL_THROUGH_PAYLOAD:
      if(action.elementLength == undefined) {
        return [];
      }
      return state.slice(0, action.elementLength).concat(state.slice(action.elementLength + 1, state.length));
  }
  return state || [];
}

const rootReducer = combineReducers({
  gridData: calculationDataReducer,
  financingTermData: financingTermDataReducer,
  drillDownFilters: drillDownSelectedFilterReducer,
  selectedView: selectedViewReducer,
  drillThroughData: drillThroughDataReducer,
  drillThroughPayload: drillThroughPayloadReducer,
  resizeCanvas: resizeCanvasReducer,
  drillThroughFilter: drillThroughSelectedFilterReducer,
  selectedDrillThroughView: selectedDrillThroughViewReducer
});

export default rootReducer;
