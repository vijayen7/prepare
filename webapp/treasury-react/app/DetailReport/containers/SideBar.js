import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Error from "commons/components/Error";
import { Card } from "arc-react-components";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import NettingGroupFilter from "commons/container/NettingGroupFilter";
import SaveSetting from "commons/container/SaveSetting";
import SidebarWithCopySearchUrl from "commons/components/SidebarWithCopySearchUrl";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import BusinessUnitGroupTypeFilter from "commons/container/BusinessUnitGroupTypeFilter";
import BusinessUnitGroupFilter from "commons/container/BusinessUnitGroupFilter";
import BusinessUnitFilter from "commons/container/BusinessUnitFilter";
import BundleFilter from "commons/container/BundleFilter";
import BundleGroupFilter from "commons/container/BundleGroupFilter";
import BundleGroupTypeFilter from "commons/container/BundleGroupTypeFilter";
import PositionTypeFilter from "commons/container/PositionTypeFilter";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import InputFilter from "commons/components/InputFilter";
import DateFilter from "commons/container/DateFilter";
import SpnFilterAutocomplete from "commons/components/SpnFilterAutocomplete";
import CheckboxFilter from "commons/components/CheckboxFilter";
import { getCommaSeparatedListValue } from "commons/util";
import {
  getMultiSelectFilterData,
  getSingleSelectFilterData,
  viewLevels,
  getViewFilterData,
  getViewFilterDataForView,
  getDataLevelForView,
  getFilterDataInArray,
  getIntegerArrayFromObjectArray,
  createAPIPayload
} from "../util";
import {
  fetchCalculationData,
  destroyAgreementTermsDataState,
  updateDrillDownSelectedFilters,
  updateSelectedView,
  removeDrillThroughPayload,
  removeDrillThroughData,
  updateDrillThroughSelectedView
} from "../actions";
import {
  getPreviousBusinessDay
} from "commons/util";
import store from "../../store";

let DEFAULT_VIEW = "METHODOLOGY";
class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.onViewChange = this.onViewChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.createFilterDataJSON = this.createFilterDataJSON.bind(this);
    this.JSONToFilterData = this.JSONToFilterData.bind(this);
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
    this.props.updateSelectedView(getViewFilterDataForView(DEFAULT_VIEW));
    this.props.updateDrillThroughSelectedView(getViewFilterDataForView(DEFAULT_VIEW));
  }

  getDefaultFilters() {
    return {
      viewLevels,
      selectedLegalEntities: [],
      selectedAgreementTypes: [],
      selectedCpes: [],
      selectedCurrencies: [],
      selectedReportingCurrency: { key: 1760000, value: `USD [1760000]`},
      selectedNettingGroups: [],
      selectedBusinessUnitGroupTypes: [{ key: 0, value: "Standard Partition" }],
      selectedBusinessUnitGroups: [],
      selectedBusinessUnits: [],
      selectedBundles: [],
      selectedPositionTypes: [],
      selectedSpns: [],
      selectedTermIds: [],
      selectedPortfolios: [],
      includeYtdAccrual: false,
      fromDate: getPreviousBusinessDay(),
      toDate: getPreviousBusinessDay()
    };
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
    this.props.updateSelectedView(getViewFilterDataForView(DEFAULT_VIEW));

  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  JSONToFilterData(filterData) {
    var newState = Object.assign({}, this.state);

    newState["fromDate"] = filterData["startDate"];
    newState["toDate"] = filterData["endDate"];

    newState["selectedAgreementTypes"] = filterData["agreementTypeIds"];
    newState["selectedCpes"] = filterData["cpeIds"];
    newState["selectedLegalEntities"] = filterData["legalEntityIds"];
    newState["selectedNettingGroups"] = filterData["nettingGroupIds"];
    newState["selectedCurrencies"] = filterData["ccySpns"];
    newState["selectedReportingCurrency"] = filterData["reportingCurrency"];

    newState["selectedBusinessUnitGroupTypes"] =
      filterData["businessUnitGroupTypeIds"];
    newState["selectedBusinessUnitGroups"] = filterData["businessUnitGroupIds"];
    newState["selectedBusinessUnits"] = filterData["businessUnitIds"];
    newState["selectedBundles"] = filterData["bundleIds"];
    newState["selectedSpns"] = filterData["spns"];

    newState["selectedTermIds"] = filterData["termIds"];
    newState["selectedPortfolios"] = filterData["portfolioIds"];
    newState["includeYtdAccrual"] = filterData["includeYtdAccrual"];

    this.setState(newState);
  }

  createFilterDataJSON() {
    var payload = {
      startDate: this.state.fromDate,
      endDate: this.state.toDate,
      agreementTypeIds: this.state.selectedAgreementTypes,
      cpeIds: this.state.selectedCpes,
      legalEntityIds: this.state.selectedLegalEntities,
      nettingGroupIds: this.state.selectedNettingGroups,
      ccySpns: this.state.selectedCurrencies,
      reportingCurrencyId: [this.state.selectedReportingCurrency],

      businessUnitGroupTypeIds: this.state.selectedBusinessUnitGroupTypes,
      businessUnitGroupIds: this.state.selectedBusinessUnitGroups,
      businessUnitIds: this.state.selectedBusinessUnits,
      bundleIds: this.state.selectedBundles,
      lbTypes: this.state.selectedPositionTypes,
      termIds: this.state.selectedTermIds,
      portfolioIds: this.state.selectedPortfolios,
      spns: this.state.selectedSpns,
      includeYtdAccrual: this.state.includeYtdAccrual
    };
    return payload;
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  onViewChange(params, callback = ()=>{}) {
    this.onSelect(params);
    this.setState({ selectedSpns: [], includeYtdAccrual: false }, () => {
      this.props.updateDrillDownSelectedFilters(this.createFilterDataJSON());
      this.props.updateSelectedView(params.value);
      callback();
    })
  }
  createPayload() {
    var payload = this.createFilterDataJSON();
    this.props.updateDrillDownSelectedFilters(payload);
    return createAPIPayload(payload);
  }

  handleClick() {
    this.props.destroyAgreementTermsDataState();
    var payload = this.createPayload();
    payload["financingCalculationDataLevel"] = this.props.selectedView.key;
    if (
      this.refs.spnInputRef &&
      this.refs.spnInputRef.refs.autocomplete.value.trim() !== ""
    ) {
      payload["spns"] = getCommaSeparatedListValue(
        this.refs.spnInputRef.refs.autocomplete.value
      )
        .split(",")
        .map(Number);
    }
    this.props.updateDrillThroughSelectedView(getViewFilterDataForView(this.props.selectedView.key));
    this.props.removeDrillThroughPayload();
    this.props.removeDrillThroughData();
    this.props.fetchCalculationData(payload);
  }

  loadConditionalFilters() {
    return [
      this.loadBusinessUnitGroupTypeFilter(),
      this.loadBusinessUnitGroupFilter(),
      this.loadBusinessUnitFilter(),
      this.loadBundleFilter(),
      this.loadPositionTypeFilter(),
      this.loadSPNFilter(),
      this.loadMtdAccrualFilter()
    ];
  }

  loadBusinessUnitGroupTypeFilter() {
    if (
      [viewLevels.METHODOLOGY, viewLevels.BUSINESS_UNIT_GROUP].includes(
        this.props.selectedView.value
      )
    ) {
      return (
        <BusinessUnitGroupTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnitGroupTypes}
        />
      );
    }
  }

  loadBusinessUnitGroupFilter() {
    if (
      [viewLevels.BUSINESS_UNIT_GROUP].includes(this.props.selectedView.value)
    ) {
      return (
        <BusinessUnitGroupFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnitGroups}
        />
      );
    }
  }

  loadBusinessUnitFilter() {
    if (
      [
        viewLevels.BUSINESS_UNIT,
        viewLevels.BUNDLE,
        viewLevels.BUNDLE_POSITION,
        viewLevels.INTEREST_BUNDLE_CUSTODIAN_ACCOUNT,
        viewLevels.GENEVA_BUNDLE_CUSTODIAN_ACCOUNT
      ].includes(this.props.selectedView.value)
    ) {
      return (
        <BusinessUnitFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnits}
        />
      );
    }
  }

  loadBundleFilter() {
    if (
      [
        viewLevels.BUNDLE,
        viewLevels.BUNDLE_POSITION,
        viewLevels.INTEREST_BUNDLE_CUSTODIAN_ACCOUNT,
        viewLevels.GENEVA_BUNDLE_CUSTODIAN_ACCOUNT
      ].includes(this.props.selectedView.value)
    ) {
      return (
        <BundleFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBundles}
        />
      );
    }
  }

  loadPositionTypeFilter() {
    if (
      [viewLevels.POSITION, viewLevels.BUNDLE_POSITION].includes(
        this.props.selectedView.value
      )
    ) {
      return (
        <PositionTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedPositionTypes}
        />
      );
    }
  }

  loadSPNFilter() {
    if (
      [viewLevels.POSITION, viewLevels.BUNDLE_POSITION].includes(
        this.props.selectedView.value
      )
    ) {
      return (
        <SpnFilterAutocomplete
          onSelect={this.onSelect}
          label="Security Identifiers"
          ref="spnInputRef"
        />
      );
    }
  }

  loadMtdAccrualFilter() {
    if (
      [viewLevels.POSITION, viewLevels.BUNDLE_POSITION].includes(
        this.props.selectedView.value
      )
    ) {
      return (
        <CheckboxFilter
          onSelect={this.onSelect}
          stateKey="includeYtdAccrual"
          label="Include YTD accrual"
          defaultChecked={this.state.includeYtdAccrual}
          style="left"
        />
      );
    }
  }

  setFilters = (filters) => {
    let selectedView = filters['selectedView'] || {key:DEFAULT_VIEW, value:viewLevels[DEFAULT_VIEW]};
    this.onViewChange({key:'selectedView', value: selectedView},()=>{
          this.setState({...this.state,...filters}, this.handleClick);
    })
  }

  render() {
    return (
      <SidebarWithCopySearchUrl
        setFilters={this.setFilters}
        selectedFilters={this.state}
        handleReset={this.handleReset}
        handleSearch={this.handleClick}
        copySearchUrl>
        <Card>
          <SingleSelectFilter
            data={getViewFilterData()}
            onSelect={this.onViewChange}
            selectedData={this.props.selectedView}
            stateKey="selectedView"
            label="View By"
          />

          <DateFilter
            onSelect={this.onSelect}
            stateKey="fromDate"
            data={this.state.fromDate}
            label="From"
          />
          <DateFilter
            onSelect={this.onSelect}
            stateKey="toDate"
            data={this.state.toDate}
            label="To"
          />
        </Card>
        <Card>
          <AgreementTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedAgreementTypes}
          />
          <LegalEntityFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntities}
          />
          <CpeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCpes}
          />
          <CurrencyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCurrencies}
          />
          <NettingGroupFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedNettingGroups}
          />

          {this.loadConditionalFilters()}
        </Card>
        <Card>
          <ReportingCurrencyFilter
            label="Reporting Currency"
            onSelect={this.onSelect}
            selectedData={this.state.selectedReportingCurrency}
            multiSelect={false}
            stateKey="selectedReportingCurrency"
          />
        </Card>
      </SidebarWithCopySearchUrl>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCalculationData,
      destroyAgreementTermsDataState,
      updateSelectedView,
      updateDrillDownSelectedFilters,
      removeDrillThroughPayload,
      removeDrillThroughData,
      updateDrillThroughSelectedView
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    selectedView: state.detailReport.selectedView,
    drillDownFilters: state.detailReport.drillDownFilters
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideBar);
