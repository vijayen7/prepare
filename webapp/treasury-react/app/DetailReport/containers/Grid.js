import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DetailGrid from "commons/components/GridWithCellClick";
import cloneDeep from "lodash/cloneDeep";
import TermGrid from "commons/components/Grid";
import Message from "commons/components/Message";
import { URL } from 'commons/constants';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import { Layout } from "arc-react-components";
import methodologyGridColumns from "../gridConfigs/methodologyColumnConfig";
import businessUnitGroupGridColumns from "../gridConfigs/businessUnitGroupColumnConfig";
import businessUnitGridColumns from "../gridConfigs/businessUnitColumnConfig";
import bundleGridColumns from "../gridConfigs/bundleColumnConfig";
import positionGridColumns from "../gridConfigs/positionColumnConfig";
import bundlePositionGridColumns from "../gridConfigs/bundlePositionColumnConfig";
import custodianAccountGridColumns from "../gridConfigs/custodianAccountColumnConfig";
import assetClassGridColumns from "../gridConfigs/assetClassColumnConfig";
import bundleCAColumns from "../gridConfigs/bundleCAColumnConfig";
import gridOptions from "../gridConfigs/common/gridOptions";
import termDetailsGridOptions from "../gridConfigs/common/termDetailsGridOptions";
import termDetailsGridColumns from "../gridConfigs/common/termDetailsGridColumns";
import {
  viewLevels,
  getViewFilterDataForView,
  getDataLevelForView,
  getObjectArrayFromIntegerId,
  createAPIPayload
} from "../util";

import {
  fetchCalculationData,
  fetchFinancingTermDetails,
  destroyDataState,
  updateDrillDownSelectedFilters,
  updateSelectedView,
  updateDrillThroughData,
  removeDrillThroughData,
  updateDrillThroughPayload,
  removeDrillThroughPayload,
  updateDrillThroughSelectedFilters,
  updateGridData,
  resizeCanvas,
  destroyAgreementTermsDataState,
  updateDrillThroughSelectedView
} from "../actions";

class CommonGrid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.drillThroughHandler = this.drillThroughHandler.bind(this);
    this.fetchDrillThroughData = this.fetchDrillThroughData.bind(this);
  }

  componentWillMount() {
    this.setState(this.getDefaultState());
  }

  componentWillReceiveProps(nextProps) {
    var newState = {};

    switch (nextProps.selectedDrillThroughView.value) {
      case viewLevels.METHODOLOGY:
        newState.gridColumns = methodologyGridColumns;
        break;
      case viewLevels.BUSINESS_UNIT_GROUP:
        newState.gridColumns = businessUnitGroupGridColumns;
        break;
      case viewLevels.BUSINESS_UNIT:
        newState.gridColumns = businessUnitGridColumns;
        break;
      case viewLevels.BUNDLE:
        newState.gridColumns = bundleGridColumns;
        break;
      case viewLevels.POSITION:
        newState.gridColumns = positionGridColumns;
        break;
      case viewLevels.BUNDLE_POSITION:
        newState.gridColumns = bundlePositionGridColumns;
        break;
      case viewLevels.CUSTODIAN_ACCOUNT:
        newState.gridColumns = custodianAccountGridColumns;
        break;
      case viewLevels.ASSET_CLASS:
        newState.gridColumns = assetClassGridColumns;
        break;
      case viewLevels.INTEREST_BUNDLE_CUSTODIAN_ACCOUNT:
        newState.gridColumns = bundleCAColumns;
        break;
      case viewLevels.GENEVA_BUNDLE_CUSTODIAN_ACCOUNT:
        newState.gridColumns = bundleCAColumns;
        break;
    }
    newState.gridData = nextProps.gridData;
    newState.selectedView = nextProps.selectedView;
    newState.drillThroughFilter = nextProps.drillThroughFilter;
    newState.drillDownFilters = nextProps.drillDownFilters;
    newState.selectedDrillThroughView = nextProps.selectedDrillThroughView;
    this.setState(newState);
  }

  getDefaultState() {
    return {
      selectedView: "",
      gridData: [],
      gridColumns: [],
      drillThroughFilter: [],
      drillDownFilters: [],
      selectedDrillThroughView: ""
    };
  }

  // create complete payload object from row args.

  drillThroughHandler(args) {
    var drillDownFilters = null;
    if(this.state.drillThroughFilter && this.state.drillThroughFilter.length > 0) {
      drillDownFilters = this.state.drillThroughFilter;
    }
    else {
      drillDownFilters = cloneDeep(this.state.drillDownFilters);
    }
    var  newView = null;
    this.props.updateDrillThroughData(this.state.gridData);
    let drillThroughPayload = cloneDeep(drillDownFilters);
    drillThroughPayload["financingCalculationDataLevel"] = cloneDeep(this.state.selectedDrillThroughView.key);
    this.props.updateDrillThroughPayload(drillThroughPayload);
    drillDownFilters["cpeIds"] = getObjectArrayFromIntegerId(args.item.cpeId);
    drillDownFilters["legalEntityIds"] = getObjectArrayFromIntegerId(
      args.item.legalEntityId
    );
    drillDownFilters["agreementTypeIds"] = getObjectArrayFromIntegerId(
      args.item.agreementType.agreementTypeId
    );

    if (
      args.item.businessUnitGroupTypeId !== undefined &&
      args.item.businessUnitGroupTypeId != -1
    ) {
      drillDownFilters["businessUnitGroupTypeId"] = getObjectArrayFromIntegerId(
        args.item.businessUnitGroupTypeId
      );
    }

    if (
      args.item.businessUnitId !== undefined &&
      args.item.businessUnitId != -1
    ) {
      drillDownFilters["businessUnitIds"] = getObjectArrayFromIntegerId(
        args.item.businessUnitId
      );
    }

    if (args.item.bundleId !== undefined && args.item.bundleId != -1) {
      drillDownFilters["bundleIds"] = getObjectArrayFromIntegerId(args.item.bundleId);
    }

    let ele = args.event.target;
    let { role } = ele.dataset;
    switch (this.state.selectedDrillThroughView.key) {
      case "GENEVA_BUNDLE_CUSTODIAN_ACCOUNT":
        drillDownFilters["financingCalculationDataLevel"] = "BUNDLE_POSITION";
        newView = "BUNDLE_POSITION";
        break;
      case "BUSINESS_UNIT":
        drillDownFilters["financingCalculationDataLevel"] = "GENEVA_BUNDLE_CUSTODIAN_ACCOUNT";
        newView = "GENEVA_BUNDLE_CUSTODIAN_ACCOUNT";
        break;
      case "BUSINESS_UNIT_GROUP":
        drillDownFilters["financingCalculationDataLevel"] = "BUSINESS_UNIT";
        newView = "BUSINESS_UNIT";
        break;
      case "POSITION":
        drillDownFilters["financingCalculationDataLevel"] = "BUNDLE_POSITION";
        newView = "BUNDLE_POSITION";
        break;
      case "METHODOLOGY":
        if(role === 'Position') {
          drillDownFilters["financingCalculationDataLevel"] = "POSITION";
          newView = "POSITION";
        } else {
          drillDownFilters["financingCalculationDataLevel"] = "BUSINESS_UNIT_GROUP";
          newView = "BUSINESS_UNIT_GROUP";
        }
        break;
    }
    this.props.updateDrillThroughSelectedView(getViewFilterDataForView(newView));
    this.props.updateDrillThroughSelectedFilters(drillDownFilters);
    this.props.fetchCalculationData(createAPIPayload(drillDownFilters));
  }

  onCellClickHandler(args) {
    switch (args.colId) {
      case "agreementTermDetails":
        var payload = {
          dataLevel: args.item.detailLevel,
          effectiveDate: args.item.date,
          methodologyTermId: args.item.termId
        };
        this.props.fetchFinancingTermDetails(payload);
        break;
      case "drillThrough":
        this.drillThroughHandler(args);
        break;
    }
  }

  fetchDrillThroughData() {
    var payloadData = this.props.drillThroughPayload;
    var resultData = this.props.drillThroughData;
    var viewLevel = payloadData.length;
    var payload = payloadData[viewLevel - 1];
    var result = resultData[viewLevel - 1]
    this.props.updateDrillThroughSelectedView(getViewFilterDataForView(payload["financingCalculationDataLevel"]));
    this.props.updateDrillThroughSelectedFilters(payload);
    this.props.updateGridData(result);
    this.props.removeDrillThroughData(viewLevel - 1);
    this.props.removeDrillThroughPayload(viewLevel - 1);
  }

  renderLinkPanel() {
    if (
      [viewLevels.POSITION, viewLevels.BUNDLE_POSITION].includes(
        this.props.selectedView.value
      )
    ) {
      return (
        <LinkPanel
          links={[
            <Link
              id="cofi-repo-financing-rule"
              text="View all Cofi Repo Financing Rule"
              href={`${URL}/treasury/financing/cofiRepoFinancing.html`}
            />
          ]}
        />
      );
    }
  }

  reportingCurrencyMessage(ccy) {
    if (ccy == undefined) {
      return (<ul className="bullet">
        <li className="critical">{`Fx Rate unavailable for selected Reporting Currency`}</li>
      </ul>);
    }
    return (
      <small><ul className="bullet">
        <li>{`Reporting Currency (RC) corresponds to ${ccy}`}</li>
      </ul></small>
    );
  }


  renderGridData() {
    let grid = null;
    let dataLength = 0;
    if (this.state.gridData) {
      dataLength = this.state.gridData.length
    }
    let messageText = (
        <span>
          {"Found "+dataLength+" for "}
          <strong>
            {this.state.selectedDrillThroughView.value+" "}
          </strong>
        </span>
    );
    if (!this.state.gridData || dataLength <= 0) {
      messageText = (<center>{"No data available for this search criteria"}</center>);
    }
    let message = (
      <div className="message" style={{height: "30px"}}>
        {messageText}
        {(this.props.drillThroughData && this.props.drillThroughData.length > 0) ? (
          <button style={{ border:"none", background:"none"}} className="button--tertiary"  onClick={this.fetchDrillThroughData}>
          Go Back to previous view
          </button>
        ) : null}
      </div>
    );
    if (!this.state.gridData || dataLength <= 0) {
      return message;
    }
    var agreementTypeId = -1;
    if (
      this.state.drillDownFilters &&
      this.state.drillDownFilters.agreementTypeIds.length == 1
    )
      agreementTypeId = this.state.drillDownFilters.agreementTypeIds[0].key;

    let RC = this.state.gridData[0].reportingCurrency
    grid = (
      <React.Fragment>
      <Layout>
        <Layout.Child size="Fit">
          {message}
        </Layout.Child>
        <Layout.Child size={8}>
        <DetailGrid
          gridColumns={this.state.gridColumns()}
          gridId="detailReportGrid"
          gridOptions={gridOptions(
            this.state.selectedDrillThroughView.value,
            agreementTypeId
          )}
          onCellClick={this.onCellClickHandler}
          data={this.state.gridData}
          resizeCanvas={this.props.resizeCanvas}
          fill={true}
        />
        </Layout.Child>
        <Layout.Child size="Fit" childId="rcGridChild4">
            {this.reportingCurrencyMessage(RC)}
        </Layout.Child>
        <Layout.Child>
            {this.renderLinkPanel()}
        </Layout.Child>
        {(this.props.termDetailData && this.props.termDetailData.length > 0) ? (
          <Layout.Child size={2}>
          <TermGrid
            gridColumns={termDetailsGridColumns()}
            gridId="termDetailGrid"
            gridOptions={termDetailsGridOptions()}
            data={this.props.termDetailData}
            resizeCanvas={this.props.resizeCanvas}
            fill={true}
          />
          </Layout.Child>
        ) : null}
      </Layout>
      </React.Fragment>
    );
    return grid;
  }

  render() {
    return this.renderGridData();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCalculationData,
      fetchFinancingTermDetails,
      updateSelectedView,
      updateDrillDownSelectedFilters,
      updateDrillThroughData,
      removeDrillThroughData,
      updateDrillThroughPayload,
      removeDrillThroughPayload,
      updateGridData,
      updateDrillThroughSelectedFilters,
      updateDrillThroughSelectedView
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    gridData: state.detailReport.gridData,
    selectedView: state.detailReport.selectedView,
    drillDownFilters: state.detailReport.drillDownFilters,
    termDetailData: state.detailReport.financingTermData,
    drillThroughData: state.detailReport.drillThroughData,
    drillThroughPayload: state.detailReport.drillThroughPayload,
    resizeCanvas: state.detailReport.resizeCanvas,
    drillThroughFilter: state.detailReport.drillThroughFilter,
    selectedDrillThroughView: state.detailReport.selectedDrillThroughView
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommonGrid);
