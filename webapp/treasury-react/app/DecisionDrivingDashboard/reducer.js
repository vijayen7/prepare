import {
  FETCH_LEGAL_ENTITY_FAMILY_TO_LEGAL_ENTITY_DATA,
  FETCH_CPE_FAMILY_TO_CPE_DATA
} from 'DecisionDrivingDashboard/constants';
import { combineReducers } from 'redux';
import SeclendReducer from './Seclend/reducer';
import FinancingReducer from './Financing/reducer';
import MarginReducer from './Margin/reducer';

function legalEntityFamilyToLegalEntityDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_LEGAL_ENTITY_FAMILY_TO_LEGAL_ENTITY_DATA}_SUCCESS`:
      return action.data || {};
    default:
      return state;
  }
}

function cpeFamilyToCpeDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_CPE_FAMILY_TO_CPE_DATA}_SUCCESS`:
      return action.data || {};
    case `${FETCH_CPE_FAMILY_TO_CPE_DATA}_SUCCESS`:
      return action.data || {};
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  seclendView: SeclendReducer,
  financingView: FinancingReducer,
  marginView: MarginReducer,
  legalEntityFamilyToLegalEntityData: legalEntityFamilyToLegalEntityDataReducer,
  cpeFamilyToCpeData: cpeFamilyToCpeDataReducer
});

export default rootReducer;
