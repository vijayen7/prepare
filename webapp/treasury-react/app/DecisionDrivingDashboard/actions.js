import {
  FETCH_LEGAL_ENTITY_FAMILY_TO_LEGAL_ENTITY_DATA,
  FETCH_CPE_FAMILY_TO_CPE_DATA
} from 'DecisionDrivingDashboard/constants';

export function fetchLegalEntityFamilyToLegalEntityData(payload) {
  return {
    type: FETCH_LEGAL_ENTITY_FAMILY_TO_LEGAL_ENTITY_DATA,
    payload
  };
}

export function fetchCpeFamilyToCpeData(payload) {
  return {
    type: FETCH_CPE_FAMILY_TO_CPE_DATA,
    payload
  };
}


