import { BASE_URL } from 'commons/constants';

export function getLegalEntityFamilyToLegalEntityData(payload) {
  const url = `${BASE_URL}service/organizationDataService/getLegalEntityFamilyIdToLegalEntityNames?inputFormat=PROPERTIES&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getCpeFamilyToCpeData(payload) {
  const url = `${BASE_URL}service/organizationDataService/getCpeFamilyIdToCpeNames?inputFormat=PROPERTIES&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}
