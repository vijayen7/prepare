import { url } from "../../api";
export function gridOptions(columns) {
  const options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [{ columnId: 'slfFeeChange', sortAsc: false }],
    page: true,
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: 'Total',
    frozenColumn: 9,
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    },
    groupColumns: getGroupColumns(columns)
  };
  return options;
}

function getGroupColumns(columns) {
  const groupColumns = [];
  const availabilityRateColumns = [];

  const borrowRateColumns = [];

  for (let i = 1; i < columns.length; i++) {
    if (columns[i].rateType != undefined && columns[i].rateType.toLowerCase() === 'availability rate') {
      availabilityRateColumns.push(columns[i].id);
    } else if (columns[i].rateType != undefined && columns[i].rateType.toLowerCase() === 'borrow rate') {
      borrowRateColumns.push(columns[i].id);
    }
  }
  if (undefined != availabilityRateColumns && availabilityRateColumns.length > 0) {
    groupColumns.push(['Availability Rate', availabilityRateColumns[0], availabilityRateColumns[availabilityRateColumns.length - 1]]);
  }
  if (undefined != borrowRateColumns && borrowRateColumns.length > 0) { groupColumns.push(['Borrow Rate', borrowRateColumns[0], borrowRateColumns[borrowRateColumns.length - 1]]); }
  return groupColumns;
}

