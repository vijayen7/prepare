import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class CustomizedAxisTick extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { x, y, payload } = this.props;
    return (
      <g transform={`translate(${x},${y}) rotate(-35)`}>
        <text x={0} y={0} dy={16} textAnchor="end" fill="#666" >
          <tspan x="0" dy="1.2em">{payload.value}</tspan>
        </text>
      </g>
    );
  }
}

CustomizedAxisTick.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  payload: PropTypes.array.isRequired
};
