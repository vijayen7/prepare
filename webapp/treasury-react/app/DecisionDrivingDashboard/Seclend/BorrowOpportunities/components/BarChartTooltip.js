import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumberAsMoneyWithoutPrefix } from 'commons/util';

export default class BarChartTooltip extends Component {
  constructor(props) {
    super(props);
  }

  getViewBoxStyle() {
    return {
      background: '#EBF1F5',
      borderColor: '#394B59',
      padding: '10px',
      textAlign: 'left'
    };
  }

  render() {
    const { active } = this.props;
    if (active) {
      const { payload, label } = this.props;
      return (
        <div className="custom-tooltip" style={this.getViewBoxStyle()}>
          <p className="label">{label}</p>
          <p style={{ color: '#87A629' }}>
            {'Savings : '}
            {formatNumberAsMoneyWithoutPrefix(payload[0].value)}
          </p>

          <p style={{ color: payload[1].stroke }}>
            {'Borrow Rate : '}
            {`${parseFloat(payload[1].value).toFixed(2)}%`}
          </p>

          <p style={{ color: payload[2].stroke }}>
            {'Best Borrow Rate : '}
            {`${parseFloat(payload[2].value).toFixed(2)}%`}
          </p>
        </div>
      );
    }

    return null;
  }
}

BarChartTooltip.propTypes = {
  active: PropTypes.bool,
  payload: PropTypes.array,
  label: PropTypes.string
};
