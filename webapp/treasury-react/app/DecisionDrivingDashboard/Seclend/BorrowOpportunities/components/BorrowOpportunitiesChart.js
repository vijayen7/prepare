import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumberAsMoneyWithoutPrefix } from 'commons/util';
import BiaxialCompositeChart from 'commons/components/BiaxialCompositeChart';
import BarChartTooltip from './BarChartTooltip';
import CustomizedAxisTick from './CustomizedAxisTick';

export default class BorrowOpportunitiesChart extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const isReportingFxNull = this.props.checkReportingFxNullOrNot(this.props.baseData);
    const yLabel = isReportingFxNull ? '(in USD)' : '(in RC)';
    const barDataKey = isReportingFxNull ? 'savings' : 'savingsRC';
    return (
      <React.Fragment>
        <BiaxialCompositeChart
          data={this.props.getPlotData()}
          bars={[{ yAxisId: 'left', dataKey: barDataKey, size: 35, name: 'Savings' }]}
          lines={
            [
              { yAxisId: 'right', dataKey: 'SLFRate', name: 'Borrow Rate' },
              { yAxisId: 'right', dataKey: 'bestSlfRate', name: 'Best Borrow Rate' }
            ]
          }
          xAxis={{ tick: <CustomizedAxisTick />, label: 'Ticker' }}
          yAxis={{ left: { label: `Savings ${yLabel}`, tickFormatter: formatNumberAsMoneyWithoutPrefix },
            right: { label: 'Borrow and Best Borrow Rate (in %)', unit: '%' } }}
          legend={{ wrapperStyle: { paddingTop: 80 } }}
          tooltip={{ formatter: <BarChartTooltip /> }}
        />
      </React.Fragment>
    );
  }
}

BorrowOpportunitiesChart.propTypes = {
  baseData: PropTypes.array,
  getPlotData: PropTypes.func,
  checkReportingFxNullOrNot: PropTypes.func
};
