import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { URL } from 'commons/constants';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import SliderPanel from 'commons/components/SliderPanel';
import GridContainer from 'commons/components/GridWithCellClick';
import Message from 'commons/components/Message';
import { getUrlEncodedOrgNamesFromOrgFamilyIds, checkReportingFxNullOrNot, reportingCurrencyMessage } from 'DecisionDrivingDashboard/util';
import { gridColumns as borrowNegotiationGridColumns } from '../grid/columnConfig';
import { gridOptions as borrowNegotiationGridOptions } from '../grid/gridOptions';
import AvailabilityDataPanel from "../../../../AvailabilityDataPanel/components/AvailabilityDataPanel";
import BorrowOpportunitiesChart from '../components/BorrowOpportunitiesChart';
import { saveBorrowOpportunitiesContainerData, fetchSecurityMarketData, destroySecurityMarketData } from '../../actions';
import {
  loadNegotiationPopUp as loadNegotiationPopUpData,
} from "seclend/RateNegotiationDialog/actions"
import NegotiationDialog from "seclend/RateNegotiationDialog/RateNegotiationDialog"


class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.checkReportingFxNullOrNot = checkReportingFxNullOrNot.bind(this);
    this.reportingCurrencyMessage = reportingCurrencyMessage.bind(this);
    this.getPlotData = this.getPlotData.bind(this);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.borrowNegotiationDataComparator = this.borrowNegotiationDataComparator.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length ?
      this.props.containerData : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.borrowNegotiationData !== this.props.borrowNegotiationData) {
      this.setState({ value: Math.min(nextProps.borrowNegotiationData.length, 20) });
    }
  }

  componentWillUnmount() {
    this.props.saveBorrowOpportunitiesContainerData(this.state);
    this.props.destroySecurityMarketData();
  }

  handleSliderChange(value) {
    this.setState({ value });
  }

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  onCellClickHandler = args => {
    if (args.colId === "negotiation") {
        var brokerPositionDataId = args.item.brokerPositionDataId;
        args.item = args.item.seclendGadgetBaseData;
        args.item.view = "position";
        args.item.id = brokerPositionDataId;
        args.item.spn = args.item.pnlSpn;
        args.item.securityLocalName = args.item.ticker;
        args.item.borrowType = "BORROW";
        this.props.loadNegotiationPopUpData(args.item);
    }
  };

  getDefaultContainerData() {
    const length = this.props.borrowNegotiationData !== undefined ?
      Math.min(this.props.borrowNegotiationData.length, 20) : 0;
    return { view: 'grid', value: length };
  }

  /*
    Slices the data to Top N ( where N = value)
    */
  getGridData(data) {
    if (data && data instanceof Array) { return data.sort(this.borrowNegotiationDataComparator).slice(0, this.state.value); }
  }

  onDblClickHandler = args => {
    var payload = {
      spn: args.seclendGadgetBaseData.pnlSpn,
      cusip: "__null__",
      sedol: "__null__",
      date: (this.props.filters.selectedDate)
    };
    this.props.fetchSecurityMarketData(payload);
    this.setState({ selectedCpeIdForDrillDown: args.seclendGadgetBaseData.cpeId });
  };

  getPlotData() {
    const plotData = [];
    const data = this.props.borrowNegotiationData;
    const sortedBySavingsDesc = data.sort(this.borrowNegotiationDataComparator);
    for (let i = 0; i < Math.min(data.length, this.state.value); i++) {
      const { savings, savingsRC, SLFRate, bestSlfRate } = data[i];
      plotData.push({
        name: data[i].seclendGadgetBaseData.ticker,
        savings,
        savingsRC,
        SLFRate,
        bestSlfRate
      });
    }
    return plotData;
  }

  borrowNegotiationDataComparator(a,b){
    return Math.abs(b.savingsRC) - Math.abs(a.savingsRC);
  }

  renderSliderPanel() {
    return (<SliderPanel
      min={1}
      max={this.props.borrowNegotiationData.length}
      value={this.state.value}
      view={this.state.view}
      onSliderChange={this.handleSliderChange}
      toggleOptions={[
        { key: 'grid', value: 'Grid' },
        { key: 'chart', value: 'Chart' }
      ]}
      onToggleGroupChange={this.handleToggleGroupChange}
    />);
  }

  renderAvailabilityDataPanel() {
    let availabilityDataPanel = this.props.securityMarketData.length > 0 && (
      <div style={{ height: "280px" }}>
        <AvailabilityDataPanel
          data={this.props.securityMarketData}
          selectedCpeIdForDrillDown={this.state.selectedCpeIdForDrillDown}
        />
      </div>
    );
    return availabilityDataPanel;
  }

  renderLinkPanel() {
    const date = this.props.filters.selectedDate;
    const lef = this.props.filters.selectedLegalEntityFamilies;
    const cpef = this.props.filters.selectedCpeFamilies;
    const lefNames = getUrlEncodedOrgNamesFromOrgFamilyIds(lef, this.props.legalEntityFamilyToLegalEntityData);
    const cpefNames = getUrlEncodedOrgNamesFromOrgFamilyIds(cpef, this.props.cpeFamilyToCpeData);
    return (
      <LinkPanel
        links={[
          <Link
            id="borrow-report"
            text="View all borrow optimization opportunities"
            href={`${URL}/ivy/searchSecLendBestBorrowSummary.do?viewName=default&descoEntityNames=${lefNames}&counterpartyEntitiesNames=${cpefNames}&asOfDate=${date}&bookType=0`}
          />,
          <Link
            id="impact-summary"
            text="View YTD savings"
            href={`${URL}/treasury/seclend/impactSummary`}
          />
        ]}
      />
    );
  }

  renderGrid() {
    return (<GridContainer
      data={this.getGridData(this.props.borrowNegotiationData)}
      gridId="BorrowNegotiationData"
      gridColumns={borrowNegotiationGridColumns(this.props.borrowNegotiationData)}
      gridOptions={borrowNegotiationGridOptions(this.props.borrowNegotiationData)}
      resizeCanvas={this.props.resizeCanvas}
      onDblClick={this.onDblClickHandler}
      heightValue = {520}
      onCellClick={this.onCellClickHandler}
    />);
  }

  renderChart() {
    return (
      <BorrowOpportunitiesChart getPlotData={this.getPlotData} baseData={this.props.borrowNegotiationData} checkReportingFxNullOrNot={this.checkReportingFxNullOrNot}/>
    );
  }

  renderMainContainer() {
    let data = this.props.borrowNegotiationData;
    if (data.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        <NegotiationDialog/>
        {this.renderSliderPanel()}
        {(this.state.view === 'grid') ? this.renderGrid() : this.renderChart()}
        {(this.reportingCurrencyMessage(data))}
        {this.renderLinkPanel()}
        <hr />
        {this.renderAvailabilityDataPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

MainContainer.defaultProps = {
  containerData: { view: 'grid', value: 5 },
  borrowNegotiationData: []
};

MainContainer.propTypes = {
  containerData: PropTypes.object,
  borrowNegotiationData: PropTypes.array,
  saveBorrowOpportunitiesContainerData: PropTypes.func,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    containerData: state.decisionDrivingDashboard.seclendView.borrowOpportunitiesContainerData,
    borrowNegotiationData: state.decisionDrivingDashboard.seclendView.borrowNegotiationData,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    filters: state.decisionDrivingDashboard.seclendView.borrowOpportunitiesFilters,
    securityMarketData: state.decisionDrivingDashboard.seclendView.securityMarketData,
    resizeCanvas: state.decisionDrivingDashboard.seclendView.resizeCanvas
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveBorrowOpportunitiesContainerData,
      fetchSecurityMarketData,
      destroySecurityMarketData,
      loadNegotiationPopUpData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(MainContainer);
