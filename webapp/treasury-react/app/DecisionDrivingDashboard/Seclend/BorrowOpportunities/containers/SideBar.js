import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {Card} from 'arc-react-components';
import DateFilter from 'commons/container/DateFilter';
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';
import InputFilter from 'commons/components/InputFilter';
import LegalEntityFamilyFilter from 'commons/container/LegalEntityFamilyFilter';
import CpeFamilyFilter from 'commons/container/CpeFamilyFilter';
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import { getCommaSeparatedValuesOrEmpty, getCommaSeparatedValuesOrEmptyForSingleSelect } from 'commons/util';
import { getCodexFilters } from 'DecisionDrivingDashboard/util';

import {
  fetchBorrowNegotiationData,
  destroyBorrowNegotiationData,
  saveBorrowOpportunitiesFilters,
  resizeCanvas
} from '../../actions';


class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
  }

  componentWillMount() {
    const state = this.props.filters;
    if (Object.keys(state).length === 0) {
      this.setState(this.getDefaultFilters());
      this.props.saveBorrowOpportunitiesFilters(this.getDefaultFilters());
    }
    this.setState(state);
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedDate: getCodexFilters('DATE'),
      potentialSavingsThreshold: getCodexFilters('POTENTIAL_SAVINGS_THRESHOLD'),
      selectedLegalEntityFamilies: getCodexFilters('LEGAL_ENTITY_FAMILIES'),
      selectedCpeFamilies: getCodexFilters('CPE_FAMILIES'),
      selectedReportingCurrency: getCodexFilters('REPORTING_CURRENCY_ID')
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedDate === '' && this.state.selectedDate !== '') {
      this.handleClick();
    }
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    this.props.destroyBorrowNegotiationData();
    const payload = {
      date: this.state.selectedDate,
      potentialSavingsThreshold: this.state.potentialSavingsThreshold || 0,
      legalEntityFamilyIds: getCommaSeparatedValuesOrEmpty(
        this.state.selectedLegalEntityFamilies
      ),
      cpeFamilyIds: getCommaSeparatedValuesOrEmpty(
        this.state.selectedCpeFamilies
      ),
      reportingCurrencyId: getCommaSeparatedValuesOrEmptyForSingleSelect(
        this.state.selectedReportingCurrency
      )
    };
    this.props.saveBorrowOpportunitiesFilters(this.state);
    this.props.fetchBorrowNegotiationData(payload);
  }

  render() {
    return (
      <Sidebar
        collapsible
        onToggle={this.props.onToggle}
        enterFunction={this.handleClick}
        id="borrowOpportunitiesSideBar"
        resizeCanvas={this.props.resizeCanvas}>
        <Card>
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedDate"
            data={this.state.selectedDate}
          />
        </Card>
        <Card>
          <CpeFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCpeFamilies}
          />
          <LegalEntityFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntityFamilies}
          />
        </Card>
        <Card>
          <InputFilter
            data={this.state.potentialSavingsThreshold}
            label="Potential Savings Threshold ($)"
            onSelect={this.onSelect}
            stateKey="potentialSavingsThreshold"
          />
        </Card>
        <Card>
          <ReportingCurrencyFilter
            label="Reporting Currency"
            onSelect={this.onSelect}
            selectedData={this.state.selectedReportingCurrency}
            multiSelect={false}
            stateKey="selectedReportingCurrency"
          />
        </Card>
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.defaultProps = {
  filters: {}
};

SideBar.propTypes = {
  fetchBorrowNegotiationData: PropTypes.func,
  destroyBorrowNegotiationData: PropTypes.func,
  saveBorrowOpportunitiesFilters: PropTypes.func,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.func
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.seclendView.borrowOpportunitiesFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchBorrowNegotiationData,
      destroyBorrowNegotiationData,
      saveBorrowOpportunitiesFilters,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
