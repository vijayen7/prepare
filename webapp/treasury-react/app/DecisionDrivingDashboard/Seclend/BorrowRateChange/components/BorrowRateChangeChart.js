import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumberAsMoneyWithoutPrefix } from 'commons/util';
import BiaxialCompositeChart from 'commons/components/BiaxialCompositeChart';
import BarChartTooltip from './BarChartTooltip';
import CustomizedAxisTick from './CustomizedAxisTick';

export default class BorrowRateChangeChart extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const isReportingFxNull = this.props.checkReportingFxNullOrNot(this.props.baseData);
    const yLabel = isReportingFxNull ? '(in USD)' : '(in RC)';
    const barDataKey = isReportingFxNull ? 'slfFeeChange' : 'slfFeeChangeRC';
    return (
      <React.Fragment>
        <BiaxialCompositeChart
          data={this.props.getPlotData()}
          bars={[{ yAxisId: 'left', dataKey: barDataKey, size: 35, name: 'Borrow Rate Change' }]}
          lines={
            [
              { yAxisId: 'right', dataKey: 'currentSLFRate', name: 'Current Borrow Rate %' },
              { yAxisId: 'right', dataKey: 'previousSLFRate', name: 'Previous Borrow Rate %' }
            ]
          }
          xAxis={{ tick: <CustomizedAxisTick />, label: 'Ticker' }}
          yAxis={{ left: { label: `Borrow Rate Change Amount ${yLabel}`, tickFormatter: formatNumberAsMoneyWithoutPrefix },
            right: { label: 'Previous and Current Borrow Rate (in %)', unit: '%' } }}
          legend={{ wrapperStyle: { paddingTop: 80 } }}
          tooltip={{ formatter: <BarChartTooltip /> }}
        />
      </React.Fragment>
    );
  }
}

BorrowRateChangeChart.propTypes = {
  baseData: PropTypes.array,
  getPlotData: PropTypes.func,
  checkReportingFxNullOrNot: PropTypes.func
};
