import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { URL } from 'commons/constants';
import SliderPanel from 'commons/components/SliderPanel';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import GridContainer from 'commons/components/Grid';
import Message from 'commons/components/Message';
import { getUrlEncodedOrgNamesFromOrgFamilyIds, checkReportingFxNullOrNot, reportingCurrencyMessage } from 'DecisionDrivingDashboard/util';
import { gridColumns as borrowRateChangeGridColumns } from '../grid/columnConfig';
import { gridOptions as borrowRateChangeGridOptions } from '../grid/gridOptions';
import BorrowRateChangeChart from '../components/BorrowRateChangeChart';
import { saveBorrowRateChangeContainerData } from '../../actions';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.checkReportingFxNullOrNot = checkReportingFxNullOrNot.bind(this);
    this.reportingCurrencyMessage = reportingCurrencyMessage.bind(this);
    this.getPlotData = this.getPlotData.bind(this);
    this.borrowRateComparator = this.borrowRateComparator.bind(this);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length ?
      this.props.containerData : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.borrowRateChangeData !== this.props.borrowRateChangeData) {
      this.setState({ value: Math.min(nextProps.borrowRateChangeData.length, 20) });
    }
  }

  componentWillUnmount() {
    this.props.saveBorrowRateChangeContainerData(this.state);
  }

  handleSliderChange(value) {
    this.setState({ value });
  }

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  getDefaultContainerData() {
    const length = this.props.borrowRateChangeData !== undefined ?
      Math.min(this.props.borrowRateChangeData.length, 20) : 0;
    return { view: 'grid', value: length };
  }

  /*
    Slices the data to Top N ( where N = value)
    */
  getGridData(data) {
    if (data && data instanceof Array) { return data.sort(this.borrowRateComparator).slice(0, this.state.value); }
  }

  getPlotData() {
    const plotData = [];
    const data = this.props.borrowRateChangeData;

    const sortedBySlfFeeChangeDesc = data.sort(this.borrowRateComparator);
    for (let i = 0; i < Math.min(data.length, this.state.value); i++) {
      const { slfFeeChange, slfFeeChangeRC, currentSLFRate, previousSLFRate } = data[i];
      plotData.push({
        name: data[i].seclendGadgetBaseData.ticker,
        slfFeeChange,
        slfFeeChangeRC,
        currentSLFRate,
        previousSLFRate
      });
    }
    return plotData;
  }

  borrowRateComparator(a, b) {
    return Math.abs(b.slfFeeChange) - Math.abs(a.slfFeeChange);
  }

  renderSliderPanel() {
    return (<SliderPanel
      min={1}
      max={this.props.borrowRateChangeData.length}
      value={this.state.value}
      view={this.state.view}
      onSliderChange={this.handleSliderChange}
      toggleOptions={[
        { key: 'grid', value: 'Grid' },
        { key: 'chart', value: 'Chart' }
      ]}
      onToggleGroupChange={this.handleToggleGroupChange}
    />);
  }

  renderLinkPanel() {
    const date = this.props.filters.selectedDate;
    const lef = this.props.filters.selectedLegalEntityFamilies;
    const cpef = this.props.filters.selectedCpeFamilies;
    const lefNames = getUrlEncodedOrgNamesFromOrgFamilyIds(lef, this.props.legalEntityFamilyToLegalEntityData);
    const cpefNames = getUrlEncodedOrgNamesFromOrgFamilyIds(cpef, this.props.cpeFamilyToCpeData);
    return (
      <LinkPanel
        links={[
          <Link
            id="borrow-rate-change-report"
            text="View all borrow rates"
            href={`${URL}/ivy/searchSecLendMarketData.do?viewName=default&descoEntityNames=${lefNames}&counterpartyEntitiesNames=${cpefNames}&ccyCodes=&startDate=${date}&endDate=${date}`}
          />
        ]}
      />
    );
  }

  renderGrid() {
    return (<GridContainer
      data={this.getGridData(this.props.borrowRateChangeData)}
      gridId="BorrowRateChangeData"
      gridColumns={borrowRateChangeGridColumns()}
      gridOptions={borrowRateChangeGridOptions()}
      resizeCanvas={this.props.resizeCanvas}
    />);
  }

  renderChart() {
    return (
        <BorrowRateChangeChart getPlotData={this.getPlotData} baseData={this.props.borrowRateChangeData} checkReportingFxNullOrNot={this.checkReportingFxNullOrNot}/>);
  }


  renderMainContainer() {
    let data = this.props.borrowRateChangeData;
    if (data.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderSliderPanel()}
        {(this.state.view === 'grid') ? this.renderGrid() : this.renderChart()}
        {this.reportingCurrencyMessage(data)}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

MainContainer.defaultProps = {
  containerData: { view: 'grid', value: 5 },
  borrowRateChangeData: []
};

MainContainer.propTypes = {
  containerData: PropTypes.object,
  borrowRateChangeData: PropTypes.array,
  saveBorrowRateChangeContainerData: PropTypes.func,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    containerData: state.decisionDrivingDashboard.seclendView.borrowRateChangeContainerData,
    borrowRateChangeData: state.decisionDrivingDashboard.seclendView.borrowRateChangeData,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    filters: state.decisionDrivingDashboard.seclendView.borrowRateChangeFilters,
    resizeCanvas: state.decisionDrivingDashboard.seclendView.resizeCanvas
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveBorrowRateChangeContainerData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(MainContainer);
