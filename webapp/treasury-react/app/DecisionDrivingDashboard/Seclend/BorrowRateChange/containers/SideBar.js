import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Card } from 'arc-react-components';
import DateFilter from 'commons/container/DateFilter';
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';
import InputFilter from 'commons/components/InputFilter';
import LegalEntityFamilyFilter from 'commons/container/LegalEntityFamilyFilter';
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import CpeFamilyFilter from 'commons/container/CpeFamilyFilter';
import { getCommaSeparatedValuesOrEmpty, getCommaSeparatedValuesOrEmptyForSingleSelect } from 'commons/util';
import { getCodexFilters } from 'DecisionDrivingDashboard/util';
import {
  fetchBorrowRateChangeData,
  destroyBorrowRateChangeData,
  saveBorrowRateChangeFilters,
  resizeCanvas
} from '../../actions';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  componentWillMount() {
    const state = this.props.filters;
    if (Object.keys(state).length === 0) {
      this.setState(this.getDefaultFilters());
      this.props.saveBorrowRateChangeFilters(this.getDefaultFilters());
    }
    this.setState(state);
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedDate: getCodexFilters('DATE'),
      stockLoanFeeSpikeThreshold: getCodexFilters('STOCK_LOAN_FEE_SPIKE_THRESHOLD'),
      selectedLegalEntityFamilies: getCodexFilters('LEGAL_ENTITY_FAMILIES'),
      selectedCpeFamilies: getCodexFilters('CPE_FAMILIES'),
      selectedReportingCurrency: getCodexFilters('REPORTING_CURRENCY_ID')
    };
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    this.props.destroyBorrowRateChangeData();

    const payload = {
      date: this.state.selectedDate,
      stockLoanFeeSpikeThreshold: this.state.stockLoanFeeSpikeThreshold || 0,
      legalEntityFamilyIds: getCommaSeparatedValuesOrEmpty(
        this.state.selectedLegalEntityFamilies
      ),
      cpeFamilyIds: getCommaSeparatedValuesOrEmpty(
        this.state.selectedCpeFamilies
      ),
      reportingCurrencyId: getCommaSeparatedValuesOrEmptyForSingleSelect(
        this.state.selectedReportingCurrency
      )
    };
    this.props.saveBorrowRateChangeFilters(this.state);
    this.props.fetchBorrowRateChangeData(payload);
  }

  render() {
    return (
      <Sidebar
        collapsible
        onToggle={this.props.onToggle}
        enterFunction={this.handleClick}
        id="borrowRateChangeSideBar"
        resizeCanvas={this.props.resizeCanvas}>
        <Card>
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedDate"
            data={this.state.selectedDate}
          />
        </Card>
        <Card>
          <CpeFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCpeFamilies}
          />
          <LegalEntityFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntityFamilies}
          />
        </Card>
        <Card>
          <InputFilter
            data={this.state.stockLoanFeeSpikeThreshold}
            label="Stock Loan Fee Change Threshold ($)"
            onSelect={this.onSelect}
            stateKey="stockLoanFeeSpikeThreshold"
            isStandard
          />
        </Card>
        <Card>
          <ReportingCurrencyFilter
            label="Reporting Currency"
            onSelect={this.onSelect}
            selectedData={this.state.selectedReportingCurrency}
            multiSelect={false}
            stateKey="selectedReportingCurrency"
          />
        </Card>
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.defaultProps = {
  filters: {}
};

SideBar.propTypes = {
  fetchBorrowRateChangeData: PropTypes.func,
  destroyBorrowRateChangeData: PropTypes.func,
  saveBorrowRateChangeFilters: PropTypes.func,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.func
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.seclendView.borrowRateChangeFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchBorrowRateChangeData,
      destroyBorrowRateChangeData,
      saveBorrowRateChangeFilters,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
