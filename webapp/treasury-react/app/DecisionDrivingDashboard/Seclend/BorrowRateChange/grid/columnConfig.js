import {
  percentFormatter,
  drillThroughFormatter,
  numberTotalsFormatter,
  percentTotalsFormatter,
  excelTotalsFormatter,
  marketDataPositionTypeFormatter
} from 'commons/grid/formatters';

import {
  textComparator,
  numberComparator
} from 'commons/grid/comparators';

const columns = [];

export function gridColumns(params) {
  const columns = [{
    id: 'spn',
    name: 'PNL SPN',
    field: 'seclendGadgetBaseData',
    toolTip: 'PNL SPN',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 65,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.pnlSpn;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.pnlSpn;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return numberComparator(a.seclendGadgetBaseData.pnlSpn, b.seclendGadgetBaseData.pnlSpn); }
      return numberComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'securityDescription',
    name: 'Security Description',
    field: 'seclendGadgetBaseData',
    toolTip: 'Security Description',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 120,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.securityDescription;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.securityDescription;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.securityDescription, b.seclendGadgetBaseData.securityDescription); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'gboTypeName',
    name: 'GBO Type',
    field: 'seclendGadgetBaseData',
    toolTip: 'GBO TypeName',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 110,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.gboTypeName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.gboTypeName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.gboTypeName, b.seclendGadgetBaseData.gboTypeName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'sedol',
    name: 'Sedol',
    field: 'seclendGadgetBaseData',
    toolTip: 'sedol',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.sedol;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.sedol;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.sedol, b.seclendGadgetBaseData.sedol); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'ticker',
    name: 'Ticker',
    field: 'seclendGadgetBaseData',
    toolTip: 'Ticker',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.ticker;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.ticker;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.ticker, b.seclendGadgetBaseData.ticker); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'positionType',
    name: 'Position Type',
    field: 'seclendGadgetBaseData',
    toolTip: 'Position Type',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 90,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.positionType;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.positionType;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.positionType, b.seclendGadgetBaseData.positionType); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'bookName',
    name: 'Book',
    field: 'seclendGadgetBaseData',
    toolTip: 'Book',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 90,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.bookName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.bookName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.bookName, b.seclendGadgetBaseData.bookName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'counterpartyEntity',
    name: 'Counterparty Entity',
    field: 'seclendGadgetBaseData',
    toolTip: 'Counterparty Entity',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 100,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.cpeName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.cpeName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.cpeName, b.seclendGadgetBaseData.cpeName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'legalEntity',
    name: 'Legal Entity',
    field: 'seclendGadgetBaseData',
    toolTip: 'Legal Entity',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.legalEntityName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.legalEntityName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.legalEntityName, b.seclendGadgetBaseData.legalEntityName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'currencyCode',
    name: 'Currency',
    field: 'seclendGadgetBaseData',
    toolTip: 'Currency',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.currencyCode;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.currencyCode;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.currencyCode, b.seclendGadgetBaseData.currencyCode); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'custodianAccount',
    name: 'Custodian Account',
    field: 'seclendGadgetBaseData',
    toolTip: 'Custodian Account',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 110,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.custodianAccountName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.custodianAccountName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.custodianAccountName, b.seclendGadgetBaseData.custodianAccountName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'currentQuantity',
    name: 'Current Quantity',
    field: 'currentQuantity',
    toolTip: 'Current Quantity',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80
  },
  {
    id: 'previousQuantity',
    name: 'Previous Quantity',
    field: 'previousQuantity',
    toolTip: 'Previous Quantity',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80
  },
  {
    id: 'currentPrice',
    name: 'Current Price (USD)',
    field: 'currentPrice',
    toolTip: 'Current Price (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 50
  },
  {
    id: 'currentPriceRC',
    name: 'Current Price (RC)',
    field: 'currentPriceRC',
    toolTip: 'Current Price (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 50
  },
  {
    id: 'previousPrice',
    name: 'Previous Price (USD)',
    field: 'previousPrice',
    toolTip: 'Previous Price (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 50
  },
  {
    id: 'previousPriceRC',
    name: 'Previous Price (RC)',
    field: 'previousPriceRC',
    toolTip: 'Previous Price (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 50
  },
  {
    id: 'currentMV',
    name: 'Current MV (USD)',
    field: 'currentMV',
    toolTip: 'Current MV (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 90
  },
  {
    id: 'currentMVRC',
    name: 'Current MV (RC)',
    field: 'currentMVRC',
    toolTip: 'Current MV (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 90
  },
  {
    id: 'previousMV',
    name: 'Previous MV (USD)',
    field: 'previousMV',
    toolTip: 'Previous MV (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 90
  },
  {
    id: 'previousMVRC',
    name: 'Previous MV (RC)',
    field: 'previousMVRC',
    toolTip: 'Previous MV (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 90
  },
  {
    id: 'currentSLFRate',
    name: 'Current SLF Rate',
    field: 'currentSLFRate',
    toolTip: 'Current  SLF Rate',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 50
  },
  {
    id: 'previousSLFRate',
    name: 'Previous SLF Rate',
    field: 'previousSLFRate',
    toolTip: 'Previous  SLF Rate',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 50
  },
  {
    id: 'slfFeeChange',
    name: 'SLF Fee change',
    field: 'slfFeeChange',
    toolTip: 'SLF Fee change',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 99
  },
  {
    id: 'slfFeeChangeRC',
    name: 'SLF Fee change RC',
    field: 'slfFeeChangeRC',
    toolTip: 'SLF Fee change',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 99
  }
  ];

  return columns;
}
