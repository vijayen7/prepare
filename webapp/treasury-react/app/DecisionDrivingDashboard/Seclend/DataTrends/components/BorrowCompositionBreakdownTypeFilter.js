import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getComboboxFilter } from 'commons/components/ComboboxFilter'

export default class BorrowCompositionBreakdownTypeFilter extends Component {
  getData() {
    return [
      { key: 'CPE_FAMILY', value: 'CPE Family Breakdown' },
      { key: 'BORROW_TYPE', value: 'Borrow Type Breakdown' }
    ];
  }

  render() {
    const ComboboxFilter = getComboboxFilter();
    return (
      <ComboboxFilter
        data={this.getData()}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        multiSelect={false}
        stateKey="selectedBorrowCompositionBreakdownType"
        label="Chart View Type"
      />
    );
  }
}

BorrowCompositionBreakdownTypeFilter.propTypes = {
  onSelect: PropTypes.func,
  selectedData: PropTypes.any
};
