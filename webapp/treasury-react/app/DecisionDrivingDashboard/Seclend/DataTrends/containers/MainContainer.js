import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { BASE_URL } from 'commons/constants';
import DataTrendsChart from 'commons/components/DataTrendsChart';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import Message from 'commons/components/Message';
import { getCommaSeparatedValues } from 'commons/util';
import { checkReportingFxNullOrNot, reportingCurrencyMessage } from 'DecisionDrivingDashboard/util';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.checkReportingFxNullOrNot = checkReportingFxNullOrNot.bind(this);
    this.reportingCurrencyMessage = reportingCurrencyMessage.bind(this);
    this.getPlotData = this.getPlotData.bind(this);
    this.renderMainContainer = this.renderMainContainer.bind(this);
  }

  getPlotData() {
    const plotData = [];
    const data = this.props.borrowCompositionTrendData;

    for (let i=0; i<data.length; i++) {
      let element = data[i], plotDataElement = {};

      plotDataElement.name = parseInt(element.date.match(/\((.*)\)/)[1]);
      for (const constituent in element.constituents) {
        plotDataElement[constituent] = element.constituents[constituent];
      }
      plotData.push(plotDataElement);
    }

    plotData.sort((a, b) => a.name - b.name);
    return plotData;
  }

  renderLinkPanel() {
    return (<LinkPanel
        links={[
        <Link id="borrow-rate-comparison" text="View Detailed Borrow Composition" href={`${BASE_URL}counterpartyRelationship/#/tab/borrowRateComparison`} />
        ]}
    />);
  }

  renderChart() {
    const yLabel = checkReportingFxNullOrNot(this.props.borrowCompositionTrendData) ? 'Borrow  (in USD)' : 'Borrow  (in RC)';
    return (
      <DataTrendsChart
        data={this.getPlotData()}
        xAxis={{ dataKey: 'name', label: 'Date' }}
        yAxis={{ left: { label: yLabel} }}
      />
    );
  }

  renderMainContainer() {
    const { borrowCompositionTrendData } = this.props;

    if (borrowCompositionTrendData === null) {
      return <Message className="margin--top" messageData="Search to load data." />;
    }

    if (borrowCompositionTrendData.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderChart()}
        {this.reportingCurrencyMessage(borrowCompositionTrendData)}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

function mapStateToProps(state) {
  return {
    borrowCompositionTrendData: state.decisionDrivingDashboard.seclendView.borrowCompositionTrendData,
    filters: state.decisionDrivingDashboard.seclendView.borrowCompositionTrendFilters
  };
}

MainContainer.propTypes = {
  borrowCompositionTrendData: PropTypes.array,
  filters: PropTypes.object
};

export default connect(
  mapStateToProps
)(MainContainer);
