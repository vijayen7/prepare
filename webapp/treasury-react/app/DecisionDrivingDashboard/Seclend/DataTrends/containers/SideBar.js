import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Card } from 'arc-react-components';
import DateFilter from 'commons/container/DateFilter';
import LegalEntityFamilyFilter from 'commons/container/LegalEntityFamilyFilter';
import CpeFamilyFilter from 'commons/container/CpeFamilyFilter';
import ClassificationFilter from "commons/container/ClassificationFilter";
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';
import { getCommaSeparatedValuesOrEmpty, getCommaSeparatedValuesOrEmptyForSingleSelect } from 'commons/util';
import { getCodexFilters } from 'DecisionDrivingDashboard/util';
import {
  fetchBorrowCompositionTrendData,
  destroyBorrowCompositionTrendData,
  saveBorrowCompositionTrendFilters,
  resizeCanvas
 } from '../../actions';
import BorrowCompositionBreakdownTypeFilter from '../components/BorrowCompositionBreakdownTypeFilter';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  componentWillMount() {
    const state = this.props.filters;
    if (Object.keys(state).length === 0) {
      this.setState(this.getDefaultFilters());
      this.props.saveBorrowCompositionTrendFilters(this.getDefaultFilters());
    }
    this.setState(state);
  }


  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    const CPE_FAMILY = { key: 'CPE_FAMILY', value: 'CPE Family Breakdown' };
    return {
      selectedStartDate: getCodexFilters('SECLEND_TREND_START_DATE'),
      selectedEndDate: getCodexFilters('TREND_END_DATE'),
      selectedCpeFamilies: getCodexFilters('CPE_FAMILIES'),
      selectedLegalEntityFamilies: getCodexFilters('LEGAL_ENTITY_FAMILIES'),
      selectedClassification: getCodexFilters('CLASSIFICATION'),
      selectedBorrowCompositionBreakdownType: CPE_FAMILY,
      selectedReportingCurrency: getCodexFilters('REPORTING_CURRENCY_ID')
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    this.props.destroyBorrowCompositionTrendData();
    const payload = {
      startDate: this.state.selectedStartDate,
      endDate: this.state.selectedEndDate,
      cpeFamilyIds: getCommaSeparatedValuesOrEmpty(this.state.selectedCpeFamilies),
      legalEntityFamilyIds: getCommaSeparatedValuesOrEmpty(this.state.selectedLegalEntityFamilies),
      borrowTypeIds: getCommaSeparatedValuesOrEmpty(this.state.selectedClassification),
      breakdownType: getCommaSeparatedValuesOrEmpty([this.state.selectedBorrowCompositionBreakdownType]),
      reportingCurrencyId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedReportingCurrency)
    };
    this.props.saveBorrowCompositionTrendFilters(this.state);
    this.props.fetchBorrowCompositionTrendData(payload);
  }

  render() {
    return (
      <Sidebar
        collapsible
        onToggle={this.props.onToggle}
        enterFunction={this.handleClick}
        id="dataTrendsSideBar"
        resizeCanvas={this.props.resizeCanvas}>
        <Card>
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedStartDate"
            data={this.state.selectedStartDate}
            label="Start Date"
          />
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedEndDate"
            data={this.state.selectedEndDate}
            label="End Date"
          />
        </Card>
        <Card>
          <CpeFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCpeFamilies}
          />
          <LegalEntityFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntityFamilies}
          />
        </Card>
        <Card>
          <ClassificationFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedClassification}
          />
          <BorrowCompositionBreakdownTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedBorrowCompositionBreakdownType}
          />
        </Card>
        <Card>
          <ReportingCurrencyFilter
            label="Reporting Currency"
            onSelect={this.onSelect}
            selectedData={this.state.selectedReportingCurrency}
            multiSelect={false}
            stateKey="selectedReportingCurrency"
          />
        </Card>
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.propTypes = {
  fetchBorrowCompositionTrendData: PropTypes.func,
  destroyBorrowCompositionTrendData: PropTypes.func,
  saveBorrowCompositionTrendFilters: PropTypes.func,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.seclendView.borrowCompositionTrendFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchBorrowCompositionTrendData,
      destroyBorrowCompositionTrendData,
      saveBorrowCompositionTrendFilters,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
