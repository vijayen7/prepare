import { BASE_URL, INPUT_FORMAT_JSON } from 'commons/constants';
const queryString = require("query-string");

export let url = '';
export function getBorrowRateChangeData(payload) {
  const { date, legalEntityFamilyIds, cpeFamilyIds, stockLoanFeeSpikeThreshold, reportingCurrencyId } = payload;
  const paramString = `date="${date}"&legalEntityFamilyIds=${legalEntityFamilyIds}&cpeFamilyIds=${cpeFamilyIds}&slfSpikeThreshold=${stockLoanFeeSpikeThreshold}&reportingCurrencyId=${reportingCurrencyId}`;
  url = `${BASE_URL}service/borrowRateChanges/getBorrowRateChanges?${paramString}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getLendOpportunityData(payload) {
  const { date, legalEntityFamilyIds, cpeFamilyIds, potentialIncomeThreshold, reportingCurrencyId } = payload;
  const paramString = `date="${date}"&legalEntityFamilyIds=${legalEntityFamilyIds}&cpeFamilyIds=${cpeFamilyIds}&potentialIncomeThreshold=${potentialIncomeThreshold}&reportingCurrencyId=${reportingCurrencyId}`;
  url = `${BASE_URL}service/lendOpportunities/getLendOpportunitiesData?${paramString}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}


export function getBorrowNegotiationData(payload) {
  const { date, legalEntityFamilyIds, cpeFamilyIds, potentialSavingsThreshold, reportingCurrencyId } = payload;
  const paramString = `date="${date}"&legalEntityFamilyIds=${legalEntityFamilyIds}&cpeFamilyIds=${cpeFamilyIds}&potentialSavingsThreshold=${potentialSavingsThreshold}&reportingCurrencyId=${reportingCurrencyId}`;
  url = `${BASE_URL}service/borrowNegotiationOpportunity/getBorrowNegotiationOpportunity?${paramString}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getBorrowCompositionTrendData(payload) {
  const { startDate, endDate, legalEntityFamilyIds, cpeFamilyIds, borrowTypeIds, breakdownType, reportingCurrencyId } = payload;
  const paramString = `startDate="${startDate}"&endDate="${endDate}"&legalEntityFamilyIds=${legalEntityFamilyIds}&cpeFamilyIds=${cpeFamilyIds}&borrowTypeIds=${borrowTypeIds}&breakdownType="${breakdownType}"&reportingCurrencyId=${reportingCurrencyId}`;
  url = `${BASE_URL}service/borrowCompositionTrendService/getBorrowCompositionTrendData?${paramString}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getSecurityMarketData(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/marketDataService/getMarketDataForSecurityPnlSpn?spns=${payload.spn}&cusip=__null__&sedol=__null__&date="${payload.date}"${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}
