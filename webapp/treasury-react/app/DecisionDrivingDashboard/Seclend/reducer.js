import {
  FETCH_BORROW_RATE_CHANGE_DATA,
  DESTROY_BORROW_RATE_CHANGE_DATA,
  FETCH_LEND_OPPORTUNITY_DATA,
  DESTROY_LEND_OPPORTUNITY_DATA,
  FETCH_BORROW_NEGOTIATION_DATA,
  DESTROY_BORROW_NEGOTIATION_DATA,
  SAVE_BORROW_OPPORTUNITIES_FILTERS,
  SAVE_BORROW_OPPORTUNITIES_CONTAINER_DATA,
  SAVE_BORROW_RATE_CHANGE_FILTERS,
  SAVE_BORROW_RATE_CHANGE_CONTAINER_DATA,
  SAVE_LEND_OPPORTUNITIES_FILTERS,
  SAVE_LEND_OPPORTUNITIES_CONTAINER_DATA,
  SAVE_BORROW_TREND_FILTERS,
  FETCH_BORROW_COMPOSITION_TREND_DATA,
  DESTROY_BORROW_COMPOSITION_TREND_DATA,
  RESIZE_CANVAS,
  FETCH_SECURITY_MARKET_DATA_PNL,
  DESTROY_DATA_FOR_SECURITY_MARKET_DATA
} from 'DecisionDrivingDashboard/constants';
import { combineReducers } from 'redux';

function borrowRateChangeDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_BORROW_RATE_CHANGE_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_BORROW_RATE_CHANGE_DATA:
      return [];
  }
  return state;
}

function borrowRateChangeFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_BORROW_RATE_CHANGE_FILTERS}`:
      return action.payload || {};
  }
  return state;
}

function borrowRateChangeContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_BORROW_RATE_CHANGE_CONTAINER_DATA}`:
      return action.payload || {};
  }
  return state;
}

function lendOpportunitiesDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LEND_OPPORTUNITY_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_LEND_OPPORTUNITY_DATA:
      return [];
  }
  return state;
}

function lendOpportunitiesFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_LEND_OPPORTUNITIES_FILTERS}`:
      return action.payload || {};
  }
  return state;
}

function lendOpportunitiesContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_LEND_OPPORTUNITIES_CONTAINER_DATA}`:
      return action.payload || {};
  }
  return state;
}

function borrowNegotiationDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_BORROW_NEGOTIATION_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_BORROW_NEGOTIATION_DATA:
      return [];
  }
  return state;
}

function borrowOpportunitiesFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_BORROW_OPPORTUNITIES_FILTERS}`:
      return action.payload || {};
    }
  return state;
}

function borrowTrendFiltersReducer(state = {}, action) {
  switch (action.type) {
    case SAVE_BORROW_TREND_FILTERS:
      return action.payload || {};
    default:
      return state;
  }
}

function borrowCompositionTrendDataReducer(state = null, action) {
  switch (action.type) {
    case `${FETCH_BORROW_COMPOSITION_TREND_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_BORROW_COMPOSITION_TREND_DATA:
      return null;
  }
  return state;
}

function borrowOpportunitiesContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_BORROW_OPPORTUNITIES_CONTAINER_DATA}`:
      return action.payload || {};
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state !== true;
    default:
      return false;
  }
}

function securityMarketDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SECURITY_MARKET_DATA_PNL}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_FOR_SECURITY_MARKET_DATA:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  borrowRateChangeData: borrowRateChangeDataReducer,
  borrowRateChangeFilters: borrowRateChangeFiltersReducer,
  borrowRateChangeContainerData: borrowRateChangeContainerDataReducer,
  lendOpportunitiesData: lendOpportunitiesDataReducer,
  lendOpportunitiesFilters: lendOpportunitiesFiltersReducer,
  lendOpportunitiesContainerData: lendOpportunitiesContainerDataReducer,
  borrowNegotiationData: borrowNegotiationDataReducer,
  borrowOpportunitiesFilters: borrowOpportunitiesFiltersReducer,
  borrowOpportunitiesContainerData: borrowOpportunitiesContainerDataReducer,
  borrowCompositionTrendFilters: borrowTrendFiltersReducer,
  borrowCompositionTrendData: borrowCompositionTrendDataReducer,
  securityMarketData: securityMarketDataReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;

