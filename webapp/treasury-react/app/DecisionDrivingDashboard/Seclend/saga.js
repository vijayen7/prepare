import { put, takeEvery, call, all } from 'redux-saga/effects';
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from 'commons/constants';

import {
  FETCH_BORROW_RATE_CHANGE_DATA,
  FETCH_LEND_OPPORTUNITY_DATA,
  FETCH_BORROW_NEGOTIATION_DATA,
  FETCH_BORROW_COMPOSITION_TREND_DATA,
  FETCH_SECURITY_MARKET_DATA_PNL
} from 'DecisionDrivingDashboard/constants';

import {
  getBorrowRateChangeData,
  getLendOpportunityData,
  getBorrowNegotiationData,
  getBorrowCompositionTrendData,
  getSecurityMarketData
} from './api';

function* fetchBorrowRateChangeData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getBorrowRateChangeData, action.payload);
    yield put({ type: `${FETCH_BORROW_RATE_CHANGE_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_BORROW_RATE_CHANGE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}
function* fetchLendOpportunityData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLendOpportunityData, action.payload);
    yield put({ type: `${FETCH_LEND_OPPORTUNITY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LEND_OPPORTUNITY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchBorrowNegotiationData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getBorrowNegotiationData, action.payload);
    yield put({ type: `${FETCH_BORROW_NEGOTIATION_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_BORROW_NEGOTIATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchBorrowCompositionTrendData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getBorrowCompositionTrendData, action.payload);
    yield put({ type: `${FETCH_BORROW_COMPOSITION_TREND_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_BORROW_COMPOSITION_TREND_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSecurityMarketData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getSecurityMarketData, action.payload);
    yield put({ type: `${FETCH_SECURITY_MARKET_DATA_PNL}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_SECURITY_MARKET_DATA_PNL}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* seclendSaga() {
  yield all([
    takeEvery(FETCH_BORROW_RATE_CHANGE_DATA, fetchBorrowRateChangeData),
    takeEvery(FETCH_LEND_OPPORTUNITY_DATA, fetchLendOpportunityData),
    takeEvery(FETCH_BORROW_NEGOTIATION_DATA, fetchBorrowNegotiationData),
    takeEvery(FETCH_BORROW_COMPOSITION_TREND_DATA, fetchBorrowCompositionTrendData),
    takeEvery(FETCH_SECURITY_MARKET_DATA_PNL, fetchSecurityMarketData)
  ]);
}
