import {
  FETCH_BORROW_RATE_CHANGE_DATA,
  DESTROY_BORROW_RATE_CHANGE_DATA,
  FETCH_LEND_OPPORTUNITY_DATA,
  DESTROY_LEND_OPPORTUNITY_DATA,
  FETCH_BORROW_NEGOTIATION_DATA,
  DESTROY_BORROW_NEGOTIATION_DATA,
  SAVE_BORROW_OPPORTUNITIES_CONTAINER_DATA,
  SAVE_BORROW_OPPORTUNITIES_FILTERS,
  SAVE_LEND_OPPORTUNITIES_CONTAINER_DATA,
  SAVE_LEND_OPPORTUNITIES_FILTERS,
  SAVE_BORROW_RATE_CHANGE_CONTAINER_DATA,
  SAVE_BORROW_RATE_CHANGE_FILTERS,
  SAVE_BORROW_TREND_FILTERS,
  FETCH_BORROW_COMPOSITION_TREND_DATA,
  DESTROY_BORROW_COMPOSITION_TREND_DATA,
  FETCH_SECURITY_MARKET_DATA_PNL,
  DESTROY_DATA_FOR_SECURITY_MARKET_DATA,
  RESIZE_CANVAS
} from 'DecisionDrivingDashboard/constants';


export function fetchBorrowRateChangeData(payload) {
  return {
    type: FETCH_BORROW_RATE_CHANGE_DATA,
    payload
  };
}

export function destroyBorrowRateChangeData() {
  return {
    type: DESTROY_BORROW_RATE_CHANGE_DATA
  };
}

export function saveBorrowRateChangeFilters(payload) {
  return {
    type: SAVE_BORROW_RATE_CHANGE_FILTERS,
    payload
  };
}
export function saveBorrowRateChangeContainerData(payload) {
  return {
    type: SAVE_BORROW_RATE_CHANGE_CONTAINER_DATA,
    payload
  };
}

export function fetchLendOpportunityData(payload) {
  return {
    type: FETCH_LEND_OPPORTUNITY_DATA,
    payload
  };
}

export function destroyLendOpportunityData() {
  return {
    type: DESTROY_LEND_OPPORTUNITY_DATA
  };
}


export function saveLendOpportunitiesFilters(payload) {
  return {
    type: SAVE_LEND_OPPORTUNITIES_FILTERS,
    payload
  };
}
export function saveLendOpportunitiesContainerData(payload) {
  return {
    type: SAVE_LEND_OPPORTUNITIES_CONTAINER_DATA,
    payload
  };
}

export function fetchBorrowNegotiationData(payload) {
  return {
    type: FETCH_BORROW_NEGOTIATION_DATA,
    payload
  };
}

export function destroyBorrowNegotiationData() {
  return {
    type: DESTROY_BORROW_NEGOTIATION_DATA
  };
}

export function saveBorrowOpportunitiesFilters(payload) {
  return {
    type: SAVE_BORROW_OPPORTUNITIES_FILTERS,
    payload
  };
}
export function saveBorrowOpportunitiesContainerData(payload) {
  return {
    type: SAVE_BORROW_OPPORTUNITIES_CONTAINER_DATA,
    payload
  };
}

export function saveBorrowCompositionTrendFilters(payload) {
  return {
    type: SAVE_BORROW_TREND_FILTERS,
    payload
  };
}

export function fetchBorrowCompositionTrendData(payload) {
  return {
    type: FETCH_BORROW_COMPOSITION_TREND_DATA,
    payload
  };
}

export function destroyBorrowCompositionTrendData() {
  return {
    type: DESTROY_BORROW_COMPOSITION_TREND_DATA
  };
}

export function fetchSecurityMarketData(payload) {
  return {
    type: FETCH_SECURITY_MARKET_DATA_PNL,
    payload
  };
}

export function destroySecurityMarketData(payload) {
  return {
    type: DESTROY_DATA_FOR_SECURITY_MARKET_DATA,
    payload
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  }
}
