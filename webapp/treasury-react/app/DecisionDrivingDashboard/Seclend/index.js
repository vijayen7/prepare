import React, { Component } from 'react';
import Tab from 'commons/components/Tab';
import TabPanel from 'commons/components/TabPanel';
import Loader from 'commons/container/Loader';

import BorrowOpportunitiesSideBar from './BorrowOpportunities/containers/SideBar';
import LendOpportunitiesSideBar from './LendOpportunities/containers/SideBar';
import BorrowRateChangeSideBar from './BorrowRateChange/containers/SideBar';
import DataTrendsSideBar from './DataTrends/containers/SideBar';

import BorrowOpportunitiesMainContainer from './BorrowOpportunities/containers/MainContainer';
import LendOpportunitiesMainContainer from './LendOpportunities/containers/MainContainer';
import BorrowRateChangeContainerData from './BorrowRateChange/containers/MainContainer';
import DataTrendsMainContainer from './DataTrends/containers/MainContainer';

export default class SeclendDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedGadgetId: 'borrowOpportunities',
      collapsed: false
    };
    this.handleGadgetChange = this.handleGadgetChange.bind(this);
    this.onToggle = this.onToggle.bind(this);
  }

  getTabsList() {
    const tabs = [
      <Tab
        id="borrowOpportunities"
        label="Borrow Optimization"
        isSelected={this.state.selectedGadgetId === 'borrowOpportunities'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="lendOpportunities"
        label="Lend Opportunities"
        isSelected={this.state.selectedGadgetId === 'lendOpportunities'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="borrowRateChange"
        label="Borrow Rate Spikes"
        isSelected={this.state.selectedGadgetId === 'borrowRateChange'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="dataTrends"
        label="Borrow Composition Trend"
        isSelected={this.state.selectedGadgetId === 'dataTrends'}
        onClick={this.handleGadgetChange}
      />
    ];
    return tabs;
  }
  handleGadgetChange(event) {
    this.setState({...this.state, selectedGadgetId: event.target.id,collapsed:false });
  }

  onToggle(collapsed){
      this.setState({...this.state,collapsed});
  }

  getSideBar() {
    switch (this.state.selectedGadgetId) {
      case 'borrowOpportunities':
        return <BorrowOpportunitiesSideBar onToggle={this.onToggle}/>;
      case 'lendOpportunities':
        return <LendOpportunitiesSideBar onToggle={this.onToggle}/>;
      case 'borrowRateChange':
        return <BorrowRateChangeSideBar onToggle={this.onToggle}/>;
      case 'dataTrends':
        return <DataTrendsSideBar onToggle={this.onToggle}/>;
      default:
        return <BorrowOpportunitiesSideBar onToggle={this.onToggle}/>;
    }
  }

  getMainContainer() {
    switch (this.state.selectedGadgetId) {
      case 'borrowOpportunities':
        return <BorrowOpportunitiesMainContainer />;
      case 'lendOpportunities':
        return <LendOpportunitiesMainContainer />;
      case 'borrowRateChange':
        return <BorrowRateChangeContainerData />;
      case 'dataTrends':
        return <DataTrendsMainContainer />;
      default:
        return <BorrowOpportunitiesMainContainer />;
    }
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex" style={{display:this.state.collapsed?'block':''}}>
          <div className="size--1" style={{position:this.state.collapsed?'absolute':''}}>
            {this.getSideBar()}
          </div>
          <div className="size--4 padding--horizontal--double" style={{marginLeft:this.state.collapsed?'20px':''}}>
            <TabPanel
              className="align--center"
              tabs={this.getTabsList()}
              onClick={this.handleGadgetChange}
              selectedId={this.state.selectedGadgetId}
            />
            {this.getMainContainer()}
            <br />
          </div>
        </div>
      </React.Fragment>
    );
  }
}
