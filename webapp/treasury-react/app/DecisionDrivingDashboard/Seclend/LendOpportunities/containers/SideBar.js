import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Card } from 'arc-react-components';
import DateFilter from 'commons/container/DateFilter';
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';
import InputFilter from 'commons/components/InputFilter';
import LegalEntityFamilyFilter from 'commons/container/LegalEntityFamilyFilter';
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import CpeFamilyFilter from 'commons/container/CpeFamilyFilter';
import { getCommaSeparatedValuesOrEmpty, getCommaSeparatedValuesOrEmptyForSingleSelect } from 'commons/util';
import { getCodexFilters } from 'DecisionDrivingDashboard/util';
import {
  fetchLendOpportunityData,
  destroyLendOpportunityData,
  saveLendOpportunitiesFilters,
  resizeCanvas
} from '../../actions';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  componentWillMount() {
    const state = this.props.filters;
    if (Object.keys(state).length === 0) {
      this.setState(this.getDefaultFilters());
      this.props.saveLendOpportunitiesFilters(this.getDefaultFilters());
    }
    this.setState(state);
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedDate: getCodexFilters('DATE'),
      potentialIncomeThreshold: getCodexFilters('POTENTIAL_INCOME_THRESHOLD'),
      selectedLegalEntityFamilies: getCodexFilters('LEGAL_ENTITY_FAMILIES'),
      selectedCpeFamilies: getCodexFilters('CPE_FAMILIES'),
      selectedReportingCurrency: getCodexFilters('REPORTING_CURRENCY_ID')
    };
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    this.props.destroyLendOpportunityData();

    const payload = {
      date: this.state.selectedDate,
      potentialIncomeThreshold: this.state.potentialIncomeThreshold || 0,
      legalEntityFamilyIds: getCommaSeparatedValuesOrEmpty(
        this.state.selectedLegalEntityFamilies
      ),
      cpeFamilyIds: getCommaSeparatedValuesOrEmpty(
        this.state.selectedCpeFamilies
      ),
      reportingCurrencyId: getCommaSeparatedValuesOrEmptyForSingleSelect(
        this.state.selectedReportingCurrency
      )
    };

    this.props.saveLendOpportunitiesFilters(this.state);
    this.props.fetchLendOpportunityData(payload);
  }

  render() {
    return (
      <Sidebar
          collapsible
          onToggle={this.props.onToggle}
          enterFunction={this.handleClick}
          id="lendOpportunitiesSideBar"
          resizeCanvas={this.props.resizeCanvas}>
        <Card>
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedDate"
            data={this.state.selectedDate}
          />
        </Card>
        <Card>
          <CpeFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCpeFamilies}
          />
          <LegalEntityFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntityFamilies}
          />
        </Card>
        <Card>
          <InputFilter
            data={this.state.potentialIncomeThreshold}
            label="Potential Income Threshold ($)"
            onSelect={this.onSelect}
            stateKey="potentialIncomeThreshold"
          />
        </Card>
        <Card>
          <ReportingCurrencyFilter
            label="Reporting Currency"
            onSelect={this.onSelect}
            selectedData={this.state.selectedReportingCurrency}
            multiSelect={false}
            stateKey="selectedReportingCurrency"
          />
        </Card>
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.defaultProps = {
  filters: {}
};

SideBar.propTypes = {
  filters: PropTypes.object,
  saveLendOpportunitiesFilters: PropTypes.func,
  fetchLendOpportunityData: PropTypes.func,
  destroyLendOpportunityData: PropTypes.func,
  resizeCanvas: PropTypes.func
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.seclendView.lendOpportunitiesFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchLendOpportunityData,
      destroyLendOpportunityData,
      saveLendOpportunitiesFilters,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
