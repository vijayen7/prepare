import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { URL } from 'commons/constants';
import SliderPanel from 'commons/components/SliderPanel';
import GridContainer from 'commons/components/GridWithCellClick';
import LinkPanel from 'commons/components/LinkPanel';
import Link from 'commons/components/Link';
import Resizer from "commons/components/Resizer";
import ResizerContainer from "commons/container/ResizerContainer";
import Message from 'commons/components/Message';
import { getOrgIdsFromOrgFamilyIds, reportingCurrencyMessage } from 'DecisionDrivingDashboard/util';
import { gridColumns as lendOpportunitiesGridColumns } from '../grid/columnConfig';
import { gridOptions as lendOpportunitiesGridOptions } from '../grid/gridOptions';
import { convertJavaDate } from "commons/util";
import AvailabilityDataPanel from "../../../../AvailabilityDataPanel/components/AvailabilityDataPanel";
import { saveLendOpportunitiesContainerData, fetchSecurityMarketData, destroySecurityMarketData } from '../../actions';
import {
  loadNegotiationPopUp as loadNegotiationPopUpData,
} from "seclend/RateNegotiationDialog/actions"
import NegotiationDialog from "seclend/RateNegotiationDialog/RateNegotiationDialog"

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.reportingCurrencyMessage = reportingCurrencyMessage.bind(this);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length ?
      this.props.containerData : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.lendOpportunityData !== this.props.lendOpportunityData) {
      this.setState({ value: Math.min(nextProps.lendOpportunityData.length, 20) });
    }
  }

  componentWillUnmount() {
    this.props.saveLendOpportunitiesContainerData(this.state);
    this.props.destroySecurityMarketData();
  }

  handleSliderChange(value) {
    this.setState({ value });
  }

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  onCellClickHandler = args => {
    if (args.colId === "negotiation") {
        var positionDataId = args.item.positionDataId;
        args.item = args.item.seclendGadgetBaseData;
        args.item.view = "longPosition";
        args.item.id = positionDataId;
        args.item.spn = args.item.pnlSpn;
        args.item.securityLocalName = args.item.ticker;
        args.item.borrowType = "LEND";
        this.props.loadNegotiationPopUpData(args.item);
    }
  };

  getDefaultContainerData() {
    const length = this.props.lendOpportunityData !== undefined ? Math.min(this.props.lendOpportunityData.length, 20) : 0;
    return { view: 'grid', value: length };
  }

  /*
    Slices the data to Top N ( where N = value)
    */
  getGridData(data) {
    if (data && data instanceof Array) { return data.slice(0, this.state.value); }
  }

  onDblClickHandler = args => {
    var payload = {
      spn: args.seclendGadgetBaseData.pnlSpn,
      cusip: "__null__",
      sedol: "__null__",
      date: (this.props.filters.selectedDate)
    };
    this.props.fetchSecurityMarketData(payload);
    this.setState({ selectedCpeIdForDrillDown: args.seclendGadgetBaseData.cpeId });
  };

  renderSliderPanel() {
    return (<SliderPanel
      min={1}
      max={this.props.lendOpportunityData.length}
      value={this.state.value}
      view={this.state.view}
      onSliderChange={this.handleSliderChange}
      toggleOptions={[
        { key: 'grid', value: 'Grid' }
      ]}
      onToggleGroupChange={this.handleToggleGroupChange}
    />);
  }

  renderLinkPanel() {
    let cpeIds = getOrgIdsFromOrgFamilyIds(this.props.filters.selectedCpeFamilies, this.props.cpeFamilyToCpeData);
    let leIds = getOrgIdsFromOrgFamilyIds(this.props.filters.selectedLegalEntityFamilies, this.props.legalEntityFamilyToLegalEntityData);
    var filters = {
      'selectedCpes': cpeIds,
      'selectedLegalEntities': leIds,
       selectedDate:this.props.filters.selectedDate
    };
    return (
      <LinkPanel
        links={[
          <Link
            id="lend-report"
            text="Detailed Lend Opportunity Report"
            href={`${URL}/treasury/seclend/lendreportdata.html#${JSON.stringify(filters)}`}
          />,
          <Link
            id="impact-summary"
            text="View YTD savings"
            href={`${URL}/treasury/seclend/impactSummary`}
          />
        ]}
      />
    );
  }

  renderAvailabilityDataPanel() {
    let availabilityDataPanel = this.props.securityMarketData.length > 0 && (
      <div style={{ height: "280px" }}>
        <AvailabilityDataPanel
          data={this.props.securityMarketData}
          selectedCpeIdForDrillDown={this.state.selectedCpeIdForDrillDown}
        />
      </div>
    );
    return availabilityDataPanel;
  }

  renderGrid() {
    return (<GridContainer
      data={this.getGridData(this.props.lendOpportunityData)}
      gridId="LendOpportunityData"
      gridColumns={lendOpportunitiesGridColumns(this.props.lendOpportunityData)}
      gridOptions={lendOpportunitiesGridOptions()}
      resizeCanvas={this.props.resizeCanvas}
      onDblClick={this.onDblClickHandler}
      heightValue = {520}
      onCellClick={this.onCellClickHandler}
    />);
  }

  renderMainContainer() {
    let data = this.props.lendOpportunityData;
    if (data.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        <NegotiationDialog/>
        {this.renderSliderPanel()}
        {this.renderGrid()}
        {this.reportingCurrencyMessage(data)}
        {this.renderLinkPanel()}
        <hr/>
        {this.renderAvailabilityDataPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

MainContainer.defaultProps = {
  containerData: { view: 'grid', value: 5 },
  lendOpportunityData: []
};

MainContainer.propTypes = {
  containerData: PropTypes.object,
  lendOpportunityData: PropTypes.array,
  saveLendOpportunitiesContainerData: PropTypes.func,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    containerData: state.decisionDrivingDashboard.seclendView.lendOpportunitiesContainerData,
    lendOpportunityData: state.decisionDrivingDashboard.seclendView.lendOpportunitiesData,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    filters: state.decisionDrivingDashboard.seclendView.lendOpportunitiesFilters,
    securityMarketData: state.decisionDrivingDashboard.seclendView.securityMarketData,
    resizeCanvas: state.decisionDrivingDashboard.seclendView.resizeCanvas
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveLendOpportunitiesContainerData,
      fetchSecurityMarketData,
      destroySecurityMarketData,
      loadNegotiationPopUpData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(MainContainer);
