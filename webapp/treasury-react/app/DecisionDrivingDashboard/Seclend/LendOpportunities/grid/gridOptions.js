import { url } from "../../api";
export function gridOptions(paging) {
  const options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [{ columnId: 'slfFeeChange', sortAsc: false }],
    page: true,
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: 'Total',
    frozenColumn: 9,
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    }
  };
  return options;
}
