import {
  percentFormatter,
  priceFormatter,
  drillThroughFormatter,
  numberTotalsFormatter,
  percentTotalsFormatter,
  excelTotalsFormatter,
  marketDataPositionTypeFormatter,
  linkFormatter
} from 'commons/grid/formatters';

import {
  textComparator,
  numberComparator
} from 'commons/grid/comparators';

const columns = [];

function getCpeList(params) {
  if (params == undefined) {
    return [];
  }
  const cpeNames = [];
  for (let i = 0; i < params.length; i++) {
    for (let j = 0; j < params[i].ratesPerCpeList.length; j++) {
      if (cpeNames.indexOf(params[i].ratesPerCpeList[j].cpeName) == -1) {
        cpeNames.push(params[i].ratesPerCpeList[j].cpeName);
      }
    }
  }
  return cpeNames;
}

function getDynamicColumns(params) {
  const dynamic = [];
  const cpeNames = getCpeList(params);
  for (const name in cpeNames) {
    dynamic.push({
      id: cpeNames[name],
      name: `${cpeNames[name].toUpperCase()} Availability Rate`,
      field: 'ratesPerCpeList',
      toolTip: cpeNames[name],
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65,
      excelFormatter: '#,##0.0',
      formatter(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null) {
          for (let j = 0; j < value.length; j++) {
            if (value[j].cpeName == columnDef.id) {
              return priceFormatter(row, cell, value[j].rate, columnDef, dataContext);
            }
          }
        } else return null;
      },
      excelDataFormatter(value, rowElem, currentGrid, isCurrentViewBeingExported, columnDef) {
        if (value !== undefined && value !== null) {
          for (let j = 0; j < value.length; j++) {
            if (value[j].cpeName == columnDef.id) {
              return value[j].rate;
            }
          }
        } else return null;
      },
      filter: {
        isFilterOnFormattedValue: true
      }
    });
  }

  return dynamic;
}

export function gridColumns(params) {
  let columns = [{
    id: 'spn',
    name: 'PNL SPN',
    field: 'seclendGadgetBaseData',
    toolTip: 'PNL SPN',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 65,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.pnlSpn;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.pnlSpn;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return numberComparator(a.seclendGadgetBaseData.pnlSpn, b.seclendGadgetBaseData.pnlSpn); }
      return numberComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'securityDescription',
    name: 'Security Description',
    field: 'seclendGadgetBaseData',
    toolTip: 'Security Description',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 120,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.securityDescription;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.securityDescription;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.securityDescription, b.seclendGadgetBaseData.securityDescription); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'gboTypeName',
    name: 'GBO Type',
    field: 'seclendGadgetBaseData',
    toolTip: 'GBO TypeName',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 110,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.gboTypeName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.gboTypeName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.gboTypeName, b.seclendGadgetBaseData.gboTypeName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'sedol',
    name: 'Sedol',
    field: 'seclendGadgetBaseData',
    toolTip: 'sedol',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.sedol;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.sedol;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.sedol, b.seclendGadgetBaseData.sedol); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'ticker',
    name: 'Ticker',
    field: 'seclendGadgetBaseData',
    toolTip: 'Ticker',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.ticker;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.ticker;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.ticker, b.seclendGadgetBaseData.ticker); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'bookName',
    name: 'Book',
    field: 'seclendGadgetBaseData',
    toolTip: 'Book',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 90,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.bookName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.bookName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.bookName, b.seclendGadgetBaseData.bookName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'counterpartyEntity',
    name: 'Counterparty Entity',
    field: 'seclendGadgetBaseData',
    toolTip: 'Counterparty Entity',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 100,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.cpeName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.cpeName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.cpeName, b.seclendGadgetBaseData.cpeName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'legalEntity',
    name: 'Legal Entity',
    field: 'seclendGadgetBaseData',
    toolTip: 'Legal Entity',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.legalEntityName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.legalEntityName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.legalEntityName, b.seclendGadgetBaseData.legalEntityName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'currencyCode',
    name: 'Currency',
    field: 'seclendGadgetBaseData',
    toolTip: 'Currency',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.currencyCode;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.currencyCode;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.currencyCode, b.seclendGadgetBaseData.currencyCode); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
          id: "negotiation",
          name: "Negotiation",
          field: "negotiation",
          toolTip: "Negotiation",
          type: "text",
          filter: true,
          sortable: true,
          formatter: function(row, cell, value, columnDef, dataContext) {
            if (dataContext.pnlSpn !== -1 && dataContext.pnlSpn !== -2)
              return linkFormatter(
                row,
                cell,
                "<i class='icon-link' />",
                columnDef,
                dataContext
              );
             else return null;
          },
          headerCssClass: "b",
          width: 85
  },
  {
    id: 'custodianAccount',
    name: 'Custodian Account',
    field: 'seclendGadgetBaseData',
    toolTip: 'Custodian Account',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 110,
    formatter(row, cell, value, columnDef, dataContext) {
      if (value !== undefined && value !== null) return value.custodianAccountName;
      return null;
    },
    excelDataFormatter(value) {
      if (value !== undefined && value !== null) return value.custodianAccountName;
      return null;
    },
    comparator(a, b) {
      if (a['@CLASS'] !== undefined) { return textComparator(a.seclendGadgetBaseData.custodianAccountName, b.seclendGadgetBaseData.custodianAccountName); }
      return textComparator(a, b);
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  },
  {
    id: 'quantity',
    name: 'Quantity',
    field: 'quantity',
    toolTip: 'Quantity',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 80
  },
  {
    id: 'price',
    name: 'Price (USD)',
    field: 'price',
    toolTip: 'Price (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 60
  },
  {
    id: 'priceRC',
    name: 'Price (RC)',
    field: 'priceRC',
    toolTip: 'Price (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 60
  },
  {
    id: 'marketValue',
    name: 'Market Value (USD)',
    field: 'marketValue',
    toolTip: 'Market Value (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'marketValueRC',
    name: 'Market Value (RC)',
    field: 'marketValueRC',
    toolTip: 'Market Value (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'potentialLendIncome',
    name: 'Potential Lend Income (USD)',
    field: 'potentialLendIncome',
    toolTip: 'Potential Lend Income (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'potentialLendIncomeRC',
    name: 'Potential Lend Income (RC)',
    field: 'potentialLendIncomeRC',
    toolTip: 'Potential Lend Income (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 120
  },
  {
    id: 'bestSLFRate',
    name: 'Best SLF Rate',
    field: 'bestSLFRate',
    toolTip: 'Best SLF Rate',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    excelFormatter: '#,##0.0',
    width: 70
  }];
  columns = columns.concat(getDynamicColumns(params));

  return columns;
}
