import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from 'commons/constants';
import {
  FETCH_LEGAL_ENTITY_FAMILY_TO_LEGAL_ENTITY_DATA,
  FETCH_CPE_FAMILY_TO_CPE_DATA
} from 'DecisionDrivingDashboard/constants';
import { getLegalEntityFamilyToLegalEntityData, getCpeFamilyToCpeData } from './api';

import { put, takeEvery, call, all } from 'redux-saga/effects';
import { seclendSaga } from './Seclend/saga';
import { marginSaga } from './Margin/saga';
import { financingSaga } from './Financing/saga';


function* fetchLegalEntityFamilyToLegalEntityData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLegalEntityFamilyToLegalEntityData, action.payload);
    yield put({ type: `${FETCH_LEGAL_ENTITY_FAMILY_TO_LEGAL_ENTITY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LEGAL_ENTITY_FAMILY_TO_LEGAL_ENTITY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchCpeFamilyToCpeData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCpeFamilyToCpeData, action.payload);
    yield put({ type: `${FETCH_CPE_FAMILY_TO_CPE_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_CPE_FAMILY_TO_CPE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* decisionDrivingDashboardRootSaga() {
  yield all([
    seclendSaga(),
    financingSaga(),
    marginSaga(),
    takeEvery(FETCH_LEGAL_ENTITY_FAMILY_TO_LEGAL_ENTITY_DATA, fetchLegalEntityFamilyToLegalEntityData),
    takeEvery(FETCH_CPE_FAMILY_TO_CPE_DATA, fetchCpeFamilyToCpeData)
  ]);
}

export default decisionDrivingDashboardRootSaga;
