import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Message from 'commons/components/Message';
import { BASE_URL } from 'commons/constants';
import SliderPanel from 'commons/components/SliderPanel';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import GridContainer from 'commons/components/Grid';
import { getSelectedIdList} from 'commons/util';
import { getApplicableOrgIds } from 'DecisionDrivingDashboard/util';
import { positionsPenaltyDataColumns } from '../grid/columnConfig';
import { positionsPenaltyDataOptions } from '../grid/gridOptions';
import { saveTopPenaltiesContainerData } from '../../actions';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length ?
      this.props.containerData : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.positionsPenaltyData !== this.props.positionsPenaltyData) {
      this.setState({ value: nextProps.positionsPenaltyData.length });
    }
  }

  componentWillUnmount() {
    this.props.saveTopPenaltiesContainerData(this.state);
  }

  handleSliderChange(value) {
    this.setState({ value: value });
  }

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  getDefaultContainerData() {
    const length = this.props.positionsPenaltyData !== undefined ? this.props.positionsPenaltyData.length : 0;
    return { view: 'grid', value: length };
  }

  getGridData(data) {
    return (data && data instanceof Array ? data.slice(0, this.state.value) : null);
  }

  renderSliderPanel() {
    return (<SliderPanel
      min={1}
      max={10}
      value={this.state.value}
      view={this.state.view}
      onSliderChange={this.handleSliderChange}
      toggleOptions={[
        { key: 'grid', value: 'Grid' }
      ]}
      onToggleGroupChange={this.handleToggleGroupChange}
    />);
  }

  renderLinkPanel() {
    const date = this.props.filters.selectedDate;
    const agmtTypeIds = getSelectedIdList(this.props.filters.selectedAgreementTypes);
    const leIds = getApplicableOrgIds(getSelectedIdList(this.props.filters.selectedLegalEntities),
      getSelectedIdList(this.props.filters.selectedLegalEntityFamilies), this.props.legalEntityFamilyToLegalEntityData);
    const cpeIds = getApplicableOrgIds(getSelectedIdList(this.props.filters.selectedCpes),
      getSelectedIdList(this.props.filters.selectedCpeFamilies), this.props.cpeFamilyToCpeData);
    if (leIds.length === 0) leIds.push(-1);
    if (cpeIds.length === 0) cpeIds.push(-1);

    return (
      <LinkPanel
        links={[
          <Link
            id="intermediate-data"
            text="Intermediate Margin Data"
            href={`${BASE_URL}margin/intermediate-data?legalEntityIds=${leIds.join(',')}&cpeIds=${cpeIds.join(',')}&agreementTypeIds=${agmtTypeIds.join(',')}&dateString=${date}&isCopyUrl=true&_outputKey=resultList`}
          />,
          <Link
            id="simulation"
            text="Margin Simulation"
            href={`${BASE_URL}margin/simulation?descoEntityIds=${leIds.join(',')}&cpeIds=${cpeIds.join(',')}&agreementTypeIds=${agmtTypeIds.join(',')}&marginCalculatorIds=-1&bookIds=-1&dateString=${date}&isCopyUrl=true&_outputKey=resultList`}
          />
        ]}
      />
    );
  }

  renderGrid() {
    return (<GridContainer
      data={this.getGridData(this.props.positionsPenaltyData)}
      gridId="positionsPenaltyData"
      gridColumns={positionsPenaltyDataColumns()}
      gridOptions={positionsPenaltyDataOptions()}
      gridLabel="Top Penalties Grid"
      resizeCanvas={this.props.resizeCanvas}
    />);
  }

  renderMainContainer() {
    if (this.props.positionsPenaltyData.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderSliderPanel()}
        {this.renderGrid()}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

MainContainer.defaultProps = {
  positionsPenaltyData: [],
  containerData: { view: 'grid', value: 5 }
};

MainContainer.propTypes = {
  positionsPenaltyData: PropTypes.array,
  saveTopPenaltiesContainerData: PropTypes.func,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object,
  containerData: PropTypes.array
};

function mapStateToProps(state) {
  return {
    positionsPenaltyData: state.decisionDrivingDashboard.marginView.positionsPenaltyData,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    filters: state.decisionDrivingDashboard.marginView.topPenaltiesFilters,
    containerData: state.decisionDrivingDashboard.marginView.topPenaltiesContainerData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveTopPenaltiesContainerData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(MainContainer);
