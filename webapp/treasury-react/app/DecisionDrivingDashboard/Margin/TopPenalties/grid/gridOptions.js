import { url } from "../../api";
export function positionsPenaltyDataOptions() {
  const options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    maxHeight: 300,
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    }
  };

  return options;
}
