export function positionsPenaltyDataColumns() {
  let columns = [
    {
      id: 'legalEntity',
      name: 'Legal Entity',
      field: 'legalEntity',
      toolTip: 'Legal Entity',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 100
    },
    {
      id: 'agreementType',
      name: 'Agreement Type',
      field: 'agreementType',
      toolTip: 'Agreement Type',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'cpe',
      name: 'CPE',
      field: 'cpe',
      toolTip: 'CPE',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'businessUnit',
      name: 'Business Unit',
      field: 'businessUnit',
      toolTip: 'Business Unit',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'book',
      name: 'Book',
      field: 'book',
      toolTip: 'Book',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'ccyName',
      name: 'CCY',
      field: 'ccyName',
      toolTip: 'CCY',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'gboType',
      name: 'GBO Type',
      field: 'gboType',
      toolTip: 'GBO Type',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'desname',
      name: 'Security Name',
      field: 'desname',
      toolTip: 'Security Name',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'pnlSpn',
      name: 'PNL SPN',
      field: 'pnlSpn',
      toolTip: 'PNL SPN',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'quantity',
      name: 'Quantity',
      field: 'quantity',
      toolTip: 'Quantity',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'price',
      name: 'Price (USD)',
      field: 'price',
      toolTip: 'Price (USD)',
      type: 'number',
      formatter: dpGrid.Formatters.Float,
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'marketValue',
      name: 'Market Value USD',
      field: 'marketValue',
      toolTip: 'Market Value USD',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'applicablePenalties',
      name: 'Applicable Penalties',
      field: 'applicablePenalties',
      toolTip: 'Applicable Penalties',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    },
    {
      id: 'penaltyAmount',
      name: 'Penalty Amount (USD)',
      field: 'totalPenaltyUSD',
      toolTip: 'Penalty Amount (USD)',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65
    }
  ];

  return columns;
}
