import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BarChartComponent from 'commons/components/BarChart';
import CustomizedAxisTick from './CustomizedAxisTick';
import BarChartTooltip from './BarChartTooltip';
import {
  XAxis,
  YAxis,
  Legend,
  Tooltip
} from 'recharts';
import { formatNumberAsMoney } from './../../util';

export default class BarChart extends Component {
  constructor(props) {
    super(props);
  }

  sortPlotData(list, key) {
    return list.sort((a, b) => {
      if (a[key] === 'Others') return 1;
      if (b[key] === 'Others') return -1;
      return (a[key] > b[key]) ? 1 : -1;
    });
  }

  render() {
    return (
      <BarChartComponent
        getPlotData={this.props.getPlotData}
        sortPlotData={this.sortPlotData}
        tooltip={<Tooltip
          content={<BarChartTooltip data={this.props.tooltipData} />}
          cursor={false}
        />}
        legend={
          <Legend wrapperStyle={{ paddingTop: '100px' }} />
        }
        xAxis={
          <XAxis
            dataKey="name"
            interval={0}
            tick={<CustomizedAxisTick />}
            label={{ value: 'Counterparty Families', fill: '#b9b9b9', dy: 70 }}
          />
        }
        yAxis={
          <YAxis
            label={{ value: 'Usage By Asset Class (in USD)', angle: -90, fill: '#b9b9b9', dx: -35 }}
            tickFormatter={formatNumberAsMoney}
          />
        }
        stacked
      />
    );
  }
}

BarChart.propTypes = {
  getPlotData: PropTypes.func,
  tooltipData: PropTypes.object
};
