import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumberAsMoney } from './../../util';

export default class BarChartTooltip extends Component {
  constructor(props) {
    super(props);
  }

  getViewBoxStyle() {
    return {
      background: '#EBF1F5',
      borderColor: '#394B59',
      padding: '10px',
      textAlign: 'left'
    };
  }

  getTotalValueForCategory(payload) {
    let total = 0;
    payload.forEach((constituent) => {
      total += constituent.value;
    });
    return total;
  }

  render() {
    const { active } = this.props;
    if (active) {
      const { label, payload, data } = this.props;
      return (
        <div className="custom-tooltip" style={this.getViewBoxStyle()}>
          <b className="label">{data[label]}</b>
          <p style={{ color: '#414361', fontFamily: 'Lucida Sans Unicode' }}>
            {'Total : '}
            {formatNumberAsMoney(this.getTotalValueForCategory(payload))}
          </p>
          {
            payload.map((constituent) => (
              <p style={{ color: constituent.fill }}>
                {`${constituent.dataKey} : `}
                {formatNumberAsMoney(constituent.value)}
              </p>
            ))
          }
        </div>
      );
    }
    return null;
  }
}

BarChartTooltip.propTypes = {
  active: PropTypes.bool,
  data: PropTypes.object,
  label: PropTypes.string,
  payload: PropTypes.array
};
