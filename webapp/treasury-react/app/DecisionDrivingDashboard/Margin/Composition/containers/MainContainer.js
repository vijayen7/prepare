import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Message from 'commons/components/Message';
import { BASE_URL } from 'commons/constants';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import { getSelectedIdList} from 'commons/util';
import { getApplicableOrgIds } from 'DecisionDrivingDashboard/util';
import SliderPanel from 'commons/components/SliderPanel';
import BarChart from '../components/BarChart';
import { saveCompositionContainerData } from '../../actions';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.maxNumberOfConstituents = 3;
    this.getPlotData = this.getPlotData.bind(this);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length
      ? this.props.containerData
      : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.marginAndCompositionBreakdownData !== this.props.marginAndCompositionBreakdownData
    ) {
      this.setState({ value: nextProps.marginAndCompositionBreakdownData.length - 1 });
    }
  }

  componentWillUnmount() {
    this.props.saveCompositionContainerData(this.state);
  }

  handleSliderChange(value) {
    this.setState({ value });
  }

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  getDefaultContainerData() {
    const length =
      this.props.marginAndCompositionBreakdownData !== undefined
        ? this.props.marginAndCompositionBreakdownData.length
        : 0;
    return { view: 'chart', value: length };
  }

  updateOthersCategoryData(data, n) {
    const othersData = data[data.length - 1];
    for (let i = n; i < data.length - 1; i++) {
      const categoryData = data[i];
      Object.keys(categoryData.consituents).forEach((constituent) => {
        const usage = categoryData.consituents[constituent];
        if (constituent in othersData.consituents) {
          othersData.consituents[constituent] += usage;
        } else {
          othersData.consituents[constituent] = usage;
        }
        othersData.usage += usage;
      });
    }
    const others = this.getConstituentDataWithOthersEntry(othersData.consituents);
    others.name = 'Others';
    return others;
  }

  getConstituentDataWithOthersEntry(constituents) {
    if (constituents == null) return {};
    const groupedConstituents = { Others: 0 };
    const sortedByUsageDesc = Object.keys(constituents).sort((a, b) => {
      return constituents[b] - constituents[a];
    });
    for (let i = 0; i < sortedByUsageDesc.length; i++) {
      const constituentName = sortedByUsageDesc[i];
      if (i < this.maxNumberOfConstituents) {
        groupedConstituents[constituentName] = constituents[constituentName];
      } else {
        groupedConstituents.Others += constituents[constituentName];
      }
    }
    return groupedConstituents;
  }

  getPlotData() {
    const data = JSON.parse(JSON.stringify(this.props.marginAndCompositionBreakdownData));
    const noOfCategories = Math.min(data.length - 1, this.state.value);
    const plotData = [];

    for (let categoryIdx = 0; categoryIdx < noOfCategories; categoryIdx++) {
      const categoryData = data[categoryIdx];
      const categoryObj = this.getConstituentDataWithOthersEntry(categoryData.consituents);
      categoryObj.name = categoryData.abbrev;
      plotData.push(categoryObj);
    }

    plotData.push(this.updateOthersCategoryData(data, noOfCategories));
    return plotData;
  }

  getAbbrevToNameMap() {
    const abbrevToNameMap = {};
    const data = this.props.marginAndCompositionBreakdownData;
    for (let categoryIdx = 0; categoryIdx < data.length; categoryIdx++) {
      const categoryData = data[categoryIdx];
      abbrevToNameMap[categoryData.abbrev] = categoryData.name;
    }

    return abbrevToNameMap;
  }

  renderSliderPanel() {
    return (
      <SliderPanel
        min={1}
        max={this.props.marginAndCompositionBreakdownData.length - 1}
        value={this.state.value}
        view={this.state.view}
        onSliderChange={this.handleSliderChange}
        toggleOptions={[{ key: 'chart', value: 'Chart' }]}
        onToggleGroupChange={this.handleToggleGroupChange}
      />
    );
  }

  renderLinkPanel() {
    const date = this.props.filters.selectedDate;
    const agmtTypeIds = getSelectedIdList(this.props.filters.selectedAgreementTypes);
    const leIds = getApplicableOrgIds(getSelectedIdList(this.props.filters.selectedLegalEntities),
      getSelectedIdList(this.props.filters.selectedLegalEntityFamilies), this.props.legalEntityFamilyToLegalEntityData);
    const cpeIds = getApplicableOrgIds(getSelectedIdList(this.props.filters.selectedCpes),
      getSelectedIdList(this.props.filters.selectedCpeFamilies), this.props.cpeFamilyToCpeData);
    if (leIds.length === 0) leIds.push(-1);
    if (cpeIds.length === 0) cpeIds.push(-1);

    return (
      <LinkPanel
        links={[
          <Link
            id="intermediate-data"
            text="Intermediate Margin Data"
            href={`${BASE_URL}margin/intermediate-data?legalEntityIds=${leIds.join(',')}&cpeIds=${cpeIds.join(',')}&agreementTypeIds=${agmtTypeIds.join(',')}&dateString=${date}&isCopyUrl=true&_outputKey=resultList`}
          />,
          <Link
            id="simulation"
            text="Margin Simulation"
            href={`${BASE_URL}margin/simulation?descoEntityIds=${leIds.join(',')}&cpeIds=${cpeIds.join(',')}&agreementTypeIds=${agmtTypeIds.join(',')}&marginCalculatorIds=-1&bookIds=-1&dateString=${date}&isCopyUrl=true&_outputKey=resultList`}
          />
        ]}
      />
    );
  }

  renderChart() {
    return (
      <div>
        <BarChart getPlotData={this.getPlotData} tooltipData={this.getAbbrevToNameMap()} />
      </div>
    );
  }

  renderMainContainer() {
    if (this.props.marginAndCompositionBreakdownData.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderSliderPanel()}
        {this.renderChart()}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return this.renderMainContainer();
  }
}

MainContainer.defaultProps = {
  containerData: { view: 'chart', value: 5 },
  marginAndCompositionBreakdownData: []
};

MainContainer.propTypes = {
  containerData: PropTypes.object,
  marginAndCompositionBreakdownData: PropTypes.array,
  saveCompositionContainerData: PropTypes.func,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    marginAndCompositionBreakdownData:
      state.decisionDrivingDashboard.marginView.marginAndCompositionBreakdownData,
    positionsPenaltyData: state.decisionDrivingDashboard.marginView.positionsPenaltyData,
    containerData: state.decisionDrivingDashboard.marginView.compositionContainerData,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    filters: state.decisionDrivingDashboard.marginView.compositionFilters,
    resizeCanvas: state.decisionDrivingDashboard.marginView.resizeCanvas
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveCompositionContainerData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainContainer);
