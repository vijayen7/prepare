import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import DateFilter from 'commons/container/DateFilter';
import LegalEntityFilter from 'commons/container/LegalEntityFilter';
import LegalEntityFamilyFilter from 'commons/container/LegalEntityFamilyFilter';
import CpeFilter from 'commons/container/CpeFilter';
import CpeFamilyFilter from 'commons/container/CpeFamilyFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import BusinessUnitFilter from 'commons/container/BusinessUnitFilter';
import { getCommaSeparatedValues} from "commons/util";
import { getCodexFilters } from "DecisionDrivingDashboard/util";
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';

import {
  fetchMarginAndCompositionBreakdown,
  saveCompositionFilters,
} from '../../actions';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.state = {};
  }

  componentWillMount() {
    const state = this.props.filters;
    if (Object.keys(state).length === 0) {
      this.setState(this.getDefaultFilters());
      this.props.saveCompositionFilters(this.getDefaultFilters());
    }
    this.setState(state);
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedLegalEntities: getCodexFilters('LEGAL_ENTITIES'),
      selectedLegalEntityFamilies: getCodexFilters('LEGAL_ENTITY_FAMILIES'),
      selectedCpes: getCodexFilters('CPES'),
      selectedCpeFamilies: getCodexFilters('CPE_FAMILIES'),
      selectedBusinessUnits: getCodexFilters('BUSINESS_UNITS'),
      selectedAgreementTypes: getCodexFilters('AGREEMENT_TYPES'),
      selectedDate: getCodexFilters('DATE')
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    const payload = {
      'marginFilter.startDate': this.state.selectedDate,
      'marginFilter.endDate': this.state.selectedDate,
      'marginFilter.cpeIds': getCommaSeparatedValues(this.state.selectedCpes),
      'marginFilter.legalEntityIds': getCommaSeparatedValues(this.state.selectedLegalEntities),
      'marginFilter.cpeFamilyIds': getCommaSeparatedValues(this.state.selectedCpeFamilies),
      'marginFilter.legalEntityFamilyIds': getCommaSeparatedValues(this.state.selectedLegalEntityFamilies),
      'marginFilter.businessUnitIds': getCommaSeparatedValues(this.state.selectedBusinessUnits),
      'marginFilter.agreementTypeIds': getCommaSeparatedValues(this.state.selectedAgreementTypes)
    };
    this.props.saveCompositionFilters(this.state);
    this.props.fetchMarginAndCompositionBreakdown(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedDate"
          data={this.state.selectedDate}
          label="Date"
        />
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <LegalEntityFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntityFamilies}
        />
        <CpeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpes}
        />
        <CpeFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpeFamilies}
        />
        <AgreementTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedAgreementTypes}
        />
        <BusinessUnitFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnits}
        />

        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.defaultProps = {
  filters: {}
};

SideBar.propTypes = {
  fetchMarginAndCompositionBreakdown: PropTypes.func,
  saveCompositionFilters: PropTypes.func,
  filters: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.marginView.compositionFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchMarginAndCompositionBreakdown,
      saveCompositionFilters,
    },
    dispatch
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
