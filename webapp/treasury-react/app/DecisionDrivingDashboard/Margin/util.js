
export function formatNumberAsMoney(value) {
  if (isNaN(value)) {
    return "";
  }

  value = Number(value);
  const prefix = "$";
  const numberFormatter = new Intl.NumberFormat("en");
  const conversionFactors = [
    { conversionFactor: 1000000000, suffix: "B" },
    { conversionFactor: 1000000, suffix: "M" },
    { conversionFactor: 1000, suffix: "K" },
    { conversionFactor: 1, suffix: "" },
  ];

  for (let index = 0; index < conversionFactors.length; index++) {
    const { conversionFactor, suffix } = conversionFactors[index];
    const convertedFigure = roundOffToOneDecimalPlace(value / conversionFactor);
    if (Math.abs(convertedFigure) >= 0.1) {
      const sign = convertedFigure < 0 ? "-" : "";
      const formattedValue = numberFormatter.format(Math.abs(convertedFigure));
      return `${sign}${prefix}${formattedValue}${suffix}`;
    }
  }

  return `${prefix}0`;
}

function roundOffToOneDecimalPlace(number) {
  return number.toFixed(1);
}
