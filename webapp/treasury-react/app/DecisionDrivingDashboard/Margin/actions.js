import {
  FETCH_MARGIN_AND_COMPOSITION_BREAKDOWN,
  SAVE_COMPOSITION_FILTERS,
  SAVE_COMPOSITION_CONTAINER_DATA,
  SAVE_DOD_FILTERS,
  SAVE_DOD_CONTAINER_DATA,
  SAVE_TOP_PENALTIES_FILTERS,
  SAVE_TOP_PENALTIES_CONTAINER_DATA,
  SAVE_MARGIN_TREND_FILTERS,
  FETCH_MARGIN_DAY_ON_DAY_DIFF_DATA,
  FETCH_POSITIONS_PENALTY_DATA,
  FETCH_MARGIN_TREND_DATA,
  RESIZE_CANVAS
} from 'DecisionDrivingDashboard/constants';

export function fetchMarginAndCompositionBreakdown(payload) {
  return {
    type: FETCH_MARGIN_AND_COMPOSITION_BREAKDOWN,
    payload
  };
}

export function fetchMarginDayOnDayDiffData(payload, isAsync = false) {
  return {
    type: FETCH_MARGIN_DAY_ON_DAY_DIFF_DATA,
    payload,
    isAsync
  };
}

export function fetchPositionsPenaltyData(payload, isAsync = false) {
  return {
    type: FETCH_POSITIONS_PENALTY_DATA,
    payload,
    isAsync
  };
}

export function saveCompositionFilters(payload) {
  return {
    type: SAVE_COMPOSITION_FILTERS,
    payload
  };
}

export function saveCompositionContainerData(payload) {
  return {
    type: SAVE_COMPOSITION_CONTAINER_DATA,
    payload
  };
}

export function saveDoDFilters(payload) {
  return {
    type: SAVE_DOD_FILTERS,
    payload
  };
}

export function saveDoDContainerData(payload) {
  return {
    type: SAVE_DOD_CONTAINER_DATA,
    payload
  };
}

export function saveTopPenaltiesFilters(payload) {
  return {
    type: SAVE_TOP_PENALTIES_FILTERS,
    payload
  };
}

export function saveTopPenaltiesContainerData(payload) {
  return {
    type: SAVE_TOP_PENALTIES_CONTAINER_DATA,
    payload
  };
}

export function saveMarginTrendFilters(payload) {
  return {
    type: SAVE_MARGIN_TREND_FILTERS,
    payload
  };
}

export function fetchMarginTrendData(payload, isAsync = false) {
  return {
    type: FETCH_MARGIN_TREND_DATA,
    payload,
    isAsync
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}
