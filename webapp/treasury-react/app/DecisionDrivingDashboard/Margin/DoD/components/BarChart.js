import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BarChartTooltip from './BarChartTooltip';
import { formatNumberAsMoney } from './../../util';
import CustomizedAxisTick from './CustomizedAxisTick';
import BiaxialCompositeChart from 'commons/components/BiaxialCompositeChart';

export default class BarChart extends Component {
  constructor(props) {
    super(props);
  }

  sortPlotData(list, key) {
    return list.sort((a, b) => {
      if (a[key] === 'Current Day Usage') return 1;
      if (b[key] === 'Current Day Usage') return -1;
    });
  }

  getStackId() {
    return {
      'Current Day GMV': 'a',
      'Previous Day GMV': 'b',
      'Current Day Usage': 'a',
      'Previous Day Usage': 'b'
    };
  }

  render() {
    return (
      <BiaxialCompositeChart
        data={this.props.getPlotData()}
        bars={[
          { yAxisId: 'left', dataKey: 'Previous Day Total Penalties', size: 35, name: 'Previous Day Total Penalties', stackId: 'previous', fill: '#c0a2ff' },
          { yAxisId: 'left', dataKey: 'Current Day Total Penalties', size: 35, name: 'Current Day Total Penalties', stackId: 'current', fill: '#fe819d' },
          { yAxisId: 'left', dataKey: 'Previous Day Usage', size: 35, name: 'Previous Day Usage', stackId: 'previous', fill: '#a2d5f7' },
          { yAxisId: 'left', dataKey: 'Current Day Usage', size: 35, name: 'Current Day Usage', stackId: 'current', fill: '#DAF9D8' }
        ]}
        lines={
          [
            { yAxisId: 'right', dataKey: 'Previous Day GMV', name: 'Previous Day GMV' },
            { yAxisId: 'right', dataKey: 'Current Day GMV', name: 'Current Day GMV' }
          ]
        }
        xAxis={{ tick: <CustomizedAxisTick />, label: 'Counterparty Families' }}
        yAxis={{ left: { label: 'Day on Day Diff of Usage (in USD)', tickFormatter: formatNumberAsMoney },
          right: { label: 'GMV (in USD)', tickFormatter: formatNumberAsMoney } }}
        legend={{ wrapperStyle: { paddingTop: 80 } }}
        tooltip={{ formatter: <BarChartTooltip data={this.props.tooltipData} /> }}
      />
    );
  }
}

BarChart.propTypes = {
  getPlotData: PropTypes.func,
  tooltipData: PropTypes.object
};
