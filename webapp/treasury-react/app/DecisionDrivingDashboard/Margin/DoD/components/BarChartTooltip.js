import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumberAsMoney } from './../../util';

export default class BarChartTooltip extends Component {
  constructor(props) {
    super(props);
  }

  getViewBoxStyle() {
    return {
      background: '#EBF1F5',
      borderColor: '#394B59',
      padding: '10px',
      textAlign: 'left'
    };
  }

  updateUsageData(payload) {
    payload[2].value += payload[0].value;
    payload[3].value += payload[1].value;
    return payload;
  }

  render() {
    const { active } = this.props;
    if (active) {
      const { label, payload, data } = this.props;
      this.updateUsageData(payload);
      return (
        <div className="custom-tooltip" style={this.getViewBoxStyle()} >
          <b className="label">{data[label]}</b>
          <p style={{ color: '#AAD5C1' }} >
            {`${payload[3].dataKey} : `}
            {formatNumberAsMoney(payload[3].value)}
          </p>
          <p style={{ color: payload[2].fill }} >
            {`${payload[2].dataKey} : `}
            {formatNumberAsMoney(payload[2].value)}
          </p>
          <b style={{ color: '#BC8034' }}>
            {'Usage Diff : '}
            {formatNumberAsMoney(payload[3].value - payload[2].value)}
          </b>
        <p style={{ color: payload[1].fill }} >
            {`${payload[1].dataKey} : `}
            {formatNumberAsMoney(payload[1].value)}
        </p>
        <p style={{ color: payload[0].fill }}>
            {`${payload[0].dataKey} : `}
            {formatNumberAsMoney(payload[0].value)}
         </p>
         <b style={{ color: '#BC8034' }} >
            {'Penalties Diff : '}
            {formatNumberAsMoney(payload[1].value - payload[0].value)}
         </b>

          <p style={{ color: payload[4].stroke }} >
            {`${payload[4].dataKey} : `}
            {formatNumberAsMoney(payload[4].value)}
          </p>
          <p style={{ color: payload[5].stroke }}>
            {`${payload[5].dataKey} : `}
            {formatNumberAsMoney(payload[5].value)}
          </p>
          <b style={{ color: '#BC8034' }}>
            {'GMV Diff : '}
            {formatNumberAsMoney(payload[5].value - payload[4].value)}
          </b>
        </div>
      );
    }

    return null;
  }
}

BarChartTooltip.propTypes = {
  active: PropTypes.bool,
  label: PropTypes.string,
  data: PropTypes.object,
  payload: PropTypes.array
};
