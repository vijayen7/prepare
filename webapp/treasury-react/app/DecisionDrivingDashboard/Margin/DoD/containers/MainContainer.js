import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Message from 'commons/components/Message';
import { BASE_URL } from 'commons/constants';
import SliderPanel from 'commons/components/SliderPanel';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import BarChart from '../components/BarChart';
import { saveDoDContainerData } from '../../actions';
import { getSelectedIdList} from 'commons/util';
import { getApplicableOrgIds } from 'DecisionDrivingDashboard/util';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.getPlotData = this.getPlotData.bind(this);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length ?
      this.props.containerData : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.marginDayOnDayDiffData !== this.props.marginDayOnDayDiffData) {
      this.setState({ value: nextProps.marginDayOnDayDiffData.length - 1 });
    }
  }

  componentWillUnmount() {
    this.props.saveDoDContainerData(this.state);
  }

  handleSliderChange(value) {
    this.setState({ value });
  }

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  getDefaultContainerData() {
    const length = this.props.marginDayOnDayDiffData !== undefined ? this.props.marginDayOnDayDiffData.length : 0;
    return { view: 'chart', value: length };
  }

  getOthersDataExceptTopN(n) {
    const data = this.props.marginDayOnDayDiffData;
    const others = {
      name: 'Others',
      'Current Day Usage': 0,
      'Previous Day Usage': 0,
      'Current Day GMV': 0,
      'Previous Day GMV': 0,
      'Current Day Total Penalties': 0,
      'Previous Day Total Penalties': 0
    };

    // collect constituents for all categories except top N in othersConstituents
    for (let i = n; i < data.length; i++) {
      others['Current Day Usage'] += data[i].usage;
      others['Previous Day Usage'] += data[i].prevDayUsage;
      others['Current Day GMV'] += data[i].gmvUSD;
      others['Previous Day GMV'] += data[i].prevDayGmvUSD;
      others['Current Day Total Penalties'] += data[i].totalPenaltyUSD;
      others['Previous Day Total Penalties'] += data[i].prevDayTotalPenaltyUSD;
    }
    others['Current Day Usage'] -= others['Current Day Total Penalties'];
    others['Previous Day Usage'] -= others['Previous Day Total Penalties'];
    return others;
  }

  // In plot data, we subtract penalty number from usage and stack them to show penalty as a part of usage.
  // Example : Usage = 200 out of which Penalty = 50. Here, we set height of usage stack to 200 - 50 = 150
  // and add penalty stack of height 50. We add bacck this number to usage while showing total usage in tooltip.

  getPlotData() {
    const data = this.props.marginDayOnDayDiffData;
    const noOfCategories = Math.min(data.length - 1, this.state.value);
    const plotData = [];
    for (let categoryIdx = 0; categoryIdx < noOfCategories; categoryIdx++) {
      const categoryObj = {};
      const categoryData = data[categoryIdx];
      categoryObj.name = categoryData.abbrev;
      categoryObj['Current Day Usage'] = categoryData.usage - categoryData.totalPenaltyUSD;
      categoryObj['Previous Day Usage'] = categoryData.prevDayUsage - categoryData.prevDayTotalPenaltyUSD;
      categoryObj['Current Day GMV'] = categoryData.gmvUSD;
      categoryObj['Previous Day GMV'] = categoryData.prevDayGmvUSD;
      categoryObj['Current Day Total Penalties'] = categoryData.totalPenaltyUSD;
      categoryObj['Previous Day Total Penalties'] = categoryData.prevDayTotalPenaltyUSD;
      plotData.push(categoryObj);
    }

    plotData.push(this.getOthersDataExceptTopN(noOfCategories));
    return plotData;
  }

  getAbbrevToNameMap() {
    const abbrevToNameMap = {};
    const data = this.props.marginDayOnDayDiffData;
    for (let categoryIdx = 0; categoryIdx < data.length; categoryIdx++) {
      const categoryData = data[categoryIdx];
      abbrevToNameMap[categoryData.abbrev] = categoryData.name;
    }

    return abbrevToNameMap;
  }

  renderSliderPanel() {
    return (<SliderPanel
      min={1}
      max={this.props.marginDayOnDayDiffData.length - 1}
      value={this.state.value}
      view={this.state.view}
      onSliderChange={this.handleSliderChange}
      toggleOptions={[
        { key: 'chart', value: 'Chart' }
      ]}
      onToggleGroupChange={this.handleToggleGroupChange}
    />);
  }

  renderLinkPanel() {
    const date = this.props.filters.selectedDate;
    const agmtTypeIds = getSelectedIdList(this.props.filters.selectedAgreementTypes);
    const leIds = getApplicableOrgIds(getSelectedIdList(this.props.filters.selectedLegalEntities),
      getSelectedIdList(this.props.filters.selectedLegalEntityFamilies), this.props.legalEntityFamilyToLegalEntityData);
    const cpeIds = getApplicableOrgIds(getSelectedIdList(this.props.filters.selectedCpes),
      getSelectedIdList(this.props.filters.selectedCpeFamilies), this.props.cpeFamilyToCpeData);
    if (leIds.length === 0) leIds.push(-1);
    if (cpeIds.length === 0) cpeIds.push(-1);

    return (
      <LinkPanel
        links={[
          <Link
            id="intermediate-data"
            text="Intermediate Margin Data"
            href={`${BASE_URL}margin/intermediate-data?legalEntityIds=${leIds.join(',')}&cpeIds=${cpeIds.join(',')}&agreementTypeIds=${agmtTypeIds.join(',')}&dateString=${date}&isCopyUrl=true&_outputKey=resultList`}
          />,
          <Link
            id="simulation"
            text="Margin Simulation"
            href={`${BASE_URL}margin/simulation?descoEntityIds=${leIds.join(',')}&cpeIds=${cpeIds.join(',')}&agreementTypeIds=${agmtTypeIds.join(',')}&marginCalculatorIds=-1&bookIds=-1&dateString=${date}&isCopyUrl=true&_outputKey=resultList`}
          />
        ]}
      />
    );
  }

  renderChart() {
    return (
      <div>
        <BarChart getPlotData={this.getPlotData} tooltipData={this.getAbbrevToNameMap()} />
      </div>);
  }

  renderMainContainer() {
    if (this.props.marginDayOnDayDiffData.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderSliderPanel()}
        {this.renderChart()}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

MainContainer.defaultProps = {
  marginDayOnDayDiffData: [],
  containerData: { view: 'chart', value: 5 }
};

MainContainer.propTypes = {
  marginDayOnDayDiffData: PropTypes.array,
  saveDoDContainerData: PropTypes.func,
  containerData: PropTypes.array,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object
};

function mapStateToProps(state) {
  return {
    marginDayOnDayDiffData: state.decisionDrivingDashboard.marginView.marginDayOnDayDiffData,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    filters: state.decisionDrivingDashboard.marginView.dodFilters,
    containerData: state.decisionDrivingDashboard.marginView.dodContainerData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveDoDContainerData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(MainContainer);
