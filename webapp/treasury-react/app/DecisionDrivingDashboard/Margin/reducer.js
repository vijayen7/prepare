import {
  SAVE_COMPOSITION_FILTERS,
  SAVE_COMPOSITION_CONTAINER_DATA,
  SAVE_DOD_FILTERS,
  SAVE_DOD_CONTAINER_DATA,
  SAVE_TOP_PENALTIES_FILTERS,
  SAVE_TOP_PENALTIES_CONTAINER_DATA,
  SAVE_MARGIN_TREND_FILTERS,
  FETCH_MARGIN_AND_COMPOSITION_BREAKDOWN,
  FETCH_MARGIN_DAY_ON_DAY_DIFF_DATA,
  FETCH_POSITIONS_PENALTY_DATA,
  FETCH_MARGIN_TREND_DATA,
  RESIZE_CANVAS
} from 'DecisionDrivingDashboard/constants';
import { combineReducers } from 'redux';

function compositionFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_COMPOSITION_FILTERS}`:
      return action.payload || {};
  }
  return state;
}

function compositionContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_COMPOSITION_CONTAINER_DATA}`:
      return action.payload || {};
  }
  return state;
}

function dodFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_DOD_FILTERS}`:
      return action.payload || {};
  }
  return state;
}

function dodContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_DOD_CONTAINER_DATA}`:
      return action.payload || {};
  }
  return state;
}

function topPenaltiesFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_TOP_PENALTIES_FILTERS}`:
      return action.payload || {};
  }
  return state;
}

function topPenaltiesContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_TOP_PENALTIES_CONTAINER_DATA}`:
      return action.payload || {};
  }
  return state;
}

function marginTrendFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_MARGIN_TREND_FILTERS}`:
      return action.payload || {};
    default:
      return state;
  }
}

function marginAndCompositionBreakdownDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_MARGIN_AND_COMPOSITION_BREAKDOWN}_SUCCESS`:
      return action.data || [];
  }
  return state;
}

function marginDayOnDayDiffDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_MARGIN_DAY_ON_DAY_DIFF_DATA}_SUCCESS`:
      return action.data || [];
  }
  return state;
}

function positionsPenaltyDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_POSITIONS_PENALTY_DATA}_SUCCESS`:
      return action.data || [];
  }
  return state;
}

function marginTrendDataReducer(state = null, action) {
  switch (action.type) {
    case `${FETCH_MARGIN_TREND_DATA}_SUCCESS`:
      return action.data || [];
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state !== true;
    default:
      return false;
  }
}

const rootReducer = combineReducers({
  marginAndCompositionBreakdownData: marginAndCompositionBreakdownDataReducer,
  marginDayOnDayDiffData: marginDayOnDayDiffDataReducer,
  positionsPenaltyData: positionsPenaltyDataReducer,
  compositionFilters: compositionFiltersReducer,
  compositionContainerData: compositionContainerDataReducer,
  dodFilters: dodFiltersReducer,
  dodContainerData: dodContainerDataReducer,
  topPenaltiesFilters: topPenaltiesFiltersReducer,
  topPenaltiesContainerData: topPenaltiesContainerDataReducer,
  marginTrendFilters: marginTrendFiltersReducer,
  marginTrendData: marginTrendDataReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
