import { BASE_URL } from 'commons/constants';
const queryString = require('query-string');
export let url = '';

export function getMarginAndCompositionBreakdown(payload) {
  const paramString = queryString.stringify(payload);
  const defaultString = '&breakDownType=CPE_FAMILY&compositionType=ASSET_CLASS&countX=7&countY=3';
  url = `${BASE_URL}service/marginReportingService/getMarginAndCompositionBreakdown?inputFormat=PROPERTIES&${paramString + defaultString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getMarginDayOnDayDiffData(payload) {
  const paramString = queryString.stringify(payload);
  const defaultString = '&breakDownType=CPE_FAMILY&countX=7';
  url = `${BASE_URL}service/marginReportingService/getMarginDayOnDayDiff?inputFormat=PROPERTIES&${paramString + defaultString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getPositionsPenaltyData(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/marginReportingService/getTopNPositionsByPenalty?inputFormat=PROPERTIES&${paramString}&positionCount=10&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getMarginTrendData(payload) {
  const { chartType, ...filterParams } = payload;
  const paramString = queryString.stringify(filterParams);
  url = `${BASE_URL}service/marginReportingService/getHistoricalMarginData?inputFormat=PROPERTIES&${paramString}&chartType=${chartType}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}
