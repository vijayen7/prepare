import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import DateFilter from 'commons/container/DateFilter';
import LegalEntityFamilyFilter from 'commons/container/LegalEntityFamilyFilter';
import CpeFamilyFilter from 'commons/container/CpeFamilyFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import BusinessUnitFilter from 'commons/container/BusinessUnitFilter';
import { getCodexFilters } from 'DecisionDrivingDashboard/util';
import { getCommaSeparatedValuesOrNull,
  getCommaSeparatedValuesOrNullForSingleSelect } from 'commons/util';
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';
import MarginTrendChartTypeFilter from '../components/MarginTrendChartTypeFilter';

import {
  fetchMarginTrendData,
  saveMarginTrendFilters
} from '../../actions';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.state = {};
  }

  componentWillMount() {
    const state = this.props.filters;
    if (Object.keys(state).length === 0) {
      this.setState(this.getDefaultFilters());
      this.props.saveMarginTrendFilters(this.getDefaultFilters());
    }
    this.setState(state);
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    const CPE_FAMILY = { key: 'CPE_FAMILY', value: 'CPE Family Breakdown' };
    return {
      selectedLegalEntityFamilies: getCodexFilters('LEGAL_ENTITY_FAMILIES'),
      selectedCpeFamilies: getCodexFilters('CPE_FAMILIES'),
      selectedBusinessUnits: getCodexFilters('BUSINESS_UNITS'),
      selectedAgreementTypes: getCodexFilters('AGREEMENT_TYPES'),
      selectedStartDate: getCodexFilters('MARGIN_TREND_START_DATE'),
      selectedEndDate: getCodexFilters('TREND_END_DATE'),
      selectedMarginTrendChartType: CPE_FAMILY
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    const payload = {
      'marginFilter.startDate': this.state.selectedStartDate,
      'marginFilter.endDate': this.state.selectedEndDate,
      'marginFilter.legalEntityFamilyIds': getCommaSeparatedValuesOrNull(this.state.selectedLegalEntityFamilies),
      'marginFilter.cpeFamilyIds': getCommaSeparatedValuesOrNull(this.state.selectedCpeFamilies),
      'marginFilter.businessUnitIds': getCommaSeparatedValuesOrNull(this.state.selectedBusinessUnits),
      'marginFilter.agreementTypeIds': getCommaSeparatedValuesOrNull(this.state.selectedAgreementTypes),
      chartType: getCommaSeparatedValuesOrNullForSingleSelect(this.state.selectedMarginTrendChartType)
    };

    this.props.saveMarginTrendFilters(this.state);
    this.props.fetchMarginTrendData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedStartDate"
          data={this.state.selectedStartDate}
          label="Start Date"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedEndDate"
          data={this.state.selectedEndDate}
          label="End Date"
        />
        <CpeFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpeFamilies}
        />
        <LegalEntityFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntityFamilies}
        />
        <AgreementTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedAgreementTypes}
        />
        <BusinessUnitFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBusinessUnits}
        />
        <MarginTrendChartTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedMarginTrendChartType}
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.defaultProps = {
  filters: {}
};

SideBar.propTypes = {
  fetchMarginTrendData: PropTypes.func,
  saveMarginTrendFilters: PropTypes.func,
  filters: PropTypes.object
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.marginView.marginTrendFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchMarginTrendData,
      saveMarginTrendFilters
    },
    dispatch
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
