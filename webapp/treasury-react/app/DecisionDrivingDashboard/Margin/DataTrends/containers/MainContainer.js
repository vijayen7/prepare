import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getSelectedIdList} from 'commons/util';
import { getApplicableOrgIds } from 'DecisionDrivingDashboard/util';
import { BASE_URL } from 'commons/constants';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import DataTrendsChart from 'commons/components/DataTrendsChart';
import { formatNumberAsMoney } from './../../util';
import Message from 'commons/components/Message';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.getPlotDataForCpeFamilyBreakdown = this.getPlotDataForCpeFamilyBreakdown.bind(this);
    this.getPlotDataForUnderlyingValueComparison = this.getPlotDataForUnderlyingValueComparison.bind(this);
  }

  getPlotDataForCpeFamilyBreakdown() {
    const data = this.props.marginTrendData;
    const noOfCategories = data.length;
    const plotData = [];
    for (let categoryIdx = 0; categoryIdx < noOfCategories; categoryIdx++) {
      const categoryObj = {};
      const categoryData = data[categoryIdx];

      if (categoryData.name !== 'Others') {
        categoryObj.name = categoryData.name;
        for (const constituent in categoryData.consituents) {
          if (constituent !== 'Others') { categoryObj[constituent] = categoryData.consituents[constituent]; }
        }
        plotData.push(categoryObj);
      }
    }
    return plotData;
  }

  getPlotDataForUnderlyingValueComparison() {
    const data = this.props.marginTrendData;
    const plotData = [];
    for (let i = 0; i < data.length; i++) {
      const { name, usage, gmvUSD } = data[i];
      plotData.push({ name,
        'Total Usage': usage,
        'Total GMV': gmvUSD });
    }
    return plotData;
  }

  renderLinkPanel() {
    const date = this.props.filters.selectedEndDate;
    const agmtTypeIds = getSelectedIdList(this.props.filters.selectedAgreementTypes);
    const leIds = getApplicableOrgIds([-1],
      getSelectedIdList(this.props.filters.selectedLegalEntityFamilies), this.props.legalEntityFamilyToLegalEntityData);
    const cpeIds = getApplicableOrgIds([-1],
      getSelectedIdList(this.props.filters.selectedCpeFamilies), this.props.cpeFamilyToCpeData);
    if (leIds.length === 0) leIds.push(-1);
    if (cpeIds.length === 0) cpeIds.push(-1);

    return (
      <LinkPanel
        links={[
          <Link
            id="intermediate-data"
            text="Intermediate Margin Data"
            href={`${BASE_URL}margin/intermediate-data?legalEntityIds=${leIds.join(',')}&cpeIds=${cpeIds.join(',')}&agreementTypeIds=${agmtTypeIds.join(',')}&dateString=${date}&isCopyUrl=true&_outputKey=resultList`}
          />
        ]}
      />
    );
  }


  renderChart() {
    switch (this.props.filters.selectedMarginTrendChartType.key) {
      case 'CPE_FAMILY':
        return (
          <DataTrendsChart
            data={this.getPlotDataForCpeFamilyBreakdown()}
            xAxis={{ dataKey: 'name', label: 'Date' }}
            yAxis={{ left: { label: 'Counterparty Families (Usage in USD)', tickFormatter: formatNumberAsMoney } }}
            tooltip={{ formatter: formatNumberAsMoney }}
          />
        );
      case 'MARGIN_VS_GMV':
        return (
          <DataTrendsChart
            data={this.getPlotDataForUnderlyingValueComparison()}
            xAxis={{ dataKey: 'name', label: 'Date' }}
            yAxis={{
              left: { label: 'Total Usage (in USD)', tickFormatter: formatNumberAsMoney },
              right: { label: 'Total GMV (in USD)', tickFormatter: formatNumberAsMoney }
            }}
            rightYAxisDataKeys={['Total GMV']}
            tooltip={{ formatter: formatNumberAsMoney }}
          />
        );
      default:
        return null;
    }
  }

  renderMainContainer() {
    const { marginTrendData } = this.props;

    if (marginTrendData === null) {
      return <Message className="margin--top" messageData="Search to load data." />;
    }

    if (marginTrendData.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderChart()}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

function mapStateToProps(state) {
  return {
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    marginTrendData: state.decisionDrivingDashboard.marginView.marginTrendData,
    filters: state.decisionDrivingDashboard.marginView.marginTrendFilters
  };
}

MainContainer.propTypes = {
  marginTrendData: PropTypes.array,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object
};

export default connect(
  mapStateToProps
)(MainContainer);
