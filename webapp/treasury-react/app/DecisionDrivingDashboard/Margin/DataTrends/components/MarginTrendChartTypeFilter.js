import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getComboboxFilter } from 'commons/components/ComboboxFilter'

export default class MarginTrendChartTypeFilter extends Component {
  getData() {
    return [
      { key: 'CPE_FAMILY', value: 'CPE Family Breakdown' },
      { key: 'MARGIN_VS_GMV', value: 'Margin vs. GMV' }
    ];
  }

  render() {
    const ComboboxFilter = getComboboxFilter();
    return (
      <ComboboxFilter
        data={this.getData()}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        multiSelect={false}
        stateKey="selectedMarginTrendChartType"
        label="Chart Type"
      />
    );
  }
}

MarginTrendChartTypeFilter.propTypes = {
  onSelect: PropTypes.func,
  selectedData: PropTypes.any
};
