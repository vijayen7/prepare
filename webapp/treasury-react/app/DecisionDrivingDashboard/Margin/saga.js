import { put, takeEvery, call, all } from 'redux-saga/effects';
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from 'commons/constants';

import {
  FETCH_MARGIN_AND_COMPOSITION_BREAKDOWN,
  FETCH_MARGIN_DAY_ON_DAY_DIFF_DATA,
  FETCH_POSITIONS_PENALTY_DATA,
  FETCH_MARGIN_TREND_DATA
} from 'DecisionDrivingDashboard/constants';

import {
  getMarginAndCompositionBreakdown,
  getMarginDayOnDayDiffData,
  getPositionsPenaltyData,
  getMarginTrendData
} from './api';

function* fetchMarginAndCompositionBreakdown(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getMarginAndCompositionBreakdown, action.payload);
    yield put({ type: `${FETCH_MARGIN_AND_COMPOSITION_BREAKDOWN}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_MARGIN_AND_COMPOSITION_BREAKDOWN}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchMarginDayOnDayDiffData(action) {
  try {
    if (!action.isAsync) {
      yield put({ type: START_LOADING });
    }
    const data = yield call(getMarginDayOnDayDiffData, action.payload);
    yield put({ type: `${FETCH_MARGIN_DAY_ON_DAY_DIFF_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_MARGIN_DAY_ON_DAY_DIFF_DATA}_FAILURE` });
  } finally {
    if (!action.isAsync) {
      yield put({ type: END_LOADING });
    }
  }
}

function* fetchPositionsPenaltyData(action) {
  try {
    if (!action.isAsync) {
      yield put({ type: START_LOADING });
    }
    const data = yield call(getPositionsPenaltyData, action.payload);
    yield put({ type: `${FETCH_POSITIONS_PENALTY_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_POSITIONS_PENALTY_DATA}_FAILURE` });
  } finally {
    if (!action.isAsync) {
      yield put({ type: END_LOADING });
    }
  }
}

function* fetchMarginTrendData(action) {
  try {
    if (!action.isAsync) {
      yield put({ type: START_LOADING });
    }
    const data = yield call(getMarginTrendData, action.payload);
    yield put({ type: `${FETCH_MARGIN_TREND_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_MARGIN_TREND_DATA}_FAILURE` });
  } finally {
    if (!action.isAsync) {
      yield put({ type: END_LOADING });
    }
  }
}


export function* marginSaga() {
  yield all([
    takeEvery(FETCH_MARGIN_AND_COMPOSITION_BREAKDOWN, fetchMarginAndCompositionBreakdown),
    takeEvery(FETCH_MARGIN_DAY_ON_DAY_DIFF_DATA, fetchMarginDayOnDayDiffData),
    takeEvery(FETCH_POSITIONS_PENALTY_DATA, fetchPositionsPenaltyData),
    takeEvery(FETCH_MARGIN_TREND_DATA, fetchMarginTrendData)
  ]);
}
