import React, { Component } from 'react';
import Tab from 'commons/components/Tab';
import TabPanel from 'commons/components/TabPanel';
import Loader from 'commons/container/Loader';

import MarginCompositionSideBar from './Composition/containers/SideBar';
import DoDMarginChangesSideBar from './DoD/containers/SideBar';
import TopMarginPenaltiesSideBar from './TopPenalties/containers/SideBar';
import DataTrendsSideBar from './DataTrends/containers/SideBar';

import MarginCompositionMainContainer from './Composition/containers/MainContainer';
import DoDMarginChangesMainContainer from './DoD/containers/MainContainer';
import TopMarginPenaltiesMainContainer from './TopPenalties/containers/MainContainer';
import DataTrendsMainContainer from './DataTrends/containers/MainContainer';

export default class MarginDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedGadgetId: 'marginComposition'
    };
    this.handleGadgetChange = this.handleGadgetChange.bind(this);
  }

  getTabsList() {
    const tabs = [
      <Tab
        id="marginComposition"
        label="Margin Composition"
        isSelected={this.state.selectedGadgetId === 'marginComposition'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="dodMarginChanges"
        label="Day on Day Margin Changes"
        isSelected={this.state.selectedGadgetId === 'dodMarginChanges'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="topMarginPenalties"
        label="Top Margin Penalties"
        isSelected={this.state.selectedGadgetId === 'topMarginPenalties'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="dataTrends"
        label="Margin Trend"
        isSelected={this.state.selectedGadgetId === 'dataTrends'}
        onClick={this.handleGadgetChange}
      />
    ];
    return tabs;
  }
  handleGadgetChange(event) {
    this.setState({ selectedGadgetId: event.target.id });
  }

  getSideBar() {
    switch (this.state.selectedGadgetId) {
      case 'marginComposition':
        return <MarginCompositionSideBar />;
      case 'dodMarginChanges':
        return <DoDMarginChangesSideBar />;
      case 'topMarginPenalties':
        return <TopMarginPenaltiesSideBar />;
      case 'dataTrends':
        return <DataTrendsSideBar />;
      default:
        return <MarginCompositionSideBar />;
    }
  }

  getMainContainer() {
    switch (this.state.selectedGadgetId) {
      case 'marginComposition':
        return <MarginCompositionMainContainer />;
      case 'dodMarginChanges':
        return <DoDMarginChangesMainContainer />;
      case 'topMarginPenalties':
        return <TopMarginPenaltiesMainContainer />;
      case 'dataTrends':
        return <DataTrendsMainContainer />;
      default:
        return <MarginCompositionMainContainer />;
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="layout--flex">
          <div className="size--1">
            {this.getSideBar()}
          </div>
          <div className="size--5 padding--horizontal--double">
            <div className="layout--flex--row size--4">
              <Loader />
              <div className="size--content">
                <TabPanel
                  className="align--center"
                  tabs={this.getTabsList()}
                  onClick={this.handleGadgetChange}
                  selectedId={this.state.selectedGadgetId}
                />
                {this.getMainContainer()}
              </div>
              <br />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

}
