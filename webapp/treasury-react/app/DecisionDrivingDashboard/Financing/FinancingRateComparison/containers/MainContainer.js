import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import SliderPanel from 'commons/components/SliderPanel';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import { Layout } from 'arc-react-components'
import GridContainer from 'commons/components/GridWithCellClick';
import Grid from 'commons/components/Grid';
import Message from 'commons/components/Message';
import { gridColumns as financingRatesGridColumns, drillDownGridColumns as financingRatesDrillDownGridColumns } from '../grid/columnConfig';
import { gridOptions as financingRatesGridOptions, drillDownGridOptions as financingRatesDrillDownGridOptions } from '../grid/gridOptions';
import { saveFinancingRateComparisonContainerData } from '../../actions';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length ?
      this.props.containerData : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.financingRateComparisonData !== this.props.financingRateComparisonData) {
      this.setState({ value: nextProps.financingRateComparisonData.length, drillDownData:null });
    }
  }

  componentWillUnmount() {
    this.props.saveFinancingRateComparisonContainerData(this.state);
  }

  handleSliderChange(value) {
    this.setState({ value });
  }

  renderDrillDownGrid() {
    if(! (this.state.drillDownData != undefined && this.state.drillDownData.length > 0)) return;
    return (<Grid
      data={this.state.drillDownData}
      gridId="FinancingRateAssetClassDetail"
      gridColumns={financingRatesDrillDownGridColumns()}
      gridOptions={financingRatesDrillDownGridOptions()}
      label="Asset Class Detail"
      height={100}
      resizeCanvas={this.props.resizeCanvas}
    />);
  }

  onCellClickHandler = args => {
    if(args.colId == "groupingColumn")
      return;

    const payload = [];
    for(let c = 0 ; c< args.item.financingRatePerCpe.length;c++){
        if(args.item.financingRatePerCpe[c].cpeAbbrev === args.colId){
          var i =0;
          for (var key in args.item.financingRatePerCpe[c].assetClassesWithRateMap){
             payload[i] = { id: i,
                             cpeName: args.item.financingRatePerCpe[c].cpeName,
                             assetClassName: JSON.parse(key).displayName == undefined? "" : JSON.parse(key).displayName,
                             rates : args.item.financingRatePerCpe[c].assetClassesWithRateMap[key] }
              i++;
          }

        }
    }

    this.setState({ drillDown: true, drillDownData : payload });
  };

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  getDefaultContainerData() {
    const length = this.props.financingRateComparisonData !== undefined ? this.props.financingRateComparisonData.length : 0;
    return { view: 'grid', value: length };
  }

  /*
    Slices the data to Top N ( where N = value)
    */
  getGridData(data) {
    if (data && data instanceof Array) { return data; }
  }


  renderGrid() {
    return (<GridContainer
      data={this.getGridData(this.props.financingRateComparisonData)}
      gridId="FinancingRateComparison"
      onCellClick={this.onCellClickHandler}
      gridColumns={financingRatesGridColumns(this.props.financingRateComparisonData)}
      gridOptions={financingRatesGridOptions()}
      heightValue={600}
      resizeCanvas={this.props.resizeCanvas}
    />);
  }


  renderMainContainer() {
    if (this.props.financingRateComparisonData.length <= 0) {
      return <Message className="margin--top" messageData={this.props.searchMessage} />;
    }

    return (
      <React.Fragment>
          {this.renderGrid()}
          {<br/>}
          {this.renderDrillDownGrid()}

      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

MainContainer.defaultProps = {
  containerData: { view: 'grid', value: 20 },
  financingRateComparisonData: []
};

MainContainer.propTypes = {
  containerData: PropTypes.object,
  financingRateComparisonData: PropTypes.array,
  drillDownData: PropTypes.array,
  saveFinancingRateComparisonContainerData: PropTypes.func,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    containerData: state.decisionDrivingDashboard.financingView.financingRatesComparisonContainerData,
    financingRateComparisonData: state.decisionDrivingDashboard.financingView.financingRateComparisonData,
    searchMessage: state.decisionDrivingDashboard.financingView.searchMessage,
    resizeCanvas: state.decisionDrivingDashboard.financingView.resizeCanvas
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveFinancingRateComparisonContainerData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainContainer);
