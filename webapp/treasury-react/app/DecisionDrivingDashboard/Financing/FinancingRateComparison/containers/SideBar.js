import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Card } from "arc-react-components";
import DateFilter from 'commons/container/DateFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import LegalEntityFilter from 'commons/container/LegalEntityFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';
import FinancingRateFilter from "commons/container/FinancingRateTypeFilter";
import { getCodexFilters } from 'DecisionDrivingDashboard/util';
import {
  fetchFinancingRateComparisonData,
  destroyFinancingRateComparisonData,
  saveFinancingRateComparisonFilters,
  resizeCanvas
} from '../../actions';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.checkValidity = this.checkValidity.bind(this);
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  checkForInValidAgreementSelect(agreementValue) {
    if (agreementValue == "" || agreementValue == null ) {
      return false;
    }
    if (![2, 3, 5, 26].includes(parseInt(agreementValue.key))) {
      return true;
    }
    return false;
  }

  checkForInValidLegalEntitySelect(value) {
    if (value == "" || value == null) {
      return false
    }
    if (value.key == -1) {
      return true;
    }
    return false;
  }

  checkForInvalidCurrencySelect(currencyValue, currentState) {
    if (currencyValue != "") {
      return false;
    }
    return currentState;
  }

  checkForInvalidFinancingTypeSelect(rateType, currentState) {
    if (rateType != "") {
      return false;
    }
    return currentState;
  }

  updateAgreementTypeError(agreementMsg, agreementValue, currentState) {
    if (agreementMsg) {
      return true;
    }
    if (agreementValue == "") {
      return currentState;
    }
    return false;
  }

  updateLegalEntityError(legalEntityMsg, legalEntityValue, currentState) {
    if (legalEntityMsg) {
      return true;
    } if (legalEntityValue == "") {
      return currentState;
    }
    return false;
  }

  checkForInValidAgreement(agreementValue) {
    if (agreementValue == "" || agreementValue == null
    || ![2, 3, 5, 26].includes(parseInt(agreementValue.key))) {
      return true;
    }
    return false;
  }

  checkForInValidLegalEntity(value) {
    if (value == "" || value == null || value.key == -1) {
      return true;
    }
    return false;
  }

  checkForInvalidCurrency(value) {
    if (value == "" || value == null) {
      return true;
    }
    return false;
  }

  checkForInvalidFinancingType(value) {
    if (value == "" || value == null ) {
      return true;
    }
    return false;
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;

    newState['legalEntityErrorMsg'] = this.checkForInValidLegalEntitySelect(newState['selectedLegalEntities'])
    newState['agreementTypeErrorMsg'] = this.checkForInValidAgreementSelect(newState['selectedAgreementTypes'])
    newState['legalEntityError'] = this.updateLegalEntityError(newState['legalEntityErrorMsg'], newState['selectedLegalEntities'], newState['legalEntityError'])
    newState['agreementTypeError'] = this.updateAgreementTypeError(newState['agreementTypeErrorMsg'], newState['selectedAgreementTypes'], newState['agreementTypeError'])
    newState['currencyError'] = this.checkForInvalidCurrencySelect(newState['selectedCurrencies'], newState['currencyError'])
    newState['financingRateTypeError'] = this.checkForInvalidFinancingTypeSelect(newState['selectedFinancingRateTypes'], newState['financingRateTypeError'])


    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedDate: getCodexFilters('DATE'),
      multiSelect: false,
      selectedAgreementTypes: "",
      selectedLegalEntities: "",
      selectedCurrencies: "",
      selectedFinancingRateTypes: "",
      legalEntityError: false,
      agreementTypeError: false,
      financingRateTypeError: false,
      currencyError: false,
      legalEntityErrorMsg: false,
      agreementTypeErrorMsg: false
    };
  }

  checkValidity() {
    let isInvalid = true;
    let isInValidAgreement = false;
    let isInValidLegalEntity = false;
    let isInValidCurrency = false;
    let isInValidFinancingRateType = false;

    isInValidAgreement = this.checkForInValidAgreement(this.state.selectedAgreementTypes);
    isInValidLegalEntity = this.checkForInValidLegalEntity(this.state.selectedLegalEntities);
    isInValidCurrency = this.checkForInvalidCurrency(this.state.selectedCurrencies);
    isInValidFinancingRateType = this.checkForInvalidFinancingType(this.state.selectedFinancingRateTypes);

    isInvalid = (isInValidAgreement || isInValidLegalEntity || isInValidCurrency || isInValidFinancingRateType);

    this.setState({
      legalEntityError: isInValidLegalEntity,
      agreementTypeError: isInValidAgreement,
      currencyError: isInValidCurrency,
      financingRateTypeError: isInValidFinancingRateType
    })
    return isInvalid;
  }

  componentWillMount() {
    const state = Object.keys(this.props.filters).length ?
      this.props.filters : this.getDefaultFilters();
    this.setState(state);
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    if (!this.checkValidity()) {
      this.props.destroyFinancingRateComparisonData();
      const payload = {
        date: this.state.selectedDate,
        selectedLegalEntities: this.state.selectedLegalEntities.key,
        selectedAgreementTypes: this.state.selectedAgreementTypes.key,
        selectedCurrencies: this.state.selectedCurrencies.key,
        selectedFinancingRateTypes: this.state.selectedFinancingRateTypes.key
      };

      this.props.saveFinancingRateComparisonFilters(this.state);
      this.props.fetchFinancingRateComparisonData(payload);
    }
  }

  ErrorNote(error,message){
    if(error)
     return <span class="red margin--small">{message}</span>;
    else
     return;
  }

  render() {
    return (
      <Sidebar
        collapsible
        onToggle={this.props.onToggle}
        enterFunction={this.handleClick}
        id="financingRateComparisonSideBar"
        resizeCanvas={this.props.resizeCanvas}>
        <Card>
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedDate"
            data={this.state.selectedDate}
          />
        </Card>
        <Card>
          <AgreementTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedAgreementTypes}
            multiSelect={this.state.multiSelect}
            errorDiv={this.state.agreementTypeError}
          />
          {this.ErrorNote(this.state.agreementTypeErrorMsg, "Valid Agreement Types are FOC,FOC-OTC,PB and PB-FI")}
          {this.ErrorNote(this.state.agreementTypeError, "Please select valid agreement type")}
          <LegalEntityFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntities}
            multiSelect={this.state.multiSelect}
            errorDiv={this.state.legalEntityError}
          />
          {this.ErrorNote(this.state.legalEntityErrorMsg, "Rates comparison can be done for one legal entity at a time")}
          {this.ErrorNote(this.state.legalEntityError, "Please select legal entity")}
          <CurrencyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCurrencies}
            multiSelect={this.state.multiSelect}
            errorDiv={this.state.currencyError}
          />
          {this.ErrorNote(this.state.currencyError, "Please select currency")}
          <FinancingRateFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedFinancingRateTypes}
            multiSelect= {this.state.multiSelect}
            errorDiv={this.state.financingRateTypeError}
          />
          {this.ErrorNote(this.state.financingRateTypeError, "Please select financing rate type")}
        </Card>
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
         />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.defaultProps = {
  filters: {}
};

SideBar.propTypes = {
  fetchFinancingRateComparisonData: PropTypes.func,
  destroyFinancingRateComparisonData: PropTypes.func,
  saveFinancingRateComparisonFilters: PropTypes.func,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.financingView.financingRateComparisonFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchFinancingRateComparisonData,
      destroyFinancingRateComparisonData,
      saveFinancingRateComparisonFilters,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
