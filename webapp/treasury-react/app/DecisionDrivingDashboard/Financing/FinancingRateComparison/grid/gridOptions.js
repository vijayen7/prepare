import { url } from "../../api";
export function gridOptions(paging) {
  var groupColumns = ['sfsTypeName'];
  const options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: {
      exportCurrentView : true
    },
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    },
    sortList: [{ columnId: 'sfsTypeName', sortAsc: true },{ columnId: 'subtypeName', sortAsc: true }],
    page: true,
    onRowClick: function () {// highlight on row works if this callback is defined
    },
    highlightRowOnClick: true,
    enableMultilevelGrouping: {
      hideGroupingHeader: false,
      showGroupingKeyInColumn: false,
      initialGrouping: groupColumns,
    },
    expandCollapseAll: true
  };
  return options;
}

export function drillDownGridOptions(paging) {

  const options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    highlightRowOnClick: true,
    maxHeight: 150,
  };
  return options;
}


