import {
  percentFormatter,
  priceFormatter,
  drillThroughFormatter,
  numberTotalsFormatter,
  percentTotalsFormatter,
  excelTotalsFormatter,
  marketDataPositionTypeFormatter,
  linkFormatter
} from 'commons/grid/formatters';

import { financingRateAggregator } from "commons/grid/aggregators";

import { getCommaSeparatedValues } from '../../../../commons/util';

import {
  textComparator,
  numberComparator
} from 'commons/grid/comparators';

const columns = [];

function getCpeList(params) {
  if (params == undefined) {
    return [];
  }
  const cpeNames = [];
  for (let i = 0; i < params.length; i++) {
    
    for (let j = 0; j < params[i].financingRatePerCpe.length; j++) {
      if (cpeNames.indexOf(params[i].financingRatePerCpe[j].cpeAbbrev) == -1) {
        cpeNames.push(params[i].financingRatePerCpe[j].cpeAbbrev);
      }
    }
  }
  cpeNames.sort();
  return cpeNames;
}

function isAveragedOrAssetClassRate(map){
 return (Object.keys(map).length > 1 || Object.values(map)[0].length > 1
 || JSON.parse(Object.keys(map)[0]).assetClassGroupId != undefined)
}

function getDynamicColumns(params) {
  const dynamic = [];
  const cpeNames = getCpeList(params);
  if(cpeNames[0] == undefined) return [];
  for (const name in cpeNames) {
    dynamic.push({
      id: cpeNames[name],
      name: `${cpeNames[name].toUpperCase()}`,
      field: 'financingRatePerCpe',
      toolTip: cpeNames[name],
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 65,
      excelFormatter: '#,##0.0',
      groupingAggregator: financingRateAggregator(cpeNames[name]),
      groupTotalsFormatter: function(totals, columnDef, placeHolder, isExportToExcel) {
        return isExportToExcel ? totals.avg[columnDef.field + columnDef.id] : priceFormatter(null, null, totals.avg[columnDef.field + columnDef.id], columnDef, null);
      },
      formatter : function(row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null) {
          for (let j = 0; j < value.length; j++) {
            if (value[j].cpeAbbrev == columnDef.id) {
              var formattedValue = priceFormatter(row, cell, value[j].rate, columnDef, dataContext); 
              if(value[j].assetClassesWithRateMap != undefined && isAveragedOrAssetClassRate(value[j].assetClassesWithRateMap))
              {
                return linkFormatter(
                  row,
                  cell,
                  formattedValue,
                  columnDef,
                  dataContext
                );
              }
              return formattedValue;
            
              }
             
            }
          } else return null;
        },
      excelDataFormatter : function(value, rowElem, currentGrid, isCurrentViewBeingExported, columnDef) {  
        if (value !== undefined && value !== null) {
          for (let j = 0; j < value.length; j++) {
            if (value[j].cpeAbbrev == columnDef.id) {
              return value[j].rate
            }
          }
        } else return null;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      comparator : customComparator(cpeNames[name])
     
    });
  }

  return dynamic;
}

function customComparator(cpe) {
 
  return function (a, b) {
    if((a == undefined || a["@CLASS"] == undefined ) && (b == undefined || b["@CLASS"] == undefined ) )
      return numberComparator(a,b)
    var aValue;
    var bValue;
    for (let j = 0; j < a[sortcol].length || j < b[sortcol].length; j++) {
      if (a[sortcol][j] != undefined && a[sortcol][j].cpeAbbrev == cpe) {
        aValue =  a[sortcol][j].rate
      }
      if (b[sortcol][j] != undefined && b[sortcol][j].cpeAbbrev == cpe) {
        bValue =  b[sortcol][j].rate
      }
    }
    return numberComparator(aValue, bValue);
  }
}

export function gridColumns(params) {
  let columns = [{
    id: 'sfsTypeName',
    name: 'SFS Type',
    field: 'sfsTypeName',
    toolTip: 'SFS Type',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 65
  },
  {
    id: 'subtypeName',
    name: 'Security SubType',
    field: 'subtypeName',
    toolTip: 'Security SubType',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 120
  }];
  columns = columns.concat(getDynamicColumns(params));

  return columns;
}

export function drillDownGridColumns(params) {
  let columns = [{
    id: 'cpeName',
    name: 'CounterParty',
    field: 'cpeName',
    toolTip: 'CounterParty',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 65
  },
  {
    id: 'assetClassName',
    name: 'Asset Class',
    field: 'assetClassName',
    toolTip: 'Asset Class',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60
  },
  {
    id: 'rates',
    name: 'Rates Used for Average',
    field: 'rates',
    toolTip: 'Rates Used for Average',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 60,
    excelDataFormatter : function(value, rowElem, currentGrid, isCurrentViewBeingExported, columnDef) {
      return value.join(",");
    }
  }];
  

  return columns;
}
