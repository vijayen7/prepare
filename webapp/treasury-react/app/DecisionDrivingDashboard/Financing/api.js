import { BASE_URL } from 'commons/constants';
export let url = '';

export function getUnsecuredNetDebitDetails(payload) {
  const { date, selectedLegalEntityFamilies, selectedCpeFamilies, financingCostThreshold, reportingCurrencyId } = payload;
  const paramString = `date=${date}&legalEntityFamilyIds=${selectedLegalEntityFamilies}&cpeFamilyIds=${selectedCpeFamilies}&ccySpns=__null__&financingCostThreshold=${financingCostThreshold}&reportingCurrencyId=${reportingCurrencyId}`;
  url = `${BASE_URL}service/unsecuredNetDebitDetailService/getUnsecuredNetDebitDetails?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getOffsettingNetDebitDetails(payload) {
  const { date, selectedLegalEntityFamilies, selectedCpeFamilies, boxedBalanceThreshold, reportingCurrencyId } = payload;
  const paramString = `date=${date}&legalEntityFamilyIds=${selectedLegalEntityFamilies}&cpeFamilyIds=${selectedCpeFamilies}&thresholdBoxBalance=${boxedBalanceThreshold}&reportingCurrencyId=${reportingCurrencyId}`;
  url = `${BASE_URL}service/offsettingNetDebitDetail/getOffsettingNetDebitDetails?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getFinancingRateComparisonData(payload) {
  const { date, selectedLegalEntities, selectedAgreementTypes,selectedCurrencies,selectedFinancingRateTypes } = payload;
  const paramString = `date=${date}&descoEntityId=${selectedLegalEntities}&currencyId=${selectedCurrencies}&agreementTypeId=${selectedAgreementTypes}&rateTypes=${selectedFinancingRateTypes}`;
  url = `${BASE_URL}service/financingRateComparisonService/getFinancingRates?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getFinancingChargeTrendData(payload) {
  const { startDate, endDate, cpeFamilyIds, legalEntityFamilyIds, agreementTypeIds, businessUnitIds, ccySpns, financingChargeType, breakdownType, reportingCurrencyId } = payload;
  const paramString = `startDate=${startDate}&endDate=${endDate}&cpeFamilyIds=${cpeFamilyIds}&legalEntityFamilyIds=${legalEntityFamilyIds}&agreementTypeIds=${agreementTypeIds}&businessUnitIds=${businessUnitIds}&ccySpns=${ccySpns}&financingChargeType=${financingChargeType}&breakdownType=${breakdownType}&reportingCurrencyId=${reportingCurrencyId}`
  url = `${BASE_URL}service/financingChargeTrendService/getFinancingChargeTrendData?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}
