import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { URL } from 'commons/constants';
import SliderPanel from 'commons/components/SliderPanel';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import GridContainer from 'commons/components/Grid';
import BarChart from '../components/BarChart';
import Message from 'commons/components/Message';
import { getOrgIdsFromOrgFamilyIds, checkReportingFxNullOrNot, reportingCurrencyMessage } from 'DecisionDrivingDashboard/util';
import { gridColumns as offsettingNetDebitGridColumns } from '../grid/columnConfig';
import { gridOptions as offsettingNetDebitGridOptions } from '../grid/gridOptions';
import { saveOffsettingNetDebitContainerData } from '../../actions';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.checkReportingFxNullOrNot = checkReportingFxNullOrNot.bind(this);
    this.reportingCurrencyMessage = reportingCurrencyMessage.bind(this);
    this.getPlotData = this.getPlotData.bind(this);
    this.boxedDebitComparator = this.boxedDebitComparator.bind(this);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length ?
      this.props.containerData : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.offsettingNetDebitDetailData !== this.props.offsettingNetDebitDetailData) {
      this.setState({ value: Math.min(nextProps.offsettingNetDebitDetailData.length, 20) });
    }
  }

  componentWillUnmount() {
    this.props.saveOffsettingNetDebitContainerData(this.state);
  }

  handleSliderChange(value) {
    this.setState({ value });
  }

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  getDefaultContainerData() {
    const length = this.props.offsettingNetDebitDetailData !== undefined ?
      Math.min(this.props.offsettingNetDebitDetailData.length, 20) : 0;
    return { view: 'grid', value: length };
  }

  /*
    Slices the data to Top N ( where N = value)
    */
  getGridData(data) {
    if (data && data instanceof Array) { return data.sort(this.boxedDebitComparator).slice(0, this.state.value); }
  }

  boxedDebitComparator(a, b) {
    return Math.abs(b.boxedDebit) - Math.abs(a.boxedDebit);
  }

  getPlotData() {
    const plotData = [];
    const data = this.props.offsettingNetDebitDetailData;
    const sortedByBoxedBalanceDesc = data.sort(this.boxedDebitComparator);
    const isReportingFxNull = this.checkReportingFxNullOrNot(data);
    for (let i = 0; i < Math.min(data.length, this.state.value); i++) {
      const { id, debitBalance, creditBalance, debitBalanceRC, creditBalanceRC } = sortedByBoxedBalanceDesc[i];
      plotData.push({
        name: id,
        'Debit Balance': isReportingFxNull ? Math.abs(debitBalance) : Math.abs(debitBalanceRC),
        'Credit Balance': isReportingFxNull ? creditBalance : creditBalanceRC,
      });
    }
    return plotData;
  }

  renderSliderPanel() {
    return (<SliderPanel
      min={1}
      max={this.props.offsettingNetDebitDetailData.length}
      value={this.state.value}
      view={this.state.view}
      onSliderChange={this.handleSliderChange}
      toggleOptions={[
        { key: 'grid', value: 'Grid' },
        { key: 'chart', value: 'Chart' }
      ]}
      onToggleGroupChange={this.handleToggleGroupChange}
    />);
  }

  renderLinkPanel() {
    let cpeIds = getOrgIdsFromOrgFamilyIds(this.props.filters.selectedCpeFamilies, this.props.cpeFamilyToCpeData);
    let leIds = getOrgIdsFromOrgFamilyIds(this.props.filters.selectedLegalEntityFamilies, this.props.legalEntityFamilyToLegalEntityData);
    var filters = {
      'selectedCpes': cpeIds,
      'selectedLegalEntities': leIds,
       fromDate:this.props.filters.selectedDate,
       toDate:this.props.filters.selectedDate,
    };
    return (
      <LinkPanel
        links={[
          <Link
            id="interest-breakdown"
            text="View Detailed Interest Breakdown"
            href={`${URL}/treasury/detailReport.html#${JSON.stringify(filters)}`}
          />
        ]}
      />
    );
  }

  renderGrid() {
    return (<GridContainer
      data={this.getGridData(this.props.offsettingNetDebitDetailData)}
      gridId="OffsettingNetDebitDetailData"
      gridColumns={offsettingNetDebitGridColumns()}
      gridOptions={offsettingNetDebitGridOptions()}
      resizeCanvas={this.props.resizeCanvas}
    />);
  }

  renderChart() {
    return <BarChart getPlotData={this.getPlotData} baseData={this.props.offsettingNetDebitDetailData} checkReportingFxNullOrNot={this.checkReportingFxNullOrNot} />;
  }

  renderMainContainer() {
    let data = this.props.offsettingNetDebitDetailData;
    if (data.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderSliderPanel()}
        {(this.state.view === 'grid') ? this.renderGrid() : this.renderChart()}
        {this.reportingCurrencyMessage(data)}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

MainContainer.defaultProps = {
  containerData: { view: 'grid', value: 5 },
  offsettingNetDebitDetailData: []
};

MainContainer.propTypes = {
  containerData: PropTypes.object,
  offsettingNetDebitDetailData: PropTypes.array,
  saveOffsettingNetDebitContainerData: PropTypes.func,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    containerData: state.decisionDrivingDashboard.financingView.offsettingNetDebitContainerData,
    offsettingNetDebitDetailData: state.decisionDrivingDashboard.financingView.offsettingNetDebitDetailData,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    filters: state.decisionDrivingDashboard.financingView.offsettingNetDebitFilters,
    resizeCanvas: state.decisionDrivingDashboard.financingView.resizeCanvas
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveOffsettingNetDebitContainerData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainContainer);
