import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Card } from 'arc-react-components';
import DateFilter from 'commons/container/DateFilter';
import LegalEntityFamilyFilter from 'commons/container/LegalEntityFamilyFilter';
import CpeFamilyFilter from 'commons/container/CpeFamilyFilter';
import InputFilter from 'commons/components/InputFilter';
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';
import { getCodexFilters } from 'DecisionDrivingDashboard/util';
import { getCommaSeparatedValuesOrNull, getCommaSeparatedValuesOrNullForSingleSelect } from '../../../../commons/util';
import {
  fetchOffsettingNetDebitDetailData,
  destroyOffsettingNetDebitDetailData,
  saveOffsettingNetDebitFilters,
  resizeCanvas
} from '../../actions';

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  componentWillMount() {
    const state = this.props.filters;
    if (Object.keys(state).length === 0) {
      this.setState(this.getDefaultFilters());
      this.props.saveOffsettingNetDebitFilters(this.getDefaultFilters());
    }
    this.setState(state);
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedDate: getCodexFilters('DATE'),
      selectedLegalEntityFamilies: getCodexFilters('LEGAL_ENTITY_FAMILIES'),
      selectedCpeFamilies: getCodexFilters('CPE_FAMILIES'),
      boxedBalanceThreshold: getCodexFilters('BOXED_BALANCE_THRESHOLD'),
      selectedReportingCurrency: getCodexFilters('REPORTING_CURRENCY_ID')
    };
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    this.props.destroyOffsettingNetDebitDetailData();
    const payload = {
      date: this.state.selectedDate,
      selectedLegalEntityFamilies: getCommaSeparatedValuesOrNull(this.state.selectedLegalEntityFamilies),
      selectedCpeFamilies: getCommaSeparatedValuesOrNull(this.state.selectedCpeFamilies),
      boxedBalanceThreshold: this.state.boxedBalanceThreshold || 0,
      reportingCurrencyId: getCommaSeparatedValuesOrNullForSingleSelect(this.state.selectedReportingCurrency)
    };
    this.props.saveOffsettingNetDebitFilters(this.state);
    this.props.fetchOffsettingNetDebitDetailData(payload);
  }

  render() {
    return (
      <Sidebar
        collapsible
        onToggle={this.props.onToggle}
        enterFunction={this.handleClick}
        id="offsettingNetDebitSideBar"
        resizeCanvas={this.props.resizeCanvas}>
        <Card>
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedDate"
            data={this.state.selectedDate}
          />
        </Card>
        <Card>
          <CpeFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCpeFamilies}
          />
          <LegalEntityFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntityFamilies}
          />
        </Card>
        <Card>
          <InputFilter
            data={this.state.boxedBalanceThreshold}
            onSelect={this.onSelect}
            stateKey="boxedBalanceThreshold"
            label="Boxed Balance Threshold ($)"
          />
        </Card>
        <Card>
          <ReportingCurrencyFilter
            label="Reporting Currency"
            onSelect={this.onSelect}
            selectedData={this.state.selectedReportingCurrency}
            multiSelect={false}
            stateKey="selectedReportingCurrency"
          />
        </Card>
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.defaultProps = {
  filters: {}
};

SideBar.propTypes = {
  fetchOffsettingNetDebitDetailData: PropTypes.func,
  destroyOffsettingNetDebitDetailData: PropTypes.func,
  saveOffsettingNetDebitFilters: PropTypes.func,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.financingView.offsettingNetDebitFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchOffsettingNetDebitDetailData,
      destroyOffsettingNetDebitDetailData,
      saveOffsettingNetDebitFilters,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
