import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MixBarChart from 'commons/components/MixBarChart';
import { formatNumberAsMoneyWithoutPrefix } from 'commons/util';
import CustomizedAxisTick from './CustomizedAxisTick';
import BarChartTooltip from './BarChartTooltip';
import {
  XAxis,
  YAxis,
  Legend,
  Tooltip
} from 'recharts';

export default class BarChart extends Component {
  constructor(props) {
    super(props);
  }

  getStackId() {
    return {
      'Debit Balance': 'a',
      'Credit Balance': 'b'
    };
  }

  render() {
    const yLabel = this.props.checkReportingFxNullOrNot(this.props.baseData) ? '(in USD)' : '(in RC)';
    return (
      <MixBarChart
        getPlotData={this.props.getPlotData}
        stackId={this.getStackId()}
        tooltip={<Tooltip
          content={<BarChartTooltip baseData={this.props.baseData} />}
          cursor={false}
        />}
        legend={
          <Legend wrapperStyle={{ paddingTop: '80px' }} />
        }
        xAxis={
          <XAxis
            dataKey="name"
            interval={0}
            tick={<CustomizedAxisTick baseData={this.props.baseData} />}
            label={{ value: 'Currency ; Legal Entity ;', dy: 60, fill: '#b9b9b9' }}
          />
        }
        yAxis={
          <YAxis
            stroke="#85BDBF"
            label={{ value: `Offsetting Net Debit Amount ${yLabel}`, angle: -90, dx: -50, fill: '#b9b9b9' }}
            tickFormatter={formatNumberAsMoneyWithoutPrefix}
          />
        }
        stacked
      />
    );
  }
}

BarChart.propTypes = {
  getPlotData: PropTypes.func.isRequired,
  baseData: PropTypes.array.isRequired,
  checkReportingFxNullOrNot : PropTypes.func.isRequired
};
