import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumberAsMoneyWithoutPrefix } from 'commons/util';

export default class BarChartTooltip extends Component {
  constructor(props) {
    super(props);
  }

  getViewBoxStyle() {
    return {
      background: '#EBF1F5',
      borderColor: '#394B59',
      padding: '10px',
      textAlign: 'left'
    };
  }

  render() {
    const { active } = this.props;
    if (active) {
      const { payload, baseData } = this.props;
      const obj = baseData.find((o) => o.id === payload[0].payload.name);
      return (
        <div className="custom-tooltip" style={this.getViewBoxStyle()}>
          <b style={{ color: '#008075' }}>
            {obj.legalEntityName}
            {` (${obj.currencyCode})`}
          </b>

          <p style={{ color: payload[0].fill }}>
            {'Debit Balance : '}
            {formatNumberAsMoneyWithoutPrefix(payload[0].value)}
          </p>

          <p style={{ color: payload[1].fill }}>
            {'Credit Balance : '}
            {formatNumberAsMoneyWithoutPrefix(payload[1].value)}
          </p>
        </div>
      );
    }

    return null;
  }
}

BarChartTooltip.propTypes = {
  active: PropTypes.bool,
  baseData: PropTypes.array.isRequired,
  payload: PropTypes.array
};
