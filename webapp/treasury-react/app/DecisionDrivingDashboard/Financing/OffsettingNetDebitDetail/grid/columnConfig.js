export function gridColumns() {
  const columns = [
    {
      id: 'legalEntity',
      name: 'Legal Entity',
      field: 'legalEntityName',
      toolTip: 'Legal Entity',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 80
    },
    {
      id: 'currencyCode',
      name: 'Currency',
      field: 'currencyCode',
      toolTip: 'Currency',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 80
    },
    {
      id: 'debitBalance',
      name: 'Debit Balance (USD)',
      field: 'debitBalance',
      toolTip: 'Debit Balance (USD)',
      type: 'number',
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: 'aln-rt b',
      width: 80
    },
    {
      id: 'debitBalanceRC',
      name: 'Debit Balance (RC)',
      field: 'debitBalanceRC',
      toolTip: 'Debit Balance (RC)',
      type: 'number',
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: 'aln-rt b',
      width: 80
    },
    {
      id: 'creditBalance',
      name: 'Credit Balance (USD)',
      field: 'creditBalance',
      toolTip: 'Credit Balance (USD)',
      type: 'number',
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: 'aln-rt b',
      width: 80
    },
    {
      id: 'creditBalanceRC',
      name: 'Credit Balance (RC)',
      field: 'creditBalanceRC',
      toolTip: 'Credit Balance (RC)',
      type: 'number',
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: 'aln-rt b',
      width: 80
    },
    {
      id: 'boxedDebit',
      name: 'Boxed Debit Balance (USD)',
      field: 'boxedDebit',
      toolTip: 'Boxed Debit Balance (USD)',
      type: 'number',
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: 'aln-rt b',
      width: 50
    },
    {
      id: 'boxedDebitRC',
      name: 'Boxed Debit Balance (RC)',
      field: 'boxedDebitRC',
      toolTip: 'Boxed Debit Balance (RC)',
      type: 'number',
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: 'aln-rt b',
      width: 50
    }
  ];

  return columns;
}
