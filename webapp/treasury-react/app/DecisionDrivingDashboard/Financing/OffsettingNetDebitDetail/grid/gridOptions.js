import { url } from "../../api";
export function gridOptions(paging) {
  const options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [{ columnId: 'boxedDebit', sortAsc: false }],
    page: true,
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    maxHeight: 600,
    summaryRowText: 'Total',
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    }
  };
  return options;
}
