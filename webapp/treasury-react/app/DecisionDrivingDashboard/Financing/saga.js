import { put, takeEvery, call, all } from 'redux-saga/effects';
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from 'commons/constants';

import {
  FETCH_UNSECURED_NET_DEBIT_DETAIL_DATA,
  FETCH_OFFSETTING_NET_DEBIT_DETAIL_DATA,
  FETCH_FINANCING_CHARGE_TREND_DATA,
  FETCH_FINANCING_RATE_COMPARISON_DATA,
  UPDATE_SEARCH_MESSAGE
} from 'DecisionDrivingDashboard/constants';

import {
  getUnsecuredNetDebitDetails,
  getOffsettingNetDebitDetails,
  getFinancingChargeTrendData,
  getFinancingRateComparisonData
} from './api';


function* fetchUnsecuredNetDebitDetailData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getUnsecuredNetDebitDetails, action.payload);
    yield put({ type: `${FETCH_UNSECURED_NET_DEBIT_DETAIL_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_UNSECURED_NET_DEBIT_DETAIL_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}
function* fetchFinancingRateComparisonData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getFinancingRateComparisonData, action.payload);
    yield put({ type: `${FETCH_FINANCING_RATE_COMPARISON_DATA}_SUCCESS`, data });
    yield put({ type: UPDATE_SEARCH_MESSAGE, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_FINANCING_RATE_COMPARISON_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchOffsettingNetDebitDetailData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getOffsettingNetDebitDetails, action.payload);
    yield put({ type: `${FETCH_OFFSETTING_NET_DEBIT_DETAIL_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_OFFSETTING_NET_DEBIT_DETAIL_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchFinancingChargeTrendData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getFinancingChargeTrendData, action.payload);
    yield put({ type: `${FETCH_FINANCING_CHARGE_TREND_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_FINANCING_CHARGE_TREND_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}


export function* financingSaga() {
  yield all([
    takeEvery(FETCH_UNSECURED_NET_DEBIT_DETAIL_DATA, fetchUnsecuredNetDebitDetailData),
    takeEvery(FETCH_OFFSETTING_NET_DEBIT_DETAIL_DATA, fetchOffsettingNetDebitDetailData),
    takeEvery(FETCH_FINANCING_CHARGE_TREND_DATA, fetchFinancingChargeTrendData),
    takeEvery(FETCH_FINANCING_RATE_COMPARISON_DATA, fetchFinancingRateComparisonData)
  ]);
}
