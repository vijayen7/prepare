import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { URL } from 'commons/constants';
import DataTrendsChart from 'commons/components/DataTrendsChart';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import Message from 'commons/components/Message';
import { getOrgIdsFromOrgFamilyIds, checkReportingFxNullOrNot, reportingCurrencyMessage } from 'DecisionDrivingDashboard/util';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.checkReportingFxNullOrNot = checkReportingFxNullOrNot.bind(this);
    this.reportingCurrencyMessage = reportingCurrencyMessage.bind(this);
    this.getPlotData = this.getPlotData.bind(this);
  }

  getPlotData() {
    const plotData = [];
    const data = this.props.financingChargeTrendData;

    for (let i = 0; i < data.length; i++) {
      let element = data[i], plotDataElement = {};

      plotDataElement.name = parseInt(element.date.match(/\((.*)\)/)[1]);
      for (const constituent in element.constituents) {
        plotDataElement[constituent] = element.constituents[constituent];
      }
      plotData.push(plotDataElement);
    }

    plotData.sort((a, b) => a.name - b.name);
    return plotData;
  }

  renderLinkPanel() {
    let cpeIds = getOrgIdsFromOrgFamilyIds(this.props.filters.selectedCpeFamilies, this.props.cpeFamilyToCpeData);
    let leIds = getOrgIdsFromOrgFamilyIds(this.props.filters.selectedLegalEntityFamilies, this.props.legalEntityFamilyToLegalEntityData);
    var filters = {
      'selectedCpes': cpeIds,
      'selectedLegalEntities': leIds,
       fromDate:this.props.filters.selectedStartDate,
       toDate:this.props.filters.selectedEndDate,
    };
    return (
      <LinkPanel
        links={[
          <Link
            id="interest-breakdown"
            text="View Detailed Interest Breakdown"
            href={`${URL}/treasury/detailReport.html#${JSON.stringify(filters)}`}
          />
        ]}
      />
    );
  }

  renderChart() {
    const chartType = this.props.filters.selectedFinancingChargeBreakdownType.key;
    const financingChargeType = this.props.filters.selectedFinancingChargeType.value || 'Total Financing';
    const currencyLabel = this.checkReportingFxNullOrNot(this.props.financingChargeTrendData) ? '(in USD)' : '(in RC)';
    if (chartType === 'FINANCING_VS_GMV') {
      return (
        <DataTrendsChart
          data={this.getPlotData()}
          xAxis={{ dataKey: 'name', label: 'Date' }}
          yAxis={{
            left: { label: `${financingChargeType} ${currencyLabel}` },
            right: { label: `Total GMV ${currencyLabel}` }
          }}
          rightYAxisDataKeys={['Total GMV']}
        />
      );
    }

    return (
      <DataTrendsChart
        data={this.getPlotData()}
        xAxis={{ dataKey: 'name', label: 'Date' }}
        yAxis={{ left: { label: `${financingChargeType} ${currencyLabel}` } }}
      />
    );
  }

  renderMainContainer() {
    const { financingChargeTrendData } = this.props;

    if (financingChargeTrendData === null) {
      return <Message className="margin--top" messageData="Search to load data." />;
    }

    if (financingChargeTrendData.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderChart()}
        {this.reportingCurrencyMessage(financingChargeTrendData)}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

function mapStateToProps(state) {
  return {
    financingChargeTrendData: state.decisionDrivingDashboard.financingView.financingChargeTrendData,
    filters: state.decisionDrivingDashboard.financingView.financingChargeTrendFilters,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    resizeCanvas: state.decisionDrivingDashboard.financingView.resizeCanvas
  };
}

MainContainer.propTypes = {
  financingChargeTrendData: PropTypes.array,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object
};

export default connect(
  mapStateToProps,
  null
)(MainContainer);
