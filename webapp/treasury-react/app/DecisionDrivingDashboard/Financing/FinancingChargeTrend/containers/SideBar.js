import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Card } from "arc-react-components";
import DateFilter from 'commons/container/DateFilter';
import LegalEntityFamilyFilter from 'commons/container/LegalEntityFamilyFilter';
import CpeFamilyFilter from 'commons/container/CpeFamilyFilter';
import AgreementTypeFilter from 'commons/container/AgreementTypeFilter';
import BusinessUnitFilter from 'commons/container/BusinessUnitFilter';
import CurrencyFilter from 'commons/container/CurrencyFilter';
import FinancingChargeTypeFilter from 'commons/container/FinancingChargeTypeFilter';
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import Sidebar from 'commons/components/Sidebar';
import ColumnLayout from 'commons/components/ColumnLayout';
import FilterButton from 'commons/components/FilterButton';
import { getCommaSeparatedValuesOrNull, getCommaSeparatedValuesOrNullForSingleSelect } from 'commons/util';
import { getCodexFilters } from 'DecisionDrivingDashboard/util';
import {
  fetchFinancingChargeTrendData,
  destroyFinancingChargeTrendData,
  saveFinancingChargeTrendFilters,
  resizeCanvas
} from '../../actions';
import FinancingChargeBreakdownTypeFilter from '../components/FinancingChargeBreakdownTypeFilter';


class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);

    this.state = {};
  }

  componentWillMount() {
    const state = this.props.filters;
    if (Object.keys(state).length === 0) {
      this.setState(this.getDefaultFilters());
      this.props.saveFinancingChargeTrendFilters(this.getDefaultFilters());
    }
    this.setState(state);

  }

  componentDidMount() {
    console.log("node");
    console.log(this.sidebarRef);
  }

  onSelect(params) {
    let {key, value} = params;
    this.setState((previousState) => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  getDefaultFilters() {
    const CPE_FAMILY = { key: 'CPE_FAMILY', value: 'CPE Family Breakdown' };
    const TOTAL = { key: 'TOTAL', value: 'TOTAL FINANCING' };
    return {
      selectedStartDate: getCodexFilters('FINANCING_TREND_START_DATE'),
      selectedEndDate: getCodexFilters('TREND_END_DATE'),
      selectedCpeFamilies: getCodexFilters('CPE_FAMILIES'),
      selectedLegalEntityFamilies: getCodexFilters('LEGAL_ENTITY_FAMILIES'),
      selectedAgreementTypes: getCodexFilters('SIMULATION_AGREEMENT_TYPES'),
      selectedBusinessUnits: getCodexFilters('BUSINESS_UNIT'),
      selectedCurrencies: getCodexFilters('CURRENCIES'),
      selectedFinancingChargeType: TOTAL,
      selectedFinancingChargeBreakdownType: CPE_FAMILY,
      selectedReportingCurrency: getCodexFilters('REPORTING_CURRENCY_ID')
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    this.props.destroyFinancingChargeTrendData();
    const payload = {
      startDate: this.state.selectedStartDate,
      endDate: this.state.selectedEndDate,
      cpeFamilyIds: getCommaSeparatedValuesOrNull(this.state.selectedCpeFamilies),
      legalEntityFamilyIds: getCommaSeparatedValuesOrNull(this.state.selectedLegalEntityFamilies),
      agreementTypeIds: getCommaSeparatedValuesOrNull(this.state.selectedAgreementTypes),
      businessUnitIds: getCommaSeparatedValuesOrNull(this.state.selectedBusinessUnits),
      ccySpns: getCommaSeparatedValuesOrNull(this.state.selectedCurrencies),
      financingChargeType: getCommaSeparatedValuesOrNull([this.state.selectedFinancingChargeType]),
      breakdownType: getCommaSeparatedValuesOrNull([this.state.selectedFinancingChargeBreakdownType]),
      reportingCurrencyId: getCommaSeparatedValuesOrNullForSingleSelect(this.state.selectedReportingCurrency)
    };
    this.props.saveFinancingChargeTrendFilters(this.state);
    this.props.fetchFinancingChargeTrendData(payload);
  }

  render() {
    return (

      <Sidebar
        collapsible
        onToggle={this.props.onToggle}
        enterFunction={this.handleClick}
        id="financingChargeTrendSidebar"
        resizeCanvas={this.props.resizeCanvas}>
        <Card>
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedStartDate"
            data={this.state.selectedStartDate}
            label="Start Date"
            className="padding--horizontal"
          />
          <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedEndDate"
            data={this.state.selectedEndDate}
            label="End Date"
            className="margin"
          />
        </Card>
        <Card>
          <CpeFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCpeFamilies}
          />
          <LegalEntityFamilyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedLegalEntityFamilies}
          />
          <AgreementTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedAgreementTypes}
          />
          <BusinessUnitFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedBusinessUnits}
          />
          <CurrencyFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCurrencies}
          />
        </Card>
        <Card>
          <FinancingChargeTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedFinancingChargeType}
          />
          <FinancingChargeBreakdownTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedFinancingChargeBreakdownType}
          />
        </Card>
        <Card>
          <ReportingCurrencyFilter
            label="Reporting Currency"
            onSelect={this.onSelect}
            selectedData={this.state.selectedReportingCurrency}
            multiSelect={false}
            stateKey="selectedReportingCurrency"
          />
        </Card>
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

SideBar.propTypes = {
  fetchFinancingChargeTrendData: PropTypes.func,
  destroyFinancingChargeTrendData: PropTypes.func,
  saveFinancingChargeTrendFilters: PropTypes.func,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    filters: state.decisionDrivingDashboard.financingView.financingChargeTrendFilters
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchFinancingChargeTrendData,
      destroyFinancingChargeTrendData,
      saveFinancingChargeTrendFilters,
      resizeCanvas
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
