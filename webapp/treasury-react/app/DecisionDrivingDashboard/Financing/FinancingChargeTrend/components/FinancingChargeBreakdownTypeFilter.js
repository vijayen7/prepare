import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getComboboxFilter } from 'commons/components/ComboboxFilter'

export default class FinancingChargeBreakdownTypeFilter extends Component {
  getData() {
    return [
      { key: 'CPE_FAMILY', value: 'CPE Family Breakdown' },
      { key: 'FINANCING_VS_GMV', value: 'Financing vs. GMV' }
    ];
  }

  render() {
    const ComboboxFilter = getComboboxFilter();
    return (
      <ComboboxFilter
        data={this.getData()}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        multiSelect={false}
        stateKey="selectedFinancingChargeBreakdownType"
        label="Chart View Type"
      />
    );
  }
}

FinancingChargeBreakdownTypeFilter.propTypes = {
  onSelect: PropTypes.func,
  selectedData: PropTypes.any
};
