import {
  FETCH_UNSECURED_NET_DEBIT_DETAIL_DATA,
  DESTROY_UNSECURED_NET_DEBIT_DETAIL_DATA,
  SAVE_UNSECURED_NET_DEBIT_FILTERS,
  SAVE_UNSECURED_NET_DEBIT_CONTAINER_DATA,
  FETCH_OFFSETTING_NET_DEBIT_DETAIL_DATA,
  DESTROY_OFFSETTING_NET_DEBIT_DETAIL_DATA,
  SAVE_OFFSETTING_NET_DEBIT_FILTERS,
  SAVE_OFFSETTING_NET_DEBIT_CONTAINER_DATA,
  FETCH_FINANCING_CHARGE_TREND_DATA,
  SAVE_FINANCING_TREND_FILTERS,
  DESTROY_FINANCING_CHARGE_TREND_DATA,
  UPDATE_FINANCING_CHARGE_TYPE,
  UPDATE_FINANCING_CHARGE_BREAKDOWN_TYPE,
  FETCH_FINANCING_RATE_COMPARISON_DATA,
  DESTROY_FINANCING_RATE_COMPARISON_DATA,
  SAVE_FINANCING_RATE_COMPARISON_FILTERS,
  SAVE_FINANCING_RATE_COMPARISON_CONTAINER_DATA,
  RESET_SEARCH_MESSAGE,
  RESIZE_CANVAS
} from 'DecisionDrivingDashboard/constants';

export function fetchUnsecuredNetDebitDetailData(payload) {
  return {
    type: FETCH_UNSECURED_NET_DEBIT_DETAIL_DATA,
    payload
  };
}

export function destroyUnsecuredNetDebitDetailData() {
  return {
    type: DESTROY_UNSECURED_NET_DEBIT_DETAIL_DATA
  };
}

export function saveUnsecuredNetDebitFilters(payload) {
  return {
    type: SAVE_UNSECURED_NET_DEBIT_FILTERS,
    payload
  };
}
export function saveUnsecuredNetDebitMainContainer(payload) {
  return {
    type: SAVE_UNSECURED_NET_DEBIT_CONTAINER_DATA,
    payload
  };
}

export function fetchFinancingRateComparisonData(payload) {
  return {
    type: FETCH_FINANCING_RATE_COMPARISON_DATA,
    payload
  };
}

export function resetSearchMessage() {
  return{
    type:RESET_SEARCH_MESSAGE
  };
}

export function destroyFinancingRateComparisonData() {
  return {
    type: DESTROY_FINANCING_RATE_COMPARISON_DATA
  };
}

export function saveFinancingRateComparisonFilters(payload) {
  return {
    type: SAVE_FINANCING_RATE_COMPARISON_FILTERS,
    payload
  };
}
export function saveFinancingRateComparisonContainerData(payload) {
  return {
    type: SAVE_FINANCING_RATE_COMPARISON_CONTAINER_DATA,
    payload
  };
}

export function fetchOffsettingNetDebitDetailData(payload) {
  return {
    type: FETCH_OFFSETTING_NET_DEBIT_DETAIL_DATA,
    payload
  };
}

export function destroyOffsettingNetDebitDetailData() {
  return {
    type: DESTROY_OFFSETTING_NET_DEBIT_DETAIL_DATA
  };
}

export function saveOffsettingNetDebitFilters(payload) {
  return {
    type: SAVE_OFFSETTING_NET_DEBIT_FILTERS,
    payload
  };
}
export function saveOffsettingNetDebitContainerData(payload) {
  return {
    type: SAVE_OFFSETTING_NET_DEBIT_CONTAINER_DATA,
    payload
  };
}

export function saveFinancingChargeTrendFilters(payload) {
  return {
    type: SAVE_FINANCING_TREND_FILTERS,
    payload
  };
}

export function fetchFinancingChargeTrendData(payload) {
  return {
    type: FETCH_FINANCING_CHARGE_TREND_DATA,
    payload
  };
}

export function destroyFinancingChargeTrendData() {
  return {
    type: DESTROY_FINANCING_CHARGE_TREND_DATA
  };
}

export function resizeCanvas() {
  return  {
    type:RESIZE_CANVAS
  }
}
