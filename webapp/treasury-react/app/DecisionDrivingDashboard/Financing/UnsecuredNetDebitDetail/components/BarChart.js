import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumberAsMoneyWithoutPrefix } from 'commons/util';
import BiaxialCompositeChart from 'commons/components/BiaxialCompositeChart';
import CustomizedAxisTick from './CustomizedAxisTick';
import BarChartTooltip from './BarChartTooltip';

export default class BarChart extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const isReportingFxNull = this.props.checkReportingFxNullOrNot(this.props.baseData);
    const yLabel = isReportingFxNull ? '(in USD)' : '(in RC)';
    const barDataKey = isReportingFxNull ? 'unsecuredNetDebit' : 'unsecuredNetDebitRC';
    return (
      <React.Fragment>
        <BiaxialCompositeChart
          data={this.props.getPlotData()}
          bars={[{ yAxisId: 'left', dataKey: barDataKey, size: 35, name: 'Unsecured Net Debit' }]}
          lines={
            [
              { yAxisId: 'right', dataKey: 'applicableRate', name: 'Financing Rate %' }
            ]
          }
          yAxis={{ left: { label: `Unsecured Net Debit Amount ${yLabel}`, tickFormatter: formatNumberAsMoneyWithoutPrefix },
            right: { label: 'Financing Rate (in %)', unit: '%' } }}
          xAxis={{ tick: <CustomizedAxisTick baseData={this.props.baseData} />, label: 'CPE ; Currency ; Legal Entity ;' }}
          legend={{ wrapperStyle: { paddingTop: 80 } }}
          tooltip={{ formatter: <BarChartTooltip baseData={this.props.baseData} /> }}
        />
      </React.Fragment>
    );
  }
}

BarChart.propTypes = {
  getPlotData: PropTypes.func.isRequired,
  baseData: PropTypes.array.isRequired,
  checkReportingFxNullOrNot: PropTypes.func.isRequired
};
