import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class CustomizedAxisTick extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { x, y, payload, baseData } = this.props;
    const obj = baseData.find((o) => o.id === payload.value);
    return (
      <g transform={`translate(${x},${y})`}>
        <text x={0} y={0} dy={16} textAnchor="middle" fontSize={12}>
          <tspan x="0" dy="1.2em" fill="#C99765">{obj.cpeAbbrev}</tspan>
          <tspan x="0" dy="1.2em" fill="#96BA61">{obj.currencyCode}</tspan>
          <tspan x="0" dy="1.2em" fill="#6CACB9">{obj.legalEntityAbbrev}</tspan>
        </text>
      </g>
    );
  }
}

CustomizedAxisTick.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  payload: PropTypes.array.isRequired,
  baseData: PropTypes.array.isRequired
};
