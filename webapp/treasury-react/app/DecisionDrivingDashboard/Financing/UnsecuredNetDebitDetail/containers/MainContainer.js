import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { URL } from 'commons/constants';
import SliderPanel from 'commons/components/SliderPanel';
import Link from 'commons/components/Link';
import LinkPanel from 'commons/components/LinkPanel';
import GridContainer from 'commons/components/Grid';
import Message from 'commons/components/Message';
import { getUrlEncodedOrgNamesFromOrgFamilyIds } from 'DecisionDrivingDashboard/util';
import { gridColumns as unsecuredNetDebitGridColumns } from '../grid/columnConfig';
import { gridOptions as unsecuredNetDebitGridOptions } from '../grid/gridOptions';
import BarChart from '../components/BarChart';
import { saveUnsecuredNetDebitMainContainer } from '../../actions';
import { getOrgIdsFromOrgFamilyIds, checkReportingFxNullOrNot, reportingCurrencyMessage } from 'DecisionDrivingDashboard/util';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.checkReportingFxNullOrNot = checkReportingFxNullOrNot.bind(this);
    this.reportingCurrencyMessage = reportingCurrencyMessage.bind(this);
    this.getPlotData = this.getPlotData.bind(this);
    this.financingCostComparator = this.financingCostComparator.bind(this);
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleToggleGroupChange = this.handleToggleGroupChange.bind(this);
  }

  componentWillMount() {
    const state = Object.keys(this.props.containerData).length ?
      this.props.containerData : this.getDefaultContainerData();
    this.setState(state);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.unsecuredNetDebitDetailData !== this.props.unsecuredNetDebitDetailData) {
      this.setState({ value: Math.min(nextProps.unsecuredNetDebitDetailData.length, 20) });
    }
  }

  componentWillUnmount() {
    this.props.saveUnsecuredNetDebitMainContainer(this.state);
  }

  handleSliderChange(value) {
    this.setState({ value });
  }

  handleToggleGroupChange(value) {
    this.setState({ view: value });
  }

  getDefaultContainerData() {
    const length = this.props.unsecuredNetDebitDetailData !== undefined ?
      Math.min(this.props.unsecuredNetDebitDetailData.length, 20) : 0;
    return { view: 'grid', value: length };
  }

  /*
    Slices the data to Top N ( where N = value)
    */
  getGridData(data) {
    if (data && data instanceof Array) { return data.sort(this.financingCostComparator).slice(0, this.state.value); }
  }

  financingCostComparator (a, b) {
    return Math.abs(b.financingCost) - Math.abs(a.financingCost);
  }

  getPlotData() {
    const plotData = [];
    const data = this.props.unsecuredNetDebitDetailData;
    const sortedByFinancingCostDesc = data.sort(this.financingCostComparator);
    for (let i = 0; i < Math.min(data.length, this.state.value); i++) {
      const { unsecuredNetDebit, unsecuredNetDebitRC, applicableRate } = sortedByFinancingCostDesc[i];
      plotData.push({
        name: data[i].id,
        unsecuredNetDebit,
        unsecuredNetDebitRC,
        applicableRate
      });
    }
    return plotData;
  }

  renderSliderPanel() {
    return (<SliderPanel
      min={1}
      max={this.props.unsecuredNetDebitDetailData.length}
      value={this.state.value}
      view={this.state.view}
      onSliderChange={this.handleSliderChange}
      toggleOptions={[
        { key: 'grid', value: 'Grid' },
        { key: 'chart', value: 'Chart' }
      ]}
      onToggleGroupChange={this.handleToggleGroupChange}
    />);
  }

  renderLinkPanel() {
    let cpeIds = getOrgIdsFromOrgFamilyIds(this.props.filters.selectedCpeFamilies, this.props.cpeFamilyToCpeData);
    let leIds = getOrgIdsFromOrgFamilyIds(this.props.filters.selectedLegalEntityFamilies, this.props.legalEntityFamilyToLegalEntityData);
    var filters = {
      'selectedCpes': cpeIds,
      'selectedLegalEntities': leIds,
       fromDate:this.props.filters.selectedDate,
       toDate:this.props.filters.selectedDate,
    };
    return (
      <LinkPanel
        links={[
          <Link
            id="interest-breakdown"
            text="View Detailed Interest Breakdown"
            href={`${URL}/treasury/detailReport.html#${JSON.stringify(filters)}`}
          />
        ]}
      />
    );
  }

  renderGrid() {
    return (<GridContainer
      data={this.getGridData(this.props.unsecuredNetDebitDetailData)}
      gridId="UnsecuredNetDebitDetailData"
      gridColumns={unsecuredNetDebitGridColumns()}
      gridOptions={unsecuredNetDebitGridOptions()}
      resizeCanvas={this.props.resizeCanvas}
    />);
  }

  renderChart() {
    return (
      <div>
        <BarChart getPlotData={this.getPlotData} baseData={this.props.unsecuredNetDebitDetailData} checkReportingFxNullOrNot={this.checkReportingFxNullOrNot}/>
      </div>);
  }

  renderMainContainer() {
    let data = this.props.unsecuredNetDebitDetailData;
    if (data.length <= 0) {
      return <Message className="margin--top" messageData="No data found." />;
    }

    return (
      <React.Fragment>
        {this.renderSliderPanel()}
        {(this.state.view === 'grid') ? this.renderGrid() : this.renderChart()}
        {this.reportingCurrencyMessage(data)}
        {this.renderLinkPanel()}
      </React.Fragment>
    );
  }

  render() {
    return (
      this.renderMainContainer()
    );
  }
}

MainContainer.defaultProps = {
  containerData: { view: 'grid', value: 5 },
  unsecuredNetDebitDetailData: []
};

MainContainer.propTypes = {
  containerData: PropTypes.object,
  unsecuredNetDebitDetailData: PropTypes.array,
  saveUnsecuredNetDebitMainContainer: PropTypes.func,
  legalEntityFamilyToLegalEntityData: PropTypes.object,
  cpeFamilyToCpeData: PropTypes.object,
  filters: PropTypes.object,
  resizeCanvas: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    containerData: state.decisionDrivingDashboard.financingView.unsecuredNetDebitContainerData,
    unsecuredNetDebitDetailData: state.decisionDrivingDashboard.financingView.unsecuredNetDebitDetailData,
    legalEntityFamilyToLegalEntityData: state.decisionDrivingDashboard.legalEntityFamilyToLegalEntityData,
    cpeFamilyToCpeData: state.decisionDrivingDashboard.cpeFamilyToCpeData,
    filters: state.decisionDrivingDashboard.financingView.unsecuredNetDebitFilters,
    resizeCanvas: state.decisionDrivingDashboard.financingView.resizeCanvas
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveUnsecuredNetDebitMainContainer
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainContainer);

