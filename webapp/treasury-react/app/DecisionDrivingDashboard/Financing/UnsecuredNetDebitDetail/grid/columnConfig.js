export function gridColumns() {
  const columns = [{
    id: 'counterpartyEntity',
    name: 'Counterparty Entity',
    field: 'cpeName',
    toolTip: 'Counterparty Entity',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 100
  },
  {
    id: 'legalEntity',
    name: 'Legal Entity',
    field: 'legalEntityName',
    toolTip: 'Legal Entity',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80
  },
  {
    id: 'agreementType',
    name: 'Agreement Type',
    field: 'agreementTypeName',
    toolTip: 'Agreement Type',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80
  },
  {
    id: 'currencyCode',
    name: 'Currency',
    field: 'currencyCode',
    toolTip: 'Currency',
    type: 'text',
    filter: true,
    sortable: true,
    headerCssClass: 'b',
    width: 80
  },
  {
    id: 'unsecuredNetDebit',
    name: 'Unsecured Net Debit (USD)',
    field: 'unsecuredNetDebit',
    toolTip: 'Unsecured Net Debit (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    width: 80
  },
  {
    id: 'unsecuredNetDebitRC',
    name: 'Unsecured Net Debit (RC)',
    field: 'unsecuredNetDebitRC',
    toolTip: 'Unsecured Net Debit (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    width: 80
  },
  {
    id: 'financingCost',
    name: 'Financing Cost (Per Day) (USD)',
    field: 'financingCost',
    toolTip: 'Financing Cost (Per Day) (USD)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    width: 80
  },
  {
    id: 'financingCostRC',
    name: 'Financing Cost (Per Day) (RC)',
    field: 'financingCostRC',
    toolTip: 'Financing Cost (Per Day) (RC)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    aggregator: dpGrid.Aggregators.sum,
    headerCssClass: 'aln-rt b',
    width: 80
  },
  {
    id: 'spread',
    name: 'Spread (%)',
    field: 'spread',
    toolTip: 'Spread (%)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    width: 50
  },
  {
    id: 'applicableRate',
    name: 'Applicable Rate (%)',
    field: 'applicableRate',
    toolTip: 'Applicable Rate (%)',
    type: 'number',
    filter: true,
    sortable: true,
    formatter: dpGrid.Formatters.Float,
    headerCssClass: 'aln-rt b',
    width: 50
  }
  ];

  return columns;
}
