import { url } from "../../api";
export function gridOptions(paging) {
  const options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [{ columnId: 'financingCost', sortAsc: true }],
    page: true,
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: 'Total',
    frozenColumn: 7,
    maxHeight: 600,
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    }
  };
  return options;
}
