import {
  FETCH_UNSECURED_NET_DEBIT_DETAIL_DATA,
  DESTROY_UNSECURED_NET_DEBIT_DETAIL_DATA,
  SAVE_UNSECURED_NET_DEBIT_FILTERS,
  SAVE_UNSECURED_NET_DEBIT_CONTAINER_DATA,
  FETCH_OFFSETTING_NET_DEBIT_DETAIL_DATA,
  DESTROY_OFFSETTING_NET_DEBIT_DETAIL_DATA,
  SAVE_OFFSETTING_NET_DEBIT_FILTERS,
  SAVE_OFFSETTING_NET_DEBIT_CONTAINER_DATA,
  FETCH_FINANCING_CHARGE_TREND_DATA,
  DESTROY_FINANCING_CHARGE_TREND_DATA,
  SAVE_FINANCING_TREND_FILTERS,
  RESIZE_CANVAS,
  FETCH_FINANCING_RATE_COMPARISON_DATA,
  DESTROY_FINANCING_RATE_COMPARISON_DATA,
  SAVE_FINANCING_RATE_COMPARISON_FILTERS,
  SAVE_FINANCING_RATE_COMPARISON_CONTAINER_DATA,
  UPDATE_SEARCH_MESSAGE,
  RESET_SEARCH_MESSAGE,
  DEFAULT_SEARCH_MESSAGE,
  NO_DATA_SEARCH_MESSAGE

} from 'DecisionDrivingDashboard/constants';
import { combineReducers } from 'redux';

function unsecuredNetDebitDetailDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_UNSECURED_NET_DEBIT_DETAIL_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_UNSECURED_NET_DEBIT_DETAIL_DATA:
      return [];
  }
  return state;
}

function unsecuredNetDebitFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_UNSECURED_NET_DEBIT_FILTERS}`:
      return action.payload || {};
  }
  return state;
}

function unsecuredNetDebitContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_UNSECURED_NET_DEBIT_CONTAINER_DATA}`:
      return action.payload || {};
  }
  return state;
}

function financingRateComparisonDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_FINANCING_RATE_COMPARISON_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_FINANCING_RATE_COMPARISON_DATA:
      return [];
  }
  return state;
}

function financingRateComparisonFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_FINANCING_RATE_COMPARISON_FILTERS}`:
      return action.payload || {};
  }
  return state;
}

function financingRateComparisonContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_FINANCING_RATE_COMPARISON_CONTAINER_DATA}`:
      return action.payload || {};
  }
  return state;
}

function offsettingNetDebitDetailDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_OFFSETTING_NET_DEBIT_DETAIL_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_OFFSETTING_NET_DEBIT_DETAIL_DATA:
      return [];
  }
  return state;
}

function offsettingNetDebitFiltersReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_OFFSETTING_NET_DEBIT_FILTERS}`:
      return action.payload || {};
    }
    return state;
  }

function financingTrendFiltersReducer(state = {}, action) {
  switch (action.type) {
    case SAVE_FINANCING_TREND_FILTERS:
      return action.payload || {};
    default:
      return state;
  }
}

function financingChargeTrendDataReducer(state = null, action) {
  switch (action.type) {
    case `${FETCH_FINANCING_CHARGE_TREND_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_FINANCING_CHARGE_TREND_DATA:
      return null;
  }
  return state;
}

function offsettingNetDebitContainerDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_OFFSETTING_NET_DEBIT_CONTAINER_DATA}`:
      return action.payload || {};
    }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state !== true;
    default:
      return false;
  }
}

function searchMessageReducer(
  state = DEFAULT_SEARCH_MESSAGE,
  action
) {
  switch (action.type) {
    case UPDATE_SEARCH_MESSAGE:
      return !action.data || action.data.length <= 0
        ? NO_DATA_SEARCH_MESSAGE
        : "";
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  unsecuredNetDebitDetailData: unsecuredNetDebitDetailDataReducer,
  unsecuredNetDebitFilters: unsecuredNetDebitFiltersReducer,
  unsecuredNetDebitContainerData: unsecuredNetDebitContainerDataReducer,
  financingRateComparisonData: financingRateComparisonDataReducer,
  financingRateComparisonFilters: financingRateComparisonFiltersReducer,
  financingRateComparisonContainerData: financingRateComparisonContainerDataReducer,
  offsettingNetDebitDetailData: offsettingNetDebitDetailDataReducer,
  offsettingNetDebitFilters: offsettingNetDebitFiltersReducer,
  offsettingNetDebitContainerData: offsettingNetDebitContainerDataReducer,
  financingChargeTrendFilters: financingTrendFiltersReducer,
  financingChargeTrendData: financingChargeTrendDataReducer,
  searchMessage:searchMessageReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
