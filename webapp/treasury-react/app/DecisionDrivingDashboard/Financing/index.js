import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Tab from 'commons/components/Tab';
import TabPanel from 'commons/components/TabPanel';
import Loader from 'commons/container/Loader';

import OffsettingNetDebitSideBar from './OffsettingNetDebitDetail/containers/SideBar';
import UnsecuredNetDebitSideBar from './UnsecuredNetDebitDetail/containers/SideBar';
import FinancingChargeTrendSidebar from './FinancingChargeTrend/containers/SideBar';
import FinancingRateComparisonSideBar from './FinancingRateComparison/containers/SideBar';


import OffsettingNetDebitMainContainer from './OffsettingNetDebitDetail/containers/MainContainer';
import UnsecuredNetDebitMainContainer from './UnsecuredNetDebitDetail/containers/MainContainer';
import FinancingChargeTrendMainContainer from './FinancingChargeTrend/containers/MainContainer';
import FinancingRateComparisonMainContainer from './FinancingRateComparison/containers/MainContainer';

export default class MarginDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedGadgetId: 'offsettingNetDebit',
      collapsed:false,
    };
    this.handleGadgetChange = this.handleGadgetChange.bind(this);
    this.onToggle = this.onToggle.bind(this);
  }

  getTabsList() {
    const tabs = [
      <Tab
        id="offsettingNetDebit"
        label="Boxed Debits and Credits"
        isSelected={this.state.selectedGadgetId === 'offsettingNetDebit'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="unsecuredNetDebit"
        label="Unsecured Net Debits"
        isSelected={this.state.selectedGadgetId === 'unsecuredNetDebit'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="financingRateComparison"
        label="Financing Rate Comparison"
        isSelected={this.state.selectedGadgetId === 'financingRateComparison'}
        onClick={this.handleGadgetChange}
      />,
      <Tab
        id="financingChargeTrend"
        label="Financing Charge Trend"
        isSelected={this.state.selectedGadgetId === 'financingChargeTrend'}
        onClick={this.handleGadgetChange}
      />

    ];
    return tabs;
  }

  handleGadgetChange(event) {
    this.setState({...this.state, selectedGadgetId: event.target.id,collapsed:false });
  }

  onToggle(collapsed){
      this.setState({...this.state,collapsed});
  }

  getSideBar() {
    switch (this.state.selectedGadgetId) {
      case 'offsettingNetDebit':
        return <OffsettingNetDebitSideBar onToggle={this.onToggle}/>;
      case 'unsecuredNetDebit':
        return <UnsecuredNetDebitSideBar onToggle={this.onToggle}/>;
      case 'financingChargeTrend':
        return <FinancingChargeTrendSidebar onToggle={this.onToggle}/>
      case 'financingRateComparison':
        return <FinancingRateComparisonSideBar onToggle={this.onToggle}/>
      default:
        return <OffsettingNetDebitSideBar onToggle={this.onToggle}/>;
    }
  }

  getMainContainer() {
    switch (this.state.selectedGadgetId) {
      case 'offsettingNetDebit':
        return <OffsettingNetDebitMainContainer />;
      case 'unsecuredNetDebit':
        return <UnsecuredNetDebitMainContainer/>;
      case 'financingRateComparison':
        return <FinancingRateComparisonMainContainer/>;
      case 'financingChargeTrend':
        return <FinancingChargeTrendMainContainer/>
      default:
        return <OffsettingNetDebitMainContainer/>;
    }
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex" style={{display:this.state.collapsed?'block':''}}>
          <div className="size--1" style={{position:this.state.collapsed?'absolute':''}}>
            {this.getSideBar()}
          </div>
          <div className="size--4 padding--horizontal--double" style={{marginLeft:this.state.collapsed?'20px':''}}>
            <TabPanel
              tabs={this.getTabsList()}
              onClick={this.handleGadgetChange}
              selectedId={this.state.selectedGadgetId}
            />
            {this.getMainContainer()}
            <br/>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
