import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SeclendDashboard from './Seclend';
import MarginDashboard from './Margin';
import FinancingDashboard from './Financing';
import PropTypes from 'prop-types';
import Loader from 'commons/container/Loader';
import { getCodexFilters } from './util';
import { hot } from "react-hot-loader/root";

import { getCommaSeparatedValues,
  getPreviousBusinessDay,
  getCommaSeparatedValuesOrNullForSingleSelect } from 'commons/util';

import {
  fetchMarginAndCompositionBreakdown,
  fetchMarginDayOnDayDiffData,
  fetchPositionsPenaltyData
} from './Margin/actions';

import {
  fetchUnsecuredNetDebitDetailData,
  fetchOffsettingNetDebitDetailData,
  fetchFinancingRateComparisonData
} from './Financing/actions';

import {
  fetchBorrowRateChangeData,
  fetchLendOpportunityData,
  fetchBorrowNegotiationData
} from './Seclend/actions';

import { fetchLegalEntityFamilyToLegalEntityData, fetchCpeFamilyToCpeData } from './actions';

class DecisionDrivingDashboard extends Component {
  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = {
                   enableSeclendGadgets: getCodexFilters('ENABLE_SECLEND_GADGETS'),
                   enableFinancingGadgets: getCodexFilters('ENABLE_FINANCING_GADGETS'),
                   enableMarginGadgets: getCodexFilters('ENABLE_MARGIN_GADGETS')
                 };
    this.handleGadgetChange = this.handleGadgetChange.bind(this);
  }

  componentDidMount() {

    if(this.state.enableSeclendGadgets === 'true' || this.state.enableSeclendGadgets === true) {
        this.loadSeclendData();
    }
    if( this.state.enableFinancingGadgets == 'true' || this.state.enableFinancingGadgets == true) {
      this.loadFinancingData();
    }
    if( this.state.enableMarginGadgets == 'true' || this.state.enableMarginGadgets == true) {
      this.loadMarginData();
    }
    this.loadReferenceData();

    if(this.state.enableSeclendGadgets === 'true' || this.state.enableSeclendGadgets == true) {
        this.handleToggle('seclend');
    } else if(this.state.enableFinancingGadgets === 'true' ||  this.state.enableFinancingGadgets == true) {
        this.handleToggle('financing')
    } else if(this.state.enableMarginGadgets == 'true' || this.state.enableMarginGadgets == true) {
         this.handleToggle('margin')
    }
  }

  loadMarginData() {
      const payload = {
        'marginFilter.startDate': this.fetchCodexFilterValue('DATE'),
        'marginFilter.endDate': this.fetchCodexFilterValue('DATE'),
        'marginFilter.cpeIds': this.fetchCodexFilterValue('CPES'),
        'marginFilter.legalEntityIds': this.fetchCodexFilterValue('LEGAL_ENTITIES'),
        'marginFilter.cpeFamilyIds': this.fetchCodexFilterValue('CPE_FAMILIES'),
        'marginFilter.legalEntityFamilyIds': this.fetchCodexFilterValue('LEGAL_ENTITY_FAMILIES'),
        'marginFilter.businessUnitIds': this.fetchCodexFilterValue('BUSINESS_UNITS'),
        'marginFilter.agreementTypeIds': this.fetchCodexFilterValue('AGREEMENT_TYPES')
      };

      if (this.props.marginAndCompositionBreakdownData.length === 0) {
        this.props.fetchMarginAndCompositionBreakdown(payload);
      }

      // Loading the margin APIs asynchronously to speed up the page load
      const isAsync = true;
      if (this.props.marginDayOnDayDiffData.length === 0) {
        this.props.fetchMarginDayOnDayDiffData(payload, isAsync);
      }

      if (this.props.positionsPenaltyData.length === 0) {
        this.props.fetchPositionsPenaltyData(payload, isAsync);
      }
    }

    loadSeclendData() {
      const payload = {
        date: this.fetchCodexFilterValue('DATE'),
        legalEntityFamilyIds: this.fetchCodexFilterValue('LEGAL_ENTITY_FAMILIES'),
        cpeFamilyIds: this.fetchCodexFilterValue('CPE_FAMILIES'),
        potentialSavingsThreshold: getCodexFilters('POTENTIAL_SAVINGS_THRESHOLD'),
        potentialIncomeThreshold: getCodexFilters('POTENTIAL_INCOME_THRESHOLD'),
        stockLoanFeeSpikeThreshold: getCodexFilters('STOCK_LOAN_FEE_SPIKE_THRESHOLD'),
        reportingCurrencyId: this.fetchCodexFilterValue('REPORTING_CURRENCY_ID')
      };

      if (this.props.borrowRateChangeData.length === 0) {
        this.props.fetchBorrowRateChangeData(payload);
      }

      if (this.props.lendOpportunitiesData.length === 0) {
        this.props.fetchLendOpportunityData(payload);
      }

      if (this.props.borrowNegotiationData.length === 0) {
        this.props.fetchBorrowNegotiationData(payload);
      }
    }

    loadFinancingData() {
      const payload = {
        date: this.fetchCodexFilterValue('DATE'),
        selectedLegalEntityFamilies: this.fetchCodexFilterValue('LEGAL_ENTITY_FAMILIES'),
        selectedCpeFamilies: this.fetchCodexFilterValue('CPE_FAMILIES'),
        financingCostThreshold: getCodexFilters('FINANCING_COST_THRESHOLD'),
        boxedBalanceThreshold: getCodexFilters('BOXED_BALANCE_THRESHOLD'),
        selectedAgreementTypes: getCodexFilters('AGREEMENT_TYPE_FOR_RATE_COMPARISON'),
        selectedLegalEntities: getCodexFilters('LE_FOR_RATE_COMPARISON'),
        selectedCurrencies: getCodexFilters('CCY_SPN_FOR_RATE_COMPARISON'),
        selectedFinancingRateTypes: getCodexFilters('RATE_TYPE_FOR_RATE_COMPARISON'),
        reportingCurrencyId: this.fetchCodexFilterValue('REPORTING_CURRENCY_ID')
      };

      if (this.props.unsecuredNetDebitDetailData.length === 0) {
        this.props.fetchUnsecuredNetDebitDetailData(payload);
      }
      if (this.props.offsettingNetDebitDetailData.length === 0) {
        this.props.fetchOffsettingNetDebitDetailData(payload);
      }
    }

    loadReferenceData() {
      this.props.fetchLegalEntityFamilyToLegalEntityData();
      this.props.fetchCpeFamilyToCpeData();
    }

  fetchCodexFilterValue(filterName) {
      if (filterName === 'DATE') {
        return getCodexFilters('DATE') === ''
          ? getPreviousBusinessDay()
          : getCodexFilters('DATE');
      }else if(filterName === 'REPORTING_CURRENCY_ID'){
        return getCommaSeparatedValuesOrNullForSingleSelect(getCodexFilters(filterName));
      }
      return getCommaSeparatedValues(getCodexFilters(filterName));
    }

  getView(view) {
    if (view === 'seclend') {
      return <SeclendDashboard />;
    } else if (view === 'financing') {
      return <FinancingDashboard />;
    } else if (view === 'margin') {
      return <MarginDashboard />;
    }
  }


  handleToggle(view) {

    if(this.seclendView != null) {
      this.seclendView.classList.remove('active');
    }

    if(this.financingView != null) {
      this.financingView.classList.remove('active');
    }

    if(this.marginView != null) {
      this.marginView.classList.remove('active');
    }

    if (view === 'seclend' && this.seclendView != null) {
      this.seclendView.classList.add('active');
      this.setState({ view });
    } else if (view === 'financing' && this.financingView != null) {
      this.financingView.classList.add('active');
      this.setState({ view });
    } else if (view === 'margin' && this.marginView != null) {
      this.marginView.classList.add('active');
      this.setState({ view });
    }
  }

  handleGadgetChange(event) {
    this.setState({ selectedGadgetId: event.target.id });
  }

  render() {
    return (

      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header

            ref={(header) => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div slot="application-menu" className="application-menu-toggle-view">
              {this.state.enableSeclendGadgets === 'true' || this.state.enableSeclendGadgets === true?
              <a
                ref={(seclendView) => {
                  this.seclendView = seclendView;
                }}
                onClick={() => this.handleToggle('seclend')}
              >
                Borrow/Lend Opportunities
              </a> : null
              }
              {this.state.enableFinancingGadgets === 'true' || this.state.enableFinancingGadgets === true?
              <a
                ref={(financingView) => {
                  this.financingView = financingView;
                }}
                onClick={() => this.handleToggle('financing')}
              >
                Financing Opportunities
              </a> : null
              }
              {this.state.enableMarginGadgets === 'true' || this.state.enableMarginGadgets === true?
              <a
                ref={(marginView) => {
                  this.marginView = marginView;
                }}
                onClick={() => this.handleToggle('margin')}
              >
                Margin Opportunities
              </a> : null
              }
            </div>
          </arc-header>
          <div className="padding--top--double">
            {this.getView(this.state.view) }
          </div>
        </div>
      </React.Fragment>
    );
  }
}

DecisionDrivingDashboard.propTypes = {
  borrowRateChangeData: PropTypes.array,
  lendOpportunitiesData: PropTypes.array,
  borrowNegotiationData: PropTypes.array,
  offsettingNetDebitDetailData: PropTypes.array,
  unsecuredNetDebitDetailData: PropTypes.array,
  financingRateComparisonData: PropTypes.array,
  marginAndCompositionBreakdownData: PropTypes.array,
  marginDayOnDayDiffData: PropTypes.array,
  positionsPenaltyData: PropTypes.array,
  fetchBorrowRateChangeData: PropTypes.func,
  fetchLendOpportunityData: PropTypes.func,
  fetchBorrowNegotiationData: PropTypes.func,
  fetchOffsettingNetDebitDetailData: PropTypes.func,
  fetchUnsecuredNetDebitDetailData: PropTypes.func,
  fetchFinancingRateComparisonData: PropTypes.func,
  fetchMarginAndCompositionBreakdown: PropTypes.func,
  fetchMarginDayOnDayDiffData: PropTypes.func,
  fetchPositionsPenaltyData: PropTypes.func,
  fetchLegalEntityFamilyToLegalEntityData: PropTypes.func,
  fetchCpeFamilyToCpeData: PropTypes.func
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchBorrowRateChangeData,
      fetchLendOpportunityData,
      fetchBorrowNegotiationData,
      fetchOffsettingNetDebitDetailData,
      fetchUnsecuredNetDebitDetailData,
      fetchFinancingRateComparisonData,
      fetchMarginAndCompositionBreakdown,
      fetchMarginDayOnDayDiffData,
      fetchPositionsPenaltyData,
      fetchLegalEntityFamilyToLegalEntityData,
      fetchCpeFamilyToCpeData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  const { marginAndCompositionBreakdownData, marginDayOnDayDiffData, positionsPenaltyData } = state.decisionDrivingDashboard.marginView;
  const { borrowRateChangeData, lendOpportunitiesData, borrowNegotiationData } = state.decisionDrivingDashboard.seclendView;
  const { unsecuredNetDebitDetailData, offsettingNetDebitDetailData, financingRateComparisonData } = state.decisionDrivingDashboard.financingView;

  return {
    marginAndCompositionBreakdownData,
    marginDayOnDayDiffData,
    positionsPenaltyData,

    borrowRateChangeData,
    lendOpportunitiesData,
    borrowNegotiationData,

    unsecuredNetDebitDetailData,
    offsettingNetDebitDetailData,
    financingRateComparisonData
  };
}

export default hot(
  connect(mapStateToProps, mapDispatchToProps)(DecisionDrivingDashboard)
);

