import React from 'react';

function getCodexKey(filterName) {
  switch (filterName) {
    case 'DATE':
      return 'date';
    case 'FINANCING_TREND_START_DATE':
      return 'financingTrendStartDate';
    case 'SECLEND_TREND_START_DATE':
      return 'seclendTrendStartDate';
    case 'MARGIN_TREND_START_DATE':
      return 'marginTrendStartDate';
    case 'TREND_END_DATE':
      return 'trendEndDate';
    case 'LEGAL_ENTITIES':
      return 'legalEntities';
    case 'CPES':
      return 'cpes';
    case 'LEGAL_ENTITY_FAMILIES':
      return 'legalEntityFamilies';
    case 'CPE_FAMILIES':
      return 'cpeFamilies';
    case 'BUSINESS_UNITS':
      return 'businessUnits';
    case 'AGREEMENT_TYPES':
      return 'agreementTypes';
    case 'CLASSIFICATION':
      return 'classification';
    case 'CURRENCIES':
      return 'ccySpns';
    case 'POTENTIAL_SAVINGS_THRESHOLD':
      return 'potentialSavingsThreshold';
    case 'POTENTIAL_INCOME_THRESHOLD':
      return 'potentialIncomeThreshold';
    case 'STOCK_LOAN_FEE_SPIKE_THRESHOLD':
      return 'stockLoanFeeSpikeThreshold';
    case 'FINANCING_COST_THRESHOLD':
      return 'financingCostThreshold';
    case 'BOXED_BALANCE_THRESHOLD':
      return 'boxedBalanceThreshold';
    case 'AGREEMENT_TYPE_FOR_RATE_COMPARISON':
      return 'agreementTypeForRateComparison';
    case 'LE_FOR_RATE_COMPARISON':
      return 'legalEntityForRateComparison';
    case 'CCY_SPN_FOR_RATE_COMPARISON':
      return 'ccySpnForRateComparison';
    case 'RATE_TYPE_FOR_RATE_COMPARISON':
      return 'rateTypeForRateComparison';
    case 'ENABLE_SECLEND_GADGETS':
      return 'enableSeclendGadgets';
    case 'ENABLE_FINANCING_GADGETS':
      return 'enableFinancingGadgets';
    case 'ENABLE_MARGIN_GADGETS':
      return 'enableMarginGadgets';
    case 'REPORTING_CURRENCY_ID':
      return 'reportingCurrencyId';
    default:
      return '';
  }
}

export function getCodexFilters(filterName) {
  const key = `treasury.portal.decisionDrivingDashboard.${getCodexKey(
    filterName
  )}`;
  switch (filterName) {
    case 'REPORTING_CURRENCY_ID':
      return { key: 1760000, value: `USD [1760000]`};
    case 'LEGAL_ENTITIES':
    case 'CPES':
    case 'CPE_FAMILIES':
    case 'BUSINESS_UNITS':
    case 'CLASSIFICATION':
    case 'CURRENCIES':
    case 'AGREEMENT_TYPES':
      return REPORTS_CODEX_PROPERTIES[key] === '-1' ? [] : REPORTS_CODEX_PROPERTIES[key];

    case 'LEGAL_ENTITY_FAMILIES':
      return REPORTS_CODEX_PROPERTIES[key] === '-1' ? [] : typeof(REPORTS_CODEX_PROPERTIES[key]) === "string"? REPORTS_CODEX_PROPERTIES[key].split() : REPORTS_CODEX_PROPERTIES[key];

    case 'ENABLE_SECLEND_GADGETS':
    case 'ENABLE_FINANCING_GADGETS':
    case 'ENABLE_MARGIN_GADGETS':
      return REPORTS_CODEX_PROPERTIES[key] == undefined ? true : REPORTS_CODEX_PROPERTIES[key];

    default:
      return REPORTS_CODEX_PROPERTIES[key] === undefined ? [] : REPORTS_CODEX_PROPERTIES[key];
  }
}

/**
 * selectedOrg : list of selected org family Ids (legal entity family/cpe family Ids)
 * orgFamilyIdToOrgIdMap : map of org Family Id to member Org Ids list
 * This method finds list of all le/cpe Ids associated with selected le/cpe family Ids and
 * created a string with names of these le/cpe Ids. It returns encoded format of these le/cpe
 * names ready to be used as a component of search URL for ivy screens.
 */

export function getUrlEncodedOrgNamesFromOrgFamilyIds(selectedOrgFamilies, orgFamilyIdToOrgIdMap) {
  let names = '';
  if (selectedOrgFamilies === undefined || selectedOrgFamilies.includes(-1)) return names;
  if (orgFamilyIdToOrgIdMap === undefined || (Object.keys(orgFamilyIdToOrgIdMap).length === 0)) return names;

  selectedOrgFamilies.forEach((id) => {
    const familyId = (id && id.key && id.value) ? id.key : id;
    if(orgFamilyIdToOrgIdMap[familyId]){
      Object.values(orgFamilyIdToOrgIdMap[familyId]).forEach((value) => {
        value += ';';
        names += value;
      });
    }
  });
  return names.length ? encodeURIComponent(names).replace(/%20/g, '+') : '';
}

export function getOrgIdsFromOrgFamilyIds(selectedOrgFamilies, orgFamilyIdToOrgIdMap) {
  let orgObjects = [];
  if (selectedOrgFamilies === undefined || selectedOrgFamilies.includes(-1)) return orgObjects;
  if (orgFamilyIdToOrgIdMap === undefined || (Object.keys(orgFamilyIdToOrgIdMap).length === 0)) return orgObjects;

  selectedOrgFamilies.forEach((id) => {
    const familyId = (id && id.key && id.value) ? id.key : id;
    if(orgFamilyIdToOrgIdMap[familyId]){
      let objects = [];
      Object.keys(orgFamilyIdToOrgIdMap[familyId]).forEach(key=>{
        objects.push({key: parseInt(key), value:orgFamilyIdToOrgIdMap[familyId][key]});
      })
      orgObjects = orgObjects.concat(objects);
    }
  });
  return orgObjects;
}

/**
 * This method returns org Ids which are intersection of (1) members of selected org family Ids
 * and (2) selected org Ids. If all org Family Ids [-1] are selected, it returns all selected org
 * Ids. If all org Ids [-1] are selected, it returns all members of selected org family ids.
 */
export function getApplicableOrgIds(selectedOrgIds, selectedOrgFamilyIds, orgFamilyIdToOrgIdMap) {
  if (selectedOrgFamilyIds.includes(-1)) return selectedOrgIds;
  const orgIdsForSelectedFamilies = [];
  selectedOrgFamilyIds.forEach((orgFamilyId) => {
    if (orgFamilyId && orgFamilyIdToOrgIdMap[orgFamilyId]) {
        Object.keys(orgFamilyIdToOrgIdMap[orgFamilyId]).forEach(orgId => {
          orgIdsForSelectedFamilies.push(parseInt(orgId));
        });
    }
  });

  if (selectedOrgIds.includes(-1)) return orgIdsForSelectedFamilies;
  return selectedOrgIds.filter((id) => orgIdsForSelectedFamilies.includes(id));
}

export function checkReportingFxNullOrNot(data){
  if(!data)
    return false;
  for (let i=0; i<data.length; i++) {
    const { reportingFxRate } = data[i];
    if (!reportingFxRate) {
      return true;
    }
  }
  return false;
}

export function reportingCurrencyMessage(data) {
  if (checkReportingFxNullOrNot(data)) {
    return (<ul className="bullet">
      <li className="critical">{`Fx Rate unavailable for selected Reporting Currency`}</li>
    </ul>);
  }
  return (
    <small><ul className="bullet">
      <li>{`Reporting Currency (RC) corresponds to ${data[0].reportingCurrency}`}</li>
    </ul></small>
  );
}
