import AppHeader from "arc-header";
import React, { useEffect } from "react";
import { ReactLoader } from "../commons/components/ReactLoader";
import PositionDetailReportSideBar from "./containers/PositionDetailReportSideBar";
import { useStore } from "./useStore";
import { observer } from "mobx-react-lite";
import { PositionReportProps } from "./models/UIDataTypes";
import { Breadcrumbs, Layout } from "arc-react-components";
import { whenHeaderRef } from "arc-header";
import { extractfiltersFromUrl } from "./utils/SearchDataUtil";
import PositionDetailReportGrid from "./containers/PositionDetailReportGrid";

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item key="positionDetailsReport">
        Position Details Report
      </Breadcrumbs.Item>
    </>
  );
};

const PositionDetailApp: React.FC<PositionReportProps> = (
  props: PositionReportProps
) => {
  const { positionDetailDataStore } = useStore();

  useEffect(() => {
    let url = props.location.search;
    updateBreadCrumbs();
    setGlobalFilters();
    extractFiltersFromUrl(url);
  }, []);

  const setGlobalFilters = async () => {
    const headerRef = await whenHeaderRef();
    if (headerRef && headerRef.gf) {
      const options = {
        enabled: {
          strategies: true,
          fundFamilies: true,
          legalEntityFamilies: true,
        },
      };
      headerRef.gf.addListener(appHeaderListener);
      headerRef.gf.setOptions(options);
    }
  };

  const extractFiltersFromUrl = (url: string) => {
    if (url != null && url.length > 0) {
      extractfiltersFromUrl(
        url,
        positionDetailDataStore.setFilters,
        positionDetailDataStore.setIsParametersInUrl,
        positionDetailDataStore.setToggleSidebar,
        positionDetailDataStore.fetchSavedGridData
      );
    } else {
      positionDetailDataStore.filters.selectedSavedGrid = null;
      positionDetailDataStore.fetchSavedGridData(positionDetailDataStore.selectedDatasetKey + "_positionDetailReport");
    }
  };

  const appHeaderListener = ({ userSelections, expandedSelections }) => {
    positionDetailDataStore.setExpandedGlobalSelections(userSelections, expandedSelections);
  };

  return (
    <React.Fragment>
      <ReactLoader inProgress={positionDetailDataStore.inProgress} />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={true} />
        <Layout isColumnType={true} className="border">
          <Layout.Child
            childId="PositionDetailApp1"
            size={1}
            key="child1"
            title="SEARCH"
            collapsible
            collapsed={positionDetailDataStore.toggleSidebar}
            onChange={positionDetailDataStore.setToggleSidebar}
            showHeader
          >
            <PositionDetailReportSideBar
              filters={positionDetailDataStore.filters}
            />
          </Layout.Child>
          <Layout.Divider childId="PositionDetailApp2" isResizable />
          <Layout.Child childId="PositionDetailApp3" size={3}>
            <PositionDetailReportGrid />
          </Layout.Child>
        </Layout>
      </div>
    </React.Fragment>
  );
};

export default observer(PositionDetailApp);
