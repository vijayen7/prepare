import { action, observable } from "mobx";
import * as PositionDetailDataAPI from "./api";
import { SearchFilter, LcmPositionDataKeyValue } from "./models/UIDataTypes";
import { PositionDetailDataFilter } from "./models/PositionDetailDataFilter";
import { MarginDataParam } from "./models/MarginDataParam";
import {
  convertFilterObjectsToIds,
  convertFilterObjectsToIdsJson,
  getPositionDetailDataFromArrayTable,
  getPositionDetailDataFromArrayTableOptimized,
  isEnrichWithPrevDayLcmPositionData,
  getApplicablePositionDetailUrl,
  getPositionDetailUrl,
  getPositionDetailCallBody,
} from "././utils/PositionDetailUtil";
import Status from "../commons/models/Status";
import {
  DATASET_TYPE,
  MESSAGES,
  STATUS,
  MAX_RECORDS_TO_DISPLAY_IN_GRID,
  INITIAL_POSITION_DATA,
  DATA_STATUS,
  DEFAULT_SEARCH_FILTERS,
  INITIAL_TRADE_IA_DISPLAY_COLUMNS,
  DATASETS,
  INITIAL_POSITION_RULE_DATA,
  MARGIN_TYPES,
  MAX_API_CALLS_FOR_LCM_POSITION_DATA,
  MAX_API_CALLS_FOR_MARGIN_LATEST_DATA,
  INITIAL_TRADE_IA_FILTER,
  TOAST_TYPE,
  FULFILLED_STATUS,
  TOO_MANY_REQUESTS_STATUS_CODE,
  SUCCESS_STATUS_CODE,
  POSITION_RULE_ID,
  MAX_PREV_DAY_RECORDS_TO_DISPLAY_IN_GRID,
  DEFAULT_USAGE_TYPE,
  USD_SUFFIX,
  RC_SUFFIX,
  EMPTY_STRING,
  RETRY_PDR_EXCEL_COUNT,
  POSITION_RULE_TYPES_REF_DATA,
  UPDATE_MARGIN_RULE,
  ADD_MARGIN_RULE,
  DATE_19000101,
  DATE_20380101
} from "./constants";
import {
  createRuleMap,
  generateRulesReportFromAPI,
  getPopUpSelectedData,
  showToastService,
} from "./utils/CommonUtil";
import { INTERMEDIATE_DATA_URL } from "../commons/constants";
import { PositionRuleData } from "./models/PositionRuleData";
import {
  getIdFromRefData,
  getNameFromRefData,
  handleResponse,
} from "../commons/TreasuryUtils";
import MapData from "../commons/models/MapData";
import { PositionMarginData } from "./models/PositionMarginData";
import {
  convertFilterObjectsToMarginDataIds,
  getDefaultMarginDetailUrl,
  convertFilterObjectsToMarginDataJson,
  getMarginDataUrl,
  getMarginDataCallBody,
  getMarginDataFromArrayTable,
} from "././utils/MarginDetailsUtil";
import { isValidInputData } from "./utils/SearchDataUtil";
import {
  AGREEMENT_META_DATA,
  BROKER_SECURITY_ATTRS,
  BUNDLE_ATTRS,
  BUNDLE_INTERNAL_EXPOSURE,
  EXPOSURE_RECON,
  EXTENDED_SECURITY_ATTRS,
  INTERNAL_EXPOSURE,
  MARGIN_DETAILS,
  SECURITY_ATTRS,
  UNDERLYING_SECURITY_ATTRS,
  RULE_META_COLUMNS,
} from "./utils/ColumnMappersUtil";
import { TradeIndependentAmountData } from "../MarginScreens/TradeIndependentAmount/models/TradeIndependentAmountData";
import { TradeIndependentAmountFilter } from "../MarginScreens/TradeIndependentAmount/models/TradeIndependentAmountFilter";
import { TradeIndependentAmountGridData } from "../MarginScreens/TradeIndependentAmount/models/TradeIndependentAmountGridData";
import { getGridDataFromTradeIndependentAmountData } from "../MarginScreens/TradeIndependentAmount/utils/searchTradeIndependentAmountUtil";
import { getTradeIndependentAmountDetail } from "../MarginScreens/TradeIndependentAmount/api";
import { getActionColumn } from "./grid/GridColumns";
import { BASE_URL } from "../commons/constants";
import SERVICE_URLS from "../commons/UrlConfigs";
import { convertJavaDate } from "../commons/util";
import { CLASS_NAMESPACE } from "../commons/ClassConfigs";

const { Sema } = require("async-sema");
const sema = new Sema(
  MAX_API_CALLS_FOR_LCM_POSITION_DATA, // Allow 3 concurrent async calls
  {
    capacity: 100, // Prealloc space for 100 tokens
  }
);

const copy = require("clipboard-copy");

declare global {
  interface Window {
    clipboardData: any;
  }
}

export class PositionDetailDataStore {
  modifyRulesStore: any;
  searchRulesStore: any;
  constructor(rootStore) {
    this.modifyRulesStore = rootStore.modifyRulesStore;
    this.searchRulesStore = rootStore.searchRulesStore;
  }

  @observable
  toggleSidebar: boolean = false;

  @observable
  toggleTradeIAGrid: boolean = false;

  @observable
  inProgress: boolean = false;

  @observable
  overideDefaultSavedGrid: boolean = false;

  @observable.shallow
  positionDetailDataList: LcmPositionDataKeyValue[] = INITIAL_POSITION_DATA;

  @observable.shallow
  latestMarginDataList: PositionMarginData[] = [];

  @observable
  tradeIndependentAmountList: TradeIndependentAmountData[] = [];

  @observable
  tradeIndependentAmountFilter: TradeIndependentAmountFilter = INITIAL_TRADE_IA_FILTER;

  @observable
  tradeIndependentAmountGridData: TradeIndependentAmountGridData[] = [];

  @observable
  filters: SearchFilter = DEFAULT_SEARCH_FILTERS;

  @observable
  searchStatus: Status = DATA_STATUS;

  @observable
  isParametersInUrl: boolean = false;

  @observable
  datasetColumns: any[] = [];

  @observable
  displayColumns: string[] = [];

  @observable
  selectedDatasetKey: number = 2;

  @observable
  displayTradeIAColumns: string[] = INITIAL_TRADE_IA_DISPLAY_COLUMNS;

  @observable
  showAddMarginRuleDialog: boolean = false;

  @observable
  selectedRow: any;

  @observable
  houseTypeRuleData: PositionRuleData = INITIAL_POSITION_RULE_DATA;

  @observable
  exchangeTypeRuleData: PositionRuleData = INITIAL_POSITION_RULE_DATA;

  @observable
  regTypeRuleData: PositionRuleData = INITIAL_POSITION_RULE_DATA;

  @observable
  segTypeRuleData: PositionRuleData = INITIAL_POSITION_RULE_DATA;

  @observable
  regCpeTypeRuleData: PositionRuleData = INITIAL_POSITION_RULE_DATA;

  @observable
  savedGrids: any[] = [];

  @observable
  savedGridDataMap: Map<number, string> = new Map();

  @observable
  selectedLegalEntityData: MapData[] = [];

  @observable
  selectedBookData: MapData[] = [];

  @observable
  selectedLegalEntityFamilies: MapData[] = [];

  useOptimizedPdr: boolean = false;

  @observable
  isPositionDetailListEmpty: boolean = false;

  @observable
  gridDisplayColumnsArray: any[] = [];

  @observable globalFiltersApplied: boolean = false;

  pdrFilters: PositionDetailDataFilter[] = [];

  @observable
  isEnabledDownloadPdrReport: boolean = false;

  @observable
  allowReportDownload: boolean = false;

  @observable clickedRowId: number | string | undefined = undefined;
  @observable clickedRow: number | string | undefined = undefined;
  @observable isPositionRuleAvailable: boolean = true;

  @action
  modifyRuleStoreOnSelect = (selectedData: any) => {
    const { key, value } = selectedData;

    if (key === "selectedMarginTypes" && value && value.key) {
      this.enrichWithRuleDataWhenMarginTypeSelected(value.key);
    }

    if (key === "selectedRuleTypes" && value && value.key && value.value) {
      this.modifyRulesStore.changePopUpSelectedDataOnRuleType(key, value.value);
      this.enrichWithRuleDataWhenRuleTypeSelected(value.key + 1);
    }

    this.modifyRulesStore.popUpSelectedData[key] = value;
  };

  @action.bound
  onDisplayColumnsChange(newDisplayColumns) {
    let newDisplayColumnSet = new Set(newDisplayColumns);
    this.displayTradeIAColumns = this.getDisplayColumnsArray(
      newDisplayColumnSet
    );
  }

  @action.bound
  setClickedRow = (value: any) => {
    this.clickedRow = value;
    this.clickedRowId = value;
  };

  @action
  setFilters = (newFilters: SearchFilter) => {
    this.filters = newFilters;
    if (this.filters.securityFilter == null) {
      this.filters.securityFilter = {
        selectedSpns: "",
        securitySearchString: "",
        selectedTextSearchType: { key: "", value: "" },
        selectedSecuritySearchType: { key: "", value: "" },
        isAdvancedSearch: false,
      };
    }

    this.filters.isPositionLevel =
      this.filters.isPositionLevel === null
        ? true
        : this.filters.isPositionLevel;

    this.filters.isPublished =
      this.filters.isPublished === null ? true : this.filters.isPublished;

    this.filters.includeExtendedSecurityAttributes =
      this.filters.includeExtendedSecurityAttributes == null
        ? false
        : this.filters.includeExtendedSecurityAttributes;

    if (this.filters.selectedDataset?.key) {
      this.selectedDatasetKey = this.filters.selectedDataset?.key;
    }

    this.handleSearch();
  };

  getDefaultInputLegalEntities = () => {
    return this.globalFiltersApplied ? this.selectedLegalEntityData : [];
  };

  getDefaultInputLegalEntityFamilies = () => {
    return this.globalFiltersApplied ? this.selectedLegalEntityFamilies : [];
  };

  getDefaultInputBooks = () => {
    return this.globalFiltersApplied ? this.selectedBookData : [];
  };

  @action.bound
  setExpandedGlobalSelections(
    userSelections: any,
    expandedGlobalSelection: any
  ) {
    this.resetGlobalSideBarFilters();

    var bookDataList = expandedGlobalSelection["books"];
    this.globalFiltersApplied =
      userSelections?.legalEntityFamilies?.length ||
      userSelections?.strategies?.length;
    var legalEntityDataList = expandedGlobalSelection["legalEntities"];
    var legalEntityFamilyDataList =
      expandedGlobalSelection["legalEntityFamilies"];

    var bookDataMap: MapData[] = [];
    var legalEntityDataMap: MapData[] = [];
    var legalEntityFamilyDataMap: MapData[] = [];

    for (var iter = 0; iter < legalEntityDataList.length; iter++) {
      var legalEntityData = legalEntityDataList[iter];
      var legalEntityId = parseInt(legalEntityData["id"]);
      var legalEntityName = legalEntityData["name"].concat(
        "[",
        legalEntityId,
        "]"
      );
      legalEntityDataMap.push({ key: legalEntityId, value: legalEntityName });
    }

    for (var iter = 0; iter < legalEntityFamilyDataList.length; iter++) {
      var legalEntityFamilyData = legalEntityFamilyDataList[iter];
      var legalEntityFamilyId = parseInt(legalEntityFamilyData["id"]);
      var legalEntityFamilyName = legalEntityFamilyData["name"].concat(
        "[",
        legalEntityFamilyId,
        "]"
      );
      legalEntityFamilyDataMap.push({
        key: legalEntityFamilyId,
        value: legalEntityFamilyName,
      });
    }

    for (var iter = 0; iter < bookDataList.length; iter++) {
      var bookData = bookDataList[iter];
      bookDataMap.push({
        key: parseInt(bookData["id"]),
        value: bookData["name"],
      });
    }

    this.selectedLegalEntityData = legalEntityDataMap;
    this.selectedBookData = bookDataMap;
    this.selectedLegalEntityFamilies = legalEntityFamilyDataMap;
    this.searchRulesStore.selectedLegalEntityData = legalEntityDataMap;
    this.searchRulesStore.selectedBookData = bookDataMap;
  }

  resetGlobalSideBarFilters = () => {
    this.filters.selectedLegalEntities = [];
    this.filters.selectedBooks = [];
    this.filters.selectedLegalEntityFamilies = [];
  };

  @action.bound
  setToggleSidebar = (toggle: boolean) => {
    this.toggleSidebar = toggle;
  };

  @action.bound
  setToggleTradeIAGrid = (toggleTradeIAGrid: boolean) => {
    this.toggleTradeIAGrid = toggleTradeIAGrid;
  };

  @action.bound
  setOverideDefaultSavedGrid = (toggleOverrideSavedGrid: boolean) => {
    this.overideDefaultSavedGrid = toggleOverrideSavedGrid;
  };

  @action.bound
  setIsPositionLevel = (toggle: boolean) => {
    this.filters.isPositionLevel = toggle;

    if (this.filters.isPositionLevel) {
      this.filters.selectedBundles = [];
      this.filters.selectedBusinessUnits = [];
    }

    if (
      !this.filters.isPositionLevel &&
      this.filters.selectedDataset?.key ===
        DATASET_TYPE.Recon_Margin_Exposure_Details
    ) {
      this.filters.selectedDataset = null;
    }
  };

  @action.bound
  setIsPublished = (toggle: boolean) => {
    this.filters.isPublished = toggle;
  };

  @action.bound
  setIsParametersInUrl = (isParametersInUrl: boolean) => {
    this.isParametersInUrl = isParametersInUrl;
  };

  @action.bound
  setSearchStatus(status: string, message: string) {
    this.searchStatus = {
      status: status,
      message: message,
    };
  }

  @action.bound
  handleReset = () => {
    this.filters = DEFAULT_SEARCH_FILTERS;
    this.searchStatus = DATA_STATUS;
  };

  @action.bound
  handleCopySearch = () => {
    let url;
    var selectedSharedGridKey = 0;
    if (this.filters.selectedSavedGrid?.value.includes("Shared")) {
      selectedSharedGridKey = this.filters.selectedSavedGrid.key;
    }
    if (
      this.selectedDatasetKey == DATASET_TYPE.Margin_Details &&
      !this.filters.isPublished
    ) {
      let marginDataParam = convertFilterObjectsToMarginDataIds(this.filters);
      url = getDefaultMarginDetailUrl(
        window.location.origin,
        marginDataParam,
        this.selectedDatasetKey,
        selectedSharedGridKey,
        this.filters.selectedReportingCurrency?.key
      );
    } else {
      let positionDetailDataFilter = convertFilterObjectsToIds(this.filters);
      url = getApplicablePositionDetailUrl(
        window.location.origin,
        positionDetailDataFilter,
        this.selectedDatasetKey,
        true,
        selectedSharedGridKey,
        this.filters.selectedReportingCurrency?.key
      );
    }

    copy(url);
    showToastService(MESSAGES.COPY_SEARCH_URL, TOAST_TYPE.INFO);
  };

  @action.bound
  onSelect({ key, value }: any) {
    if (key === "securityFilter" && value.selectedSpns !== "") {
      value.selectedSpns = value.selectedSpns.replaceAll(" ", ";");
    }
    this.filters[key] = value;
  }

  @action.bound
  generateReportFromAPI() {
    generateRulesReportFromAPI(this.filters);
  }

  @action.bound
  onDatasetSelect({ key, value }: any) {
    this.onSelect({ key, value });
    this.selectedDatasetKey = value.key;
    this.filters.selectedSavedGrid = null;
    this.fetchSavedGridData(
      this.filters.selectedDataset?.key + "_positionDetailReport"
    );
  }

  @action.bound
  setDisplayColumns = (displayColumns) => {
    this.displayColumns = displayColumns;
    if (this.overideDefaultSavedGrid) {
      this.setOverideDefaultSavedGrid(false);
    } else {
      this.setSavedGridColumns(displayColumns);
    }
  };

  @action.bound
  handleSearch = () => {
    this.latestMarginDataList = [];
    this.positionDetailDataList = [];
    this.tradeIndependentAmountGridData = [];
    this.isEnabledDownloadPdrReport = false;
    this.allowReportDownload = true;
    this.setToggleTradeIAGrid(false);
    let isPrevDay = this.filters.isCrimsonEnabled === true ? true : false;
    let suffixCurrency =
      this.filters.selectedReportingCurrency?.key == 0 ? USD_SUFFIX : RC_SUFFIX;

    this.filters.selectedLegalEntities =
      this.filters.selectedLegalEntities?.length ||
      this.filters.selectedLegalEntityFamilies?.length
        ? this.filters.selectedLegalEntities
        : this.getDefaultInputLegalEntities();
    this.filters.selectedBooks = this.filters.selectedBooks?.length
      ? this.filters.selectedBooks
      : this.getDefaultInputBooks();

    if (isValidInputData(this.filters)) {
      let {
        dataSetColumns,
        baseDataSetColumns,
      } = this.getGridColumnsForDataset(
        DATASETS[this.selectedDatasetKey - 1],
        isPrevDay,
        suffixCurrency
      );
      this.datasetColumns = baseDataSetColumns;
      this.gridDisplayColumnsArray = [getActionColumn(), ...dataSetColumns];
      let displayColumns: string[] = [];
      if (this.filters.selectedSavedGrid == null) {
        this.gridDisplayColumnsArray.forEach((column) => {
          if (column) displayColumns.push(column.identity);
        });
        this.displayColumns = displayColumns;
      } else {
        const savedGridColumns = this.savedGridDataMap[
          this.filters.selectedSavedGrid.key
        ];
        this.setSavedGridColumns(savedGridColumns);
      }
      this.setToggleSidebar(true);
      if (
        this.selectedDatasetKey === DATASET_TYPE.Margin_Details &&
        !this.filters.isPublished
      ) {
        this.loadLatestMarginPositionData(this.filters);
      } else if (this.filters.isCrimsonEnabled) {
        this.loadPositionData(this.filters, true);
      } else {
        this.loadPositionData(this.filters, false);
      }
      this.setOverideDefaultSavedGrid(true);
    }
    this.modifyRulesStore.prefetchRefData();
  };

  @action.bound
  handleCacheRefresh = () => {
    this.latestMarginDataList = [];
    this.positionDetailDataList = [];
    this.tradeIndependentAmountGridData = [];
    this.setToggleTradeIAGrid(false);
    if (isValidInputData(this.filters)) {
      this.setToggleSidebar(true);
      if (
        !(
          this.selectedDatasetKey === DATASET_TYPE.Margin_Details &&
          !this.filters.isPublished
        )
      ) {
        this.populateCache(this.filters);
      }
    }
  };

  @action.bound
  downloadReport = async () => {
    this.allowReportDownload = false;
    showToastService(MESSAGES.DOWNLOAD_REPORT_IN_PROGRESS, TOAST_TYPE.INFO);
    let retryCount = 0;
    let objectUri: string = EMPTY_STRING;

    while (retryCount < RETRY_PDR_EXCEL_COUNT) {
      objectUri = await PositionDetailDataAPI.getUploadedFileUrl(
        this.pdrFilters[0]
      );

      if (objectUri === EMPTY_STRING) {
        retryCount++;
        continue;
      } else {
        PositionDetailDataAPI.downloadPdrExcel(
          objectUri,
          convertJavaDate(this.pdrFilters[0].date)
        );
        break;
      }
    }

    if (objectUri === EMPTY_STRING) {
      showToastService(
        MESSAGES.DOWNLOAD_REPORT_ERROR_MESSAGE,
        TOAST_TYPE.CRITICAL
      );
    } else {
      showToastService(
        MESSAGES.REPORT_DOWNLOADED_SUCCESS_MESSAGE,
        TOAST_TYPE.SUCCESS
      );
    }
    this.allowReportDownload = true;
  };

  @action.bound
  setSavedGridColumns = (columns) => {
    let newDisplayColumnSet = new Set(columns);

    this.displayColumns = this.getDisplayColumnsArray(newDisplayColumnSet);
  };

  @action.bound
  applySavedFilters = (savedFilters) => {
    this.filters = savedFilters;
    this.filters.selectedFromDate = DEFAULT_SEARCH_FILTERS.selectedFromDate;
    this.filters.selectedToDate = DEFAULT_SEARCH_FILTERS.selectedToDate;
  };

  @action.bound
  goToIntermediateScreen = (
    legalEntityId: number,
    cpeId: number,
    agreementTypeId: number,
    dateString: string
  ) => {
    let url =
      `${INTERMEDIATE_DATA_URL}legalEntityIds=` +
      legalEntityId +
      `&cpeIds=` +
      cpeId +
      `&agreementTypeIds=` +
      agreementTypeId +
      `&dateString=` +
      dateString +
      `&isCopyUrl=true&_outputKey=resultList`;
    window.open(url, "_blank");
  };

  getGridColumnsForDataset = (
    value: MapData,
    isPrevDay: boolean,
    suffixCurrency: string
  ) => {
    var baseDataSetColumns;
    var dataSetColumns = [
      ...AGREEMENT_META_DATA("", suffixCurrency),
      ...SECURITY_ATTRS(),
      ...MARGIN_DETAILS("", suffixCurrency),
      ...RULE_META_COLUMNS
    ];
    baseDataSetColumns = dataSetColumns;
    if (
      value.key == DATASET_TYPE.Margin_Details &&
      this.filters.isPositionLevel &&
      this.filters.includeExtendedSecurityAttributes
    ) {
        dataSetColumns = [
          ...dataSetColumns,
          ...UNDERLYING_SECURITY_ATTRS,
          ...EXTENDED_SECURITY_ATTRS
        ];
      baseDataSetColumns = dataSetColumns;
    } else if (
      value.key == DATASET_TYPE.Margin_Details &&
      this.filters.isPositionLevel &&
      !this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = [
        ...dataSetColumns,
      ];
      baseDataSetColumns = dataSetColumns
    } else if (
      value.key == DATASET_TYPE.Margin_Details &&
      !this.filters.isPositionLevel &&
      this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = [
        ...dataSetColumns,
        ...BUNDLE_ATTRS,
        ...UNDERLYING_SECURITY_ATTRS,
        ...EXTENDED_SECURITY_ATTRS,
      ];
      baseDataSetColumns = dataSetColumns;
    } else if (
      value.key == DATASET_TYPE.Margin_Details &&
      !this.filters.isPositionLevel &&
      !this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = this.removeMarginColumnsForBundleDataset(
        dataSetColumns,
        suffixCurrency
      );
      dataSetColumns = [...dataSetColumns, ...BUNDLE_ATTRS];
      baseDataSetColumns = dataSetColumns;
    } else if (
      value.key == DATASET_TYPE.Margin_Exposure_Details &&
      this.filters.isPositionLevel &&
      this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = [
        ...dataSetColumns,
        ...INTERNAL_EXPOSURE("", suffixCurrency),
        ...BROKER_SECURITY_ATTRS("", suffixCurrency),
        ...UNDERLYING_SECURITY_ATTRS,
        ...EXTENDED_SECURITY_ATTRS,
      ];
      baseDataSetColumns = dataSetColumns;
    } else if (
      value.key == DATASET_TYPE.Margin_Exposure_Details &&
      this.filters.isPositionLevel &&
      !this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = [
        ...dataSetColumns,
        ...INTERNAL_EXPOSURE("", suffixCurrency),
        ...BROKER_SECURITY_ATTRS("", suffixCurrency)
      ];
      baseDataSetColumns = dataSetColumns;
    } else if (
      value.key == DATASET_TYPE.Margin_Exposure_Details &&
      !this.filters.isPositionLevel &&
      this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = this.removeMarginColumnsForBundleDataset(
        dataSetColumns,
        suffixCurrency
      );
      dataSetColumns = [
        ...dataSetColumns,
        ...BUNDLE_ATTRS,
        ...BUNDLE_INTERNAL_EXPOSURE(suffixCurrency),
        ...UNDERLYING_SECURITY_ATTRS,
        ...EXTENDED_SECURITY_ATTRS,
      ];
      baseDataSetColumns = dataSetColumns;
    } else if (
      value.key == DATASET_TYPE.Margin_Exposure_Details &&
      !this.filters.isPositionLevel &&
      !this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = this.removeMarginColumnsForBundleDataset(
        dataSetColumns,
        suffixCurrency
      );
      dataSetColumns = [
        ...dataSetColumns,
        ...BUNDLE_ATTRS,
        ...BUNDLE_INTERNAL_EXPOSURE(suffixCurrency),
      ];
      baseDataSetColumns = dataSetColumns;
    } else if (
      value.key == DATASET_TYPE.Recon_Margin_Exposure_Details &&
      this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = [
        ...dataSetColumns,
        ...INTERNAL_EXPOSURE("", suffixCurrency),
        ...BROKER_SECURITY_ATTRS("", suffixCurrency),
        ...EXPOSURE_RECON("", suffixCurrency),
        ...UNDERLYING_SECURITY_ATTRS,
        ...EXTENDED_SECURITY_ATTRS
      ];
      baseDataSetColumns = dataSetColumns;
    } else if (
      value.key == DATASET_TYPE.Recon_Margin_Exposure_Details &&
      !this.filters.includeExtendedSecurityAttributes
    ) {
      dataSetColumns = [
        ...dataSetColumns,
        ...INTERNAL_EXPOSURE("", suffixCurrency),
        ...BROKER_SECURITY_ATTRS("", suffixCurrency),
        ...EXPOSURE_RECON("", suffixCurrency)
      ];

      baseDataSetColumns = dataSetColumns;
      if (isPrevDay) {
        let prefix = "prevDay";
        dataSetColumns = [
          ...dataSetColumns,
          ...AGREEMENT_META_DATA(prefix, suffixCurrency),
          ...SECURITY_ATTRS(prefix),
          ...MARGIN_DETAILS(prefix, suffixCurrency),
          ...INTERNAL_EXPOSURE(prefix, suffixCurrency),
          ...BROKER_SECURITY_ATTRS(prefix, suffixCurrency),
          ...EXPOSURE_RECON(prefix, suffixCurrency),
        ];

        if (!this.filters.isPositionLevel) {
          dataSetColumns = this.removeMarginColumnsForBundleDataset(
            dataSetColumns,
            suffixCurrency
          );
        }
      }
    }

    return { dataSetColumns, baseDataSetColumns };
  };

  removeMarginColumnsForBundleDataset = (
    dataSetColumns: any,
    suffixCurrency: string
  ) => {
    let dataSetColumnsFiltered = dataSetColumns.filter(
      (ele) =>
        ele.identity !== "marginDiff" + suffixCurrency &&
        ele.identity !== "houseMarginAdjustment" + suffixCurrency &&
        ele.identity !== "houseAdjustedMargin" + suffixCurrency &&
        ele.identity !== "houseUsageAdjustment" + suffixCurrency &&
        ele.identity !== "houseAdjustedUsage" + suffixCurrency &&
        ele.identity !== "regMarginAdjustment" + suffixCurrency &&
        ele.identity !== "regAdjustedMargin" + suffixCurrency &&
        ele.identity !== "regUsageAdjustment" + suffixCurrency &&
        ele.identity !== "regAdjustedUsage" + suffixCurrency &&
        ele.identity !== "reqHairCut" &&
        ele.identity !== "brokerQuantity"
    );

    return dataSetColumnsFiltered;
  };

  @action.bound
  getRuleDataForAllMarginTypes = async (selectedRow: any) => {
    this.inProgress = true;
    this.selectedRow = selectedRow;
    this.isPositionRuleAvailable = true;
    var ruleMap = createRuleMap(selectedRow);
    this.houseTypeRuleData = await PositionDetailDataAPI.getPositionRuleDetails(
      ruleMap
    );

    ruleMap.marginTypeId = MARGIN_TYPES.Exchange;
    this.exchangeTypeRuleData = await PositionDetailDataAPI.getPositionRuleDetails(
      ruleMap
    );

    ruleMap.marginTypeId = MARGIN_TYPES.Regulatory;
    this.regTypeRuleData = await PositionDetailDataAPI.getPositionRuleDetails(
      ruleMap
    );

    ruleMap.marginTypeId = MARGIN_TYPES.Segregated_IA;
    this.segTypeRuleData = await PositionDetailDataAPI.getPositionRuleDetails(
      ruleMap
    );

    ruleMap.marginTypeId = MARGIN_TYPES.Regulatory_CPE;
    this.regCpeTypeRuleData = await PositionDetailDataAPI.getPositionRuleDetails(
      ruleMap
    );

    this.modifyRulesStore.popUpSelectedData = getPopUpSelectedData(
      selectedRow,
      this.houseTypeRuleData
    );

    this.modifyRulesStore.operation = this.modifyRulesStore.popUpSelectedData[
      "selectedRuleId"
    ]
      ? UPDATE_MARGIN_RULE
      : ADD_MARGIN_RULE;

    if (this.houseTypeRuleData?.ruleType?.id !== POSITION_RULE_ID) {
      this.setPopupDataWithDefaultPositionRuleData(this.houseTypeRuleData);
    }

    this.modifyRulesStore.marginRuleKey = this.houseTypeRuleData
      ? this.getMarginRuleKey(this.houseTypeRuleData)
      : null;

    this.inProgress = false;
    this.modifyRulesStore.toggleShowDialog("Add/Edit Position Rule");

    this.modifyRulesStore.disablePortfolioLevelRule = true;
  };

  getMarginRuleKey = (rule: PositionRuleData) => {
    let marginRuleKey = {
      ruleId: rule.ruleId,
      marginType: rule.marginTypeId,
      "@CLASS": CLASS_NAMESPACE.arcesium.treasury.margin.common.model.MarginRuleKey,
    };
    return marginRuleKey;
  };

  fetchSavedGridData = async (params: string) => {
    let savedGridList = await PositionDetailDataAPI.getSavedGrids(params);
    let savedGridData: any[] = [];
    savedGridList.forEach((element) => {
      if (element.settingName !== null) {
        let elementMapData: MapData = {
          key: element.settingId,
          value: element.settingName,
        };
        if (element.isDefault) {
          elementMapData.value = element.settingName + " (Default)";
          if (this.filters.selectedSavedGrid == null) {
            this.filters.selectedSavedGrid = elementMapData;
          }
        } else if (element.isShared)
          elementMapData.value = element.settingName + " (Shared)";
        savedGridData.push(elementMapData);
        this.savedGridDataMap[element.settingId] = JSON.parse(
          element.settingData
        );
      }
    });
    this.savedGrids = savedGridData;
  };

  getDisplayColumnsArray = (newDisplayColumnSet): any[] => {
    let newDisplayColumnsArray: any[] = [];
    newDisplayColumnsArray = [...newDisplayColumnSet];
    return newDisplayColumnsArray;
  };

  enrichWithRuleDataWhenMarginTypeSelected = (marginType: number) => {
    if (marginType == MARGIN_TYPES.House) {
      this.setPopUpSelectedDataWithRuleData(this.houseTypeRuleData);
    } else if (marginType == MARGIN_TYPES.Exchange) {
      this.setPopUpSelectedDataWithRuleData(this.exchangeTypeRuleData);
    } else if (marginType == MARGIN_TYPES.Regulatory) {
      this.setPopUpSelectedDataWithRuleData(this.regTypeRuleData);
    } else if (marginType == MARGIN_TYPES.Segregated_IA) {
      this.setPopUpSelectedDataWithRuleData(this.segTypeRuleData);
    } else if (marginType == MARGIN_TYPES.Regulatory_CPE) {
      this.setPopUpSelectedDataWithRuleData(this.regCpeTypeRuleData);
    }
  };

  enrichWithRuleDataWhenRuleTypeSelected = (ruleType: number) => {
    let selectedMarginType = this.modifyRulesStore.popUpSelectedData[
      "selectedMarginTypes"
    ]?.key;

    if (
      selectedMarginType == MARGIN_TYPES.House &&
      this.houseTypeRuleData &&
      this.houseTypeRuleData.ruleType &&
      ruleType === this.houseTypeRuleData.ruleType.id
    ) {
      this.setPopUpSelectedDataWithRuleData(this.houseTypeRuleData);
    } else if (
      selectedMarginType == MARGIN_TYPES.Exchange &&
      this.exchangeTypeRuleData &&
      this.exchangeTypeRuleData.ruleType &&
      ruleType === this.exchangeTypeRuleData.ruleType.id
    ) {
      this.setPopUpSelectedDataWithRuleData(this.exchangeTypeRuleData);
    } else if (
      selectedMarginType == MARGIN_TYPES.Regulatory &&
      this.regTypeRuleData &&
      this.regTypeRuleData.ruleType &&
      ruleType === this.regTypeRuleData.ruleType.id
    ) {
      this.setPopUpSelectedDataWithRuleData(this.regTypeRuleData);
    } else if (
      selectedMarginType == MARGIN_TYPES.Segregated_IA &&
      this.segTypeRuleData &&
      this.segTypeRuleData.ruleType &&
      ruleType === this.segTypeRuleData.ruleType.id
    ) {
      this.setPopUpSelectedDataWithRuleData(this.segTypeRuleData);
    } else if (
      selectedMarginType == MARGIN_TYPES.Regulatory_CPE &&
      this.regCpeTypeRuleData &&
      this.regCpeTypeRuleData.ruleType &&
      ruleType === this.regCpeTypeRuleData.ruleType.id
    ) {
      this.setPopUpSelectedDataWithRuleData(this.regCpeTypeRuleData);
    }
  };

  setPopupDataWithDefaultPositionRuleData = (
    ruleTypeData?: PositionRuleData
  ) => {
    this.isPositionRuleAvailable = false;
    this.modifyRulesStore.popUpSelectedData["selectedStartDate"] = DATE_19000101;
    this.modifyRulesStore.popUpSelectedData["selectedEndDate"] = DATE_20380101;
    this.modifyRulesStore.popUpSelectedData[
      "selectedRuleTypes"
    ] = POSITION_RULE_TYPES_REF_DATA;
    this.modifyRulesStore.popUpSelectedData[
      "selectedUsageTypes"
    ] = DEFAULT_USAGE_TYPE;
    this.modifyRulesStore.popUpSelectedData["selectedUsageCurrency"] = this
      .selectedRow
      ? {
          key: this.selectedRow.currencyId,
          value: this.selectedRow.currencyIsoCode,
        }
      : null;
    this.modifyRulesStore.popUpSelectedData[
      "selectedPnlSpn"
    ] = this.selectedRow?.pnlSpn?.toString();

    this.modifyRulesStore.popUpSelectedData["selectedComments"] = "";

    this.modifyRulesStore.popUpSelectedData[
      "selectedMarginCalculators"
    ] = this.modifyRulesStore.positionMaster;

    if (
      ruleTypeData &&
      (ruleTypeData.positionAllocator !== null &&
        ruleTypeData.positionAllocator !== undefined)
    ) {
      this.modifyRulesStore.popUpSelectedData["selectedAllocators"] = {
        key: getIdFromRefData(ruleTypeData?.positionAllocator),
        value: getNameFromRefData(ruleTypeData.positionAllocator),
      };
    } else {
      this.modifyRulesStore.popUpSelectedData[
        "selectedAllocators"
      ] = this.modifyRulesStore.defaultAllocator;
    }

    if (
      ruleTypeData?.ruleType &&
      getIdFromRefData(ruleTypeData.ruleType) === POSITION_RULE_ID
    ) {
      this.modifyRulesStore.popUpSelectedData["selectedRuleId"] =
        ruleTypeData.ruleId;
      this.modifyRulesStore.operation = UPDATE_MARGIN_RULE;
    } else {
      this.modifyRulesStore.popUpSelectedData["selectedRuleId"] = null;
      this.modifyRulesStore.operation = ADD_MARGIN_RULE;
    }

    this.modifyRulesStore.marginRuleKey = ruleTypeData
      ? this.getMarginRuleKey(ruleTypeData)
      : null;
  };

  setPopUpSelectedDataWithRuleData = (ruleTypeData?: PositionRuleData) => {
    this.isPositionRuleAvailable = true;
    this.modifyRulesStore.popUpSelectedData["selectedStartDate"] =
      ruleTypeData && ruleTypeData.effectiveStartDate ? convertJavaDate(ruleTypeData?.effectiveStartDate) : DATE_19000101;
    this.modifyRulesStore.popUpSelectedData["selectedEndDate"] =
    ruleTypeData && ruleTypeData.effectiveEndDate ? convertJavaDate(ruleTypeData?.effectiveEndDate) : DATE_20380101;
    this.modifyRulesStore.popUpSelectedData["selectedUsageTypes"] = ruleTypeData
      ? {
          key: getIdFromRefData(ruleTypeData?.usageType),
          value: getNameFromRefData(ruleTypeData?.usageType),
        }
      : null;
    this.modifyRulesStore.popUpSelectedData[
      "selectedUsageCurrency"
    ] = ruleTypeData
      ? {
          key: getIdFromRefData(ruleTypeData?.usageCurrency),
          value: getNameFromRefData(ruleTypeData?.usageCurrency),
        }
      : null;
    this.modifyRulesStore.popUpSelectedData["selectedUsageHaircut"] =
      ruleTypeData && ruleTypeData.usageHaircut !== undefined
        ? ruleTypeData.usageHaircut.toString()
        : "";
    this.modifyRulesStore.popUpSelectedData["selectedComments"] =
      ruleTypeData && ruleTypeData.userComments !== undefined
        ? ruleTypeData.userComments.toString()
        : "";
    this.modifyRulesStore.popUpSelectedData[
      "selectedMarginCalculators"
    ] = ruleTypeData
      ? {
          key: getIdFromRefData(ruleTypeData?.calculator),
          value: getNameFromRefData(ruleTypeData?.calculator),
        }
      : null;
    this.modifyRulesStore.popUpSelectedData["selectedAllocators"] = ruleTypeData
      ? {
          key: getIdFromRefData(ruleTypeData?.allocator),
          value: getNameFromRefData(ruleTypeData?.allocator),
        }
      : null;
    this.modifyRulesStore.popUpSelectedData["selectedUsername"] =
      ruleTypeData && ruleTypeData.userName
        ? ruleTypeData.userName.toString()
        : "";
    this.modifyRulesStore.popUpSelectedData["selectedRuleTypes"] =
      ruleTypeData &&
      getIdFromRefData(ruleTypeData?.ruleType) === POSITION_RULE_ID
        ? {
            key: getIdFromRefData(ruleTypeData?.ruleType),
            value: getNameFromRefData(ruleTypeData?.ruleType),
          }
        : null;

    this.modifyRulesStore.popUpSelectedData["selectedRuleId"] =
      ruleTypeData &&
      getIdFromRefData(ruleTypeData?.ruleType) === POSITION_RULE_ID
        ? ruleTypeData.ruleId
        : null;
    this.modifyRulesStore.operation = this.modifyRulesStore.popUpSelectedData[
      "selectedRuleId"
    ]
      ? UPDATE_MARGIN_RULE
      : ADD_MARGIN_RULE;

    if (getIdFromRefData(ruleTypeData?.ruleType) !== POSITION_RULE_ID) {
      this.setPopupDataWithDefaultPositionRuleData(ruleTypeData);
    }

    this.modifyRulesStore.marginRuleKey = ruleTypeData
      ? this.getMarginRuleKey(ruleTypeData)
      : null;
  };

  checkValidPageFiltersResponse = (pageFilterData: any): boolean => {
    if (pageFilterData && pageFilterData.message) {
      if (
        pageFilterData.message.toLowerCase().includes("no") &&
        pageFilterData.message.toLowerCase().includes("valid") &&
        pageFilterData.message.toLowerCase().includes("agreement")
      ) {
        this.setSearchStatus(
          STATUS.FAILURE,
          MESSAGES.INVALID_AGREEMENT_MESSAGE
        );
      } else if (
        pageFilterData.message.toLowerCase().includes("date") &&
        pageFilterData.message.toLowerCase().includes("range") &&
        pageFilterData.message.toLowerCase().includes("support") &&
        pageFilterData.message.toLowerCase().includes("one") &&
        pageFilterData.message.toLowerCase().includes("valid") &&
        pageFilterData.message.toLowerCase().includes("agreement")
      ) {
        this.setSearchStatus(
          STATUS.FAILURE,
          MESSAGES.INVALID_DATE_RANGE_CHECK_MESSAGE
        );
      } else {
        this.setSearchStatus(STATUS.SUCCESS, MESSAGES.NO_DATA_FOUND_MESSAGE);
      }
      return false;
    }

    return true;
  };

  loadPositionData = async (filters, isParametersInUrl) => {
    this.inProgress = true;
    this.isParametersInUrl = isParametersInUrl;
    try {
      let positionDetailDataFilter = convertFilterObjectsToIdsJson(filters);
      let enrichWithPrevDayLcmPositionData = isEnrichWithPrevDayLcmPositionData(
        positionDetailDataFilter,
        isParametersInUrl
      );
      let maxRecordToDisplayInGrid = enrichWithPrevDayLcmPositionData
        ? MAX_PREV_DAY_RECORDS_TO_DISPLAY_IN_GRID
        : MAX_RECORDS_TO_DISPLAY_IN_GRID;

      this.useOptimizedPdr = await PositionDetailDataAPI.isUseOptimizedPdr();

      let pdrFilters: PositionDetailDataFilter[] = await PositionDetailDataAPI.getPdrFilters(
        positionDetailDataFilter,
        enrichWithPrevDayLcmPositionData,
        this.useOptimizedPdr
      );

      this.pdrFilters = pdrFilters;

      if (pdrFilters.length === 0) {
        this.inProgress = false;
        this.setSearchStatus(STATUS.SUCCESS, MESSAGES.RETRY_CACHE_MESSAGE);
        return;
      }

      if (
        pdrFilters.length === 1 &&
        pdrFilters[0].isNoDataFoundFromCache === true
      ) {
        this.inProgress = false;
        this.setSearchStatus(STATUS.SUCCESS, MESSAGES.NO_DATA_FOUND_MESSAGE);
        return;
      }

      if (!this.checkValidPageFiltersResponse(pdrFilters)) {
        this.inProgress = false;
        return;
      }

      pdrFilters.forEach(
        (_, index) =>
          (pdrFilters[index].enrichWithRCFields =
            positionDetailDataFilter.enrichWithRCFields)
      );

      let pdrDataBodies: string[] = [];
      let url = getPositionDetailUrl(
        enrichWithPrevDayLcmPositionData,
        this.useOptimizedPdr
      );
      let numberOfApiCallsMade =
        pdrFilters.length > MAX_API_CALLS_FOR_LCM_POSITION_DATA
          ? MAX_API_CALLS_FOR_LCM_POSITION_DATA
          : pdrFilters.length;

      let dataSetColumns: string[] = [];

      if (this.useOptimizedPdr) {
        this.datasetColumns.forEach((column) => {
          if (column) dataSetColumns.push(column.identity);
        });
        dataSetColumns.push("countryId");
        dataSetColumns.push("fieldTypeId");
        dataSetColumns.push("marketId");
        dataSetColumns.push("legalEntityId");
        dataSetColumns.push("exposureCounterPartyId");
        dataSetColumns.push("agreementTypeId");
        dataSetColumns.push("bookId");
        dataSetColumns.push("gboTypeId");
        dataSetColumns.push("underlyingGboTypeId");
        dataSetColumns.push("custodianAccountId");
        dataSetColumns.push("currencyId");
      }

      for (let i = 0; i < numberOfApiCallsMade; i++) {
        pdrFilters[i].requiredFields = dataSetColumns;
        let bodyNew = getPositionDetailCallBody(
          pdrFilters[i],
          enrichWithPrevDayLcmPositionData,
          dataSetColumns
        );
        pdrDataBodies.push(bodyNew);
      }

      var {
        positionDetailDataList,
        retryPdrDataBodies,
      } = await this.getLcmPositionDetailDataList(
        pdrDataBodies,
        url,
        enrichWithPrevDayLcmPositionData,
        filters.isPositionLevel,
        positionDetailDataFilter.enrichWithRCFields,
        this.useOptimizedPdr
      );

      if (retryPdrDataBodies.length > 0) {
        var retryPositionDetailData = await this.getLcmPositionDetailDataList(
          retryPdrDataBodies,
          url,
          enrichWithPrevDayLcmPositionData,
          filters.isPositionLevel,
          positionDetailDataFilter.enrichWithRCFields,
          this.useOptimizedPdr
        );
        positionDetailDataList = positionDetailDataList.concat(
          retryPositionDetailData.positionDetailDataList
        );
      }

      var avgPageSize: number =
        positionDetailDataList.length /
        (MAX_API_CALLS_FOR_LCM_POSITION_DATA - 1);
      var parallelCallsToBeMade: number = Math.ceil(
        (maxRecordToDisplayInGrid - positionDetailDataList.length) / avgPageSize
      );
      parallelCallsToBeMade =
        parallelCallsToBeMade > MAX_API_CALLS_FOR_LCM_POSITION_DATA
          ? MAX_API_CALLS_FOR_LCM_POSITION_DATA
          : parallelCallsToBeMade;

      while (
        positionDetailDataList.length < maxRecordToDisplayInGrid &&
        numberOfApiCallsMade < pdrFilters.length
      ) {
        pdrDataBodies = [];
        for (let i = 0; i < parallelCallsToBeMade; i++) {
          if (numberOfApiCallsMade + i < pdrFilters.length) {
            pdrFilters[
              numberOfApiCallsMade + i
            ].requiredFields = dataSetColumns;
            let bodyNew = getPositionDetailCallBody(
              pdrFilters[numberOfApiCallsMade + i],
              isParametersInUrl,
              dataSetColumns
            );
            pdrDataBodies.push(bodyNew);
          }
        }

        var subPositionDetailData = await this.getLcmPositionDetailDataList(
          pdrDataBodies,
          url,
          enrichWithPrevDayLcmPositionData,
          filters.isPositionLevel,
          positionDetailDataFilter.enrichWithRCFields,
          this.useOptimizedPdr
        );
        positionDetailDataList = positionDetailDataList.concat(
          subPositionDetailData.positionDetailDataList
        );

        if (subPositionDetailData.retryPdrDataBodies.length > 0) {
          var retryPositionDetailData = await this.getLcmPositionDetailDataList(
            retryPdrDataBodies,
            url,
            enrichWithPrevDayLcmPositionData,
            filters.isPositionLevel,
            positionDetailDataFilter.enrichWithRCFields,
            this.useOptimizedPdr
          );
          positionDetailDataList = positionDetailDataList.concat(
            retryPositionDetailData.positionDetailDataList
          );
        }

        numberOfApiCallsMade += parallelCallsToBeMade;
      }

      // Restricting the number of data rows to 50000 since react supports not more 100000 records in a single page
      positionDetailDataList = positionDetailDataList.slice(
        0,
        maxRecordToDisplayInGrid
      );
      this.positionDetailDataList = positionDetailDataList;

      this.inProgress = false;

      if (numberOfApiCallsMade < pdrFilters.length) {
        this.isEnabledDownloadPdrReport = await PositionDetailDataAPI.isEnabledDownloadPdrReport();
      }

      if (this.positionDetailDataList.length === 0) {
        this.isPositionDetailListEmpty = true;
        this.setSearchStatus(STATUS.SUCCESS, MESSAGES.NO_DATA_FOUND_MESSAGE);
      }
    } catch (e) {
      this.inProgress = false;
      this.setSearchStatus(STATUS.FAILURE, MESSAGES.ERROR_MESSAGE);
    }
  };

  getLcmPositionDetailDataList = async (
    pdrDataBodies: string[],
    url: string,
    enrichWithPrevDayLcmPositionData: boolean,
    isPositionLevel: boolean,
    enrichWithRCFields?: boolean,
    useOptimizedPdr?: boolean
  ) => {
    let retryPdrDataBodies: string[] = [];
    let positionDetailDataList: LcmPositionDataKeyValue[] = [];

    const dataParts: any[] = await Promise.allSettled(
      pdrDataBodies.map((pdrDataBody) => this.fetchPdrData(url, pdrDataBody))
    );
    let retryIndex = 0;
    for (let k = 0; k < dataParts.length; k++) {
      if (
        dataParts[k].status !== FULFILLED_STATUS ||
        (dataParts[k].value.status !== TOO_MANY_REQUESTS_STATUS_CODE &&
          dataParts[k].value.status !== SUCCESS_STATUS_CODE)
      ) {
        continue;
      }
      if (dataParts[k].value.status === TOO_MANY_REQUESTS_STATUS_CODE) {
        retryPdrDataBodies[retryIndex++] = pdrDataBodies[k];
        continue;
      }

      if (useOptimizedPdr) {
        positionDetailDataList = positionDetailDataList.concat(
          getPositionDetailDataFromArrayTableOptimized(
            dataParts[k].value.data,
            enrichWithPrevDayLcmPositionData,
            !isPositionLevel,
            enrichWithRCFields
          )
        );
      } else {
        positionDetailDataList = positionDetailDataList.concat(
          getPositionDetailDataFromArrayTable(
            dataParts[k].value.data,
            enrichWithPrevDayLcmPositionData,
            !isPositionLevel,
            enrichWithRCFields
          )
        );
      }
    }
    return { positionDetailDataList, retryPdrDataBodies };
  };

  fetchPdrData = async (url: string, pdrBody: any) => {
    await sema.acquire();
    try {
      console.log(sema.nrWaiting() + " calls to fetch are waiting");

      const response = await fetch(url, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        method: "POST",
        credentials: "include",
        body: pdrBody,
      }).then((resp) => {
        sema.release();
        return resp;
      });

      const data = await handleResponse(response);
      return data;
    } catch (error) {
      console.log(error);
    }
  };

  populateCache = async (filters) => {
    this.inProgress = true;
    let positionDetailDataFilter = convertFilterObjectsToIdsJson(filters);
    let lcmPositionDataParam = encodeURIComponent(
      JSON.stringify(positionDetailDataFilter)
    );

    let filterUrl = `${BASE_URL}service/${SERVICE_URLS.positionDetailReportDataReadManagerService.checkAndPopulateCache}?inputFormat=JSON&format=JSON`;

    const response = await fetch(filterUrl, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
      credentials: "include",
      body: `lcmPositionDataParam=${lcmPositionDataParam}`,
    }).then((resp) => {
      return resp;
    });

    const data = response;

    if (data.status !== SUCCESS_STATUS_CODE) {
      this.inProgress = false;
      this.setSearchStatus(STATUS.FAILURE, MESSAGES.ERROR_MESSAGE);
    } else if (data) {
      this.handleSearch();
    } else {
      this.inProgress = false;
      this.isPositionDetailListEmpty = true;
      this.setSearchStatus(STATUS.FAILURE, MESSAGES.RETRY_CACHE_MESSAGE);
    }
  };

  getMarginLatestPositionDetailDataList = async (
    marginDataBodies: string[],
    url: string
  ) => {
    let retryDataBodies: string[] = [];
    let latestMarginDataList: PositionMarginData[] = [];

    await Promise.allSettled(
      marginDataBodies.map((bodyEach) =>
        fetch(url, {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          method: "POST",
          credentials: "include",
          body: bodyEach,
        }).then((resp) => handleResponse(resp))
      )
    ).then(async (dataParts: any) => {
      let retryIndex = 0;
      for (let k = 0; k < dataParts.length; k++) {
        if (
          dataParts[k].status !== FULFILLED_STATUS ||
          (dataParts[k].value.status !== TOO_MANY_REQUESTS_STATUS_CODE &&
            dataParts[k].value.status !== SUCCESS_STATUS_CODE)
        ) {
          continue;
        }
        if (dataParts[k].value.status === TOO_MANY_REQUESTS_STATUS_CODE) {
          retryDataBodies[retryIndex++] = marginDataBodies[k];
          continue;
        }
        latestMarginDataList = latestMarginDataList.concat(
          getMarginDataFromArrayTable(dataParts[k].value.data)
        );
      }
    });
    return { latestMarginDataList, retryDataBodies };
  };

  loadLatestMarginPositionData = async (filters) => {
    this.inProgress = true;
    try {
      let marginDataParam = convertFilterObjectsToMarginDataJson(filters);
      let marginFilters: MarginDataParam[] = await PositionDetailDataAPI.getMarginDataFilters(
        marginDataParam
      );

      if (!this.checkValidPageFiltersResponse(marginFilters)) {
        this.inProgress = false;
        return;
      }

      let pdrDataBodies: string[] = [];
      let url = getMarginDataUrl();
      let numberOfApiCallsMade =
        marginFilters.length > MAX_API_CALLS_FOR_MARGIN_LATEST_DATA
          ? MAX_API_CALLS_FOR_MARGIN_LATEST_DATA
          : marginFilters.length;

      for (let i = 0; i < numberOfApiCallsMade; i++) {
        let bodyNew = getMarginDataCallBody(marginFilters[i]);
        pdrDataBodies.push(bodyNew);
      }

      var {
        latestMarginDataList,
        retryDataBodies,
      } = await this.getMarginLatestPositionDetailDataList(pdrDataBodies, url);

      if (retryDataBodies.length > 0) {
        var retryPositionDetailData = await this.getMarginLatestPositionDetailDataList(
          retryDataBodies,
          url
        );
        latestMarginDataList = latestMarginDataList.concat(
          retryPositionDetailData.latestMarginDataList
        );
      }

      // Restricting the number of data rows to 50000 since react supports not more 100000 records in a single page
      this.latestMarginDataList = latestMarginDataList.slice(
        0,
        MAX_RECORDS_TO_DISPLAY_IN_GRID
      );

      this.inProgress = false;
      if (latestMarginDataList.length === 0) {
        this.setSearchStatus(STATUS.SUCCESS, MESSAGES.NO_DATA_FOUND_MESSAGE);
      }
    } catch (e) {
      console.log(e);
      this.inProgress = false;
      this.setSearchStatus(STATUS.FAILURE, MESSAGES.ERROR_MESSAGE);
    }
  };

  @action.bound
  getTradeIndependentAmountDetails = async (row: any) => {
    try {
      this.inProgress = true;
      this.tradeIndependentAmountGridData = [];
      this.createTradeIAFilterData(row);
      this.tradeIndependentAmountList = await getTradeIndependentAmountDetail(
        this.tradeIndependentAmountFilter
      );

      // Restricting the number of data rows to 50000 since react supports not more 100000 records in a single page
      this.tradeIndependentAmountList = this.tradeIndependentAmountList.slice(
        0,
        MAX_RECORDS_TO_DISPLAY_IN_GRID
      );
      this.tradeIndependentAmountGridData = getGridDataFromTradeIndependentAmountData(
        this.tradeIndependentAmountList
      );

      if (this.tradeIndependentAmountGridData.length === 0) {
        showToastService(
          MESSAGES.NO_TRADE_IA_DATA_FOUND_MESSAGE,
          TOAST_TYPE.CRITICAL
        );
      }
      this.inProgress = false;
    } catch (error) {
      showToastService(MESSAGES.TRADE_IA_ERROR_MESSAGE, TOAST_TYPE.CRITICAL);
      this.inProgress = false;
    }
  };

  createTradeIAFilterData = (row: any) => {
    this.tradeIndependentAmountFilter.bookIds = [row.bookId];
    this.tradeIndependentAmountFilter.counterPartyIds = [
      row.exposureCounterPartyId,
    ];
    this.tradeIndependentAmountFilter.currencyIds = [row.currencyId];
    this.tradeIndependentAmountFilter.custodianAccountsIds = [
      row.custodianAccountId,
    ];
    this.tradeIndependentAmountFilter.endDate = row.date.replaceAll("-", "");
    this.tradeIndependentAmountFilter.gboTypeIds = [row.gboTypeId];
    this.tradeIndependentAmountFilter.legalEntityIds = [row.legalEntityId];
    this.tradeIndependentAmountFilter.pnlSpns = [row.pnlSpn];
    this.tradeIndependentAmountFilter.startDate = "19000101";
  };
}

export default PositionDetailDataStore;
