import ArcDataGrid from "arc-data-grid";
import { Layout } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import { DEFAULT_AGGREGATION, DEFAULT_SORT_BY } from "../grid/GridColumns";
import { getGridOptions } from "../grid/GridOptions";
import {
  isEnrichWithPrevDayLcmPositionData,
  messageType,
} from "../utils/PositionDetailUtil";
import { useStore } from "../useStore";
import { Message } from "arc-react-components";
import TradeIAGrid from "../component/TradeIAGrid";
import { Panel } from "arc-react-components";
import SaveSettingsManager from "../../commons/components/SaveSettingsManager";
import ModifyRuleDialog from "../../MarginRules/container/ModifyRuleDialog";
import { DEFAULT_PINNED_COLUMNS } from "../constants";
import _ from "lodash";
import { DATASET_TYPE, MESSAGES } from "../constants";

const PositionDetailReportGrid: React.FC<any> = () => {
  const { positionDetailDataStore } = useStore();
  let data: object[] = positionDetailDataStore.latestMarginDataList.length
    ? positionDetailDataStore.latestMarginDataList
    : positionDetailDataStore.positionDetailDataList;
  let showTradeIAGrid: boolean =
    positionDetailDataStore.tradeIndependentAmountGridData.length > 0
      ? true
      : false;

  if (data && data.length && data.length != 0) {
    return (
      <>
        <div style={{ textAlign: "right", height: "5%" }}>
          <span
            style={{
              height: "auto",
              width: "50%",
              display: "inline-block",
              float: "left",
            }}
          >
            <SaveSettingsManager
              selectedFilters={positionDetailDataStore.displayColumns}
              applySavedFilters={positionDetailDataStore.setDisplayColumns}
              applicationName={
                positionDetailDataStore.filters.selectedDataset?.key +
                "_positionDetailReport"
              }
            />
          </span>
          <span>
            <button
              className="size--content margin--left--small"
              style={{ height: "auto", float: "right" }}
              onClick={positionDetailDataStore.generateReportFromAPI}
            >
              Create Report
            </button>
          </span>
          {!(positionDetailDataStore.selectedDatasetKey === DATASET_TYPE.Margin_Details &&
            !positionDetailDataStore.filters.isPublished) && <span>
            <button
              className="size--content margin--left--small"
              style={{ height: "auto", float: "right" }}
              onClick={positionDetailDataStore.handleCacheRefresh}
            >
              Refresh Cache
            </button>
          </span>}
          <span>
            <a onClick={positionDetailDataStore.handleSearch}>
              <i title="Refresh Data" className="icon-refresh"></i>Refresh Data
            </a>
          </span>
        </div>
        {positionDetailDataStore.isEnabledDownloadPdrReport &&
        <Message type={Message.Type.WARNING}>
          <div style={{ marginTop: '5px', marginBottom: '5px' }}>
              Showing top 50,000 records. Click the button to download all data records for the search criteria.
            <button style={{marginLeft:'10px'}} disabled={!positionDetailDataStore.allowReportDownload} onClick = {positionDetailDataStore.downloadReport} > Download Report </button>
          </div>
        </Message>
        }
        <Layout style={{ height: "94%" }}>
          <Layout.Child childId="PositionDetailReportGridChild1">
            {renderGridData(data, positionDetailDataStore)}
          </Layout.Child>
          {showTradeIAGrid && (
            <Layout.Divider
              childId="PositionDetailReportGridChild2"
              isResizable
            />
          )}
          {showTradeIAGrid && (
            <Layout.Child
              childId="PositionDetailReportGridChild3"
              collapsible
              collapsed={positionDetailDataStore.toggleTradeIAGrid}
              onChange={positionDetailDataStore.setToggleTradeIAGrid}
            >
              <Layout isColumnType={false} className="border">
                <Layout.Child childId="PositionDetailReportGridChild4">
                  <Panel title="Additional Details">
                    <TradeIAGrid />
                  </Panel>
                </Layout.Child>
              </Layout>
            </Layout.Child>
          )}
        </Layout>
        <div>
          <ModifyRuleDialog
            onSelect={positionDetailDataStore.modifyRuleStoreOnSelect}
            isReadOnly={true}
            selectedRow={positionDetailDataStore.selectedRow}
            isPositionRuleAvailable={positionDetailDataStore.isPositionRuleAvailable}
          />
        </div>
      </>
    );
  }
  else {
    return (
      <Message type={messageType(positionDetailDataStore.searchStatus.status)}>
        {positionDetailDataStore.searchStatus.message !== MESSAGES.RETRY_CACHE_MESSAGE &&
        <div style={{ marginTop: "5px", marginBottom: "5px" }}>
          {positionDetailDataStore.searchStatus.message}
        </div>}
        {positionDetailDataStore.searchStatus.message === MESSAGES.RETRY_CACHE_MESSAGE &&
          <div style={{ marginTop: "5px", marginBottom: "5px" }}>
          {positionDetailDataStore.searchStatus.message}
          <button onClick = {positionDetailDataStore.handleCacheRefresh} > Refresh Cache </button>
        </div>
        }
      </Message>
    );
  }
};

const renderGridData = (dataParameter, positionDetailDataStore) => {
  let enrichWithPrevDayLcmPositionData = isEnrichWithPrevDayLcmPositionData(
    positionDetailDataStore.filters,
    positionDetailDataStore.isParametersInUrl
  );

  let defaultAggregatedColumns = DEFAULT_AGGREGATION;

  if (enrichWithPrevDayLcmPositionData) {
    _.forIn(defaultAggregatedColumns, (value, key) => {
      key = "prevDay" + key;
      defaultAggregatedColumns[key] = value;
    });
  }

  let grid = (
    <>
      <ArcDataGrid
        rows={dataParameter}
        columns={positionDetailDataStore.gridDisplayColumnsArray}
        configurations={getGridOptions}
        aggregation={defaultAggregatedColumns}
        pinnedColumns={DEFAULT_PINNED_COLUMNS}
        displayColumns={positionDetailDataStore.displayColumns}
        onDisplayColumnsChange={positionDetailDataStore.setDisplayColumns}
        sortBy={DEFAULT_SORT_BY}
        clickedRow={positionDetailDataStore.clickedRow}
        onClickedRowChange={(clickedRow) => {
          positionDetailDataStore.setClickedRow(clickedRow);
        }}
      />
    </>
  );
  return grid;
};

export default observer(PositionDetailReportGrid);
