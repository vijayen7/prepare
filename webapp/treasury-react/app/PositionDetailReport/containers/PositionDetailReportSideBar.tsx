import { Sidebar } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import CheckboxFilter from "../../commons/components/CheckboxFilter";
import FilterButton from "../../commons/components/FilterButton";
import SaveSettingsManager from "../../commons/components/SaveSettingsManager";
import AgreementTypeFilter from "../../commons/container/AgreementTypeFilter";
import CpeFilter from "../../commons/container/CpeFilter";
import GBOTypeFilter from "../../commons/container/GBOTypeFilter";
import CustodianAccountFilter from "../../commons/container/CustodianAccountFilter";
import CurrencyFilter from "../../commons/container/CurrencyFilter";
import DateFilter from "../../commons/filters/components/DateFilter";
import GenericSecurityFilter from "../../commons/components/GenericSecurityFilter";
import BusinessUnitFilter from "../../commons/container/BusinessUnitFilter";
import BundleFilter from "../../commons/container/BundleFilter";
import SingleSelectFilter from "../../commons/filters/components/SingleSelectFilter";
import { useStore } from "../useStore";
import { Card } from "arc-react-components";
import { ToggleGroup } from "arc-react-components";
import { BUNDLE_DATASETS, DATASETS, FIELD_TYPES, REPORTING_CURRENCY_FILTER_DATA, DATASET_TYPE } from "../constants";
import MultiSelectFilter from "../../commons/filters/components/MultiSelectFilter";

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

const PositionDetailReportSideBar: React.FC<any> = () => {
  const { positionDetailDataStore } = useStore();
  let clientName = CODEX_PROPERTIES["codex.client_name"];

  return (
    <Sidebar
      header={false}
      footer={
        <>
          <FilterButton
            onClick={positionDetailDataStore.handleSearch}
            label="Search"
          />
          <FilterButton
            reset={true}
            onClick={positionDetailDataStore.handleReset}
            label="Reset"
          />
          <FilterButton
            reset={true}
            onClick={positionDetailDataStore.handleCopySearch}
            label="Copy Search URL"
          />
        </>
      }
    >
      <Card>
        <SaveSettingsManager
          selectedFilters={positionDetailDataStore.filters}
          applySavedFilters={positionDetailDataStore.applySavedFilters}
          applicationName="positionDetailReport"
        />
      </Card>

      <Card>
        <DateFilter
          label="From:"
          stateKey="selectedFromDate"
          data={positionDetailDataStore.filters.selectedFromDate}
          onChange={positionDetailDataStore.onSelect}
        />
      </Card>

      <Card>
        <DateFilter
          label="To:"
          stateKey="selectedToDate"
          data={positionDetailDataStore.filters.selectedToDate}
          onChange={positionDetailDataStore.onSelect}
        />
      </Card>

      <Card>
        <MultiSelectFilter
          data={positionDetailDataStore.selectedLegalEntityFamilies}
          onSelect={positionDetailDataStore.onSelect}
          label="Legal Entity Families"
          selectedData={positionDetailDataStore.filters.selectedLegalEntityFamilies}
          stateKey="selectedLegalEntityFamilies"
          horizontalLayout
        />
        <MultiSelectFilter
          data={positionDetailDataStore.selectedLegalEntityData}
          onSelect={positionDetailDataStore.onSelect}
          label="Legal Entities"
          selectedData={positionDetailDataStore.filters.selectedLegalEntities}
          stateKey="selectedLegalEntities"
          horizontalLayout
        />
        <CpeFilter
          horizontalLayout
          multiSelect
          onSelect={positionDetailDataStore.onSelect}
          selectedData={positionDetailDataStore.filters.selectedCpes}
          stateKey="selectedCpes"
        />
        <AgreementTypeFilter
          horizontalLayout
          multiSelect
          onSelect={positionDetailDataStore.onSelect}
          selectedData={positionDetailDataStore.filters.selectedAgreementTypes}
          stateKey="selectedAgreementTypes"
        />
        <MultiSelectFilter
          horizontalLayout
          label="Field Type"
          data={FIELD_TYPES}
          onSelect={positionDetailDataStore.onSelect}
          stateKey="selectedFieldTypes"
          selectedData={positionDetailDataStore.filters.selectedFieldTypes}
        />
        <MultiSelectFilter
          data={positionDetailDataStore.selectedBookData}
          label="Books"
          horizontalLayout
          multiSelect
          onSelect={positionDetailDataStore.onSelect}
          selectedData={positionDetailDataStore.filters.selectedBooks}
          stateKey="selectedBooks"
        />
        <CustodianAccountFilter
          onSelect={positionDetailDataStore.onSelect}
          selectedData={
            positionDetailDataStore.filters.selectedCustodianAccounts
          }
          horizontalLayout
          multiSelect
          stateKey="selectedCustodianAccounts"
        />
        <CurrencyFilter
          label="Currencies"
          onSelect={positionDetailDataStore.onSelect}
          selectedData={positionDetailDataStore.filters.selectedCurrencies}
          horizontalLayout
          multiSelect
          stateKey="selectedCurrencies"
        />
        <GBOTypeFilter
          horizontalLayout={true}
          multiSelect
          onSelect={positionDetailDataStore.onSelect}
          selectedData={positionDetailDataStore.filters.selectedGboTypes}
          stateKey="selectedGboTypes"
        />
        <SingleSelectFilter
          onSelect={positionDetailDataStore.onSelect}
          selectedData={positionDetailDataStore.filters.selectedReportingCurrency}
          stateKey="selectedReportingCurrency"
          data={REPORTING_CURRENCY_FILTER_DATA}
          label="Reporting Currency*"
          horizontalLayout={true}
        />
      </Card>

      <Card>
        <GenericSecurityFilter
          onSelect={positionDetailDataStore.onSelect}
          selectedData={positionDetailDataStore.filters.securityFilter}
          stateKey="securityFilter"
        />
      </Card>
      <Card>
        <ToggleGroup
          value={positionDetailDataStore.filters.isPositionLevel}
          onChange={(value: boolean) => {
            positionDetailDataStore.setIsPositionLevel(value);
          }}
        >
          <ToggleGroup.Button key="1" data={true}>
            Position
          </ToggleGroup.Button>
          <ToggleGroup.Button key="2" data={false}>
            Bundle Position
          </ToggleGroup.Button>
        </ToggleGroup>
        <SingleSelectFilter
          onSelect={positionDetailDataStore.onDatasetSelect}
          stateKey="selectedDataset"
          label="Dataset*"
          selectedData={positionDetailDataStore.filters.selectedDataset}
          data={
            (clientName === 'desco' || clientName === 'deshaw') && positionDetailDataStore.filters.isPositionLevel
              ? DATASETS
              : BUNDLE_DATASETS
          }
          horizontalLayout={true}
        />
        <SingleSelectFilter
          data={positionDetailDataStore.savedGrids}
          onSelect={positionDetailDataStore.onSelect}
          stateKey="selectedSavedGrid"
          label="Saved Grids"
          horizontalLayout={true}
          selectedData={positionDetailDataStore.filters.selectedSavedGrid}
        />
      </Card>
      {!positionDetailDataStore.filters.isPositionLevel && (
        <Card>
          <BusinessUnitFilter
            onSelect={positionDetailDataStore.onSelect}
            selectedData={positionDetailDataStore.filters.selectedBusinessUnits}
            multiSelect
            stateKey="selectedBusinessUnits"
            horizontalLayout
          />
          <BundleFilter
            onSelect={positionDetailDataStore.onSelect}
            selectedData={positionDetailDataStore.filters.selectedBundles}
            multiSelect
            stateKey="selectedBundles"
            horizontalLayout
          />
        </Card>
      )}
      <Card>
        {positionDetailDataStore.filters.isPositionLevel && (
          positionDetailDataStore.filters.isPublished || positionDetailDataStore.selectedDatasetKey !== DATASET_TYPE.Margin_Details
        ) && (
          <CheckboxFilter
            onSelect={positionDetailDataStore.onSelect}
            stateKey="includeUnmappedBrokerRecord"
            label="Include Unmapped Positions"
            defaultChecked={
              positionDetailDataStore.filters.includeUnmappedBrokerRecord
            }
            style="left"
          />
        )}
        <CheckboxFilter
          onSelect={positionDetailDataStore.onSelect}
          stateKey="includeExtendedSecurityAttributes"
          label="Include Extended Security Attributes"
          defaultChecked={
            positionDetailDataStore.filters.includeExtendedSecurityAttributes
          }
          style="left"
        />
        <div>
          {"Data Milestone:"}
          <span style={{ float: "right" }}>
            <ToggleGroup
              value={positionDetailDataStore.filters.isPublished}
              onChange={(value: boolean) => {
                positionDetailDataStore.setIsPublished(value);
              }}
              className="padding--horizontal"
            >
              <ToggleGroup.Button key="1" data={true}>
                Published
              </ToggleGroup.Button>
              <ToggleGroup.Button key="2" data={false}>
                Latest
              </ToggleGroup.Button>
            </ToggleGroup>
          </span>
        </div>
      </Card>
    </Sidebar>
  );
};

export default observer(PositionDetailReportSideBar);
