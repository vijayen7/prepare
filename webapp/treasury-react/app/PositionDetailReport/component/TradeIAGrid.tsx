import ArcDataGrid from "arc-data-grid";
import { Layout } from "arc-react-components";
import { observer } from "mobx-react-lite";
import React from "react";
import { DEFAULT_SORT_BY, TRADE_IA_COLUMNS } from "../grid/GridColumns";
import { getGridOptions } from "../grid/TradeIAGridOptions";
import { useStore } from "../useStore";

const TradeIAGrid: React.FC<any> = () => {
  return (
    <Layout>
      <Layout.Child childId="TradeIAGridChild1">
        {renderGridData()}
      </Layout.Child>
    </Layout>
  );
};

const renderGridData = () => {
  const { positionDetailDataStore } = useStore();

  let grid = (
    <>
      <ArcDataGrid
        rows={positionDetailDataStore.tradeIndependentAmountGridData}
        columns={TRADE_IA_COLUMNS}
        configurations={getGridOptions}
        displayColumns={positionDetailDataStore.displayTradeIAColumns}
        onDisplayColumnsChange={positionDetailDataStore.onDisplayColumnsChange}
        sortBy={DEFAULT_SORT_BY}
      />
    </>
  );
  return grid;
};

export default observer(TradeIAGrid);
