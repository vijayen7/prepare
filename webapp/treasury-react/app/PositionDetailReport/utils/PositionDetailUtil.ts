import { BASE_URL } from "../../commons/constants";
import _ from "lodash";
import {
  getSelectedIdListWithNull,
  getCommaSeparatedListValue,
} from "../../commons/util";
import { SearchFilter } from "../models/UIDataTypes";
import { PositionDetailDataFilter } from "../models/PositionDetailDataFilter";
import { Message } from "arc-react-components";
import { CLASS_NAMESPACE } from "../../commons/ClassConfigs";
import { getMarginData } from "./MarginDetailsUtil";
import {
  getExtendedSecurityAttr,
  getSecurityAttr,
  getUnderlyingSecurityAttr,
} from "./SecurityDataUtil";
import { getBrokerSecurityData } from "./BrokerSecurityDataUtil";
import {
  getExposureReconData,
  getInternalExposureData,
} from "./ExposureDataUtil";
import {
  CURR_DAY_PREFIX,
  PREV_DAY_PREFIX,
  USD_SUFFIX,
  RC_SUFFIX,
} from "../constants";
import {
  getGenericColumnsData,
  getNumericColumnsData,
  getDateColumnsData,
} from "./CommonUtil";
import SERVICE_URLS, { COMPONENT_URLS } from "../../commons/UrlConfigs";

export const numberConverter = (value) => {
  if (value === undefined || value === null || isNaN(value)) return undefined;
  return Number(value);
};

const getDayOnDayDiffColumns = (
  prefixString: string,
  positionDetailDataFilter: PositionDetailDataFilter
) => {
  let dayOnDayDiffColumns =
    getPositionDetailsFields(
      prefixString,
      positionDetailDataFilter.enrichWithRCFields
    ) +
    "," +
    getSecurityDetailsFields(prefixString) +
    "," +
    getComponentKeyDetailsFields(prefixString) +
    "," +
    getBrokerPositionDetailsFields(
      prefixString,
      positionDetailDataFilter.enrichWithRCFields
    ) +
    "," +
    getExposureDetailsFields(
      prefixString,
      positionDetailDataFilter.enrichWithRCFields
    ) +
    "," +
    getMarginDetailsFields(
      prefixString,
      positionDetailDataFilter.enrichWithRCFields
    ) +
    "," +
    getCpePosReconDetailsFields(prefixString) +
    "," +
    getReconValues(prefixString);

  if (positionDetailDataFilter.fetchBundlePositionsOnly) {
    dayOnDayDiffColumns += "," + getBundlePositionValues(prefixString);
  }

  return dayOnDayDiffColumns + ",";
};

export const getSelectedIdList = (data) => {
  return _.isEmpty(data) ? [] : data.map((a) => (a === -1 ? -1 : a.key));
};

const getApplicableFields = (
  parameterString: string,
  prefixString: string,
  suffixString: string
) => {
  let isPrevDay: boolean = false;
  if (prefixString.length > 0) {
    isPrevDay = true;
  }
  let applicableParameters;
  applicableParameters = parameterString
    .split(",")
    .map(String)
    .map((eachValue) => prefixString + eachValue + suffixString);
  let applicableParameterString: string;
  applicableParameterString = applicableParameters.join(",");
  return applicableParameterString;
};

const getPositionDetailsFields = (
  prefixString: string,
  enrichWithRCFields?: boolean
) => {
  let suffixString = enrichWithRCFields ? RC_SUFFIX : USD_SUFFIX;
  return (
    getApplicableFields(
      "componentKey.date,componentKey.custodianAccount.familyLegalEntity.name," +
        "legalEntity,legalEntityId,exposureAgreementId,mnaAgreementId," +
        "exposureCounterparty,expCounterPartyId,agreementType," +
        "agreementTypeId,security.currencyId,security.countryName," +
        "security.marketName,reportingCurrency,security.death," +
        "security.currencyAbbreviation,quantity,settleDateQuantity," +
        "reqHairCut,brokerDetail.brokerPositionDetails.spn," +
        "financedQuantity,unFinancedQuantity,fxRate,reportingCurrencyFxRate," +
        "regFinancedQuantity,regUnfinancedQuantity,lcmPositionDetail.mktVal," +
        "lcmPositionDetail.quantityDiff,lcmPositionDetail.priceDiff," +
        "lcmPositionDetail.settleDateMarketValueRC,lcmPositionDetail.settleDateMarketValue," +
        "lcmPositionDetail.marginDiffRC,lcmPositionDetail.marginDiff," +
        "lcmPositionDetail.priceDiffRC,lcmPositionDetail.priceDiffPercent," +
        "lcmPositionDetail.exposureDiffRC,lcmPositionDetail.exposureDiff",
      prefixString,
      ""
    ) +
    "," +
    getApplicableFields(
      "lcmPositionDetail.price,lcmPositionDetail.mktVal," +
        "lcmPositionDetail.regAdjustedUsage," +
        "lcmPositionDetail.exposureAdjustment,lcmPositionDetail.adjustedExposure," +
        "lcmPositionDetail.requirementAdjustment,lcmPositionDetail.adjustedRequirement," +
        "lcmPositionDetail.regRequirementAdjustment,lcmPositionDetail.regAdjustedRequirement," +
        "lcmPositionDetail.usageAdjustment,lcmPositionDetail.adjustedUsage," +
        "lcmPositionDetail.regUsageAdjustment",
      prefixString,
      suffixString
    )
  );
};

const getSecurityDetailsFields = (prefixString: string) => {
  return getApplicableFields(
    "security.spn,security.spotSpn,security.bondSpn," +
      "security.issuerSpn,security.underlying,underlyingSecurity.isin," +
      "underlyingSecurity.sedol,underlyingSecurity.cusip,underlyingSecurity.spn," +
      "underlyingSecurity.ric,underlyingSecurity.local," +
      "underlyingSecurity.bloomberg,underlyingSecurity.gboTypeId," +
      "underlyingSecurity.gboTypeName,security.parentSpn," +
      "security.ultimateParentSpn,security.sfsTypeName," +
      "security.countryId,security.marketId,componentKey.fieldId," +
      "componentKey.fieldType,componentKey.bookId," +
      "componentKey.calculator,componentKey.calculatorId," +
      "componentKey.calculatorStatus,componentKey.regCalculator," +
      "componentKey.regCpeCalculator,componentKey.regCalculatorStatus," +
      "componentKey.regCpeCalculatorStatus,componentKey.gboType.name," +
      "componentKey.gboType.id,componentKey.custodianAccount.counterpartyEntity.name," +
      "componentKey.exposureCustodianAccount.displayName," +
      "componentKey.custodianAccount.displayName," +
      "componentKey.custodianAccountId,componentKey.book.displayName",
    prefixString,
    ""
  );
};

const getComponentKeyDetailsFields = (prefixString: string) => {
  return getApplicableFields(
    "security.ric,security.local,security.foTypeName," +
      "security.subtypeName,security.desname,security.gboTickUnit," +
      "security.sedol,security.cusip,security.isin,security.bloomberg",
    prefixString,
    ""
  );
};

const getExposureDetailsFields = (
  prefixString: string,
  enrichWithRCFields?: boolean
) => {
  let suffixString;
  if (enrichWithRCFields) suffixString = RC_SUFFIX;
  else suffixString = "";
  return (
    getApplicableFields(
      "exposureDetail.repoPositionExposureData.repoExposureRC,exposureDetail.repoPositionExposureData.repoMarginRC" +
        ",exposureDetail.exposureRC" +
        ",exposureDetail.repoPositionExposureData.exposure" +
        ",exposureDetail.repoPositionExposureData.margin" +
        ",exposureDetail.exposureUsd" +
        ",exposureDetail.repoPositionExposureData.poolFactor" +
        ",exposureDetail.repoPositionExposureData.onDate" +
        ",exposureDetail.isdaPositionMtmData.resetDate" +
        ",exposureDetail.isdaPositionMtmData.resetSettleDate" +
        ",exposureDetail.repoPositionExposureData.offDate",
      prefixString,
      ""
    ) +
    "," +
    getApplicableFields(getExposureDataFields(), prefixString, suffixString)
  );
};

const getExposureDataFields = () => {
  return (
    "exposureDetail.fcmPositionOteData.ote" +
    ",exposureDetail.repoPositionExposureData.cleanPrice" +
    ",exposureDetail.repoPositionExposureData.dirtyPrice" +
    ",exposureDetail.repoPositionExposureData.loanAI" +
    ",exposureDetail.repoPositionExposureData.bondAI" +
    ",exposureDetail.repoPositionExposureData.loanValue" +
    ",exposureDetail.isdaPositionMtmData.tradeReturnFromLastReset" +
    ",exposureDetail.isdaPositionMtmData.unrealizedAI" +
    ",exposureDetail.isdaPositionMtmData.unrealizedCommissions" +
    ",exposureDetail.isdaPositionMtmData.unrealizedCorpact" +
    ",exposureDetail.isdaPositionMtmData.unrealizedFinancing" +
    ",exposureDetail.isdaPositionMtmData.unrealizedPriceReturn" +
    ",exposureDetail.isdaPositionMtmData.overdueReceivablePayable" +
    ",exposureDetail.isdaPositionMtmData.markToMarket"
  );
};

const getMarginDetailsFields = (
  prefixString: string,
  enrichWithRCFields?: boolean
) => {
  let suffixString;
  if (enrichWithRCFields) suffixString = RC_SUFFIX;
  else suffixString = USD_SUFFIX;
  return (
    getApplicableFields(
      "marginDetail.margin,marginDetail.applicableMargin" +
        ",marginDetail.regFinancedMargin,marginDetail.regUnFinancedMargin" +
        ",marginDetail.unFinancedMargin,marginDetail.financedMargin" +
        ",lcmPositionDetail.underlyingValue" +
        ",marginDetail.regMargin,marginDetail.regCpeMargin" +
        ",marginDetail.marketValue" +
        ",marginDetail.regUsage" +
        ",marginDetail.regCpeUsage,marginDetail.usage",
      prefixString,
      suffixString
    ) +
    "," +
    getApplicableFields(
      "lcmPositionDetail.underlyingValue,marginDetail.usageNative,marginDetail.usageHaircut" +
        ",marginDetail.marginNative,marginDetail.quantity,marginDetail.price,marginDetail.haircut,marginDetail.marketValue" +
        ",marginDetail.regMarginHaircut,marginDetail.regUsageHaircut,marginDetail.regUsageNative" +
        ",marginDetail.regCpeMarginNative,marginDetail.regCpeMarginHaircut" +
        ",marginDetail.regCpeUsageHaircut,marginDetail.regCpeUsageNative,marginDetail.regMarginNative",
      prefixString,
      ""
    )
  );
};

const getCpePosReconDetailsFields = (prefixString: string) => {
  return getApplicableFields(
    "cpeReconciledPositionsData.recStatus,cpeReconciledPositionsData.cpeQuantity," +
      "cpeReconciledPositionsData.cpePrice,cpeReconciledPositionsData.cpeShortMarketValue," +
      "cpeReconciledPositionsData.marginUSD,cpeReconciledPositionsData.cpeMarketValue," +
      "cpeReconciledPositionsData.cpeLongMarketValue,cpeReconciledPositionsData.recCodes," +
      "cpeReconciledPositionsData.segMarginUSD,cpeReconciledPositionsData.regMarginUSD," +
      "cpeReconciledPositionsData.exchangeMarginUSD," +
      "cpeReconciledRepoData.loanAmount,cpeReconciledRepoData.bondAccruedInterest," +
      "cpeReconciledRepoData.bondPrice,cpeReconciledRepoData.loanAccruedInterest," +
      "cpeReconciledRepoData.bondMarketValue,cpeReconciledRepoData.haircut," +
      "cpeReconciledRepoData.bondQuantity,cpeReconciledRepoData.exposure," +
      "cpeReconciledRepoData.poolFactor,cpeReconciledRepoData.recCodes" ,
    prefixString,
    ""
  );
};

const getBrokerPositionDetailsFields = (
  prefixString: string,
  enrichWithRCFields?: boolean
) => {
  let parameterString;
  if (enrichWithRCFields)
    parameterString =
      "brokerDetail.brokerPositionDetails.brokerMarginRC,brokerDetail.brokerPositionDetails.brokerRegMarginRC," +
      "brokerDetail.brokerPositionDetails.brokerRegExposureRC,brokerDetail.brokerPositionDetails.brokerExposureRC," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUnderlyingStrikePriceRC," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerSpreadRC," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerSpotSpnStrikePriceRC," +
      "brokerDetail.brokerPositionDetails.brokerMarketValueRC,brokerDetail.brokerPositionDetails.brokerApplicableMarginRC," +
      "brokerDetail.brokerPositionDetails.brokerRegulatoryApplicableMarginRC," +
      "brokerDetail.brokerPositionDetails.brokerRegCpeMarginRC,brokerDetail.brokerPositionDetails.brokerRegCpeApplicableMarginRC,";
  else
    parameterString =
      "brokerDetail.brokerPositionDetails.marginUsd,brokerDetail.brokerPositionDetails.regMarginUsd," +
      "brokerDetail.brokerPositionDetails.regExposureUsd,brokerDetail.brokerPositionDetails.exposureUsd," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingStrikePrice," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.spread," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.strikePrice," +
      "brokerDetail.brokerPositionDetails.marketValueUsd,brokerDetail.brokerPositionDetails.applicableMarginUsd," +
      "brokerDetail.brokerPositionDetails.regulatoryApplicableMarginUsd," +
      "brokerDetail.brokerPositionDetails.regCpeMarginUsd,brokerDetail.brokerPositionDetails.regCpeApplicableMarginUsd,";

  return getApplicableFields(
    parameterString +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.ric," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.birth," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.local," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.death," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.isin," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.cusip," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.sedol," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.ameriEuroInd," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUniqueKey," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.bloomberg," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingDeath," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIsin," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSedol," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingLocal," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingRic," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingBloomberg," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingLocal," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingRic," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardEndDate," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardSettleDate," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.expirationDate," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingPutCallInd," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingExpirationDate," +
      "brokerDetail.brokerPositionDetails.requirementHaircutPct," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency1," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency2," +
      "brokerDetail.brokerPositionDetails.matchValue," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapStart," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapLevel," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn1," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn2," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity1," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity2," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.repoRate," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.accruedDate," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.buySell," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.instrumentName," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCanonicalSpnRic," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSpotSpnLocal," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIssuerSpnRedCode," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnIsin," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnCusip," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnIsin," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnCusip," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnIsin," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnLocal," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnRic," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnBloomberg," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.putCallInd," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.onDate," +
      "brokerDetail.brokerPositionDetails.brokerSecurityDetail.offDate," +
      "brokerDetail.brokerPositionDetails.priceNative,brokerDetail.brokerPositionDetails.regCurrencyCode," +
      "brokerDetail.brokerPositionDetails.matchKey,brokerDetail.brokerPositionDetails.marketValueLocal," +
      "brokerDetail.brokerPositionDetails.marketValueCurrencyCode,brokerDetail.brokerPositionDetails.marketValueCurrencyId," +
      "brokerDetail.brokerPositionDetails.quantity,brokerDetail.brokerPositionDetails.marginCurrency," +
      "brokerDetail.brokerPositionDetails.marginFxRate,brokerDetail.brokerPositionDetails.regFxRate," +
      "brokerDetail.brokerPositionDetails.securityName,brokerDetail.brokerPositionDetails.product," +
      "brokerDetail.brokerPositionDetails.tradeDate,brokerDetail.brokerPositionDetails.maturityDate," +
      "brokerDetail.brokerPositionDetails.fxRate",
    prefixString,
    ""
  );
};

const getReconValues = (prefixString: string) => {
  return getApplicableFields(
    "lcmReconDiffDetail.internalVsReconDiffDetail.priceDiff," +
      "lcmReconDiffDetail.reconVsBrokerDiffDetail.priceDiff," +
      "lcmReconDiffDetail.internalVsReconDiffDetail.priceDiffPercent," +
      "lcmReconDiffDetail.reconVsBrokerDiffDetail.priceDiffPercent," +
      "lcmReconDiffDetail.internalVsReconDiffDetail.quantityDiff," +
      "lcmReconDiffDetail.reconVsBrokerDiffDetail.quantityDiff," +
      "lcmReconDiffDetail.internalVsReconDiffDetail.marginDiff," +
      "lcmReconDiffDetail.reconVsBrokerDiffDetail.marginDiff," +
      "lcmReconDiffDetail.internalVsReconDiffDetail.exposureDiff," +
      "lcmReconDiffDetail.reconVsBrokerDiffDetail.exposureDiff",
    prefixString,
    ""
  );
};

const getBundlePositionValues = (prefixString: string) => {
  return getApplicableFields(
    "componentKey.bundle.name,componentKey.businessUnit.name",
    prefixString,
    ""
  );
};

export const getPositionDetailDataFromArrayTable = (
  data: any,
  enrichWithPrevDayLcmPositionData: boolean,
  fetchBundlePositionsOnly: boolean,
  enrichWithRCFields?: boolean
) => {
  if (data && data.data && data.data.length) {
    let fieldsArr: string[] = [];
    data.fields.forEach((e) => fieldsArr.push(e.name));
    data = data.data.map((value) => _.zipObject(fieldsArr, value));
    if (enrichWithPrevDayLcmPositionData && !fetchBundlePositionsOnly) {
      data = data.map((row) => ({
        ...getAgreementMetaData(row, CURR_DAY_PREFIX),
        ...getAgreementMetaData(row, PREV_DAY_PREFIX),
        ...getSecurityAttr(row, CURR_DAY_PREFIX),
        ...getSecurityAttr(row, PREV_DAY_PREFIX),
        ...getUnderlyingSecurityAttr(row, CURR_DAY_PREFIX),
        ...getUnderlyingSecurityAttr(row, PREV_DAY_PREFIX),
        ...getMarginData(row, CURR_DAY_PREFIX, enrichWithRCFields),
        ...getMarginData(row, PREV_DAY_PREFIX, enrichWithRCFields),
        ...getInternalExposureData(row, CURR_DAY_PREFIX, enrichWithRCFields),
        ...getInternalExposureData(row, PREV_DAY_PREFIX, enrichWithRCFields),
        ...getExposureReconData(row, CURR_DAY_PREFIX, enrichWithRCFields),
        ...getExposureReconData(row, PREV_DAY_PREFIX, enrichWithRCFields),
        ...getBrokerSecurityData(row, CURR_DAY_PREFIX, enrichWithRCFields),
        ...getBrokerSecurityData(row, PREV_DAY_PREFIX, enrichWithRCFields),
        ...getPositionDetailData(row, CURR_DAY_PREFIX),
        ...getPositionDetailData(row, PREV_DAY_PREFIX),
      }));
    } else if (!enrichWithPrevDayLcmPositionData && !fetchBundlePositionsOnly) {
      data = data
        .filter(
          (row) =>
            row.agreementType !== null &&
            row.legalEntity !== null &&
            row.exposureCounterParty !== null
        )
        .map((row) => ({
          ...getAgreementMetaData(row, ""),
          ...getSecurityAttr(row, ""),
          ...getExtendedSecurityAttr(row),
          ...getUnderlyingSecurityAttr(row, ""),
          ...getMarginData(row, "", enrichWithRCFields),
          ...getInternalExposureData(row, "", enrichWithRCFields),
          ...getExposureReconData(row, "", enrichWithRCFields),
          ...getBrokerSecurityData(row, "", enrichWithRCFields),
          ...getPositionDetailData(row, ""),
        }));
    } else if (!enrichWithPrevDayLcmPositionData && fetchBundlePositionsOnly) {
      data = data
        .filter(
          (row) =>
            row.agreementType !== null &&
            row.legalEntity !== null &&
            row.exposureCounterParty !== null
        )
        .map((row) => ({
          ...getAgreementMetaData(row, ""),
          ...getSecurityAttr(row, ""),
          ...getExtendedSecurityAttr(row),
          ...getUnderlyingSecurityAttr(row, ""),
          ...getMarginData(row, "", enrichWithRCFields),
          ...getInternalExposureData(row, "", enrichWithRCFields),
          ...getPositionDetailData(row, ""),
          ...getBundlePositionDetailData(row, ""),
        }));
    } else if (enrichWithPrevDayLcmPositionData && fetchBundlePositionsOnly) {
      data = data.map((row) => ({
        ...getAgreementMetaData(row, CURR_DAY_PREFIX),
        ...getAgreementMetaData(row, PREV_DAY_PREFIX),
        ...getSecurityAttr(row, CURR_DAY_PREFIX),
        ...getSecurityAttr(row, PREV_DAY_PREFIX),
        ...getUnderlyingSecurityAttr(row, CURR_DAY_PREFIX),
        ...getUnderlyingSecurityAttr(row, PREV_DAY_PREFIX),
        ...getMarginData(row, CURR_DAY_PREFIX, enrichWithRCFields),
        ...getMarginData(row, PREV_DAY_PREFIX, enrichWithRCFields),
        ...getInternalExposureData(row, CURR_DAY_PREFIX, enrichWithRCFields),
        ...getInternalExposureData(row, PREV_DAY_PREFIX, enrichWithRCFields),
        ...getPositionDetailData(row, CURR_DAY_PREFIX),
        ...getPositionDetailData(row, PREV_DAY_PREFIX),
        ...getBundlePositionDetailData(row, CURR_DAY_PREFIX),
        ...getBundlePositionDetailData(row, PREV_DAY_PREFIX),
      }));
    }

    return data;
  } else {
    return [];
  }
};


export const getPositionDetailDataFromArrayTableOptimized = (
  data: any,
  enrichWithPrevDayLcmPositionData: boolean,
  fetchBundlePositionsOnly: boolean,
  enrichWithRCFields?: boolean
) => {
  if (data && data.data && data.data.length) {
    let fieldsArr: string[] = [];
    data.fields.forEach((e) => fieldsArr.push(e.name));
    data = data.data.map((value) => _.zipObject(fieldsArr, value));
    if (enrichWithPrevDayLcmPositionData) {
      data = data.map((row) => ({
        ...getPdrColumnsData(row, CURR_DAY_PREFIX),
        ...getPdrColumnsData(row, PREV_DAY_PREFIX)
      }));
    } else {
      data = data.map((row) =>
        getPdrColumnsData(row, ""),
      );
    }

    return data;
  } else {
    return [];
  }
};

export const getDayOnDayDiffParam = (
  enrichWithPrevDayLcmPositionData: boolean,
  positionDetailDataFilter: PositionDetailDataFilter
) => {
  let dayOnDayDiffParam = "_arrayTable=true&_fieldNames=";
  if (enrichWithPrevDayLcmPositionData === true) {
    dayOnDayDiffParam +=
      getDayOnDayDiffColumns(CURR_DAY_PREFIX, positionDetailDataFilter) +
      CURR_DAY_PREFIX +
      "extendedSecurity," +
      getDayOnDayDiffColumns(PREV_DAY_PREFIX, positionDetailDataFilter);
  } else {
    dayOnDayDiffParam +=
      getDayOnDayDiffColumns("", positionDetailDataFilter) +
      "extendedSecurity,";
  }
  return dayOnDayDiffParam;
};

export const convertFilterObjectsToIds = (
  filters: SearchFilter
): PositionDetailDataFilter => {
  const parameters: PositionDetailDataFilter = {
    "@CLASS":
      CLASS_NAMESPACE.deshaw.treasury.common.model.comet.LcmPositionDataParam,
    legalEntityIds: getSelectedIdList(filters.selectedLegalEntities),
    legalEntityFamilyIds: getSelectedIdList(filters.selectedLegalEntityFamilies),
    date: filters.selectedFromDate,
    startDate: filters.selectedFromDate,
    endDate: filters.selectedToDate,
    cpeIds: getSelectedIdList(filters.selectedCpes),
    agreementTypeIds: getSelectedIdList(filters.selectedAgreementTypes),
    custodianAccountIds: getSelectedIdList(filters.selectedCustodianAccounts),
    currencyIds: getSelectedIdList(filters.selectedCurrencies),
    fieldTypeIds: getSelectedIdList(filters.selectedFieldTypes),
    bookIds: getSelectedIdList(filters.selectedBooks),
    gboTypeIds: getSelectedIdList(filters.selectedGboTypes),
    liveOnly: filters.liveOnly ? filters.liveOnly : false,
    enrichWithPrevDayLcmPositionData: filters.isCrimsonEnabled
      ? filters.isCrimsonEnabled
      : false,
    getOnlyLatestData: !filters.isPublished,
    includeUnmappedBrokerRecord: filters.includeUnmappedBrokerRecord,
    excessDeficitDataType: filters.excessDeficitDataType
      ? filters.excessDeficitDataType
      : false,
    showSegDrilldown: filters.showSegDrilldown
      ? filters.showSegDrilldown
      : false,
    fromExcessDeficitDataSource: filters.fromExcessDeficitDataSource
      ? filters.fromExcessDeficitDataSource
      : false,
    toExcessDeficitDataSource: filters.toExcessDeficitDataSource
      ? filters.toExcessDeficitDataSource
      : false,
    enrichWithExtendedSecurityAttributes:
      filters.includeExtendedSecurityAttributes,
    dataSet: filters.selectedDataset?.key,
    dataSetId: filters.selectedDataset?.key,
    fetchBundlePositionsOnly: !filters.isPositionLevel,
    bundleIds: getSelectedIdList(filters.selectedBundles),
    businessUnitIds: getSelectedIdList(filters.selectedBusinessUnits),
    enrichWithRCFields: Boolean(filters.selectedReportingCurrency.key)
  };

  if (
    filters.securityFilter?.isAdvancedSearch &&
    filters.securityFilter.securitySearchString.trim() !== ""
  ) {
    parameters.securityFilter = {
      "@CLASS":
        CLASS_NAMESPACE.com.arcesium.treasury.model.common.security
          .SecurityFilter,
      securitySearchType:
        filters.securityFilter.selectedSecuritySearchType.value,
      textSearchType: filters.securityFilter.selectedTextSearchType.value,
      searchStrings: getCommaSeparatedListValue(
        filters.securityFilter.securitySearchString
      ).split(","),
    };
  } else {
    parameters.pnlSpns =
      filters.securityFilter?.selectedSpns &&
      filters.securityFilter.selectedSpns.trim() !== ""
        ? getCommaSeparatedListValue(filters.securityFilter.selectedSpns).split(
            ","
          )
        : null;
  }
  return parameters;
};

export const convertFilterObjectsToIdsJson = (
  filters: SearchFilter
): PositionDetailDataFilter => {
  const parameters: PositionDetailDataFilter = {
    "@CLASS":
      CLASS_NAMESPACE.deshaw.treasury.common.model.comet.LcmPositionDataParam,
    legalEntityIds: getSelectedIdListWithNull(filters.selectedLegalEntities),
    legalEntityFamilyIds: getSelectedIdListWithNull(filters.selectedLegalEntityFamilies),
    date: filters.selectedFromDate,
    startDate: filters.selectedFromDate,
    endDate: filters.selectedToDate,
    cpeIds: getSelectedIdListWithNull(filters.selectedCpes),
    agreementTypeIds: getSelectedIdListWithNull(filters.selectedAgreementTypes),
    bookIds: getSelectedIdListWithNull(filters.selectedBooks),
    gboTypeIds: getSelectedIdListWithNull(filters.selectedGboTypes),
    custodianAccountIds: getSelectedIdListWithNull(
      filters.selectedCustodianAccounts
    ),
    currencyIds: getSelectedIdListWithNull(filters.selectedCurrencies),
    fieldTypeIds: getSelectedIdListWithNull(filters.selectedFieldTypes),
    liveOnly: filters.liveOnly ? filters.liveOnly : false,
    enrichWithPrevDayLcmPositionData: filters.isCrimsonEnabled
      ? filters.isCrimsonEnabled
      : false,
    getOnlyLatestData: !filters.isPublished,
    includeUnmappedBrokerRecord: filters.includeUnmappedBrokerRecord,
    excessDeficitDataType: filters.excessDeficitDataType
      ? filters.excessDeficitDataType
      : false,
    showSegDrilldown: filters.showSegDrilldown
      ? filters.showSegDrilldown
      : false,
    fromExcessDeficitDataSource: filters.fromExcessDeficitDataSource
      ? filters.fromExcessDeficitDataSource
      : false,
    toExcessDeficitDataSource: filters.toExcessDeficitDataSource
      ? filters.toExcessDeficitDataSource
      : false,
    enrichWithExtendedSecurityAttributes:
      filters.includeExtendedSecurityAttributes,
    dataSet: filters.selectedDataset?.key,
    dataSetId: filters.selectedDataset?.key,
    fetchBundlePositionsOnly: !filters.isPositionLevel,
    bundleIds: getSelectedIdListWithNull(filters.selectedBundles),
    businessUnitIds: getSelectedIdListWithNull(filters.selectedBusinessUnits),
    enrichWithRCFields: Boolean(filters.selectedReportingCurrency.key)
  };
  if (
    filters.securityFilter?.isAdvancedSearch &&
    filters.securityFilter.securitySearchString.trim() !== ""
  ) {
    parameters.securityFilter = {
      "@CLASS":
        CLASS_NAMESPACE.com.arcesium.treasury.model.common.security
          .SecurityFilter,
      securitySearchType:
        filters.securityFilter.selectedSecuritySearchType.value,
      textSearchType: filters.securityFilter.selectedTextSearchType.value,
      searchStrings: getCommaSeparatedListValue(
        filters.securityFilter.securitySearchString
      ).split(","),
    };
  } else {
    parameters.pnlSpns =
      filters.securityFilter?.selectedSpns &&
      filters.securityFilter.selectedSpns.trim() !== ""
        ? getCommaSeparatedListValue(filters.securityFilter.selectedSpns).split(
            ","
          )
        : null;
  }
  return parameters;
};

export const getApplicablePositionDetailUrl = (
  location,
  parameters: PositionDetailDataFilter,
  selectedDatasetKey: number,
  isParametersInUrl: boolean,
  selectedSharedGridKey: number,
  selectedReportingCurrencyKey: number
) => {
  let url;
  let isCrimsonEnabled = parameters.enrichWithPrevDayLcmPositionData === true;
  let showCrimsonDiffDrilldown = isShowCrimsonDiffDrilldown(parameters);
  let enrichWithPrevDayLcmPositionData = isEnrichWithPrevDayLcmPositionData(
    parameters,
    isParametersInUrl
  );
  if (isCrimsonEnabled) {
    if (showCrimsonDiffDrilldown) {
      url =
        `${BASE_URL}service/lcmExcessDeficitService/getCrimsonDiffDrilldownPositionsLight?` +
        `crimsonDrilldownPositionsInputParam.legalEntityIds=` +
        parameters.legalEntityIds +
        `&crimsonDrilldownPositionsInputParam.cpeIds=` +
        parameters.cpeIds +
        `&crimsonDrilldownPositionsInputParam.agreementTypeIds=` +
        parameters.agreementTypeIds +
        `&crimsonDrilldownPositionsInputParam.date=` +
        parameters.date +
        `&crimsonDrilldownPositionsInputParam.liveOnly=` +
        parameters.liveOnly +
        `&crimsonDrilldownPositionsInputParam.includeUnmappedBrokerRecord=` +
        parameters.includeUnmappedBrokerRecord +
        `&crimsonDrilldownPositionsInputParam.excessDeficitDataType=` +
        parameters.excessDeficitDataType +
        `&crimsonDrilldownPositionsInputParam.showSegDrilldown=` +
        parameters.showSegDrilldown +
        `&crimsonDrilldownPositionsInputParam.fromExcessDeficitDataSource=` +
        parameters.fromExcessDeficitDataSource +
        `&crimsonDrilldownPositionsInputParam.toExcessDeficitDataSource=` +
        parameters.toExcessDeficitDataSource +
        `&crimsonDrilldownPositionsInputParam.addCalculatedFields=` +
        true +
        `&crimsonDrilldownPositionsInputParam.enrichWithPrevDayLcmPositionData=` +
        enrichWithPrevDayLcmPositionData +
        `&format=JSON&inputFormat=PROPERTIES`;
    } else {
      //we dont want to display prev day position data if user has opened crimson diff drilldown for an agreement
      url = getDefaultPositionDetailUrl(
        location,
        isParametersInUrl,
        parameters,
        selectedDatasetKey,
        enrichWithPrevDayLcmPositionData,
        selectedSharedGridKey,
        selectedReportingCurrencyKey
      );
    }
  } else {
    url = getDefaultPositionDetailUrl(
      location,
      isParametersInUrl,
      parameters,
      selectedDatasetKey,
      enrichWithPrevDayLcmPositionData,
      selectedSharedGridKey,
      selectedReportingCurrencyKey
    );
  }
  return url;
};

export const isEnrichWithPrevDayLcmPositionData = (
  parameters: PositionDetailDataFilter,
  isParametersInUrl: boolean
): boolean => {
  let enrichWithPrevDayLcmPositionData = false;
  if (!isParametersInUrl) {
    return enrichWithPrevDayLcmPositionData;
  }
  let isCrimsonEnabled = parameters.enrichWithPrevDayLcmPositionData === true;
  let showCrimsonDiffDrilldown = isShowCrimsonDiffDrilldown(parameters);
  if (isCrimsonEnabled && !showCrimsonDiffDrilldown) {
    enrichWithPrevDayLcmPositionData = true;
  }
  return enrichWithPrevDayLcmPositionData;
};

export const isShowCrimsonDiffDrilldown = (
  parameters: PositionDetailDataFilter
) => {
  let fromExcessDeficitDataSource = parameters.fromExcessDeficitDataSource;
  let toExcessDeficitDataSource = parameters.toExcessDeficitDataSource;
  let excessDeficitDataType = parameters.excessDeficitDataType;
  let showSegDrilldown = parameters.showSegDrilldown;
  let showCrimsonDiffDrilldown =
    fromExcessDeficitDataSource &&
    toExcessDeficitDataSource &&
    excessDeficitDataType &&
    showSegDrilldown != undefined;
  return showCrimsonDiffDrilldown;
};

export const getDefaultPositionDetailUrl = (
  location,
  isParametersInUrl: boolean,
  parameters: PositionDetailDataFilter,
  selectedDatasetKey: number,
  enrichWithPrevDayLcmPositionData: boolean,
  selectedSavedGridKey: number,
  selectedReportingCurrencyKey: number
) => {
  let url = isParametersInUrl
    ? location + `${COMPONENT_URLS.POSITION_DETAIL_REPORT}?`
    : `${SERVICE_URLS.positionDetailReportDataReadManagerService.getPdrData}?`;
  url =
    url +
    `legalEntityIds=` +
    parameters.legalEntityIds +
    `&legalEntityFamilyIds=` +
    parameters.legalEntityFamilyIds +
    `&cpeIds=` +
    parameters.cpeIds +
    `&agreementTypeIds=` +
    parameters.agreementTypeIds +
    `&startDate=` +
    parameters.startDate +
    `&endDate=` +
    parameters.endDate +
    `&liveOnly=` +
    true +
    `&includeUnmappedBrokerRecord=` +
    parameters.includeUnmappedBrokerRecord +
    `&enrichWithPrevDayLcmPositionData=` +
    enrichWithPrevDayLcmPositionData +
    `&addCalculatedFields=` +
    true +
    `&dataSet=` +
    selectedDatasetKey +
    "&reportingCurrencyKey=" +
    selectedReportingCurrencyKey +
    `&includeExtendedSecurityAttributes=` +
    parameters.enrichWithExtendedSecurityAttributes +
    `&bookIds=` +
    parameters.bookIds +
    `&custodianAccountIds=` +
    parameters.custodianAccountIds +
    `&isPublished=` +
    !parameters.getOnlyLatestData +
    `&isPositionLevel=` +
    !parameters.fetchBundlePositionsOnly +
    `&businessUnitIds=` +
    parameters.businessUnitIds +
    `&bundleIds=` +
    parameters.bundleIds +
    `&fieldTypeIds=` +
    parameters.fieldTypeIds +
    `&gboTypeIds=` +
    parameters.gboTypeIds +
    `&currencyIds=` +
    parameters.currencyIds;

  if (parameters.pnlSpns != undefined && parameters.pnlSpns.length !== 0) {
    url += `&pnlSpns=` + parameters.pnlSpns;
  } else if (
    parameters.securityFilter?.searchStrings != undefined &&
    parameters.securityFilter.searchStrings.length !== 0
  ) {
    url +=
      `&securitySearchType=` +
      parameters.securityFilter?.securitySearchType +
      `&searchStrings=` +
      parameters.securityFilter?.searchStrings +
      `&textSearchType=` +
      parameters.securityFilter?.textSearchType +
      `&isAdvancedSearch=true`;
  }

  if (selectedSavedGridKey !== 0) {
    url += `&sharedSavedGrid=` + selectedSavedGridKey;
  }
  url += `&format=Json&inputFormat=PROPERTIES`;
  return url;
};

export const getPositionDetailUrl = (
  enrichWithPrevDayLcmPositionData: boolean,
  useOptimizedPdr?: boolean
) => {
  let filterUrl: string;

  if (!enrichWithPrevDayLcmPositionData && !useOptimizedPdr) {
    filterUrl = `${BASE_URL}service/positionDetailReportService/getLcmPositionData?inputFormat=JSON&format=JSON`;
  } else if (!enrichWithPrevDayLcmPositionData && useOptimizedPdr) {
    filterUrl = `${BASE_URL}service/positionDetailReportDataReadManager/getPdrData?inputFormat=JSON&format=JSON`;
  } else if (enrichWithPrevDayLcmPositionData && useOptimizedPdr) {
    filterUrl = `${BASE_URL}service/positionDetailReportDataReadManager/getDayOnDayDiffPdrData?inputFormat=JSON&format=JSON`;
  } else {
    filterUrl = `${BASE_URL}service/positionDetailReportService/getLcmPositionDataForDayOnDay?inputFormat=JSON&format=JSON`;
  }
  return filterUrl;
};

export const getPositionDetailCallBody = (
  positionDetailDataFilter: PositionDetailDataFilter,
  enrichWithPrevDayLcmPositionData: boolean,
  displayColumns: string[]
) => {
  let lcmPositionDataParam = encodeURIComponent(
    JSON.stringify(positionDetailDataFilter)
  );

  let dayOnDayDiffParam = getDayOnDayDiffParam(
    enrichWithPrevDayLcmPositionData,
    positionDetailDataFilter
  );

  if (displayColumns.length > 0) {
    let url = "_arrayTable=true&_fieldNames=";
    let fieldNames = "";

    if (!enrichWithPrevDayLcmPositionData) {
      displayColumns.forEach((column) => {
       fieldNames += column + ",";
      });
    } else {
      displayColumns.forEach((column) => {
        fieldNames += CURR_DAY_PREFIX + column + "," + PREV_DAY_PREFIX + column + ","
       });
    }

    url += fieldNames;

    return `lcmPositionDataParam=${lcmPositionDataParam}&${url}`;
  }

  return `lcmPositionDataParam=${lcmPositionDataParam}&${dayOnDayDiffParam}`;
};

export const messageType = (status: string) => {
  if (status == "FAILURE") return Message.Type.CRITICAL;
  else if (status == "SUCCESS") return Message.Type.WARNING;
  return Message.Type.PRIMARY;
};

export const getAgreementMetaData = (
  row: { [key: string]: string | number | boolean | undefined },
  prefixString: string
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let genericMap = new Map();
  let numericMap = new Map();
  let dateMap = new Map();

  genericMap.set("legalEntity", "legalEntity");
  genericMap.set("legalEntityId", "legalEntityId");
  genericMap.set("agreementType", "agreementType");
  genericMap.set("agreementId", "exposureAgreementId");
  genericMap.set("mnaAgreementId", "mnaAgreementId");
  genericMap.set("agreementTypeId", "agreementTypeId");
  genericMap.set("book", "componentKey.book.displayName");
  genericMap.set(
    "legalEntityFamily",
    "componentKey.custodianAccount.familyLegalEntity.name"
  );
  genericMap.set("bookId", "componentKey.bookId");
  genericMap.set(
    "custodianAccount",
    "componentKey.custodianAccount.displayName"
  );
  genericMap.set("custodianAccountId", "componentKey.custodianAccountId");
  genericMap.set("exposureCounterParty", "exposureCounterparty");
  genericMap.set("fieldTypeId", "componentKey.fieldId");
  genericMap.set(
    "custodianAccountCounterParty",
    "componentKey.custodianAccount.counterpartyEntity.name"
  );
  genericMap.set("exposureCounterPartyId", "expCounterPartyId");
  genericMap.set("fieldType", "componentKey.fieldType");
  genericMap.set("pnlSpn", "security.spn");

  dateMap.set("date", "componentKey.date");

  numericMap.set("marketValueUsd", "lcmPositionDetail.mktValUsd");
  numericMap.set("marketValueRC", "lcmPositionDetail.mktValRC");
  numericMap.set("marketValue", "lcmPositionDetail.mktVal");
  numericMap.set("priceRC", "lcmPositionDetail.priceRC");
  numericMap.set("priceUsd", "lcmPositionDetail.priceUsd");
  numericMap.set("quantity", "quantity");

  let genericAgreementMetaData: {
    [key: string]: string | number | boolean | undefined;
  } = getGenericColumnsData(row, columnMapperPrefix, prefixString, genericMap);

  let numericAgreementMetaData: {
    [key: string]: string | number | boolean | undefined;
  } = getNumericColumnsData(row, columnMapperPrefix, prefixString, numericMap);

  let dateAgreementMetaData: {
    [key: string]: string | number | boolean | undefined;
  } = getDateColumnsData(row, columnMapperPrefix, prefixString, dateMap);

  return {
    ...genericAgreementMetaData,
    ...numericAgreementMetaData,
    ...dateAgreementMetaData,
  };
};

export const getPositionDetailData = (
  row: { [key: string]: string | number | boolean | undefined },
  prefixString: string
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let genericMap = new Map();
  let numericMap = new Map();
  let dateMap = new Map();

  genericMap.set(
    "ecaDisplayName",
    "componentKey.exposureCustodianAccount.displayName"
  );
  genericMap.set("reportingCurrency", "reportingCurrency");
  genericMap.set("brokerSpn", "brokerDetail.brokerPositionDetails.spn");
  genericMap.set(
    "brokerRic",
    "brokerDetail.brokerPositionDetails.brokerSecurityDetail.ric"
  );
  genericMap.set(
    "brokerRegCurrencyCode",
    "brokerDetail.brokerPositionDetails.regCurrencyCode"
  );
  genericMap.set(
    "brokerMarketValueCurrencyId",
    "brokerDetail.brokerPositionDetails.marketValueCurrencyId"
  );

  numericMap.set("quantityDiff", "lcmPositionDetail.quantityDiff");
  numericMap.set(
    "repoMarginRC",
    "exposureDetail.repoPositionExposureData.repoMarginRC"
  );
  numericMap.set(
    "repoMarginUsd",
    "exposureDetail.repoPositionExposureData.margin"
  );
  numericMap.set("mktValRC", "lcmPositionDetail.mktValRC");
  numericMap.set(
    "brokerRegExposureRC",
    "brokerDetail.brokerPositionDetails.brokerRegExposureRC"
  );
  numericMap.set(
    "brokerMarketValueRC",
    "brokerDetail.brokerPositionDetails.brokerMarketValueRC"
  );
  numericMap.set(
    "brokerMarginRC",
    "brokerDetail.brokerPositionDetails.brokerMarginRC"
  );

  dateMap.set("offDate", "exposureDetail.repoPositionExposureData.offDate");
  dateMap.set("onDate", "exposureDetail.repoPositionExposureData.onDate");

  let genericPositionDetailData: {
    [key: string]: string | number | boolean | undefined;
  } = getGenericColumnsData(row, columnMapperPrefix, prefixString, genericMap);

  let numericPositionDetailData: {
    [key: string]: string | number | boolean | undefined;
  } = getNumericColumnsData(row, columnMapperPrefix, prefixString, numericMap);

  let datePositionDetailData: {
    [key: string]: string | number | boolean | undefined;
  } = getDateColumnsData(row, columnMapperPrefix, prefixString, dateMap);

  return {
    ...genericPositionDetailData,
    ...numericPositionDetailData,
    ...datePositionDetailData,
  };
};

export const getBundlePositionDetailData = (
  row: { [key: string]: string | number },
  prefixString: string
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let bundlePositionDetailData: { [key: string]: string | number } = {
    [columnMapperPrefix + "bundle"]: row[
      prefixString + "componentKey.bundle.name"
    ],
    [columnMapperPrefix + "businessUnit"]: row[
      prefixString + "componentKey.businessUnit.name"
    ],
  };

  return bundlePositionDetailData;
};


export const getPdrColumnsData = (
  row: { [key: string]: string | number | boolean | undefined },
  prefixString: string
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let genericArray = ["legalEntity","legalEntityId","agreementType","agreementId","mnaAgreementId","agreementTypeId",
  "book","legalEntityFamily","bookId","custodianAccount","custodianAccountId","exposureCounterParty","fieldTypeId",
  "exposureCustodianAccount",
  "custodianAccountCounterParty","exposureCounterPartyId","fieldType","pnlSpn","reportingCurrency",
  "brokerRic","brokerRegCurrencyCode","brokerMarketValueCurrencyId","bundle","businessUnit",
  "brokerSecurityName","brokerGboType","brokerCountry","brokerMarketValueCurrencyCode","brokerLocal",
  "brokerIsin","brokerCusip","brokerSedol","brokerAmeriEuroInd","brokerUniqueKey","brokerBloomberg",
  "brokerUnderlyingIsin","brokerUnderlyingSedol","brokerUnderlyingLocal","brokerUnderlyingRic","brokerBuySell",
  "brokerUnderlyingBloomberg","brokerUnderlyingUnderlyingLocal","brokerUnderlyingUnderlyingRic","brokerRepoRate",
  "brokerUnderlyingPutCallInd","brokerRequirementHaircutPct","brokerMatchValue","brokerSwapLevel","brokerForwardSpn1",
  "brokerForwardSpn2","brokerInstrumentName","brokerUnderlyingCanonicalSpnRic","brokerUnderlyingSpotSpnLocal",
  "brokerUnderlyingIssuerSpnRedCode","brokerBondSpnIsin","brokerBondSpnCusip","brokerReferenceSpnIsin",
  "brokerReferenceSpnCusip","brokerSpotSpnIsin","brokerSpotSpnLocal","brokerSpotSpnRic","brokerSpotSpnBloomberg",
  "brokerPutCallInd","brokerProduct","brokerMatchKey","brokerMarginCurrency","brokerForwardCurrency1",
  "brokerForwardCurrency2","recCode","recStatus","houseCalculatorStatus","houseCalculator","houseCalculationComment",
  "houseCalculatorId","regCalculator","regCalculationComment","regCalculatorStatus","regCpeCalculator",
  "regCpeCalculationComment","regCpeCalculatorStatus","sedol","cusip","bloomberg","isin","ric","local","foTypeName",
  "subtypeName","countryName","currencyIsoCode","desName","sfsTypeName","underlyingSpn","parentSpn","ultimateParentSpn",
  "gboTickUnit","currencyId","countryId","marketId","bondSpn","spotSpn","issuerSpn","market","gboTypeName",
  "gboTypeId","underlyingGboTypeId","gicsIndustry","gicsSector","gicsIndustryGroup","absType",
  "absSubtype","clearingMethod","countryOfRisk","putCallInd","ticker","ratingMoody","ratingSandP","bicsIndustrySector",
  "bicsIndustryGroup","bicsIndustrySubgroup","propeqIgroup","isNdf","forwardSpn1","forwardSpn2","referenceSpn",
  "underlyingIsin","underlyingSedol","underlyingCusip","underlyingBloomberg","underlyingLocal","underlyingRic","houseRuleUserName",
  "regRuleUserName","regCpeRuleUserName","exchangeRuleUserName","segRuleUserName","houseRuleUserComments","regRuleUserComments",
  "regCpeRuleUserComments","exchangeRuleUserComments","segRuleUserComments","houseRuleUsageType","houseRuleUsageCurrency",
  "regRuleUsageType","regRuleUsageCurrency","regCpeRuleUsageType","regCpeRuleUsageCurrency","exchangeRuleUsageType",
  "exchangeRuleUsageCurrency","segRuleUsageType","segRuleUsageCurrency"];

  let numericArray = ["marketValueUsd","marketValueRC","marketValue","priceRC","priceUsd","quantity","quantityDiff",
  "repoMarginRC","repoMarginUsd","mktValRC","brokerRegExposureRC","brokerMarketValueRC","brokerMarginRC",
  "brokerExposureRC","brokerMarginRC","brokerRegMarginRC","brokerRegCpeMarginRC","brokerMarketValueRC",
  "brokerRegExposureRC","brokerSpreadRC","brokerUnderlyingStrikePriceRC","brokerStrikePriceRC",
  "brokerApplicableMarginRC","brokerRegulatoryApplicableMarginRC","brokerRegCpeApplicableMarginRC",
  "brokerExposureUsd","brokerMarginUsd","brokerRegMarginUsd","brokerRegCpeMarginUsd","brokerMarketValueUsd",
  "brokerRegExposureUsd","brokerSpreadUsd","brokerUnderlyingStrikePriceUsd","brokerStrikePriceUsd",
  "brokerApplicableMarginUsd","brokerRegulatoryApplicableMarginUsd","brokerRegCpeApplicableMarginUsd",
  "brokerQuantity","brokerMarketValueLocal","brokerForwardQuantity1","brokerForwardQuantity2","brokerMarginFxRate",
  "brokerRegFxRate","brokerFxRate","brokerNativePrice","internalVsBrokerPriceDiffRC","settleDateMarketValueRC",
  "internalVsBrokerPriceDiffUsd","settleDateMarketValueUsd","internalVsBrokerPriceDiffPercent","exposureRC",
  "exposureAdjustmentUsd","adjustedExposureRC","tradeReturnFromLastResetRC","unrealizedAIRC","unrealizedCommissionsRC",
  "unrealizedCorpactRC","unrealizedFinancingRC","unrealizedPriceReturnRC","overdueReceivablePayableRC","markToMarketRC",
  "oteRC","exposureDiffRC","cleanPriceRC","dirtyPriceRC","loanAIRC","bondAIRC","bondValueRC","loanValueRC",
  "repoExposureRC","exposureUsd","exposureAdjustmentRC","adjustedExposureUsd","tradeReturnFromLastResetUsd",
  "unrealizedAIUsd","unrealizedCommissionsUsd","unrealizedCorpactUsd","unrealizedFinancingUsd",
  "unrealizedPriceReturnUsd","overdueReceivablePayableUsd","markToMarketUsd","oteUsd","exposureDiffUsd",
  "cleanPriceUsd","dirtyPriceUsd","loanAIUsd","bondAIUsd","bondValueUsd","loanValueUsd","repoExposureUsd",
  "settleDateQuantity","poolFactor","cpeQuantity","cpePrice","internalVsReconQuantityDiff","reconVsBrokerQuantityDiff",
  "cpeMarginUsd","reconVsBrokerMarginDiffUsd","reconVsBrokerMarginDiffRC","reconVsBrokerPriceDiffPercent","internalVsReconMarginDiffUsd","internalVsReconMarginDiffRC",
  "internalVsReconPriceDiffUsd","internalVsReconPriceDiffRC","reconVsBrokerPriceDiffUsd","reconVsBrokerPriceDiffRC",
  "internalVsReconPriceDiffPercent","internalVsReconExposureDiffUsd","internalVsReconExposureDiffRC",
  "reconVsBrokerExposureDiffUsd","reconVsBrokerExposureDiffRC","cpeMarketValue","cpeMarketValueRC","cpeLongMarketValue",
  "cpeLongMarketValueRC","cpeShortMarketValue","cpeShortMarketValueRC","marginDiffUsd","marginDiffRC","marginPrice",
  "marginMarketValue","underlyingValue","marginQuantity","houseMarginHaircut","financedQuantity","unfinancedQuantity",
  "houseMarginNative","houseUsageNative","houseMarginHaircut","houseUsageHaircut","regFinancedQuantity",
  "regUnfinancedQuantity","regMarginNative","regMarginHaircut","regUsageNative","regUsageHaircut","regCpeMarginNative",
  "regCpeMarginHaircut","regCpeUsageNative","regCpeUsageHaircut","reqHairCut","marginMarketValueUsd",
  "marginMarketValueRC","underlyingValueUsd","underlyingValueRC","houseFinancedMarginUsd","houseFinancedMarginRC",
  "houseUnfinancedMarginRC","houseUnfinancedMarginUsd","houseMarginUsd","houseMarginRC","houseUsageUsd","houseUsageRC","houseMarginAdjustmentUsd",
  "houseMarginAdjustmentRC","houseAdjustedMarginUsd","houseAdjustedMarginRC","houseUsageAdjustmentRC",
  "houseUsageAdjustmentUsd","houseAdjustedUsageRC","houseAdjustedUsageUsd","regFinancedMarginUsd",
  "regFinancedMarginRC","regUnfinancedMarginUsd","regUnfinancedMarginRC","regCpeMarginUsd","regCpeMarginRC",
  "applicableMarginUsd","applicableMarginRC","regMarginAdjustmentUsd","regMarginAdjustmentRC","regAdjustedMarginUsd",
  "regAdjustedMarginRC","regUsageAdjustmentUsd","regUsageAdjustmentRC","regAdjustedUsageUsd","regAdjustedUsageRC",
  "regCpeUsageUsd","regCpeUsageRC","regMarginRC","regMarginUsd","regUsageUsd","regUsageRC","tickUnit","forwardQuantity1",
  "forwardQuantity2","strikePrice","outstandingAmt","issueSize","reconLoanAmount","reconBondPrice","reconBondMarketValue",
  "reconBondQuantity","reconPoolFactor","reconPoolFactor","reconBondAccruedInterest","reconLoanAccruedInterest",
  "reconHaircut","reconExposureUSD","reconExposureRC","reconSegMarginUSD","reconSegMarginRC","reconRegMarginUSD","reconRegMarginRC",
  "reconExchangeMarginUSD","reconExchangeMarginRC","requirementAdjustmentUsd","requirementAdjustmentRC",
  "adjustedRequirementUsd","adjustedRequirementRC","houseRuleUsageHaircut","regRuleUsageHaircut","regCpeRuleUsageHaircut",
  "exchangeRuleUsageHaircut","segRuleUsageHaircut",];

  let dateArray = ["date","offDate","onDate","brokerBirth","brokerDeath","brokerUnderlyingDeath","brokerSwapStart",
  "brokerUnderlyingForwardEndDate","brokerUnderlyingForwardSettleDate","brokerExpirationDate","brokerOnDate",
  "brokerUnderlyingExpirationDate","brokerAccruedDate","brokerOffDate","brokerTradeDate","brokerMaturityDate",
  "resetDate","resetSettleDate","death"];

  let genericPositionDetailData: {
    [key: string]: string | number | boolean | undefined;
  } = getGenericColumnsData(row, columnMapperPrefix, prefixString, undefined, genericArray);

  let numericPositionDetailData: {
    [key: string]: string | number | boolean | undefined;
  } = getNumericColumnsData(row, columnMapperPrefix, prefixString, undefined, undefined, numericArray);

  let datePositionDetailData: {
    [key: string]: string | number | boolean | undefined;
  } = getDateColumnsData(row, columnMapperPrefix, prefixString, undefined, dateArray);

  return {
    ...genericPositionDetailData,
    ...numericPositionDetailData,
    ...datePositionDetailData,
  };
};
