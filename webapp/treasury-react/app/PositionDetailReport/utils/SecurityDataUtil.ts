import { PREV_DAY_PREFIX } from "../constants";
import { getDateColumnsData, getGenericColumnsData } from "./CommonUtil";
import { numberConverter } from "./PositionDetailUtil";

export const getSecurityAttr = (
  row: { [key: string]: string | number | boolean },
  prefixString: string
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let genericMap = new Map();
  let dateMap = new Map();

  genericMap.set("sedol", "sedol");
  genericMap.set("cusip", "cusip");
  genericMap.set("bloomberg", "bloomberg");
  genericMap.set("isin", "isin");
  genericMap.set("ric", "ric");
  genericMap.set("local", "local");
  genericMap.set("foTypeName", "foTypeName");
  genericMap.set("subtypeName", "subtypeName");
  genericMap.set("countryName", "countryName");
  genericMap.set("currencyIsoCode", "currencyAbbreviation");
  genericMap.set("desName", "desname");
  genericMap.set("sfsTypeName", "sfsTypeName");
  genericMap.set("underlyingSpn", "underlying");
  genericMap.set("parentSpn", "parentSpn");
  genericMap.set("ultimateParentSpn", "ultimateParentSpn");
  genericMap.set("gboTickUnit", "gboTickUnit");
  genericMap.set("currencyId", "currencyId");
  genericMap.set("countryId", "countryId");
  genericMap.set("marketId", "marketId");
  genericMap.set("bondSpn", "bondSpn");
  genericMap.set("spotSpn", "spotSpn");
  genericMap.set("issuerSpn", "issuerSpn");
  genericMap.set("market", "marketName");

  dateMap.set("death", "death");

  let genericSecurityAttrsData: {
    [key: string]: string | number | boolean | undefined;
  } = getGenericColumnsData(
    row,
    columnMapperPrefix,
    prefixString + "security.",
    genericMap
  );

  let dateSecurityAttrsData: {
    [key: string]: string | number | boolean | undefined;
  } = getDateColumnsData(
    row,
    columnMapperPrefix,
    prefixString + "security.",
    dateMap
  );

  let securityAttrsData = {
    ...genericSecurityAttrsData,
    ...dateSecurityAttrsData,
  };

  securityAttrsData[columnMapperPrefix + "gboTypeName"] =
    row[prefixString + "componentKey.gboType.name"];
  securityAttrsData[columnMapperPrefix + "gboTypeId"] =
    row[prefixString + "componentKey.gboType.id"];
  securityAttrsData[columnMapperPrefix + "underlyingGboTypeId"] =
    row[prefixString + "underlyingSecurity.gboTypeId"];
  securityAttrsData[columnMapperPrefix + "underlyingGboType"] =
    row[prefixString + "underlyingSecurity.gboTypeName"];

  return securityAttrsData;
};

export const getExtendedSecurityAttr = (row: {
  [key: string]: string | number | boolean | undefined;
}) => {
  let extendedSecurityAttrsData: {
    [key: string]: string | number | boolean | undefined;
  } = {
    ["gicsIndustry"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["gicsIndustryName"]
      : undefined,
    ["gicsSector"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["gicsSectorName"]
      : undefined,
    ["gicsIndustryGroup"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["gicsIndustryGroupName"]
      : undefined,
    ["absType"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["absTypeName"]
      : undefined,
    ["absSubType"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["absSubTypeName"]
      : undefined,
    ["clearingMethod"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["clearingMethodName"]
      : undefined,
    ["countryOfRisk"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["countryOfRiskName"]
      : undefined,
    ["putCallInd"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["putCallInd"]
      : undefined,
    ["ticker"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["ticker"]
      : undefined,
    ["ratingMoody"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["ratingMoodyName"]
      : undefined,
    ["ratingSandP"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["ratingSandPName"]
      : undefined,
    ["bicsIndustrySector"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["bicsIndustrySectorName"]
      : undefined,
    ["bicsIndustryGroup"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["bicsIndustryGroupName"]
      : undefined,
    ["bicsIndustrySubgroup"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["bicsIndustrySubGroupName"]
      : undefined,
    ["propeqIgroup"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["propeqIgroupName"]
      : undefined,
    ["isNdf"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["isNdf"]
      : undefined,
    ["tickUnit"]: numberConverter(
      row["extendedSecurity"] ? row["extendedSecurity"]["tickUnit"] : undefined
    ),
    ["forwardQuantity1"]: numberConverter(
      row["extendedSecurity"]
        ? row["extendedSecurity"]["forwardQuantity1"]
        : undefined
    ),
    ["forwardQuantity2"]: numberConverter(
      row["extendedSecurity"]
        ? row["extendedSecurity"]["forwardQuantity2"]
        : undefined
    ),
    ["strikePrice"]: numberConverter(
      row["extendedSecurity"]
        ? row["extendedSecurity"]["strikePrice"]
        : undefined
    ),
    ["forwardSpn1"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["forwardSpn1"]
      : undefined,
    ["forwardSpn2"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["forwardSpn2"]
      : undefined,
    ["referenceSpn"]: row["extendedSecurity"]
      ? row["extendedSecurity"]["referenceSpn"]
      : undefined,
    ["outstandingAmt"]: numberConverter(
      row["extendedSecurity"]
        ? row["extendedSecurity"]["outStandingAmt"]
        : undefined
    ),
    ["issueSize"]: numberConverter(
      row["extendedSecurity"] ? row["extendedSecurity"]["issueSize"] : undefined
    ),
  };
  return extendedSecurityAttrsData;
};

export const getUnderlyingSecurityAttr = (
  row: { [key: string]: string | number | boolean },
  prefixString: string
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let genericMap = new Map();
  genericMap.set("underlyingIsin", "isin");
  genericMap.set("underlyingSedol", "sedol");
  genericMap.set("underlyingCusip", "cusip");
  genericMap.set("underlyingBloomberg", "bloomberg");
  genericMap.set("underlyingLocal", "local");
  genericMap.set("underlyingRic", "ric");
  genericMap.set("underlyingSpn", "spn");

  let underlyingSecurityAttrsData: {
    [key: string]: string | number | boolean | undefined;
  } = getGenericColumnsData(
    row,
    columnMapperPrefix,
    prefixString + "underlyingSecurity.",
    genericMap
  );

  return underlyingSecurityAttrsData;
};
