import { getSelectedIdList, numberConverter } from "./PositionDetailUtil";
import CLASS_NAMESPACE from "../../commons/ClassConfigs";
import { BASE_URL, NOT_AVAILABLE } from "../../commons/constants";
import {
  getNameFromRefData,
  getIdFromRefData,
  getParsedDate,
} from "../../commons/TreasuryUtils";
import {
  convertJavaNYCDashedDate,
  getCommaSeparatedListValue,
  getSelectedIdListWithNull,
} from "../../commons/util";
import {
  MARGIN_TYPES,
  PREV_DAY_PREFIX,
  RC_SUFFIX,
  USD_SUFFIX,
} from "../constants";
import { MarginDetails } from "../models/MarginDetails";
import { PositionMarginData } from "../models/PositionMarginData";
import {
  LatestMarginDetailsGridData,
  SearchFilter,
} from "../models/UIDataTypes";
import { getGenericColumnsData, getNumericColumnsData } from "./CommonUtil";
import { MarginDataParam } from "../models/MarginDataParam";
import SERVICE_URLS, { COMPONENT_URLS } from "../../commons/UrlConfigs";

export const getMarginData = (
  row: { [key: string]: string | number | boolean | undefined },
  prefixString: string,
  enrichWithRCFields?: boolean
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";
  let suffixCurrency = enrichWithRCFields ? RC_SUFFIX : USD_SUFFIX;
  let genericMap = new Map();
  let numericMap = getNumericMapWithSuffixForMarginData(suffixCurrency);

  numericMap.set("marginDiffUsd", "lcmPositionDetail.marginDiff");
  numericMap.set("marginDiffRC", "lcmPositionDetail.marginDiffRC");
  numericMap.set("marginPrice", "marginDetail.price");
  numericMap.set("marginMarketValue", "marginDetail.marketValue");
  numericMap.set("underlyingValue", "lcmPositionDetail.underlyingValue");
  genericMap.set("houseCalculatorStatus", "componentKey.calculatorStatus");
  genericMap.set("houseCalculator", "componentKey.calculator");
  genericMap.set(
    "houseCalculationComment",
    "marginDetail.houseCalculationComment"
  );
  numericMap.set("marginQuantity", "marginDetail.quantity");
  numericMap.set("houseMarginHaircut", "marginDetail.haircut");
  numericMap.set("financedQuantity", "financedQuantity");
  numericMap.set("unfinancedQuantity", "unFinancedQuantity");
  numericMap.set("houseMarginNative", "marginDetail.marginNative");
  numericMap.set("houseUsageNative", "marginDetail.usageNative");
  numericMap.set("houseMarginHaircut", "marginDetail.haircut");
  numericMap.set("houseUsageHaircut", "marginDetail.usageHaircut");
  numericMap.set("regFinancedQuantity", "regFinancedQuantity");
  numericMap.set("regUnfinancedQuantity", "regUnfinancedQuantity");
  numericMap.set("regMarginNative", "marginDetail.regMarginNative");
  numericMap.set("regMarginHaircut", "marginDetail.regMarginHaircut");
  numericMap.set("regUsageNative", "marginDetail.regUsageNative");
  numericMap.set("regUsageHaircut", "marginDetail.regUsageHaircut");
  numericMap.set("regCpeMarginNative", "marginDetail.regCpeMarginNative");
  numericMap.set("regCpeMarginHaircut", "marginDetail.regCpeMarginHaircut");
  numericMap.set("regCpeUsageNative", "marginDetail.regCpeUsageNative");
  numericMap.set("regCpeUsageHaircut", "marginDetail.regCpeUsageHaircut");
  numericMap.set("reqHairCut", "reqHairCut");
  genericMap.set("houseCalculatorId", "componentKey.calculatorId");
  genericMap.set("regCalculator", "componentKey.regCalculator");
  genericMap.set(
    "regCalculationComment",
    "marginDetails.regCalculationComment"
  );
  genericMap.set("regCalculatorStatus", "componentKey.regCalculatorStatus");
  genericMap.set("regCpeCalculator", "componentKey.regCpeCalculator");
  genericMap.set(
    "regCpeCalculationComment",
    "componentKey.regCpeCalculationComment"
  );
  genericMap.set(
    "regCpeCalculatorStatus",
    "componentKey.regCpeCalculatorStatus"
  );

  let genericMarginData: {
    [key: string]: string | number | boolean | undefined;
  } = getGenericColumnsData(row, columnMapperPrefix, prefixString, genericMap);

  let numericMarginData: {
    [key: string]: string | number | boolean | undefined;
  } = getNumericColumnsData(row, columnMapperPrefix, prefixString, numericMap);

  return { ...genericMarginData, ...numericMarginData };
};

const getNumericMapWithSuffixForMarginData = (
  suffixCurrency: string
): Map<string, string> => {
  let numericMap = new Map();
  numericMap.set(
    "marginMarketValue" + suffixCurrency,
    "marginDetail.marketValue" + suffixCurrency
  );
  numericMap.set(
    "underlyingValue" + suffixCurrency,
    "lcmPositionDetail.underlyingValue" + suffixCurrency
  );
  numericMap.set(
    "houseFinancedMargin" + suffixCurrency,
    "marginDetail.financedMargin" + suffixCurrency
  );
  numericMap.set(
    "houseUnfinancedMargin" + suffixCurrency,
    "marginDetail.unFinancedMargin" + suffixCurrency
  );
  numericMap.set(
    "houseMargin" + suffixCurrency,
    "marginDetail.margin" + suffixCurrency
  );
  numericMap.set(
    "houseUsage" + suffixCurrency,
    "marginDetail.usage" + suffixCurrency
  );
  numericMap.set(
    "houseMarginAdjustment" + suffixCurrency,
    "lcmPositionDetail.requirementAdjustment" + suffixCurrency
  );
  numericMap.set(
    "houseAdjustedMargin" + suffixCurrency,
    "lcmPositionDetail.adjustedRequirement" + suffixCurrency
  );
  numericMap.set(
    "houseUsageAdjustment" + suffixCurrency,
    "lcmPositionDetail.usageAdjustment" + suffixCurrency
  );
  numericMap.set(
    "houseAdjustedUsage" + suffixCurrency,
    "lcmPositionDetail.adjustedUsage" + suffixCurrency
  );
  numericMap.set(
    "regFinancedMargin" + suffixCurrency,
    "marginDetail.regFinancedMargin" + suffixCurrency
  );
  numericMap.set(
    "regUnfinancedMargin" + suffixCurrency,
    "marginDetail.regUnFinancedMargin" + suffixCurrency
  );
  numericMap.set(
    "regCpeMargin" + suffixCurrency,
    "marginDetail.regCpeMargin" + suffixCurrency
  );
  numericMap.set(
    "applicableMargin" + suffixCurrency,
    "marginDetail.applicableMargin" + suffixCurrency
  );
  numericMap.set(
    "regMarginAdjustment" + suffixCurrency,
    "lcmPositionDetail.regRequirementAdjustment" + suffixCurrency
  );
  numericMap.set(
    "regAdjustedMargin" + suffixCurrency,
    "lcmPositionDetail.regAdjustedRequirement" + suffixCurrency
  );
  numericMap.set(
    "regUsageAdjustment" + suffixCurrency + suffixCurrency,
    "lcmPositionDetail.regUsageAdjustment" + suffixCurrency
  );
  numericMap.set(
    "regAdjustedUsage" + suffixCurrency,
    "lcmPositionDetail.regAdjustedUsage" + suffixCurrency
  );
  numericMap.set(
    "regCpeUsage" + suffixCurrency,
    "marginDetail.regCpeUsage" + suffixCurrency
  );
  numericMap.set(
    "regMargin" + suffixCurrency,
    "marginDetail.regMargin" + suffixCurrency
  );
  numericMap.set(
    "regUsage" + suffixCurrency,
    "marginDetail.regUsage" + suffixCurrency
  );
  return numericMap;
};

export const convertFilterObjectsToMarginDataJson = (
  filters: SearchFilter
): MarginDataParam => {
  const parameters: MarginDataParam = {
    "@CLASS":
      CLASS_NAMESPACE.com.arcesium.treasury.margin.model.MarginDataParam,
    legalEntityIds: getSelectedIdListWithNull(filters.selectedLegalEntities),
    legalEntityFamilyIds: getSelectedIdListWithNull(filters.selectedLegalEntityFamilies),
    startDate: filters.selectedFromDate,
    endDate: filters.selectedToDate,
    cpeIds: getSelectedIdListWithNull(filters.selectedCpes),
    agreementTypeIds: getSelectedIdListWithNull(filters.selectedAgreementTypes),
    bookIds: getSelectedIdListWithNull(filters.selectedBooks),
    gboTypeIds: getSelectedIdListWithNull(filters.selectedGboTypes),
    fieldIds: getSelectedIdListWithNull(filters.selectedFieldTypes),
    currencyIds: getSelectedIdListWithNull(filters.selectedCurrencies),
    custodianAccountIds: getSelectedIdListWithNull(
      filters.selectedCustodianAccounts
    ),
    showExtendedSecurityAttrs: filters.includeExtendedSecurityAttributes,
    showAdjustmentAttrs: true,
    showDummyPositionsForAdjustments: true,
    enrichWithRCFields: Boolean(filters.selectedReportingCurrency.key),
    bundleFlag: !filters.isPositionLevel,
    bundleIds: getSelectedIdListWithNull(filters.selectedBundles),
    businessUnitIds: getSelectedIdListWithNull(filters.selectedBusinessUnits),
    includeUnmappedBrokerRecord: filters.includeUnmappedBrokerRecord
  };

  if (
    filters.securityFilter?.isAdvancedSearch &&
    filters.securityFilter.securitySearchString.trim() !== ""
  ) {
    parameters.securityFilter = {
      "@CLASS":
        CLASS_NAMESPACE.com.arcesium.treasury.model.common.security
          .SecurityFilter,
      securitySearchType:
        filters.securityFilter.selectedSecuritySearchType.value,
      textSearchType: filters.securityFilter.selectedTextSearchType.value,
      searchStrings: getCommaSeparatedListValue(
        filters.securityFilter.securitySearchString
      ).split(","),
    };
  } else {
    parameters.pnlSpns =
      filters.securityFilter?.selectedSpns &&
      filters.securityFilter.selectedSpns.trim() !== ""
        ? getCommaSeparatedListValue(filters.securityFilter.selectedSpns).split(
            ","
          )
        : null;
  }
  return parameters;
};

export const convertFilterObjectsToMarginDataIds = (
  filters: SearchFilter
): MarginDataParam => {
  const parameters: MarginDataParam = {
    "@CLASS":
      CLASS_NAMESPACE.com.arcesium.treasury.margin.model.MarginDataParam,
    legalEntityIds: getSelectedIdList(filters.selectedLegalEntities),
    legalEntityFamilyIds: getSelectedIdList(filters.selectedLegalEntityFamilies),
    startDate: filters.selectedFromDate,
    endDate: filters.selectedToDate,
    cpeIds: getSelectedIdList(filters.selectedCpes),
    agreementTypeIds: getSelectedIdList(filters.selectedAgreementTypes),
    bookIds: getSelectedIdList(filters.selectedBooks),
    gboTypeIds: getSelectedIdList(filters.selectedGboTypes),
    fieldIds: getSelectedIdList(filters.selectedFieldTypes),
    currencyIds: getSelectedIdList(filters.selectedCurrencies),
    custodianAccountIds: getSelectedIdList(filters.selectedCustodianAccounts),
    showExtendedSecurityAttrs: filters.includeExtendedSecurityAttributes,
    showAdjustmentAttrs: true,
    showDummyPositionsForAdjustments: true,
    enrichWithRCFields: Boolean(filters.selectedReportingCurrency.key),
    bundleFlag: !filters.isPositionLevel,
    bundleIds: getSelectedIdList(filters.selectedBundles),
    businessUnitIds: getSelectedIdList(filters.selectedBusinessUnits),
    includeUnmappedBrokerRecord: filters.includeUnmappedBrokerRecord
  };

  if (
    filters.securityFilter?.isAdvancedSearch &&
    filters.securityFilter.securitySearchString.trim() !== ""
  ) {
    parameters.securityFilter = {
      "@CLASS":
        CLASS_NAMESPACE.com.arcesium.treasury.model.common.security
          .SecurityFilter,
      securitySearchType:
        filters.securityFilter.selectedSecuritySearchType.value,
      textSearchType: filters.securityFilter.selectedTextSearchType.value,
      searchStrings: getCommaSeparatedListValue(
        filters.securityFilter.securitySearchString
      ),
    };
  } else {
    parameters.pnlSpns =
      filters.securityFilter?.selectedSpns &&
      filters.securityFilter.selectedSpns.trim() !== ""
        ? getCommaSeparatedListValue(filters.securityFilter.selectedSpns).split(
            ","
          )
        : null;
  }

  return parameters;
};

export const getMarginDataFromArrayTable = (data: any) => {
  if (data && data.length) {
    data = getGridDataFromLatestMarginData(data);
  }
  return data;
};

export const getGridDataFromLatestMarginData = (
  latestMarginData: PositionMarginData[]
) => {
  let gridDataList: LatestMarginDetailsGridData[] = [];
  latestMarginData.forEach((data) => {
    let gridData: LatestMarginDetailsGridData = {
      agreementType: getNameFromRefData(data.agreementType),
      agreementTypeId: getIdFromRefData(data.agreementType),
      agreementId: data.exposureAgreementId,
      mnaAgreementId: data.mnaAgreementId,
      bundle: getNameFromRefData(data.bundle),
      businessUnit: getNameFromRefData(data.businessUnit),
      legalEntity: getNameFromRefData(data.legalEntity),
      legalEntityId: getIdFromRefData(data.legalEntity),
      exposureCounterParty: getNameFromRefData(data.exposureCounterParty),
      exposureCounterPartyId: getIdFromRefData(data.exposureCounterParty),
      reportingCurrency: getNameFromRefData(data.reportingCurrency),
      book: getNameFromRefData(data.book),
      bookId: getIdFromRefData(data.book),
      legalEntityFamily: getNameFromRefData(data.legalEntityFamily),
      custodianAccount: getNameFromRefData(data.custodianAccount),
      exposureCustodianAccount: getNameFromRefData(data.exposureCustodianAccount),
      custodianAccountId: getIdFromRefData(data.custodianAccount),
      custodianAccountCounterParty: getNameFromRefData(
        data.custodianAccountCounterParty
      ),
      pnlSpn: data?.pnlSpn ? data.pnlSpn : NOT_AVAILABLE,
      priceUsd: numberConverter(data.price),
      priceRC: numberConverter(data.price),
      priceTick: data.priceTick,
      quantity: numberConverter(data.quantity),
      fieldType: getNameFromRefData(data.fieldType),
      fieldTypeId: getIdFromRefData(data.fieldType),
      fxRate: data.fxRate,
      spn: data.spn,
      marketValue: numberConverter(data.marketValue),
      marketValueUsd: numberConverter(data.marketValueUsd),
      marketValueRC: numberConverter(data.marketValueRC),
      date: data.date ? convertJavaNYCDashedDate(data.date) : NOT_AVAILABLE,
      absSubtype: data.extendedSecurityData?.absSubTypeName,
      absType: data.extendedSecurityData?.absTypeName,
      bicsIndustryGroup: data.extendedSecurityData?.bicsIndustryGroupName,
      bicsIndustrySector: data.extendedSecurityData?.bicsIndustrySectorName,
      bicsIndustrySubgroup: data.extendedSecurityData?.bicsIndustrySubGroupName,
      strikePrice: data.extendedSecurityData?.strikePrice,
      clearingMethod: data.extendedSecurityData?.clearingMethodName,
      countryOfRisk: data.extendedSecurityData?.countryOfRiskName,
      forwardQuantity1: data.extendedSecurityData?.forwardQuantity1,
      forwardQuantity2: data.extendedSecurityData?.forwardQuantity2,
      forwardSpn1: data.extendedSecurityData?.forwardSpn1,
      forwardSpn2: data.extendedSecurityData?.forwardSpn2,
      gicsIndustry: data.extendedSecurityData?.gicsIndustryName,
      gicsIndustryGroup: data.extendedSecurityData?.gicsIndustryGroupName,
      gicsSector: data.extendedSecurityData?.gicsSectorName,
      isNdf: data.extendedSecurityData?.isNdf,
      issueSize: data.extendedSecurityData?.issueSize,
      outstandingAmt: data.extendedSecurityData?.outStandingAmt,
      propeqIgroup: data.extendedSecurityData?.propeqIgroupName,
      putCallInd: data.extendedSecurityData?.putCallInd,
      ratingMoody: data.extendedSecurityData?.ratingMoodyName,
      ratingSandP: data.extendedSecurityData?.ratingSandPName,
      referenceSpn: data.extendedSecurityData?.referenceSpn,
      ticker: data.extendedSecurityData?.ticker,
      tickUnit: data.extendedSecurityData?.tickUnit,

      regCalculatorStatus: data.regMarginDetails?.calculationStatus,
      regCalculator:
        getNameFromRefData(data.regMarginDetails?.calculator) === NOT_AVAILABLE
          ? undefined
          : getNameFromRefData(data.regMarginDetails?.calculator),
      regCalculationComment: data.regMarginDetails?.calculationComment,
      regFinancedMarginUsd: numberConverter(
        data.regMarginDetails?.financedMarginUsd
      ),
      regFinancedMarginRC: numberConverter(
        data.regMarginDetails?.financedMarginRC
      ),
      regFinancedQuantity: numberConverter(
        data.regMarginDetails?.financedQuantity
      ),
      regMarginHaircut: data.regMarginDetails?.marginHaircut,
      regMarginNative: numberConverter(data.regMarginDetails?.marginNative),
      regMarginUsd: numberConverter(data.regMarginDetails?.marginUsd),
      regMarginRC: numberConverter(data.regMarginDetails?.marginRC),
      regUsageHaircut: data.regMarginDetails?.usageHaircut,
      regUsageUsd: numberConverter(data.regMarginDetails?.usageUsd),
      regUsageRC: numberConverter(data.regMarginDetails?.usageRC),
      regUsageNative: numberConverter(data.regMarginDetails?.usageNative),
      regAdjustedMarginUsd: numberConverter(
        data.regMarginDetails?.adjustedMargin
      ),
      regUsageAdjustmentUsd: numberConverter(
        data.regMarginDetails?.usageAdjustment
      ),
      regMarginAdjustmentUsd: numberConverter(
        data.regMarginDetails?.marginAdjustment
      ),
      regAdjustedUsageUsd: numberConverter(
        data.regMarginDetails?.adjustedUsage
      ),
      regAdjustedMarginRC: numberConverter(
        data.regMarginDetails?.adjustedMarginRC
      ),
      regUsageAdjustmentRC: numberConverter(
        data.regMarginDetails?.usageAdjustmentRC
      ),
      regMarginAdjustmentRC: numberConverter(
        data.regMarginDetails?.marginAdjustmentRC
      ),
      regAdjustedUsageRC: numberConverter(
        data.regMarginDetails?.adjustedUsageRC
      ),
      regUnfinancedMarginUsd: numberConverter(
        data.regMarginDetails?.unFinancedMarginUsd
      ),
      regUnfinancedMarginRC: numberConverter(
        data.regMarginDetails?.unFinancedMarginRC
      ),
      regUnfinancedQuantity: numberConverter(
        data.regMarginDetails?.unFinancedQuantity
      ),

      regCpeCalculatorStatus: data.regCpeMarginDetails?.calculationStatus,
      regCpeCalculator:
        getNameFromRefData(data.regCpeMarginDetails?.calculator) ===
        NOT_AVAILABLE
          ? undefined
          : getNameFromRefData(data.regCpeMarginDetails?.calculator),
      regCpeCalculationComment: data.regCpeMarginDetails?.calculationComment,
      regCpeMarginHaircut: data.regCpeMarginDetails?.marginHaircut,
      regCpeMarginNative: numberConverter(
        data.regCpeMarginDetails?.marginNative
      ),
      regCpeMarginUsd: numberConverter(data.regCpeMarginDetails?.marginUsd),
      regCpeMarginRC: numberConverter(data.regCpeMarginDetails?.marginRC),
      regCpeUsageHaircut: data.regCpeMarginDetails?.usageHaircut,
      regCpeUsageUsd: numberConverter(data.regCpeMarginDetails?.usageUsd),
      regCpeUsageRC: numberConverter(data.regCpeMarginDetails?.usageRC),
      regCpeUsageNative: numberConverter(data.regCpeMarginDetails?.usageNative),

      houseCalculatorStatus: data.houseMarginDetails?.calculationStatus,
      houseCalculator:
        getNameFromRefData(data.houseMarginDetails?.calculator) ===
        NOT_AVAILABLE
          ? undefined
          : getNameFromRefData(data.houseMarginDetails?.calculator),
      houseCalculatorId: getIdFromRefData(data.houseMarginDetails?.calculator),
      houseCalculationComment: data.houseMarginDetails?.calculationComment,
      houseFinancedMarginUsd: numberConverter(
        data.houseMarginDetails?.financedMarginUsd
      ),
      houseFinancedMarginRC: numberConverter(
        data.houseMarginDetails?.financedMarginRC
      ),
      financedQuantity: numberConverter(
        data.houseMarginDetails?.financedQuantity
      ),
      houseMarginHaircut: data.houseMarginDetails?.marginHaircut,
      houseMarginNative: numberConverter(data.houseMarginDetails?.marginNative),
      houseMarginUsd: numberConverter(data.houseMarginDetails?.marginUsd),
      houseMarginRC: numberConverter(data.houseMarginDetails?.marginRC),
      houseUsageHaircut: data.houseMarginDetails?.usageHaircut,
      houseUsageUsd: numberConverter(data.houseMarginDetails?.usageUsd),
      houseUsageRC: numberConverter(data.houseMarginDetails?.usageRC),
      houseUsageNative: numberConverter(data.houseMarginDetails?.usageNative),
      houseAdjustedMarginUsd: numberConverter(
        data.houseMarginDetails?.adjustedMargin
      ),
      houseMarginAdjustmentUsd: numberConverter(
        data.houseMarginDetails?.marginAdjustment
      ),
      houseUsageAdjustmentUsd: numberConverter(
        data.houseMarginDetails?.usageAdjustment
      ),
      houseAdjustedUsageUsd: numberConverter(
        data.houseMarginDetails?.adjustedUsage
      ),
      houseAdjustedMarginRC: numberConverter(
        data.houseMarginDetails?.adjustedMarginRC
      ),
      houseMarginAdjustmentRC: numberConverter(
        data.houseMarginDetails?.marginAdjustmentRC
      ),
      houseUsageAdjustmentRC: numberConverter(
        data.houseMarginDetails?.usageAdjustmentRC
      ),
      houseAdjustedUsageRC: numberConverter(
        data.houseMarginDetails?.adjustedUsageRC
      ),
      houseUnfinancedMarginUsd: numberConverter(
        data.houseMarginDetails?.unFinancedMarginUsd
      ),
      houseUnfinancedMarginRC: numberConverter(
        data.houseMarginDetails?.unFinancedMarginRC
      ),
      unfinancedQuantity: numberConverter(
        data.houseMarginDetails?.unFinancedQuantity
      ),

      countryName: data.security?.countryName,
      countryId: data.security?.countryId,
      cusip: data.security?.cusip,
      death: data.security?.death
        ? getParsedDate(data.security?.death)
        : NOT_AVAILABLE,
      desName: data.security?.desName,
      foTypeName: data.security?.foTypeName,
      foTypeId: data.security?.foTypeId,
      gboTypeName: data.security?.gboTypeName,
      gboTypeId: data.security?.gboTypeId,
      isin: data.security?.isin,
      local: data.security?.local,
      ric: data.security?.ric,
      sedol: data.security?.sedol,
      sfsTypeName: data.security?.sfsTypeName,
      subtypeName: data.security?.subtypeName,
      bloomberg: data.security?.bloomberg,
      currencyIsoCode: data.security?.currencyIsoCode,
      underlyingSpn: data.security?.underlying,
      parentSpn: data.security?.parentSpn,
      ultimateParentSpn: data.security?.ultimateParentSpn,
      gboTickUnit: data.security?.gboTickUnit,
      currencyId: data.security?.currencyId,
      spotSpn: data.security?.spotSpn,
      bondSpn: data.security?.bondSpn,
      market: data.security?.marketName,
      marketId: data.security?.marketId,
      issuerSpn: data.security?.issuerSpn,

      underlyingBloomberg: data.underlyingSecurity?.bloomberg,
      underlyingSedol: data.underlyingSecurity?.sedol,
      underlyingCusip: data.underlyingSecurity?.cusip,
      underlyingIsin: data.underlyingSecurity?.isin,
      underlyingLocal: data.underlyingSecurity?.local,
      underlyingRic: data.underlyingSecurity?.ric,
      underlyingGboTypeId: data.underlyingSecurity?.gboTypeId,

      houseRuleUserName: data.houseMarginDetails?.positionRuleData?.userName,
      houseRuleUserComments: data.houseMarginDetails?.positionRuleData?.userComments,
      houseRuleUsageType: data.houseMarginDetails?.positionRuleData?.usageType?.name,
      houseRuleUsageHaircut: data.houseMarginDetails?.positionRuleData?.usageHaircut,
      houseRuleUsageCurrency: data.houseMarginDetails?.positionRuleData?.usageCurrency?.abbrev,

      regRuleUserName: data.regMarginDetails?.positionRuleData?.userName,
      regRuleUserComments: data.regMarginDetails?.positionRuleData?.userComments,
      regRuleUsageType: data.regMarginDetails?.positionRuleData?.usageType?.name,
      regRuleUsageHaircut: data.regMarginDetails?.positionRuleData?.usageHaircut,
      regRuleUsageCurrency: data.regMarginDetails?.positionRuleData?.usageCurrency?.abbrev,

      regCpeRuleUserName: data.regCpeMarginDetails?.positionRuleData?.userName,
      regCpeRuleUserComments: data.regCpeMarginDetails?.positionRuleData?.userComments,
      regCpeRuleUsageType: data.regCpeMarginDetails?.positionRuleData?.usageType?.name,
      regCpeRuleUsageHaircut: data.regCpeMarginDetails?.positionRuleData?.usageHaircut,
      regCpeRuleUsageCurrency: data.regCpeMarginDetails?.positionRuleData?.usageCurrency?.abbrev,
    };

    if (data.houseMarginDetails) {
      gridData.marginPrice = numberConverter(
        data.houseMarginDetails.marginPrice
      );
      gridData.marginQuantity = numberConverter(
        data.houseMarginDetails.marginQuantity
      );
      gridData.marginMarketValue = numberConverter(
        data.houseMarginDetails.marginMarketValue
      );
      gridData.marginMarketValueUsd = numberConverter(
        data.houseMarginDetails.marginMarketValueUsd
      );
      gridData.marginMarketValueRC = numberConverter(
        data.houseMarginDetails.marginMarketValueRC
      );
      gridData.underlyingValueUsd = numberConverter(
        data.houseMarginDetails.underlyingValueUsd
      );
      gridData.underlyingValueRC = numberConverter(
        data.houseMarginDetails.underlyingValueRC
      );
      gridData.underlyingValue = numberConverter(
        data.houseMarginDetails.underlyingValue
      );
    }

    gridDataList.push(gridData);
  });
  return gridDataList;
};

export const getMarginDataUrl = () => {
  let filterUrl = `${BASE_URL}service/marginPositionDataService/getPositionMarginData?inputFormat=JSON&format=JSON`;
  return filterUrl;
};

export const getMarginDataCallBody = (marginDataParam: MarginDataParam) => {
  let lcmMarginDataFilter = encodeURIComponent(JSON.stringify(marginDataParam));

  return `lcmMarginDataFilter=${lcmMarginDataFilter}`;
};

export const getDefaultMarginDetailUrl = (
  location: any,
  parameters: MarginDataParam,
  selectedDatasetKey: number,
  selectedSavedGridKey: number,
  selectedReportingCurrencyKey: number
) => {
  let url = location + `${COMPONENT_URLS.POSITION_DETAIL_REPORT}?`;
  url =
    url +
    `legalEntityIds=` +
    parameters.legalEntityIds +
    `&legalEntityFamilyIds=` +
    parameters.legalEntityFamilyIds +
    `&cpeIds=` +
    parameters.cpeIds +
    `&agreementTypeIds=` +
    parameters.agreementTypeIds +
    `&startDate=` +
    parameters.startDate +
    `&endDate=` +
    parameters.endDate +
    `&dataSet=` +
    selectedDatasetKey +
    `&reportingCurrencyKey=` +
    selectedReportingCurrencyKey +
    `&isPositionLevel=` +
    !parameters.bundleFlag +
    `&includeExtendedSecurityAttributes=` +
    parameters.showExtendedSecurityAttrs +
    `&fieldTypeIds=` +
    parameters.fieldIds +
    `&bookIds=` +
    parameters.bookIds +
    `&businessUnitIds=` +
    parameters.businessUnitIds +
    `&bundleIds=` +
    parameters.bundleIds +
    `&custodianAccountIds=` +
    parameters.custodianAccountIds +
    `&currencyIds=` +
    parameters.currencyIds +
    `&gboTypeIds=` +
    parameters.gboTypeIds +
    `&isPublished=false`;

  if (parameters.pnlSpns != undefined && parameters.pnlSpns.length !== 0) {
    url += `&pnlSpns=` + parameters.pnlSpns + `&isAdvancedSearch=false`;
  } else if (
    parameters.securityFilter?.searchStrings != undefined &&
    parameters.securityFilter.searchStrings.length !== 0
  ) {
    url +=
      `&securitySearchType=` +
      parameters.securityFilter?.securitySearchType +
      `&searchStrings=` +
      parameters.securityFilter?.searchStrings +
      `&textSearchType=` +
      parameters.securityFilter?.textSearchType +
      `&isAdvancedSearch=true`;
  }

  if (selectedSavedGridKey !== 0) {
    url += `&sharedSavedGrid=` + selectedSavedGridKey;
  }
  url += `&format=Json&inputFormat=PROPERTIES`;
  return url;
};
