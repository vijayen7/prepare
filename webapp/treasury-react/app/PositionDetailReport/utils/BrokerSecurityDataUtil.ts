import { PREV_DAY_PREFIX } from "../constants";
import {
  getDateColumnsData,
  getGenericColumnsData,
  getNumericColumnsData,
} from "./CommonUtil";
import { numberConverter } from "./PositionDetailUtil";

export const getBrokerSecurityData = (
  row: { [key: string]: string | number | boolean | undefined },
  prefixString: string,
  enrichWithRCFields?: boolean
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let genericMap = new Map();
  let numericMap = new Map();
  let dateMap = new Map();

  if (enrichWithRCFields) {
    numericMap.set("brokerExposureRC", "brokerExposureRC");
    numericMap.set("brokerMarginRC", "brokerMarginRC");
    numericMap.set("brokerRegMarginRC", "brokerRegMarginRC");
    numericMap.set("brokerRegCpeMarginRC", "brokerRegCpeMarginRC");
    numericMap.set("brokerMarketValueRC", "brokerMarketValueRC");
    numericMap.set("brokerRegExposureRC", "brokerRegExposureRC");
    numericMap.set("brokerSpreadRC", "brokerSecurityDetail.brokerSpreadRC");
    numericMap.set(
      "brokerUnderlyingStrikePriceRC",
      "brokerSecurityDetail.brokerUnderlyingStrikePriceRC"
    );
    numericMap.set(
      "brokerStrikePriceRC",
      "brokerSecurityDetail.brokerSpotSpnStrikePriceRC"
    );
    numericMap.set("brokerApplicableMarginRC", "brokerApplicableMarginRC");
    numericMap.set(
      "brokerRegulatoryApplicableMarginRC",
      "brokerRegulatoryApplicableMarginRC"
    );
    numericMap.set(
      "brokerRegCpeApplicableMarginRC",
      "brokerRegCpeApplicableMarginUsd"
    );
  } else {
    numericMap.set("brokerExposureUsd", "exposureUsd");
    numericMap.set("brokerMarginUsd", "marginUsd");
    numericMap.set("brokerRegMarginUsd", "regMarginUsd");
    numericMap.set("brokerRegCpeMarginUsd", "regCpeMarginUsd");
    numericMap.set("brokerMarketValueUsd", "marketValueUsd");
    numericMap.set("brokerRegExposureUsd", "regExposureUsd");
    numericMap.set("brokerSpreadUsd", "brokerSecurityDetail.spread");
    numericMap.set(
      "brokerUnderlyingStrikePriceUsd",
      "brokerSecurityDetail.underlyingStrikePrice"
    );
    numericMap.set("brokerStrikePriceUsd", "brokerSecurityDetail.strikePrice");
    numericMap.set("brokerApplicableMarginUsd", "applicableMarginUsd");
    numericMap.set(
      "brokerRegulatoryApplicableMarginUsd",
      "regulatoryApplicableMarginUsd"
    );
    numericMap.set(
      "brokerRegCpeApplicableMarginUsd",
      "regCpeApplicableMarginUsd"
    );
  }
  genericMap.set("brokerSecurityName", "securityName");
  genericMap.set("brokerGboType", "gboType");
  genericMap.set("brokerCountry", "country");
  numericMap.set("brokerQuantity", "quantity");
  numericMap.set("brokerMarketValueLocal", "marketValueLocal");
  genericMap.set("brokerMarketValueCurrencyCode", "marketValueCurrencyCode");
  genericMap.set("brokerLocal", "brokerSecurityDetail.local");
  genericMap.set("brokerIsin", "brokerSecurityDetail.isin");
  genericMap.set("brokerCusip", "brokerSecurityDetail.cusip");
  genericMap.set("brokerSedol", "brokerSecurityDetail.sedol");
  genericMap.set("brokerAmeriEuroInd", "brokerSecurityDetail.ameriEuroInd");
  genericMap.set("brokerUniqueKey", "brokerSecurityDetail.brokerUniqueKey");
  genericMap.set("brokerBloomberg", "brokerSecurityDetail.bloomberg");
  genericMap.set("brokerUnderlyingIsin", "brokerSecurityDetail.underlyingIsin");
  genericMap.set(
    "brokerUnderlyingSedol",
    "brokerSecurityDetail.underlyingSedol"
  );
  genericMap.set(
    "brokerUnderlyingLocal",
    "brokerSecurityDetail.underlyingLocal"
  );
  genericMap.set("brokerUnderlyingRic", "brokerSecurityDetail.underlyingRic");
  genericMap.set(
    "brokerUnderlyingBloomberg",
    "brokerSecurityDetail.underlyingBloomberg"
  );
  genericMap.set(
    "brokerUnderlyingUnderlyingLocal",
    "brokerSecurityDetail.underlyingUnderlyingLocal"
  );
  genericMap.set(
    "brokerUnderlyingUnderlyingRic",
    "brokerSecurityDetail.underlyingUnderlyingRic"
  );
  genericMap.set(
    "brokerUnderlyingPutCallInd",
    "brokerSecurityDetail.underlyingPutCallInd"
  );
  genericMap.set(
    "brokerRequirementHaircutPct",
    "brokerSecurityDetail.requirementHaircutPct"
  );
  genericMap.set("brokerMatchValue", "matchValue");
  genericMap.set("brokerSwapLevel", "brokerSecurityDetail.swapLevel");
  genericMap.set("brokerForwardSpn1", "brokerSecurityDetail.forwardSpn1");
  genericMap.set("brokerForwardSpn2", "brokerSecurityDetail.forwardSpn2");
  genericMap.set("brokerRepoRate", "brokerSecurityDetail.repoRate");
  genericMap.set("brokerBuySell", "brokerSecurityDetail.buySell");
  genericMap.set("brokerInstrumentName", "brokerSecurityDetail.instrumentName");
  genericMap.set(
    "brokerUnderlyingCanonicalSpnRic",
    "brokerSecurityDetail.underlyingCanonicalSpnRic"
  );
  genericMap.set(
    "brokerUnderlyingSpotSpnLocal",
    "brokerSecurityDetail.underlyingSpotSpnLocal"
  );
  genericMap.set(
    "brokerUnderlyingIssuerSpnRedCode",
    "brokerSecurityDetail.underlyingIssuerSpnRedCode"
  );
  genericMap.set("brokerBondSpnIsin", "brokerSecurityDetail.bondSpnIsin");
  genericMap.set("brokerBondSpnCusip", "brokerSecurityDetail.bondSpnCusip");
  genericMap.set(
    "brokerReferenceSpnIsin",
    "brokerSecurityDetail.referenceSpnIsin"
  );
  genericMap.set(
    "brokerReferenceSpnCusip",
    "brokerSecurityDetail.referenceSpnCusip"
  );
  genericMap.set("brokerSpotSpnIsin", "brokerSecurityDetail.spotSpnIsin");
  genericMap.set("brokerSpotSpnLocal", "brokerSecurityDetail.spotSpnLocal");
  genericMap.set("brokerSpotSpnRic", "brokerSecurityDetail.spotSpnRic");
  genericMap.set(
    "brokerSpotSpnBloomberg",
    "brokerSecurityDetail.spotSpnBloomberg"
  );
  genericMap.set("brokerPutCallInd", "brokerSecurityDetail.putCallInd");
  genericMap.set("brokerProduct", "product");
  genericMap.set("brokerMatchKey", "matchKey");
  genericMap.set("brokerMarginCurrency", "marginCurrency");

  dateMap.set("brokerBirth", "brokerSecurityDetail.birth");
  dateMap.set("brokerDeath", "brokerSecurityDetail.death");
  dateMap.set("brokerUnderlyingDeath", "brokerSecurityDetail.underlyingDeath");
  dateMap.set(
    "brokerUnderlyingForwardEndDate",
    "brokerSecurityDetail.underlyingForwardEndDate"
  );
  dateMap.set(
    "brokerUnderlyingForwardSettleDate",
    "brokerSecurityDetail.underlyingForwardSettleDate"
  );
  dateMap.set(
    "brokerExpirationDate",
    "brokerSecurityDetail.brokerExpirationDate"
  );
  dateMap.set(
    "brokerUnderlyingExpirationDate",
    "brokerSecurityDetail.underlyingExpirationDate"
  );
  dateMap.set("brokerSwapStart", "brokerSecurityDetail.swapStart");
  dateMap.set("brokerAccruedDate", "brokerSecurityDetail.accruedDate");
  dateMap.set("brokerOnDate", "brokerSecurityDetail.onDate");
  dateMap.set("brokerOffDate", "brokerSecurityDetail.offDate");
  dateMap.set("brokerTradeDate", "tradeDate");
  dateMap.set("brokerMaturityDate", "maturityDate");

  numericMap.set(
    "brokerForwardCurrency1",
    "brokerSecurityDetail.forwardCurrency1"
  );
  numericMap.set(
    "brokerForwardCurrency2",
    "brokerSecurityDetail.forwardCurrency2"
  );
  numericMap.set(
    "brokerForwardQuantity1",
    "brokerSecurityDetail.forwardQuantity1"
  );
  numericMap.set(
    "brokerForwardQuantity2",
    "brokerSecurityDetail.forwardQuantity2"
  );
  numericMap.set("brokerMarginFxRate", "marginFxRate");
  numericMap.set("brokerRegFxRate", "regFxRate");
  numericMap.set("brokerFxRate", "fxRate");
  numericMap.set("brokerNativePrice", "priceNative");

  let genericBrokerSecurityData: {
    [key: string]: string | number | boolean | undefined;
  } = getGenericColumnsData(
    row,
    columnMapperPrefix,
    prefixString + "brokerDetail.brokerPositionDetails.",
    genericMap
  );

  let dateBrokerSecurityData: {
    [key: string]: string | number | boolean | undefined;
  } = getDateColumnsData(
    row,
    columnMapperPrefix,
    prefixString + "brokerDetail.brokerPositionDetails.",
    dateMap
  );

  let numericBrokerSecurityData: {
    [key: string]: string | number | boolean | undefined;
  } = getNumericColumnsData(
    row,
    columnMapperPrefix,
    prefixString + "brokerDetail.brokerPositionDetails.",
    numericMap
  );

  let brokerSecurityData = {
    ...genericBrokerSecurityData,
    ...dateBrokerSecurityData,
    ...numericBrokerSecurityData,
  };

  if (enrichWithRCFields) {
    brokerSecurityData[
      columnMapperPrefix + "internalVsBrokerPriceDiffRC"
    ] = numberConverter(row[prefixString + "lcmPositionDetail.priceDiffRC"]);
    brokerSecurityData[
      columnMapperPrefix + "settleDateMarketValueRC"
    ] = numberConverter(
      row[prefixString + "lcmPositionDetail.settleDateMarketValueRC"]
    );
  } else {
    brokerSecurityData[
      columnMapperPrefix + "internalVsBrokerPriceDiffUsd"
    ] = numberConverter(row[prefixString + "lcmPositionDetail.priceDiff"]);
    brokerSecurityData[
      columnMapperPrefix + "settleDateMarketValueUsd"
    ] = numberConverter(
      row[prefixString + "lcmPositionDetail.settleDateMarketValue"]
    );
  }

  brokerSecurityData[
    columnMapperPrefix + "internalVsBrokerPriceDiffPercent"
  ] = numberConverter(row[prefixString + "lcmPositionDetail.priceDiffPercent"]);

  return brokerSecurityData;
};
