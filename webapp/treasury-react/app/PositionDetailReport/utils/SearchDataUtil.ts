import {
  DATASETS,
  DATASET_TYPE,
  MAX_DATE_RANGE_IN_DAYS,
  MESSAGES,
  TOAST_TYPE,
  REPORTING_CURRENCY_FILTER_DATA
} from "../constants";
import queryString from "query-string";
import { showToastService } from "./CommonUtil";
import { SearchFilter } from "../models/UIDataTypes";
import { getNumberOfWeekDaysBetweenTwoDates } from "../../commons/TreasuryUtils";

export const isValidInputData = (searchFilter: SearchFilter | null) => {
  let returnVal: boolean = true;
  if (searchFilter?.selectedDataset == null || searchFilter?.selectedReportingCurrency == null) {
    showToastService(MESSAGES.FILL_REQUIRED_INPUT_MESSAGE, TOAST_TYPE.CRITICAL);
    returnVal = false;
  }
  if (searchFilter?.selectedFromDate && searchFilter.selectedToDate) {
    if (searchFilter?.selectedToDate < searchFilter?.selectedFromDate) {
      showToastService(MESSAGES.INPUT_DATE_CHECK_MESSAGE, TOAST_TYPE.CRITICAL);
      returnVal = false;
    }
    if (
      getNumberOfWeekDaysBetweenTwoDates(
        searchFilter.selectedFromDate,
        searchFilter.selectedToDate
      ) > MAX_DATE_RANGE_IN_DAYS
    ) {
      showToastService(
        MESSAGES.INPUT_DATE_RANGE_CHECK_MESSAGE,
        TOAST_TYPE.CRITICAL
      );
      returnVal = false;
    }
    if (searchFilter.selectedFromDate !== searchFilter.selectedToDate) {
      if (
        (searchFilter.securityFilter?.selectedSpns === null ||
          searchFilter.securityFilter?.selectedSpns === "" ||
          searchFilter.securityFilter?.isAdvancedSearch) &&
        (searchFilter.selectedAgreementTypes === null ||
          searchFilter.selectedAgreementTypes.length === 0 ||
          searchFilter.selectedLegalEntities === null ||
          searchFilter.selectedLegalEntities.length === 0 ||
          searchFilter.selectedCpes === null ||
          searchFilter.selectedCpes.length === 0)
      ) {
        showToastService(
          MESSAGES.AGREEMENT_COMBINATION_MESSAGE,
          TOAST_TYPE.CRITICAL
        );
        returnVal = false;
      }
    }
  }
  return returnVal;
};

export const getParsedPnlSpns = (pnlSpn: string) => {
  let pnlSpns: string[] = pnlSpn.split(";");
  let parsedPnlSpns: number[] = [];
  pnlSpns.forEach((pnlSpn) => {
    if (pnlSpn != "") {
      parsedPnlSpns.push(parseInt(pnlSpn));
    }
  });
  return parsedPnlSpns;
};

export const extractfiltersFromUrl = async (
  searchUrl: String,
  setFilters: Function,
  setIsParametersInUrl: Function,
  setToggleSidebar: Function,
  fetchSavedGridData: Function
) => {
  const values = queryString.parse(searchUrl);

  let filters = {
    selectedFromDate: values.startDate,
    selectedToDate: values.endDate,
    selectedLegalEntities: values.legalEntityIds
      ? splitCommaSeparatedNumbersToString(values.legalEntityIds)
      : [],
    selectedLegalEntityFamilies: values.legalEntityFamilyIds
      ? splitCommaSeparatedNumbersToString(values.legalEntityFamilyIds)
      : [],
    selectedCpes: values.cpeIds
      ? splitCommaSeparatedNumbersToString(values.cpeIds)
      : [],
    selectedAgreementTypes: values.agreementTypeIds
      ? splitCommaSeparatedNumbersToString(values.agreementTypeIds)
      : [],
    selectedBooks: values.bookIds
      ? splitCommaSeparatedNumbersToString(values.bookIds)
      : [],
    selectedGboTypes: values.gboTypeIds
      ? splitCommaSeparatedNumbersToString(values.gboTypeIds)
      : [],
    selectedFieldTypes: values.fieldTypeIds
      ? splitCommaSeparatedNumbersToString(values.fieldTypeIds)
      : [],
    selectedCustodianAccounts: splitCommaSeparatedNumbersToString(
      values.custodianAccountIds
    ),
    selectedCurrencies: values.currencyIds
      ? splitCommaSeparatedNumbersToString(values.currencyIds)
      : [],
    selectedBusinessUnits: values.businessUnitIds
      ? splitCommaSeparatedNumbersToString(values.businessUnitIds)
      : [],
    selectedBundles: values.bundleIds
      ? splitCommaSeparatedNumbersToString(values.bundleIds)
      : [],
    selectedReportingCurrency: values.reportingCurrencyKey
      ? REPORTING_CURRENCY_FILTER_DATA[values.reportingCurrencyKey]
      : REPORTING_CURRENCY_FILTER_DATA[1],
    selectedDataset: values.dataSet
      ? DATASETS[values.dataSet - 1]
      : DATASETS[DATASET_TYPE.Margin_Exposure_Details - 1],
    liveOnly: extractBooleanFromFilterValues(values.liveOnly),
    isCrimsonEnabled: extractBooleanFromFilterValues(values.isCrimsonEnabled),
    isPositionLevel: extractBooleanFromFilterValues(values.isPositionLevel),
    isPublished: extractBooleanFromFilterValues(values.isPublished),
    includeExtendedSecurityAttributes: extractBooleanFromFilterValues(
      values.includeExtendedSecurityAttributes
    ),
    includeUnmappedBrokerRecord: extractBooleanFromFilterValues(
      values.includeUnmappedBrokerRecord
    ),
    showSegDrilldown: extractBooleanFromFilterValues(values.showSegDrilldown),
    securityFilter: {
      selectedSpns: values.pnlSpns ? values.pnlSpns : null,
      selectedSecuritySearchType: values.securitySearchType
        ? { key: values.securitySearchType, value: values.securitySearchType }
        : { key: "", value: "" },
      securitySearchString: values.searchStrings ? values.searchStrings : "",
      selectedTextSearchType: values.textSearchType
        ? { key: values.textSearchType, value: values.textSearchType }
        : { key: "", value: "" },
      isAdvancedSearch: extractBooleanFromFilterValues(values.isAdvancedSearch)
    },
    excessDeficitDataType: values.excessDeficitDataType
      ? values.excessDeficitDataType
      : null,
    fromExcessDeficitDataSource: values.fromExcessDeficitDataSource
      ? values.fromExcessDeficitDataSource
      : null,
    toExcessDeficitDataSource: values.toExcessDeficitDataSource
      ? values.toExcessDeficitDataSource
      : null,
    isRowClicked: false
  };
  if (filters.selectedDataset) {
    await fetchSavedGridData(
      filters.selectedDataset?.key + "_positionDetailReport"
    );
    filters["selectedSavedGrid"] = values.sharedSavedGrid
      ? { key: parseInt(values.sharedSavedGrid), value: null }
      : null;
    setFilters(filters);
    setIsParametersInUrl(true);
  }
  setToggleSidebar(true);
};

const extractBooleanFromFilterValues = (value) => {
  if (value) {
    return value === "true";
  }
  return null;
};

const splitCommaSeparatedNumbersToString = (inputStringObjects) => {
  if (inputStringObjects) {
    return inputStringObjects
      .split(",")
      .map(Number)
      .map((eachValue) => ({
        key: eachValue,
        value: null,
      }));
  }
  return null;
};
