import {
  genericBooleanFormatter,
  genericFormatter,
  getFormattedNumericColumn,
  getFormattedPercentColumn,
  getFormattedPriceColumn,
} from "../../commons/TreasuryUtils";
import { getFormattedPnlSpn } from "./CommonUtil";

export const AGREEMENT_META_DATA = (
  prefix?: string,
  suffixCurrency?: string
) => [
  genericFormatter("date", "Date", 120, prefix),
  genericFormatter("legalEntity", "Legal Entity", 180, prefix),
  genericFormatter("agreementType", "Agreement Type", 120, prefix),
  genericFormatter("agreementId", "Agreement ID", 100, prefix),
  genericFormatter("mnaAgreementId", "MNA Agreement ID", 120, prefix),
  genericFormatter("book", "Book", 120, prefix),
  genericFormatter("custodianAccount", "Custodian Account", 150, prefix),
  genericFormatter("exposureCustodianAccount", "Exposure Custodian Account", 200, prefix),
  genericFormatter(
    "custodianAccountCounterParty",
    "Custodian Account Counter Party",
    220,
    prefix
  ),
  genericFormatter(
    "exposureCounterParty",
    "Exposure Counter Party",
    180,
    prefix
  ),
  genericFormatter("fieldType", "Field Type", 80, prefix),
  genericFormatter("legalEntityFamily", "Legal Entity Family", 150, prefix),
  getFormattedNumericColumn("marketValue", "Market Value", 100, prefix),
  getFormattedNumericColumn(
    "marketValue" + suffixCurrency,
    "Market Value " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedPnlSpn("pnlSpn", "P&L SPN", 80, prefix),
  getFormattedNumericColumn(
    "price" + suffixCurrency,
    "Price",
    100,
    prefix,
    4
  ),
  getFormattedNumericColumn("quantity", "Quantity", 80, prefix),
  getFormattedNumericColumn("brokerQuantity", "Broker Quantity", 120, prefix),
  genericFormatter("reportingCurrency", "Reporting Currency", 140, prefix),
];

export const BUNDLE_ATTRS = [
  genericFormatter("bundle", "Bundle", 100),
  genericFormatter("businessUnit", "Business Unit", 150),
];

export const SECURITY_ATTRS = (prefix?: string) => [
  genericFormatter("cusip", "Cusip", 60, prefix),
  genericFormatter("sedol", "Sedol", 60, prefix),
  genericFormatter("bloomberg", "Bloomberg", 100, prefix),
  genericFormatter("ric", "RIC", 60, prefix),
  genericFormatter("local", "Local", 60, prefix),
  genericFormatter("isin", "ISIN", 60, prefix),
  genericFormatter("gboTypeName", "GBO Type", 120, prefix),
  genericFormatter("foTypeName", "FO Type", 120, prefix),
  genericFormatter("subtypeName", "Sub Type", 120, prefix),
  genericFormatter("countryName", "Country", 120, prefix),
  genericFormatter("currencyIsoCode", "Currency", 60, prefix),
  genericFormatter("desName", "Description", 250, prefix),
  genericFormatter("sfsTypeName", "Sfs Type Name", 120, prefix),
  genericFormatter("death", "Death", 150, prefix),
  genericFormatter("underlyingSpn", "Underlying SPN", 120, prefix),
  genericFormatter("parentSpn", "Parent SPN", 120, prefix),
  genericFormatter("ultimateParentSpn", "Ultimate Parent SPN", 150, prefix),
  getFormattedNumericColumn("gboTickUnit", "Gbo Tick Unit", 100, prefix, 4),
  genericFormatter("market", "Market", 120, prefix),
  genericFormatter("spotSpn", "Spot SPN", 120, prefix),
  genericFormatter("bondSpn", "Bond SPN", 120, prefix),
];

export const EXTENDED_SECURITY_ATTRS = [
  genericFormatter("absSubtype", "ABS Subtype", 120),
  genericFormatter("absType", "ABS Type", 120),
  genericFormatter("bicsIndustryGroup", "Bics Industry Group", 150),
  genericFormatter("bicsIndustrySector", "Bics Industry Sector", 150),
  genericFormatter("bicsIndustrySubgroup", "Bics Industry Subgroup", 180),
  genericFormatter("strikePrice", "Strike Price", 120),
  genericFormatter("clearingMethod", "Clearing Method", 120),
  genericFormatter("countryOfRisk", "Country Of Risk", 120),
  genericFormatter("forwardQuantity1", "Forward Quantity 1", 150),
  genericFormatter("forwardQuantity2", "Forward Quantity 2", 150),
  genericFormatter("forwardSpn1", "Forward SPN 1", 120),
  genericFormatter("forwardSpn2", "Forward SPN 2", 120),
  genericFormatter("gicsIndustry", "Gics Industry", 120),
  genericFormatter("gicsIndustryGroup", "Gics Industry Group", 150),
  genericFormatter("gicsSector", "Gics Sector", 120),
  genericBooleanFormatter("isNdf", "Is Ndf", 120),
  genericFormatter("issueSize", "Issue Size", 120),
  genericFormatter("issuerSpn", "Issuer SPN", 120),
  genericFormatter("offDate", "Off Date", 150),
  genericFormatter("onDate", "On Date", 150),
  genericFormatter("outstandingAmt", "Outstanding Amount", 150),
  genericFormatter("propeqIgroup", "PropEq IGroup", 120),
  genericFormatter("putCallInd", "Put Call Index", 120),
  genericFormatter("ratingMoody", "Rating Moody", 120),
  genericFormatter("ratingSandP", "Rating S&P", 120),
  genericFormatter("referenceSpn", "Reference SPN", 120),
  genericFormatter("ticker", "Ticker", 120),
  genericFormatter("tickUnit", "Tick Unit", 60),
];

export const UNDERLYING_SECURITY_ATTRS = [
  genericFormatter("underlyingSedol", "Underlying Sedol", 120),
  genericFormatter("underlyingRic", "Underlying Ric", 120),
  genericFormatter("underlyingIsin", "Underlying Isin", 120),
  genericFormatter("underlyingLocal", "Underlying Local", 120),
  genericFormatter("underlyingBloomberg", "Underlying Bloomberg", 150),
  genericFormatter("underlyingCusip", "Underlying Cusip", 120),
];

export const RULE_META_COLUMNS = [
  genericFormatter("houseRuleUserName", "House Rule User Name", 120),
  genericFormatter("houseRuleUserComments", "House Rule User Comments", 200),
  genericFormatter("houseRuleUsageType", "House Rule Usage Type", 120),
  getFormattedNumericColumn(
    "houseRuleUsageHaircut",
    "House Rule Usage Haircut",
    150,
    "",
    4
  ),
  genericFormatter("houseRuleUsageCurrency", "House Rule Usage Currency", 120),

  genericFormatter("regRuleUserName", "Reg Rule User Name", 120),
  genericFormatter("regRuleUserComments", "Reg Rule User Comments", 200),
  genericFormatter("regRuleUsageType", "Reg Rule Usage Type", 120),
  getFormattedNumericColumn(
    "regRuleUsageHaircut",
    "Reg Rule Usage Haircut",
    150,
    "",
    4
  ),
  genericFormatter("regRuleUsageCurrency", "Reg Rule Usage Currency", 120),

  genericFormatter("regCpeRuleUserName", "Reg CPE Rule User Name", 120),
  genericFormatter("regCpeRuleUserComments", "Reg CPE Rule User Comments", 200),
  genericFormatter("regCpeRuleUsageType", "Reg CPE Rule Usage Type", 120),
  getFormattedNumericColumn(
    "regCpeRuleUsageHaircut",
    "Reg CPE Rule Usage Haircut",
    150,
    "",
    4
  ),
  genericFormatter("regCpeRuleUsageCurrency", "Reg CPE Rule Usage Currency", 120),
]

export const MARGIN_DETAILS = (prefix?: string, suffixCurrency?: string) => [
  getFormattedPercentColumn(
    "reqHairCut",
    "Requirement Haircut (in %)",
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "requirementAdjustment" + suffixCurrency,
    "Requirement Adjustment " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "adjustedRequirement" + suffixCurrency,
    "Adjusted Requirement " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "houseFinancedMargin" + suffixCurrency,
    "House Financed Margin " + suffixCurrency?.toUpperCase(),
    190,
    prefix
  ),
  getFormattedNumericColumn(
    "financedQuantity",
    "Financed Quantity",
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "houseUnfinancedMargin" + suffixCurrency,
    "House Unfinanced Margin " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "unfinancedQuantity",
    "Unfinanced Quantity",
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "marginDiff" + suffixCurrency,
    "Requirement Difference " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn("marginPrice", "Margin Price", 100, prefix, 4),
  getFormattedNumericColumn("marginQuantity", "Margin Quantity", 120, prefix),
  getFormattedNumericColumn(
    "marginMarketValue",
    "Margin Market Value",
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "marginMarketValue" + suffixCurrency,
    "Margin Market Value " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn("underlyingValue", "Underlying Value", 150, prefix),
  getFormattedNumericColumn(
    "underlyingValue" + suffixCurrency,
    "Underlying Value " + suffixCurrency?.toUpperCase(),
    160,
    prefix
  ),
  getFormattedNumericColumn(
    "applicableMargin" + suffixCurrency,
    "Requirement " + suffixCurrency?.toUpperCase(),
    140,
    prefix
  ),
  genericFormatter(
    "houseCalculatorStatus",
    "House Calculator Status",
    180,
    prefix
  ),
  genericFormatter("houseCalculator", "House Calculator", 150, prefix),
  genericFormatter("houseCalculationComment", "House Calculation Comment", 200, prefix),
  getFormattedNumericColumn(
    "houseMarginHaircut",
    "House Margin Haircut",
    150,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "houseMarginNative",
    "House Margin Native",
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "houseMargin" + suffixCurrency,
    "House Margin " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "houseUsageHaircut",
    "House Usage Haircut",
    150,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "houseUsage" + suffixCurrency,
    "House Usage " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "houseUsageNative",
    "House Usage Native",
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "houseMarginAdjustment" + suffixCurrency,
    "House Margin Adjustment " + suffixCurrency?.toUpperCase(),
    210,
    prefix
  ),
  getFormattedNumericColumn(
    "houseAdjustedMargin" + suffixCurrency,
    "Adjusted House Margin " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "houseUsageAdjustment" + suffixCurrency,
    "House Usage Adjustment " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "houseAdjustedUsage" + suffixCurrency,
    "Adjusted House Usage " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),

  getFormattedNumericColumn(
    "regFinancedMargin" + suffixCurrency,
    "Reg Financed Margin " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "regFinancedQuantity",
    "Reg Financed Quantity",
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "regUnfinancedMargin" + suffixCurrency,
    "Reg Unfinanced Margin " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "regUnfinancedQuantity",
    "Reg Unfinanced Quantity",
    180,
    prefix
  ),
  genericFormatter("regCalculatorStatus", "Reg Calculator Status", 180, prefix),
  genericFormatter("regCalculator", "Reg Calculator", 120, prefix),
  getFormattedNumericColumn(
    "regMarginHaircut",
    "Reg Margin Haircut",
    180,
    prefix,
    4
  ),
  genericFormatter("regCalculationComment", "Reg Calculation Comment", 200, prefix),
  getFormattedNumericColumn(
    "regMarginNative",
    "Reg Margin Native",
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "regMargin" + suffixCurrency,
    "Reg Margin " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "regUsageHaircut",
    "Reg Usage Haircut",
    150,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "regUsage" + suffixCurrency,
    "Reg Usage " + suffixCurrency?.toUpperCase(),
    120,
    prefix
  ),
  getFormattedNumericColumn("regUsageNative", "Reg Usage Native", 150, prefix),
  getFormattedNumericColumn(
    "regMarginAdjustment" + suffixCurrency,
    "Reg Margin Adjustment " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "regAdjustedMargin" + suffixCurrency,
    "Adjusted Reg Margin " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "regUsageAdjustment" + suffixCurrency,
    "Reg Usage Adjustment " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "regAdjustedUsage" + suffixCurrency,
    "Adjusted Reg Usage " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),

  genericFormatter(
    "regCpeCalculatorStatus",
    "Reg Cpe Calculator Status",
    200,
    prefix
  ),
  genericFormatter("regCpeCalculator", "Reg Cpe Calculator", 180, prefix),
  genericFormatter("regCpeCalculationComment", "Reg Cpe Calculation Comment", 200, prefix),
  getFormattedNumericColumn(
    "regCpeMarginHaircut",
    "Reg Cpe Margin Haircut",
    180,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "regCpeMarginNative",
    "Reg Cpe Margin Native",
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "regCpeMargin" + suffixCurrency,
    "Reg Cpe Margin " + suffixCurrency?.toUpperCase(),
    160,
    prefix
  ),
  getFormattedNumericColumn(
    "regCpeUsageHaircut",
    "Reg Cpe Usage Haircut",
    180,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "regCpeUsage" + suffixCurrency,
    "Reg Cpe Usage " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "regCpeUsageNative",
    "Reg Cpe Usage Native",
    180,
    prefix
  ),
];

export const INTERNAL_EXPOSURE = (prefix?: string, suffixCurrency?: string) => [
  getFormattedNumericColumn(
    "exposure" + suffixCurrency,
    "Exposure " + suffixCurrency?.toUpperCase(),
    120,
    prefix
  ),
  getFormattedNumericColumn(
    "exposureAdjustment" + suffixCurrency,
    "Exposure Adjustment " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "adjustedExposure" + suffixCurrency,
    "Adjusted Exposure " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  genericFormatter("resetDate", "Reset Date", 100, prefix),
  genericFormatter("resetSettleDate", "Reset Settle Date", 150, prefix),
  getFormattedNumericColumn(
    "tradeReturnFromLastReset" + suffixCurrency,
    "Trade Return From Last Reset " + suffixCurrency?.toUpperCase(),
    240,
    prefix
  ),
  getFormattedNumericColumn(
    "unrealizedAI" + suffixCurrency,
    "Unrealized AI " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "unrealizedCommissions" + suffixCurrency,
    "Unrealized Commissions " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "unrealizedCorpact" + suffixCurrency,
    "Unrealized Corpact " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "unrealizedFinancing" + suffixCurrency,
    "Unrealized Financing " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "unrealizedPriceReturn" + suffixCurrency,
    "Unrealized Price Return " + suffixCurrency?.toUpperCase(),
    200,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "overdueReceivablePayable" + suffixCurrency,
    "Overdue Receivable/Payable due to Trades " + suffixCurrency?.toUpperCase(),
    280,
    prefix
  ),
  getFormattedNumericColumn(
    "markToMarket" + suffixCurrency,
    "Mark to Market " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "cleanPrice" + suffixCurrency,
    "Clean Price " + suffixCurrency?.toUpperCase(),
    120,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "dirtyPrice" + suffixCurrency,
    "Dirty Price " + suffixCurrency?.toUpperCase(),
    120,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "loanValue" + suffixCurrency,
    "Loan Value " + suffixCurrency?.toUpperCase(),
    120,
    prefix
  ),
  getFormattedNumericColumn(
    "loanAI" + suffixCurrency,
    "Loan AI " + suffixCurrency?.toUpperCase(),
    120,
    prefix
  ),
  getFormattedNumericColumn(
    "bondValue" + suffixCurrency,
    "Bond Value " + suffixCurrency?.toUpperCase(),
    120,
    prefix
  ),
  getFormattedNumericColumn(
    "bondAI" + suffixCurrency,
    "Bond AI " + suffixCurrency?.toUpperCase(),
    120,
    prefix
  ),
  getFormattedNumericColumn("poolFactor", "Pool Factor", 100, prefix),
  getFormattedNumericColumn(
    "repoExposure" + suffixCurrency,
    "Repo Exposure " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "ote" + suffixCurrency,
    "OTE " + suffixCurrency?.toUpperCase(),
    100,
    prefix
  ),
  getFormattedNumericColumn(
    "repoMargin" + suffixCurrency,
    "Repo Margin " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "exposureDiff" + suffixCurrency,
    "Exposure Difference " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "settleDateQuantity",
    "Settle Date Quantity",
    180,
    prefix
  ),
];

export const BROKER_SECURITY_ATTRS = (
  prefix?: string,
  suffixCurrency?: string
) => [
  getFormattedNumericColumn("quantityDiff", "Quantity Diff", 100, prefix),
  genericFormatter("brokerSecurityName", "Broker Security Name", 150, prefix),
  genericFormatter("brokerGboType", "Broker Gbo Type", 120, prefix),
  genericFormatter("brokerCountry", "Broker Country", 120, prefix),
  genericFormatter("brokerDeath", "Broker Death", 100, prefix),
  genericFormatter("brokerIsin", "Broker-ISIN", 100, prefix),
  genericFormatter("brokerCusip", "Broker-Cusip", 100, prefix),
  genericFormatter("brokerSedol", "Broker-Sedol", 100, prefix),
  genericFormatter(
    "brokerInstrumentName",
    "Broker Instrument Name",
    180,
    prefix
  ),
  genericFormatter("brokerProduct", "Broker Product", 120, prefix),
  genericFormatter("brokerTradeDate", "Broker Trade Date", 150, prefix),
  genericFormatter("brokerMaturityDate", "Broker Maturity Date", 150, prefix),
  genericFormatter("brokerMatchKey", "Match Key", 100, prefix),
  genericFormatter("brokerMatchValue", "Match Value", 100, prefix),
  genericFormatter("brokerUniqueKey", "Broker Unique Key", 150, prefix),
  getFormattedNumericColumn("brokerFxRate", "Broker Fx Rate", 120, prefix),
  getFormattedNumericColumn(
    "brokerExposure" + suffixCurrency,
    "Broker Exposure " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerRegExposure" + suffixCurrency,
    "Broker Reg Exposure " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  genericFormatter(
    "brokerMarketValueCurrencyCode",
    "Broker Market Value Currency",
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerMarketValueLocal",
    "Broker Market Value Local",
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerMarketValue" + suffixCurrency,
    "Broker Market Value " + suffixCurrency?.toUpperCase(),
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerMargin" + suffixCurrency,
    "Broker Margin " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerRegMargin" + suffixCurrency,
    "Broker Reg Margin " + suffixCurrency?.toUpperCase(),
    170,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerMarginFxRate",
    "Broker Margin FxRate",
    150,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerRegFxRate",
    "Broker Reg FxRate",
    150,
    prefix
  ),
  genericFormatter("brokerBirth", "Broker-Birth", 100, prefix),
  genericFormatter("brokerLocal", "Broker-Local", 100, prefix),
  genericFormatter("brokerRic", "Broker-Ric", 100, prefix),
  genericFormatter("brokerBloomberg", "Broker-Bloomberg", 150, prefix),
  genericFormatter(
    "brokerUnderlyingDeath",
    "Broker-Underlying Death",
    180,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingIsin",
    "Broker-Underlying ISIN",
    180,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingSedol",
    "Broker-Underlying Sedol",
    180,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingLocal",
    "Broker-Underlying Local",
    180,
    prefix
  ),
  genericFormatter("brokerUnderlyingRic", "Broker-Underlying Ric", 180, prefix),
  genericFormatter(
    "brokerUnderlyingBloomberg",
    "Broker-Underlying Bloomberg",
    200,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingCanonicalSpnRic",
    "Broker-Underlying Canonical Ric",
    220,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingSpotSpnLocal",
    "Broker-Underlying Spot Local",
    220,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingUnderlyingLocal",
    "Broker-Underlying Underlying Local",
    240,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingUnderlyingRic",
    "Broker-Underlying Underlying Ric",
    240,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingForwardEndDate",
    "Broker-Underlying Forward End Date",
    240,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingForwardSettleDate",
    "Broker-Underlying Forward Settle Date",
    260,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingIssuerSpnRedCode",
    "Broker-Underlying Issuer Red Code",
    240,
    prefix
  ),
  genericFormatter("brokerBondSpnIsin", "Broker-Bond ISIN", 150, prefix),
  genericFormatter("brokerBondSpnCusip", "Broker-Bond Cusip", 150, prefix),
  genericFormatter(
    "brokerReferenceSpnIsin",
    "Broker-Reference ISIN",
    180,
    prefix
  ),
  genericFormatter(
    "brokerReferenceSpnCusip",
    "Broker-Reference Cusip",
    180,
    prefix
  ),
  genericFormatter("brokerSpotSpnIsin", "Broker-Spot ISIN", 150, prefix),
  genericFormatter("brokerSpotSpnLocal", "Broker-Spot Local", 150, prefix),
  genericFormatter("brokerSpotSpnRic", "Broker-Spot Ric", 150, prefix),
  genericFormatter(
    "brokerSpotSpnBloomberg",
    "Broker-Spn Bloomberg",
    180,
    prefix
  ),
  genericFormatter("brokerAmeriEuroInd", "Broker-Ameri Euro Ind", 180, prefix),
  genericFormatter("brokerPutCallInd", "Broker-Put Call Ind", 150, prefix),
  getFormattedNumericColumn(
    "brokerStrikePrice" + suffixCurrency,
    "Broker-Strike Price " + suffixCurrency?.toUpperCase(),
    180,
    prefix,
    4
  ),
  genericFormatter(
    "brokerExpirationDate",
    "Broker-Expiration Date",
    180,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingPutCallInd",
    "Broker-Underlying Put Call Ind",
    200,
    prefix
  ),
  genericFormatter(
    "brokerUnderlyingExpirationDate",
    "Broker-Underlying Expiration Date",
    220,
    prefix
  ),
  genericFormatter("brokerSwapStart", "Broker-Swap Start", 150, prefix),
  genericFormatter("brokerSwapLevel", "Broker-Swap Level", 150, prefix),
  getFormattedPercentColumn(
    "brokerRequirementHaircutPct",
    "Broker Requirement Haircut (in %)",
    240,
    prefix
  ),
  genericFormatter("brokerForwardSpn1", "Broker-Forward SPN 1", 150, prefix),
  genericFormatter("brokerForwardSpn2", "Broker-Forward SPN 2", 150, prefix),
  genericFormatter(
    "brokerForwardCurrency1",
    "Broker-Forward Currency 1",
    200,
    prefix
  ),
  genericFormatter(
    "brokerForwardCurrency2",
    "Broker-Forward Currency 2",
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerForwardQuantity1",
    "Broker-Forward Quantity 1",
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerForwardQuantity2",
    "Broker-Forward Quantity 2",
    200,
    prefix
  ),
  genericFormatter("brokerOnDate", "Broker-Repo On Date", 150, prefix),
  genericFormatter("brokerOffDate", "Broker-Repo Off Date", 150, prefix),
  genericFormatter("brokerRepoRate", "Broker-Repo Rate", 150, prefix),
  genericFormatter("brokerAccruedDate", "Broker-Accrued Date", 150, prefix),
  genericFormatter(
    "brokerSpread" + suffixCurrency,
    "Broker-Spread " + suffixCurrency?.toUpperCase(),
    150,
    prefix
  ),
  genericFormatter("brokerBuySell", "Broker-Buy Sell", 150, prefix),
  getFormattedNumericColumn(
    "brokerNativePrice",
    "Broker Native Price",
    150,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "internalVsBrokerPriceDiff" + suffixCurrency,
    "Internal Vs Broker Price Diff " + suffixCurrency?.toUpperCase(),
    220,
    prefix,
    4
  ),
  getFormattedPercentColumn(
    "internalVsBrokerPriceDiffPercent",
    "Internal Vs Broker Price Diff (in %)",
    240,
    prefix
  ),
  getFormattedNumericColumn(
    "settleDateMarketValue" + suffixCurrency,
    "Settle Date Market Value " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  genericFormatter(
    "brokerMarginCurrency",
    "Broker Margin Currency",
    180,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerUnderlyingStrikePrice" + suffixCurrency,
    "Broker Underlying Strike Price " + suffixCurrency?.toUpperCase(),
    240,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "brokerApplicableMargin" + suffixCurrency,
    "Broker Applicable Margin " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerRegulatoryApplicableMargin" + suffixCurrency,
    "Broker Regulatory Applicable Margin " + suffixCurrency?.toUpperCase(),
    280,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerRegCpeMargin" + suffixCurrency,
    "Broker Reg Cpe Margin " + suffixCurrency?.toUpperCase(),
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "brokerRegCpeApplicableMargin" + suffixCurrency,
    "Broker Reg Cpe Applicable Margin " + suffixCurrency?.toUpperCase(),
    260,
    prefix
  ),
];

export const EXPOSURE_RECON = (prefix?: string, suffixCurrency?: string) => [
  getFormattedNumericColumn("cpeQuantity", "Recon Quantity", 150, prefix),
  getFormattedNumericColumn(
    "internalVsReconQuantityDiff",
    "Internal Vs Recon Quantity Diff",
    200,
    prefix
  ),
  getFormattedNumericColumn(
    "reconVsBrokerQuantityDiff",
    "Recon Vs Broker Quantity Diff",
    200,
    prefix
  ),
  getFormattedNumericColumn("cpeMarginUsd", "Recon Margin " + suffixCurrency?.toUpperCase(), 150, prefix),
  getFormattedNumericColumn(
    "internalVsReconMarginDiffUsd",
    "Internal Vs Recon Margin Diff " + suffixCurrency?.toUpperCase(),
    220,
    prefix
  ),
  getFormattedNumericColumn(
    "reconVsBrokerMarginDiffUsd",
    "Recon Vs Broker Margin Diff " + suffixCurrency?.toUpperCase(),
    220,
    prefix
  ),
  getFormattedNumericColumn("cpePrice", "Recon Price", 100, prefix,4),
  getFormattedNumericColumn(
    "internalVsReconPriceDiffUsd",
    "Internal Vs Recon Price Diff " + suffixCurrency?.toUpperCase(),
    220,
    prefix,
    4
  ),
  getFormattedNumericColumn(
    "reconVsBrokerPriceDiffUsd",
    "Recon Vs Broker Price Diff " + suffixCurrency?.toUpperCase(),
    220,
    prefix,
    4
  ),
  getFormattedPercentColumn(
    "internalVsReconPriceDiffPercent",
    "Internal Vs Recon Price Diff (in %)",
    220,
    prefix
  ),
  getFormattedPercentColumn(
    "reconVsBrokerPriceDiffPercent",
    "Recon Vs Broker Price Diff (in %)",
    220,
    prefix
  ),
  getFormattedNumericColumn(
    "cpeMarketValue",
    "Recon Market Value " + suffixCurrency?.toUpperCase(),
    220,
    prefix
  ),
  getFormattedNumericColumn(
    "internalVsReconExposureDiffUsd",
    "Internal Vs Recon Exposure Diff " + suffixCurrency?.toUpperCase(),
    240,
    prefix
  ),
  getFormattedNumericColumn(
    "reconVsBrokerExposureDiffUsd",
    "Recon Vs Broker Exposure Diff " + suffixCurrency?.toUpperCase(),
    240,
    prefix
  ),
  getFormattedNumericColumn(
    "cpeLongMarketValue",
    "Recon Long Market Value " + suffixCurrency?.toUpperCase(),
    220,
    prefix
  ),
  getFormattedNumericColumn(
    "cpeShortMarketValue",
    "Recon Short Market Value " + suffixCurrency?.toUpperCase(),
    220,
    prefix
  ),
  genericFormatter("recStatus", "Recon Status", 100, prefix),
  genericFormatter("recCode", "Recon Code", 100, prefix),
  getFormattedNumericColumn("reconLoanAmount", "Recon Loan Amount", 150, prefix),
  getFormattedNumericColumn("reconBondPrice", "Recon Bond Price", 150, prefix,4),
  getFormattedPriceColumn("reconBondMarketValue", "Recon Bond Market Value", 180, prefix),
  getFormattedNumericColumn("reconBondQuantity", "Recon Bond Quantity", 150, prefix),
  getFormattedPriceColumn("reconPoolFactor", "Recon Pool Factor", 150, prefix),
  getFormattedNumericColumn("reconBondAccruedInterest", "Recon Bond Accrued Interest", 200, prefix),
  getFormattedNumericColumn("reconLoanAccruedInterest", "Recon Loan Accrued Interest", 200, prefix),
  getFormattedPercentColumn("reconHaircut", "Recon Bond Haircut", 150, prefix),
  getFormattedNumericColumn(
     "reconExposureUSD",
     "Recon Exposure " + suffixCurrency?.toUpperCase(),
      150,
      prefix
  ),
  getFormattedNumericColumn(
     "reconSegMarginUSD",
     "Recon Seg Margin " + suffixCurrency?.toUpperCase(),
      180,
      prefix
  ),
  getFormattedNumericColumn(
     "reconRegMarginUSD" ,
     "Recon Reg Margin " + suffixCurrency?.toUpperCase(),
      180,
      prefix
  ),
  getFormattedNumericColumn(
     "reconExchangeMarginUSD" ,
     "Recon Exchange Margin " + suffixCurrency?.toUpperCase(),
      200,
      prefix
  )
];

export const BUNDLE_INTERNAL_EXPOSURE = (suffixCurrency?: string) => [
  getFormattedNumericColumn(
    "exposure" + suffixCurrency,
    "Exposure " + suffixCurrency?.toUpperCase(),
    100
  ),
  getFormattedNumericColumn(
    "exposureAdjustment" + suffixCurrency,
    "Exposure Adjustment " + suffixCurrency?.toUpperCase(),
    120
  ),
  getFormattedNumericColumn(
    "adjustedExposure" + suffixCurrency,
    "Adjusted Exposure " + suffixCurrency?.toUpperCase(),
    120
  ),
];
