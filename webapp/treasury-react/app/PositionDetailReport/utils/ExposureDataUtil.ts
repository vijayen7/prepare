import { PREV_DAY_PREFIX } from "../constants";
import {
  getDateColumnsData,
  getGenericColumnsData,
  getNumericColumnsData,
} from "./CommonUtil";

export const getInternalExposureData = (
  row: { [key: string]: string | number | boolean | undefined },
  prefixString: string,
  enrichWithRCFields?: boolean
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let numericMap = new Map();
  let dateMap = new Map();
  let numericLocalValuesMap = new Map();

  if (enrichWithRCFields) {
    numericMap.set("exposureRC", "exposureDetail.exposureRC");
    numericMap.set(
      "exposureAdjustmentUsd",
      "lcmPositionDetail.exposureAdjustmentUsd"
    );
    numericMap.set(
      "adjustedExposureRC",
      "lcmPositionDetail.adjustedExposureRC"
    );
    numericMap.set(
      "tradeReturnFromLastResetRC",
      "exposureDetail.isdaPositionMtmData.tradeReturnFromLastResetRC"
    );
    numericMap.set(
      "unrealizedAIRC",
      "exposureDetail.isdaPositionMtmData.unrealizedAIRC"
    );
    numericMap.set(
      "unrealizedCommissionsRC",
      "exposureDetail.isdaPositionMtmData.unrealizedCommissionsRC"
    );
    numericMap.set(
      "unrealizedCorpactRC",
      "exposureDetail.isdaPositionMtmData.unrealizedCorpactRC"
    );
    numericMap.set(
      "unrealizedFinancingRC",
      "exposureDetail.isdaPositionMtmData.unrealizedFinancingRC"
    );
    numericMap.set(
      "unrealizedPriceReturnRC",
      "exposureDetail.isdaPositionMtmData.unrealizedPriceReturnRC"
    );
    numericMap.set(
      "overdueReceivablePayableRC",
      "exposureDetail.isdaPositionMtmData.overdueReceivablePayableRC"
    );
    numericMap.set(
      "markToMarketRC",
      "exposureDetail.isdaPositionMtmData.markToMarketRC"
    );
    numericMap.set("oteRC", "exposureDetail.fcmPositionOteData.oteRC");
    numericMap.set("exposureDiffRC", "lcmPositionDetail.exposureDiffRC");
    numericMap.set(
      "cleanPriceRC",
      "exposureDetail.repoPositionExposureData.cleanPriceRC"
    );
    numericMap.set(
      "dirtyPriceRC",
      "exposureDetail.repoPositionExposureData.dirtyPriceRC"
    );
    numericMap.set(
      "loanAIRC",
      "exposureDetail.repoPositionExposureData.loanAIRC"
    );
    numericMap.set(
      "bondAIRC",
      "exposureDetail.repoPositionExposureData.bondAIRC"
    );
    numericMap.set(
      "bondValueRC",
      "exposureDetail.repoPositionExposureData.bondValueRC"
    );
    numericMap.set(
      "loanValueRC",
      "exposureDetail.repoPositionExposureData.loanValueRC"
    );
    numericMap.set(
      "repoExposureRC",
      "exposureDetail.repoPositionExposureData.repoExposureRC"
    );
  } else {
    numericMap.set("exposureUsd", "exposureDetail.exposureUsd");
    numericMap.set(
      "exposureAdjustmentRC",
      "lcmPositionDetail.exposureAdjustmentRC"
    );
    numericMap.set(
      "adjustedExposureUsd",
      "lcmPositionDetail.adjustedExposureUsd"
    );
    numericLocalValuesMap.set(
      "tradeReturnFromLastResetUsd",
      "exposureDetail.isdaPositionMtmData.tradeReturnFromLastReset"
    );
    numericLocalValuesMap.set(
      "unrealizedAIUsd",
      "exposureDetail.isdaPositionMtmData.unrealizedAI"
    );
    numericLocalValuesMap.set(
      "unrealizedCommissionsUsd",
      "exposureDetail.isdaPositionMtmData.unrealizedCommissions"
    );
    numericLocalValuesMap.set(
      "unrealizedCorpactUsd",
      "exposureDetail.isdaPositionMtmData.unrealizedCorpact"
    );
    numericLocalValuesMap.set(
      "unrealizedFinancingUsd",
      "exposureDetail.isdaPositionMtmData.unrealizedFinancing"
    );
    numericLocalValuesMap.set(
      "unrealizedPriceReturnUsd",
      "exposureDetail.isdaPositionMtmData.unrealizedPriceReturn"
    );
    numericLocalValuesMap.set(
      "overdueReceivablePayableUsd",
      "exposureDetail.isdaPositionMtmData.overdueReceivablePayable"
    );
    numericMap.set(
      "markToMarketUsd",
      "exposureDetail.isdaPositionMtmData.markToMarket"
    );
    numericMap.set("oteUsd", "exposureDetail.fcmPositionOteData.ote");
    numericMap.set("exposureDiffUsd", "lcmPositionDetail.exposureDiff");
    numericMap.set(
      "cleanPriceUsd",
      "exposureDetail.repoPositionExposureData.cleanPrice"
    );
    numericMap.set(
      "dirtyPriceUsd",
      "exposureDetail.repoPositionExposureData.dirtyPrice"
    );
    numericMap.set(
      "loanAIUsd",
      "exposureDetail.repoPositionExposureData.loanAI"
    );
    numericMap.set(
      "bondAIUsd",
      "exposureDetail.repoPositionExposureData.bondAI"
    );
    numericMap.set(
      "bondValueUsd",
      "exposureDetail.repoPositionExposureData.bondValue"
    );
    numericMap.set(
      "loanValueUsd",
      "exposureDetail.repoPositionExposureData.loanValue"
    );
    numericMap.set(
      "repoExposureUsd",
      "exposureDetail.repoPositionExposureData.exposure"
    );
  }
  dateMap.set("resetDate", "exposureDetail.isdaPositionMtmData.resetDate");
  dateMap.set(
    "resetSettleDate",
    "exposureDetail.isdaPositionMtmData.resetSettleDate"
  );
  numericMap.set("settleDateQuantity", "settleDateQuantity");
  numericMap.set(
    "poolFactor",
    "exposureDetail.repoPositionExposureData.poolFactor"
  );

  let dateInternalExposureData: {
    [key: string]: string | number | boolean | undefined;
  } = getDateColumnsData(row, columnMapperPrefix, prefixString, dateMap);

  let numericInternalExposureData: {
    [key: string]: string | number | boolean | undefined;
  } = getNumericColumnsData(row, columnMapperPrefix, prefixString, numericMap);

  let numericLocalInternalExposureData: {
    [key: string]: string | number | boolean | undefined;
  } = getNumericColumnsData(
    row,
    columnMapperPrefix,
    prefixString,
    numericLocalValuesMap,
    Number(row[prefixString + "fxRate"])
  );

  return {
    ...dateInternalExposureData,
    ...numericInternalExposureData,
    ...numericLocalInternalExposureData,
  };
};

export const getExposureReconData = (
  row: { [key: string]: string | number | boolean | undefined },
  prefixString: string,
  enrichWithRCFields?: boolean
) => {
  let columnMapperPrefix: string =
    prefixString === PREV_DAY_PREFIX ? "prevDay" : "";

  let numericMap = new Map();
  let genericMap = new Map();
  let numericReconUsdMap = new Map();

  numericMap.set("cpeQuantity", "cpeReconciledPositionsData.cpeQuantity");
  numericMap.set("cpePrice", "cpeReconciledPositionsData.cpePrice");
  numericMap.set(
    "internalVsReconQuantityDiff",
    "lcmReconDiffDetail.internalVsReconDiffDetail.quantityDiff"
  );
  numericMap.set(
    "reconVsBrokerQuantityDiff",
    "lcmReconDiffDetail.reconVsBrokerDiffDetail.quantityDiff"
  );
  numericReconUsdMap.set(
    "cpeMarginUsd",
    "cpeReconciledPositionsData.marginUSD"
  );
  numericMap.set(
    "reconVsBrokerPriceDiffPercent",
    "lcmReconDiffDetail.reconVsBrokerDiffDetail.priceDiffPercent"
  );
  numericReconUsdMap.set(
    "internalVsReconMarginDiffUsd",
    "lcmReconDiffDetail.internalVsReconDiffDetail.marginDiff"
  );
  numericReconUsdMap.set(
    "reconVsBrokerMarginDiffUsd",
    "lcmReconDiffDetail.reconVsBrokerDiffDetail.marginDiff"
  );
  numericReconUsdMap.set(
    "internalVsReconPriceDiffUsd",
    "lcmReconDiffDetail.internalVsReconDiffDetail.priceDiff"
  );
  numericReconUsdMap.set(
    "reconVsBrokerPriceDiffUsd",
    "lcmReconDiffDetail.reconVsBrokerDiffDetail.priceDiff"
  );
  numericMap.set(
    "internalVsReconPriceDiffPercent",
    "lcmReconDiffDetail.internalVsReconDiffDetail.priceDiffPercent"
  );
  numericReconUsdMap.set(
    "internalVsReconExposureDiffUsd",
    "lcmReconDiffDetail.internalVsReconDiffDetail.exposureDiff"
  );
  numericReconUsdMap.set(
    "reconVsBrokerExposureDiffUsd",
    "lcmReconDiffDetail.reconVsBrokerDiffDetail.exposureDiff"
  );
  numericReconUsdMap.set(
    "cpeMarketValue",
    "cpeReconciledPositionsData.cpeMarketValue"
  );
  numericReconUsdMap.set(
    "cpeLongMarketValue",
    "cpeReconciledPositionsData.cpeLongMarketValue"
  );
  numericReconUsdMap.set(
    "cpeShortMarketValue",
    "cpeReconciledPositionsData.cpeShortMarketValue"
  );
  numericReconUsdMap.set(
    "reconExposureUSD",
    "cpeReconciledRepoData.exposure"
  );
  numericMap.set(
    "reconLoanAmount",
    "cpeReconciledRepoData.loanAmount"
  );
  numericMap.set(
    "reconBondPrice",
    "cpeReconciledRepoData.bondPrice"
  );
  numericMap.set(
    "reconBondMarketValue",
    "cpeReconciledRepoData.bondMarketValue"
  );
  numericMap.set(
    "reconBondQuantity",
    "cpeReconciledRepoData.bondQuantity"
  );
  numericMap.set(
    "reconPoolFactor",
    "cpeReconciledRepoData.poolFactor"
  );
  numericMap.set(
    "reconBondAccruedInterest",
    "cpeReconciledRepoData.bondAccruedInterest"
  );
  numericMap.set(
    "reconLoanAccruedInterest",
    "cpeReconciledRepoData.loanAccruedInterest"
  );
  numericMap.set(
    "reconHaircut",
    "cpeReconciledRepoData.haircut"
  );
  numericReconUsdMap.set(
    "reconSegMarginUSD",
    "cpeReconciledPositionsData.segMarginUSD"
  );
  numericReconUsdMap.set(
    "reconRegMarginUSD",
    "cpeReconciledPositionsData.regMarginUSD"
  );
  numericReconUsdMap.set(
    "reconExchangeMarginUSD",
    "cpeReconciledPositionsData.exchangeMarginUSD"
  );
  genericMap.set("recCode", "cpeReconciledPositionsData.recCodes");
  genericMap.set("recStatus", "cpeReconciledPositionsData.recStatus");

  let genericExposureReconData: {
    [key: string]: string | number | boolean | undefined;
  } = getGenericColumnsData(row, columnMapperPrefix, prefixString, genericMap);

  let numericExposureReconData: {
    [key: string]: string | number | boolean | undefined;
  } = getNumericColumnsData(row, columnMapperPrefix, prefixString, numericMap);

  let numericExposureReconDataForRC: {
    [key: string]: string | number | boolean | undefined;
  };
  if (enrichWithRCFields)
    numericExposureReconDataForRC = getNumericColumnsData(
      row,
      columnMapperPrefix,
      prefixString,
      numericReconUsdMap,
      Number(row[prefixString + "reportingCurrencyFxRate"])
    );
  else
    numericExposureReconDataForRC = getNumericColumnsData(
      row,
      columnMapperPrefix,
      prefixString,
      numericReconUsdMap
    );

  return {
    ...genericExposureReconData,
    ...numericExposureReconData,
    ...numericExposureReconDataForRC,
  };
};
