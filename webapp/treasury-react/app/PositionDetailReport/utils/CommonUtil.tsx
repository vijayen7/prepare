import React from "react";
import {
  DATASET_TYPE,
  POSITION_RULE_ID,
  TOAST_TYPE,
  DATE_19000101,
  DATE_20380101
} from "../constants";
import { ToastService } from "arc-react-components";
import {
  convertJavaNYCDashedDate,
  decamelize,
  openReportManager,
  convertJavaDate
} from "../../commons/util";
import { SearchFilter } from "../models/UIDataTypes";
import { PopUpSelectedData } from "../../MarginRules/models/UIDataTypes";
import {
  getLatestMarginReportUrl,
  getPositionDetailDataReportUrl,
} from "../api";
import SecuritySearch from "../grid/SecuritySearch";
import { PositionRuleData } from "../models/PositionRuleData";
import { getIdFromRefData } from "../../commons/TreasuryUtils";
import { getNameFromRefData } from "../../MarginRules/utils/commonUtil";
import { PositionMarginRuleInput } from "../models/PositionMarginRuleInput";
import CLASS_NAMESPACE from "../../commons/ClassConfigs";
import { CALCULATOR_TYPES_REF_DATA } from "../../MarginRules/constants";
import { numberConverter } from "./PositionDetailUtil";
import moment from "moment";

export function getPreviousWorkday() {
  const workday = moment();
  const day = workday.day();
  let diff = 1; // returns yesterday
  if (day == 0 || day == 1) {
    // is Sunday or Monday
    diff = day + 2; // returns Friday
  }
  return workday.subtract(diff, "days");
}

const getToastType = (type: string) => {
  let toastType: any = ToastService.ToastType.SUCCESS;
  switch (type) {
    case TOAST_TYPE.CRITICAL:
      toastType = ToastService.ToastType.CRITICAL;
      break;
    case TOAST_TYPE.SUCCESS:
      toastType = ToastService.ToastType.SUCCESS;
      break;
    case TOAST_TYPE.INFO:
      toastType = ToastService.ToastType.INFO;
      break;
  }
  return toastType;
};

export const showToastService = (content: string, type: string) => {
  ToastService.append({
    content: content,
    type: getToastType(type),
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: 7000,
  });
};

export const generateRulesReportFromAPI = (
  searchFilter: SearchFilter
) => {
  let apiUrl;

  if (
    searchFilter.selectedDataset?.key === DATASET_TYPE.Margin_Details &&
    !searchFilter.isPublished
  ) {
    apiUrl = getLatestMarginReportUrl(searchFilter);
  } else {
    apiUrl = getPositionDetailDataReportUrl(searchFilter);
  }
  openReportManager(apiUrl, null);
};

export const getPopUpSelectedData = (
  row: any,
  positionRuleData: PositionRuleData
) => {
  const popUpSelectedData: PopUpSelectedData = {
    selectedLegalEntities: getApplicablePopUpSelectedData(
      row.legalEntityId,
      row.legalEntity
    ),
    selectedCpes: getApplicablePopUpSelectedData(
      row.exposureCounterPartyId,
      row.exposureCounterParty
    ),
    selectedAgreementTypes: getApplicablePopUpSelectedData(
      row.agreementTypeId,
      row.agreementType
    ),
    selectedRuleTypes:
      positionRuleData &&
      getIdFromRefData(positionRuleData.ruleType) === POSITION_RULE_ID
        ? {
            key: getIdFromRefData(positionRuleData.ruleType) - 1,
            value: getNameFromRefData(positionRuleData.ruleType),
          }
        : null,
    selectedBooks: getApplicablePopUpSelectedData(row.bookId, row.book),
    selectedPnlSpn: row.pnlSpn ? row.pnlSpn.toString() : "",
    selectedCustodianAccounts: getApplicablePopUpSelectedData(
      row.custodianAccountId,
      row.custodianAccount
    ),
    selectedStartDate: positionRuleData && positionRuleData.effectiveStartDate ?
       convertJavaDate(positionRuleData.effectiveStartDate) : DATE_19000101,
    selectedEndDate: positionRuleData && positionRuleData.effectiveEndDate ?
       convertJavaDate(positionRuleData.effectiveEndDate) : DATE_20380101,
    selectedFieldTypes: getApplicablePopUpSelectedData(
      row.fieldTypeId,
      row.fieldType
    ),
    selectedMarginTypes: CALCULATOR_TYPES_REF_DATA[0],
    selectedUsageTypes:
      positionRuleData && positionRuleData.usageType
        ? {
            key: getIdFromRefData(positionRuleData.usageType),
            value: getNameFromRefData(positionRuleData.usageType),
          }
        : null,
    selectedUsageCurrency:
      positionRuleData && positionRuleData.usageCurrency
        ? {
            key: getIdFromRefData(positionRuleData.usageCurrency),
            value: getNameFromRefData(positionRuleData.usageCurrency),
          }
        : null,
    selectedUsageHaircut:
      positionRuleData && positionRuleData.usageHaircut
        ? positionRuleData.usageHaircut.toString()
        : "",
    selectedComments:
      positionRuleData && positionRuleData.userComments
        ? positionRuleData.userComments.toString()
        : "",
    selectedMarginCalculators:
      positionRuleData && positionRuleData.calculator
        ? {
            key: getIdFromRefData(positionRuleData.calculator),
            value: getNameFromRefData(positionRuleData.calculator),
          }
        : null,
    selectedAllocators:
      positionRuleData && positionRuleData.allocator
        ? {
            key: getIdFromRefData(positionRuleData.allocator),
            value: getNameFromRefData(positionRuleData.allocator),
          }
        : null,
    selectedRuleId:  positionRuleData &&
      getIdFromRefData(positionRuleData.ruleType) === POSITION_RULE_ID ? positionRuleData.ruleId : null
  };

  return popUpSelectedData;
};

export const createRuleMap = (selectedRow: any) => {
  var ruleDataMap: PositionMarginRuleInput = {
    "@CLASS":
      CLASS_NAMESPACE.com.arcesium.treasury.margin.model
        .PositionMarginRuleInput,
    marginTypeId: 1,
    date: selectedRow.date.replaceAll("-", ""),
    legalEntityId: selectedRow.legalEntityId,
    counterPartyEntityId: selectedRow.exposureCounterPartyId,
    agreementTypeId: selectedRow.agreementTypeId,
    pnlSpn: selectedRow.pnlSpn,
    custodianAccountId: selectedRow.custodianAccountId,
    bookId: selectedRow.bookId,
    marketId: selectedRow.marketId,
    ccySpn: selectedRow.currencyId,
    countryId: selectedRow.countryId,
    fieldId: selectedRow.fieldTypeId,
    gboType: selectedRow.gboTypeId,
    foType: selectedRow.foTypeId,
    underlyingGboType: selectedRow.underlyingGboTypeId,
    isValid: 1,
  };

  return ruleDataMap;
};

export const getApplicablePopUpSelectedData = (
  inputKey: number,
  inputValue: string
) => {
  return inputKey ? { key: inputKey, value: inputValue } : null;
};

export const getApplicableRuleMapData = (fieldId: number) => {
  return fieldId ? fieldId.toString() : "";
};

export const getFormattedPnlSpn = (
  identity: any,
  header?: string,
  width?: number,
  prefix?: string
) => {
  let formattedTextColumn: any = {
    identity: prefix !== undefined ? prefix + identity : identity,
    Cell: (value, context) => <SecuritySearch context={context} />,
  };
  if (header) {
    formattedTextColumn.header =
      prefix !== undefined ? decamelize(prefix + header, " ") : header;
  }
  if (width) {
    formattedTextColumn.width = width;
  }
  return formattedTextColumn;
};

export const getGenericColumnsData = (
  row: any,
  columnMapperPrefix: string,
  prefixString: string,
  genericMap?: Map<string, string> | undefined,
  genericArray?: string[] | undefined
) => {
  let genericColumnsData: {
    [key: string]: string | number | boolean | undefined;
  } = {};

  if (genericMap?.size) {
    genericMap.forEach((value, key) => {
      genericColumnsData[columnMapperPrefix + key] = row[prefixString + value]
        ? row[prefixString + value]
        : undefined;
    });
  }

  if (genericArray && genericArray.length > 0) {
    genericArray.forEach((column) => {
      genericColumnsData[columnMapperPrefix + column] = row[prefixString + column]
        ? row[prefixString + column]
        : undefined;
    });
  }

  return genericColumnsData;
};

export const getDateColumnsData = (
  row: any,
  columnMapperPrefix: string,
  prefixString: string,
  dateMap?: Map<string, string> | undefined,
  dateArray?: string[]| undefined
) => {
  let dateColumnsData: {
    [key: string]: string | number | boolean | undefined;
  } = {};

  if (dateMap?.size) {
    dateMap.forEach((value, key) => {
      dateColumnsData[columnMapperPrefix + key] = row[prefixString + value]
        ? convertJavaNYCDashedDate(row[prefixString + value])
        : undefined;
    });
  }

  if (dateArray && dateArray?.length) {
    dateArray.forEach((column) => {
      dateColumnsData[columnMapperPrefix + column] = row[prefixString + column]
        ? convertJavaNYCDashedDate(row[prefixString + column])
        : undefined;
    });
  }

  return dateColumnsData;
};

export const getNumericColumnsData = (
  row: any,
  columnMapperPrefix: string,
  prefixString: string,
  numericMap?: Map<string, string> | undefined,
  fxRate?: number,
  numericArray?: string[] | undefined,
) => {
  let numericColumnsData: {
    [key: string]: string | number | boolean | undefined;
  } = {};
  let numericValue;
  if (numericMap?.size) {
    numericMap.forEach((value, key) => {
      numericValue = numberConverter(row[prefixString + value]);
      if (fxRate && numericValue)
        numericColumnsData[columnMapperPrefix + key] = numericValue * fxRate;
      else numericColumnsData[columnMapperPrefix + key] = numericValue;
    });
  }

  if (numericArray && numericArray?.length) {
    numericArray.forEach((column) => {
      numericColumnsData[columnMapperPrefix + column] = numberConverter(row[prefixString + column]);
    });
  }

  return numericColumnsData;
};
