import { CLASS_NAMESPACE } from "../../commons/ClassConfigs";
export interface SecurityFilter {
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.model.common.security.SecurityFilter;
  textSearchType: any;
  searchStrings: any;
  securitySearchType: any;
}
