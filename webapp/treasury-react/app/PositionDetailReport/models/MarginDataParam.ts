import { SecurityFilter } from "./SecurityFilter";
import { CLASS_NAMESPACE } from "../../commons/ClassConfigs";
export interface MarginDataParam {
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.margin.model.MarginDataParam;
  startDate?: string;
  endDate?: string;
  pageNumber?: number;
  legalEntityIds?: number[];
  legalEntityFamilyIds?: number[];
  agreementTypeIds?: number[];
  cpeIds?: number[];
  bookIds?: number[];
  gboTypeIds?: number[];
  fieldIds?: number[];
  currencyIds?: number[];
  custodianAccountIds?: number[];
  pnlSpns?: number[];
  showExtendedSecurityAttrs: boolean;
  showAdjustmentAttrs: boolean;
  bundleFlag?: boolean | null;
  bundleIds?: number[];
  businessUnitIds?: number[];
  requestId?: string;
  totalPages?: number;
  showDummyPositionsForAdjustments: boolean;
  securityFilter?: SecurityFilter;
  enrichWithRCFields: boolean;
  includeUnmappedBrokerRecord: boolean;
}
