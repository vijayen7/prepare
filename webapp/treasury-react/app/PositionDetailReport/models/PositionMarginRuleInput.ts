import { CLASS_NAMESPACE } from "../../commons/ClassConfigs";

export interface PositionMarginRuleInput {
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.margin.model.PositionMarginRuleInput;
  counterPartyEntityId?: number;
  legalEntityId?: number;
  agreementTypeId?: number;
  pnlSpn?: number;
  custodianAccountId?: number;
  bookId?: number;
  strategyId?: number;
  ccySpn?: number;
  marketId?: number;
  countryId?: number;
  fieldId?: number;
  gboType?: number;
  foType?: number;
  foSubType?: number;
  underlyingGboType?: number;
  underlyingFoType?: number;
  underlyingFoSubtype?: number;
  isLong?: number;
  date?: string;
  marginTypeId?: number;
  isValid?: number;
}
