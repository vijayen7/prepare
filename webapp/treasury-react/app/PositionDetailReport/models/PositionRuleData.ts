import ReferenceData from "../../commons/models/ReferenceData";
import { CLASS_NAMESPACE } from "../../commons/ClassConfigs";

export interface PositionRuleData {
  "@CLASS": typeof CLASS_NAMESPACE.com.arcesium.treasury.margin.model.PositionRuleData;
  userComments?: string;
  userName?: string;
  usageType?: ReferenceData;
  marginTypeId: number;
  ruleType?: ReferenceData;
  usageCurrency?: ReferenceData;
  usageHaircut?: number;
  calculator?: ReferenceData;
  allocator?: ReferenceData;
  positionAllocator?: ReferenceData;
  ruleId?: number;
  effectiveStartDate?: Date;
  effectiveEndDate?: Date;
}
