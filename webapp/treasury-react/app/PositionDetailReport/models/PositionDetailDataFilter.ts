import { SecurityFilter } from "./SecurityFilter";

export interface PositionDetailDataFilter {
  "@CLASS": string;
  legalEntityIds?: number[];
  date?: string;
  startDate?: string;
  endDate?: string;
  pnlSpns?: number[];
  legalEntityFamilyIds?: number[];
  cpeIds?: number[];
  agreementTypeIds?: number[];
  custodianAccountIds?: number[];
  currencyIds?: number[];
  bookIds?: number[];
  gboTypeIds?: number[];
  fieldTypeIds?: number[];
  liveOnly?: boolean;
  enrichWithPrevDayLcmPositionData?: boolean;
  getOnlyLatestData?: boolean;
  includeUnmappedBrokerRecord?: boolean;
  excessDeficitDataType?: boolean;
  showSegDrilldown?: boolean;
  fromExcessDeficitDataSource?: boolean;
  toExcessDeficitDataSource?: boolean;
  cacheIds?: string;
  enrichWithExtendedSecurityAttributes?: boolean;
  securityFilter?: SecurityFilter;
  dataSet?: number;
  dataSetId?: number;
  fetchBundlePositionsOnly?: boolean;
  bundleIds?: number[];
  businessUnitIds?: number[];
  enrichWithRCFields?: boolean;
  requiredFields?: string[];
  isNoDataFoundFromCache?: boolean;
  requestId?: string;
  pageId?: number;
}
