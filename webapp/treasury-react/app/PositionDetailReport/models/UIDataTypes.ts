import MapData from "../../commons/models/MapData";
import * as H from "history";

export interface SearchFilter {
  [key: string]: any;
  selectedFromDate: string;
  selectedToDate: string;
  selectedLegalEntities: MapData[];
  selectedLegalEntityFamilies: MapData[];
  selectedCpes: MapData[];
  selectedAgreementTypes: MapData[];
  selectedBooks: MapData[];
  selectedGboTypes: MapData[];
  selectedFieldTypes: MapData[];
  selectedCustodianAccounts: MapData[];
  selectedCurrencies: MapData[];
  selectedReportingCurrency: MapData;
  selectedBusinessUnits: MapData[];
  selectedBundles: MapData[];
  selectedDataset: MapData | null;
  selectedSavedGrid: MapData | null;
  liveOnly?: boolean | null;
  isCrimsonEnabled?: boolean | null;
  isPositionLevel: boolean;
  isPublished: boolean;
  includeUnmappedBrokerRecord: boolean;
  includeExtendedSecurityAttributes: boolean;
  excessDeficitDataType?: boolean | null;
  showSegDrilldown?: boolean | null;
  fromExcessDeficitDataSource?: boolean | null;
  toExcessDeficitDataSource?: boolean | null;
  securityFilter?: SecuritySearchFilter | null;
}

export interface PositionReportProps {
  location: H.Location;
}

export interface SecuritySearchFilter {
  selectedSpns: string;
  selectedSecuritySearchType: { key: string; value: string };
  selectedTextSearchType: { key: string; value: string };
  securitySearchString: string;
  isAdvancedSearch: boolean;
}

export type LcmPositionDataKeyValue = { [key: string]: string | number };
export interface extendedSecurityMap {
  selectedSpns: string;
  selectedSecuritySearchType: { key: string; value: string };
  selectedTextSearchType: { key: string; value: string };
  securitySearchString: string;
  isAdvancedSearch: boolean;
}

export interface LatestMarginDetailsGridData {
  agreementType: string;
  agreementTypeId: number;
  agreementId: number;
  mnaAgreementId?: number;
  bundle: string;
  businessUnit: string;
  reportingCurrency?: string;
  book: string;
  bookId: number;
  custodianAccount: string;
  exposureCustodianAccount: string;
  custodianAccountId: number;
  custodianAccountCounterParty: string;
  exposureCounterParty: string;
  exposureCounterPartyId: number;
  fieldType?: string;
  fieldTypeId?: number;
  fxRate?: number;
  legalEntity: string;
  legalEntityId: number;
  legalEntityFamily: string;
  marketValue?: number;
  marketValueUsd?: number;
  marketValueRC?: number;
  pnlSpn: string | number;
  priceUsd?: number;
  priceRC?: number;
  priceTick: number;
  quantity?: number;
  spn: number;
  date: string;
  market?: string;
  marketId?: number;

  absSubtype?: string;
  absType?: string;
  bicsIndustryGroup?: string;
  bicsIndustrySector?: string;
  bicsIndustrySubgroup?: string;
  bondSpn?: number;
  strikePrice?: number;
  clearingMethod?: string;
  countryOfRisk?: string;
  forwardQuantity1?: number;
  forwardQuantity2?: number;
  forwardSpn1?: number;
  forwardSpn2?: number;
  gicsIndustry?: string;
  gicsIndustryGroup?: string;
  gicsSector?: string;
  isNdf?: any;
  issueSize?: number;
  issuerSpn?: number;
  offDate?: string;
  onDate?: string;
  outstandingAmt?: number;
  propeqIgroup?: string;
  putCallInd?: number | string;
  ratingMoody?: string;
  ratingSandP?: string;
  referenceSpn?: number;
  spotSpn?: number;
  ticker?: string;
  tickUnit?: number | string;

  marginPrice?: number;
  marginQuantity?: number;
  marginMarketValue?: number;
  marginMarketValueUsd?: number;
  marginMarketValueRC?: number;
  regAdjustedMarginUsd?: number;
  regAdjustedUsageUsd?: number;
  regMarginAdjustmentUsd?: number;
  regUsageAdjustmentUsd?: number;
  regAdjustedMarginRC?: number;
  regAdjustedUsageRC?: number;
  regMarginAdjustmentRC?: number;
  regUsageAdjustmentRC?: number;
  regCalculatorStatus?: string;
  regCalculationComment?: string;
  regCalculator?: string;
  regClassifier?: string;
  regFinancedMarginUsd?: number;
  regFinancedMarginRC?: number;
  regFinancedQuantity?: number;
  regMarginCcySpn?: string;
  regMarginHaircut?: number;
  regMarginNative?: number;
  regMarginType?: string;
  regMarginUsd?: number;
  regMarginRC?: number;
  regUsageHaircut?: number;
  regUsageUsd?: number;
  regUsageRC?: number;
  regUsageNative?: number;
  regUnfinancedQuantity?: number;
  regUnfinancedMarginUsd?: number;
  regUnfinancedMarginRC?: number;

  regCpeCalculatorStatus?: string;
  regCpeCalculationComment?: string;
  regCpeCalculator?: string;
  regCpeClassifier?: string;
  regCpeFinancedMarginUsd?: number;
  regCpeFinancedMarginRC?: number;
  regCpeFinancedQuantity?: number;
  regCpeMarginCcySpn?: string;
  regCpeMarginHaircut?: number;
  regCpeMarginNative?: number;
  regCpeMarginType?: string;
  regCpeMarginUsd?: number;
  regCpeMarginRC?: number;
  regCpeUsageHaircut?: number;
  regCpeUsageUsd?: number;
  regCpeUsageRC?: number;
  regCpeUsageNative?: number;

  houseCalculatorStatus?: string;
  houseCalculationComment?: string;
  houseCalculator?: string;
  houseCalculatorId?: number;
  houseClassifier?: string;
  houseFinancedMarginUsd?: number;
  houseFinancedMarginRC?: number;
  financedQuantity?: number;
  houseMarginCcySpn?: string;
  houseMarginHaircut?: number;
  houseMarginNative?: number;
  houseMarginType?: string;
  houseMarginUsd?: number;
  houseMarginRC?: number;
  houseUsageHaircut?: number;
  houseUsageUsd?: number;
  houseUsageRC?: number;
  houseUsageNative?: number;
  houseAdjustedMarginUsd?: number;
  houseAdjustedUsageUsd?: number;
  houseMarginAdjustmentUsd?: number;
  houseUsageAdjustmentUsd?: number;
  houseAdjustedMarginRC?: number;
  houseAdjustedUsageRC?: number;
  houseMarginAdjustmentRC?: number;
  houseUsageAdjustmentRC?: number;
  unfinancedQuantity?: number;
  houseUnfinancedMarginUsd?: number;
  houseUnfinancedMarginRC?: number;

  countryName?: string;
  countryId?: number;
  cusip?: string;
  death?: string;
  desName?: string;
  foTypeName?: string;
  foTypeId?: number;
  gboTypeName?: string;
  gboTypeId?: number;
  isin?: string;
  local?: string;
  ric?: string;
  sedol?: string;
  sfsTypeName?: string;
  subtypeName?: string;
  bloomberg?: string;
  currencyIsoCode?: string;
  underlyingSpn?: number;
  parentSpn?: number;
  ultimateParentSpn?: number;
  gboTickUnit?: number;
  underlyingValueUsd?: number;
  underlyingValueRC?: number;
  underlyingValue?: number;
  currencyId?: number;

  underlyingCusip?: string;
  underlyingSedol?: string;
  underlyingIsin?: string;
  underlyingLocal?: string;
  underlyingBloomberg?: string;
  underlyingRic?: string;
  underlyingGboTypeId?: number;

  houseRuleUserName?: string,
  houseRuleUserComments?: string,
  houseRuleUsageType?: string,
  houseRuleUsageHaircut?: number,
  houseRuleUsageCurrency?: string,
  regRuleUserName?: string,
  regRuleUserComments?: string,
  regRuleUsageType?: string,
  regRuleUsageHaircut?: number,
  regRuleUsageCurrency?: string,
  regCpeRuleUserName?: string,
  regCpeRuleUserComments?: string,
  regCpeRuleUsageType?: string,
  regCpeRuleUsageHaircut?: number,
  regCpeRuleUsageCurrency?: string,
}
