import PositionDetailDataStore from "./PositionDetailDataStore";
import { createContext, useContext } from "react";
import rootStore from "../MarginRules/useStores";

const storesContext = createContext({
  positionDetailDataStore: new PositionDetailDataStore(rootStore),
});

export const useStore = () => useContext(storesContext);
