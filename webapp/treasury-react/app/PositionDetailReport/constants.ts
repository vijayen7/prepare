import MapData from "../commons/models/MapData";
import { SearchFilter, LcmPositionDataKeyValue } from "./models/UIDataTypes";
import Status from "../commons/models/Status";
import { getPreviousWorkday } from "./utils/CommonUtil";
import { PositionRuleData } from "./models/PositionRuleData";
import { CLASS_NAMESPACE } from "../commons/ClassConfigs";
import { TradeIndependentAmountFilter } from
"../MarginScreens/TradeIndependentAmount/models/TradeIndependentAmountFilter";

export const DATE_20380101: string = "2038-01-01";
export const DATE_19000101: string = "1900-01-01";

export const MAX_API_CALLS_FOR_LCM_POSITION_DATA = 3;

export const MAX_API_CALLS_FOR_MARGIN_LATEST_DATA = 5;

export const DEFAULT_PINNED_COLUMNS = { actions: true, pnlSpn: true };

export const MAX_DATE_RANGE_IN_DAYS = 7;

export const FULFILLED_STATUS = "fulfilled";

export const TOO_MANY_REQUESTS_STATUS_CODE = 429;

export const SUCCESS_STATUS_CODE = 200;

export const POSITION_RULE_ID = 3;

export const RETRY_PDR_EXCEL_COUNT = 3;

export const EMPTY_STRING = "";

export const UPDATE_MARGIN_RULE = "UPDATE_MARGIN_RULE";

export const ADD_MARGIN_RULE = "ADD_MARGIN_RULE";

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const ISDA_AGREEMENT_TYPE_ID = 6;
export const MNA_AGREEMENT_TYPE_ID = 20;

export const PREV_DAY_PREFIX = "prevDayLcmPositionData.";

export const CURR_DAY_PREFIX = "lcmPositionData.";

export const DATASETS: MapData[] = [
  { key: 1, value: "Margin Details" },
  { key: 2, value: "Margin & Exposure Details" },
  { key: 3, value: "Recon Margin & Exposure Details" },
];

export const BUNDLE_DATASETS: MapData[] = [
  { key: 1, value: "Margin Details" },
  { key: 2, value: "Margin & Exposure Details" },
];

export enum DATASET_TYPE {
  Margin_Details = 1,
  Margin_Exposure_Details = 2,
  Recon_Margin_Exposure_Details = 3,
}

export enum MARGIN_TYPES {
  House = 1,
  Exchange = 3,
  Regulatory = 4,
  Segregated_IA = 5,
  Regulatory_CPE = 6,
}

export const FIELD_TYPES: MapData[] = [
  { key: 1, value: "Accurued Interest" },
  { key: 14, value: "Real" },
  { key: 17, value: "Swap Finance Charge" },
  { key: 22, value: "Bad Debt" },
];

export const ALL_VALUES: MapData[] = [
  { key: -1, value: "All" },
];

export const MESSAGES = {
  INITIAL_SEARCH_MESSAGE: "Perform search to view Position Detail Report",
  NO_DATA_FOUND_MESSAGE:
    "No records found for search criteria",
  ERROR_MESSAGE: "Error Occurred while fetching Position Detail Report",
  DATA_FOUND_MESSAGE: "Data found",
  FILL_REQUIRED_INPUT_MESSAGE: "Please fill all the required inputs(*)",
  INPUT_DATE_CHECK_MESSAGE: "To date can not be before From date",
  INPUT_DATE_RANGE_CHECK_MESSAGE:
    "Date range cannot be greater than " +
    MAX_DATE_RANGE_IN_DAYS +
    " week days.",
  TRADE_IA_ERROR_MESSAGE:
    "Error Occurred while fetching Position Detail Report",
  INVALID_AGREEMENT_MESSAGE: "Not a valid agreement search.",
  INVALID_DATE_RANGE_CHECK_MESSAGE:
    "Date Range supports search for one valid agreement or pnlSpns",
  AGREEMENT_COMBINATION_MESSAGE:
    "Either give PnlSpns or Agreement definition i.e " +
    "Legal Entity, CounterParty and Agreement Type for the date range search.",
  NO_TRADE_IA_DATA_FOUND_MESSAGE:
    "No Trade IA Data is available for the performed search",
  COPY_SEARCH_URL: "Search URL copied to clipboard.",
  NO_POSITION_RULE_EXISTS_MESSAGE:
    "Position Rule doesn't exist for selected position and margin type",
  RETRY_CACHE_MESSAGE: "Data is not available in cache for the searched filter. Please try to refresh cache ",
  DOWNLOAD_REPORT_ERROR_MESSAGE: "Error occurred while downloading the Posiiton Detail Report.",
  REPORT_DOWNLOADED_SUCCESS_MESSAGE: "Position Detail Report has been downloaded successfully.",
  DOWNLOAD_REPORT_IN_PROGRESS: "Position Detail Report will be downloaded shortly."
};

export const STATUS = {
  INITIAL_STATUS: "",
  SUCCESS: "SUCCESS",
  FAILURE: "FAILURE",
};

export const DEFAULT_SEARCH_FILTERS: SearchFilter = {
  selectedFromDate: getPreviousWorkday().format("YYYY-MM-DD"),
  selectedToDate: getPreviousWorkday().format("YYYY-MM-DD"),
  selectedLegalEntities: [],
  selectedLegalEntityFamilies: [],
  selectedCpes: [],
  selectedAgreementTypes: [],
  selectedBooks: [],
  selectedGboTypes: [],
  selectedDataset: {key: 2, value: "Margin & Exposure Details"},
  selectedSavedGrid: null,
  selectedFieldTypes: [],
  selectedCustodianAccounts: [],
  selectedCurrencies: [],
  selectedReportingCurrency: {key: 1, value: "RC"},
  selectedBusinessUnits: [],
  selectedBundles: [],
  liveOnly: null,
  isPositionLevel: true,
  isPublished: true,
  isCrimsonEnabled: null,
  includeUnmappedBrokerRecord: false,
  includeExtendedSecurityAttributes: false,
  excessDeficitDataType: null,
  showSegDrilldown: null,
  fromExcessDeficitDataSource: null,
  toExcessDeficitDataSource: null,
  securityFilter: {
    selectedSpns: "",
    selectedSecuritySearchType: { key: "TICKER", value: "TICKER" },
    selectedTextSearchType: { key: "EXACT_MATCH", value: "EXACT_MATCH" },
    securitySearchString: "",
    isAdvancedSearch: false,
  }
};

export const DATA_STATUS: Status = {
  status: STATUS.INITIAL_STATUS,
  message: MESSAGES.INITIAL_SEARCH_MESSAGE,
};

export const MAX_RECORDS_TO_DISPLAY_IN_GRID = 50000;
export const MAX_PREV_DAY_RECORDS_TO_DISPLAY_IN_GRID = 30000;
export const INITIAL_POSITION_DATA: LcmPositionDataKeyValue[] = [];

export const INITIAL_TRADE_IA_DISPLAY_COLUMNS: string[] = [
  "tradeDate",
  "tradeId",
  "pnlSpn",
  "description",
  "legalEntity",
  "counterPartyEntity",
  "executingBroker",
  "primeBroker",
  "book",
  "bundle",
  "gboType",
  "currency",
  "price",
  "quantity",
  "independentAmountType",
  "independentAmountValue",
  "independentAmountCurrency",
  "isIndependentAmountNegative",
  "independentAmountEffectiveDate",
  "comment",
  "independentAmountSource",
  "userName",
  "independentAmountUnderlying",
  "independentAmount",
  "trader",
  "custodianAccount",
  "independentAmountStatus",
];

export const INITIAL_POSITION_RULE_DATA: PositionRuleData = {
  marginTypeId: 1,
  "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.margin.model.PositionRuleData,
};

export const INITIAL_TRADE_IA_FILTER: TradeIndependentAmountFilter = {
  "@CLASS":
    CLASS_NAMESPACE.com.arcesium.treasury.margin.model
      .TradeIndependentAmountFilter,
  startDate: "",
  endDate: "",
  legalEntityIds: [],
  counterPartyIds: [],
  bookIds: [],
  custodianAccountsIds: [],
  currencyIds: [],
  gboTypeIds: [],
  independentAmountStatus: undefined,
  pnlSpns: [],
  tradeIds: [],
};

export const DEFAULT_USAGE_TYPE = {
  key: 1,
  value: "PERCENT_MARKET_VALUE",
};

export const REPORTING_CURRENCY_FILTER_DATA: MapData[] = [
  { key: 0, value: "USD" },
  { key: 1, value: "RC" },
];

export const USD_SUFFIX = "Usd";
export const RC_SUFFIX = "RC";

export const POSITION_RULE_TYPES_REF_DATA: MapData = {
  key: 2,
  value: "Position"
};
