import {
  fetchPostURL,
  getReportingUrl,
  getSaveSettingsUrl,
  fetchURL,
} from "../commons/util";
import { BASE_URL } from "../commons/constants";
import SERVICE_URLS from "../commons/UrlConfigs";
import { PositionDetailDataFilter } from "./models/PositionDetailDataFilter";
import { MarginDataParam } from "./models/MarginDataParam";
import { PositionMarginRuleInput } from "./models/PositionMarginRuleInput";
import { getCommaSeparatedListValue } from "../commons/util";
import { SearchFilter } from "./models/UIDataTypes";
import { getSelectedIdList } from "./utils/PositionDetailUtil";
import { LcmPositionDataKeyValue } from "./models/UIDataTypes";

export const getMarginDataList = (marginDataParam: MarginDataParam) => {
  let positionMarginDataFilter = encodeURIComponent(
    JSON.stringify(marginDataParam)
  );
  let filterUrl = `${BASE_URL}service/${SERVICE_URLS.marginPositionDataService.getPositionMarginData}?inputFormat=JSON&format=JSON`;

  return fetchPostURL(
    filterUrl,
    `positionMarginDataFilter=${positionMarginDataFilter}`
  );
};

export const getPdrFilters = (
  positionDetailDataFilter: PositionDetailDataFilter,
  enrichWithPrevDayLcmPositionData: boolean,
  useOptimizedPdr: boolean
) => {
  let lcmPositionDataParam = encodeURIComponent(
    JSON.stringify(positionDetailDataFilter)
  );
  let filterUrl: string;

  if (!enrichWithPrevDayLcmPositionData && !useOptimizedPdr) {
    filterUrl = `${BASE_URL}service/${SERVICE_URLS.positionDetailReportService.getPdrFilters}?inputFormat=JSON&format=JSON`;
  } else if(!enrichWithPrevDayLcmPositionData && useOptimizedPdr) {
    filterUrl = `${BASE_URL}service/${SERVICE_URLS.positionDetailReportDataReadManagerService.getPdrFilters}?inputFormat=JSON&format=JSON`;
  } else if(enrichWithPrevDayLcmPositionData && useOptimizedPdr) {
    filterUrl = `${BASE_URL}service/${SERVICE_URLS.positionDetailReportDataReadManagerService.getLcmPositionDataFiltersForDayOnDayDiff}?inputFormat=JSON&format=JSON`;
  } else {
    filterUrl = `${BASE_URL}service/${SERVICE_URLS.positionDetailReportService.getLcmPositionDataFiltersForDayOnDayDiff}?inputFormat=JSON&format=JSON`;
  }

  return fetchPostURL(
    filterUrl,
    `lcmPositionDataParam=${lcmPositionDataParam}`
  );
};

export const getMarginDataFilters = (marginDataParam: MarginDataParam) => {
  let lcmMarginDataFilter = encodeURIComponent(JSON.stringify(marginDataParam));
  let filterUrl = `${BASE_URL}service/${SERVICE_URLS.marginPositionDataService.getMarginPageFilters}?inputFormat=JSON&format=JSON`;

  return fetchPostURL(filterUrl, `lcmMarginDataFilter=${lcmMarginDataFilter}`);
};

export const getPositionRuleDetails = (ruleMap: PositionMarginRuleInput) => {
  let ruleMapInput = encodeURIComponent(JSON.stringify(ruleMap));
  let url = `${BASE_URL}service/${SERVICE_URLS.marginRuleSystemService.getMarginRuleForPosition}?inputFormat=JSON&format=JSON`;

  return fetchPostURL(url, `ruleMap=${ruleMapInput}`);
};

export const getPositionDetailDataReportUrl = (
  filter: SearchFilter
) => {

  let url = getReportingUrl(
    SERVICE_URLS.positionDetailReportDataReadManagerService.getPdrDataView
  );

  url += `?filter.startDate=${filter.selectedFromDate}`
  url += `&filter.endDate=${filter.selectedToDate}`
  url += `&filter.legalEntityIds=${filter.selectedLegalEntityFamilies?.length
    ? getSelectedIdList(filter.selectedLegalEntityFamilies) : -1}`;
  url += `&filter.legalEntityFamilyIds=${filter.selectedLegalEntityFamilies?.length
    ? getSelectedIdList(filter.selectedLegalEntityFamilies) : -1}`;
  url += `&filter.cpeIds=${filter.selectedCpes?.length ? getSelectedIdList(filter.selectedCpes) : -1}`;
  url += `&filter.agreementTypeIds=${filter.selectedAgreementTypes?.length
    ? getSelectedIdList(filter.selectedAgreementTypes) : -1}`;
  url += `&filter.bookIds=${filter.selectedBooks?.length ? getSelectedIdList(filter.selectedBooks) : -1}`;
  url += `&filter.currencyIds=${filter.selectedCurrencies?.length
    ? getSelectedIdList(filter.selectedCurrencies) : -1}`;
  url += `&filter.bundleIds=${filter.selectedBundles?.length ? getSelectedIdList(filter.selectedBundles) : -1}`;
  url += `&filter.businessUnitIds=${filter.selectedBusinessUnits?.length
    ? getSelectedIdList(filter.selectedBusinessUnits) : -1}`;
  url += `&filter.custodianAccountIds=${filter.selectedCustodianAccounts?.length
    ? getSelectedIdList(filter.selectedCustodianAccounts) : -1}`;
  url += `&filter.fieldTypeIds=${filter.selectedFieldTypes?.length
    ? getSelectedIdList(filter.selectedFieldTypes) : -1}`;
  url += `&filter.gboTypeIds=${filter.selectedGboTypes?.length ? getSelectedIdList(filter.selectedGboTypes) : -1}`;
  url += `&filter.pnlSpns=${filter.securityFilter?.selectedSpns &&
    filter.securityFilter.selectedSpns.trim() !== ""
      ? getCommaSeparatedListValue(filter.securityFilter.selectedSpns).split(
          ","
        )
      : -1}`;
  url += `&filter.dataSetId=${filter.selectedDataset?.key}`;
  url += `&filter.enrichWithExtendedSecurityAttributes=${filter.includeExtendedSecurityAttributes}`;
  url += `&filter.fetchBundlePositionsOnly=${!filter.isPositionLevel}`;
  url += `&filter.getOnlyLatestData=${!filter.isPublished}`;
  url += `&filter.includeUnmappedBrokerRecord=${filter.includeUnmappedBrokerRecord}`;
  url += `&inputFormat=properties&format=json`
  return url;
};

export const getLatestMarginReportUrl = (marginDataParam: SearchFilter) => {
  let url = getReportingUrl(
    SERVICE_URLS.marginPositionDataService.getLatestMarginDataView
  );
  url += `?filter.startDate=${marginDataParam.selectedFromDate}`
  url += `&filter.endDate=${marginDataParam.selectedToDate}`
  url += `&filter.legalEntityIds=${marginDataParam.selectedLegalEntityFamilies?.length ?
    getSelectedIdList(marginDataParam.selectedLegalEntityFamilies) : -1}`;
  url += `&filter.legalEntityFamilyIds=${marginDataParam.selectedLegalEntityFamilies?.length ?
      getSelectedIdList(marginDataParam.selectedLegalEntityFamilies) : -1}`;
  url += `&filter.cpeIds=${marginDataParam.selectedCpes?.length
    ? getSelectedIdList(marginDataParam.selectedCpes) : -1}`;
  url += `&filter.agreementTypeIds=${marginDataParam.selectedAgreementTypes?.length ?
    getSelectedIdList(marginDataParam.selectedAgreementTypes) : -1}`;
  url += `&filter.bookIds=${marginDataParam.selectedBooks?.length ?
    getSelectedIdList(marginDataParam.selectedBooks) : -1}`;
  url += `&filter.currencyIds=${marginDataParam.selectedCurrencies?.length ?
    getSelectedIdList(marginDataParam.selectedCurrencies) : -1}`;
  url += `&filter.bundleIds=${marginDataParam.selectedBundles?.length ?
    getSelectedIdList(marginDataParam.selectedBundles) : -1}`;
  url += `&filter.businessUnitIds=${marginDataParam.selectedBusinessUnits?.length ?
    getSelectedIdList(marginDataParam.selectedBusinessUnits) : -1}`;
  url += `&filter.custodianAccountIds=${marginDataParam.selectedCustodianAccounts?.length ?
    getSelectedIdList(marginDataParam.selectedCustodianAccounts) : -1}`;
  url += `&filter.fieldIds=${marginDataParam.selectedFieldTypes?.length ?
    getSelectedIdList(marginDataParam.selectedFieldTypes) : -1}`;
  url += `&filter.gboTypeIds=${marginDataParam.selectedGboTypes?.length ?
    getSelectedIdList(marginDataParam.selectedGboTypes) : -1}`;
  url += `&filter.pnlSpns=${marginDataParam.securityFilter?.selectedSpns &&
    marginDataParam.securityFilter.selectedSpns.trim() !== ""
      ? getCommaSeparatedListValue(marginDataParam.securityFilter.selectedSpns).split(
          ","
        )
      : -1}`;
  url += `&filter.showExtendedSecurityAttrs=${getSelectedIdList(marginDataParam.includeExtendedSecurityAttributes)}`;
  url += `&filter.bundleFlag=${!marginDataParam.isPositionLevel}`;
  url += `&inputFormat=properties&format=json`
  return url;
};

export const getSavedGrids = (applicationCategory: string) => {
  const url = `${getSaveSettingsUrl()}/getSavedSettings?applicationId=3&applicationCategory=${applicationCategory}&format=json`;
  return fetchURL(url);
};

export const isUseOptimizedPdr = () => {
  const url = `${BASE_URL}service/lcmFeatureToggle/isUseOptimizedPdr?format=json`;
  return fetchURL(url);
};

export const checkAndPopulateCache = (
  positionDetailDataFilter: PositionDetailDataFilter
) => {
  let lcmPositionDataParam = encodeURIComponent(
    JSON.stringify(positionDetailDataFilter)
  );
  let filterUrl = `${BASE_URL}service/${SERVICE_URLS.positionDetailReportDataReadManagerService.checkAndPopulateCache}?inputFormat=JSON&format=JSON`;

  return fetchPostURL(
    filterUrl,
    `lcmPositionDataParam=${lcmPositionDataParam}`
  );
};

export const getUploadedFileUrl = (
  pdrFilters: any
) => {
  let param = encodeURIComponent(
    JSON.stringify(pdrFilters)
  );
  let filterUrl = `${BASE_URL}service/${SERVICE_URLS.positionDetailReportDataReadManagerService.getObjectStoreUrlForPdrPositionData}?inputFormat=JSON&format=JSON`;

  return fetchPostURL(
    filterUrl,
    `lcmPositionDataParams=${param}`
  );
};

export const downloadPdrExcel = (
  objectUri: string,
  date: string
) => {
  let fileName = `pdrDataReport_${date}.xlsx`;
  let url = BASE_URL + "margin/downloadSimulationReport?fileName=" + fileName + "&contentType=&fileUrl="+objectUri;
  window.open(url,"_blank");
}

export const isEnabledDownloadPdrReport = () => {
  const url = `${BASE_URL}service/lcmFeatureToggle/isEnabledDownloadPdrReport?format=json`;
  return fetchURL(url);
};
