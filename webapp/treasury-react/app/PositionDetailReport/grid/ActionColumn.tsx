import React from "react";
import { ISDA_AGREEMENT_TYPE_ID, MNA_AGREEMENT_TYPE_ID } from "../constants";
import { useStore } from "../useStore";

const ActionColumn: React.FC<any> = ({ context }) => {
  const { positionDetailDataStore } = useStore();

  const renderActionColumn = () => {
    return (
      <>
        {context.row.agreementTypeId === ISDA_AGREEMENT_TYPE_ID &&
          context.row.pnlSpn !== null && context.row.pnlSpn !== -1 && (
            <i
              title="View Trade IA Details"
              className="icon-view"
              onClick={(e) => {
                e.stopPropagation();
                positionDetailDataStore.setToggleTradeIAGrid(false);
                positionDetailDataStore.getTradeIndependentAmountDetails(
                  context.row
                );
              }}
            ></i>
          )}
        {context.row.agreementTypeId !== ISDA_AGREEMENT_TYPE_ID &&
          context.row.pnlSpn !== null && context.row.pnlSpn !== -1 && (
            <i
              title="Trade IA Details Not Available for Non ISDA Agreement"
              className="icon-view"
              onClick={(e) => {
                e.stopPropagation();
              }}
            ></i>
          )}
        {(context.row.pnlSpn === null || context.row.pnlSpn === -1) && (
          <i
            title="Trade IA Details Not Available for Dummy Position"
            className="icon-view"
            onClick={(e) => {
              e.stopPropagation();
            }}
          ></i>
        )}
        <i
          title="Intermediate Screen"
          className="icon-link"
          onClick={(e) => {
            e.stopPropagation();
            positionDetailDataStore.goToIntermediateScreen(
              context.row.legalEntityId,
              context.row.exposureCounterPartyId,
              (context.row.mnaAgreementId !== null && context.row.mnaAgreementId !== undefined)
               ? MNA_AGREEMENT_TYPE_ID : context.row.agreementTypeId,
              context.row.date
            );
          }}
        ></i>
        {context.row.pnlSpn !== null && context.row.pnlSpn !== -1 &&
          context.row.pnlSpn !== undefined &&
          positionDetailDataStore.filters.isPositionLevel && (
            <i
              title="Add Margin Rule"
              className="icon-add"
              onClick={(e) => {
                e.stopPropagation();
                positionDetailDataStore.setClickedRow(context.rowId);
                positionDetailDataStore.getRuleDataForAllMarginTypes(
                  context.row
                );
              }}
            ></i>
          )}
        {(context.row.pnlSpn == null || context.row.pnlSpn === -1) &&
          positionDetailDataStore.filters.isPositionLevel && (
            <i title="Rule not editable" className="icon-add"></i>
          )}
      </>
    );
  };

  return <>{renderActionColumn()}</>;
};

export default ActionColumn;
