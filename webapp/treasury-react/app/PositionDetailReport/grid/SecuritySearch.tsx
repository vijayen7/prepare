import React from "react";
import { NOT_AVAILABLE, SECURITY_SEARCH_URL } from "../../commons/constants";

const SecuritySearch: React.FC<any> = ({ context }) => {
  const goToSecuritySearch = (pnlSpn: string) => {
    let url = `${SECURITY_SEARCH_URL}` + pnlSpn + `%3B&groupingId=0` + pnlSpn;
    window.open(url, "_blank");
  };

  const render = () => {
    if (
      context.row.pnlSpn &&
      context.row.pnlSpn !== null &&
      context.row.pnlSpn !== NOT_AVAILABLE &&
      context.row.pnlSpn !== undefined &&
      context.row.pnlSpn !== -1
    ) {
      return (
        <>
          <a
            title="Security Search"
            onClick={(e) => {
              e.stopPropagation();
              goToSecuritySearch(context.row.pnlSpn);
            }}
          >
            {context.row.pnlSpn}
          </a>
        </>
      );
    } else {
      return (context.row.pnlSpn && context.row.pnlSpn !== -1) ? context.row.pnlSpn : NOT_AVAILABLE;
    }
  };

  return <>{render()}</>;
};

export default SecuritySearch;
