import ArcDataGrid from "arc-data-grid";

export const getGridOptions: ArcDataGrid.GridConfiguration = {
  clickableRows: true,
  showColumnAggregationRow: true,
  getExportFileName: ({ fileType, all }) => {
    return `${all ? "Position-Data" : "Position-Data-Current-View"}`;
  },
};
