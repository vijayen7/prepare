import ArcDataGrid from "arc-data-grid";

export const getGridOptions: ArcDataGrid.GridConfiguration = {
  clickableRows: true,
  showColumnAggregationRow: true,
  getExportFileName: ({ fileType, all }) => {
    return `${all ? "Trade-IA-Data" : "Trade-IA-Current-View"}`;
  },
};
