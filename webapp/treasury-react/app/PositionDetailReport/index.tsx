import { observer } from "mobx-react-lite";
import React from "react";
import PositionDetailApp from "./PositionDetailApp";
import { RouteComponentProps } from "react-router-dom";

type TParams = { id: string };

const PositionDetailReport: React.FC<RouteComponentProps<TParams>> = (
  props: RouteComponentProps<TParams>
) => {
  return (
    <>
      <PositionDetailApp location={props.location}></PositionDetailApp>
    </>
  );
};

export default observer(PositionDetailReport);
