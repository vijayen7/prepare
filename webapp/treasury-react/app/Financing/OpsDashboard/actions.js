import {
  FETCH_INTEREST_OPS_DATA,
  FETCH_PORTFOLIO_GENERATOR_OPS_DATA,
  FETCH_SECLEND_OPS_DATA,
  FETCH_COCOA_OPS_DATA,
  FETCH_COCOA_AUDIT_DATA,
  DESTROY_FINANCING_OPS_DATA,
  DESTROY_PORTFOLIO_GENERATOR_OPS_DATA,
  DESTROY_COCOA_AUDIT_DATA,
  FETCH_COCOA_BROKER_DATA,
  DESTROY_COCOA_BROKER_DATA,
  FETCH_COCOA_DROPPED_RECORDS,
  DESTROY_COCOA_DROPPED_RECORDS,
  FETCH_VALIDATE_PARSED_FILE,
  FETCH_IS_USER_AUTHORIZED_FOR_TREASURY_EDIT
} from "./constants";

export function fetchInterestOpsData(payload) {
  return {
    type: FETCH_INTEREST_OPS_DATA,
    payload
  };
}

export function fetchPortfolioGeneratorOpsData(payload) {
  return {
    type: FETCH_PORTFOLIO_GENERATOR_OPS_DATA,
    payload
  };
}

export function fetchSeclendOpsData(payload) {
  return {
    type: FETCH_SECLEND_OPS_DATA,
    payload
  };
}

export function fetchCocoaOpsData(payload) {
  return {
    type: FETCH_COCOA_OPS_DATA,
    payload
  };
}

export function fetchCocoaAuditData(payload) {
  return {
    type: FETCH_COCOA_AUDIT_DATA,
    payload
  };
}

export function fetchCocoaBrokerData(payload) {
  return {
    type: FETCH_COCOA_BROKER_DATA,
    payload
  };
}

export function fetchCocoaDroppedRecords(payload) {
  return {
    type: FETCH_COCOA_DROPPED_RECORDS,
    payload
  };
}

export function isUserAuthorizedForTreasuryEdit() {
  return {
    type: FETCH_IS_USER_AUTHORIZED_FOR_TREASURY_EDIT
  };
}
export function fetchValidateParsedFile(payload) {
  return {
    type: FETCH_VALIDATE_PARSED_FILE,
    payload
  };
}

export function destroyFinancingOpsData() {
  return {
    type: DESTROY_FINANCING_OPS_DATA
  };
}

export function destroyPortfolioGeneratorOpsData() {
  return {
    type: DESTROY_PORTFOLIO_GENERATOR_OPS_DATA
  };
}

export function destroyCocoaAuditData() {
  return {
    type: DESTROY_COCOA_AUDIT_DATA
  };
}

export function destroyCocoaBrokerData() {
  return {
    type: DESTROY_COCOA_BROKER_DATA
  };
}

export function destroyCocoaDroppedRecords() {
  return {
    type: DESTROY_COCOA_DROPPED_RECORDS
  };
}
