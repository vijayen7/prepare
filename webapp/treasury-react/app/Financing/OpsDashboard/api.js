import { BASE_URL } from 'commons/constants';
import { fetchURL } from 'commons/util';
const queryString = require('query-string');

export function getInterestOpsData(payload) {
  let url = `${BASE_URL}financing/get-interest-jobs?dateString=${payload.date}&numberOfJobs=${
    payload.numberOfJobs
    }`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getPortfolioGeneratorOpsData(payload) {
  let url = `${BASE_URL}service/financingRunService/getFinancingPortfolioGeneratorRuns?date=${payload.date}&numberOfJobs=${
    payload.numberOfJobs}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getSeclendOpsData(payload) {
  let url = `${BASE_URL}seclend/load-seclend-ops-results?dateString=${payload.date}&isLatest=${
    payload.onlyLatest
    }&seclendDataTypeIds=${payload.seclendDataTypes}`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getCocoaOpsData(payload) {
  let url;
  if (payload.useRunDate == true) {
    url = `${BASE_URL}service/seclendOpsService/getCocoaSyncsForRunDate?date=${payload.date}&format=JSON`;
  } else {
    url = `${BASE_URL}service/seclendOpsService/getCocoaSyncsForArchivalDate?date=${payload.date}&format=JSON`;
  }
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getCocoaAuditData(payload) {
  let url = `${BASE_URL}service/seclendOpsService/getBrokerData?cocoaNotificationId=${payload.cocoaNotificationId}&onlyUnMapped=true&format=JSON`;
  return fetchURL(url);
}

export function downloadCocoaFile(payload) {
  let url = `${BASE_URL}service/cocoaFileService/getFileByCocoaNotificationId?cocoaNotificationId=${payload.cocoaNotificationId}&format=JSON`;
  window.open(url);
}

export function getCocoaBrokerData(payload) {
  let url = `${BASE_URL}service/seclendOpsService/getBrokerData?cocoaNotificationId=${payload.cocoaNotificationId}&onlyUnMapped=false&format=JSON`;
  return fetchURL(url);
}

export function getCocoaDroppedRecords(payload) {
  let url = `${BASE_URL}service/seclendOpsService/getDroppedRecords?cocoaNotificationStatusId=${payload.cocoaNotificationId}&format=JSON`;
  return fetchURL(url);
}

export function getValidateParsedFile(payload){
  let url = `${BASE_URL}service/seclendOpsService/validatePfAndRunSyncsByPfId?date=${payload.date}&feedId=${payload.feedId}&pfId=${payload.pfId}&dataType=${payload.dataType}&parserName=${payload.parserName}&format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getIsUserAuthorizedForTreasuryEdit() {
  let url = `${BASE_URL}service/seclendOpsService/isUserAuthorizedForTreasuryEdit?format=JSON`;
  return fetchURL(url);
}
