import { commonAuditColumns } from './commonAuditConfig';
import { convertJavaDate } from 'commons/util';

function borrowConfig() {
  var columns = [
    {
      id: 'brokerQuantity',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Quantity',
      field: 'brokerQuantity',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerPrice',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Price',
      field: 'brokerPrice',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerTickUnit',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Tick Unit',
      field: 'brokerTickUnit',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerMarketValue',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Market Value',
      field: 'brokerMarketValue',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerDaysOpen',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'Broker Days Open',
      field: 'brokerDaysOpen',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerFxRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker FX Rate',
      field: 'brokerFxRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerBaseRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Base Rate',
      field: 'brokerBaseRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerAccrualDays',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'Broker Accrual Days',
      field: 'brokerAccrualDays',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'netDividend',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Net Dividend',
      field: 'netDividend',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'settleDate',
      type: 'text',
      name: 'Settle Date',
      field: 'settleDate',
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaDate(value);
      },
      headerCssClass: 'b',
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: 'tradeReference',
      type: 'text',
      name: 'Trade Reference',
      field: 'tradeReference',
      sortable: true,
      headerCssClass: 'b',
      excelFormatter: '#,##0'
    },
    {
      id: 'rebateRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Rebate Rate',
      field: 'rebateRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'rebateLbc',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Rebate Lbc',
      field: 'rebateLbc',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'stockLoanFeeRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Stock Loan Fee Rate',
      field: 'stockLoanFeeRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'stockLoanFeeLbc',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Stock Loan Fee Lbc',
      field: 'stockLoanFeeLbc',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'financingFeeRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Financing Fee Rate',
      field: 'financingFeeRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'financingFeeLbc',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Financing Fee Lbc',
      field: 'financingFeeLbc',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'borrowType',
      type: 'text',
      name: 'Borrow Type',
      field: 'borrowType',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'callableType',
      type: 'text',
      name: 'Callable Type',
      field: 'callableType',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  return columns;
}

export function borrowAuditColumns() {
  let columns = [
    {
      id: 'spn',
      type: 'number',
      name: 'SPN',
      field: 'spn',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'bookName',
      type: 'text',
      name: 'Book',
      field: 'bookName',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'accountName',
      type: 'text',
      name: 'Custodian Account',
      field: 'accountName',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  columns = columns.concat(commonAuditColumns());
  columns = columns.concat(borrowConfig());
  return columns;
}

function excessBorrowConfig() {
  var columns = [
    {
      id: 'shortedQuantity',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Shorted Quantity',
      field: 'shortedQuantity',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'marketValue',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Market Value',
      field: 'marketValue',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'fxRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Fx Rate',
      field: 'fxRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'rebateRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Rebate Rate',
      field: 'rebateRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'stockLoanFeeRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Stock Loan Fee Rate',
      field: 'stockLoanFeeRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'financingFeeRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Financing Fee Rate',
      field: 'financingFeeRate',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  return columns;
}

export function excessBorrowAuditColumns() {
  let columns = [
    {
      id: 'pnlSpn',
      type: 'number',
      name: 'Pnl Spn',
      field: 'pnlSpn',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'bookName',
      type: 'text',
      name: 'Book',
      field: 'bookName',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'accountName',
      type: 'text',
      name: 'Custodian Account',
      field: 'accountName',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  columns = columns.concat(commonAuditColumns());
  columns = columns.concat(excessBorrowConfig());
  return columns;
}

function swapConfig() {
  var columns = [
    {
      id: 'brokerQuantity',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Quantity',
      field: 'brokerQuantity',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerPrice',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Price',
      field: 'brokerPrice',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerTickUnit',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Tick Unit',
      field: 'brokerTickUnit',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerNotional',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Notional',
      field: 'brokerNotional',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerDaysOpen',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'Broker Days Open',
      field: 'brokerDaysOpen',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerFxRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker FX Rate',
      field: 'brokerFxRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerAccrualDays',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'Broker Accrual Days',
      field: 'brokerAccrualDays',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'brokerBaseRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Broker Base Rate',
      field: 'brokerBaseRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'netDividend',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Net Dividend',
      field: 'netDividend',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'settleDate',
      type: 'text',
      name: 'Settle Date',
      field: 'settleDate',
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaDate(value);
      },
      headerCssClass: 'b',
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: 'tradeDate',
      type: 'text',
      name: 'Trade Date',
      field: 'tradeDate',
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaDate(value);
      },
      headerCssClass: 'b',
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: 'expirationDate',
      type: 'text',
      name: 'Expiration Date',
      field: 'expirationDate',
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaDate(value);
      },
      headerCssClass: 'b',
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: 'lastResetDate',
      type: 'text',
      name: 'Last Reset Date',
      field: 'lastResetDate',
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaDate(value);
      },
      headerCssClass: 'b',
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: 'nextResetDate',
      type: 'text',
      name: 'Next Reset Date',
      field: 'nextResetDate',
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaDate(value);
      },
      headerCssClass: 'b',
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: 'tradeReference',
      type: 'text',
      name: 'Trade Reference',
      field: 'tradeReference',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'swapNumber',
      type: 'text',
      name: 'Swap Number',
      field: 'swapNumber',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'mtmPrice',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'MTM Price',
      field: 'mtmPrice',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'mtmNotional',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'MTM Notional',
      field: 'mtmNotional',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'rebateRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Rebate Rate',
      field: 'rebateRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'rebateLbc',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Rebate Lbc',
      field: 'rebateLbc',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'stockLoanFeeRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Stock Loan Fee Rate',
      field: 'stockLoanFeeRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'stockLoanFeeLbc',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Stock Loan Fee Lbc',
      field: 'stockLoanFeeLbc',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'financingFeeRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Financing Fee Rate',
      field: 'financingFeeRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'positionType',
      type: 'text',
      name: 'Position Type',
      field: 'positionType',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'callableType',
      type: 'text',
      name: 'Callable Type',
      field: 'callableType',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  return columns;
}

export function swapAuditColumns() {
  let columns = [
    {
      id: 'pnlSpn',
      type: 'number',
      name: 'Pnl Spn',
      field: 'pnlSpn',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'spn',
      type: 'number',
      name: 'SPN',
      field: 'spn',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'bookName',
      type: 'text',
      name: 'Book',
      field: 'bookName',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'accountName',
      type: 'text',
      name: 'Custodian Account',
      field: 'accountName',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  columns = columns.concat(commonAuditColumns());
  columns = columns.concat(swapConfig());
  return columns;
}

function locateConfig() {
  var columns = [
    {
      id: 'availability',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'Availability',
      field: 'availability',
      sortable: true,
      headerCssClass: 'b',
      excelFormatter: '#,##0'
    },
    {
      id: 'stockLoanFeeRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Stock Loan Fee Rate',
      field: 'stockLoanFeeRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'rebateRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Rebate Rate',
      field: 'rebateRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'classificationType',
      type: 'text',
      name: 'Classification Type',
      field: 'classificationType',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'chargeType',
      type: 'text',
      name: 'Charge Type',
      field: 'chargeType',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  return columns;
}

function availabilityConfig() {
  var columns = [
    {
      id: 'availability',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'Availability',
      field: 'availability',
      sortable: true,
      headerCssClass: 'b',
      excelFormatter: '#,##0'
    },
    {
      id: 'stockLoanFeeRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Stock Loan Fee Rate',
      field: 'stockLoanFeeRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'rebateRate',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Rebate Rate',
      field: 'rebateRate',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'classificationType',
      type: 'text',
      name: 'Classification Type',
      field: 'classificationType',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'statusType',
      type: 'text',
      name: 'Status Type',
      field: 'statusType',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'chargeType',
      type: 'text',
      name: 'Charge Type',
      field: 'chargeType',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  return columns;
}

export function availabilityAuditColumns() {
  let columns = [
    {
      id: 'spn',
      type: 'number',
      name: 'SPN',
      field: 'spn',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  columns = columns.concat(commonAuditColumns());
  columns = columns.concat(availabilityConfig());
  return columns;
}

export function locateAuditColumns() {
  let columns = [
    {
      id: 'spn',
      type: 'number',
      name: 'SPN',
      field: 'spn',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'lineNumber',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'File Line Number',
      field: 'lineNumber',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
  columns = columns.concat(locateConfig());
  return columns;
}

export function exclusivePortfolioAuditColumns() {
  return [
    {
      id: 'spn',
      type: 'number',
      name: 'Spn',
      field: 'spn',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'matchKey',
      type: 'text',
      name: 'Match Key',
      field: 'matchKey',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'matchValue',
      type: 'text',
      name: 'Match Value',
      field: 'matchValue',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'spnAudit',
      type: 'text',
      name: 'SPN Audit',
      field: 'spnAudit',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'lineNumber',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'File Line Number',
      field: 'lineNumber',
      sortable: true,
      headerCssClass: 'b',
      excelFormatter: '#,##0'
    },
    {
      id: 'quantity',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Quantity',
      field: 'quantity',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'marketValue',
      type: 'float',
      formatter: dpGrid.Formatters.Float,
      name: 'Market Value',
      field: 'marketValue',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'altFundAgreementId',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'AltFund Agreement Id',
      field: 'altFundAgreementId',
      sortable: true,
      headerCssClass: 'b'
    }
  ];
}
