export function cocoaDroppedRecordsColumns() {
  var columns = [
    {
      id: 'rawRecordId',
      type: 'number',
      name: 'Raw Record Id',
      field: 'rawRecordId',
      sortable: true,
      headerCssClass: 'b',
      width: 5
    },
    {
      id: 'errorMessage',
      type: 'text',
      name: 'Reason',
      field: 'errorMessage',
      sortable: true,
      headerCssClass: 'aln-rt b'
    }
  ];

  return columns;
}
