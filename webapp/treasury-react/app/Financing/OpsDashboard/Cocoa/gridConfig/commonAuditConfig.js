export function commonAuditColumns() {
  var columns = [
    {
      id: 'name',
      type: 'text',
      name: 'Name',
      field: 'name',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'ccyName',
      type: 'text',
      name: 'Currency',
      field: 'ccyName',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'cpeName',
      type: 'text',
      name: 'Counterparty Entity',
      field: 'cpeName',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'cusip',
      type: 'text',
      name: 'Cusip',
      field: 'cusip',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'sedol',
      type: 'text',
      name: 'Sedol',
      field: 'sedol',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'isin',
      type: 'text',
      name: 'Isin',
      field: 'isin',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'ticker',
      type: 'text',
      name: 'Ticker',
      field: 'ticker',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'matchKey',
      type: 'text',
      name: 'Match Key',
      field: 'matchKey',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'matchValue',
      type: 'text',
      name: 'Match Value',
      field: 'matchValue',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'spnAudit',
      type: 'text',
      name: 'SPN Audit',
      field: 'spnAudit',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'bookAudit',
      type: 'text',
      name: 'Book Audit',
      field: 'bookAudit',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'custodianAccountAudit',
      type: 'text',
      name: 'Custodian Account Audit',
      field: 'custodianAccountAudit',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'lineNumber',
      type: 'number',
      formatter: dpGrid.Formatters.Number,
      name: 'File Line Number',
      field: 'lineNumber',
      sortable: true,
      headerCssClass: 'b'
    }
  ];

  return columns;
}
