let auth = false
export function cocoaOpsColumns(isUserAuthorizedForTreasuryEdit) {
  auth = isUserAuthorizedForTreasuryEdit
  var columns = [
    {
      id: 'actions',
      name: 'Actions',
      sortable: false,
      width: 110,
      minWidth: 110,
      maxWidth: 110,
      formatter: _applicableColumnIdentifier,
      fixed: true
    },
    {
      id: 'syncStatusId',
      type: 'text',
      name: 'Status',
      field: 'syncStatusId',
      sortable: true,
      headerCssClass: 'aln-rt b',
      formatter: _applicableColumnIdentifier,
      width: 70
    },
    {
      id: 'isLatest',
      type: 'text',
      name: 'Latest',
      field: 'isLatest',
      sortable: true,
      headerCssClass: 'aln-rt b',
      formatter: _applicableColumnIdentifier,
      width: 50
    },
    {
      id: 'archivalDate',
      type: 'text',
      name: 'Archival Date',
      field: 'archivalDate',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 90
    },
    {
      id: 'feedId',
      type: 'text',
      name: 'Feed Id',
      field: 'feedId',
      sortable: true,
      headerCssClass: 'b',
      width: 60
    },
    {
      id: 'newFilePattern',
      type: 'text',
      name: 'Description',
      field: 'newFilePattern',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'originalFilePattern',
      type: 'text',
      name: 'File Pattern',
      field: 'originalFilePattern',
      sortable: true,
      headerCssClass: 'b'
    },
    {
      id: 'parserName',
      type: 'text',
      name: 'Parser Name',
      field: 'parserName',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'brokerDatasetTypes',
      type: 'text',
      name: 'Data Set Types',
      field: 'brokerDatasetTypes',
      sortable: true,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'rowsRead',
      type: 'number',
      name: 'Rows Read',
      field: 'rowsRead',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'recordsPersisted',
      type: 'number',
      name: 'Records Persisted',
      field: 'recordsPersisted',
      sortable: true,
      width: 100
    },
    {
      id: 'mappedCount',
      type: 'number',
      name: 'Mapped Count',
      field: 'mappedCount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'unmappedCount',
      type: 'number',
      name: 'Unmapped Count',
      field: 'unmappedCount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'droppedCount',
      type: 'number',
      name: 'Dropped Count',
      field: 'droppedCount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'recordSetId',
      type: 'number',
      name: 'Record Set Id ',
      field: 'recordSetId',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'parserInstanceId',
      type: 'number',
      name: 'Parser Instance Id',
      field: 'parserInstanceId',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'parserVersion',
      type: 'number',
      name: 'Parser Version',
      field: 'parserVersion',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'fileId',
      type: 'number',
      name: 'File Id',
      field: 'fileId',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'fileVersion',
      type: 'number',
      name: 'File Version',
      field: 'fileVersion',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 100
    },
    {
      id: 'id',
      type: 'number',
      name: 'Id',
      field: 'id',
      sortable: true,
      width: 60,
      headerCssClass: 'b',
      formatter: _applicableColumnIdentifier,
    },
    {
      id: 'syncId',
      type: 'number',
      name: 'Sync Id',
      field: 'syncId',
      sortable: true,
      width: 60,
      headerCssClass: 'b'
    },
    {
      id: 'pfId',
      type: 'number',
      name: 'Pf Id',
      field: 'pfId',
      sortable: true,
      width: 60,
      headerCssClass: 'aln-rt b'
    },
    {
      id: 'startTime',
      type: 'text',
      name: 'Start Time (UTC)',
      field: 'startTime',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 140
    },
    {
      id: 'endTime',
      type: 'text',
      name: 'End Time (UTC)',
      field: 'endTime',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 140
    },
    {
      id: 'rollupStartTime',
      type: 'text',
      name: 'Rollup Start Time (UTC)',
      field: 'rollupStartTime',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 140
    },
    {
      id: 'rollupEndTime',
      type: 'text',
      name: 'Rollup End Time (UTC)',
      field: 'rollupEndTime',
      sortable: true,
      headerCssClass: 'aln-rt b',
      width: 140
    }
  ];

  return columns;
}

function _applicableColumnIdentifier(row, cell, value, columnnDef, dataContext) {
  if (columnnDef.id == 'syncStatusId') {
    return _sync_status_formatter(dataContext);
  } else if (columnnDef.id == 'isLatest') {
    return _is_latest_formatter(dataContext);
  } else if (dataContext.recordSetId && columnnDef.id == 'actions') {
    return _link_formatter('downloadLink', 'Download', 'icon-download', true) +
      (
        (dataContext.syncStatusId == 2 && dataContext.recordsPersisted > 0) ?
          _link_formatter('rawRecords', 'Raw Records', 'icon-info', true) + _link_formatter('unmappedRecords', 'Unmapped Records', 'icon-warning', true)
          : _link_formatter('rawRecords', 'Raw Records', 'icon-info', false) + _link_formatter('unmappedRecords', 'Unmapped Records', 'icon-warning', false)
      ) +
      (
        (dataContext.syncStatusId == 2 && dataContext.droppedCount > 0) ?
          _link_formatter('droppedRecords', 'Dropped Records', 'icon-error', true)
          : _link_formatter('droppedRecords', 'Dropped Records', 'icon-error', false)
      ) +
      (
        (dataContext.syncStatusId == 2 && !dataContext.isLatest && auth == true) ?
         _link_formatter('markAsLatest','Mark as Valid','icon-approve',true)
          : _link_formatter('markAsLatest','Mark as Valid','icon-approve',false)
      );
  } else if (columnnDef.id == 'id') {
    return dataContext.recordSetId? value : null;
  }
}

function _sync_status_formatter(dataContext) {
  if (!dataContext.recordSetId) {
    return _get_div_element('NA', 'disabled');
  } else if (dataContext.syncId == null) {
    return _get_div_element('SCHEDULED', 'info');
  } else if (dataContext.syncStatusId == 2) {
    return _get_div_element('SUCCESS', 'success');
  } else if (dataContext.syncStatusId == 1) {
    return _get_div_element('ACTIVE', 'primary');
  } else if (dataContext.syncStatusId == 3) {
    return _get_div_element('FAILED', 'critical');
  } else return _get_div_element('NA', 'disabled');
}

function _is_latest_formatter(dataContext) {
  if (dataContext.latest == 'Yes') {
    return _get_div_element('True', 'success');
  } else if (dataContext.latest == 'No') {
    return _get_div_element('False', 'disabled');
  } else return _get_div_element('NA', 'disabled');
}

function _link_formatter(action, title, iconType, isVisible) {
  return '<a title="' + title + '"' + (isVisible ? '' : 'style="visibility: hidden;"') + '><i data-role="' + action + '" class="' + iconType + '" /></a>';
}

function _get_div_element(value, element_class) {
  return (
    '<div class="' + element_class + '" style="height:100%; text-align:center;">' + value + '</div>'
  );
}
