import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ReactArcGrid } from 'arc-grid';
import Message from 'commons/components/Message';
import { bindActionCreators } from 'redux';
import { TabPanel, Panel } from 'arc-react-components';
import { cocoaOpsColumns } from '../gridConfig/cocoaColumnConfig';
import {
  borrowAuditColumns,
  availabilityAuditColumns,
  swapAuditColumns,
  locateAuditColumns,
  excessBorrowAuditColumns,
  exclusivePortfolioAuditColumns
} from '../gridConfig/auditConfig';
import { cocoaDroppedRecordsColumns } from '../gridConfig/droppedRecordsConfig';
import {
  cocoaGridOptions,
  auditGridOptions,
  cocoaDroppedRecordsGridOptions
} from '../gridConfig/gridOptions';
import {
  fetchCocoaAuditData,
  fetchCocoaBrokerData,
  fetchCocoaDroppedRecords,
  destroyCocoaAuditData,
  destroyCocoaBrokerData,
  destroyCocoaDroppedRecords,
  fetchValidateParsedFile,
  isUserAuthorizedForTreasuryEdit
} from '../../actions';
import { downloadCocoaFile } from '../../api';

class CocoaGrid extends Component {

  constructor(props) {
    super(props);
    this.state = {
      bottomPanelType: null,
      showBottomPanel: false
    };
    this.renderGridData = this.renderGridData.bind(this);
    this.renderAuditPanel = this.renderAuditPanel.bind(this);
    this.renderDroppedRecordsPanel = this.renderDroppedRecordsPanel.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.openBottomPanel = this.openBottomPanel.bind(this);
    this.closeBottomPanel = this.closeBottomPanel.bind(this);
  }

  componentDidMount() {
    this.props.isUserAuthorizedForTreasuryEdit();
  }

  onCellClickHandler = (args) => {
    let { role } = args.event.target.dataset;
    let payload = {
      cocoaNotificationId: args.item.id,
      feedId: args.item.feedId,
      pfId: args.item.pfId,
      date: args.item.archivalDate,
      parserName: args.item.parserName,
      dataType: args.item.brokerDatasetTypes
    }

    if (!role) {
      return;
    }

    switch (role) {
      case "downloadLink":
        downloadCocoaFile(payload);
        break;
      case "markAsLatest":
        this.props.fetchValidateParsedFile(payload);
        break;
      case "rawRecords":
      case "unmappedRecords":
      case "droppedRecords":
        this.openBottomPanel(role, payload);
        break;
      default:
    }
  };

  openBottomPanel(type, payload) {
    if (type === "rawRecords") {
      this.props.fetchCocoaBrokerData(payload);
    } else if (type === "unmappedRecords") {
      this.props.fetchCocoaAuditData(payload);
    } else if (type === "droppedRecords") {
      this.props.fetchCocoaDroppedRecords(payload);
    }
    this.setState({ showBottomPanel: true, bottomPanelType: type });
  }

  closeBottomPanel() {
    this.props.destroyCocoaAuditData();
    this.props.destroyCocoaBrokerData();
    this.props.destroyCocoaDroppedRecords();
    this.setState({ showBottomPanel: false, bottomPanelType: null });
  }

  renderGridData() {
    if (this.props.cocoaOpsData.length <= 0) {
      return <Message messageData="No data available for this search criteria." />;
    }
    return <ReactArcGrid
      data={this.props.cocoaOpsData}
      gridId="cocoaOpsGrid"
      columns={cocoaOpsColumns(this.props.isUserAuthorizedForValidate)}
      options={cocoaGridOptions()}
      onCellClick={this.onCellClickHandler}
      containerHeight="95%"
    />;
  }

  renderAuditPanel() {
    let borrowPanel = null;
    let availabilityPanel = null;
    let locatePanel = null;
    let excessBorrowPanel = null;
    let swapPanel = null;
    let pthPanel = null;
    let exclusivePortfolioPanel = null;
    let selectedTabId = null;

    if (
      !_.isEmpty(this.props.cocoaAuditData) &&
      this.props.cocoaAuditData.EXCESS_BORROW &&
      this.props.cocoaAuditData.EXCESS_BORROW.length > 0
    ) {
      selectedTabId = 'Excess Borrow';
      excessBorrowPanel = (
        <TabPanel.Tab label="Excess Borrow">
          <ReactArcGrid
            data={this.props.cocoaAuditData.EXCESS_BORROW}
            gridId="excessBorrowAuditGrid"
            columns={excessBorrowAuditColumns()}
            options={auditGridOptions()}
            containerHeight="100%"
          />
        </TabPanel.Tab>
      );
    }
    if (
      !_.isEmpty(this.props.cocoaAuditData) &&
      this.props.cocoaAuditData.EXCESS_BORROW_WITHOUT_RATES &&
      this.props.cocoaAuditData.EXCESS_BORROW_WITHOUT_RATES.length > 0
    ) {
      selectedTabId = 'Excess Borrow';
      excessBorrowPanel = (
        <TabPanel.Tab label="Excess Borrow">
          <ReactArcGrid
            data={this.props.cocoaAuditData.EXCESS_BORROW_WITHOUT_RATES}
            gridId="excessBorrowWithoutRatesAuditGrid"
            columns={excessBorrowAuditColumns()}
            options={auditGridOptions()}
            containerHeight="100%"
          />
        </TabPanel.Tab>
      );
    }
    if (
      !_.isEmpty(this.props.cocoaAuditData) &&
      this.props.cocoaAuditData.PTH &&
      this.props.cocoaAuditData.PTH.length > 0
    ) {
      selectedTabId = 'PTH';
      pthPanel = (
        <TabPanel.Tab label="PTH">
          <ReactArcGrid
            data={this.props.cocoaAuditData.PTH}
            gridId="pthAuditGrid"
            columns={excessBorrowAuditColumns()}
            options={auditGridOptions()}
            containerHeight="100%"
          />
        </TabPanel.Tab>
      );
    }
    if (
      !_.isEmpty(this.props.cocoaAuditData) &&
      this.props.cocoaAuditData.EXCLUSIVE_PORTFOLIO &&
      this.props.cocoaAuditData.EXCLUSIVE_PORTFOLIO.length > 0
    ) {
      selectedTabId = 'Exclusive Portfolio';
      exclusivePortfolioPanel = (
        <TabPanel.Tab label="Exclusive Portfolio">
          <ReactArcGrid
            data={this.props.cocoaAuditData.EXCLUSIVE_PORTFOLIO}
            gridId="exclusivePortfolioAuditGrid"
            columns={exclusivePortfolioAuditColumns()}
            options={auditGridOptions()}
            containerHeight="100%"
          />
        </TabPanel.Tab>
      );
    }
    if (
      !_.isEmpty(this.props.cocoaAuditData) &&
      this.props.cocoaAuditData.BORROW &&
      this.props.cocoaAuditData.BORROW.length > 0
    ) {
      selectedTabId = 'Borrow';
      borrowPanel = (
        <TabPanel.Tab label="Borrow">
          <ReactArcGrid
            data={this.props.cocoaAuditData.BORROW}
            gridId="borrowAuditGrid"
            columns={borrowAuditColumns()}
            options={auditGridOptions()}
            containerHeight="100%"
          />
        </TabPanel.Tab>
      );
    }
    if (
      !_.isEmpty(this.props.cocoaAuditData) &&
      this.props.cocoaAuditData.AVAILABILITY &&
      this.props.cocoaAuditData.AVAILABILITY.length > 0
    ) {
      selectedTabId = 'Availability';
      availabilityPanel = (
        <TabPanel.Tab label="Availability">
          <ReactArcGrid
            data={this.props.cocoaAuditData.AVAILABILITY}
            gridId="availabilityAuditGrid"
            columns={availabilityAuditColumns()}
            options={auditGridOptions()}
            containerHeight="100%"
          />
        </TabPanel.Tab>
      );
    }
    if (
      !_.isEmpty(this.props.cocoaAuditData) &&
      this.props.cocoaAuditData.LOCATE &&
      this.props.cocoaAuditData.LOCATE.length > 0
    ) {
      selectedTabId = 'Locate';
      locatePanel = (
        <TabPanel.Tab label="Locate">
          <ReactArcGrid
            data={this.props.cocoaAuditData.LOCATE}
            gridId="locateAuditGrid"
            columns={locateAuditColumns()}
            options={auditGridOptions()}
            containerHeight="100%"
          />
        </TabPanel.Tab>
      );
    }
    if (
      !_.isEmpty(this.props.cocoaAuditData) &&
      this.props.cocoaAuditData.SWAP &&
      this.props.cocoaAuditData.SWAP.length > 0
    ) {
      selectedTabId = 'Swap';
      swapPanel = (
        <TabPanel.Tab label="Swap">
          <ReactArcGrid
            data={this.props.cocoaAuditData.SWAP}
            gridId="swapAuditGrid"
            columns={swapAuditColumns()}
            options={auditGridOptions()}
            containerHeight="100%"
          />
        </TabPanel.Tab>
      );
    }
    let bottomPanelData = <Message messageData="No Audit Info Found" />;
    if (selectedTabId != null) {
      bottomPanelData = <TabPanel selectedTabId={selectedTabId}>
        {borrowPanel}
        {availabilityPanel}
        {locatePanel}
        {swapPanel}
        {pthPanel}
        {excessBorrowPanel}
        {exclusivePortfolioPanel}
      </TabPanel>;
    }
    return (
      <Panel
        className={`position--relative margin--top size--1`}
        title='Audit Info'
        dismissible
        onClose={this.closeBottomPanel}
      >
        {bottomPanelData}
      </Panel>
    );
  }

  renderDroppedRecordsPanel() {
    let bottomPanelData = <Message messageData="No Dropped Records Found" />;
    if (this.props.cocoaDroppedRecords.length > 0) {
      bottomPanelData = <ReactArcGrid
        data={this.props.cocoaDroppedRecords}
        gridId="droppedRecordsGrid"
        columns={cocoaDroppedRecordsColumns()}
        options={cocoaDroppedRecordsGridOptions()}
        containerHeight="100%"
      />;
    }
    return (
      <Panel
        className={`position--relative margin--top size--1`}
        title='Dropped Records'
        dismissible
        onClose={this.closeBottomPanel}
      >
        {bottomPanelData}
      </Panel>
    );
  }

  render() {
    if (this.props.view !== 'cocoa') {
      return <React.Fragment />;
    }
    return (
      <div className="layout--flex--row">
        {this.renderGridData()}
        {
          this.state.showBottomPanel &&
          (
            (
              (this.state.bottomPanelType === "rawRecords" || this.state.bottomPanelType === "unmappedRecords") &&
              this.renderAuditPanel()
            ) ||
            (
              this.state.bottomPanelType === "droppedRecords" &&
              this.renderDroppedRecordsPanel()
            )
          )
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cocoaOpsData: state.financingOps.cocoaOpsData,
    cocoaAuditData: state.financingOps.cocoaAuditData,
    cocoaDroppedRecords: state.financingOps.cocoaDroppedRecords,
    isUserAuthorizedForValidate: state.financingOps.isUserAuthorizedForValidate
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCocoaAuditData,
      fetchCocoaBrokerData,
      fetchCocoaDroppedRecords,
      destroyCocoaAuditData,
      destroyCocoaBrokerData,
      destroyCocoaDroppedRecords,
      fetchValidateParsedFile,
      isUserAuthorizedForTreasuryEdit
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CocoaGrid);
