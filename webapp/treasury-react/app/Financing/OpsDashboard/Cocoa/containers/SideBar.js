import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TabPanel } from 'arc-react-components';
import { bindActionCreators } from 'redux';
import Sidebar from 'commons/components/Sidebar';
import FilterButton from 'commons/components/FilterButton';
import DateFilter from 'commons/container/DateFilter';
import { getPreviousDate } from 'commons/util';
import {
  fetchCocoaOpsData,
  destroyFinancingOpsData,
  destroyCocoaAuditData,
  destroyCocoaBrokerData,
  destroyCocoaDroppedRecords
} from '../../actions';

class CocoaSideBar extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.onChangeTab = this.onChangeTab.bind(this);
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      asOfDate: getPreviousDate(),
      useRunDate: false
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.view !== prevProps.view) {
      this.setState(this.getDefaultFilters());
      this.props.destroyFinancingOpsData();
      this.props.destroyCocoaAuditData();
      this.props.destroyCocoaBrokerData();
      this.props.destroyCocoaDroppedRecords();
    }
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    let payload;
    payload = {
      date: this.state.asOfDate,
      useRunDate: this.state.useRunDate
    };
    this.props.fetchCocoaOpsData(payload);
    this.props.destroyCocoaAuditData();
  }

  onChangeTab(args) {
    const useRunDate = !this.state.useRunDate;
    this.setState({ useRunDate });
  }

  loadFilters() {
    const dateFilter = (
      <DateFilter
        onSelect={this.onSelect}
        stateKey="asOfDate"
        data={this.state.asOfDate}
        label="Date"
      />
    );
    return (
      <React.Fragment>
        <TabPanel onSelect={this.onChangeTab} tabId="dateFilter">
          <TabPanel.Tab label="Archival Date" id="archival">
            {dateFilter}
          </TabPanel.Tab>
          <TabPanel.Tab label="Run Date" id="run">
            {dateFilter}
          </TabPanel.Tab>
        </TabPanel>
      </React.Fragment>
    );
  }

  render() {
    if (this.props.view !== 'cocoa') {
      return <React.Fragment />;
    }
    return (
      <Sidebar>
        {this.loadFilters()}
        <FilterButton onClick={this.handleClick} reset={false} label="Search" />
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCocoaOpsData,
      destroyFinancingOpsData,
      destroyCocoaAuditData,
      destroyCocoaBrokerData,
      destroyCocoaDroppedRecords
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(CocoaSideBar);
