import React, { Component } from 'react';
import { connect } from 'react-redux';
import OpsGrid from 'commons/components/Grid';
import Message from 'commons/components/Message';
import { portfolioGeneratorOpsColumns } from '../gridConfig/portfolioGeneratorColumnConfig';
import { portfolioGeneratorGridOptions } from '../gridConfig/gridOptions';

class PortfolioGeneratorGrid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  renderGridData() {
    if (this.props.portfolioGeneratorOpsData.length <= 0) {
      return <Message messageData="No data available for this search criteria." />;
    }
    return (
      <React.Fragment>
        <OpsGrid
          data={this.props.portfolioGeneratorOpsData}
          gridId="portfolioGeneratorOpsGrid"
          gridColumns={portfolioGeneratorOpsColumns()}
          gridOptions={portfolioGeneratorGridOptions()}
          label="Search Results"
        />
      </React.Fragment>
    );
  }

  render() {
    if (this.props.view !== 'portfolioGenerator') {
      return <React.Fragment />;
    }
    return this.renderGridData();
  }
}

function mapStateToProps(state) {
  return {
    portfolioGeneratorOpsData: state.financingOps.portfolioGeneratorOpsData
  };
}

export default connect(
  mapStateToProps,
  null
)(PortfolioGeneratorGrid);
