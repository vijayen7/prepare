export function portfolioGeneratorGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    expandTillLevel: -1,
    highlightRowOnClick: true,
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    frozenColumn: 2,
    customColumnSelection: {
      defaultcolumns: getDefaultColumns(),
      totalColumns: getTotalColumns()
    }
  };
  return options;
}


function getDefaultColumns() {
  let defaultColumns = [
    "status", "validatorStatus", "date", "agreementTypes",
    "startTimeString", "endTimeString", "legalEntities", "cpes",
    "nettingGroups", "currencies", "userName", "id",
    "validationRunId"
  ];

  return defaultColumns;
}

function getTotalColumns() {
  let totalColumns = [
    "status", "validatorStatus", "date", "agreementTypes",
    "startTimeString", "endTimeString", "legalEntities", "cpes",
    "nettingGroups", "currencies", "userName", "id",
    "validationRunId", "agreementTypeIds", "legalEntityIds", "cpeIds",
    "nettingGroupIds", "currencyIds"
  ];
  return totalColumns;
}
