export function portfolioGeneratorOpsColumns() {
  var columns = [
    {
      id: 'status',
      name: 'Status',
      field: 'status',
      type: 'text',
      filter: true,
      sortable: true,
      ignoreInColumnSelection: false,
      formatter: _applicableColumnIdentifier,
      excelDataFormatter: _linkExcelFormatter,
      headerCssClass: 'b',
      width: 120
    },
    {
      id: 'validatorStatus',
      name: 'Validator Status',
      field: 'validatorStatus',
      type: 'text',
      filter: true,
      sortable: true,
      ignoreInColumnSelection: false,
      formatter: _applicableColumnIdentifier,
      excelDataFormatter: _linkExcelFormatter,
      headerCssClass: 'b',
      width: 110
    },
    {
      id: 'date',
      type: 'text',
      name: 'Job Date',
      field: 'date',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'agreementTypes',
      type: 'text',
      name: 'Agreement Type',
      field: 'agreementTypes',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 100
    },
    {
      id: 'startTimeString',
      type: 'text',
      name: 'Start Time(UTC)',
      field: 'startTimeString',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'endTimeString',
      type: 'text',
      name: 'End Time(UTC)',
      field: 'endTimeString',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'legalEntities',
      type: 'text',
      name: 'Legal Entity',
      field: 'legalEntities',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'cpes',
      type: 'text',
      name: 'Counter Party',
      field: 'cpes',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'nettingGroups',
      type: 'text',
      name: 'Netting Group',
      field: 'nettingGroups',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'currencies',
      type: 'text',
      name: 'Currency',
      field: 'currencies',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'userName',
      type: 'text',
      name: 'User Login',
      field: 'userName',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'id',
      type: 'text',
      name: 'Run Id',
      field: 'id',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'validationRunId',
      type: 'text',
      name: 'Validator Run Id',
      field: 'validationRunId',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'agreementTypeIds',
      type: 'text',
      name: 'Agreement Type Ids',
      field: 'agreementTypeIds',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 100
    },
    {
      id: 'legalEntityIds',
      type: 'text',
      name: 'Legal Entity Ids',
      field: 'legalEntityIds',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'cpeIds',
      type: 'text',
      name: 'Counter Party Ids',
      field: 'cpeIds',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'nettingGroupIds',
      type: 'text',
      name: 'Netting Group Ids',
      field: 'nettingGroupIds',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'currencyIds',
      type: 'text',
      name: 'Currency Ids',
      field: 'currencyIds',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    }
  ];

  return columns;
}

function _applicableColumnIdentifier(row, cell, value, columnnDef, dataContext) {
  if (columnnDef.field == 'status') {
    return _status_formatter(value, dataContext);
  } else if (columnnDef.field == 'validatorStatus') {
    return _validator_status_formatter(value, dataContext);
  }
}

function _status_formatter(value, dataContext) {
  if (dataContext.status == 'FAILED') {
    return _get_div_element(value, 'critical');
  } else if (dataContext.status == 'NO VALID TPM DATA') {
    return _get_div_element(value, 'info');
  } else if (dataContext.status == 'SUCCESS') {
    return _get_div_element(value, 'success');
  } else if (dataContext.status == 'IN_PROGRESS') {
    return _get_div_element(value, 'primary');
  }
}

function _validator_status_formatter(value, dataContext) {
  if (dataContext.validatorStatus == 'FAILED') {
    return _get_div_element(value, 'critical');
  } else if (dataContext.validatorStatus == 'NO VALID TPM DATA') {
    return _get_div_element(value, 'info');
  } else if (dataContext.validatorStatus == 'SUCCESS') {
    return _get_div_element(value, 'success');
  } else if (dataContext.validatorStatus == 'IN_PROGRESS') {
    return _get_div_element(value, 'primary');
  } else if (dataContext.validatorStatus == undefined) {
    return _get_div_element('N/A', 'disabled');
  }
}

function _get_div_element(value, element_class) {
  return (
    '<div class="' + element_class + '" style="height:100%; text-align:center;">' + value + '</div>'
  );
}

function _linkExcelFormatter(row, cell, value, columnnDef, dataContext) {
  cell.actions = '';
}
