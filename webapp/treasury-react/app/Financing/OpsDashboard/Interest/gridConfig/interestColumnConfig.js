export function interestOpsColumns() {
  var columns = [
    {
      id: 'calculationStatus',
      name: 'Run Status',
      field: 'calculation_status',
      type: 'text',
      filter: true,
      sortable: true,
      ignoreInColumnSelection: false,
      formatter: _applicableColumnIdentifier,
      excelDataFormatter: _linkExcelFormatter,
      headerCssClass: 'b',
      width: 120
    },
    {
      id: 'genevaSyncStatus',
      name: 'BEST Sync Status',
      field: 'geneva_sync_status',
      type: 'text',
      filter: true,
      sortable: true,
      ignoreInColumnSelection: false,
      formatter: _applicableColumnIdentifier,
      excelDataFormatter: _linkExcelFormatter,
      headerCssClass: 'b',
      width: 110
    },
    {
      id: 'jobDate',
      type: 'text',
      name: 'Job Date',
      field: 'job_date',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'agreement',
      type: 'text',
      name: 'Agreement Type',
      field: 'agreement',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 100
    },
    {
      id: 'startTime',
      type: 'text',
      name: 'Start Time(UTC)',
      field: 'start_time',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'endTime',
      type: 'text',
      name: 'End Time(UTC)',
      field: 'end_time',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'descoEntity',
      type: 'text',
      name: 'Legal Entity',
      field: 'desco_entity',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'cpe',
      type: 'text',
      name: 'Counter Party',
      field: 'CPE',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'nettingGroup',
      type: 'text',
      name: 'Netting Group',
      field: 'netting_group',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'currency',
      type: 'text',
      name: 'Currency',
      field: 'currency',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 150
    },
    {
      id: 'host',
      type: 'text',
      name: 'Host',
      field: 'host',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'pid',
      type: 'text',
      name: 'PID',
      field: 'pid',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'userLogin',
      type: 'text',
      name: 'User Login',
      field: 'user_login',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'runId',
      type: 'text',
      name: 'Run Id',
      field: 'run_id',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    },
    {
      id: 'builderRunIds',
      type: 'text',
      name: 'Portfolio Generator Run Id',
      field: 'builder_run_ids',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    }
  ];

  return columns;
}

function _applicableColumnIdentifier(row, cell, value, columnnDef, dataContext) {
  if (columnnDef.field == 'calculation_status') {
    return _calculation_status_formatter(value, dataContext);
  } else if (columnnDef.field == 'geneva_sync_status') {
    return _geneva_sync_status_formatter(value, dataContext);
  }
}

function _geneva_sync_status_formatter(value, dataContext) {
  if (dataContext.geneva_sync_status == 'SYNC_FINISHED') {
    return _get_div_element(value, 'success');
  } else if (
    dataContext.geneva_sync_status == 'GENEVA_VALIDATION_FAILURE' ||
    dataContext.geneva_sync_status == 'GENEVA_POSTING_FAILED' ||
    dataContext.geneva_sync_status == 'ENRICHMENT_FAILED' ||
    dataContext.geneva_sync_status == 'SYNC_FAILED'
  ) {
    return _get_div_element(value, 'critical');
  } else {
    return _get_div_element(value, 'info');
  }
}

function _calculation_status_formatter(value, dataContext) {
  if (dataContext.calculation_status == 'FAILED') {
    return _get_div_element(value, 'critical');
  } else if (dataContext.calculation_status == 'KILLED') {
    return _get_div_element(value, 'warning');
  } else if (dataContext.calculation_status == 'NO TPM DATA') {
    return _get_div_element(value, 'info');
  } else if (dataContext.calculation_status == 'SCHEDULED') {
    return _get_div_element(value, 'disabled');
  } else if (dataContext.calculation_status == 'ACTIVE') {
    return _get_div_element(value, 'primary');
  } else if (dataContext.calculation_status == 'SUCCESS') {
    return _get_div_element(value, 'success');
  } else if (dataContext.calculation_status == 'NO BUILDER DATA') {
       return _get_div_element(value, 'info');
  }
}

function _get_div_element(value, element_class) {
  return (
    '<div class="' + element_class + '" style="height:100%; text-align:center;">' + value + '</div>'
  );
}

function _linkExcelFormatter(row, cell, value, columnnDef, dataContext) {
  cell.actions = '';
}
