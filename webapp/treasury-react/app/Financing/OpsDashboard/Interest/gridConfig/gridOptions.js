export function interestGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    expandTillLevel: -1,
    highlightRowOnClick: true,
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    frozenColumn: 2
  };
  return options;
}
