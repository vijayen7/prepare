import React, { Component } from 'react';
import { connect } from 'react-redux';
import OpsGrid from 'commons/components/Grid';
import Message from 'commons/components/Message';
import { interestOpsColumns } from '../gridConfig/interestColumnConfig';
import { interestGridOptions } from '../gridConfig/gridOptions';

class InterestGrid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  renderGridData() {
    if (this.props.interestOpsData.length <= 0) {
      return <Message messageData="No data available for this search criteria." />;
    }
    return (
      <React.Fragment>
        <OpsGrid
          data={this.props.interestOpsData}
          gridId="financingOpsGrid"
          gridColumns={interestOpsColumns()}
          gridOptions={interestGridOptions()}
          height={950}
          label="Search Results"
        />
      </React.Fragment>
    );
  }

  render() {
    if (this.props.view !== 'interest') {
      return <React.Fragment />;
    }
    return this.renderGridData();
  }
}

function mapStateToProps(state) {
  return {
    interestOpsData: state.financingOps.interestOpsData
  };
}

export default connect(
  mapStateToProps,
  null
)(InterestGrid);
