import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Sidebar from 'commons/components/Sidebar';
import FilterButton from 'commons/components/FilterButton';
import InputFilter from 'commons/components/InputFilter';
import DateFilter from 'commons/container/DateFilter';
import { getCurrentDate } from 'commons/util';
import { fetchInterestOpsData, destroyFinancingOpsData } from '../../actions';

class InterestSideBar extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      asOfDate: getCurrentDate(),
      numberOfJobs: 20
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.view !== prevProps.view) {
      this.setState(this.getDefaultFilters());
      this.props.destroyFinancingOpsData();
    }
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleClick() {
    let payload;
    payload = {
      numberOfJobs: this.state.numberOfJobs,
      date: this.state.asOfDate
    };
    this.props.fetchInterestOpsData(payload);
  }

  loadFilters() {
    return (
      <React.Fragment>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="asOfDate"
          data={this.state.asOfDate}
          label="Date"
        />
        <InputFilter
          data={this.state.numberOfJobs}
          onSelect={this.onSelect}
          stateKey="numberOfJobs"
          label="Last N Jobs"
        />
      </React.Fragment>
    );
  }

  render() {
    if (this.props.view !== 'interest') {
      return <React.Fragment />;
    }
    return (
      <Sidebar>
        {this.loadFilters()}
        <FilterButton onClick={this.handleClick} reset={false} label="Search" />
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchInterestOpsData,
      destroyFinancingOpsData
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(InterestSideBar);
