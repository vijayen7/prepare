import { put, takeEvery, call, all } from 'redux-saga/effects';
import { START_LOADING, END_LOADING, HANDLE_EXCEPTION } from 'commons/constants';
import { ToastService } from 'arc-react-components';
import {
  FETCH_INTEREST_OPS_DATA,
  FETCH_PORTFOLIO_GENERATOR_OPS_DATA,
  FETCH_SECLEND_OPS_DATA,
  FETCH_COCOA_OPS_DATA,
  FETCH_COCOA_AUDIT_DATA,
  FETCH_COCOA_BROKER_DATA,
  FETCH_COCOA_DROPPED_RECORDS,
  FETCH_VALIDATE_PARSED_FILE,
  FETCH_IS_USER_AUTHORIZED_FOR_TREASURY_EDIT
} from './constants';
import {
  getInterestOpsData,
  getPortfolioGeneratorOpsData,
  getSeclendOpsData,
  getCocoaOpsData,
  getCocoaAuditData,
  getCocoaBrokerData,
  getCocoaDroppedRecords,
  getValidateParsedFile,
  getIsUserAuthorizedForTreasuryEdit
} from './api';

function* fetchInterestOpsData(action) {
  try {
    yield put({ type: START_LOADING });

    const data = yield call(getInterestOpsData, action.payload);
    yield put({ type: `${action.type}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchPortfolioGeneratorOpsData(action) {
  try {
    yield put({ type: START_LOADING });

    const data = yield call(getPortfolioGeneratorOpsData, action.payload);
    yield put({ type: `${action.type}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSeclendOpsData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getSeclendOpsData, action.payload);
    yield put({ type: `${action.type}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchCocoaOpsData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCocoaOpsData, action.payload);
    yield put({ type: `${action.type}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* getTreasuryEditUserAuth(action) {
  try {
    let data = yield call(getIsUserAuthorizedForTreasuryEdit);
    yield put({
      type: `${FETCH_IS_USER_AUTHORIZED_FOR_TREASURY_EDIT}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_IS_USER_AUTHORIZED_FOR_TREASURY_EDIT}_FAILURE` });
  }
}

function*  fetchValidateParsedFile(action) {
  let payload = action.payload;
  ToastService.append({
    content: "Attempting to validate PfId: "+ payload.pfId,
    type: ToastService.ToastType.INFO,
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: 10000
  });
  const response = yield call(getValidateParsedFile, payload);
  if(response==null){
    yield call (fetchCocoaOpsData, {
      type: FETCH_COCOA_OPS_DATA,
      payload
    });
  }
  else{
    let error = response.message;
    if(error.search("UnauthorizedException") > -1){
      error = "User Not Authorized to Validate Parsed File"
    }
    else {
      error = "Validation is already in Progress for FeedId : "+ payload.feedId
    }
    ToastService.append({
      content: error,
      type: ToastService.ToastType.CRITICAL,
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 20000
    })
  }
}

function* fetchCocoaBrokerData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCocoaBrokerData, action.payload);
    yield put({ type: `${FETCH_COCOA_AUDIT_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_COCOA_AUDIT_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchCocoaAuditData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCocoaAuditData, action.payload);
    yield put({ type: `${action.type}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchCocoaDroppedRecords(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCocoaDroppedRecords, action.payload);
    yield put({ type: `${action.type}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* interestOpsDataSaga() {
  yield [takeEvery(FETCH_INTEREST_OPS_DATA, fetchInterestOpsData)];
}

function* portfolioGeneratorOpsDataSaga() {
  yield [takeEvery(FETCH_PORTFOLIO_GENERATOR_OPS_DATA, fetchPortfolioGeneratorOpsData)];
}

function* seclendOpsDataSaga() {
  yield [takeEvery(FETCH_SECLEND_OPS_DATA, fetchSeclendOpsData)];
}

function* cocoaOpsDataSaga() {
  yield [takeEvery(FETCH_COCOA_OPS_DATA, fetchCocoaOpsData)];
}

function* fetchIsUserAuthorizedForTreasuryEditSaga () {
  yield [takeEvery(FETCH_IS_USER_AUTHORIZED_FOR_TREASURY_EDIT, getTreasuryEditUserAuth)];
}

function* validateParsedFileSaga() {
  yield [takeEvery(FETCH_VALIDATE_PARSED_FILE, fetchValidateParsedFile)];
}

function* cocoaBrokerDataSaga() {
  yield [takeEvery(FETCH_COCOA_BROKER_DATA, fetchCocoaBrokerData)];
}

function* cocoaAuditDataSaga() {
  yield [takeEvery(FETCH_COCOA_AUDIT_DATA, fetchCocoaAuditData)];
}

function* cocoaDroppedRecordsSaga() {
  yield [takeEvery(FETCH_COCOA_DROPPED_RECORDS, fetchCocoaDroppedRecords)];
}

export default function* financingOpsDataSaga() {
  yield all([interestOpsDataSaga(), portfolioGeneratorOpsDataSaga(), seclendOpsDataSaga(), cocoaOpsDataSaga(), fetchIsUserAuthorizedForTreasuryEditSaga(), validateParsedFileSaga() , cocoaAuditDataSaga(), cocoaBrokerDataSaga(), cocoaDroppedRecordsSaga()]);
}
