import React, { Component } from 'react';
import InterestSideBar from './Interest/containers/SideBar';
import PortfolioGeneratorSideBar from './PortfolioGenerator/containers/SideBar';
import SeclendSideBar from './Seclend/containers/SideBar';
import CocoaSideBar from './Cocoa/containers/SideBar';
import Loader from 'commons/container/Loader';
import InterestGrid from './Interest/containers/Grid';
import PortfolioGeneratorGrid from './PortfolioGenerator/containers/Grid';
import SeclendGrid from './Seclend/containers/Grid';
import CocoaGrid from './Cocoa/containers/Grid';

export default class OpsDashboard extends Component {
  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = [];
  }

  componentWillMount() {
    this.setState({ view: 'cocoa' });
  }

  componentDidMount() {
    this.cocoaView.classList.add('active');
  }

  handleToggle(view) {
    this.interestView.classList.remove('active');
    this.seclendView.classList.remove('active');
    this.cocoaView.classList.remove('active');
    this.portfolioGeneratorView.classList.remove('active');
    switch (view) {
      case 'cocoa':
        this.cocoaView.classList.add('active');
        break;
      case 'interest':
        this.interestView.classList.add('active');
        break;
      case 'seclend':
        this.seclendView.classList.add('active');
        break;
      case 'portfolioGenerator':
        this.portfolioGeneratorView.classList.add('active');
        break;
    }
    this.setState({ view });
  }

  renderSideBar(view) {
    return (
      <React.Fragment>
        <InterestSideBar view={view} />
        <PortfolioGeneratorSideBar view={view} />
        <SeclendSideBar view={view} />
        <CocoaSideBar view={view} />
      </React.Fragment>
    );
  }

  renderGrid(view) {
    return (
      <React.Fragment>
        <InterestGrid view={view} />
        <PortfolioGeneratorGrid view={view} />
        <SeclendGrid view={view} />
        <CocoaGrid view={view} />
      </React.Fragment>
    );
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            ref={(header) => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div slot="application-menu" className="application-menu-toggle-view">
              <a
                ref={(cocoa) => {
                  this.cocoaView = cocoa;
                }}
                onClick={() => this.handleToggle('cocoa')}
              >
                <span className="margin--left--small margin--right--small">
                  Cocoa
                </span>
              </a>
              <a
                ref={(interest) => {
                  this.interestView = interest;
                }}
                onClick={() => this.handleToggle('interest')}
              >
                <span className="margin--left--small margin--right--small">Interest</span>
              </a>
              <a
                ref={(portfolioGenerator) => {
                  this.portfolioGeneratorView = portfolioGenerator;
                }}
                onClick={() => this.handleToggle('portfolioGenerator')}
              >
                <span className="margin--left--small margin--right--small"> Portfolio Generator </span>
              </a>
              <a
                ref={(seclend) => {
                  this.seclendView = seclend;
                }}
                onClick={() => this.handleToggle('seclend')}
              >
                Seclend
              </a>
            </div>
          </arc-header>
          <div className="layout--flex">
            <div>{this.renderSideBar(this.state.view)}</div>
            <div className="size--5 padding--horizontal--double">
              {this.renderGrid(this.state.view)}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
