import { combineReducers } from 'redux';
import {
  FETCH_INTEREST_OPS_DATA,
  FETCH_PORTFOLIO_GENERATOR_OPS_DATA,
  FETCH_SECLEND_OPS_DATA,
  FETCH_COCOA_OPS_DATA,
  FETCH_COCOA_AUDIT_DATA,
  DESTROY_FINANCING_OPS_DATA,
  DESTROY_PORTFOLIO_GENERATOR_OPS_DATA,
  DESTROY_COCOA_AUDIT_DATA,
  FETCH_COCOA_DROPPED_RECORDS,
  DESTROY_COCOA_DROPPED_RECORDS,
  FETCH_IS_USER_AUTHORIZED_FOR_TREASURY_EDIT
} from './constants';

function interestOpsDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_INTEREST_OPS_DATA}_SUCCESS`:
      return action.data.tableData;
    case DESTROY_FINANCING_OPS_DATA:
      return [];
  }
  return state;
}

function portfolioGeneratorOpsDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_PORTFOLIO_GENERATOR_OPS_DATA}_SUCCESS`:
      return action.data;
    case DESTROY_PORTFOLIO_GENERATOR_OPS_DATA:
      return [];
  }
  return state;
}

function seclendOpsDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SECLEND_OPS_DATA}_SUCCESS`:
      return action.data.resultList;
    case DESTROY_FINANCING_OPS_DATA:
      return [];
  }
  return state;
}

function cocoaOpsDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_COCOA_OPS_DATA}_SUCCESS`:
      return action.data;
    case DESTROY_FINANCING_OPS_DATA:
      return [];
  }
  return state;
}

function cocoaAuditDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_COCOA_AUDIT_DATA}_SUCCESS`:
      return action.data;
    case DESTROY_COCOA_AUDIT_DATA:
      return [];
  }
  return state;
}

function cocoaDroppedRecordsReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_COCOA_DROPPED_RECORDS}_SUCCESS`:
      return action.data;
    case DESTROY_COCOA_DROPPED_RECORDS:
      return [];
  }
  return state;
}

function isUserAuthorizedForTreasuryEditReducer(state = true, action) {
  switch (action.type) {
    case `${FETCH_IS_USER_AUTHORIZED_FOR_TREASURY_EDIT}_SUCCESS`:
      return action.data;
    default:
      return state;
  }
}

function exceptionReducer(state = false, action) {
  switch (action.type) {
    case `*_FAILURE`:
      return true;
  }
  return state;
}

const rootReducer = combineReducers({
  interestOpsData: interestOpsDataReducer,
  portfolioGeneratorOpsData: portfolioGeneratorOpsDataReducer,
  seclendOpsData: seclendOpsDataReducer,
  cocoaOpsData: cocoaOpsDataReducer,
  cocoaAuditData: cocoaAuditDataReducer,
  cocoaDroppedRecords: cocoaDroppedRecordsReducer,
  isUserAuthorizedForValidate: isUserAuthorizedForTreasuryEditReducer,
  exceptionOccurred: exceptionReducer
});

export default rootReducer;
