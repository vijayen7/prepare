import React, { Component } from 'react';
import { connect } from 'react-redux';
import OpsGrid from 'commons/components/Grid';
import Message from 'commons/components/Message';
import { seclendOpsColumns } from '../gridConfig/seclendColumnConfig';
import { seclendGridOptions } from '../gridConfig/gridOptions';

class SeclendGrid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  renderGridData() {
    if (this.props.seclendOpsData.length <= 0) {
      return <Message messageData="No data available for this search criteria." />;
    }
    return (
      <React.Fragment>
        <OpsGrid
          data={this.props.seclendOpsData}
          gridId="financingOpsGrid"
          gridColumns={seclendOpsColumns()}
          gridOptions={seclendGridOptions()}
          height={950}
          label="Search Results"
        />
      </React.Fragment>
    );
  }

  render() {
    if (this.props.view !== 'seclend') {
      return <React.Fragment />;
    }
    return this.renderGridData();
  }
}

function mapStateToProps(state) {
  return {
    seclendOpsData: state.financingOps.seclendOpsData
  };
}

export default connect(
  mapStateToProps,
  null
)(SeclendGrid);
