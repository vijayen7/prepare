import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Sidebar from 'commons/components/Sidebar';
import FilterButton from 'commons/components/FilterButton';
import CheckboxFilter from 'commons/components/CheckboxFilter';
import SeclendDataTypeFilter from 'commons/container/SeclendDataTypeFilter';
import DateFilter from 'commons/container/DateFilter';
import { getPreviousDate, isAllKeySelected, getCommaSeparatedValues } from 'commons/util';
import { fetchSeclendOpsData, destroyFinancingOpsData } from '../../actions';

class SeclendSideBar extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      asOfDate: getPreviousDate(),
      onlyLatest: true,
      selectedSeclendDataTypes: []
    };
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  componentDidUpdate(prevProps) {
    if (this.props.view !== prevProps.view) {
      this.setState(this.getDefaultFilters());
      this.props.destroyFinancingOpsData();
    }
  }

  handleClick() {
    let payload;
    payload = {
      onlyLatest: this.state.onlyLatest,
      date: this.state.asOfDate,
      seclendDataTypes:
        this.state.selectedSeclendDataTypes.length > 0 &&
        !isAllKeySelected(this.state.selectedSeclendDataTypes)
          ? getCommaSeparatedValues(this.state.selectedSeclendDataTypes)
          : -1
    };
    this.props.fetchSeclendOpsData(payload);
  }

  loadFilters() {
    return (
      <React.Fragment>
        <DateFilter
          onSelect={this.onSelect}
          stateKey="asOfDate"
          data={this.state.asOfDate}
          label="Date"
        />
        <br />
        <SeclendDataTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedSeclendDataTypes}
          label="Data Type"
        />
        <CheckboxFilter
          defaultChecked={this.state.onlyLatest}
          onSelect={this.onSelect}
          stateKey="onlyLatest"
          label="Only Latest"
        />
      </React.Fragment>
    );
  }

  render() {
    if (this.props.view !== 'seclend') {
      return <React.Fragment />;
    }
    return (
      <Sidebar>
        {this.loadFilters()}
        <FilterButton onClick={this.handleClick} reset={false} label="Search" />
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchSeclendOpsData,
      destroyFinancingOpsData
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(SeclendSideBar);
