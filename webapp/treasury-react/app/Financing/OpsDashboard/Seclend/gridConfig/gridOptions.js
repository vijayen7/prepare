export function seclendGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    expandTillLevel: -1,
    highlightRowOnClick: true,
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    sortList: [{ columnId: 'jobStatus', sortAsc: true }],
    frozenColumn: 1
  };
  return options;
}
