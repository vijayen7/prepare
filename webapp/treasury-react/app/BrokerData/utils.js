import moment from "moment";

export function getPreviousWorkday() {
  const workday = moment();
  const day = workday.day();
  let diff = 1; // returns yesterday
  if (day == 0 || day == 1) {
    // is Sunday or Monday
    diff = day + 2; // returns Friday
  }
  return workday.subtract(diff, "days");
}

export function splitCommaSeparatedString (inputStringObjects) {
  if (inputStringObjects) {
    return inputStringObjects.split(',').map(Number).map(eachValue => ({
      key: eachValue, value: null
    }));
  }
  else {
    return [];
  }
}

export function convertListToString (inputList) {
  let valueString = "";
  valueString = Array.prototype.map.call(inputList, s => s.key.toString()).toString();
  return valueString;
}
