/* eslint-disable no-restricted-syntax */
import { action, flow, observable } from 'mobx';
import * as BrokerDataAPI from './api';
import { getPreviousWorkday, convertListToString } from './utils';
import { parseDJSrvString } from '../commons/util';
import { BASE_URL } from "commons/constants";

const copy = require('clipboard-copy')

export const INITIAL_FILTERS = {
  selectedLegalEntities: [],
  selectedCpes: [],
  selectedAgreementTypes: [],
  selectedDataSetSource: [],
  selectedDate: getPreviousWorkday().format('YYYY-MM-DD'),
};

const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  message: '',
  error: ''
};

const INITIAL_DRILLDOWN_STATUS = {
  inProgress: false,
  message: '',
  error: ''
};

const INITIAL_SUMMARY = null;
const INITIAL_DETAIL = null;
const INITIAL_SELECTED_SUMMARY = null;

export default class ReportDataStore {
  @observable
  filters = INITIAL_FILTERS;

  @observable
  summary = INITIAL_SUMMARY;

  @observable
  detail = INITIAL_DETAIL;

  @observable
  selectedSummary = INITIAL_SELECTED_SUMMARY;

  @observable
  searchStatus = INITIAL_SEARCH_STATUS;

  @observable
  drillDownStatus = INITIAL_DRILLDOWN_STATUS;

  @action
  setFilters(newFilters) {
    this.filters = newFilters;
  }

  @action.bound
  reset() {
    this.summary = INITIAL_SUMMARY;
    this.detail = INITIAL_DETAIL;
    this.selectedSummary = INITIAL_SELECTED_SUMMARY;
    this.searchStatus = INITIAL_SEARCH_STATUS;
    this.drillDownStatus = INITIAL_DRILLDOWN_STATUS;
  }

  @action.bound
  resetFilters() {
    this.filters = INITIAL_FILTERS;
  }

  @action.bound
  copySearchUrl(location) {
    let url;
    url = location + `${BASE_URL}comet/brokerdata.html?`
    url += `legalEntityIds=${convertListToString(this.filters.selectedLegalEntities)}`
    url += `&cpeIds=${convertListToString(this.filters.selectedCpes)}`
    url += `&agmtTypeIds=${convertListToString(this.filters.selectedAgreementTypes)}`
    url += `&dateString=${this.filters.selectedDate}`;
    copy(url);
  }

  searchSummary = flow(
    function* searchSummary(filters) {
      this.filters = filters;

      this.searchStatus = {
        inProgress: true,
        message: '',
        error: ''
      };

      try {
        let lcmPositionDataParam = this.getLcmPositionDataParam(filters);
        const summary = yield BrokerDataAPI.getParsedDataSummary(lcmPositionDataParam);

        summary.forEach(item => {
          if (item.key) {
            item.legalEntityId = item.key.legalEntityId
            item.cpeId = item.key.cpeId
            item.agreementTypeId = item.key.agreementTypeId
            item.levelId = item.key.levelId;
            item.levelCode = item.key.levelCode;
            item.fileAlias = item.key.fileAlias;
            item.fileName = item.key.fileName;
            item.date = item.key.date;
            item.segCollateralUSD = item.finalCashBalanceUSD;
          }
        });

        this.summary = summary;
        this.searchStatus = {
          message: '',
          error: '',
          inProgress: false
        };
      } catch (e) {
        console.log(e);
        this.searchStatus = {
          message: '',
          error: e.message,
          inProgress: false
        };
      }
    }.bind(this)
  );

  drillDown = flow(
    function* drillDown(summary) {
      this.selectedSummary = summary;

      this.drillDownStatus = {
        inProgress: true,
        message: '',
        error: ''
      };

      try {
        let brokerDataLcmFilter = this.getBrokerDataLcmFilter(summary);
        const detail = yield BrokerDataAPI.getParsedDataByFile(brokerDataLcmFilter);
        detail.forEach((item) => {
          item.id = item.dataIdBds;
          if (item.key) {
            item.fileName = item.key.fileName
            item.fileAlias = item.key.fileAlias
            item.legalEntityId = item.key.legalEntityId
            item.cpeId = item.key.cpeId
            item.custodianAccountId = item.key.custodianAccountId
            item.agreementTypeId = item.key.agreementTypeId
            item.levelId = item.key.levelId
            item.date = item.key.date
            item.gboType = item.cometPositionDetailExtended.gboType
            item.country = item.cometPositionDetailExtended.country
            item.matchKey = item.cometPositionDetailExtended.matchKey
            item.matchValue = item.cometPositionDetailExtended.matchValue
            item.securityName = item.cometPositionDetailExtended.securityName
            item.priceNative = item.cometPositionDetailExtended.priceNative
            item.underlyingLocal = item.cometPositionDetailExtended.underlyingLocal
            item.underlyingCanonicalSpnRic = item.cometPositionDetailExtended.underlyingCanonicalSpnRic
            item.underlyingDeath = item.cometPositionDetailExtended.underlyingDeath
            item.underlyingBloomberg = item.cometPositionDetailExtended.underlyingBloomberg
            item.underlyingPutCallInd = item.cometPositionDetailExtended.underlyingPutCallInd
            item.underlyingUnderlyingRic = item.cometPositionDetailExtended.underlyingUnderlyingRic
            item.underlyingUnderlyingLocal = item.cometPositionDetailExtended.underlyingUnderlyingLocal
            item.underlyingSpotSpnLocal = item.cometPositionDetailExtended.underlyingSpotSpnLocal
            item.underlyingForwardEndDate = item.cometPositionDetailExtended.underlyingForwardEndDate
            item.underlyingForwardBeginDate = item.cometPositionDetailExtended.underlyingForwardBeginDate
            item.underlyingExpirationDate = item.cometPositionDetailExtended.underlyingExpirationDate
            item.underlyingStrikePrice = item.cometPositionDetailExtended.underlyingStrikePrice
            item.underlyingIssuerSpnRedCode = item.cometPositionDetailExtended.underlyingIssuerSpnRedCode
            item.feedId = item.key.feedId
            item.fileId = item.key.fileId
            item.rawRecordId = item.key.rawRecordId
            item.fileVersion = item.key.fileVersion
            item.spotSpnRic = item.cometPositionDetailExtended.spotSpnRic
            item.spotSpnIsin = item.cometPositionDetailExtended.spotSpnIsin
            item.spotSpnBloomberg = item.cometPositionDetailExtended.spotSpnBloomberg
            item.forwardCurrency1 = item.cometPositionDetailExtended.forwardCurrency1
            item.forwardCurrency2 = item.cometPositionDetailExtended.forwardCurrency2
            item.mapType = item.cometPositionDetailExtended.mapType
            item.fxRate = item.cometPositionDetailExtended.fxRate
            item.marginFxRate = item.cometPositionDetailExtended.marginFxRate
            item.tradeDate = item.cometPositionDetailExtended.tradeDate
            item.isin = item.cometPositionDetailExtended.isin
            item.cusip = item.cometPositionDetailExtended.cusip
            item.sedol = item.cometPositionDetailExtended.sedol
            item.ric = item.cometPositionDetailExtended.ric
            item.bloomberg = item.cometPositionDetailExtended.bloomberg
            item.death = item.cometPositionDetailExtended.death
            item.birth = item.cometPositionDetailExtended.birth
            item.strikePrice = item.cometPositionDetailExtended.strikePrice
            item.swapLevel = item.cometPositionDetailExtended.swapLevel
            item.swapStart = item.cometPositionDetailExtended.swapStart
            item.buySell = item.cometPositionDetailExtended.buySell
            item.putCallInd = item.cometPositionDetailExtended.putCallInd
            item.forwardQuantity1 = item.cometPositionDetailExtended.forwardQuantity1
            item.forwardQuantity2 = item.cometPositionDetailExtended.forwardQuantity2
            item.underlyingForwardSettleDate = item.cometPositionDetailExtended.underlyingForwardSettleDate
            item.forwardSpn1 = item.cometPositionDetailExtended.forwardSpn1
            item.forwardSpn2 = item.cometPositionDetailExtended.forwardSpn2
            item.underlyingIsin = item.cometPositionDetailExtended.underlyingIsin
            item.underlyingRic = item.cometPositionDetailExtended.underlyingRic
            item.underlyingSedol = item.cometPositionDetailExtended.underlyingSedol
            item.bondSpnIsin = item.cometPositionDetailExtended.bondSpnIsin
            item.bondSpnCusip = item.cometPositionDetailExtended.bondSpnCusip
            item.accruedate = item.cometPositionDetailExtended.accruedate
            item.expirationDate = item.cometPositionDetailExtended.expirationDate
            item.maturityDate = item.cometPositionDetailExtended.maturityDate
            item.onDate = item.cometPositionDetailExtended.onDate
            item.referenceSpnIsin = item.cometPositionDetailExtended.referenceSpnIsin
            item.repoRate = item.cometPositionDetailExtended.repoRate
            item.spotSpnLocal = item.cometPositionDetailExtended.spotSpnLocal
            item.mapType = item.cometPositionDetailExtended.mapType
            item.segCollateralUSD = item.finalCashBalanceUSD
          }
        });

        this.detail = detail;
        this.drillDownStatus = {
          message: '',
          error: '',
          inProgress: false
        };
      } catch (e) {
        this.drillDownStatus = {
          message: '',
          error: e.message,
          inProgress: false
        };
      }
    }.bind(this)
  );

  getBrokerDataLcmFilter(summary) {
    let date = parseDJSrvString(summary.date);
    return {
      '@CLASS': 'com.arcesium.treasury.lcmcore.dao.BrokerDataLcmFilter',
      date: date ? date.format('YYYY-MM-DD') : this.filters.selectedDate,
      cpeId: summary.cpeId,
      agreementTypeId: summary.agreementTypeId,
      legalEntityId: summary.legalEntityId,
      levelId: summary.levelId,
      fileAlias: summary.fileAlias,
      fileName: summary.fileName,
      fileId: summary.fileId
    };
  }

  getLcmPositionDataParam(filters) {
    return {
      '@CLASS': 'deshaw.treasury.common.model.comet.LcmPositionDataParam',
      date: filters.selectedDate,
      cpeIds: filters.selectedCpes.map(item => item.key),
      agreementTypeIds: filters.selectedAgreementTypes.map(item => item.key),
      legalEntityIds: filters.selectedLegalEntities.map(item => item.key)
    };
  }
}
