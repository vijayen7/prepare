import { ReactArcGrid as Grid } from 'arc-grid';
import { inject, observer } from "mobx-react";
import { Button, ComboBox, Layout } from 'arc-react-components';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Loader from 'commons/container/Loader';
import { gridColumns } from './grid-columns';
import LegalEntityFilter from '../commons/container/LegalEntityFilter';
import AgreementTypeFilter from '../commons/container/AgreementTypeFilter';
import CpeFilter from '../commons/container/CpeFilter';
import DateFilter from "../commons/filters/components/DateFilter";
import queryString from "query-string";
import { splitCommaSeparatedString } from './utils';

@inject('brokerDataStore')
@observer
export default class Render extends Component {
  static propTypes = {
    brokerDataStore: PropTypes.object.isRequired
  };

  store = this.props.brokerDataStore;

  constructor(props) {
    super(props);
    this.summaryColumns = gridColumns('summary');
    this.detailColumns = gridColumns('detail');
  }

  handleSearch = () => {
    this.store.searchSummary(this.store.filters);
  };

  handleResetFilters = () => {
    this.store.resetFilters();
  }

  handleCopySearch = () => {
    this.store.copySearchUrl(window.location.origin);
  }

  onSelect = (params) => {
    const { key, value } = params;
    const selectedFilters = { ...this.store.filters };
    selectedFilters[key] = value;
    this.store.setFilters(selectedFilters);
  };

  handleSelection = (selectedItem) => {
    this.store.drillDown(selectedItem);
  };

  gridOptions = {
    onRowClick: this.handleSelection,
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true
  };

  componentDidMount() {
    const values = queryString.parse(this.props.location.search);
    let filters = {
      selectedDate: values.dateString,
      selectedLegalEntities: splitCommaSeparatedString(values.legalEntityIds),
      selectedCpes: splitCommaSeparatedString(values.cpeIds),
      selectedAgreementTypes: splitCommaSeparatedString(values.agmtTypeIds),
    };
    if (
      (filters.selectedCpes && filters.selectedCpes.length) ||
      (filters.selectedLegalEntities && filters.selectedLegalEntities.length) ||
      (filters.selectedAgreementTypes && filters.selectedAgreementTypes.length) ||
      filters.selectedDate
    ) {
      this.props.brokerDataStore.setFilters(filters);
      this.handleSearch();
    }
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.detail && this.store.detail &&
      prevProps.detail.length === this.store.detail.length &&
      this.store.detail !== prevProps.detail
    ) {
      ArcGrid.grid.elementGridMap.cometBrokerDataDetail.resizeCanvas();
    }
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            ref={(header) => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          />
          <div className="padding--top">
            <Layout isColumnType>
              <Layout.Child
                size={1}
                title="Broker data search"
                collapsible
                resizable
                childId="child1"
              >
                <React.Fragment>
                  <div className="form padding--double container margin--left">
                    <div className="row no--padding">
                      <DateFilter
                        label="Date"
                        stateKey={"selectedDate"}
                        onChange={this.onSelect}
                        data={this.store.filters.selectedDate}
                      />
                    </div>
                    <div className="row no--padding">
                      <LegalEntityFilter
                        multiSelect
                        onSelect={this.onSelect}
                        selectedData={this.store.filters.selectedLegalEntities}
                      />
                    </div>
                    <div className="row no--padding">
                      <CpeFilter
                        multiSelect
                        onSelect={this.onSelect}
                        selectedData={this.store.filters.selectedCpes}
                      />
                    </div>
                    <div className="row no--padding">
                      <AgreementTypeFilter
                        multiSelect
                        onSelect={this.onSelect}
                        selectedData={this.store.filters.selectedAgreementTypes}
                      />
                    </div>
                    <div className="row no--padding">
                      <label>Dataset Source</label>
                      <ComboBox
                        options={['Broker Data / CPE']}
                        value="Broker Data / CPE"
                      />
                    </div>
                  </div>
                  <div className="form container margin--top">
                    <div className="row">
                      <Button className="button--tertiary" onClick={this.handleResetFilters}>Reset</Button>
                      <Button className="button--tertiary" onClick={this.handleCopySearch}>Copy Search URL</Button>
                      <Button onClick={this.handleSearch}>Search</Button>
                    </div>
                  </div>
                </React.Fragment>

              </Layout.Child>
              <Layout.Child size={3} className="padding--double" childId="child2">
                <React.Fragment>
                  <Layout>
                    <Layout.Child childId="child1">
                      {this.store.summary && (
                        <Layout>
                          <Layout.Child size="fit" childId="child1">
                            <h3>
                              Summary
                              </h3>
                          </Layout.Child>
                          <Layout.Child childId="child2">
                            <Grid
                              data={this.store.summary}
                              gridId="cometBrokerDataSummary"
                              columns={this.summaryColumns}
                              options={this.gridOptions}
                              containerHeight="89%"
                            />
                          </Layout.Child>
                        </Layout>
                      )}
                      {this.store.searchStatus.inProgress && 'Loading...'}
                      {!this.store.summary &&
                        !this.store.searchStatus.inProgress && (
                          <div className="message">Perform a search</div>
                        )}
                    </Layout.Child>
                    <Layout.Divider childId="divider"/>
                    <Layout.Child childId="child2">
                      {this.store.detail && (
                        <Layout>
                          <Layout.Child size="fit" childId="child1">
                            <h3 className="margin--top--double">
                              Details per agreement
                              </h3>
                          </Layout.Child>
                          <Layout.Child childId="child2">
                            <Grid
                              data={this.store.detail}
                              gridId="cometBrokerDataDetail"
                              columns={this.detailColumns}
                              options={this.gridOptions}
                              containerHeight="80%"
                            />
                          </Layout.Child>
                        </Layout>
                      )}
                      {this.store.drillDownStatus.inProgress && 'Loading...'}
                      {this.store.summary &&
                        !this.store.detail &&
                        !this.store.drillDownStatus.inProgress && (
                          <div className="message margin--top--double">
                            Select a row to see its details
                          </div>
                        )}
                    </Layout.Child>
                  </Layout>
                </React.Fragment>
              </Layout.Child>
            </Layout>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
