import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Render from './Render';
import { ReactLoader } from "../commons/components/ReactLoader";

@inject('brokerDataStore')
@observer
export default class BrokerData extends Component {
  static propTypes = {
    brokerDataStore: PropTypes.object.isRequired
  };

  store = this.props.brokerDataStore;

  render() {
    return (
      <>
        <ReactLoader
          inProgress={this.store.searchStatus.inProgress || this.store.drillDownStatus.inProgress}
        />
        <Render
          location={this.props.location}
        />
      </>
    );
  }
}
