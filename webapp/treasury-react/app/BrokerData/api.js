import { ArcFetch } from 'arc-commons';
import { BASE_URL } from "commons/constants";

const QUERY_ARGS = {
  credentials: 'include',
  method: 'GET'
};

export const getParsedDataSummary = (lcmPositionDataParam) => {
  const url = `${BASE_URL}/service/lcmBrokerDataService/getBrokerParsedDataSummary?lcmPositionDataParam=${encodeURIComponent(
    JSON.stringify(lcmPositionDataParam)
  )}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS).then((data) => {
    data.forEach((row, i) => {
      row.id = i;
    });
    return data;
  });
};

export const getParsedDataByFile = (brokerDataLcmFilter) => {
  const url = `${BASE_URL}/service/lcmBrokerDataService/getBrokerParsedDataByFile?brokerDataLcmFilter=${encodeURIComponent(
    JSON.stringify(brokerDataLcmFilter)
  )}&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS).then((data) => {
    data.forEach((row, i) => {
      row.id = i;
    });
    return data;
  });
};
