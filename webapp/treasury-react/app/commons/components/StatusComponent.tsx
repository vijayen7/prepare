import React from "react";
import { Dialog } from "arc-react-components";
import Message from "../../commons/components/Message";
import Status from "../../commons/models/Status";
import { SUCCESS, FAILURE } from "../constants";

interface Props {
  status: Status;
  isOpen: boolean;
  onClose: () => void;
  title?: string;
}

const StatusComponent: React.FC<Props> = ({ status, ...props }) => {
  return (
    <Dialog
      isOpen={props.isOpen}
      onClose={props.onClose}
      title={props.title ? props.title : "Status"}
      footer={
        <React.Fragment>
          <button onClick={props.onClose}>OK</button>
        </React.Fragment>
      }
    >
      <Message
        messageData={status.message}
        success={status.status == SUCCESS}
        error={status.status == FAILURE}
      />
    </Dialog>
  );
};

export default StatusComponent;
