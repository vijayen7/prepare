import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SliderComponent from 'commons/components/Slider';
import ToggleGroup from 'commons/components/ToggleGroupContainer';

export default class SliderPanel extends Component {
  constructor(props) {
    super(props);
    this.renderSlider = this.renderSlider.bind(this);
    this.renderSlider = this.renderSlider.bind(this);
    this.renderToggleGroup = this.renderToggleGroup.bind(this);
  }

  renderSlider() {
    return (<SliderComponent
      min={this.props.min}
      max={this.props.max}
      value={this.props.value}
      onChange={this.props.onSliderChange}
    />);
  }

  renderToggleGroup() {
    return (
      <div align='right'>
        <ToggleGroup
            options={this.props.toggleOptions}
            onChange={this.props.onToggleGroupChange}
            value={this.props.view}
          />
      </div>
    );
  }

  render() {
    return (
      <div className="container size--content padding--double">
        {this.renderSlider()}
        <div className="display--inline-block" style={{float: 'right'}}>
            {this.renderToggleGroup()}
        </div>
      </div>
    );
  }
}

SliderPanel.defaultProps = {
  min: 0,
  max: 20,
  value: 10,
  view: '',
  toggleOptions: []
};

SliderPanel.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  value: PropTypes.number,
  view: PropTypes.string,
  onSliderChange: PropTypes.func.isRequired,
  toggleOptions: PropTypes.array,
  onToggleGroupChange: PropTypes.func.isRequired
};
