import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  ComposedChart,
  Line,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from "recharts";

export default class BiaxialCompositeChart extends Component {
  constructor(props) {
    super(props);
  }

  getColorPaletteForBars() {
    return [
      "#a2d5f7",
      "#EEEFA8",
      "#7fbf7f",
      "#C5D86D",
      "#8AAA79",
      "#5B8266",
      "#AEF6C7"
    ];
  }

  getColorPaletteForLines() {
    return [
      "#4082AD",
      "#769A75",
      "#5B8266",
      "#AEF6C7",
      "#0D5C63",
      "#1F2F16",
      "#2F394D"
    ];
  }

  render() {
    return (
      <React.Fragment>
        <div className="border layout--flex--row size--content padding--horizontal--double">
          <div className="chart-container" align="center">
            <ResponsiveContainer height={600}>
              <ComposedChart
                data={this.props.data}
                barGap={0}
                margin={{ top: 50, right: 50, bottom: 20, left: 20 }}
              >
                <CartesianGrid
                  strokeDasharray="3 3"
                  strokeOpacity="0.4"
                  stroke="#8e8d84"
                />
                <XAxis
                  type="category"
                  dataKey="name"
                  tick={this.props.xAxis.tick}
                  label={{
                    value: this.props.xAxis.label,
                    fill: "#b9b9b9",
                    dy: 70
                  }}
                  interval={0}
                />
                <YAxis
                  type="number"
                  yAxisId="left"
                  orientation="left"
                  stroke="#85BDBF"
                  unit={this.props.yAxis.left.unit}
                  tickFormatter={this.props.yAxis.left.tickFormatter}
                  label={{
                    value: this.props.yAxis.left.label,
                    angle: -90,
                    fill: "#b9b9b9",
                    dx: -45
                  }}
                />

                <YAxis
                  type="number"
                  yAxisId="right"
                  orientation="right"
                  stroke="#20b2aa"
                  unit={this.props.yAxis.right.unit}
                  tickFormatter={this.props.yAxis.right.tickFormatter}
                  label={{
                    value: this.props.yAxis.right.label,
                    angle: 90,
                    fill: "#b9b9b9",
                    dx: 55
                  }}
                />

                <Tooltip content={this.props.tooltip.formatter} cursor={false} />
                <Legend wrapperStyle={this.props.legend.wrapperStyle} />
                {this.props.bars.map((bar, index) => (
                  <Bar
                    yAxisId={bar.yAxisId}
                    dataKey={bar.dataKey}
                    barSize={bar.size}
                    name={bar.name}
                    stackId={bar.stackId}
                    fill={bar.fill || this.getColorPaletteForBars()[index]}
                  />
                ))}
                {this.props.lines.map((line, index) => (
                  <Line
                    yAxisId={line.yAxisId}
                    type="monotone"
                    legendType="circle"
                    name={line.name}
                    dataKey={line.dataKey}
                    key={line.dataKey}
                    stroke={this.getColorPaletteForLines()[index]}
                    strokeWidth={0}
                    dot={{
                      stroke: this.getColorPaletteForLines()[index],
                      strokeWidth: 5.5
                    }}
                  />
                ))}
              </ComposedChart>
            </ResponsiveContainer>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

BiaxialCompositeChart.defaultProps = {
  bars: [],
  lines: [],
  legend: {},
  tooltip: {},
  xAxis: {
    tick: ""
  }
};

BiaxialCompositeChart.propTypes = {
  data: PropTypes.array.isRequired,
  bars: PropTypes.array,
  lines: PropTypes.array,
  xAxis: PropTypes.shape({
    tick: PropTypes.string,
    label: PropTypes.string
  }),
  yAxis: PropTypes.shape({
    left: PropTypes.shape({
      unit: PropTypes.string,
      label: PropTypes.string,
      tickFormatter: PropTypes.func
    }),
    right: PropTypes.shape({
      unit: PropTypes.string,
      label: PropTypes.string,
      tickFormatter: PropTypes.func
    })
  }),
  legend: PropTypes.shape({
    wrapperStyle: PropTypes.object
  }),
  tooltip: PropTypes.shape({
    formatter: PropTypes.object
  })
};
