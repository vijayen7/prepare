import React from "react";

const Warning = props => {
  return (
    <div className="message--warning" style={getStyle(props)}>
      <p>{props.messageData}</p>
    </div>
  );
};

function getStyle(props) {
  return props.width !== undefined
    ? { width: props.width + "px" }
    : { width: "300px" };
}

export default Warning;
