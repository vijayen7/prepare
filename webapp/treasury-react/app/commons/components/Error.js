import React from "react";

const Error = props => {
  return (
    <div className="message--critical">
      <p>{props.messageData}</p>
    </div>
  );
};

export default Error;
