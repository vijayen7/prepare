/*
    This component renders a timeseries line chart using the generic LineChart Component.
    The component expects getPlotData function as a prop, which returns data in the format
    [
        {dataKey: 'category1', constituent1: 4000, constituent2: 2400, constituent3: 2400},
        {dataKey: 'category2, constituent1: 3000, constituent1: 1398, constituent3: 2210}
    ]
    The component also expects props for xAxis and yAxis, specify labels and dataKey. The dataKey
    provided for the xAxis should always contain a epoch timestamp, so as to display the timeseries
    chart.
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  XAxis,
  YAxis,
  Legend,
  Tooltip,
  Brush,
  ResponsiveContainer
} from 'recharts';
import { convertTimestampToNycDateString, formatNumberAsMoneyWithoutPrefix } from 'commons/util';
import LineChart from './LineChart';

export default class DataTrendsChart extends Component {
  constructor(props) {
    super(props);
    this.getLines = this.getLines.bind(this);
    this.getXAxis = this.getXAxis.bind(this);
    this.getLeftYAxis = this.getLeftYAxis.bind(this);
    this.getRightYAxis = this.getRightYAxis.bind(this);
    this.getLegend = this.getLegend.bind(this);
    this.getTooltip = this.getTooltip.bind(this);
    this.getBrush = this.getBrush.bind(this);
    this.left = 'left';
    this.right = 'right';
  }

  getLines() {
    const addedConstituents = [];
    const lines = [];
    this.props.data.forEach((element) => {
      Object.keys(element).forEach((constituent) => {
        if (constituent !== 'name' && addedConstituents.indexOf(constituent) === -1) {
          addedConstituents.push(constituent);
          lines.push({
            name: constituent,
            dataKey: constituent,
            yAxisId: this.props.rightYAxisDataKeys.indexOf(constituent) === -1 ? this.left : this.right
          });
        }
      });
    });
    return lines;
  }

  getXAxis() {
    return (
      <XAxis
        dataKey={this.props.xAxis.dataKey}
        label={{ value: this.props.xAxis.label, fill: 'white', dy: 20 }}
        domain={['dataMin', 'dataMax']}
        tickFormatter={convertTimestampToNycDateString}
        type="number"
      />
    );
  }

  getLeftYAxis(lines) {
    const isVisible = lines.some((line) => line.yAxisId === this.left);

    if (!isVisible) {
      return null;
    }

    return (
      <YAxis
        type="number"
        yAxisId={this.left}
        orientation={this.left}
        stroke="#85BDBF"
        label={{ value: this.props.yAxis.left.label, angle: -90, fill: '#b9b9b9', dx: -45 }}
        tickFormatter={this.props.yAxis.left.tickFormatter || formatNumberAsMoneyWithoutPrefix}
        domain={["auto", "auto"]}
      />
    );
  }

  getRightYAxis(lines) {
    const isVisible = lines.some((line) => line.yAxisId === this.right);

    if (!isVisible) {
      return null;
    }

    return (
      <YAxis
        type="number"
        yAxisId={this.right}
        orientation={this.right}
        stroke="#20b2aa"
        label={{ value: this.props.yAxis.right.label, angle: 90, fill: '#b9b9b9', dx: 45 }}
        tickFormatter={this.props.yAxis.right.tickFormatter || formatNumberAsMoneyWithoutPrefix}
        domain={["auto", "auto"]}
      />
    );
  }

  getLegend() {
    return <Legend wrapperStyle={{ paddingTop: '50px' }} />;
  }

  getTooltip() {
    return (
      <Tooltip
        formatter={this.props.tooltip.formatter}
        labelFormatter={convertTimestampToNycDateString}
        cursor={false}
      />
    );
  }

  getBrush() {
    return (
      <Brush
        dataKey="name"
        tickFormatter={convertTimestampToNycDateString}
        height={30}
      />
    );
  }

  render() {
    const lines = this.getLines();
    return (
      <ResponsiveContainer height={600}>
        <LineChart
          data={this.props.data}
          lines={lines}
          xAxis={this.getXAxis()}
          leftYAxis={this.getLeftYAxis(lines)}
          rightYAxis={this.getRightYAxis(lines)}
          legend={this.getLegend()}
          tooltip={this.getTooltip()}
          brush={this.getBrush()}
        />
      </ResponsiveContainer>

    );
  }
}

DataTrendsChart.defaultProps = {
  tooltip: {
    formatter: formatNumberAsMoneyWithoutPrefix
  },
  rightYAxisDataKeys: []
};

DataTrendsChart.propTypes = {
  data: PropTypes.array,
  tooltip: PropTypes.shape({
    formatter: PropTypes.func
  }),
  xAxis: PropTypes.shape({
    dataKey: PropTypes.string,
    label: PropTypes.string
  }),
  yAxis: PropTypes.shape({
    left: {
      label: PropTypes.string,
      tickFormatter: PropTypes.func
    },
    right: PropTypes.shape({
      label: PropTypes.string,
      tickFormatter: PropTypes.func
    })
  }),
  rightYAxisDataKeys: PropTypes.arrayOf(PropTypes.string)
};
