import React from "react";

const FilterButton = props => {
  return (
    <button
      className={
        typeof props.className !== 'undefined' ? props.className
          : (props.reset ? 'button--tertiary size--content float--right margin--small'
            : (props.secondary ? 'button--secondary size--content float--right margin--small'
              : 'button--primary size--content float--right margin--small'))
      }
      disabled={props.disabled}
      onClick={props.onClick}
      style={props.style}
    >
      {props.label}
    </button>
  );
};

export default FilterButton;
