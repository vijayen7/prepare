import React, { Component } from "react";
import SpnFilterAutocomplete from "../components/SpnFilterAutocomplete";
import AdvancedSearchFilter from "../components/AdvancedSecurityFilter";
import Button from "commons/components/Button";
import { Layout } from "arc-react-components";
import Label from "commons/components/Label";

export default class GenericSecurityFilter extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }

  onSelect(params) {
    const { key, value } = params;
    let newSelectedData = Object.assign({}, this.props.selectedData);
    newSelectedData[key] = value;
    this.props.onSelect({ key: this.props.stateKey, value: newSelectedData });
  }

  handleToggle() {
    this.onSelect({
      key: "isAdvancedSearch",
      value: !this.props.selectedData.isAdvancedSearch
    });
  }

  render() {
    return (
      <Layout>
      <Layout.Child childId="child1">
        <Layout isColumnType={true}>
          <Layout.Child size={1} className="margin--top" childId="child1">
            <Label label="Security Search" />
          </Layout.Child>
          <Layout.Child size={2} childId="child2">
            <Button
              tertiary={true}
              label={
                this.props.selectedData.isAdvancedSearch
                  ? "Switch to General Search"
                  : "Switch to Advanced Search"
              }
              onClick={() => this.handleToggle()}
            />
          </Layout.Child>
        </Layout>
        {!this.props.selectedData.isAdvancedSearch && (
          <SpnFilterAutocomplete
            data={this.props.selectedData.selectedSpns}
            label="SPNs"
            onSelect={this.onSelect}
          />
        )}
        {this.props.selectedData.isAdvancedSearch && (
          <AdvancedSearchFilter
            onSelect={this.onSelect}
            selectedSecuritySearchType={
              this.props.selectedData.selectedSecuritySearchType || ""
            }
            selectedTextSearchType={
              this.props.selectedData.selectedTextSearchType || ""
            }
            securitySearchString={
              this.props.selectedData.securitySearchString || ""
            }
          />
        )}
      </Layout.Child>
      </Layout>
    );
  }
}
