import React, { Component } from "react";
import { ComboBox } from "arc-react-components";

export function getComboboxInputFilter(style) {
  return class ComboboxInputFilter extends Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.state = {
        value: [],
        options: [],
      };
    }

    componentDidUpdate(prevProps, prevState) {
      this.combobox.data = this.props.data;
      this.combobox.value = this.props.selectedData;
    }

    handleChange(e) {
      this.props.onSelect({ key: this.props.stateKey, value: e });
    }

    handleSubmit(draftValue) {
      if (this.props.readonly !== undefined && this.props.readonly) {
        return;
      }
      this.props.onSelect({
        key: this.props.stateKey,
        value: { key: null, value: draftValue },
      });
    }

    render() {
      return (
        <div className={style ? style : "margin"}>
          <label title={this.props.label}>{this.props.label}</label>
          <ComboBox
            options={this.props.data}
            value={this.props.selectedData}
            placeholder={this.props.placeholder ? this.props.placeholder : ""}
            onChange={this.handleChange}
            onSubmit={this.handleSubmit}
            class={this.props.isError ? "error" : ""}
          />
        </div>
      );
    }
  };
}
