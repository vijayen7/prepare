import React from "react";
import PropTypes from 'prop-types';
import _ from "lodash";
import Dialog from './Dialog';

const MessageDialog = props => {
  return (
    <Dialog
    isOpen={props.isOpen}
    onClose={props.onClose}
    style={{
      width: '920px'
    }}
    title={props.title}
  >
    <div style={{ width: '900px' }}>{props.content}</div>
  </Dialog>
  );
};

MessageDialog.propTypes = {
    isOpen: PropTypes.bool,
    onClose: PropTypes.func,
    content: PropTypes.element,
    title: PropTypes.String
  };

export default MessageDialog;
