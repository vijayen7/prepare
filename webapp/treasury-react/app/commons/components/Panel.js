import React from "react";
import { Panel as PanelComponent } from "arc-react-components";

export default class Panel extends React.Component {
  render() {
    return (
      <PanelComponent style={{ height: "auto" }}>
        {this.props.children}
      </PanelComponent>
    );
  }
}
