/*
    This component renders a Bar Chart using Recharts library (http://recharts.org/en-US/).
    If 'stacked' is set to  True, it renders Stacked Bar Chart, otherwise Simple Bar Chart.
    The component expects a callback function 'getPlotData' as a prop. This method must return
    graph data in the following format :
    [
        {name: 'category1', constituent1: 4000, constituent2: 2400, constituent3: 2400},
        {name: 'category2, constituent1: 3000, constituent1: 1398, constituent3: 2210}
    ]
    For more  information see the examples and JS fiddles of on Simple Bar Chart and
    Stacked Bar Chart on Recharts official website.
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  BarChart,
  Bar,
  CartesianGrid
} from 'recharts';

export default class BarChartComponent extends Component {
  constructor(props) {
    super(props);
    this.formatDataByConstituents = this.formatDataByConstituents.bind(this);
  }

  getColorPalette() {
    return ['#2E5266', '#1B998B', '#DC965A', '#FE819D', '#71807d', '#5db3ef',
      '#a2d5f7', '#8AAA79', '#5B8266', '#AEF6C7', '#0D5C63', '#1F2F16', '#2F394D'];
  }

  formatDataByConstituents() {
    let colorIdx = 0;
    const constituentList = [];
    const addedConstituents = [];
    const colors = this.getColorPalette();
    const plotData = this.props.getPlotData();

    for (let categoryIdx = 0; categoryIdx < plotData.length; categoryIdx++) {
      const category = plotData[categoryIdx];
      for (const constituent in category) {
        if (constituent === 'name' || addedConstituents.indexOf(constituent) > -1) continue;
        constituentList.push({
          dataKey: constituent,
          color: colors[colorIdx]
        });

        colorIdx = (colorIdx + 1) % (colors.length);
        addedConstituents.push(constituent);
      }
    }

    if (this.props.sortPlotData !== undefined) {
      return this.props.sortPlotData(constituentList, 'dataKey');
    }
    return constituentList;
  }

  render() {
    return (
      <React.Fragment>
        <div className="layout--flex--row size--content border">
          <div className="chart-container" align="center">
            <BarChart
              data={this.props.getPlotData()}
              stackOffset="sign"
              width={this.props.width}
              height={this.props.height}
              margin={
                { top: 5,
                  right: 30,
                  bottom: 5,
                  left: 30 }
              }
              barGap={-50}
            >
              <CartesianGrid strokeDasharray="3 3" strokeOpacity="0.4" stroke="#8e8d84" />
              {this.props.xAxis}
              {this.props.yAxis}
              {this.props.tooltip}
              {this.props.legend}
              {
                this.formatDataByConstituents().map((constituent) =>
                  (<Bar
                    maxBarSize={50}
                    dataKey={constituent.dataKey}
                    key={constituent.dataKey}
                    stackId={this.props.stacked ? 'a' : undefined}
                    fill={constituent.color}
                  />
                  )
                )
              }

            </BarChart>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

BarChartComponent.defaultProps = {
  stacked: false,
  width: 1400,
  height: 700
};

BarChartComponent.propTypes = {
  getPlotData: PropTypes.func.isRequired,
  sortPlotData: PropTypes.func,
  stacked: PropTypes.bool,
  xAxis: PropTypes.object,
  yAxis: PropTypes.object,
  tooltip: PropTypes.object,
  legend: PropTypes.object,
  width: PropTypes.number,
  height: PropTypes.number
};
