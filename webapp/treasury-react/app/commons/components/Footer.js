import React from "react";

const Footer = props => {
  return <div className="text-align--center padding--top--large">{props.content}</div>;
};

export default Footer;
