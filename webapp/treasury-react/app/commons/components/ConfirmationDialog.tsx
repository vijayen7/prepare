import React from "react";
import { Layout, Dialog } from "arc-react-components";
import Message from "./Message";

interface Props {
  title?: string;
  isOpen: boolean;
  onClose: () => void;
  onConfirm: () => void;
  message: string;
}

const ConfirmationDialog: React.FC<Props> = (props: Props) => {
  return (
    <Dialog
      title={props.title ? props.title : "Confirm"}
      isOpen={props.isOpen}
      onClose={props.onClose}
      footer={
        <Layout>
          <Layout.Child childId="buttons">
            <button onClick={props.onConfirm}>Yes</button>
            <button onClick={props.onClose}>Cancel</button>
          </Layout.Child>
        </Layout>
      }
    >
      <Message messageData={props.message} primary />
    </Dialog>
  );
};

export default ConfirmationDialog;
