import React, { Component } from "react";
export default class Combobox extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.combobox.addEventListener("optionSelected", this.handleChange);
    this.combobox.multiSelect = this.props.multiSelect;
  }

  componentDidUpdate(prevProps, prevState) {
    this.combobox.data = this.props.data;
    this.combobox.value = this.props.selectedData;
  }

  handleChange = e => {
    this.props.onSelect({ key: this.props.selectKey, value: e.detail.value });
  };

  componentWillUnmount() {
    this.combobox.removeEventListener("optionSelected", this.handleChange);
  }

  render() {
    return (
      <arc-combobox
          placeholder={this.props.placeholder}
        ref={combobox => {
          this.combobox = combobox;
        }}
        />
    );
  }
}