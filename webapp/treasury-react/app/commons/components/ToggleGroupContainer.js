import React from 'react';
import PropTypes from 'prop-types';
import { ToggleGroup } from 'arc-react-components';

const ToggleGroupContainer = (props) => (
  <React.Fragment>
    <ToggleGroup options={props.options} onChange={props.onChange} value={props.value} />
  </React.Fragment>
);

ToggleGroupContainer.defaultProps = {
  options: []
};

ToggleGroupContainer.propTypes = {
  options: PropTypes.array,
  value: PropTypes.string,
  onChange: PropTypes.func
};

export default ToggleGroupContainer;
