import React from 'react';
import PropTypes from 'prop-types';
import { Slider } from 'arc-react-components';

const SliderComponent = (props) => (
  <div className="display--inline-block">
    <Slider min={props.min} max={props.max} value={props.value} onChange={props.onChange} />
  </div>
);

SliderComponent.defaultProps = {
  min: 1,
  max: 10,
  value: 5
};

SliderComponent.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  value: PropTypes.number,
  onChange: PropTypes.func
};

export default SliderComponent;
