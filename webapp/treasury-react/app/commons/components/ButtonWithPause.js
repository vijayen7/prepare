import React, { useState } from 'react';
import PropTypes from 'prop-types';

const ButtonWithPause = props => {
  const [executing, setExecuting] = useState(false);

  const onRealClick = async (event) => {
    setExecuting(true);
    try {
      await props.onClick();
      await new Promise(r => setTimeout(r, 500));
    } finally {
        setExecuting(false);
    }
  };

  return (
    <button
      id={props.id}
      className={props.className}
      hidden={props.hidden}
      disabled={executing || props.disabled}
      style={props.style}
      onClick={onRealClick}
    >
      {props.data}
    </button>
  );
};

ButtonWithPause.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  hidden: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.object,
  data: PropTypes.object.isRequired
 };

export default ButtonWithPause;
