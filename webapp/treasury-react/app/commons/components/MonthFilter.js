import React, { Component } from "react";
import "jquery-ui-monthpicker/jquery.ui.monthpicker.js";

export default class MonthFilter extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (this.refs.monthpicker.classList.contains("hasMonthpicker")) return;
    $(this.refs.monthpicker).monthpicker({
      changeYear: true,
      buttonText: "<i class='icon-calendar' />",
      showOn: "both",
      onSelect: value => this.handleChange(value)
    });
    $(this.refs.monthpicker).monthpicker("setDate", this.props.data);
  }

  componentWillReceiveProps(nextProps) {
      $(this.refs.monthpicker).monthpicker("setDate", nextProps.data);
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.stateKey,
      value: e
    };
    this.props.onSelect(returnValue);
  };

  render() {
    return (
      <div className="form-field--split">
        <label title={this.props.label}>{this.props.label}</label>
        <div className="layout--flex">
          <input className="size--9" type="text" ref="monthpicker" />
        </div>
      </div>
    );
  }
}
