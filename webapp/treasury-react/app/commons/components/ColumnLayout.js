import React from "react";

export default class ColumnLayout extends React.Component {
  render() {
    return (
      <div className="layout--flex">
        <div className="text-align--right margin--horizontal--double ">{this.props.children}</div>
      </div>
    );
  }
}
