import React from "react";

export default class ErrorDismissible extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return this.props.messageData !== "" ? (
      <div className="message--critical no-icon size--grow padding--vertical">
        {this.props.messageData}
      </div>
    ) : null;
  }
}
