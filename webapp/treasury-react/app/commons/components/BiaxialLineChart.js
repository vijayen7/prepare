import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { LineChart, Line, YAxis, CartesianGrid } from 'recharts';

export default class BiaxialLineChart extends Component {
  getColorPalette() {
    return ['#DC965A', '#DDD92A', '#71807d', '#EEEFA8', '#C5D86D', '#8AAA79', '#5B8266', '#AEF6C7', '#0D5C63', '#1F2F16', '#2F394D'];
  }

  render() {
    return (
      <React.Fragment>
        <div className="border layout--flex--row size--content">
          <div className="chart-container" align="center">
            <LineChart
              width={1400}
              height={600}
              data={this.props.data}
              margin={{ top: 50, right: 20, bottom: 20, left: 20 }}
            >
              <CartesianGrid strokeDasharray="3 3" strokeOpacity="0.4" stroke="#8e8d84" />
              {this.props.xAxis}
              <YAxis
                type="number"
                yAxisId="left"
                orientation="left"
                stroke="#85BDBF"
                label={{ value: this.props.yAxis.left.label, angle: -90, fill: '#b9b9b9', dx: -35 }}
                tickFormatter={this.props.yAxis.left.tickFormatter}
              />

              <YAxis
                type="number"
                yAxisId="right"
                orientation="right"
                stroke="#F1F0CC"
                label={{ value: this.props.yAxis.right.label, angle: 90, fill: '#b9b9b9', dx: 35 }}
                tickFormatter={this.props.yAxis.right.tickFormatter}
              />

              {this.props.tooltip}
              {this.props.legend}
              {
                this.props.lines.map((line, index) =>
                  (<Line
                    yAxisId={line.yAxisId}
                    type="monotone"
                    legendType="circle"
                    name={line.name}
                    dataKey={line.dataKey}
                    key={line.dataKey}
                    stroke={this.getColorPalette()[index]}
                    connectNulls={this.props.connectNulls}
                    strokeWidth={2}
                    dot={this.props.dot}
                  />)
                )
              }
              {this.props.brush}
            </LineChart>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

BiaxialLineChart.defaultProps = {
  connectNulls: true,
  dot: false
};

BiaxialLineChart.propTypes = {
  data: PropTypes.array.isRequired,
  lines: PropTypes.array.isRequired,
  xAxis: PropTypes.object,
  yAxis: PropTypes.shape({
    left: PropTypes.shape({
      label: PropTypes.string,
      tickFormatter: PropTypes.func
    }),
    right: PropTypes.shape({
      label: PropTypes.string,
      tickFormatter: PropTypes.func
    })
  }),
  legend: PropTypes.object,
  tooltip: PropTypes.object,
  dot: PropTypes.object,
  brush: PropTypes.object,
  connectNulls: PropTypes.bool
};
