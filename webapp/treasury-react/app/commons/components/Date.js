import React, { Component } from "react";

export default class Date extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (this.refs.datepicker.classList.contains("hasDatePicker")) return;
    $(this.refs.datepicker)
      .datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: value => this.handleChange(value)
      })
      .datepicker("setDate", this.props.data);
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.selectKey,
      value: e
    };
    this.props.onSelect(returnValue);
  };

  render() {
    return <input type="text" ref="datepicker" />;
  }
}
