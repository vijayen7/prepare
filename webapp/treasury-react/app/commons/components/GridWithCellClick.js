import { ReactArcGrid } from "arc-grid";
import React, { Component } from "react";
import _ from "lodash";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderLabel = this.renderLabel.bind(this);
    this.renderGrid = this.renderGrid.bind(this);
    this.getStyle = this.getStyle.bind(this);
  }

  renderLabel(props) {
    if (props.label !== undefined) {
      if (props.labelStyle !== undefined && props.labelStyle === "bold") {
        return (
          <div className="text-align--center margin--vertical--small">
            <h2>{props.label}</h2>
          </div>
        );
      } else {
        return <div className="text-align--center margin--vertical--small">{props.label}</div>;
      }
    } else {
      return null;
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.resizeCanvas != nextProps.resizeCanvas) this.forceResizeCanvas();
    if (_.isEqual(this.props.data, nextProps.data)) {
      return false;
    } else {
      return true;
    }
  }

  gridUpdateCallback = () => {
    return false;
  };

  renderGrid(props) {
    if (props.data !== undefined && props.data.length > 0) {
      return (
        <ReactArcGrid
          gridId={props.gridId}
          data={props.data}
          columns={props.gridColumns}
          options={props.gridOptions}
          shouldGridForceUpdate={this.gridUpdateCallback}
          onCellClick={e => props.onCellClick(e)}
          onDblClick={e => props.onDblClick(e)}
          forceResizeCanvas={forceResizeCanvas => {
            this.forceResizeCanvas = forceResizeCanvas;
          }}
        />
      );
    }
    return null;
  }

  getStyle() {
    if (this.props.fill !== undefined && this.props.fill) {
      return { height: "100%" };
    } else {
      return this.props.heightValue === undefined
        ? { height: 400 + "px" }
        : { height: this.props.heightValue + "px" };
    }
  }

  render() {
    var style = this.getStyle();
    return (
      <div style={style}>
        {this.renderLabel(this.props)}
        {this.renderGrid(this.props)}
      </div>
    );
  }
}

export default Grid;
