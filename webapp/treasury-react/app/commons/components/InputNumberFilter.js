import React, { Component } from "react";
import PropTypes from 'prop-types';
export default class InputNumberFilter extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.refs.date;
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.stateKey,
      value: e.target.value
    };
    this.props.onSelect(returnValue);
  };

  loadLabel(isStandard, label) {
    if (isStandard) {
      return (<div><label title={label}>{label}</label><br /></div>);
    }
    return (<label title={label}>{label}</label>);
  }

  render() {
    const style = (this.props.isStandard) ? "size--content" : "form-field--split";
    return (
      <div className={style}>
        {this.loadLabel(this.props.isStandard, this.props.label)}
        <input
          type="number"
          className={this.props.isError ? "error" : ""}
          value={this.props.data}
          onChange={this.handleChange}
          style={this.props.style}
          disabled={this.props.readonly}
        />
      </div>
    );
  }
}

InputNumberFilter.propTypes = {
  stateKey: PropTypes.string,
  isStandard: PropTypes.bool,
  label: PropTypes.string,
  isError: PropTypes.bool,
  readonly: PropTypes.bool,
};
