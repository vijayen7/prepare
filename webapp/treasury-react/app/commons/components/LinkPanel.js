import React from 'react';
import PropTypes from 'prop-types';

const LinkPanel = (props) => (
  <div className="padding--double container size--content">
    {React.Children.toArray(props.links)}
  </div>
);

LinkPanel.defaultProps = {
  links: []
};

LinkPanel.propTypes = {
  links: PropTypes.array
};

export default LinkPanel;
