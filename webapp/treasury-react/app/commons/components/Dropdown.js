import React, { Component } from "react";
export default class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.setData = this.setData.bind(this);
  }

  componentDidMount() {
    this.select.addEventListener("change", this.handleChange);
    this.setData();
  }

  setData() {
    $(this.select).empty();
    for (var field in this.props.data) {
      $(
        '<option value="' +
          this.props.data[field] +
          '">' +
          this.props.data[field] +
          "</option>"
      ).appendTo(this.select);
    }
    this.select.value = this.props.selectedData;
  }

  componentDidUpdate(prevProps, prevState) {
    this.select.removeEventListener("change", this.handleChange);
    this.setData();
    this.select.addEventListener("change", this.handleChange);
  }

  handleChange = e => {
    this.props.optionSelected(this.select.value);
  };

  componentWillUnmount() {
    this.select.removeEventListener("change", this.handleChange);
  }

  render() {
    return (
      <div>
        <select
          ref={select => {
            this.select = select;
          }}
        />
      </div>
    );
  }
}
