import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Dialog as ArcDialog } from "arc-react-components";

class Dialog extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ArcDialog
        isOpen={this.props.isOpen}
        title={this.props.title}
        onClose={this.props.onClose}
        style={this.props.style}
        footer={this.props.footer}
      >
        {this.props.children}
      </ArcDialog>
    );
  }
}

Dialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string,
  onClose: PropTypes.func,
  style: PropTypes.object
};

export default Dialog;
