import React, { Component } from "react";
import PropTypes from 'prop-types';

export default class Input extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.stateKey,
      value: e.target.value
    };
    this.props.onSelect(returnValue);
  };

  loadLabel(isStandard, label) {
    if (isStandard) {
      return (<div><label title={label}>{label}</label><br /></div>);
    }
    return (<label title={label}>{label}</label>);
  }

  render() {
    const style = (this.props.isStandard) ? "size--content"  : "form-field--split";
    const size = (this.props.inputSize !== undefined) ? this.props.inputSize  : ""
    return (
      <div className={style}>
        {this.loadLabel(this.props.isStandard, this.props.label)}
        <input
          type="text"
          size={size}
          className = {this.props.isError ? "error" : ""}
          value={this.props.data}
          disabled={this.props.disabled ? this.props.disabled : false}
          onChange={this.handleChange}
          title={this.props.title}
        />
      </div>
    );
  }
}

Input.propTypes = {
  stateKey: PropTypes.string,
  isStandard: PropTypes.bool,
  label: PropTypes.string,
  isError: PropTypes.bool,
  disabled: PropTypes.bool,
  title: PropTypes.string
};
