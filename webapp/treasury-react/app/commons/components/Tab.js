import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Tab extends Component {
  render() {
    const classList = 'size--content margin--horizontal padding blue--border';
    return (
      <a
        className={classList + (this.props.isSelected ? ' active' : '')}
        id={this.props.id}
        onClick={(e) => { this.props.onClick(e); }}
      >
        {this.props.label}
      </a>
    );
  }
}

Tab.defaultProps = {
  isSelected: false
};

Tab.propTypes = {
  id: PropTypes.string.isRequired,
  isSelected: PropTypes.bool,
  label: PropTypes.string,
  onClick: PropTypes.func
};
