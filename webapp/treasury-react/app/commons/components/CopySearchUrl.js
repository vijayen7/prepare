import React, { Component } from "react";
import FilterButton from "commons/components/FilterButton";
import copy from 'clipboard-copy';
import { ToastService } from 'arc-react-components';

export default class CopySearchUrl extends Component {
  constructor(props) {
    super(props);
  }

  copySearchURL = () => {
    let path = window.location.origin+window.location.pathname + '?' + encodeURIComponent(JSON.stringify(this.props.copySearchParams));
    copy(path);
    ToastService.append({
      content: "Search URL copied to clipboard",
      type: "info",
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 1000,
    });
  }

  render() {
    return (
      <FilterButton
        reset={true}
        onClick={this.copySearchURL}
        label="Copy Search URL"
        className="float--left button--tertiary margin--small"
      />
    );
  }
}
