import { Loader } from "arc-react-components";
import React from "react";

export const ReactLoader = props => {
  return props.inProgress ? (
    <Loader hasOverlay={true} title="Loading.." delay={500}></Loader>
  ) : (
    <React.Fragment />
  );
};
