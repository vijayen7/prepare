import React, { Component } from "react";

export default class Checkbox extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.selectKey,
      value: e.target.checked
    };
    this.props.onSelect(returnValue);
  };

  render() {
    return (
      <input
        type="checkbox"
        defaultChecked={this.props.defaultChecked}
        onChange={this.handleChange}
      />
    );
  }
}
