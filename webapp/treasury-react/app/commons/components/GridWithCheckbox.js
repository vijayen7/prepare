import { ReactArcGrid } from "arc-grid";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import _ from "lodash";
import PropTypes from 'prop-types';

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderLabel = this.renderLabel.bind(this);
    this.renderGrid = this.renderGrid.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getSelectedRowDataForIds = this.getSelectedRowDataForIds.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.gridUpdateCallback = this.gridUpdateCallback.bind(this);

  }

  componentDidMount() {
    if (this.props.preSelectAllRows) {
      this.selectAllRows();
    }
  }

  componentDidUpdate() {
    if (this.props.preSelectAllRows) {
      this.selectAllRows();
    }
    if (this.props.deselectAllSelectedRows) {
      this.deselectAllSelectedRows();
    }
  }

  deselectAllSelectedRows() {
    if (!(this.props.data !== undefined && this.props.data.length > 0)) {
      return;
    }
    let gridRef = this.getArcGridObject();
    let idList = [];
    gridRef.setSelectedRowIds(idList);
  }

  selectAllRows() {
    if (!(this.props.data !== undefined && this.props.data.length > 0)) {
      return;
    }
    var gridRef = this.getArcGridObject();
    var idList = [];
    this.props.data.forEach(
        (o, i, arr) => idList.push(arr[i].id)
    );
    gridRef.setSelectedRowIds(idList);
    const selectedRowDataKey = typeof this.props.selectedRowDataKey !== 'undefined' ? this.props.selectedRowDataKey : 'selectedRowData';
    this.props.onSelect({ key: selectedRowDataKey, value: this.getArcGridObject().data });
  }

  handleChange = e => {
    const selectedRowDataKey = typeof this.props.selectedRowDataKey !== 'undefined' ? this.props.selectedRowDataKey : 'selectedRowData';
    this.props.onSelect({ key: selectedRowDataKey, value: this.getSelectedRowDataForIds(this.getArcGridObject(),e) });
  };

    getSelectedRowDataForIds(gridRef, selectedRowIds) {
    var selectedData = [];
    if(typeof selectedRowIds !== undefined)
    {
      selectedRowIds.forEach(
        (o, i, arr) => selectedData.push(gridRef.dataView.getItemById(arr[i]))
      );
      return selectedData;
    }
    return null;
  }

  renderLabel(props) {
    return props.label !== undefined ? (
      <div className="text-align--center margin--vertical--small">
        {props.label}
      </div>
    ) : null;
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.resizeCanvas != nextProps.resizeCanvas)
      this.forceResizeCanvas();
    if (_.isEqual(this.props.data, nextProps.data)) {
      return false;
    } else {
      return true;
    }
  }

  gridUpdateCallback = () => {
    return false;
  };

  renderGrid(props) {
    if (props.data !== undefined && props.data.length > 0) {
      return (
        <ReactArcGrid
          gridId={props.gridId}
          data={props.data}
          columns={props.gridColumns}
          options={props.gridOptions}
          shouldGridForceUpdate={this.gridUpdateCallback}
          onCellClick={e => props.onCellClick(e)}
          onDblClick={e => props.onDblClick(e)}
          onSelectedRowsChanged={this.handleChange}
          getGridReference={getArcGridObject => {
            this.getArcGridObject = getArcGridObject;
          }}
        />
      );
    }
    return null;
  }

  getStyle() {
    if (this.props.heightValue === -1) {
      return { height: "auto" };
    }

    return this.props.heightValue === undefined
      ? { height: 400 + "px" }
      : { height: this.props.heightValue + "px" };
  }

  render() {
    var style = this.getStyle();
    return (
      <div style={style}>
        {this.renderLabel(this.props)}
        {this.renderGrid(this.props)}
      </div>
    );
  }
}

Grid.propTypes = {
  data: PropTypes.array.isRequired,
  gridId: PropTypes.string,
  gridColumns: PropTypes.array.isRequired,
  gridOptions: PropTypes.object.isRequired,
  heightValue: PropTypes.number,
  resizeCanvas: PropTypes.func,
  onSelect: PropTypes.func,
  preSelectAllRows: PropTypes.bool,
  deselectAllSelectedRows: PropTypes.bool
};

export default Grid;
