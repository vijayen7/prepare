import React, { Component } from "react";
import { Resizer as ArcResizer } from "arc-react-components";

export default class Resizer extends Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <ArcResizer>
            {this.props.children}
        </ArcResizer>
      );
    }
  }
  