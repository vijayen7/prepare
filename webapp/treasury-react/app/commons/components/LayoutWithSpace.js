import { Layout } from "arc-react-components";
import PropTypes from "prop-types";
import React from "react";
const LayoutWithSpace = props => {
  const objects = React.Children.toArray(props.children);
  return (
    <Layout>
      {objects.map((object, i) => (
        <Layout.Child
          key={i}
          className="margin--left--double margin--bottom margin--right--double"
          childId="child1"
        >
          {object}
        </Layout.Child>
      ))}
    </Layout>
  );
};

LayoutWithSpace.propTypes = {
  children: PropTypes.any
};
export default LayoutWithSpace;
