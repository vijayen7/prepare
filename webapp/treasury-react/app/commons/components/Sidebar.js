import React from "react";
import { Sidebar as SideBar } from "arc-react-components";


export default class Sidebar extends React.Component {
  static defaultProps = {
    header: "Filter",
    collapsible: false,
    toggleSidebar: false,
    resizeCanvas: () => {}
  };

  constructor(props) {
    super(props);
    this.state = { collapsed: false };
    this.triggerKeyboardEvent = this.triggerKeyboardEvent.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.toggleSidebar == nextProps.toggleSidebar) {
      this.handleToggle();
    }
  }
  componentDidMount() {
      document.body.addEventListener("keyup",this.triggerKeyboardEvent);
  }

  componentWillUnmount() {
    document.body.removeEventListener("keyup",this.triggerKeyboardEvent);
  }

  triggerKeyboardEvent(event) {
    let element = document.getElementById(this.props.id);
    let activeElement = document.activeElement;
    if(element && element.contains(activeElement)){
      switch(event.keyCode){
        case $.ui.keyCode.ENTER:
          if(typeof this.props.enterFunction == "function"){
            // To hide datapicker when enter key pressed.
            if(element.querySelectorAll('.hasDatepicker').length > 0){
              $(element.querySelectorAll('.hasDatepicker')).datepicker('hide');
            }
            this.props.enterFunction();
           }
           break;
         case $.ui.keyCode.ESCAPE:
          if(element.querySelector('.icon-caret--up')){
            element.querySelector('.icon-caret--up').click();
          }
          break;
          default:
            break;
      }
    }
  }
  handleToggle = state => {
    let collapsed = !this.state.collapsed;
    this.setState({ collapsed: collapsed });
    this.props.resizeCanvas();
    if(typeof this.props.onToggle === "function"){
      this.props.onToggle(state);
    }
  };

  render() {
    var filters = React.Children.toArray(this.props.children);
    return (
      <SideBar
        title={this.props.header}
        collapsible={this.props.collapsible}
        footer={filters[filters.length - 1]}
        onToggle={this.handleToggle}
        collapsed={this.state.collapsed}
        id={this.props.id}
      >
        {filters.slice(0, -1)}
      </SideBar>
    );
  }
}
