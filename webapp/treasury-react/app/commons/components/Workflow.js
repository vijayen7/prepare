import React, { Component } from "react";
import ResumeWorkflow from "commons/container/ResumeWorkflow";
import BeginWorkflow from "commons/container/BeginWorkflow";
import FailedWorkflow from "commons/container/FailedWorkflow";
import LockedWorkflow from "commons/container/LockedWorkflow";

export default class Workflow extends Component {
  constructor(props) {
    super(props);
  }

  handleBeginWorkflowCallback = (workflowStatus, workflowId) => {
    this.props.onbeginWorkflow(workflowStatus, workflowId);
  }

  render() {
    return (
      <React.Fragment>
        <BeginWorkflow url={this.props.url} onBeginWorkflow={this.handleBeginWorkflowCallback} />
        <ResumeWorkflow showResume={this.props.showResume} />
        <FailedWorkflow />
        <LockedWorkflow />
      </React.Fragment>
    );
  }
}
