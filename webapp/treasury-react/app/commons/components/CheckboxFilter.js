import React, { Component } from "react";

export default class CheckboxFilter extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.stateKey,
      value: e.target.checked
    };
    this.props.onSelect(returnValue);
  };

  render() {
    if (
      typeof this.props.style !== "undefined" &&
      this.props.style === "left"
    ) {
      return (
        <div>
          <input
            type="checkbox"
            checked={this.props.defaultChecked}
            onChange={this.handleChange}
            disabled={
              this.props.disabled !== undefined && this.props.disabled
                ? true
                : false
            }
          />
          <label className="margin--left"
          title={this.props.onHoverDisplay !== undefined ? this.props.onHoverDisplay :this.props.label}>
            {this.props.label}
          </label>
        </div>
      );
    }
    return (
      <div>
        <label className="margin--right"
         title={this.props.onHoverDisplay !== undefined ? this.props.onHoverDisplay :this.props.label}>
          {this.props.label}
        </label>
        <input
          type="checkbox"
          checked={this.props.defaultChecked}
          onChange={this.handleChange}
          disabled={
            this.props.disabled !== undefined && this.props.disabled
              ? true
              : false
          }
        />
      </div>
    );
  }
}
