import React, { Component } from "react";
import { BASE_URL } from "commons/constants";

const SPN_DELIMITER = ";";

export default class SpnFilterAutocomplete extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.loadSecurityData = this.loadSecurityData.bind(this);
  }

  componentDidMount() {
    this.refs.date;
  }

  handleChange = e => {
    this.loadSecurityData(e.target.value);
    this.props.onSelect({ key: "selectedSpns", value: this.refs.autocomplete.value });
  };

  onAutocompleteSelect(ui) {
    var types = ui.item.label.split("~");
    this.refs.autocomplete.value =
      this.refs.autocomplete.value.substring(0, this.refs.autocomplete.value.lastIndexOf(SPN_DELIMITER) + 1) +
      types[0] +
      SPN_DELIMITER;
    this.props.onSelect({ key: "selectedSpns", value: this.refs.autocomplete.value });
    return false;
  }

  loadSecurityData(searchString) {
    searchString = searchString.substring(searchString.lastIndexOf(SPN_DELIMITER) + 1);
    $(this.refs.autocomplete)
      .autocomplete({
        source: function(request, response) {
          if (searchString && searchString.trim() !== "") {
            var self = $(this);
            var url = `${BASE_URL}service/securityAutocompleteService/getAutocompleteSecurityList?search=${searchString}&format=JSON`;
            $.ajax({
              url: url,
              type: "GET",
              dataType: "text",
              success: function(data) {
                data = data.slice(1, -1);
                var dataArray = data.split("^");
                response(
                  $.map(dataArray, function(item) {
                    if (item.trim()) {
                      return item.trim();
                    } else {
                      self[0].element.removeClass("sec_loading");
                    }
                  })
                );
              }
            });
          }
        },
        search: function() {
          if (searchString && searchString.trim() !== "") {
            $(this).addClass("sec_loading");
          }
        },
        open: function() {
          $(this)
            .addClass("sec_input")
            .removeClass("sec_loading");
          $('ul').css("z-index", "9999");
        },
        close: function() {
          $(this)
            .removeClass("sec_input")
            .removeClass("sec_loading");
        },
        focus: function(event, ui) {
          event.preventDefault();
          // or return false;
        },
        minLength: 3,
        select: (e, ui) => this.onAutocompleteSelect(ui)
      })
      .data("ui-autocomplete")._renderItem = function(ul, item) {
        var types = item.label.split("~");
        var newLabel =
          "<div>" +
          "<div class='aln-lft aln-top ac-spn ac-b'>" +
          types[0].trim() +
          "</div>" +
          "<div class='aln-lft aln-top ac-name ac-b'>" +
          types[1] +
          "</div>" +
          "<div class='aln-lft aln-top ac-identifier ac-b'>" +
          types[2] +
          "</div>" +
          "<div class='aln-lft aln-top ac-type'>" +
          types[3].trim() +
          "</div>" +
          "</div>";

        var term = "";
        if (this.term.indexOf(":") !== -1) {
          var terms = this.term.split(":");
          term = terms[1];
        }
        item.value = types[1];
        newLabel = newLabel.replace(
          new RegExp(
            "(?![^&;]+;)(?!<[^<>]*)(" +
              $.ui.autocomplete.escapeRegex(term) +
              ")(?![^<>]*>)(?![^&;]+;)",
            "gi"
          ),
          "<strong>$1</strong>"
        );
        return $("<li></li>")
          .data("item.autocomplete", item)
          .append(newLabel)
          .appendTo(ul);
      };
  }

  loadLabel(style) {
    if(style === "standard") {
      return (<div><label title={this.props.label}> {this.props.label} </label><br/></div>);
    }
    else {
      return (<label title={this.props.label}> {this.props.label} </label>);
    }
  }

  render() {
    var style = (this.props.layout === "standard") ? "size--content" : "form-field--split";
    return (
      <div className={style}>
        {this.loadLabel(this.props.layout)}{" "}
        <input
          ref="autocomplete"
          type="text"
          disabled={this.props.readonly}
          value={this.props.data}
          onKeyDown={this.handleChange}
          onMouseDown={this.handleChange}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}
