import React from 'react';
import PropTypes from 'prop-types';


const TabPanel = (props) => (
  <div className="application-menu-toggle-view size--content margin--bottom--large">
    {React.Children.toArray(props.tabs)}
  </div>
);

TabPanel.defaultProps = {
  tabs: []
};

TabPanel.propTypes = {
  tabs: PropTypes.array
};

export default TabPanel;
