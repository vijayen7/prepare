import { ReactArcGrid } from "arc-grid";
import React, { Component } from "react";
import _ from "lodash";

class GridWithCellChange extends Component {
  constructor(props) {
    super(props);
    this.renderLabel = this.renderLabel.bind(this);
    this.renderGrid = this.renderGrid.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.gridUpdateCallback = this.gridUpdateCallback.bind(this);
    this.getSelectedRowDataForIds = this.getSelectedRowDataForIds.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  static defaultProps = {
    onCellClick: () => null,
    onDblClick: () => null,
    onCellChange: () => null,
    onBeforeEditCell: () => null
  };

  renderLabel(props) {
    if (props.label !== undefined) {
      if (props.labelStyle !== undefined && props.labelStyle === "bold") {
        return (
          <div className="text-align--center margin--vertical--small">
            <h2>{props.label}</h2>
          </div>
        );
      } else {
        return (
          <div className="text-align--center margin--vertical--small">
            {props.label}
          </div>
        );
      }
    } else {
      return null;
    }
  }

  getStyle() {
    if (this.props.fill !== undefined && this.props.fill) {
      return { height: "100%" };
    } else {
      return this.props.heightValue === undefined
        ? { height: 400 + "px" }
        : { height: this.props.heightValue + "px" };
    }
  }

  getSelectedRowDataForIds(gridRef, selectedRowIds) {
    var selectedData = [];
    if (typeof selectedRowIds !== undefined) {
      selectedRowIds.forEach(
        (o, i, arr) => selectedData.push(gridRef.dataView.getItemById(arr[i]))
      );
      return selectedData;
    }
    return null;
  }

  handleChange = e => {
    const selectedRowDataKey = typeof this.props.selectedRowDataKey !== 'undefined' ? this.props.selectedRowDataKey : 'selectedRowData';
    this.props.onSelect({ key: selectedRowDataKey, value: this.getSelectedRowDataForIds(this.getArcGridObject(), e) });
  };


  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.resizeCanvas != nextProps.resizeCanvas)
      this.forceResizeCanvas();
    if (_.isEqual(this.props.data, nextProps.data)) {
      return false;
    } else {
      return true;
    }
  }

  gridUpdateCallback = () => {
    return false;
  };

  onCellClick = e => {
    this.props.onCellClick(e);
  };

  onDblClick = e => {
    this.props.onDblClick(e);
  };

  onCellChange = e => {
    const gridRef = this.getArcGridObject();
    gridRef.refreshGrid([]);
    this.props.onCellChange(e);
  };

  onBeforeEditCell = (e, args) => {
    return this.props.onBeforeEditCell(e, args);
  };

  renderGrid(props) {
    if (props.data !== undefined && props.data.length > 0) {
      return (
        <ReactArcGrid
          gridId={props.gridId}
          data={props.data}
          columns={props.gridColumns}
          options={props.gridOptions}
          shouldGridForceUpdate={this.gridUpdateCallback}
          onCellClick={this.onCellClick}
          onDblClick={this.onDblClick}
          onCellChange={this.onCellChange}
          onBeforeEditCell={this.onBeforeEditCell}
          onSelectedRowsChanged={this.handleChange}
          forceResizeCanvas={forceResizeCanvas => {
            this.forceResizeCanvas = forceResizeCanvas;
          }}
          getGridReference={getArcGridObject => {
            this.getArcGridObject = getArcGridObject;
          }}
        />
      );
    }
    return null;
  }

  render() {
    var style = this.getStyle();
    return (
      <div style={style}>
        {this.renderLabel(this.props)}
        {this.renderGrid(this.props)}
      </div>
    );
  }
}

export default GridWithCellChange;
