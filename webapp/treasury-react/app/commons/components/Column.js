import React from "react";

export default class Column extends React.Component {
  render() {
    return (
      <div className="layout--flex margin--vertical">{this.props.children}
      </div>
    );
  }
}
