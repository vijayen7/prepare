/*
    This component renders a Line Chart using Recharts library (http://recharts.org/en-US/).
    It renders the X-Axis, Y-Axis, legend, tooltip and brush passed to it as params.
    The component expects a callback function 'getPlotData' as a prop. This method must return
    graph data in the following format :
    [
        {name: 'category1', constituent1: 4000, constituent2: 2400, constituent3: 2400},
        {name: 'category2, constituent1: 3000, constituent1: 1398, constituent3: 2210}
    ]
    For more  information see the examples and JS fiddles of on Simple Line Chart
    on Recharts official website.
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { LineChart, Line, CartesianGrid } from 'recharts';
import {COLORS} from 'arc-commons';

export default class LineChartComponent extends Component {
  getColor(index) {
    const light = COLORS.light;
    const dark = COLORS.dark;
    const colors = [dark["blue"], dark["green"], dark["orange"], dark["red"], dark["yellow"], dark["teal4"], dark["pink5"], dark["purple4"], dark["indigo2"],
                    dark["lime4"], light["blue2"] ]
    return colors[index % colors.length];
  }

  render() {
    return (
      <React.Fragment>
        <div className="border layout--flex--row size--content">
          <div className="chart-container" align="center">
            <LineChart
              width={this.props.width}
              height={this.props.height}
              data={this.props.data}
              margin={{ top: 50, right: 20, bottom: 20, left: 20 }}
            >
              <CartesianGrid strokeDasharray="3 3" strokeOpacity="0.4" stroke="#8e8d84" />
              {this.props.xAxis}
              {this.props.leftYAxis}
              {this.props.rightYAxis}
              {this.props.tooltip}
              {this.props.legend}
              {
                this.props.lines.map((line, index) =>
                  (<Line
                    yAxisId={line.yAxisId}
                    type="monotone"
                    legendType="circle"
                    name={line.name}
                    dataKey={line.dataKey}
                    key={line.dataKey}
                    stroke={this.getColor(index)}
                    connectNulls={this.props.connectNulls}
                    strokeWidth={2}
                    dot={this.props.dot}
                  />)
                )
              }
              {this.props.brush}
            </LineChart>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

LineChartComponent.defaultProps = {
  width: 1400,
  height: 600,
  connectNulls: true,
  dot: false,
  rightYAxis: undefined
};

LineChartComponent.propTypes = {
  data: PropTypes.array.isRequired,
  lines: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    dataKey: PropTypes.string,
    yAxisId: PropTypes.string
  })).isRequired,
  xAxis: PropTypes.object,
  leftYAxis: PropTypes.object,
  rightYAxis: PropTypes.object,
  legend: PropTypes.object,
  tooltip: PropTypes.object,
  dot: PropTypes.any,
  brush: PropTypes.object,
  height: PropTypes.number,
  width: PropTypes.number,
  connectNulls: PropTypes.bool
};
