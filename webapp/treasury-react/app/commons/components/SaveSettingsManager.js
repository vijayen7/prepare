import React, { Component } from "react";
import FilterButton from "../components/FilterButton";
import { getSaveSettingsUrl } from "../util.js";
import { Layout, SettingsManager } from "arc-react-components";
import Panel from "commons/components/Panel";

export default class SaveSetting extends Component {
  constructor(props) {
    super(props);
    this.save = this.save.bind(this);
    this.apply = this.apply.bind(this);
    this.saveRef = React.createRef();
    this.state = { selectedFilters: {} };
  }

  componentDidMount() {
    this.setState({ selectedFilters: this.props.selectedFilters });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ selectedFilters: nextProps.selectedFilters });
  }

  save() {
    this.saveRef.current.save(this.state.selectedFilters);
  }

  apply(settingsJson, selectedSettingMetaData) {
    this.props.applySavedFilters(settingsJson, selectedSettingMetaData);
  }

  render() {
    return (
      <Layout isColumnType style={{ height: "40px" }}>
        <Layout.Child childId="child1">
          <SettingsManager
            ref={this.saveRef}
            serviceURL={getSaveSettingsUrl()}
            applicationId={3}
            onApply={this.apply.bind(this)}
            applicationCategory={this.props.applicationName}
          />
        </Layout.Child>
        <Layout.Child size="content" childId="child2">
          <FilterButton
            onClick={this.save}
            className ="button--tertiary size--content float--right margin--left--small"
            label={<i className="icon-save" />}
          />
        </Layout.Child>
      </Layout>
    );
  }
}
