import React from "react";

const Button = props => {
  return (
    <button
      className={
        props.tertiary
          ? "button--tertiary size--content margin--small"
          : "button--primary size--content margin--small" +
            (props.floatRight ? "float--right" : "")
      }
      disabled={props.disabled}
      onClick={props.onClick}
      style={props.style}
    >
      {props.label}
    </button>
  );
};

export default Button;
