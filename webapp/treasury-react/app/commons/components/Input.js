import React, { Component } from "react";

export default class Input extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.selectKey,
      value: e.target.value
    };
    this.props.onSelect(returnValue);
  };

  render() {
    return (
      <input
        type="text"
        defaultValue={this.props.value}
        onChange={this.handleChange}
      />
    );
  }
}
