import React, { Component } from "react";

export default class SingleSelectFilter extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.combobox.addEventListener("optionSelected", this.handleChange);
    this.combobox.multiSelect = false;
    this.combobox.data = this.props.data;
    this.combobox.value = this.props.selectedData;
  }

  componentDidUpdate(prevProps, prevState) {
    this.combobox.data = this.props.data;
    this.combobox.value = this.props.selectedData;
  }

  handleChange = e => {
    this.props.onSelect({ key: this.props.stateKey, value: e.detail.value });
  };

  componentWillUnmount() {
    this.combobox.removeEventListener("optionSelected", this.handleChange);
  }

  render() {
    return (
      <div className={this.props.style ? this.props.style : "form-field--split"}>
        <label title={this.props.label}>{this.props.label}</label>
        <arc-combobox
          placeholder={this.props.placeholder}
          readonly={this.props.readonly ? this.props.readonly : null}
          ref={combobox => {
            this.combobox = combobox;
          }}
        />
      </div>
    );
  }
}
