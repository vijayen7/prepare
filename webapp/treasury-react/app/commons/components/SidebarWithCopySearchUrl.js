import React from "react";
import { Sidebar as SideBar } from "arc-react-components";
import copy from 'clipboard-copy';
const queryString = require("query-string");
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import { ToastService } from 'arc-react-components';


export default class SidebarWithCopySearchUrl extends React.Component {
  static defaultProps = {
    header: "Filter",
    collapsible: false,
    toggleSidebar: false,
    resizeCanvas: () => {}
  };

  constructor(props) {
    super(props);
    this.state = { collapsed: false };
    this.triggerKeyboardEvent = this.triggerKeyboardEvent.bind(this);
    this.copySearchUrlToClipboard = this.copySearchUrlToClipboard.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.toggleSidebar == nextProps.toggleSidebar) {
      this.handleToggle();
    }
  }

  componentDidMount() {
      document.body.addEventListener("keyup",this.triggerKeyboardEvent);
      if(this.props.copySearchUrl) {
        var filters = {};
          try {
             filters = JSON.parse(decodeURI(window.location.hash.slice(1)));
          } catch(err){
             filters = {};
          }
          if(Object.keys(filters).length > 0){
             this.props.setFilters(filters);
          }
      }
  }

  componentWillUnmount() {
    document.body.removeEventListener("keyup",this.triggerKeyboardEvent);
  }

  copySearchUrlToClipboard() {
    copy(location.href.replace(location.hash,"")+'#'+JSON.stringify(this.props.selectedFilters));
    ToastService.append({
      content: `Search URL copied to clipboard.`,
      type: ToastService.ToastType.INFO,
      placement: ToastService.Placement.BOTTOM_LEFT,
      dismissTime: 1000
    });
  }

  triggerKeyboardEvent(event) {
    let element = document.getElementById(this.props.id);
    let activeElement = document.activeElement;
    if(element && element.contains(activeElement)){
      switch(event.keyCode){
        case $.ui.keyCode.ENTER:
          if(typeof this.props.enterFunction == "function"){
            // To hide datapicker when enter key pressed.
            if(element.querySelectorAll('.hasDatepicker').length > 0){
              $(element.querySelectorAll('.hasDatepicker')).datepicker('hide');
            }
            this.props.enterFunction();
           }
           break;
         case $.ui.keyCode.ESCAPE:
          if(element.querySelector('.icon-caret--up')){
            element.querySelector('.icon-caret--up').click();
          }
          break;
          default:
            break;
      }
    }
  }
  handleToggle = state => {
    let collapsed = !this.state.collapsed;
    this.setState({ collapsed: collapsed });
    this.props.resizeCanvas();
    if(typeof this.props.onToggle === "function"){
      this.props.onToggle(state);
    }
  };

  render() {
    var filters = React.Children.toArray(this.props.children);
    const footer = (
      <ColumnLayout>
        <FilterButton
          onClick={this.props.handleSearch}
          reset={false}
          label="Search"
        />
        <FilterButton
          reset={true}
          onClick={this.props.handleReset}
          label="Reset"
        />
        {this.props.copySearchUrl?(
          <FilterButton
            reset={true}
            onClick={this.copySearchUrlToClipboard}
            label="Copy Search URL"
            className="float--left button--tertiary margin--small"
          />
        ):null}
      </ColumnLayout>
    )
    return (
      <SideBar
        title={this.props.header}
        collapsible={this.props.collapsible}
        footer={footer}
        onToggle={this.handleToggle}
        collapsed={this.state.collapsed}
        id={this.props.id}
      >
        {filters}
      </SideBar>
    );
  }
}
