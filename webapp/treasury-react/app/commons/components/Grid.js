import { ReactArcGrid } from "arc-grid";
import React from "react";

function renderLabel(props) {
  return props.label !== undefined ? (
    <div className="text-align--center margin--vertical--small">
      {props.label}
    </div>
  ) : null;
}

function renderMessage(props) {
  if (
    props.actualRowCount &&
    props.truncatedRowCount &&
    props.truncatedRowCount < props.actualRowCount
  ) {
    return (
      <div className="text-align--center margin--vertical--small message--critical">
        Search returned too many records ({props.actualRowCount}). Truncating to{" "}
        {props.truncatedRowCount}
      </div>
    );
  }
}

function getStyle(props) {
  return props.height !== undefined ? { height: props.height + "px" } : {};
}

function renderGrid(props) {
  var style = getStyle(props);
  if (props.data !== undefined && props.data.length > 0) {
    return (
      <div style={style}>
        <ReactArcGrid
          gridId={props.gridId}
          data={props.data}
          columns={props.gridColumns}
          options={props.gridOptions}
        />
      </div>
    );
  }
  return null;
}

const Grid = props => {
  return (
    <div>
      {renderLabel(props)}
      {renderMessage(props)}
      {renderGrid(props)}
    </div>
  );

  return null;
};

export default Grid;
