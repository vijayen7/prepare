import React from "react";
import CheckboxFilter from "./CheckboxFilter";

const CheckboxFilters = props => {
  
  return (
    <div>
      <CheckboxFilter
        defaultChecked={props.state.showAnnualized}
        onSelect={props.onSelect}
        stateKey="showAnnualized"
        label="Show Annualized"
      />
      <CheckboxFilter
        defaultChecked={props.state.explicitCommission}
        onSelect={props.onSelect}
        stateKey="explicitCommission"
        label="Explicit Commission"
      />
      <CheckboxFilter
        defaultChecked={props.state.implicitCommission}
        onSelect={props.onSelect}
        stateKey="implicitCommission"
        label="Implicit Commission"
      />
      <CheckboxFilter
        defaultChecked={props.state.financing}
        onSelect={props.onSelect}
        stateKey="financing"
        label="Financing"
      />
      <CheckboxFilter
        defaultChecked={props.state.clearingFee}
        onSelect={props.onSelect}
        stateKey="clearingFee"
        label="Clearing Fee"
      />
      <CheckboxFilter
        defaultChecked={props.state.ticketCharges}
        onSelect={props.onSelect}
        stateKey="ticketCharges"
        label="Ticket Charges"
      />
      <CheckboxFilter
        defaultChecked={props.state.bankCharge}
        onSelect={props.onSelect}
        stateKey="bankCharge"
        label="Bank Charge"
      />
      <CheckboxFilter
        defaultChecked={props.state.custodyFee}
        onSelect={props.onSelect}
        stateKey="custodyFee"
        label="Custody Fee"
      />
    </div>
  );
};

export default CheckboxFilters;
