import PropTypes from "prop-types";
import React from "react";
import { ComboBox } from "arc-react-components";

export default class ReactComboBoxFilter extends React.Component {
  getObject = (element) => {
    if (element) {
      return element;
    }
    return null;
  };

  getObjects = (optionList = []) => {
    const data = [];
    if (optionList !== null) {
      [].concat(optionList).forEach((element) => {
        data.push(this.getObject(element));
      });
    }
    return data;
  };

  handleChange = (e) => {
    const multiSelectEnabled =
      typeof this.props.multiSelect !== "undefined"
        ? this.props.multiSelect
        : true;
    this.props.onSelect({
      key: this.props.stateKey,
      value: multiSelectEnabled ? this.getObjects(e) : this.getObject(e),
    });
  };

  render() {
    const comboBoxClass = this.props.errorDiv ? "error" : "";
    const selectedData = this.getObjects(this.props.selectedData);
    const options = this.getObjects(this.props.data);
    const labelMarkup = this.props.label ? this.props.label : "";
    const multiSelectEnabled =
      typeof this.props.multiSelect !== "undefined"
        ? this.props.multiSelect
        : true;
    return (
      <div className={this.props.style ? this.props.style : "margin"}>
        <label title={this.props.label} className="padding--right--large">
          {labelMarkup}
        </label>
        <ComboBox
          className={comboBoxClass}
          multiSelect={multiSelectEnabled}
          value={selectedData}
          placeholder={this.props.placeholder}
          options={options}
          onChange={(selectedValue) => {
            this.handleChange(selectedValue);
          }}
          showTokensBelow={this.props.showTokens}
          maxTokenHeight = {this.props.maxTokenHeight}
          disabled={this.props.disabled}
        />
      </div>
    );
  }
}

ReactComboBoxFilter.defaultProps = {
  data: [],
  selectedData: [],
  onSelect: undefined,
  labelName: "",
  placeholder: "",
  showTokens: false,
  maxTokenHeight: 110,
  disabled: false
};

ReactComboBoxFilter.propTypes = {
  label: PropTypes.string,
  data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.number),
    PropTypes.object,
    PropTypes.string,
    PropTypes.number,
  ]),
  onSelect: PropTypes.func,
  selectedData: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.number),
    PropTypes.object,
    PropTypes.string,
    PropTypes.number,
  ]),
  placeholder: PropTypes.string,
  errorDiv: PropTypes.bool,
  stateKey: PropTypes.string,
  style: PropTypes.string,
  disabled: PropTypes.bool
};
