import React, { useState } from 'react';
import PropTypes from 'prop-types';

const ClickableIconWithPause = props => {
  const [executing, setExecuting] = useState(false);

  const onRealClick = async (event) => {
    if (executing === true) {
      return
    }
    setExecuting(true);
    try {
        await props.onClick(event);
        await new Promise(r => setTimeout(r, 500));
    } finally {
        setExecuting(false);
    }
  };

  return (
    <i
    title={props.title}
    hidden={props.hidden}
    className={props.className}
    onClick={onRealClick}
  />
  );
};

ClickableIconWithPause.propTypes = {
  className: PropTypes.string,
  hidden: PropTypes.hidden,
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string
 };

export default ClickableIconWithPause;
