import React from "react";
import PropTypes from 'prop-types';
import _ from "lodash";
const Message = props => {
  var style;
  if (props.error && props.error === true){
    style = "message message--critical";
  } else if (props.success && props.success === true) {
    style = "message message--success";
  } else if (props.warning && props.warning === true) {
    style = "message message--warning";
  } else if (props.primary && props.primary === true) {
    style = "message message--primary";
  } else {
    style = "message";
  }

  return (
    <center>
      <p className={style}  style={{ width: "100%" }}>
        {props.messageData}
      </p>
    </center>
  );
};

Message.propTypes = {
  messageData: PropTypes.string.isRequired,
  error: PropTypes.bool,
  success: PropTypes.bool,
  warning: PropTypes.bool,
  primary: PropTypes.bool
};

export default Message;
