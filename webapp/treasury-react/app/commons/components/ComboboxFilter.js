import React, { Component } from "react";

export function getComboboxFilter(style){
return  class ComboboxFilter extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.combobox.addEventListener("optionSelected", this.handleChange);
    this.combobox.multiSelect = (typeof this.props.multiSelect === "undefined") ? true : this.props.multiSelect;
    this.combobox.disabled =
      this.props.readonly == undefined ? false : this.props.readonly;
    this.combobox.data = this.props.data;
    this.combobox.value = this.props.selectedData;
  }

  componentDidUpdate(prevProps, prevState) {
    this.combobox.data = this.props.data;
    this.combobox.value = this.props.selectedData;
  }

  handleChange = e => {
    this.props.onSelect({ key: this.props.stateKey, value: e.detail.value });
  };

  componentWillUnmount() {
    this.combobox.removeEventListener("optionSelected", this.handleChange);
  }

  render() {
    let label = this.props.label;
    return (
      <div className={(style)?style:"margin"}>
        <label title={this.props.label} className="padding--right--large">{label}</label>
        <arc-combobox
          placeholder={this.props.placeholder}
          class = {this.props.isError ? "error" : ""}
          ref={combobox => {
            this.combobox = combobox;
          }}
        />
      </div>
    );
  }
}
}
