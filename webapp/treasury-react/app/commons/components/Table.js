import React, { Component } from 'react';
import PropTypes from 'prop-types';
class Table extends Component {
  generateRows(dataList, keys) {
    return dataList.map((item) => {
      const cells = keys.map((colData, i) => {
        return <td key={i}>{item[colData]}</td>;
      });
      return <tr key={item.id}>{cells}</tr>;
    });
  }

  render() {
    const tableHeaders = [];
    const dataList = this.props.content;
    const { labels, dataKeys } = this.props;
    labels.map((label) => tableHeaders.push(<th key={label} style={{ width:this.props.width }}>{label}</th>));

    const rowComponents = this.generateRows(dataList, dataKeys);
    const style = typeof this.props.style !== "undefined" ? this.props.style : { width: '100%', display: 'table'}
    return (
      <table className="table" style={style}>
        <thead>
          <tr>{tableHeaders}</tr>
        </thead>
        <tbody>{rowComponents}</tbody>
      </table>
    );
  }
}

Table.propTypes = {
  content: PropTypes.array,
  width: PropTypes.string,
  labels: PropTypes.array,
  dataKeys: PropTypes.array,
};

export default Table;
