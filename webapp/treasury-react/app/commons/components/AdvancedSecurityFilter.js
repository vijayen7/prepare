import React, { Component } from "react";
import { getComboboxFilter } from "commons/components/ComboboxFilter";
import InputFilter from "commons/components/InputFilter";

export default class AdvancedSecurityFilter extends Component {
    constructor(props) {
        super(props);
        this.securitySearchTypes = ["SECURITY_NAME", "ISIN", "CUSIP", "SEDOL", "TICKER"];
        this.textSearchTypes = ["BEGINS_WITH", "EXACT_MATCH", "CONTAINS"];
    }

    render() {
        const ComboboxFilter = getComboboxFilter();
        return (
            <React.Fragment>
                <ComboboxFilter
                    data={this.securitySearchTypes}
                    onSelect={this.props.onSelect}
                    selectedData={this.props.selectedSecuritySearchType}
                    multiSelect="false"
                    stateKey="selectedSecuritySearchType"
                    label="Security Search Type"
                />
                <ComboboxFilter
                    data={this.textSearchTypes}
                    onSelect={this.props.onSelect}
                    selectedData={this.props.selectedTextSearchType}
                    multiSelect="false"
                    stateKey="selectedTextSearchType"
                    label="Text Search Type"
                />
                <InputFilter
                    data={this.props.securitySearchString}
                    onSelect={this.props.onSelect}
                    stateKey="securitySearchString"
                    label="Search String"
                />
            </React.Fragment>
        );
    }
}