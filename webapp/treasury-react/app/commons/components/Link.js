import React from 'react';
import PropTypes from 'prop-types';

const Link = (props) => (
  <React.Fragment>
    <a id={props.id} onClick={() => window.open(props.href)}>
      {props.text}
    </a>
    <i className="icon-link margin--right--double"></i>
  </React.Fragment>
);

Link.defaultProps = {
  text: '',
  href: '#'
};

Link.propTypes = {
  id: PropTypes.string,
  text: PropTypes.string,
  href: PropTypes.string
};

export default Link;
