import { ArcFetch } from "arc-commons";
import { BASE_URL } from "../../commons/constants";
import SERVICE_URLS from "../UrlConfigs";

const QUERY_ARGS = {
  credentials: "include",
  method: "GET",
};

export const getMarginAllocatorData = () => {
  const url = `${BASE_URL}service/${SERVICE_URLS.marginFilterService.getAllocators}?&inputFormat=json&format=json`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const getDateFilterData = () => {
  const url = `${BASE_URL}data/load-date-filter`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};
