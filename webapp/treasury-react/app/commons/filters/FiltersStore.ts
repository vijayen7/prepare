import { action, observable } from "mobx";
import MapData from "../models/MapData";
import { getMarginAllocatorData } from "./api";
import { createContext } from "react";

class FilterStore {
  @observable
  inProgress: boolean = false;

  @observable
  marginAllocatorFilterRefData: MapData[] = [];

  fetchMarginAllocatorRefData = async () => {
    try {
      this.inProgress = true;
      let data: MapData[] = await getMarginAllocatorData();
      //This check is in order to prevent the crashing of UI while developing when the dev instance is down
      if (data && data.length) {
        this.marginAllocatorFilterRefData = data;
      }
      this.inProgress = false;
    } catch (error) {
      this.inProgress = false;
    }
  };
}

export default createContext(new FilterStore());
