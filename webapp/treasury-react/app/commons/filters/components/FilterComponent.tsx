import React from "react";
import { ComboBox } from "arc-react-components";
import FilterProps from "../../models/FilterProps";

const FilterComponent: React.FC<FilterProps> = (props: FilterProps) => {
  return (
    <>
      <div className={props.horizontalLayout ? "form-field--split" : "margin"}>
        <label title={props.label}>{props.label}</label>
        <ComboBox
          options={props.data}
          value={props.selectedData}
          placeholder={props.placeholder ? props.placeholder : ""}
          onChange={props.onSelect}
          multiSelect={props.multiSelect}
          readOnly={props.readonly}
          disabled={props.disabled}
        />
      </div>
    </>
  );
};

export default FilterComponent;
