import React from "react";
import FilterProps from "../../models/FilterProps";
import FilterComponent from "../components/FilterComponent";
import MapData from "../../models/MapData";

const SingleSelectFilter: React.FC<FilterProps> = (props: FilterProps) => {
  const onChange = (val: MapData) => {
    props.onSelect({ key: props.stateKey, value: val });
  };

  return (
    <>
      <FilterComponent
        horizontalLayout={props.horizontalLayout}
        label={props.label ? props.label : "Filter"}
        data={props.data ? props.data : []}
        selectedData={props.selectedData}
        onSelect={onChange}
        multiSelect={false}
        readonly={props.readonly}
        disabled={props.disabled}
      />
    </>
  );
};

export default SingleSelectFilter;
