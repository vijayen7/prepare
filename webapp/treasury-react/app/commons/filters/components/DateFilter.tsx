import React from "react";
import { DatePicker } from "arc-react-components";

interface Props {
  label?: string;
  stateKey: number | string;
  data: string;
  onChange: Function;
}

const DateFilter: React.FC<Props> = (props: Props) => {
  const onChange = (date: string) => {
    let returnVal = {
      key: props.stateKey,
      value: date,
    };
    props.onChange(returnVal);
  };

  const getDateFilter = () => {
    if (props.label) {
      return (
        <>
          <div className={"form-field--split"}>
            <label>{props.label}</label>
            <DatePicker
              value={props.data}
              onChange={(date: string) => {
                onChange(date);
              }}
              renderInBody
            />
          </div>
        </>
      );
    } else {
      return (
        <>
          <DatePicker
            value={props.data}
            onChange={(date: string) => {
              onChange(date);
            }}
            renderInBody
          />
        </>
      );
    }
  };

  return <>{getDateFilter()}</>;
};

export default DateFilter;
