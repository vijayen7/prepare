import React, { useEffect, useContext } from "react";
import FilterProps from "../../models/FilterProps";
import FilterStore from "../FiltersStore";
import FilterComponent from "../components/FilterComponent";
import { observer } from "mobx-react-lite";
import MapData from "../../models/MapData";

const MarginAllocatorFilter: React.FC<FilterProps> = (props: FilterProps) => {
  const store = useContext(FilterStore);

  const stateKey = props.stateKey ? props.stateKey : "selectedAllocators";

  const onChange = (val: MapData | MapData[]) => {
    props.onSelect({ key: stateKey, value: val });
  };

  useEffect(() => {
    if (store.marginAllocatorFilterRefData.length == 0) {
      store.fetchMarginAllocatorRefData();
    }
  }, []);

  return (
    <>
      <FilterComponent
        horizontalLayout={props.horizontalLayout}
        label={
          props.label
            ? props.label
            : props.multiSelect
            ? "Allocators"
            : "Allocator"
        }
        data={store.marginAllocatorFilterRefData}
        selectedData={props.selectedData}
        onSelect={onChange}
        multiSelect={props.multiSelect}
        readonly={props.readonly}
        disabled={props.disabled}
      />
    </>
  );
};

export default observer(MarginAllocatorFilter);
