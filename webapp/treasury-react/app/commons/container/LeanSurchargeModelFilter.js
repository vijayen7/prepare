import React from "react";
import { getFilterContainer } from './DynamicFilterWrapper';
import { LEAN_SURCHARGE_MODEL_FILTER } from "../constants";

var FilterComponent = getFilterContainer(
  LEAN_SURCHARGE_MODEL_FILTER,
  "leanSurchargeModels",
  "selectedLeanSurchargeModel",
  "Lean Surcharge Model"
);

const LeanSurchargeModelFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
      label={props.label}
      payload={props.payload}
      useReactComboboxFilter={true}
    />
  );
};

export default LeanSurchargeModelFilter;
