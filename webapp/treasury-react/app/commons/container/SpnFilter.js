import React, { Component } from "react";
import { connect } from "react-redux";
import MultiSelectFilter from "commons/components/MultiSelectFilter";
import { Async } from "react-select";
import "react-select/dist/react-select.css";

export default class SpnFilter extends Component {
  constructor(props) {
    super(props);
    this.getResults = this.getResults.bind(this);
    this.onChange = this.onChange.bind(this);
    this.state = {};
  }

  getResults(input) {
    if (!input) {
      return Promise.resolve({ options: [] });
    }
    return fetch(
      `http://u-budhani-treasury-dns.sd.deshaw.com:14081/treasury/service/securityAutocompleteService/getAutocompleteSecurityList?q=${input}&format=JSON`,
      {
        credentials: "include"
      }
    )
      .then(response => response.json())
      .then(json => {
        return { options: json.autoCompleteList };
      });
  }

  onChange(value) {
    this.setState({
      selectedValue: value
    });
  }

  render() {
    return (
      <div className="margin--top">
      <Async
        placeholder="Search securities"
        multi={true}
        isLoading={true}
        backspaceRemoves={true}
        name="form-field-name"
        value={this.state.selectedValue}
        valueKey="key"
        labelKey="value"
        loadOptions={this.getResults}
        onChange={this.onChange}
      />
      </div>
    );
  }
}
