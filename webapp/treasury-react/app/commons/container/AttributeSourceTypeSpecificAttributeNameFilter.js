import React from 'react';

import { getFilterContainer } from './DynamicFilterWrapper';
import { ATTRIBUTE_SOURCE_TYPE_SPECIFIC_ATTRIBUTE_NAME_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  ATTRIBUTE_SOURCE_TYPE_SPECIFIC_ATTRIBUTE_NAME_FILTER,
  'attrNames',
  'selectedAttributeName',
  'Attribute Name'
);

const AttributeSourceTypeSpecificAttributeNameFilter = (props) => {
  const payload = {
    sourceTypeId: props.selectedAttributeSourceTypeIds
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      payload={payload}
    />
  );
};

export default AttributeSourceTypeSpecificAttributeNameFilter;
