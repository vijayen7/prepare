import React from 'react';
import { getFilterContainer } from './FilterWrapper';
import { NEGATIVE_IRATE_FILTER } from '../constants';

const NegativeIrateFilter = (props) => {
  const label = typeof props.label !== 'undefined' ? props.label : 'Negative Irate';
  const stateKey = typeof props.stateKey !== 'undefined' ? props.stateKey : 'selectedApplyNegativeIrates';
  const FilterComponent = getFilterContainer(
    NEGATIVE_IRATE_FILTER,
    'applyNegativeIrates',
    stateKey,
    label
  );
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      readonly={props.readonly}
      multiSelect={props.multiSelect}
      label={label}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default NegativeIrateFilter;
