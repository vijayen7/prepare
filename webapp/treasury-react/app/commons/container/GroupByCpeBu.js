import React, { Component } from "react";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

export default class GroupByCpeBu extends Component {
  render() {
    return (
      <SingleSelectFilter
        data={[
          { key: "CPE", value: "Counterparty" },
          { key: "BU", value: "Business Unit" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="groupByLevel"
        label="Group By"
      />
    );
  }
}
