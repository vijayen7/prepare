import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { CURRENCY_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  CURRENCY_FILTER,
  "currencies",
  "selectedCurrencies",
  "Currencies"
);

const CurrencyFilter = props => {

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      singleSelect={props.singleSelect}
      label={props.label}
      stateKey={props.stateKey}
      disabled={props.disabled}
    />
  );
};

export default CurrencyFilter;
