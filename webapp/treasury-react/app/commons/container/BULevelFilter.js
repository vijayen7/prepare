import React, { Component } from "react";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

export default class BULevelFilter extends Component {
  render() {
    return (
      <SingleSelectFilter
        data={[
          { key: "BU", value: "Business Unit" },
          { key: "BUNDLE_GROUP", value: "Bundle Group" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedBULevel"
        label="BU Level"
      />
    );
  }
}
