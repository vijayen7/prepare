import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { BOOK_FILTER_WITH_SPECIAL_ACCOUNTING_BOOKS } from "../constants";

const FilterComponent = getFilterContainer(
  BOOK_FILTER_WITH_SPECIAL_ACCOUNTING_BOOKS,
  "booksWithSpecialAccountingBooks",
  "selectedBooks",
  "Books"
);
const BookFilter = props => {
  return (
    <FilterComponent
      label={props.label ? props.label : "Books"}
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default BookFilter;
