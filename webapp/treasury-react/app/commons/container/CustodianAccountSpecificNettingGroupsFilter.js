import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { CUSTODIAN_ACCOUNT_SPECIFIC_NETTING_GROUP_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  CUSTODIAN_ACCOUNT_SPECIFIC_NETTING_GROUP_FILTER,
  'custodianAccountSpecificNettingGroups',
  'selectedNettingGroups',
  'Netting Groups'
);

const CustodianAccountSpecificNettingGroupsFilter = (props) => {
  const payload = {
    custodianAccountFilterIds: props.custodianAccountIds
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      payload={payload}
    />
  );
};

export default CustodianAccountSpecificNettingGroupsFilter;
