import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { CPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  CPE_FILTER,
  "cpes",
  "selectedCpes",
  "CPEs"
);

const CpeFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      singleSelect={props.singleSelect}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      stateKey={props.stateKey}
      label={props.label ? props.label : props.multiSelect ? "CPEs" : "CPE"}
      disabled={props.disabled}
    />
  );
};

export default CpeFilter;
