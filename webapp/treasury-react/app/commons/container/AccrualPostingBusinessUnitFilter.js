import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { ACCRUAL_POSTING_BUSINESS_UNIT_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  ACCRUAL_POSTING_BUSINESS_UNIT_FILTER,
  'accrualPostingBUs',
  'selectedBusinessUnits',
  'Business Unit'
);

const AccrualPostingBusinessUnitFilter = (props) => {
  const payload = {
    chargeTypeId: props.parameters.chargeTypeFilterIds
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      payload={payload}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AccrualPostingBusinessUnitFilter;
