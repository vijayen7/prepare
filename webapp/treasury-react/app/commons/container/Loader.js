import React, { Component } from 'react';
import { connect } from 'react-redux';

class Loader extends Component {
  componentDidMount() {
    this.loader.style.display = 'none';
  }

  componentDidUpdate() {
    if (this.props.isLoading > 0) {
      this.loader.style.display = 'block';
    } else {
      this.loader.style.display = 'none';
    }
  }

  render() {
    return (
      <div
        style={{ zIndex: '9200' }}
        className="overlay form-field--center"
        ref={(loader) => {
          this.loader = loader;
        }}
      >
        <div style={{ zIndex: '9200' }} className="loader loader--block form-field--center" style={{ width: '100px' }}>
          Loading...
        </div>
      </div>
    );
  }
}

function mapStateToProps({ isLoading }) {
  return { isLoading };
}

export default connect(mapStateToProps)(Loader);
