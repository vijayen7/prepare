import React, { Component } from "react";
import { connect } from "react-redux";
import { getComboboxFilter } from "commons/components/ComboboxFilter";

const ComboboxFilter = getComboboxFilter();

export default class OutperformanceTypeFilter extends Component  {
  render() {

      const label = ( typeof this.props.label !== "undefined") ? this.props.label : "Outerperformance Type"
      return (
      <ComboboxFilter
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        multiSelect={this.props.multiSelect}
        readonly={this.props.readonly}
        errorDiv={this.props.errorDiv}
        stateKey={this.props.stateKey}
        horizontalLayout={this.props.horizontalLayout}
        label={label}
        data={[
        {key : 1 , value : "Swap" },
        {key : 2 , value : "Cash"}
        ]}
        />
    );
  }
}
