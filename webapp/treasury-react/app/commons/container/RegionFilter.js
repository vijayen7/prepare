import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { REGION_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  REGION_FILTER,
  "regions",
  "selectedRegions",
  "Region"
);

const RegionFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      label={props.label}
    />
  );
};

export default RegionFilter;
