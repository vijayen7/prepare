import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { MARKET_FILTER } from "../constants";

const MarketFilter = props => {
  let label = typeof props.label !== "undefined" ? props.label : "Markets";
  let stateKey =
    typeof props.stateKey !== "undefined" ? props.stateKey : "selectedMarket";
  let FilterComponent = getFilterContainer(
    MARKET_FILTER,
    "markets",
    stateKey,
    label
  );

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default MarketFilter;
