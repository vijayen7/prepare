import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { AGREEMENT_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  AGREEMENT_FILTER,
  "agreementIds",
  "selectedAgreements",
  "Agreements"
);

const AgreementFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      readonly={props.readonly}
      multiSelect={props.multiSelect}
      label={"Agreements"}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AgreementFilter;
