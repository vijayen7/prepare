import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { BICS_INDUSTRY_SUBGROUP_FILTER } from "../constants";

const BicsIndustrySubgroupFilter = props => {
  var label = (typeof props.label !== "undefined") ? props.label : "Bics Industry Subgroup";
  var stateKey = (typeof props.stateKey !== "undefined") ? props.stateKey : "selectedBicsIndustrySubgroups";
  var FilterComponent = getFilterContainer(
    BICS_INDUSTRY_SUBGROUP_FILTER,
    "bicsIndustrySubgroups",
    stateKey,
    label
  );

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default BicsIndustrySubgroupFilter;
