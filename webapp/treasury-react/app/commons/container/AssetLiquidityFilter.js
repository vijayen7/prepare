import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { ASSET_LIQUIDITY_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  ASSET_LIQUIDITY_FILTER,
  "assetLiquidity",
  "selectedAssetLiquidity",
  "Asset Lquidity"
);

const AssetLiquidityFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
    />
  );
};

export default AssetLiquidityFilter;
