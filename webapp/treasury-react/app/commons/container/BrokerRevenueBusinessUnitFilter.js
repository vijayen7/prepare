import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { BROKER_REVENUE_BUSINESS_UNIT_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  BROKER_REVENUE_BUSINESS_UNIT_FILTER,
  "brokerRevenueBusinessUnits",
  "selectedBrokerRevenueBusinessUnits",
  "Business Units"
);

const BrokerRevenueBusinessUnitFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
    />
  );
};

export default BrokerRevenueBusinessUnitFilter;
