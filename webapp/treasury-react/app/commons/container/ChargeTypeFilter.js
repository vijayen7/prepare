import React from 'react';
import { getFilterContainer } from './FilterWrapper';
import { CHARGE_TYPE_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  CHARGE_TYPE_FILTER,
  'chargeTypes',
  'selectedChargeTypes',
  'Charge Types'
);

const ChargeTypeFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default ChargeTypeFilter;
