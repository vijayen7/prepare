import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchBrokerRevenueYear } from "../actions/filters";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

class BrokerRevenueYearFilter extends Component {
  componentDidMount() {
    if (this.props.data.length <= 0) this.props.fetchBrokerRevenueYear();
  }

  render() {
    return (
      <SingleSelectFilter
        data={this.props.data}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedYear"
        label="Year"        
      />
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchBrokerRevenueYear }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    data: state.filters.brokerRevenueYears,
    ...ownProps
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  BrokerRevenueYearFilter
);
