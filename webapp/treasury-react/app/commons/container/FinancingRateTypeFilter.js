import React, { Component } from "react";
import { getFilterContainer } from "./FilterWrapper";
import { FINANCING_RATE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
    FINANCING_RATE_FILTER,
    "financingRateFilter",
    "selectedFinancingRateTypes",
    "Financing Rate Types"
  );
  

  const FinancingRateTypeFilter = props => {
    return (
      <FilterComponent
        onSelect={props.onSelect}
        selectedData={props.selectedData}
        multiSelect={props.multiSelect}
        errorDiv={props.errorDiv}
        horizontalLayout={props.horizontalLayout}
      />
    );
  };
  
  export default FinancingRateTypeFilter;
  