import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { FRONT_OFFICE_PRIME_BROKER_FILTER } from "../constants";
import FilterProps from "../models/FilterProps";

const FilterComponent = getFilterContainer(
  FRONT_OFFICE_PRIME_BROKER_FILTER,
  "primeBrokers",
  "selectedPrimeBrokers",
  "Front Office Prime Brokers"
);

const FrontOfficePrimeBrokerFilter: React.FC<FilterProps> = (props: FilterProps) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      singleSelect={props.singleSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
      label={
        props.label
          ? props.label
          : props.multiSelect
          ? "Front Office Prime Brokers"
          : "Front Office Prime Broker"
      }
    />
  );
};

export default FrontOfficePrimeBrokerFilter;
