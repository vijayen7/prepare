import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { AGREEMENT_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  AGREEMENT_TYPE_FILTER,
  "agreementTypes",
  "selectedAgreementTypes",
  "Agreement Types"
);

const AgreementTypeFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      singleSelect={props.singleSelect}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
      label={
        props.label
          ? props.label
          : props.multiSelect
          ? "Agreement Types"
          : "Agreement Type"
      }
      readonly={props.readonly}
       showTokens = {props.showTokens}
      maxTokenHeight = {props.maxTokenHeight}
      disabled={props.disabled}
    />
  );
};

export default AgreementTypeFilter;
