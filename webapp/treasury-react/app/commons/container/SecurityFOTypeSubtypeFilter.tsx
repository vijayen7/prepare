import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { SECURITY_FOTYPE_SUBTYPE_FILTER } from "../constants";
import FilterProps from "../models/FilterProps";

const FilterComponent = getFilterContainer(
  SECURITY_FOTYPE_SUBTYPE_FILTER,
  "securityTypeSubtypes",
  "selectedSecurityTypeSubtypes",
  "Security Types"
);

const SecurityFOTypeSubtypeFilter: React.FC<FilterProps> = (props: FilterProps) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      singleSelect={props.singleSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
      label={
        props.label
          ? props.label
          : props.multiSelect
          ? "Security Types"
          : "Security Type"
      }
    />
  );
};

export default SecurityFOTypeSubtypeFilter;
