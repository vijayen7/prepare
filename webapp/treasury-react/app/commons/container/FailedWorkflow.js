import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Dialog } from "arc-react-components";
import { cancelAndBeginWorkflow } from "../actions/workflow";

class FailedWorkflow extends Component {
  constructor(props) {
    super(props);
    this.handleCancelAndBegin = this.handleCancelAndBegin.bind(this);
    this.hideDialog = this.hideDialog.bind(this);
    this.state = { showDialog: false };
  }

  hideDialog() {
    this.setState({ showDialog: false });
  }

  handleCancelAndBegin() {
    var payload = {
      "workflowParam.workflowId": this.props.workflowStatus.resultList[0]
        .workflowId,
      "workflowParam.dateStr": this.props.workflowParams.dateStr,
      "workflowParam.entityFamilyId": this.props.workflowParams.entityFamilyId,
      "workflowParam.descoEntityId": this.props.workflowParams.descoEntityId,
      "workflowParam.counterpartyEntityId": this.props.workflowParams
        .counterpartyEntityId,
      "workflowParam.agreementTypeId": this.props.workflowParams
        .agreementTypeId,
      "workflowParam.workflowTypeName": this.props.workflowParams
        .workflowTypeName,
      "workflowParam.workflowUnit": this.props.workflowParams.workflowUnit
    };

    this.props.cancelAndBeginWorkflow(payload);
    this.hideDialog();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      nextProps.workflowStatus.hasOwnProperty("resultList") &&
      nextProps.workflowStatus.resultList[0].workflowState === "FAILED_WORKFLOW"
    ) {
      this.setState({ showDialog: true });
    }
  }

  render() {
    if (this.state.showDialog == true) {
      return (
        <Dialog
          isOpen={this.state.showDialog}
          title="Workflow Conflict"
          onClose={this.hideDialog}
          footer={
            <React.Fragment>
              <button onClick={this.handleCancelAndBegin}>
                Cancel And Begin Workflow
              </button>
            </React.Fragment>
          }
        >
          {this.props.workflowStatus.resultList[0].message}
        </Dialog>
      );
    }
    return null;
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      cancelAndBeginWorkflow
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    workflowStatus: state.workflow.workflowStatus,
    workflowParams: state.workflow.workflowParams
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FailedWorkflow);
