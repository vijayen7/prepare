import React, { Component } from "react";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

export default class FinancingStatusFilter extends Component {
  render() {
    return (
      <SingleSelectFilter
        data={[
          { key: "-1", value: "Total" },
          { key: "0", value: "Unfinanced" },
          { key: "1", value: "Financed" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedFinancingStatus"
        label="Financing Status"
      />
    );
  }
}
