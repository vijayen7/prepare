import React, { Component } from "react";
import { connect } from "react-redux";
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class MappingTagFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[
          { key: 0, value: "Mapped" },
          { key: 1, value: "Internal Missing" },
          { key: 2, value: "Quantity Mismatch" },
          { key: 3, value: "Broker Missing" },
          { key: 4, value: "Rolled Over" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedMappingTag"
        label="Mapping Tag"
      />
    );
  }
}
