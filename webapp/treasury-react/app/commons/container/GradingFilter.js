import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { GRADING_FILTER } from "../constants";

const GradingFilter = props => {
  var label = (typeof props.label !== "undefined") ? props.label : "Grading";
  var stateKey = (typeof props.stateKey !== "undefined") ? props.stateKey : "selectedGradings";
  var FilterComponent = getFilterContainer(
    GRADING_FILTER,
    "grading",
    stateKey,
    label
  );

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default GradingFilter;
