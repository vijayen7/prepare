import React from "react";
import { getFilterContainer } from "commons/container/DynamicFilterWrapper";
import { RULE_NAME_FILTER } from "commons/constants";

const FilterComponent = getFilterContainer(
  RULE_NAME_FILTER,
  "ruleName",
  "selectedRule",
  "Rule Name"
);

const RuleNameFilter = props => {
  let label = typeof props.label !== "undefined" ? props.label : "Rule Name";
  let selectKey =
    typeof props.selectKey !== "undefined" ? props.selectKey : "selectedRule";

  const payload = {
    ruleConfigTypeId: props.selectedRuleConfigType.key
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      payload={payload}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      singleSelect={props.singleSelect}
      label={label}
      selectKey={selectKey}
    />
  );
};

export default RuleNameFilter;
