import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { BUNDLE_GROUP_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  BUNDLE_GROUP_FILTER,
  "bundleGroups",
  "selectedBundleGroups",
  "Bundle Groups"
);

const BundleGroupFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
    />
  );
};

export default BundleGroupFilter;
