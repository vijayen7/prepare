import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { CPE_FAMILY_GROUPED_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
    CPE_FAMILY_GROUPED_FILTER,
    "cpeFamiliesGroup",
    "selectedCpeFamiliesGroup",
    "CPE Families Grouped"
);

const CpeFamilyGroupFilter = props => {
    return (
        <FilterComponent
            onSelect={props.onSelect}
            selectedData={props.selectedData}
            multiSelect={props.multiSelect}
            horizontalLayout={props.horizontalLayout}
             showTokens = {props.showTokens}
      maxTokenHeight = {props.maxTokenHeight}
        />
    );
};

export default CpeFamilyGroupFilter;
