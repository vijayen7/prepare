import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Dialog } from "arc-react-components";
const queryString = require("query-string");

class BeginWorkflow extends Component {
  constructor(props) {
    super(props);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      (nextProps.workflowStatus.hasOwnProperty("resultList") &&
        nextProps.workflowStatus.resultList[0].workflowState ===
        "BEGIN_WORKFLOW") ||
      (nextProps.workflowStatus.hasOwnProperty("resultList") &&
        nextProps.workflowStatus.resultList[0].workflowState ===
        "LOCKED_WORKFLOW" &&
        nextProps.workflowStatus.resultList[0].message === "Workflow resumed")
    ) {
      if (nextProps.url == undefined) {
        nextProps.onBeginWorkflow(nextProps.workflowStatus.resultList[0].workflowState, nextProps.workflowStatus.resultList[0].workflowId);
      } else {
        let url = "";
        const BASE_URL =
          window.location.protocol + "//" + window.location.host + nextProps.url;
        var params = nextProps.workflowParams;
        params["workflowId"] = nextProps.workflowStatus.resultList[0].workflowId;
        delete params["workflowUnit"];
        delete params["workflowTypeName"];
        const paramString = queryString.stringify(nextProps.workflowParams);
        url = `${BASE_URL}?inputFormat=PROPERTIES&${paramString}`;
        window.open(url, "_blank");
      }
    }
  }

  render() {
    return null;
  }
}

function mapStateToProps(state) {
  return {
    workflowStatus: state.workflow.workflowStatus,
    workflowParams: state.workflow.workflowParams
  };
}

export default connect(
  mapStateToProps,
  null
)(BeginWorkflow);
