import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchDateFilter } from "../actions/filters";
import PropTypes from 'prop-types';

class DateFilter extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (this.props.data === "" || this.props.dates === null ||
    typeof this.props.dates === 'undefined' || this.props.dates.length === 0 ) {
      this.props.fetchDateFilter();
    }
    var disabledStatus = (typeof this.props.disable === "undefined") ? false : true;
    if (this.refs.datepicker.classList.contains("hasDatePicker")) return;
    $(this.refs.datepicker)
      .datepicker({
        dateFormat: "yy-mm-dd",
        buttonText: "<i class='icon-calendar' />",
        showOn: "both",
        onSelect: value => this.handleChange(value),
        disabled: disabledStatus,
        onClose: (value, datePickerInstance) => this.handleChange(value) //Work around for keyboard date input
      })
      .datepicker("setDate", this.props.data);
  }

  componentWillReceiveProps(nextProps) {

    if ($(this.refs.datepicker).datepicker("getDate") == null || nextProps.data == "") {
      if (this.props.dateType === "tMinusTwo") {
        $(this.refs.datepicker).datepicker("setDate", nextProps.dates["tMinusTwoFilterDate"]);
        this.handleChange(nextProps.dates["tMinusTwoFilterDate"]);
      }
      else if (this.props.dateType === 'tFilterDate') {
        $(this.refs.datepicker).datepicker('setDate', nextProps.dates["tFilterDate"]);
        this.handleChange(nextProps.dates['tFilterDate']);
      }
      else {
        $(this.refs.datepicker).datepicker("setDate", nextProps.dates["tMinusOneFilterDate"]);
        this.handleChange(nextProps.dates["tMinusOneFilterDate"]);
      }
    }
    else
      $(this.refs.datepicker).datepicker("setDate", nextProps.data);
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.stateKey,
      value: e
    };
    this.props.onSelect(returnValue);
  };

  loadLabel(style, dateLabel) {
    if(style === "standard") {
      return (<div><label title={dateLabel}>{dateLabel}</label><br/></div>);
    }
    else {
      return (<label title={dateLabel} className="padding--right--large">{dateLabel}</label>);
    }
  }

  render() {
    let dateLabel = "Date";
    if (this.props.label !== undefined) dateLabel = this.props.label;
    var style = (this.props.layout === "standard") ? "size--content" : (this.props.layout === "no-padding-form-field") ? "form-field--split" : "form-field--split padding--horizontal";
    return (
      <div className={style}>
        {this.loadLabel(this.props.layout,dateLabel)}{" "}
        <div className="layout--flex">
          <input className={this.props.isError ? "size--9 error" : "size--9"} type="text" ref="datepicker" disabled={this.props.disable} />
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchDateFilter }, dispatch);
}

function mapStateToProps(state) {
  return {
    dates: state.filters.dates
  };
}

DateFilter.propTypes = {
  dateType: PropTypes.string,
  data: PropTypes.string,
  stateKey: PropTypes.string,
  label: PropTypes.string,
  onSelect: PropTypes.func,
  disable: PropTypes.bool,
  fetchDateFilter: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(
  DateFilter
);
