
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {getComboboxFilter} from "commons/components/ComboboxFilter"
import { fetchReasonCodeFilter } from "../actions/filters";

class ReasonCodeFilter extends Component {
  componentWillMount() {
    if (this.props.data.length <= 0) this.props.fetchReasonCodeFilter(this.props.adjustmentType);
  }

  render() {
    let style = (this.props.horizontalLayout && this.props.horizontalLayout === true) ? "form-field--split":"margin"
    const ComboboxFilter = getComboboxFilter(style);
    return (
      <ComboboxFilter
          data={this.props.data}
          onSelect={this.props.onSelect}
          selectedData={this.props.selectedData}
          multiSelect={this.props.multiSelect == undefined ? true : this.props.multiSelect}
          stateKey="selectedReasonCodes"
          label = "Reason Code"
        />
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchReasonCodeFilter }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    data: state.filters.reasonCodes,
    ...ownProps
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  ReasonCodeFilter
);
