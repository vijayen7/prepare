import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { SUB_TYPE_FILTER } from '../constants';

const FilterComponent = getFilterContainer(SUB_TYPE_FILTER, 'foSubtypes', 'selectedSubType', 'SubType');

const SubTypeFilter = (props) => {
  const payload = {
    foTypeId: props.selectedFOTypeIds
  };
  var stateKey = typeof props.stateKey !== 'undefined' ? props.stateKey : 'selectedSubType';
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      payload={payload}
      selectKey={stateKey}
    />
  );
};

export default SubTypeFilter;
