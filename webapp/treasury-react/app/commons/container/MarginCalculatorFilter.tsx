import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { MARGIN_CALCULATOR_FILTER } from "../constants";
import FilterProps from "../models/FilterProps";

const FilterComponent = getFilterContainer(
  MARGIN_CALCULATOR_FILTER,
  "marginCalculators",
  "selectedMarginCalculators",
  "Margin Calculators"
);

const MarginCalculatorFilter: React.FC<FilterProps> = (props: FilterProps) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      singleSelect={props.singleSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
      label={
        props.label
          ? props.label
          : props.multiSelect
          ? "Margin Calculators"
          : "Margin Calculator"
      }
    />
  );
};

export default MarginCalculatorFilter;
