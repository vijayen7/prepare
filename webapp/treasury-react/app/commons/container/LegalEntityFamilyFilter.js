import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { LEGAL_ENTITY_FAMILY_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  LEGAL_ENTITY_FAMILY_FILTER,
  "legalEntityFamilies",
  "selectedLegalEntityFamilies",
  "Legal Entity Families"
);

const LegalEntityFamilyFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
    />
  );
};

export default LegalEntityFamilyFilter;
