import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { ACCRUAL_POSTING_CUSTODIAN_ACCOUNT_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  ACCRUAL_POSTING_CUSTODIAN_ACCOUNT_FILTER,
  'accrualPostingCAs',
  'selectedCustodianAccounts',
  'Custodian Account'
);

const AccrualPostingCustodianAccountFilter = (props) => {
  const payload = {
    chargeTypeId: props.parameters.chargeTypeFilterIds,
    businessUnitId: props.parameters.businessUnitFilterIds,
    bundleId: props.parameters.bundleFilterIds
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      payload={payload}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AccrualPostingCustodianAccountFilter;
