import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { LEGAL_ENTITY_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  LEGAL_ENTITY_FILTER,
  "legalEntities",
  "selectedLegalEntities",
  "Legal Entities"
);

const LegalEntityFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      singleSelect={props.singleSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
      label={
        props.label
          ? props.label
          : props.multiSelect
          ? "Legal Entities"
          : "Legal Entity"
      }
    />
  );
};

export default LegalEntityFilter;
