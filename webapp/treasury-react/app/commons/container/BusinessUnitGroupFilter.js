import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { BUSINESS_UNIT_GROUP_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  BUSINESS_UNIT_GROUP_FILTER,
  "businessUnitGroups",
  "selectedBusinessUnitGroups",
  "Business Unit Groups"
);

const BusinessUnitGroupFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
    />
  );
};

export default BusinessUnitGroupFilter;
