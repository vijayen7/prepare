import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { ATTRIBUTE_SOURCE_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  ATTRIBUTE_SOURCE_TYPE_FILTER,
  "attrSourceTypes",
  "selectedAttributeSourceTypes",
  "Attribute Source Types"
);

const AttributeSourceTypeFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      label={props.multiSelect ?  "Attribute Source Types" : "Attribute Source Type"}
    />
  );
};

export default AttributeSourceTypeFilter;
