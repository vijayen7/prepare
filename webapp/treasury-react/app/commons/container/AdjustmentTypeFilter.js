import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getComboboxFilter } from "commons/components/ComboboxFilter";
import { fetchAdjustmentTypeFilter } from "../actions/filters";

class AdjustmentTypeFilter extends Component {
  componentDidMount() {
    if (this.props.data.length <= 0)
      this.props.fetchAdjustmentTypeFilter({
        adjustmentType: this.props.adjustmentType
      });
  }

  render() {
    let style =
      this.props.horizontalLayout && this.props.horizontalLayout === true
        ? "form-field--split"
        : "margin";
    const ComboboxFilter = getComboboxFilter(style);
    return (
      <ComboboxFilter
        data={this.props.data}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        multiSelect={
          this.props.multiSelect == undefined ? true : this.props.multiSelect
        }
        stateKey="selectedAdjustmentTypes"
        label="Adjustment Types"
      />
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchAdjustmentTypeFilter }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    data: state.filters.adjustmentTypes,
    ...ownProps
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdjustmentTypeFilter);
