import React, { Component } from "react";
import { ResizerContainer as ArcResizerContainer } from "arc-react-components";

class ResizerContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ArcResizerContainer
        key={this.props.key}
        className={this.props.className}
      >
        {this.props.children}
      </ArcResizerContainer>
    );
  }
}

export default ResizerContainer;