import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchRoaCalculationFactorYearFilter } from "../actions/filters";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

class RoaCalculationFactorYearFilter extends Component {
  componentDidMount() {
    if (this.props.data.length <= 0) this.props.fetchRoaCalculationFactorYearFilter();
  }

  render() {
    return (
      <SingleSelectFilter
        data={this.props.data}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedYear"
        label="Effective from year"
      />
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchRoaCalculationFactorYearFilter }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    data: state.filters.roaCalculationFactorYears,
    ...ownProps
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  RoaCalculationFactorYearFilter
);
