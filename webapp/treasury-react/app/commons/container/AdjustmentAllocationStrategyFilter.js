import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { ADJUSTMENT_ALLOCATION_STRATEGY_FILTER } from "../constants";

const AdjustmentAllocationStrategyFilter = props => {
  var label = (typeof props.label !== "undefined") ? props.label : "Adjustment Allocation";
  var stateKey = (typeof props.stateKey !== "undefined") ? props.stateKey : "selectedAllocationStrategy";
  var FilterComponent = getFilterContainer(
    ADJUSTMENT_ALLOCATION_STRATEGY_FILTER,
    "adjustmentAllocationStrategies",
    stateKey,
    label
  );

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AdjustmentAllocationStrategyFilter;
