import React from 'react';
import { getFilterContainer } from './FilterWrapper';
import { BUNDLE_FILTER } from '../constants';

const FilterComponent = getFilterContainer(BUNDLE_FILTER, 'bundles', 'selectedBundles', 'Bundles');

const BundleFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
    />
  );
};

export default BundleFilter;
