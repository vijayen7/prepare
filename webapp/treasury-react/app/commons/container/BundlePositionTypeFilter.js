import React, { Component } from "react";
import { connect } from "react-redux";
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class BundlePositionTypeFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[
          { key: 0, value: "Long" },
          { key: 1, value: "Short" },
          { key: 2, value: "Partially Netted" },
          { key: 3, value: "Netted"}
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedBundlePositionType"
        label="Bundle Position Type"
      />
    );
  }
}
