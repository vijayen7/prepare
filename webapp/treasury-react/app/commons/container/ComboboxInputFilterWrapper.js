import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchFilter } from "../actions";
import { isEqual } from 'lodash';

import { getComboboxInputFilter } from "../components/ComboBoxInputFilter";

export function getFilterContainer(
  filterType,
  stateName,
  selectKey,
  label
) {
  class BaseFilter extends Component {

    componentDidMount() {
      var filter = {
        key: filterType,
        filterData: this.props.payload
      };
      this.props.fetchFilter(filter);
    }

    componentDidUpdate(prevProps) {
      if (!isEqual(this.props.payload, prevProps.payload)) {
        var filter = {
          key: filterType,
          filterData: this.props.payload
        };
        this.props.fetchFilter(filter);
      }
    }

    render() {
      let style =
        this.props.horizontalLayout && this.props.horizontalLayout === true
          ? "form-field--split"
          : "margin";
      const ComboboxFilter = getComboboxInputFilter(style);
      return (
        <ComboboxFilter
          data={this.props.data}
          onSelect={this.props.onSelect}
          selectedData={this.props.selectedData}
          multiSelect={this.props.multiSelect}
          readonly={this.props.readonly}
          stateKey={selectKey}
          isError={this.props.errorDiv}
          placeholder={this.props.placeholder}
          label={this.props.label ? this.props.label : label}
        />
      );
    }
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchFilter }, dispatch);
  }

  function mapStateToProps(state, ownProps) {
    return {
      data: state.filters[stateName],
      ...ownProps
    };
  }

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(BaseFilter);
}
