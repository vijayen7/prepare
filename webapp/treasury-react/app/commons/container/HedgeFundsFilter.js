import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { getComboboxFilter } from 'commons/components/ComboboxFilter'
import { fetchHedgeFundsFilter } from '../actions/filters';

class HedgeFundsFilter extends Component {
  componentDidMount() {
    if (this.props.data.length <= 0) this.props.fetchHedgeFundsFilter();
  }

  render() {
    const ComboboxFilter = getComboboxFilter();
    return (
      <ComboboxFilter
        data={this.props.data}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        multiSelect
        stateKey="selectedLegalEntityFamilies"
        label="Legal Entitiy Families"
      />
    );
  }
}

HedgeFundsFilter.propTypes = {
  data: PropTypes.array,
  fetchHedgeFundsFilter: PropTypes.func,
  onSelect: PropTypes.func,
  selectedData: PropTypes.array
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchHedgeFundsFilter }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    data: state.filters.hedgeFunds,
    ...ownProps
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  HedgeFundsFilter
);
