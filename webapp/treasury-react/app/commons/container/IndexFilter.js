import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { INDEX_FILTER } from "../constants";

const IndexFilter = props => {
  var label = (typeof props.label !== "undefined") ? props.label : "Index";
  var stateKey = (typeof props.stateKey !== "undefined") ? props.stateKey : "selectedIndex";
  var FilterComponent = getFilterContainer(
    INDEX_FILTER,
    "index",
    stateKey,
    label
  );

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default IndexFilter;
