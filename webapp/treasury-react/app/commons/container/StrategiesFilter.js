import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { STRATEGIES_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  STRATEGIES_FILTER,
  "strategies",
  "selectedStrategies",
  "Strategies"
);

const StrategiesFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      showTokens = {props.showTokens}
      maxTokenHeight = {props.maxTokenHeight}
    />
  );
};

export default StrategiesFilter;
