import React from "react";
import { getFilterContainer } from "./DynamicFilterWrapper";
import { REHYPOTHECATION_RULE_TYPE } from "../constants";

const FilterComponent = getFilterContainer(
  REHYPOTHECATION_RULE_TYPE,
  "rehypothecationRuleTypes",
  "selectedRehypothecationRuleTypes",
  "Type"
);

const RehypothecationRuleTypeFilter  = props => {
  const payload = {
    attribute: props.attribute
  };
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      label={props.label}
      payload={payload}
    />
  );
};

export default RehypothecationRuleTypeFilter;
