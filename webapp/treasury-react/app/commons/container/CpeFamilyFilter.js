import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { CPE_FAMILY_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  CPE_FAMILY_FILTER,
  "cpeFamilies",
  "selectedCpeFamilies",
  "CPE Families"
);

const CpeFamilyFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
       showTokens = {props.showTokens}
      maxTokenHeight = {props.maxTokenHeight}
    />
  );
};

export default CpeFamilyFilter;
