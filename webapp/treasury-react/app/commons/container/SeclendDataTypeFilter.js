import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { SECLEND_DATA_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
    SECLEND_DATA_TYPE_FILTER,
  "seclendDataTypes",
  "selectedSeclendDataTypes",
  "Seclend Data Types"
);

const SeclendDataTypeFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
    />
  );
};

export default SeclendDataTypeFilter;