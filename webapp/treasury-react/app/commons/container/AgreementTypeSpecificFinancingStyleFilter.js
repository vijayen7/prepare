import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { AGREEMENT_TYPE_SPECIFIC_FINANCING_STYLE_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  AGREEMENT_TYPE_SPECIFIC_FINANCING_STYLE_FILTER,
  'agreementTypeSpecificFinancingStyles',
  'selectedFinancingStyles',
  'Financing Style'
);

const AgreementTypeSpecificFinancingStyleFilter = (props) => {
  const payload = {
    agreementId: props.agreementId,
    financingTypeId: props.financingTypeId,
    financingStyleId: props.financingStyleId
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      payload={payload}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AgreementTypeSpecificFinancingStyleFilter;
