import React, { Component } from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import Dropzone from "react-dropzone";
import { toSizeFormatted } from "../util.js";
import styles from "./../../../../css/seclend/DropzoneInput.css";
import noop from "lodash/noop";

const MAX_FILE_SIZE = 10 * (1 << 20);
const MAX_FILE_NAME_LENGTH = 100;

export default class DropzoneInput extends Component {
  static propTypes = {
    name: PropTypes.string,
    value: PropTypes.shape({
      accepted: PropTypes.array.isRequired,
      rejected: PropTypes.array.isRequired,
      rejectedReasons: PropTypes.instanceOf(Map).isRequired
    }),
    /** Callback that gets called on change */
    onChange: PropTypes.func.isRequired,
    /** Callback that gets called on Focus */
    onFocus: PropTypes.func.isRequired,
    /** Callback that gets called on blur */
    onBlur: PropTypes.func.isRequired,
    /** Message  */
    accept: PropTypes.string
  };

  static defaultProps = {
    value: {
      accepted: [],
      rejected: [],
      rejectedReasons: new Map()
    },
    onChange: noop,
    onFocus: noop,
    onBlur: noop
  };

  handleDrop = dropped => {
    const { value: files, onChange, onFocus, onBlur } = this.props;
    const validated = toValidated(dropped, files);

    onChange({
      accepted: [...files.accepted, ...validated.accepted],
      rejected: [...files.rejected, ...validated.rejected],
      rejectedReasons: new Map([...files.rejectedReasons, ...validated.rejectedReasons])
    });
    onFocus();
    onBlur();
  };

  render() {
    const { name, value: files, accept } = this.props;

    return (
      <div>
        <Dropzone
          data-testid="dropzone-input"
          accept={accept}
          name={name}
          onDrop={this.handleDrop}
          className={styles.dropzoneDisableDefaultStyles}
        >
          {({ isDragActive }) => {
            const classes = cn(styles.dropzone, {
              [styles.dropzone_active]: isDragActive
            });

            return (
              <div data-testid="drop-zone" className={classes}>
                <i className="icon-attach" /> Click or drag and drop to add an attachment.
              </div>
            );
          }}
        </Dropzone>
      </div>
    );
  }
}

function toValidated(dropped, files) {
  return dropped.reduce(
    (validated, file) => {
      if (file.size > MAX_FILE_SIZE) {
        validated.rejected.push(file);
        validated.rejectedReasons.set(file, `${toSizeFormatted(MAX_FILE_SIZE)} size limit`);
      } else if (file.name.length > MAX_FILE_NAME_LENGTH) {
        validated.rejected.push(file);
        validated.rejectedReasons.set(file, `${MAX_FILE_NAME_LENGTH} character name limit`);
      } else {
        validated.accepted.push(file);
      }

      return validated;
    },
    { accepted: [], rejected: [], rejectedReasons: new Map() }
  );
}
