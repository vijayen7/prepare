import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { BUSINESS_UNIT_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  BUSINESS_UNIT_FILTER,
  "businessUnits",
  "selectedBusinessUnits",
  "Business Units"
);

const BusinessUnitFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
    />
  );
};

export default BusinessUnitFilter;
