import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { ACCRUAL_POSTING_BUNDLE_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  ACCRUAL_POSTING_BUNDLE_FILTER,
  'accrualPostingBundles',
  'selectedBundles',
  'Bundle'
);

const AccrualPostingBundleFilter = (props) => {
  const payload = {
    chargeTypeId: props.parameters.chargeTypeFilterIds,
    businessUnitId: props.parameters.businessUnitFilterIds
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      payload={payload}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AccrualPostingBundleFilter;
