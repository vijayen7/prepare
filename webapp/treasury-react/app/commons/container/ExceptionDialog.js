import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { resetExceptionHandler } from "../actions";

class ExceptionDialog extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.dialog.addEventListener("stateChange", this.handleChange);
    }

    componentDidUpdate() {
        if (this.props.isException == true) {
            this.dialog.visible = true;
        }
        else {
            this.dialog.visible = false;
        }
    }

    handleChange = e => {        
        if(!e.detail.visible){
            this.props.resetExceptionHandler();
        }
    }

    render() {
        return (
            <arc-error-dialog ref={
                dialog => {
                    this.dialog = dialog;
                }
            }></arc-error-dialog>
        );
    }
}

function mapStateToProps({ isException }) {
    return { isException };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
        resetExceptionHandler
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(ExceptionDialog);
