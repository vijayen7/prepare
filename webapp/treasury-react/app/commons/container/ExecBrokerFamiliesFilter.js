import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { EXEC_BROKER_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  EXEC_BROKER_FILTER,
  "execBrokerFamilies",
  "selectedExecBrokerFamilies",
  "Exec Broker Families"
);

const ExecBrokerFamiliesFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      label = {props.label}
      multiSelect={props.multiSelect}
    />
  );
};

export default ExecBrokerFamiliesFilter;
