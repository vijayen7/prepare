import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { INTERACTION_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  INTERACTION_TYPE_FILTER,
  "interactionTypes",
  "selectedInteractionTypes",
  "Interaction Types"
);

const InteractionTypeFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
    />
  );
};

export default InteractionTypeFilter;
