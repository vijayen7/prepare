import React from "react";
import { getFilterContainer } from './DynamicFilterWrapper';
import { REGION_SPECIFIC_COUNTRY_FILTER } from "../constants";

var FilterComponent = getFilterContainer(
    REGION_SPECIFIC_COUNTRY_FILTER,
    "regionSpecificCountries",
    "selectedCountries",
    "Country1"
  );

const RegionSpecificCountryFilter = props => {
  var label = (typeof props.label !== "undefined") ? props.label : "Country";
  var stateKey = (typeof props.stateKey !== "undefined") ? props.stateKey : "selectedCountries";

  const payload = {
    regionId: props.regionId
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      payload={payload}
      label={label}
      selectKey={stateKey}
    />
  );
};

export default RegionSpecificCountryFilter;
