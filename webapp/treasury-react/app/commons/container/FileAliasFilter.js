import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { FILE_ALIAS_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  FILE_ALIAS_FILTER,
  "fileAliases",
  "selectedFileAliases",
  "File Aliases"
);

const LegalEntityFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
      label={props.multiSelect ?  "File Aliases" : "File Alias"}
    />
  );
};

export default LegalEntityFilter;
