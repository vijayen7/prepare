import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Dialog } from "arc-react-components";

class LockedWorkflow extends Component {
  constructor(props) {
    super(props);
    this.hideDialog = this.hideDialog.bind(this);
    this.state = { showDialog: false };
  }

  hideDialog() {
    console.log("hide called");
    this.setState({ showDialog: false });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      nextProps.workflowStatus.hasOwnProperty("resultList") &&
      nextProps.workflowStatus.resultList[0].workflowState ===
      "LOCKED_WORKFLOW" && nextProps.workflowStatus.resultList[0].message != undefined &&
      nextProps.workflowStatus.resultList[0].message != "Workflow resumed"
    ) {
      this.setState({ showDialog: true });
    }
  }

  render() {
    if (this.state.showDialog == true) {
      return (
        <Dialog
          isOpen={this.state.showDialog}
          title="Workflow Conflict"
          onClose={this.hideDialog}
        >
          {this.props.workflowStatus.resultList[0].message}
        </Dialog>
      );
    }
    return null;
  }
}

function mapStateToProps(state) {
  return {
    workflowStatus: state.workflow.workflowStatus
  };
}

export default connect(
  mapStateToProps,
  null
)(LockedWorkflow);
