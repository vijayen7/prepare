import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getComboboxFilter } from "commons/components/ComboboxFilter"
import { fetchFinancingChargeTypeFilter } from "../actions/filters";

class FinancingChargeTypeFilter extends Component {
  componentDidMount() {
    if (this.props.data.length <= 0) this.props.fetchFinancingChargeTypeFilter();
  }

  render() {
    const ComboboxFilter = getComboboxFilter();
    return (
      <ComboboxFilter
          data={this.props.data}
          onSelect={this.props.onSelect}
          selectedData={this.props.selectedData}
          multiSelect={false}
          stateKey="selectedFinancingChargeType"
          label = "Financing Type"
        />
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchFinancingChargeTypeFilter }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    data: state.filters.financingChargeTypes,
    ...ownProps
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  FinancingChargeTypeFilter
);
