import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class RateNegotiationVerificationStatusFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[
          { key: "MATCH", value: "Match" },
          { key: "MISSING", value: "Missing" },
          { key: "BREAK", value: "Break" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedRateNegotiationVerificationStatus"
        label="Verification Status"
      />
    );
  }
}

RateNegotiationVerificationStatusFilter.propTypes = {
  data: PropTypes.array,
  onSelect: PropTypes.func,
  selectedData: PropTypes.array,
  stateKey: PropTypes.string,
  label: PropTypes.string
};
