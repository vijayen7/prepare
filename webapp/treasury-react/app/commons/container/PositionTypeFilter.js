import React, { Component } from "react";
import { connect } from "react-redux";
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class PositionTypeFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[
          { key: 0, value: "Long" },
          { key: 3, value: "Lend" },
          { key: 1, value: "Borrow-Fee" },
          { key: 2, value: "Borrow-Rebate" },
          { key: 4, value: "Short" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedPositionTypes"
        label="Position Type"
      />
    );
  }
}
