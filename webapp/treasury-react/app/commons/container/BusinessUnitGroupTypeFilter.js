import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { BUSINESS_UNIT_GROUP_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  BUSINESS_UNIT_GROUP_TYPE_FILTER,
  "businessUnitGroupTypes",
  "selectedBusinessUnitGroupTypes",
  "Business Unit Group Types"
);

const BusinessUnitGroupTypeFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
    />
  );
};

export default BusinessUnitGroupTypeFilter;
