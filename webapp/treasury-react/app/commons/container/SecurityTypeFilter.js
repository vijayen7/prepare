import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { SECURITY_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  SECURITY_TYPE_FILTER,
  "securityTypes",
  "selectedSecurityTypes",
  "Security Type"
);

const SecurityTypeFilter  = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      label={props.label}
    />
  );
};

export default SecurityTypeFilter;
