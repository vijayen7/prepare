import React, { Component } from "react";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

export default class TopNFilter extends Component {
  render() {
    return (
      <SingleSelectFilter
        data={[
          { key: 10, value: "10" },
          { key: 20, value: "20" },
          { key: 30, value: "30" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedTopNRecords"
        label="Top N"
      />
    );
  }
}
