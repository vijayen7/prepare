import React, { Component } from "react";
import { connect } from "react-redux";
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class BorrowTypeFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[
          { key: 0, value: "Borrow" },
          { key: 1, value: "Lend" },
          { key: 2, value: "Rehype" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedBorrowType"
        label="Borrow Type"
      />
    );
  }
}
