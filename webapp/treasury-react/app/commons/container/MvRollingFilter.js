import React, { Component } from "react";
import { connect } from "react-redux";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

export default class MvRollingFilter extends Component {
  render() {
    return (
      <SingleSelectFilter
        data={[
          { key: 7, value: "7" },
          { key: 14, value: "14" },
          { key: 30, value: "30" },
          { key: 60, value: "60" },
          { key: 90, value: "90" },
          { key: 180, value: "180" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedMVRolling"
        label="MV Rolling in next"
      />
    );
  }
}
