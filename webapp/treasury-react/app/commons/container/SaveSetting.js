import React, { Component } from "react";
import FilterButton from "../components/FilterButton";

export default class SaveSetting extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleCopySearch = this.handleCopySearch.bind(this);
    this.state = { selectedFilters: {} };
  }

  componentDidMount() {
    this.setState({ selectedFilters: this.props.selectedFilters });
    var host = "";
    if (CODEX_PROPERTIES["codex.client_name"] === "desco") {
      if (CODEX_PROPERTIES["codex.stability_level"] == "prod") {
    	  host += "http://landing-app.deshaw.c.ia55.net";
      } else if (CODEX_PROPERTIES["codex.stability_level"] == "uat") {
    	  host += "http://landing-app.deshawuat.c.ia55.net";
      } else if (CODEX_PROPERTIES["codex.stability_level"] == "qa") {
    	  host += "https://mars.arcesium.com";
      } else if (CODEX_PROPERTIES["codex.stability_level"] == "dev") {
    	  host += "https://terra.arcesium.com";
      }
    }

    this.saveSetting.serviceURL = host + "/service/SettingsService";
    this.saveSetting.applicationId = 3;
    this.saveSetting.applicationCategory = this.props.applicationName;
    this.saveSetting.retrieveSettings();
    var self = this;
    this.saveSetting.saveSettingsCallback = function(isLinkButton) {
      return self.state.selectedFilters;
    };
    var props = this.props;
    this.saveSetting.applySettingsCallback = function(
      parameters,
      isLinkButton
    ) {
      props.applySavedFilters(parameters);
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ selectedFilters: nextProps.selectedFilters });
  }

  handleChange = e => {
    this.saveSetting.openSaveDialog();
  };

  handleCopySearch() {
    this.saveSetting.createShareableLink();
  }

  render() {
    return (
      <div>
        <arc-panel-element>
          <div className="layout--flex margin--vertical">
            <arc-save-settings
              generate-links
              ref={saveSetting => {
                this.saveSetting = saveSetting;
              }}
            />
            <div className="size--content margin--horizontal">
              <button
                id="summary-search-save"
                className="button--tertiary size--content"
                onClick={this.handleChange}
              >
                <i className="icon-save" id="save-button" size="content" />
              </button>
            </div>
          </div>
        </arc-panel-element>
        <hr />
        <arc-panel-element>
          <FilterButton
            reset={true}
            onClick={this.handleCopySearch}
            label="Copy Search URL"
          />
        </arc-panel-element>
      </div>
    );
  }
}
