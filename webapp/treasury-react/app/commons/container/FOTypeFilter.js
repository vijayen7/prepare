import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { FO_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  FO_TYPE_FILTER,
  "foTypes",
  "selectedFoTypes",
  "FO Types"
);

const FOTypeFilter = props => {

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      label={props.label}
      stateKey={props.stateKey}
    />
  );
};

export default FOTypeFilter;
