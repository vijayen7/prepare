import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { CURRENCY_SPECIFIC_BASE_RATE_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  CURRENCY_SPECIFIC_BASE_RATE_FILTER,
  'baseRates',
  'selectedBaseRates',
  'Base Rate'
);

const CurrencySpecificBaseRateFilter = (props) => {
  const payload = {
    currencyId: props.currencyId
  };
  const label = typeof props.label !== 'undefined' ? props.label : 'Base Rate';
  const stateKey = typeof props.stateKey !== 'undefined' ? props.stateKey : 'selectedBaseRates';

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      payload={payload}
      label={label}
      selectKey={stateKey}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default CurrencySpecificBaseRateFilter;
