import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { ASSET_CLASS_GROUP_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  ASSET_CLASS_GROUP_FILTER,
  "assetClassGroups",
  "selectedAssetClassGroups",
  "Asset Class Groups"
);

const AssetClassGroupFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      multiSelect={props.multiSelect}
      label={"Asset Class Groups"}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AssetClassGroupFilter;
