import React from "react";
import { getFilterContainer } from "../container/ComboboxInputFilterWrapper";
import { ADD_RULE_FORM_RULE_NAME_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  ADD_RULE_FORM_RULE_NAME_FILTER,
  "addRuleFormRuleName",
  "selectedRule",
  "Rule Name"
);

const RuleNameFilter = props => {
  let label = typeof props.label !== "undefined" ? props.label : "Rule Name";
  let selectKey =
    typeof props.selectKey !== "undefined" ? props.selectKey : "selectedRule";

  const payload = {
    ruleConfigTypeId: props.selectedRuleConfigType.key
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      payload={payload}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      label={label}
      selectKey={selectKey}
    />
  );
};

export default RuleNameFilter;
