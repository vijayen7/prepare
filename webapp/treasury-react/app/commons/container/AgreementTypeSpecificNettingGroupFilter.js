import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { AGREEMENT_TYPE_SPECIFIC_NETTING_GROUP_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  AGREEMENT_TYPE_SPECIFIC_NETTING_GROUP_FILTER,
  'agreementTypeSpecificNettingGroups',
  'selectedNettingGroups',
  'Netting Group'
);

const AgreementTypeSpecificNettingGroupFilter = (props) => {
  const payload = {
    agreementTypeFilterIds: props.agreementTypeFilterIds
  };
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      payload={payload}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AgreementTypeSpecificNettingGroupFilter;
