import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { OPTION_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  OPTION_TYPE_FILTER,
  "optionTypes",
  "selectedOptionType",
  "Option Type"
);

const OptionTypeFilter = props => {

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      singleSelect={props.singleSelect}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      label={props.label}
      stateKey={props.stateKey}
      readonly={props.readonly}
    />
  );
};

export default OptionTypeFilter;
