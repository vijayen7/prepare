import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getComboboxFilter } from "commons/components/ComboboxFilter"
import FOTypeFilter from "commons/container/FOTypeFilter";
import { fetchSubtypeFilter } from "../actions/filters";
import { getCommaSeparatedValues, getKeysAsArray } from "commons/util";

class TypeSubtypeFilter extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.onTypeSelect = this.onTypeSelect.bind(this);
  }

  onSelect(params) {
    const { key, value } = params;
    let newSelectedData = Object.assign({}, this.props.selectedData);
    newSelectedData[key] = value;
    newSelectedData["selectedSubtypes"] = _.filter(
      newSelectedData["selectedSubtypes"],
      selectedSubtype => {
        let selectedFoTypeIds = getKeysAsArray(newSelectedData["selectedFoTypes"]);
        return selectedFoTypeIds && selectedFoTypeIds.includes(selectedSubtype["foTypeId"]);
      }
    );
    this.props.onSelect({ key: "selectedTypeSubtype", value: newSelectedData });
  }

  onTypeSelect(params) {
    const { key, value } = params;
    this.props.fetchSubtypeFilter(getCommaSeparatedValues(value));
    this.onSelect(params);
  }

  render() {
    const ComboboxFilter = getComboboxFilter();
    let subtypefilter = null;
    if (this.props.selectedData.selectedFoTypes.length > 0) {
      subtypefilter = (
        <ComboboxFilter
          data={this.props.subtypes}
          onSelect={this.onSelect}
          selectedData={this.props.selectedData.selectedSubtypes}
          multiSelect={true}
          stateKey="selectedSubtypes"
          label = "Subtypes"
        />
      );
    }
    return (
      <React.Fragment>
        <FOTypeFilter
          onSelect={this.onTypeSelect}
          selectedData={this.props.selectedData.selectedFoTypes}
          stateKey="selectedFoTypes"
        />
        {subtypefilter}
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchSubtypeFilter }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    subtypes: state.filters.subtypes,
    ...ownProps
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  TypeSubtypeFilter
);
