import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { COUNTRY_CODE_FILTER } from "../constants";

const CountryCodeFilter = props => {
  var label = (typeof props.label !== "undefined") ? props.label : "Issuer Country";
  var stateKey = (typeof props.stateKey !== "undefined") ? props.stateKey : "selectedCountryCodes";
  var FilterComponent = getFilterContainer(
    COUNTRY_CODE_FILTER,
    "countryCodes",
    stateKey,
    label
  );

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default CountryCodeFilter;
