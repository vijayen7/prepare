import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { GBO_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  GBO_TYPE_FILTER,
  "gboTypes",
  "selectedGboTypes",
  "GBO Types"
);

const GBOTypeFilter = props => {

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      singleSelect={props.singleSelect}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      label={props.label}
      stateKey={props.stateKey}
      readonly={props.readonly}
      disabled={props.disabled}
    />
  );
};

export default GBOTypeFilter;
