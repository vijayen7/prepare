import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { COUNTRY_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  COUNTRY_FILTER,
  "countries",
  "selectedCountries",
  "Countries"
);

const CountryFilter = props => {

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      stateKey={props.stateKey}
      label={props.label}
      horizontalLayout={props.horizontalLayout}
       showTokens = {props.showTokens}
      maxTokenHeight = {props.maxTokenHeight}
    />
  );
};

export default CountryFilter;
