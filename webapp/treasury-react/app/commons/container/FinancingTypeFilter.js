

import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { FINANCING_TYPE_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  FINANCING_TYPE_FILTER,
  "financingTypes",
  "selectedFinancingTypes",
  "Financing Types"
);

const FinancingTypeFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
    />
  );
};

export default FinancingTypeFilter;
