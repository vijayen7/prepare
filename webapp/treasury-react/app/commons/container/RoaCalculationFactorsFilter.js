import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchRoaCalculationFactorFilter } from "../actions/filters";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

class RoaCalculationFactorFilter extends Component {
  componentDidMount() {
    if (this.props.data.length <= 0) this.props.fetchRoaCalculationFactorFilter();
  }

  render() {
    return (
      <SingleSelectFilter
        data={this.props.data}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedKnobType"
        label="Calculation Factor"
      />
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchRoaCalculationFactorFilter }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    data: state.filters.roaCalculationFactor,
    ...ownProps
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  RoaCalculationFactorFilter
);
