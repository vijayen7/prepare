import React, { Component } from "react";
import { connect } from "react-redux";
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class ClassificationFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[
          { key: 1, value: "ETB" },
          { key: 2, value: "HTB" },
          { key: 3, value: "VHTB" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedClassification"
        label="Classification"
      />
    );
  }
}
