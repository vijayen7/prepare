import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { NETTING_GROUP_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  NETTING_GROUP_FILTER,
  "nettingGroups",
  "selectedNettingGroups",
  "Netting Groups"
);

const NettingGroupFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default NettingGroupFilter;
