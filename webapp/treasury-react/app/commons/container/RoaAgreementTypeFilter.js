import React, { Component } from "react";
import { connect } from "react-redux";
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class RoaAgreementTypeFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[
          { key: 2, value: "PB" },
          { key: 6, value: "ISDA" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedAgreementTypes"
        label="Agreement Type"
      />
    );
  }
}
