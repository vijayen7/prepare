import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { EXEC_BROKER_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  EXEC_BROKER_FILTER,
  "execBrokers",
  "selectedExecBrokers",
  "Exec Brokers"
);

const ExecBrokerFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
    />
  );
};

export default ExecBrokerFilter;
