import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { CUSTODIAN_ACCOUNT_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  CUSTODIAN_ACCOUNT_FILTER,
  "custodianAccounts",
  "selectedCustodianAccounts",
  "Custodian Account"
);

const CustodianAccountFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      singleSelect={props.singleSelect}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      horizontalLayout={props.horizontalLayout}
      errorDiv={props.errorDiv}
      label = {props.label}
      disabled={props.disabled}
    />
  );
};

export default CustodianAccountFilter;
