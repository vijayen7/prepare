import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {getComboboxFilter} from "commons/components/ComboboxFilter"
import { fetchWorkflowTypeFilter } from "../actions/filters";

class WorkflowTypeFilter extends Component {
  componentDidMount() {
    if (this.props.data.length <= 0) this.props.fetchWorkflowTypeFilter();
  }

  render() {
    const ComboboxFilter = getComboboxFilter();
    return (
      <ComboboxFilter
          data={this.props.data}
          onSelect={this.props.onSelect}
          selectedData={this.props.selectedData}
          multiSelect={true}
          stateKey="selectedWorkflowTypes"
          label = "Workflow Type"
        />
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchWorkflowTypeFilter }, dispatch);
}

function mapStateToProps(state, ownProps) {
  return {
    data: state.filters.workflowTypes,
    ...ownProps
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  WorkflowTypeFilter
);
