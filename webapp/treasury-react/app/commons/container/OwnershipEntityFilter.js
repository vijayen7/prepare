import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { OWNERSHIP_ENTITY_FILTER } from "../constants";

const FilterComponent = getFilterContainer(
  OWNERSHIP_ENTITY_FILTER,
  "ownershipEntities",
  "selectedOwnershipEntities",
  "Ownership Entities"
);

const OwnershipEntityFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
    />
  );
};

export default OwnershipEntityFilter;
