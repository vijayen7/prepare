import React, { Component } from "react";
import { DatePicker } from 'arc-react-components';
import PropTypes from 'prop-types';

export default class ArcDateFilter extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.refs.date;
  }

  handleChange = e => {
    var returnValue = {
      key: this.props.stateKey,
      value: e
    };
    console.log(returnValue)
    this.props.onSelect(returnValue);
  };

  loadLabel(layout, label) {
    if (layout === "standard") {
      return (<div><label title={label}>{label}</label></div>);
    }
    return (<label title={label}>{label}</label>);
  }

  render() {
    const style = (this.props.layout === "standard") ? "margin" : "form-field--split";
    return (
      <div className={style}>
        {this.loadLabel(this.props.layout, this.props.label)}
        <DatePicker
          placeholder="Select Date"
          className={this.props.isError ? "error" : ""}
          value={this.props.data}
          onChange={this.handleChange}
          style={this.props.style}
          disabled={this.props.readonly}
          renderInBody
        />
      </div>
    );
  }
}

ArcDateFilter.propTypes = {
  stateKey: PropTypes.string,
  layout: PropTypes.string,
  label: PropTypes.string,
  isError: PropTypes.bool,
  readonly: PropTypes.bool,
};
