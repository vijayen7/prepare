import React from 'react';
import { getFilterContainer } from './FilterWrapper';
import { FINANCING_STYLE_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  FINANCING_STYLE_FILTER,
  'financingStyles',
  'selectedFinancingStyles',
  'Financing Style'
);

const AllFinancingStylesFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      readonly={props.readonly}
      multiSelect={props.multiSelect}
      label="Financing Style"
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AllFinancingStylesFilter;
