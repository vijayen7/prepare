import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER,
  'agmtNettingGroupSpecificCustodianAccounts',
  'selectedCustodianAccounts',
  'Custodian Account'
);

const AgmtNettingGroupSpecificCustodianAccountsFilter = (props) => {
  const payload = {
    nettingGroupFilterId: props.nettingGroupIds,
    agreementTypeFilterId: props.agreementIds
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      payload={payload}
    />
  );
};

export default AgmtNettingGroupSpecificCustodianAccountsFilter;
