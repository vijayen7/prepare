import React from 'react';
import { getFilterContainer } from './FilterWrapper';
import { ACCRUAL_CONVENTION_FILTER } from '../constants';

const AccrualConventionFilter = (props) => {
  const label = typeof props.label !== 'undefined' ? props.label : 'Accrual Convention';
  const stateKey = typeof props.stateKey !== 'undefined' ? props.stateKey : 'selectedAccrualConventions';
  const FilterComponent = getFilterContainer(
    ACCRUAL_CONVENTION_FILTER,
    'accrualConvention',
    stateKey,
    label
  );

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      readonly={props.readonly}
      multiSelect={props.multiSelect}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AccrualConventionFilter;
