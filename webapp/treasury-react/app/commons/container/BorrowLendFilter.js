import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class BorrowLendFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[{ key: "BORROW", value: "Borrow" },
               { key: "LEND", value: "Lend" }]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedBorrowType"
        label="Data Type"
      />
    );
  }
}

BorrowLendFilter.propTypes = {
  data: PropTypes.array,
  onSelect: PropTypes.func,
  selectedData: PropTypes.array,
  stateKey: PropTypes.string,
  label: PropTypes.string
};
