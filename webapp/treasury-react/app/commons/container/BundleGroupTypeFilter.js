import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { BUNDLE_GROUP_TYPE_FILTER } from "../constants";
const FilterComponent = getFilterContainer(
  BUNDLE_GROUP_TYPE_FILTER,
  "bundleGroupTypes",
  "selectedBundleGroupTypes",
  "Bundle Group Type"
);

const BundleGroupTypeFilter = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={false}
    />
  );
};

export default BundleGroupTypeFilter;
