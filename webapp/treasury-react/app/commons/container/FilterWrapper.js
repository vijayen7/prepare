import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchFilter } from "../actions";

import { getComboboxInputFilter } from "../components/ComboBoxInputFilter";
import ReactComboBoxFilter from "../components/ReactComboBoxFilter";

export function getFilterContainer(
  filterType,
  stateName,
  selectKey,
  label,
  payload = {}
) {
  class BaseFilter extends Component {
    componentDidMount() {
      if (this.props.data.length <= 0) {
        var filter = {
          key: filterType,
          filterData: payload,
        };
        this.props.fetchFilter(filter);
      }
    }

    render() {
      let style =
        this.props.horizontalLayout && this.props.horizontalLayout === true
          ? "form-field--split"
          : "margin";
      let ComboboxFilter;
      if (this.props.singleSelect) {
        // getComboboxInputFilter is used when single select values are given and need to be cleared.
        // This uses the arc-react-components
        ComboboxFilter = getComboboxInputFilter(style);
      } else {
        ComboboxFilter = ReactComboBoxFilter;
      }

      return this.props.singleSelect ? (
        <ComboboxFilter
          data={this.props.data}
          selectedData={this.props.selectedData}
          onSelect={this.props.onSelect}
          isError={this.props.errorDiv}
          label={this.props.label ? this.props.label : label}
          stateKey={this.props.stateKey ? this.props.stateKey : selectKey}
          placeholder={this.props.placeholder ? this.props.placeholder : ""}
          readonly={this.props.readonly}
          multiSelect={this.props.multiSelect}
        />
      ) : (
        <ComboboxFilter
          data={this.props.data}
          onSelect={this.props.onSelect}
          selectedData={this.props.selectedData}
          multiSelect={this.props.multiSelect}
          readonly={this.props.readonly}
          stateKey={this.props.stateKey ? this.props.stateKey : selectKey}
          errorDiv={this.props.errorDiv}
          label={this.props.label ? this.props.label : label}
          style={style}
          showTokens={this.props.showTokens}
          maxTokenHeight={this.props.maxTokenHeight}
          disabled={this.props.disabled}
        />
      );
    }
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchFilter }, dispatch);
  }

  function mapStateToProps(state, ownProps) {
    return {
      data: state.filters[stateName],
      ...ownProps,
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(BaseFilter);
}
