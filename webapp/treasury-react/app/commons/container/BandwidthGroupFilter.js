import React from 'react';
import { getFilterContainer } from './FilterWrapper';
import { BANDWIDTH_GROUP_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  BANDWIDTH_GROUP_FILTER,
  'bandwidthGroup',
  'selectedBandwidthGroups',
  'Bandwidth Group'
);

const BandwidthGroupFilter = (props) => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      readonly={props.readonly}
      multiSelect={props.multiSelect}
      label="Bandwidth Group"
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default BandwidthGroupFilter;
