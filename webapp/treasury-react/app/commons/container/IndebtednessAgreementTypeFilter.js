import React, { Component } from "react";
import MultiSelectFilter from "commons/components/MultiSelectFilter";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

export default class IndebtednessAgreementTypeFilter extends Component {
  render() {
    if (this.props.multiSelect == false) {
      return (
        <SingleSelectFilter
          data={[
            { key: 2, value: "PB" },
            { key: 20, value: "MNA" }
          ]}
          onSelect={this.props.onSelect}
          selectedData={this.props.selectedData}
          stateKey="selectedAgreementTypes"
          label="Agreement Type"
        />
      );
    } else {
      return (
        <MultiSelectFilter
          data={[
            { key: 2, value: "PB" },
            { key: 20, value: "MNA" }
          ]}
          onSelect={this.props.onSelect}
          selectedData={this.props.selectedData}
          stateKey="selectedAgreementTypes"
          label="Agreement Type"
          style="margin"
        />
      );
    }
  }
}
