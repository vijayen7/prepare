import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { CALCULATOR_GROUP_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  CALCULATOR_GROUP_FILTER,
  'calculatorGroups',
  'selectedCalculatorGroups',
  'Calculator Groups'
);

const CalculatorGroupFilter = (props) => {
  var payload = {
    agreementTypeFilterIds: props.agreementTypeFilterIds,
    nettingGroupFilterIds: props.nettingGroupFilterIds
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      payload={payload}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default CalculatorGroupFilter;
