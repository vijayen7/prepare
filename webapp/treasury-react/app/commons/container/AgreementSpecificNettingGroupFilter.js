import React from 'react';
import { getFilterContainer } from './DynamicFilterWrapper';
import { AGREEMENT_SPECIFIC_NETTING_GROUP_FILTER } from '../constants';

const FilterComponent = getFilterContainer(
  AGREEMENT_SPECIFIC_NETTING_GROUP_FILTER,
  'agreementSpecificNettingGroups',
  'selectedNettingGroups',
  'Netting Groups'
);

const AgreementSpecificNettingGroupFilter = (props) => {
  const payload = {
    agreementId: props.agreementId
  };

  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      payload={payload}
      horizontalLayout={props.horizontalLayout}
    />
  );
};

export default AgreementSpecificNettingGroupFilter;
