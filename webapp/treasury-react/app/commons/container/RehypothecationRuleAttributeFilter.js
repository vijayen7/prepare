import React from "react";
import { getFilterContainer } from "./FilterWrapper";
import { REHYPOTHECATION_RULE_ATTRIBUTE } from "../constants";

const FilterComponent = getFilterContainer(
  REHYPOTHECATION_RULE_ATTRIBUTE,
  "rehypothecationRuleAttributes",
  "selectedRehypothecationRuleAttributes",
  "Attribute Name"
);

const RehypothecationRuleAttributeFilter  = props => {
  return (
    <FilterComponent
      onSelect={props.onSelect}
      selectedData={props.selectedData}
      multiSelect={props.multiSelect}
      readonly={props.readonly}
      errorDiv={props.errorDiv}
      horizontalLayout={props.horizontalLayout}
      label={props.label}
    />
  );
};

export default RehypothecationRuleAttributeFilter;
