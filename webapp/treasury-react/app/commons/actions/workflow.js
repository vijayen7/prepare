import {
  BEGIN_WORKFLOW,
  RESUME_WORKFLOW,
  EXECUTE_WORKFLOW,
  CANCEL_AND_BEGIN_WORKFLOW
} from "../constants";

export function beginWorkflow(payload, params) {
  return {
    type: BEGIN_WORKFLOW,
    payload,
    params
  };
}

export function resumeWorkflow(payload) {
  return {
    type: RESUME_WORKFLOW,
    payload
  };
}

export function executeWorkflow(payload) {
  return {
    type: EXECUTE_WORKFLOW,
    payload
  };
}

export function cancelAndBeginWorkflow(payload) {
  return {
    type: CANCEL_AND_BEGIN_WORKFLOW,
    payload
  };
}
