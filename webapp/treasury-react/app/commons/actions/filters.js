import {
  FETCH_FILTER,
  FETCH_BROKER_REVENUE_SIGN_OFF_YEAR,
  FETCH_DATE_FILTER,
  WORKFLOW_TYPE_FILTER,
  FINANCING_RATE_FILTER,
  REASON_CODE_FILTER,
  ADJUSTMENT_TYPE_FILTER,
  FETCH_HEDGE_FUNDS_FILTER,
  FINANCING_CHARGE_TYPE_FILTER,
  FETCH_SUBTYPE_FILTER,
  FETCH_ROA_CALCULATION_FACTOR_YEARS,
  ROA_CALCULATION_FACTOR_FILTER
} from "../constants";

export function fetchFilter(payload) {
  return {
    type: FETCH_FILTER,
    payload
  };
}

export function fetchBrokerRevenueYear() {
  return {
    type: FETCH_BROKER_REVENUE_SIGN_OFF_YEAR
  };
}

export function fetchDateFilter() {
  return {
    type: FETCH_DATE_FILTER
  };
}

export function fetchWorkflowTypeFilter() {
  return {
    type: WORKFLOW_TYPE_FILTER
  };
}


export function fetchReasonCodeFilter(payload) {
  return {
    type: REASON_CODE_FILTER,
    payload
  };
}
export function fetchAdjustmentTypeFilter(payload) {
  return {
    type: ADJUSTMENT_TYPE_FILTER,
    payload
  };
}

export function fetchHedgeFundsFilter() {
  return {
    type: FETCH_HEDGE_FUNDS_FILTER
  };
}

export function fetchFinancingChargeTypeFilter() {
  return {
    type: FINANCING_CHARGE_TYPE_FILTER
  };
}

export function fetchSubtypeFilter(payload) {
  return {
    type: FETCH_SUBTYPE_FILTER,
    payload
  };
}

export function fetchRoaCalculationFactorYearFilter() {
  return {
    type: FETCH_ROA_CALCULATION_FACTOR_YEARS
  };
}

export function fetchRoaCalculationFactorFilter() {
  return {
    type: ROA_CALCULATION_FACTOR_FILTER
  };
}
