import {
  UPDATE_DRILL_DOWN_SELECTED_FILTERS,
  FETCH_FILTER,
  HANDLE_EXCEPTION_RESET,
  RESIZE_CANVAS
} from '../constants';

export function fetchFilter(payload) {
  return {
    type: FETCH_FILTER,
    payload
  };
}

export function updateDrillDownSelectedFilters(payload) {
  return {
    type: UPDATE_DRILL_DOWN_SELECTED_FILTERS,
    payload
  };
}

export function resetExceptionHandler() {
  return {
    type: HANDLE_EXCEPTION_RESET
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}
