import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  FETCH_FILTER,
  START_LOADING,
  END_LOADING,
  FETCH_BROKER_REVENUE_SIGN_OFF_YEAR,
  BROKER_REVENUE_SIGN_OFF_YEAR,
  FETCH_DATE_FILTER,
  DATE_FILTER,
  WORKFLOW_TYPE_FILTER,
  FINANCING_RATE_FILTER,
  REASON_CODE_FILTER,
  ADJUSTMENT_TYPE_FILTER,
  FETCH_HEDGE_FUNDS_FILTER,
  BEGIN_WORKFLOW,
  RESUME_WORKFLOW,
  EXECUTE_WORKFLOW,
  CANCEL_AND_BEGIN_WORKFLOW,
  HANDLE_EXCEPTION,
  FINANCING_CHARGE_TYPE_FILTER,
  AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER,
  FETCH_SUBTYPE_FILTER,
  ROA_CALCULATION_FACTOR_FILTER
} from "../constants";
import { getFilter } from "../api/filters";
import { beginWorkflow, resumeWorkflow, executeWorkflow, cancelAndBeginWorkflow } from "../api/workflow";

function* fetchFilter(action) {
  try {
    const key = action.payload.key;
    yield put({ type: START_LOADING });
    const data = yield call(getFilter, action.payload);
    yield put({
      type: `${FETCH_FILTER}_SUCCESS`,
      key,
      data
    });
  } catch (e) {
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchBrokerRevenueYear(action) {
  try {
    yield put({ type: START_LOADING });
    const signOffYear = yield call(getFilter, {
      key: BROKER_REVENUE_SIGN_OFF_YEAR
    });
    yield put({
      type: `${FETCH_BROKER_REVENUE_SIGN_OFF_YEAR}_SUCCESS`,
      signOffYear
    });
  } catch (e) {
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchDateFilter(action) {
  try {
    yield put({ type: START_LOADING });
    const date = yield call(getFilter, {
      key: DATE_FILTER
    });
    yield put({
      type: `${FETCH_DATE_FILTER}_SUCCESS`,
      date
    });
  } catch (e) {
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchWorkflowTypeFilter(action) {
  try {
    yield put({ type: START_LOADING });
    const workflowTypes = yield call(getFilter, {
      key: WORKFLOW_TYPE_FILTER

    });
    yield put({
      type: `${WORKFLOW_TYPE_FILTER}_SUCCESS`,
      data: workflowTypes
    });
  } catch (e) {
  } finally {
    yield put({ type: END_LOADING });
  }
}
function* fetchHedgeFundsFilter(action) {
  try {
    yield put({ type: START_LOADING });
    const hedgeFunds = yield call(getFilter, {
      key: FETCH_HEDGE_FUNDS_FILTER
    });
    yield put({
      type: `${FETCH_HEDGE_FUNDS_FILTER}_SUCCESS`,
      data: hedgeFunds
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchReasonCodeFilter(action) {
  try {
    const adjustmentType = action.payload;
    yield put({ type: START_LOADING });
    const reasonCodes = yield call(getFilter, {
      key: REASON_CODE_FILTER,
      adjustmentType: adjustmentType
    });
    yield put({
      type: `${REASON_CODE_FILTER}_SUCCESS`,
      data: reasonCodes
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${BEGIN_WORKFLOW}_FAILURE` });

  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchFinancingChargeTypeFilter(action) {
  try {
    yield put({ type: START_LOADING });
    const financingChargeTypes = yield call(getFilter, {
      key: FINANCING_CHARGE_TYPE_FILTER
    });
    yield put({
      type: `${FINANCING_CHARGE_TYPE_FILTER}_SUCCESS`,
      data: financingChargeTypes
    });
  } catch (e) {
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchAgmtNettingGroupSpecificCustodianAccountFilter(action) {
  try {
    yield put({ type: START_LOADING });
    const agmtNettingGroupSpecificCustodianAccounts = yield call(getFilter, {
      key: AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER
    });
    yield put({
      type: `${AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER}_SUCCESS`,
      data: agmtNettingGroupSpecificCustodianAccounts
    });
  } catch (e) {
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* beginWorkflowSaga(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(beginWorkflow, action.payload);
    let params = action.params;
    yield put({ type: `${BEGIN_WORKFLOW}_SUCCESS`, data, params });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${BEGIN_WORKFLOW}_FAILURE` });

  } finally {
    yield put({ type: END_LOADING });
  }
}


function* resumeWorkflowSaga(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(resumeWorkflow, action.payload);
    yield put({ type: `${RESUME_WORKFLOW}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${RESUME_WORKFLOW}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* executeWorkflowSaga(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(executeWorkflow, action.payload);
    yield put({ type: `${EXECUTE_WORKFLOW}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${EXECUTE_WORKFLOW}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchAdjustmentTypeFilter(action) {
  try {
    yield put({ type: START_LOADING });
    const adjustmentTypes = yield call(getFilter, {
      key: ADJUSTMENT_TYPE_FILTER
    });
    yield put({
      type: `${ADJUSTMENT_TYPE_FILTER}_SUCCESS`,
      data: adjustmentTypes,
      payload: action.payload
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${CANCEL_AND_BEGIN_WORKFLOW}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* cancelAndBeginWorkflowSaga(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(cancelAndBeginWorkflow, action.payload);
    yield put({ type: `${CANCEL_AND_BEGIN_WORKFLOW}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${CANCEL_AND_BEGIN_WORKFLOW}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSubtypeFilter(action) {
  try {
    yield put({ type: START_LOADING });
    const subtypes = yield call(getFilter, {
      key: FETCH_SUBTYPE_FILTER,
      foTypeIds: action.payload
    });
    yield put({
      type: `${FETCH_SUBTYPE_FILTER}_SUCCESS`,
      data: subtypes
    });
  } catch (e) {
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchRoaCalculationFactor(action) {
  try {
    yield put({ type: START_LOADING });
    const knobTypes = yield call(getFilter, {
      key: ROA_CALCULATION_FACTOR_FILTER
    });
    yield put({
      type: `${ROA_CALCULATION_FACTOR_FILTER}_SUCCESS`,
      data: knobTypes,
      payload: action.payload
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${CANCEL_AND_BEGIN_WORKFLOW}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* filterRootSaga() {
  yield [
    takeEvery(FETCH_FILTER, fetchFilter),
    takeEvery(FETCH_DATE_FILTER, fetchDateFilter),
    takeEvery(WORKFLOW_TYPE_FILTER, fetchWorkflowTypeFilter),
    takeEvery(REASON_CODE_FILTER, fetchReasonCodeFilter),
    takeEvery(ADJUSTMENT_TYPE_FILTER, fetchAdjustmentTypeFilter),
    takeEvery(BEGIN_WORKFLOW, beginWorkflowSaga),
    takeEvery(RESUME_WORKFLOW, resumeWorkflowSaga),
    takeEvery(EXECUTE_WORKFLOW, executeWorkflowSaga),
    takeEvery(CANCEL_AND_BEGIN_WORKFLOW, cancelAndBeginWorkflowSaga),
    takeEvery(FETCH_HEDGE_FUNDS_FILTER, fetchHedgeFundsFilter),
    takeEvery(FINANCING_CHARGE_TYPE_FILTER, fetchFinancingChargeTypeFilter),
    takeEvery(AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER, fetchAgmtNettingGroupSpecificCustodianAccountFilter),
    takeEvery(FETCH_SUBTYPE_FILTER, fetchSubtypeFilter),
    takeEvery(ROA_CALCULATION_FACTOR_FILTER, fetchRoaCalculationFactor)
  ];
}

function* commonRootSaga() {
  yield all([filterRootSaga()]);
}

export default commonRootSaga;
