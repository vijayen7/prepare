import React from "react";
import ReferenceData from "./models/ReferenceData";
import { decamelize, parseDJSrvString } from "./util";
import { formatFinancial } from 'arc-commons/utils/number';
import { NOT_AVAILABLE } from "./constants";
import MapData from "./models/MapData";
import moment from "moment";
import _ from "lodash";

export const genericFormatter = (
  identity: any,
  header?: string,
  width?: number,
  prefix?: string) => {
  let formattedGenericColumn: any = {
    identity: prefix !== undefined ? prefix + identity : identity,
    Cell: (value) => <>{(value !== null && value !== undefined) ? value : undefined}</>,
  };
  if (header) {
    formattedGenericColumn.header = prefix !== undefined ? decamelize(prefix + header, " ") : header;
  }
  if (width) {
    formattedGenericColumn.width = width;
  }
  return formattedGenericColumn;
};

export const genericBooleanFormatter = (
  identity: any,
  header?: string,
  width?: number,
  prefix?: string) => {
  let formattedBooleanColumn: any = {
    identity: prefix !== undefined ? prefix + identity : identity,
    Cell: (value) => <>{(value !== null && value !== undefined) ? value === true ?
      <i className="icon-fw icon-accept"></i> : <i className="icon-fw icon-reject"></i> : undefined}</>,
  };
  if (header) {
    formattedBooleanColumn.header = prefix !== undefined ? decamelize(prefix + header, " ") : header;
  }
  if (width) {
    formattedBooleanColumn.width = width;
  }
  return formattedBooleanColumn;
};

export const getFormattedDateColumn = (
  identity: any,
  header?: string,
  width?: number,
  prefix?: string) => {
  let formatedDateColumn: any = {
    identity: prefix !== undefined ? prefix + identity : identity,
    Cell: (value) => <>{(value !== null && value !== undefined) ? getParsedDate(value) : undefined}</>,
  };
  if (header) {
    formatedDateColumn.header = prefix !== undefined ? decamelize(prefix + header, " ") : header;
  }
  if (width) {
    formatedDateColumn.width = width;
  }
  return formatedDateColumn;
};

/* getFormattedFinancialNumericColumn (identity: any, suffix?: string, header?: string):

  This Util is used to initialise the numeric column with identity and display header.
  The numeric data of these column will be formatted using format financial and
  will be right aligned. If suffix is passed, it would be appended to the numeric data.

  Ex: getFormattedFinancialNumericColumn("id1")
      If the data corresponding to id1 is
      100.1234" - > "100.12"
      1000.123456 -> "1,000.123456"

      getFormattedFinancialNumericColumn("id1", "%")
      If the data corresponding to id1 is
      100.1234" - > "100.12%""
      1000.123456 -> "1,000.123456%"
*/
export const getFormattedFinancialNumericColumn = (
  identity: any,
  suffix?: string,
  header?: string
  ) => {

  if(suffix == undefined || suffix == null) {
    suffix = "";
  }
  let formattedNumericColumn: any = {
    identity: identity,
    type: 'number',
    Cell: (value) => (
      <div className="text-align--right">{(value !== null && value !== undefined)
        ? formatFinancial(value) + suffix : undefined}</div>
    ),
    AggregatedCell: (value: any) => <div className="text-align--right">{validateAndGetFormattedText(value)}</div>,
  };

  if (header) {
    formattedNumericColumn.header = header;
  }

  return formattedNumericColumn;
};

export const getFormattedNumericColumn = (
  identity: any,
  header?: string,
  width?: number,
  prefix?: string,
  roundOffDigits?: number) => {
  let formattedNumericColumn: any = {
    identity: prefix !== undefined ? prefix + identity : identity,
    type: 'number',
    Cell: (value) => (
      <div className="text-align--right">{(value !== null && value !== undefined)
        ? validateAndGetFormattedText(value, roundOffDigits) : undefined}</div>
    ),
    AggregatedCell: (value: any) => <div className="text-align--right">{validateAndGetFormattedText(value, roundOffDigits)}</div>,
  };
  if (header) {
    formattedNumericColumn.header = prefix !== undefined ? decamelize(prefix + header, " ") : header;
  }
  if (width) {
    formattedNumericColumn.width = width;
  }
  return formattedNumericColumn;
};

export const getFormattedPercentColumn = (
  identity: any,
  header?: string,
  width?: number,
  prefix?: string) => {
  let formatedPercentColumn: any = {
    identity: prefix !== undefined ? prefix + identity : identity,
    type: 'number',
    Cell: (value) => (
      <>
        {value === undefined || value === null
          ? undefined
          : String(formatFinancial(value)) + "%"}
      </>
    ),
  };
  if (header) {
    formatedPercentColumn.header = prefix !== undefined ? decamelize(prefix + header, " ") : header;
  }
  if (width) {
    formatedPercentColumn.width = width;
  }
  return formatedPercentColumn;
};

export const getFormattedPriceColumn = (
  identity: any,
  header?: string,
  width?: number,
  prefix?: string) => {
  let formatedPriceColumn: any = {
    identity: prefix !== undefined ? prefix + identity : identity,
    type: 'number',
    Cell: (value) => <>{(value !== null && value !== undefined) ? formatFinancial(value) : undefined}</>,
  };
  if (header) {
    formatedPriceColumn.header = prefix !== undefined ? decamelize(prefix + header, " ") : header;
  }
  if (width) {
    formatedPriceColumn.width = width;
  }
  return formatedPriceColumn;
};

export const priceFormatter = (identity: any, header?: string, width?: number) => {
  let formatedNumericColumn: any = {
    identity: identity,
    Cell: (value) => <>{(value !== null && value !== undefined) ? formatFinancial(value) : undefined}</>,
  };
  if (header) {
    formatedNumericColumn.header = header;
  }
  if (width) {
    formatedNumericColumn.width = width;
  }
  return formatedNumericColumn;
};

export const percentFormatter = (identity: any, header?: string, width?: number) => {
  let formatedNumericColumn: any = {
    identity: identity,
    Cell: (value) => (
      <>
        {value === undefined || value === null
          ? undefined
          : String(formatFinancial(value)) + "%"}
      </>
    ),
  };
  if (header) {
    formatedNumericColumn.header = header;
  }
  if (width) {
    formatedNumericColumn.width = width;
  }
  return formatedNumericColumn;
};

export const getFormattedStringToInteger = (
  identity: any,
  header?: string,
  width?: number,
  prefix?: string) => {
  let formattedTextColumn: any = {
    identity: prefix !== undefined ? prefix + identity : identity,
    Cell: (value) => (
      <>{(value !== null && value !== undefined) ? validateAndGetFormattedText(value) : undefined}</>
    )
  };
  if (header) {
    formattedTextColumn.header = prefix !== undefined ? decamelize(prefix + header, " ") : header;
  }
  if (width) {
    formattedTextColumn.width = width;
  }
  return formattedTextColumn;
};

export const validateAndGetFormattedText = (value, roundOffDigits = 0, defaultText = NOT_AVAILABLE) => {
  return isValidNumber(value)
    ? convertNumberWithCommas(parseFloat(value).toFixed(roundOffDigits))
    : defaultText;
};

export const isValidNumber = (value) => {
  return value !== undefined && !isNaN(parseFloat(value));
};

export const getNameFromRefData = (referenceData?: ReferenceData) => {
  return referenceData?.name ? referenceData.name : NOT_AVAILABLE;
};

export const getIdFromRefData = (referenceData?: ReferenceData) => {
  return referenceData?.id ? referenceData.id : 0;
};

export const getAbbrevFromRefData = (referenceData?: ReferenceData) => {
  return referenceData?.abbrev ? referenceData.abbrev : NOT_AVAILABLE;
};

// Convert the date in unixTimeStamp to yyyy-mm-dd format
export const getParsedDate = (date?: string) => {
  const parsedDate = parseDJSrvString(date);
  return parsedDate ? parsedDate.toISOString().substring(0, 10) : NOT_AVAILABLE;
};

export const getIdFromMapData = (data: MapData | null) => {
  return data ? data.key : undefined;
};

export const getNumberOfWeekDaysBetweenTwoDates = (startDate: string, endDate: string) => {
  var day = moment(startDate);
  var businessDays = 0;

  while(day.isSameOrBefore(endDate,'day')) {
    if (day.day()!= 0 && day.day()!= 6) {
      businessDays++;
    }
    day.add(1,'d');
  }
  return businessDays;
};

export const handleResponse = (response: any) => {
  return response.json().then(data => ({
    data: data,
    status: response.status
  }))
};

/* convertNumberWithCommas (value: string):
  The regex uses 2 lookahead assertions:
  1) a positive one to look for any point in the string that has a multiple of 3 digits in a row
  2) a negative assertion to make sure that point only has exactly a multiple of 3 digits.
  The replacement expression puts a comma there.
  The \B keeps the regex from putting a comma at the beginning of the string.
  (?<!\.\d*) is a negative lookbehind that says the match can't be preceded by a "."
  followed by zero or more digits.

  Ex: 1000.123456 -> "1,000.123456"
      10000       -> "10,000"
*/
export const convertNumberWithCommas = (value: string, defaultText = NOT_AVAILABLE) => {
  if(isValidNumber(value)) {
    return value.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
  }
  return defaultText;
}

/* converts the given minutes in HH:MM format */
export const getHHmmTime = (minutes: number) => {
  if (minutes >= 0) {
    let timeString = "";
    timeString = ("0" + Math.floor(minutes / 60).toString())
      .slice(-2)
      .concat(":")
      .concat(("0" + (minutes % 60).toString()).slice(-2));
    return timeString;
  }
  return null;
};


