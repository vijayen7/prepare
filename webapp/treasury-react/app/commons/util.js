import moment from "moment";
import { ToastService } from 'arc-react-components';
import { REPORT_MGR_URL, BASE_URL } from "./constants";

const dateRegex = /\\\/Date\((-?\d+)\)\\\//;

export function parseDJSrvString(input) {
  if (input && input.match(dateRegex)) {
    const matches = input.match(dateRegex);
    const timestamp = parseInt(matches[1], 10);
    return moment.unix(timestamp / 1000);
  }
  return null;
}

export function generateReportUrl(apiUrl, saveSettingFormat) {
  apiUrl = "https://" + window.location.hostname + apiUrl;
  let reportUrl = `${REPORT_MGR_URL}` + "?url=" + encodeURIComponent(apiUrl)
  if(saveSettingFormat != null){
    reportUrl += "&format=" + encodeURIComponent(saveSettingFormat);
  }
  return reportUrl
}

export function openReportManager(apiUrl , saveSettingFormat) {
  let reportUrl = generateReportUrl(apiUrl, saveSettingFormat);
  window.open(reportUrl);
}

export function getReportingUrl(url) {
  return `${BASE_URL}service/${url}`;
}

export function getPreviousDate() {
  let date = new Date();
  date.setDate(date.getDate() - 1);
  return date.toISOString().substring(0, 10);
}

export function getCurrentDate() {
  var date = new Date();
  return date.toISOString().substring(0, 10);
}

export function getCurrentMonth() {
  return moment().format("MM/YYYY");
}

export function isDateToday(date) {
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1; //January is 0!

  let yyyy = today.getFullYear();
  if (dd < 10) {
    dd = "0" + dd;
  }
  if (mm < 10) {
    mm = "0" + mm;
  }
  today = yyyy + "" + mm + "" + dd;
  return date === today;
}

export function getFirstDateOfCurrentMonth() {
  var date = new Date();
  var firstDate = new Date(date.getFullYear(), date.getMonth(), 2);
  return firstDate.toISOString().substring(0, 10);
}

export function getFirstDateOfMonthForGivenDate(date) {
  var firstDate = new Date(date.getFullYear(), date.getMonth(), 2);
  return firstDate.toISOString().substring(0, 10);
}

export function getPreviousBusinessDay() {
  const date = new Date();
  date.setDate(date.getDate() - 1);
  if (date.getDay() === 6) {
    // Saturday, move to Friday
    date.setDate(date.getDate() - 1);
  } else if (date.getDay() === 0) {
    // Sunday, move 2 days back to Friday
    date.setDate(date.getDate() - 2);
  }
  return date.toISOString().substring(0, 10);
}

export function getMinusOneBusinessDay(date) {
  date.setDate(date.getDate() - 1);
  if (date.getDay() === 6) {
    // Saturday, move to Friday
    date.setDate(date.getDate() - 1);
  } else if (date.getDay() === 0) {
    // Sunday, move 2 days back to Friday
    date.setDate(date.getDate() - 2);
  }
  return date.toISOString().substring(0, 10);
}

export function getNextBusinessDay() {
  let date = new Date();
  date.setDate(date.getDate() + 1);
  if (date.getDay() === 6) {
    // Saturday, move to Monday
    date.setDate(date.getDate() + 2);
  } else if (date.getDay() === 0) {
    // Sunday, move to Monday
    date.setDate(date.getDate() + 1);
  }
  return date.toISOString().substring(0, 10);
}

export function getDateDifference(startDate, endDate) {
  let start = moment(startDate, "YYYYMMDD");
  let end = moment(endDate, "YYYYMMDD");
  return end.diff(start, "days");
}

export function getStartDateForDifference(endDate, difference) {
  let end = moment(endDate, "YYYYMMDD");
  let start = end.subtract(difference, "d");
  return start.format("YYYYMMDD");
}

export function convertArrayToFilterObjects(dataArray, sort = false) {
  if (sort) {
    let dataList = _.map(dataArray, (data) => {
      return {
        key: data[0],
        value: data[1],
      };
    });
    return _.orderBy(
      dataList,
      [(dataList) => dataList.value.toLowerCase()],
      ["asc"]
    );
  }
  return _.map(dataArray, (data) => {
    return {
      key: data[0],
      value: data[1],
    };
  });
}

export function convertJsonArrayToFilterObjects(
  jsonArray,
  keyOfDataKey,
  keyOfDataValue,
  sort = false
) {
  if (sort) {
    let dataList = jsonArray.map((data) => {
      return { key: data[keyOfDataKey], value: data[keyOfDataValue] };
    });
    return _.orderBy(
      dataList,
      [(dataList) => dataList.value.toLowerCase()],
      ["asc"]
    );
  } else {
    return jsonArray.map((data) => {
      return { key: data[keyOfDataKey], value: data[keyOfDataValue] };
    });
  }
}

export function getCommaSeparatedValues(data) {
  let returnValue = _.join(
    _.map(data, (e) => e.key),
    ","
  );
  if (returnValue === "") return "-1";
  return returnValue;
}

export function getCommaSeparatedValuesOrUndefined(data) {
  let returnValue = _.join(
    _.map(data, (e) => e.key),
    ","
  );
  if (returnValue === "") return undefined;
  return returnValue;
}

function _removeFromList(list, elementToBeRemoved) {
  for (var i = 0; i < list.length; i++) {
    if (list[i][0] === elementToBeRemoved) {
      list.splice(i, 1);
      break;
    }
  }
  return list;
}

export function getKeysAsArray(data) {
  return _.isEmpty(data) ? null : data.map((a) => a.key);
}

export function getSelectedIdList(data) {
  return _.isEmpty(data) ? [-1] : data.map((a) => (a === -1 ? -1 : a.key));
}
export function getSelectedIdListWithNull(data) {
  return _.isEmpty(data) ? null : data.map((a) => (a === -1 ? -1 : a.key));
}

export function getTruncatedMessage(message) {
  const regexForException = new RegExp('^.*Exception:');
  if (regexForException.test(message)) {
    const truncatedMessage = message.replace(regexForException, '');
    if (truncatedMessage) {
      message = truncatedMessage.trim();
    }
  }
  return message;
}

export function getCommaSeparatedValuesOrNull(data) {
  let returnValue = _.join(
    _.map(data, (e) => e.key),
    ","
  );
  if (returnValue === "") return "__null__";
  return returnValue;
}

export function getCommaSeparatedValuesOrEmpty(data) {
  let returnValue = _.join(
    _.map(data, (e) => e.key),
    ","
  );
  return returnValue;
}

export function getCommaSeparatedValuesOrNullForAll(data) {
  let returnValue = _.join(
    _.map(data, (e) => e.key),
    ","
  );
  if (returnValue === "" || returnValue === "-1") return "__null__";
  return returnValue;
}

export function getCommaSeparatedValuesOrNullForSingleSelect(data) {
  let returnValue = data.key;
  if (returnValue === "" || returnValue === -1) return "__null__";
  return returnValue;
}

export function getCommaSeparatedValuesOrEmptyForSingleSelect(data) {
  let returnValue = data.key;
  if (returnValue === "" || returnValue === -1) return "null";
  return returnValue;
}

export function fetchURL(url) {
  return fetch(url, {
    credentials: "include",
  }).then((data) => data.json());
}

export function getEncodedParams(payload, className) {
  payload["@CLASS"] = className;
  const jsonString = JSON.stringify(payload);
  return encodeURI(jsonString);
}

export function fetchPostURL(url, param) {
  return fetch(url, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    method: "POST",
    credentials: "include",
    body: param,
  }).then((data) => data.json());
}

export function fetchJSONPostURL(url, param) {
  return fetch(url, {
    headers: {
      "Content-Type": "application/json; charset=UTF-8",
    },
    method: "POST",
    credentials: "include",
    body: param,
  }).then((data) => data.json());
}


export function convertJavaDate(str) {
  if (str === null || str === undefined || str.trim() === "" || str === "Total")
    return "";
  let res = str.match(/\((.*)\)/);
  if (res != undefined && res != null && res.length > 1) {
    let deserializedDate = parseInt(res[1]);
    let date = new Date(deserializedDate),
      mnth = `0${date.getMonth() + 1}`.slice(-2),
      day = `0${date.getDate()}`.slice(-2);
    return [date.getFullYear(), mnth, day].join("");
  } else return "";
}

export function formatterFourDecimal(row, cell, value, columnDef, dataContext) {
  return (
    "<div class='aln-rt'>" +
    (value == null
      ? "<span class='message'>n/a</span>"
      : Number(value).toFixed(4)) +
    "</div>"
  );
}

export function convertJavaDateDashed(str) {
  if (str === null || str === undefined || str.trim() === "" || str === "Total")
    return "";
  var res = str.match(/\((.*)\)/);
  var deserializedDate = parseInt(res[1]);
  var date = new Date(deserializedDate),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
  return [date.getFullYear(), mnth, day].join("-");
}

export function convertJavaDateTime(str) {
  if (str === null || str === undefined || str.trim() === "" || str === "Total")
    return "";
  let res = str.match(/\((.*)\)/);
  let deserializedDate = parseInt(res[1]);
  let date = new Date(deserializedDate),
    year = date.getFullYear(),
    mnth = `0${date.getMonth() + 1}`.slice(-2),
    day = `0${date.getDate()}`.slice(-2),
    hours = `0${date.getHours()}`.slice(-2),
    minutes = `0${date.getMinutes()}`.slice(-2),
    seconds = `0${date.getSeconds()}`.slice(-2);
  return `${year}-${mnth}-${day} ${hours}:${minutes}:${seconds}`;
}

export function getYearRangeFilterDataFromEndYear(endYear) {
  let startYear = 2013;
  let filterArray = [];
  while (startYear <= endYear) {
    filterArray.unshift({
      key: startYear.toString(),
      value: startYear.toString(),
    });
    startYear++;
  }
  return filterArray;
}

export function getSelectedFilterArray(valueString) {
  let filterArray = [];
  valueString.split(",").forEach((value) => {
    filterArray.push({
      key: parseInt(value),
      value,
    });
  });

  return filterArray;
}

export function getArrayFromString(valueString) {
  let filterArray = [];
  valueString.split(",").forEach((value) => {
    filterArray.push(value);
  });

  return filterArray;
}

function getSelectedFilterArrayForSingleSelect(valueString) {
  let arr = valueString.split(",");
  return [
    {
      key: parseInt(arr[0]),
      value: arr[1],
    },
  ];
}

export function getIntegerArrayFromObjectArray(data) {
  var filterArray = [];
  data.forEach(item => {
    filterArray.push(item.key);
  });
  return filterArray;
}

export function isAllKeySelected(data, allKey = -1) {
  const keys = getKeysAsArray(data);
  return keys && keys.includes(allKey);
}

export function getCodexFilters(filterName) {
  switch (filterName) {
    case "LEGAL_ENTITY_FAMILIES":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.summary.legal_entity_families"
      ] === "-1"
        ? []
        : getSelectedFilterArray(
            REPORTS_CODEX_PROPERTIES[
              "treasury.portal.summary.legal_entity_families"
            ]
          );

    case "BUSINESS_UNIT":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.summary.business_unit"
      ] === "-1"
        ? []
        : getSelectedFilterArray(
            REPORTS_CODEX_PROPERTIES["treasury.portal.summary.business_unit"]
          );

    case "BUSINESS_UNIT_GROUP":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.summary.business_unit_group"
      ] === "-1"
        ? []
        : getSelectedFilterArray(
            REPORTS_CODEX_PROPERTIES[
              "treasury.portal.summary.business_unit_group"
            ]
          );

    case "LEGAL_ENTITIES":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.summary.legal_entities"
      ] === "-1"
        ? []
        : getSelectedFilterArray(
            REPORTS_CODEX_PROPERTIES["treasury.portal.summary.legal_entities"]
          );

    case "OWNERSHIP_ENTITIES":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.summary.ownership_entities"
      ] === "-1"
        ? []
        : getSelectedFilterArray(
            REPORTS_CODEX_PROPERTIES[
              "treasury.portal.summary.ownership_entities"
            ]
          );

    case "ROLLING_DAYS":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.summary.rolling_days"
      ] === "-1"
        ? []
        : getSelectedFilterArray(
            REPORTS_CODEX_PROPERTIES["treasury.portal.summary.rolling_days"]
          );

    case "CPE_FAMILIES_BROKER_REVENUE":
      return REPORTS_CODEX_PROPERTIES["treasury.portal.broker.cpe_families"] ===
        "-1"
        ? []
        : getSelectedFilterArrayForSingleSelect(
            REPORTS_CODEX_PROPERTIES["treasury.portal.broker.cpe_families"]
          );

    case "BUSINESS_UNIT_GROUP_BROKER_REVENUE":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.broker.business_unit_group"
      ] === "-1"
        ? [[]]
        : getSelectedFilterArrayForSingleSelect(
            REPORTS_CODEX_PROPERTIES[
              "treasury.portal.broker.business_unit_group"
            ]
          );
    case "SIMULATION_CPES":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.defaults.simulation.counterPartyEntities"
      ] === "-1"
        ? []
        : REPORTS_CODEX_PROPERTIES[
            "treasury.portal.defaults.simulation.counterPartyEntities"
          ];
    case "SIMULATION_AGREEMENT_TYPES":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.defaults.simulation.agreementTypes"
      ] === "-1"
        ? []
        : REPORTS_CODEX_PROPERTIES[
            "treasury.portal.defaults.simulation.agreementTypes"
          ];
    case "CPE_FAMILIES":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.defaults.cpe_families"
      ] === "-1"
        ? []
        : getSelectedFilterArray(
            REPORTS_CODEX_PROPERTIES["treasury.portal.defaults.cpe_families"]
          );
    case "DATE":
      return REPORTS_CODEX_PROPERTIES["treasury.portal.summary.date"];

    case "YEAR":
      return REPORTS_CODEX_PROPERTIES["treasury.portal.summary.year"] === "-1"
        ? [{}]
        : getSelectedFilterArray(
            REPORTS_CODEX_PROPERTIES["treasury.portal.summary.year"]
          );

    case "START_MONTH":
      return REPORTS_CODEX_PROPERTIES["treasury.portal.summary.start_month"] ===
        "-1"
        ? "01/2019"
        : REPORTS_CODEX_PROPERTIES["treasury.portal.summary.start_month"];

    case "END_MONTH":
      return REPORTS_CODEX_PROPERTIES["treasury.portal.summary.end_month"] ===
        "-1"
        ? "12/2019"
        : REPORTS_CODEX_PROPERTIES["treasury.portal.summary.end_month"];

    case "DISABLE_FOOTNOTE":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.summary.disable_footnote"
      ];
    case "CLASSIFICATION":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.defaults.simulation.classification"
      ] === "-1"
        ? []
        : REPORTS_CODEX_PROPERTIES[
            "treasury.portal.defaults.simulation.classification"
          ];
    case "CURRENCIES":
      return REPORTS_CODEX_PROPERTIES[
        "treasury.portal.defaults.simulation.ccySpns"
      ] === "-1"
        ? []
        : REPORTS_CODEX_PROPERTIES[
            "treasury.portal.defaults.simulation.ccySpns"
          ];
    default:
      return [];
  }
}

export function getSaveSettingsUrl() {
  let host = "";
  if (CODEX_PROPERTIES["codex.stability_level"] === "dev") {
    if (
      CODEX_PROPERTIES["codex.client_name"] === "terra" ||
      CODEX_PROPERTIES["codex.client_name"] === "desco"
    ) {
      host = "http://landing-app.terra.c.ia55.net";
    }
  }
  return `${host}/service/SettingsService`;
}

export function convertJavaNYCDateTime(str) {
  if (str===undefined||str==null||str.trim() === "") return "";
  const res = str.match(/\((.*)\)/);
  const deserializedDate = parseInt(res[1]);
  const nycDateTime = new Date(deserializedDate)
    .toLocaleString("en-US", { timeZone: "America/New_York" })
    .replace(",", "");

  const nycDateTimeArray = nycDateTime.split(" ");
  const date = nycDateTimeArray[0];
  const time = nycDateTimeArray[1];
  const mmddyyyy = date.split("/");
  const year = mmddyyyy[2];
  // Adding a leading zero for month and day incase of single digit value
  const mnth = ("0" + mmddyyyy[0]).slice(-2);
  const day = ("0" + mmddyyyy[1]).slice(-2);

  return `${year}-${mnth}-${day} ${time}`;
}

export function convertJavaNYCDashedDate(str) {
  if (str===undefined||str==null||str.trim() === "" || str === "Total" || str === "Summary") return "";
  const res = str.match(/\((.*)\)/);
  const deserializedDate = parseInt(res[1]);
  const nycDateTime = new Date(deserializedDate)
    .toLocaleString("en-US", { timeZone: "America/New_York" })
    .replace(",", "");

  const nycDateTimeArray = nycDateTime.split(" ");
  const date = nycDateTimeArray[0];
  const mmddyyyy = date.split("/");
  const year = mmddyyyy[2];
  const mnth = ("0" + mmddyyyy[0]).slice(-2);
  const day = ("0" + mmddyyyy[1]).slice(-2);

  return `${year}-${mnth}-${day}`;
}

export function convertJavaNYCDate(str) {
  if (str===undefined||str==null||str.trim() === "" || str === "Total") return "";
  const res = str.match(/\((.*)\)/);
  const deserializedDate = parseInt(res[1]);
  const nycDateTime = new Date(deserializedDate)
    .toLocaleString("en-US", { timeZone: "America/New_York" })
    .replace(",", "");

  const nycDateTimeArray = nycDateTime.split(" ");
  const date = nycDateTimeArray[0];
  const mmddyyyy = date.split("/");
  const year = mmddyyyy[2];
  const mnth = ("0" + mmddyyyy[0]).slice(-2);
  const day = ("0" + mmddyyyy[1]).slice(-2);

  return `${year}${mnth}${day}`;
}

export function checkForException(data) {
  if (
    data === undefined ||
    data === null ||
    data["@CLASS"] === "java.rmi.RemoteException"
  ) {
    throw new Error("Exception occured");
  }
}

export function convertTimestampToNycDateString(timestamp) {
  timestamp = Number(timestamp);
  if (Number.isFinite(timestamp)) {
    return Intl.DateTimeFormat("en", { timeZone: "America/New_York" }).format(timestamp);
  }
  return "";
}

export function convertDatetoLocalDate(date) {
  if(date === null || date === undefined)
    return date
  return moment(date).format("YYYY-MM-DD") ;
}

export function convertLocalDatetoDate(date) {
  if(date === null || date === undefined)
    return date
  return moment(date).format("YYYYMMDD") ;
}

export function formatNumberAsMoney(value, includePrefix = true) {
  value = Number(value);

  if (Number.isNaN(value)) {
    return "";
  }

  const prefix = "$";
  const conversionFactors = [
    { conversionFactor: 1000000000, suffix: "B" },
    { conversionFactor: 1000000, suffix: "M" },
    { conversionFactor: 1000, suffix: "K" },
    { conversionFactor: 1, suffix: "" },
  ];

  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var sign = "";
  if (value < 0) sign = "-";

  var i;
  for (i = 0; i < conversionFactors.length - 1; i++) {
    if (Math.abs(value) >= conversionFactors[i].conversionFactor) {
      break;
    }
  }
  var number =
    (Math.abs(value) / conversionFactors[i].conversionFactor)
      .toFixed(2)
      .replace(rx, "$1") + conversionFactors[i].suffix;

  if (includePrefix){
    return `${sign}${prefix}${number}`;
  }
  else{
    return `${sign}${number}`;
  }
}

export function formatNumberAsMoneyWithoutPrefix(value) {
  return formatNumberAsMoney(value, false);
}

function roundHalfEven(number) {
  const integralPart = Math.trunc(number);
  const fractionalPart = Math.abs(number - integralPart);
  if (fractionalPart === 0.5) {
    return integralPart + (integralPart % 2);
  }
  return Math.round(number);
}

export function formatAsPercent(decimal, fixed = 2) {
  return `${decimal.toFixed(fixed)}%`;
}

export function ratedListValue(inputString) {
  if (inputString.trim() !== "") {
    if (inputString[inputString.length - 1] === ";") {
      inputString = inputString.slice(0, -1);
    }
    return inputString.replace(/;/g, ",");
  }
  return "";
}

export function getCommaSeparatedListValue(inputString) {
  if (inputString.trim() !== "") {
    if (inputString[inputString.length - 1] === ";") {
      inputString = inputString.slice(0, -1);
    }
    return inputString.replace(/;/g, ",");
  }
  return "";
}

export function getCommaSeparatedNumber(amount) {
  if (
    amount === undefined ||
    amount === null ||
    amount === "" ||
    amount === "-"
  ) {
    return amount;
  }

  let amt = amount.toString().replace(/,/g, "");
  amt = amt.match(/-?[0-9.]+/g);
  if (amt != null) {
    amt = amt.join("");
  } else {
    return null;
  }
  if (isNaN(Number(amt))) {
    return NaN;
  }
  let lastDot = false;
  if (amt.substr(-1) === ".") {
    lastDot = true;
  }
  let result = new Intl.NumberFormat("en").format(parseStringToFloat(amt, 10));
  if (lastDot) {
    result = result.concat(".");
  }
  return result;
}
export function getCommaSeparatedNegativeOnlyNumber(amount) {
  if (
    amount === undefined ||
    amount === null ||
    amount === "" ||
    amount === "-"
  ) {
    return amount;
  }

  let amt = amount.toString().replace(/,/g, "");
  amt = amt.match(/-?[0-9.]+/g);
  if (amt != null) {
    amt = amt.join("");
  }
  if (isNaN(Number(amt))) {
    return NaN;
  }
  if (amt > 0) {
    return "";
  }
  let lastDot = false;
  if (amt.substr(-1) === ".") {
    lastDot = true;
  }
  let result = new Intl.NumberFormat("en").format(parseStringToFloat(amt, 10));
  if (lastDot) {
    result = result.concat(".");
  }
  return result;
}
export function parseStringToFloat(x, base) {
  const parsed = parseFloat(x, base);
  if (isNaN(parsed)) {
    return 0;
  }
  return parsed;
}

export function getRoaCalculationFactorYearFilter() {
  let date = new Date();
  let startYear = date.getFullYear() - 3;
  let endYear = date.getFullYear() + 3;
  let filterArray = [];
  while (startYear <= endYear) {
    filterArray.unshift({
      key: startYear.toString(),
      value: startYear.toString(),
    });
    startYear++;
  }
  return filterArray;
}

export function decamelize(str, separator) {
  separator = typeof separator === "undefined" ? "_" : separator;

  return str
    .replace(/([a-z\d])([A-Z])/g, "$1" + separator + "$2")
    .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, "$1" + separator + "$2")
    .replace(/(^[a-zA-Z]{1})/, (s) => {
      return s.toUpperCase();
    });
}

export function toSizeFormatted(bytes) {
  if (bytes < 1) {
    return "0B";
  }
  const GB = bytes / (1 << 30);
  const MB = bytes / (1 << 20);
  const KB = bytes / (1 << 10);

  if (GB >= 1) {
    return `${roundHundredth(GB)}G`;
  } else if (MB >= 1) {
    return `${roundHundredth(MB)}M`;
  } else if (KB >= 1) {
    return `${roundHundredth(KB)}K`;
  }
  return `${bytes}B`;
}

export const getHHmmTime = (minutes) => {
  if (minutes >= 0) {
    let timeString = "";
    timeString = ("0" + Math.floor(minutes / 60).toString())
      .slice(-2)
      .concat(":")
      .concat(("0" + (minutes % 60).toString()).slice(-2));
    return timeString;
  }
  return null;
};

export function validateInputEmpty(value) {
  if (value === undefined || value === null || value === "") {
    return true;
  }
  return false;
}

export function TextEditor(args) {
  var $input;
  var defaultValue;
  var scope = this;
  this.init = function () {
    $input = $("<INPUT type=text class='editor-text' />")
      .appendTo(args.container)
      .bind("keydown.nav", function (e) {
        if (
          e.keyCode === $.ui.keyCode.LEFT ||
          e.keyCode === $.ui.keyCode.RIGHT
        ) {
          e.stopImmediatePropagation();
        }
      })
      .focus()
      .select();
  };
  this.destroy = function () {
    $input.remove();
  };
  this.focus = function () {
    $input.focus();
  };
  this.getValue = function () {
    return $input.val();
  };
  this.setValue = function (val) {
    $input.val(val);
  };
  this.loadValue = function (item) {
    defaultValue = item[args.column.field] || "";
    $input.val(defaultValue);
    $input[0].defaultValue = defaultValue;
    $input.select();
  };
  this.serializeValue = function () {
    return $input.val();
  };
  this.applyValue = function (item, state) {
    item[args.column.field] = state;
  };
  this.isValueChanged = function () {
    return (
      !($input.val() == "" && defaultValue == null) &&
      $input.val() != defaultValue
    );
  };
  this.validate = function () {
    return editorCustomValidator(args.column, $input.val());
  };
  this.init();
}

function editorCustomValidator(column, val) {
  if (column.editorOptions && column.editorOptions.validator) {
    var validationResults = column.editorOptions.validator(val);
    if (!validationResults.valid) {
      return validationResults;
    }
  }
  return {
    valid: true,
    msg: null,
  };
}

export function executeCopySearchUrl() {
  let path = window.location.search;
  if (path != "") {
    let copySearchUrlState = JSON.parse(decodeURIComponent(path.slice(1)));
    return {
      isTrue: true,
      copySearchUrlState,
    };
  } else
    return {
      isTrue: false,
      coySearchUrlState: {},
    };
}

export function roundWithAtmostDecimal(value, n) {
  if (
    _.isNumber(value) &&
    !_.isNaN(value) &&
    _.isNumber(n) &&
    !_.isNaN(n) &&
    n > 0 &&
    _.isEqual(n % 1, 0)
  ) {
    return (
      Math.round((value + Number.EPSILON) * Math.pow(10, n)) / Math.pow(10, n)
    );
  }
  return value;
}

export function getCommaSeparatedKeysFromFilterList (inputList) {
  let valueString = "";
  valueString = inputList.map.call(inputList, s => s.key.toString()).toString();
  return valueString;
};

export function getFilterListFromCommaSeparatedString (inputStringObjects) {
  if (inputStringObjects) {
    return inputStringObjects.split(',').map(Number).map(eachValue => ({
      key: eachValue, value: null
    }));
  }
  else {
    return [];
  }
};

export function getFilterKeyList(selectedFilter) {
  if (selectedFilter === null || selectedFilter.length <= 0) return '__null__';
  let filterKeyList = []
  selectedFilter.forEach(element => {
    filterKeyList.push(element['key']);
  });
  return filterKeyList;
};

export function getGlobalFilterFromUrl(urlString){
  let params = new URLSearchParams(urlString);
  const globalFiltersFromUrl = params.get('gf')
  const globalFilterInactiveFromUrl = params.get('gf-inactive')

  return {
    "globalFiltersFromUrl": globalFiltersFromUrl,
    "globalFilterInactiveFromUrl": globalFilterInactiveFromUrl
  }
}

export function showToastService(content, type = ToastService.ToastType.INFO,
  placement = ToastService.Placement.TOP_RIGHT, dismissTime = 2000) {
  ToastService.append({
    content: content,
    type: type,
    placement: placement,
    dismissTime: dismissTime,
  });
};

export function getFormattedFloatValue(value) {
   if (!value) return value
   let formattedValue = Math.round(value).toString();
   if (formattedValue < 0) {
     return "(" + formattedValue.replace(/-/, "") + ")";
   } else {
     return formattedValue;
   }
}
