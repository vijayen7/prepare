import {
  BASE_URL,
  BASE_MOSS_SERVICE_URL,
  LEGAL_ENTITY_FILTER,
  MARGIN_CALCULATOR_FILTER,
  FILE_ALIAS_FILTER,
  OWNERSHIP_ENTITY_FILTER,
  CPE_FAMILY_FILTER,
  LEGAL_ENTITY_FAMILY_FILTER,
  CPE_FILTER,
  BUSINESS_UNIT_FILTER,
  AGREEMENT_TYPE_FILTER,
  ADJUSTMENT_TYPE_FILTER,
  REASON_CODE_FILTER,
  CUSTODIAN_ACCOUNT_FILTER,
  NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER,
  AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER,
  ASSET_LIQUIDITY_FILTER,
  CURRENCY_FILTER,
  NETTING_GROUP_FILTER,
  FINANCING_STATUS_FILTER,
  BROKER_REVENUE_SIGN_OFF_YEAR,
  DATE_FILTER,
  BUSINESS_UNIT_GROUP_TYPE_FILTER,
  SECURITY_TYPE_FILTER,
  BUSINESS_UNIT_GROUP_FILTER,
  BUNDLE_FILTER,
  BUNDLE_GROUP_FILTER,
  BUNDLE_GROUP_TYPE_FILTER,
  BROKER_REVENUE_BUSINESS_UNIT_FILTER,
  EXEC_BROKER_FILTER,
  GBO_TYPE_FILTER,
  INTERACTION_TYPE_FILTER,
  BOOK_FILTER,
  BOOK_FILTER_WITH_SPECIAL_ACCOUNTING_BOOKS,
  WORKFLOW_TYPE_FILTER,
  FETCH_HEDGE_FUNDS_FILTER,
  FINANCING_CHARGE_TYPE_FILTER,
  CHARGE_TYPE_FILTER,
  FINANCING_RATE_FILTER,
  FO_TYPE_FILTER,
  FETCH_SUBTYPE_FILTER,
  SECLEND_DATA_TYPE_FILTER,
  AGREEMENT_TYPE_SPECIFIC_NETTING_GROUP_FILTER,
  AGREEMENT_SPECIFIC_NETTING_GROUP_FILTER,
  CALCULATOR_GROUP_FILTER,
  AGREEMENT_FILTER,
  ASSET_CLASS_GROUP_FILTER,
  CUSTODIAN_ACCOUNT_SPECIFIC_NETTING_GROUP_FILTER,
  FINANCING_STYLE_FILTER,
  ACCRUAL_CONVENTION_FILTER,
  REHYPOTHECATION_RULE_ATTRIBUTE,
  REHYPOTHECATION_RULE_TYPE,
  NEGATIVE_IRATE_FILTER,
  BANDWIDTH_GROUP_FILTER,
  CURRENCY_SPECIFIC_BASE_RATE_FILTER,
  ACCRUAL_POSTING_BUSINESS_UNIT_FILTER,
  ACCRUAL_POSTING_BUNDLE_FILTER,
  ACCRUAL_POSTING_CUSTODIAN_ACCOUNT_FILTER,
  FINANCING_TYPE_FILTER,
  AGREEMENT_TYPE_SPECIFIC_FINANCING_STYLE_FILTER,
  ATTRIBUTE_SOURCE_TYPE_FILTER,
  ATTRIBUTE_SOURCE_TYPE_SPECIFIC_ATTRIBUTE_NAME_FILTER,
  COUNTRY_FILTER,
  COUNTRY_CODE_FILTER,
  GRADING_FILTER,
  SUB_TYPE_FILTER,
  RULE_NAME_FILTER,
  ADD_RULE_FORM_RULE_NAME_FILTER,
  MARKET_FILTER,
  REGION_FILTER,
  REGION_SPECIFIC_COUNTRY_FILTER,
  BICS_INDUSTRY_SUBGROUP_FILTER,
  STRATEGIES_FILTER,
  CPE_FAMILY_GROUPED_FILTER,
  ROA_CALCULATION_FACTOR_FILTER,
  ADJUSTMENT_ALLOCATION_STRATEGY_FILTER,
  FRONT_OFFICE_PRIME_BROKER_FILTER,
  SECURITY_FOTYPE_SUBTYPE_FILTER,
  FINANCING_AGREEMENT_TERM_TYPE_FILTER,
  OPTION_TYPE_FILTER,
  LEAN_SURCHARGE_MODEL_FILTER
} from "../constants";
import { fetchURL } from "../util";
import _ from "lodash";

const queryString = require("query-string");

export function getFilter(payload) {
  var key = payload.key;
  var paramString = "";
  switch (key) {
    case LEGAL_ENTITY_FILTER:
      return fetchURL(`${BASE_URL}data/load-legal-entity-filter`);
    case SECURITY_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-security-types`);
    case MARGIN_CALCULATOR_FILTER:
      return fetchURL(`${BASE_URL}data/load-margin-calculator-filter`);
    case FILE_ALIAS_FILTER:
      return fetchURL(
        `${BASE_URL}service/fileAliasService/getRecentlyParsedFileAliases?format=JSON`
      );
    case OWNERSHIP_ENTITY_FILTER:
      return fetchURL(`${BASE_URL}data/load-ownership-entity-filter`);
    case CPE_FAMILY_FILTER:
      return fetchURL(`${BASE_URL}data/load-cpe-family-filter`);
    case BUNDLE_FILTER:
      return fetchURL(`${BASE_URL}data/load-bundles`);
    case CPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-cpe-filter`);
    case BUSINESS_UNIT_FILTER:
      return fetchURL(`${BASE_URL}data/load-business-unit-filter`);
    case AGREEMENT_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-agreemen-type-filter`);
    case CURRENCY_FILTER:
      return fetchURL(`${BASE_URL}data/load-currencies-filter`);
    case NETTING_GROUP_FILTER:
      return fetchURL(`${BASE_URL}data/load-nettingGroups-filter`);
    case ADJUSTMENT_TYPE_FILTER:
      return fetchURL(
        `${BASE_URL}service/lcmAdjustmentService/getAdjustmentTypes?format=JSON`
      );
    case REASON_CODE_FILTER:
      return fetchURL(
        `${BASE_URL}service/lcmAdjustmentService/getReasonCodeTypes?format=JSON&adjustmentTypeName=` +
          (payload.adjustmentType ? payload.adjustmentType : "")
      );
    case CUSTODIAN_ACCOUNT_FILTER:
      return fetchURL(`${BASE_URL}data/load-custodian-accounts-filter`);
    case NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER:
      return fetchURL(`${BASE_URL}data/load-netting-group-specific-custodian-accounts-filter`);
    case AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER:
      return fetchURL(`${BASE_URL}service/interestFiltersService/getAgreementNettingGroupSpecificCustodianAccount?agreementId=` + payload.filterData.agreementTypeFilterId + `&nettingGroupId=` + payload.filterData.nettingGroupFilterId + `&format=JSON`);
    case ASSET_LIQUIDITY_FILTER:
      return fetchURL(`${BASE_URL}jsp/filter/load-asset-liquidity-filter.jsp`);
    case FINANCING_STATUS_FILTER:
      return fetchURL(
        `${BASE_URL}jsp/filter/load-financing-status-type-filter.jsp`
      );
    case CURRENCY_FILTER:
      return fetchURL(`${BASE_URL}data/load-currencies-filter`);
    case LEGAL_ENTITY_FAMILY_FILTER:
      return fetchURL(`${BASE_URL}data/load-legal-entity-family-filter`);
    case BROKER_REVENUE_SIGN_OFF_YEAR:
      return fetchURL(
        `${BASE_URL}service/brokerRevenueService/getBrokerRevenueSignOffYear?format=JSON`
      );
    case DATE_FILTER:
      return fetchURL(`${BASE_URL}data/load-date-filter`);
    case BUSINESS_UNIT_GROUP_TYPE_FILTER:
      return fetchURL(
        `${BASE_MOSS_SERVICE_URL}/bundleService/getBusinessUnitGroupTypes?&format=json`
      );
    case BUSINESS_UNIT_GROUP_FILTER:
      return fetchURL(`${BASE_URL}data/load-business-unit-group-filter`);
    case BUNDLE_FILTER:
      return fetchURL(`${BASE_URL}data/load-bundles`);
    case BUNDLE_GROUP_FILTER:
      return fetchURL(`${BASE_URL}data/load-bundle-group-filter`);
    case BUNDLE_GROUP_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-bundle-group-type-filter`);
    case BROKER_REVENUE_BUSINESS_UNIT_FILTER:
      return fetchURL(
        `${BASE_URL}data/load-broker-revenue-business-unit-filter`
      );
    case EXEC_BROKER_FILTER:
      return fetchURL(`${BASE_URL}data/load-exec-broker-filter`);
    case GBO_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-gbo-type-filter`);
    case OPTION_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-option-type-filter`);
    case INTERACTION_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-interaction-type-filter`);
    case BOOK_FILTER:
      return fetchURL(`${BASE_URL}data/load-book-filter`);
    case BOOK_FILTER_WITH_SPECIAL_ACCOUNTING_BOOKS:
      return fetchURL(
        `${BASE_URL}data/load-book-filter-with-special-accounting-books`
      );
    case CHARGE_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-charge-type-filter`);
    case WORKFLOW_TYPE_FILTER:
      return fetchURL(
        `${BASE_URL}service/workflowStatusService/getWorkflowTypesForWorkflowStatusScreen?format=JSON`
      );
    case FINANCING_CHARGE_TYPE_FILTER:
      return fetchURL(
        `${BASE_URL}service/financingChargeTrendService/getFinancingChargeTypes?format=JSON`
      );
    case REHYPOTHECATION_RULE_ATTRIBUTE:
      return fetchURL(
        `${BASE_URL}service/indebtednessService/getRecommendationRuleAttributes?format=JSON`
      );
    case REHYPOTHECATION_RULE_TYPE:
      paramString = queryString.stringify(payload.filterData);
      paramString = paramString === "" ? "attribute=__null__" : paramString;
      return fetchURL(
        `${BASE_URL}service/indebtednessService/getRecommendationRuleTypes?${paramString}&format=JSON`
      );
    case FETCH_HEDGE_FUNDS_FILTER:
      return fetchURL(
        `${BASE_URL}service/treasuryFiltersService/getHedgeFunds?format=JSON`
      );
    case FINANCING_RATE_FILTER:
      return fetchURL(
        `${BASE_URL}service/financingRateComparisonService/getFinancingRateTypes?format=json`
      );
    case FO_TYPE_FILTER:
      return fetchURL(
        `${BASE_URL}service/interestFiltersService/getFOTypes?format=JSON`
      );
    case FETCH_SUBTYPE_FILTER:
      return fetchURL(
        `${BASE_URL}service/interestFiltersService/getSubtypesForFOTypes?format=JSON&foTypeIds=` +
          payload.foTypeIds
      );
    case SECLEND_DATA_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-seclend-data-type-filter`);
    case AGREEMENT_FILTER:
      return fetchURL(`${BASE_URL}data/load-agreemen-id-filter`);
    case ASSET_CLASS_GROUP_FILTER:
      return fetchURL(`${BASE_URL}data/load-asset-class-group-filter`);
    case FINANCING_STYLE_FILTER:
      return fetchURL(`${BASE_URL}data/load-all-financing-style-filter`);
    case ACCRUAL_CONVENTION_FILTER:
      return fetchURL(`${BASE_URL}data/load-accrual-convention`);
    case NEGATIVE_IRATE_FILTER:
      return fetchURL(`${BASE_URL}data/load-apply-negative-irate-filter`);
    case BANDWIDTH_GROUP_FILTER:
      return fetchURL(`${BASE_URL}data/load-bandwidth-group`);
    case FINANCING_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-financing-type-filter`);
    case ATTRIBUTE_SOURCE_TYPE_FILTER:
      return fetchURL(`${BASE_URL}data/load-attrSourceTypes`);
    case COUNTRY_FILTER:
      return fetchURL(`${BASE_URL}data/load-countries`);
    case GRADING_FILTER:
      return fetchURL(`${BASE_URL}data/load-grading`);
    case COUNTRY_CODE_FILTER:
      return fetchURL(
        `${BASE_URL}service/interestFiltersService/getCountries?format=JSON`
      );
    case REGION_FILTER:
      return fetchURL(
        `${BASE_URL}service/financingFiltersService/getRegions?format=JSON`
      );
    case REGION_SPECIFIC_COUNTRY_FILTER:
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}service/financingFiltersService/getCountriesByRegionId?${paramString}&format=JSON`
      );
    case BICS_INDUSTRY_SUBGROUP_FILTER:
      return fetchURL(
        `${BASE_URL}service/financingFiltersService/getBicsIndustrySubGroups?format=JSON`
      );
    case ADJUSTMENT_ALLOCATION_STRATEGY_FILTER:
      return fetchURL(
        `${BASE_URL}service/adjustmentAllocationRuleSystemService/getAdjustmentAllocationStrategies?format=JSON`
      );
      case FINANCING_AGREEMENT_TERM_TYPE_FILTER:
      return fetchURL(
        `${BASE_URL}service/interestFiltersService/getFinancingAgreementTermTypes?format=JSON`
      );
    case SUB_TYPE_FILTER:
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}data/load-subtypes?${paramString}&format=JSON`
      );
    case AGREEMENT_TYPE_SPECIFIC_NETTING_GROUP_FILTER:
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}data/load-agreement-type-specific-netting-group-filter?${paramString}&format=JSON`
      );
    case CALCULATOR_GROUP_FILTER:
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}data/load-calculatorGroups-filter?${paramString}&format=JSON`
      );
    case ATTRIBUTE_SOURCE_TYPE_SPECIFIC_ATTRIBUTE_NAME_FILTER:
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}data/load-attrNames?${paramString}&format=JSON`
      );
    case CURRENCY_SPECIFIC_BASE_RATE_FILTER:
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}data/load-base-rate-filter?${paramString}&format=JSON`
      );
    case CUSTODIAN_ACCOUNT_SPECIFIC_NETTING_GROUP_FILTER:
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}data/load-custodian-account-specific-netting-groups-filter?${paramString}&format=JSON`
      );
    case AGREEMENT_TYPE_SPECIFIC_FINANCING_STYLE_FILTER:
      if (payload.filterData.agreementId === "-1") {
        return fetchURL(`${BASE_URL}data/load-financing-style-filter`);
      }
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}data/load-agreement-type-specific-financial-style-filter?${paramString}&format=JSON`
      );
    case AGREEMENT_SPECIFIC_NETTING_GROUP_FILTER:
      paramString = queryString.stringify(payload.filterData);
      return fetchURL(
        `${BASE_URL}data/load-netting-group-by-agreement-id?${paramString}&format=JSON`
      );
    case ACCRUAL_POSTING_BUSINESS_UNIT_FILTER:
      return fetchURL(
        `${BASE_URL}service/interestFiltersService/getAccrualPostingBusinessUnits?chargeTypeId=` +
          payload.filterData.chargeTypeId +
          `&format=JSON`
      );
    case ACCRUAL_POSTING_BUNDLE_FILTER:
      return fetchURL(
        `${BASE_URL}service/interestFiltersService/getAccrualPostingBundles?chargeTypeId=` +
          payload.filterData.chargeTypeId +
          `&businessUnitId=` +
          payload.filterData.businessUnitId +
          `&format=JSON`
      );
    case ACCRUAL_POSTING_CUSTODIAN_ACCOUNT_FILTER:
      return fetchURL(
        `${BASE_URL}service/interestFiltersService/getAccrualPostingCustodianAccounts?chargeTypeId=` +
          payload.filterData.chargeTypeId +
          `&businessUnitId=` +
          payload.filterData.businessUnitId +
          `&bundleId=` +
          payload.filterData.bundleId +
          `&format=JSON`
      );
    case RULE_NAME_FILTER:
    case ADD_RULE_FORM_RULE_NAME_FILTER:
      return fetchURL(
        `${BASE_URL}service/aanaRuleService/getAanaRuleNames?ruleConfigTypeId=` +
        payload.filterData.ruleConfigTypeId +
        `&format=JSON`
      );
    case MARKET_FILTER:
      return fetchURL(
        `${BASE_MOSS_SERVICE_URL}refService/getActiveMarkets?format=JSON`
      );
    case STRATEGIES_FILTER:
      return fetchURL(
        `${BASE_MOSS_SERVICE_URL}refService/getStrategies?format=JSON`
      );
    case CPE_FAMILY_GROUPED_FILTER:
      return fetchURL(
        `${BASE_URL}service/counterpartyReportingGroupingService/getAllGroups?agreementTypeIds=__null__&format=JSON`
      );
    case ROA_CALCULATION_FACTOR_FILTER:
      return fetchURL(
        `${BASE_URL}service/returnOnAssetsKnobService/getKnobTypes?format=JSON`
      );
    case FRONT_OFFICE_PRIME_BROKER_FILTER:
      return fetchURL(
        `${BASE_URL}service/treasuryFiltersService/getFrontOfficePrimeBrokers?format=JSON`
      );
    case SECURITY_FOTYPE_SUBTYPE_FILTER:
      return fetchURL(
        `${BASE_URL}service/treasuryFiltersService/getSecurityTypeSubtypes?format=JSON`
      );
    case LEAN_SURCHARGE_MODEL_FILTER:
      return fetchURL(
        `${BASE_URL}service/leanSurchargeService/getAllLeanSurchargeModels?format=JSON`
      );
    default:
      console.log("Nothing found");
  }
}
