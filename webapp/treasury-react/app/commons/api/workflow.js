import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
export let url = "";
export function beginWorkflow(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}comet/workflowBegin?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function resumeWorkflow(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}comet/resumeWorkflow?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function executeWorkflow(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}comet/workflowExecute?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function cancelAndBeginWorkflow(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}comet/cancelAndBeginWorkflow?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
