const SERVICE_URLS = {
  cashProjectionService: {
    getCashProjectionReportData:
      "cashProjectionService/getCashProjectionReportData",
    getCashProjectionReportFlattenedData:
      "cashProjectionService/getCashProjectionReportFlattenedData",
  },
  cashProjectionConfigService: {
    getCashProjectionConfigurationList:
      "cashProjectionConfigService/getCashProjectionConfigurationList",
    getWhitelistedBooksList:
      "cashProjectionConfigService/getWhitelistedBooksList",
    getBalanceTypeList:
      "cashProjectionConfigService/getBalanceTypeList",
  },
  tradeIndependentAmountService: {
    getTradeIndependentAmountDetail: "tradeIndependentAmountService/getTradeIndependentAmountDetail",
    updateIndependentAmountDetail: "tradeIndependentAmountService/updateIndependentAmountDetail",
    invalidateIndependentAmountDetail: "tradeIndependentAmountService/invalidateIndependentAmountDetail",
  },
  marginFilterService: {
    getAllocators: "marginFilterService/getAllocators",
    getMarginCalculators: "marginFilterService/getMarginCalculators"
  },
  marginPositionDataService: {
    getMarginPageFilters: "marginPositionDataService/getMarginDataPageFilters",
    getPositionMarginData: "marginPositionDataService/getPositionMarginData",
    getLatestMarginDataView: "marginPositionDataService/getLatestMarginDataView"
  },
  marginRuleService: {
    searchMarginRules: "marginRuleService/searchMarginRules",
    getConflictingRules: "marginRuleService/getConflictingRules",
    bulkUpdateMarginRule: "marginRuleService/bulkUpdateMarginRule",
    getAllAgreementData: "marginRuleService/getAllAgreementData",
    addMarginRule: "marginRuleService/addMarginRule",
    updateMarginRule: "marginRuleService/updateMarginRule",
    invalidateMarginRule: "marginRuleService/invalidateMarginRule",
  },
  marginRuleSystemService: {
    getMarginRuleForPosition: "marginRuleSystemService/getMarginRuleForPosition",
  },
  positionDetailReport: {
    searchPositionDetailData: "positionDetailReport/searchPositionDetailData",
  },
  positionDetailReportService: {
    getPdrFilters: "positionDetailReportService/getPdrFilters",
    getLcmPositionDataFiltersForDayOnDayDiff: "positionDetailReportService/getLcmPositionDataFiltersForDayOnDayDiff",
    getLcmPositionDataForDayOnDay: "positionDetailReportService/getLcmPositionDataForDayOnDay",
  },
  independentAmountRuleService: {
    searchIndependentAmountRules: "independentAmountRuleService/searchIndependentAmountRules"
  },
  positionDetailReportDataReadManagerService: {
    getPdrFilters: "positionDetailReportDataReadManager/getPdrFilters",
    getPdrData: "positionDetailReportDataReadManager/getPdrData",
    getPdrDataView: "positionDetailReportDataReadManager/getPdrDataView",
    checkAndPopulateCache: "positionDetailReportDataReadManager/checkAndPopulateCache",
    getLcmPositionDataFiltersForDayOnDayDiff: "positionDetailReportDataReadManager/getLcmPositionDataFiltersForDayOnDayDiff",
    getDayOnDayDiffPdrData: "positionDetailReportDataReadManager/getDayOnDayDiffPdrData",
    getObjectStoreUrlForPdrPositionData: "positionDetailReportDataReadManager/getObjectStoreUrlForPdrPositionData"
  },
  liquidityManager: {
    getLiquidityReport:
      "liquidityManager/getLiquidityReport",
    getFlattenedLiquidityReport:
      "liquidityManager/getFlattenedLiquidityReport",
    getMMFBookingDetailsForFund:
      "liquidityManager/getMMFBookingDetailsForFund",
    getDefaultMMFBookingDetailsForFund:
      "liquidityManager/getDefaultMMFBookingDetailsForFund",
  },
  lcmAdjustmentService: {
    saveAdjustments:
      "lcmAdjustmentService/saveAdjustments",
  },
  tradeWorkflowManager: {
    initiateTradeBooking:
      "tradeWorkflowManager/initiateTradeBooking",
    getTradeWorkflowDetails: "tradeWorkflowManager/getTradeWorkflowDetails",
    getFlattenTradeWorkflowDetails: "tradeWorkflowManager/getFlattenTradeWorkflowDetails",
  },
  liquidityAssetManager: {
    getGovtBondHoldings:
      "liquidityAssetManager/getGovtBondHoldings",
    getTDaySettlingGovtBondHoldings:
      "liquidityAssetManager/getTDaySettlingGovtBondHoldings"
  },
  liquidityConfigManager: {
    getAllLiquidityConfigWithEnrichment:
      "liquidityConfigManager/getAllLiquidityConfigWithEnrichment",
    getAllLiquidityConfig:
      "liquidityConfigManager/getAllLiquidityConfig"
  },
  calmWorkflowManagement: {
    beginWorkflow: "calmWorkflowManagement/beginWorkflow",
    publishWorkflow: "calmWorkflowManagement/publishWorkflow",
    cancelWorkflow: "calmWorkflowManagement/cancelWorkflow",
    cancelAndBeginWorkflow: "calmWorkflowManagement/cancelAndBeginWorkflow",
    getWorkflow: "calmWorkflowManagement/getWorkflow"
  }
};

export const COMPONENT_URLS = {
  INDEPENDENT_AMOUNT_RULES: "/treasury/margin/independentAmountRules.html",
  TRADE_INDEPENDENT_AMOUNT: "/treasury/margin/tradeIndependentAmount.html",
  POSITION_DETAIL_REPORT: "/treasury/comet/positiondetailreport.html"
}

export default SERVICE_URLS;
