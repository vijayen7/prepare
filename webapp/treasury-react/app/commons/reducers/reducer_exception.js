import { HANDLE_EXCEPTION , HANDLE_EXCEPTION_RESET} from "../constants";

export function exceptionReducer(state = false, action) {
    switch (action.type) {
        case HANDLE_EXCEPTION:
            return true;
        case HANDLE_EXCEPTION_RESET:
            return false;
    }
    return state;
}
