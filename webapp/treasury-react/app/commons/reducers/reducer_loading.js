import { START_LOADING, END_LOADING } from '../constants';

export function loadingReducer(state = 0, action) {
  switch (action.type) {
    case START_LOADING:
      return state + 1;
    case END_LOADING:
      return state - 1;
    default:
      return state;
  }
}
