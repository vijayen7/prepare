import {
  FETCH_FILTER,
  LEGAL_ENTITY_FILTER,
  MARGIN_CALCULATOR_FILTER,
  FILE_ALIAS_FILTER,
  OWNERSHIP_ENTITY_FILTER,
  CPE_FAMILY_FILTER,
  BUSINESS_UNIT_FILTER,
  AGREEMENT_TYPE_FILTER,
  NETTING_GROUP_FILTER,
  ADJUSTMENT_TYPE_FILTER,
  CUSTODIAN_ACCOUNT_FILTER,
  NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER,
  AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER,
  REASON_CODE_FILTER,
  ASSET_LIQUIDITY_FILTER,
  LEGAL_ENTITY_FAMILY_FILTER,
  FETCH_BROKER_REVENUE_SIGN_OFF_YEAR,
  FETCH_DATE_FILTER,
  CPE_FILTER,
  BUNDLE_FILTER,
  CURRENCY_FILTER,
  BUSINESS_UNIT_GROUP_TYPE_FILTER,
  SECURITY_TYPE_FILTER,
  BUSINESS_UNIT_GROUP_FILTER,
  BUNDLE_GROUP_FILTER,
  BUNDLE_GROUP_TYPE_FILTER,
  BROKER_REVENUE_BUSINESS_UNIT_FILTER,
  EXEC_BROKER_FILTER,
  GBO_TYPE_FILTER,
  OPTION_TYPE_FILTER,
  INTERACTION_TYPE_FILTER,
  BOOK_FILTER,
  BOOK_FILTER_WITH_SPECIAL_ACCOUNTING_BOOKS,
  WORKFLOW_TYPE_FILTER,
  FETCH_HEDGE_FUNDS_FILTER,
  FINANCING_CHARGE_TYPE_FILTER,
  CHARGE_TYPE_FILTER,
  FO_TYPE_FILTER,
  FETCH_SUBTYPE_FILTER,
  FINANCING_RATE_FILTER,
  SECLEND_DATA_TYPE_FILTER,
  AGREEMENT_TYPE_SPECIFIC_NETTING_GROUP_FILTER,
  CALCULATOR_GROUP_FILTER,
  AGREEMENT_FILTER,
  ASSET_CLASS_GROUP_FILTER,
  AGREEMENT_SPECIFIC_NETTING_GROUP_FILTER,
  CUSTODIAN_ACCOUNT_SPECIFIC_NETTING_GROUP_FILTER,
  FINANCING_STYLE_FILTER,
  ACCRUAL_CONVENTION_FILTER,
  NEGATIVE_IRATE_FILTER,
  BANDWIDTH_GROUP_FILTER,
  REHYPOTHECATION_RULE_ATTRIBUTE,
  REHYPOTHECATION_RULE_TYPE,
  CURRENCY_SPECIFIC_BASE_RATE_FILTER,
  ACCRUAL_POSTING_BUSINESS_UNIT_FILTER,
  ACCRUAL_POSTING_BUNDLE_FILTER,
  ACCRUAL_POSTING_CUSTODIAN_ACCOUNT_FILTER,
  FINANCING_TYPE_FILTER,
  AGREEMENT_TYPE_SPECIFIC_FINANCING_STYLE_FILTER,
  ATTRIBUTE_SOURCE_TYPE_FILTER,
  ATTRIBUTE_SOURCE_TYPE_SPECIFIC_ATTRIBUTE_NAME_FILTER,
  REGION_FILTER,
  COUNTRY_FILTER,
  COUNTRY_CODE_FILTER,
  GRADING_FILTER,
  SUB_TYPE_FILTER,
  FETCH_ROA_CALCULATION_FACTOR_YEARS,
  RULE_NAME_FILTER,
  ADD_RULE_FORM_RULE_NAME_FILTER,
  MARKET_FILTER,
  REGION_SPECIFIC_COUNTRY_FILTER,
  BICS_INDUSTRY_SUBGROUP_FILTER,
  STRATEGIES_FILTER,
  CPE_FAMILY_GROUPED_FILTER,
  ROA_CALCULATION_FACTOR_FILTER,
  ADJUSTMENT_ALLOCATION_STRATEGY_FILTER,
  FRONT_OFFICE_PRIME_BROKER_FILTER,
  SECURITY_FOTYPE_SUBTYPE_FILTER,
  FINANCING_AGREEMENT_TERM_TYPE_FILTER,
  LEAN_SURCHARGE_MODEL_FILTER
} from "../constants";
import { combineReducers } from "redux";
import {
  convertArrayToFilterObjects,
  convertJsonArrayToFilterObjects,
  getYearRangeFilterDataFromEndYear,
  getRoaCalculationFactorYearFilter,
} from "commons/util";
import { create } from "domain";

function createFilterReducer(key = "", dataKey = "", sort = false) {
  return function (state = [], action) {
    if (key != action.key) {
      return state;
    }
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        return convertArrayToFilterObjects(action.data[dataKey], sort);
    }
    return state;
  };
}

function createServiceFilterReducer(key = "", keyOfDataKey, keyOfDataValue, sort = false) {
  return function (state = [], action) {
    if (key != action.key) {
      return state;
    }
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        return convertJsonArrayToFilterObjects(
          action.data,
          keyOfDataKey,
          keyOfDataValue,
          sort
        );
    }
  };
}

function fileAliasFilterReducer(state = [], action) {
  if (action.key != FILE_ALIAS_FILTER) {
    return state;
  }
  switch (action.type) {
    case `${FETCH_FILTER}_SUCCESS`:
      return _.map(action.data, function (el) {
        return {
          key: el.fileAliasId,
          value: el.description,
        };
      }).concat([{ key: -1, value: "All" }]);
  }
}

function createServiceFilterReducerWithoutNull(
  key = "",
  keyOfDataKey,
  keyOfDataValue,
  includeInactive = true,
  includeInactiveKey = null,
  sort = false
) {
  return function (state = [], action) {
    if (key != action.key) {
      return state;
    }
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        let filteredData = action.data.filter((obj) => {
          return (
            (includeInactive || obj[includeInactiveKey]) &&
            keyOfDataKey in obj &&
            keyOfDataValue in obj
          );
        });
        return convertJsonArrayToFilterObjects(
          filteredData,
          keyOfDataKey,
          keyOfDataValue,
          sort
        );
    }
  };
}

function brokerRevenueYearsReducer(state = [], action) {
  switch (action.type) {
    case `FETCH_BROKER_REVENUE_SIGN_OFF_YEAR`:
      return getYearRangeFilterDataFromEndYear(new Date().getFullYear());
  }
  return state;
}

function dateFilterReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_DATE_FILTER}_SUCCESS`:
      return action.date;
  }
  return state;
}

function workflowTypeReducer(state = [], action) {
  switch (action.type) {
    case `${WORKFLOW_TYPE_FILTER}_SUCCESS`:
      return _.map(action.data, (data) => {
        return {
          key: data.id,
          value: data.description,
        };
      });
  }
  return state;
}

function reasonCodeReducer(state = [], action) {
  switch (action.type) {
    case `${REASON_CODE_FILTER}_SUCCESS`:
      return _.map(action.data, (data) => {
        return {
          key: data.id,
          value: data.name,
        };
      });
  }
  return state;
}
function adjustmentTypeReducer(state = [], action) {
  switch (action.type) {
    case `${ADJUSTMENT_TYPE_FILTER}_SUCCESS`:
      let adjustmentTypes = action.data;
      let filteredAdjustmentTypes = [];
      if (
        action.payload.adjustmentType != undefined &&
        action.payload.adjustmentType != null
      ) {
        adjustmentTypes.forEach(function (adjustmentType) {
          if (adjustmentType.name === action.payload.adjustmentType) {
            filteredAdjustmentTypes.push(adjustmentType);
          }
        });
      } else {
        filteredAdjustmentTypes = action.data;
      }
      return _.map(filteredAdjustmentTypes, (data) => {
        return {
          key: data.id,
          value: data.name,
        };
      });
    default:
      return state;
  }
}

function HedgeFundsReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_HEDGE_FUNDS_FILTER}_SUCCESS`:
      return _.map(action.data, (data) => {
        return { key: data.id, value: data.name };
      });
    default:
      return state;
  }
}

function financingChargeTypeReducer(state = [], action) {
  switch (action.type) {
    case `${FINANCING_CHARGE_TYPE_FILTER}_SUCCESS`:
      return _.map(action.data, (data) => {
        return {
          key: data.id,
          value: data.description,
        };
      });
  }
  return state;
}

function rehypothecationRuleAttributeReducer(state = [], action) {
  if (action.key === 'REHYPOTHECATION_RULE_ATTRIBUTE') {
  switch (action.type) {
    case `${FETCH_FILTER}_SUCCESS`:
      return _.map(action.data, (data) => {
        return {
          key: data.attributeName,
          value: data.attributeName,
        };
      });
    }
  }
  return state;
}

function rehypothecationRuleTypeReducer(state = [], action) {
  if (action.key === 'REHYPOTHECATION_RULE_TYPE') {
  switch (action.type) {
    case `${FETCH_FILTER}_SUCCESS`:
      return _.map(action.data, (data) => {
        return {
          key: data.type,
          value: data.type,
        };
      });
    }
  }
  return state;
}

function subtypeReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_SUBTYPE_FILTER}_SUCCESS`:
      return _.map(action.data, (data) => {
        return {
          key: data.id,
          value: data.name,
          foTypeId: data.foTypeId,
        };
      });
  }
  return state;
}

function accrualPostingSpecialBusinessUnitReducer(state = [], action) {
  if (action.key === "ACCRUAL_POSTING_BUSINESS_UNIT_FILTER") {
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        var arr = [];
        for (var obj in action.data) {
          arr.push({ key: obj, value: action.data[obj] });
        }
        return arr;
    }
  }
  return state;
}

function accrualPostingSpecialBundleReducer(state = [], action) {
  if (action.key === "ACCRUAL_POSTING_BUNDLE_FILTER") {
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        var arr = [];
        for (var obj in action.data) {
          arr.push({ key: obj, value: action.data[obj] });
        }
        return arr;
    }
  }
  return state;
}

function accrualPostingSpecialCustodianAccountReducer(state = [], action) {
  if (action.key === "ACCRUAL_POSTING_CUSTODIAN_ACCOUNT_FILTER") {
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        var arr = [];
        for (var obj in action.data) {
          arr.push({ key: obj, value: action.data[obj] });
        }
        return arr;
    }
  }
  return state;
}

function bicsIndustrySubgroupReducer(state = [], action) {
  if (action.key === BICS_INDUSTRY_SUBGROUP_FILTER) {
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        var arr = [];
        for (var obj in action.data) {
          arr.push({ key: obj, value: action.data[obj] });
        }
        return arr;
    }
  }
  return state;
}

function adjustmentAllocationStrategyReducer(state = [], action) {
  if (action.key === ADJUSTMENT_ALLOCATION_STRATEGY_FILTER) {
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        var arr = [];
        for (var obj in action.data) {
          arr.push({ key: obj, value: action.data[obj] });
        }
        return arr;
    }
  }
  return state;
}

function financingAgreementTermTypeReducer(state = [], action) {
  if (action.key === FINANCING_AGREEMENT_TERM_TYPE_FILTER) {
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        var arr = [];
        for (var obj in action.data) { arr.push({ key: obj, value: action.data[obj] }); }
        return arr;
    }
  }
  return state;
}

function roaCalculationFactorYearsReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_ROA_CALCULATION_FACTOR_YEARS}`:
      return getRoaCalculationFactorYearFilter();
  }
  return state;
}

function roaCalculationFactorReducer(state = [], action) {
  switch (action.type) {
    case `${ROA_CALCULATION_FACTOR_FILTER}_SUCCESS`:
      return _.map(action.data, (data) => {
        return {
          key: data.knobTypeId,
          value: data.knobName,
        };
      });
  }
  return state;
}

function agmtNettingGroupSpecificCustodianAccountReducer(state = [], action) {
  if (action.key === 'AGREEMENT_NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER') {
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        var arr = [];
        for (var obj in action.data) { arr.push({ key: obj, value: action.data[obj] }); }
        return arr;
    }
  }
  return state;
}

function securityTypeSubtypeFilterReducer(state = [], action) {
  if (action.key == SECURITY_FOTYPE_SUBTYPE_FILTER) {
    switch (action.type) {
      case `${FETCH_FILTER}_SUCCESS`:
        return _.map(action.data, (data) => {
          return {
            key: `${data.foTypeId}_${data.subtypeId}`,
            value: data.name,
          };
        });
    }
  }
  return state;
}

const rootReducer = combineReducers({
  legalEntities: createFilterReducer(LEGAL_ENTITY_FILTER, "descoEntities"),
  securityTypes: createFilterReducer(
    SECURITY_TYPE_FILTER,
    "borrowFinancingSecurityTypes"
  ),
  marginCalculators: createFilterReducer(
    MARGIN_CALCULATOR_FILTER,
    "marginCalculators"
  ),
  fileAliases: fileAliasFilterReducer,
  ownershipEntities: createFilterReducer(
    OWNERSHIP_ENTITY_FILTER,
    "ownershipEntities"
  ),
  cpeFamilies: createFilterReducer(
    CPE_FAMILY_FILTER,
    "counterPartyEntityFamilies",
    true
  ),
  businessUnits: createFilterReducer(BUSINESS_UNIT_FILTER, "businessUnits"),
  agreementTypes: createFilterReducer(
    AGREEMENT_TYPE_FILTER,
    "agreementTypes",
    true
  ),

  currencies: createFilterReducer(CURRENCY_FILTER, "currency"),
  nettingGroups: createFilterReducer(NETTING_GROUP_FILTER, "nettingGroups"),
  custodianAccounts: createFilterReducer(
    CUSTODIAN_ACCOUNT_FILTER,
    "custodianAccounts"
  ),
  nettingGroupSpecificCustodianAccounts: createFilterReducer(
    NETTING_GROUP_SPECIFIC_CUSTODIAN_ACCOUNT_FILTER,
    "custodianAccounts"
  ),
  agmtNettingGroupSpecificCustodianAccounts: agmtNettingGroupSpecificCustodianAccountReducer,
  assetLiquidity: createFilterReducer(ASSET_LIQUIDITY_FILTER, "assetLiquidity"),
  cpes: createFilterReducer(CPE_FILTER, "cpes"),
  bundles: createFilterReducer(BUNDLE_FILTER, "bundles"),
  currencies: createFilterReducer(CURRENCY_FILTER, "currency"),
  legalEntityFamilies: createFilterReducer(
    LEGAL_ENTITY_FAMILY_FILTER,
    "descoEntityFamilies"
  ),
  brokerRevenueYears: brokerRevenueYearsReducer,
  dates: dateFilterReducer,
  businessUnitGroupTypes: createServiceFilterReducer(
    BUSINESS_UNIT_GROUP_TYPE_FILTER,
    "id",
    "name"
  ),
  businessUnitGroups: createFilterReducer(
    BUSINESS_UNIT_GROUP_FILTER,
    "businessUnitGroups"
  ),
  bundles: createFilterReducer(BUNDLE_FILTER, "bundles"),
  bundleGroups: createFilterReducer(BUNDLE_GROUP_FILTER, "bundleGroups"),
  bundleGroupTypes: createFilterReducer(
    BUNDLE_GROUP_TYPE_FILTER,
    "bundleGroupTypes"
  ),
  brokerRevenueBusinessUnits: createFilterReducer(
    BROKER_REVENUE_BUSINESS_UNIT_FILTER,
    "brokerRevenueBusinessUnits"
  ),
  execBrokers: createFilterReducer(EXEC_BROKER_FILTER, "execBrokers"),
  execBrokerFamilies: createFilterReducer(
    EXEC_BROKER_FILTER,
    "execBrokerFamilies"
  ),
  gboTypes: createFilterReducer(GBO_TYPE_FILTER, "gboTypes"),
  optionTypes: createFilterReducer(OPTION_TYPE_FILTER, "optionTypes"),
  interactionTypes: createFilterReducer(
    INTERACTION_TYPE_FILTER,
    "interactionTypes"
  ),
  books: createFilterReducer(BOOK_FILTER, "books"),
  booksWithSpecialAccountingBooks: createFilterReducer(
    BOOK_FILTER_WITH_SPECIAL_ACCOUNTING_BOOKS,
    "books"
  ),
  workflowTypes: workflowTypeReducer,
  financingRateFilter: createServiceFilterReducer(
    FINANCING_RATE_FILTER,
    "id",
    "name"
  ),
  reasonCodes: reasonCodeReducer,
  adjustmentTypes: adjustmentTypeReducer,
  hedgeFunds: HedgeFundsReducer,
  financingChargeTypes: financingChargeTypeReducer,
  rehypothecationRuleAttributes: rehypothecationRuleAttributeReducer,
  rehypothecationRuleTypes: rehypothecationRuleTypeReducer,
  seclendDataTypes: createFilterReducer(
    SECLEND_DATA_TYPE_FILTER,
    "seclendDataTypes"
  ),
  foTypes: createServiceFilterReducer(FO_TYPE_FILTER, "id", "name"),
  subtypes: subtypeReducer,
  agreementTypeSpecificNettingGroups: createFilterReducer(
    AGREEMENT_TYPE_SPECIFIC_NETTING_GROUP_FILTER,
    "nettingGroups"
  ),
  calculatorGroups: createFilterReducer(
    CALCULATOR_GROUP_FILTER,
    "calculatorGroups"
  ),
  agreementIds: createFilterReducer(AGREEMENT_FILTER, "agreementIds"),
  assetClassGroups: createFilterReducer(
    ASSET_CLASS_GROUP_FILTER,
    "assetClassGroups"
  ),
  agreementSpecificNettingGroups: createFilterReducer(
    AGREEMENT_SPECIFIC_NETTING_GROUP_FILTER,
    "nettingGroups"
  ),
  custodianAccountSpecificNettingGroups: createFilterReducer(
    CUSTODIAN_ACCOUNT_SPECIFIC_NETTING_GROUP_FILTER,
    "nettingGroups"
  ),
  chargeTypes: createFilterReducer(CHARGE_TYPE_FILTER, "chargeTypes"),
  financingStyles: createFilterReducer(
    FINANCING_STYLE_FILTER,
    "financingStyles"
  ),
  accrualConvention: createFilterReducer(
    ACCRUAL_CONVENTION_FILTER,
    "accrualConvention"
  ),
  applyNegativeIrates: createFilterReducer(
    NEGATIVE_IRATE_FILTER,
    "applyNegativeIrates"
  ),
  bandwidthGroup: createFilterReducer(BANDWIDTH_GROUP_FILTER, "bandwidthGroup"),
  baseRates: createFilterReducer(
    CURRENCY_SPECIFIC_BASE_RATE_FILTER,
    "baseRates"
  ),
  accrualPostingBUs: accrualPostingSpecialBusinessUnitReducer,
  accrualPostingBundles: accrualPostingSpecialBundleReducer,
  accrualPostingCAs: accrualPostingSpecialCustodianAccountReducer,
  bicsIndustrySubgroups: bicsIndustrySubgroupReducer,
  adjustmentAllocationStrategies: adjustmentAllocationStrategyReducer,
  financingAgreementTermTypes: financingAgreementTermTypeReducer,
  financingTypes: createFilterReducer(FINANCING_TYPE_FILTER, "financingTypes"),
  agreementTypeSpecificFinancingStyles: createFilterReducer(
    AGREEMENT_TYPE_SPECIFIC_FINANCING_STYLE_FILTER,
    "financingStyles"
  ),
  attrSourceTypes: createFilterReducer(
    ATTRIBUTE_SOURCE_TYPE_FILTER,
    "attrSourceTypes"
  ),
  attrNames: createFilterReducer(
    ATTRIBUTE_SOURCE_TYPE_SPECIFIC_ATTRIBUTE_NAME_FILTER,
    "attrNames"
  ),
  regions: createServiceFilterReducer(REGION_FILTER, "id", "name"),
  countries: createFilterReducer(COUNTRY_FILTER, "countries", true),
  countryCodes: createServiceFilterReducer(
    COUNTRY_CODE_FILTER,
    "abbreviation",
    "name"
  ),
  grading: createFilterReducer(GRADING_FILTER, "grading"),
  foSubtypes: createFilterReducer(SUB_TYPE_FILTER, "foSubtypes"),
  roaCalculationFactorYears: roaCalculationFactorYearsReducer,
  ruleName: createServiceFilterReducer(
    RULE_NAME_FILTER,
    "ruleNameId",
    "ruleName"
  ),
  addRuleFormRuleName: createServiceFilterReducer(
    ADD_RULE_FORM_RULE_NAME_FILTER,
    "ruleNameId",
    "ruleName"
  ),
  markets: createServiceFilterReducer(MARKET_FILTER, "id", "name"),
  regionSpecificCountries: createServiceFilterReducer(REGION_SPECIFIC_COUNTRY_FILTER, "id", "name"),
  strategies: createServiceFilterReducerWithoutNull(
    STRATEGIES_FILTER,
    "id",
    "displayName",
    false,
    "isActive",
    true
  ),
  primeBrokers: createServiceFilterReducer(
    FRONT_OFFICE_PRIME_BROKER_FILTER,
    "id",
    "name"
  ),
  securityTypeSubtypes: securityTypeSubtypeFilterReducer,
  cpeFamiliesGroup: createServiceFilterReducer(
    CPE_FAMILY_GROUPED_FILTER,
    "groupId",
    "groupName",
    true
  ),
  roaCalculationFactor: roaCalculationFactorReducer,
  leanSurchargeModels: createServiceFilterReducer(LEAN_SURCHARGE_MODEL_FILTER, "id", "name")
});

export default rootReducer;
