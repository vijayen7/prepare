import { combineReducers } from "redux";
import { BEGIN_WORKFLOW, CANCEL_AND_BEGIN_WORKFLOW, EXECUTE_WORKFLOW, RESUME_WORKFLOW } from "../../commons/constants";

function workflowReducer(state = [], action) {
  switch (action.type) {
    case `${BEGIN_WORKFLOW}_SUCCESS`:
      return action.data || [];
    case `${RESUME_WORKFLOW}_SUCCESS`:
      return action.data || [];
    case `${EXECUTE_WORKFLOW}_SUCCESS`:
      return action.data || [];
    case `${CANCEL_AND_BEGIN_WORKFLOW}_SUCCESS`:
      return action.data || [];
    default:
      return state;
  }
}

function workflowParamReducer(state = {}, action) {
  switch (action.type) {
    case `${BEGIN_WORKFLOW}_SUCCESS`:
      return action.params || {};
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  workflowStatus: workflowReducer,
  workflowParams: workflowParamReducer
});

export default rootReducer;
