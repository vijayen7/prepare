export default interface MapData {
  key: number;
  value: string;
}
