export default interface RefData {
  key: string;
  value: string;
}
