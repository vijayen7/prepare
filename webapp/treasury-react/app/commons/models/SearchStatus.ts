export interface SearchStatus {
  firstSearch: boolean;
  inProgress: boolean;
  error: boolean;
}
