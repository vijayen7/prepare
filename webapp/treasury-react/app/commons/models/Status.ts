export default interface Status {
  status: string;
  message: string;
}
