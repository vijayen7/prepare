import MapData from "./MapData";
import RefData from "./RefData";

export default interface FilterProps {
  onSelect: Function;
  selectedData: MapData | MapData[] | RefData[] | RefData | null | undefined;
  data?: MapData[] | RefData[];
  multiSelect?: boolean;
  singleSelect?: boolean;
  readonly?: boolean;
  horizontalLayout?: boolean;
  errorDiv?: boolean;
  label?: string;
  disabled?: boolean;
  stateKey?: string;
  placeholder?: string;
}
