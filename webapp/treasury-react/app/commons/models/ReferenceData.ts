export default interface ReferenceData {
  id?: number;
  abbrev?: string;
  name?: string;
  "@CLASS"?: "arcesium.treasury.model.common.ReferenceData";
}
