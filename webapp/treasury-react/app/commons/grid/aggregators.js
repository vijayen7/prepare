export function linkSumAggregator(items, field) {
  var agg = 0;
  for (var i = 0; i < items.length; i++) {
    if (items[i].gridParent === undefined || items[i].gridParent === "root") {
      var val = items[i][field]["value"];
      if (val != null && val != "" && val != NaN) {
        agg += parseFloat(val);
      }
    }
  }
  return {
    value: agg
  }; // params is undefined.
}

export function sumIgnoringUndefinedAggregator(items, field) {
  var agg = 0;
  for (var i = 0; i < items.length; i++) {
    if (items[i].gridParent === undefined || items[i].gridParent === "root") {
      var val = items[i][field];
      if (val !== null && val !== '' && !isNaN(val) && val !== '-') {
        agg += parseFloat(val);
      }
    }
  }

  return agg;
}

export function dGridsumAggregator(items, field, column) {
  if (column) {
    column.updateAggregation = dpGrid.updateAggregation.sum;
  }
  let agg = 0;
  let isAllUndefined = true;
  for (let i = 0; i < items.length; i++) {
    if (items[i].gridParent === undefined || items[i].gridParent === 'root') {
      let val = items[i][field];
      if (val !== null && val !== '' && !isNaN(val) && val !== '-') {
        isAllUndefined = false;
        agg += parseFloat(val);
      }
    }
  }
  if (isAllUndefined) {
    return '';
  }
  return agg;
}

export function SummationAggregator(field) {
  this.field_ = field;

  this.init = function() {
    this.sum_ = null;
  };

  this.accumulate = function(item) {
    var val = item[this.field_];
    if (val != null && val !== "" && !isNaN(val) && item.gridParent == "root") {
      this.sum_ += parseFloat(val);
    }
  };

  this.storeResult = function(groupTotals) {
    if (!groupTotals.sum) {
      groupTotals.sum = {};
    }
    groupTotals.sum[this.field_] = this.sum_;
  };
}

export function roaAggregator(field) {
  this.field_ = field;

  this.init = function() {
    this.sum_ = null;
  };

  this.accumulate = function(item) {
    var val = item[this.field_];
    if (val != null && val !== "" && !isNaN(val)) {
      this.sum_ += parseFloat(val);
    }
  };

  this.storeResult = function(groupTotals) {
    if (!groupTotals.sum) {
      groupTotals.sum = {};
    }
    groupTotals.sum[this.field_] = this.sum_;
  };
}

export function financingRateAggregator(columnName,field) {
  return function(field) {
    this.field_ = field;

    this.init = function() {
      this.sum_ = null;
      this.length_ = 0
      this.column_ = columnName
    };

    this.accumulate = function(item) {
      var value = item[this.field_];
      for (let j = 0; j < value.length; j++) {
        if (value[j].cpeAbbrev == this.column_ && !isNaN(value[j].rate)) {
            this.sum_ += parseFloat(value[j].rate);
            this.length_ += 1
        }
      }

    };

    this.storeResult = function(groupTotals) {
      if (!groupTotals.avg) {
        groupTotals.avg = {};
      }
      if(this.length_ != 0)
       groupTotals.avg[this.field_ + this.column_] = this.sum_/this.length_;

    };
}
}
