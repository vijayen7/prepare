import { parseDJSrvString } from "../util";


//Returns date in "YYYY-MM-DD" format
export function dateFormatter(row, cell, value) {
  const date = parseDJSrvString(value);
  if (date) {
    return date.format("YYYY-MM-DD");
  }
  return "";
}

export function percentFormatter(row, cell, value, columnDef, dataContext) {
  if (value == null || value === "") {
    return '<div class="text-align--right"> <span class="message"> n/a </span> </div>';
  } else {
    return (
      '<div class="text-align--right">' + Number(value).toFixed(1) + "%</div>"
    );
  }
}

export function percentFormatterDefaultZero(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  return (
    '<div class="text-align--right">' +
    (value == null || value === ""
      ? Number(0).toFixed(1)
      : Number(value).toFixed(1)) +
    "%</div>"
  );
}

export function textRightAlignFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  if (value == null || value === "") {
    return '<div class="text-align--right"> <span class="message"> n/a </span> </div>';
  } else {
    return '<div class="text-align--right">' + value + "</div>";
  }
}

export function numberTotalsFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  columnValue
) {
  if (row == null) {
    return dpGrid.Formatters.Number(
      row,
      cell,
      columnValue,
      columnDef,
      dataContext
    );
  } else {
    return dpGrid.Formatters.Number(row, cell, value, columnDef, dataContext);
  }
}

export function decimalToPercentFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  className
) {
  if (value == null || value === "") {
    return '<div class="aln-rt"> <span class="message"> n/a </span> </div>';
  } else {
    if (className != null && className != undefined)
      return (
        '<div class="aln-rt ' +
        className +
        '">' +
        Number(value * 100).toFixed(1) +
        "%</div>"
      );
    else
      return (
        '<div class="aln-rt">' + Number(value * 100).toFixed(1) + "%</div>"
      );
  }
}

export function decimalToPercentTwoFormatterDefaultZero(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  className
) {
  if (value == null || value === "") {
    value = 0;
  }
  if (className != null && className != undefined)
    return (
      '<div class="aln-rt ' +
      className +
      '">' +
      Number(value * 100).toFixed(2) +
      "%</div>"
    );
  else
    return '<div class="aln-rt">' + Number(value * 100).toFixed(2) + "%</div>";
}

export function cashReportSumGroupFormatter(
  totals,
  columnDef,
  placeHolder,
  isExportToExcel
) {
  return isExportToExcel
    ? totals.sum[columnDef.field]
    : dpGrid.Formatters.Number(
      null,
      null,
      totals.sum[columnDef.field],
      columnDef,
      null
    );
}

export function sumGroupFormatter(
  totals,
  columnDef,
  placeHolder,
  isExportToExcel
) {
  var value = "";
  if (totals && totals.sum) {
    if (columnDef.field === "haircut") {
      value = "";
      if (totals.sum["underlyingValueUsd"]) {
        return isExportToExcel
          ? totals.sum["usageUsd"] / totals.sum["underlyingValueUsd"]
          : decimalToPercentFormatter(
            null,
            null,
            totals.sum["usageUsd"] / totals.sum["underlyingValueUsd"],
            columnDef,
            null
          );
      }
    } else {
      value = totals.sum[columnDef.field];
    }
  }
  return isExportToExcel
    ? value
    : dpGrid.Formatters.Number(null, null, value, columnDef, null);
}

export function textTotalsNumberFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  columnValue
) {
  if (row == null) {
    return dpGrid.Formatters.AlignRight(
      row,
      cell,
      columnValue,
      columnDef,
      dataContext
    );
  } else {
    return dpGrid.Formatters.Number(row, cell, value, columnDef, dataContext);
  }
}

export function percentTotalsFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  columnValue
) {
  if (row == null) {
    return percentFormatter(row, cell, columnValue, columnDef, dataContext);
  } else {
    return percentFormatter(row, cell, value, columnDef, dataContext);
  }
}

export function drillThroughFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  customFormatter,
  predicate,
  params,
  totalsValue
) {
  function formattedValue() {
    if (typeof customFormatter !== "undefined") {
      return customFormatter(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        totalsValue
      );
    }
    return value;
  }

  if (
    params !== undefined &&
    (typeof predicate === "undefined" ||
      predicate(row, cell, value, columnDef, dataContext))
  ) {
    return (
      "<a class='treasury-drill-through" +
      "' data-params='" +
      params +
      "'>" +
      formattedValue() +
      "</a>"
    );
  } else {
    return formattedValue();
  }
}

export function linkExcelFormatter(row, cell, value, columnnDef, dataContext) {
  cell.actions = "";
}

export function linkFormatter(row, cell, value, columnDef, dataContext) {
  return "<a>" + value + "</a>";
}

export function marketDataPositionTypeFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  cpeId,
  dataTypeFormatter
) {
  function formattedValue() {
    if (dataTypeFormatter !== "undefined") {
      if (dataTypeFormatter === "float")
        return dpGrid.Formatters.Float(
          row,
          cell,
          value,
          columnDef,
          dataContext
        );
      else if (dataTypeFormatter === "number")
        return dpGrid.Formatters.Number(
          row,
          cell,
          value,
          columnDef,
          dataContext
        );
      else return value;
    }
    return value;
  }
  if (dataContext.cpeId === cpeId) {
    if (value === null || value === undefined)
      return '<div style="font-style:italic;color:#20B2AA">n/a</div>';
    else
      return (
        '<div style="font-style:italic;color:#20B2AA"><span>' +
        formattedValue() +
        "</span></div>"
      );
  } else {
    if (value === null || value === undefined)
      return "<div><span>n/a</span></div>";
    else return "<div><span>" + formattedValue() + "</span></div>";
  }
}

export function priceFormatter(row, cell, value, columnDef, dataContext) {
  return (
    "<div class='aln-rt'>" +
    (value == null
      ? "<span class='message'>n/a</span>"
      : Number(value).toFixed(2)) +
    "</div>"
  );
}

export function priceFormatterDefaultZero(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  return (
    "<div class='aln-rt'>" +
    (value == null ? Number(0).toFixed(2) : Number(value).toFixed(2)) +
    "</div>"
  );
}

export function spreadFormatter(row, cell, value, columnDef, dataContext) {
  return (
    "<div class='aln-rt'>" +
    (value == null
      ? "<span class='message'>n/a</span>"
      : Number(value).toFixed(6)) +
    "</div>"
  );
}

export function millionFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  columnValue
) {
  if (row === null) {
    if (columnValue !== undefined) {
      return dpGrid.Formatters.Float(
        row,
        cell,
        Number(columnValue / 1000000).toFixed(2),
        columnDef,
        dataContext
      );
    } else {
      return dpGrid.Formatters.Float(
        row,
        cell,
        Number(value / 1000000).toFixed(2),
        columnDef,
        dataContext
      );
    }
  }
  if (value == null) {
    return dpGrid.Formatters.Float(row, cell, value, columnDef, dataContext);
  }
  return dpGrid.Formatters.Float(
    row,
    cell,
    Number(value / 1000000).toFixed(2),
    columnDef,
    dataContext
  );
}

export function toolTipFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  toolTip
) {
  return (
    '<div style="display:inline" title="' + toolTip + '">' + value + "</div>"
  );
}

export function excelTotalsFormatter(itemValue, dataItem, totalsValue) {
  if (dataItem["0"] === "Total") return totalsValue;
  else return itemValue;
}

export function defaultNumberToZeroFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  if (value == null) {
    return dpGrid.Formatters.Number(
      row,
      cell,
      Number(0),
      columnDef,
      dataContext
    );
  } else {
    return dpGrid.Formatters.Number(row, cell, value, columnDef, dataContext);
  }
}

export function textFormatter(row, cell, value, columnDef, dataContext) {
  return (
    "<div>" +
    (value == null ? "<span class='message'>n/a</span>" : value) +
    "</div>"
  );
}

export function groupFormatter(
  totals,
  columnDef,
  placeHolder,
  isExportToExcel
) {
  var value = "";
  if (totals && totals.sum) {
    value = totals.sum[columnDef.field];
  }
  return isExportToExcel
    ? value
    : numberFormatter(null, null, value, columnDef, null);
}

export function groupFloatFormatter(
  totals,
  columnDef,
  placeHolder,
  isExportToExcel
) {
  var value = "";
  if (totals && totals.sum) {
    value = totals.sum[columnDef.field];
  }
  return isExportToExcel
    ? value
    : dpGrid.Formatters.Float(null, null, value, columnDef, null);
}

export function numberFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  setDefaultToZero
) {
  if ((value == undefined || value == "" || isNaN(value)) && setDefaultToZero) {
    value = 0;
  }
  if (
    value != undefined &&
    value !== "" &&
    (typeof value == "string" || !isNaN(value))
  ) {
    value += "";
    var num = Math.round(value.replace(/,/g, "").replace(/x/g, ""));
    value = formatNumberUsingCommas(num);
    if (num < 0) {
      return (
        '<div style="text-align:right"><span>(' +
        value.replace(/-/, "") +
        ")</span></div>"
      );
    } else {
      return '<div style="text-align: right;"><span>' + value + "</span></div>";
    }
  } else {
    return '<div style="text-align: right;"><span class="message">n/a</span></div>';
  }
}

export function numberFormatterDefaultZero(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  if (value == undefined || value == "" || isNaN(value)) {
    value = 0;
  }
  value += "";
  var num = Math.round(value.replace(/,/g, "").replace(/x/g, ""));
  value = formatNumberUsingCommas(num);
  if (num < 0) {
    return (
      '<div style="text-align:right"><span>(' +
      value.replace(/-/, "") +
      ")</span></div>"
    );
  } else {
    return '<div style="text-align: right;"><span>' + value + "</span></div>";
  }
}

export function plainNumberFormatter(row, cell, value, columnDef, dataContext) {
  if (
    value != undefined &&
    value !== "" &&
    (typeof value == "string" || !isNaN(value))
  ) {
    value += "";
    var num = Math.round(value.replace(/,/g, "").replace(/x/g, ""));
    value = formatNumberUsingCommas(num);
    if (num < 0) {
      return "(" + value.replace(/-/, "") + ")";
    } else {
      return value;
    }
  } else {
    return "n/a";
  }
}

export function formatNumberUsingCommas(nStr) {
  if (!isNumeric(nStr)) {
    return nStr;
  }
  nStr = String(nStr);
  var x = nStr.split("."),
    formattedNumberString = numberFormat(nStr, x.length > 1 ? x[1].length : 0);
  return formattedNumberString;
}

export function numberFormat(number, decimals, decPoint, thousandsSep) {
  var n = number,
    c = isNaN((decimals = Math.abs(decimals))) ? 2 : decimals,
    d = decPoint === undefined ? "." : decPoint,
    t = thousandsSep === undefined ? "," : thousandsSep,
    s = n < 0 ? "-" : "",
    i = String(
      parseInt((n = ((n < 0 ? -n : +n) || 0).toFixed(Math.min(c, 20))), 10)
    ),
    j = i.length > 3 ? i.length % 3 : 0;
  return (
    s +
    (j ? i.substr(0, j) + t : "") +
    i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
    (c
      ? d +
      Math.abs(n - i)
        .toFixed(Math.min(c, 20))
        .slice(2)
      : "")
  );
}

export function twoDecimalsPercentFormatterDefaultZero(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  className
) {
  if (value == null || value === "") {
    value = 0;
  }
  if (className != null && className != undefined) {
    return (
      '<div class="aln-rt ' +
      className +
      '">' +
      Number(value).toFixed(2) +
      "%</div>"
    );
  } else {
    return '<div class="aln-rt">' + Number(value).toFixed(2) + "%</div>";
  }
}

export function plainDecimalsPercentFormatter(value, numberOfDecimalpoints) {
  if (value === undefined || value == null || value === "") {
    return "n/a";
  } else {
    if (numberOfDecimalpoints === undefined || _.isNaN(numberOfDecimalpoints)) {
      return Number(value).toFixed(2);
    } else {
      return Number(value).toFixed(numberOfDecimalpoints);
    }
  }
}

export function clickableAmountFormatterDefaultZero(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  var numberFormatValue;
  numberFormatValue = numberFormatterDefaultZero(
    row,
    cell,
    value,
    columnDef,
    dataContext
  );
  if (row == null || cell == null) {
    return numberFormatValue;
  } else {
    return "<a>" + numberFormatValue + "</a>";
  }
}

export function clickablePercentFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  var result = percentFormatter(row, cell, value, columnDef, dataContext);
  return "<a>" + result + "</a>";
}


