export function numberComparator(a, b) {
  if (a == null) {
    return isSortAsc ? 1 : -1;
  }
  if (b == null) {
    return isSortAsc ? -1 : 1;
  }
  return a - b;
}

export function percentComparator(a, b) {
  // pushing null's and N/As to the bottom.
  if (a == null || a === "N/A") {
    return isSortAsc ? 1 : -1;
  }
  if (b == null || b === "N/A") {
    return isSortAsc ? -1 : 1;
  }

  // a and b are %s. removing % and comparing the numbers.
  a = a.substring(0, a.length - 1);
  b = b.substring(0, b.length - 1);

  return a - b;
}

export function textComparator(a, b) {
  
  if (a == null ) {
    return isSortAsc ? 1 : -1;
  }
  if (b == null ) {
    return isSortAsc ? -1 : 1;
  }

 return a.toLowerCase() < b.toLowerCase() ? -1 : 1;
}
