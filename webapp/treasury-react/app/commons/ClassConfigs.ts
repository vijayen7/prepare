/*
 * Constants to describe class names (Can be used as typeof <CLASS_NAME> while adding @CLASS property to data interfaces)
 */
export const CLASS_NAMESPACE = {
  deshaw: {
    treasury: {
      common: {
        model: {
          comet: {
            LcmPositionDataParam: "deshaw.treasury.common.model.comet.LcmPositionDataParam",
          },
          margin: {
            MarginReportingFilter: "deshaw.treasury.common.model.margin.MarginReportingFilter",
          }
        }
      }
    }
  },
  com: {
    arcesium: {
      treasury: {
        margin: {
          model: {
            PositionRuleData: "com.arcesium.treasury.margin.model.PositionRuleData",
            MarginDataParam: "com.arcesium.treasury.margin.model.MarginDataParam",
            MarginDetails: "com.arcesium.treasury.margin.model.MarginDetails",
            PositionMarginData: "com.arcesium.treasury.margin.model.PositionMarginData",
            PositionMarginRuleInput: "com.arcesium.treasury.margin.model.PositionMarginRuleInput",
            IndependentAmountRuleFilter: "com.arcesium.treasury.margin.model.IndependentAmountRuleFilter",
            EnrichedIndependentAmountRule: "com.arcesium.treasury.margin.model.EnrichedIndependentAmountRule",
            TradeIndependentAmountFilter:"com.arcesium.treasury.margin.model.TradeIndependentAmountFilter",
            TradeIndependentAmountDetail:"com.arcesium.treasury.margin.model.TradeIndependentAmountDetail",
            ModifyIndependentAmountParam:"com.arcesium.treasury.margin.model.ModifyIndependentAmountParam"
          }
        },
        model: {
          common: {
            security: {
              SecurityFilter: "com.arcesium.treasury.model.common.security.SecurityFilter",
              TreasurySecurity: "com.arcesium.treasury.model.common.security.TreasurySecurity",
              ExtendedSecurityData: "com.arcesium.treasury.model.common.security.ExtendedSecurityData"
            }
          },
          seclend: {
            LendReportDataFilter: "com.arcesium.treasury.model.seclend.LendReportDataFilter",
            OutperformanceRuleFilter: "com.arcesium.treasury.model.seclend.OutperformanceRuleFilter",
            BrokerSecurityFilter: "com.arcesium.treasury.model.seclend.BrokerSecurityFilter",
            MarketDataFilter: "com.arcesium.treasury.model.seclend.MarketDataFilter",
            negotiation: {
              RateNegotiationVerificationFilter: "com.arcesium.treasury.model.seclend.negotiation.RateNegotiationVerificationFilter",
              VerificationResult: "com.arcesium.treasury.model.seclend.negotiation.VerificationResult",
              PositionRateNegotiation: "com.arcesium.treasury.model.seclend.negotiation.PositionRateNegotiation",
              LotPositionRateNegotiation: "com.arcesium.treasury.model.seclend.negotiation.LotPositionRateNegotiation"
            }
          }
        }
      }
    }
  },
  arcesium: {
    treasury: {
      margin: {
        common: {
          model: {
            MarginRuleKey: "arcesium.treasury.margin.common.model.MarginRuleKey"
          }
        }
      }
    }
  }
} as const;

export default CLASS_NAMESPACE;
