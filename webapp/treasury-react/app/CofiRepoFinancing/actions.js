import {
    FETCH_COFI_REPO_FINANCING_DATA
} from "./constants";

export function fetchCofiRepoFinancingData(payload) {
    return {
      type: FETCH_COFI_REPO_FINANCING_DATA,
      payload
    };
}
