export function cofiRepoFinancingColumns() {
  var columns = [
    {
      id: "ruleId",
      type: "number",
      name: "Rule Id",
      field: "ruleId",
      toolTip: "Rule Id",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50
    },
    {
      id: "gboTypeId",
      name: "GBO Type",
      field: "gboType",
      toolTip: "GBO Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "foTypeId",
      name: "FO Type",
      field: "foType",
      toolTip: "FO Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "foSubTypeId",
      name: "FO Sub Type",
      field: "foSubType",
      toolTip: "FO Sub Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "countryId",
      name: "Country",
      field: "country",
      toolTip: "Country",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "countryRegionId",
      name: "Region",
      field: "countryRegion",
      toolTip: "Region",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "pnlSpn",
      type: "text",
      name: "SPN",
      field: "pnlSpnDesc",
      toolTip: "SPN",
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "termInDays",
      type: "text",
      name: "Term In Days",
      field: "termInDays",
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "applicableRate",
      type: "text",
      name: "Applicable Rate",
      field: "applicableRate",
      toolTip: "Applicable Rate",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    }
  ];
  return columns;
}
