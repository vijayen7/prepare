export function cofiRepoFinancingGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    expandTillLevel: -1,
    highlightRowOnClick: true,
    editable: false,
    asyncEditorLoading: false,
    autoEdit: false,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    sortList: [{ columnId: 'ruleId', sortAsc: true }]
  };
  return options;
}
