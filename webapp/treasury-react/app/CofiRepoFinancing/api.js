import { BASE_URL } from "commons/constants";
import { fetchURL } from 'commons/util';
import { convertJavaDate } from "commons/util";

const queryString = require("query-string");
export let url = "";

export function getCofiRepoFinancingData(payload) {
    const paramString = queryString.stringify(payload);
    url = `${BASE_URL}service/cofiRepoFinancingRuleService/getCofiRepoFinancingRule?inputFormat=PROPERTIES&${paramString}&format=JSON`;
    return fetch(url, {
        credentials: "include"
    }).then(data => data.json())
}
