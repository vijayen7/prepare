import { put, takeEvery, call, all } from 'redux-saga/effects';
import { START_LOADING, END_LOADING, HANDLE_EXCEPTION } from 'commons/constants';
import {
    FETCH_COFI_REPO_FINANCING_DATA
} from "./constants";
import {
  getCofiRepoFinancingData
 } from './api';

function* fetchCofiRepoFinancingData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getCofiRepoFinancingData, action.payload);
    yield put({ type: `${action.type}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchCofiRepoFinancingDataSaga() {
  yield takeEvery(FETCH_COFI_REPO_FINANCING_DATA, fetchCofiRepoFinancingData);
}

function* CofiRepoFinancingSaga() {
  yield all([fetchCofiRepoFinancingDataSaga()]);
}

export default CofiRepoFinancingSaga;
