import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from 'commons/components/GridWithCellClick';
import { bindActionCreators } from 'redux';
import Message from 'commons/components/Message';
import { cofiRepoFinancingColumns } from './../gridConfig/cofiRepoFinancingColumnConfig';
import { cofiRepoFinancingGridOptions } from './../gridConfig/gridOptions';
import FilterButton from 'commons/components/FilterButton';
import { Layout } from 'arc-react-components';

class CofiRepoFinancingGrid extends Component {
  constructor(props) {
    super(props);
  }

  renderGrid = () => {
    if (_.isEmpty(this.props.cofiRepoFinancingData) || this.props.cofiRepoFinancingData.length <= 0) {
      return (
      <div>
        <Message messageData="Search to load results." />
      </div>
      );
    }
    return <Grid
            data={this.props.cofiRepoFinancingData}
            gridId="cofiRepoFinancingGrid"
            gridColumns={cofiRepoFinancingColumns()}
            gridOptions={cofiRepoFinancingGridOptions()}
            height={950}
            label="Search Results"
            />;
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <Layout.Child size={22}>
            <Layout>
              <Layout.Child>{this.renderGrid()}</Layout.Child>
            </Layout>
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    cofiRepoFinancingData: state.cofiRepoFinancing.cofiRepoFinancingData
  };
}

export default connect(
  mapStateToProps,
  null
)(CofiRepoFinancingGrid);
