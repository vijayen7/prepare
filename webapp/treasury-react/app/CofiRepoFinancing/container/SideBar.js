import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card } from "arc-react-components";
import Sidebar from 'commons/components/Sidebar';
import FilterButton from 'commons/components/FilterButton';
import TypeSubtypeFilter from "commons/container/TypeSubtypeFilter";
import GBOTypeFilter from "./../../commons/container/GBOTypeFilter";
import GenericSecurityFilter from "commons/components/GenericSecurityFilter";
import SpnFilterAutocomplete from "./../../commons/components/SpnFilterAutocomplete";
import InputNumberFilter from "./../../commons/components/InputNumberFilter";
import CountryFilter from "./../../commons/container/CountryFilter";
import RegionFilter from "./../../commons/container/RegionFilter";
import {
    fetchCofiRepoFinancingData
} from "./../actions";
import {
    getCommaSeparatedValues,
    getCommaSeparatedListValue,
    isAllKeySelected
} from "./../../commons/util";

class CofiRepoFinancingSideBar extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters = () => {
    return {
        securityFilter: {
          selectedSpns: "",
          selectedSecuritySearchType: { key: "TICKER", value: "TICKER" },
          selectedTextSearchType: { key: "EXACT_MATCH", value: "EXACT_MATCH" },
          securitySearchString: "",
          isAdvancedSearch: false
        },
        selectedTermInDays: "",
        selectedGBOTypes: [],
        selectedFoSubTypes: [],
        selectedTypeSubtype: {
          selectedFoTypes: [],
          selectedSubtypes: [],
        },
        selectedCountries: [],
        selectedRegions: []
    };
  }

  onSelect = (params) => {
    const { key, value } = params;
    let newState = {...this.state};
    newState[key] = value;
    this.setState(newState);
  }

  handleClick = () => {
    let payload = {
        ...(this.state.selectedTypeSubtype.selectedFoTypes.length > 0
            ? {
                "cofiRepoFinancingRuleFilter.foTypeIds": getCommaSeparatedValues(
                    this.state.selectedTypeSubtype.selectedFoTypes
                )
            }
        : null),
        ...(this.state.selectedTypeSubtype.selectedSubtypes.length > 0
            ? {
                "cofiRepoFinancingRuleFilter.foSubTypeIds": getCommaSeparatedValues(
                    this.state.selectedTypeSubtype.selectedSubtypes
                )
            }
        : null),
        ...(this.state.selectedGBOTypes.length > 0
                ? {
                    "cofiRepoFinancingRuleFilter.gboTypeIds": getCommaSeparatedValues(
                        this.state.selectedGBOTypes
                    )
                }
        : null),
        ...(this.state.securityFilter.isAdvancedSearch &&
        this.state.securityFilter.securitySearchString.trim() !== ""
          ? {
              "cofiRepoFinancingRuleFilter.securityFilter.securitySearchType": this.state
                .securityFilter.selectedSecuritySearchType.value,
              "cofiRepoFinancingRuleFilter.securityFilter.textSearchType": this.state
                .securityFilter.selectedTextSearchType.value,
              "cofiRepoFinancingRuleFilter.securityFilter.searchStrings": getCommaSeparatedListValue(
                this.state.securityFilter.securitySearchString
              )
            }
          : {
              ...(this.state.securityFilter.selectedSpns &&
              this.state.securityFilter.selectedSpns.trim() !== ""
                ? {
                    "cofiRepoFinancingRuleFilter.pnlSpn": getCommaSeparatedListValue(
                      this.state.securityFilter.selectedSpns
                    )
                  }
                : null)
            }),
        ...(isFinite(this.state.selectedTermInDays) &&
            this.state.selectedTermInDays.trim() !==""
                  ? {
                      "cofiRepoFinancingRuleFilter.termInDays":
                          this.state.selectedTermInDays
                  }
          : {
              "cofiRepoFinancingRuleFilter.termInDays":
                "__null__"
          }),
        ...(this.state.selectedCountries.length > 0
            ? {
                "cofiRepoFinancingRuleFilter.countryIds": getCommaSeparatedValues(
                  this.state.selectedCountries
                )
            }
        : null),
        ...(this.state.selectedRegions.length > 0
             ? {
                 "cofiRepoFinancingRuleFilter.countryRegionIds": getCommaSeparatedValues(
                   this.state.selectedRegions
                 )
             }
        : null)
    }
    this.props.fetchCofiRepoFinancingData(payload);
  }

  render() {
    return (
      <Sidebar>
        <Card>
          <GBOTypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedGBOTypes}
            stateKey={'selectedGBOTypes'}
            multiSelect={true}
            label={'GBO Type'}
          />
          <TypeSubtypeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedTypeSubtype}
          />
        </Card>
        <Card>
          <CountryFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCountries}
            stateKey={'selectedCountries'}
            multiSelect={true}
            label={'Country'}
          />
          <RegionFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedRegions}
          stateKey={'selectedRegions'}
          multiSelect={true}
          label={'Region'}
          />
        </Card>
        <Card>
          <GenericSecurityFilter
            onSelect={this.onSelect}
            selectedData={this.state.securityFilter}
            stateKey="securityFilter"
          />
        </Card>
        <Card>
          <InputNumberFilter
            onSelect={this.onSelect}
            data={this.state.selectedTermInDays}
            stateKey={'selectedTermInDays'}
            label={'Term In Days'}
          />
        </Card>
        <FilterButton onClick={this.handleClick} reset={false} label="Search" />
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
        fetchCofiRepoFinancingData,
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(CofiRepoFinancingSideBar);
