import { combineReducers } from "redux";
import {
  FETCH_COFI_REPO_FINANCING_DATA
} from "./constants";
import {
  RESIZE_CANVAS
} from '../commons/constants';

function cofiRepoFinancingDataSearchMessageReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_COFI_REPO_FINANCING_DATA}_SUCCESS`:
      if(action.data == null && typeof action.data.message !== "undefined" && typeof action.data.localizedMessage !== "undefined")
        return [];
      else
        return action.data

  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

const rootReducer = combineReducers({
  cofiRepoFinancingData: cofiRepoFinancingDataSearchMessageReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
