import React, { Component } from "react";
import CofiRepoFinancingSideBar from "./container/SideBar";
import CofiRepoFinancingGrid from "./container/Grid";
import Loader from "commons/container/Loader";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FilterButton from 'commons/components/FilterButton';
import { Layout } from 'arc-react-components';

class CofiRepoFinancing extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.getBreadCrumbs = this.getBreadCrumbs.bind(this);
    }

  getBreadCrumbs() {
    return [
      {
        name: 'Treasury-Financing',
        link: '#'
      },
      {
        name: 'Cofi Repo Financing',
       link: 'cofiRepoFinancing.html'
      }
    ];
  }

  componentDidMount() {
      var header = this.header;
      var breadcrumbs = this.getBreadCrumbs();

      if (header.ready) {
        header.setBreadcrumb(breadcrumbs);
      } else {
        header.addEventListener('ready', header.setBreadcrumb(breadcrumbs));
     }
  }

    render() {
        return (
            <React.Fragment>
                <Loader />
                <Layout>
                  <Layout.Child>
                      <arc-header
                          ref={header => {
                          this.header = header;
                          }}
                          className="size--content"
                          user={USER}
                          modern-themes-enabled
                      />
                      <Layout isColumnType>
                          <Layout.Child size={3}>
                              <CofiRepoFinancingSideBar />
                          </Layout.Child>
                          <Layout.Child size={0.1} />
                          <Layout.Child size={12}>
                              <CofiRepoFinancingGrid/>
                          </Layout.Child>
                      </Layout>
                  </Layout.Child>
                </Layout>
            </React.Fragment>
        );
    }
}

export default CofiRepoFinancing;
