import { put, takeEvery, call, all } from "redux-saga/effects";
import { getWorkflowStatusData } from "./api";
import {
  FETCH_WORKFLOW_STATUS_DATA,
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from "commons/constants";

function* fetchWorkflowStatusData(action) {
  try {
    yield put({type: START_LOADING});
    const data = yield call(getWorkflowStatusData, action.payload);
    yield put({ type: `${FETCH_WORKFLOW_STATUS_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_WORKFLOW_STATUS_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* workflowStatusData() {
  yield [
    takeEvery(FETCH_WORKFLOW_STATUS_DATA, fetchWorkflowStatusData)
  ];
}

function* workflowStatusSaga() {
  yield all([workflowStatusData()]);
}

export default workflowStatusSaga;
