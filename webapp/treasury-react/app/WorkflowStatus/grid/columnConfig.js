import { convertJavaNYCDateTime, convertJavaNYCDashedDate } from "commons/util";
import { textComparator } from "commons/grid/comparators";

export function gridColumns() {
  function getUpdatedAt(dataContext) {
    var when = (dataContext.endTime) ? dataContext.endTime : dataContext.beginTime;
    return (when) ? convertJavaNYCDateTime(when) : null;
  }

  return [{
    id: "entityFamily",
    name: "Entity Family",
    field: "entityFamilyName",
    toolTip: "Entity Family",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b",
    filter: {
      isFilterOnFormattedValue: true
    },
  }, {
    id: "workflowType",
    name: "Workflow Type",
    field: "type",
    toolTip: "Workflow Type",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b"
  }, {
    id: "workflowDate",
    name: "Workflow Date",
    field: "date",
    toolTip: "Workflow Date",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b",
    formatter: function(row, cell, value, columnDef, dataContext) {
      if(value != null && value != undefined) return convertJavaNYCDashedDate(value);
      return null;
    },
    excelDataFormatter: function(value) {
      if (value !== undefined && value !== null) return convertJavaNYCDashedDate(value);
      else return null;
    },
    groupingFormatter: function(group) {
      console.log(group);
      if(group != null && group != undefined) {
        return convertJavaNYCDashedDate(group.value || "") || "N/A";
      }
      return "N/A";
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  }, {
    id: "workflowStatus",
    name: "Workflow Status",
    field: "status",
    toolTip: "Workflow Status",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b"
  }, {
    id: "login",
    name: "Login",
    field: "login",
    toolTip: "Login",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b"
  }, {
    id: "updatedAt",
    name: "Updated At",
    field: "updatedAt",
    toolTip: "Updated At",
    type: "text",
    filter: true,
    sortable: true,
    headerCssClass: "b",
    formatter: function(row, cell, value, columnDef, dataContext) {
      return getUpdatedAt(dataContext);
    },
    excelDataFormatter: function(value, rowElem, currentGrid, isCurrentViewBeingExported, columnDef) {
      return getUpdatedAt(rowElem);
    },
    comparator: function(a,b){
      return textComparator(getUpdatedAt(a),getUpdatedAt(b));
    },
    filter: {
      isFilterOnFormattedValue: true
    }
  }];
}
