export function gridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    page: true,
    onRowClick: function() {
      // highlight on row works if this callback is defined
    },
    highlightRowOnClick: true,
    multiColumnSort: true,
    maxHeight: 750,
    enableMultilevelGrouping: {
      showGroupingKeyInColumn: true
    },
    sheetName: "Workflow Status data"
  };
  return options;
}
