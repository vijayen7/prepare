import React, { Component } from "react";
import Loader from "commons/container/Loader";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";

export default class WorkflowStatus extends Component {
  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
           
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div slot="application-menu" className="application-menu-toggle-view">
            </div>
          </arc-header>
          <div className="layout--flex">
            <div className="size--content">
              <SideBar />
            </div>
            <Grid />
          </div>
        </div>
      </React.Fragment>
    );
  }
}
