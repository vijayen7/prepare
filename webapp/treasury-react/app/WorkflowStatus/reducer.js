import { combineReducers } from "redux";
import { FETCH_WORKFLOW_STATUS_DATA, RESIZE_CANVAS } from "commons/constants";
import _ from "lodash";

function flattenWorkflowStatusData(dataArray) {
  return _.map(dataArray, data => {
    let {parameters, type, ...rest} = data;
    return {
      type: type.name,
      ...parameters,
      ...rest
    };
  })
}

function workflowStatusDataReducer (state = [], action) {
  switch(action.type) {
    case `${FETCH_WORKFLOW_STATUS_DATA}_SUCCESS`:
      return flattenWorkflowStatusData(action.data || []);
    default:
      return state;
  }
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

const rootReducer = combineReducers({
  workflowStatusData: workflowStatusDataReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
