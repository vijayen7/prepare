import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
export let url = "";

export function getWorkflowStatusData(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/workflowStatusService/getWorkflows?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}
