import { FETCH_WORKFLOW_STATUS_DATA, RESIZE_CANVAS } from "commons/constants";

export function fetchWorkflowStatusData(payload) {
  return {
    type: FETCH_WORKFLOW_STATUS_DATA,
    payload
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}
