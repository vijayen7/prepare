import React, { Component } from "react";
import { gridColumns } from "../grid/columnConfig";
import { gridOptions } from "../grid/gridOptions";
import Message from "commons/components/Message";

import { connect } from "react-redux";
import  ReactArcGrid from "commons/components/Grid";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
  }

  renderGridData() {
    if(this.props.workflowStatusData <= 0)
      return <Message messageData="Search to load data." />;

    return (
      <ReactArcGrid
        data={this.props.workflowStatusData}
        gridId="WorkflowStatusData"
        gridColumns={gridColumns()}
        gridOptions={gridOptions()}
        resizeCanvas={this.props.resizeCanvas}
      />
    );
  }

  render() {
    return (
      <div className="padding">
        {this.renderGridData()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    workflowStatusData: state.workflowStatus.workflowStatusData,
    resizeCanvas: state.workflowStatus.resizeCanvas
  };
}

export default connect(
  mapStateToProps,
  null
)(Grid);
