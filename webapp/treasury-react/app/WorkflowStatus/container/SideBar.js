import React, { Component } from "react";
import { connect } from "react-redux";
import Sidebar from "commons/components/Sidebar";
import DateFilter from "commons/container/DateFilter";
import HedgeFundsFilter from "commons/container/HedgeFundsFilter";
import LegalEntityFamilyFilter from "commons/container/LegalEntityFamilyFilter";
import WorkflowTypeFilter from "commons/container/WorkflowTypeFilter";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import { fetchWorkflowStatusData, resizeCanvas } from "../actions";
import { getCommaSeparatedValuesOrNull } from "../../commons/util";
import SaveSettingsManager from "commons/components/SaveSettingsManager";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.getCurrentFilterState = this.getCurrentFilterState.bind(this);
    this.state = {};
  }

  componentDidMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      selectedStartDate: "",
      selectedEndDate: "",
      selectedLegalEntityFamilies: [],
      selectedWorkflowTypes: [],
      toggleSidebar: false
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  getCurrentFilterState(){
    let {toggleSidebar, ...filterState} = this.state;
    return filterState;
  }

  onSelect(params) {
    let {key, value} = params;
    this.setState((previousState) => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  handleClick() {
    let payload = {
      "workflowFilter.startDate": this.state.selectedStartDate,
      "workflowFilter.endDate": this.state.selectedEndDate,
      "workflowFilter.entityFamilyIds": getCommaSeparatedValuesOrNull(this.state.selectedLegalEntityFamilies),
      "workflowFilter.workflowTypeIds": getCommaSeparatedValuesOrNull(this.state.selectedWorkflowTypes)
    };
    this.props.fetchWorkflowStatusData(payload);
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  render() {
    const clientName = CODEX_PROPERTIES['codex.client_name'];
    const ClientBasedLegalEntityFamilyFilter = ['desco', 'deshaw', 'terra'].indexOf(clientName) > -1
      ? HedgeFundsFilter : LegalEntityFamilyFilter;
    return (
      <Sidebar
        collapsible={true}
        resizeCanvas={this.props.resizeCanvas}
        toggleSidebar={this.state.toggleSidebar}
      >
        <SaveSettingsManager
          selectedFilters={this.getCurrentFilterState()}
          applySavedFilters={this.applySavedFilters}
          applicationName="workflowStatusFilter"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedStartDate"
          data={this.state.selectedStartDate}
          label="Start Date"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedEndDate"
          data={this.state.selectedEndDate}
          label="End Date"
        />
        <WorkflowTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedWorkflowTypes}
        />
        <ClientBasedLegalEntityFamilyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntityFamilies}
        />
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

const mapDispatchToProps = {
  fetchWorkflowStatusData,
  resizeCanvas
}

export default connect(null, mapDispatchToProps) (SideBar);
