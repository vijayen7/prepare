import { hot } from "react-hot-loader/root";
import React, { Component } from "react";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";
import Loader from "commons/container/Loader";
import { resetAvailabilityDataSearchMessage } from "./actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class AvailabilityData extends Component {
  constructor(props) {
    super(props);
    this.setBreadcrumb = this.setBreadcrumb.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = [];
  }

  componentDidMount() {
    this.lotView.classList.add("active");
    this.setState({ view: "lotView" });
    this.setBreadcrumb();
  }

  setBreadcrumb() {
    var header = this.header;
    var breadcrumbs = [
      {
        name: 'Treasury',
        link: '/treasury'
      },
      {
        name: 'Availability Data',
       link: './availabilitydata.html'
      }
    ];

    if (header.ready) {
      header.setBreadcrumb(breadcrumbs);
    } else {
      header.addEventListener('ready', header.setBreadcrumb(breadcrumbs));
   }
  }

  handleToggle(view) {
    this.lotView.classList.remove("active");
    this.positionView.classList.remove("active");
    switch (view) {
      case "lotView":
        this.lotView.classList.add("active");
        break;
      case "positionView":
        this.positionView.classList.add("active");
        break;
    }
    this.setState({ view });
    this.props.resetAvailabilityDataSearchMessage();
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div
              slot="application-menu"
              className="application-menu-toggle-view"
            >
              <a
                ref={lotView => {
                  this.lotView = lotView;
                }}
                onClick={() => this.handleToggle("lotView")}
              >
                Lot View
              </a>
              <a
                ref={positionView => {
                  this.positionView = positionView;
                }}
                onClick={() => this.handleToggle("positionView")}
              >
                Position View
              </a>
            </div>
          </arc-header>
          <div className="layout--flex padding--top">
            <div className="size--content padding--right">
              <SideBar view={this.state.view}/>
            </div>
            <Grid view={this.state.view}/>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetAvailabilityDataSearchMessage
    },
    dispatch
  );
}

export default hot(connect(null, mapDispatchToProps)(AvailabilityData));
