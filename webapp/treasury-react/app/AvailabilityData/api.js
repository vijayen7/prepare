import {
  BASE_URL
} from "commons/constants";

const queryString = require("query-string");
export let url = "";
export function getLotViewAvailabilityData(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/availabilityDataService/getAvailabilityData?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getRolledUpAvailabilityData(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/availabilityDataService/getBrokerAvailabilityData?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}
