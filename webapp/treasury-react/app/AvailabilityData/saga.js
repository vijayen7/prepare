import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
  RESIZE_CANVAS
} from "commons/constants";
import {
  DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA,
  DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA,
  FETCH_LOT_VIEW_AVAILABILITY_DATA,
  FETCH_POSITION_VIEW_AVAILABILITY_DATA,
  UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE
} from "./constants";
import {
  getLotViewAvailabilityData,
  getRolledUpAvailabilityData
} from "./api";

function* fetchLotViewAvailabilityData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLotViewAvailabilityData, action.payload);
    yield put({ type: `${FETCH_LOT_VIEW_AVAILABILITY_DATA}_SUCCESS`, data });
    yield put({ type: UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LOT_VIEW_AVAILABILITY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchPositionViewAvailabilityData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getRolledUpAvailabilityData, action.payload);
    yield put({ type: `${FETCH_POSITION_VIEW_AVAILABILITY_DATA}_SUCCESS`, data });
    yield put({ type: UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_POSITION_VIEW_AVAILABILITY_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* destroyLotViewAvailabilityData(action) {
  try {
    yield put({ type: `${DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA}_SUCCESS` });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  }
}

function* destroyRolledupLevelAvailabilityData(action) {
  try {
    yield put({ type: `${DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA}_SUCCESS` });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  }
}

function* lotViewAvailabilityData() {
  yield [takeEvery(FETCH_LOT_VIEW_AVAILABILITY_DATA, fetchLotViewAvailabilityData)];
  yield [takeEvery(DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA, destroyLotViewAvailabilityData)];
}

function* positionViewAvailabilityData() {
  yield [takeEvery(FETCH_POSITION_VIEW_AVAILABILITY_DATA, fetchPositionViewAvailabilityData)]
  yield [takeEvery(DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA, destroyRolledupLevelAvailabilityData)];
}

function* availabilityDataSaga() {
  yield all([
    lotViewAvailabilityData(),
    positionViewAvailabilityData()
  ]);
}

export default availabilityDataSaga;
