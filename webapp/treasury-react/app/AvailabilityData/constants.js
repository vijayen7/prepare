export const DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA = "DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA";
export const DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA = "DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA";
export const FETCH_LOT_VIEW_AVAILABILITY_DATA = "FETCH_LOT_VIEW_AVAILABILITY_DATA";
export const FETCH_POSITION_VIEW_AVAILABILITY_DATA = "FETCH_POSITION_VIEW_AVAILABILITY_DATA";
export const UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE = "UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE";
export const RESET_AVAILABILITY_DATA_SEARCH_MESSAGE = "RESET_AVAILABILITY_DATA_SEARCH_MESSAGE";
