import {
  SEARCH_IS_TOO_GENERAL,
  RESIZE_CANVAS,
  DEFAULT_SEARCH_MESSAGE,
  NO_DATA_SEARCH_MESSAGE
} from "commons/constants";
import {
  FETCH_LOT_VIEW_AVAILABILITY_DATA,
  FETCH_POSITION_VIEW_AVAILABILITY_DATA,
  DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA,
  DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA,
  UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE,
  RESET_AVAILABILITY_DATA_SEARCH_MESSAGE
} from "./constants";
import { combineReducers } from "redux";

function availabilityDataSearchMessageReducer(
  state = DEFAULT_SEARCH_MESSAGE,
  action
) {
  switch (action.type) {
    case UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE:
      if(action.isSearchTooGeneral)
        return SEARCH_IS_TOO_GENERAL;
      else
        return !action.data || action.data.length <= 0
          ? NO_DATA_SEARCH_MESSAGE
          : "";
    case RESET_AVAILABILITY_DATA_SEARCH_MESSAGE:
      return DEFAULT_SEARCH_MESSAGE;
    default:
      return state;
  }
}

function lotViewAvailabilityDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LOT_VIEW_AVAILABILITY_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA:
      return [];
  }
  return state;
}

function positionViewAvailabilityDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_POSITION_VIEW_AVAILABILITY_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA:
      return [];
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

const rootReducer = combineReducers({
  lotViewAvailabilityData: lotViewAvailabilityDataReducer,
  positionViewAvailabilityData: positionViewAvailabilityDataReducer,
  availabilityDataSearchMessage:availabilityDataSearchMessageReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
