import { url } from "../api";
import { getSaveSettingsUrl } from "commons/util";

export function gridOptions(view) {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    },
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    frozenColumn: 2,
    sortList: [
      {
        columnId: "date",
        sortAsc: true
      }
    ],
    page: true,
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    sheetName:(view == "lotView")? "Raw_Level_Availability_Data":"Rolledup_Level_Availability_Data",
    saveSettings: {
      applicationId: 3,
      applicationCategory: "AvailabilityData",
      serviceURL: getSaveSettingsUrl(),
      onBeforeLoadedCallback: function(settingsData) {},
      loadedCallback: function(gridObject) {}
    },
  };
  return options;
}
