import { convertJavaNYCDate, getCommaSeparatedNumber } from "commons/util";

function allColumns() {
  return [
    {
      id: "date",
      name: "Date",
      field: "date",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 120
    },
    {
      name: "CCY",
      id: "currency",
      field: "ccyName",
      toolTip: "Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 70
    },
    {
      id: "isin",
      name: "ISIN",
      field: "isin",
      toolTip: "isin",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 90
    },
    {
      id: "cusip",
      name: "CUSIP",
      field: "cusip",
      toolTip: "CUSIP",
      type: "text",
      filter: true,
      sortable: true,
      width: 90,
      headerCssClass: "b"
    },
    {
      name: "SEDOL",
      id: "sedol",
      field: "sedol",
      toolTip: "SEDOL",
      type: "text",
      filter: true,
      sortable: true,
      width: 80,
      headerCssClass: "b"
    },
    {
      name: "Ticker",
      id: "ticker",
      field: "ticker",
      toolTip: "Ticker",
      type: "text",
      filter: true,
      sortable: true,
      width: 60,
      headerCssClass: "b"
    },
    {
      name: "Security Name",
      id: "securityName",
      field: "securityName",
      toolTip: "Security Name",
      type: "text",
      filter: true,
      sortable: true,
      width: 90,
      headerCssClass: "b"
    },
    {
      name: "Availability",
      field: "availability",
      id: "availability",
      toolTip: "Availability",
      type: "number",
      filter: true,
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return getCommaSeparatedNumber(value);
      },
      excelDataFormatter: function (value) {
        return getCommaSeparatedNumber(value);
      },
      width: 100,
      headerCssClass: "b"
    },
    {
      name: "Stock Loan Fee Rate (%)",
      id: "stokLoanFeeRate",
      field: "stockLoanFeeRate",
      type: "number",
      filter: true,
      sortable: true,
      width: 120,
      headerCssClass: "b",
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
    },
    {
      name: "SPN",
      id: "spn",
      field: "spn",
      type: "text",
      filter: true,
      sortable: true,
      width: 80,
      headerCssClass: "b"
    },
    {
      name: "Classification",
      id: "calssifcation",
      field: "classification",
      type: "text",
      filter: true,
      sortable: true,
      width: 60,
      headerCssClass: "b"
    },
    {
      name: "Internal Classification",
      field: "internalClassification",
      id: "internalClassification",
      type: "text",
      filter: true,
      sortable: true,
      width: 60,
      headerCssClass: "b"
    },
    {
      name: "Country",
      field: "country",
      id: "country",
      type: "text",
      filter: true,
      sortable: true,
      width: 60,
      headerCssClass: "b"
    },
    {
      name: "Gbo Type",
      field: "gboTypeName",
      id: "gboTypeName",
      type: "text",
      filter: true,
      sortable: true,
      width: 60,
      headerCssClass: "b"
    },
    {
      name: "Source",
      field: "fileAliasName",
      id: "fileAliasName",
      type: "text",
      filter: true,
      sortable: true,
      width: 90,
      headerCssClass: "b"
    },
    {
      name: "Source Date",
      field: "sourceDate",
      id: "sourceDate",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDate(value);
      },
      width: 90,
      headerCssClass: "b",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
  ]
}

export function lotViewColumns() {
  const columns = ["date",
    "cpeName",
    "spn",
    "ccyName",
    "isin",
    "cusip",
    "sedol",
    "ticker",
    "securityName",
    "availability",
    "stockLoanFeeRate",
    "classification",
    "internalClassification",
    "country",
    "gboTypeName",
    "fileAliasName"];
  return columns.map((column) => _.find(allColumns(), { field: column }));
}

export function positionViewColumns() {
  const columns = ["date",
    "cpeName",
    "spn",
    "ccyName",
    "isin",
    "cusip",
    "sedol",
    "ticker",
    "securityName",
    "availability",
    "stockLoanFeeRate",
    "classification",
    "internalClassification",
    "country",
    "gboTypeName",
    "sourceDate"];
  return columns.map((column) => _.find(allColumns(), { field: column }));
}
