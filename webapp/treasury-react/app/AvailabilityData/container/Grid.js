import React, { Component } from "react";
import {
  lotViewColumns,
  positionViewColumns
} from "../grid/columnConfig";
import { Layout, Panel } from "arc-react-components";
import { gridOptions } from "../grid/gridOptions";
import Resizer from "commons/components/Resizer";
import ResizerContainer from "commons/container/ResizerContainer";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GridWithCellClick from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import Dialog from "commons/components/Dialog";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import {
destroyLotViewAvailabilityData,
destroyRolledupLevelAvailabilityData
} from "../actions";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.reportingCurrencyMessage = this.reportingCurrencyMessage.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }

  componentWillUnmount() {
    if(this.props.view == "lotView"){
      this.props.destroyLotViewAvailabilityData();
    }else {
      this.props.destroyRolledupLevelAvailabilityData();
    }
  }

  reportingCurrencyMessage(data) {
    let isReportingFxNull = false;
    if(!data) {
      for (let i=0; i<data.length; i++) {
        const { reportingFxRate } = data[i];
        if (!reportingFxRate) {
          isReportingFxNull = true;
        }
      }
    }
    if(isReportingFxNull){
      return (
        <ul className="bullet">
          <li className="critical">{`Fx Rate unavailable for selected Reporting Currency`}</li>
        </ul>);
    }
    return (
      <small><ul className="bullet">
        <li>{`Reporting Currency (RC) corresponds to ${data[0].reportingCurrency}`}</li>
      </ul></small>
    );
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  renderGridData() {
    if(this.props.view == "lotView"){
      if (this.props.lotViewAvailabilityData.length <= 0)
        return <Message messageData={this.props.availabilityDataSearchMessage} />;
      return (
        <React.Fragment>
          <Layout>
            <Layout.Child size={8}>
              <GridWithCellClick
                data={this.props.lotViewAvailabilityData}
                gridId="lotViewAvailabilityData"
                gridColumns={lotViewColumns()}
                gridOptions={gridOptions()}
                resizeCanvas={this.props.resizeCanvas}
                fill={true}
              />
            </Layout.Child>
            <Layout.Child size="Fit" childId="rcGridChild4">
              {this.reportingCurrencyMessage(this.props.lotViewAvailabilityData)}
            </Layout.Child>
          </Layout>
        </React.Fragment>
      );
    } else {
      if (this.props.positionViewAvailabilityData.length <= 0)
        return <Message messageData={this.props.availabilityDataSearchMessage} />;
      return (
        <React.Fragment>
          <Layout>
            <Layout.Child size={8}>
              <GridWithCellClick
                data={this.props.positionViewAvailabilityData}
                gridId="positionViewAvailabilityData"
                gridColumns={positionViewColumns()}
                gridOptions={gridOptions()}
                resizeCanvas={this.props.resizeCanvas}
                fill={true}
              />
            </Layout.Child>
            <Layout.Child size="Fit" childId="rcGridChild4">
              {this.reportingCurrencyMessage(this.props.positionViewAvailabilityData)}
            </Layout.Child>
          </Layout>
        </React.Fragment>
      );
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.renderGridData()}
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyLotViewAvailabilityData,
      destroyRolledupLevelAvailabilityData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    positionViewAvailabilityData: state.availabilityData.positionViewAvailabilityData,
    lotViewAvailabilityData: state.availabilityData.lotViewAvailabilityData,
    availabilityDataSearchMessage: state.availabilityData.availabilityDataSearchMessage,
    resizeCanvas: state.resizeCanvas
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
