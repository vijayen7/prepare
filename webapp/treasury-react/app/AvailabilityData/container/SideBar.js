import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Card, Layout, DateRangePicker } from "arc-react-components";
import Warning from "commons/components/Warning";
import CurrencyFilter from "commons/container/CurrencyFilter";
import FileAliasFilter from "commons/container/FileAliasFilter";
import SidebarWithCopySearchUrl from "commons/components/SidebarWithCopySearchUrl";
import Label from "commons/components/Label";
import ColumnLayout from "commons/components/ColumnLayout";
import Column from "commons/components/Column";
import CheckboxFilter from "commons/components/CheckboxFilter";
import FilterButton from "commons/components/FilterButton";
import CopySearchUrl from "commons/components/CopySearchUrl";
import GenericSecurityFilter from "commons/components/GenericSecurityFilter";
import ClassificationFilter from "../../MarketData/components/ClassificationFilter";
import CpeFilter from "commons/container/CpeFilter";
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";
import Dialog from "commons/components/Dialog";

import {
  fetchLotViewAvailabilityData,
  fetchPositionViewAvailabilityData,
  destroyLotViewAvailabilityData,
  destroyRolledupLevelAvailabilityData,
  resizeCanvas,
  searchIsTooGeneral
} from "../actions";
import {
  getCommaSeparatedValues,
  getCommaSeparatedListValue,
  getCommaSeparatedValuesOrNullForSingleSelect,
  isAllKeySelected,
  getDateDifference,
  getCurrentDate,
  executeCopySearchUrl
} from "commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.state = {
      showInvalidSPNDailog: false
    };
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  resizeCanvas() {
    this.props.resizeCanvas();
  }

  getDefaultFilters() {
    return {
      securityFilter: {
        selectedSpns: "",
        selectedSecuritySearchType: { key: "TICKER", value: "TICKER" },
        selectedTextSearchType: { key: "EXACT_MATCH", value: "EXACT_MATCH" },
        securitySearchString: "",
        isAdvancedSearch: false
      },
      selectedCurrencies: [],
      selectedCpes: [],
      selectedFileAliases: [],
      includeGC: false,
      includeHTB: false,
      includeNonGC: false,
      includeETB: false,
      includeSpecial: false,
      selectedStartDate: getCurrentDate(),
      selectedEndDate: getCurrentDate(),
      toggleSidebar: false,
      showInvalidSPNDailog: false,
      selectedReportingCurrency: { key: 1760000, value: `USD [1760000]`}
    };
  }

  componentDidMount() {
    this.node.addEventListener("keydown", this.enterFunction, false);
    let copySearchUrl = executeCopySearchUrl();
    if (copySearchUrl.isTrue) {
      this.setState(copySearchUrl.copySearchUrlState, () => this.handleClick());
    }
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleCloseDialog() {
    this.setState({
      showInvalidSPNDailog: false
    });
  }

  enterFunction = e => {
    if (e.keyCode == 13) {
      const node = e.path[0];
      if (node.id !== undefined && node.id === "sidebar") {
        this.handleClick();
        this.node.blur();
      } else this.node.focus();
    }
  };

  handleClick() {
    var payload = {
      "availabilityDataFilter.startDate": this.state.selectedStartDate,
      "availabilityDataFilter.endDate": this.state.selectedEndDate,
      ...((this.state.selectedCpes.length > 0 &&
        !isAllKeySelected(this.state.selectedCpes))
        ? {
          "availabilityDataFilter.cpeIds": getCommaSeparatedValues(
            this.state.selectedCpes
          )
        }
        : null),
      ...(this.state.securityFilter.isAdvancedSearch &&
        this.state.securityFilter.securitySearchString.trim() !== ""
        ? {
          "availabilityDataFilter.securityFilter.securitySearchType": this.state
            .securityFilter.selectedSecuritySearchType.value,
          "availabilityDataFilter.securityFilter.textSearchType": this.state
            .securityFilter.selectedTextSearchType.value,
          "availabilityDataFilter.securityFilter.searchStrings": getCommaSeparatedListValue(
            this.state.securityFilter.securitySearchString
          ),
        }
        : {
          ...(this.state.securityFilter.selectedSpns &&
            this.state.securityFilter.selectedSpns.trim() !== ""
            ? {
              "availabilityDataFilter.spns": getCommaSeparatedListValue(
                this.state.securityFilter.selectedSpns
              )
            }
            : null)
        }),
      ...((this.state.selectedCurrencies.length > 0 &&
        !isAllKeySelected(this.state.selectedCurrencies))
        ? {
          "availabilityDataFilter.currencyIds": getCommaSeparatedValues(
            this.state.selectedCurrencies
          )
        }
        : null),
      ...((this.state.selectedFileAliases.length > 0 &&
        !isAllKeySelected(this.state.selectedFileAliases))
        ? {
          "availabilityDataFilter.fileAliasIds": getCommaSeparatedValues(
            this.state.selectedFileAliases
          )
        }
        : null),
      "availabilityDataFilter.includeHTB": this.state.includeHTB,
      "availabilityDataFilter.includeVHTB": this.state.includeVHTB,
      "availabilityDataFilter.includeGC": this.state.includeGC,
      "availabilityDataFilter.reportingCurrencyId" : getCommaSeparatedValuesOrNullForSingleSelect(this.state.selectedReportingCurrency)
    };
    if (this.props.view == "lotView") {
      this.props.destroyLotViewAvailabilityData();
      if (!this.isSearchTooGeneral(payload)) {
        this.props.fetchLotViewAvailabilityData(payload);
      } else {
        this.props.searchIsTooGeneral();
      }
    } else {
      this.props.destroyRolledupLevelAvailabilityData();
      if (!this.isSearchTooGeneral(payload)) {
        delete payload["availabilityDataFilter.fileAliasIds"];
        this.props.fetchPositionViewAvailabilityData(payload);
      } else {
        this.props.searchIsTooGeneral();
      }
    }
    this.setState({ toggleSidebar: !this.state.toggleSidebar });
  }

  isSearchTooGeneral(payload) {
    for (let key in payload) {
      if (key != "availabilityDataFilter.startDate" &&
        key != "availabilityDataFilter.endDate" && payload[key] != null && payload[key].length > 0) {
        return false;
      }
    }
    return true;
  }

  onDateChange = date => {
    this.setState({ selectedStartDate: date[0], selectedEndDate: date[1] });
  };

  setFilters = filters => {
    this.setState({...this.state, ...filters}, this.handleClick);
  }

  render() {
    let spnDialog = null;
    let dateDiff = getDateDifference(
      this.state.selectedStartDate,
      this.state.selectedEndDate
    );
    let warnDialog = null;
    if (this.state.showInvalidSPNDailog) {
      spnDialog = (
        <Dialog
          isOpen={this.state.showInvalidSPNDailog}
          title="Enter valid SPN"
          onClose={this.handleCloseDialog}
          style={{
            height: "200px"
          }}
        >
          <h3>SPNs have to be entered for searching over a Date Range</h3>
        </Dialog>
      );
    }
    if (dateDiff >= 3) {
      warnDialog = (
        <Warning messageData="Search over a large Date Range may affect the performance" />
      );
    }

    return (
      <React.Fragment>
        {spnDialog}
        <div
          id="sidebar"
          ref={node => (this.node = node)}
          tabindex="0"
          style={{ height: "100%" }}
        >
          <SidebarWithCopySearchUrl
            collapsible={true}
            size="200px"
            resizeCanvas={this.resizeCanvas}
            handleSearch={this.handleClick}
            handleReset={this.handleReset}
            copySearchUrl
            setFilters={this.setFilters}
            selectedFilters={this.state}
          >
            <Card>
              {warnDialog}
              <DateRangePicker
                placeholder="Select Date"
                value={[
                  this.state.selectedStartDate,
                  this.state.selectedEndDate
                ]}
                onChange={this.onDateChange}
              />
            </Card>
            <Card>
              <GenericSecurityFilter
                onSelect={this.onSelect}
                selectedData={this.state.securityFilter}
                stateKey="securityFilter"
              />
            </Card>
            <Card>
              <CpeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCpes}
              />
              {(this.props.view === "lotView") ? (
                <React.Fragment>
                  <FileAliasFilter
                    onSelect={this.onSelect}
                    selectedData={this.state.selectedFileAliases}
                  />
                </React.Fragment>) : null
              }
              <CurrencyFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCurrencies}
              />
            </Card>
            <Card>
              <Column>
                <CheckboxFilter
                  onSelect={this.onSelect}
                  stateKey="includeGC"
                  label="GC"
                  style="left"
                  key="1"
                  defaultChecked={this.state.includeGC}
                />
                <CheckboxFilter
                  onSelect={this.onSelect}
                  stateKey="includeHTB"
                  label="HTB"
                  style="left"
                  key="2"
                  defaultChecked={this.state.includeHTB}
                />
                <CheckboxFilter
                  onSelect={this.onSelect}
                  stateKey="includeVHTB"
                  label="VHTB"
                  style="left"
                  key="3"
                  defaultChecked={this.state.includeVHTB}
                />
              </Column>
            </Card>
            <Card>
              <ReportingCurrencyFilter
                label="Reporting Currency"
                onSelect={this.onSelect}
                selectedData={this.state.selectedReportingCurrency}
                multiSelect={false}
                stateKey="selectedReportingCurrency"
              />
            </Card>
          </SidebarWithCopySearchUrl>
        </div>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchLotViewAvailabilityData,
      fetchPositionViewAvailabilityData,
      destroyRolledupLevelAvailabilityData,
      destroyLotViewAvailabilityData,
      resizeCanvas,
      searchIsTooGeneral
    },
    dispatch
  );
}
export default connect(
  null,
  mapDispatchToProps
)(SideBar);
