import {
  RESIZE_CANVAS
} from "commons/constants"
import {
  DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA,
  DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA,
  FETCH_LOT_VIEW_AVAILABILITY_DATA,
  FETCH_POSITION_VIEW_AVAILABILITY_DATA,
  RESET_AVAILABILITY_DATA_SEARCH_MESSAGE,
  UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE
} from "./constants";
export function fetchLotViewAvailabilityData(payload) {
  return {
    type: FETCH_LOT_VIEW_AVAILABILITY_DATA,
    payload
  };
}

export function fetchPositionViewAvailabilityData(payload) {
  return {
    type: FETCH_POSITION_VIEW_AVAILABILITY_DATA,
    payload
  };
}

export function destroyLotViewAvailabilityData(payload) {
  return {
    type: DESTROY_DATA_STATE_FOR_LOT_VIEW_AVAILABILITY_DATA,
    payload
  };
}

export function destroyRolledupLevelAvailabilityData(payload) {
  return {
    type: DESTROY_DATA_STATE_FOR_POSITION_VIEW_AVAILABILITY_DATA,
    payload
  };
}

export function resetAvailabilityDataSearchMessage() {
  return {
    type: RESET_AVAILABILITY_DATA_SEARCH_MESSAGE
  }
}

export function searchIsTooGeneral(){
  return {
    type: UPDATE_AVAILABILITY_DATA_SEARCH_MESSAGE,
    isSearchTooGeneral: true
  }
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}
