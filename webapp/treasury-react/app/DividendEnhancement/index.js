import { hot } from "react-hot-loader/root";
import React, { Component } from "react";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";
import Loader from "commons/container/Loader";
import { Layout } from "arc-react-components";

class DividendEnhancement extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            ref={(header) => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          ></arc-header>
          <Layout isColumnType>
              <Layout.Child childId="DivEnhancementIndex1" size={2}>
                <SideBar/>
              </Layout.Child>
              <Layout.Divider childId="DivEnhancementDivider" />
              <Layout.Child childId="DivEnhancementIndex2" size={0.1} />
              <Layout.Child childId="DivEnhancementIndex3" size={12}>
                <Grid/>
              </Layout.Child>
          </Layout>
        </div>
      </React.Fragment>
    );
  }
}

export default hot(DividendEnhancement);
