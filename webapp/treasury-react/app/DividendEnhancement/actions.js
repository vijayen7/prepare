import {
  FETCH_DIVIDEND_ENHANCEMENT_DETAIL_DATA,
  SET_SELECTED_ROW_DATA,
  RESET_SELECTED_ROW_DATA,
  SET_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE,
  DESTROY_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE,
  FETCH_RENEGOTIATION_DATA,
  SHOW_RENEGOTIATION_POP_UP,
  HIDE_RENEGOTIATION_POP_UP,
  SAVE_RENEGOTIATION_DATA,
  RESET_RENEGOTIATION_DATA,
  UPLOAD_NEGOTIATION_DATA,
  RESET_SAVE_RENEGOTIATION_DATA,
  RESET_UPLOAD_DATA
} from "./constants";

import {
  RESIZE_CANVAS
} from "commons/constants";

export function fetchDividendEnhancementDetailData(payload) {
  return {
    type: FETCH_DIVIDEND_ENHANCEMENT_DETAIL_DATA,
    payload
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

export function setSelectedRowData(payload) {
  return {
    type: SET_SELECTED_ROW_DATA,
    payload
  };
}

export function resetSelectedRowData() {
  return {
    type: RESET_SELECTED_ROW_DATA
  };
}

export function setUploadDividendNegotiationToggle(payload) {
   return {
     type: SET_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE,
     payload
   };
 }

export function destroyUploadDividendNegotiationToggle(payload) {
   return {
     type: DESTROY_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE,
     payload
   };
 }

export function getRenegotiationData(payload) {
  return {
    type: FETCH_RENEGOTIATION_DATA,
    payload
  };
}

export function showRenegotiationPopUp(payload) {
  return {
    type: SHOW_RENEGOTIATION_POP_UP,
    payload
  };
}

export function hideRenegotiationPopup(payload) {
   return {
     type: HIDE_RENEGOTIATION_POP_UP,
     payload
   };
 }

 export function saveRenegotiationData(payload) {
    return {
      type: SAVE_RENEGOTIATION_DATA,
      payload
    };
  }

  export function resetSavedNegotiationData() {
    return {
      type: RESET_SAVE_RENEGOTIATION_DATA,
    };
  }
  export function uploadDividendNegotiationData(payload) {
    return {
      type: UPLOAD_NEGOTIATION_DATA,
      payload
    };
  }

  export function resetRenegotiationData() {
     return {
       type: RESET_RENEGOTIATION_DATA,
     };
   }

  export function resetUploadData() {
    return {
      type: RESET_UPLOAD_DATA,
    };
  }
