import { url } from "../api";
import { getSaveSettingsUrl } from "commons/util";

export function gridOptions(view) {
  var options = {
    configureReport: {
      urlCallback: function () {
        return window.location.origin + url;
      },
    },
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [
      {
        columnId: "positionDate",
        sortAsc: true,
      },
    ],
    page: true,
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    saveSettings: {
      applicationId: 3,
      applicationCategory: "DividendNegotiation",
      serviceURL: getSaveSettingsUrl(),
      onBeforeLoadedCallback: function (settingsData) {},
      loadedCallback: function (gridObject) {},
    },
  };
  return options;
}
