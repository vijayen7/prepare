import { linkFormatter, numberFormat } from "commons/grid/formatters";
import { textComparator } from "commons/grid/comparators";
import { convertJavaDate } from "commons/util";

export function dividendEnhancementDetailDataColumns() {
  var columns = [
    {
      id: "positionDate",
      name: "Position Date",
      field: "positionDate",
      toolTip: "Position Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true,
      },
    },
    {
      id: "book",
      name: "Book",
      field: "bookName",
      toolTip: "Book",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80,
    },
    {
      id: "legalEntity",
      name: "Legal Entity",
      field: "legalEntityName",
      toolTip: "Legal Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80,
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100,
    },
    {
      id: "custodianAccountName",
      name: "Custodian Account",
      field: "custodianAccountName",
      toolTip: "Custodian Account",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 150,
    },
    {
      id: "spn",
      name: "SPN",
      field: "spn",
      toolTip: "SPN",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 65,
    },
    {
      id: "pnlSpn",
      name: "PNL SPN",
      field: "pnlSpn",
      toolTip: "PNL SPN",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 65,
    },
    {
      id: "securityDescription",
      name: "Security Name",
      field: "securityDescription",
      toolTip: "Security Name",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 200,
    },
    {
      id: "sedol",
      name: "Sedol",
      field: "sedol",
      toolTip: "sedol",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 60,
    },
    {
      id: "ticker",
      name: "Ticker",
      field: "securityLocalName",
      toolTip: "ticker",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50,
    },
    {
      id: "cusip",
      name: "CUSIP",
      field: "cusip",
      toolTip: "cusip",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 60,
    },
    {
      id: "isin",
      name: "ISIN",
      field: "isin",
      toolTip: "isin",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 70,
    },
    {
      id: "currency",
      name: "Currency",
      field: "currency",
      toolTip: "Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50,
    },
    {
      id: "gboType",
      name: "GBO Type",
      field: "gboType",
      toolTip: "GBO Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50,
    },
    {
      id: "country",
      name: "Country",
      field: "country",
      toolTip: "Country",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50,
    },
    {
      id: "price",
      name: "Price",
      field: "price",
      toolTip: "Price",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "priceUSD",
      name: "Price USD",
      field: "priceUSD",
      toolTip: "Price USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "quantity",
      name: "Quantity",
      field: "quantity",
      toolTip: "Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "marketValue",
      name: "Market Value",
      field: "marketValue",
      toolTip: "Market Value",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "mvUSD",
      name: "Market Value USD",
      field: "mvUSD",
      toolTip: "Market Value USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "divRecordDate",
      name: "Record Date",
      field: "divRecordDate",
      toolTip: "Record Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true,
      },
    },
    {
      id: "dividendPerShare",
      name: "Dividend Per Share",
      field: "dividendPerShare",
      toolTip: "Dividend Per Share",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "dividendPerShareUSD",
      name: "Dividend Per Share USD",
      field: "dividendPerShareUSD",
      toolTip: "Dividend Per Share USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },

    {
      id: "positionType",
      name: "Long/Short",
      field: "positionType",
      toolTip: "Long/Short",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50,
    },
    {
      id: "borrowQuantity",
      name: "Broker Quantity",
      field: "borrowQuantity",
      toolTip: "Broker Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },

    {
      id: "borrowRate",
      name: "SLF Rate",
      field: "borrowRate",
      toolTip: "SLF Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "borrowMv",
      name: "Broker Market Value",
      field: "borrowMv",
      toolTip: "Broker Market Value",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "borrowMvUSD",
      name: "Broker Market Value USD",
      field: "borrowMvUSD",
      toolTip: "Broker Market Value USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "dividendType",
      name: "Corpact Type",
      field: "dividendType",
      toolTip: "Corpact Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50,
    },
    {
      id: "divExDate",
      name: "Ex Date",
      field: "divExDate",
      toolTip: "Ex Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true,
      },
    },
    {
      id: "divPayDate",
      name: "Pay Date",
      field: "divPayDate",
      toolTip: "Pay Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true,
      },
    },
    {
      id: "divAnnDate",
      name: "Announced Date",
      field: "divAnnDate",
      toolTip: "Announced Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true,
      },
    },
    {
      id: "totalDividend",
      name: "Total Dividend",
      field: "totalDividend",
      toolTip: "Total Dividend",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "totalDividendUSD",
      name: "Total Dividend USD",
      field: "totalDividendUSD",
      toolTip: "Total Dividend USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "possibleNegotiation",
      name: "Possible Negotiation",
      field: "possibleNegotiation",
      toolTip: "Possible Negotiation",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "negotiatedPayReceive",
      name: "Negotiated Pay/Receive",
      field: "negotiatedPay",
      toolTip: "Negotiated Pay/Receive",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "negotiatedQty",
      name: "Negotiated Qty",
      field: "negotiatedQty",
      toolTip: "Negotiated Qty",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "differenceValue",
      name: "Difference in Pay/Receive",
      field: "differenceValue",
      toolTip: "Difference in Pay/Receive",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
        {
      id: "expectedPayOrReceive",
      name: "Expected Pay Or Receive",
      field: "expectedPayOrReceive",
      toolTip: "Expected Pay Or Receive",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
        {
      id: "possibleNegotiation",
      name: "Possible Negotiation",
      field: "possibleNegotiation",
      toolTip: "Possible Negotiation",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
        {
      id: "maxImpact",
      name: "Max Impact",
      field: "maxImpact",
      toolTip: "Max Impact",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
        {
      id: "expectedValue",
      name: "Expected Value",
      field: "expectedValue",
      toolTip: "Expected Value",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
        {
      id: "expectedValueUSD",
      name: "Expected Value USD",
      field: "expectedValueUSD",
      toolTip: "Expected Value USD",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 60,
    },
    {
      id: "bundleName",
      name: "Bundle",
      field: "bundleName",
      toolTip: "Bundle",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80,
    },
    {
      id: 'actions',
      name: 'Renegotiation',
      exportToExcel: false,
      sortable: false,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return (
          "  <a title='Renegotiate'><i data-role='renegotiate' class='icon-edit--block' /></a>  "
        );
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true
    },
    {
      id: "comment",
      name: "Comment",
      field: "comment",
      toolTip: "Comment",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80,
    },
  ];

  return columns;
}
