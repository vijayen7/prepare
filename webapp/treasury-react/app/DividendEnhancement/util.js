import React from "react";
import _ from "lodash";

export function getDisabledFilter(label, value) {
  return (
    <div className="form-field--split">
      <div className="padding--right--large">{label}</div>
      <div class="text-align--left">{value}</div>
    </div>
  );
}
