import {
  RESIZE_CANVAS,
} from "commons/constants";
import {
  FETCH_DIVIDEND_ENHANCEMENT_DETAIL_DATA,
  SET_SELECTED_ROW_DATA,
  RESET_SELECTED_ROW_DATA,
  SET_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE,
  DESTROY_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE,
  FETCH_RENEGOTIATION_DATA,
  SHOW_RENEGOTIATION_POP_UP,
  HIDE_RENEGOTIATION_POP_UP,
  SAVE_RENEGOTIATION_DATA,
  RESET_RENEGOTIATION_DATA,
  UPLOAD_NEGOTIATION_DATA,
  RESET_SAVE_RENEGOTIATION_DATA,
  RESET_UPLOAD_DATA
} from "./constants";

import { combineReducers } from "redux";

function marketDataSearchMessageReducer(
  state = DEFAULT_SEARCH_MESSAGE,
  action
) {
  switch (action.type) {
    case UPDATE_MARKET_DATA_SEARCH_MESSAGE:
      return !action.data || action.data.length <= 0 || typeof action.data.message !== "undefined"
        ? NO_DATA_SEARCH_MESSAGE
        : "";
    case RESET_MARKET_DATA_SEARCH_MESSAGE:
      return DEFAULT_SEARCH_MESSAGE;
    default:
      return state;
  }
}

function dividendEnhancementDetailDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_DIVIDEND_ENHANCEMENT_DETAIL_DATA}_SUCCESS`:
      return action.data || [];
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

function setSelectedRowDataReducer(state = {}, action) {
  switch (action.type) {
    case SET_SELECTED_ROW_DATA:
      return action.payload || {};
    case RESET_SELECTED_ROW_DATA:
      return {};
    default:
      return state;
  }
}

function renegotiationDataReducer(state = {}, action) {
  switch (action.type) {
    case `${FETCH_RENEGOTIATION_DATA}_SUCCESS`:
      return action.data || {};
    case RESET_RENEGOTIATION_DATA:
      return {};
  }
  return state;
}

function saveNegotiationDataReducer(state = false, action) {
  switch (action.type) {
    case `${SAVE_RENEGOTIATION_DATA}_SUCCESS`:
      return true;
    case `${SAVE_RENEGOTIATION_DATA}_FAILURE`:
      return false;
    case RESET_SAVE_RENEGOTIATION_DATA:
      return false;
  }
  return state;
}

function uploadNegotiationDataReducer(state = '', action) {
  switch (action.type) {
    case `${UPLOAD_NEGOTIATION_DATA}_SUCCESS`:
      return action.data;
    case `${UPLOAD_NEGOTIATION_DATA}_FAILURE`:
      return 'Upload Failed';
    case RESET_UPLOAD_DATA:
      return '';
 }
  return state;
}

function uploadDividendNegotiationToggleReducer(state = false, action) {
  switch (action.type) {
    case SET_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE:
      return true;
    case DESTROY_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE:
      return false;
    default:
      return state;
  }
}

function toggleRenegotiationPopUpReducer(state = false, action) {
  switch (action.type) {
    case SHOW_RENEGOTIATION_POP_UP:
      return true;
    case HIDE_RENEGOTIATION_POP_UP:
      return false;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  dividendEnhancementDetailData: dividendEnhancementDetailDataReducer,
  resizeCanvas: resizeCanvasReducer,
  selectedRowData : setSelectedRowDataReducer,
  renegotiationData : renegotiationDataReducer,
  isUploadDividendNegotiationClicked : uploadDividendNegotiationToggleReducer,
  isRenegotiationPopUpOpen : toggleRenegotiationPopUpReducer,
  savedNegotiationData : saveNegotiationDataReducer,
  isFileUploaded : uploadNegotiationDataReducer
});

export default rootReducer;
