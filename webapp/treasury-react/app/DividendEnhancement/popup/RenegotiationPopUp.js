import React, { Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Dialog from 'commons/components/Dialog';
import InputFilter from 'commons/components/InputFilter';
import InputNumberFilter from 'commons/components/InputNumberFilter';
import Combobox from 'commons/components/Combobox';
import Message from 'commons/components/Message';
import FilterButton from 'commons/components/FilterButton';
import { getDisabledFilter } from '../util';
import { Card } from 'arc-react-components';
import { convertJavaDate } from "commons/util";
import{
  resetRenegotiationData,
  saveRenegotiationData,
  resetSavedNegotiationData,
} from "../actions";

class RenegotiationpopUp extends Component {
  constructor(props) {
    super(props);
    this.handleClosePopUp = this.handleClosePopUp.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.getMessageComponent = this.getMessageComponent.bind(this);
    this.validateParameters = this.validateParameters.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.state = this.getDefaultState();
  }

  componentDidMount(){
    this.setState({
        quantity : this.props.renegotiationData.quantity,
        originalRate : this.props.renegotiationData.expectedPayOrReceivePercent,
        negotiatedRate : this.props.renegotiationData.negotiatedPayOrReceivePercent
    });

  }

  componentDidUpdate(prevProps) {
    if(_.isEmpty(prevProps.renegotiationData) && !_.isEmpty(this.props.renegotiationData)){
      const positionName = (this.props.renegotiationData.positionTypeId === 18)? { key : 18 , value : "Short" } : {key : 19 , value : "Long"};
      this.setState({
        positionType : positionName,
        quantity : this.props.renegotiationData.quantity,
        originalRate : this.props.renegotiationData.expectedPayOrReceivePercent,
        negotiatedRate : this.props.renegotiationData.negotiatedPayOrReceivePercent
      })
      console.log(this.state);
    }
  }


  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    console.log("New State");
    console.log(newState);
    this.setState(newState);
    this.props.resetSavedNegotiationData();
  }

  getDefaultState() {
    return {
      positionType : null,
      quantity : '',
      originalRate : '',
      negotiatedRate : '',
      comment : '',
      error: {
        negotiatedRate: false,
        comment: false
      }
    };
  }

  getMessageComponent() {
    if(!this.props.savedNegotiationData){
      return <React.Fragment/>
    }
    return <Message messageData="Renegotiation Successfull." success />;
  }

  handleClosePopUp() {
    this.props.hideRenegotiationPopup();
    this.props.resetRenegotiationData();
    this.props.resetSavedNegotiationData();
    this.setState(this.getDefaultState());
  }

  handleSubmit() {
    const {
      positionType,
      quantity,
      originalRate,
      negotiatedRate,
      comment
    } = this.state;

    const parameters = {};


    var payload = {
      "dividendNegotiationData.custodianAccountId" : this.props.selectedRowData.custodianAccountId,
      "dividendNegotiationData.pnlSpn" : this.props.selectedRowData.pnlSpn,
      "dividendNegotiationData.spn" : this.props.selectedRowData.spn,
      "dividendNegotiationData.quantity" : quantity,
      "dividendNegotiationData.divRecordDate" : `${convertJavaDate(this.props.selectedRowData.divRecordDate)}`,
      "dividendNegotiationData.divExDate" : `${convertJavaDate(this.props.selectedRowData.divExDate)}`,
      "dividendNegotiationData.divPayDate" : `${convertJavaDate(this.props.selectedRowData.divPayDate)}`,
      "dividendNegotiationData.positionDate" : `${convertJavaDate(this.props.selectedRowData.positionDate)}`,
      "dividendNegotiationData.expectedPayOrReceive" : originalRate,
      "dividendNegotiationData.negotiatedPay" : negotiatedRate,
      "dividendNegotiationData.positionTypeId" : positionType.key,
      "dividendNegotiationData.comment" : comment,
      "dividendNegotiationData.userId" : `${USER}`,
      ...(!_.isEmpty(this.props.renegotiationData)
          ? ({
              "dividendNegotiationData.negotiationRecordId": this.props.renegotiationData.negotiatedRecordId,
              "dividendNegotiationData.bookId" :  this.props.renegotiationData.bookId,
              "dividendNegotiationData.price" : this.props.renegotiationData.priceLBC,
              "dividendNegotiationData.dividendPerShare" : this.props.renegotiationData.dividendPerShareLBC,
              "dividendNegotiationData.corpActTypeId" : this.props.renegotiationData.corpactTypeId,

            })
          : null),
      };
    if (this.validateParameters()) {
      this.props.saveRenegotiationData(payload);
    }
  }

  validateParameters() {
    let ret = true;
    const error = {
      quantity: false,
      originalRate: false,
      negotiatedRate: false,
      comment: false
    };
    if (this.state.quantity<0) {
      ret = false;
      error.quantity = true;
    }
    if ( this.state.originalRate < 0 || this.state.originalRate > 100) {
      ret = false;
      error.originalRate = true;
    }
    if ( this.state.negotiatedRate<0 || this.state.negotiatedRate > 100) {
      ret = false;
      error.negotiatedRate = true;
    }
    if (this.state.comment.length === 0) {
      ret = false;
      error.comment = true;
    }
    this.setState({ error });
    return ret;
  }

  render() {
    console.log(this.props.renegotiationData);

    return (
      <React.Fragment>
        <Dialog
          title="Renegotiation Dialog"
          isOpen={this.props.isRenegotiationPopUpOpen}
          style={{ width: '550px' }}
          onClose={this.handleClosePopUp}
          footer={
            <React.Fragment>
              <FilterButton onClick={this.handleSubmit} label="Submit" />
            </React.Fragment>
          }
        >
          <div style={{ width: '500px' }}>
            {this.getMessageComponent()}
            <div>
              <Card>
                  {getDisabledFilter('Counter Party:', this.props.selectedRowData.cpeName)}
                  {getDisabledFilter('Legal Entity:', this.props.selectedRowData.legalEntityName)}
                  {getDisabledFilter('Book:', this.props.selectedRowData.bookName)}
                  {getDisabledFilter('Security Name:', this.props.selectedRowData.securityDescription)}
                  {getDisabledFilter('SPN:', this.props.selectedRowData.spn)}
                  {getDisabledFilter('SEDOL:', this.props.selectedRowData.sedol)}
                  {getDisabledFilter('Country:', this.props.selectedRowData.country)}

                  <Combobox
                    data={[
                      { key: 18, value: "Short" },
                      { key: 19, value: "Long" }
                    ]}
                    selectKey = 'positionType'
                    selectedData={this.state.positionType}
                    onSelect={this.onSelect}
                    multiSelect = {false}
                  />

                  <InputNumberFilter
                      data={this.state.quantity}
                      label="Quantity"
                      onSelect={this.onSelect}
                      stateKey="quantity"
                      isError={this.state.error.quantity}
                  />

                  <InputNumberFilter
                      data={this.state.originalRate}
                      label="Original Expected Pay %"
                      onSelect={this.onSelect}
                      stateKey="originalRate"
                      isError={this.state.error.originalRate}
                  />

                  <InputNumberFilter
                      data={this.state.negotiatedRate}
                      label="Negotiated Expected Pay %"
                      onSelect={this.onSelect}
                      stateKey="negotiatedRate"
                      isError={this.state.error.negotiatedRate}
                  />

                  <InputFilter
                      data={this.state.comment}
                      label="Comment"
                      onSelect={this.onSelect}
                      stateKey="comment"
                      isError={this.state.error.comment}
                  />
              </Card>
            </div>
          </div>
        </Dialog>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
      resetRenegotiationData,
      saveRenegotiationData,
      resetSavedNegotiationData,
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    savedNegotiationData: state.dividendEnhancementDetailData.savedNegotiationData,
  };
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(RenegotiationpopUp);

