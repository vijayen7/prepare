import React,{Component} from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Dialog from 'commons/components/Dialog';
import Message from 'commons/components/Message';
import FilterButton from 'commons/components/FilterButton';
import { Card } from 'arc-react-components';
import{
  destroyUploadDividendNegotiationToggle,
  uploadDividendNegotiationData,
  resetUploadData
} from "../actions";

class UploadDividendNegotiationDialog extends Component {

    constructor(props) {
      super(props);
      this.handleClosePopUp = this.handleClosePopUp.bind(this);
      this.onFileChange = this.onFileChange.bind(this);
      this.onFileUpload = this.onFileUpload.bind(this);
      this.state = {
        selectedFile: null
      };
    }

    onFileChange (event){
      this.setState({ selectedFile: event.target.files[0] });
      this.props.resetUploadData();
    }

    onFileUpload() {
      const formData = new FormData();
      formData.append(
        "dividendNegotiationsFile",
        this.state.selectedFile
      );
      formData.append(
        "userName",
        `${USER}`
      );
      console.log(this.state.selectedFile);
      this.props.uploadDividendNegotiationData(formData);

    }

    handleClosePopUp() {
      this.setState({selectedFile: null});
      this.props.resetUploadData();
      this.props.destroyUploadDividendNegotiationToggle();
    }

    getMessageComponent() {
      if(_.isEmpty(this.props.isFileUploaded)){
        return <React.Fragment/>
      }
      if(this.props.isFileUploaded === 'Upload Successfull'){
        return <Message messageData="Upload Successfull." success />;
      }
      return <Message messageData={this.props.isFileUploaded} error />;
    }

    render() {
      return (
        <React.Fragment>
          <Dialog
            title="Negotiation Upload Dialog"
            isOpen={this.props.isOpen}
            style={{ width: '550px' }}
            onClose={this.handleClosePopUp}
          >
            <div style={{ width: '500px' }}>
              {this.getMessageComponent()}
              <div>
                <Card>
                  <div className= "size--content padding--horizontal">
                    <input type="file" onChange={this.onFileChange} />
                  </div>
                    <FilterButton onClick={this.onFileUpload} label="Upload" />
                </Card>
              </div>
            </div>
          </Dialog>
        </React.Fragment>
      )
    }
  }

  function mapDispatchToProps(dispatch){
    return bindActionCreators({
        destroyUploadDividendNegotiationToggle,
        uploadDividendNegotiationData,
        resetUploadData,
      },
      dispatch
    );
  }

  function mapStateToProps(state) {
    return {
      isFileUploaded: state.dividendEnhancementDetailData.isFileUploaded,
    };
  }

  export default connect(
     mapStateToProps,
     mapDispatchToProps
  )(UploadDividendNegotiationDialog);

