import React, { Component } from "react";
import { connect } from "react-redux";
import MultiSelectFilter from "commons/components/MultiSelectFilter";

export default class CorpactTypesFilter extends Component {
  render() {
    return (
      <MultiSelectFilter
        data={[
          { key: 1, value: "Regular Cash Dividend" },
          { key: 2, value: "Other Cash Dividend" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedCorpactTypes"
        label="CorpactTypes"
      />
    );
  }
}
