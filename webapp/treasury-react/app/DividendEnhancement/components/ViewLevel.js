import React, { Component } from "react";
import { connect } from "react-redux";
import SingleSelectFilter from "commons/components/SingleSelectFilter";

export default class ViewLevel extends Component {
  render() {
    return (
      <SingleSelectFilter
        data={[
          { key: 1, value: "Position" },
          { key: 2, value: "Bundle" }
        ]}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedViewLevel"
        label="View"
      />
    );
  }
}
