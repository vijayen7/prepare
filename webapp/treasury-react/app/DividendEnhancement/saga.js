import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  FETCH_DIVIDEND_ENHANCEMENT_DETAIL_DATA,
  SET_SELECTED_ROW_DATA,
  RESET_SELECTED_ROW_DATA,
  SET_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE,
  DESTROY_UPLOAD_DIVIDEND_NEGOTIATION_TOGGLE,
  FETCH_RENEGOTIATION_DATA,
  SHOW_RENEGOTIATION_POP_UP,
  HIDE_RENEGOTIATION_POP_UP,
  SAVE_RENEGOTIATION_DATA,
  RESET_RENEGOTIATION_DATA,
  UPLOAD_NEGOTIATION_DATA
} from "./constants";
import {
  getDividendEnhancementDetailData,
  getDividendNegotiationData,
  fetchSaveNegotiationData,
  fetchUploadNegotiationData
} from "./api";
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
} from "commons/constants";

import { ToastService } from "arc-react-components";

function* fetchDividendEnhancementDetailData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getDividendEnhancementDetailData, action.payload);
    yield put({
      type: `${FETCH_DIVIDEND_ENHANCEMENT_DETAIL_DATA}_SUCCESS`,
      data,
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_DIVIDEND_ENHANCEMENT_DETAIL_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchRenegotiationData(action) {
  try {
    yield put({ type: START_LOADING });
    let data = yield call(getDividendNegotiationData, action.payload);
    yield put({
      type: `${FETCH_RENEGOTIATION_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_RENEGOTIATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveNegotiationData(action) {
  try {
    yield put({ type: START_LOADING });

    yield call(fetchSaveNegotiationData, action.payload);

    yield put({
      type: `${SAVE_RENEGOTIATION_DATA}_SUCCESS`
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_RENEGOTIATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* uploadNegotiationData(action) {
  try {
    yield put({ type: START_LOADING });

    let data = yield call(fetchUploadNegotiationData, action.payload);

    yield put({
      type: `${UPLOAD_NEGOTIATION_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${UPLOAD_NEGOTIATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* dividendEnhancementDetailData() {
  yield [
    takeEvery(
      FETCH_DIVIDEND_ENHANCEMENT_DETAIL_DATA,
      fetchDividendEnhancementDetailData
    ),
  ];
}

export function* renegotiationData() {
  yield [
    takeEvery(
      FETCH_RENEGOTIATION_DATA,
      fetchRenegotiationData
    ),
  ];
}

export function* NegotiationData() {
  yield [
    takeEvery(
      SAVE_RENEGOTIATION_DATA,
      saveNegotiationData
    ),
  ];
}

export function* uploadNegotiationDataFile() {
  yield [
    takeEvery(
      UPLOAD_NEGOTIATION_DATA,
      uploadNegotiationData
    ),
  ];
}
function* dividendEnhancementDetailDataSaga() {
  yield all([
    dividendEnhancementDetailData(),
    renegotiationData(),
    NegotiationData(),
    uploadNegotiationDataFile(),
  ]);
}

export default dividendEnhancementDetailDataSaga;
