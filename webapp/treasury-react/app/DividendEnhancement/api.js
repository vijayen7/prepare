import {
  BASE_URL
} from "commons/constants";
import { convertJavaDate } from "commons/util";
const queryString = require("query-string");
export let url = "";

export function getDividendEnhancementDetailData(payload) {

  const paramString = queryString.stringify(payload);

  url = `${BASE_URL}service/dividendEnhancementDetailService/getDividendEnhancementDetailData?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}


export function getDividendNegotiationData(payload) {

  url = `${BASE_URL}service/renegotiationService/getDivSpikeNegotiation?pnlSpn=${payload.pnlSpn}&custodianAccountId=${payload.custodianAccountId}&bookId=${payload.bookId}&exDate=${convertJavaDate(payload.divExDate)}&corpactTypeId=${payload.corpActTypeId}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function fetchSaveNegotiationData(payload) {

  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/renegotiationService/updateDivSpikeNegotiation?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  })
}

export function fetchUploadNegotiationData(payload) {
  url = `${BASE_URL}service/dividendNegotiationBulkUploadService/uploadDividendNegotiations`;
  return fetch(url, {
    method: "POST",
    credentials: "include",
    body: payload
  }).then(data => data.json());
}
