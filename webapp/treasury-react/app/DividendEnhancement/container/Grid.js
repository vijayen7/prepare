import React, { Component } from "react";
import { dividendEnhancementDetailDataColumns } from "../grid/columnConfig";
import { Layout, Panel } from "arc-react-components";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { gridOptions } from "../grid/gridOptions";
import FilterButton from "commons/components/FilterButton";
import DividendEnhancementDataGrid from "commons/components/GridWithCellClick";
import RenegotiationPopUp from "../popup/RenegotiationPopUp";
import UploadDividendNegotiationDialog from "../popup/UploadDividendNegotiationDialog";
import Message from "commons/components/Message";
import{
  showRenegotiationPopUp,
  setSelectedRowData,
  setUploadDividendNegotiationToggle,
  hideRenegotiationPopup,
  getRenegotiationData
} from "../actions"

class Grid extends Component {
  constructor(props) {
    super(props);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.renderGridData = this.renderGridData.bind(this);
    this.renderButtons = this.renderButtons.bind(this);
    this.renderPopUps = this.renderPopUps.bind(this);
  }

  onCellClickHandler = args => {
    let ele = args.event.target;
    let { role } = ele.dataset;
    if (role === 'renegotiate'){
      this.props.setSelectedRowData(args.item);
      this.props.getRenegotiationData(this.props.selectedRowData);
      this.props.showRenegotiationPopUp();
    }
  }

  renderButtons() {
    let data = this.props.dividendEnhancementDetailData;

    let primary = "button--primary size--content float--right margin--small";

    if (data.length <= 0) {
      primary = "button--success button--medium size--content margin--double";
    }

    let buttons = (
      <React.Fragment>
        <FilterButton onClick={this.props.setUploadDividendNegotiationToggle} className={primary} label={`Upload Dividend Negotiation`} />
      </React.Fragment>
    );

    if (data.length <= 0) {
      return (
        <div
          className="text-align--center"
          style={{ width: "300px", height: "170px", margin: "auto" }}
        >
          {buttons}
        </div>
      );
    }

    return <Layout.Child childId="GridButtons" size={1}>{buttons}</Layout.Child>;
  }

  renderPopUps() {
    return (
      <React.Fragment>
        <UploadDividendNegotiationDialog isOpen = {this.props.isUploadDividendNegotiationClicked}/>
        <RenegotiationPopUp
          isRenegotiationPopUpOpen={this.props.isRenegotiationPopUpOpen}
          hideRenegotiationPopup={this.props.hideRenegotiationPopup}
          selectedRowData = {this.props.selectedRowData}
          renegotiationData={this.props.renegotiationData}
        />
      </React.Fragment>
    );
  }

  renderGridData() {
    let grid = null;

    if ( typeof this.props.dividendEnhancementDetailData.message !== "undefined" ||
         this.props.dividendEnhancementDetailData.length <= 0 ){
      return(
        <React.Fragment />
      );
    }
    grid = (
      <DividendEnhancementDataGrid
        data={this.props.dividendEnhancementDetailData}
        gridId="dividendEnhancementDetailData"
        gridColumns={dividendEnhancementDetailDataColumns()}
        gridOptions={gridOptions()}
        onCellClick={this.onCellClickHandler}
        resizeCanvas={this.props.resizeCanvas}
        fill={true}
      />
    );
    return grid;
  }

  render() {
    if (this.props.dividendEnhancementDetailData.length <= 0) {
      return (
        <React.Fragment>

          <div style={{ width: "450px", margin: "auto" }}>
            <br />
              <Message messageData="Search to load Data..." />
            <br />
          </div>
          {this.renderButtons()}
          {this.renderPopUps()}
        </React.Fragment>
      );
    }

    return (
      <Layout>
        {this.renderButtons()}
        <Layout.Child size={25} childId="DividendEnhancementGrid">
          <Layout>
            <Layout.Child>{this.renderGridData()}</Layout.Child>
          </Layout>
          {this.renderPopUps()}
        </Layout.Child>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    dividendEnhancementDetailData:
      state.dividendEnhancementDetailData.dividendEnhancementDetailData,
    resizeCanvas: state.dividendEnhancementDetailData.resizeCanvas,
    isRenegotiationPopUpOpen: state.dividendEnhancementDetailData.isRenegotiationPopUpOpen,
    renegotiationData : state.dividendEnhancementDetailData.renegotiationData,
    isUploadDividendNegotiationClicked : state.dividendEnhancementDetailData.isUploadDividendNegotiationClicked,
    selectedRowData : state.dividendEnhancementDetailData.selectedRowData,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      showRenegotiationPopUp,
      setSelectedRowData,
      setUploadDividendNegotiationToggle,
      hideRenegotiationPopup,
      getRenegotiationData,
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(Grid);
