import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Card, Layout, DateRangePicker } from "arc-react-components";
import CurrencyFilter from "commons/container/CurrencyFilter";
import Sidebar from "commons/components/Sidebar";
import Label from "commons/components/Label";
import ColumnLayout from "commons/components/ColumnLayout";
import Column from "commons/components/Column";
import CheckboxFilter from "commons/components/CheckboxFilter";
import FilterButton from "commons/components/FilterButton";
import InputNumberFilter from "commons/components/InputNumberFilter";
import CopySearchUrl from "commons/components/CopySearchUrl";
import GenericSecurityFilter from "commons/components/GenericSecurityFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import BookFilter from "commons/container/BookFilter";
import CpeFilter from "commons/container/CpeFilter";
import CorpactTypesFilter from "../components/CorpactTypesFilter";
import ViewLevel from "../components/ViewLevel";

import { fetchDividendEnhancementDetailData, resizeCanvas } from "../actions";
import {
  getPreviousBusinessDay,
  getCommaSeparatedValues,
  getCommaSeparatedListValue,
  isAllKeySelected,
  executeCopySearchUrl,
} from "commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  resizeCanvas() {
    this.props.resizeCanvas();
  }

  getDefaultFilters() {
    return {
      securityFilter: {
        selectedSpns: "",
        selectedSecuritySearchType: { key: "TICKER", value: "TICKER" },
        selectedTextSearchType: { key: "EXACT_MATCH", value: "EXACT_MATCH" },
        securitySearchString: "",
        isAdvancedSearch: false,
      },
      selectedLegalEntities: [],
      selectedCurrencies: [],
      selectedCpes: [],
      selectedStartDate: getPreviousBusinessDay(),
      selectedEndDate: getPreviousBusinessDay(),
      selectedBooks: [],
      includeLong: true,
      includeShort: true,
      toggleSidebar: false,
      totalDividendThreshold: 0.0,
      selectedCorpactTypes: [],
      selectedViewLevel: { key: 1, value: "Position" }
    };
  }

  componentDidMount() {
    this.node.addEventListener("keydown", this.enterFunction, false);
    let copySearchUrl = executeCopySearchUrl();
    if (copySearchUrl.isTrue) {
      this.setState(copySearchUrl.copySearchUrlState, () => this.handleClick());
    }
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  enterFunction = (e) => {
    if (e.keyCode == 13) {
      const node = e.path[0];
      if (node.id !== undefined && node.id === "sidebar") {
        this.handleClick();
        this.node.blur();
      } else this.node.focus();
    }
  };

  applySavedFilters = (selectedFilters) => {
    this.setState(selectedFilters);
  };

  handleClick() {
    var payload = {
      "dividendEnhancementDataFilter.startDate": this.state.selectedStartDate,
      "dividendEnhancementDataFilter.endDate": this.state.selectedEndDate,
      ...(this.state.selectedCpes.length > 0 &&
      !isAllKeySelected(this.state.selectedCpes)
        ? {
            "dividendEnhancementDataFilter.cpeIds": getCommaSeparatedValues(
              this.state.selectedCpes
            ),
          }
        : null),
      ...(this.state.selectedBooks.length > 0 &&
      !isAllKeySelected(this.state.selectedBooks)
        ? {
            "dividendEnhancementDataFilter.bookIds": getCommaSeparatedValues(
              this.state.selectedBooks
            ),
          }
        : null),
      ...(this.state.selectedLegalEntities.length > 0 &&
      !isAllKeySelected(this.state.selectedLegalEntities)
        ? {
            "dividendEnhancementDataFilter.legalEntityIds": getCommaSeparatedValues(
              this.state.selectedLegalEntities
            ),
          }
        : null),
      ...(this.state.securityFilter.isAdvancedSearch &&
      this.state.securityFilter.securitySearchString.trim() !== ""
        ? {
            "dividendEnhancementDataFilter.securityFilter.securitySearchType": this
              .state.securityFilter.selectedSecuritySearchType.value,
            "dividendEnhancementDataFilter.securityFilter.textSearchType": this
              .state.securityFilter.selectedTextSearchType.value,
            "dividendEnhancementDataFilter.securityFilter.searchStrings": getCommaSeparatedListValue(
              this.state.securityFilter.securitySearchString
            ),
          }
        : {
            ...(this.state.securityFilter.selectedSpns &&
            this.state.securityFilter.selectedSpns.trim() !== ""
              ? {
                  "dividendEnhancementDataFilter.pnlSpns": getCommaSeparatedListValue(
                    this.state.securityFilter.selectedSpns
                  ),
                }
              : null),
          }),
      ...(this.state.selectedCurrencies.length > 0
        ? {
            "dividendEnhancementDataFilter.currencyIds": getCommaSeparatedValues(
              this.state.selectedCurrencies
            ),
          }
        : null),
      ...(this.state.selectedCorpactTypes.length > 0
        ? {
            "dividendEnhancementDataFilter.divTypes": getCommaSeparatedValues(
              this.state.selectedCorpactTypes
            ),
          }
        : null),
      "dividendEnhancementDataFilter.includeLong": this.state.includeLong,
      "dividendEnhancementDataFilter.includeShort": this.state.includeShort,
      "dividendEnhancementDataFilter.totalDividendThreshold": this.state
        .totalDividendThreshold,
    "dividendEnhancementDataFilter.viewLevel": getCommaSeparatedValues(
            [this.state.selectedViewLevel]),
    };
    this.props.fetchDividendEnhancementDetailData(payload);
    this.setState({ toggleSidebar: !this.state.toggleSidebar });
  }

  onDateChange = (date) => {
    this.setState({ selectedStartDate: date[0], selectedEndDate: date[1] });
  };

  render() {
    return (
      <React.Fragment>
        <div
          id="sidebar"
          ref={(node) => (this.node = node)}
          tabindex="0"
          style={{ height: "100%" }}
        >
          <Sidebar
            collapsible={true}
            size="200px"
            resizeCanvas={this.resizeCanvas}
          >
           <Card>
            <ViewLevel
                onSelect={this.onSelect}
                selectedData={this.state.selectedViewLevel}
              />
                 </Card>
            <Card>
              <DateRangePicker
                placeholder="Select Date"
                value={[
                  this.state.selectedStartDate,
                  this.state.selectedEndDate,
                ]}
                onChange={this.onDateChange}
              />
            </Card>
            <Card>
              <GenericSecurityFilter
                onSelect={this.onSelect}
                selectedData={this.state.securityFilter}
                stateKey="securityFilter"
              />
            </Card>
            <Card>
              <LegalEntityFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedLegalEntities}
              />
              <CpeFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCpes}
              />
              <BookFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedBooks}
              />
              <CurrencyFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCurrencies}
              />
            </Card>
            <Card>
              <Label label="Position Type" />
              <Column>
                <CheckboxFilter
                  defaultChecked={this.state.includeLong}
                  onSelect={this.onSelect}
                  stateKey="includeLong"
                  label="Long"
                  style="left"
                />
                <CheckboxFilter
                  defaultChecked={this.state.includeShort}
                  onSelect={this.onSelect}
                  stateKey="includeShort"
                  label="Short"
                  style="left"
                />
              </Column>
            </Card>
            <Card>
              <CorpactTypesFilter
                onSelect={this.onSelect}
                selectedData={this.state.selectedCorpactTypes}
              />
            </Card>
            <Card>
              <InputNumberFilter
                onSelect={this.onSelect}
                data={this.state.totalDividendThreshold}
                label="Total Dividend Threshold"
                stateKey="totalDividendThreshold"
              />
            </Card>
            <ColumnLayout>
              <FilterButton
                onClick={this.handleClick}
                reset={false}
                label="Search"
              />
              <FilterButton
                reset={true}
                onClick={this.handleReset}
                label="Reset"
              />
              <CopySearchUrl copySearchParams={this.state} />
            </ColumnLayout>
          </Sidebar>
        </div>
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchDividendEnhancementDetailData,
      resizeCanvas,
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SideBar);
