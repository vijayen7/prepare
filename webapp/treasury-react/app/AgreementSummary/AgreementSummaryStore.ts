import { action, observable, computed } from 'mobx';
import { SearchFilter, SearchStatus, FilterLoadStatus } from './models';
import { getDateFilterData } from '../commons/filters/api';
import { getPreviousBusinessDay, getGlobalFilterFromUrl } from '../commons/util';
import {
  getValueOrDefault,
  applyQuickFilter,
  convertListToString,
  populateSearchFilterFromUrl,
  flattenDictToList,
  showToastService,
  firstLetterCap,
  convertToDisplayColumns,
  convertToReportManagerColumns
} from './utils';
import {
  INITIAL_QF_COUNTER,
  TOAST_TYPE,
  MARGIN_TYPES,
  DATA_FIELD_NAMES,
  RECON_DATA_FIELD_NAMES,
  INITIAL_SEARCH_STATUS,
  INITIAL_FILTER_LOAD_STATUS,
  CURRENCY_SPN_MAP,
  INITIAL_FILTERS,
  MESSAGES,
  CLASSES,
  CLEAR,
  NOT_STARTED,
  REPORTING_CURRENCY,
} from './constants';
import { fetchAgreementSummaryData, getAgreementSummaryCreateReportUrl, isRedirectToOldPdrScreen, updateSavedSetting } from './api';
import MapData from '../commons/models/MapData';
import { DEFAULT_COLUMNS } from './containers/Grid/constants';
import { openReportManager } from '../commons/util'

const copy = require('clipboard-copy');

export class AgreementSummaryStore {
  @observable searchStatus: SearchStatus = INITIAL_SEARCH_STATUS;
  @observable filterLoadStatus: FilterLoadStatus = INITIAL_FILTER_LOAD_STATUS;
  initialSearchFilter: SearchFilter = {
    ...INITIAL_FILTERS,
    selectedDate: getPreviousBusinessDay()
  };
  @observable searchFilter: SearchFilter = this.initialSearchFilter;
  @observable lastSearchFilter: SearchFilter = this.initialSearchFilter;
  @observable quickFilterCounter: any = INITIAL_QF_COUNTER;
  @observable selectedQuickFilter: Array<any> = [];
  @observable agreementData: Array<any> = [];
  @observable agreementDataToShow: Array<any> = [];

  @observable searchedDate: string = this.searchFilter.selectedDate;
  @observable toggleSidebar: boolean = false;
  @observable resizeCanvasFlag: boolean = false;
  @observable clickedRowId: number | string | undefined = undefined;
  @observable selectedRowIds: any = [];
  @observable displayColumns: string[] = DEFAULT_COLUMNS;
  @observable reportManagerColumns: string = "";
  defaultDate: string = this.searchFilter.selectedDate;

  @observable globalFiltersApplied: boolean = false;
  @observable redirectToOldPdrScreen: boolean = false;
  reportManagerSaveSettingFormat = ''


  @computed
  get enableMultiPublish() {
    // checks if all Selected Agreements are Eligible To Be Quick Published
    const applicableTreasuryAgreements = this.selectedRowIds
      .map((element) => this.keyToTrsyAgreementDataMap[element])
      .filter((e) => e?.parentMarginCallTypeEdSummaryDataList);

    if (applicableTreasuryAgreements.length === 0) return false;

    return !applicableTreasuryAgreements.find(
      (element) => element.workflow || element.overallRagStatusDetail?.ragCategoryCode !== CLEAR
    );
  }

  @observable showBottomPanel: boolean = false;

  @observable legalEntityDataList: MapData[] = [];
  @observable legalEntityFamilyDataList: MapData[] = [];

  keyToTrsyAgreementDataMap = {};

  @computed
  get selectedAgreementData() {
    return this.clickedRowId !== undefined ? this.keyToTrsyAgreementDataMap[this.clickedRowId] : undefined;
  }

  @action.bound
  setDisplayColumns(displayColumns: string[]) {
    this.displayColumns = displayColumns;
    this.setReportManagerColumns(convertToReportManagerColumns(displayColumns))
  }

  @action.bound
  setDisplayColumnsFromReportManagerColumns(reportManagerColsFromSetting: any, saveSettingMetaData: any) {

    if (Array.isArray(reportManagerColsFromSetting)) {
      this.setDisplayColumns(reportManagerColsFromSetting)
      this.createAndUpdateNewSavedSettingFormat(reportManagerColsFromSetting, saveSettingMetaData)
    } else {
      let reportManagerCols = JSON.parse(reportManagerColsFromSetting)
      this.setDisplayColumns(convertToDisplayColumns(reportManagerCols))
    }

    if (saveSettingMetaData?.settingName != null && saveSettingMetaData?.settingId != null) {
      this.reportManagerSaveSettingFormat = '{"applicationCategory":"AgreementSummaryReactUI","applicationId":3,"settingName":"' + saveSettingMetaData.settingName + '","settingId":' + saveSettingMetaData.settingId + '}'
    } else {
      this.reportManagerSaveSettingFormat = ''
    }
  }

  createAndUpdateNewSavedSettingFormat(displayColsFormat: string[], settingMetaData: any) {
    let settingData: string = convertToReportManagerColumns(displayColsFormat)
    settingMetaData['settingData'] = JSON.stringify(settingData)
    updateSavedSetting(settingMetaData)
  }

  @action.bound
  setReportManagerColumns(colsString: string) {
    this.reportManagerColumns = colsString
  }






  resetLEFilters = () => {
    this.searchFilter.selectedLegalEntities = [];
    this.searchFilter.selectedLegalEntityFamilies = [];
  }

  setDefaultDate = async () => {

    this.filterLoadStatus.inProgress = true;
    let validDate = new Date();
    try {
      let res = await getDateFilterData();

      if (res && res.tMinusOneFilterDate) {
        validDate = new Date(res.tMinusOneFilterDate);
        this.defaultDate = validDate.toISOString().substring(0, 10);
        this.searchFilter.selectedDate = this.defaultDate;
      }
      this.filterLoadStatus.inProgress = false;
    } catch (e) {
      this.filterLoadStatus = {
        inProgress: false,
        error: true
      };
    }
  };

  @action.bound
  setExpandedGlobalSelections(userSelections: any, expandedGlobalSelection: any) {
    if (userSelections?.legalEntityFamilies?.length || userSelections?.strategies?.length) {
      this.resetLEFilters();
    }
    this.globalFiltersApplied = (userSelections?.legalEntityFamilies?.length || userSelections?.strategies?.length);
    var legalEntityDataList = expandedGlobalSelection['legalEntities'];
    var legalEntityFamilyDataList = expandedGlobalSelection['legalEntityFamilies'];

    var legalEntityDataMap: MapData[] = [];
    var legalEntityFamilyDataMap: MapData[] = [];

    for (var iter = 0; iter < legalEntityDataList.length; iter++) {
      var legalEntityData = legalEntityDataList[iter];
      var legalEntityId = parseInt(legalEntityData['id']);
      var legalEntityName = legalEntityData['name'].concat('[', legalEntityId, ']');
      legalEntityDataMap.push({ key: legalEntityId, value: legalEntityName });
    }
    for (var iter = 0; iter < legalEntityFamilyDataList.length; iter++) {
      var legalEntityFamilyData = legalEntityFamilyDataList[iter];
      var legalEntityFamilyId = legalEntityFamilyData['id'];
      var legalEntityFamilyName = legalEntityFamilyData['name'].concat(
        '[',
        legalEntityFamilyId,
        ']'
      );
      legalEntityFamilyDataMap.push({ key: legalEntityFamilyId, value: legalEntityFamilyName });
    }

    this.legalEntityDataList = legalEntityDataMap;
    this.legalEntityFamilyDataList = legalEntityFamilyDataMap;
  }

  getRecValue(dictionary, ...arg) {
    if (dictionary == null) return undefined;
    if (arg.length == 0) return dictionary;
    return this.getRecValue(dictionary[arg[0]], ...arg.slice(1));
  }

  @action
  getTreeData = (data) => {
    this.quickFilterCounter = INITIAL_QF_COUNTER;
    let treeData: any = [];
    if (data.length !== 0) {
      let counter = 0;

      data.forEach((trsyEDData) => {
        let treasuryAgreementDetail = trsyEDData.applicableEdSummaryDataKey.agreementDetail;
        trsyEDData.treasuryAgreementDetail = treasuryAgreementDetail;
        let applicableMarginCallType = trsyEDData.applicableEdSummaryDataKey.marginCallType.abbrev;

        let parentAllData = {};
        let parentAllMarginCallType = new Set();
        let parentAllAcountType = new Set();
        trsyEDData.parentMarginCallTypeEdSummaryDataList.forEach((parentData) => {
          parentAllData[parentData.edSummaryDataKey.marginCallType.abbrev] = parentData;
          parentAllMarginCallType.add(parentData.edSummaryDataKey.marginCallType.abbrev);
        });

        let childrenData: any = [];
        trsyEDData.actionableMarginCallTypeEdSummaryDataList.forEach((childData) => {
          childData.treasuryAgreementDetail = treasuryAgreementDetail;
          childData.reportingCurrency = trsyEDData?.reportingCurrency;
          childData.workflow = trsyEDData.workflow;
          let childAgreementDetail = childData?.edSummaryDataKey?.agreementDetail;
          let flatChildData = {
            id: counter++,
            date: this.searchFilter.selectedDate,
            currency: this.searchFilter.selectedCurrency.value,
            agreementId: childAgreementDetail?.agreementId,
            workflowStatus: trsyEDData?.workflow?.workflowStatus ? trsyEDData?.workflow?.workflowStatus?.name : NOT_STARTED,
            ragCategoryCode: childData?.curDayEdSummaryData?.ragStatusDetail?.ragCategoryCode,
            ragStatusCode: childData?.curDayEdSummaryData?.ragStatusDetail?.displayText,
            legalEntityId: childAgreementDetail?.legalEntityId,
            legalEntity: childAgreementDetail?.legalEntityName,
            exposureCounterpartyId: childAgreementDetail?.cpeId,
            exposureCounterparty: childAgreementDetail?.cpeName,
            agreementTypeId: childAgreementDetail?.agreementTypeId,
            agreementType: childAgreementDetail?.agreementTypeName,
            reportingCurrency: trsyEDData?.reportingCurrency?.isoCode,
            marginCallType: childData?.edSummaryDataKey?.marginCallType?.abbrev,
            callType: childData?.curDayEdSummaryData?.accountType,
            isSimmApplicable: childData?.edSummaryDataKey?.isSimmApplicable,
            physicalCollateralRagStatus: childData?.curDayEdSummaryData?.physicalCollateralRagStatus[1],
          };
          flatChildData["securitiesCollateralMargin"] = this.getRecValue(childData,
            'curDayEdSummaryData',
            'internalEdSummaryDataDetail',
            "physicalCollateralMargin");
          flatChildData["actualSecuritiesCollateralMargin"] = this.getRecValue(childData,
            'curDayEdSummaryData',
            'internalEdSummaryDataDetail',
            "actualPhysicalCollateralMargin");
          flatChildData["roundedBrokerExcessDeficit"] = this.getRecValue(childData,
            'curDayEdSummaryData',
            "roundedBrokerExcessDeficit");
          for (let dataType of DATA_FIELD_NAMES) {
            flatChildData[dataType] = this.getRecValue(
              childData,
              'curDayEdSummaryData',
              'adjustedEdSummaryDataDetail',
              dataType
            );
          }

          for (let dataType of DATA_FIELD_NAMES) {
            flatChildData['prevDay' + firstLetterCap(dataType)] = this.getRecValue(
              childData,
              'prevDayEdSummaryData',
              'adjustedEdSummaryDataDetail',
              dataType
            );
          }

          for (let dataType of DATA_FIELD_NAMES) {
            flatChildData['broker' + firstLetterCap(dataType)] = this.getRecValue(
              childData,
              'curDayEdSummaryData',
              'externalEdSummaryDataDetail',
              dataType
            );
          }

          for (let dataType of RECON_DATA_FIELD_NAMES) {
            flatChildData['recon' + firstLetterCap(dataType)] = this.getRecValue(
              childData,
              'curDayEdSummaryData',
              'reconEdSummaryDataDetail',
              dataType
            );
          }

          for (let dataType of DATA_FIELD_NAMES) {
            flatChildData[dataType + 'Diff'] =
              flatChildData[dataType] - flatChildData['broker' + firstLetterCap(dataType)];
          }

          if (flatChildData.isSimmApplicable) {
            if (flatChildData['applicableExcessDeficit'])
              flatChildData['excessDeficit'] = flatChildData['applicableExcessDeficit'];
            flatChildData['excessDeficitDiff'] =
              flatChildData['excessDeficit'] - flatChildData['brokerApplicableExcessDeficit'];
          }

          flatChildData['actualSecuritiesMovement'] =
            childData?.curDayEdSummaryData?.actualSecuritiesMovement;
          flatChildData['expectedCashMovement'] =
            childData?.curDayEdSummaryData?.expectedCashMovement;
          flatChildData['disputeAmount'] = childData?.curDayEdSummaryData?.disputeAmount;

          this.keyToTrsyAgreementDataMap[flatChildData.id] = childData;
          childrenData.push(flatChildData);
          parentAllAcountType.add(childData.curDayEdSummaryData.accountType);
        });

        let flatParentData = {
          id: counter++,
          date: this.searchFilter.selectedDate,
          currency: this.searchFilter.selectedCurrency.value,
          agreementId: treasuryAgreementDetail.agreementId,
          workflowStatus: trsyEDData?.workflow?.workflowStatus ? trsyEDData?.workflow?.workflowStatus?.name : NOT_STARTED,
          ragCategoryCode: trsyEDData?.overallRagStatusDetail?.ragCategoryCode,
          ragStatusCode: trsyEDData?.overallRagStatusDetail?.displayText,
          legalEntityId: treasuryAgreementDetail?.legalEntityId,
          legalEntity: treasuryAgreementDetail?.legalEntityName,
          exposureCounterpartyId: treasuryAgreementDetail?.cpeId,
          exposureCounterparty: treasuryAgreementDetail?.cpeName,
          agreementTypeId: treasuryAgreementDetail?.agreementTypeId,
          agreementType: treasuryAgreementDetail?.agreementTypeName,
          reportingCurrency: trsyEDData?.reportingCurrency?.isoCode,
          marginCallType: [...parentAllMarginCallType].join(','),
          callType: [...parentAllAcountType].join(','),
          isSimmApplicable: trsyEDData?.applicableEdSummaryDataKey?.isSimmApplicable,
          wireRequest: trsyEDData?.wireRequestNumbers?.toString(),
          bookingReference: trsyEDData?.bookingReferenceIds?.toString(),
          bundleTransferBookingReference: trsyEDData?.bundleTransferBookingReferenceIds?.toString(),
          children: childrenData,
          physicalCollateralRagStatus: trsyEDData?.overallPhyscialCollateralRagStatus[1]
        };
        this.keyToTrsyAgreementDataMap[flatParentData.id] = trsyEDData;

        flatParentData["securitiesCollateralMargin"] = this.getRecValue(parentAllData, applicableMarginCallType,
          'curDayEdSummaryData',
          'internalEdSummaryDataDetail', 'physicalCollateralMargin')

        flatParentData["actualSecuritiesCollateralMargin"] = this.getRecValue(parentAllData, applicableMarginCallType,
          'curDayEdSummaryData',
          'internalEdSummaryDataDetail', 'actualPhysicalCollateralMargin')

        flatParentData["roundedBrokerExcessDeficit"] = this.getRecValue(parentAllData, applicableMarginCallType,
          'curDayEdSummaryData', "roundedBrokerExcessDeficit");

        for (let dataType of DATA_FIELD_NAMES) {
          flatParentData[dataType] = this.getRecValue(
            parentAllData,
            applicableMarginCallType,
            'curDayEdSummaryData',
            'adjustedEdSummaryDataDetail',
            dataType
          );
        }
        for (let marginType in MARGIN_TYPES) {
          let prefix = MARGIN_TYPES[marginType];
          for (let dataType of DATA_FIELD_NAMES) {
            flatParentData[prefix + firstLetterCap(dataType)] = this.getRecValue(
              parentAllData,
              marginType,
              'curDayEdSummaryData',
              'adjustedEdSummaryDataDetail',
              dataType
            );
          }
        }
        for (let dataType of DATA_FIELD_NAMES) {
          flatParentData['prevDay' + firstLetterCap(dataType)] = this.getRecValue(
            parentAllData,
            applicableMarginCallType,
            'prevDayEdSummaryData',
            'adjustedEdSummaryDataDetail',
            dataType
          );
        }
        for (let dataType of DATA_FIELD_NAMES) {
          flatParentData['dayOnDay' + firstLetterCap(dataType) + 'Diff'] =
            flatParentData[dataType] - flatParentData['prevDay' + firstLetterCap(dataType)];
        }
        for (let dataType of DATA_FIELD_NAMES) {
          flatParentData['broker' + firstLetterCap(dataType)] = this.getRecValue(
            parentAllData,
            applicableMarginCallType,
            'curDayEdSummaryData',
            'externalEdSummaryDataDetail',
            dataType
          );
        }

        for (let dataType of RECON_DATA_FIELD_NAMES) {
          flatParentData['recon' + firstLetterCap(dataType)] = this.getRecValue(
            parentAllData,
            applicableMarginCallType,
            'curDayEdSummaryData',
            'reconEdSummaryDataDetail',
            dataType
          );
        }

        flatParentData['reconSegRequirement'] = this.getRecValue(
          parentAllData,
          'SEG',
          'curDayEdSummaryData',
          'reconEdSummaryDataDetail',
          'requirement'
        );

        for (let marginType in MARGIN_TYPES) {
          let prefix = MARGIN_TYPES[marginType];
          for (let dataType of DATA_FIELD_NAMES) {
            flatParentData[
              'broker' + firstLetterCap(prefix) + firstLetterCap(dataType)
            ] = this.getRecValue(
              parentAllData,
              marginType,
              'curDayEdSummaryData',
              'externalEdSummaryDataDetail',
              dataType
            );
          }
        }
        for (let dataType of DATA_FIELD_NAMES) {
          flatParentData[dataType + 'Diff'] =
            flatParentData[dataType] - flatParentData['broker' + firstLetterCap(dataType)];
        }
        for (let marginType in MARGIN_TYPES) {
          let prefix = MARGIN_TYPES[marginType];
          for (let dataType of DATA_FIELD_NAMES) {
            flatParentData[prefix + firstLetterCap(dataType) + 'Diff'] =
              flatParentData[prefix + firstLetterCap(dataType)] -
              flatParentData['broker' + firstLetterCap(prefix) + firstLetterCap(dataType)];
          }
        }

        if (flatParentData.isSimmApplicable) {
          if (flatParentData['applicableExcessDeficit'])
            flatParentData['excessDeficit'] = flatParentData['applicableExcessDeficit'];
          flatParentData['excessDeficitDiff'] =
            flatParentData['excessDeficit'] - flatParentData['brokerApplicableExcessDeficit'];
          if (flatParentData['houseApplicableExcessDeficit'])
            flatParentData['houseExcessDeficit'] = flatParentData['houseApplicableExcessDeficit'];
          if (flatParentData['regApplicableExcessDeficit'])
            flatParentData['regExcessDeficit'] = flatParentData['regApplicableExcessDeficit'];
          if (flatParentData['segApplicableExcessDeficit'])
            flatParentData['segExcessDeficit'] = flatParentData['segApplicableExcessDeficit'];
        }

        flatParentData['actualSecuritiesMovement'] = this.getRecValue(
          parentAllData,
          applicableMarginCallType,
          'curDayEdSummaryData',
          'actualSecuritiesMovement'
        );
        flatParentData['segActualSecuritiesMovement'] = this.getRecValue(
          parentAllData,
          'SEG',
          'curDayEdSummaryData',
          'actualSecuritiesMovement'
        );
        flatParentData['expectedCashMovement'] = this.getRecValue(
          parentAllData,
          applicableMarginCallType,
          'curDayEdSummaryData',
          'expectedCashMovement'
        );
        flatParentData['segExpectedCashMovement'] = this.getRecValue(
          parentAllData,
          'SEG',
          'curDayEdSummaryData',
          'expectedCashMovement'
        );
        flatParentData['disputeAmount'] = this.getRecValue(
          parentAllData,
          applicableMarginCallType,
          'curDayEdSummaryData',
          'disputeAmount'
        );
        flatParentData['segDisputeAmount'] = this.getRecValue(
          parentAllData,
          'SEG',
          'curDayEdSummaryData',
          'disputeAmount'
        );

        this.quickFilterCounter[flatParentData['workflowStatus']] =
          getValueOrDefault(this.quickFilterCounter[flatParentData['workflowStatus']], 0) + 1;
        this.quickFilterCounter[flatParentData['ragCategoryCode']] =
          getValueOrDefault(this.quickFilterCounter[flatParentData['ragCategoryCode']], 0) + 1;

        treeData.push(flatParentData);
      });
    }
    return treeData;
  };

  @action
  resetAgreementSumamryGridData = () => {
    this.agreementData = [];
    this.agreementDataToShow = [];
    this.clickedRowId = undefined;
    this.showBottomPanel = false;
    this.selectedRowIds = [];
  };

  @action
  handleSearch = async () => {
    if (this.searchFilter.selectedCurrency == null) {
      this.searchFilter.selectedCurrency = REPORTING_CURRENCY;
    }
    this.lastSearchFilter = this.searchFilter;
    this.setToggleSidebar(true);
    this.resetAgreementSumamryGridData();
    await this.getAgreementSummaryData();
    this.selectedQuickFilter = [];
    this.handleApplyQuickFilter();
  };

  @action
  setQuickFilters = (quickFilters) => {
    let filtersApplied: any = []
    quickFilters.forEach(element => {
      filtersApplied.push(element);
    });
    this.selectedQuickFilter = filtersApplied;
  }

  @action
  handleApplyQuickFilter = () => {
    this.agreementDataToShow = applyQuickFilter(this.agreementData, this.selectedQuickFilter);
  };

  getDefaultInputLegalEntities = () => {
    return this.globalFiltersApplied ? flattenDictToList(this.legalEntityDataList) : []
  }

  getDefaultInputLegalEntityFamilies = () => {
    return this.globalFiltersApplied ? flattenDictToList(this.legalEntityFamilyDataList) : []
  }

  getPayload = () => {
    let input = {
      date: this.searchFilter.selectedDate,
      legalEntityFamilyIds: flattenDictToList(this.searchFilter.selectedLegalEntityFamilies),
      legalEntityIds: flattenDictToList(this.searchFilter.selectedLegalEntities),
      cpeIds: flattenDictToList(this.searchFilter.selectedCpes),
      agreementTypeIds: flattenDictToList(this.searchFilter.selectedAgreementTypes),
      currencySpn: CURRENCY_SPN_MAP[this.searchFilter.selectedCurrency.value]
    };
    return input;
  }

  //Honoring the global filters for all-all search
  getAgreementSummaryPayload = () => {
    let input = this.getPayload();
    input.legalEntityIds = (input.legalEntityIds.length || input.legalEntityFamilyIds.length) ? input.legalEntityIds
      : this.getDefaultInputLegalEntities();
    return input;
  }

  //Considering LE_Families for Report Manager URL since using LEs would exceed the char limit easily in the request URL and RM supports only get calls.
  getReportManagerPayload = () => {
    let input = this.getPayload();
    input.legalEntityFamilyIds = (input.legalEntityIds.length || input.legalEntityFamilyIds.length) ? input.legalEntityFamilyIds
      : this.getDefaultInputLegalEntityFamilies();
    return input;
  }

  @action
  getAgreementSummaryData = async () => {
    this.searchStatus = INITIAL_SEARCH_STATUS;
    this.searchStatus.inProgress = true;

    try {
      let input = this.getAgreementSummaryPayload();
      this.searchedDate = input.date;
      if (this.searchFilter.workflowStatus.length > 0)
        input['workflowStatusList'] = this.searchFilter.workflowStatus;
      if (this.searchFilter.exceptionStatus.length > 0)
        input['exceptionStatusList'] = this.searchFilter.exceptionStatus;

      input['@CLASS'] = CLASSES.TREASURY_AGREEMENT_EXCESS_DEFICIT_SUMMARY_FILTER;

      const agreementSummaryData = await fetchAgreementSummaryData(input);
      this.agreementData = this.getTreeData(agreementSummaryData);
      this.redirectToOldPdrScreen = await isRedirectToOldPdrScreen();

      if (agreementSummaryData.length == 0) {
        this.searchStatus = {
          inProgress: false,
          error: false,
          message: MESSAGES.NO_DATA_FOUND_MESSAGE,
        };
      } else {
        this.searchStatus.inProgress = false;
      }
    } catch (e) {
      this.searchStatus = {
        inProgress: false,
        error: true,
        message: MESSAGES.ERROR_MESSAGE,
      };
    }
  };

  @action
  createReport = () => {
    try {
      let input = this.getReportManagerPayload();
      this.searchedDate = input.date;
      if (this.searchFilter.workflowStatus.length > 0)
        input['workflowStatusList'] = this.searchFilter.workflowStatus;
      if (this.searchFilter.exceptionStatus.length > 0)
        input['exceptionStatusList'] = this.searchFilter.exceptionStatus;

      input['@CLASS'] = CLASSES.TREASURY_AGREEMENT_EXCESS_DEFICIT_SUMMARY_FILTER;

      let url = getAgreementSummaryCreateReportUrl(input);

      if (url.length >= 2000) {
        showToastService(MESSAGES.REPORT_MANAGER_URL_LIMIT_CROSS, TOAST_TYPE.INFO, 1000);
        input.legalEntityFamilyIds = []
        input.legalEntityIds = []
        url = getAgreementSummaryCreateReportUrl(input)
      }

      if (this.reportManagerSaveSettingFormat != '') {
        openReportManager(url, this.reportManagerSaveSettingFormat)
      } else {
        openReportManager(url, null)
      }


    } catch (e) {
      showToastService(MESSAGES.CREATE_REPORT_FAILURE, TOAST_TYPE.CRITICAL, 1000);
    };
  };

  @action
  handleCopySearchURL = () => {
    let url = `${window.location.origin}${window.location.pathname}?`
    url += `dateString=${this.searchFilter.selectedDate}`
    url += `&legalEntityFamilyIds=${convertListToString(this.searchFilter.selectedLegalEntityFamilies)}`
    url += `&legalEntityIds=${convertListToString(this.searchFilter.selectedLegalEntities)}`
    url += `&cpeIds=${convertListToString(this.searchFilter.selectedCpes)}`
    url += `&agreementTypeIds=${convertListToString(this.searchFilter.selectedAgreementTypes)}`
    url += `&currencyId=${convertListToString([this.searchFilter.selectedCurrency])}`
    url += `&workflowStatus=${convertListToString(this.searchFilter.workflowStatus, true)}`
    url += `&exceptionStatus=${convertListToString(this.searchFilter.exceptionStatus, true)}`;

    const gfFromUrl = getGlobalFilterFromUrl(window.location.search)
    if (gfFromUrl["globalFiltersFromUrl"] != null) {
      url += `&gf=${encodeURIComponent(gfFromUrl["globalFiltersFromUrl"])}`
    }
    if (gfFromUrl["globalFilterInactiveFromUrl"] != null) {
      url += `&gf-inactive=${encodeURIComponent(gfFromUrl["globalFilterInactiveFromUrl"])}`
    }

    copy(url);
    showToastService(MESSAGES.COPY_SEARCH_URL_SUCCESS, TOAST_TYPE.INFO, 1000);
  };

  @action.bound
  setFiltersFromUrl = (urlString: string) => {
    this.searchFilter = populateSearchFilterFromUrl(urlString);
  };

  @action.bound
  onSelect = (params) => {
    const { key, value } = params;
    const selectedFilters = { ...this.searchFilter };
    selectedFilters[key] = value;
    this.setFilters(selectedFilters);
    this.resizeCanvas();
  };

  @action.bound
  onChecked = ([checkboxType, value]) => {
    if (checkboxType == 'Workflow Status') {
      if (this.searchFilter.workflowStatus.includes(value)) {
        this.searchFilter.workflowStatus = this.searchFilter.workflowStatus.filter(
          (x) => x != value
        );
      } else {
        this.searchFilter.workflowStatus.push(value);
      }
    } else if (checkboxType == 'Exception Status') {
      if (this.searchFilter.exceptionStatus.includes(value)) {
        this.searchFilter.exceptionStatus = this.searchFilter.exceptionStatus.filter(
          (x) => x != value
        );
      } else {
        this.searchFilter.exceptionStatus.push(value);
      }
    }
  };

  @action.bound
  handleReset = () => {
    this.resetFilters();
    this.resizeCanvas();
  };

  @action.bound
  applySavedFilters = (savedFilters: SearchFilter) => {
    savedFilters.selectedDate = this.searchFilter.selectedDate;
    this.setFilters(savedFilters);
  };

  @action.bound
  setToggleSidebar = (value: boolean) => {
    this.toggleSidebar = value;
  };

  @action
  setFilters = (newFilter: SearchFilter) => {
    this.searchFilter = newFilter;
  };

  @action
  resetFilters = () => {
    this.searchFilter = {
      ...INITIAL_FILTERS,
      selectedDate: this.defaultDate
    };
    this.searchStatus = INITIAL_SEARCH_STATUS;
  };

  @action.bound
  resizeCanvas = () => {
    setTimeout(() => {
      this.resizeCanvasFlag = !this.resizeCanvasFlag;
    }, 0);
  };

  @action.bound
  onRowClick = (rowId) => {
    // When a row is clicked twice, grid assumes that no row is selected, and sends rowId as null
    this.setShowBottomPanel(rowId != null && this.clickedRowId !== rowId);
    this.clickedRowId = rowId;
  };

  @action
  setSelectedRowIds = (selectedRowIds) => {
    this.selectedRowIds = selectedRowIds;
  };

  @action
  setShowBottomPanel = (value) => {
    this.showBottomPanel = value;
  };

  @action
  setIsLoading = (isLoading: boolean) => {
    this.searchStatus.inProgress = isLoading;
  };
}
