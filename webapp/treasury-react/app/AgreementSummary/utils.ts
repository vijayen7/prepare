import { ToastService } from 'arc-react-components';
import { BASE_URL } from '../commons/constants';
import { ExcessDeficitData } from '../Lcm/AgreementWorkflow/models';
import { showReloadAgreementSummaryConfirmationDialog } from './components/dialog';
import { TOAST_TYPE, INITIAL_FILTERS, MESSAGES, gridColumnsToReportManagerColumnsMap} from './constants';
import {
  getAgreementDetail,
  getApplicableEdSummaryData,
  getInternalVsExternalData,
  getSegEdSummaryData,
} from './containers/BottomPanel/utils/agreementDetailUtils';
import { beginWorkflow, getWorkflowParam } from './containers/BottomPanel/utils/workflowUtils';
import { BrokerFileDetail, SearchFilter, ReportManagerSaveSettingData } from './models';
import agreementSummaryStore, { agreementWorkflowStore, postCollateralStore } from './useStores';

export function convertListToString(inputList, flatList = false) {
  inputList = inputList?.filter((s) => s != null);
  let valuesList = flatList
    ? inputList?.map((s) => s.toString())
    : inputList?.map((s) => s.key?.toString());
  return valuesList.toString();
}

export function flattenDictToList(inputList): any[] {
  return inputList?.map((s) => s?.key?.toString());
}

export function paramStringToList(paramString: string | null) {
  if (!paramString) return null;
  return paramString?.split(',').map((id) => {
    return { key: parseInt(id), value: '' };
  });
}

export function sortAgmtSummaryData(treeData: any[]) {
  var outputList: any[] = [];
  for (let data of treeData) {
    if (data['workflowStatus'] != 'PUBLISHED') outputList.push(data);
  }
  for (let data of treeData) {
    if (data['workflowStatus'] == 'PUBLISHED') outputList.push(data);
  }
  return outputList;
}

export function applyQuickFilter(treeData: any[], selectedQuickFilters: any[]) {
  if (selectedQuickFilters.length == 0) return treeData;

  var outputList: any[] = [];
  for (let data of treeData) {
    let exists = false;
    for (let quickFilter of selectedQuickFilters) {
      if (data['workflowStatus'] === quickFilter || data['ragCategoryCode'] === quickFilter)
        exists = true;
    }
    if (exists) {
      outputList.push(data);
    }
  }
  return outputList;
}

const getToastType = (type: string) => {
  let toastType: any = ToastService.ToastType.SUCCESS;
  switch (type) {
    case TOAST_TYPE.CRITICAL:
      toastType = ToastService.ToastType.CRITICAL;
      break;
    case TOAST_TYPE.SUCCESS:
      toastType = ToastService.ToastType.SUCCESS;
      break;
    case TOAST_TYPE.INFO:
      toastType = ToastService.ToastType.INFO;
      break;
  }
  return toastType;
};

export const showToastService = (content: string, type: string, dismissTime: number = 2000) => {
  ToastService.append({
    content: content,
    type: getToastType(type),
    placement: ToastService.Placement.TOP_RIGHT,
    dismissTime: dismissTime,
  });
};

export const firstLetterCap = (str: string) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

export const getMarginCallTypes = (marginCallTypeEdSummaryDataList: Array<any>): string => {
  return marginCallTypeEdSummaryDataList
    .map((e) => e.edSummaryDataKey.marginCallType.abbrev)
    .join();
};

export const getValueOrDefault = (obj: any, defaultValue = {}) => {
  if (obj) return obj;
  else return defaultValue;
};

export const launchWindow = (url: string) => {
  let childWindow = window.open(url, '_blank');
  if (childWindow == null || childWindow == undefined) {
    showToastService(MESSAGES.LAUNCH_WINDOW_FAILURE, TOAST_TYPE.INFO);
    return;
  }
};

export function populateSearchFilterFromUrl(urlString: string): SearchFilter {
  if (urlString == '') return {
    ...INITIAL_FILTERS,
    selectedDate: agreementSummaryStore.defaultDate
  };

  let params = new URLSearchParams(urlString);
  const dateString = params.get('dateString');
  const legalEntityFamiliesFromUrl = paramStringToList(params.get('legalEntityFamilyIds'));
  const legalEntitiesFromUrl = paramStringToList(params.get('legalEntityIds'));
  const cpeFromUrl = paramStringToList(params.get('cpeIds'));
  const agreementTypesFromUrl = paramStringToList(params.get('agreementTypeIds'));
  const currencyFromUrl = paramStringToList(params.get('currencyId'));
  const workflowStatusFromUrl = params
    .get('workflowStatus')
    ?.split(',')
    ?.filter((x) => x != '');
  const exceptionStatusFromUrl = params
    .get('exceptionStatus')
    ?.split(',')
    ?.filter((x) => x != '');

  let searchFilter = {
    selectedDate: dateString ? dateString : agreementSummaryStore.defaultDate,
    selectedLegalEntityFamilies: legalEntityFamiliesFromUrl
      ? legalEntityFamiliesFromUrl
      : INITIAL_FILTERS.selectedLegalEntityFamilies,
    selectedLegalEntities: legalEntitiesFromUrl
      ? legalEntitiesFromUrl
      : INITIAL_FILTERS.selectedLegalEntities,
    selectedCpes: cpeFromUrl ? cpeFromUrl : INITIAL_FILTERS.selectedCpes,
    selectedAgreementTypes: agreementTypesFromUrl
      ? agreementTypesFromUrl
      : INITIAL_FILTERS.selectedAgreementTypes,
    selectedCurrency: currencyFromUrl ? currencyFromUrl[0] : INITIAL_FILTERS.selectedCurrency,
    workflowStatus: workflowStatusFromUrl ? workflowStatusFromUrl : INITIAL_FILTERS.workflowStatus,
    exceptionStatus: exceptionStatusFromUrl
      ? exceptionStatusFromUrl
      : INITIAL_FILTERS.exceptionStatus,
  };
  return searchFilter;
}

export const onClickDownloadBrokerFile = (selectedBrokerFile: BrokerFileDetail, date: string) => {
  let brokerFileUrl = selectedBrokerFile.brokerFileUrl;
  if (brokerFileUrl.indexOf('transfers') != -1) {
    window.alert("Broker file URL is unavailable. Please trigger respective cocoa parser and broker & rag sync to sync latest file info.");
  } else {
    window.open(`/cocoa/api/file/downloadFile?objectUri=&fileId=${brokerFileUrl}&fileDate=${date}`);
  }
};

export const onClickPostCollateral = (
  date: string,
  isTreasuryAgreement: boolean,
  selectedAgreementData
) => {
  displayPostCollateralPopup(date, isTreasuryAgreement, selectedAgreementData, () =>
    showReloadAgreementSummaryConfirmationDialog(
      MESSAGES.RELOAD_AGREEMENT_SUMMARY_CONFIRMATION_POST_LOG_WIRE_SUCCESS
    )
  );
};

export const displayPostCollateralPopup = async (
  date: string,
  isTreasuryAgreement: boolean,
  selectedAgreementData,
  onPostCollateralSuccess
) => {
  const legalEntityId = selectedAgreementData.treasuryAgreementDetail.legalEntityId;
  const cpeEntityId = selectedAgreementData.treasuryAgreementDetail.cpeId;
  const agreementTypeId = selectedAgreementData.treasuryAgreementDetail.agreementTypeId;
  const actionableAgreementId = getAgreementDetail(selectedAgreementData).agreementId;
  const applicableEdSummaryData = getApplicableEdSummaryData(selectedAgreementData);
  const internalVsExternalData = getInternalVsExternalData(applicableEdSummaryData);
  const isSegAgreement = selectedAgreementData.edSummaryDataKey?.agreementDetail.agreementTypeName === 'CNC'

  try {
    let workflowId = await getNewOrExistingWorkflowId(
      date,
      legalEntityId,
      cpeEntityId,
      agreementTypeId
    );
    if (workflowId) {
      agreementWorkflowStore.searchDate = agreementSummaryStore.lastSearchFilter.selectedDate;
      agreementWorkflowStore.agreementDetail.internalVsExternalData = internalVsExternalData;
      agreementWorkflowStore.workflowId = workflowId;
      agreementWorkflowStore.actionableAgreementId = actionableAgreementId;

      postCollateralStore.setAgreementData(
        {
          tag: getMarginCallType(isTreasuryAgreement, selectedAgreementData),
          accountType: getApplicableAccountType(isTreasuryAgreement, selectedAgreementData),
          tripartyAgreementId: isSegAgreement ? selectedAgreementData.treasuryAgreementDetail.agreementId : null,
          isPopulateAsm: applicableEdSummaryData.curDayEdSummaryData.actualSecuritiesMovement !== 0,
          acmRC: getRoundedAcmRC(
            applicableEdSummaryData.curDayEdSummaryData.expectedCashMovement,
            selectedAgreementData.reportingCurrency.price
          ),
          asmRC: getRoundedAcmRC(
            applicableEdSummaryData.curDayEdSummaryData.actualSecuritiesMovement,
            selectedAgreementData.reportingCurrency.price
          ),
          custodianAccountId: isSegAgreement ? selectedAgreementData.edSummaryDataKey.agreementDetail.tripartyCustodianAccountId : null,
          reportingCurrency: selectedAgreementData.reportingCurrency.isoCode,
          reportingCurrencySpn: selectedAgreementData.reportingCurrency.spn,
          reportingCurrencyFxRate: selectedAgreementData.reportingCurrency.price,
        },
        actionableAgreementId,
        'AgreementSummary',
        onPostCollateralSuccess
      );
    }
  } catch (e) {
    showToastService(MESSAGES.SHOW_POST_COLLATERAL_POPUP_FAILURE, TOAST_TYPE.CRITICAL);
  }
};

function getRoundedAcmRC(acmRC: any, reportingCurrencyPrice: any) {
  if (acmRC) {
    acmRC = agreementSummaryStore.lastSearchFilter.selectedCurrency.value == "USD" ? getRCFromUSD(acmRC, reportingCurrencyPrice) : acmRC
    return roundValue(Math.round(acmRC), 100000);
  }
}

function getRCFromUSD(inputUsd, reportingCurrencyPrice) {
  if (inputUsd && reportingCurrencyPrice) {
    return inputUsd * reportingCurrencyPrice
  }
  return inputUsd
}

function roundValue(input, digits) {
  var output = 0;

  if (input > 0) {
    output = input % digits == 0 ? input : input - (input % digits) + digits;
  } else if (input < 0) {
    // when acm is -ve, we can't round up as its a margin pull
    output = input % digits == 0 ? input : input - (input % digits);
  }
  return output;
}

const getMarginCallType = (isTreasuryAgreement: boolean, selectedAgreementdata) => {
  if (isTreasuryAgreement) {
    return selectedAgreementdata.parentMarginCallTypeEdSummaryDataList
      ?.map((e) => e.edSummaryDataKey.marginCallType.abbrev)
      .join();
  } else {
    return selectedAgreementdata.edSummaryDataKey?.marginCallType.abbrev;
  }
};

const getApplicableAccountType = (isTreasuryAgreement: boolean, selectedAgreementdata) => {
  if (isTreasuryAgreement) {
    let applicableMarginCallType = selectedAgreementdata.applicableEdSummaryDataKey?.marginCallType.abbrev;
    return selectedAgreementdata.childMarginCallTypeEdSummaryDataList
      ?.find((e) => e.edSummaryDataKey?.marginCallType.abbrev == applicableMarginCallType)?.curDayEdSummaryData.accountType;
  } else {
    return selectedAgreementdata.curDayEdSummaryData.accountType;
  }
};

const getNewOrExistingWorkflowId = async (
  date: string,
  legalEntityId: number,
  cpeEntityId: number,
  agreementTypeId: number
) => {
  let workflowParams = getWorkflowParam(date, legalEntityId, cpeEntityId, agreementTypeId);
  try {
    let workflowStatusDetail = await beginWorkflow(workflowParams);
    if (workflowStatusDetail) {
      return workflowStatusDetail.workflowId;
    }
  } catch (e) {
    showToastService(MESSAGES.INITIATE_WORKFLOW_FAILURE, TOAST_TYPE.CRITICAL);
  }
};

// this is required to handle cases where for a simm applicable agreement, the HOUSE leg is not applicable.
// thus setting the applicable excess/deficit as raw excess/deficit
export const getApplicableExcessDeficit = (excessDeficitData: ExcessDeficitData | undefined) => {
  if (excessDeficitData?.applicableExcessDeficit != undefined) {
    return excessDeficitData.applicableExcessDeficit;
  } else {
    return excessDeficitData?.excessDeficit;
  }
};

export function convertToReportManagerColumns(displayColumns: string[]): string {
  let reportManagerCols: string[] = [...displayColumns];
  for (let col of displayColumns) {
    let mappingCols: string[] = gridColumnsToReportManagerColumnsMap[col]
    if (mappingCols != null && mappingCols.length > 0) {
      reportManagerCols = reportManagerCols.concat(mappingCols)
    }
  }
  reportManagerCols = [... new Set(reportManagerCols)]
  let reportManagerSaveSettingData: ReportManagerSaveSettingData = { columns: [] }
  for (let col of reportManagerCols) {
    reportManagerSaveSettingData.columns.push({ 'id': col })
  }

  return JSON.stringify(reportManagerSaveSettingData)
}

export function convertToDisplayColumns(reportManagerCols: ReportManagerSaveSettingData): string[] {
  let displayCols: string[] = [];

  if ('columns' in reportManagerCols) {
    for (let col of reportManagerCols.columns) {
      if ('id' in col && gridColumnsToReportManagerColumnsMap.hasOwnProperty(col.id)) {
        displayCols.push(col.id)
      }
    }
  }
  return displayCols
}
