
import PostCollateralStore from '../Lcm/AgreementWorkflow/stores/PostCollateralStore';
import AgreementWorkflowStore from '../Lcm/AgreementWorkflow/stores/AgreementWorkflowStore';
import { AgreementSummaryStore } from './AgreementSummaryStore';

const agreementSummaryStore = new AgreementSummaryStore()

export default agreementSummaryStore;
export const agreementWorkflowStore = new AgreementWorkflowStore();
export const postCollateralStore = new PostCollateralStore(agreementWorkflowStore)
