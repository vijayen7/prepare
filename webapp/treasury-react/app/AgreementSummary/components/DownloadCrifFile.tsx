import React, { useState } from "react";
import { downloadCrifFile } from '../../Lcm/AgreementWorkflow/utils';
import { SplitButton } from 'arc-react-components';


export const DownloadCrifFile: React.FC<any> = (props) => {

  if (props.agreementData?.curDayEdSummaryData?.crifFileUrl !== undefined) {
    const crifFile = props.agreementData?.curDayEdSummaryData?.crifFileUrl;

    return (
      <button className="margin--horizontal" onClick={() => downloadCrifFile(crifFile)}>
        <i className="icon-download" /> Internal CRIF File
      </button>
    )
  }

  const filteredResults = props.agreementData?.childMarginCallTypeEdSummaryDataList?.filter(
    (e) => e.curDayEdSummaryData?.crifFileUrl !== undefined
  )

  const [stateOptions, setStateValues] = useState(filteredResults);

  return filteredResults?.length > 0 ?
    (
      <SplitButton style={{ display: 'inline' }} label="Internal CRIF File">
        {stateOptions.map((option, index) => (
          <SplitButton.Item>
            <button className="margin--horizontal" onClick={() => downloadCrifFile(option.curDayEdSummaryData?.crifFileUrl)}>
              <i className="icon-download" /> {option.edSummaryDataKey?.marginCallType?.abbrev}
            </button>
          </SplitButton.Item>
        ))}
      </SplitButton>
    ) : null;

};
