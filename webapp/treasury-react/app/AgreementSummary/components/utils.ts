import { MESSAGES, TOAST_TYPE } from "../constants";
import { cancelWorkflow as cancelWorkflowApi, resumeWorkflow as resumeWorkflowApi } from "../containers/BottomPanel/api";
import agreementSummaryStore from "../useStores";
import { launchWindow, showToastService } from "../utils";
import { showConfirmationDialog } from "./dialog";

export async function resumeWorkflow(workflowId: number, wfDataUrl, wfDataParams) {
  try {
    await resumeWorkflowApi(workflowId)
    launchWindow(wfDataUrl + "?" + wfDataParams);
    agreementSummaryStore.handleSearch()
  } catch (e) {
    showToastService(MESSAGES.WORKFLOW_EXECUTE_FAILURE, TOAST_TYPE.CRITICAL);
  }
}

export async function cancelLastWorkflow(workflowId: number) {
  const cancelWorklfow = await showConfirmationDialog(MESSAGES.CONFIRM_WORKLFOW_CANCEL)
  if (!cancelWorklfow) {
    return false;
  }

  try {
    await cancelWorkflowApi(workflowId)
    agreementSummaryStore.handleSearch()
  } catch (e) {
    showToastService(MESSAGES.WORKFLOW_CANCEL_FAILURE, TOAST_TYPE.CRITICAL);
  }
}
