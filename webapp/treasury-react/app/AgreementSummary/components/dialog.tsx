import React from 'react';
import { DialogService, Card } from 'arc-react-components';
import { displayBeginWorkflowDialog } from '../containers/BottomPanel/utils/workflowUtils';
import { cancelLastWorkflow, resumeWorkflow } from './utils';
import agreementSummaryStore from './../useStores';
import { MarginTypes } from '../containers/Grid/grid-columns';
import { getMarginCallTypes } from '../utils';

export async function showDialog(title: string, message: any, footer?: any) {
  return await DialogService.open({
    title: title,
    renderBody: () => message,
    renderFooter: ({ submitProps, cancelProps, submit }) =>
      footer ? footer({ submitProps, cancelProps, submit }) : undefined,
  });
}

export async function showConfirmationDialog(message: string) {
  let footer = ({ submitProps, cancelProps }) => (
    <div>
      <button {...cancelProps}>Go Back</button>
      <button {...submitProps}>Confirm</button>
    </div>
  );

  const dialog = await showDialog('Confirm', message, footer);
  if (dialog.submitted) {
    return true;
  }
}

export async function showResumeWorkflowDialog(
  title: string,
  message: string,
  workflowId: number,
  wfDataUrl: string,
  wfDataParam: any
) {
  let footer = ({ submitProps, cancelProps, submit }) => (
    <div>
      <button {...cancelProps}>Go Back</button>
      <button
        {...submitProps}
        onClick={() => {
          cancelLastWorkflow(workflowId);
          submit();
        }}
      >
        Cancel
      </button>
      <button
        {...submitProps}
        onClick={() => {
          resumeWorkflow(workflowId, wfDataUrl, wfDataParam);
          submit();
        }}
      >
        Resume Workflow
      </button>
    </div>
  );

  await showDialog(title, message, footer);
}

export async function showFailedWorkflowDialog(
  title: string,
  message: string,
  workflowId: number,
  wfParams: any,
  wfDataUrl: string,
  wfDataParam: any
) {
  let footer = ({ submitProps, cancelProps }) => (
    <div>
      <button {...cancelProps}>Go Back</button>
      <button {...submitProps}>Confirm</button>
    </div>
  );

  const failedWorkflowDialog = await showDialog(title, message, footer);

  if (failedWorkflowDialog.submitted) {
    const isWorkflowCancelled = await cancelLastWorkflow(workflowId);
    if (isWorkflowCancelled) {
      await displayBeginWorkflowDialog(wfParams, wfDataUrl, wfDataParam);
    }
  }
}

export async function showReloadAgreementSummaryConfirmationDialog(message: string) {
  let title = 'Confirm';
  let footer = ({ submitProps, cancelProps }) => (
    <div>
      <button {...cancelProps}>Go Back</button>
      <button {...submitProps}>Confirm</button>
    </div>
  );

  const postEmailCallReportDialog = await showDialog(title, message, footer);

  if (postEmailCallReportDialog.submitted) {
    // reloading agreement summary search
    agreementSummaryStore.handleSearch();
  }
}

export async function showAgreementInfoDialog(props: any) {
  let title = 'Agreement Info';
  let message = (
    <div className="margin--vertical--small">
      <Card title="Legal Entity">{props.legalEntityName}</Card>
      <Card title="Exposure Counterparty">{props.cpeName}</Card>
      <Card title="Agreement Type">{props.agreementTypeName}</Card>
      <Card title="Margin Call Type">
        <MarginTypes
          value={getMarginCallTypes(props.agreementData.parentMarginCallTypeEdSummaryDataList)}
        />
      </Card>
    </div>
  );
  await showDialog(title, message);
}
