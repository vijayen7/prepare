import React from 'react';
import { displayExcessDeficitDataRow } from '../../../Lcm/AgreementWorkflow/components/utils';
import { ExcessDeficitData, InternalVsExternalData } from '../../../Lcm/AgreementWorkflow/models';
import { DIFF, EXTERNAL, INTERNAL } from '../../../commons/constants';

interface SegIntVsExtPropType {
  isSimmApplicable: boolean;
  segInternalVsExternalData: InternalVsExternalData;
}

export const SegInternalVsExternalTable: React.FC<SegIntVsExtPropType> = (
  props: SegIntVsExtPropType
) => {
  return props.isSimmApplicable ? (
    <SimmSegInternalVsExternalTable {...props} />
  ) : (
    <NonSimmSegInternalVsExternalData {...props} />
  );
};

const SimmSegInternalVsExternalTable = ({ segInternalVsExternalData }: SegIntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th id="header" colSpan={6}>
            Segregated Internal vs External Data
          </th>
        </tr>
        <tr id="rowHeader">
          <th id="source">Source</th>
          <th id="requirement">Requirement</th>
          <th id="applicableRequirement">Applicable Requirement</th>
          <th id="segCashCollateral">Seg Cash Collateral</th>
          <th id="segSecuritiesCollateral">Seg Securities Collateral</th>
          <th id="segExcessDeficit">Seg E/D</th>
        </tr>
        {getSimmApplicableExcessDeficitDataRow(segInternalVsExternalData.internalEDData, INTERNAL)}

        {getSimmApplicableExcessDeficitDataRow(segInternalVsExternalData.externalEDData, EXTERNAL)}

        {getSimmApplicableExcessDeficitDataRow(segInternalVsExternalData.diffEDData, DIFF)}
      </tbody>
    </table>
  );
};

const NonSimmSegInternalVsExternalData = ({ segInternalVsExternalData }: SegIntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th id="header" colSpan={5}>
            Segregated Internal vs External Data
          </th>
        </tr>
        <tr id="rowHeader">
          <th id="source">Source</th>
          <th id="segRequirement">Seg Requirement</th>
          <th id="segCashCollateral">Seg Cash Collateral</th>
          <th id="segSecuritiesCollateral">Seg Securities Collateral</th>
          <th id="segExcessDeficit">Seg E/D</th>
        </tr>
        {getExcessDeficitDataRow(segInternalVsExternalData.internalEDData, INTERNAL)}

        {getExcessDeficitDataRow(segInternalVsExternalData.externalEDData, EXTERNAL)}

        {getExcessDeficitDataRow(segInternalVsExternalData.diffEDData, DIFF)}
      </tbody>
    </table>
  );
};

const getSimmApplicableExcessDeficitDataRow = (
  excessDeficitData: ExcessDeficitData,
  rowName: string
): any => {
  return (
    <tr id={rowName}>
      <td id={`${rowName}_data`} style={{ textAlign: 'right' }}>
        {rowName}
      </td>
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.applicableRequirement)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.applicableExcessDeficit)}
    </tr>
  );
};

const getExcessDeficitDataRow = (excessDeficitData: ExcessDeficitData, rowName: string): any => {
  return (
    <tr id={rowName}>
      <td id={`${rowName}_data`} style={{ textAlign: 'right' }}>
        {rowName}
      </td>
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.excessDeficit)}
    </tr>
  );
};
