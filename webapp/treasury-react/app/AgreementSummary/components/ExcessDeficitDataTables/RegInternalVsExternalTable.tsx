import React from 'react';
import { DIFF, EXTERNAL, INTERNAL } from '../../../commons/constants';
import { displayExcessDeficitDataRow } from '../../../Lcm/AgreementWorkflow/components/utils';
import { ExcessDeficitData, InternalVsExternalData } from '../../../Lcm/AgreementWorkflow/models';

interface RegIntVsExtPropType {
  isSimmApplicable: boolean;
  regInternalVsExternalData: InternalVsExternalData;
}

export const RegInternalVsExternalTable: React.FC<RegIntVsExtPropType> = (
  props: RegIntVsExtPropType
) => {
  return props.isSimmApplicable ? (
    <SimmRegInternalVsExternalTable {...props} />
  ) : (
    <NonSimmRegInternalVsExternalData {...props} />
  );
};

const SimmRegInternalVsExternalTable = ({ regInternalVsExternalData }: RegIntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th colSpan={6}>Reg Internal vs External Data</th>
        </tr>
        <tr>
          <th>Source</th>
          <th>Requirement</th>
          <th>Applicable Requirement</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>E/D</th>
        </tr>

        {getSimmApplicableExcessDeficitDataRow(regInternalVsExternalData.internalEDData, INTERNAL)}

        {getSimmApplicableExcessDeficitDataRow(regInternalVsExternalData.externalEDData, EXTERNAL)}

        {getSimmApplicableExcessDeficitDataRow(regInternalVsExternalData.diffEDData, DIFF)}
      </tbody>
    </table>
  );
};

const NonSimmRegInternalVsExternalData = ({ regInternalVsExternalData }: RegIntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th colSpan={6}>Reg Internal vs External Data</th>
        </tr>
        <tr>
          <th>Source</th>
          <th>Exposure</th>
          <th>Requirement</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>E/D</th>
        </tr>

        {getExcessDeficitDataRow(regInternalVsExternalData.internalEDData, INTERNAL)}

        {getExcessDeficitDataRow(regInternalVsExternalData.externalEDData, EXTERNAL)}

        {getExcessDeficitDataRow(regInternalVsExternalData.diffEDData, DIFF)}
      </tbody>
    </table>
  );
};

const getSimmApplicableExcessDeficitDataRow = (
  excessDeficitData: ExcessDeficitData,
  rowName: string
): any => {
  return (
    <tr>
      <td style={{ textAlign: 'right' }}>{rowName}</td>
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.applicableRequirement)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.applicableExcessDeficit)}
    </tr>
  );
};

const getExcessDeficitDataRow = (excessDeficitData: ExcessDeficitData, rowName: string): any => {
  return (
    <tr>
      <td style={{ textAlign: 'right' }}>{rowName}</td>
      {displayExcessDeficitDataRow(excessDeficitData.exposure)}
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.excessDeficit)}
    </tr>
  );
};
