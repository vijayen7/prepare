import React from 'react';
import { AdjustedIntVsInt } from '../../../Lcm/AgreementWorkflow/models';
import { AdjustIntVsIntTable as DefaultAdjustIntVsIntTable } from '../../../Lcm/AgreementWorkflow/components/ExcessDeficitAdditionalSummary/AdjustIntVsIntTable';
import { getSimmExcessDeficitDataRow } from './InternalVsExternalTable';

interface AdjustIntVsIntPropType {
  isSimmApplicable: boolean;
  adjustedIntVsInt: AdjustedIntVsInt;
}

export const AdjustIntVsIntTable: React.FC<AdjustIntVsIntPropType> = (
  props: AdjustIntVsIntPropType
) => {
  return props.isSimmApplicable ? (
    <SimmAppplicableAdjustedIntVsIntTable {...props} />
  ) : (
    <DefaultAdjustIntVsIntTable adjustedIntVsInt={props.adjustedIntVsInt} />
  );
};

const SimmAppplicableAdjustedIntVsIntTable = ({ adjustedIntVsInt }: AdjustIntVsIntPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsInternalAdj"
    >
      <tbody>
        <tr>
          <th colSpan={7}>Internal Vs Internal Adjusted</th>
        </tr>
        <tr>
          <th></th>
          <th>Requirement</th>
          <th>Applicable Requirement</th>
          <th>Exposure</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>E/D</th>
        </tr>
        {getSimmExcessDeficitDataRow(adjustedIntVsInt.internalAdjustedData, 'Adjusted')}

        {getSimmExcessDeficitDataRow(adjustedIntVsInt.internalUnadjustedData, 'Unadjusted')}

        {getSimmExcessDeficitDataRow(adjustedIntVsInt.internalAdjustedDiffData, 'Diff')}
      </tbody>
    </table>
  );
};
