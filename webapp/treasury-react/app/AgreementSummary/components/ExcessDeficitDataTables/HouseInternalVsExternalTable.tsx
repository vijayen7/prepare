import React from 'react';
import { DIFF, EXTERNAL, INTERNAL } from '../../../commons/constants';
import { displayExcessDeficitDataRow } from '../../../Lcm/AgreementWorkflow/components/utils';
import { ExcessDeficitData, InternalVsExternalData } from '../../../Lcm/AgreementWorkflow/models';

interface HouseIntVsExtPropType {
  isSimmApplicable: boolean;
  houseInternalVsExternalData: InternalVsExternalData;
}

export const HouseInternalVsExternalTable: React.FC<HouseIntVsExtPropType> = (
  props: HouseIntVsExtPropType
) => {
  return props.isSimmApplicable ? (
    <SimmHouseInternalVsExternalTable {...props} />
  ) : (
    <NonSimmHouseInternalVsExternalData {...props} />
  );
};

export const SimmHouseInternalVsExternalTable: React.FC<HouseIntVsExtPropType> = ({
  houseInternalVsExternalData,
}: HouseIntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th id="header" colSpan={7}>
            House Internal vs External Data
          </th>
        </tr>
        <tr id="rowHeader">
          <th id="source">Source</th>
          <th id="exposure">Exposure</th>
          <th id="requirement">Requirement</th>
          <th id="applicableRequirement">Applicable Requirement</th>
          <th id="cashCollateral">Cash Collateral</th>
          <th id="securitiesCollateral">Securities Collateral</th>
          <th id="excessDeficit">E/D</th>
        </tr>

        {getSimmApplicableExcessDeficitDataRow(houseInternalVsExternalData.internalEDData, INTERNAL)}

        {getSimmApplicableExcessDeficitDataRow(houseInternalVsExternalData.externalEDData, EXTERNAL)}

        {getSimmApplicableExcessDeficitDataRow(houseInternalVsExternalData.diffEDData, DIFF)}
      </tbody>
    </table>
  );
};

export const NonSimmHouseInternalVsExternalData: React.FC<HouseIntVsExtPropType> = ({
  houseInternalVsExternalData,
}: HouseIntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th id="header" colSpan={7}>
            House Internal vs External Data
          </th>
        </tr>
        <tr id="rowHeader">
          <th id="source">Source</th>
          <th id="exposure">Exposure</th>
          <th id="requirement">Requirement</th>
          <th id="cashCollateral">Cash Collateral</th>
          <th id="securitiesCollateral">Securities Collateral</th>
          <th id="excessDeficit">E/D</th>
        </tr>

        {getExcessDeficitDataRow(houseInternalVsExternalData.internalEDData, INTERNAL)}

        {getExcessDeficitDataRow(houseInternalVsExternalData.externalEDData, EXTERNAL)}

        {getExcessDeficitDataRow(houseInternalVsExternalData.diffEDData, DIFF)}
      </tbody>
    </table>
  );
};

const getSimmApplicableExcessDeficitDataRow = (excessDeficitData: ExcessDeficitData, rowName: string): any => {
  return (
    <tr id={rowName}>
      <td id={`${rowName}_data`} style={{ textAlign: 'right' }}>
        {rowName}
      </td>
      {displayExcessDeficitDataRow(excessDeficitData.exposure)}
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.applicableRequirement)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.excessDeficit)}
    </tr>
  );
};

const getExcessDeficitDataRow = (excessDeficitData: ExcessDeficitData, rowName: string): any => {
  return (
    <tr id={rowName}>
      <td id={`${rowName}_data`} style={{ textAlign: 'right' }}>
        {rowName}
      </td>
      {displayExcessDeficitDataRow(excessDeficitData.exposure)}
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.excessDeficit)}
    </tr>
  );
};
