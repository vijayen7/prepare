import React from 'react';
import { displayExcessDeficitDataRow } from '../../../Lcm/AgreementWorkflow/components/utils';
import { ExcessDeficitData, InternalVsExternalData } from '../../../Lcm/AgreementWorkflow/models';
import { DIFF, EXTERNAL, INTERNAL } from '../../../commons/constants';

interface RegCpeIntVsExtPropType {
  regCpeInternalVsExternalData: InternalVsExternalData;
}

export const RegCpeInternalVsExternalTable = ({
  regCpeInternalVsExternalData,
}: RegCpeIntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th colSpan={6}>Reg CPE Internal vs External Data</th>
        </tr>
        <tr>
          <th>Source</th>
          <th>Requirement</th>
          <th>Applicable Requirement</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>E/D</th>
        </tr>

        {getExcessDeficitDataRow(regCpeInternalVsExternalData.internalEDData, INTERNAL)}

        {getExcessDeficitDataRow(regCpeInternalVsExternalData.externalEDData, EXTERNAL)}

        {getExcessDeficitDataRow(regCpeInternalVsExternalData.diffEDData, DIFF)}
      </tbody>
    </table>
  );
};

const getExcessDeficitDataRow = (excessDeficitData: ExcessDeficitData, rowName: string): any => {
  return (
    <tr>
      <td style={{ textAlign: 'right' }}>{rowName}</td>
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.applicableRequirement)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.applicableExcessDeficit)}
    </tr>
  );
};
