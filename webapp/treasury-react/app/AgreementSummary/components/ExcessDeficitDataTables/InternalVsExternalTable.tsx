import React from 'react';
import { DIFF, EXTERNAL, INTERNAL } from '../../../commons/constants';
import { InternalVsExternalTable as DefaultInternalVsExternalTable } from '../../../Lcm/AgreementWorkflow/components/ExcessDeficitDataSummary/InternalVsExternalTable';
import { displayExcessDeficitDataRow } from '../../../Lcm/AgreementWorkflow/components/utils';
import { ExcessDeficitData, InternalVsExternalData } from '../../../Lcm/AgreementWorkflow/models';
import { getApplicableExcessDeficit } from '../../utils';

interface IntVsExtPropType {
  isSimmApplicable: boolean;
  internalVsExternalData: InternalVsExternalData;
}

export const InternalVsExternalTable: React.FC<IntVsExtPropType> = (props: IntVsExtPropType) => {
  return props.isSimmApplicable ? (
    <SimmHouseInternalVsExternalTable {...props} />
  ) : (
    <DefaultInternalVsExternalTable internalVsExternalData={props.internalVsExternalData} />
  );
};

const SimmHouseInternalVsExternalTable = ({
  internalVsExternalData: houseInternalVsExternalData,
}: IntVsExtPropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalVsExternalId"
    >
      <tbody>
        <tr>
          <th id="header" colSpan={7}>
            Internal vs External Data
          </th>
        </tr>
        <tr id="rowHeader">
          <th id="source">Source</th>
          <th id="requirement">Requirement</th>
          <th id="applicableRequirement">Applicable Requirement</th>
          <th id="exposure">Exposure</th>
          <th id="cashCollateral">Cash Collateral</th>
          <th id="securitiesCollateral">Securities Collateral</th>
          <th id="excessDeficit">E/D</th>
        </tr>

        {getSimmExcessDeficitDataRow(houseInternalVsExternalData.internalEDData, INTERNAL)}

        {getSimmExcessDeficitDataRow(houseInternalVsExternalData.externalEDData, EXTERNAL)}

        {getSimmExcessDeficitDataRow(houseInternalVsExternalData.diffEDData, DIFF)}
      </tbody>
    </table>
  );
};

export const getSimmExcessDeficitDataRow = (
  excessDeficitData: ExcessDeficitData,
  rowName: string
): any => {
  return (
    <tr id={rowName}>
      <td id={`${rowName}_data`} style={{ textAlign: 'right' }}>
        {rowName}
      </td>
      {displayExcessDeficitDataRow(excessDeficitData.requirement)}
      {displayExcessDeficitDataRow(excessDeficitData.applicableRequirement)}
      {displayExcessDeficitDataRow(excessDeficitData.exposure)}
      {displayExcessDeficitDataRow(excessDeficitData.cashCollateral)}
      {displayExcessDeficitDataRow(excessDeficitData.securitiesCollateral)}
      {displayExcessDeficitDataRow(getApplicableExcessDeficit(excessDeficitData))}
    </tr>
  );
};
