import React from 'react';
import { ExternalT1VsT2 } from '../../../Lcm/AgreementWorkflow/models';
import { ExternalT1VsT2Table as DefaultExternalT1VsT2Table } from '../../../Lcm/AgreementWorkflow/components/ExcessDeficitAdditionalSummary/ExternalT1VsT2Table';
import { getSimmExcessDeficitDataRow } from './InternalVsExternalTable';

interface ExtT1VsT2PropType {
  isSimmApplicable: boolean;
  externalT1VsT2: ExternalT1VsT2;
}

export const ExternalT1VsT2Table: React.FC<ExtT1VsT2PropType> = (props: ExtT1VsT2PropType) => {
  return props.isSimmApplicable ? (
    <SimmAppplicableExtT1VsT2Table {...props} />
  ) : (
    <DefaultExternalT1VsT2Table externalT1VsT2={props.externalT1VsT2} />
  );
};

const SimmAppplicableExtT1VsT2Table = ({ externalT1VsT2 }: ExtT1VsT2PropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="externalT1VsT2Table"
    >
      <tbody>
        <tr>
          <th colSpan={7}>External Data: T-1 vs T-2</th>
        </tr>
        <tr>
          <th></th>
          <th>Requirement</th>
          <th>Applicable Requirement</th>
          <th>Exposure</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>E/D</th>
        </tr>
        {getSimmExcessDeficitDataRow(externalT1VsT2.todayExternalT1VsT2Data, 'Today')}

        {getSimmExcessDeficitDataRow(externalT1VsT2.yesterdayExternalT1VsT2Data, 'Yesterday')}

        {getSimmExcessDeficitDataRow(externalT1VsT2.diffExternalT1VsT2Data, 'Diff')}
      </tbody>
    </table>
  );
};
