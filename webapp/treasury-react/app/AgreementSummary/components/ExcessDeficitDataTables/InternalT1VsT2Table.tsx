import React from 'react';
import { InternalT1VsT2 } from '../../../Lcm/AgreementWorkflow/models';
import { InternalT1VsT2Table as DefaultInternalT1VsT2Table } from '../../../Lcm/AgreementWorkflow/components/ExcessDeficitAdditionalSummary/InternalT1VsT2Table';
import { getSimmExcessDeficitDataRow } from './InternalVsExternalTable';

interface IntT1VsT2PropType {
  isSimmApplicable: boolean;
  internalT1VsT2: InternalT1VsT2;
}

export const InternalT1VsT2Table: React.FC<IntT1VsT2PropType> = (props: IntT1VsT2PropType) => {
  return props.isSimmApplicable ? (
    <SimmAppplicableIntT1VsT2Table {...props} />
  ) : (
    <DefaultInternalT1VsT2Table internalT1VsT2={props.internalT1VsT2} />
  );
};

const SimmAppplicableIntT1VsT2Table = ({ internalT1VsT2 }: IntT1VsT2PropType) => {
  return (
    <table
      className="table treasury-table--data-summary margin--vertical--small margin--horizontal--double"
      id="internalT1VsT2Table"
    >
      <tbody>
        <tr>
          <th colSpan={7}>Internal Data: T-1 vs T-2</th>
        </tr>
        <tr>
          <th></th>
          <th>Requirement</th>
          <th>Applicable Requirement</th>
          <th>Exposure</th>
          <th>Cash Collateral</th>
          <th>Securities Collateral</th>
          <th>E/D</th>
        </tr>
        {getSimmExcessDeficitDataRow(internalT1VsT2.todayInternalT1VsT2Data, 'Today')}

        {getSimmExcessDeficitDataRow(internalT1VsT2.yesterdayInternalT1VsT2Data, 'Yesterday')}

        {getSimmExcessDeficitDataRow(internalT1VsT2.diffInternalT1VsT2Data, 'Diff')}
      </tbody>
    </table>
  );
};
