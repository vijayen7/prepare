import React from 'react';
import { Select } from 'arc-react-components';
import _ from 'lodash';
import { BROKER_DATA_LEVEL_MAP } from '../constants';
import { BrokerFileDetail } from '../models';
import { onClickDownloadBrokerFile } from '../utils';
import { getValueOrDefault } from '../utils';

export const DownloadBrokerFile: React.FC<any> = (props) => {
  const brokerFiles: Array<BrokerFileDetail> = getValueOrDefault(
    props.agreementData?.curDayEdSummaryData?.brokerFileDetail,
    []
  );
  const [selectedBrokerFile, setSelectedBrokerFile] = React.useState(brokerFiles[0]);

  React.useEffect(() => {
    setSelectedBrokerFile(brokerFiles[0]);
  }, [props.agreementData]);

  return selectedBrokerFile?.brokerDataLevelId ? (
    <>
      <Select
        options={brokerFiles.map((e) => BROKER_DATA_LEVEL_MAP[e.brokerDataLevelId])}
        value={BROKER_DATA_LEVEL_MAP[selectedBrokerFile.brokerDataLevelId]}
        onChange={(val: string) => {
          let brokerFile = brokerFiles.find(
            (e) => Number(_.invert(BROKER_DATA_LEVEL_MAP)[val]) === e.brokerDataLevelId
          );
          if (brokerFile) setSelectedBrokerFile(brokerFile);
        }}
      />
      <button
        onClick={() => {
          onClickDownloadBrokerFile(selectedBrokerFile, props.date);
          props.setIsDownloadBrokerFileClicked();
        }}
      >
        <i className="icon-download" /> Broker File
      </button>
    </>
  ) : (
    <div />
  );
};
