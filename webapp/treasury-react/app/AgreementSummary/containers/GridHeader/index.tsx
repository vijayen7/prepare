import React, { Fragment } from 'react';
import { ToggleGroup } from 'arc-react-components';
import { observer } from 'mobx-react-lite';
import agreementSummaryStore from '../../useStores';
import { TokenCounter } from './TokenCounter';
import { Layout } from 'arc-react-components';
import {
  BROKER_DATA_MISSING,
  CLEAR,
  CRITICAL,
  IN_PROGRESS,
  PUBLISHED,
  WARNING,
} from '../../constants';
import { quickPublishMultipleAgreements } from '../BottomPanel/utils/workflowUtils';
import SaveSettingsManager from "../../../commons/components/SaveSettingsManager";
import ButtonWithPause from "../../../commons/components/ButtonWithPause";

const GridHeader: React.FC = () => {
  return (
    <Layout className="padding--vertical" isColumnType={true}>
      <Layout.Child childId="child1" size="fit">
        <ButtonWithPause
          className="margin--right"
          disabled={!agreementSummaryStore.enableMultiPublish}
          onClick={quickPublishMultipleAgreements}
          data={<Fragment><i className="icon-quick margin--right" style={{ fontSize: '11px' }} />Quick Publish</Fragment>}
        />

        <button onClick={agreementSummaryStore.handleSearch}>
          <i className="icon-refresh margin--right" style={{ fontSize: '11px' }} />
          Refresh Data
        </button>

        <label className="size--content margin--left--double margin--right">Quick Filters:</label>
        <ToggleGroup
          multiSelect
          value={agreementSummaryStore.selectedQuickFilter}
          onChange={async (value) => {
            agreementSummaryStore.setQuickFilters(value);
            agreementSummaryStore.handleApplyQuickFilter();
          }}
        >
          <ToggleGroup.Button key="quick2" data="CLEAR">
            Clear <TokenCounter value={agreementSummaryStore.quickFilterCounter[CLEAR]} />
          </ToggleGroup.Button>
          <ToggleGroup.Button key="quick3" data="WARNING">
            Warning <TokenCounter value={agreementSummaryStore.quickFilterCounter[WARNING]} />
          </ToggleGroup.Button>
          <ToggleGroup.Button key="quick4" data="CRITICAL">
            Critical <TokenCounter value={agreementSummaryStore.quickFilterCounter[CRITICAL]} />
          </ToggleGroup.Button>
          <ToggleGroup.Button key="quick5" data="BROKER_DATA_MISSING">
            Missing Broker Data
            <TokenCounter value={agreementSummaryStore.quickFilterCounter[BROKER_DATA_MISSING]} />
          </ToggleGroup.Button>
          <ToggleGroup.Button key="quick6" data="IN_PROGRESS">
            In-Progress <TokenCounter value={agreementSummaryStore.quickFilterCounter[IN_PROGRESS]} />
          </ToggleGroup.Button>
          <ToggleGroup.Button key="quick1" data="PUBLISHED">
            Published <TokenCounter value={agreementSummaryStore.quickFilterCounter[PUBLISHED]} />
          </ToggleGroup.Button>
        </ToggleGroup>
      </Layout.Child>
      <Layout.Child childId="dummyChild"></Layout.Child>
      <Layout.Child childId="child2" size='fit'>
        <Layout isColumnType={true}>
          <Layout.Child childId="child3" size='fit'>
            <>Showing for Date: <b>{agreementSummaryStore.searchedDate}&nbsp;&nbsp;</b></>
          </Layout.Child>
          <Layout.Child childId="child4" size='fit'>
            <button className="margin--right" onClick={agreementSummaryStore.createReport}>
              Create Report
            </button>
          </Layout.Child>
          <Layout.Child childId="child5">
            <SaveSettingsManager
              selectedFilters={agreementSummaryStore.reportManagerColumns}
              applySavedFilters={agreementSummaryStore.setDisplayColumnsFromReportManagerColumns}
              applicationName={"AgreementSummaryReactUI"}
            />
          </Layout.Child>
        </Layout>
      </Layout.Child>

    </Layout>
  );
};

export default observer(GridHeader);
