import React from 'react';

type Props = {
  value: string
};

export const TokenCounter: React.FC<Props> = (props: Props) => {
  return <>
    {props.value != undefined &&
      <span
        className="padding--horizontal padding--vertical--small margin--left--small"
      >
        {props.value}
      </span>
    }
  </>
}

