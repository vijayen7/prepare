import React from 'react';
import LegendDot from './LegendDot';

const gridLegends = [
  { title: 'Clear', icon: <LegendDot height="13px" color="success" /> },
  { title: 'Warning', icon: <LegendDot height="13px" color="warning" /> },
  { title: 'Critical', icon: <LegendDot height="13px" color="critical" /> },
  { title: 'Missing Broker Data', icon: <LegendDot height="13px" color="primary" /> },
  { title: 'In-Progress', icon: <i className="icon-locked" /> },
  { title: 'Published', icon: <i className="icon-success" /> },
];

export const GridLegend = () => {
  return (
    <span>
      {gridLegends.map((icon, index) => (
        <span key={index} className="margin--horizontal">
          <span className="margin--horizontal--small">{icon['icon']}</span>
          <span>{icon['title']}</span>
        </span>
      ))}
    </span>
  );
};
