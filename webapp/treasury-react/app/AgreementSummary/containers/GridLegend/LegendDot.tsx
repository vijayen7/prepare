import React from "react";

type Props = {
  color: string,
  height: string
}

const LegendDot: React.FC<Props> = (props: Props) => {
  return (
    <>
      <div
      className={props.color}
      style={{
        height:props.height,
        width:props.height,
        borderRadius: "50%",
        display: "inline-block",
        marginBottom: "-2px"
        }}
      ></div>
    </>
  )
};

export default LegendDot;
