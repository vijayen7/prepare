import React from "react";
import { observer } from "mobx-react-lite";
import { Layout } from 'arc-react-components';
import { CATEGORY } from '../../../constants';

type Props = {
  title: string;
  options: string[];
  data: string[];
  onCheck: any;
  layout?: string;
  titleColSize?: number;
  optionsColSize?: number;
}

const Checkbox: React.FC<Props> = (props: Props) => {
  if (props.layout == "vertical") {
    return (
      <Layout isColumnType>
        <Layout.Child
          size={props.titleColSize ? props.titleColSize : 1}
          childId={props.title + "Question"}
        >
          <div className="margin--small"><label style={{ fontWeight: 500 }}>{props.title}</label></div>
        </Layout.Child>
        <Layout.Child
          size={props.optionsColSize ? props.optionsColSize : 1}
          childId={props.title + "Options"}
        >
          {props.options.map((value, index) =>
          (<div>
            <input
              id={value}
              type="checkbox"
              checked={props.data.includes(value)}
              onChange={(e) => props.onCheck([props.title, value], e)}
              className="margin--horizontal--double" />
            <label className="size--content margin--right--double">{CATEGORY[value]}</label>
          </div>)
          )}
        </Layout.Child>
      </Layout>
    );
  }
  return (
    <>
      <div className="margin--small"><label style={{ fontWeight: 500 }}>{props.title}</label></div>
      {props.options.map((value, index) =>
      (<span>
        <input id="notStartedCBFilter" type="checkbox" className="margin--horizontal"
          checked={props.data.includes(value)}
          onChange={(e) => props.onCheck([props.title, index], e)} />
        <label className="size--content margin--right--double">{CATEGORY[value]}</label>
      </span>)
      )}
    </>
  );
};

export default observer(Checkbox);
