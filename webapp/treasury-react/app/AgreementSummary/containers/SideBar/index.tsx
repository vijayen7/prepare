import React from 'react';
import { observer } from 'mobx-react-lite';
import { Card, Sidebar as ArcSideBar } from 'arc-react-components';
import SaveSettingsManager from '../../../commons/components/SaveSettingsManager';
import ArcDateFilter from '../../../commons/container/ArcDateFilter';
import FilterButton from '../../../commons/components/FilterButton';
import MultiSelectFilter from '../../../commons/filters/components/MultiSelectFilter';
import CpeFilter from '../../../commons/container/CpeFilter';
import SingleSelectFilter from '../../../commons/filters/components/SingleSelectFilter';
import AgreementTypeFilter from '../../../commons/container/AgreementTypeFilter';
import agreementSummaryStore from '../../useStores';
import Checkbox from './Checkbox';
import {
  CLEAR,
  CRITICAL,
  IN_PROGRESS,
  NOT_STARTED,
  PUBLISHED,
  currencyFilterData,
  WARNING,
} from '../../constants';

const SideBar: React.FC = () => {

  const getSelectedFiltersForSaveSettings = () => {
    let {selectedDate, ...searchFilters} = agreementSummaryStore.searchFilter;
    return searchFilters;
  }

  return (
    <ArcSideBar
      style={{ minWidth: '225px' }}
      footer={
        <span>
          <FilterButton
            label="Reset"
            onClick={agreementSummaryStore.handleReset}
            className="margin--horizontal button--tertiary"
          />
          <FilterButton
            label="Search"
            onClick={agreementSummaryStore.handleSearch}
            className="margin--horizontal button--primary float--right"
          />
          <FilterButton
            label="Copy Search URL"
            onClick={agreementSummaryStore.handleCopySearchURL}
            className="margin--horizontal button--tertiary float--right"
          />
        </span>
      }
      title={'Search Criteria'}
    >
      <div className="margin" style={{ height: '30px' }}>
        <SaveSettingsManager
          selectedFilters={getSelectedFiltersForSaveSettings()}
          applySavedFilters={agreementSummaryStore.applySavedFilters}
          applicationName="agreementWorklow"
        />
      </div>
      <Card>
        <ArcDateFilter
          layout="standard"
          stateKey="selectedDate"
          label="Date"
          onSelect={agreementSummaryStore.onSelect}
          data={agreementSummaryStore.searchFilter.selectedDate}
        />
      </Card>
      <Card>
        <MultiSelectFilter
          data={agreementSummaryStore.legalEntityFamilyDataList}
          onSelect={agreementSummaryStore.onSelect}
          label="Legal Entity Families"
          selectedData={agreementSummaryStore.searchFilter.selectedLegalEntityFamilies}
          stateKey="selectedLegalEntityFamilies"
        />
        <MultiSelectFilter
          data={agreementSummaryStore.legalEntityDataList}
          onSelect={agreementSummaryStore.onSelect}
          label="Legal Entities"
          selectedData={agreementSummaryStore.searchFilter.selectedLegalEntities}
          stateKey="selectedLegalEntities"
        />
        <CpeFilter
          onSelect={agreementSummaryStore.onSelect}
          selectedData={agreementSummaryStore.searchFilter.selectedCpes}
          multiSelect={true}
          label={'Counterparty Entities'}
        />
        <AgreementTypeFilter
          onSelect={agreementSummaryStore.onSelect}
          selectedData={agreementSummaryStore.searchFilter.selectedAgreementTypes}
          multiSelect={true}
        />
        <SingleSelectFilter
          onSelect={agreementSummaryStore.onSelect}
          selectedData={agreementSummaryStore.searchFilter.selectedCurrency}
          stateKey="selectedCurrency"
          data={currencyFilterData}
          label="Currency"
        />
      </Card>
      <Card>
        <div className="margin--vertical--double">
          <Checkbox
            title="Workflow Status"
            data={agreementSummaryStore.searchFilter.workflowStatus}
            onCheck={agreementSummaryStore.onChecked}
            options={[NOT_STARTED, IN_PROGRESS, PUBLISHED]}
            layout="vertical"
            titleColSize={2}
            optionsColSize={3}
          />
        </div>
        <div className="margin--vertical--double">
          <Checkbox
            title="Exception Status"
            data={agreementSummaryStore.searchFilter.exceptionStatus}
            onCheck={agreementSummaryStore.onChecked}
            options={[CRITICAL, WARNING, CLEAR]}
            layout="vertical"
            titleColSize={2}
            optionsColSize={3}
          />
        </div>
      </Card>
    </ArcSideBar>
  );
};

export default observer(SideBar);
