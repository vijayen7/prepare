import {
  BEGIN_WORKFLOW,
  FAILED_WORKFLOW,
  FAILURE,
  LOCKED_WORKFLOW,
  RESUME_WORKFLOW,
  SUCCESS,
} from '../../../../commons/constants';
import {launchWindow, showToastService} from '../../../utils';
import {
  showDialog,
  showFailedWorkflowDialog,
  showReloadAgreementSummaryConfirmationDialog,
  showResumeWorkflowDialog,
} from '../../../components/dialog';
import {TOAST_TYPE} from '../../../constants';
import {
  beginWorkflow as beginWorkflowApi,
  cancelWorkflow,
  generateCallReport,
  quickPublishAgreementWorkflow,
  quickPublishMultipleAgreementWorkflows,
  quickPublishWorkflow,
  saveAgreementArtifacts
} from '../api';
import { WorkflowStatusDetail } from '../models';
import ApiResponse from '../../../../commons/models/ApiResponse'
import agreementSummaryStore from '../../../useStores';
import {MESSAGES, QUICK_WIRE_PUBLISH} from '../constants';
import {getAgreementDetail} from './agreementDetailUtils';

/**
 * Begins/Resume Workflow and open new Agreement Workflow window
 */
export const beginAgreementWorkflow = (
  date: string,
  treasuryAgreementId: number,
  legalEntityId: number,
  cpeEntityId: number,
  agreementTypeId: number
) => {
  // Url to show data on begin Workflow
  let wfDataUrl = '/treasury/comet/agreementWorkflow.html';
  let workflowParams = getWorkflowParam(date, legalEntityId, cpeEntityId, agreementTypeId);
  let wfDataParam = `configName=yakAgreementWorkflowConfig&date=${date}&actionableAgreementId=${treasuryAgreementId}`;

  displayBeginWorkflowDialog(workflowParams, wfDataUrl, wfDataParam);
};

export const beginWorkflowAndGenerateCallReport = async (
  date: string,
  legalEntityId: number,
  cpeEntityId: number,
  agreementTypeId: number,
  actionableAgreementId: number,
  treasuryAgreementId: number
) => {
  let workflowParams = getWorkflowParam(date, legalEntityId, cpeEntityId, agreementTypeId);
  try {
    agreementSummaryStore.setIsLoading(true);
    let workflowStatusDetail = await beginWorkflow(workflowParams);
    if (workflowStatusDetail) {
      let apiResponse = await generateCallReport(workflowStatusDetail.workflowId, actionableAgreementId, treasuryAgreementId);
      agreementSummaryStore.setIsLoading(false);
      if (apiResponse.successStatus === true) {
        await showDialog(SUCCESS, MESSAGES.GENERATE_CALL_REPORT_SUCCESS);
        await showReloadAgreementSummaryConfirmationDialog(
          MESSAGES.RELOAD_AGREEMENT_SUMMARY_CONFIRMATION_POST_CALL_REPORT_SUCCESS
        );
      }
      else {
        showToastService(MESSAGES.GENERATE_CALL_REPORT_FAILURE, TOAST_TYPE.CRITICAL);
      }
    } else {
      agreementSummaryStore.setIsLoading(false);
      showToastService(MESSAGES.GENERATE_CALL_REPORT_FAILURE, TOAST_TYPE.CRITICAL);
    }
  } catch (e) {
    agreementSummaryStore.setIsLoading(false);
    showToastService(MESSAGES.GENERATE_CALL_REPORT_FAILURE, TOAST_TYPE.CRITICAL);
  }
};

export const beginWorkflow = async (
  wfParams: string
): Promise<WorkflowStatusDetail | undefined> => {
  try {
    let data = await beginWorkflowApi(wfParams);
    return {
      workflowId: data.resultList[0].workflowId,
      workflowState: data.resultList[0].workflowState,
      subject: data.resultList[0].subject,
      message: data.resultList[0].message,
    };
  } catch (e) {
    showToastService(MESSAGES.INITIATE_WORKFLOW_FAILURE, TOAST_TYPE.CRITICAL);
  }
};

export const displayBeginWorkflowDialog = async (
  wfParams: string,
  wfDataUrl: string,
  wfDataParam: string
): Promise<void> => {
  // Service to be invoked to begin workflow
  try {
    let data = await beginWorkflow(wfParams);

    if (!data) {
      return;
    }

    // Append workflowId after begin workflow
    let workflowId = data.workflowId;
    wfDataParam = wfDataParam + '&workflowId=' + workflowId;
    let workflowState = data.workflowState;
    // Conflict scenario popup subject and message
    let conflictSubject = 'Workflow Conflict - ' + data.subject;
    let conflictMessage = data.message;

    // For new workflow the state will be BEGIN_WORKFLOW,
    // reload the agreement summary screen and open a new
    // child window
    if (workflowState == BEGIN_WORKFLOW) {
      launchWindow(wfDataUrl + '?' + wfDataParam);
      agreementSummaryStore.handleSearch();
    }

    // A workflow already started by the same user
    if (workflowState == RESUME_WORKFLOW) {
      // On Cancel workflow clearing the workflow conflict
      // popup, cancelling the workflow and reloading the
      // summary page

      showResumeWorkflowDialog(
        conflictSubject,
        conflictMessage,
        workflowId,
        wfDataUrl,
        wfDataParam
      );

      // When the workflow is started by a different user
      // and it is IN_PROGRESS for more then 60 mins , it
      // is considered stale.
    } else if (workflowState == FAILED_WORKFLOW) {
      // On Go Back clear and hide the workflow popup

      // On Confirmation Cancel the workflow, start new
      // workflow and reload the agreement summary screen
      showFailedWorkflowDialog(
        conflictSubject,
        conflictMessage,
        workflowId,
        wfParams,
        wfDataUrl,
        wfDataParam
      );

      // Set different properties of workflow conflict
      // properties and reveal it

      // When the workflow is started by different user
      // and its not stale, then no user can cancel the
      // workflow or start a new workflow
    } else if (workflowState == LOCKED_WORKFLOW) {
      // On Go Back click clear and hide the workflow
      showDialog(conflictSubject, conflictMessage);

      // conflict popup
    }
  } catch (e) {
    showToastService(MESSAGES.INITIATE_WORKFLOW_FAILURE, TOAST_TYPE.CRITICAL);
  }
};

export const quickPublishMultipleAgreements = async () => {
  const date = agreementSummaryStore.lastSearchFilter.selectedDate;
  const selectedRowIds = agreementSummaryStore.selectedRowIds;
  agreementSummaryStore.setIsLoading(true);
  const agreementIds: Array<number> = [];

  for (const element of selectedRowIds) {
    const selectedAgreementData = agreementSummaryStore.keyToTrsyAgreementDataMap[element];
    const isTreasuryAgreement = selectedAgreementData?.parentMarginCallTypeEdSummaryDataList;
    if (isTreasuryAgreement) {
      const agreementDetail = getAgreementDetail(selectedAgreementData);
      agreementIds.push(agreementDetail.agreementId);
    }
  }

  try {
    const quickPublishApiResponse: Array<ApiResponse> = await quickPublishMultipleAgreementWorkflows(date, agreementIds)
    agreementSummaryStore.setIsLoading(false);
    const failedAgreementIds: Array<number> = quickPublishApiResponse.filter(e => e.successStatus == false).map(e => e.response);

    if (failedAgreementIds.length > 0) {
      await showDialog(FAILURE, MESSAGES.MULTI_QUICK_PUBLISH_FAILURE + failedAgreementIds.join());
    }

    agreementSummaryStore.handleSearch();
  } catch (e) {
    agreementSummaryStore.setIsLoading(false);
    await showDialog(FAILURE, MESSAGES.MULTI_QUICK_PUBLISH_INITIATION_FAILURE);
  }
}

export const quickWirePublishAgreement = async (
  date: string,
  legalEntityId: number,
  cpeEntityId: number,
  agreementTypeId: number,
  agreementId: number = -1,
  wireAmount: string = '',
  applicableMarginCallType: string = ''
) => {
  let quickPublishParams = getWorkflowParam(date, legalEntityId, cpeEntityId, agreementTypeId);

  try {
    agreementSummaryStore.setIsLoading(true);
    let workflowStatusDetail = await beginWorkflow(quickPublishParams);

    if (workflowStatusDetail) {
      let workflowId = workflowStatusDetail.workflowId;
      await quickPublishWorkflow(workflowId, quickPublishParams);

      try {
        let data = await saveAgreementArtifacts(
          workflowId,
          QUICK_WIRE_PUBLISH,
          agreementId,
          wireAmount,
          applicableMarginCallType
        );
        if (data && data.resultList && data.resultList[0].status == SUCCESS) {
          agreementSummaryStore.setIsLoading(false);
          agreementSummaryStore.handleSearch();
        } else {
          agreementSummaryStore.setIsLoading(false);
          await showDialog(FAILURE, MESSAGES.WORKFLOW_PUBLISH_FAILURE);
          cancelWorkflow(workflowId);
          return false;
        }
      } catch (e) {
        agreementSummaryStore.setIsLoading(false);
        cancelWorkflow(workflowId);
        return false;
      }
    }
  } catch (e) {
    showToastService(MESSAGES.INITIATE_WORKFLOW_FAILURE, TOAST_TYPE.CRITICAL);
    agreementSummaryStore.setIsLoading(false);
    return false;
  }
};

export const quickPublishAgreement = async (
  date: string,
  legalEntityId: number,
  cpeEntityId: number,
  agreementTypeId: number,
  agreementId: number = -1,
) => {
  let quickPublishParams = getWorkflowParam(date, legalEntityId, cpeEntityId, agreementTypeId);

  try {
    agreementSummaryStore.setIsLoading(true);
    let workflowStatusDetail = await beginWorkflow(quickPublishParams);

    if (workflowStatusDetail) {
      let workflowId = workflowStatusDetail.workflowId;
      await quickPublishWorkflow(workflowId, quickPublishParams);

      try {
        let data = await quickPublishAgreementWorkflow(
          workflowId,
          agreementId
        );
        if (data && data.resultList && data.resultList[0].status == SUCCESS) {
          agreementSummaryStore.setIsLoading(false);
          agreementSummaryStore.handleSearch();
        } else {
          agreementSummaryStore.setIsLoading(false);
          await showDialog(FAILURE, MESSAGES.WORKFLOW_PUBLISH_FAILURE);
          cancelWorkflow(workflowId);
          return false;
        }
      } catch (e) {
        agreementSummaryStore.setIsLoading(false);
        cancelWorkflow(workflowId);
        return false;
      }
    }
  } catch (e) {
    showToastService(MESSAGES.INITIATE_WORKFLOW_FAILURE, TOAST_TYPE.CRITICAL);
    agreementSummaryStore.setIsLoading(false);
    return false;
  }
};

export const getWorkflowParam = (
  date: string,
  legalEntityId: number,
  cpeEntityId: number,
  agreementTypeId: number
) => {
  let workflowUnit = 'AgreementDate';
  let workflowType = 'EXCESS_DEFICIT';
  let workflowConfig = 'yakExpReqTabConfig';
  let records =
    workflowUnit +
    '_' +
    workflowType +
    '_' +
    legalEntityId +
    '_' +
    cpeEntityId +
    '_' +
    agreementTypeId +
    '_' +
    date;

  records = records.replace(/-/g, '');

  let workflowParam = 'workflowParam.dateStr=' + date;
  workflowParam += '&workflowParam.descoEntityId=' + legalEntityId;
  workflowParam += '&workflowParam.counterpartyEntityId=' + cpeEntityId;
  workflowParam += '&workflowParam.agreementTypeId=' + agreementTypeId;
  workflowParam += '&workflowParam.workflowUnit=' + workflowUnit;
  workflowParam += '&workflowParam.workflowTypeName=' + workflowType;
  workflowParam += '&configName=' + workflowConfig;
  workflowParam += '&records=' + records;
  return workflowParam;
};
