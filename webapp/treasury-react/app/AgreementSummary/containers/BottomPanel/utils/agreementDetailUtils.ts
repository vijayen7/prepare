import { ALL_ID, BASE_URL } from '../../../../commons/constants';
import {
  AdjustedIntVsInt,
  ExcessDeficitData,
  ExternalT1VsT2,
  InternalT1VsT2,
  InternalVsExternalData,
} from '../../../../Lcm/AgreementWorkflow/models';
import { getApplicableExcessDeficit, getValueOrDefault } from '../../../utils';
import { fetchURL } from "../../../../commons/util";

export const getTreasuryAndChildAgreementDetails = (agreementData) => {
  let agreementDetail = agreementData.edSummaryDataKey?.agreementDetail;
  if (!agreementDetail) {
    agreementDetail = agreementData.treasuryAgreementDetail;
  }

  let legalEntityIds = new Set([agreementDetail.legalEntityId]);
  let cpeIds = new Set([agreementDetail.cpeId]);
  let agreementTypeIds = new Set([agreementDetail.agreementTypeId]);

  agreementData.childMarginCallTypeEdSummaryDataList?.forEach((childAgreementData) => {
    legalEntityIds.add(childAgreementData.edSummaryDataKey.agreementDetail.legalEntityId);
    cpeIds.add(childAgreementData.edSummaryDataKey.agreementDetail.cpeId);
    agreementTypeIds.add(childAgreementData.edSummaryDataKey.agreementDetail.agreementTypeId);
  });

  return {
    legalEntityIds: [...legalEntityIds].join(),
    cpeIds: [...cpeIds].join(),
    agreementTypeIds: [...agreementTypeIds].join(),
    isPublished: agreementData?.workflow?.workflowStatus?.name==="PUBLISHED"
  };
};

export const getPositionDetailReportLink = (
  date: string,
  agreementSummaryData,
  isCrimsonEnabled: boolean,
  selectedCurrencyKey: number,
  redirectToOldPdrScreen: boolean
) => {
  let positionDetailReportParam = createPositionDetailReportParam(
    date,
    getTreasuryAndChildAgreementDetails(agreementSummaryData),
    isCrimsonEnabled,
    selectedCurrencyKey,
    redirectToOldPdrScreen
  );

  if (!redirectToOldPdrScreen) {
    return `${BASE_URL}comet/positiondetailreport.html?${positionDetailReportParam}`;
  } else {
    return `${BASE_URL}comet/display-position-detail?${positionDetailReportParam}`;
  }
};

const createPositionDetailReportParam = (
  date: string,
  agreementDetails: any,
  isCrimsonEnabled: boolean,
  selectedCurrencyKey: number,
  redirectToOldPdrScreen: boolean
) => {
  let positionDetailParam;

  if (!redirectToOldPdrScreen) {
    positionDetailParam = 'legalEntityIds=' + agreementDetails.legalEntityIds;
    positionDetailParam += '&cpeIds=' + agreementDetails.cpeIds;
    positionDetailParam += '&agreementTypeIds=' + agreementDetails.agreementTypeIds;
    positionDetailParam += '&startDate=' + date;
    positionDetailParam += '&endDate=' + date;
    positionDetailParam += '&custodianAccountIds=';
    positionDetailParam += '&liveOnly=' + true;
    positionDetailParam += '&includeUnmappedBrokerRecord=' + true;
    positionDetailParam += '&isCrimsonEnabled=' + isCrimsonEnabled;
    positionDetailParam += '&dataSet=' + (isCrimsonEnabled ? 3 : 2);
    positionDetailParam += '&reportingCurrencyKey=' + selectedCurrencyKey;
    positionDetailParam += '&includeExtendedSecurityAttributes=' + false;
    positionDetailParam +=  '&isPublished=' + agreementDetails.isPublished;
    positionDetailParam += '&isPositionLevel=' + true;
  } else {
    positionDetailParam = 'dateString=' + date;
    positionDetailParam += '&includeLiveAgreements=' + true;
    positionDetailParam += '&loadPositionDetail=' + true;
    positionDetailParam += '&entityFamilyIds=' + -1;
    positionDetailParam += '&includeUnmappedBrokerRecords=' + true;
    positionDetailParam += '&bookIds=';
    positionDetailParam += '&gboTypeIds=';
    positionDetailParam += '&cpeIds=' + agreementDetails.cpeIds;
    positionDetailParam += '&legalEntityIds=' + agreementDetails.legalEntityIds;
    positionDetailParam += '&agreementTypeIds=' + agreementDetails.agreementTypeIds;
    positionDetailParam += '&isCopyUrl=true';
    positionDetailParam += '&isCrimsonEnabled=' + isCrimsonEnabled;
  }
  return positionDetailParam;
};

export const getViewAdjustmentsLink = (date: string, agreementSummaryData) => {
  let adjustmentParams = createAdjustmentScreenParam(
    date,
    getTreasuryAndChildAgreementDetails(agreementSummaryData)
  );
  return `${BASE_URL}lcm/Adjustments.html?${adjustmentParams}`;
};

const createAdjustmentScreenParam = (date: string, agreementDetails: any) => {
  let positionDetailParam = 'tradingEntityIds=' + agreementDetails.legalEntityIds;
  positionDetailParam += '&cpeIds=' + agreementDetails.cpeIds;
  positionDetailParam += '&agreementTypeIds=' + agreementDetails.agreementTypeIds;
  positionDetailParam += '&dateString=' + date;
  return positionDetailParam;
};

export const getBrokerDataViewUrl = (date: string, agreementSummaryData) => {
  let agreementDetails = getTreasuryAndChildAgreementDetails(agreementSummaryData);
  return `${BASE_URL}comet/brokerdata.html?legalEntityIds=${agreementDetails.legalEntityIds}&cpeIds=${agreementDetails.cpeIds}&agmtTypeIds=${agreementDetails.agreementTypeIds}&dateString=${date}`;
};

export const getCollateralTermSearchUrl = (date: string, agreementSummaryData) => {
  let agreementDetails = getTreasuryAndChildAgreementDetails(agreementSummaryData);
  return `${BASE_URL}comet/collateralTerms.html?legalEntityIds=${agreementDetails.legalEntityIds}&cpeIds=${agreementDetails.cpeIds}&agreementTypeIds=${agreementDetails.agreementTypeIds}`;
};

export const getRagRulesSearchUrl = (date: string, agreementSummaryData) => {
  let agreementDetails = getTreasuryAndChildAgreementDetails(agreementSummaryData);
  return `${BASE_URL}comet/ragrules.html?legalEntityIds=${agreementDetails.legalEntityIds}&cpeIds=${agreementDetails.cpeIds}&agreementTypeIds=${agreementDetails.agreementTypeIds}`;
};

export function getAgreementDetail(selectedAgreementData: any) {
  let agreementDetail = selectedAgreementData?.applicableEdSummaryDataKey?.agreementDetail;
  if (!agreementDetail) {
    agreementDetail = selectedAgreementData?.edSummaryDataKey?.agreementDetail;
  }
  return agreementDetail;
}

export function getMarginCallType(selectedAgreementData: any) {
  let marginCallType = selectedAgreementData?.applicableEdSummaryDataKey?.marginCallType;
  if (!marginCallType) {
    marginCallType = selectedAgreementData?.edSummaryDataKey?.marginCallType;
  }
  return marginCallType;
}


export const getApplicableEdSummaryData = (agreementData) => {
  if (agreementData.parentMarginCallTypeEdSummaryDataList) {
    return agreementData.parentMarginCallTypeEdSummaryDataList.find(
      (e) =>
        JSON.stringify(e.edSummaryDataKey) ===
        JSON.stringify(agreementData.applicableEdSummaryDataKey)
    );
  } else {
    return agreementData;
  }
};

export const getSegEdSummaryData = (agreementData) => {
  return getMarginCallTypeEdSummaryData('SEG', agreementData.parentMarginCallTypeEdSummaryDataList);
};

export function getAdjustedIntVsIntData(currentDayEdSummaryData: any): AdjustedIntVsInt {
  return {
    internalAdjustedData: getValueOrDefault(currentDayEdSummaryData?.adjustedEdSummaryDataDetail),
    internalUnadjustedData: getValueOrDefault(currentDayEdSummaryData?.internalEdSummaryDataDetail),
    internalAdjustedDiffData: getDiffEdData(
      currentDayEdSummaryData?.adjustedEdSummaryDataDetail,
      currentDayEdSummaryData?.internalEdSummaryDataDetail
    ),
  };
}

export function getExternalT1VsT2Data(
  currentDayEdSummaryData: any,
  prevDayEdSummaryData: any
): ExternalT1VsT2 {
  return {
    todayExternalT1VsT2Data: getValueOrDefault(
      currentDayEdSummaryData?.externalEdSummaryDataDetail
    ),
    yesterdayExternalT1VsT2Data: getValueOrDefault(
      prevDayEdSummaryData?.externalEdSummaryDataDetail
    ),
    diffExternalT1VsT2Data: getDiffEdData(
      currentDayEdSummaryData?.externalEdSummaryDataDetail,
      prevDayEdSummaryData?.externalEdSummaryDataDetail
    ),
  };
}

export function getInternalT1VsT2Data(
  currentDayEdSummaryData: any,
  prevDayEdSummaryData: any
): InternalT1VsT2 {
  return {
    todayInternalT1VsT2Data: getValueOrDefault(
      currentDayEdSummaryData?.adjustedEdSummaryDataDetail
    ),
    yesterdayInternalT1VsT2Data: getValueOrDefault(
      prevDayEdSummaryData?.adjustedEdSummaryDataDetail
    ),
    diffInternalT1VsT2Data: getDiffEdData(
      currentDayEdSummaryData?.adjustedEdSummaryDataDetail,
      prevDayEdSummaryData?.adjustedEdSummaryDataDetail
    ),
  };
}

export function getInternalVsExternalData(currentDayEdSummaryData: any): InternalVsExternalData {
  return {
    internalEDData: getValueOrDefault(currentDayEdSummaryData?.adjustedEdSummaryDataDetail),
    externalEDData: getValueOrDefault(currentDayEdSummaryData?.externalEdSummaryDataDetail),
    diffEDData: getDiffEdData(
      currentDayEdSummaryData?.adjustedEdSummaryDataDetail,
      currentDayEdSummaryData?.externalEdSummaryDataDetail
    ),
  };
}

export function getInternalVsExternalDataForMarginCallType(
  marginCallType: string,
  parentMarginCallTypeEdSummaryDataList: Array<any>
): InternalVsExternalData | undefined {
  let currentDayEdSummaryData = getMarginCallTypeEdSummaryData(
    marginCallType,
    parentMarginCallTypeEdSummaryDataList
  )?.curDayEdSummaryData;
  return currentDayEdSummaryData ? getInternalVsExternalData(currentDayEdSummaryData) : undefined;
}

export const getMarginCallTypeEdSummaryData = (
  marginCallType: string,
  parentMarginCallTypeEdSummaryDataList: Array<any>
) => {
  return parentMarginCallTypeEdSummaryDataList?.find(
    (e) => e.edSummaryDataKey.marginCallType.abbrev === marginCallType
  );
};

export const getDiffEdData = (
  edDataA?: ExcessDeficitData,
  edDataB?: ExcessDeficitData
): ExcessDeficitData => ({
  exposure: getDiffFormating(edDataA?.exposure, edDataB?.exposure),
  requirement: getDiffFormating(edDataA?.requirement, edDataB?.requirement),
  cashCollateral: getDiffFormating(edDataA?.cashCollateral, edDataB?.cashCollateral),
  securitiesCollateral: getDiffFormating(edDataA?.securitiesCollateral, edDataB?.securitiesCollateral),
  excessDeficit: getDiffFormating(edDataA?.excessDeficit, edDataB?.excessDeficit),
  applicableRequirement: getDiffFormating(edDataA?.applicableRequirement, edDataB?.applicableRequirement),
  applicableExcessDeficit: getDiffFormating(getApplicableExcessDeficit(edDataA), getApplicableExcessDeficit(edDataB))
});

const getValidNumberOrZero = (value) => {
  return getValidNumberOrDefault(value, 0);
};

const getValidNumberOrDefault = (value, defaultValue) => {
  if (!isNaN(value)) {
    return Number(value);
  }
  return defaultValue;
};

const getDiffFormating = (valA, valB): any => {
  let validA: boolean = valA !== undefined && !isNaN(valA);
  let validB: boolean = valB !== undefined && !isNaN(valB);

  if (!validA || !validB) {
    return "n/a";
  }

  return getValidNumberOrZero(valA) - getValidNumberOrZero(valB);
}
