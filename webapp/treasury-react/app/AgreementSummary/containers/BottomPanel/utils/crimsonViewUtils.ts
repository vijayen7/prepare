import {
  AGREEMENT_TYPE_TABLES, BREAK,
  CASH_COLLATERAL, COLLATERAL, COMPONENT, CPE_MISS, CPE_MISSING, CRIMSON_TABLES, DIFF, EXCESS_DEFICIT,
  EXPOSURE, INTERNAL, INTERNAL_MISS, INTERNAL_MISSING, LMV_DIFF, MARGIN_FILE, MATCH, NO_OF_RECORDS,
  POS_REC, REPO_REC, PREV_DAY_ADJ, REQUIREMENT, SECURITIES_COLLATERAL, SEG_CASH_COLLATERAL, SEG_ED, SEG_REQUIREMENT,
  SEG_SECURITIES_COLLATERAL, SMV_DIFF, SOURCE, TOTAL, NOT_AVAILABLE, BASE_URL
} from "../../../../commons/constants";
import { AGREEMENT_TYPES_WITH_STATIC_CRIMSON_VIEW, DIFF_ROW_INDEX, DIFF_ROW_NAMES, EXCESS_DEFICIT_COLUMN_INDEX, MESSAGES } from '../constants'
import { plainNumberFormatter } from "../../../../commons/grid/formatters";
import { TableDiff } from "../models";
import { fetchReconData as fetchReconDataApi } from "../api";
import { getMarginCallTypes, getValueOrDefault, launchWindow, showToastService } from "../../../utils";
import { TOAST_TYPE } from "../../../constants";
import { getApplicableEdSummaryData, getSegEdSummaryData } from "./agreementDetailUtils";

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

export const isCrimsonEnabledForAgreementType = (agreementTypeId: number) => {
  let isCrimsonEnabled = CODEX_PROPERTIES["treasury.lcm.isCrimsonEnabled"]
  return isCrimsonEnabled && isCrimsonEnabled.toString() === 'true' && isCrimsonEnabledForAgmtTypeId(agreementTypeId)
}

function isCrimsonEnabledForAgmtTypeId(agreementTypeId: number) {
  let crimsonEnabledAgmtTypeIds = CODEX_PROPERTIES['treasury.lcm.crimsonEnabledAgmtTypeIds'];
  if (typeof crimsonEnabledAgmtTypeIds === 'string')
    crimsonEnabledAgmtTypeIds = crimsonEnabledAgmtTypeIds.split(',').map(e => Number(e));
  return crimsonEnabledAgmtTypeIds?.includes(agreementTypeId)
}

export const getCollateralMgmtVsReconTabData = async (date: string, agreementData) => {
  let agreementDetail = agreementData.applicableEdSummaryDataKey.agreementDetail
  let agreementType = agreementDetail.agreementTypeName;
  let tag = getMarginCallTypes(agreementData.parentMarginCallTypeEdSummaryDataList);
  let crimsonTables = AGREEMENT_TYPE_TABLES[agreementType];
  let applicableAgreementData = getApplicableEdSummaryData(agreementData)

  let internalVsMarginFileData = getInternalVsMarginFileData(applicableAgreementData.curDayEdSummaryData, crimsonTables);
  let posRecVsMarginFileData = getPosRecVsMarginFileData(applicableAgreementData.curDayEdSummaryData, crimsonTables);
  let internalVsPosRecData = getInternalVsPosRecData(applicableAgreementData.curDayEdSummaryData, crimsonTables);
  let internalVsRepoRecData = getInternalVsRepoRecData(applicableAgreementData.curDayEdSummaryData, crimsonTables);
  let reconStatusSummaryData = await getReconStatusSummaryData(date, agreementDetail.agreementId, crimsonTables);
  let dayOnDayChangesData = getDayOnDayChanges(applicableAgreementData, crimsonTables);
  let internalVsMarginFileSegData = getInternalVsMarginFileSegData(getSegEdSummaryData(agreementData)?.curDayEdSummaryData, crimsonTables);

  let crimsonDiffData = {
    internalVsMarginFileData: internalVsMarginFileData,
    posRecVsMarginFileData: posRecVsMarginFileData,
    internalVsPosRecData: internalVsPosRecData,
    internalVsRepoRecData: internalVsRepoRecData,
    reconStatusSummaryData: reconStatusSummaryData,
    dayOnDayChangesData: dayOnDayChangesData,
    internalVsMarginFileSegData: internalVsMarginFileSegData,
    agreementType: agreementType,
    tag: tag
  }

  return { ...crimsonDiffData, todaySummaryDetail: getTodaysSummaryDetails(crimsonDiffData) }
}

export const showCrimsonDiffDrilldown = (date: string, agreementSummaryData: any, fromExcessDeficitDataSource: string, toExcessDeficitDataSource: string,
  excessDeficitDataType: string, showSegDrilldown: boolean) => {
  let crimsonDiffDrilldownParam = createCrimsonDiffDrilldownParam(date, agreementSummaryData,
    fromExcessDeficitDataSource, toExcessDeficitDataSource, excessDeficitDataType, showSegDrilldown);

  let url = `${BASE_URL}comet/positiondetailreport.html?${crimsonDiffDrilldownParam}`;
  launchWindow(url)
}

const createCrimsonDiffDrilldownParam = (date: string, agreementSummaryData: any, fromExcessDeficitDataSource: string,
  toExcessDeficitDataSource: string, excessDeficitDataType: string, showSegDrilldown: boolean) => {
  let positionDetailParam = 'legalEntityIds=' + agreementSummaryData.legalEntityIds;
  positionDetailParam += '&cpeIds=' + agreementSummaryData.cpeIds;
  positionDetailParam += '&agreementTypeIds=' + agreementSummaryData.agreementTypeIds;
  positionDetailParam += '&startDate=' + date;
  positionDetailParam += '&endDate=' + date;
  positionDetailParam += '&custodianAccountIds=';
  positionDetailParam += '&liveOnly=' + false;
  positionDetailParam += '&includeUnmappedBrokerRecords=' + true;
  positionDetailParam += '&fromExcessDeficitDataSource=' + fromExcessDeficitDataSource;
  positionDetailParam += '&toExcessDeficitDataSource=' + toExcessDeficitDataSource;
  positionDetailParam += '&excessDeficitDataType=' + excessDeficitDataType;
  positionDetailParam += '&showSegDrilldown=' + showSegDrilldown;
  positionDetailParam += '&isCrimsonEnabled=' + true;
  positionDetailParam += '&dataSet=' + 3;
  positionDetailParam += '&reportingCurrencyKey=' + 1;
  positionDetailParam += '&includeExtendedSecurityAttributes=' + false;
  positionDetailParam += '&isPublished=' + true;
  positionDetailParam += '&isPositionLevel=' + true;
  return positionDetailParam;
}

function getTodaysSummaryDetails(crimsonDiffData: any) {
  if (AGREEMENT_TYPES_WITH_STATIC_CRIMSON_VIEW.includes(crimsonDiffData.agreementType)) {
    return getStaticTodaysSummaryView(crimsonDiffData);
  } else {
    return computeTopThreeDiffs(crimsonDiffData);
  }
}

function getStaticTodaysSummaryView(crimsonDiffData: any) {

  let excessDeficitDiff = getExcessDeficitDiff(crimsonDiffData);

  let internalVsPosRecExposureDiffRow = getDiffRowForHover(crimsonDiffData.internalVsPosRecData.unformattedDiffDataRow, EXPOSURE, CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA);
  let internalVsRepoRecExposureDiffRow = getDiffRowForHover(crimsonDiffData.internalVsRepoRecData.unformattedDiffDataRow, EXPOSURE, CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA);
  let posRecVsMarginFileRequirementDiff = getDiffRowForHover(crimsonDiffData.posRecVsMarginFileData.unformattedDiffDataRow, REQUIREMENT, CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA);
  let posRecVsMarginFileExposureDiff = getDiffRowForHover(crimsonDiffData.posRecVsMarginFileData.unformattedDiffDataRow, EXPOSURE, CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA);

  let diffList = [...internalVsPosRecExposureDiffRow, ...internalVsRepoRecExposureDiffRow, ...posRecVsMarginFileRequirementDiff, ...posRecVsMarginFileExposureDiff];

  let tableRows = getTodaySummaryDiffTableRows(diffList);

  return {
    excessDeficitDiff: excessDeficitDiff,
    tableRows: tableRows
  };
}

function getExcessDeficitDiff(crimsonDiffData) {
  return crimsonDiffData.internalVsMarginFileData.tableRows[DIFF_ROW_INDEX][EXCESS_DEFICIT_COLUMN_INDEX];
}

function getTodaySummaryDiffTableRows(diffList) {
  let tableRows: Array<Array<string>> = new Array;

  for (let i = 1; i <= Math.min(diffList.length, 3); i++) {
    diffList[i - 1].value = validateAndFormatNumber(diffList[i - 1].value);
    tableRows.push([`${i})  `, diffList[i - 1], DIFF_ROW_NAMES[diffList[i - 1].hoverOnTable][diffList[i - 1].hoverOnColumn]]);
  }

  return tableRows;
}

const getCrimsonSourceDataRow = (columns, edSummaryDataDetail) => {
  return columns.map(column => validateAndFormatNumber(getValueOrDefault(edSummaryDataDetail)[column]))
}

const getCrimsonDiffRow = (columns, edSummaryDataDetailA, edSummaryDataDetailB, formatResult) => {
  return columns.map(column => validateAndComputeDiff(getValueOrDefault(edSummaryDataDetailA)[column], getValueOrDefault(edSummaryDataDetailB)[column], formatResult))
}

function getInternalVsMarginFileData(agreementSummary, tables: Array<string>) {
  if (!tables.includes(CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA)) {
    return {};
  }
  const internalVsMarginFileTableColumns = ["exposure", "requirement", "cashCollateral", "securitiesCollateral", "excessDeficit"]

  let internalDataRow = [
    INTERNAL,
    ...getCrimsonSourceDataRow(internalVsMarginFileTableColumns, agreementSummary.adjustedEdSummaryDataDetail)
  ];

  let externalDataRow = [
    MARGIN_FILE,
    ...getCrimsonSourceDataRow(internalVsMarginFileTableColumns, agreementSummary.externalEdSummaryDataDetail)
  ]

  let diffDataRow = [
    DIFF,
    ...getCrimsonDiffRow(internalVsMarginFileTableColumns, agreementSummary.adjustedEdSummaryDataDetail, agreementSummary.externalEdSummaryDataDetail, true)
  ]

  let tableRows = [internalDataRow, externalDataRow, diffDataRow];

  let unformattedDiffDataRow = new TableDiff(
    ...getCrimsonDiffRow(internalVsMarginFileTableColumns.slice(0, 3), agreementSummary.adjustedEdSummaryDataDetail, agreementSummary.externalEdSummaryDataDetail, false)
  );

  return {
    tableName: CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA,
    numberOfColumns: 6,
    headerRow: [SOURCE, EXPOSURE, REQUIREMENT, CASH_COLLATERAL, SECURITIES_COLLATERAL, EXCESS_DEFICIT],
    tableRows: tableRows,
    unformattedDiffDataRow: unformattedDiffDataRow
  };
}

function getPosRecVsMarginFileData(agreementSummary, tables: Array<string>) {
  if (!tables.includes(CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA)
    && !tables.includes(CRIMSON_TABLES.BOTTOM_POS_REC_VS_MARGIN_FILE_DATA)) {
    return {};
  }

  const posRecVsMarginTableColumns = ["exposure", "requirement"]

  let externalReconDataRow = [
    POS_REC,
    ...getCrimsonSourceDataRow(posRecVsMarginTableColumns, agreementSummary.reconEdSummaryDataDetail)
  ]

  let externalLCMDataRow = [
    MARGIN_FILE,
    ...getCrimsonSourceDataRow(posRecVsMarginTableColumns, agreementSummary.externalEdSummaryDataDetail)
  ]

  let diffDataRow = [
    DIFF,
    ...getCrimsonDiffRow(posRecVsMarginTableColumns, agreementSummary.reconEdSummaryDataDetail, agreementSummary.externalEdSummaryDataDetail, true)
  ]

  let unformattedDiffDataRow = new TableDiff(
    ...getCrimsonDiffRow(posRecVsMarginTableColumns, agreementSummary.reconEdSummaryDataDetail, agreementSummary.externalEdSummaryDataDetail, false)
  )

  return {
    tableName: CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA,
    numberOfColumns: 3,
    headerRow: [SOURCE, EXPOSURE, REQUIREMENT],
    tableRows: [externalReconDataRow, externalLCMDataRow, diffDataRow],
    unformattedDiffDataRow: unformattedDiffDataRow
  };
}

function getInternalVsPosRecData(agreementSummary, tables: Array<string>) {
  if (!tables.includes(CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA)) {
    return {};
  }

  const internalVsPosRecTableColumns = ["exposure", "requirement"]

  let internalDataRow = [
    INTERNAL,
    ...getCrimsonSourceDataRow(internalVsPosRecTableColumns, agreementSummary.adjustedEdSummaryDataDetail)
  ]

  let externalReconDataRow = [
    POS_REC,
    ...getCrimsonSourceDataRow(internalVsPosRecTableColumns, agreementSummary.reconEdSummaryDataDetail)
  ]

  let diffDataRow = [
    DIFF,
    ...getCrimsonDiffRow(internalVsPosRecTableColumns, agreementSummary.adjustedEdSummaryDataDetail, agreementSummary.reconEdSummaryDataDetail, true)
  ]

  let unformattedDiffDataRow = new TableDiff(
    ...getCrimsonDiffRow(internalVsPosRecTableColumns, agreementSummary.adjustedEdSummaryDataDetail, agreementSummary.reconEdSummaryDataDetail, false)
  );

  return {
    tableName: CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA,
    numberOfColumns: 3,
    headerRow: [SOURCE, EXPOSURE, REQUIREMENT],
    tableRows: [internalDataRow, externalReconDataRow, diffDataRow],
    unformattedDiffDataRow: unformattedDiffDataRow
  };
}

function getInternalVsRepoRecData(agreementSummary, tables: Array<string>) {
  if (!tables.includes(CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA)) {
    return {};
  }

  const internalVsRepoRecTableColumns = ["exposure", "requirement"]

  let internalDataRow = [
    INTERNAL,
    ...getCrimsonSourceDataRow(internalVsRepoRecTableColumns, agreementSummary.adjustedEdSummaryDataDetail)
  ]

  let externalReconDataRow = [
    REPO_REC,
    ...getCrimsonSourceDataRow(internalVsRepoRecTableColumns, agreementSummary.reconEdSummaryDataDetail)
  ]

  let diffDataRow = [
    DIFF,
    ...getCrimsonDiffRow(internalVsRepoRecTableColumns, agreementSummary.adjustedEdSummaryDataDetail, agreementSummary.reconEdSummaryDataDetail, true)
  ]

  let unformattedDiffDataRow = new TableDiff(
    ...getCrimsonDiffRow(internalVsRepoRecTableColumns, agreementSummary.adjustedEdSummaryDataDetail, agreementSummary.reconEdSummaryDataDetail, false)
  );

  return {
    tableName: CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA,
    numberOfColumns: 3,
    headerRow: [SOURCE, EXPOSURE, REQUIREMENT],
    tableRows: [internalDataRow, externalReconDataRow, diffDataRow],
    unformattedDiffDataRow: unformattedDiffDataRow
  };
}

const getReconStatusSummaryData = async (date: string, agreementId: number, tables: Array<string>) => {
  if (!tables.includes(CRIMSON_TABLES.RECON_STATUS_SUMMARY)) {
    return {};
  }

  try {
    let data = await fetchReconDataApi(date, agreementId)
    return createReconStatusDataFromResponse(data)
  } catch (e) {
    showToastService(MESSAGES.RECON_DATA_FETCH_FAILURE, TOAST_TYPE.CRITICAL)
  }
}

function createReconStatusDataFromResponse(reconData) {

  let lmvMap = new Map();
  let smvMap = new Map();
  let noOfRecordsMap = new Map();

  for (let i in reconData) {
    lmvMap.set(reconData[i].recStatus, reconData[i].lmvDiff);
    smvMap.set(reconData[i].recStatus, reconData[i].smvDiff);
    noOfRecordsMap.set(reconData[i].recStatus, reconData[i].dataCount);
  }

  let lmvDiffRow = [
    LMV_DIFF,
    validateAndFormatRecStatusNumber(lmvMap.get(BREAK)),
    validateAndFormatRecStatusNumber(lmvMap.get(INTERNAL_MISSING)),
    validateAndFormatRecStatusNumber(lmvMap.get(CPE_MISSING)),
    validateAndFormatRecStatusNumber(lmvMap.get(MATCH))
  ];

  let smvDiffRow = [
    SMV_DIFF,
    validateAndFormatRecStatusNumber(smvMap.get(BREAK)),
    validateAndFormatRecStatusNumber(smvMap.get(INTERNAL_MISSING)),
    validateAndFormatRecStatusNumber(smvMap.get(CPE_MISSING)),
    validateAndFormatRecStatusNumber(smvMap.get(MATCH))
  ];

  let noOfRecordsRow = [
    NO_OF_RECORDS,
    validateAndFormatRecStatusNumber(noOfRecordsMap.get(BREAK)),
    validateAndFormatRecStatusNumber(noOfRecordsMap.get(INTERNAL_MISSING)),
    validateAndFormatRecStatusNumber(noOfRecordsMap.get(CPE_MISSING)),
    validateAndFormatRecStatusNumber(noOfRecordsMap.get(MATCH))
  ];

  let lmvDiffSum = validateAndAdd(lmvMap.get(BREAK), lmvMap.get(INTERNAL_MISSING), lmvMap.get(CPE_MISSING), lmvMap.get(MATCH));
  lmvDiffRow.push(validateAndFormatRecStatusNumber(lmvDiffSum));

  let smvDiffSum = validateAndAdd(smvMap.get(BREAK), smvMap.get(INTERNAL_MISSING), smvMap.get(CPE_MISSING), smvMap.get(MATCH));
  smvDiffRow.push(validateAndFormatRecStatusNumber(smvDiffSum));

  let recordCountSum: number | string = 0;
  recordCountSum = validateAndAdd(noOfRecordsMap.get(BREAK), noOfRecordsMap.get(INTERNAL_MISSING), noOfRecordsMap.get(CPE_MISSING), noOfRecordsMap.get(MATCH));
  noOfRecordsRow.push(validateAndFormatRecStatusNumber(recordCountSum));

  return {
    numberOfColumns: 6,
    tableName: CRIMSON_TABLES.RECON_STATUS_SUMMARY,
    headerRow: [COMPONENT, BREAK, INTERNAL_MISS, CPE_MISS, MATCH, TOTAL],
    tableRows: [lmvDiffRow, smvDiffRow, noOfRecordsRow]

  };
}

function getDayOnDayChanges(agreementSummary, tables: Array<string>) {
  if (!tables.includes(CRIMSON_TABLES.DAY_ON_DAY_CHANGES)) {
    return {};
  }

  let exposureDataRow = [
    EXPOSURE,
    ...getDayOnDayDiffRow(EXPOSURE, agreementSummary)
  ]

  let requirementDataRow = [
    REQUIREMENT,
    ...getDayOnDayDiffRow(REQUIREMENT, agreementSummary)
  ]

  let collateralDataRow = [
    COLLATERAL,
    ...getDayOnDayDiffRow(COLLATERAL, agreementSummary)
  ]

  return {
    tableName: CRIMSON_TABLES.DAY_ON_DAY_CHANGES,
    numberOfColumns: 4,
    headerRow: [COMPONENT, INTERNAL, MARGIN_FILE, PREV_DAY_ADJ],
    tableRows: [exposureDataRow, requirementDataRow, collateralDataRow]
  };
}

const getDayOnDayDiffRow = (column: string, agreementSummary) => {
  column = column.toLowerCase()
  return [...getCrimsonDiffRow([column],
    agreementSummary.curDayEdSummaryData?.adjustedEdSummaryDataDetail,
    agreementSummary.prevDayEdSummaryData?.adjustedEdSummaryDataDetail,
    true),
  ...getCrimsonDiffRow([column],
    agreementSummary.curDayEdSummaryData?.externalEdSummaryDataDetail,
    agreementSummary.prevDayEdSummaryData?.externalEdSummaryDataDetail,
    true),
  ...getCrimsonDiffRow([column],
    agreementSummary.prevDayEdSummaryData?.adjustedEdSummaryDataDetail,
    agreementSummary.prevDayEdSummaryData?.internalEdSummaryDataDetail,
    true)]
}

function getInternalVsMarginFileSegData(agreementSummary, tables: Array<string>) {
  if (!tables.includes(CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG)) {
    return {};
  }

  const internalVsMarginFileSegTableColumns = ["requirement", "cashCollateral", "securitiesCollateral", "excessDeficit"]

  let internalSegDataRow = [
    INTERNAL,
    ...getCrimsonSourceDataRow(internalVsMarginFileSegTableColumns, agreementSummary?.adjustedEdSummaryDataDetail)
  ];

  let externalSegDataRow = [
    MARGIN_FILE,
    ...getCrimsonSourceDataRow(internalVsMarginFileSegTableColumns, agreementSummary?.externalEdSummaryDataDetail)
  ];

  let segDiffDataRow = [
    DIFF,
    ...getCrimsonDiffRow(internalVsMarginFileSegTableColumns, agreementSummary?.adjustedEdSummaryDataDetail, agreementSummary?.externalEdSummaryDataDetail, true)
  ];

  let unformattedDiffDataRow = getCrimsonDiffRow(
    internalVsMarginFileSegTableColumns.slice(0, 2),
    agreementSummary?.adjustedEdSummaryDataDetail,
    agreementSummary?.externalEdSummaryDataDetail, false)

  return {
    tableName: CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG,
    numberOfColumns: 5,
    headerRow: [SOURCE, SEG_REQUIREMENT, SEG_CASH_COLLATERAL, SEG_SECURITIES_COLLATERAL, SEG_ED],
    tableRows: [internalSegDataRow, externalSegDataRow, segDiffDataRow],
    unformattedDiffDataRow: unformattedDiffDataRow,
  };
}

function getDiffRowForHover(unformattedDiffDataRow: any, excessDeficitDataType: string, tableName: string) {
  let diffRow: Array<any> = [];
  if (unformattedDiffDataRow) {
    diffRow.push(createObjectForHover(unformattedDiffDataRow[excessDeficitDataType.toLowerCase()],
      excessDeficitDataType, tableName));
  }
  return diffRow;
}

function getAllDiffsForTableData(tableData, tableName) {
  if (tableData != null && tableData.unformattedDiffDataRow !== undefined) {
    let diffData = tableData.unformattedDiffDataRow;
    return createHoverRowsForData(diffData, tableName);
  }
  return [];
}

function computeTopThreeDiffs(crimsonDiffData) {
  let diffList: Array<any> = [];
  diffList = diffList.concat(getAllDiffsForTableData(crimsonDiffData.internalVsMarginFileData, CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA));
  diffList = diffList.concat(getAllDiffsForTableData(crimsonDiffData.posRecVsMarginFileData, CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA));
  diffList = diffList.concat(getAllDiffsForTableData(crimsonDiffData.internalVsPosRecData, CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA));
  diffList = diffList.concat(getAllDiffsForTableData(crimsonDiffData.internalVsRepoRecData, CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA));
  let segDiffData = crimsonDiffData.internalVsMarginFileSegData.unformattedDiffDataRow
  if (segDiffData !== undefined) {
    diffList = diffList.concat([
      createObjectForHover(segDiffData[0], SEG_REQUIREMENT, CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG),
      createObjectForHover(segDiffData[1], SEG_CASH_COLLATERAL, CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG),
      createObjectForHover(segDiffData[2], SEG_SECURITIES_COLLATERAL, CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG)
    ]);
  }

  return { tableRows: topThreeDiffs(diffList), excessDeficitDiff: getExcessDeficitDiff(crimsonDiffData) };
}

function topThreeDiffs(diffList) {
  let validDiffList: Array<any> = [];

  for (let i = 0; i < diffList.length; i++) {
    if (diffList[i].value && diffList[i].value != NOT_AVAILABLE && Math.abs(diffList[i].value) >= 0.5) {
      validDiffList.push(diffList[i]);
    }
  }
  validDiffList.sort(function (a, b) {
    return Math.abs(Number(b.value)) - Math.abs(Number(a.value));
  });

  // Since we only need remove duplicates from collateral diffs , only one diff at max will be removed
  // hence we are taking 4 as max value.
  if (validDiffList.length > 0) {
    validDiffList = validDiffList.slice(0, Math.min(4, validDiffList.length));
  }

  if (validDiffList.length >= 2 && isDuplicateCollateralPresent(validDiffList[0], validDiffList[1])) {
    validDiffList = validDiffList.slice(1);
  }
  else if (validDiffList.length >= 3 && isDuplicateCollateralPresent(validDiffList[1], validDiffList[2])) {
    validDiffList.splice(2, 1);
  }

  let tableRows = getTodaySummaryDiffTableRows(validDiffList);

  if (tableRows.length == 0) {
    tableRows.push(["No Diffs to highlight", "", ""]);
  }

  return tableRows;
}

function isDuplicateCollateralPresent(e1, e2) {
  return (e1.value == e2.value && e1.hoverOnColumn == e2.hoverOnColumn && e1.hoverOnColumn == COLLATERAL)
}

// Here data is instance of TableDiff class , use accordingly.
function createHoverRowsForData(data, tableName) {
  return [
    createObjectForHover(data.exposure, EXPOSURE, tableName),
    createObjectForHover(data.requirement, REQUIREMENT, tableName),
    createObjectForHover(data.cashCollateral, CASH_COLLATERAL, tableName),
    createObjectForHover(data.securitiesCollateral, SECURITIES_COLLATERAL, tableName)
  ]
}

function createObjectForHover(value: string, column: string, table: string) {
  return {
    value: value,
    hoverOnColumn: column,
    hoverOnTable: table
  }
}

function validateAndComputeDiff(value1, value2, formatResult) {
  if (formatResult) {
    return (!checkUndefinedOrEmpty(value1) && !checkUndefinedOrEmpty(value2) &&
      !checkUndefinedOrEmpty(value1 - value2) && !isNaN(value1 - value2)) ? validateAndFormatNumber(value1 - value2) : NOT_AVAILABLE;
  } else {
    return (!checkUndefinedOrEmpty(value1) && !checkUndefinedOrEmpty(value2) &&
      !checkUndefinedOrEmpty(value1 - value2) && !isNaN(value1 - value2)) ? value1 - value2 : NOT_AVAILABLE;
  }
}

function validateAndAdd(...args: any[]) {
  let sum = 0;
  let allValuesNaN = true;
  for (let i = 0; i < args.length; i++) {
    let value = args[i];
    if (!isNaN(value)) {
      sum += args[i];
      allValuesNaN = false;
    }
  }
  return allValuesNaN ? NOT_AVAILABLE : sum
}



function validateAndFormatNumber(value) {
  return !isNaN(value) && !checkUndefinedOrEmpty(value) ? plainNumberFormatter(null, null, value, null) : NOT_AVAILABLE;
}

function validateAndFormatRecStatusNumber(value: any) {
  return !isNaN(value) && !checkUndefinedOrEmpty(value) ? plainNumberFormatter(null, null, value, null) : 0;
}

function checkUndefinedOrEmpty(value) {
  return (value == undefined || value === "")
}
