import {
  CASH_COLLATERAL,
  CRIMSON_TABLES,
  EXPOSURE,
  REQUIREMENT,
  SECURITIES_COLLATERAL,
  SEG_CASH_COLLATERAL,
  SEG_REQUIREMENT,
  SEG_SECURITIES_COLLATERAL,
} from '../../../commons/constants';

export const IN_PROGRESS = 'IN_PROGRESS';
export const WORKFLOW_CONFLICT = 'Workflow Conflict';
export const QUICK_PUBLISH = 'QUICK_PUBLISH';
export const QUICK_WIRE_PUBLISH = 'QUICK_WIRE_PUBLISH';

const EXPOSURE_DIFF_INTERNAL_VS_MARGIN_FILE =
  'Exposure Diff (Internal Exposure - Margin File Exposure)';
const REQUIREMENT_DIFF_INTERNAL_VS_MARGIN_FILE =
  'Margin Calc Diff (Internal Calc - Margin File Calc)';
const CASH_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE =
  'Cash Collateral Diff (Internal Cash Collateral - Margin File Cash Collateral)';
const SECURITIES_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE =
  'Securities Collateral Diff (Internal Sec Collateral - Margin File Sec Collateral)';

const EXPOSURE_DIFF_POS_REC_VS_MARGIN_FILE =
  'External Exposure Diff (PosRec Exposure - Margin File Exposure)';
const REQUIREMENT_DIFF_POS_REC_VS_MARGIN_FILE =
  'Margin Calc Diff (PosRec Data/Internal Calc -  Margin File Data/Broker Calc)';

const EXPOSURE_DIFF_INTERNAL_VS_POS_REC = 'Exposure Diff (Internal Exposure - PosRec Exposure)';
const EXPOSURE_DIFF_INTERNAL_VS_REPO_REC = 'Exposure Diff (Internal Exposure - RepoRec Exposure)';
const REQUIREMENT_DIFF_INTERNAL_VS_POS_REC = 'Margin Calc Diff (Internal Calc - PosRec Calc)'
const REQUIREMENT_DIFF_INTERNAL_VS_REPO_REC = 'Margin Calc Diff (Internal Calc - RepoRec Calc)'

const SEG_REQUIREMENT_DIFF_INTERNAL_VS_MARGIN_FILE =
  'Margin Calc Diff (Segregated Internal Calc - Margin File Calc)';
const SEG_CASH_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE =
  'Cash Collateral Diff (Segregated Internal Cash Collateral - Margin File Cash Collateral)';
const SEG_SECURITIES_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE =
  'Securities Collateral Diff (Segregated Internal Sec Collateral - Margin File Sec Collateral)';

export const AGREEMENT_TYPES_WITH_STATIC_CRIMSON_VIEW = ['PB', 'FOC', 'MNA'];
export const EXCESS_DEFICIT_COLUMN_INDEX = 5;
export const DIFF_ROW_INDEX = 2;

export const DIFF_ROW_NAMES = {
  [CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA]: {
    [EXPOSURE]: EXPOSURE_DIFF_INTERNAL_VS_MARGIN_FILE,
    [REQUIREMENT]: REQUIREMENT_DIFF_INTERNAL_VS_MARGIN_FILE,
    [CASH_COLLATERAL]: CASH_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE,
    [SECURITIES_COLLATERAL]: SECURITIES_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE,
  },
  [CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE_DATA]: {
    [EXPOSURE]: EXPOSURE_DIFF_POS_REC_VS_MARGIN_FILE,
    [REQUIREMENT]: REQUIREMENT_DIFF_POS_REC_VS_MARGIN_FILE,
  },
  [CRIMSON_TABLES.INTERNAL_VS_POS_REC_DATA]: {
    [EXPOSURE]: EXPOSURE_DIFF_INTERNAL_VS_POS_REC,
    [REQUIREMENT]: REQUIREMENT_DIFF_INTERNAL_VS_POS_REC,
  },
  [CRIMSON_TABLES.INTERNAL_VS_REPO_REC_DATA]: {
    [EXPOSURE]: EXPOSURE_DIFF_INTERNAL_VS_REPO_REC,
    [REQUIREMENT]: REQUIREMENT_DIFF_INTERNAL_VS_REPO_REC
  },
  [CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_DATA_SEG]: {
    [SEG_REQUIREMENT]: SEG_REQUIREMENT_DIFF_INTERNAL_VS_MARGIN_FILE,
    [SEG_CASH_COLLATERAL]: SEG_CASH_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE,
    [SEG_SECURITIES_COLLATERAL]: SEG_SECURITIES_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE,
  },
};

export const MESSAGES = {
  INITIATE_WORKFLOW_FAILURE: 'Error occurred while initiating workflow',
  GENERATE_CALL_REPORT_FAILURE: 'Error occurred while generating call report',
  GENERATE_CALL_REPORT_SUCCESS: 'Call report generated and emailed successfully',
  WORKFLOW_PUBLISH_FAILURE: 'Workflow published failed. Cancelling workflow',
  MULTI_QUICK_PUBLISH_FAILURE: 'Quick Workflow published failed for following agreements :',
  MULTI_QUICK_PUBLISH_INITIATION_FAILURE: 'Error occurred while initiating Quick Publish Operation for all selected agreements',
  SHOW_POST_COLLATERAL_POPUP_FAILURE: 'Error occurred while opening Post Collateral popup',
  RECON_DATA_FETCH_FAILURE: 'Error occurred while fetching recon data',
  RELOAD_AGREEMENT_SUMMARY_CONFIRMATION_POST_LOG_WIRE_SUCCESS:
    'Logging Wire from summary screen changes the status of the corresponding agreement workflow. Do you want to reload this screen ?',
  RELOAD_AGREEMENT_SUMMARY_CONFIRMATION_POST_CALL_REPORT_SUCCESS:
    'Call report generation from the summary screen changes the status of the corresponding agreement workflow. Do you want to reload this screen ?',
};
