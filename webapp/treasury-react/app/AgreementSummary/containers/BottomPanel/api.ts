import {ArcFetch} from "arc-commons";
import {BASE_URL, QUERY_ARGS} from "../../../commons/constants"
import SERVICE_URLS from "../../../Lcm/AgreementWorkflow/commons/urlConfigs"
import {getPublishWorkflowParams} from '../../../Lcm/AgreementWorkflow/utils/agreementWorkflowStoreUtils';
import {fetchJSONPostURL, fetchPostURL} from "../../../commons/util";

export const quickCallReport = (workflowId: number, agreementId: number, treasuryAgreementId: number) => {
  const saveCallReportUrl = `${SERVICE_URLS.agreementWorkflowService.saveCallReport}?workflowId=${workflowId
  }&actionableAgreementId=${agreementId}&treasuryAgreementId=${treasuryAgreementId}&format=Json&inputFormat=PROPERTIES`;
  return ArcFetch.getJSON(saveCallReportUrl, QUERY_ARGS)
}

export const fetchReconData = (date, agreementId) => {
  const url = `${BASE_URL}/service/reconDataService/getReconAgreementStatusData?lcmPositionDataParam.date=${date
    }&lcmPositionDataParam.agreementIds=${agreementId}&format=Json&inputFormat=PROPERTIES`;

  return ArcFetch.getJSON(url, QUERY_ARGS)
}

export const beginWorkflow = (paramString: string) => {
  let beginWorkflowUrl = `${BASE_URL}comet/workflowBegin?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return ArcFetch.getJSON(beginWorkflowUrl, QUERY_ARGS)
}

export const resumeWorkflow = (workflowId: number) => {
  let resumeWorkflowUrl = `${BASE_URL}comet/resumeWorkflow?workflowParam.workflowId=${workflowId}`;
  return ArcFetch.getJSON(resumeWorkflowUrl, QUERY_ARGS)
}

export const cancelWorkflow = (workflowId: number) => {
  let cancelWorkflowUrl = `${BASE_URL}comet/workflowExecute?workflowParam.workflowId=${workflowId}&status=CANCELLED`;
  return ArcFetch.getJSON(cancelWorkflowUrl, QUERY_ARGS)
}

export const generateCallReport = (workflowId: number, actionableAgreementId: number, treasuryAgreementId: number) => {
  const saveCallReportUrl = `${BASE_URL}service/callReportService/emailCallReport?workflowId=${workflowId
  }&actionableAgreementId=${actionableAgreementId}&treasuryAgreementId=${treasuryAgreementId}&format=Json&inputFormat=PROPERTIES`
  return ArcFetch.getJSON(saveCallReportUrl, QUERY_ARGS)
}

export const quickPublishWorkflow = (workflowId, quickPublishParams) => {
  let url = `${BASE_URL}comet/workflowExecute?${quickPublishParams}&workflowParam.workflowId=${workflowId}&status=PUBLISHED`;
  return ArcFetch.getJSON(url, QUERY_ARGS)
}

export const saveAgreementArtifacts = (workflowId: number, workflowOperation: string, agreementId: number, wireAmount: string, marginCallType: string) => {
  let params = getPublishWorkflowParams(workflowOperation, workflowId, agreementId, agreementId, wireAmount, marginCallType);
  const saveWorkflowUrl = SERVICE_URLS.agreementWorkflowService.saveWorkflowArtifacts
  return fetchJSONPostURL(saveWorkflowUrl, params)
}

export const quickPublishAgreementWorkflow = (workflowId: number, agreementId: number) => {
  const saveWorkflowUrl = `${BASE_URL}service/agreementWorkflowPublishService/quickPublishAgreementWorkflow` +
    `?actionableAgreementId=${agreementId}&workflowId=${workflowId}&format=JSON`
  return ArcFetch.getJSON(saveWorkflowUrl, QUERY_ARGS)
}

export const quickPublishMultipleAgreementWorkflows = (date: string, actionableAgreementIds: Array<number>) => {
  const quickPublishMultipleAgreementUrl = `${BASE_URL}service/agreementWorkflowPublishService/quickPublishMultipleAgreements`
  const params = `date=${date}&actionableAgreementIds=${actionableAgreementIds.join(',')}&format=JSON`
  return fetchPostURL(quickPublishMultipleAgreementUrl, params)
}
