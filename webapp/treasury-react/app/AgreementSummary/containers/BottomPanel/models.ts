export class TableDiff {
  exposure: any;
  requirement: any;
  cashCollateral: any;
  securitiesCollateral: any;
  constructor(exposure?: number, requirement?: number, cashCollateral?: number, securitiesCollateral?: number) {
    this.exposure = exposure;
    this.requirement = requirement;
    this.cashCollateral = cashCollateral;
    this.securitiesCollateral = securitiesCollateral;
  }
}

export interface WorkflowStatusDetail {
  workflowId: number,
  workflowState: string,
  subject: string,
  message: string
}
