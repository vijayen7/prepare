import React, { Fragment } from 'react';
import { useObserver } from 'mobx-react-lite';
import Link from '../../../commons/components/Link';
import ButtonWithPause from '../../../commons/components/ButtonWithPause';
import { DownloadBrokerFile } from '../../components/DownloadBrokerFile';
import { DownloadCrifFile } from '../../components/DownloadCrifFile';
import { WORKFLOW_STATUS_TO_LOCK_AGREEMENT_ACTIONS } from '../../constants'
import {
  getAgreementDetail,
  getApplicableEdSummaryData,
  getBrokerDataViewUrl,
  getCollateralTermSearchUrl,
  getPositionDetailReportLink,
  getRagRulesSearchUrl,
  getViewAdjustmentsLink,
  getMarginCallType
} from './utils/agreementDetailUtils';
import {
  beginAgreementWorkflow,
  beginWorkflowAndGenerateCallReport,
  quickWirePublishAgreement,
  quickPublishAgreement
} from './utils/workflowUtils';
import { isCrimsonEnabledForAgreementType } from './utils/crimsonViewUtils';
import { displayPostCollateralPopup, onClickPostCollateral } from '../../utils';

export const ActionPanel: React.FC<any> = (props) => {
  const selectedAgreementData = props.selectedAgreementData;
  const applicableEdSummaryData = getApplicableEdSummaryData(props.selectedAgreementData);

  const isTreasuryAgreement = selectedAgreementData?.parentMarginCallTypeEdSummaryDataList;
  const agreementDetail = getAgreementDetail(selectedAgreementData);
  const treasuryAgreementDetail = selectedAgreementData?.treasuryAgreementDetail;
  const areActionsDisabledForAgreement = WORKFLOW_STATUS_TO_LOCK_AGREEMENT_ACTIONS.includes(
    selectedAgreementData?.workflow?.workflowStatus?.name
  );
  const redirectToOldPdrScreen = props.redirectToOldPdrScreen;

  const onClickQuickPublish = () =>
  quickPublishAgreement(
      props.date,
      treasuryAgreementDetail.legalEntityId,
      treasuryAgreementDetail.cpeId,
      treasuryAgreementDetail.agreementTypeId,
      treasuryAgreementDetail.agreementId
    );

  const onClickPostCollateralAndPublish = () =>
    displayPostCollateralPopup(
      props.date,
      isTreasuryAgreement,
      selectedAgreementData,
      (wireAmount) =>
        quickWirePublishAgreement(
          props.date,
          treasuryAgreementDetail.legalEntityId,
          treasuryAgreementDetail.cpeId,
          treasuryAgreementDetail.agreementTypeId,
          getAgreementDetail(selectedAgreementData).agreementId,
          wireAmount,
          getMarginCallType(selectedAgreementData)?.abbrev
        )
    );

  const onClickBeginWorkflow = () =>
    beginAgreementWorkflow(
      props.date,
      treasuryAgreementDetail.agreementId,
      treasuryAgreementDetail.legalEntityId,
      treasuryAgreementDetail.cpeId,
      treasuryAgreementDetail.agreementTypeId
    );

  const onClickGenerateCallReport = () =>
    beginWorkflowAndGenerateCallReport(
      props.date,
      treasuryAgreementDetail.legalEntityId,
      treasuryAgreementDetail.cpeId,
      treasuryAgreementDetail.agreementTypeId,
      agreementDetail.agreementId,
      treasuryAgreementDetail.agreementId
    );

  return useObserver(() => (
    <>
      <div className="layout--flex container">
        <div className="margin--horizontal margin--vertical">

          <ButtonWithPause
            id="quickPublish"
            className="margin--horizontal--small"
            disabled={areActionsDisabledForAgreement}
            hidden={!isTreasuryAgreement}
            onClick={onClickQuickPublish}
            data={<Fragment> <i className="icon-quick" /> Quick Publish</Fragment>}
          />
          <button
            id="postCollateralAndPublish"
            className="margin--horizontal--small"
            disabled={areActionsDisabledForAgreement}
            hidden={!isTreasuryAgreement}
            onClick={onClickPostCollateralAndPublish}
          >
            <i className="icon-transfer" /> Post Collateral &amp; Publish
          </button>
          <button
            disabled={areActionsDisabledForAgreement}
            hidden={isTreasuryAgreement}
            className="margin--horizontal--small"
            onClick={() =>
              onClickPostCollateral(props.date, isTreasuryAgreement, selectedAgreementData)
            }
          >
            Post Collateral
          </button>
          <button
            id="beginWorkflow"
            className="button--primary margin--horizontal--small"
            onClick={onClickBeginWorkflow}
            hidden={!isTreasuryAgreement}
          >
            <i className="icon-edit" /> Begin Workflow
          </button>
        </div>
        <div className="margin--horizontal margin--vertical size--content">
          <button
            id="generateCallReport"
            disabled={areActionsDisabledForAgreement}
            onClick={onClickGenerateCallReport}
          >
            <i className="icon-mail" /> Call Report
          </button>
          &nbsp;
          <DownloadBrokerFile date={props.date} agreementData={applicableEdSummaryData} />
          <DownloadCrifFile agreementData={props.selectedAgreementData} />
        </div>
      </div>
      <div className="layout--flex container">
        <div className="margin--horizontal margin--vertical">
          {isTreasuryAgreement && (
            <>
              <Link
                id="agreementLevelPositionDetail"
                text="Agreement Level Position Detail"
                href={getPositionDetailReportLink(props.date, props.selectedAgreementData, false, props.selectedCurrencyKey, redirectToOldPdrScreen)}
              />
              &nbsp;&nbsp;&nbsp;
            </>
          )}
          {isTreasuryAgreement &&
            isCrimsonEnabledForAgreementType(
              props.selectedAgreementData.treasuryAgreementDetail.agreementTypeId
            ) && (
              <>
                <Link
                  id="positionDetailDayOnDayDiff"
                  text="Position Detail (Day on Day Diff)"
                  href={getPositionDetailReportLink(props.date, props.selectedAgreementData, true, props.selectedCurrencyKey, redirectToOldPdrScreen)}
                />
                &nbsp;&nbsp;&nbsp;
              </>
            )}
          <Link
            id="viewAdjustments"
            text="View Adjustments"
            href={getViewAdjustmentsLink(props.date, props.selectedAgreementData)}
          />
          &nbsp;&nbsp;&nbsp;
          <Link
            id="viewBrokerData"
            text="Broker Data"
            href={getBrokerDataViewUrl(props.date, props.selectedAgreementData)}
          />
          &nbsp;&nbsp;&nbsp;
          <Link
            id="viewCollateralTerms"
            text="Collateral Terms"
            href={getCollateralTermSearchUrl(props.date, props.selectedAgreementData)}
          />
          &nbsp;&nbsp;&nbsp;
          <Link
            id="viewRagRules"
            text="RAG Rules"
            href={getRagRulesSearchUrl(props.date, props.selectedAgreementData)}
          />
        </div>
      </div>
    </>
  ));
};
