import React from 'react';
import { Panel, TabPanel, Message } from 'arc-react-components';
import { observer } from 'mobx-react-lite';
import agreementSummaryStore from '../../useStores';
import { isCrimsonEnabledForAgreementType } from './utils/crimsonViewUtils';
import { CrimsonView } from './tabs/CrimsonView';
import { Summary } from './tabs/Summary';
import { AgreementMarginCallTypeDetail } from './tabs/AgreementMarginCallTypeDetail';
import { RAG_CATEGORY_CODE_MAP } from '../../constants';
import { getAgreementDetail } from './utils/agreementDetailUtils';
import { getValueOrDefault } from '../../utils';
import { AGREEMENT_TYPE_ID } from '../../../commons/constants';
import { AttributeDetail } from './tabs/AttributeDetail';
import { ActionPanel } from './ActionPanel';
import { showAgreementInfoDialog } from '../../components/dialog';

const BottomPanel: React.FC<any> = () => {
  let selectedAgreementData = agreementSummaryStore.selectedAgreementData;
  let date = agreementSummaryStore.lastSearchFilter.selectedDate;
  let agreementDetail = getAgreementDetail(selectedAgreementData);
  let selectedCurrencyKey = agreementSummaryStore.lastSearchFilter.selectedCurrency.key;
  let redirectToOldPdrScreen = agreementSummaryStore.redirectToOldPdrScreen;

  return selectedAgreementData ? (
    <>
      <Panel
        dismissible
        onClose={() => agreementSummaryStore.setShowBottomPanel(false)}
        title={
          <div>
            Excess Deficit Summary for {agreementDetail.legalEntityAbbrev}-
            {agreementDetail.cpeAbbrev}-{agreementDetail.agreementTypeName}
            <i
              className="icon-info"
              onClick={() =>
                showAgreementInfoDialog({
                  ...agreementDetail,
                  agreementData: selectedAgreementData,
                })
              }
            />
          </div>
        }
      >
        <div>
          <ActionPanel date={date} selectedAgreementData={selectedAgreementData} selectedCurrencyKey={selectedCurrencyKey} redirectToOldPdrScreen={redirectToOldPdrScreen}/>
          {selectedAgreementData.parentMarginCallTypeEdSummaryDataList ? (
            <MarginCallTypeAgreementDetails
              date={date}
              selectedAgreementData={selectedAgreementData}
            />
          ) : (
            <AgreementMarginCallTypeDetail agreementEdSummaryData={selectedAgreementData} />
          )}
        </div>
      </Panel>
    </>
  ) : (
    <div />
  );
};

const MarginCallTypeAgreementDetails = (props) => {
  let marginCallTypeDetailTabs: Array<any> = props.selectedAgreementData?.parentMarginCallTypeEdSummaryDataList.map(
    (element) => {
      let marginCallType = element.edSummaryDataKey.marginCallType.abbrev;
      return (
        <TabPanel.Tab key={marginCallType} tabId={marginCallType} label={marginCallType}>
          <AgreementMarginCallTypeDetail agreementEdSummaryData={element} />
        </TabPanel.Tab>
      );
    }
  );

  const [selectedTabId, onSelectTabChange] = React.useState('summary');

  React.useEffect(() => {
    onSelectTabChange('summary');
  }, [props.selectedAgreementData]);

  return (
    <div className="margin--vertical">
      <Message
        className="margin--vertical"
        type={
          RAG_CATEGORY_CODE_MAP[props.selectedAgreementData?.overallRagStatusDetail.ragCategoryCode]
        }
      >
        <div>
          {getValueOrDefault(
            props.selectedAgreementData?.overallRagStatusDetail.ragStatusMsg,
            'Unknown'
          )}
        </div>
      </Message>

      <TabPanel
        id="marginCallTypeDetail"
        selectedTabId={selectedTabId}
        onSelect={onSelectTabChange}
      >
        <TabPanel.Tab key="Summary" tabId="summary" label="Summary">
          <Summary
            date={props.date}
            agreementData={props.selectedAgreementData}
            {...props.selectedAgreementData.applicableEdSummaryDataKey?.agreementDetail}
          />
        </TabPanel.Tab>

        {isCrimsonEnabledForAgreementType(
          props.selectedAgreementData.applicableEdSummaryDataKey.agreementDetail.agreementTypeId
        ) && (
          <TabPanel.Tab key="crimson" tabId="crimson" label="Collateral Mgmt. vs Reconciler">
            <CrimsonView date={props.date} agreementData={props.selectedAgreementData} />
          </TabPanel.Tab>
        )}

        {marginCallTypeDetailTabs}

        {props.selectedAgreementData.applicableEdSummaryDataKey.agreementDetail.agreementTypeId ==
          AGREEMENT_TYPE_ID.ISDA && (
          <TabPanel.Tab key="isdaAttributes" tabId="isdaAttributes" label="Attributes">
            <AttributeDetail
              reportingCurrency={props.selectedAgreementData.reportingCurrency}
              selectedAgreementData={props.selectedAgreementData}
            />
          </TabPanel.Tab>
        )}
      </TabPanel>
    </div>
  );
};

export default observer(BottomPanel);
