import _ from "lodash"
import React, { useEffect } from "react"
import { Message } from "arc-react-components"
import AgmtSummaryVsReconcilerSummaryTab from "../../../../Lcm/AgreementSummary/components/AgmtSummaryVsReconcilerSummaryTab"
import { getCollateralMgmtVsReconTabData, showCrimsonDiffDrilldown } from "../utils/crimsonViewUtils"
import { getTreasuryAndChildAgreementDetails } from "../utils/agreementDetailUtils"

interface CrimsonViewProps {
  date: string
  agreementData: any
}

export const CrimsonView: React.FC<CrimsonViewProps> = (props) => {

  const [crimsonData, setCrimsonData] = React.useState({})
  const [isLoading, setIsLoading] = React.useState(false)

  useEffect(() => {
    setCrimsonData({})
    setIsLoading(true)
    getCollateralMgmtVsReconTabData(props.date, props.agreementData)
      .then(data => setCrimsonData(data))
      .catch(e => console.log(e))
      .finally(() => setIsLoading(false))
  }, [props.agreementData])

  if (isLoading) {
    return <div className="loader margin--top"> Loading </div>
  } else if (_.isEmpty(crimsonData)) {
    return <Message type={Message.Type.WARNING}>Error occurred while loading Crimson view</Message>
  } else {
    return <div>
      <AgmtSummaryVsReconcilerSummaryTab
        data={crimsonData}
        onClick={(fromExcessDeficitDataSource, toExcessDeficitDataSource, excessDeficitDataType, showSegDrilldown) =>
          showCrimsonDiffDrilldown(props.date, getTreasuryAndChildAgreementDetails(props.agreementData), fromExcessDeficitDataSource, toExcessDeficitDataSource, excessDeficitDataType, showSegDrilldown)} />
    </div>
  }
}
