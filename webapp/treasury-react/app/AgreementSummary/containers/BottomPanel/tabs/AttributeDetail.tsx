import React from "react"
import { Card } from "arc-react-components";
import { plainNumberFormatter } from "../../../../commons/grid/formatters";
import { NOT_AVAILABLE } from "../../../../commons/constants";
import { getValueOrDefault } from "../../../utils";

export const AttributeDetail = (props) => {
  const agreementDetail = props.selectedAgreementData.applicableEdSummaryDataKey.agreementDetail
  const currentDayEdSummaryData = props.selectedAgreementData.childMarginCallTypeEdSummaryDataList?.find(e => e.edSummaryDataKey?.marginCallType?.abbrev === "HOUSE")?.curDayEdSummaryData
  const isdaExcessDeficitDetails = currentDayEdSummaryData.isdaExcessDeficitDetails

  return <div>
    <div className="layout--flex margin--vertical--small">
      <Card title="Legal Entity Abbrev.:">{textFormatter(agreementDetail?.legalEntityAbbrev)}</Card>
      <Card title="Legal Entity Name:">{textFormatter(agreementDetail?.legalEntityName)}</Card>
      <Card title="Counterparty Abbrev.:">{textFormatter(agreementDetail?.cpeAbbrev)}</Card>
      <Card title="Counterparty Name:">{textFormatter(agreementDetail?.cpeName)}</Card>
    </div>
    <div className="layout--flex margin--vertical--small">
      <Card title="Reporting Currency:">{textFormatter(props.reportingCurrency.abbreviation)}</Card>
      <Card title="Rounding Convention:">{textFormatter(isdaExcessDeficitDetails?.roundingConvention)}</Card>
      <Card title="Applicable Delivery Amount:">{numberFormatter(isdaExcessDeficitDetails?.seApplicableDeliveryAmount)}</Card>
      <Card title="Applicable Return Amount:">{numberFormatter(isdaExcessDeficitDetails?.seApplicableReturnAmount)}</Card>
    </div>
    { isdaExcessDeficitDetails?.simmLeMarginMethodology &&  isdaExcessDeficitDetails?.simmCpeMarginMethodology && (
      <div className="layout--flex margin--vertical--small">
        <Card title="SIMM LE Threshold:">{numberFormatter(isdaExcessDeficitDetails?.simmLeThreshold)}</Card>
        <Card title="SIMM LE Margin-Methodology:">{textFormatter(isdaExcessDeficitDetails?.simmLeMarginMethodology)}</Card>
        <Card title="SIMM CPE Threshold:">{numberFormatter(isdaExcessDeficitDetails?.simmCpeThreshold)}</Card>
        <Card title="SIMM CPE Margin-Methodology:">{textFormatter(isdaExcessDeficitDetails?.simmCpeMarginMethodology)}</Card>
      </div>
    )}
    <div className="layout--flex margin--vertical--small">
      <AttributeDetailTable
        currentDayEdSummaryData={currentDayEdSummaryData}
        isdaExcessDeficitDetails={isdaExcessDeficitDetails}
      />
    </div>
  </div>
}

const AttributeDetailTable = ({ currentDayEdSummaryData, isdaExcessDeficitDetails }) => {
  return <table className="table table--fill--width margin--vertical--small margin--horizontal--small"
    id="internalVsExternalId"
  >
    <tbody>
      <tr>
        <th>Field</th>
        <th>Internal</th>
        <th>External</th>
      </tr>

      <tr>
        <td>Exposure</td>
        <td className="text-align--right" id="adInternalExposure">{numberFormatter(currentDayEdSummaryData?.adjustedEdSummaryDataDetail?.exposure)}</td>
        <td className="text-align--right" id="adExternalExposure">{numberFormatter(currentDayEdSummaryData?.externalEdSummaryDataDetail?.exposure)}</td>
      </tr>
      <tr>
        <td>Independent Amount</td>
        <td className="text-align--right" id="internalIA">{numberFormatter(currentDayEdSummaryData?.adjustedEdSummaryDataDetail?.requirement)}</td>
        <td className="text-align--right" id="externalIA">{numberFormatter(currentDayEdSummaryData?.externalEdSummaryDataDetail?.requirement)}</td>
      </tr>
      <tr>
        <td>Collateral</td>
        <td className="text-align--right" id="adInternalCollateral">{numberFormatter(currentDayEdSummaryData?.adjustedEdSummaryDataDetail?.collateral)}</td>
        <td className="text-align--right" id="adExternalCollateral">{numberFormatter(currentDayEdSummaryData?.externalEdSummaryDataDetail?.collateral)}</td>
      </tr>
      <tr>
        <td>Posted Collateral</td>
        <td className="text-align--right" id="internalPostedCollateral">{numberFormatter(isdaExcessDeficitDetails?.sePostedCollateral)}</td>
        <td className="text-align--right" id="externalPostedCollateral">{numberFormatter(currentDayEdSummaryData?.externalEdSummaryDataDetail?.collateral)}</td>
      </tr>
      <tr>
        <td>Credit Support Amount</td>
        <td className="text-align--right" id="internalCreditSupport">{numberFormatter(isdaExcessDeficitDetails?.seCreditSupportAmount)}</td>
        <td className="text-align--right" id="externalCreditSupport">{numberFormatter(isdaExcessDeficitDetails?.cpeCreditSupportAmount)}</td>
      </tr>
      <tr>
        <td>Secured Party/Pledgor</td>
        <td className="text-align--right" id="seCategory">{textFormatter(isdaExcessDeficitDetails?.seCategory)}</td>
        <td className="text-align--right" id="cpeCategory">{textFormatter(isdaExcessDeficitDetails?.cpeCategory)}</td>
      </tr>
      <tr>
        <td>Threshold Amount</td>
        <td className="text-align--right" id="internalThresholdAmount">{numberFormatter(isdaExcessDeficitDetails?.legalEntilegalEntityThresholdtyRoundingAmount)}</td>
        <td className="text-align--right" id="externalThresholdAmount">{numberFormatter(isdaExcessDeficitDetails?.cpeThreshold)}</td>
      </tr>
      <tr>
        <td>Minimum Transfer Amount</td>
        <td className="text-align--right" id="internalMinimumTransferAmount">{numberFormatter(isdaExcessDeficitDetails?.legalEntityMinTransferAmount)}</td>
        <td className="text-align--right" id="externalMinimumTransferAmount">{numberFormatter(isdaExcessDeficitDetails?.cpeMinTransferAmount)}</td>
      </tr>
      <tr>
        <td>Rounding Amount</td>
        <td className="text-align--right" id="internalRoundingAmount">{numberFormatter(isdaExcessDeficitDetails?.legalEntityRoundingAmount)}</td>
        <td className="text-align--right" id="externalRoundingAmount">{numberFormatter(isdaExcessDeficitDetails?.cpeRoundingAmount)}</td>
      </tr>
      <tr>
        <td>Raw Excess/Deficit</td>
        <td className="text-align--right" id="rawInternalED">{numberFormatter(currentDayEdSummaryData?.internalEdSummaryDataDetail?.excessDeficit)}</td>
        <td className="text-align--right" id="rawExternalED">{numberFormatter(currentDayEdSummaryData?.externalEdSummaryDataDetail?.excessDeficit)}</td>
      </tr>
      <tr>
        <td>Excess/Deficit</td>
        <td className="text-align--right" id="adInternalED">{numberFormatter(currentDayEdSummaryData?.adjustedEdSummaryDataDetail?.excessDeficit)}</td>
        <td className="text-align--right" id="adExternalED">{numberFormatter(currentDayEdSummaryData?.externalEdSummaryDataDetail?.excessDeficit)}</td>
      </tr>
    </tbody>
  </table>
}

const numberFormatter = value => plainNumberFormatter(null, null, value)
const textFormatter = value => getValueOrDefault(value, NOT_AVAILABLE)
