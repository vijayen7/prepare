import React from 'react';
import { Layout } from 'arc-react-components';
import { AdjustIntVsIntTable } from '../../../components/ExcessDeficitDataTables/AdjustIntVsIntTable';
import { ExternalT1VsT2Table } from '../../../components/ExcessDeficitDataTables/ExternalT1VsT2Table';
import { InternalT1VsT2Table } from '../../../components/ExcessDeficitDataTables/InternalT1VsT2Table';
import { InternalVsExternalTable } from '../../../components/ExcessDeficitDataTables/InternalVsExternalTable';
import {
  InternalVsExternalData,
  InternalT1VsT2,
  ExternalT1VsT2,
  AdjustedIntVsInt,
} from '../../../../Lcm/AgreementWorkflow/models';
import {
  getAdjustedIntVsIntData,
  getExternalT1VsT2Data,
  getInternalT1VsT2Data,
  getInternalVsExternalData,
} from '../utils/agreementDetailUtils';

export const AgreementMarginCallTypeDetail = (props) => {
  let currentDayEdSummaryData = props.agreementEdSummaryData.curDayEdSummaryData;
  let prevDayEdSummaryData = props.agreementEdSummaryData.prevDayEdSummaryData;
  let isSimmApplicable = props.agreementEdSummaryData.edSummaryDataKey.isSimmApplicable;

  let arr: any = [];
  const numTablesToNumTopRow = { 1: 1, 2: 2, 3: 1, 4: 1, 5: 2, 6: 3, 7: 3 }

  let internalVsExternalData: InternalVsExternalData = getInternalVsExternalData(
    currentDayEdSummaryData
  );
  if (internalVsExternalData) {

    arr.push(<InternalVsExternalTable
      isSimmApplicable={isSimmApplicable}
      internalVsExternalData={internalVsExternalData}
    />)
  };

  let internalT1VsT2: InternalT1VsT2 = getInternalT1VsT2Data(
    currentDayEdSummaryData,
    prevDayEdSummaryData
  );
  if (internalT1VsT2) {
    arr.push(<InternalT1VsT2Table
      isSimmApplicable={isSimmApplicable}
      internalT1VsT2={internalT1VsT2}
    />)
  };

  let externalT1VsT2: ExternalT1VsT2 = getExternalT1VsT2Data(
    currentDayEdSummaryData,
    prevDayEdSummaryData
  );
  if (externalT1VsT2) {
    arr.push(<ExternalT1VsT2Table
      isSimmApplicable={isSimmApplicable}
      externalT1VsT2={externalT1VsT2}
    />)
  };

  let adjustedIntVsInt: AdjustedIntVsInt = getAdjustedIntVsIntData(currentDayEdSummaryData);
  if (adjustedIntVsInt) {
    arr.push(<AdjustIntVsIntTable
      isSimmApplicable={isSimmApplicable}
      adjustedIntVsInt={adjustedIntVsInt}
    />)
  };


  function getLayout(startPos: number, numElement: number) {
    let layout: any = [];
    for (let i = 0; i < numElement && i < arr.length; i++) {
      let childId = `${startPos + i}`;
      layout.push(
        <Layout.Child childId={childId}>
          <div style={{ display: "flex", justifyContent: "center" }}>
            {arr[startPos + i]}
          </div>
        </Layout.Child>
      );
    }
    return (<Layout isColumnType={true}>{layout}</Layout>)
  }


  let numTopRow = numTablesToNumTopRow[arr.length];
  if (numTopRow === undefined) {
    numTopRow = 3;
  }

  if (arr.length == 0) {
    return null;
  } else if (arr.length <= numTopRow) {
    return (
      <div className="margin--vertical--small margin--horizontal text-align--center">
        {getLayout(0, numTopRow)}
      </div>
    )
  } else {
    return (
      <div className="margin--vertical--small margin--horizontal text-align--center">
        <Layout>
          <Layout.Child childId="row0" className="margin--bottom">
            {getLayout(0, numTopRow)}
          </Layout.Child>
          <Layout.Child childId="row1" className="margin--bottom">
            {getLayout(numTopRow, arr.length - numTopRow)}
          </Layout.Child>
        </Layout>
      </div>);
  }

};
