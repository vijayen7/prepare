import React from 'react';
import { Layout } from 'arc-react-components';
import {
  getApplicableEdSummaryData,
  getInternalVsExternalData,
  getInternalVsExternalDataForMarginCallType,
} from '../utils/agreementDetailUtils';
import { InternalVsExternalData } from '../../../../Lcm/AgreementWorkflow/models';

import { InternalVsExternalTable } from '../../../components/ExcessDeficitDataTables/InternalVsExternalTable';
import { RegCpeInternalVsExternalTable } from '../../../components/ExcessDeficitDataTables/RegCpeInternalVsExternalTable';
import { RegInternalVsExternalTable } from '../../../components/ExcessDeficitDataTables/RegInternalVsExternalTable';
import { SegInternalVsExternalTable } from '../../../components/ExcessDeficitDataTables/SegInternalVsExternalTable';
import { HouseInternalVsExternalTable } from '../../../components/ExcessDeficitDataTables/HouseInternalVsExternalTable';

export const Summary: React.FC<any> = (props) => {
  const applicableEdSummaryData = getApplicableEdSummaryData(props.agreementData);

  // SIMM is applicable when for an agreement, SIMM collateral terms are setup.
  const isSimmApplicable: boolean = applicableEdSummaryData.edSummaryDataKey.isSimmApplicable;

  // REG is applicable only for PB with REG cases and not in ISDA with SIMM case
  // Assumption : When REG is applicable, SIMM will not be applicable
  const isRegApplicable: boolean =
    applicableEdSummaryData.edSummaryDataKey.marginCallType.abbrev === 'REG';

  let arr: any = [];
  const numTablesToNumTopRow = { 1: 1, 2: 2, 3: 1, 4: 1, 5: 2, 6: 3, 7: 3 }

  let internalVsExternalData: InternalVsExternalData = getInternalVsExternalData(
    applicableEdSummaryData.curDayEdSummaryData
  );
  if (internalVsExternalData) {
    arr.push(<InternalVsExternalTable
      isSimmApplicable={isSimmApplicable}
      internalVsExternalData={internalVsExternalData}
    />)
  };

  let houseInternalVsExternalData = getInternalVsExternalDataForMarginCallType(
    'HOUSE',
    props.agreementData.parentMarginCallTypeEdSummaryDataList
  );
  if ((isRegApplicable && houseInternalVsExternalData)) {
    arr.push(<HouseInternalVsExternalTable
      isSimmApplicable={isSimmApplicable}
      houseInternalVsExternalData={houseInternalVsExternalData}
    />)
  };

  let regInternalVsExternalData = getInternalVsExternalDataForMarginCallType(
    'REG',
    props.agreementData.parentMarginCallTypeEdSummaryDataList
  );
  if (!isRegApplicable && regInternalVsExternalData) {
    arr.push(<RegInternalVsExternalTable
      isSimmApplicable={isSimmApplicable}
      regInternalVsExternalData={regInternalVsExternalData}
    />)
  };

  let regCpeInternalVsExternalData:
    | InternalVsExternalData
    | undefined = getInternalVsExternalDataForMarginCallType(
      'REG_CPE',
      props.agreementData.parentMarginCallTypeEdSummaryDataList
    );
  if (regCpeInternalVsExternalData) {
    arr.push(<RegCpeInternalVsExternalTable
      regCpeInternalVsExternalData={regCpeInternalVsExternalData}
    />)
  }

  let segInternalVsExternalData:
    | InternalVsExternalData
    | undefined = getInternalVsExternalDataForMarginCallType(
      'SEG',
      props.agreementData.parentMarginCallTypeEdSummaryDataList
    );
  if (segInternalVsExternalData) {
    arr.push(<SegInternalVsExternalTable
      isSimmApplicable={isSimmApplicable}
      segInternalVsExternalData={segInternalVsExternalData}
    />)
  };


  function getLayout(startPos: number, numElement: number) {
    let layout: any = [];
    for (let i = 0; i < numElement && i < arr.length; i++) {
      let childId = `${startPos + i}`;
      layout.push(
        <Layout.Child childId={childId}>
          <div style={{ display: "flex", justifyContent: "center" }}>
            {arr[startPos + i]}
          </div>
        </Layout.Child>
      );
    }
    return (<Layout isColumnType={true}>{layout}</Layout>)
  }


  let numTopRow = numTablesToNumTopRow[arr.length];
  if (numTopRow === undefined) {
    numTopRow = 3;
  }

  if (arr.length == 0) {
    return null;
  } else if (arr.length <= numTopRow) {
    return (
      <div className="margin--vertical--small margin--horizontal text-align--center">
        {getLayout(0, numTopRow)}
      </div>
    )
  } else {
    return (
      <div className="margin--vertical--small margin--horizontal text-align--center">
        <Layout>
          <Layout.Child childId="row0" className="margin--bottom">
            {getLayout(0, numTopRow)}
          </Layout.Child>
          <Layout.Child childId="row1" className="margin--bottom">
            {getLayout(numTopRow, arr.length - numTopRow)}
          </Layout.Child>
        </Layout>
      </div>);
  }

};
