import React from 'react';
import ArcDataGrid from 'arc-data-grid';
import {
  displayAgreementWorkflow,
  emailCallReport,
  onClickPostCollateralAndPublish,
  onClickPostCollateralFromGrid,
  quickPublish,
} from './grid-actions';
import { plainNumberFormatter } from '../../../commons/grid/formatters';
import ClickableIconWithPause from '../../../commons/components/ClickableIconWithPause';
import { ACCOUNT_TYPES, RAG_CATEGORY_CODE_MAP, CATEGORY_RANK, WORKFLOW_STATUS_TO_LOCK_AGREEMENT_ACTIONS, RUN_BROKER_AND_RAG_SYNC } from '../../constants';
import { RowFormatter } from './RowFormatter';
import { getValueOrDefault } from '../../utils';
import agreementSummaryStore from '../../useStores';
import { BASE_URL } from "../../../commons/constants";

declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

export function getColumns(): ArcDataGrid.ColumnProperties<any, any>[] {
  return [
    getStringColumn('agreementId'),
    {
      identity: 'actions',
      Cell: (_, context) => <ActionPanel context={context} />,
      disableFilters: true,
      disableSearches: true,
      disableSortBy: true,
      disableExport: true,
      disableGroupBy: true,
      disableAggregation: true,
      width: 90,
    },
    {
      identity: 'ragStatusCode',
      header: 'Status',
      Cell: (_, context) => {
        let isPublished = context?.row?.workflowStatus === 'PUBLISHED';
        return (
          <RowFormatter isPublished={isPublished}>
            <StatusFlags row={context.row} />
          </RowFormatter>
        );
      },
    },
    {
      identity: 'workflowStatus',
      sortComparator: (a, b) => {
        let rankA = CATEGORY_RANK[a] ? CATEGORY_RANK[a] : CATEGORY_RANK['NOT_STARTED'];
        let rankB = CATEGORY_RANK[b] ? CATEGORY_RANK[b] : CATEGORY_RANK['NOT_STARTED'];
        return rankA === rankB ? 0 : rankA > rankB ? 1 : -1;
      },
    },
    {
      identity: 'ragCategoryCode',
      sortComparator: (a, b) => {
        let rankA = CATEGORY_RANK[a] ? CATEGORY_RANK[a] : CATEGORY_RANK['NOT_STARTED'];
        let rankB = CATEGORY_RANK[b] ? CATEGORY_RANK[b] : CATEGORY_RANK['NOT_STARTED'];
        return rankA === rankB ? 0 : rankA > rankB ? 1 : -1;
      },
    },
    getStringColumn('legalEntity'),
    getStringColumn('legalEntityId'),
    getStringColumn('exposureCounterparty'),
    getStringColumn('exposureCounterpartyId'),
    getStringColumn('agreementType'),
    getStringColumn('agreementTypeId'),
    getStringColumn('reportingCurrency'),
    getStringColumn('currency'),
    getStringColumn('date'),
    {
      identity: 'marginCallType',
      Cell: (value, context) => {
        let isPublished = context?.row?.workflowStatus === 'PUBLISHED';
        return (
          <RowFormatter isPublished={isPublished}>
            <MarginTypes value={value} />
          </RowFormatter>
        );
      },
    },
    {
      identity: 'callType',
      Cell: (value, context) => {
        let isPublished = context?.row?.workflowStatus === 'PUBLISHED';
        return (
          <RowFormatter isPublished={isPublished}>
            <AccountTypes value={value} />
          </RowFormatter>
        );
      },
    },

    getNumericColumn('exposure'),
    getNumericColumn('requirement'),
    getNumericColumn('securitiesCollateralMargin'),
    getNumericColumn('actualSecuritiesCollateralMargin'),
    getNumericColumn('applicableRequirement'),
    getNumericColumn('usage'),
    getNumericColumn('collateral'),
    getNumericColumn('cashCollateral'),
    getNumericColumn('securitiesCollateral'),
    getNumericColumn('excessDeficit'),
    getNumericColumn('houseExposure'),
    getNumericColumn('houseRequirement'),
    getNumericColumn('houseUsage'),
    getNumericColumn('houseCollateral'),
    getNumericColumn('houseCashCollateral'),
    getNumericColumn('houseSecuritiesCollateral'),
    getNumericColumn('houseExcessDeficit'),
    getNumericColumn('regExposure'),
    getNumericColumn('regRequirement'),
    getNumericColumn('regUsage'),
    getNumericColumn('regCollateral'),
    getNumericColumn('regCashCollateral'),
    getNumericColumn('regSecuritiesCollateral'),
    getNumericColumn('regExcessDeficit'),
    getNumericColumn('segExposure'),
    getNumericColumn('segRequirement'),
    getNumericColumn('segUsage'),
    getNumericColumn('segCollateral'),
    getNumericColumn('segCashCollateral'),
    getNumericColumn('segSecuritiesCollateral'),
    getNumericColumn('segExcessDeficit'),

    getNumericColumn('brokerExposure'),
    getNumericColumn('brokerRequirement'),
    getNumericColumn('brokerCollateral'),
    getNumericColumn('brokerCashCollateral'),
    getNumericColumn('brokerSecuritiesCollateral'),
    getNumericColumn('brokerExcessDeficit'),
    getNumericColumn('brokerHouseExposure'),
    getNumericColumn('brokerHouseRequirement'),
    getNumericColumn('brokerHouseCollateral'),
    getNumericColumn('brokerHouseCashCollateral'),
    getNumericColumn('brokerHouseSecuritiesCollateral'),
    getNumericColumn('brokerHouseExcessDeficit'),
    getNumericColumn('brokerRegExposure'),
    getNumericColumn('brokerRegRequirement'),
    getNumericColumn('brokerRegCollateral'),
    getNumericColumn('brokerRegCashCollateral'),
    getNumericColumn('brokerRegSecuritiesCollateral'),
    getNumericColumn('brokerRegExcessDeficit'),
    getNumericColumn('brokerSegExposure'),
    getNumericColumn('brokerSegRequirement'),
    getNumericColumn('brokerSegCollateral'),
    getNumericColumn('brokerSegCashCollateral'),
    getNumericColumn('brokerSegSecuritiesCollateral'),
    getNumericColumn('brokerSegExcessDeficit'),
    getNumericColumn('roundedBrokerExcessDeficit'),

    getNumericColumn('reconLongMarketValue'),
    getNumericColumn('reconShortMarketValue'),
    getNumericColumn('reconMarketValue'),
    getNumericColumn('reconRequirement'),
    getNumericColumn('reconSegRequirement'),
    getNumericColumn('reconExposure'),

    getNumericColumn('exposureDiff'),
    getNumericColumn('requirementDiff'),
    getNumericColumn('collateralDiff'),
    getNumericColumn('cashCollateralDiff'),
    getNumericColumn('securitiesCollateralDiff'),
    getNumericColumn('excessDeficitDiff'),
    getNumericColumn('houseExposureDiff'),
    getNumericColumn('houseRequirementDiff'),
    getNumericColumn('houseCollateralDiff'),
    getNumericColumn('houseCashCollateralDiff'),
    getNumericColumn('houseSecuritiesCollateralDiff'),
    getNumericColumn('houseExcessDeficitDiff'),
    getNumericColumn('houseApplicableRequirement'),
    getNumericColumn('regExposureDiff'),
    getNumericColumn('regRequirementDiff'),
    getNumericColumn('regCollateralDiff'),
    getNumericColumn('regCashCollateralDiff'),
    getNumericColumn('regSecuritiesCollateralDiff'),
    getNumericColumn('regExcessDeficitDiff'),
    getNumericColumn('regApplicableRequirement'),
    getNumericColumn('segExposureDiff'),
    getNumericColumn('segRequirementDiff'),
    getNumericColumn('segCollateralDiff'),
    getNumericColumn('segCashCollateralDiff'),
    getNumericColumn('segSecuritiesCollateralDiff'),
    getNumericColumn('segExcessDeficitDiff'),
    getNumericColumn('segApplicableRequirement'),

    getNumericColumn('prevDayExposure'),
    getNumericColumn('prevDayRequirement'),
    getNumericColumn('prevDayUsage'),
    getNumericColumn('prevDayCollateral'),
    getNumericColumn('prevDayCashCollateral'),
    getNumericColumn('prevDaySecuritiesCollateral'),
    getNumericColumn('prevDayExcessDeficit'),
    getNumericColumn('dayOnDayExposureDiff'),
    getNumericColumn('dayOnDayRequirementDiff'),
    getNumericColumn('dayOnDayUsageDiff'),
    getNumericColumn('dayOnDayCollateralDiff'),
    getNumericColumn('dayOnDayCashCollateralDiff'),
    getNumericColumn('dayOnDaySecuritiesCollateralDiff'),
    getNumericColumn('dayOnDayExcessDeficitDiff'),
    getNumericColumn('actualSecuritiesMovement'),
    getNumericColumn('segActualSecuritiesMovement'),
    getNumericColumn('expectedCashMovement'),
    getNumericColumn('segExpectedCashMovement'),
    getNumericColumn('disputeAmount'),
    getNumericColumn('segDisputeAmount'),

    getWireRequestColumn('wireRequest'),
    getPTGBookingRefColumn('bookingReference', getPtgSearchInstructionHyperlink),
    getPTGBookingRefColumn('bundleTransferBookingReference', getPtgSearchBundleTransferInstructionHyperlink),
    getBrokerAndRagSyncColumn('runBrokerAndRagSync'),
    getStringColumn('physicalCollateralRagStatus'),
  ];
}


function getNumericColumn(identity: string): ArcDataGrid.ColumnProperties<any, any> {
  return {
    identity: identity,
    Cell: (value, context) => {
      let isPublished = context?.row?.workflowStatus === 'PUBLISHED';
      return (
        <RowFormatter isPublished={isPublished} className="text-align--right">
          {plainNumberFormatter(undefined, undefined, value)}
        </RowFormatter>
      );
    },
  };
}

function getStringColumn(identity: string): ArcDataGrid.ColumnProperties<any, any> {
  return {
    identity: identity,
    Cell: (value, context) => {
      let isPublished = context?.row?.workflowStatus === 'PUBLISHED';
      return <RowFormatter isPublished={isPublished}>{value}</RowFormatter>;
    },
  };
}

function getPrefixedIdsString(value, prefix): string {
  let ids = value ? value.split(',') : [];
  let result = "";
  ids.forEach(it => result += (prefix + it + ','));
  return result.slice(0,-1);
}

function getWireRequestColumn(identity: string) {
  return {
    identity: 'wireRequest',
    Cell: (value, context) => {
      let wireRequestResult = getPrefixedIdsString(value, "WRS#");
      let isPublished = context?.row?.workflowStatus === 'PUBLISHED';
      return <RowFormatter isPublished={isPublished}>{wireRequestResult}</RowFormatter>;
    },
    exportAccessor: (row, rowId) => {
      return getPrefixedIdsString(row?.wireRequest, "WRS#");
    }
  }
}

function getPTGBookingRefColumn(identity: string, getUrl: Function) {
  return {
    identity: identity,
    Cell: (value, context) => {
      let bookingReferenceResult = getPrefixedIdsString(value, "PTG#");
      let isPublished = context?.row?.workflowStatus === 'PUBLISHED';
      return (
        <RowFormatter isPublished={isPublished}>
          <a href={getUrl(value)} target="_blank">
            {bookingReferenceResult}
          </a>
        </RowFormatter>
      );
    },
    exportAccessor: (row, rowId) => {
      return getPrefixedIdsString(row?.bookingReference, "PTG#");
    }
  };
}

function getBrokerAndRagSyncColumn(identity: string) {
  return {
    identity: identity,
    Cell: (value, context) => {
      let date = context?.row?.date;
      let agreementId = context?.row?.agreementId;
      let display = RUN_BROKER_AND_RAG_SYNC;
      let isPublished = context?.row?.workflowStatus === 'PUBLISHED';
      let url = BASE_URL + 'service/treasuryEngineSyncExecutorService/executeBrokerAndRagSync?' +
      'startDate=' + date + '&endDate=' + date + '&treasuryAgreementIds=' + agreementId + '&includeAlreadySyncedAgreements=true';
      let isTreasuryAgreement = context?.row?.children;
      return (
        <RowFormatter isPublished={isPublished} isHidden={!isTreasuryAgreement}>
          <a href={url} target="_blank">
            {display}
          </a>
        </RowFormatter>
      );
    }
  };
}

function getPtgSearchInstructionHyperlink(instructionGroupId: number) {
  let url = `${CODEX_PROPERTIES['com.arcesium.treasury.ptg.url']}/searchInstructions?instructionGroupId=${instructionGroupId}`;
  return instructionGroupId ? url : undefined;
}

function getPtgSearchBundleTransferInstructionHyperlink(instructionGroupIds: string) {
  let url = `${CODEX_PROPERTIES['com.arcesium.treasury.ptg.url']}/search-bundle-transfer-instructions?instructionGroupIds=${instructionGroupIds}`;
  return instructionGroupIds ? url : undefined;
}

export function MarginTypes({ value }) {
  return value ? (
    value.split(',').map((item, index) => (
      <span key={index} className="token margin--horizontal">
        {item}
      </span>
    ))
  ) : (
    <></>
  );
}

function AccountTypes({ value }) {
  return value ? (
    value
      .split(',')
      .map((item) => (item in ACCOUNT_TYPES ? ACCOUNT_TYPES[item] : item))
      .join(', ')
  ) : (
    <></>
  );
}

function StatusFlags({ row }) {
  let ragStatusCode = row?.ragStatusCode ? row?.ragStatusCode : 'Unknown';
  let tokenCss = getValueOrDefault(RAG_CATEGORY_CODE_MAP[row?.ragCategoryCode], '');
  let workflowIcon = <></>;
  if (row?.children == undefined) workflowIcon = <></>;
  else if (row?.workflowStatus == 'IN_PROGRESS' || row?.workflowStatus == 'SAVED')
    workflowIcon = <i className="icon-locked margin--right" />;
  else if (row?.workflowStatus == 'PUBLISHED' && row['ragCategoryCode'] == 'CLEAR')
    workflowIcon = <i className="icon-success margin--right" style={{ color: 'white' }} />;
  else if (row?.workflowStatus == 'PUBLISHED')
    workflowIcon = <i className="icon-success margin--right" />;

  let token = (
    <span
      style={{ borderRadius: '1em' }}
      className={'token padding--horizontal--double ' + tokenCss}
    >
      {workflowIcon}
      {ragStatusCode}
    </span>
  );
  return (
    <div className="text-align--center" title={ragStatusCode}>
      {token}
    </div>
  );
}

const ActionPanel: React.FC<any> = ({ context }) => {
  let isTreasuryAgreement = context?.row?.children;
  const areActionsDisabledForAgreement = WORKFLOW_STATUS_TO_LOCK_AGREEMENT_ACTIONS.includes(
    context?.row?.workflowStatus
  );

  return (
    <>
      <i
        title="Begin Workflow"
        hidden={!isTreasuryAgreement}
        className="icon-edit padding--horizontal--small"
        onClick={(e) => {
          e.stopPropagation();
          displayAgreementWorkflow(context.row.id);
        }}
      />
      <ClickableIconWithPause
        title="Quick Publish"
        hidden={!isTreasuryAgreement || areActionsDisabledForAgreement}
        className="icon-quick padding--horizontal--small"
        onClick={(e) => {
          e.stopPropagation();
          quickPublish(context.row.id);
        }}
      />
      <i
        title="Log Wire and Quick Publish"
        hidden={!isTreasuryAgreement || areActionsDisabledForAgreement}
        className="icon-transfer padding--horizontal--small"
        onClick={(e) => {
          e.stopPropagation();
          onClickPostCollateralAndPublish(context.row.id);
        }}
      />
      <i
        title="Post Collateral"
        hidden={isTreasuryAgreement || areActionsDisabledForAgreement}
        className=" icon-transfer padding--horizontal--small"
        onClick={(e) => {
          e.stopPropagation();
          onClickPostCollateralFromGrid(context.row.id);
        }}
      />
      <i
        title="Email Call Report"
        hidden={areActionsDisabledForAgreement}
        className="icon-mail padding--horizontal--small"
        onClick={(e) => {
          e.stopPropagation();
          emailCallReport(context.row.id);
        }}
      />
      <i
        title="Refresh Data"
        hidden={!isTreasuryAgreement}
        className="icon-refresh padding--horizontal--small"
        onClick={agreementSummaryStore.handleSearch}
      />
    </>
  );
};
