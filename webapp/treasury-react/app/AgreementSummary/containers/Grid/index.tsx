import React from "react";
import { observer } from "mobx-react-lite";
import agreementSummaryStore from "../../useStores";
import { Layout, Message } from "arc-react-components";
import ArcDataGrid from "arc-data-grid";
import GridHeader from "../GridHeader";
import { GridLegend } from "../GridLegend";
import { getColumns } from "./grid-columns";

declare global {
  interface Window {
    treasury: any;
  }
}

window.treasury = window.treasury || {};
window.treasury.comet = window.treasury.comet || {};
window.treasury.comet.agreementsummary =
  window.treasury.comet.agreementsummary || {};
window.treasury.comet.agreementsummary.reloadAgreementSummary =
  agreementSummaryStore.handleSearch;

const gridConfig = {
  enableCheckboxes: true,
  clickableRows: true,
  enableTreeView: true,
  getExportFileName: ({ fileType, all }) => {
    return 'agreement-summary';
  }
};

const Grid: React.FC<any> = () => {
  const gridColumns = React.useMemo(getColumns, []);
  const [sortBy, setSortBy] = React.useState([
    {
      identity: "workflowStatus",
      desc: true,
    },
    {
      identity: "ragCategoryCode",
      desc: true,
    },
    "agreementType",
    "agreementId",
  ]);

  const onSortByChange = (sortBy) => {
    setSortBy(sortBy);
  };

  const onColumnChange = (displayColumns) => {
    agreementSummaryStore.setDisplayColumns(displayColumns);
  };

  function renderGridData() {

    let showGrid = agreementSummaryStore.agreementData.length ? true : false;
    let treeData = agreementSummaryStore.agreementDataToShow;

    if (showGrid) {
      let grid = (
        <>
          <ArcDataGrid
            rows={treeData}
            columns={gridColumns}
            configurations={gridConfig}
            getRowId={(row, _) => row.id}
            clickedRow={agreementSummaryStore.clickedRowId}
            onClickedRowChange={agreementSummaryStore.onRowClick}
            selectedRowIds={agreementSummaryStore.selectedRowIds}
            onSelectedRowIdsChange={agreementSummaryStore.setSelectedRowIds}
            displayColumns={agreementSummaryStore.displayColumns}
            onDisplayColumnsChange={onColumnChange}
            sortBy={sortBy}
            onSortByChange={onSortByChange}
          />
          <div
            className="container--primary padding--horizontal padding--vertical--small"
            style={{
              position: "absolute",
              bottom: "-12px",
              left: "50%",
              transform: "translate(-50%, -50%)",
              borderRadius: "5px",
            }}
          >
            <GridLegend />
          </div>
        </>
      );
      let gridWithHeaders = (
        <Layout>
          <Layout.Child childId="quickActionsHeader" size="fit">
            <GridHeader />
          </Layout.Child>
          <Layout.Child childId="agreementWorkflowGrid">{grid}</Layout.Child>
        </Layout>
      );
      return gridWithHeaders;
    } else {
      const messageType = agreementSummaryStore.searchStatus.error
        ? Message.Type.CRITICAL
        : Message.Type.PRIMARY;
      return (
        !agreementSummaryStore.searchStatus.inProgress && (
          <div className="margin--horizontal">
            <Message
              children={agreementSummaryStore.searchStatus.message}
              type={messageType}
            />
          </div>
        )
      );
    }
  }

  return <>{renderGridData()}</>;
};

export default observer(Grid);
