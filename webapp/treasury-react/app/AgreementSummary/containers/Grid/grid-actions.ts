import agreementSummaryStore from "../../useStores";
import { displayPostCollateralPopup, onClickPostCollateral } from "../../utils";
import { getAgreementDetail, getMarginCallType } from "../BottomPanel/utils/agreementDetailUtils";
import { beginAgreementWorkflow, beginWorkflowAndGenerateCallReport, quickWirePublishAgreement, quickPublishAgreement } from "../BottomPanel/utils/workflowUtils";

export function displayAgreementWorkflow(key: number) {
  let clickedRowData = agreementSummaryStore.keyToTrsyAgreementDataMap[key]
  beginAgreementWorkflow(agreementSummaryStore.lastSearchFilter.selectedDate,
    clickedRowData.treasuryAgreementDetail.agreementId,
    clickedRowData.treasuryAgreementDetail.legalEntityId,
    clickedRowData.treasuryAgreementDetail.cpeId,
    clickedRowData.treasuryAgreementDetail.agreementTypeId
  )
}

export function quickPublish(key: number) {
  let clickedRowData = agreementSummaryStore.keyToTrsyAgreementDataMap[key]
  quickPublishAgreement(agreementSummaryStore.lastSearchFilter.selectedDate,
    clickedRowData.treasuryAgreementDetail.legalEntityId,
    clickedRowData.treasuryAgreementDetail.cpeId,
    clickedRowData.treasuryAgreementDetail.agreementTypeId,
    clickedRowData.treasuryAgreementDetail.agreementId
    )
}

export function emailCallReport(key: number) {
  let clickedRowData = agreementSummaryStore.keyToTrsyAgreementDataMap[key]
  beginWorkflowAndGenerateCallReport(agreementSummaryStore.lastSearchFilter.selectedDate,
    clickedRowData.treasuryAgreementDetail.legalEntityId,
    clickedRowData.treasuryAgreementDetail.cpeId,
    clickedRowData.treasuryAgreementDetail.agreementTypeId,
    getAgreementDetail(clickedRowData).agreementId,
    clickedRowData.treasuryAgreementDetail.agreementId)
}

export function onClickPostCollateralFromGrid(key: number) {
  let clickedRowData = agreementSummaryStore.keyToTrsyAgreementDataMap[key]
  const isTreasuryAgreement = clickedRowData?.parentMarginCallTypeEdSummaryDataList
  onClickPostCollateral(agreementSummaryStore.lastSearchFilter.selectedDate, isTreasuryAgreement, clickedRowData)
}

export const onClickPostCollateralAndPublish = (key: number) => {
  let clickedRowData = agreementSummaryStore.keyToTrsyAgreementDataMap[key];
  const isTreasuryAgreement = clickedRowData?.parentMarginCallTypeEdSummaryDataList;

  displayPostCollateralPopup(
    agreementSummaryStore.lastSearchFilter.selectedDate,
    isTreasuryAgreement,
    clickedRowData,
    (wireAmount) =>
      quickWirePublishAgreement(
        agreementSummaryStore.lastSearchFilter.selectedDate,
        clickedRowData.treasuryAgreementDetail.legalEntityId,
        clickedRowData.treasuryAgreementDetail.cpeId,
        clickedRowData.treasuryAgreementDetail.agreementTypeId,
        getAgreementDetail(clickedRowData).agreementId,
        wireAmount,
        getMarginCallType(clickedRowData)?.abbrev
      )
  );
};
