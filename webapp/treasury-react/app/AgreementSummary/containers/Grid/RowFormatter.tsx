import React, { ReactChildren, ReactChild } from "react";

type Props = {
  isPublished?: boolean;
  className?: string;
  children: ReactChild | ReactChildren;
  isHidden?: boolean;
}

export const RowFormatter: React.FC<Props> = (props: Props) => {
  let publishedStyle = props.isPublished == true ? { opacity: 0.75, fontStyle: "italic" } : {}
  return (
    <div className={props.className} style={publishedStyle} hidden={props.isHidden}>
      {props.children}
    </div>
  )
};

RowFormatter.defaultProps = {
  isPublished: false,
  className: "",
  isHidden: false
};
