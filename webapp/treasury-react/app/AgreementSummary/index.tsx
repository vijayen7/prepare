import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { Breadcrumbs, Layout } from 'arc-react-components';
import AppHeader, { whenHeaderRef } from 'arc-header';
import Grid from './containers/Grid';
import SideBar from './containers/SideBar';
import Loader from '../commons/container/Loader';
import { ReactLoader } from '../commons/components/ReactLoader';
import agreementSummaryStore, { agreementWorkflowStore, postCollateralStore } from './useStores';
import { URLS } from './constants';
import BottomPanel from './containers/BottomPanel';
import DialogContainer from '../Lcm/AgreementWorkflow/containers/DialogContainer';
import AddAdjustmentStore from '../Lcm/AgreementWorkflow/stores/AddAdjustmentStore';
import { DEFAULT_COLUMNS } from './containers/Grid/constants'
import { convertToReportManagerColumns } from './utils'

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={'/'} key="collateralManagement">
        Collateral Management
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={URLS.AGREEMENT_SUMMARY} key="agreementSummary">
        Agreement Summary
      </Breadcrumbs.Item>
    </>
  );
};

const AgreementSummary: React.FC = () => {
  useEffect(() => {
    document.title = 'Agreement Summary';
    updateBreadCrumbs();
    setGlobalFilters();
    setFilters();
  }, []);

  const setGlobalFilters = async () => {
    const headerRef = await whenHeaderRef();
    if (headerRef && headerRef.gf) {
      const options = {
        enabled: {
          strategies: true,
          fundFamilies: true,
          legalEntityFamilies: true,
        },
      };
      headerRef.gf.addListener(appHeaderListener);
      headerRef.gf.setOptions(options);
    }
  };

  const setFilters = async () => {
    await agreementSummaryStore.setDefaultDate();
    if (window.location.search !== '') {
      agreementSummaryStore.setFiltersFromUrl(window.location.search);
      agreementSummaryStore.handleSearch();
    }
  }

  function appHeaderListener({ userSelections, expandedSelections }) {
    agreementSummaryStore.setExpandedGlobalSelections(userSelections, expandedSelections);
  }

  agreementSummaryStore.reportManagerColumns = convertToReportManagerColumns(DEFAULT_COLUMNS)

  return (
    <React.Fragment>
      <Loader />
      <ReactLoader
        inProgress={
          agreementSummaryStore.searchStatus.inProgress ||
          postCollateralStore.collateralTermSearchStatus.inProgress ||
          postCollateralStore.inventoryPositionDataSearchStatus.inProgress ||
          postCollateralStore.logWirePopupDataSearchStatus.inProgress ||
          postCollateralStore.bookMmfStatus.inProgress
        }
      />
      <DialogContainer
        postCollateralStore={postCollateralStore}
        addAdjustmentStore={new AddAdjustmentStore(agreementWorkflowStore)}
      />
      <div className="layout--flex--row">
        <AppHeader className="size--content" />
        <div className="layout--flex--row">
          <div className="layout--flex">
            <Layout isColumnType={true}>
              <Layout.Child
                childId="sideBar"
                size={2}
                collapsible
                title="Search Criteria"
                collapsed={agreementSummaryStore.toggleSidebar}
                onChange={agreementSummaryStore.setToggleSidebar}
              >
                <SideBar />
              </Layout.Child>
              <Layout.Divider childId="sideBarDivider" key="sideBarDivider" isResizable />
              <Layout.Child childId="dataDetail" size={9}>
                <Layout>
                  <Layout.Child childId="grid">
                    <Grid />
                  </Layout.Child>
                  <Layout.Divider childId="gridDivider" key="gridDivider" isResizable />
                  <Layout.Child
                    childId="bottomPanel"
                    key="bottomPanel"
                    collapsible
                    collapsed={!agreementSummaryStore.showBottomPanel}
                  >
                    <BottomPanel />
                  </Layout.Child>
                </Layout>
              </Layout.Child>
            </Layout>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default observer(AgreementSummary);
