import MapData from '../commons/models/MapData';

export interface SearchFilter {
  selectedDate: string;
  selectedCpes: MapData[];
  selectedLegalEntityFamilies: MapData[];
  selectedLegalEntities: MapData[];
  selectedAgreementTypes: MapData[];
  selectedCurrency: MapData;
  workflowStatus: string[];
  exceptionStatus: string[];
}

export interface SearchStatus{
  inProgress: boolean;
  error: boolean;
  message: string;
}

export interface FilterLoadStatus{
  inProgress: boolean;
  error: boolean;
}

export interface BrokerFileDetail {
  brokerFileUrl: string,
  brokerDataLevelId: number
}

export interface ReportManagerSaveSettingData {
  columns : {id : string}[]
}
