import { BASE_URL , URL} from '../commons/constants';
import { fetchPostURL, fetchURL } from "../commons/util";

export const fetchAgreementSummaryData = (input) => {
  const url = `${BASE_URL}service/agreementSummaryManager/getExcessDeficitSummaryData`
  let param = encodeURIComponent(JSON.stringify(input))
  let encodedParam = "treasuryAgreementExcessDeficitSummaryFilter=" + param + "&inputFormat=json&format=json"

  return fetchPostURL(url, encodedParam)
};

export const isRedirectToOldPdrScreen = () => {
  const url = `${BASE_URL}service/lcmFeatureToggle/isRedirectToOldPdrScreen?format=json`;
  return fetchURL(url);
};

export const getAgreementSummaryCreateReportUrl = (input) => {
  let url = `${BASE_URL}service/agreementSummaryManager/getExcessDeficitSummaryDataView?filter.date=${input.date}`
  url += input.legalEntityFamilyIds.length ? `&filter.legalEntityFamilyIds=${input.legalEntityFamilyIds}` : ``;
  url += input.legalEntityIds.length ? `&filter.legalEntityIds=${input.legalEntityIds}` : ``;
  url += input.cpeIds.length ? `&filter.cpeIds=${input.cpeIds}` : ``;
  url += input.agreementTypeIds.length ? `&filter.agreementTypeIds=${input.agreementTypeIds}` : ``;
  url += input.workflowStatusList?.length ? `&filter.workflowStatusList=${input.workflowStatusList}` : ``;
  url += input.exceptionStatusList?.length ? `&filter.exceptionStatusList=${input.exceptionStatusList}` : ``;
  url += `&inputFormat=properties&format=json`
  return url;
}


export const updateSavedSetting = (savedSetting) => {
  let url = `${URL}/service/SettingsService/updateSavedSetting`
  let param = encodeURIComponent(JSON.stringify(savedSetting))
  let encodedParam = "savedSetting=" + param + "&inputFormat=json&format=json"

  return fetchPostURL(url, encodedParam)
}
