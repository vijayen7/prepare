import MapData from '../commons/models/MapData';
import { MESSAGES as BOTTOM_PANEL_MESSAGES } from "./containers/BottomPanel/constants"
export const BUNDLE_AGREEMENT_ACTION = 'BUNDLE_AGREEMENT_ACTION';
export const REFRESH_ACTION = 'REFRESH_ACTION';
export const AMEND_ACTION = 'AMEND_ACTION';
export const PNL_SPN_ACTION = 'PNL_SPN_ACTION';

export const URLS = {
  AGREEMENT_SUMMARY: "/treasury/comet/agreementSummary.html",
};

export const currencyFilterData: MapData[] = [
  { key: 0, value: "USD" },
  { key: 1, value: "Reporting Currency" },
]

export const REPORTING_CURRENCY = { key: 1, value: "Reporting Currency" }

export const CURRENCY_SPN_MAP = {
  "USD": 1760000,
  "Reporting Currency": -1
}

export const TOAST_TYPE = {
  INFO: "info",
  SUCCESS: "success",
  CRITICAL: "critical",
};

export const DATA_FIELD_NAMES: string[] = ["exposure", "requirement", "usage", "collateral", "cashCollateral", "securitiesCollateral", "excessDeficit", "applicableRequirement", "applicableExcessDeficit"]
export const RECON_DATA_FIELD_NAMES: string[] = ["exposure", "requirement", "marketValue", "longMarketValue", "shortMarketValue"];

export const INTERNAL_ONLY_DATA_FIELD_NAMES: string[] = ["usage"]
export const MARGIN_TYPES = {
  HOUSE: "house",
  REG: "reg",
  SEG: "seg"
}

export const MESSAGES = {
  INITIAL_SEARCH_MESSAGE: "Perform search to view Agreement Workflow Summary",
  NO_DATA_FOUND_MESSAGE:
    "No Agreement Worklow found for the performed search",
  ERROR_MESSAGE: "Error Occurred while fetching Agreement Workflow Summary",
  DATA_FOUND_MESSAGE: "Data found",
  COPY_SEARCH_URL_SUCCESS: "Search URL copied to clipboard",
  LAUNCH_WINDOW_FAILURE: "Please disable the pop-up block option in the browser settings.",
  WORKFLOW_EXECUTE_FAILURE: "Error occurred while Workflow execution",
  WORKFLOW_CANCEL_FAILURE: "Error occurred while cancelling Workflow",
  CONFIRM_WORKLFOW_CANCEL: "Do you really want to cancel the workflow ?",
  CREATE_REPORT_FAILURE: "Error occured during Create Report",
  REPORT_MANAGER_URL_LIMIT_CROSS: "URL length exceeds the MAX Char limit for the given input. Generating report for all Legal Entities",
  ...BOTTOM_PANEL_MESSAGES
};

export const CLASSES = {
  TREASURY_AGREEMENT_EXCESS_DEFICIT_SUMMARY_FILTER: "deshaw.treasury.common.model.lcm.TreasuryAgreementExcessDeficitSummaryFilter"
}

export const ACCOUNT_TYPES = {
  VM: "VM",
  IM: "IM",
  VM_PLUS_IM: "VM + IM",
  SIMM_LE: "SIMM LE",
  SIMM_CPE: "SIMM CPE"
}

export const INITIAL_SEARCH_STATUS = {
  inProgress: false,
  error: false,
  message: MESSAGES.INITIAL_SEARCH_MESSAGE
};

export const INITIAL_FILTER_LOAD_STATUS = {
  inProgress: false,
  error: false
};

export const INITIAL_FILTERS = {
  selectedLegalEntityFamilies: [],
  selectedLegalEntities: [],
  selectedCpes: [],
  selectedAgreementTypes: [],
  selectedCurrency: REPORTING_CURRENCY,
  workflowStatus: [],
  exceptionStatus: []
};

export const RAG_CATEGORY_CODE_MAP = {
  "CLEAR": "success",
  "WARNING": "warning",
  "CRITICAL": "critical",
  "BROKER_DATA_MISSING": "primary"
}

export const INITIAL_QF_COUNTER = {
  "PUBLISHED": 0,
  "IN_PROGRESS": 0,
  "CLEAR": 0,
  "WARNING": 0,
  "CRITICAL": 0,
  "BROKER_DATA_MISSING": 0
}

export const CATEGORY_RANK = {
  "PUBLISHED": 1,
  "SAVED": 2,
  "IN_PROGRESS": 2,
  "NOT_STARTED": 3,
  "CLEAR": 4,
  "BROKER_DATA_MISSING": 5,
  "WARNING": 6,
  "CRITICAL": 7,
}

export enum CATEGORY {
  "PUBLISHED" = "Published",
  "IN_PROGRESS" = "In-Progress",
  "NOT_STARTED" = "Not Started",
  "CLEAR" = "Clear",
  "BROKER_DATA_MISSING" = "Broker Data Missing",
  "WARNING" = "Warning",
  "CRITICAL" = "Critical",
}

export const PUBLISHED = "PUBLISHED"
export const IN_PROGRESS = "IN_PROGRESS"
export const NOT_STARTED = "NOT_STARTED"
export const CLEAR = "CLEAR"
export const BROKER_DATA_MISSING = "BROKER_DATA_MISSING"
export const WARNING = "WARNING"
export const CRITICAL = "CRITICAL"

export const BROKER_DATA_LEVEL_MAP = {
  1: "AGREEMENT",
  2: "ACCOUNT",
  3: "POSITION"
}

// Actions should be disabled for Agreements having these workflow statuses
export const WORKFLOW_STATUS_TO_LOCK_AGREEMENT_ACTIONS = ['IN_PROGRESS', 'SAVED', 'PUBLISHED']

export const RUN_BROKER_AND_RAG_SYNC = "Run Broker and Rag Sync"

export const gridColumnsToReportManagerColumnsMap = {
  "agreementId": ["agreementId"],
  "ragStatusCode": ["ragStatusDetail.ragStatus"],
  "legalEntity": ["legalEntityName"],
  "exposureCounterparty": ["cpeName"],
  "agreementType": ["agreementTypeName"],
  "reportingCurrency": ["reportingCurrency"],
  "marginCallType": ["marginCallType"],
  "callType": ["applicableData.accountType", "applicableDataRC.accountType"],
  "exposure": ["applicableData.exposure", "applicableData.adjustedExposure", "applicableDataRC.exposure", "applicableDataRC.adjustedExposure"],
  "requirement": ["applicableData.requirement", "applicableData.adjustedRequirement", "applicableDataRC.requirement", "applicableDataRC.adjustedRequirement"],
  "applicableRequirement": ["applicableData.applicableRequirement", "applicableData.adjustedApplicableRequirement", "applicableDataRC.applicableRequirement", "applicableDataRC.adjustedApplicableRequirement"],
  "usage": ["applicableData.usage", "applicableData.adjustedUsage", "applicableDataRC.usage", "applicableDataRC.adjustedUsage"],
  "collateral": ["applicableData.collateral", "applicableData.adjustedCollateral", "applicableDataRC.collateral", "applicableDataRC.adjustedCollateral"],
  "cashCollateral": ["applicableData.cashCollateral", "applicableData.adjustedCashCollateral", "applicableDataRC.cashCollateral", "applicableDataRC.adjustedCashCollateral"],
  "securitiesCollateral": ["applicableData.securitiesCollateral", "applicableData.adjustedSecuritiesCollateral", "applicableDataRC.securitiesCollateral", "applicableDataRC.adjustedSecuritiesCollateral"],
  "securitiesCollateralMargin": ["applicableData.securitiesCollateralMargin", "applicableData.adjustedSecuritiesCollateralMargin", "applicableDataRC.securitiesCollateralMargin", "applicableDataRC.adjustedSecuritiesCollateralMargin"],
  "actualSecuritiesCollateralMargin": ["applicableData.actualSecuritiesCollateralMargin", "applicableData.adjustedActualSecuritiesCollateralMargin", "applicableDataRC.actualSecuritiesCollateralMargin", "applicableDataRC.adjustedActualSecuritiesCollateralMargin"],
  "excessDeficit": ["applicableData.excessDeficit", "applicableData.adjustedExcessDeficit", "applicableDataRC.excessDeficit", "applicableDataRC.adjustedExcessDeficit"],
  "regExcessDeficit": ["regExcessDeficitData.excessDeficit", "regExcessDeficitData.adjustedExcessDeficit", "regExcessDeficitDataRC.excessDeficit", "regExcessDeficitDataRC.adjustedExcessDeficit"],
  "houseExcessDeficit": ["houseExcessDeficitData.excessDeficit", "houseExcessDeficitData.adjustedExcessDeficit", "houseExcessDeficitDataRC.excessDeficit", "houseExcessDeficitDataRC.adjustedExcessDeficit"],
  "brokerExposure": ["applicableData.brokerExposure", "applicableDataRC.brokerExposure"],
  "brokerRequirement": ["applicableData.brokerRequirement", "applicableDataRC.brokerRequirement"],
  "brokerCollateral": ["applicableData.brokerCollateral", "applicableDataRC.brokerCollateral"],
  "brokerCashCollateral": ["applicableData.brokerCashCollateral", "applicableDataRC.brokerCashCollateral"],
  "brokerSecuritiesCollateral": ["applicableData.brokerSecuritiesCollateral", "applicableDataRC.brokerSecuritiesCollateral"],
  "brokerExcessDeficit": ["applicableData.brokerExcessDeficit", "applicableDataRC.brokerExcessDeficit"],
  "exposureDiff": ["applicableData.exposureDiff", "applicableDataRC.exposureDiff"],
  "requirementDiff": ["applicableData.requirementsDiff", "applicableDataRC.requirementsDiff"],
  "collateralDiff": ["applicableData.collateralDiff", "applicableDataRC.collateralDiff"],
  "cashCollateralDiff": ["applicableData.cashCollateralDiff", "applicableDataRC.cashCollateralDiff"],
  "securitiesCollateralDiff": ["applicableData.securitiesCollateralDiff", "applicableDataRC.securitiesCollateralDiff"],
  "excessDeficitDiff": ["applicableData.excessDeficitDiff", "applicableDataRC.excessDeficitDiff"],
  "expectedCashMovement": ["applicableData.expectedCashMovement", "applicableDataRC.expectedCashMovement"],
  "segExpectedCashMovement": ["segExcessDeficitData.expectedCashMovement", "segExcessDeficitDataRC.expectedCashMovement"],
  "disputeAmount": ["applicableData.disputeAmount", "applicableDataRC.disputeAmount"],
  "wireRequest": ["wireRequestNumbers"],
  "bookingReference": ["bookingReferenceIds"],
  "workflowStatus": ["workFlowDetails.workflowStatus.name"],
  "ragCategoryCode": ["ragStatusDetail.ragCategoryCode"],
  "legalEntityId": ["legalEntityId"],
  "exposureCounterpartyId": ["cpeId"],
  "agreementTypeId": ["agreementTypeId"],
  "currency": [""],
  "date": ["date"],
  "houseExposure": ["houseExcessDeficitData.exposure", "houseExcessDeficitData.adjustedExposure", "houseExcessDeficitDataRC.exposure", "houseExcessDeficitDataRC.adjustedExposure"],
  "houseRequirement": ["houseExcessDeficitData.requirement", "houseExcessDeficitData.adjustedRequirement", "houseExcessDeficitDataRC.requirement", "houseExcessDeficitDataRC.adjustedRequirement"],
  "houseUsage": ["houseExcessDeficitData.usage", "houseExcessDeficitData.adjustedUsage", "houseExcessDeficitDataRC.usage", "houseExcessDeficitDataRC.adjustedUsage"],
  "houseCollateral": ["houseExcessDeficitData.collateral", "houseExcessDeficitData.adjustedCollateral", "houseExcessDeficitDataRC.collateral", "houseExcessDeficitDataRC.adjustedCollateral"],
  "houseCashCollateral": ["houseExcessDeficitData.cashCollateral", "houseExcessDeficitData.adjustedCashCollateral", "houseExcessDeficitDataRC.cashCollateral", "houseExcessDeficitDataRC.adjustedCashCollateral"],
  "houseSecuritiesCollateral": ["houseExcessDeficitData.securitiesCollateral", "houseExcessDeficitData.adjustedSecuritiesCollateral", "houseExcessDeficitDataRC.securitiesCollateral", "houseExcessDeficitDataRC.adjustedSecuritiesCollateral"],
  "regExposure": ["regExcessDeficitData.exposure", "regExcessDeficitData.adjustedExposure", "regExcessDeficitDataRC.exposure", "regExcessDeficitDataRC.adjustedExposure"],
  "regRequirement": ["regExcessDeficitData.requirement", "regExcessDeficitData.adjustedRequirement", "regExcessDeficitDataRC.requirement", "regExcessDeficitDataRC.adjustedRequirement"],
  "regUsage": ["regExcessDeficitData.usage", "regExcessDeficitData.adjustedUsage", "regExcessDeficitDataRC.usage", "regExcessDeficitDataRC.adjustedUsage"],
  "regCollateral": ["regExcessDeficitData.collateral", "regExcessDeficitData.adjustedCollateral", "regExcessDeficitDataRC.collateral", "regExcessDeficitDataRC.adjustedCollateral"],
  "regCashCollateral": ["regExcessDeficitData.cashCollateral", "regExcessDeficitData.adjustedCashCollateral", "regExcessDeficitDataRC.cashCollateral", "regExcessDeficitDataRC.adjustedCashCollateral"],
  "regSecuritiesCollateral": ["regExcessDeficitData.securitiesCollateral", "regExcessDeficitData.adjustedSecuritiesCollateral", "regExcessDeficitDataRC.securitiesCollateral", "regExcessDeficitDataRC.adjustedSecuritiesCollateral"],
  "segExposure": ["segExcessDeficitData.exposure", "segExcessDeficitData.adjustedExposure", "segExcessDeficitDataRC.exposure", "segExcessDeficitDataRC.adjustedExposure"],
  "segRequirement": ["segExcessDeficitData.requirement", "segExcessDeficitData.adjustedRequirement", "segExcessDeficitDataRC.requirement", "segExcessDeficitDataRC.adjustedRequirement"],
  "segUsage": ["segExcessDeficitData.usage", "segExcessDeficitData.adjustedUsage", "segExcessDeficitDataRC.usage", "segExcessDeficitDataRC.adjustedUsage"],
  "segCollateral": ["segExcessDeficitData.collateral", "segExcessDeficitData.adjustedCollateral", "segExcessDeficitDataRC.collateral", "segExcessDeficitDataRC.adjustedCollateral"],
  "segCashCollateral": ["segExcessDeficitData.cashCollateral", "segExcessDeficitData.adjustedCashCollateral", "segExcessDeficitDataRC.cashCollateral", "segExcessDeficitDataRC.adjustedCashCollateral"],
  "segSecuritiesCollateral": ["segExcessDeficitData.securitiesCollateral", "segExcessDeficitData.adjustedSecuritiesCollateral", "segExcessDeficitDataRC.securitiesCollateral", "segExcessDeficitDataRC.adjustedSecuritiesCollateral"],
  "segExcessDeficit": ["segExcessDeficitData.excessDeficit", "segExcessDeficitData.adjustedExcessDeficit", "segExcessDeficitDataRC.excessDeficit", "segExcessDeficitDataRC.adjustedExcessDeficit"],
  "brokerHouseExposure": ["houseExcessDeficitData.brokerExposure", "houseExcessDeficitDataRC.brokerExposure"],
  "brokerHouseRequirement": ["houseExcessDeficitData.brokerRequirement", "houseExcessDeficitDataRC.brokerRequirement"],
  "brokerHouseCollateral": ["houseExcessDeficitData.brokerCollateral", "houseExcessDeficitDataRC.brokerCollateral"],
  "brokerHouseCashCollateral": ["houseExcessDeficitData.brokerCashCollateral", "houseExcessDeficitDataRC.brokerCashCollateral"],
  "brokerHouseSecuritiesCollateral": ["houseExcessDeficitData.brokerSecuritiesCollateral", "houseExcessDeficitDataRC.brokerSecuritiesCollateral"],
  "brokerHouseExcessDeficit": ["houseExcessDeficitData.brokerExcessDeficit", "houseExcessDeficitDataRC.brokerExcessDeficit"],
  "brokerRegExposure": ["regExcessDeficitData.brokerExposure", "regExcessDeficitDataRC.brokerExposure"],
  "brokerRegRequirement": ["regExcessDeficitData.brokerRequirement", "regExcessDeficitDataRC.brokerRequirement"],
  "brokerRegCollateral": ["regExcessDeficitData.brokerCollateral", "regExcessDeficitDataRC.brokerCollateral"],
  "brokerRegCashCollateral": ["regExcessDeficitData.brokerCashCollateral", "regExcessDeficitDataRC.brokerCashCollateral"],
  "brokerRegSecuritiesCollateral": ["regExcessDeficitData.brokerSecuritiesCollateral", "regExcessDeficitDataRC.brokerSecuritiesCollateral"],
  "brokerRegExcessDeficit": ["regExcessDeficitData.brokerExcessDeficit", "regExcessDeficitDataRC.brokerExcessDeficit"],
  "brokerSegExposure": ["segExcessDeficitData.brokerExposure", "segExcessDeficitDataRC.brokerExposure"],
  "brokerSegRequirement": ["segExcessDeficitData.brokerRequirement", "segExcessDeficitDataRC.brokerRequirement"],
  "brokerSegCollateral": ["segExcessDeficitData.brokerCollateral", "segExcessDeficitDataRC.brokerCollateral"],
  "brokerSegCashCollateral": ["segExcessDeficitData.brokerCashCollateral", "segExcessDeficitDataRC.brokerCashCollateral"],
  "brokerSegSecuritiesCollateral": ["segExcessDeficitData.brokerSecuritiesCollateral", "segExcessDeficitDataRC.brokerSecuritiesCollateral"],
  "brokerSegExcessDeficit": ["segExcessDeficitData.brokerExcessDeficit", "segExcessDeficitDataRC.brokerExcessDeficit"],
  "roundedBrokerExcessDeficit": ["applicableData.roundedBrokerExcessDeficit", "applicableDataRC.roundedBrokerExcessDeficit"],
  "reconLongMarketValue": ["applicableData.reconLongMarketValue", "applicableDataRC.reconLongMarketValue"],
  "reconShortMarketValue": ["applicableData.reconShortMarketValue", "applicableDataRC.reconShortMarketValue"],
  "reconMarketValue": ["applicableData.reconMarketValue", "applicableDataRC.reconMarketValue"],
  "reconRequirement": ["applicableData.reconRequirement", "applicableDataRC.reconRequirement"],
  "reconSegRequirement": ["segExcessDeficitData.reconRequirement", "segExcessDeficitDataRC.reconRequirement"],
  "reconExposure": ["applicableData.reconExposure", "applicableDataRC.reconExposure"],
  "houseExposureDiff": ["houseExcessDeficitData.exposureDiff", "houseExcessDeficitDataRC.exposureDiff"],
  "houseRequirementDiff": ["houseExcessDeficitData.requirementsDiff", "houseExcessDeficitDataRC.requirementsDiff"],
  "houseCollateralDiff": ["houseExcessDeficitData.collateralDiff", "houseExcessDeficitDataRC.collateralDiff"],
  "houseCashCollateralDiff": ["houseExcessDeficitData.cashCollateralDiff", "houseExcessDeficitDataRC.cashCollateralDiff"],
  "houseSecuritiesCollateralDiff": ["houseExcessDeficitData.securitiesCollateralDiff", "houseExcessDeficitDataRC.securitiesCollateralDiff"],
  "houseExcessDeficitDiff": ["houseExcessDeficitData.excessDeficitDiff", "houseExcessDeficitDataRC.excessDeficitDiff"],
  "houseApplicableRequirement": ["houseExcessDeficitData.applicableRequirement", "houseExcessDeficitData.adjustedApplicableRequirement", "houseExcessDeficitDataRC.applicableRequirement", "houseExcessDeficitDataRC.adjustedApplicableRequirement"],
  "regExposureDiff": ["regExcessDeficitData.exposureDiff", "regExcessDeficitDataRC.exposureDiff"],
  "regRequirementDiff": ["regExcessDeficitData.requirementsDiff", "regExcessDeficitDataRC.requirementsDiff"],
  "regCollateralDiff": ["regExcessDeficitData.collateralDiff", "regExcessDeficitDataRC.collateralDiff"],
  "regCashCollateralDiff": ["regExcessDeficitData.cashCollateralDiff", "regExcessDeficitDataRC.cashCollateralDiff"],
  "regSecuritiesCollateralDiff": ["regExcessDeficitData.securitiesCollateralDiff", "regExcessDeficitDataRC.securitiesCollateralDiff"],
  "regExcessDeficitDiff": ["regExcessDeficitData.excessDeficitDiff", "regExcessDeficitDataRC.excessDeficitDiff"],
  "regApplicableRequirement": ["regExcessDeficitData.applicableRequirement", "regExcessDeficitData.adjustedApplicableRequirement", "regExcessDeficitDataRC.applicableRequirement", "regExcessDeficitDataRC.adjustedApplicableRequirement"],
  "segExposureDiff": ["segExcessDeficitData.exposureDiff", "segExcessDeficitDataRC.exposureDiff"],
  "segRequirementDiff": ["segExcessDeficitData.requirementsDiff", "segExcessDeficitDataRC.requirementsDiff"],
  "segCollateralDiff": ["segExcessDeficitData.collateralDiff", "segExcessDeficitDataRC.collateralDiff"],
  "segCashCollateralDiff": ["segExcessDeficitData.cashCollateralDiff", "segExcessDeficitDataRC.cashCollateralDiff"],
  "segSecuritiesCollateralDiff": ["segExcessDeficitData.securitiesCollateralDiff", "segExcessDeficitDataRC.securitiesCollateralDiff"],
  "segExcessDeficitDiff": ["segExcessDeficitData.excessDeficitDiff", "segExcessDeficitDataRC.excessDeficitDiff"],
  "segApplicableRequirement": ["segExcessDeficitData.applicableRequirement", "segExcessDeficitData.adjustedApplicableRequirement", "segExcessDeficitDataRC.applicableRequirement", "segExcessDeficitDataRC.adjustedApplicableRequirement"],
  "prevDayExposure": ["prevDayApplicableData.exposure", "prevDayApplicableData.adjustedExposure", "prevDayApplicableDataRC.exposure", "prevDayApplicableDataRC.adjustedExposure"],
  "prevDayRequirement": ["prevDayApplicableData.requirement", "prevDayApplicableData.adjustedRequirement", "prevDayApplicableDataRC.requirement", "prevDayApplicableDataRC.adjustedRequirement"],
  "prevDayUsage": ["prevDayApplicableData.usage", "prevDayApplicableData.adjustedUsage", "prevDayApplicableDataRC.usage", "prevDayApplicableDataRC.adjustedUsage"],
  "prevDayCollateral": ["prevDayApplicableData.collateral", "prevDayApplicableData.adjustedCollateral", "prevDayApplicableDataRC.collateral", "prevDayApplicableDataRC.adjustedCollateral"],
  "prevDayCashCollateral": ["prevDayApplicableData.cashCollateral", "prevDayApplicableData.adjustedCashCollateral", "prevDayApplicableDataRC.cashCollateral", "prevDayApplicableDataRC.adjustedCashCollateral"],
  "prevDaySecuritiesCollateral": ["prevDayApplicableData.securitiesCollateral", "prevDayApplicableData.adjustedSecuritiesCollateral", "prevDayApplicableDataRC.securitiesCollateral", "prevDayApplicableDataRC.adjustedSecuritiesCollateral"],
  "prevDayExcessDeficit": ["prevDayApplicableData.excessDeficit", "prevDayApplicableData.adjustedExcessDeficit", "prevDayApplicableDataRC.excessDeficit", "prevDayApplicableDataRC.adjustedExcessDeficit"],
  "dayOnDayExposureDiff": [""],
  "dayOnDayRequirementDiff": [""],
  "dayOnDayUsageDiff": [""],
  "dayOnDayCollateralDiff": [""],
  "dayOnDayCashCollateralDiff": [""],
  "dayOnDaySecuritiesCollateralDiff": [""],
  "dayOnDayExcessDeficitDiff": [""],
  "actualSecuritiesMovement": ["applicableData.actualSecuritiesMovement", "applicableDataRC.actualSecuritiesMovement"],
  "segActualSecuritiesMovement": ["segExcessDeficitData.actualSecuritiesMovement", "segExcessDeficitDataRC.actualSecuritiesMovement"],
  "segDisputeAmount": ["segExcessDeficitData.disputeAmount", "segExcessDeficitDataRC.disputeAmount"]
}
