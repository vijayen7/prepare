import { put, takeEvery, call, all } from 'redux-saga/effects';
import { START_LOADING, END_LOADING, HANDLE_EXCEPTION } from 'commons/constants';
import { 
    FETCH_OUTPERFORMANCE_DATA,
    RESET_OUTPERFORMANCE_DATA_SEARCH_MESSAGE,
    FETCH_OUTPERFORMANCE_SIMULATE_DATA,
    SAVE_OUTPERFORMANCE_SIMULATE_DATA,
    DELETE_OUTPERFORMANCE_RULE,
    SET_SIMULATE_FLAG,
    SAVE_OUTPERFORMANCE_DATA_SEARCH_PAYLOAD
} from "./constants";
import { 
  getOutperformanceData,
  getSimulateData,
  fetchSaveSimulateData,
  deleteOutperformanceData
 } from './api';

function* fetchOutperformanceData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getOutperformanceData, action.payload);
    const payload = action.payload;
    yield put({ type: `${action.type}_SUCCESS`, data });
    yield put({ type: SAVE_OUTPERFORMANCE_DATA_SEARCH_PAYLOAD, payload });

  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${action.type}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchSimulateData(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(getSimulateData, action.payload);
    
    yield put({
      type: SET_SIMULATE_FLAG,
      data
    });
    yield put({
      type: `${FETCH_OUTPERFORMANCE_SIMULATE_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_OUTPERFORMANCE_SIMULATE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* saveSimulateData(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(fetchSaveSimulateData, action.payload);
    yield put({
      type: `${SAVE_OUTPERFORMANCE_SIMULATE_DATA}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SAVE_OUTPERFORMANCE_SIMULATE_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deleteOutperformanceRuleData(action) {
  let data = [];
  try {
    yield put({ type: START_LOADING });
    data = yield call(deleteOutperformanceData, action.payload);
    yield put({
      type: `${DELETE_OUTPERFORMANCE_RULE}_SUCCESS`,
      data
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_OUTPERFORMANCE_RULE}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchOutperformanceDataSaga() {
  yield takeEvery(FETCH_OUTPERFORMANCE_DATA, fetchOutperformanceData);
}

export function* simulateData() {
  yield [takeEvery(FETCH_OUTPERFORMANCE_SIMULATE_DATA, fetchSimulateData)];
}

export function* saveData() {
  yield [takeEvery(SAVE_OUTPERFORMANCE_SIMULATE_DATA, saveSimulateData)];
}

export function* deleteData() {
  yield [takeEvery(DELETE_OUTPERFORMANCE_RULE, deleteOutperformanceRuleData)];
}


function* OutperformanceSaga() {
  yield all([fetchOutperformanceDataSaga(), simulateData(), saveData(), deleteData()]);
}

export default OutperformanceSaga;

