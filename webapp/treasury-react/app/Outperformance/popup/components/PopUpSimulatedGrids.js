import React, { Component } from 'react';
import { Layout } from 'arc-react-components';

import FilterButton from 'commons/components/FilterButton';
import OutperformanceSimulateGrid from 'commons/components/Grid';
import _ from 'lodash';
import OutperformanceCheckboxGrid from 'commons/components/GridWithCheckbox';
import Message from 'commons/components/Message';
import { getOutperformanceColumns, outperformanceSimulatedColumns } from '../../gridConfig/outperformanceColumnConfig';
import { outperformanceGridOptions, getSimulatedGridOptions } from './../../gridConfig/gridOptions';

export default class PopUpSimulatedGrids extends Component {
  constructor(props) {
    super(props);
    this.renderData = this.renderData.bind(this);
  }

  renderData() {
    if (!this.props.isSimulate) {
      return null;
    }
    let existingRules = null;
    let simulatedRules = null;
    const existingList = this.props.simulatedData;
    const simulatedList = [this.props.popupData];

    if (typeof existingList === 'undefined' || typeof simulatedList === 'undefined') {
      return null;
    }
    //Existing rules similar to the search query present in rule DB
    if (existingList.length > 0) {
      existingRules = (
        <OutperformanceSimulateGrid
          data={existingList}
          gridId="ExistingRules"
          gridColumns={getOutperformanceColumns()}
          gridOptions={outperformanceGridOptions()}
          label="Existing Rules"
        />
      );
    }
    //The rule which user is trying to simulate and add in the rule DB
    if (simulatedList.length > 0) {
      simulatedRules = (
        <OutperformanceCheckboxGrid
          data={simulatedList}
          gridId="SimulatedRules"
          gridColumns={outperformanceSimulatedColumns()}
          gridOptions={getSimulatedGridOptions()}
          label="Simulated Rules"
          onSelect={this.props.onSelect}
          preSelectAllRows={false}
          heightValue={-1}
        />
      );
    }
    return (
        <Layout isRowType>
          <Layout.Child>
            {existingRules}
            <hr />
          </Layout.Child>
          <Layout.Child>
            {simulatedRules}
            {this.props.saveSelectMessage ? (
              <Message messageData="Please select one or more rules to save." error />
            ) : null}
          </Layout.Child>
          <Layout.Child>
            <FilterButton onClick={this.props.handleSave} reset={false} label="Save" />
          </Layout.Child>
        </Layout>
    );
  }

  render() {
    return this.renderData();
  }
}
