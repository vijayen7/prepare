import React, { Component } from 'react';
import _ from 'lodash';
import { Layout } from 'arc-react-components';
import DateFilter from './../../../commons/container/DateFilter';
import CpeFilter from './../../../commons/container/CpeFilter';
import BundleFilter from './../../../commons/container/BundleFilter';
import RegionSpecificCountryFilter from './../../../commons/container/RegionSpecificCountryFilter';
import FilterButton from './../../../commons/components/FilterButton';
import InputNumberFilter from './../../../commons/components/InputNumberFilter';
import OutperformanceTypeFilter from './../../../commons/container/OutperformanceTypeFilter';
import RegionFilter from './../../../commons/container/RegionFilter';
import { getValueForSingleSelect } from '../../util';

const OutperformancePopupFilters = props => {

    let selectedRegionId = getValueForSingleSelect(props.state.selectedRegions);
    return (
        <Layout isRowType>
            <Layout.Child>
                <Layout isColumnType>
                    <Layout.Child size={1} className="padding--top padding--left">
                        <DateFilter
                        layout="standard"
                        onSelect={props.onSelect}
                        stateKey="selectedDate"
                        data={props.state.selectedDate}
                        label="Date"
                        />
                    </Layout.Child>
                    <Layout.Child size={1}>
                        <OutperformanceTypeFilter
                        readonly={props.isDeleteClicked || props.isEditClicked}
                        onSelect={props.onSelect}
                        selectedData={props.state.selectedOutperformanceTypes}
                        stateKey="selectedOutperformanceTypes"
                        multiSelect={false}
                        />
                    </Layout.Child>
                    <Layout.Child size={1}>
                        <RegionFilter
                          onSelect={props.onSelect}
                          readonly={props.isDeleteClicked || props.isEditClicked}
                          selectedData={props.state.selectedRegions}
                          multiSelect={false}
                          label={'Region'}
                        />
                    </Layout.Child>
                    <Layout.Child size={1}>
                        <RegionSpecificCountryFilter
                        readonly={props.isDeleteClicked || props.isEditClicked}
                        onSelect={props.onSelect}
                        selectedData={props.state.selectedCountries}
                        multiSelect={false}
                        regionId={selectedRegionId}
                        />
                    </Layout.Child>
                </Layout>
            </Layout.Child>
            <Layout.Child>
                <Layout isColumnType>
                    <Layout.Child size={1}>
                        <CpeFilter
                        readonly={props.isDeleteClicked || props.isEditClicked}
                        onSelect={props.onSelect}
                        selectedData={props.state.selectedCpes}
                        multiSelect={false}
                        />
                    </Layout.Child>
                    <Layout.Child size={1}>
                        <BundleFilter
                        readonly={props.isDeleteClicked || props.isEditClicked}
                        onSelect={props.onSelect}
                        selectedData={props.state.selectedBundles}
                        multiSelect={false}
                        />
                    </Layout.Child>
                    <Layout.Child size={1} className="padding--top padding--left">
                        <InputNumberFilter
                        onSelect={props.onSelect}
                        data={props.state.longGcRate}
                        label="Long GC Rate (bps)"
                        stateKey="longGcRate"
                        style={{ width: '100%' }}
                        isStandard
                        isError={props.state.error.longGcRate}
                        readonly={props.isDeleteClicked || (!_.isEmpty(props.state.selectedOutperformanceTypes) && props.state.selectedOutperformanceTypes.key == 2)}
                        />
                    </Layout.Child>
                    <Layout.Child size={1} className="padding--top padding--left">
                        <InputNumberFilter
                        onSelect={props.onSelect}
                        data={props.state.shortGcRate}
                        label="Short GC Rate (bps)"
                        stateKey="shortGcRate"
                        style={{ width: '100%' }}
                        isStandard
                        isError={props.state.error.shortGcRate}
                        readonly={props.isDeleteClicked}
                        />
                    </Layout.Child>
                </Layout>
            </Layout.Child>
            <Layout.Child>
                {props.isDeleteClicked ? (
                    <FilterButton onClick={props.handleDelete} reset={false} label="Delete" />
                    ) : (
                    <FilterButton onClick={props.handleSimulate} reset={false} label="Simulate" />
                )}
            </Layout.Child>
        </Layout>
    )
};

export default OutperformancePopupFilters;
