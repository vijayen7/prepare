import React, { Component } from 'react';
import _ from 'lodash';
import Dialog from 'commons/components/Dialog';
import { Layout } from 'arc-react-components';
import Message from 'commons/components/Message';
import MessageDialog from 'commons/components/MessageDialog';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getValueForSingleSelect,
  getNullIfUndefined,
  getNullIfEmptyString,
  getNullIfEmptyOrUndefinedString
} from '../../util';
import { getFirstDateOfCurrentMonth } from 'commons/util';
import OutperformancePopupFilters from './../components/OutperformancePopupFilters';
import PopUpSimulatedGrids from './../components/PopUpSimulatedGrids';
import {
  destroyDeleteToggle,
  destroyEditToggle,
  hidePrimaryPopUp,
  fetchSimulateData,
  saveSimulateData,
  resetSaveData,
  resetDeleteData,
  resetSimulateData,
  resetSelectedRowData,
  resetSimulateFlag,
  deleteOutperformanceRule,
  fetchOutperformanceData
} from '../../actions';

class OutperformancePopUp extends Component {
  constructor(props) {
    super(props);
    this.state = this.getDefaultState();
  }

  componentDidUpdate(prevProps) {
    if (_.isEmpty(prevProps.selectedRowProps) && !_.isEmpty(this.props.selectedRowProps)) {
      var populateValuesForEdit = this.populateValuesForEdit();
      this.setState(populateValuesForEdit);
    }
  }

  onSelect = (params) => {
    if(this.props.isPopUpOpen
      || !_.isEmpty(this.props.saveSimulatedData)
      || !_.isEmpty(this.props.deleteOutperformanceData)) {
      if(params.value != null && !_.isEmpty(params.value)) {
        if(params.key === "selectedCountries" || params.key === "selectedRegions") {
          this.setState({ isCountryOrRegionFilterEmpty : false });
        } else if(params.key === "longGcRate" || params.key === "shortGcRate") {
          this.setState({ isGcRateEmpty : false });
        } else if(params.key === "selectedOutperformanceTypes") {
          this.setState({ isOutperformanceTypeFilterEmpty : false });
        }

        //Reset longGc if Cash
        if(params.key === "selectedOutperformanceTypes" && params.value.key == 2 ) {
          this.setState({ longGcRate : [] });
        }

        //Refresh selectedCountries when selected a region
        if(params.key === "selectedRegions") {
          this.setState({ selectedCountries : [] });
        }
      }
    }

    const { key, value } = params;
    this.setState({ [key]: value });
  }

  getDefaultState = () => {
    return {
        selectedDate: getFirstDateOfCurrentMonth(),
        selectedOutperformanceTypes: [],
        selectedCpes: [],
        selectedBundles: [],
        selectedCountries: [],
        selectedRegions: [],
        longGcRate: [],
        shortGcRate: [],
        isSimulate: false,
        selectedRowData: [],
        saveSelectMessage: false,
        isCountryOrRegionFilterEmpty: false,
        isGcRateEmpty: false,
        isOutperformanceTypeFilterEmpty: false,
        popupData: {
          id: "1_null_null",
          countryId: "",
          cpeId: "",
          bundleId: "",
          effectiveStartDate: "19000101",
          effectiveEndDate: "20380101",
          cpeName: "",
          bundleName: "",
          countryName: "",
          longGcRate: "",
          shortGcRate: ""
        },
        error: {}
    };
  }

  populateValuesForEdit = () => {
    const rowData = this.props.selectedRowProps;
    let populateValues = {};

    //Populate value for dataType
    if (typeof rowData.dataTypeId !== 'undefined') {
      populateValues.selectedOutperformanceTypes = {
        key: rowData.dataTypeId,
        value: `${rowData.dataTypeName}`
      };
    }

    //Populate value for country
    if (typeof rowData.countryId !== 'undefined') {
      populateValues.selectedCountries = {
        key: rowData.countryId,
        value: `${rowData.countryName} [${rowData.countryId}]`
      };
    }

    //Populate value for region
    if (typeof rowData.regionId !== 'undefined') {
      populateValues.selectedRegions = {
        key: rowData.regionId,
        value: `${rowData.regionName} [${rowData.regionId}]`
      };
    }

    //Populate value for Counterparty entity
    if (typeof rowData.cpeId !== 'undefined') {
      populateValues.selectedCpes = { key: rowData.cpeId, value: `${rowData.cpeName} [${rowData.cpeId}]` };
    }
    //Populate value for bundle
    if (typeof rowData.bundleId !== 'undefined') {
      populateValues.selectedBundles = {
        key: rowData.bundleId,
        value: `${rowData.bundleName} [${rowData.bundleId}]`
      };
    }
    //Populate value for longGcRate
    if (typeof rowData.longGcRate !== 'undefined') {
      populateValues.longGcRate = rowData.longGcRate;
    }
    //Populate value for shortGcRate
    if (typeof rowData.shortGcRate !== 'undefined') {
      populateValues.shortGcRate = rowData.shortGcRate;
    }
    //Populate value for date
    populateValues.selectedDate = this.state.selectedDate;

    return populateValues;
  }

  handleClosePopUp = () => {
    this.props.hidePrimaryPopUp();
    this.props.destroyDeleteToggle();
    this.props.destroyEditToggle();
    this.setState(this.getDefaultState());
    this.props.resetSimulateData();
    this.props.resetSelectedRowData();
    this.props.resetSimulateFlag()
    this.state.isCountryOrRegionFilterEmpty = false;
    this.state.isOutperformanceTypeFilterEmpty = false;
    this.state.isGcRateEmpty = false;
  }

  handleDelete = () => {
    if(this.props.selectedRowProps.countryId === undefined && this.props.selectedRowProps.regionId === undefined) {
      this.setState({ isCountryOrRegionFilterEmpty : true });
      return;
    }

    //Payload for deleting a rule
    const payload = {
      dataTypeId: getNullIfEmptyOrUndefinedString(this.props.selectedRowProps.dataTypeId),
      countryId: getNullIfEmptyOrUndefinedString(this.props.selectedRowProps.countryId),
      regionId: getNullIfEmptyOrUndefinedString(this.props.selectedRowProps.regionId),
      date: this.state.selectedDate,
      cpeId: getNullIfEmptyOrUndefinedString(this.props.selectedRowProps.cpeId),
      bundleId: getNullIfEmptyOrUndefinedString(this.props.selectedRowProps.bundleId)
    };

    this.props.deleteOutperformanceRule(payload);
    this.handleClosePopUp();
  }

  handleCloseMessagePopup = () => {
    this.props.resetSaveData();
    this.props.resetDeleteData();
    if (!_.isEmpty(this.props.saveSimulatedData) && this.props.saveSimulatedData === "success") {
       this.props.fetchOutperformanceData(this.props.searchPayload)
    }
  }

  populateValuesForSimulation = (payload) => {
    //Copying values to be sent as props to the child component
    var newState = {...this.state.popupData};
    newState.id = this.state.popupData.id;
    newState.countryId = payload.countryId;
    newState.bundleId = payload.bundleId;
    newState.regionId = payload.regionId;
    newState.dataTypeId = payload.dataTypeId;
    newState.dataTypeName = this.state.selectedOutperformanceTypes.value;
    newState.cpeId = payload.cpeId;
    newState.countryName = this.state.selectedCountries.value;
    newState.regionName = this.state.selectedRegions.value;
    newState.cpeName = this.state.selectedCpes.value;
    newState.bundleName = this.state.selectedBundles.value;
    newState.longGcRate = typeof this.state.longGcRate === 'object' ?  this.state.popupData.longGcRate : this.state.longGcRate;
    newState.shortGcRate = typeof this.state.shortGcRate === 'object' ?  this.state.popupData.shortGcRate : this.state.shortGcRate;
    newState.effectiveStartDate = this.state.selectedDate;
    if (typeof newState.effectiveStartDate !== 'undefined') {
      newState.effectiveStartDate =  newState.effectiveStartDate.replace(/-/g, '');
    }
    newState.effectiveEndDate = this.state.popupData.effectiveEndDate;
    this.setState({ popupData: newState });
  }

  handleSimulate = () => {
    let payload;
    let _countryId = getValueForSingleSelect(this.state.selectedCountries);
    let _regionId = getValueForSingleSelect(this.state.selectedRegions);
    let _outperformanceDataTypeId = getValueForSingleSelect(this.state.selectedOutperformanceTypes);

    if(_countryId === "null" && _regionId === "null") {
      this.setState({ isCountryOrRegionFilterEmpty : true });
      return;
    }

    if(_outperformanceDataTypeId === "null") {
      this.setState({ isOutperformanceTypeFilterEmpty : true });
      return;
    }

    if(typeof this.state.longGcRate === 'object' && typeof this.state.shortGcRate === 'object') {
      this.setState({ isGcRateEmpty: true });
      return;
    }

    payload = {
      dataTypeId: _outperformanceDataTypeId,
      countryId: _countryId,
      regionId: _regionId,
      date: this.state.selectedDate,
      cpeId: getValueForSingleSelect(this.state.selectedCpes),
      bundleId: getValueForSingleSelect(this.state.selectedBundles),
    };

    this.props.fetchSimulateData(payload);
    this.populateValuesForSimulation(payload);
    this.setState({ isSimulate: true });
  }

  handleSave = () => {
    if (this.state.selectedRowData.length === 0) {
      this.setState({ saveSelectMessage: true });
      return;
    }

    if(this.state.selectedRowData[0].countryId === "null" && this.state.selectedRowData[0].regionId === "null") {
      this.setState({ isCountryOrRegionFilterEmpty : true });
      return;
    }

    //Payload for saving rule in DB
    let payload = {
      dataTypeId: this.state.selectedRowData[0].dataTypeId,
      countryId: this.state.selectedRowData[0].countryId,
      regionId: this.state.selectedRowData[0].regionId,
      date: this.state.selectedRowData[0].effectiveStartDate,
      cpeId: this.state.selectedRowData[0].cpeId,
      bundleId: this.state.selectedRowData[0].bundleId,
      longGcRate: getNullIfEmptyOrUndefinedString(this.state.selectedRowData[0].longGcRate),
      shortGcRate: getNullIfEmptyOrUndefinedString(this.state.selectedRowData[0].shortGcRate)
    }

    this.props.saveSimulateData(payload);
    this.handleClosePopUp();
  }

  renderPopUp = () => {
    let popUp = null;
    let saveMessage = null;
    let deleteMessage = null;
    let title;
    let simulatedRules = null;
    let countryEmptyMessage = null;
    let gcRateEmptyMessage = null;
    let outperformanceTypeFilterEmptyMessage = null;

    title = _.isEmpty(this.props.selectedRowProps) ? 'ADD OUTPERFORMANCE RULES' : 'EDIT OUTPERFORMANCE RULES';
    title = this.props.isDeleteClicked ? 'DELETE OUTPERFORMANCE RULES' : title;

    if (!_.isEmpty(this.props.saveSimulatedData)) {
      if(this.props.saveSimulatedData === "success") {
        saveMessage = (<Message messageData="Rule(s) have been saved" success />);
      } else if(this.props.saveSimulatedData === "failure") {
        saveMessage = (<Message messageData="Error occurred while saving rule(s)" error />);
      }
    }

    if (!_.isEmpty(this.props.deleteOutperformanceData)) {
      if(this.props.deleteOutperformanceData === "success") {
        deleteMessage = (<Message messageData="Rule(s) have been deleted" success />);
      } else if(this.props.deleteOutperformanceData === "failure"){
        deleteMessage = (<Message messageData="Error occurred while deleting rule(s)" error />);
      }
    }

    if(this.state.isCountryOrRegionFilterEmpty) {
      countryEmptyMessage = <Message messageData="Enter a valid country or region to save the simulated data." />;
    } else if(this.state.isGcRateEmpty) {
      gcRateEmptyMessage = <Message messageData="Atleast one of the GC rates has to be entered" />;
    } else if(this.state.isOutperformanceTypeFilterEmpty) {
      outperformanceTypeFilterEmptyMessage = <Message messageData="Enter a valid outperformance type to save the simulated data." />;
    }

    if(this.props.simulatedData.length != 0 || this.props.hasSimulateApiReturned) {
      simulatedRules = <PopUpSimulatedGrids
        isSimulate={this.state.isSimulate}
        simulatedData={this.props.simulatedData}
        saveSelectMessage={this.state.saveSelectMessage}
        onSelect={this.onSelect}
        popupData={this.state.popupData}
        handleSave={this.handleSave}
      />
    }

    popUp = (
      <React.Fragment>
        <Dialog
          isOpen={(this.props.isPopUpOpen || !_.isEmpty(this.props.selectedRowProps))}
          title={title}
          onClose={this.handleClosePopUp}
          style={{
            width: '1080px'
          }}
        >
          <div style={{ width: '1060px' }}>
            <Layout isRowType>
              <Layout.Child>
                <OutperformancePopupFilters
                  onSelect={this.onSelect}
                  state={this.state}
                  isEditClicked={!_.isEmpty(this.props.selectedRowProps)}
                  isDeleteClicked={this.props.isDeleteClicked}
                  handleSimulate={this.handleSimulate}
                  handleDelete={this.handleDelete}
                />
              </Layout.Child>
              <Layout.Child>
                <hr />
                {simulatedRules}
              </Layout.Child>
              <Layout.Child>
                {countryEmptyMessage}
                {gcRateEmptyMessage}
                {outperformanceTypeFilterEmptyMessage}
              </Layout.Child>
            </Layout>
          </div>
        </Dialog>
        <MessageDialog
          isOpen={!_.isEmpty(this.props.saveSimulatedData)}
          onClose={this.handleCloseMessagePopup}
          content={saveMessage}
        />
        <MessageDialog
          isOpen={!_.isEmpty(this.props.deleteOutperformanceData)}
          onClose={this.handleCloseMessagePopup}
          content={deleteMessage}
        />
      </React.Fragment>
    );

    return popUp;
  }

  render() {
    return this.renderPopUp();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      destroyDeleteToggle,
      destroyEditToggle,
      hidePrimaryPopUp,
      fetchSimulateData,
      saveSimulateData,
      resetSaveData,
      resetDeleteData,
      resetSimulateFlag,
      resetSimulateData,
      resetSelectedRowData,
      deleteOutperformanceRule,
      fetchOutperformanceData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    isPopUpOpen: state.outperformance.isPrimaryPopUpOpen.outperformance,
    isDeleteClicked: state.outperformance.isDeleteClicked,
    isEditClicked: state.outperformance.isEditClicked,
    simulatedData: state.outperformance.simulatedData,
    saveSimulatedData: state.outperformance.saveSimulatedData,
    selectedRowProps: state.outperformance.selectedRowData,
    deleteOutperformanceData: state.outperformance.deleteOutperformanceData,
    hasSimulateApiReturned: state.outperformance.hasSimulateApiReturned,
    searchPayload: state.outperformance.searchPayload
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OutperformancePopUp);
