export function getValueForSingleSelect(data) {
  if (typeof data === 'undefined' || data.length === 0 || data.key === -1) return 'null';
  const returnValue = data.key;
  return returnValue;
}

export function getNullIfUndefined(data) {
  return typeof data === undefined ? '__null__' : data;
}

export function getNullIfEmptyString(data) {
  return data === "" ? "__null__" : data;
}

export function getNullIfEmptyOrUndefinedString(data) {
  return data === "" || data ===  undefined ? "null" : data;
}

export function getZeroForDefault(data) {
  if (typeof data !== "undefined" && data !== null && data.length === 0) return '0';
  return data;
}
