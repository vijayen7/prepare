import React, { Component } from "react";
import OutperformanceSideBar from "./container/SideBar";
import OutperformanceGrid from "./container/Grid";
import Loader from "commons/container/Loader";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FilterButton from 'commons/components/FilterButton';
import { Layout } from 'arc-react-components';
import { showPrimaryPopUp } from './actions';

class Outperformance extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    openPrimaryPopUp = () => {
        this.props.showPrimaryPopUp();
    }

    render() {
        return (
            <React.Fragment>
                <Loader />
                <div className="layout--flex--row">
                    <arc-header
                        ref={header => {
                        this.header = header;
                        }}
                        className="size--content"
                        user={USER}
                        modern-themes-enabled
                    />
                    <Layout isColumnType>
                        <Layout.Child size={2}>
                            <OutperformanceSideBar />
                        </Layout.Child>
                        <Layout.Child size={0.1} />
                        <Layout.Child size={12}>
                            <OutperformanceGrid />
                        </Layout.Child>
                    </Layout>
                </div>
            </React.Fragment>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
    {
        showPrimaryPopUp
    },
      dispatch
    );
}

export default connect(
    null,
    mapDispatchToProps
)(Outperformance);
