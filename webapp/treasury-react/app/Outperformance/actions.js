import { 
    FETCH_OUTPERFORMANCE_DATA,
    RESET_OUTPERFORMANCE_DATA_SEARCH_MESSAGE,
    SHOW_OUTPERFORMANCE_PRIMARY_POPUP,
    HIDE_OUTPERFORMANCE_PRIMARY_POPUP,
    FETCH_OUTPERFORMANCE_SIMULATE_DATA,
    RESET_OUTPERFORMANCE_SAVE_DATA,
    DESTROY_OUTPERFORMANCE_DELETE_TOGGLE,
    SAVE_OUTPERFORMANCE_SIMULATE_DATA,
    SET_OUTPERFORMANCE_DELETE_TOGGLE,
    RESET_OUTPERFORMANCE_SIMULATE_DATA,
    RESET_OUTPERFORMANCE_SELECTED_ROW_DATA,
    SET_OUTPERFORMANCE_SELECTED_ROW_DATA,
    DELETE_OUTPERFORMANCE_RULE,
    RESET_DELETE_OUTPERFORMANCE_DATA,
    SET_SIMULATE_FLAG,
    RESET_SIMULATE_FLAG,
    SET_OUTPERFORMANCE_EDIT_TOGGLE,
    DESTROY_OUTPERFORMANCE_EDIT_TOGGLE
} from "./constants";

export function fetchOutperformanceData(payload) {
    return {
      type: FETCH_OUTPERFORMANCE_DATA,
      payload
    };
}

export function resetOutperformanceDataSearchMessage() {
    return {
      type: RESET_OUTPERFORMANCE_DATA_SEARCH_MESSAGE
    };
}

export function showPrimaryPopUp() {
    return {
      type: SHOW_OUTPERFORMANCE_PRIMARY_POPUP
    };
}

export function hidePrimaryPopUp() {
    return {
      type: HIDE_OUTPERFORMANCE_PRIMARY_POPUP
    };
}

export function destroyDeleteToggle() {
    return {
      type: DESTROY_OUTPERFORMANCE_DELETE_TOGGLE
    };
}

export function destroyEditToggle() {
    return {
      type: DESTROY_OUTPERFORMANCE_EDIT_TOGGLE
    };
}

export function fetchSimulateData(payload) {
    return {
      type: FETCH_OUTPERFORMANCE_SIMULATE_DATA,
      payload
    };
}

export function resetSaveData() {
    return {
      type: RESET_OUTPERFORMANCE_SAVE_DATA
    };
}

export function saveSimulateData(payload) {
    return {
      type: SAVE_OUTPERFORMANCE_SIMULATE_DATA,
      payload
    };
}

export function resetSimulateData() {
    return {
      type: RESET_OUTPERFORMANCE_SIMULATE_DATA
    };
}

export function resetSelectedRowData() {
    return {
      type: RESET_OUTPERFORMANCE_SELECTED_ROW_DATA
    };
}

export function resetDeleteData() {
    return {
        type: RESET_DELETE_OUTPERFORMANCE_DATA
    };
}

export function setSelectedRowData(payload) {
    return {
      type: SET_OUTPERFORMANCE_SELECTED_ROW_DATA,
      payload
    };
}

export function setDeleteToggle() {
    return {
      type: SET_OUTPERFORMANCE_DELETE_TOGGLE
    };
}

export function setEditToggle() {
    return {
      type: SET_OUTPERFORMANCE_EDIT_TOGGLE
    };
}

export function setSimulateFlag() {
    return {
        type: SET_SIMULATE_FLAG
    };
}

export function resetSimulateFlag() {
    return {
        type: RESET_SIMULATE_FLAG
    };
}

export function deleteOutperformanceRule(payload) {
    return {
      type: DELETE_OUTPERFORMANCE_RULE,
      payload
    };
}
  

  