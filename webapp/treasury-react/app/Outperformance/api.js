import { BASE_URL, INPUT_FORMAT_JSON } from 'commons/constants';
import { fetchURL, getEncodedParams, convertDatetoLocalDate} from 'commons/util';
import { CLASS_NAMESPACE } from "commons/ClassConfigs"

const queryString = require("query-string");
export let url = "";

export function getOutperformanceData(payload) {
  const encodedParams = getEncodedParams(payload, CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.OutperformanceRuleFilter);
    url = `${BASE_URL}service/outperformanceRuleService/getOutperformanceRuleData?outperformanceRuleFilter=${encodedParams}${INPUT_FORMAT_JSON}`;
    return fetch(url, {
        credentials: "include"
    }).then(data => data.json())
}

export function getSimulateData(payload) {
  let date = undefined;
    if (typeof payload.date !== 'undefined') {
      date = '"'+payload.date+'"';
      delete payload["date"];
    }

    const paramString = queryString.stringify(payload);
    url = `${BASE_URL}service/outperformanceRuleService/simulateRules?${paramString}&date=${date}${INPUT_FORMAT_JSON}`;
    return fetchURL(url);
}

export function fetchSaveSimulateData(payload) {
    let date = undefined;
    if (typeof payload.date !== 'undefined') {
      date = convertDatetoLocalDate(payload.date);
      delete payload["date"];
    }

    const paramString = queryString.stringify(payload);
    url = `${BASE_URL}service/outperformanceRuleService/addOrUpdateOutperformanceRule?${paramString}&date="${date}"${INPUT_FORMAT_JSON}`;
    return fetchURL(url);
}

export function deleteOutperformanceData(payload) {
  let date = undefined;
  if (typeof payload.date !== 'undefined') {
    date = '"'+payload.date+'"';
    delete payload["date"];
  }

    const paramString = queryString.stringify(payload);
    url = `${BASE_URL}service/outperformanceRuleService/deleteOutperformanceRule?${paramString}&date=${date}${INPUT_FORMAT_JSON}`;
    return fetchURL(url);
}
