import { combineReducers } from "redux";
import { 
  FETCH_OUTPERFORMANCE_DATA,
  RESET_OUTPERFORMANCE_DATA_SEARCH_MESSAGE,
  RESET_OUTPERFORMANCE_SELECTED_ROW_DATA,
  SET_OUTPERFORMANCE_SELECTED_ROW_DATA,
  SAVE_OUTPERFORMANCE_SIMULATE_DATA,
  FETCH_OUTPERFORMANCE_SIMULATE_DATA,
  RESET_OUTPERFORMANCE_SAVE_DATA,
  RESET_OUTPERFORMANCE_SIMULATE_DATA,
  SHOW_OUTPERFORMANCE_PRIMARY_POPUP,
  HIDE_OUTPERFORMANCE_PRIMARY_POPUP,
  DESTROY_OUTPERFORMANCE_DELETE_TOGGLE,
  SET_OUTPERFORMANCE_DELETE_TOGGLE,
  DELETE_OUTPERFORMANCE_RULE,
  RESET_DELETE_OUTPERFORMANCE_DATA,
  SET_SIMULATE_FLAG,
  RESET_SIMULATE_FLAG,
  SET_OUTPERFORMANCE_EDIT_TOGGLE,
  DESTROY_OUTPERFORMANCE_EDIT_TOGGLE,
  SAVE_OUTPERFORMANCE_DATA_SEARCH_PAYLOAD
} from "./constants";
import {
  RESIZE_CANVAS
} from '../commons/constants';

function outperformanceDataSearchMessageReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_OUTPERFORMANCE_DATA}_SUCCESS`:
      return action.data || [];
    case `${RESET_OUTPERFORMANCE_DATA_SEARCH_MESSAGE}`:
      return [];
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

function exceptionReducer(state = false, action) {
  switch (action.type) {
    case `*_FAILURE`:
      return true;
  }
  return state;
}

function simulatedDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_OUTPERFORMANCE_SIMULATE_DATA}_SUCCESS`:
      return action.data || [];
    case RESET_OUTPERFORMANCE_SIMULATE_DATA:
      return [];
    default:
      return state;
  }
}

function saveSimulatedDataReducer(state = {}, action) {
  switch (action.type) {
    case `${SAVE_OUTPERFORMANCE_SIMULATE_DATA}_SUCCESS`:
      if(action.data == null)
        return "success";
      else if(typeof action.data.message !== "undefined" && typeof action.data.localizedMessage !== "undefined")
        return "failure";
    case RESET_OUTPERFORMANCE_SAVE_DATA:
      return {};
    default:
      return state;
  }
}

function saveSearchPayloadDataReducer(state = {}, action) {
  switch (action.type) {
    case SAVE_OUTPERFORMANCE_DATA_SEARCH_PAYLOAD:
     return action.payload;
    default:
     return state;
  }

}

function deleteToggleReducer(state = false, action) {
  switch (action.type) {
    case SET_OUTPERFORMANCE_DELETE_TOGGLE:
      return true;
    case DESTROY_OUTPERFORMANCE_DELETE_TOGGLE:
      return false;
    default:
      return state;
  }
}

function editToggleReducer(state = false, action) {
  switch (action.type) {
    case SET_OUTPERFORMANCE_EDIT_TOGGLE:
      return true;
    case DESTROY_OUTPERFORMANCE_EDIT_TOGGLE:
      return false;
    default:
      return state;
  }
}

function setSelectedRowDataReducer(state = {}, action) {
  switch (action.type) {
    case SET_OUTPERFORMANCE_SELECTED_ROW_DATA:
      return action.payload || {};
    case RESET_OUTPERFORMANCE_SELECTED_ROW_DATA:
      return {};
    default:
      return state;
  }
}

const closeOutperformancePopups = {
  outperformance: false
};

function togglePopUpReducer(state = closeOutperformancePopups, action) {
  switch (action.type) {
    case SHOW_OUTPERFORMANCE_PRIMARY_POPUP:
      return { ...state, outperformance: true };
    case HIDE_OUTPERFORMANCE_PRIMARY_POPUP:
      return closeOutperformancePopups;
    default:
      return state;
  }
}

function deleteOutperformanceRuleReducer(state = {}, action) {
  switch (action.type) {
    case `${DELETE_OUTPERFORMANCE_RULE}_SUCCESS`:
      if(action.data == null)
        return "success";
      else if(typeof action.data.message !== "undefined" && typeof action.data.localizedMessage !== "undefined")
        return "failure";
    case RESET_DELETE_OUTPERFORMANCE_DATA:
      return {};
    default:
      return state;
  }
}

function setSimulateFlagReducer(state = false, action) {
  switch (action.type) {
    case SET_SIMULATE_FLAG:
      return true;
    case RESET_SIMULATE_FLAG:
      return false;
    default:
      return state;
  }
}


const rootReducer = combineReducers({
  outperformanceData: outperformanceDataSearchMessageReducer,
  resizeCanvas: resizeCanvasReducer,
  exceptionOccurred: exceptionReducer,
  selectedRowData: setSelectedRowDataReducer,
  simulatedData: simulatedDataReducer,
  saveSimulatedData: saveSimulatedDataReducer,
  isPrimaryPopUpOpen: togglePopUpReducer,
  isDeleteClicked: deleteToggleReducer,
  isEditClicked: editToggleReducer,
  deleteOutperformanceData: deleteOutperformanceRuleReducer,
  hasSimulateApiReturned: setSimulateFlagReducer,
  searchPayload: saveSearchPayloadDataReducer
});
  
export default rootReducer;
