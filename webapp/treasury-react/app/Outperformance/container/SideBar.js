import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Sidebar from 'commons/components/Sidebar';
import FilterButton from 'commons/components/FilterButton';
import DateFilter from 'commons/container/DateFilter';
import CpeFilter from "./../../commons/container/CpeFilter";
import BundleFilter from "./../../commons/container/BundleFilter";
import CountryFilter from "./../../commons/container/CountryFilter";
import OutperformanceTypeFilter from "./../../commons/container/OutperformanceTypeFilter";
import RegionFilter from "./../../commons/container/RegionFilter";
import {
    fetchOutperformanceData,
    addOutperformanceData,
    deleteOutperformanceData,
    updateOutperformanceData,
    resetOutperformanceDataSearchMessage
} from "./../actions";
import {
    getPreviousBusinessDay,
    getIntegerArrayFromObjectArray,
    isAllKeySelected
} from "./../../commons/util";

class OutperformanceSideBar extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters = () => {
    return {
        selectedDate: getPreviousBusinessDay(),
        selectedOutperformanceDataTypes : [],
        selectedCpes: [],
        selectedBundles: [],
        selectedCountries: [],
        selectedRegions: []
    };
  }

  onSelect = (params) => {
    const { key, value } = params;
    let newState = {...this.state};
    newState[key] = value;
    this.setState(newState);
  }

  handleClick = () => {
    let payload = {
        "localDate": this.state.selectedDate,
        ...(this.state.selectedOutperformanceDataTypes.length > 0 &&
        !isAllKeySelected(this.state.selectedOutperformanceDataTypes)
            ? {
                "dataTypeIds": getIntegerArrayFromObjectArray(
                    this.state.selectedOutperformanceDataTypes
                )
            }
        : null),
        ...(this.state.selectedCpes.length > 0 &&
        !isAllKeySelected(this.state.selectedCpes)
            ? {
                "cpeIds": getIntegerArrayFromObjectArray(
                    this.state.selectedCpes
                )
            }
        : null),
        ...(this.state.selectedBundles.length > 0 &&
            !isAllKeySelected(this.state.selectedBundles)
                ? {
                    "bundleIds": getIntegerArrayFromObjectArray(
                        this.state.selectedBundles
                    )
                }
        : null),
        ...(this.state.selectedCountries.length > 0
            ? {
                "countryIds": getIntegerArrayFromObjectArray(
                  this.state.selectedCountries
                )
            }
        : null),
        ...(this.state.selectedRegions.length > 0
             ? {
                 "regionIds": getIntegerArrayFromObjectArray(
                   this.state.selectedRegions
                 )
             }
        : null)
    }
    this.props.fetchOutperformanceData(payload);
  }

  render() {
    return (
      <Sidebar>
        <DateFilter
            onSelect={this.onSelect}
            stateKey="selectedDate"
            data={this.state.selectedDate}
            label="Date"
        />
        <OutperformanceTypeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedOutperformanceDataTypes}
          stateKey={'selectedOutperformanceDataTypes'}
          label={'Type'}
        />
        <RegionFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedRegions}
          multiSelect={true}
          label={'Region'}
        />
        <CountryFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCountries}
          stateKey={'selectedCountries'}
          multiSelect={true}
          label={'Country'}
        />
        <CpeFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedCpes}
        />
        <BundleFilter
            onSelect={this.onSelect}
            selectedData={this.state.selectedBundles}
        />
        <FilterButton onClick={this.handleClick} reset={false} label="Search" />
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
        fetchOutperformanceData,
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(OutperformanceSideBar);
