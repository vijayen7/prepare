import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from 'commons/components/GridWithCellClick';
import { bindActionCreators } from 'redux';
import Message from 'commons/components/Message';
import { outperformanceColumns } from './../gridConfig/outperformanceColumnConfig';
import { outperformanceGridOptions } from './../gridConfig/gridOptions';
import OutperformancePopUp from './../popup/container/OutperformancePopup';
import FilterButton from 'commons/components/FilterButton';
import { Layout } from 'arc-react-components';
import { 
  showPrimaryPopUp,
  setSelectedRowData,
  setDeleteToggle,
  setEditToggle
} from './../actions';

class OutperformanceGrid extends Component {
  constructor(props) {
    super(props);
  }

  openPrimaryPopUp = () => {
    this.props.showPrimaryPopUp();
  }

  onCellClickHandler = (args) => {
    if (args.colId === 'edit' || args.colId === 'delete') {
      this.props.setSelectedRowData(args.item);
      this.openPrimaryPopUp();
    }
    if (args.colId === 'delete') {
      this.props.setDeleteToggle();
    }
    else if (args.colId === 'edit') {
      this.props.setEditToggle();
    }
  };

  renderGrid = () => {
    if (this.props.outperformanceData.length <= 0) {
      return (
      <div>
        <Message messageData="Search to load results." />
      </div>
      );
    }
    return <Grid
            data={this.props.outperformanceData}
            gridId="outperformanceGrid"
            gridColumns={outperformanceColumns()}
            gridOptions={outperformanceGridOptions()}
            height={950}
            label="Search Results"
            onCellClick={this.onCellClickHandler}
            />;
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <Layout.Child size={1}>
            <FilterButton onClick={this.openPrimaryPopUp} reset={false} label="Add" />
          </Layout.Child>
          <Layout.Child size={25}>
            <Layout>
              <Layout.Child>{this.renderGrid()}</Layout.Child>
            </Layout>
            <OutperformancePopUp />
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    outperformanceData: state.outperformance.outperformanceData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
  {
    showPrimaryPopUp,
    setSelectedRowData,
    setDeleteToggle,
    setEditToggle
  },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OutperformanceGrid);
