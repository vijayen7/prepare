import { convertJavaNYCDate } from "commons/util";
import {
  linkFormatter
} from 'commons/grid/formatters';


export function getSimulatedColumns() {
  var columns = [
    {
      id: "dataTypeId",
      name: "OutPerformance Type",
      field: "dataTypeName",
      toolTip: "Outperformance Data Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "regionId",
      name: "Region",
      field: "regionName",
      toolTip: "Region",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "country",
      name: "Country",
      field: "countryName",
      toolTip: "Country",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "bundle",
      name: "Bundle",
      field: "bundleName",
      toolTip: "Bundle",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "longGcRate",
      type: "number",
      name: "Long GC Rate",
      field: "longGcRate",
      toolTip: "Bundle",
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter: dpGrid.Formatters.Float,
      excelFormatter: "#,##0.0",
    },
    {
      id: "shortGcRate",
      type: "number",
      name: "Short GC Rate",
      field: "shortGcRate",
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter: dpGrid.Formatters.Float,
      excelFormatter: "#,##0.0"
    },
    {
      id: "effectiveStartDate",
      name: "Effective Start Date",
      field: "effectiveStartDate",
      toolTip: "Effective Start Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return value;
      },
      excelDataFormatter: function(value) {
        return value;
      },
      headerCssClass: "b",
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "effectiveEndDate",
      name: "Effective End Date",
      field: "effectiveEndDate",
      toolTip: "Effective End Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return value;
      },
      excelDataFormatter: function(value) {
        return value;
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    }
  ];
  return columns;
}

function getActionColumn() {
  return [
    {
      id: "edit",
      name: "",
      field: "edit",
      toolTip: "Edit",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
          return linkFormatter(
            row,
            cell,
            "<i class='icon-edit--block' />",
            columnDef,
            dataContext
          );
      },
      headerCssClass: "b",
      width: 10
    },
    {
      id: "delete",
      name: "",
      field: "delete",
      toolTip: "Delete",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
          return linkFormatter(
            row,
            cell,
            "<i class='icon-delete margin--right ' />",
            columnDef,
            dataContext
          );
      },
      headerCssClass: "b",
      width: 10
    }
  ];
}

export function getOutperformanceColumns() {
  return [
    {
        id: "dataTypeId",
        name: "OutPerformance Type",
        field: "dataTypeName",
        toolTip: "Outperformance Data Type",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "b",
        width: 100
    },
    {
        id: "regionId",
        name: "Region",
        field: "regionName",
        toolTip: "Region",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "b",
        width: 75
    },
    {
      id: "country",
      name: "Country",
      field: "countryName",
      toolTip: "Country",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "bundle",
      name: "Bundle",
      field: "bundleName",
      toolTip: "Bundle",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "longGcRate",
      type: "number",
      name: "Long GC Rate",
      field: "longGcRate",
      toolTip: "Bundle",
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter: dpGrid.Formatters.Float,
      excelFormatter: "#,##0.0",
    },
    {
      id: "shortGcRate",
      type: "number",
      name: "Short GC Rate",
      field: "shortGcRate",
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter: dpGrid.Formatters.Float,
      excelFormatter: "#,##0.0"
    },
    {
      id: "effectiveStartDate",
      name: "Effective Start Date",
      field: "effectiveStartDate",
      toolTip: "Effective Start Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "effectiveEndDate",
      name: "Effective End Date",
      field: "effectiveEndDate",
      toolTip: "Effective End Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    }
  ];

  return columns;
}

export function outperformanceColumns() {
  let columns = getActionColumn();
  columns = columns.concat(getOutperformanceColumns());
  return columns;
}

export function outperformanceSimulatedColumns() {
  const columns = getSimulatedColumns();
  return columns;
}

