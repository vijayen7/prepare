export function outperformanceGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    expandTillLevel: -1,
    highlightRowOnClick: true,
    editable: false,
    asyncEditorLoading: false,
    autoEdit: false,
    useAvailableScreenSpace: true,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    sortList: [{ columnId: 'country', sortAsc: true }]
  };
  return options;
}

export function getSimulatedGridOptions() {
  const gridOptions = outperformanceGridOptions();
  gridOptions.checkboxHeader = [
    {
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    }
  ];
  gridOptions.addCheckboxHeaderAsLastColumn = false;
  return gridOptions;
}
