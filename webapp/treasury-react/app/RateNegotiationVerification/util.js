import { getSelectedFilterArray } from 'commons/util';
import { CLASS_NAMESPACE } from "commons/ClassConfigs";
export function getIntegerArrayFromObjectArray(data) {
  var filterArray = [];
  data.forEach(item => {
    filterArray.push(item.key);
  });
  return filterArray;
}

export function getArrayFromString(valueString) {
  let filterArray = [];
  valueString.split(",").forEach((value) => {
          filterArray.push(value);
  });

  return filterArray;
}

export function getArrayFromEnum(data, className) {
  let filterArray = [];
  data.forEach(item => {
    filterArray.push([className, item.key]);
  });
  return filterArray;
}

export function createAPIPayload(payload) {
  var payload = Object.assign({}, payload);
  Object.keys(payload).forEach(function(key) {
    if (
      key !== "localStartDate" &&
      key !== "localEndDate" &&
      key !== "includeBorrow" &&
      key !== "includeSwap"
    ) {
      if (payload[key] === undefined || payload[key] == null || payload[key].length === 0) {
        payload[key] = null;
      } else if (key == "spns") {
        payload[key] = getIntegerArrayFromObjectArray(getSelectedFilterArray(payload[key]));
      } else if (key == "securityFilter") {
        payload[key]["searchStrings"] = getArrayFromString(payload[key]["searchStrings"]);
      } else if (key == "verificationResult") {
        payload[key] = getArrayFromEnum(payload[key], CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.negotiation.VerificationResult);
      } else {
        payload[key] = getIntegerArrayFromObjectArray(payload[key]);
      }
    }
  });

  return payload;
}
