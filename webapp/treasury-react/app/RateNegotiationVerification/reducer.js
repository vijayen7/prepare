import {
  RESIZE_CANVAS
} from "commons/constants";

import {
  FETCH_RATE_NEGOTIATION_VERIFICATION_DATA,
  DESTROY_RATE_NEGOTIATION_VERIFICATION_DATA,
  DELETE_RATE_NEGOTIATION_DATA,
  RESOLVE_RATE_NEGOTIATION_DATA,
  EDIT_RATE_NEGOTIATION_DATA,
  DESTROY_NEGOTIATION_ACTION_FLAGS,
  FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_CURRENT,
  FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_PREV,
  DESTROY_DRILL_DOWN_LOT_LEVEL_DATA,
  SET_SEARCH_PAYLOAD
} from "./constants";
import { combineReducers } from "redux";

function rateNegotiationVerificationDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_RATE_NEGOTIATION_VERIFICATION_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_RATE_NEGOTIATION_VERIFICATION_DATA:
      return [];
  }
  return state;
}

function searchPayloadReducer(state = [], action) {
  switch (action.type) {
    case SET_SEARCH_PAYLOAD:
      return action.data || [];
  }
  return state;
}

function deleteRateNegotiationDataReducer(state = 0, action) {
  switch (action.type) {
    case `${DELETE_RATE_NEGOTIATION_DATA}_SUCCESS`:
      return 1;
    case `${DELETE_RATE_NEGOTIATION_DATA}_FAILURE`:
      return -1;
    case DESTROY_NEGOTIATION_ACTION_FLAGS:
      return 0;
  }
  return state;
}

function resolveRateNegotiationDataReducer(state = [], action) {
  switch (action.type) {
    case `${RESOLVE_RATE_NEGOTIATION_DATA}_SUCCESS`:
      return action.data || [];
    case `${RESOLVE_RATE_NEGOTIATION_DATA}_FAILURE`:
      return [];
    case DESTROY_NEGOTIATION_ACTION_FLAGS:
      return [];
  }
  return state;
}

function editRateNegotiationDataReducer(state = 0, action) {
  switch (action.type) {
    case `${EDIT_RATE_NEGOTIATION_DATA}_SUCCESS`:
      return 1;
    case `${EDIT_RATE_NEGOTIATION_DATA}_FAILURE`:
      return -1;
    case DESTROY_NEGOTIATION_ACTION_FLAGS:
      return 0;
  }
  return state;
}

function lotLevelDataForDrillDownCurrentReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_CURRENT}_SUCCESS`:
      return action.data || [];
    case DESTROY_DRILL_DOWN_LOT_LEVEL_DATA:
      return [];
  }
  return state;
}

function lotLevelDataForDrillDownPrevReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_PREV}_SUCCESS`:
      return action.data || [];
    case DESTROY_DRILL_DOWN_LOT_LEVEL_DATA:
      return [];
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

const rootReducer = combineReducers({
  rateNegotiationVerificationData: rateNegotiationVerificationDataReducer,
  resizeCanvas: resizeCanvasReducer,
  deleteStatus: deleteRateNegotiationDataReducer,
  resolveStatus: resolveRateNegotiationDataReducer,
  editStatus: editRateNegotiationDataReducer,
  prevDayLotData: lotLevelDataForDrillDownPrevReducer,
  currentDayLotData: lotLevelDataForDrillDownCurrentReducer,
  searchPayload: searchPayloadReducer
});

export default rootReducer;
