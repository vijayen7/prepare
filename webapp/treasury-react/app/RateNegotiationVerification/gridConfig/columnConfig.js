import {
  numberComparator,
  textComparator
} from "commons/grid/comparators";
import {
  linkFormatter,
  textFormatter,
  numberFormatter,
  numberFormat
} from "commons/grid/formatters";
import {
  convertJavaNYCDashedDate
} from "commons/util";

var totals = { negotiatedQuantity:0, marketValueUSD: 0, impactUSD:0 };

export function getNegotiationVerificationColumns() {
  var columns = [{
            id: "edit",
            name: "",
            field: "edit",
            toolTip: "Edit",
            type: "text",
            filter: true,
            sortable: true,
            formatter: function(row, cell, value, columnDef, dataContext) {
                return linkFormatter(
                  row,
                  cell,
                  "<i class='icon-edit--block' />",
                  columnDef,
                  dataContext
                );
            },
            headerCssClass: "b",
            width: 30
    },
    {
            id: "delete",
            name: "",
            field: "delete",
            toolTip: "Delete",
            type: "text",
            filter: true,
            sortable: true,
            formatter: function(row, cell, value, columnDef, dataContext) {
                return linkFormatter(
                  row,
                  cell,
                  "<i class='icon-delete margin--right ' />",
                  columnDef,
                  dataContext
                );
            },
            headerCssClass: "b",
            width: 30
    },
    {
            id: "resolve",
            name: "",
            field: "resolve",
            toolTip: "Resolve",
            type: "text",
            filter: true,
            sortable: true,
            formatter: function(row, cell, value, columnDef, dataContext) {
            if(dataContext.verificationResult[1] === "BREAK")
                return textFormatter(
                  row,
                  cell,
                  "<i class='text margin--right'><a>Resolve</a><i/>",
                  columnDef,
                  dataContext
                );
            },
            headerCssClass: "b",
            width: 60
    },
    {
      id: "verificationResult",
      name: "Verification Status",
      field: "verificationResult",
      toolTip: "Break/Match/Missing",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 70,
      formatter: function(row, cell, value, columnDef, dataContext) {
        var status = getRateNegotiationAttribute(value, "verificationResult");
        return getFormattedColumnValue(status,"verificationResult");
      },
      comparator : function(a, b) {
          var aValue = getRateNegotiationAttribute(a.verificationResult,"verificationResult");
          var bValue = getRateNegotiationAttribute(b.verificationResult,"verificationResult");
          return textComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return getRateNegotiationAttribute(value, "verificationResult");;
      },
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "securityName",
      name: "Security Name",
      field: "securityName",
      toolTip: "Security Name",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "sedol",
      name: "Sedol",
      field: "sedol",
      toolTip: "sedol",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 60
    },
    {
      id: "ticker",
      name: "Ticker",
      field: "ticker",
      toolTip: "ticker",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50
    },
    {
      id: "date",
      name: "Date",
      field: "localDate",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDashedDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "legalEntity",
      name: "Legal Entity",
      field: "rateNegotiationData",
      toolTip: "Legal Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 60,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return getRateNegotiationAttribute(value, "legalEntityAbbrev");
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"legalEntityAbbrev") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"legalEntityAbbrev") : "" );
          return textComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return getRateNegotiationAttribute(value, "legalEntityAbbrev");;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 130
    },
    {
      id: "pnlSpn",
      name: "PNL SPN",
      field: "rateNegotiationData",
      toolTip: "PNL SPN",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return getRateNegotiationAttribute(value, "pnlSpn");
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"pnlSpn") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"pnlSpn") : "" );
          return numberComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return getRateNegotiationAttribute(value, "pnlSpn");;
      },
      width: 65,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "spn",
      name: "SPN",
      field: "rateNegotiationData",
      toolTip: "SPN",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return getRateNegotiationAttribute(value, "spn");
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"spn") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"spn") : "" );
          return numberComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return getRateNegotiationAttribute(value, "spn");;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 65
    },
    {
      id: "currency",
      name: "Ccy",
      field: "ccyName",
      toolTip: "Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 30,
    },
    {
      id: "positionType",
      name: "C/S",
      field: "positionType",
      toolTip: "Cash/Swap",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 50,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return getRateNegotiationAttribute(value, "positionType");
      },
      comparator : function(a, b) {
          var aValue = getRateNegotiationAttribute(a,"positionType");
          var bValue = getRateNegotiationAttribute(b,"positionType");
          return textComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return getRateNegotiationAttribute(value, "positionType");;
      },
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "book",
      name: "Book",
      field: "rateNegotiationData",
      toolTip: "Book",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return getRateNegotiationAttribute(value, "bookName");
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"bookName") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"bookName") : "" );
          return textComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return getRateNegotiationAttribute(value, "bookName");;
      },
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "weightedAverageStockLoanFeeRate",
      name: "WSLF Rate",
      field: "brokerWeightedAvgSlfRate",
      toolTip: "WSLF Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if(typeof value === "undefined"){
          return dpGrid.Formatters.Number(
            row,
            cell,
            value,
            columnDef,
            dataContext
          );
        }
        return numberFormat(value,4)},
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0000",
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 50
    },
    {
      id: "negotiatedRate",
      name: "Negotiated Rate",
      field: "rateNegotiationData",
      toolTip: "Negotiated Rate",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        var returnValue = getRateNegotiationAttribute(value, "negotiatedWslfRate");
        return numberFormat(returnValue,4)
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"negotiatedWslfRate") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"negotiatedWslfRate") : "" );
          return numberComparator(aValue,bValue);
      },
      headerCssClass: "aln-rt b",
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value, rowElem, currentGrid, isCurrentViewBeingExported, columnDef) {
          if (typeof rowElem.sel !== "undefined" && rowElem.sel !== null && rowElem.sel === "Summary") { return ""; }
          return numberFormat(getRateNegotiationAttribute(value, "negotiatedWslfRate"),4);
      },
      excelFormatter: "#,##0.0000",
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 50
    },
    {
      id: "quantity",
      name: "Current Quantity",
      field: "brokerQuantity",
      toolTip: "Current Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        var formattedValue = dpGrid.Formatters.Number(
          row,
          cell,
          value,
          columnDef,
          dataContext
        );
        if(dataContext.rateNegotiationData.negotiatedQuantity !== value) { return '<font class="red7">' + formattedValue + '</font>'; }
        return formattedValue;
      },
      aggregator: function(items, field, column){
      return dpGrid.Aggregators.sum(items, field , column);
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "negotiatedQuantity",
      name: "Negotiated Quantity",
      field: "rateNegotiationData",
      toolTip: "Negotiated Quantity",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value === undefined || value === ""){
         var formattedTotal = dpGrid.Formatters.Number(
                          row,
                          cell,
                          totals.negotiatedQuantity,
                          columnDef,
                          dataContext
                        );
         if(dataContext.brokerQuantity !== returnValue) { return '<font class="red7">' + formattedTotal + '</font>'; }
         return formattedTotal;
        }
        var returnValue = getRateNegotiationAttribute(value, "negotiatedQuantity");
        var formattedReturnValue = dpGrid.Formatters.Number(
          row,
          cell,
          returnValue,
          columnDef,
          dataContext
        );
        if(dataContext.brokerQuantity !== returnValue) { return '<font class="red7">' + formattedReturnValue + '</font>'; }
        return formattedReturnValue;
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"negotiatedQuantity") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"negotiatedQuantity") : "" );
          return numberComparator(aValue,bValue);
      },
      aggregator: function(items,field,column) {return sumAgg(items,field,column,"negotiatedQuantity");},
      filter: {
        isFilterOnFormattedValue: true
      },
      headerCssClass: "aln-rt b",
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value, rowElem, currentGrid, isCurrentViewBeingExported, columnDef) {
          if (typeof rowElem.sel !== "undefined" && rowElem.sel !== null && rowElem.sel === "Summary") { return totals.negotiatedQuantity; }
          return getRateNegotiationAttribute(value, "negotiatedQuantity");
      },
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "brokerMarketValueUSD",
      name: "Current Market Value (USD)",
      field: "brokerMarketValueUSD",
      toolTip: "Current Market Value (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: function(items, field, column){
        return dpGrid.Aggregators.sum(items, field , column);
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 90
    },
    {
      id: "negotiatedMvUsd",
      name: "Negotiated MV (USD)",
      field: "rateNegotiationData",
      toolTip: "Negotiated MV (USD)",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value === undefined || value === ""){
         return dpGrid.Formatters.Number(
                          row,
                          cell,
                          totals.marketValueUSD,
                          columnDef,
                          dataContext
                        );
        }
        var returnValue = getRateNegotiationAttribute(value, "marketValueUSD");
        return numberFormatter(
          row,
          cell,
          returnValue,
          columnDef,
          dataContext
        );
      },
      aggregator: function(items,field,column) {return sumAgg(items,field,column,"marketValueUSD");},
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"marketValueUSD") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"marketValueUSD") : "" );
          return numberComparator(aValue,bValue);
      },
      excelDataFormatter: function(value, rowElem, currentGrid, isCurrentViewBeingExported, columnDef) {
          if (typeof rowElem.sel !== "undefined" && rowElem.sel !== null && rowElem.sel === "Summary") { return totals.marketValueUSD; }
          return getRateNegotiationAttribute(value, "marketValueUSD");;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 90
    },
    {
      id: "impactUSD",
      name: "Impact (USD)",
      field: "rateNegotiationData",
      toolTip: "Impact (USD)",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        if (value === undefined || value === ""){
         return dpGrid.Formatters.Number(
                          row,
                          cell,
                          totals.impactUSD,
                          columnDef,
                          dataContext
                        );
        }
        var returnValue = getRateNegotiationAttribute(value, "impactUSD");
        return numberFormatter(
          row,
          cell,
          returnValue,
          columnDef,
          dataContext
        );
      },
      aggregator: function(items,field,column) {return sumAgg(items,field,column,"impactUSD");},
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"impactUSD") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"impactUSD") : "" );
          return numberComparator(aValue,bValue);
      },
      excelDataFormatter: function(value, rowElem, currentGrid, isCurrentViewBeingExported, columnDef) {
          if (typeof rowElem.sel !== "undefined" && rowElem.sel !== null && rowElem.sel === "Summary") { return totals.impactUSD; }
          return getRateNegotiationAttribute(value, "impactUSD");;
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 90
    },
    {
      id: "brokerExerciseDate",
      name: "Broker Exercise Date (MDATE)",
      field: "brokerExerciseDate",
      toolTip: "Broker Exercise Date (MDATE)",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDashedDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "daysDelayed",
      name: "Days Delayed",
      field: "daysDelayed",
      toolTip: "Days Delayed",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 60
    },
    {
      id: "username",
      name: "UserName",
      field: "rateNegotiationData",
      toolTip: "UserName",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return getRateNegotiationAttribute(value, "username");
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"username") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"username") : "" );
          return textComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return getRateNegotiationAttribute(value, "username");;
      },
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "effectiveStartDate",
      name: "Effective Start Date",
      field: "rateNegotiationData",
      toolTip: "Effective Start Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        var returnValue = getRateNegotiationAttribute(value, "verificationStartDate");
        return convertJavaNYCDashedDate(returnValue);
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? convertJavaNYCDashedDate(getRateNegotiationAttribute(a.rateNegotiationData,"verificationStartDate")) : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? convertJavaNYCDashedDate(getRateNegotiationAttribute(b.rateNegotiationData,"verificationStartDate")) : "" );
          return textComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return convertJavaNYCDashedDate(getRateNegotiationAttribute(value, "verificationStartDate"));
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 80
    },
    {
      id: "effectiveEndDate",
      name: "Effective End Date",
      field: "rateNegotiationData",
      toolTip: "Effective End Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        var returnValue = getRateNegotiationAttribute(value, "verificationEndDate");
        return convertJavaNYCDashedDate(returnValue);
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? convertJavaNYCDashedDate(getRateNegotiationAttribute(a.rateNegotiationData,"verificationEndDate")) : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? convertJavaNYCDashedDate(getRateNegotiationAttribute(b.rateNegotiationData,"verificationEndDate")) : "" );
          return textComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return convertJavaNYCDashedDate(getRateNegotiationAttribute(value, "verificationEndDate"));
      },
      filter: {
        isFilterOnFormattedValue: true
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "comment",
      name: "Comment",
      field: "rateNegotiationData",
      toolTip: "Comment",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return getRateNegotiationAttribute(value, "comment");
      },
      comparator : function(a, b) {
          var aValue = (typeof a !== "undefined" && a!== null ? getRateNegotiationAttribute(a.rateNegotiationData,"comment") : "" );
          var bValue = (typeof b !== "undefined" && b!== null ? getRateNegotiationAttribute(b.rateNegotiationData,"comment") : "" );
          return textComparator(aValue,bValue);
      },
      excelHeaderFormatter: function(row, cell) {
          return cell.id;
      },
      excelDataFormatter: function(value) {
          return getRateNegotiationAttribute(value, "comment");;
      },
      filter: {
        isFilterOnFormattedValue: true
      }
    },
  ]

  return columns;
}

function getRateNegotiationAttribute(obj, attributeName)
{
    var value = "";
    if (typeof obj !== "undefined" &&
        obj !== null) {
      if(attributeName === "spn") { value = obj.spn; }
      else if (attributeName === "pnlSpn") { value = obj.rateNegotiation.pnlSpn; }
      else if (attributeName === "negotiatedWslfRate") { value = obj.negotiatedWslfRate; }
      else if (attributeName === "negotiatedQuantity") { value = obj.negotiatedQuantity; }
      else if (attributeName === "legalEntityAbbrev") { value = obj.legalEntityAbbrev; }
      else if (attributeName === "bookName") { value = obj.bookName; }
      else if (attributeName === "marketValueUSD") { value = obj.marketValueUSD; }
      else if (attributeName === "verificationStartDate") { value = obj.verificationStartDate; }
      else if (attributeName === "verificationEndDate") { value = obj.verificationEndDate; }
      else if (attributeName === "comment") { value = obj.comment; }
      else if (attributeName === "username") { value = obj.username; }
      else if (attributeName === "impactUSD") { value = obj.impactUSD; }
      else if (attributeName === "verificationResult") { value = obj[1]; }
      else if (attributeName === "positionType") { value = obj[1]; }
    }
    return value;
}

function sumAgg(items, field, column, attrName) {
  console.log(items)
  var agg = 0;
  var isAllUndefined = true;
  for (var i = 0; i < items.length; i++){
  var val = items[i][field][attrName];
  if (val !== null && val !== "" && !isNaN(val) && val !== "-") {
      isAllUndefined = false;
      agg += Number(val);
    }
  }
  if (isAllUndefined) {
    return "";
  }
  totals[attrName] = agg;
  return agg;
}

export function getDrillDownColumns() {
  var columns = [{
      id: "sourceDate",
      name: "Source Date",
      field: "sourceDate",
      toolTip: "Source Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDashedDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "date",
      name: "Date",
      field: "date",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function(value) {
        return convertJavaNYCDashedDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "legalEntity",
      name: "Legal Entity",
      field: "legalEntityName",
      toolTip: "Legal Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 100
    },
    {
      id: "custodianAccount",
      name: "Custodian Account",
      field: "custodianAccDisplayName",
      toolTip: "Custodian Account",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 110
    },
    {
      id: "book",
      name: "Book",
      field: "bookName",
      toolTip: "Book",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80
    },
    {
      id: "quantity",
      name: "Broker Quantity",
      field: "brokerQuantity",
      toolTip: "Broker Quantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80
    },
    {
      id: "mv",
      name: "MV (USD)",
      field: "brokerMarketValue",
      toolTip: "Broker Position MV",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 90
    },
    {
      id: "SLF Rate(%)",
      name: "SLF Rate",
      field: "stockLoanFeeRate",
      toolTip: "SLF Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      filter: {
        isFilterOnFormattedValue: true
      },
      width: 50
    }];
  return columns;
}

function getFormattedColumnValue(value, columnName) {
  if (columnName === "verificationResult") {
    return getStatusColumnFormatter(value);
  }
}

function getStatusColumnFormatter(value) {
  var status;
  if (value === "BREAK") { status = 'critical'; }
  else if (value === "MATCH") { status = 'success'; }
  else if (value === "MISSING") { status = 'info'; }
  return '<div class="' + status + '" style="height:100%; text-align:center;">'
                + value + '</div>';
}
