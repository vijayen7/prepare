import { url } from "../api";
import { getSaveSettingsUrl } from "commons/util";
import {
  getDefaultColumns,
  getTotalColumns
} from "./columnSelector";

export function gridOptions() {
  var options = {
    configureReport: {
      urlCallback: function() {
        return window.location.origin + url;
      }
    },
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [
      {
        columnId: "verificationResult",
        sortAsc: true
      }
    ],
    page: true,
    checkboxHeader: {
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    },
    onRowClick: function() {
      // highlight on row works if this callback is defined
    },
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "",
    saveSettings: {
      applicationId: 3,
      serviceURL: getSaveSettingsUrl(),
      onBeforeLoadedCallback: function(settingsData) {},
      loadedCallback: function(gridObject) {},
      applicationCategory: "RateNegotiationVerificationData"
    },
    customColumnSelection: {
      defaultcolumns: getDefaultColumns(),
      totalColumns: getTotalColumns()
    },
    sheetName: "Rate Negotiation Verification"
  };

  return options;
}

export function drillDownGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: false,
    exportToExcel: true,
    sortList: [
      {
        columnId: "description",
        sortAsc: true
      }
    ]
  };
  return options;
}
