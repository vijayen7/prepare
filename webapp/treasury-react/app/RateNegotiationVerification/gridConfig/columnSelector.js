import {
  getNegotiationVerificationColumns
} from "./columnConfig";

export function getDefaultColumns() {
  var defaultColumns = [
    "edit",
    "delete",
    "resolve",
    "verificationResult",
    "date",
    "securityName",
    "legalEntity",
    "counterpartyEntity",
    "spn",
    "currency",
    "positionType",
    "book",
    "weightedAverageStockLoanFeeRate",
    "negotiatedRate",
    "quantity",
    "negotiatedQuantity",
    "negotiatedMvUsd",
    "brokerMarketValueUSD",
    "impactUSD",
    "brokerExerciseDate",
    "daysDelayed",
    "username",
    "effectiveStartDate",
    "effectiveEndDate",
    "comment"
  ];

  return defaultColumns;
}

export function getTotalColumns() {
      return getNegotiationVerificationColumns().map(column => [column.id, column.name]);
}
