import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { TabPanel } from 'arc-react-components';
import RateNegotiationVerificationDataGrid from "commons/components/GridWithCheckbox";
import Legend from "../components/Legend";
import Message from "commons/components/Message";
import { Layout } from "arc-react-components";
import {
  gridOptions,
  drillDownGridOptions
} from "../gridConfig/gridOptions";
import {
  getNegotiationVerificationColumns,
  getDrillDownColumns
 } from "../gridConfig/columnConfig";
import Resizer from "commons/components/Resizer";
import ResizerContainer from "commons/container/ResizerContainer";
import Dialog from "commons/components/Dialog";
import InputFilter from "commons/components/InputFilter";
import FilterButton from "commons/components/FilterButton";
import Label from "commons/components/Label";
import Column from "commons/components/Column";
import CheckboxFilter from "commons/components/CheckboxFilter";
import GridContainer from "commons/components/Grid";
import DateFilter from "commons/container/DateFilter";
import InputNumberFilter from "commons/components/InputNumberFilter";
import {
  getNextBusinessDay,
  getCurrentDate,
  getMinusOneBusinessDay,
  convertJavaNYCDashedDate
} from "commons/util";
import {
  deleteRateNegotiationData,
  resolveRateNegotiationData,
  editRateNegotiationData,
  closeNegotiationActionMessage,
  fetchCurrentDayLotLevelData,
  fetchPrevDayLotLevelData,
  fetchRateNegotiationVerificationData
}from "../actions";

class CommonGrid extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.onDblClickHandler = this.onDblClickHandler.bind(this);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleActionItem = this.handleActionItem.bind(this);
    this.handleBulkDelete = this.handleBulkDelete.bind(this);
    this.handleBulkEdit = this.handleBulkEdit.bind(this);
    this.handleBulkResolve = this.handleBulkResolve.bind(this);
    this.loadActionMessageDialogs = this.loadActionMessageDialogs.bind(this);
  }

  componentWillMount() {
    this.setState(this.getDefaultState());
  }

  getDefaultState() {
    return {
          selectedRowProps: {},
          isDelete: false,
          deleteComment: "Negotiation(s) being deleted by " + USER,
          isResolve: false,
          resolveComment: "Negotiation(s) being resolved by " + USER,
          isEdit: false,
          editComment: "Negotiation(s) being edited by " + USER,
          selectedEditStartDate: getNextBusinessDay(),
          selectedEditEndDate: getNextBusinessDay(),
          selectedEditNegotiationRate: "",
          isBulk: false,
          action: ""
    };
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  onCellClickHandler = args => {
    if (args.colId === "edit") {
      this.setState({
        isBulk: false,
        isEdit: true,
        selectedRowProps: args.item,
        action: "edit"
      });
    } else if (args.colId === "delete") {
      this.setState({
        isBulk: false,
        isDelete: true,
        selectedRowProps: args.item,
        action: "delete"
      });
    } else if (args.colId === "resolve") {
      this.setState({
        isBulk: false,
        isResolve: true,
        selectedRowProps: args.item,
        action: "resolve"
      });
    } else return;
  };

  onDblClickHandler = args => {
    var currentDate = convertJavaNYCDashedDate(args.rateNegotiationData.verificationStartDate);
    var date = new Date(currentDate);
    var payload = {
      rateNegotiationVerificationData: args,
      date: convertJavaNYCDashedDate(args.date)
    };
    this.props.fetchCurrentDayLotLevelData(payload);
    payload.date = getMinusOneBusinessDay(date);
    this.props.fetchPrevDayLotLevelData(payload);
  };

  handleBulkDelete() {
      this.setState({
        isBulk: true,
        isDelete: true,
        action: "delete"
      });
  }

  handleBulkEdit() {
      this.setState({
        isBulk: true,
        isEdit: true,
        action: "edit"
      });
  }

  handleBulkResolve() {
    this.setState({
        isBulk: true,
        isResolve:true,
        action: "resolve"
    });
  }

  handleCloseDialog() {
    this.setState(this.getDefaultState());
  }

  handleActionItem() {
    if (this.state.action  === "delete") {
      if (typeof this.state.selectedRowData !== "undefined" && this.state.selectedRowData.length > 0) {
        var selectedRowDataIds = [];
        this.state.selectedRowData.forEach(
          (o, i, arr) => (selectedRowDataIds.push(arr[i].rateNegotiationData.id))
        );
      }
      var payload = {
        ids: this.state.isBulk ? selectedRowDataIds : this.state.selectedRowProps.rateNegotiationData.id,
        comment: this.state.deleteComment
      };
      this.props.deleteRateNegotiationData(payload);
    }else if (this.state.action === "resolve") {
      var selectedData = [];
      this.state.isBulk ? (selectedData = this.state.selectedRowData) : selectedData.push(this.state.selectedRowProps);
      if (selectedData.length > 0) {
        selectedData.forEach(
          (o, i, arr) => (arr[i].rateNegotiationData.comment = this.state.resolveComment)
        );
        this.props.resolveRateNegotiationData(selectedData);
      }
    }else if (this.state.action === "edit") {
      var selectedData = [];
      this.state.isBulk ? (selectedData = this.state.selectedRowData) : selectedData.push(this.state.selectedRowProps);
      if (selectedData.length > 0) {
        selectedData.forEach(
          (o, i, arr) => (arr[i] = arr[i].rateNegotiationData)
        );
        selectedData.forEach(
          (o, i, arr) => (arr[i].negotiatedWslfRate = this.state.selectedEditNegotiationRate, arr[i].comment = this.state.editComment, arr[i].verificationStartDate = this.state.selectedEditStartDate, arr[i].verificationEndDate = this.state.selectedEditEndDate, arr[i].knowledgeStartDate = getCurrentDate())
        );
        this.props.editRateNegotiationData(selectedData);
      }
    }
    this.props.fetchRateNegotiationVerificationData(this.props.searchPayload);
    this.handleCloseDialog();
  }

  loadActionMessageDialogs() {
    var status = false;
    var message = "";
    if(this.props.deleteStatus === 1 || this.props.editStatus === 1) { status = true;}
    if(this.props.deleteStatus === -1 || this.props.editStatus === -1) { status = false;}
    if(status) {
      if (this.props.deleteStatus === 1) { message = "Rate Negotiation(s) deleted successfully."; }
      else if (this.props.editStatus === 1) { message = "Rate Negotiation(s) edited successfully."; }
    } else {
      if (this.props.deleteStatus === -1) { message = "Unable to delete rate negotiation(s). Please try again."; }
      else if (this.props.editStatus === -1) { message = "Unable to edit rate negotiation(s). Please try again."; }
    }
    var resolveToggle = (typeof this.props.resolveStatus[0] !== "undefined") && (this.props.resolveStatus[0].trim() !== "");
    return (
          <React.Fragment>
          <Dialog
            isOpen={this.props.deleteStatus !== 0 || this.props.editStatus !== 0}
            title={status ? "SUCCESS" : "FAILURE"}
            onClose={this.props.closeNegotiationActionMessage}
          >
            <Message error={!status} success={status} messageData={message}/>
                          <FilterButton
                          onClick={this.props.closeNegotiationActionMessage}
                          reset={false}
                          label="OK"
                          />
          </Dialog>
          <Dialog
            isOpen={resolveToggle}
            title={"RESULT"}
            onClose={this.props.closeNegotiationActionMessage}
          >
            <Message messageData={this.props.resolveStatus}/>
                          <FilterButton
                          onClick={this.props.closeNegotiationActionMessage}
                          reset={false}
                          label="OK"
                          />
          </Dialog>
          </React.Fragment>
    );
  }

  loadEditDialog() {
    if(this.state.isEdit) {
      return (
            <Dialog
              isOpen={this.state.isEdit}
              title={"EDIT NEGOTIATION RECORD(S)"}
              onClose={this.handleCloseDialog}
            >
                <Message warning={true} messageData={"Are you sure you want to edit the selected negotiation records?"}/><br/>
                <Layout isColumnType={true}>
                <Layout.Child size={1}>
                  <DateFilter
                    onSelect={this.onSelect}
                    stateKey="selectedEditStartDate"
                    data={this.state.selectedEditStartDate}
                    label="Effective Start Date"
                  />
                  <DateFilter
                    onSelect={this.onSelect}
                    stateKey="selectedEditEndDate"
                    data={this.state.selectedEditStartDate}
                    label="Effective End Date"
                  />
                  <InputNumberFilter
                    onSelect={this.onSelect}
                    stateKey="selectedEditNegotiationRate"
                    label="Negotiated Rate (*) : "
                  />
                </Layout.Child>
                <Layout.Child size={0.75}>
                </Layout.Child>
                </Layout><br/>
                <Layout isColumnType={true}>
                <Layout.Child size={1}>
                  <InputFilter
                    data={this.state.editComment}
                    onSelect={this.onSelect}
                    stateKey="editComment"
                    label="Comment (*) : "
                  /><br/>
                </Layout.Child>
                <Layout.Child size={0.75}>
                  <FilterButton
                    onClick={this.handleCloseDialog}
                    reset={false}
                    label="CANCEL"
                  />
                  <FilterButton
                    onClick={this.handleActionItem}
                    reset={false}
                    disabled={(this.state.editComment !== "undefined" && this.state.editComment.trim() !== "" && this.state.selectedEditNegotiationRate !== "undefined" && this.state.selectedEditNegotiationRate.trim() !== "") ? false : true}
                    label="OK"
                  />
                </Layout.Child>
                </Layout>
            </Dialog>
      );
    }
  }

  loadResolveDialog() {
    if(this.state.isResolve) {
      return (
            <Dialog
              isOpen={this.state.isResolve}
              title={"RESOLVE NEGOTIATION RECORD(S)"}
              onClose={this.handleCloseDialog}
            >
                <Message warning={true} messageData={"Are you sure you want to mark the selected record(s) as resolved?"}/><br/>
                <Layout isColumnType={true}>
                <Layout.Child size={1.25}>
                  <Label label="Are you sure to mark the selected records as resolved and overwrite the negotiated rate with broker reported rate ?" />
                </Layout.Child>
                </Layout><br/>
                <Layout isColumnType={true}>
                <Layout.Child size={1}>
                  <InputFilter
                    data={this.state.resolveComment}
                    onSelect={this.onSelect}
                    stateKey="resolveComment"
                    label="Comment (*) : "
                  /><br/>
                </Layout.Child>
                <Layout.Child size={0.75}>
                  <FilterButton
                    onClick={this.handleCloseDialog}
                    reset={false}
                    label="CANCEL"
                  />
                  <FilterButton
                    onClick={this.handleActionItem}
                    reset={false}
                    disabled={(this.state.resolveComment !== "undefined" && this.state.resolveComment.trim() !== "") ? false : true}
                    label="OK"
                  />
                </Layout.Child>
                </Layout>
            </Dialog>
      );
    }
  }

  loadDeleteDialog() {
    if(this.state.isDelete) {
      return (
            <Dialog
              isOpen={this.state.isDelete}
              title={"DELETE NEGOTIATION RECORD(S)"}
              onClose={this.handleCloseDialog}
            >
                <Message warning={true} messageData={"Are you sure you want to delete the selected negotiation record(s)?"}/><br/>
                <Layout isColumnType={true}>
                <Layout.Child size={1}>
                  <InputFilter
                    data={this.state.deleteComment}
                    onSelect={this.onSelect}
                    stateKey="deleteComment"
                    label="Comment (*) : "
                  />
                </Layout.Child>
                <Layout.Child size={0.75}>
                  <FilterButton
                    onClick={this.handleCloseDialog}
                    reset={false}
                    label="CANCEL"
                  />
                  <FilterButton
                    onClick={this.handleActionItem}
                    reset={false}
                    disabled={(this.state.deleteComment !== "undefined" && this.state.deleteComment.trim() !== "") ? false : true}
                    label="OK"
                  />
                </Layout.Child>
                </Layout>
            </Dialog>
      );
    }
  }

  loadDrillDownPanel() {
    if((this.props.currentDayLotData !== "undefined" && this.props.currentDayLotData.length > 0) || (this.props.prevDayLotData !== "undefined" && this.props.prevDayLotData.length > 0))
    return (
        <React.Fragment><br/>
          <Layout isColumnType={true}>
            <Layout.Child size={20}>
              <GridContainer
                  data={this.props.currentDayLotData}
                  label={(typeof this.props.currentDayLotData[0] !== "undefined") ? ("Lot Level at " + convertJavaNYCDashedDate(this.props.currentDayLotData[0].date)) : ""}
                  gridId={"currentDayLotDataGrid"}
                  gridColumns={ getDrillDownColumns() }
                  gridOptions={ drillDownGridOptions() }
              />
            </Layout.Child>
            <Layout.Child size={0.5}>
            </Layout.Child>
            <Layout.Child size={20}>
              <GridContainer
                  data={this.props.prevDayLotData}
                  label={(typeof this.props.prevDayLotData[0] !== "undefined") ? ("Lot Level at " + convertJavaNYCDashedDate(this.props.prevDayLotData[0].date)) : ""}
                  gridId={"prevDayLotDataGrid"}
                  gridColumns={ getDrillDownColumns() }
                  gridOptions={ drillDownGridOptions() }
              />
            </Layout.Child>
          </Layout>
        </React.Fragment>
    );
  }

  render() {
    let grid = null;
    if (!this.props.rateNegotiationVerificationData || this.props.rateNegotiationVerificationData.length <= 0) {
      return (
      <React.Fragment>
        <Message messageData="No data available for this search criteria." />
        {this.loadActionMessageDialogs()}
      </React.Fragment>
      );
    }
    grid = (
      <React.Fragment>
          <Layout>
          <Layout.Child>
              <FilterButton
                onClick={this.handleBulkResolve}
                reset={false}
                label="Bulk Resolve"
                disabled={(typeof this.state.selectedRowData !== "undefined" && this.state.selectedRowData.length > 0) ? false : true}
              />
              <FilterButton
                onClick={this.handleBulkDelete}
                reset={false}
                label="Bulk Delete"
                disabled={(typeof this.state.selectedRowData !== "undefined" && this.state.selectedRowData.length > 0) ? false : true}
              />
          </Layout.Child>
          </Layout>
          <Legend/>
          <Resizer>
          <ResizerContainer key="verificationGrid" className="size--content"><br/>
            <RateNegotiationVerificationDataGrid
              gridColumns={getNegotiationVerificationColumns()}
              gridId={"rateNegotiationVerificationGrid"}
              gridOptions={gridOptions()}
              onCellClick={this.onCellClickHandler}
              onDblClick={this.onDblClickHandler}
              data={this.props.rateNegotiationVerificationData}
              heightValue={600}
              onSelect={this.onSelect}
              preSelectAllRows={false}
            />
          </ResizerContainer>
          <ResizerContainer key="panel" className="size--content">
          {this.loadDrillDownPanel()}
          </ResizerContainer>
          </Resizer>
      {this.loadDeleteDialog()}
      {this.loadResolveDialog()}
      {this.loadEditDialog()}
      {this.loadActionMessageDialogs()}
      </React.Fragment>
    );
    return grid;
  }

}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      deleteRateNegotiationData,
      resolveRateNegotiationData,
      editRateNegotiationData,
      closeNegotiationActionMessage,
      fetchCurrentDayLotLevelData,
      fetchPrevDayLotLevelData,
      fetchRateNegotiationVerificationData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    rateNegotiationVerificationData: state.rateNegotiationVerification.rateNegotiationVerificationData,
    deleteStatus: state.rateNegotiationVerification.deleteStatus,
    resolveStatus: state.rateNegotiationVerification.resolveStatus,
    editStatus: state.rateNegotiationVerification.editStatus,
    prevDayLotData: state.rateNegotiationVerification.prevDayLotData,
    currentDayLotData: state.rateNegotiationVerification.currentDayLotData,
    searchPayload: state.rateNegotiationVerification.searchPayload
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommonGrid);
