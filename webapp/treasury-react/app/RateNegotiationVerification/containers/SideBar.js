import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import Sidebar from "commons/components/Sidebar";
import Label from "commons/components/Label";
import ColumnLayout from "commons/components/ColumnLayout";
import Column from "commons/components/Column";
import Panel from "commons/components/Panel";
import CheckboxFilter from "commons/components/CheckboxFilter";
import FilterButton from "commons/components/FilterButton";
import GenericSecurityFilter from "commons/components/GenericSecurityFilter";
import RateNegotiationVerificationStatusFilter from "commons/container/RateNegotiationVerificationStatusFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import BookFilter from "commons/container/BookFilter";
import CpeFilter from "commons/container/CpeFilter";
import BorrowLendFilter from "commons/container/BorrowLendFilter";
import { createAPIPayload } from "../util";
import { CLASS_NAMESPACE } from "commons/ClassConfigs";
import {
  fetchRateNegotiationVerificationData,
  destroyRateNegotiationVerificationData,
  destroyLotLevelData,
  resizeCanvas
} from "../actions";
import {
  getPreviousDate,
  getCommaSeparatedValues,
  getCommaSeparatedListValue,
  getCommaSeparatedValuesOrNullForAll
} from "commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
    this.state = {};
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  resizeCanvas() {
    this.props.resizeCanvas();
  }

  getDefaultFilters() {
    return {
      securityFilter: {
        selectedSpns: "",
        selectedSecuritySearchType: { key: "SECURITY_NAME", value: "SECURITY_NAME" },
        selectedTextSearchType: { key: "BEGINS_WITH", value: "BEGINS_WITH" },
        securitySearchString: "",
        isAdvancedSearch: false
      },
      selectedRateNegotiationVerificationStatus: [],
      selectedLegalEntities: [],
      selectedCurrencies: [],
      selectedCpes: [],
      selectedBooks: [],
      selectedBorrowType: [],
      selectedStartDate: getPreviousDate(),
      selectedEndDate: getPreviousDate(),
      includeCash: true,
      includeSwap: true,
      toggleSidebar: false
    };
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
    var payload = {
      "localStartDate": this.state.selectedStartDate,
      "localEndDate": this.state.selectedEndDate,
      ...(this.state.selectedCpes.length > 0
        ? {
            "counterpartyIds":
              this.state.selectedCpes

          }
        : null),
      ...(this.state.selectedLegalEntities.length > 0
        ? {
            "legalEntityIds":
              this.state.selectedLegalEntities

          }
        : null),
      ...(
          this.state.securityFilter.isAdvancedSearch &&
          this.state.securityFilter.securitySearchString.trim() !== ""
        ? { "securityFilter":{
          "@CLASS": CLASS_NAMESPACE.com.arcesium.treasury.model.common.security.SecurityFilter,
          "securitySearchType": this.state.securityFilter.selectedSecuritySearchType.value,
          "textSearchType": this.state.securityFilter.selectedTextSearchType.value,
          "searchStrings": this.state.securityFilter.securitySearchString
        } }
        : {
          ...(this.state.securityFilter.selectedSpns && this.state.securityFilter.selectedSpns.trim() !== ""
            ? {
              "spns": this.state.securityFilter.selectedSpns
            }
            : null)
        }
      ),
      ...(this.state.selectedCurrencies.length > 0
        ? {
            "ccySpns":
              this.state.selectedCurrencies

          }
        : null),
      ...(this.state.selectedBorrowType.length > 0
        ? {
            "borrowTypes":
              this.state.selectedBorrowType

          }
        : null),
      ...(this.state.selectedRateNegotiationVerificationStatus.length > 0
        ? {
            "verificationResult":
              this.state.selectedRateNegotiationVerificationStatus

          }
        : null),
      "includeBorrow": this.state.includeCash,
      "includeSwap": this.state.includeSwap
    };

      if (this.state.selectedBooks.length > 0) {
        payload["bookIds"] =
          this.state.selectedBooks
        ;
      }
      this.props.destroyLotLevelData();
      this.props.fetchRateNegotiationVerificationData(createAPIPayload(payload));
  }

  render() {
    return (
      <Sidebar
        collapsible={true}
        size="400px"
        resizeCanvas={this.resizeCanvas}
        toggleSidebar={this.state.toggleSidebar}
      >
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedStartDate"
          data={this.state.selectedStartDate}
          label="Start Date"
        />
        <DateFilter
          onSelect={this.onSelect}
          stateKey="selectedEndDate"
          data={this.state.selectedEndDate}
          label="End Date"
        />
        <Panel>
          <Label label="Securities" />
          <GenericSecurityFilter
            onSelect={this.onSelect}
            selectedData={this.state.securityFilter}
            stateKey="securityFilter"
          />
        </Panel>
        <LegalEntityFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedLegalEntities}
        />
        <CpeFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCpes}
        />
        <BookFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBooks}
        />
        <CurrencyFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedCurrencies}
        />
        <Panel>
          <Label label="Position Type" />
          <Column>
            <CheckboxFilter
              defaultChecked={this.state.includeCash}
              onSelect={this.onSelect}
              stateKey="includeCash"
              label="Cash"
            />
            <CheckboxFilter
              defaultChecked={this.state.includeSwap}
              onSelect={this.onSelect}
              stateKey="includeSwap"
              label="Swap"
            />
          </Column>
        </Panel>
        <RateNegotiationVerificationStatusFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedRateNegotiationVerificationStatus}
        /><br/>
        <BorrowLendFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedBorrowType}
          excludeRehype={true}
        /><br/>
        <ColumnLayout>
          <FilterButton
            onClick={this.handleClick}
            reset={false}
            label="Search"
          />
          <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchRateNegotiationVerificationData,
      destroyRateNegotiationVerificationData,
      destroyLotLevelData,
      resizeCanvas
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    rateNegotiationVerificationData: state.rateNegotiationVerification.rateNegotiationVerificationData
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
