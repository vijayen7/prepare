import React from "react";

export default class Legend extends React.Component {
  render() {
    return (
          <div className="legend__item"><br/>
            <span className="margin--left--small margin--right--double">
              <b>NOTE:</b> The quantities shown in
              <span className="red7 margin--left--small margin--right--small">
                red
              </span>
              indicate the cases where there is a quantity mismatch
            </span>
          </div>
    );
  }
}
