import {
  RESIZE_CANVAS
} from "commons/constants";

import {
  FETCH_RATE_NEGOTIATION_VERIFICATION_DATA,
  DESTROY_RATE_NEGOTIATION_VERIFICATION_DATA,
  DELETE_RATE_NEGOTIATION_DATA,
  RESOLVE_RATE_NEGOTIATION_DATA,
  EDIT_RATE_NEGOTIATION_DATA,
  DESTROY_NEGOTIATION_ACTION_FLAGS,
  FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_CURRENT,
  FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_PREV,
  DESTROY_DRILL_DOWN_LOT_LEVEL_DATA
} from "./constants";

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

export function fetchRateNegotiationVerificationData(payload) {
  return {
    type: FETCH_RATE_NEGOTIATION_VERIFICATION_DATA,
    payload
  };
}

export function destroyRateNegotiationVerificationData() {
  return {
    type: DESTROY_RATE_NEGOTIATION_VERIFICATION_DATA
  };
}

export function deleteRateNegotiationData(payload) {
  return {
    type: DELETE_RATE_NEGOTIATION_DATA,
    payload
  };
}

export function resolveRateNegotiationData(payload) {
  return {
    type: RESOLVE_RATE_NEGOTIATION_DATA,
    payload
  };
}

export function editRateNegotiationData(payload) {
  return {
    type: EDIT_RATE_NEGOTIATION_DATA,
    payload
  };
}

export function closeNegotiationActionMessage(payload) {
  return {
    type: DESTROY_NEGOTIATION_ACTION_FLAGS
  };
}

export function fetchCurrentDayLotLevelData(payload) {
  return {
    type: FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_CURRENT,
    payload
  };
}

export function fetchPrevDayLotLevelData(payload) {
  return {
    type: FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_PREV,
    payload
  };
}

export function destroyLotLevelData(payload) {
  return {
    type: DESTROY_DRILL_DOWN_LOT_LEVEL_DATA
  };
}
