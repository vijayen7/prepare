import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION
} from "commons/constants";

import {
  FETCH_RATE_NEGOTIATION_VERIFICATION_DATA,
  DELETE_RATE_NEGOTIATION_DATA,
  RESOLVE_RATE_NEGOTIATION_DATA,
  EDIT_RATE_NEGOTIATION_DATA,
  FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_CURRENT,
  FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_PREV,
  SET_SEARCH_PAYLOAD
} from "./constants";

import {
  getRateNegotiationVerificationData,
  deleteRateNegotiationData,
  resolveRateNegotiationData,
  editRateNegotiationData,
  fetchLotLevelDataForDrillDown
} from "./api";

function* fetchRateNegotiationVerificationData(action) {
  try {
    yield put({ type: START_LOADING });
    const payload = action.payload;
    const data = yield call(getRateNegotiationVerificationData, action.payload);
    yield put({ type: `${FETCH_RATE_NEGOTIATION_VERIFICATION_DATA}_SUCCESS`, data });
    yield put({ type: SET_SEARCH_PAYLOAD, data: payload });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_RATE_NEGOTIATION_VERIFICATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* removeRateNegotiationData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(deleteRateNegotiationData, action.payload);
    if (data === true) { yield put({ type: `${DELETE_RATE_NEGOTIATION_DATA}_SUCCESS`, data }); } else { yield put({ type: `${DELETE_RATE_NEGOTIATION_DATA}_FAILURE`, data }); }
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${DELETE_RATE_NEGOTIATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* resolveRateNegotiationVerificationData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(resolveRateNegotiationData, action.payload);
    yield put({ type: `${RESOLVE_RATE_NEGOTIATION_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${RESOLVE_RATE_NEGOTIATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* editRateNegotiationVerificationData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(editRateNegotiationData, action.payload);
    if (data === true) { yield put({ type: `${EDIT_RATE_NEGOTIATION_DATA}_SUCCESS`, data }); } else { yield put({ type: `${EDIT_RATE_NEGOTIATION_DATA}_FAILURE`, data }); }
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${EDIT_RATE_NEGOTIATION_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchLotLevelDataCurrent(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(fetchLotLevelDataForDrillDown, action.payload);
    yield put({ type: `${FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_CURRENT}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_CURRENT}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchLotLevelDataPrev(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(fetchLotLevelDataForDrillDown, action.payload);
    yield put({ type: `${FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_PREV}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_PREV}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* lotLevelDataForDrillDownPrev() {
  yield [
    takeEvery(FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_PREV, fetchLotLevelDataPrev)
  ];
}

export function* lotLevelDataForDrillDownCurrent() {
  yield [
    takeEvery(FETCH_LOT_LEVEL_DATA_FOR_DRILL_DOWN_CURRENT, fetchLotLevelDataCurrent)
  ];
}

export function* rateNegotiationVerificationDataEdit() {
  yield [
    takeEvery(EDIT_RATE_NEGOTIATION_DATA, editRateNegotiationVerificationData)
  ];
}

export function* rateNegotiationVerificationDataResolution() {
  yield [
    takeEvery(RESOLVE_RATE_NEGOTIATION_DATA, resolveRateNegotiationVerificationData)
  ];
}

export function* rateNegotiationVerificationDataDeletion() {
  yield [
    takeEvery(DELETE_RATE_NEGOTIATION_DATA, removeRateNegotiationData)
  ];
}

export function* rateNegotiationVerificationData() {
  yield [
    takeEvery(FETCH_RATE_NEGOTIATION_VERIFICATION_DATA, fetchRateNegotiationVerificationData)
  ];
}

function* rateNegotiationVerificationSaga() {
  yield all([rateNegotiationVerificationData(), rateNegotiationVerificationDataDeletion(), rateNegotiationVerificationDataResolution(), rateNegotiationVerificationDataEdit(), lotLevelDataForDrillDownCurrent(), lotLevelDataForDrillDownPrev()]);
}

export default rateNegotiationVerificationSaga;
