import { BASE_URL, INPUT_FORMAT_JSON } from 'commons/constants';
import { CLASS_NAMESPACE } from "commons/ClassConfigs";
import { fetchPostURL, getEncodedParams } from "commons/util";
const queryString = require("query-string");
export let url = "";

export function getRateNegotiationVerificationData(payload) {
  const encodedParams = getEncodedParams(payload, CLASS_NAMESPACE.com.arcesium.treasury.model.seclend.negotiation.RateNegotiationVerificationFilter);
  url = `${BASE_URL}service/rateNegotiationVerificationService/verify?verificationFilter=${encodedParams}${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function deleteRateNegotiationData(payload) {
  const paramString = queryString.stringify(payload);
  console.log(paramString);
  url = `${BASE_URL}service/rateNegotiationDataService/delete?rateNegotiationDataIds=${payload.ids}&comment=${payload.comment}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function resolveRateNegotiationData(payload) {
  var encodedParam = "verificationData=" + encodeURIComponent(JSON.stringify(payload)) + "&inputFormat=JSON_WITH_REF&format=JSON"
  url = `${BASE_URL}service/rateNegotiationDataService/resolve`;
  return fetchPostURL(url,encodedParam);
}

export function editRateNegotiationData(payload) {
  var encodedParam = "rateNegotiationDataList=" + encodeURIComponent(JSON.stringify(payload)) + "&inputFormat=JSON_WITH_REF&format=JSON"
  url = `${BASE_URL}service/rateNegotiationDataService/edit`;
  return fetchPostURL(url,encodedParam);
}

export function fetchLotLevelDataForDrillDown(payload) {
  var encodedParam = "verificationData=" + encodeURIComponent(JSON.stringify(payload.rateNegotiationVerificationData)) + "&date=" + encodeURIComponent(JSON.stringify(payload.date)) + "&inputFormat=JSON_WITH_REF&format=JSON"
  url = `${BASE_URL}service/rateNegotiationVerificationService/getLotLevelMarketData`;
  return fetchPostURL(url,encodedParam);
}
