import cssVars from '../css_vars';
const lightVars = Object.keys(cssVars['light-theme']);
const darkVars = Object.keys(cssVars['dark-theme']);

describe('CSS Vars', () => {
  it('should have same size of dark and light theme variables', () => {
    expect(lightVars.length === darkVars.length).toBeTruthy();
  });
  it('should have same keys in both dark and light objects', () => {
    const keyFlag = lightVars.some((cssVar) => typeof darkVars[cssVar] === 'undefined');
    expect(keyFlag).toBeFalsy();
  });
});
