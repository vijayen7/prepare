import { BASE_URL } from 'commons/constants';
import { fetchPostURL } from 'commons/util';
export let url = '';

export function getThresholdsData(payload) {
  url = `${BASE_URL}service/cashManagementLiquidityService/getCashManagementThreshold?format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function saveThresholdsData(payload) {
  let param = JSON.stringify(payload.cashManagementThreshold);
  let mmfCutoffTimeList = JSON.stringify(payload.mmfCutoffTimeList);
  url = `${BASE_URL}service/cashManagementLiquidityService/publishCashManagementThreshold`;
  param = encodeURIComponent(param);
  mmfCutoffTimeList = encodeURIComponent(mmfCutoffTimeList);
  let workflowId = encodeURIComponent(payload.workflowId);
  let encodedParam =
    'cashManagementThreshold=' +
    param +
    '&mmfCutoffTimeList=' +
    mmfCutoffTimeList +
    '&workflowId=' +
    workflowId +
    '&inputFormat=JSON_WITH_REF&format=JSON';
  return fetchPostURL(url, encodedParam);
}

export function fetchAllMMFs(payload) {
  url = `${BASE_URL}service/cashManagementLiquidityService/getAllMMFs?format=JSON`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}

export function getMMFCutoffTimeData(mmfSpnList) {
  url = `${BASE_URL}service/cashManagementLiquidityService/getMMFCutoffTimeList?format=JSON&mmfSpnList=${mmfSpnList.toString()}`;
  return fetch(url, {
    credentials: 'include'
  }).then((data) => data.json());
}
