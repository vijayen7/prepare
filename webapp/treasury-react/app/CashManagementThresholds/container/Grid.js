import React, { Component } from 'react';
import { gridColumns } from '../grid/columnConfig';
import { gridOptions } from '../grid/gridOptions';
import FilterButton from 'commons/components/FilterButton';
import { beginWorkflow } from '../../commons/actions/workflow';
import { Panel, Layout, Dialog } from 'arc-react-components';
import { VISIBLE, HIDDEN } from '../../commons/constants';
import { RagThresholds } from './RagThresholdPanel';
import { EntityThresholds } from '../component/EntityThresholdPanel';
import { TimeThresholdPanel } from '../component/TimeThresholdPanel';
import {
  createCashManagementThresholds,
  getCurrentDate,
  validateEntityThresholds,
  validateCutoffTime,
  getMmfCutoffTimeList
} from '../Utils';
import { inject, observer } from 'mobx-react';
import AddMMFDialog from './AddMMFDialog';

@inject('cashManagementThresholdsStore')
@observer
export default class Grid extends Component {
  componentWillMount() {
    let payload = {};
    let mmfSpnList = [-1];
    this.props.cashManagementThresholdsStore.fetchThresholdsData(payload);
    this.props.cashManagementThresholdsStore.getMMFCutoffTimeList(mmfSpnList);
  }

  componentDidUpdate(prevProps) {
    this.props.cashManagementThresholdsStore.handleWorkflowStatusChange(
      prevProps,
      this.props.workflowStatus,
      this.props.workflowId
    );
  }

  onEdit = () => {
    this.beginWorkflow();
  };

  beginWorkflow = () => {
    this.props.cashManagementThresholdsStore.toggelLoadingStatus();
    let date = getCurrentDate();
    const payload = {
      'workflowParam.dateStr': date,
      'workflowParam.entityFamilyId': -1,
      'workflowParam.workflowTypeName': 'CASH_MANAGEMENT_THRESHOLD_EDIT',
      'workflowParam.workflowUnit': 'EntityDate'
    };
    const params = {
      dateStr: date,
      entityFamilyId: -1,
      workflowTypeName: 'CASH_MANAGEMENT_THRESHOLD_EDIT',
      workflowUnit: 'EntityDate'
    };
    window.store.dispatch(beginWorkflow(payload, params));
    this.props.cashManagementThresholdsStore.toggelLoadingStatus();
  };

  onSave = () => {
    if (
      !validateEntityThresholds(
        this.props.cashManagementThresholdsStore.thresholdsData.cashManagementEntityThresholdList
      )
    ) {
      this.displayAlert('Invalid Entity Values');
      return;
    }

    if (!validateCutoffTime(this.props.cashManagementThresholdsStore.mmfCutoffTimeList)) {
      this.displayAlert('Invalid Cutoff Time');
      return;
    }

    let cashManagementThreshold = createCashManagementThresholds(
      this.props.cashManagementThresholdsStore.thresholdsData.ragThresholds,
      this.props.cashManagementThresholdsStore.thresholdsData.cashManagementEntityThresholdList,
      this.props.cashManagementThresholdsStore.mmfCutoffTimeList
    );
    let mmfCutoffTimeList = getMmfCutoffTimeList(this.props.cashManagementThresholdsStore.mmfCutoffTimeList);
    if (this.props.cashManagementThresholdsStore.thresholdsData.ragThresholds.entityLevelAmberMinThreshold == 0) {
      delete cashManagementThreshold.cashManagementRAGThresholdList;
    }
    let payload = {
      cashManagementThreshold: cashManagementThreshold,
      mmfCutoffTimeList: mmfCutoffTimeList,
      workflowId: this.props.cashManagementThresholdsStore.workflowData.workflowId
    };

    this.props.cashManagementThresholdsStore.saveThresholdsData(payload);
  };

  addMMF = () => {
    let mmfList = this.props.cashManagementThresholdsStore.mmfList;
    if (Object.entries(mmfList).length === 0 && mmfList.constructor === Object) {
      this.fetchMMFs();
    }
    this.props.cashManagementThresholdsStore.showAddMMFDialog();
  };
  fetchMMFs = () => {
    this.props.cashManagementThresholdsStore.fetchMMFs();
  };

  closeAlert = () => {
    this.props.cashManagementThresholdsStore.closeAlertDialog();
  };

  displayAlert = (message) => {
    this.props.cashManagementThresholdsStore.displayAlertDialog(message);
  };

  closeSaveDialog = () => {
    this.props.cashManagementThresholdsStore.closeSaveDialog();
  };

  changeRagThresholds = (ragThreholdValues) => {
    this.props.cashManagementThresholdsStore.changeRagThresholds(ragThreholdValues);
  };

  hideAddMMFDialog = () => {
    this.props.cashManagementThresholdsStore.hideAddMMFDialog();
  };

  saveMMFCutoffTime = (newMMFCutoffTime) => {
    this.props.cashManagementThresholdsStore.saveMMFCutoffTime(newMMFCutoffTime);
  };

  renderGridData = () => {
    let ragThreholdValues = this.props.cashManagementThresholdsStore.thresholdsData.ragThresholds;
    if (ragThreholdValues.entityLevelAmberMinThreshold == 0) {
      return '';
    }

    let editButtonVisibility = HIDDEN;

    if (this.props.cashManagementThresholdsStore.thresholdsData.isUserAuthorizedForEdit) {
      editButtonVisibility = VISIBLE;
    }

    return (
      <React.Fragment>
        <div className="button">
          <FilterButton
            onClick={this.onEdit}
            label="EDIT"
            disabled={this.props.cashManagementThresholdsStore.editButtonDisabled}
            style={{ visibility: editButtonVisibility }}
          />
        </div>
        <div>
          <Dialog
            isOpen={this.props.cashManagementThresholdsStore.showAlertDialog.isDialogOpen}
            title="Alert"
            onClose={this.closeAlert}
          >
            {this.props.cashManagementThresholdsStore.showAlertDialog.displayText}
          </Dialog>
          <Dialog
            isOpen={this.props.cashManagementThresholdsStore.showSaveDialog.isDialogOpen}
            title="Alert"
            onClose={this.closeSaveDialog}
          >
            {this.props.cashManagementThresholdsStore.showSaveDialog.displayText}
          </Dialog>
          <Layout>
            <Layout.Child size={1} childId="child1">
              <RagThresholds
                ragThresholds={ragThreholdValues}
                editInProgress={this.props.cashManagementThresholdsStore.editInProgress}
                changeRagThresholds={this.changeRagThresholds}
                displayAlert={this.displayAlert}
              />
            </Layout.Child>
            <Layout.Child size={1} childId="child2">
              <Panel title="ENTITY THRESHOLDS">
                <EntityThresholds
                  data={this.props.cashManagementThresholdsStore.thresholdsData.cashManagementEntityThresholdList}
                  gridId="ThresholdsData"
                  columns={gridColumns(this.props.cashManagementThresholdsStore.editInProgress, 'ENTITY THRESHOLDS')}
                  options={gridOptions(this.props.cashManagementThresholdsStore.editInProgress)}
                  resizeCanvas={this.props.cashManagementThresholdsStore.resizeCanvasAction}
                />
              </Panel>
            </Layout.Child>
            <Layout.Child size={1} childId="child3">
              <FilterButton
                style={{
                  visibility: this.props.cashManagementThresholdsStore.saveButtonVisibility
                }}
                label="Add MMF Cutoff Time"
                onClick={this.addMMF}
              />
              <AddMMFDialog
                display={this.props.cashManagementThresholdsStore.isNewMMFDialogVisible}
                hideAddMMFDialog={this.hideAddMMFDialog}
                mmfList={this.props.cashManagementThresholdsStore.mmfList}
                saveMMFCutoffTime={this.saveMMFCutoffTime}
              />
              {this.props.cashManagementThresholdsStore.mmfCutoffTimeList.length > 0 && (
                <Panel title="MMF CUTOFF TIMES">
                  <TimeThresholdPanel
                    data={this.props.cashManagementThresholdsStore.mmfCutoffTimeList}
                    gridId="CutoffTimeData"
                    columns={gridColumns(this.props.cashManagementThresholdsStore.editInProgress, 'CUTOFF TIME')}
                    options={gridOptions(this.props.cashManagementThresholdsStore.editInProgress)}
                    resizeCanvas={this.props.cashManagementThresholdsStore.resizeCanvasAction}
                  />
                </Panel>
              )}
              <FilterButton
                style={{
                  visibility: this.props.cashManagementThresholdsStore.saveButtonVisibility
                }}
                label="SAVE"
                onClick={this.onSave}
              />
            </Layout.Child>
          </Layout>
        </div>
      </React.Fragment>
    );
  };

  render() {
    return <div className="padding">{this.renderGridData()}</div>;
  }
}
