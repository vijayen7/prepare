import React, { Component } from 'react';
import { Inputbox } from '../component/Inputbox';
import { Panel } from 'arc-react-components';

export class RagThresholds extends Component {
  constructor(props) {
    super(props);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.ragChange = this.ragChange.bind(this);
    this.isRagThresholdValid = this.isRagThresholdValid.bind(this);
  }

  componentWillMount() {
    this.setState(this.getDefaultState());
  }

  getDefaultState() {
    return {
      ragThresholds: {
        entityLevelAmberMinThreshold: this.props.ragThresholds.entityLevelAmberMinThreshold,
        entityLevelRedMinThreshold: this.props.ragThresholds.entityLevelRedMinThreshold,
        fundLevelAmberMinThreshold: this.props.ragThresholds.fundLevelAmberMinThreshold,
        fundLevelRedMinThreshold: this.props.ragThresholds.fundLevelRedMinThreshold
      }
    };
  }

  ragChange(field, args) {
    if (!args) {
      return;
    }

    let ragThresholds = { ...this.state.ragThresholds };
    ragThresholds[field] = Number(args.target.value);

    if (!this.isRagThresholdValid(ragThresholds)) {
      let message = 'Invalid RAG Values, Changes will be reverted';
      this.props.displayAlert(message);
      return;
    }
    this.setState((previousState) => {
      return {
        ...previousState,
        ragThresholds: ragThresholds
      };
    });
    this.props.changeRagThresholds(ragThresholds);
  }

  isRagThresholdValid(ragThresholds) {
    if (
      ragThresholds.entityLevelRedMinThreshold <= ragThresholds.entityLevelAmberMinThreshold ||
      ragThresholds.fundLevelRedMinThreshold <= ragThresholds.fundLevelAmberMinThreshold
    ) {
      return false;
    }
    return true;
  }

  render() {
    return (
      <Panel title="RAG THRESHOLDS">
        <div>
          ENTITY LEVEL
          <div style={{ margin: '20px' }}>
            AMBER
            <Inputbox
              value={this.state.ragThresholds.entityLevelAmberMinThreshold}
              onChange={(e) => this.ragChange('entityLevelAmberMinThreshold', e)}
              disabled={!this.props.editInProgress}
              min="0"
              max="99"
            />
            RED
            <Inputbox
              value={this.state.ragThresholds.entityLevelRedMinThreshold}
              onChange={(e) => this.ragChange('entityLevelRedMinThreshold', e)}
              disabled={!this.props.editInProgress}
              min="1"
              max="100"
            />
          </div>
        </div>
        <div>
          FUND LEVEL
          <div style={{ margin: '20px' }}>
            AMBER
            <Inputbox
              value={this.state.ragThresholds.fundLevelAmberMinThreshold}
              onChange={(e) => this.ragChange('fundLevelAmberMinThreshold', e)}
              disabled={!this.props.editInProgress}
              min="0"
              max="99"
            />
            RED
            <Inputbox
              value={this.state.ragThresholds.fundLevelRedMinThreshold}
              onChange={(e) => this.ragChange('fundLevelRedMinThreshold', e)}
              disabled={!this.props.editInProgress}
              min="1"
              max="100"
            />
          </div>
        </div>
      </Panel>
    );
  }
}
