import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dialog } from 'arc-react-components';
import SingleSelectFilter from '../../commons/components/SingleSelectFilter';
import TimeInputBox from '../component/TimeInputBox';
export default class AddMMFDialog extends Component {
  constructor(props) {
    super(props);
    this.getDefaultState = this.getDefaultState.bind(this);
    this.hideAddMMFDialog = this.hideAddMMFDialog.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.hideSaveMMFDialog = this.hideSaveMMFDialog.bind(this);
    this.saveMMFCutoffTime = this.saveMMFCutoffTime.bind(this);
    this.state = this.getDefaultState();
  }

  getDefaultState() {
    return {
      selectedMMFSpn: {
        key: 0,
        value: ''
      },
      businessDayMMFThresholdTime: '00:00',
      holidayMMFThresholdTime: '00:00',
      displaySaveMMFDialog: false
    };
  }

  onSelect(params) {
    const { key, value } = params;
    const newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  handleReset() {
    this.setState(this.getDefaultState());
  }

  saveMMFCutoffTime() {
    let data = {
      id: parseInt(this.state.selectedMMFSpn.key),
      spn: parseInt(this.state.selectedMMFSpn.key),
      ticker: this.state.selectedMMFSpn.value,
      businessDayMMFThresholdTimeString: this.state.businessDayMMFThresholdTime,
      holidayMMFThresholdTimeString: this.state.holidayMMFThresholdTime
    };

    this.props.saveMMFCutoffTime(data);
    this.setState({ ...this.state, displaySaveMMFDialog: true });
  }

  hideAddMMFDialog() {
    this.props.hideAddMMFDialog();
    this.handleReset();
  }

  hideSaveMMFDialog() {
    this.setState({ ...this.state, displaySaveMMFDialog: false });
    this.hideAddMMFDialog();
  }

  render() {
    return (
      <Dialog
        isOpen={this.props.display}
        title="Add Cutoff Time for an MMF"
        onClose={this.hideAddMMFDialog}
        footer={
          <React.Fragment>
            <button onClick={this.saveMMFCutoffTime}>Add</button>
            <button onClick={this.handleReset}>Reset</button>
          </React.Fragment>
        }
      >
        <Dialog
          isOpen={this.state.displaySaveMMFDialog}
          title="Alert"
          // onClose={this.hideSaveMMFDialog}
          footer={
            <React.Fragment>
              <button onClick={this.hideSaveMMFDialog}>Okay</button>
            </React.Fragment>
          }
        >
          Please press 'SAVE' button to save this data.
        </Dialog>
        <SingleSelectFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedMMFSpn}
          stateKey="selectedMMFSpn"
          data={this.props.mmfList}
          label="MMF Spn"
        />
        <TimeInputBox
          label="Business Day MMF Threshold Time"
          stateKey="businessDayMMFThresholdTime"
          data={this.state.businessDayMMFThresholdTime}
          onSelect={this.onSelect}
        />
        <TimeInputBox
          label="Holiday MMF Threshold Time"
          stateKey="holidayMMFThresholdTime"
          data={this.state.holidayMMFThresholdTime}
          onSelect={this.onSelect}
        />
      </Dialog>
    );
  }
}

AddMMFDialog.propTypes = {
  saveMMFCutoffTime: PropTypes.func,
  hideAddMMFDialog: PropTypes.func,
  display: PropTypes.bool,
  mmfList: PropTypes.any
};
