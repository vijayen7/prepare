import { Component } from 'react';
import { observable, action, flow } from 'mobx';
import { getThresholdsData, getMMFCutoffTimeData, saveThresholdsData, fetchAllMMFs } from './api';
declare const window: any;
import _ from 'lodash';
import {
  DATA_SAVED,
  SAVE_FAILED,
  BEGIN_WORKFLOW,
  RESUME_WORKFLOW,
  VISIBLE,
  HIDDEN,
  HANDLE_EXCEPTION
} from '../commons/constants';
import { flattenThresholdsData, prepareMMFCutoffTimeData } from './Utils';

const INITIAL_THRESHOLD_DATA = {
  cashManagementEntityThresholdList: [],
  isUserAuthorizedForEdit: false,
  ragThresholds: {
    entityLevelAmberMinThreshold: 0,
    entityLevelRedMinThreshold: 0,
    fundLevelAmberMinThreshold: 0,
    fundLevelRedMinThreshold: 0
  }
};

interface Dialog {
  isDialogOpen: boolean;
  displayText: string;
}
const INITIAL_DIALOG_STATE = {
  isDialogOpen: false,
  displayText: ''
};

interface WorkflowData {
  workflowId: number;
  workflowStatus: string;
}

const INITIAL_WORKFLOW_DATA = {
  workflowId: 0,
  workflowStatus: ''
};

export default class CashManagementThresholdsStore extends Component {
  @observable
  saveButtonVisibility: string = HIDDEN;

  @observable
  editButtonDisabled: boolean = false;

  @observable
  editInProgress: boolean = false;

  @observable
  workflowData: WorkflowData = INITIAL_WORKFLOW_DATA;

  @observable
  thresholdsData = INITIAL_THRESHOLD_DATA;

  @observable
  mmfCutoffTimeList: any = [];

  @observable
  resizeCanvas: boolean = false;

  @observable
  savedThresholdsData: any = {};

  @observable
  showAlertDialog: Dialog = INITIAL_DIALOG_STATE;

  @observable
  showSaveDialog: Dialog = INITIAL_DIALOG_STATE;

  @observable
  loadingInProgress: boolean = false;

  @observable
  isNewMMFDialogVisible: boolean = false;

  @observable
  mmfList: Object = {};

  @action
  displayAlertDialog = (payload: string): void => {
    this.showAlertDialog = {
      isDialogOpen: true,
      displayText: payload
    };
  };

  @action
  closeAlertDialog(): void {
    this.showAlertDialog.isDialogOpen = false;
  }

  @action
  displaySaveDialog = (payload: string): void => {
    this.showSaveDialog = {
      isDialogOpen: true,
      displayText: payload
    };
  };

  @action
  closeSaveDialog(): void {
    this.showSaveDialog.isDialogOpen = false;
  }

  @action
  resizeCanvasAction(): void {
    this.resizeCanvas = !this.resizeCanvas;
  }

  @action
  updateWorkflowStatus(workflowStatus: string, workflowId: number): void {
    this.workflowData = {
      workflowStatus: workflowStatus,
      workflowId: workflowId
    };
  }

  @action
  showAddMMFDialog() {
    this.isNewMMFDialogVisible = true;
  }

  @action
  hideAddMMFDialog() {
    this.isNewMMFDialogVisible = false;
  }

  @action
  saveMMFCutoffTime(newMMFCutoffTime: { id: any; ticker: any; spn: any; businessDayMMFThresholdTimeString: any; holidayMMFThresholdTimeString: any; }) {
    const { id, ticker, spn, businessDayMMFThresholdTimeString, holidayMMFThresholdTimeString } = newMMFCutoffTime;
    if (
      typeof id !== 'number' ||
      id === 0 ||
      typeof ticker !== 'string' ||
      ticker === '' ||
      typeof spn !== 'number' ||
      typeof businessDayMMFThresholdTimeString !== 'string' ||
      typeof holidayMMFThresholdTimeString !== 'string'
    ) {
      this.displayAlertDialog('Invalid MMF Cutoff Time Added');
    } else if (this.mmfCutoffTimeList.filter((e: { spn: number; }) => e.spn === spn).length === 1) {
      this.displayAlertDialog('MMF Cutoff Time already Present');
    } else {
      this.mmfCutoffTimeList.push(newMMFCutoffTime);
    }
  }

  @action
  handleWorkflowStatusChange(prevProps: any, workflowStatus: string, workflowId: number): void {
    if (
      (workflowStatus == BEGIN_WORKFLOW || workflowStatus == RESUME_WORKFLOW) &&
      prevProps.workflowStatus != workflowStatus
    ) {
      this.saveButtonVisibility = VISIBLE;
      this.editButtonDisabled = true;
      this.editInProgress = true;
      this.workflowData.workflowId = workflowId;
    }
  }

  @action.bound
  changeRagThresholds(ragThreholdValues: any): void {
    this.thresholdsData.ragThresholds = ragThreholdValues;
  }

  @action
  toggelLoadingStatus(): void {
    this.loadingInProgress = !this.loadingInProgress;
  }

  fetchThresholdsData = flow(function* fetchThresholdsData(this: any, payload) {
    try {
      this.toggelLoadingStatus();
      const data = yield getThresholdsData(payload);
      this.thresholdsData = flattenThresholdsData(data);
      this.toggelLoadingStatus();
    } catch (e) {
      this.toggelLoadingStatus();
      window.store.dispatch({ type: HANDLE_EXCEPTION });
    }
  });

  getMMFCutoffTimeList = flow(function* getMMFCutoffTimeList(this: any, mmfSpnList) {
    try {
      this.toggelLoadingStatus();
      const data = yield getMMFCutoffTimeData(mmfSpnList);
      this.mmfCutoffTimeList = prepareMMFCutoffTimeData(data);
      this.toggelLoadingStatus();
    } catch (e) {
      this.toggelLoadingStatus();
      window.store.dispatch({ type: HANDLE_EXCEPTION });
    }
  });

  saveThresholdsData = flow(function* saveCashManagementThresholdsData(this: any, payload) {
    try {
      this.toggelLoadingStatus();
      const data = yield saveThresholdsData(payload);
      this.savedThresholdsData = data;
      if (this.savedThresholdsData && !this.showSaveDialog.isDialogOpen) {
        if (this.savedThresholdsData.response) {
          this.displaySaveDialog(DATA_SAVED);
        } else {
          this.displaySaveDialog(SAVE_FAILED);
        }
      }
      this.toggelLoadingStatus();
    } catch (e) {
      this.toggelLoadingStatus();
      window.store.dispatch({ type: HANDLE_EXCEPTION });
    }
  });

  fetchMMFs = flow(function* fetchMMFs(this: any) {
    try {
      this.toggelLoadingStatus();
      const data = yield fetchAllMMFs();
      if (data !== undefined) {
        let mmfs: any[] | never[] = [];
        _.forEach(data, (value: any, key: any) => {
          let obj: any = {};
          obj.key = key;
          obj.value = value;
          mmfs = mmfs.concat(obj);
        });

        mmfs.sort((a, b) => {
          const valueA = a.value.toLowerCase();
          const valueB = b.value.toLowerCase();
          if (valueA < valueB) return -1;
          if (valueA > valueB) return 1;
          return 0;
        });
        this.mmfList = mmfs;
        this.toggelLoadingStatus();
      }
    } catch (e) {
      this.toggelLoadingStatus();
      window.store.dispatch({ type: HANDLE_EXCEPTION });
    }
  });
}
