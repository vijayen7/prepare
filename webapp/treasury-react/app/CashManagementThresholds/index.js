import React, { Component } from 'react';
import Grid from './container/Grid';
import Workflow from '../commons/components/Workflow';
import { inject, observer } from 'mobx-react';
import { ReactLoader } from '../commons/components/ReactLoader';
import ExceptionDialog from '../commons/container/ExceptionDialog';

@inject('cashManagementThresholdsStore')
@observer
export default class CashManagementThresholds extends Component {
  updateWorkflowStatus = (workflowStatus, workflowId) => {
    this.props.cashManagementThresholdsStore.updateWorkflowStatus(workflowStatus, workflowId);
  };

  render() {
    return (
      <React.Fragment>
        <ReactLoader inProgress={this.props.cashManagementThresholdsStore.loadingInProgress} />
        <ExceptionDialog />
        <Workflow onbeginWorkflow={this.updateWorkflowStatus} />
        <div className="layout--flex--row">
          <arc-header className="size--content" user={USER} modern-themes-enabled>
            <div slot="application-menu" className="application-menu-toggle-view" />
          </arc-header>
          <div className="layout--flex">
            <Grid
              workflowStatus={this.props.cashManagementThresholdsStore.workflowData.workflowStatus}
              user={USER}
              workflowId={this.props.cashManagementThresholdsStore.workflowData.workflowId}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}
