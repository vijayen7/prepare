import { ReactArcGrid } from 'arc-grid';
import React from 'react';

export const EntityThresholds = (props) => {
  return (
    <ReactArcGrid
      data={props.data}
      gridId="EntityThresholdsData"
      columns={props.columns}
      options={props.options}
      resizeCanvas={props.resizeCanvas}
    />
  );
};
