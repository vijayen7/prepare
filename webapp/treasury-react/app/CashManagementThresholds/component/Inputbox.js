import React from 'react';

export const Inputbox = (props) => {
  return (
    <input
      style={{
        marginLeft: '25px',
        marginRight: '25px',
        width: '50px'
      }}
      value={props.value}
      className="border"
      type="number"
      onChange={props.onChange}
      disabled={props.disabled}
      min={props.min}
      max={props.max}
    />
  );
};
