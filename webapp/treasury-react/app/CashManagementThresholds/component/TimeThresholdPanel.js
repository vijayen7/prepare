import { ReactArcGrid } from 'arc-grid';
import React from 'react';

export const TimeThresholdPanel = (props) => {
  return (
    <ReactArcGrid
      data={props.data}
      gridId="TimeThresholdData"
      columns={props.columns}
      options={props.options}
      resizeCanvas={props.resizeCanvas}
    />
  );
};
