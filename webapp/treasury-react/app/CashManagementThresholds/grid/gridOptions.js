export function gridOptions(view) {
  var options = {
    autoHorizontalScrollBar: true,
    maxHeight: 400,
    sheetName: 'Thresholds data',
    exportToExcel: true,
    forceFitColumns: true,
    editable: view
      ? {
          changeCellStyleOnEdit: true
        }
      : false
  };

  return options;
}
