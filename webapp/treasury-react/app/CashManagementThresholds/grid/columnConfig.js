import { textFormatter,numberFormatterDefaultZero } from '../../commons/grid/formatters';
import { TimeEditor } from '../Utils';

export function gridColumns(view, panel) {
  switch (panel) {
    case 'ENTITY THRESHOLDS':
      return [
        {
          id: 'fundName',
          name: 'FUND NAME',
          field: 'fundName',
          toolTip: 'FUND NAME',
          type: 'text',
          formatter: textFormatter,
          headerCssClass: 'aln-rt b'
        },
        {
          id: 'minThreshold',
          name: 'Min Threshold',
          field: 'minThreshold',
          toolTip: 'Min Threshold',
          type: 'text',
          isEditable: view,
          sortable: false,
          editor: dpGrid.Editors.Text,
          editorOptions: {
            showEditIcon: view
          },
          formatter: numberFormatterDefaultZero,
          headerCssClass: 'aln-rt b'
        },
        {
          id: 'maxThreshold',
          name: 'Max Threshold',
          field: 'maxThreshold',
          toolTip: 'Max Threshold',
          type: 'text',
          isEditable: view,
          sortable: false,
          editor: dpGrid.Editors.Text,
          editorOptions: {
            showEditIcon: view
          },
          formatter: numberFormatterDefaultZero,
          headerCssClass: 'aln-rt b'
        }
      ];
    case 'CUTOFF TIME':
      return [
        {
          id: 'ticker',
          name: 'Ticker',
          field: 'ticker',
          toolTip: 'Ticker',
          type: 'text',
          isEditable: view,
          sortable: false,
          formatter: textFormatter,
          headerCssClass: 'aln-rt b'
        },
        {
          id: 'businessDayMMFThresholdTimeString',
          name: 'Business Day Threshold Time',
          field: 'businessDayMMFThresholdTimeString',
          toolTip: 'Business Day Threshold Time',
          type: 'text',
          isEditable: view,
          sortable: false,
          editor: TimeEditor,
          editorOptions: {
            showEditIcon: view
          },
          formatter: textFormatter,
          headerCssClass: 'aln-rt b'
        },
        {
          id: 'holidayMMFThresholdTimeString',
          name: 'Holiday Threshold Time',
          field: 'holidayMMFThresholdTimeString',
          toolTip: 'Holiday Threshold Time',
          type: 'text',
          isEditable: view,
          sortable: false,
          editor: TimeEditor,
          formatter: textFormatter,
          editorOptions: {
            showEditIcon: view
          },
          headerCssClass: 'aln-rt b'
        }
      ];
  }
}
