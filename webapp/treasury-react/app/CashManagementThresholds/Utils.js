import { ENTITYLEVEL, FUNDLEVEL, AMBER, RED, GREEN } from '../commons/constants';
import { getHHmmTime } from '../commons/util';

const INITIAL_THRESHOLD_DATA = {
  cashManagementEntityThresholdList: [],
  isUserAuthorizedForEdit: false,
  ragThresholds: {
    entityLevelAmberMinThreshold: 0,
    entityLevelRedMinThreshold: 0,
    fundLevelAmberMinThreshold: 0,
    fundLevelRedMinThreshold: 0
  },
  mmfCutoffTimeList: []
};

export function getCurrentDate() {
  let dt = new Date();
  let date =
    '' +
    dt.getFullYear() +
    (dt.getMonth() < 9 ? '0' + (dt.getMonth() + 1) : dt.getMonth() + 1) +
    (dt.getDate() < 10 ? '0' + dt.getDate() : dt.getDate());
  return date;
}
export function prepareMMFCutoffTimeData(mmfCutoffTimeList) {
  if (!mmfCutoffTimeList) {
    return [];
  }
  mmfCutoffTimeList.forEach((mmfCutoffTime) => {
    mmfCutoffTime.holidayMMFThresholdTimeString = getHHmmTime(mmfCutoffTime.holidayMMFThresholdTime);
    mmfCutoffTime.businessDayMMFThresholdTimeString = getHHmmTime(mmfCutoffTime.businessDayMMFThresholdTime);
    mmfCutoffTime.id = mmfCutoffTime.spn;
  });
  return mmfCutoffTimeList;
}

export function flattenThresholdsData(data) {
  if (!data) {
    return INITIAL_THRESHOLD_DATA;
  }

  const { cashManagementEntityThresholdList } = data;
  const { cashManagementRAGThresholdList } = data;

  cashManagementEntityThresholdList.forEach((element) => {
    element.id = element.fundId;
  });

  let rag = getReducedRagThresholds(cashManagementRAGThresholdList);

  let flattendata = {
    cashManagementEntityThresholdList: cashManagementEntityThresholdList,
    isUserAuthorizedForEdit: data.isUserAuthorizedForEdit,
    ragThresholds: rag
  };
  return flattendata;
}

function getReducedRagThresholds(cmRAGList) {
  let rag = {
    entityLevelAmberMinThreshold: 0,
    entityLevelRedMinThreshold: 0,
    fundLevelAmberMinThreshold: 0,
    fundLevelRedMinThreshold: 0
  };

  for (let i = 0; i < cmRAGList.length; i++) {
    let cashManagementRagThreshold = cmRAGList[i];
    if (cashManagementRagThreshold.ownershipLevel == ENTITYLEVEL) {
      if (cashManagementRagThreshold.ragStatus == AMBER)
        rag.entityLevelAmberMinThreshold = cashManagementRagThreshold.minThreshold;
      else if (cashManagementRagThreshold.ragStatus == RED)
        rag.entityLevelRedMinThreshold = cashManagementRagThreshold.minThreshold;
    } else {
      if (cashManagementRagThreshold.ragStatus == AMBER)
        rag.fundLevelAmberMinThreshold = cashManagementRagThreshold.minThreshold;
      else if (cashManagementRagThreshold.ragStatus == RED)
        rag.fundLevelRedMinThreshold = cashManagementRagThreshold.minThreshold;
    }
  }
  return rag;
}

export function createCashManagementThresholds(ragThresholdList, entityThresholdList) {
  let cashManagementRAGThresholdList = getRagThresholdList(ragThresholdList);
  let cashManagementEntityThresholdList = getEntityThreholdList(entityThresholdList);

  return {
    cashManagementRAGThresholdList: cashManagementRAGThresholdList,
    cashManagementEntityThresholdList: cashManagementEntityThresholdList,
    '@CLASS': 'com.arcesium.treasury.lcm.common.cashmanagement.CashManagementThreshold'
  };
}

function getRagThresholdList(ragThresholdList) {
  let cmRAGThresholdList = [];
  let RAGStatus = [GREEN, AMBER, RED];

  for (let i = 0; i < 3; i++) {
    cmRAGThresholdList.push({
      ownershipLevel: ENTITYLEVEL,
      ragStatus: RAGStatus[i],
      minThreshold: 0,
      maxThreshold: 0
    });
    cmRAGThresholdList.push({
      ownershipLevel: FUNDLEVEL,
      ragStatus: RAGStatus[i],
      minThreshold: 0,
      maxThreshold: 0
    });
  }
  cmRAGThresholdList = assignThresholdValues(cmRAGThresholdList, ragThresholdList);
  return cmRAGThresholdList;
}

function getEntityThreholdList(entityThresholdList) {
  let cashManagementEntityThresholdList = [];

  for (let i = 0; i < entityThresholdList.length; i++) {
    let cmEntityThreshold = JSON.parse(JSON.stringify(entityThresholdList[i]));
    cmEntityThreshold['@CLASS'] = 'com.arcesium.treasury.lcm.common.cashmanagement.CashManagementEntityThreshold';
    cmEntityThreshold.minThreshold = Number(cmEntityThreshold.minThreshold);
    cmEntityThreshold.maxThreshold = Number(cmEntityThreshold.maxThreshold);
    cashManagementEntityThresholdList.push(cmEntityThreshold);
  }
  return cashManagementEntityThresholdList;
}
export const getMmfCutoffTimeList = (mmfCutoffTimeList) => {
  mmfCutoffTimeList.forEach((element) => {
    element['@CLASS'] = 'com.arcesium.treasury.lcm.common.cashmanagement.MMFCutoffTime';
    element.holidayMMFThresholdTime = convertToMinutes(element.holidayMMFThresholdTimeString);
    element.businessDayMMFThresholdTime = convertToMinutes(element.businessDayMMFThresholdTimeString);
  });
  return mmfCutoffTimeList;
};

const convertToMinutes = (HHmmString) => {
  let hhmmParts = HHmmString.split(':');
  return parseInt(hhmmParts[0], 10) * 60 + parseInt(hhmmParts[1], 10);
};

function assignThresholdValues(cmRAGThresholdList, ragThresholdList) {
  for (let i = 0; i < cmRAGThresholdList.length; i++) {
    let cmRagThreshold = cmRAGThresholdList[i];
    if (cmRagThreshold.ownershipLevel == ENTITYLEVEL) {
      if (cmRagThreshold.ragStatus == RED) {
        cmRagThreshold.minThreshold = ragThresholdList.entityLevelRedMinThreshold;
        cmRagThreshold.maxThreshold = 100;
      } else if (cmRagThreshold.ragStatus == AMBER) {
        cmRagThreshold.minThreshold = ragThresholdList.entityLevelAmberMinThreshold;
        cmRagThreshold.maxThreshold = ragThresholdList.entityLevelRedMinThreshold;
      } else if (cmRagThreshold.ragStatus == GREEN) {
        cmRagThreshold.minThreshold = 0;
        cmRagThreshold.maxThreshold = ragThresholdList.entityLevelAmberMinThreshold;
      }
    } else {
      if (cmRagThreshold.ragStatus == RED) {
        cmRagThreshold.minThreshold = ragThresholdList.fundLevelRedMinThreshold;
        cmRagThreshold.maxThreshold = 100;
      } else if (cmRagThreshold.ragStatus == AMBER) {
        cmRagThreshold.minThreshold = ragThresholdList.fundLevelAmberMinThreshold;
        cmRagThreshold.maxThreshold = ragThresholdList.fundLevelRedMinThreshold;
      } else if (cmRagThreshold.ragStatus == GREEN) {
        cmRagThreshold.minThreshold = 0;
        cmRagThreshold.maxThreshold = ragThresholdList.fundLevelAmberMinThreshold;
      }
    }
    cmRagThreshold['@CLASS'] = 'com.arcesium.treasury.lcm.common.cashmanagement.CashManagementRAGThreshold';
    cmRAGThresholdList[i] = cmRagThreshold;
  }
  return cmRAGThresholdList;
}

export const validateEntityThresholds = (cashManagementEntityThresholdList) => {
  for (let i = 0; i < cashManagementEntityThresholdList.length; i++) {
    let cmEntityThreshold = cashManagementEntityThresholdList[i];
    if (Number.isNaN(Number(cmEntityThreshold.minThreshold)) || Number.isNaN(Number(cmEntityThreshold.maxThreshold))) {
      return false;
    }
    if (Number(cmEntityThreshold.maxThreshold) < Number(cmEntityThreshold.minThreshold)) {
      return false;
    }
  }
  return true;
};

export const validateCutoffTime = (mmfCutoffTimeList) => {
  const timeRegEx = /^([01]\d|2[0-3]):([0-5]\d)$/;
  for (let i = 0; i < mmfCutoffTimeList.length; i++) {
    let mmfCutoffTime = mmfCutoffTimeList[i];
    let holidayMMFThresholdTime = mmfCutoffTime.holidayMMFThresholdTimeString;
    let businessDayMMFThresholdTime = mmfCutoffTime.businessDayMMFThresholdTimeString;
    if (holidayMMFThresholdTime.search(timeRegEx) === -1 || businessDayMMFThresholdTime.search(timeRegEx) === -1) {
      return false;
    }
  }
  return true;
};

export function TimeEditor(args) {
  var $input;
  var defaultValue;
  var scope = this;

  this.init = function() {
    $input = $("<INPUT type=time style='width: 100px;' class='editor-text' />")
      .appendTo(args.container)
      .bind('keydown.nav', function(e) {
        if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
          e.stopImmediatePropagation();
        }
      })
      .focus()
      .select();
  };

  this.destroy = function() {
    $input.remove();
  };

  this.focus = function() {
    $input.focus();
  };

  this.getValue = function() {
    return $input.val();
  };

  this.setValue = function(val) {
    $input.val(val);
  };

  this.loadValue = function(item) {
    defaultValue = item[args.column.field] || '';
    $input.val(defaultValue);
    $input[0].defaultValue = defaultValue;
    $input.select();
  };

  this.serializeValue = function() {
    return $input.val();
  };

  this.applyValue = function(item, state) {
    item[args.column.field] = state;
  };

  this.isValueChanged = function() {
    return !($input.val() == '' && defaultValue == null) && $input.val() != defaultValue;
  };

  this.validate = function() {
    return editorCustomValidator(args.column, $input.val());
  };

  this.init();
}
function editorCustomValidator(column, val) {
  if (column.editorOptions && column.editorOptions.validator) {
    var validationResults = column.editorOptions.validator(val);
    if (!validationResults.valid) {
      return validationResults;
    }
  }
  return {
    valid: true,
    msg: null
  };
}
