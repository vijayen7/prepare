import _ from "lodash";
import { convertJavaDate } from "commons/util";
import { COLORS } from "arc-commons";
import React from "react";

export const dashboards = [
  { key: "shortBalance", value: "Short Balances" },
  { key: "htbBalance", value: "HTB Balances" },
  { key: "gcBalance", value: "GC Balances" },
  { key: "shortSpend", value: "Short Spend" },
  { key: "htbSpend", value: "HTB Spend" },
  { key: "gcSpend", value: "GC Spend" },
  { key: "percentHtb", value: "% HTB" },
  { key: "skew", value: "Skew" },
  { key: "gmv", value: "GMV" }
];


var nest = function (seq, keys) {
  if (!keys.length)
    return seq;
  var first = keys[0];
  var rest = keys.slice(1);
  return _.mapValues(_.groupBy(seq, first), function (value) {
    return nest(value, rest)
  });
};

export function formatChartData(datasetArray, scenarioTypes) {
  let dataset = _.cloneDeep(datasetArray);
  let scenarioMappedData = nest(dataset, ["scenarioType", "date"]);
  let formattedData = {};
  let xAxisTicks = [];
  let scenarios = _.map(scenarioTypes, scenarioType => scenarioType.id);
  _.forEach(scenarios, scenario => (formattedData[scenario] = {}));
  _.forEach(scenarios, scenario => {
    _.forEach(dashboards, dashboard => (formattedData[scenario][dashboard.key] = {}));
  });

  _.forEach(Object.keys(scenarioMappedData), (scenarioType) => {
    let mappedData = scenarioMappedData[scenarioType];
    _.forEach(Object.keys(mappedData), (key, idx, arr) => {
      _.forEach(
        dashboards,
        dashboard =>
          (formattedData[scenarioType][dashboard.key][key] = {
            date: convertJavaDate(key),
            ...(isLastWorkingDayOfMonth(key)
              ? { activeDot: true }
              : { activeDot: false })
          })
      );
      if (isLastWorkingDayOfMonth(key)) {
        xAxisTicks.push(convertJavaDate(key));
      }
    });

    Object.keys(mappedData).forEach(key =>
      mappedData[key].forEach(obj => {
        formattedData[scenarioType]["shortBalance"][key][obj["cpeFamilyName"]] =
          obj["shortBalance"];
        formattedData[scenarioType]["gcBalance"][key][obj["cpeFamilyName"]] = obj["gcBalance"];
        formattedData[scenarioType]["htbBalance"][key][obj["cpeFamilyName"]] =
          obj["htbBalance"];
        formattedData[scenarioType]["shortSpend"][key][obj["cpeFamilyName"]] =
          obj["shortSpend"];
        formattedData[scenarioType]["gcSpend"][key][obj["cpeFamilyName"]] = obj["gcSpend"];
        formattedData[scenarioType]["htbSpend"][key][obj["cpeFamilyName"]] = obj["htbSpend"];
        if (obj["shortBalance"] != 0) {
          formattedData[scenarioType]["percentHtb"][key][obj["cpeFamilyName"]] =
            (obj["htbBalance"] / obj["shortBalance"]).toFixed(5) * 100;
        }
        if ((obj["longBalance"] + obj["shortBalance"]) != 0) {
          formattedData[scenarioType]["skew"][key][obj["cpeFamilyName"]] =
            ((obj["longBalance"] - obj["shortBalance"]) / (obj["longBalance"] + obj["shortBalance"])).toFixed(5) * 100;
        }
        formattedData[scenarioType]["gmv"][key][obj["cpeFamilyName"]] =
          obj["shortBalance"] + obj["longBalance"];
      })
    );

  })

  let lines = [];
  let cpeFamilyNames = new Set();
  _.forEach(Object.keys(scenarioMappedData), (scenarioType) => {
    let mappedData = scenarioMappedData[scenarioType];
    _.forEach(Object.keys(mappedData), obj => {
      _.forEach(mappedData[obj], data => {
        cpeFamilyNames.add(data.cpeFamilyName);
      })
    });
  })

  return { lines: Array.from(cpeFamilyNames), data: formattedData, xAxisTicks: xAxisTicks };
}

export function formatGridData(dataset) {
  let counter = 1;
  dataset.map(data => {
    data["id"] = counter++;
  });

  return dataset;
}

export function getColors(index) {
  const light = COLORS.light;
  const dark = COLORS.dark;
  const colors = [
    dark["blue"],
    dark["green"],
    dark["orange"],
    dark["red"],
    dark["yellow"],
    dark["teal4"],
    dark["pink5"],
    dark["purple4"],
    dark["indigo2"],
    dark["lime4"],
    light["blue2"],
    dark["black"],
    dark["gray5"],
    dark["teal5"],
    light["red5"],
    light["teal1"],
    light["yellow1"],
    light["orange1"]
  ];
  return colors[index % colors.length];
}

function isLastWorkingDayOfMonth(serialisedDate) {
  let res = serialisedDate.match(/\((.*)\)/);
  let deserializedDate = parseInt(res[1]);
  let formattedDate = new Date(deserializedDate);
  let year = formattedDate.getFullYear();
  var month = formattedDate.getMonth() + 1;
  if (12 === month) {
    month = 0;
    year++;
  }

  var offset = 0;
  var result = null;

  do {
    result = new Date(year, month, offset);

    offset--;
  } while (0 === result.getDay() || 6 === result.getDay());

  return formattedDate.setHours(0, 0, 0, 0) === result.setHours(0, 0, 0, 0);
}

export function getPreviousBusinessDay() {
  const startDate = new Date();
  startDate.setDate(startDate.getDate() - 1);
  if (startDate.getDay() === 6) {
    // Saturday, move to Friday
    startDate.setDate(startDate.getDate() - 1);
  } else if (startDate.getDay() === 0) {
    // Sunday, move 2 days back to Friday
    startDate.setDate(startDate.getDate() - 2);
  }
  let endDate = new Date();
  endDate.setDate(startDate.getDate() - 365);
  return [
    startDate.toISOString().substring(0, 10),
    endDate.toISOString().substring(0, 10)
  ];
}

export function getPreviousMonthDay() {
  const startDate = new Date();
  startDate.setDate(startDate.getDate() - 1);
  if (startDate.getDay() === 6) {
    // Saturday, move to Friday
    startDate.setDate(startDate.getDate() - 1);
  } else if (startDate.getDay() === 0) {
    // Sunday, move 2 days back to Friday
    startDate.setDate(startDate.getDate() - 2);
  }
  let endDate = new Date();
  endDate.setDate(startDate.getDate() - 30);
  return [
    startDate.toISOString().substring(0, 10),
    endDate.toISOString().substring(0, 10)
  ];
}

function formatDataByCounterparty(datasetArray, scenarioTypes) {
  let dataset = _.cloneDeep(datasetArray);
  let scenarioMappedData = nest(dataset, ["scenarioType", "date"]);
  let formattedData = {};
  let xAxisTicks = [];
  let scenarios = _.map(scenarioTypes, scenarioType => scenarioType.id);
  _.forEach(scenarios, scenario => (formattedData[scenario] = {}));
  if (
    Object.keys(scenarioMappedData) !== undefined &&
    Object.keys(scenarioMappedData).length > 0
  ) {
    _.forEach(Object.keys(scenarioMappedData), (scenarioType) => {
      let mappedData = scenarioMappedData[scenarioType];
      let cpeFamilyNames = new Set();
      _.forEach(Object.keys(mappedData), obj => {
        _.forEach(mappedData[obj], data => {
          cpeFamilyNames.add(data.cpeFamilyName);
        })
      });

      cpeFamilyNames.forEach(cpeFamilyName => {
        (formattedData[scenarioType][cpeFamilyName] = {})
      });



      _.forEach(Object.keys(mappedData), (key, idx, arr) => {
        _.forEach(Object.keys(formattedData[scenarioType]), cpeFamily => {
          formattedData[scenarioType][cpeFamily][key] = {
            date: convertJavaDate(key),
            ...(isLastWorkingDayOfMonth(key)
              ? { activeDot: true }
              : { activeDot: false })
          };
        });
        if (isLastWorkingDayOfMonth(key)) {
          xAxisTicks.push(convertJavaDate(key));
        }
      });

      Object.keys(mappedData).forEach(key =>
        mappedData[key].forEach(obj => {
          formattedData[scenarioType][obj["cpeFamilyName"]][key]["Short Balances"] =
            obj["shortBalance"];
          formattedData[scenarioType][obj["cpeFamilyName"]][key]["GC Balances"] = obj["gcBalance"];
          formattedData[scenarioType][obj["cpeFamilyName"]][key]["HTB Balances"] =
            obj["htbBalance"];
          formattedData[scenarioType][obj["cpeFamilyName"]][key]["Short Spend"] =
            obj["shortSpend"];
          formattedData[scenarioType][obj["cpeFamilyName"]][key]["GC Spend"] = obj["gcSpend"];
          formattedData[scenarioType][obj["cpeFamilyName"]][key]["HTB Spend"] = obj["htbSpend"];
          if (obj["shortBalance"] != 0) {
            formattedData[scenarioType][obj["cpeFamilyName"]][key]["% HTB"] =
              (obj["htbBalance"] / obj["shortBalance"]).toFixed(5) * 100;

          }
          if ((obj["longBalance"] + obj["shortBalance"]) != 0) {
            formattedData[scenarioType][obj["cpeFamilyName"]][key]["Skew"] =
              ((obj["longBalance"] - obj["shortBalance"]) / (obj["longBalance"] + obj["shortBalance"])).toFixed(5) * 100;
          }
          formattedData[scenarioType][obj["cpeFamilyName"]][key]["GMV"] =
            obj["shortBalance"] + obj["longBalance"];
        })
      );
    })
  }


  let lines = _.map(dashboards, dashboard => dashboard.value);

  return { lines: lines, data: formattedData, xAxisTicks: xAxisTicks };
}

export function formatData(dataset, scenarioTypes) {
  let dataByDashboard = formatChartData(dataset, scenarioTypes);
  let dataByCpeFamily = formatDataByCounterparty(dataset, scenarioTypes);
  let lineColorMapByDashboard = {};
  let lineColorMapByCpeFamily = {};
  dataByDashboard.lines.forEach((line, index) => {
    lineColorMapByDashboard[line] = getColors(index);
  });
  dataByCpeFamily.lines.forEach((line, index) => {
    lineColorMapByCpeFamily[line] = getColors(index);
  });

  return {
    dataByDashboard: {
      data: dataByDashboard,
      lineColorMap: lineColorMapByDashboard
    },
    dataByCpeFamily: {
      data: dataByCpeFamily,
      lineColorMap: lineColorMapByCpeFamily
    }
  };
}

export function checkForPercentFormatter(dashboard, name) {
  return dashboard === "skew" || dashboard === "percentHtb" || name === "% HTB" || name == "Skew";
}

export function checkForMillionFormatter(dashboard, name) {
  return dashboard === "shortBalance" || dashboard === "htbBalance" || dashboard === "gcBalance" || dashboard === "gmv"
    || name === "Short Balances" || name == "HTB Balances" || name == "GC Balances" || name == "GMV";
}

export function formatNumberAsMoney(value) {
  value = Number(value);
  if (Number.isNaN(value)) {
    return "";
  }
  const prefix = "$";
  const conversionFactors = [
    { conversionFactor: 1000000000, suffix: "B" },
    { conversionFactor: 1000000, suffix: "M" },
    { conversionFactor: 1000, suffix: "K" },
    { conversionFactor: 1, suffix: "" }
  ];

  for (let index = 0; index < conversionFactors.length; index++) {
    const { conversionFactor, suffix } = conversionFactors[index];
    const convertedFigure = (value / conversionFactor).toFixed(1);
    if (Math.abs(convertedFigure) >= 1) {
      const sign = convertedFigure < 0 ? "-" : "";
      return `${sign}${prefix}${convertedFigure}${suffix}`;
    }
  }

  return `${prefix}0`;
}

export function millionFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  columnValue
) {
  if (row === null) {
    if (columnValue !== undefined) {
      return dpGrid.Formatters.Float(
        row,
        cell,
        Number(columnValue / 1000000).toFixed(4),
        columnDef,
        dataContext
      );
    } else {
      return dpGrid.Formatters.Float(
        row,
        cell,
        Number(value / 1000000).toFixed(4),
        columnDef,
        dataContext
      );
    }
  }
  if (value == null) {
    return dpGrid.Formatters.Float(row, cell, value, columnDef, dataContext);
  }
  return dpGrid.Formatters.Float(
    row,
    cell,
    Number(value / 1000000).toFixed(4),
    columnDef,
    dataContext
  );
}

export function thousandFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  columnValue
) {
  if (row === null) {
    if (columnValue !== undefined) {
      return dpGrid.Formatters.Float(
        row,
        cell,
        Number(columnValue / 1000).toFixed(4),
        columnDef,
        dataContext
      );
    } else {
      return dpGrid.Formatters.Float(
        row,
        cell,
        Number(value / 1000).toFixed(4),
        columnDef,
        dataContext
      );
    }
  }
  if (value == null) {
    return dpGrid.Formatters.Float(row, cell, value, columnDef, dataContext);
  }
  return dpGrid.Formatters.Float(
    row,
    cell,
    Number(value / 1000).toFixed(4),
    columnDef,
    dataContext
  );
}

export function percentFormatterDefaultZero(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  return (
    '<div class="text-align--right">' +
    (value == null || value === ""
      ? Number(0).toFixed(3)
      : Number(value).toFixed(3)) +
    "%</div>"
  );
}

export function getFormatContent(dashboard) {
  if (dashboard === "shortBalance" || dashboard === "htbBalance" ||
    dashboard === "gcBalance" || dashboard === "Short Balances" || dashboard === "HTB Balances" || dashboard === "GC Balances"
    || dashboard === "gmv" || dashboard === "GMV") {
    return <p>* Values are in $mn</p>
  }
  else if (dashboard === "shortSpend" || dashboard === "htbSpend" ||
    dashboard === "gcSpend" || dashboard === "Short Spend" || dashboard === "HTB Spend" || dashboard === "GC Spend") {
    return <p>* Values are in $K</p>
  }
  else if (dashboard === "percentHtb" || dashboard === "skew" ||
    dashboard === "% HTB" || dashboard === "Skew Spend") {
    return <p>* Values are in %</p>
  }
  else {
    return null
  }

}
