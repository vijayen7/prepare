import React, { Component } from "react";
import ReactComboBoxFilter from "commons/components/ReactComboBoxFilter";
import { dashboards } from "../util";
export default class DashboardFilter extends Component {
  render() {
    return (
      <ReactComboBoxFilter
        data={dashboards}
        onSelect={this.props.onSelect}
        selectedData={this.props.selectedData}
        stateKey="selectedDashboard"
        label="Dashboards"
        style="margin"
        showTokens={this.props.showTokens}
        maxTokenHeight={this.props.maxTokenHeight}
      />
    );
  }
}
