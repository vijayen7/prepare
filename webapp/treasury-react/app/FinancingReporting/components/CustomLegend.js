import React from "react";
const CustomLegend = props => {
  let res = [];
  Object.entries(props.data).forEach(([key, value]) => {
    res.push({ counterparty: key, color: value });
  });
  return (
    <div className="padding">
      <br />
      <div className="legend">
        {res.map(obj => {
          return (
            <div
              className="legend__item"
              style={{
                opacity:
                  props.disabled === undefined ||
                    !props.disabled[obj.counterparty]
                    ? 1
                    : 0.6
              }}
              onClick={() => {
                props.toggleLegend(obj.counterparty);
              }}
            >
              <div
                className="legend__item__swatch"
                style={{ background: obj.color }}
              />
              <span className="margin--left--small margin--right--double">
                {obj.counterparty}
              </span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CustomLegend;
