import React, { Component } from "react";
import { Dialog } from "arc-react-components";

import { Panel, Layout, ToggleGroup } from "arc-react-components";
import Button from "commons/components/Button";
import Label from "commons/components/Label";
import { ReactArcGrid } from "arc-grid";
import { getOptions } from "../grid/gridOptions";
import { getColumns } from "../grid/columnConfig";
import Chart from "./Chart";
import { formatGridData, getFormatContent } from "../util";

export default class DialogContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showChart: true,
      gcRate: 1
    };
  }

  render() {
    return (
      <Dialog
        isOpen={this.props.open}
        title=""
        onClose={() => {
          this.setState({ showChart: true, gcRate: 1 });
          this.props.toggleDialog({ key: "", value: "" });
        }}
        style={{
          width: "100%",
          height: "100%"
        }}
      >
        <Layout>
          <Layout.Child size="Fit">
            <Panel>
              <Label label={this.props.label} />
              <Button
                tertiary={true}
                label={
                  this.state.showChart
                    ? "Switch to Grid View"
                    : "Switch to Chart View"
                }
                onClick={() => {
                  this.setState({ showChart: !this.state.showChart });
                }}
              />
            </Panel>
          </Layout.Child>

          <Layout.Child>
            GC Rates
            <ToggleGroup
              className="padding--small"
              value={this.state.gcRate}
              onChange={(value) => {
                this.setState({ gcRate: value })
              }}
            >
              {this.props.scenarioTypes.map(scenarioType => {
                return (<ToggleGroup.Button key={scenarioType.id} data={scenarioType.id}>
                  {scenarioType.description}
                </ToggleGroup.Button>)
              })}
            </ToggleGroup>
            {this.state.showChart && (
              <React.Fragment>
                <Chart
                  data={this.props.data}
                  chartHeight={600}
                  dashboard={this.props.lines[0]}
                  dialogueView={true}
                  lineColorMap={this.props.lineColorMap}
                  scenarioType={this.state.gcRate}
                />
                <p>* Click and drag a rectangle for a zoomed in view</p>
              </React.Fragment>
            )}
            {!this.state.showChart && (
              <React.Fragment>
                {getFormatContent(this.props.lines[0])}
                <ReactArcGrid
                  gridId="grid-container"
                  data={formatGridData(
                    Object.values(this.props.data["data"][this.state.gcRate][this.props["lines"][0]])
                  )}
                  columns={getColumns(
                    this.props.data["lines"],
                    this.props.lines[0]
                  )}
                  options={getOptions(this.props.lines[0])}
                />
              </React.Fragment>
            )}
          </Layout.Child>
        </Layout>
      </Dialog >
    );
  }
}
