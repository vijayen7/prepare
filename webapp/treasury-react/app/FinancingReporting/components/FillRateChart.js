import React, { PureComponent } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import { formatAsPercent, convertJavaDate } from "commons/util";
import _ from "lodash";


export default class FillRateChart extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    let fillRateData = []
    if (this.props.fillRateData.length > 0)
      fillRateData = _.sortBy(this.props.fillRateData, data => parseInt(data["date"].match(/\((.*)\)/)[1]))
    return (
      <React.Fragment>
        <ResponsiveContainer height={500} width="100%">
          <LineChart
            data={fillRateData}
            margin={{ top: 30, right: 20, left: 5, bottom: 5 }}
          >
            <CartesianGrid
              strokeDasharray="3 3"
              strokeOpacity="0.4"
              stroke="#8e8d84"
            />
            <XAxis
              dataKey="date"
              dx={-20}
              dy={8}
              angle={-20}
              height={50}
              tick={true}
              tickFormatter={convertJavaDate}
            />
            <YAxis
              yAxisId="percent"
              type="number"
              stroke="#85BDBF"
              label={{
                angle: -90,
                fill: "#b9b9b9",
                dx: -45
              }}
              tickFormatter={formatAsPercent}
              domain={["auto", "auto"]}
            />
            <Tooltip
              isAnimationActive={false}
              formatter={(value, name, props) => {
                return `${formatAsPercent(value)}`;
              }}
              labelFormatter={(value, name, props) => {
                return `${convertJavaDate(value)}`;
              }}
            />
            <Line
              yAxisId="percent"
              type="monotone"
              dataKey="fillRatePercent"
              strokeWidth={2}
              dot={false}
              animationDuration={500}
            />

          </LineChart>
        </ResponsiveContainer>
      </React.Fragment>)
  }
}
