import React, { PureComponent } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  Legend,
  ReferenceArea
} from "recharts";
import { Loading } from "arc-react-components";
import { formatAsPercent } from "commons/util";
import { checkForPercentFormatter, formatNumberAsMoney } from "../util";
import FilterButton from "commons/components/FilterButton";
import CustomLegend from "./CustomLegend";

export default class Chart extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      disabled: {},
      refAreaLeft: '',
      refAreaRight: '',
      leftIndex: -1,
      rightIndex: -1,
      leftDataIndex: 0,
      rightDataIndex: 0
    };
  }

  zoom = () => {
    let { refAreaLeft, refAreaRight, leftIndex, rightIndex, leftDataIndex, rightDataIndex } = this.state;

    if (refAreaLeft === refAreaRight || refAreaRight === '') {
      this.setState(() => ({
        refAreaLeft: '',
        refAreaRight: '',
        leftIndex: -1,
        rightIndex: -1,
        leftDataIndex: 0,
        rightDataIndex: 0
      }));
      return;
    }

    if (refAreaLeft > refAreaRight) {
      [refAreaLeft, refAreaRight] = [refAreaRight, refAreaLeft]
    }
    if (leftIndex > rightIndex) {
      [leftIndex, rightIndex] = [rightIndex, leftIndex]
    }
    [leftDataIndex, rightDataIndex] = [leftDataIndex + leftIndex, leftDataIndex + rightIndex];
    this.setState(() => ({
      refAreaLeft: '',
      refAreaRight: '',
      leftIndex: -1,
      rightIndex: -1,
      leftDataIndex,
      rightDataIndex
    }));
  }

  zoomOut = () => {
    this.setState(() => ({
      refAreaLeft: '',
      refAreaRight: '',
      leftIndex: -1,
      rightIndex: -1,
      leftDataIndex: 0,
      rightDataIndex: 0
    }));
  }

  getFormatter = (dashboard, name) => {
    if (checkForPercentFormatter(dashboard, name)) {
      return formatAsPercent;
    } else {
      return formatNumberAsMoney;
    }
  };

  toggleLegend = line => {
    let disabled = Object.assign({}, this.state.disabled);
    if (disabled[line] == undefined) {
      disabled[line] = true;
    } else {
      disabled[line] = !disabled[line];
    }
    this.setState({ disabled });
  };

  filterLines = lines => {
    if (this.props.dialogueView)
      return lines.filter(
        line =>
          this.state.disabled[line] === undefined || !this.state.disabled[line]
      );
    else {
      return lines.filter(
        line =>
          this.props.disabled[line] === undefined || !this.props.disabled[line]
      );
    }
  };

  getMargin = dialogueView => {
    if (dialogueView) return { top: 25, right: 80, left: 35, bottom: 5 };
    else return { top: 10, right: 20, left: 5, bottom: 5 };
  };

  render() {
    let { lines, data } = this.props.data;
    if (lines == undefined || data == undefined) {
      return null;
    }
    let dashboard = this.props.dashboard;
    lines = this.filterLines(lines);
    data = data[this.props.scenarioType];
    return data[dashboard] !== undefined ? (
      <React.Fragment>
        {this.props.dialogueView && <FilterButton label="Zoom out" onClick={this.zoomOut} />}
        <ResponsiveContainer height={this.props.chartHeight} width={"100%"}>
          <LineChart
            data={!this.props.dialogueView ||
              (this.state.leftDataIndex == 0 && this.state.rightDataIndex == 0) ?
              Object.values(data[dashboard]) : Object.values(data[dashboard]).slice(this.state.leftDataIndex, this.state.rightDataIndex + 1)}
            margin={this.getMargin(this.props.dialogueView)}
            onMouseDown={(e) => {
              this.props.dialogueView && this.setState({ refAreaLeft: e.activeLabel, leftIndex: e.activeTooltipIndex })
            }}
            onMouseMove={(e) => {
              this.props.dialogueView && this.state.refAreaLeft && this.setState({ refAreaRight: e.activeLabel, rightIndex: e.activeTooltipIndex })
            }}
            onMouseUp={this.props.dialogueView && this.zoom}
          >
            <CartesianGrid
              strokeDasharray="3 3"
              strokeOpacity="0.4"
              stroke="#8e8d84"
            />
            <XAxis
              dataKey="date"
              dx={-20}
              dy={8}
              angle={-20}
              height={50}
              tick={true}
            />
            <YAxis
              yAxisId="number"
              type="number"
              orientation="left"
              stroke="#85BDBF"
              label={{
                angle: -90,
                fill: "#b9b9b9",
                dx: -45
              }}
              tickFormatter={this.getFormatter(dashboard)}
              domain={["auto", "auto"]}
            />
            {!this.props.pivotByDashboard && <YAxis
              yAxisId="percent"
              type="number"
              orientation="right"
              stroke="#85BDBF"
              label={{
                angle: -90,
                fill: "#b9b9b9",
                dx: -45
              }}
              tickFormatter={formatAsPercent}
              domain={["auto", "auto"]}
            />}

            {this.props.dialogueView && (
              <Tooltip
                isAnimationActive={false}
                formatter={(value, name, props) => {
                  return `${this.getFormatter(dashboard, name)(value)}`;
                }}
              />
            )}
            {this.props.dialogueView && (
              <Legend
                content={
                  <CustomLegend
                    data={this.props.lineColorMap}
                    toggleLegend={this.toggleLegend}
                    disabled={this.state.disabled}
                  />
                }
              />
            )}
            {lines.map((dataPoint, index) => (
              dataPoint == "% HTB" || dataPoint == "Skew" ?
                <Line
                  yAxisId="percent"
                  type="monotone"
                  dataKey={dataPoint}
                  strokeDasharray="5 5"
                  stroke={this.props.lineColorMap[dataPoint]}
                  strokeWidth={2}
                  dot={false}
                  animationDuration={500}
                /> :
                <Line
                  yAxisId="number"
                  type="monotone"
                  dataKey={dataPoint}
                  stroke={this.props.lineColorMap[dataPoint]}
                  strokeWidth={2}
                  dot={false}
                  animationDuration={500}
                />
            ))}
            {
              (this.state.refAreaLeft && this.state.refAreaRight) ? (
                <ReferenceArea yAxisId="number" x1={this.state.refAreaLeft} x2={this.state.refAreaRight} strokeOpacity={0.3} />) : null

            }
          </LineChart>
        </ResponsiveContainer>
      </React.Fragment>
    ) : (
        <Loading isLoading={true} pastDelay={true} />
      );
  }
}
