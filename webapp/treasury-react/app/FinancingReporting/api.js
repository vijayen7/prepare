import { BASE_URL } from "commons/constants";
const queryString = require("query-string");
export let url = "";

export function getData(payload) {
  const paramString = queryString.stringify(payload, { sort: false });
  url = `${BASE_URL}service/counterpartyReportingService/getCounterpartyReportingData?${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function getLocateEfficiencyData(payload) {
  const paramString = queryString.stringify(payload, { sort: false });
  url = `${BASE_URL}service/locateEfficiencyService/getLocateEfficiencyData?${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function getScenarioTypes(date) {
  url = `${BASE_URL}service/counterpartyReportingService/getScenarioTypes?date=${date}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function getLocateEfficiencyDetailData(payload) {
  const paramString = queryString.stringify(payload, { sort: false });
  url = `${BASE_URL}service/locateEfficiencyService/getDetailLocateData?${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function getFillRateTrendData(payload) {
  console.log(payload);
  const paramString = queryString.stringify(payload, { sort: false });
  url = `${BASE_URL}service/locateEfficiencyService/getFillRateTrend?${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}


