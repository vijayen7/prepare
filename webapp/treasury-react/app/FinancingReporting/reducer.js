import {
  LOAD_SELECTED_DASHBOARDS,
  LOAD_DATA,
  DESTROY_DATA,
  LOAD_LOCATE_EFFICIENCY_DATA,
  FETCH_LOCATE_DETAIL_DATA,
  FETCH_FILL_RATE_TREND_DATA,
  SELECTED_STRATGIES,
  RESET_FILL_RATE_AND_DETAIL_DATA,
  RESIZE_CANVAS,
} from "./constants";
import { combineReducers } from "redux";
import { formatData } from "./util";

function loadCounterpartyDataReducer(
  state = {
    dataByDashboard: { data: {}, lineColorMap: {} },
    dataByCpeFamily: { data: {}, lineColorMap: {} },
  },
  action
) {
  switch (action.type) {
    case `${LOAD_DATA}_SUCCESS`:
      return (
        formatData(action.result.data, action.result.scenarioTypes) || {
          dataByDashboard: { data: {}, lineColorMap: {} },
          dataByCpeFamily: { data: {}, lineColorMap: {} },
        }
      );
    case DESTROY_DATA:
      return {
        dataByDashboard: { data: {}, lineColorMap: {} },
        dataByCpeFamily: { data: {}, lineColorMap: {} },
      };
  }
  return state;
}

function loadScenarioTypesReducer(state = [], action) {
  switch (action.type) {
    case `${LOAD_DATA}_SUCCESS`:
      return action.result.scenarioTypes || [];
  }
  return state;
}

function loadSelectedDashboardsReducer(state = [], action) {
  switch (action.type) {
    case LOAD_SELECTED_DASHBOARDS:
      return action.payload || [];
  }
  return state;
}

function loadLocateEfficiencyDataReducer(state = [], action) {
  switch (action.type) {
    case `${LOAD_LOCATE_EFFICIENCY_DATA}_SUCCESS`:
      return action.data || [];
    case RESET_FILL_RATE_AND_DETAIL_DATA:
      return [];
  }
  return state;
}

function loadLocateEfficiencyDetailDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_LOCATE_DETAIL_DATA}_SUCCESS`:
      return action.data || [];
    case RESET_FILL_RATE_AND_DETAIL_DATA:
      return [];
  }
  return state;
}

function loadFillRateTrendDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_FILL_RATE_TREND_DATA}_SUCCESS`:
      return action.data || [];
    case RESET_FILL_RATE_AND_DETAIL_DATA:
      return [];
  }
  return state;
}

function selectedStragiesReducer(state = [], action) {
  switch (action.type) {
    case SELECTED_STRATGIES:
      return action.payload || [];
  }
  return state;
}

function resizeCanvasReducer(state = false, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  selectedDashboard: loadSelectedDashboardsReducer,
  counterpartyData: loadCounterpartyDataReducer,
  scenarioTypes: loadScenarioTypesReducer,
  locateEfficiencyData: loadLocateEfficiencyDataReducer,
  locateEfficiencyDetailData: loadLocateEfficiencyDetailDataReducer,
  fillRateData: loadFillRateTrendDataReducer,
  selectedStrategies: selectedStragiesReducer,
  resizeCanvas: resizeCanvasReducer,
});

export default rootReducer;
