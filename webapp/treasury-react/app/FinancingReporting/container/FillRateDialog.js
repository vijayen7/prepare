import React, { Component } from "react";
import Dialog from "commons/components/Dialog";
import { Layout } from "arc-react-components";
import SingleSelectFilter from "commons/components/SingleSelectFilter";
import FillRateChart from "./../components/FillRateChart"
import { getStartDateForDifference, convertJavaDate, getCommaSeparatedValuesOrNullForAll } from "commons/util";

export default class FillRateDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedNumberOfDays: { key: 30, value: "30" }
    };
  }

  closeTrendDialog = () => {
    this.setState({
      selectedNumberOfDays: { key: 30, value: "30" }
    }, () => this.props.closeFillRateDialog());
  }

  onSelect = (params) => {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }


  onSelectNumberOfDays = (params) => {
    this.onSelect(params);
    let payload = {
      startDate: getStartDateForDifference(convertJavaDate(this.props.selectedRowItem.date), params.value.key),
      endDate: convertJavaDate(this.props.selectedRowItem.date),
      strategyIds: getCommaSeparatedValuesOrNullForAll(this.props.selectedStrategies),
      cpeFamilyIds: [this.props.selectedRowItem.cpeFamilyId]
    }
    this.props.fetchFillRateTrendData(payload)
  }

  render() {
    return (
      <Dialog
        isOpen={this.props.showTrend}
        title={this.props.selectedRowItem.cpeFamilyName}
        onClose={this.closeTrendDialog}
      >
        <Layout>
          <Layout.Child>
            <SingleSelectFilter
              data={[
                { key: 30, value: "30" },
                { key: 60, value: "60" },
                { key: 90, value: "90" }
              ]}
              onSelect={this.onSelectNumberOfDays}
              selectedData={this.state.selectedNumberOfDays}
              stateKey="selectedNumberOfDays"
              label="Time Frame(Days)"
            />
          </Layout.Child>
          <Layout.Child size="Fit">
            <FillRateChart fillRateData={this.props.fillRateData} />
          </Layout.Child>
        </Layout>
      </Dialog>
    );
  }
}
