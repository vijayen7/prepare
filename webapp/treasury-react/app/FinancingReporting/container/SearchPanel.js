import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Layout,
  Panel,
  DateRangePicker,
  TabPanel,
  Card,
} from "arc-react-components";
import Sidebar from "commons/components/Sidebar";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import CountryFilter from "commons/container/CountryFilter";
import StrategiesFilter from "commons/container/StrategiesFilter";
import CpeFamilyGroupFilter from "commons/container/CpeFamilyGroupFilter";
import CpeFamilyFilter from "commons/container/CpeFamilyFilter";
import DashboardFilter from "../components/DashboardFilter";
import FilterButton from "commons/components/FilterButton";
import Button from "commons/components/Button";
import SaveSettingsManager from "commons/components/SaveSettingsManager";
import {
  fetchData,
  loadSelectedDashboards,
  destroyData,
  fetchLocateEfficiencyData,
  selectedStrategies,
  resetFillRateAndDetailData,
  resizeCanvas,
} from "../actions";
import {
  getPreviousBusinessDay,
  dashboards,
  getPreviousMonthDay,
} from "../util";
import { getCommaSeparatedValuesOrNullForAll } from "commons/util";

class SearchPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: getPreviousBusinessDay()[1],
      selectedEndDate: getPreviousBusinessDay()[0],
      selectedAgreementTypes: [{ key: 2, value: "PB [2]" }],
      selectedStrategies: [],
      selectedCpeFamiliesGroup: [],
      selectedCpeFamilies: [],
      selectedCountries: [],
      selectedDashboard: [],
      selectedSearch: "basic",
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.view == nextProps.view) return;
    else if (nextProps.view == "locateEfficiency") {
      let newState = Object.assign({}, this.state);
      newState["selectedStartDate"] = getPreviousMonthDay()[1];
      newState["selectedEndDate"] = getPreviousBusinessDay()[0];
      this.setState(newState);
    } else if (nextProps.view == "counterpartyReporting") {
      let newState = Object.assign({}, this.state);
      newState["selectedStartDate"] = getPreviousBusinessDay()[1];
      newState["selectedEndDate"] = getPreviousBusinessDay()[0];
      this.setState(newState);
    } else return;
  }

  componentDidMount() {
    this.node.addEventListener("keydown", this.enterFunction, false);
  }

  enterFunction = (e) => {
    if (e.keyCode == 13) {
      const node = e.path[0];
      if (node.id !== undefined && node.id === "search-panel") {
        this.onClick();
        this.node.blur();
      } else this.node.focus();
    }
  };

  onSelect = (params) => {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  };

  onClick = () => {
    if (this.props.view == "counterpartyReporting") {
      this.props.destroyData();
      if (this.state.selectedDashboard.length === 0) {
        this.setState({ selectedDashboard: dashboards }, () => {
          this.props.loadSelectedDashboards(this.state.selectedDashboard);
        });
      } else {
        this.props.loadSelectedDashboards(this.state.selectedDashboard);
      }
      let payload = {
        startDate: this.state.selectedStartDate,
        endDate: this.state.selectedEndDate,
        agreementTypeIds: getCommaSeparatedValuesOrNullForAll(
          this.state.selectedAgreementTypes
        ),
        strategyIds: getCommaSeparatedValuesOrNullForAll(
          this.state.selectedStrategies
        ),
        cpeFamilyIds: getCommaSeparatedValuesOrNullForAll(
          this.state.selectedCpeFamilies
        ),
        countryIds: getCommaSeparatedValuesOrNullForAll(
          this.state.selectedCountries
        ),
        cpeFamilyGroupIds: getCommaSeparatedValuesOrNullForAll(
          this.state.selectedCpeFamiliesGroup
        ),
      };
      this.props.fetchData(payload, this.state.selectedDashboard);
    } else {
      let payload = {
        startDate: this.state.selectedStartDate,
        endDate: this.state.selectedEndDate,
        strategyIds: getCommaSeparatedValuesOrNullForAll(
          this.state.selectedStrategies
        ),
        cpeFamilyIds: getCommaSeparatedValuesOrNullForAll(
          this.state.selectedCpeFamilies
        ),
      };
      this.props.selectedStrategies(this.state.selectedStrategies);
      this.props.resetFillRateAndDetailData();
      this.props.fetchLocateEfficiencyData(payload);
    }
  };

  onDateChange = (date) => {
    this.setState({ selectedStartDate: date[0], selectedEndDate: date[1] });
  };

  applySavedFilters = (selectedFilters) => {
    this.setState(selectedFilters);
  };

  resizeCanvas = () => {
    this.props.resizeCanvas();
  };

  render() {
    return (
      <div
        id="search-panel"
        ref={(node) => (this.node = node)}
        tabindex="0"
        style={{ height: "100%" }}
      >
        <Sidebar
          collapsible={true}
          size="200px"
          resizeCanvas={this.resizeCanvas}
        >
          <TabPanel>
            <TabPanel.Tab label="Basic Search" tabId="basic" id="basic">
              <Card className="margin--vertical">
                <SaveSettingsManager
                  selectedFilters={this.state}
                  applySavedFilters={this.applySavedFilters}
                  applicationName="Counterparty_Reporting_Filter"
                />
              </Card>
              <Card className="margin--vertical">
                <div className="margin--vertical">
                  <label title="Date">Date</label>
                  <DateRangePicker
                    placeholder="Select Date"
                    value={[
                      this.state.selectedStartDate,
                      this.state.selectedEndDate,
                    ]}
                    onChange={this.onDateChange}
                  />
                </div>
              </Card>
            </TabPanel.Tab>
            <TabPanel.Tab
              label="Advanced Search"
              tabId="advanced"
              id="advanced"
            >
              <Card className="margin--vertical">
                <SaveSettingsManager
                  selectedFilters={this.state}
                  applySavedFilters={this.applySavedFilters}
                  applicationName="Counterparty_Reporting_Filter"
                />
              </Card>
              <Card className="margin--vertical">
                <div className={"margin--vertical"}>
                  <label title="Date">Date</label>
                  <DateRangePicker
                    placeholder="Select Date"
                    value={[
                      this.state.selectedStartDate,
                      this.state.selectedEndDate,
                    ]}
                    onChange={this.onDateChange}
                  />
                </div>
              </Card>
              <Card className="margin--vertical">
                <StrategiesFilter
                  onSelect={this.onSelect}
                  selectedData={this.state.selectedStrategies}
                  horizontalLayout={false}
                  showTokens={true}
                  maxTokenHeight={100}
                />
                <CpeFamilyFilter
                  onSelect={this.onSelect}
                  selectedData={this.state.selectedCpeFamilies}
                  horizontalLayout={false}
                  showTokens={true}
                  maxTokenHeight={100}
                />
                {this.props.view == "counterpartyReporting" ? (
                  <React.Fragment>
                    <AgreementTypeFilter
                      onSelect={this.onSelect}
                      selectedData={this.state.selectedAgreementTypes}
                      horizontalLayout={false}
                      showTokens={true}
                      maxTokenHeight={100}
                    />

                    <CpeFamilyGroupFilter
                      onSelect={this.onSelect}
                      selectedData={this.state.selectedCpeFamiliesGroup}
                      horizontalLayout={false}
                      showTokens={true}
                      maxTokenHeight={100}
                    />

                    <CountryFilter
                      onSelect={this.onSelect}
                      selectedData={this.state.selectedCountries}
                      horizontalLayout={false}
                      showTokens={true}
                      maxTokenHeight={100}
                    />

                    <DashboardFilter
                      onSelect={this.onSelect}
                      selectedData={this.state.selectedDashboard}
                      horizontalLayout={false}
                      showTokens={true}
                      maxTokenHeight={100}
                    />
                  </React.Fragment>
                ) : null}
              </Card>
            </TabPanel.Tab>
          </TabPanel>
          <FilterButton onClick={this.onClick} reset={false} label="Search" />
        </Sidebar>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchData,
      loadSelectedDashboards,
      destroyData,
      fetchLocateEfficiencyData,
      selectedStrategies,
      resetFillRateAndDetailData,
      resizeCanvas,
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(SearchPanel);
