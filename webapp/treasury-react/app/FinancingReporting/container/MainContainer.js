import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Chart from "../components/Chart";
import Message from "commons/components/Message";
import DialogContainer from "../components/DialogContainer";
import { URL } from "commons/constants";
import Link from "commons/components/Link";
import { Card, Layout, ToggleGroup, Panel } from "arc-react-components";
import Label from "commons/components/Label";
import _ from "lodash";
import CustomLegend from "../components/CustomLegend";
import { ReactArcGrid } from "arc-grid";
import LocateEfficiencyGrid from "commons/components/GridWithCellClick";
import {
  getLocateEfficiencyColumns,
  getLocateEfficiencyDetailDataColumns,
} from "../grid/columnConfig";
import {
  getLocateEfficiencyOptions,
  getLocateEfficiencyDetailDataOptions,
} from "../grid/gridOptions";
import { fetchLocateDetailData, fetchFillRateTrendData } from "../actions";
import {
  convertJavaDate,
  getStartDateForDifference,
  getCommaSeparatedValuesOrNullForAll,
} from "commons/util";
import FillRateDialog from "./FillRateDialog";
class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDialog: false,
      activeDashboard: { key: "", value: "" },
      disabled: {},
      activeDashboardView: "dataByDashboard",
      numberOfDashboards: "9",
      currentPage: 1,
      showDetailPanel: false,
      showTrend: false,
      selectedRowItem: {},
    };
  }

  componentDidMount() {
    this.node.addEventListener("keydown", this.escFunction, false);
  }

  escFunction = (e) => {
    if (e.keyCode == 27) {
      this.setState({ openDialog: false });
    }
  };

  toggleDialog = (dashboard) => {
    this.setState({
      openDialog: !this.state.openDialog,
      activeDashboard: dashboard,
    });
  };

  toggleLegend = (line) => {
    let disabled = Object.assign({}, this.state.disabled);
    if (disabled[line] == undefined) {
      disabled[line] = true;
    } else {
      disabled[line] = !disabled[line];
    }
    this.setState({ disabled });
  };

  getChartHeight = (numberOfDashboards) => {
    if (numberOfDashboards == 1) return 500;
    else if (numberOfDashboards == 4) return 250;
    else if (numberOfDashboards == 6) return 250;
    else return 170;
  };

  renderLink() {
    return (
      <span className="form-field--inline--right">
        <Link
          id="counterparty-grouping"
          text="View counterparty Grouping"
          href={`${URL}/treasury/financing/reporting/counterparty-grouping.html`}
        />
      </span>
    );
  }

  paginate = (dashboards, currentPage = 1, numberPerPage) => {
    let totalPages = Math.ceil(dashboards.length / numberPerPage);
    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > totalPages) {
      currentPage = totalPages;
    }

    let startIndex = (currentPage - 1) * numberPerPage;
    let endIndex = Math.min(
      startIndex + numberPerPage - 1,
      dashboards.length - 1
    );
    return {
      dashboardList: dashboards.slice(startIndex, endIndex + 1),
      totalPages,
    };
  };

  onCellClickHandler = (args) => {
    let { role } = args.event.target.dataset;
    if (role == "fillRate") {
      this.setState(
        {
          showTrend: true,
          selectedRowItem: args.item,
        },
        () => {
          let payload = {
            startDate: getStartDateForDifference(
              convertJavaDate(args.item.date),
              30
            ),
            endDate: convertJavaDate(args.item.date),
            strategyIds: getCommaSeparatedValuesOrNullForAll(
              this.props.selectedStrategies
            ),
            cpeFamilyIds: [args.item.cpeFamilyId],
          };
          this.props.fetchFillRateTrendData(payload);
        }
      );
    } else if (role == "detail") {
      let payload = {
        startDate: convertJavaDate(args.item.date),
        endDate: convertJavaDate(args.item.date),
        strategyIds: getCommaSeparatedValuesOrNullForAll(
          this.props.selectedStrategies
        ),
        cpeFamilyIds: [args.item.cpeFamilyId],
      };
      this.showDetailPanel();
      this.props.fetchLocateDetailData(payload);
    } else {
      return;
    }
  };

  showDetailPanel = () => {
    this.setState({ showDetailPanel: true });
  };

  closeDetailPanel = () => {
    this.setState({ showDetailPanel: false });
  };

  closeFillRateDialog = () => {
    this.setState({ showTrend: false });
  };

  render() {
    if (this.props.view == "locateEfficiency") {
      if (this.props.locateEfficiencyData.length > 0) {
        return (
          <React.Fragment>
            <FillRateDialog
              fillRateData={this.props.fillRateData}
              selectedStrategies={this.props.selectedStrategies}
              showTrend={this.state.showTrend}
              selectedRowItem={this.state.selectedRowItem}
              closeFillRateDialog={this.closeFillRateDialog}
              fetchFillRateTrendData={this.props.fetchFillRateTrendData}
            />
            <Layout>
              <Layout.Child size={8} childId="1">
                <LocateEfficiencyGrid
                  gridId="locate-efficiency-container"
                  data={this.props.locateEfficiencyData}
                  gridColumns={getLocateEfficiencyColumns()}
                  gridOptions={getLocateEfficiencyOptions()}
                  onCellClick={this.onCellClickHandler}
                  resizeCanvas={this.props.resizeCanvas}
                  fill={true}
                />
              </Layout.Child>
              {this.state.showDetailPanel &&
                this.props.locateEfficiencyDetailData.length > 0 && (
                  <Layout.Child size={4} childId="2">
                    <Panel
                      title="Detail Info"
                      dismissible
                      onClose={this.closeDetailPanel}
                    >
                      <ReactArcGrid
                        gridId="locate-efficiency-detail"
                        data={this.props.locateEfficiencyDetailData}
                        columns={getLocateEfficiencyDetailDataColumns()}
                        options={getLocateEfficiencyDetailDataOptions()}
                      />
                    </Panel>
                  </Layout.Child>
                )}
            </Layout>
          </React.Fragment>
        );
      } else {
        return <Message messageData="No data." />;
      }
    } else {
      let { data, lineColorMap } =
        this.props.data[this.state.activeDashboardView];
      let dashboards = [];
      if (this.state.activeDashboardView == "dataByDashboard") {
        dashboards = this.props.dashboards;
      } else {
        if ("lines" in data) {
          let selectedDashboards = _.chain(this.props.dashboards)
            .keyBy("value")
            .mapValues("value")
            .value();
          data["lines"] = _.intersection(
            _.keys(lineColorMap),
            _.keys(selectedDashboards)
          );
          _.difference(
            _.keys(lineColorMap),
            _.keys(selectedDashboards)
          ).forEach((o) => delete lineColorMap[o]);
        }
        if ("data" in data) {
          _.forEach(Object.keys(data.data["1"]), (cpeFamily) => {
            dashboards.push({ key: cpeFamily, value: cpeFamily });
          });
        }
      }
      let { dashboardList, totalPages } = this.paginate(
        dashboards,
        this.state.currentPage,
        parseInt(this.state.numberOfDashboards)
      );

      let dashboardsPerRow = 3;
      if (parseInt(this.state.numberOfDashboards) == 4) {
        dashboardsPerRow = 2;
      }
      let dashboardChunkList = _.chunk(dashboardList, dashboardsPerRow);
      let chartheight = this.getChartHeight(
        parseInt(this.state.numberOfDashboards)
      );

      return (
        <React.Fragment>
          <div
            id="main-container"
            ref={(node) => (this.node = node)}
            tabindex="0"
            style={{ height: "100%" }}
          >
            <DialogContainer
              data={data}
              lines={[this.state.activeDashboard.key]}
              open={this.state.openDialog}
              toggleDialog={this.toggleDialog}
              label={this.state.activeDashboard.value}
              lineColorMap={lineColorMap}
              scenarioTypes={this.props.scenarioTypes}
            />
            {!("data" in data) ? null : (
              <Layout>
                <Layout.Child size="Fit">
                  <Layout isColumnType>
                    <Layout.Child className="padding--horizontal">
                      Number of Dashboards per view
                      <ToggleGroup
                        className="padding--horizontal"
                        value={this.state.numberOfDashboards}
                        onChange={(value) => {
                          this.setState({
                            numberOfDashboards: value,
                            currentPage: 1,
                          });
                        }}
                      >
                        <ToggleGroup.Button key="1" data="1">
                          1
                        </ToggleGroup.Button>
                        <ToggleGroup.Button key="4" data="4">
                          4
                        </ToggleGroup.Button>
                        <ToggleGroup.Button key="6" data="6">
                          6
                        </ToggleGroup.Button>
                        <ToggleGroup.Button key="9" data="9">
                          9
                        </ToggleGroup.Button>
                      </ToggleGroup>
                    </Layout.Child>
                    <Layout.Child>
                      <ToggleGroup
                        className="form-field--center"
                        value={this.state.activeDashboardView}
                        onChange={(value) => {
                          this.setState({ activeDashboardView: value });
                        }}
                      >
                        <ToggleGroup.Button
                          key="dataByDashboard"
                          data="dataByDashboard"
                        >
                          Dashboard View
                        </ToggleGroup.Button>
                        <ToggleGroup.Button
                          key="dataByCpeFamily"
                          data="dataByCpeFamily"
                        >
                          CPE Family View
                        </ToggleGroup.Button>
                      </ToggleGroup>
                    </Layout.Child>
                    <Layout.Child> {this.renderLink()}</Layout.Child>
                  </Layout>
                </Layout.Child>
                <Layout.Child size="Fit">
                  <Panel className="padding">
                    <Layout isColumnType>
                      <Layout.Child size="Fit">
                        {this.state.currentPage > 1 ? (
                          <i
                            className="icon-arrow--circle--left"
                            onClick={() =>
                              this.setState({
                                currentPage: this.state.currentPage - 1,
                              })
                            }
                            style={{ fontSize: "1.2em" }}
                          ></i>
                        ) : null}
                      </Layout.Child>
                      <Layout.Child>
                        <Layout>
                          {dashboardChunkList.map((dashboardChunk) => {
                            return (
                              <Layout.Child>
                                <Layout isColumnType={true}>
                                  {dashboardChunk.map((dashboard) => {
                                    return (
                                      <Layout.Child className="padding--double">
                                        <Card
                                          onClick={() =>
                                            this.toggleDialog(dashboard)
                                          }
                                        >
                                          <Label label={dashboard.value} />
                                          <Chart
                                            data={data}
                                            dashboard={dashboard.key}
                                            dialogueView={false}
                                            chartHeight={chartheight}
                                            lineColorMap={lineColorMap}
                                            disabled={this.state.disabled}
                                            pivotByDashboard={false}
                                            scenarioType={"1"}
                                          />
                                        </Card>
                                      </Layout.Child>
                                    );
                                  })}
                                </Layout>
                              </Layout.Child>
                            );
                          })}
                        </Layout>
                      </Layout.Child>
                      <Layout.Child size="Fit">
                        {this.state.currentPage < totalPages ? (
                          <i
                            className="icon-arrow--circle--right"
                            onClick={() =>
                              this.setState({
                                currentPage: this.state.currentPage + 1,
                              })
                            }
                            style={{ fontSize: "1.2em" }}
                          ></i>
                        ) : null}
                      </Layout.Child>
                    </Layout>
                  </Panel>
                </Layout.Child>
                <Layout.Child size="Fit">
                  <p className="padding">
                    * Click on a dashboard for an enlarged view
                  </p>
                </Layout.Child>
                <Layout.Child size="Fit">
                  <CustomLegend
                    data={lineColorMap}
                    toggleLegend={this.toggleLegend}
                    disabled={this.state.disabled}
                  />
                </Layout.Child>
              </Layout>
            )}
          </div>
        </React.Fragment>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    dashboards: state.financingReporting.selectedDashboard,
    data: state.financingReporting.counterpartyData,
    scenarioTypes: state.financingReporting.scenarioTypes,
    locateEfficiencyData: state.financingReporting.locateEfficiencyData,
    locateEfficiencyDetailData:
      state.financingReporting.locateEfficiencyDetailData,
    fillRateData: state.financingReporting.fillRateData,
    selectedStrategies: state.financingReporting.selectedStrategies,
    resizeCanvas: state.financingReporting.resizeCanvas,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchLocateDetailData,
      fetchFillRateTrendData,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
