export function getOptions(dashboard) {
  let options = {
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    forceFitColumns: true,
    sortList: [
      {
        columnId: "date",
        sortAsc: true
      }
    ],
    sheetName: "TreasuryReporting_View"
  };

  return options;
}

export function getLocateEfficiencyOptions() {
  let options = {
    applyFilteringOnGrid: true,
    exportToExcel: true,
    forceFitColumns: true,
    sortList: [
      {
        columnId: "date",
        sortAsc: true
      }
    ],
    sheetName: "Locate_Efficiency_View"
  };

  return options;
}

export function getLocateEfficiencyDetailDataOptions() {
  let options = {
    applyFilteringOnGrid: true,
    exportToExcel: true,
    forceFitColumns: true,
    sortList: [
      {
        columnId: "date",
        sortAsc: true
      }
    ],
    sheetName: "Locate_Efficiency_Detail_Data_View"
  };

  return options;
}



