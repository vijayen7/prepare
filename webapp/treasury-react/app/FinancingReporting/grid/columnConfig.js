import {
  checkForPercentFormatter,
  checkForMillionFormatter,
  millionFormatter,
  thousandFormatter,
  percentFormatterDefaultZero,
} from "./../util";
import { convertJavaNYCDate, getCommaSeparatedNumber } from "commons/util";
import { linkFormatter } from "commons/grid/formatters";
import { numberComparator } from "commons/grid/comparators";

export function getColumns(lines, dashboard) {
  let columns = [
    {
      id: "date",
      name: "Date",
      field: "date",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      width: 90,
    },
  ];
  let counter = 1;
  lines.map((line) => {
    columns.push({
      id: counter.toString(),
      name: line,
      field: line,
      toolTip: line,
      type: "text",
      filter: true,
      sortable: true,
      formatter: getColumnFormatter(line, dashboard),
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.00",
      width: 90,
    });
    counter++;
  });

  return columns;
}

function getColumnFormatter(line, dashboard) {
  if (checkForPercentFormatter(dashboard, line)) {
    return percentFormatterDefaultZero;
  } else if (checkForMillionFormatter(dashboard, line)) {
    return millionFormatter;
  } else {
    return thousandFormatter;
  }
}

export function getLocateEfficiencyColumns() {
  return [
    {
      id: "detail",
      name: "Detail",
      field: "detail",
      toolTip: "Details",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return linkFormatter(
          row,
          cell,
          "<i class='icon-info' data-role='detail' />",
          columnDef,
          dataContext
        );
      },
      headerCssClass: "b",
      width: 30,
    },
    {
      id: "date",
      name: "Date",
      field: "date",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDate(value);
      },
      width: 90,
    },
    {
      id: "cpeFamilyName",
      name: "Cpe Family",
      field: "cpeFamilyName",
      toolTip: "Cpe Family",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80,
    },
    {
      id: "fillRatePercent",
      name: "Fill Rate Percent",
      field: "fillRatePercent",
      toolTip: "Fill Rate Percent",
      type: "text",
      filter: true,
      sortable: true,
      formatter: percentFormatterDefaultZero,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
      comparator: function (a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
    },
    {
      id: "tickerCountFillRate0to20",
      name: "0-20% Fill Rate Ticker",
      field: "tickerCountFillRate0to20",
      toolTip: "0-20% Fill Rate Ticker",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
    },
    {
      id: "tickerCountFillRate20to30",
      name: "20%-30% Fill Rate Ticker",
      field: "tickerCountFillRate20to30",
      toolTip: "20%-30% Fill Rate Ticker",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
    },
    {
      id: "tickerCountFillRate30to100",
      name: "30%-100% Fill Rate Ticker",
      field: "tickerCountFillRate30to100",
      toolTip: "30%-100% Fill Rate Ticker",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
    },
    {
      id: "fillRateTrend",
      name: "Fill Rate Trend",
      field: "fillRateTrend",
      toolTip: "Fill Rate Trend",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return linkFormatter(
          row,
          cell,
          "<i class='icon-link' data-role='fillRate' />",
          columnDef,
          dataContext
        );
      },
      headerCssClass: "b",
      width: 50,
    },
    {
      id: "noGrantPercent",
      name: "% of tickers with no grant",
      field: "noGrantPercent",
      toolTip: "% of tickers with no grant",
      type: "text",
      filter: true,
      sortable: true,
      formatter: percentFormatterDefaultZero,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
      comparator: function (a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
    },
    {
      id: "fullGrantGCPercent",
      name: "% of GC tickers with full grant",
      field: "fullGrantGCPercent",
      toolTip: "Full Grant GC Percent",
      type: "text",
      filter: true,
      sortable: true,
      formatter: percentFormatterDefaultZero,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
      comparator: function (a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
    },
    {
      id: "fullGrantHTBPercent",
      name: "% of HTB tickers with full grant",
      field: "fullGrantHTBPercent",
      toolTip: "Full Grant HTB Percent",
      type: "text",
      filter: true,
      sortable: true,
      formatter: percentFormatterDefaultZero,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
      comparator: function (a, b) {
        return numberComparator(a[sortcol], b[sortcol]);
      },
    },
    {
      id: "totalGCTickerRequest",
      name: "Total GC Ticker Requested",
      field: "totalGCTickerRequest",
      toolTip: "Total GC Ticker Requested",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
    },
    {
      id: "totalHTBTickerRequest",
      name: "Total HTB Ticker Requested",
      field: "totalHTBTickerRequest",
      toolTip: "Total HTB Ticker Requested",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
    },
    {
      id: "totalRequestedTicker",
      name: "Total Ticker Requested",
      field: "totalRequestedTicker",
      toolTip: "Total Ticker Requested",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Number,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 80,
    },
  ];
}

function allColumns() {
  return [
    {
      id: "date",
      name: "Date",
      field: "date",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true,
      },
    },
    {
      id: "counterpartyEntity",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 120,
    },
    {
      id: "strategyName",
      name: "Strategy",
      field: "strategyName",
      toolTip: "Strategy",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 120,
    },
    {
      name: "CCY",
      id: "currency",
      field: "ccyName",
      toolTip: "Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 70,
    },
    {
      id: "isin",
      name: "ISIN",
      field: "isin",
      toolTip: "isin",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 90,
    },
    {
      id: "cusip",
      name: "CUSIP",
      field: "cusip",
      toolTip: "CUSIP",
      type: "text",
      filter: true,
      sortable: true,
      width: 90,
      headerCssClass: "b",
    },
    {
      name: "SEDOL",
      id: "sedol",
      field: "sedol",
      toolTip: "SEDOL",
      type: "text",
      filter: true,
      sortable: true,
      width: 80,
      headerCssClass: "b",
    },
    {
      name: "Ticker",
      id: "ticker",
      field: "ticker",
      toolTip: "Ticker",
      type: "text",
      filter: true,
      sortable: true,
      width: 60,
      headerCssClass: "b",
    },
    {
      name: "Security Name",
      id: "securityName",
      field: "securityName",
      toolTip: "Security Name",
      type: "text",
      filter: true,
      sortable: true,
      width: 90,
      headerCssClass: "b",
    },
    {
      name: "Availability",
      field: "availability",
      id: "availability",
      toolTip: "Availability",
      type: "number",
      filter: true,
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return getCommaSeparatedNumber(value);
      },
      excelFormatter: "#,##0.0",
      width: 100,
      headerCssClass: "b",
    },
    {
      name: "Request Quantity",
      id: "requestQuantity",
      field: "requestQuantity",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return getCommaSeparatedNumber(value);
      },
      excelFormatter: "#,##0.0",
      width: 120,
      headerCssClass: "b",
    },
    {
      name: "Stock Loan Fee Rate (%)",
      id: "stokLoanFeeRate",
      field: "stockLoanFeeRate",
      type: "number",
      filter: true,
      sortable: true,
      width: 120,
      headerCssClass: "b",
    },
    {
      name: "SPN",
      id: "spn",
      field: "spn",
      type: "text",
      filter: true,
      sortable: true,
      width: 80,
      headerCssClass: "b",
    },
    {
      name: "Classification",
      field: "internalClassification",
      id: "internalClassification",
      type: "text",
      filter: true,
      sortable: true,
      width: 60,
      headerCssClass: "b",
    },
    {
      name: "Source",
      field: "fileAliasName",
      id: "fileAliasName",
      type: "text",
      filter: true,
      sortable: true,
      width: 90,
      headerCssClass: "b",
    },
    {
      name: "Source Date",
      field: "sourceDate",
      id: "sourceDate",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (
        row,
        cell,
        value,
        columnDef,
        dataContext,
        isExportToExcel
      ) {
        return convertJavaNYCDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDate(value);
      },
      width: 90,
      headerCssClass: "b",
    },
  ];
}

export function getLocateEfficiencyDetailDataColumns() {
  const columns = [
    "date",
    "cpeName",
    "strategyName",
    "spn",
    "ccyName",
    "isin",
    "cusip",
    "sedol",
    "ticker",
    "securityName",
    "availability",
    "stockLoanFeeRate",
    "internalClassification",
    "fileAliasName",
    "requestQuantity",
  ];
  return columns.map((column) => _.find(allColumns(), { field: column }));
}
