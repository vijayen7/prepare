import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  LOAD_DATA,
  LOAD_LOCATE_EFFICIENCY_DATA,
  FETCH_LOCATE_DETAIL_DATA,
  FETCH_FILL_RATE_TREND_DATA,
  RESIZE_CANVAS,
} from "./constants";
import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
} from "commons/constants";
import {
  getData,
  getScenarioTypes,
  getLocateEfficiencyData,
  getLocateEfficiencyDetailData,
  getFillRateTrendData,
} from "./api";

function* fetchData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getData, action.payload);
    const scenarioTypes = yield call(
      getScenarioTypes,
      action.payload["endDate"]
    );
    yield put({
      type: `${LOAD_DATA}_SUCCESS`,
      result: { data, scenarioTypes },
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* data() {
  yield [takeEvery(LOAD_DATA, fetchData)];
}

function* fetchLocateEfficiencyDetailData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLocateEfficiencyDetailData, action.payload);
    yield put({ type: `${FETCH_LOCATE_DETAIL_DATA}_SUCCESS`, data });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* fetchLocateEfficiencyDetail() {
  yield [takeEvery(FETCH_LOCATE_DETAIL_DATA, fetchLocateEfficiencyDetailData)];
}

function* fetchFillRateTrendData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getFillRateTrendData, action.payload);
    yield put({ type: `${FETCH_FILL_RATE_TREND_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* fetchFillRateTrend() {
  yield [takeEvery(FETCH_FILL_RATE_TREND_DATA, fetchFillRateTrendData)];
}

function* fetchLocateEfficiencyData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getLocateEfficiencyData, action.payload);
    yield put({ type: `${LOAD_LOCATE_EFFICIENCY_DATA}_SUCCESS`, data });
    yield put({ type: RESIZE_CANVAS });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* fetchLocateEfficiency() {
  yield [takeEvery(LOAD_LOCATE_EFFICIENCY_DATA, fetchLocateEfficiencyData)];
}

function* financingReportingSaga() {
  yield all([
    data(),
    fetchLocateEfficiencyDetail(),
    fetchFillRateTrend(),
    fetchLocateEfficiency(),
  ]);
}

export default financingReportingSaga;
