import {
  LOAD_SELECTED_DASHBOARDS,
  DESTROY_DATA,
  LOAD_DATA,
  LOAD_LOCATE_EFFICIENCY_DATA,
  FETCH_LOCATE_DETAIL_DATA,
  FETCH_FILL_RATE_TREND_DATA,
  SELECTED_STRATGIES,
  RESET_FILL_RATE_AND_DETAIL_DATA,
  RESIZE_CANVAS,
} from "./constants";

export function fetchData(payload) {
  return {
    type: LOAD_DATA,
    payload,
  };
}

export function fetchLocateEfficiencyData(payload) {
  return {
    type: LOAD_LOCATE_EFFICIENCY_DATA,
    payload,
  };
}

export function loadSelectedDashboards(payload) {
  return {
    type: LOAD_SELECTED_DASHBOARDS,
    payload,
  };
}

export function destroyData() {
  return {
    type: DESTROY_DATA,
  };
}

export function fetchLocateDetailData(payload) {
  return {
    type: FETCH_LOCATE_DETAIL_DATA,
    payload,
  };
}

export function fetchFillRateTrendData(payload) {
  return {
    type: FETCH_FILL_RATE_TREND_DATA,
    payload,
  };
}

export function selectedStrategies(payload) {
  return {
    type: SELECTED_STRATGIES,
    payload,
  };
}

export function resetFillRateAndDetailData(payload) {
  return {
    type: RESET_FILL_RATE_AND_DETAIL_DATA,
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS,
  };
}
