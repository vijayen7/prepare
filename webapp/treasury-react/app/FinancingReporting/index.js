import React, { Component } from "react";
import { hot } from "react-hot-loader/root";
import MainContainer from "./container/MainContainer";
import SearchPanel from "./container/SearchPanel";
import { Layout } from "arc-react-components";
import Loader from "commons/container/Loader";

class FinancingReporting extends Component {
  constructor(props) {
    super(props);
    this.getBreadCrumbs = this.getBreadCrumbs.bind(this);
    this.state = { view: "counterpartyReporting" };
  }

  getBreadCrumbs() {
    return [
      {
        name: "Treasury-Financing",
        link: "#",
      },
      {
        name: "Counterparty Report",
        link: "./counterparty-reporting.html",
      },
    ];
  }

  componentDidMount() {
    this.counterpartyReporting &&
      this.counterpartyReporting.classList.add("active");
    var header = this.header;
    var breadcrumbs = this.getBreadCrumbs();
    if (header) {
      if (header.ready) {
        header.setBreadcrumb(breadcrumbs);
      } else {
        header.addEventListener("ready", header.setBreadcrumb(breadcrumbs));
      }
    }
  }

  handleToggle = (view) => {
    this.counterpartyReporting.classList.remove("active");
    this.locateEfficiency.classList.remove("active");
    switch (view) {
      case "locateEfficiency":
        this.locateEfficiency.classList.add("active");
        break;
      case "counterpartyReporting":
        this.counterpartyReporting.classList.add("active");
        break;
    }
    this.setState({ view });
  };

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            ref={(header) => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div
              slot="application-menu"
              className="application-menu-toggle-view"
            >
              <a
                ref={(counterpartyReporting) => {
                  this.counterpartyReporting = counterpartyReporting;
                }}
                onClick={() => this.handleToggle("counterpartyReporting")}
              >
                Counterparty Reporting
              </a>
              <a
                ref={(locateEfficiency) => {
                  this.locateEfficiency = locateEfficiency;
                }}
                onClick={() => this.handleToggle("locateEfficiency")}
              >
                Locate Efficiency
              </a>
            </div>
          </arc-header>
          <div className="layout--flex padding--top">
            <div className="size--content padding--right">
              <SearchPanel view={this.state.view} />
            </div>
            <MainContainer view={this.state.view} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default hot(FinancingReporting);
