import { put, takeEvery, call, all } from "redux-saga/effects";
import {
  START_LOADING,
  END_LOADING
} from "commons/constants";
import {
  UPLOAD_RATE_NEGOTIATION_SAVINGS_DATA,
  FETCH_RATE_NEGOTIATION_SAVINGS_STATUS_DATA,
  DOWNLOAD_RATE_NEGOTIATION_SAVINGS_DATA,
  RESET_RATE_NEGOTIATION_SAVINGS_SEARCH_DATA,
  PROCESS_RATE_NEGOTIATION_SAVINGS_DATA,
  CALCULATION_STATUS,
  ACCRUAL_DAYS_LIMIT
} from "./constants";
import {
  uploadRateNegotiationSavingsData,
  fetchRateNegotiationSavingsData,
  downloadRateNegotiationSavingsData,
  processRateNegotiationSavingsData
} from "./api";
import { ToastService } from 'arc-react-components';

function* uploadRateNegotiationSavings(action) {
  try {
    yield put({ type: START_LOADING });
    const processPayload = action.uploadData;
    const searchPayload = action.searchData;
    const calculationStatusData = yield call(
      uploadRateNegotiationSavingsData,
      action.uploadData
    );
    if(calculationStatusData) {
      ToastService.append({
        content: `File uploaded successfully`,
        type: ToastService.ToastType.SUCCESS,
        placement: ToastService.Placement.TOP_RIGHT,
        dismissTime: 5000
      });
      processPayload.append(CALCULATION_STATUS, JSON.stringify(calculationStatusData));
      processPayload.append(ACCRUAL_DAYS_LIMIT, JSON.stringify(action.accrualDaysLimitData));

      yield put({ type: PROCESS_RATE_NEGOTIATION_SAVINGS_DATA, processPayload });
      yield put({type: FETCH_RATE_NEGOTIATION_SAVINGS_STATUS_DATA, searchPayload});
    } else {
        ToastService.append({
          content: `There was a problem in uploading the input file.`,
          type: ToastService.ToastType.CRITICAL,
          placement: ToastService.Placement.TOP_RIGHT,
          dismissTime: 5000
        });
    }
  } catch (e) {
    ToastService.append({
      content: `There was a problem in uploading the input file.`,
      type: ToastService.ToastType.CRITICAL,
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 5000
    });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* processRateNegotiationSavings(action) {
  try {
    const data = yield call(processRateNegotiationSavingsData, action.processPayload);
  } catch (e) {
    ToastService.append({
      content: `There was a problem in processing the input file.`,
      type: ToastService.ToastType.CRITICAL,
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 5000
    });
  }
}

function* fetchRateNegotiationSavings(action) {
  try {
    yield put({ type: START_LOADING });

    const data = yield call(
      fetchRateNegotiationSavingsData,
      action.searchPayload
    );

    if (typeof data.message !== "undefined" && typeof data.localizedMessage !== "undefined") {
      yield put({ type: RESET_RATE_NEGOTIATION_SAVINGS_SEARCH_DATA, data });
    } else {
      if (data.length == 0) {
        ToastService.append({
          content: `There was no data for the searched date.`,
          type: ToastService.ToastType.WARNING,
          placement: ToastService.Placement.TOP_RIGHT,
          dismissTime: 5000
        });
        yield put({ type: RESET_RATE_NEGOTIATION_SAVINGS_SEARCH_DATA, data });
      } else {
        yield put({ type: `${action.type}_SUCCESS`, data });
      }
    }
  } catch (e) {
    ToastService.append({
      content: `There was some problem retrieving calculation status.`,
      type: ToastService.ToastType.WARNING,
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 5000
    });
    yield put({ type: RESET_RATE_NEGOTIATION_SAVINGS_SEARCH_DATA, data });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* downloadRateNegotiationSavings(action) {
  try {
    yield put({ type: START_LOADING });
    yield call(downloadRateNegotiationSavingsData, action.payload);
  } catch (e) {
    ToastService.append({
      content: `There was some problem downloading output file.`,
      type: ToastService.ToastType.WARNING,
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 5000
    });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* uploadRateNegotiationSavingsDataSaga() {
  yield takeEvery(
    UPLOAD_RATE_NEGOTIATION_SAVINGS_DATA,
    uploadRateNegotiationSavings
  );
}

function* fetchRateNegotiationSavingsDataSaga() {
  yield takeEvery(
    FETCH_RATE_NEGOTIATION_SAVINGS_STATUS_DATA,
    fetchRateNegotiationSavings
  );
}

function* downloadRateNegotiationSavingsDataSaga() {
  yield takeEvery(
    DOWNLOAD_RATE_NEGOTIATION_SAVINGS_DATA,
    downloadRateNegotiationSavings
  );
}

function* processRateNegotiationSavingsDataSaga() {
  yield takeEvery(PROCESS_RATE_NEGOTIATION_SAVINGS_DATA, processRateNegotiationSavings);
}

function* rateNegotiationSavingsSaga() {
  yield all([
    uploadRateNegotiationSavingsDataSaga(),
    fetchRateNegotiationSavingsDataSaga(),
    downloadRateNegotiationSavingsDataSaga(),
    processRateNegotiationSavingsDataSaga()
  ]);
}

export default rateNegotiationSavingsSaga;
