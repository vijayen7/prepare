import { convertJavaNYCDashedDate } from "commons/util";
import { linkFormatter } from "commons/grid/formatters";
import { getTitleCase, getColourCode } from "./../util";

export function rateNegotiationSavingsColumns() {
  var columns = [
    {
      id: "date",
      name: "Date",
      field: "date",
      toolTip: "Date",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDashedDate(value);
      },
      headerCssClass: "b",
      width: 70,
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "inputFileDownload",
      name: "",
      field: "inputFileDownload",
      toolTip: "Download",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (dataContext.inputFileUri) {
          return linkFormatter(
            row,
            cell,
            "<i class='icon-download' />",
            columnDef,
            dataContext
          );
        } else {
          return "<i class='icon-warning' title='Input file upload failed'>";
        }
      },
      headerCssClass: "b",
      width: 5
    },
    {
      id: "inputFileName",
      name: "Input File Name",
      field: "inputFileName",
      toolTip: "Input File Name",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "outputFileDownload",
      name: "",
      field: "outputFileDownload",
      toolTip: "Download",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (dataContext.outputFileName) {
          return linkFormatter(row, cell, "<i class='icon-download' />", columnDef, dataContext);
        } else {
          return "";
        }
      },
      headerCssClass: "b",
      width: 5
    },
    {
      id: "outputFileName",
      name: "Output File Name",
      field: "outputFileName",
      toolTip: "Output File Name",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (dataContext.outputFileName !== undefined && dataContext.outputFileName !== "") {
          return dataContext.outputFileName;
        } else {
          return "N/A";
        }
      },
      headerCssClass: "b"
    },
    {
      id: "user",
      name: "User",
      field: "userName",
      toolTip: "User",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "statusType",
      name: "Calculation Status",
      field: "statusType",
      toolTip: "Calculation Status",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (value === undefined) {
          return "";
        } else {
          return getColourCode(getTitleCase(value));
        }
      },
      headerCssClass: "aln-cntr b"
    },
    {
      id: "savingsAccrued",
      name: "Total Savings Accrued",
      field: "savingsAccrued",
      toolTip: "Total Savings Accrued",
      type: "number",
      filter: true,
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter: dpGrid.Formatters.Float,
      excelFormatter: "#,##0.0"
    }
  ];
  return columns;
}
