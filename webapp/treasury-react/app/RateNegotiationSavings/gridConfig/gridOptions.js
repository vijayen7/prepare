export function rateNegotiationSavingsGridOptions() {
  var options = {
    forceFitColumns: true,
    highlightRowOnClick: true,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    sortList: [{ columnId: "uploadedDate", sortAsc: true }]
  };
  return options;
}
