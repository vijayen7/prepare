export function getTitleCase(value) {
  value = value.toLowerCase().split("_");
  for (var i = 0; i < value.length; i++) {
    value[i] = value[i].charAt(0).toUpperCase() + value[i].slice(1);
  }
  return value.join(" ");
}

export function isInteger(value) {
  // truncate value from both sides
  if (value == null) {
    return false;
  }

  var truncated_value = trim(value);

  if (truncated_value.toString().match(/^\d+$/)) {
    return true;
  }

  return false;
}

export function getColourCode(value) {
  if (value === 'Output File Uploaded') {
    return '<span class="token success size--small">' + value + '</span>';
  } else if (value === 'Calculation Failed'
    || value === 'Unexpected Sheet Name'
    || value === 'No Valid Records' || value === 'Output File Upload Failed' || value == 'Output File Generation Failed') {
    return '<span class="token critical size--small">' + value + '</span>';
  } else {
    return '<span class="token size--small">' + value + '</span>';
  }
}
