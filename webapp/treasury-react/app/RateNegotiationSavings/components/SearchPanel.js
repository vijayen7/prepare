import React from "react";
import { Card } from "arc-react-components";
import { DatePicker } from "arc-react-components";
import FilterButton from "commons/components/FilterButton";

const RateNegotiationSavingsSearchPanel = props => {
  return (
    <Card className="padding">
      <div className="form">
        <div className="row">
          <div className="size--1" />
          <div className="size--content form">
            <div className="row">
              <label title={"maintenanceAccrualDaysLimit"} className="size--content">Maintenance accrual days limit:</label>
              <input
                className="margin--left--small size--content"
                type="number"
                value={props.maintenanceAccrualDaysLimit}
                onChange={e => {
                  var returnValue = {
                    key: "maintenanceAccrualDaysLimit",
                    value: e.target.value
                  };
                  props.onSelect(returnValue);
                }}
                style={{ width: "50px" }}
              />
            </div>
          </div>
          <div className="size--content form margin--left--double">
            <div className="row">
              <label title={"optimizationAccrualDaysLimit"} className="size--content">
                Optimization accrual days limit:
              </label>
              <input
                className="margin--left--small size--content"
                type="number"
                value={props.optimizationAccrualDaysLimit}
                onChange={e => {
                  var returnValue = {
                    key: "optimizationAccrualDaysLimit",
                    value: e.target.value
                  };
                  props.onSelect(returnValue);
                }}
                style={{ width: "50px" }}
              />
            </div>
          </div>
          <div className="size--content form margin--left--double">
            <div className="row">
              <label className="size--content">As of date:</label>
              <DatePicker
                placeholder="Select Date"
                value={props.selectedDate}
                onChange={props.onDatePick}
                className="size--content"
                renderInBody
              />
            </div>
          </div>
          <div className="size--content form">
            <div className="row">
              <FilterButton
                className="button--primary size--content"
                onClick={props.handleSearch}
                reset={false}
                label="Search"
              />
            </div>
          </div>
          <div className="size--1" />
        </div>
      </div>
    </Card >
  );
};

export default RateNegotiationSavingsSearchPanel;
