import React from "react";
import Link from "commons/components/Link";
import { BASE_URL } from "commons/constants";
import { INPUT_TEMPLATE_URL } from "./../constants";

const RateNegotiationSavingsTemplateDownloader = props => {
  return (
    <div>
      Enter a date and search to load results or you can use the options below to upload a new file
      or{" "}
      <Link
        id="downloadTemplate"
        text="download the file template"
        href={BASE_URL + INPUT_TEMPLATE_URL}
      />
    </div>
  );
};

export default RateNegotiationSavingsTemplateDownloader;
