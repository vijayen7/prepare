import { BASE_URL, INPUT_FORMAT_JSON } from "commons/constants";

const queryString = require("query-string");
export let url = "";

export function uploadRateNegotiationSavingsData(payload) {
  url = `${BASE_URL}service/rateNegotiationSavingsFileService/uploadRateNegotiationSavingsInputFile`;
  return fetch(url, {
    method: "POST",
    credentials: "include",
    body: payload
  }).then(data => data.json());
}

export function processRateNegotiationSavingsData(payload) {
  url = `${BASE_URL}service/rateNegotiationSavingsService/calculateAndUploadRateNegotiationSavingsOutputFile?inputFormat=PROPERTIES`;
  return fetch(url, {
    method: "POST",
    credentials: "include",
    body: payload
  });
}

export function fetchRateNegotiationSavingsData(payload) {
  const paramString = queryString.stringify(payload);
  url = `${BASE_URL}service/rateNegotiationSavingsCalculationStatusService/getCalculationStatus?date="${payload.date}"${INPUT_FORMAT_JSON}`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function downloadRateNegotiationSavingsData(payload) {
  url = `${BASE_URL}service/rateNegotiationSavingsFileService/downloadFile?fileName=${payload.fileName}&folderType=${payload.folderType}&format=JSON`;
  window.open(url);
}
