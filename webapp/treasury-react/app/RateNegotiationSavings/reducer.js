import { combineReducers } from "redux";
import {
  FETCH_RATE_NEGOTIATION_SAVINGS_STATUS_DATA,
  RESET_RATE_NEGOTIATION_SAVINGS_SEARCH_DATA
} from "./constants";

function fetchRateNegotiationSavingsStatusDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_RATE_NEGOTIATION_SAVINGS_STATUS_DATA}_SUCCESS`:
      return action.data || [];
    case RESET_RATE_NEGOTIATION_SAVINGS_SEARCH_DATA:
      return [];
  }
  return state;
}

const rootReducer = combineReducers({
  statusData: fetchRateNegotiationSavingsStatusDataReducer
});

export default rootReducer;
