import React, { Component } from "react";
import { hot } from "react-hot-loader/root";
import Loader from "commons/container/Loader";
import { Layout } from "arc-react-components";
import RateNegotitationSavingsGrid from "./containers/Grid";
import RateNegotiationSavingsController from "./containers/Controller";
import AppHeader from 'arc-header';

class RateNegotiationSavings extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
        <arc-header
            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          />
          <Layout isRowType>
            <Layout.Child size="Fit" className="padding--double">
              <RateNegotiationSavingsController />
            </Layout.Child>
            <Layout.Child size="Fit" className="padding--double">
              <RateNegotitationSavingsGrid />
            </Layout.Child>
          </Layout>
        </div>
      </React.Fragment >
    );
  }
}
export default hot(RateNegotiationSavings);
