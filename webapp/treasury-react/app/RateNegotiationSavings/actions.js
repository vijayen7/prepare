import {
  UPLOAD_RATE_NEGOTIATION_SAVINGS_DATA,
  FETCH_RATE_NEGOTIATION_SAVINGS_STATUS_DATA,
  RESET_RATE_NEGOTIATION_SAVINGS_SEARCH_DATA,
  DOWNLOAD_RATE_NEGOTIATION_SAVINGS_DATA
} from "./constants";

export function uploadRateNegotiationSavingsData(
  uploadData,
  searchData,
  accrualDaysLimitData
) {
  return {
    type: UPLOAD_RATE_NEGOTIATION_SAVINGS_DATA,
    uploadData,
    searchData,
    accrualDaysLimitData
  };
}

export function fetchRateNegotiationSavingsData(searchPayload) {
  return {
    type: FETCH_RATE_NEGOTIATION_SAVINGS_STATUS_DATA,
    searchPayload
  };
}

export function resetRateNegotiationSavingsSearchData() {
  return {
    type: RESET_RATE_NEGOTIATION_SAVINGS_SEARCH_DATA
  };
}

export function downloadRateNegotiationSavingsData(payload) {
  return {
    type: DOWNLOAD_RATE_NEGOTIATION_SAVINGS_DATA,
    payload
  };
}
