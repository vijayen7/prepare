import React, { Component } from "react";
import { connect } from "react-redux";
import Grid from "commons/components/GridWithCellClick";
import { bindActionCreators } from "redux";
import { rateNegotiationSavingsColumns } from "./../gridConfig/rateNegotiationSavingsColumnConfig";
import { rateNegotiationSavingsGridOptions } from "./../gridConfig/gridOptions";
import {
  downloadRateNegotiationSavingsData
} from "./../actions";
import { INPUT, OUTPUT } from "./../constants";

class RateNegotitationSavingsGrid extends Component {
  constructor(props) {
    super(props);
  }

  onCellClickHandler = args => {
    if (args.colId === "inputFileDownload" || args.colId === "outputFileDownload") {
      if (this.props.statusData !== null) {
        let fileName =
          args.colId === "inputFileDownload" ? args.item.inputFileName : args.item.outputFileName;
        let folderType = args.colId === "inputFileDownload" ? INPUT : OUTPUT;
        let payload = {
          fileName: fileName,
          folderType: folderType
        };
        this.props.downloadRateNegotiationSavingsData(payload);
      }
    }
  };

  renderGrid = () => {
    return (
      <React.Fragment>
        {
          this.props.statusData.length !== 0 ?
            <Grid
              data={this.props.statusData}
              gridId="rateNegotiationSavingsGrid"
              gridColumns={rateNegotiationSavingsColumns()}
              gridOptions={rateNegotiationSavingsGridOptions()}
              fill={true}
              onCellClick={this.onCellClickHandler}
            /> : null
        }
      </React.Fragment>
    );
  };

  render() {
    return <React.Fragment>{this.renderGrid()}</React.Fragment>;
  }
}

function mapStateToProps(state) {
  return {
    statusData: state.rateNegotiationSavings.statusData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      downloadRateNegotiationSavingsData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RateNegotitationSavingsGrid);
