import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ALLOWED_FILE_TYPES } from "./../constants";
import {
  uploadRateNegotiationSavingsData,
  fetchRateNegotiationSavingsData,
  resetRateNegotiationSavingsSearchData
} from "./../actions";
import { getCurrentDate } from "./../../commons/util";
import { FILE } from "./../constants";
import RateNegotiationSavingsFileUploader from "./../components/FileUploader";
import RateNegotiationSavingsSearchPanel from "../components/SearchPanel";
import RateNegotiationSavingsTemplateDownloader from "../components/TemplateDownloader";
import { Layout } from "arc-react-components";
import { isInteger } from "./../util";
import { ToastService } from 'arc-react-components';

class RateNegotiationSavingsController extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters = () => {
    return {
      selectedDate: getCurrentDate(),
      files: { accepted: [], rejected: [], rejectedReasons: new Map() },
      maintenanceAccrualDaysLimit: 2,
      optimizationAccrualDaysLimit: 90
    };
  }

  handleSearch = () => {
    this.props.resetRateNegotiationSavingsSearchData();
    let payload = {
      date: this.state.selectedDate
    };
    this.props.fetchRateNegotiationSavingsData(payload);
  }

  clearFileArray = () => {
    this.setState({
      files: { accepted: [], rejected: [], rejectedReasons: new Map() }
    });
  }

  onFileUpload = files => {
    this.setState({ files: files });
    this.handleUpload(files);
  }

  getUploadedFileName = () => {
    return this.state.files.accepted[this.state.files.accepted.length - 1] ===
      undefined
      ? undefined
      : this.state.files.accepted[this.state.files.accepted.length - 1].name;
  }

  onSelect = (params) => {
    const { key, value } = params;
    let newState = { ...this.state };
    newState[key] = value;
    this.setState(newState);
  }

  getRateNegotiationSavingsAccrualDaysLimit = () => {
    let rateNegotiationSavingsAccrualDaysLimit = {}
    if (isInteger(this.state.maintenanceAccrualDaysLimit) && isInteger(this.state.optimizationAccrualDaysLimit)) {
      if (this.state.maintenanceAccrualDaysLimit == 0 || this.state.optimizationAccrualDaysLimit == 0) {
        ToastService.append({
          content: `The accrual days limit should be greater than zero`,
          type: ToastService.ToastType.CRITICAL,
          placement: ToastService.Placement.TOP_RIGHT,
          dismissTime: 8000
        });
      } else if (this.state.maintenanceAccrualDaysLimit > 20 || this.state.optimizationAccrualDaysLimit > 365) {
        ToastService.append({
          content: `The maximum accrual days limit for maintenance is 20 and for optimization is 365`,
          type: ToastService.ToastType.CRITICAL,
          placement: ToastService.Placement.TOP_RIGHT,
          dismissTime: 8000
        });
      } else {
        rateNegotiationSavingsAccrualDaysLimit = {
          "@CLASS": "com.arcesium.treasury.model.seclend.negotiation.savings.RateNegotiationSavingsAccrualDaysLimit",
          "maintenanceAccrualDaysLimit": this.state.maintenanceAccrualDaysLimit,
          "optimizationAccrualDaysLimit": this.state.optimizationAccrualDaysLimit
        };
      }
      return rateNegotiationSavingsAccrualDaysLimit;
    }

    ToastService.append({
      content: `The accrual days limit should be a positive number.`,
      type: ToastService.ToastType.CRITICAL,
      placement: ToastService.Placement.TOP_RIGHT,
      dismissTime: 8000
    });
    return rateNegotiationSavingsAccrualDaysLimit;
  }

  handleUpload = files => {
    this.props.resetRateNegotiationSavingsSearchData();
    let accrualDaysLimitData = this.getRateNegotiationSavingsAccrualDaysLimit();

    if (_.isEmpty(accrualDaysLimitData)) {
      return;
    }

    const uploadData = new FormData();

    if (files.accepted.length > 0) {
      let file = files.accepted[files.accepted.length - 1];
      let searchPayload = {
        date: getCurrentDate()
      };
      if (file.name.lastIndexOf(ALLOWED_FILE_TYPES) == -1) {
        ToastService.append({
          content: `Please check if file format and name are proper.`,
          type: ToastService.ToastType.CRITICAL,
          placement: ToastService.Placement.TOP_RIGHT,
          dismissTime: 8000
        });
        return;
      }
      uploadData.append(FILE, file);
      this.props.uploadRateNegotiationSavingsData(
        uploadData,
        searchPayload,
        accrualDaysLimitData
      );
    } else {
      ToastService.append({
        content: `Please check if the format of the file is excel.`,
        type: ToastService.ToastType.CRITICAL,
        placement: ToastService.Placement.TOP_RIGHT,
        dismissTime: 8000
      });
    }

    this.clearFileArray();
  }

  onDatePick = date => {
    this.setState({
      selectedDate: date
    });
  }

  getRateNegotiationSavingsSearchPanel = () => {
    return (
      <Layout.Child className="text-align--center padding--double" size="Fit">
        <RateNegotiationSavingsSearchPanel
          onDatePick={this.onDatePick}
          selectedDate={this.state.selectedDate}
          handleSearch={this.handleSearch}
          onSelect={this.onSelect}
          maintenanceAccrualDaysLimit={this.state.maintenanceAccrualDaysLimit}
          optimizationAccrualDaysLimit={this.state.optimizationAccrualDaysLimit}
        />
      </Layout.Child>
    );
  }

  getRateNegotiationSavingsTemplateDownloader = () => {
    return (
      <Layout.Child className="text-align--center padding--double" size="Fit">
        <RateNegotiationSavingsTemplateDownloader />
      </Layout.Child>
    );
  }

  getRateNegotiationSavingsFileUploader = () => {
    let uploadedFileName = this.getUploadedFileName();

    return (
      <Layout.Child className="text-align--center padding--top">
        <RateNegotiationSavingsFileUploader
          style={{ width: "45%" }}
          fileName={uploadedFileName}
          files={this.state.files}
          allowedFileTypes={ALLOWED_FILE_TYPES}
          onFileUpload={this.onFileUpload}
        />
      </Layout.Child>
    );
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          {this.getRateNegotiationSavingsSearchPanel()}
          {this.getRateNegotiationSavingsTemplateDownloader()}
          {this.getRateNegotiationSavingsFileUploader()}
        </Layout>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    statusData: state.rateNegotiationSavings.statusData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      uploadRateNegotiationSavingsData,
      fetchRateNegotiationSavingsData,
      resetRateNegotiationSavingsSearchData
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RateNegotiationSavingsController);
