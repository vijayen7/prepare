import { BASE_URL } from "commons/constants";
import { ArcFetch } from "arc-commons";
import { getCommaSeparatedValues } from "commons/util";
import { SearchFilter } from "./models/SearchFilter";

const QUERY_ARGS = {
  credentials: "include",
  method: "GET"
};

export const getBrokerCashBalanceDataList = (filters: SearchFilter) => {
  const url = getBrokerCashBalanceURL(filters);
  return ArcFetch.getJSON(url,QUERY_ARGS);
};

export const getBrokerCashBalanceURL = (filters: SearchFilter) => {
  let selectedLegalEntities = getCommaSeparatedValues(filters.selectedLegalEntities) == "-1" ? "__null__" : getCommaSeparatedValues(filters.selectedLegalEntities);
  let selectedAgreementTypes = getCommaSeparatedValues(filters.selectedAgreementTypes) == "-1" ? "__null__" : getCommaSeparatedValues(filters.selectedAgreementTypes);
  let selectedCurrencies = getCommaSeparatedValues(filters.selectedCurrencies) == "-1" ? "__null__" : getCommaSeparatedValues(filters.selectedCurrencies);
  let selectedCustodianAccounts = getCommaSeparatedValues(filters.selectedCustodianAccounts) == "-1" ? "__null__" : getCommaSeparatedValues(filters.selectedCustodianAccounts);
  let selectedCpes = getCommaSeparatedValues(filters.selectedCpes) == "-1" ? "__null__" : getCommaSeparatedValues(filters.selectedCpes);

  const url = `${BASE_URL}service/brokerCashBalanceService/getBrokerCashBalance?brokerCashBalanceFilter.startDate=${filters.startDate}&brokerCashBalanceFilter.endDate=${filters.endDate}&brokerCashBalanceFilter.legalEntityIds=${selectedLegalEntities}&brokerCashBalanceFilter.agreementTypeIds=${selectedAgreementTypes}&brokerCashBalanceFilter.ccySpns=${selectedCurrencies}&brokerCashBalanceFilter.caIds=${selectedCustodianAccounts}&brokerCashBalanceFilter.cpeIds=${selectedCpes}&inputFormat=PROPERTIES&format=json`;

  return url;
};
