import BrokerCashBalanceStore from "./store";
import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../commons/components/ReactLoader";
import Loader from "../commons/container/Loader";
import AppHeader, { whenHeaderRef } from "arc-header";
import BrokerCashBalanceGrid from "./container/Grid";
import BrokerCashBalanceSideBar from "./container/SideBar";
import { Layout, Breadcrumbs } from "arc-react-components";
import { extendObservable } from "mobx";
import { useLocalStore } from "mobx-react-lite";

const dummyObservable = Symbol("dummy-observable");

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item
        link="/treasury/financing/currencyBalanceReport.html"
        key="currencyBalanceReport"
      >
        Currency Balance Report
      </Breadcrumbs.Item>
    </>
  );
};

const BrokerCashBalance: React.FC = () => {
  useEffect(() => {
    updateBreadCrumbs();
  }, []);
  const brokerCashBalanceStore = useLocalStore(() =>
    extendObservable(new BrokerCashBalanceStore(), {
      [dummyObservable]: true,
    })
  );

  const openReportManager = () => {
    let url = brokerCashBalanceStore.generateReportFromAPI();
    window.open(url);
  };

  return (
    <>
      <Loader />
      <ReactLoader
        inProgress={brokerCashBalanceStore.sideBarStore.searchStatus.inProgress}
      />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} />
        <Layout isColumnType={true} style={{ height: "100%" }}>
          <Layout.Child
            size={2}
            key="child1"
            title="Search Criteria"
            collapsible
            showHeader
          >
            <BrokerCashBalanceSideBar store={brokerCashBalanceStore} />
          </Layout.Child>
          <Layout.Divider isResizable key="child2" />
          <Layout.Child size={8} key="child3">
            <Layout>
              <Layout.Child size="fit">
                <div style={{ textAlign: "right" }}>
                  <button onClick={openReportManager}>Create Report</button>
                </div>
              </Layout.Child>
              <Layout.Child>
                <BrokerCashBalanceGrid store={brokerCashBalanceStore} />
              </Layout.Child>
            </Layout>
          </Layout.Child>
        </Layout>
      </div>
    </>
  );
};

export default observer(BrokerCashBalance);
