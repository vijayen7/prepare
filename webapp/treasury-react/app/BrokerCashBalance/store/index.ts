import { observable, action, flow } from "mobx";
import {
  getBrokerCashBalanceDataList,
  getBrokerCashBalanceURL
} from "../api";
import SideBarStore from "./SideBarStore";

export class BrokerCashBalanceStore {
  // Store initializations
  sideBarStore: SideBarStore = new SideBarStore();

  // Observable Variables
  @observable brokerCashBalanceDataList = [];

  // Action Methods
  @action.bound onSelect({ key, value }: any) {
    const newFilters = Object.assign({}, this.sideBarStore.filters);
    newFilters[key] = value;
    this.sideBarStore.setFilters(newFilters);
  }

  @action.bound handleSearch() {
    let searchStatus = { firstSearch: false, inProgress: true, error: false };
    this.sideBarStore.setSearchStatus(searchStatus);
    this.getBrokerCashBalanceData();
  }

  // Method to fetch data from API
  getBrokerCashBalanceData = flow(
    function* getBrokerCashBalanceData(this: BrokerCashBalanceStore) {
      try {
        this.brokerCashBalanceDataList = yield getBrokerCashBalanceDataList(
          this.sideBarStore.filters
        );
        this.sideBarStore.searchStatus.inProgress = false;
      } catch (e) {
        alert("Error occurred while getting Data From API");
        this.sideBarStore.searchStatus = {
          firstSearch: false,
          inProgress: false,
          error: true,
        };
      }
    }.bind(this)
  );

  @action generateReportFromAPI() {
    let paramsUrlString = "";
    let apiUrl = getBrokerCashBalanceURL(this.sideBarStore.filters);
    let baseUrl = "/report-manager/editor.html";
    apiUrl = "https://" + window.location.hostname + apiUrl;
    try {
      paramsUrlString += "?url=" + encodeURIComponent(apiUrl);
    } catch (e) {
      console.log("Error while encoding: " + apiUrl);
    }
    baseUrl += paramsUrlString;
    return baseUrl;
  }
}

export default BrokerCashBalanceStore;
