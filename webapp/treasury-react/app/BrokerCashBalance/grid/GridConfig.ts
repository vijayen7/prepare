declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

export function getGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    frozenColumn: 3,
    highlightRowOnClick: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    sortList: [
      { columnId: "date", sortAsc: true },
      { columnId: "cpeName", sortAsc: true },
      { columnId: "legalEntityName", sortAsc: true },
      { columnId: "agreementTypeAbbrev", sortAsc: true }
    ],
    enableMultilevelGrouping: {
      hideGroupingHeader: false,
      showGroupingKeyInColumn: false,
      showGroupingTotals: true,
      customGroupingRendering: true,
      initialGrouping: ["agreementTypeAbbrev", "legalEntityName", "cpeName"]
    },
    customColumnSelection: {
      defaultcolumns: getDefaultColumns(),
      totalColumns: getTotalColumns()
    },
    // groupColumns :[ ['Arc Balances', 'smv', 'accrualRate'], ['Broker Balances', 'brokerSmv', 'brokerAccrualRate'], ['Broker Projected Balances', 'tPlusOneBrokerAccrualBalance', 'tPlusThreeBrokerAccrualBalance'] ]
  };
  return options;
}



function getDefaultColumns() {
  let defaultColumns = [
    "date",
    "cpeName",
    "legalEntityName",
    "agreementTypeAbbrev",
    "custodianAccountName",
    "currencyAbbrev",
    "smv",
    "cash",
    "tradeDateCash",
    "accrualBalance",
    "accrualAmount",
    "accrualRate",
    "cashEquivalent",
    "fxRate",
    "brokerSmv",
    "brokerCash",
    "brokerTradeDateCashBalance",
    "brokerAccrualBalance",
    "brokerAccrualAmount",
    "brokerAccrualRate"
  ];

  let clientName = CODEX_PROPERTIES["codex.client_name"];
  if (clientName === "tbird") {
    defaultColumns = defaultColumns.concat([
    "tPlusOneBrokerAccrualBalance",
    "tPlusTwoBrokerAccrualBalance",
    "tPlusThreeBrokerAccrualBalance"
    ])
  }

  return defaultColumns;
}

function getTotalColumns() {
  let totalColumns = [
    "date",
    "cpeName",
    "legalEntityName",
    "agreementTypeAbbrev",
    "custodianAccountName",
    "currencyAbbrev",
    "smv",
    "cash",
    "tradeDateCash",
    "accrualBalance",
    "accrualAmount",
    "accrualRate",
    "cashEquivalent",
    "fxRate",
    "brokerSmv",
    "brokerCash",
    "brokerTradeDateCashBalance",
    "brokerAccrualBalance",
    "brokerAccrualAmount",
    "brokerAccrualRate",
    "brokerSmvUSD",
    "brokerCashUSD",
    "brokerAccrualBalanceUSD",
    "brokerAccrualAmountUSD",
    "smvUSD",
    "cashUSD",
    "accrualBalanceUSD",
    "accrualAmountUSD",
    "tradeDateCashUSD",
    "brokerTradeDateCashBalanceUSD",
    "cashEquivalentUSD"
  ];

  let clientName = CODEX_PROPERTIES["codex.client_name"];
  if (clientName === "tbird") {
    totalColumns = totalColumns.concat([
      "tPlusOneBrokerAccrualBalance",
      "tPlusTwoBrokerAccrualBalance",
      "tPlusThreeBrokerAccrualBalance",
      "tPlusOneBrokerAccrualBalanceUSD",
      "tPlusTwoBrokerAccrualBalanceUSD",
      "tPlusThreeBrokerAccrualBalanceUSD",
    ])
  }

  return totalColumns;
}
