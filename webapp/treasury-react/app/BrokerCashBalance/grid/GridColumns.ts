import {
  convertJavaNYCDashedDate
} from "commons/util";
import { groupFloatFormatter } from "commons/grid/formatters";

export default function gridColumns() {
  var columns = [
    {
      id: "date",
      name: "Date",
      field: "date",
      type: "text",
      sortable: true,
      headerCssClass: "b",
      formatter: function (row, cell, value, columnDef, dataContext, isExportToExcel) {
        return convertJavaNYCDashedDate(value);
      },
      excelDataFormatter: function (value) {
        return convertJavaNYCDashedDate(value);
      },
      filter: {
        isFilterOnFormattedValue: true
      }
    },
    {
      id: "cpeName",
      name: "Counterparty Entity",
      field: "cpeName",
      toolTip: "Counterparty Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "legalEntityName",
      name: "Legal Entity",
      field: "legalEntityName",
      toolTip: "Legal Entity",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "agreementTypeAbbrev",
      name: "Agreement Type",
      field: "agreementTypeAbbrev",
      toolTip: "Agreement Type",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 80
    },
    {
      id: "custodianAccountName",
      name: "Custodian Account",
      field: "custodianAccountName",
      toolTip: "Custodian Account",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b"
    },
    {
      id: "currencyAbbrev",
      name: "Currency",
      field: "currencyAbbrev",
      toolTip: "Currency",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "b",
      width: 70
    },
    {
      id: "smv",
      name: "Settled SMV",
      field: "smv",
      toolTip: "Settled SMV",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "cash",
      name: "Settled Cash Balance",
      field: "cash",
      toolTip: "Settled Cash Balance",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "tradeDateCash",
      name: "Trade Date Cash Balance",
      field: "tradeDateCash",
      toolTip: "Trade Date Cash Balance",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "accrualBalance",
      name: "Net Debit",
      field: "accrualBalance",
      toolTip: "Net Debit",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "accrualAmount",
      name: "Debit Accrual",
      field: "accrualAmount",
      toolTip: "Debit Accrual",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "accrualRate",
      name: "Accrual Rate",
      field: "accrualRate",
      toolTip: "Accrual Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(row, cell, value, columnDef, dataContext);
        else return null;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0"
    },
    {
      id: "cashEquivalent",
      name: "Cash Equivalent",
      field: "cashEquivalent",
      toolTip: "Cash Equivalent",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "fxRate",
      name: "FX Rate",
      field: "fxRate",
      toolTip: "FX Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(row, cell, value, columnDef, dataContext);
        else return null;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
    },
    {
      id: "brokerSmv",
      name: "Broker Settled SMV",
      field: "brokerSmv",
      toolTip: "Broker Settled SMV",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerTradeDateCashBalance",
      name: "Broker Trade Date Cash Balance",
      field: "brokerTradeDateCashBalance",
      toolTip: "Broker Trade Date Cash Balance",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerCash",
      name: "Broker Settled Cash Balance",
      field: "brokerCash",
      toolTip: "Broker Settled Cash Balance",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerAccrualBalance",
      name: "Broker Net Debit",
      field: "brokerAccrualBalance",
      toolTip: "Broker Net Debit",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerAccrualAmount",
      name: "Broker Debit Accrual",
      field: "brokerAccrualAmount",
      toolTip: "Broker Debit Accrual",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerAccrualRate",
      name: "Broker Accrual Rate",
      field: "brokerAccrualRate",
      toolTip: "Broker Accrual Rate",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(row, cell, value, columnDef, dataContext);
        else return null;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0"
    },
    {
      id: "tPlusOneBrokerAccrualBalance",
      name: "Broker Settled Cash Balance (Date+1)",
      field: "tPlusOneBrokerAccrualBalance",
      toolTip: "Broker Settled Cash Balance (Date+1)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "tPlusTwoBrokerAccrualBalance",
      name: "Broker Settled Cash Balance (Date+2)",
      field: "tPlusTwoBrokerAccrualBalance",
      toolTip: "Broker Settled Cash Balance (Date+2)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "tPlusThreeBrokerAccrualBalance",
      name: "Broker Settled Cash Balance (Date+3)",
      field: "tPlusThreeBrokerAccrualBalance",
      toolTip: "Broker Settled Cash Balance (Date+3)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "tPlusOneBrokerAccrualBalanceUSD",
      name: "Broker Settled Cash Balance (USD) (Date+1)",
      field: "tPlusOneBrokerAccrualBalanceUSD",
      toolTip: "Broker Settled Cash Balance (USD) (Date+1)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "tPlusTwoBrokerAccrualBalanceUSD",
      name: "Broker Settled Cash Balance (USD) (Date+2)",
      field: "tPlusTwoBrokerAccrualBalanceUSD",
      toolTip: "Broker Settled Cash Balance (USD) (Date+2)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "tPlusThreeBrokerAccrualBalanceUSD",
      name: "Broker Settled Cash Balance (USD) (Date+3)",
      field: "tPlusThreeBrokerAccrualBalanceUSD",
      toolTip: "Broker Settled Cash Balance (USD) (Date+3)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerSmvUSD",
      name: "Broker Settled SMV (USD)",
      field: "brokerSmvUSD",
      toolTip: "Broker Settled SMV (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerCashUSD",
      name: "Broker Settled Cash Balance (USD)",
      field: "brokerCashUSD",
      toolTip: "Broker Settled Cash Balance (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerAccrualBalanceUSD",
      name: "Broker Net Debit (USD)",
      field: "brokerAccrualBalanceUSD",
      toolTip: "Broker Net Debit (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerAccrualAmountUSD",
      name: "Broker Debit Accrual (USD)",
      field: "brokerAccrualAmountUSD",
      toolTip: "Broker Debit Accrual (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "smvUSD",
      name: "Settled SMV (USD)",
      field: "smvUSD",
      toolTip: "Settled SMV (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "cashUSD",
      name: "Settled Cash (USD)",
      field: "cashUSD",
      toolTip: "Settled Cash (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "accrualBalanceUSD",
      name: "Net Debit (USD)",
      field: "accrualBalanceUSD",
      toolTip: "Net Debit (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "accrualAmountUSD",
      name: "Debit Accrual (USD)",
      field: "accrualAmountUSD",
      toolTip: "Debit Accrual (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "cashEquivalentUSD",
      name: "Cash Equivalent (USD)",
      field: "cashEquivalentUSD",
      toolTip: "Cash Equivalent (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "brokerTradeDateCashBalanceUSD",
      name: "Broker Trade Date Cash Balance (USD)",
      field: "brokerTradeDateCashBalanceUSD",
      toolTip: "Broker Trade Date Cash Balance (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    },
    {
      id: "tradeDateCashUSD",
      name: "Trade Date Cash Balance (USD)",
      field: "tradeDateCashUSD",
      toolTip: "Trade Date Cash Balance (USD)",
      type: "number",
      filter: true,
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: groupFloatFormatter
    }
  ];

  return columns;
}


export function formatterFourDecimal(row, cell, value, columnDef, dataContext) {
  return (
    "<div class='aln-rt'>" +
    (value == null
      ? "<span class='message'>n/a</span>"
      : Number(value).toFixed(4)) +
    "</div>"
  );
}
