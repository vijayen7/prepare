import BrokerCashBalanceStore from "../store";

export interface BrokerCashBalanceProps {
  store: BrokerCashBalanceStore,
};
