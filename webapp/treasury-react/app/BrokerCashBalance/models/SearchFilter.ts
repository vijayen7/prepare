import ReferenceData from "./../../commons/models/ReferenceData";

export interface SearchFilter {
  startDate: string;
  endDate: string;
  selectedLegalEntities?: Array<ReferenceData>;
  selectedAgreementTypes?: Array<ReferenceData>;
  selectedCurrencies?: Array<ReferenceData>;
  selectedCustodianAccounts?: Array<ReferenceData>;
  selectedCpes?: Array<ReferenceData>;
}

export interface SearchStatus {
  firstSearch: boolean;
  inProgress: boolean;
  error: boolean;
}
