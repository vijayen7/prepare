import React from "react";
import { Sidebar, DateRangePicker } from "arc-react-components";
import { observer } from "mobx-react";
import ColumnLayout from "../../commons/components/ColumnLayout";
import FilterButton from "../../commons/components/FilterButton";
import { BrokerCashBalanceProps } from "../models/BrokerCashBalanceProps";
import SaveSettingsManager from "commons/components/SaveSettingsManager";
import { Card } from "arc-react-components";
import CustodianAccountFilter from "commons/container/CustodianAccountFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";

const BrokerCashBalanceSideBar: React.FC<BrokerCashBalanceProps> = (
  props: BrokerCashBalanceProps
) => {

  let filters = (
    <Card>
      <DateRangePicker
        placeholder="Select Date"
        value={[
          props.store.sideBarStore.filters.startDate,
          props.store.sideBarStore.filters.endDate,
        ]}
        onChange={(date) => {
          props.store.sideBarStore.filters.startDate = date[0];
          props.store.sideBarStore.filters.endDate = date[1];
        }}
      />
      <AgreementTypeFilter
        onSelect={props.store.onSelect}
        selectedData={
          props.store.sideBarStore.filters.selectedAgreementTypes
        }
        multiSelect={true}
      />
      <LegalEntityFilter
        onSelect={props.store.onSelect}
        selectedData={
          props.store.sideBarStore.filters.selectedLegalEntities
        }
        multiSelect={true}
      />
      <CpeFilter
        onSelect={props.store.onSelect}
        selectedData={
          props.store.sideBarStore.filters.selectedCpes
        }
        multiSelect={true}
      />
      <CurrencyFilter
        onSelect={props.store.onSelect}
        selectedData={
          props.store.sideBarStore.filters.selectedCurrencies
        }
        multiSelect={true}
      />
      <CustodianAccountFilter
        onSelect={props.store.onSelect}
        selectedData={
          props.store.sideBarStore.filters.selectedCustodianAccounts
        }
        multiSelect={true}
      />
    </Card>
  );

  let searchButtons = (
    <ColumnLayout>
      <FilterButton onClick={props.store.handleSearch} label="Search" />
      <FilterButton
        onClick={props.store.sideBarStore.handleReset}
        reset
        label="Reset"
      />
    </ColumnLayout>
  );

  let saveSettings = (
    <Card>
      <SaveSettingsManager
        selectedFilters={props.store.sideBarStore.filters}
        applySavedFilters={props.store.sideBarStore.applySavedFilters}
        applicationName="BrokerCashBalance"
      />
    </Card>
  );

  return (
    <Sidebar header={false} footer={searchButtons}>
      {saveSettings}
      {filters}
    </Sidebar>
  );
};

export default observer(BrokerCashBalanceSideBar);
