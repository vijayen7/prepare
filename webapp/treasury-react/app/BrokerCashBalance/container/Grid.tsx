import React from "react";
import { observer } from "mobx-react";
import Message from "../../commons/components/Message";
import GridWithCellClick from "commons/components/GridWithCellClick";
import _ from "lodash";
import { BrokerCashBalanceProps } from "../models/BrokerCashBalanceProps";
import { Layout } from "arc-react-components";
import { getGridOptions } from "../grid/GridConfig";
import gridColumns from "../grid/GridColumns";
import { Panel } from "arc-react-components";

const BrokerCashBalanceGrid: React.FC<BrokerCashBalanceProps> = (
  props: BrokerCashBalanceProps
) => {
  const renderGridData = () => {
    let cashBalanceData = props.store.brokerCashBalanceDataList;

    if (props.store.sideBarStore.searchStatus.error) {
      return <Message messageData="Error occurred while loading data." />;
    }

    if (!cashBalanceData.length) {
      let noDataMessage = "Please search to load data";
      if (props.store.sideBarStore.searchStatus.firstSearch == false) {
        noDataMessage = "No data available for search criteria";
      }
      return <Message messageData={noDataMessage} />;
    }

    let grid = (
      <Layout
        isColumnType={false}
        className="border"
        style={{ height: "100%" }}
      >
        <Layout.Child
          size={1}
          title="Currency Balance Report"
          key="gridChild1"
          collapsible
        >
          <Panel title="Currency Balance Report">
            <GridWithCellClick
              gridColumns={gridColumns()}
              onCellClick={() => {}}
              gridId="brokerCashBalanceGrid"
              gridOptions={getGridOptions()}
              data={cashBalanceData}
              fill={true}
            />
          </Panel>
        </Layout.Child>
      </Layout>
    );

    return grid;
  };

  return <>{renderGridData()}</>;
};

export default observer(BrokerCashBalanceGrid);
