import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import SideBar from "../commons/SideBar";
import MainContainer from "./containers/MainContainer";
export default class AssetsDetail extends Component {

  render() {
    return (
      <React.Fragment>
        <div className="layout--flex">
          <div className="size--content">
            <SideBar disabled = {this.props.view !== "roaHome"} />
          </div>
          <div className="size--5 padding--horizontal--double">
            <MainContainer handleViewToggle = {this.props.handleViewToggle} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

AssetsDetail.propTypes = {
  handleViewToggle: PropTypes.func,
  view: PropTypes.string,
  assetsDetailData: PropTypes.array,
  selectedFilters: PropTypes.array
};
