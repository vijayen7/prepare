import {
  getDefaultColumns,
  getTotalColumns
} from "./columnSelector";


export function assetsGridOptions() {
    var groupColumns = ['agreementDisplayName'];
    var options = {
      autoHorizontalScrollBar: true,
      applyFilteringOnGrid: true,
      showHeaderRow: true,
      exportToExcel: true,
      sortList: [{ columnId: "agreementDisplayName", sortAsc: true }],
      highlightRowOnClick: true,
      enableMultilevelGrouping: {
          hideGroupingHeader: false,
          showGroupingKeyInColumn: false,
          initialGrouping: groupColumns,
        },
        customColumnSelection: {
          defaultcolumns: getDefaultColumns(),
          totalColumns: getTotalColumns()
        },
      expandCollapseAll: true,
      useAvailableScreenSpace : true,
    }

  return options;
}
