import {
  linkFormatter,
  cashReportSumGroupFormatter
} from "commons/grid/formatters";

import { roaAggregator } from "commons/grid/aggregators";
export function assetsGridColumns() {
  var columns = [{
        id: "agreementDisplayName",
        name: "Agreement",
        field: "agreementDisplayName",
        toolTip: "agreementDisplayName",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 110
      }, {
        id: "agreementTypeName",
        name: "Agreement Type",
        field: "agreementTypeName",
        toolTip: "Agreement Type",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 110
      }, {
        id: "currency",
        name: "Currency",
        field: "currency",
        toolTip: "Currency",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 50
      }, {
        id: "viewCalcRules",
        name: "View Calc Rules",
        field: "viewCalcRules",
        toolTip: "View calculation rules",
        type: "text",
        filter: true,
        sortable: true,
        formatter: function(row, cell, value, columnDef, dataContext) {
            return linkFormatter(
              row,
              cell,
              "View Calc Rules",
              columnDef,
              dataContext
            );
        },
        headerCssClass: "aln-rt b",
        width: 70
      }, {
        id: "lmvUsd",
        name: "LMV",
        field: "lmvUsd",
        toolTip: "Long Market Value",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 70
      }, {
        id: "onShoreLmvInternalizationPct",
        name: "Onshore Longs Internalization Factor",
        field: "onShoreAssetsKnob",
        toolTip: "Onshore Longs Internalization Factor",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.onShoreAssetsKnob.lmvInternalizationPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.lmvInternalizationPct;
        },
        width: 70
      }, {
        id: "offShoreLmvInternalizationPct",
        name: "Offshore Longs Internalization Factor",
        field: "offShoreAssetsKnob",
        toolTip: "Offshore Longs Internalization Factor",
        type: "text",
        type: "text",
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.offShoreAssetsKnob.lmvInternalizationPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.lmvInternalizationPct;
        },
        width: 70
      }, {
        id: "onShoreLongsBenefitPct",
        name: "Onshore Longs Benefit",
        field: "onShoreAssetsKnob",
        toolTip: "Onshore Longs Benefit",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.onShoreAssetsKnob.lmvBenefitPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.lmvBenefitPct;
        },
        width: 70
      }, {
        id: "offShoreLongsBenefitPct",
        name: "Offshore Longs Benefit",
        field: "offShoreAssetsKnob",
        toolTip: "Offshore Longs Benefit",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.offShoreAssetsKnob.lmvBenefitPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.lmvBenefitPct;
        },
        width: 70
      }, {
        id: "arrangingEntityBalancePct",
        name: "Arranging LMV Share",
        field: "onShoreAssetsKnob",
        toolTip: "Arranging LMV Share",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.onShoreAssetsKnob.arrangingEntityBalancePct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.arrangingEntityBalancePct;
        },
        width: 70
      },{
        id: "onShoreHTBborrowSpreadInternalizationPct",
        name: "Onshore HTB Borrow Spread Internalization Factor",
        field: "onShoreAssetsKnob",
        toolTip: "Onshore HTB Borrow Spread Internalization Factor",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.onShoreAssetsKnob.htbBorrowSpreadInternalizationPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.htbBorrowSpreadInternalizationPct;
        },
        width: 90
      },  {
        id: "onShoreGCborrowSpreadInternalizationPct",
        name: "Onshore GC Borrow Spread Internalization Factor",
        field: "onShoreAssetsKnob",
        toolTip: "Onshore GC Borrow Spread Internalization Factor",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.onShoreAssetsKnob.gcBorrowSpreadInternalizationPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.gcBorrowSpreadInternalizationPct;
        },
        width: 90
      },{
        id: "smvUsd",
        name: "SMV",
        field: "smvUsd",
        toolTip: "Short Market Value",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }, {
        id: "offshorHTBborrowSpreadInternalizationPct",
        name: "Offshore HTB Borrow Spread Internalization Factor",
        field: "offShoreAssetsKnob",
        toolTip: "Offshore HTB Borrow Spread Internalization Factor",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.offShoreAssetsKnob.htbBorrowSpreadInternalizationPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.htbBorrowSpreadInternalizationPct;
        },
        width: 90
      },  {
        id: "offshoreGCborrowSpreadInternalizationPct",
        name: "Offshore GC Borrow Spread Internalization Factor",
        field: "offShoreAssetsKnob",
        toolTip: "Offshore GC Borrow Spread Internalization Factor",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.offShoreAssetsKnob.gcBorrowSpreadInternalizationPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.gcBorrowSpreadInternalizationPct;
        },
        width: 90
      },{
        id: "longOutflowImpactUsd",
        name: "Balance Sheet Impact (PB Cash/ISDA Long)",
        field: "longOutflowImpactUsd",
        toolTip: "Balance Sheet Impact (PB Cash/ISDA Long)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }, {
        id: "shortsImpactUsd",
        name: "Balance Sheet Impact (PB/ISDA Shorts)",
        field: "shortsImpactUsd",
        toolTip: "Balance Sheet Impact (PB/ISDA Shorts)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }, {
        id: "balanceSheetLongsImpactUsd",
        name: "Balance Sheet Benefit (PB Long Lend)",
        field: "balanceSheetLongsImpactUsd",
        toolTip: "Balance Sheet Benefit (PB Long Lend)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }, {
        id: "assetsValueUsd",
        name: "Total Assets",
        field: "assetsValueUsd",
        toolTip: "Total Assets",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },{
        id: "cashUsd",
        name: "Cash (USD)",
        field: "cashUsd",
        toolTip: "Cash (USD)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },{
        id: "htbMarketValueUsd",
        name: "HTB Value (USD)",
        field: "htbMarketValueUsd",
        toolTip: "HTB Value (USD)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }, {
        id: "gcMarketValueUsd",
        name: "GC Value (USD)",
        field: "gcMarketValueUsd",
        toolTip: "GC Value (USD)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }

  ];
  return columns;
}
