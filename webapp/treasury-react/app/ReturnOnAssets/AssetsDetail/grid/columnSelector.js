import {
  assetsGridColumns
  } from "./columnConfig";

  export function getDefaultColumns() {
    var defaultColumns = [
      "agreementDisplayName",
      "agreementTypeName",
      "currency",
      "viewCalcRules",
      "lmvUsd",
      "longOutflowImpactUsd",
      "shortsImpactUsd",
      "balanceSheetLongsImpactUsd",
      "assetsValueUsd"
    ];

    return defaultColumns;
  }

  export function getTotalColumns() {
        return assetsGridColumns().map(column => [column.id, column.name]);
  }
