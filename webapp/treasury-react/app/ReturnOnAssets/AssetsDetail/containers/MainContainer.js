import React, { Component } from "react";
import { Layout } from 'arc-react-components';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  assetsGridColumns
} from "../grid/columnConfig";
import {
  assetsGridOptions
} from "../grid/gridOptions";
import {
  getMessage,
  getDrillDownGrid
} from "../../commons/util";
import GridContainer from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import {
  fetchCalculationRulesData,
  updateDrillDownSelectedFilters
} from "../../actions";

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.onViewCalcRules = this.onViewCalcRules.bind(this);
  }

  onViewCalcRules = args => {
    if(args.colId !== "viewCalcRules")
      return;

    var message = 'Calculation Rules applied for ';
    message = message.concat(args.item.agreementDisplayName).concat('-').concat(args.item.currency);
    var payload = {
      date: this.props.selectedFilters.selectedDate,
      cpeIds: args.item.cpeId,
      agreementTypeIds: args.item.agreementTypeId,
      legalEntityIds: args.item.legalEntityId,
      currencyIds: args.item.currencyId,
      dialogView: "",
      message: message,
      selectedDate: this.props.selectedFilters.selectedDate,
      selectedAgreementTypes: [{
        key: args.item.agreementTypeId,
        value: args.item.agreementTypeName
      }],
      selectedCpes: [{
        key: args.item.cpeId,
        value: args.item.cpeName
      }],
      selectedLegalEntities: [{
        key: args.item.legalEntityId,
        value: args.item.legalEntityName
      }],
      selectedCurrencies: [{
        key: args.item.currencyId,
        value: args.item.currency
      }]
    };

   this.props.updateDrillDownSelectedFilters(payload);
   this.props.fetchCalculationRulesData(payload);
   this.props.handleViewToggle("calcRules");
  };

  render() {
    if(this.props.assetsDetailData === null || this.props.assetsDetailData === undefined || this.props.assetsDetailData.data == null)
      return getMessage("assets");
    let grid = getDrillDownGrid(this.props.assetsDetailData.data, "AssetsDetailData", assetsGridColumns(),
               assetsGridOptions(), this.props.assetsDetailData.cpeFamilyName, "Assets Detail", this.onViewCalcRules);
    return grid;
  }
}

MainContainer.propTypes = {
  assetsDetailData: PropTypes.object,
  handleViewToggle: PropTypes.func,
  fetchCalculationRulesData: PropTypes.func,
  selectedFilters: PropTypes.array,
  updateDrillDownSelectedFilters: PropTypes.func
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCalculationRulesData,
      updateDrillDownSelectedFilters
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    assetsDetailData: state.returnOnAssetsData.assetsDetailData,
    selectedFilters: state.returnOnAssetsData.drillDownFilters
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainContainer);
