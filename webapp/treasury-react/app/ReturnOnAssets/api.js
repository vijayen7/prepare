import { BASE_URL } from "commons/constants";
const queryString = require("query-string");

export let url = "";
export function getReturnOnAssetsData(payload) {
  url = `${BASE_URL}service/returnOnAssetsCalculatorService/getReturnOnAssetsData?date="${payload.date}"&cpeFamilyIds=${payload.cpeFamilyIds}&agreementTypeIds=${payload.agreementTypeIds}&format=JSON&inputFormat=json`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json())
}

export function getReturnsDetailData(payload) {
  var returnsDetailList = payload.drillDownData;

  var results = {
    cpeFamilyId : payload.cpeFamilyId,
    cpeFamilyName : payload.cpeFamilyName,
    data : returnsDetailList
  };
  return results;
}

export function getAssetsDetailData(payload) {
  var assetsDetailList = payload.drillDownData;

  var results = {
    cpeFamilyId : payload.cpeFamilyId,
    cpeFamilyName : payload.cpeFamilyName,
    data : assetsDetailList
  };
  return results;
}

export function getCalculationRulesData(payload) {
   url = `${BASE_URL}service/returnOnAssetsKnobService/getMatchingReturnOnAssetsKnobs?date="${payload.date}"&cpeIds=${payload.cpeIds}&legalEntityIds=${payload.legalEntityIds}&agreementTypeIds=${payload.agreementTypeIds}&currencyIds=${payload.currencyIds}&format=JSON&inputFormat=json`;
   return fetch(url, {
     credentials: "include"
   }).then(data => data.json())
 }

 export function doAddEditCalculationRule(payload) {
   url=`${BASE_URL}service/returnOnAssetsKnobService/addEditCalculationRule?effectiveDate="${payload.date}"&cpeId=${payload.cpeId}&legalEntityId=${payload.legalEntityId}&agreementTypeId=${payload.agreementTypeId}&currencyId=${payload.currencyId}&knobTypeId=${payload.knobTypeId}&knobValue=${payload.knobValue}&format=JSON&inputFormat=json`;
   return fetch(url, {
     credentials: "include"
   }).then(data => data.json())
 }

 export function addOrUpdateCpeFamilyAdjustments(payload) {
   url = `${BASE_URL}service/returnOnAssetsCalculatorService/addCpeFamilyAdjustments?date="${payload.date}"&cpeFamilyId=${payload.cpeFamilyId}&adjustedReturns=${payload.adjustedReturns}&adjustedAssets=${payload.adjustedAssets}&comment=${payload.comment}&format=JSON&inputFormat=json`;
   return fetch(url, {
     credentials: "include"
   }).then(data => data.json())
 }


export function getSimulatedReturnOnAssetsData(payload) {
    url = `${BASE_URL}service/returnOnAssetsCalculatorService/simulateReturnOnAssets?effectiveDate="${payload.date}"&cpeId=${payload.cpeId}&legalEntityId=${payload.legalEntityId}&agreementTypeId=${payload.agreementTypeId}&currencyId=${payload.currencyId}&knobTypeId=${payload.knobTypeId}&knobValue=${payload.knobValue}&format=JSON&inputFormat=json`;
    return fetch(url, {
      credentials: "include"
    }).then(data => data.json())
}
