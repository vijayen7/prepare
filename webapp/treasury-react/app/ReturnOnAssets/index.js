import React, { Component } from "react";
import Loader from "commons/container/Loader";
import RoaSummary from "./RoaSummary";
import CalculationRules from "./CalculationRules";
import ReturnsDetail from "./ReturnsDetail";
import AssetsDetail from "./AssetsDetail";
export default class ReturnOnAssets extends Component {

  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.getBreadCrumbs = this.getBreadCrumbs.bind(this);
    this.state = [];
  }

  getBreadCrumbs() {
    return [
      {
        name: 'Treasury',
        link: '/treasury'
      },
      {
        name: 'Return On Assets',
      link: '#'
      }
    ];
  }

  componentDidMount() {
    var header = this.header;
    var breadcrumbs = this.getBreadCrumbs();

    if (header.ready) {
      header.setBreadcrumb(breadcrumbs);
    } else {
      header.addEventListener('ready', header.setBreadcrumb(breadcrumbs));
    }
    this.roaHome.classList.add("active");
    this.setState({ view: "roaHome" });
  }

  handleToggle(view) {
    this.roaHome.classList.remove("active");
    this.returnsDetail.classList.remove("active");
    this.assetsDetail.classList.remove("active");
    this.calcRules.classList.remove("active");
    this.setState({ view });
  if (view === "roaHome") {
    this.roaHome.classList.add("active");
  } else if (view === "returnsDetail") {
    this.returnsDetail.classList.add("active");
  } else if (view === "assetsDetail") {
    this.assetsDetail.classList.add("active");
  } else if(view === "calcRules") {
    this.calcRules.classList.add("active");
  }
}

getView(view) {
  if (view === 'calcRules') {
    return <CalculationRules handleViewToggle = {this.handleToggle} view = {this.state.view} />;
  } else if (view === 'roaHome') {
    return <RoaSummary handleViewToggle = {this.handleToggle} view = {this.state.view} />;
  } else if (view === 'returnsDetail') {
    return <ReturnsDetail handleViewToggle = {this.handleToggle} view = {this.state.view} />;
  } else if (view === 'assetsDetail') {
    return <AssetsDetail handleViewToggle = {this.handleToggle} view = {this.state.view} />;
  }
}


  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header

            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled>
            <div slot="application-menu" className="application-menu-toggle-view">
              <a
                ref={roaHome => {
                  this.roaHome = roaHome;
                }}
                onClick={() => this.handleToggle("roaHome")}
              >
                Return on Assets
              </a>
              <a
                ref={returnsDetail => {
                  this.returnsDetail = returnsDetail;
                }}
                onClick={() => this.handleToggle("returnsDetail")}
              >
                Returns Detail
              </a>
              <a
                ref={assetsDetail => {
                  this.assetsDetail = assetsDetail;
                }}
                onClick={() => this.handleToggle("assetsDetail")}
              >
                Assets Detail
              </a>
              <a
                ref={calcRules => {
                  this.calcRules = calcRules;
                }}
                onClick={() => this.handleToggle("calcRules")}
              >
                Calculation Factors
              </a>
            </div>
            </arc-header>
            <div className="layout--flex padding--top">
              {this.getView(this.state.view) }
            </div>
        </div>
      </React.Fragment>
    );
  }
}
