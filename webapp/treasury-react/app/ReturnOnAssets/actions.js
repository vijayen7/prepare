import {
    FETCH_RETURN_ON_ASSETS_DATA,
    DESTROY_RETURN_ON_ASSETS_DATA,
    FETCH_RETURNS_DETAIL_DATA,
    FETCH_ASSETS_DETAIL_DATA,
    FETCH_ROA_CALCULATION_RULES_DATA,
    DESTROY_ROA_CALCULATION_RULES_DATA,
    ADD_EDIT_ROA_CALCULATION_RULE,
    ADD_ROA_ADJUSTMENTS,
    SIMULATE_RETURN_ON_ASSETS_CALCULATION
  } from "./commons/constants";

  import {
    UPDATE_DRILL_DOWN_SELECTED_FILTERS
  } from "commons/constants";

  export function fetchReturnOnAssetsData(payload) {
    return {
      type: FETCH_RETURN_ON_ASSETS_DATA,
      payload
    };
  }

  export function destroyReturnOnAssetsData() {
    return {
      type: DESTROY_RETURN_ON_ASSETS_DATA
    };
  }

  export function fetchReturnsDetailData(payload) {
    return {
      type: FETCH_RETURNS_DETAIL_DATA,
      payload
    };
  }

  export function fetchAssetsDetailData(payload) {
    return {
      type: FETCH_ASSETS_DETAIL_DATA,
      payload
    };
  }

  export function fetchCalculationRulesData(payload) {
    return {
      type: FETCH_ROA_CALCULATION_RULES_DATA,
      payload
    };
  }

  export function destroyCalculationRulesData() {
    return {
      type: DESTROY_ROA_CALCULATION_RULES_DATA
    };
  }

  export function updateDrillDownSelectedFilters(payload) {
    return {
      type: UPDATE_DRILL_DOWN_SELECTED_FILTERS,
      payload
    };
  }

  export function addEditCalculationRule(payload) {
    return {
      type: ADD_EDIT_ROA_CALCULATION_RULE,
      payload
    };
  }

  export function addCpeFamilyAdjustments(payload) {
    return {
      type: ADD_ROA_ADJUSTMENTS,
      payload
    };
  }

  export function simulateReturnOnAssetsCalculation(payload) {
    return {
      type: SIMULATE_RETURN_ON_ASSETS_CALCULATION,
      payload
    };
  }
