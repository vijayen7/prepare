import {
  FETCH_RETURN_ON_ASSETS_DATA,
  DESTROY_RETURN_ON_ASSETS_DATA,
  FETCH_RETURNS_DETAIL_DATA,
  FETCH_ASSETS_DETAIL_DATA,
  FETCH_ROA_CALCULATION_RULES_DATA,
  DESTROY_ROA_CALCULATION_RULES_DATA,
  ADD_EDIT_ROA_CALCULATION_RULE,
  ADD_ROA_ADJUSTMENTS,
  SIMULATE_RETURN_ON_ASSETS_CALCULATION
} from "./commons/constants";

import {
  UPDATE_DRILL_DOWN_SELECTED_FILTERS,
  RESIZE_CANVAS,
} from "commons/constants";

import {
  combineReducers
} from "redux";

import {
  getReturnsDetailData,
  getAssetsDetailData
} from "./api";

function returnOnAssetsDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_RETURN_ON_ASSETS_DATA}_SUCCESS`:
      if(action.data.length <= 0) {
        action.message = 'No data available for selected search parameters';
      }
      return action || {};
    case DESTROY_RETURN_ON_ASSETS_DATA:
      return [];
  }
  return state;
}

function returnsDetailDataReducer(state = [], action) {
  switch(action.type) {
    case FETCH_RETURNS_DETAIL_DATA:
    const data = getReturnsDetailData(action.payload);
      return data || [];
  }
  return state;
}

function assetsDetailDataReducer(state = [], action) {
  switch(action.type) {
    case FETCH_ASSETS_DETAIL_DATA:
    const data = getAssetsDetailData(action.payload);
      return data || [];
  }
  return state;
}

function resizeCanvasReducer(state, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return false;
  }
}

function calculationRulesDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_ROA_CALCULATION_RULES_DATA}_SUCCESS`:
      return action.data || [];
    case DESTROY_ROA_CALCULATION_RULES_DATA:
      return [];
  }
  return state;
}

function drillDownSelectedFilterReducer(state = {}, action) {
  switch (action.type) {
    case `${UPDATE_DRILL_DOWN_SELECTED_FILTERS}_SUCCESS`:
      return action.selectedFilters || {};
  }
  return state;
}

function addEditCalculationRuleReducer(state = [], action) {
  switch(action.type) {
    case `${ADD_EDIT_ROA_CALCULATION_RULE}_SUCCESS`:
    var data = {
      'addEditStatus' : action.data['@CLASS'].includes('RemoteException') ? false : true
    };
    return data || {};
  }
  return state;
}

function addCpeFamilyAdjustmentsReducer(state = [], action) {
  switch(action.type) {
    case `${ADD_ROA_ADJUSTMENTS}_SUCCESS`:
    var data = {
      'addAdjustmentStatus': action.data['@CLASS'].includes('RemoteException') ? false : true
    };
    return data || {};
  }
  return state;
}

function simulateReturnOnAssetsCalculationReducer(state = [], action) {
  switch(action.type) {
    case `${SIMULATE_RETURN_ON_ASSETS_CALCULATION}_SUCCESS`:
    if(action.data.length <= 0) {
      action.message = 'No simulated data could be fetched.';
    } else {
      action.isSimulationComplete = true;
    }
    return action || {};
    case `${SIMULATE_RETURN_ON_ASSETS_CALCULATION}_FAILURE`:
      action.message = 'Error while simulating Return on Assets calculation.';
      action.isSimulationComplete = false;
      return action || {};
  }
  return state;
}

const rootReducer = combineReducers({
  returnOnAssetsData: returnOnAssetsDataReducer,
  resizeCanvas: resizeCanvasReducer,
  returnsDetailData: returnsDetailDataReducer,
  assetsDetailData: assetsDetailDataReducer,
  calculationRulesData: calculationRulesDataReducer,
  drillDownFilters: drillDownSelectedFilterReducer,
  addEditCalculationRuleData: addEditCalculationRuleReducer,
  cpeFamilyAdjustmentsData: addCpeFamilyAdjustmentsReducer,
  simulatedReturnOnAssetsData: simulateReturnOnAssetsCalculationReducer
});

export default rootReducer;
