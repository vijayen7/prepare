import React, { Component } from "react";
import { Layout } from 'arc-react-components';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  calcRulesGridColumns,
  simulatedReturnOnAssetsGridColumns
} from "../grid/columnConfig";
import {
  calcRulesGridOptions,
  simulatedReturnOnAssetsGridOptions
} from "../grid/gridOptions";
import {
  validateNumericInput,
  getStatusMessage,
  validateInputEmpty,
  knobType
} from "../../commons/util";
import {
  getCommaSeparatedValuesOrEmptyForSingleSelect
} from "commons/util";
import CalcRulesGrid from "commons/components/GridWithCellClick";
import SimulatedRoaGrid from "commons/components/Grid";
import Message from "commons/components/Message";
import Dialog from "commons/components/Dialog";
import InputFilter from "commons/components/InputFilter";
import InputNumberFilter from "commons/components/InputNumberFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import DateFilter from "commons/container/DateFilter";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import RoaCalculationFactorYearFilter from "commons/container/RoaCalculationFactorYearFilter";
import RoaCalculationFactorsFilter from "commons/container/RoaCalculationFactorsFilter";

import {
  fetchCalculationRulesData,
  addEditCalculationRule,
  destroyCalculationRulesData,
  simulateReturnOnAssetsCalculation
} from "../../actions";

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.onAddEditCalcRules = this.onAddEditCalcRules.bind(this);
    this.updateDialogFilters = this.updateDialogFilters.bind(this);
    this.updateDialogFiltersForAdd = this.updateDialogFiltersForAdd.bind(this);
    this.onCreateSave = this.onCreateSave.bind(this);
    this.onSimulate = this.onSimulate.bind(this);
    this.updateAddEditRuleStatus = this.updateAddEditRuleStatus.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.loadActionMessageDialogs = this.loadActionMessageDialogs.bind(this);
    this.isTopSidedAdjustmentKnob = this.isTopSidedAdjustmentKnob.bind(this);
    this.state = {};
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      selectedDate: this.props.selectedFilters.selectedDate,
      selectedEffectiveDate: this.props.selectedFilters.selectedDate,
      calcFactorTitle: '',
      selectedCpes: [],
      selectedLegalEntities: [],
      selectedAgreementTypes: [],
      selectedCurrencies: [],
      selectedBusinessUnitGroups: [],
      selectedKnobValue: '',
      selectedKnobTypeId: '',
      selectedComment: "",
      showModal: false,
      dialogView: '',
      addEditCalculationRuleData: {},
      selectedYear: ''
    };
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

updateDialogFiltersForAdd() {
  const all = {
    key: -1,
    value: "All"
  };

  const calcFactorTitle = all;
  const selectedCpe = all;
  const selectedLegalEntity = all;
  const selectedAgreementType = all;
  const selectedCurrency = all;
  const selectedBu = all;

  const selectedKnobType = null;
  var addCalculationFactorData = {
    selectedDate: this.props.selectedFilters.selectedDate,
    selectedEffectiveDate: this.props.selectedFilters.selectedDate,
    selectedCpes: selectedCpe,
    selectedLegalEntities: selectedLegalEntity,
    selectedAgreementTypes: selectedAgreementType,
    selectedCurrencies: selectedCurrency,
    selectedBusinessUnitGroups: selectedBu,
    selectedKnobValue: "",
    selectedKnobType: null,
    selectedKnobTypeId: null,
    selectedComment: "",
    showModal: true,
    dialogView: "addCalcRules",
    addEditCalculationRuleData: {},
    selectedYear: ''
  };

  this.setState(addCalculationFactorData);
}
  updateDialogFilters(selectedRow, dialogViewName) {
    const all = {
      key: -1,
      value: "All"
    };

    const calcFactorTitle = selectedRow.groupingKey === undefined ? selectedRow.knobName : selectedRow.groupingKey;
    const knobTypeId = selectedRow.groupingKey === undefined ? selectedRow.knobTypeId : selectedRow.rows[0].knobTypeId;
    const selectedCpe = selectedRow.cpeId ? {
            key: selectedRow.cpeId,
            value: selectedRow.cpeName
          } : all;

      const selectedLegalEntity = selectedRow.legalEntityId ? {
            key: selectedRow.legalEntityId,
            value: selectedRow.legalEntityName
          } : all;

      const selectedAgreementType = selectedRow.agreementTypeId ? {
          key: selectedRow.agreementTypeId,
          value: selectedRow.agreementType
        } : all;

      const selectedCurrency = selectedRow.currencyId ? {
          key: selectedRow.currencyId,
          value: selectedRow.currency
      } : all;

      return {
        selectedDate: this.props.selectedFilters.selectedDate,
        selectedEffectiveDate: this.props.selectedFilters.selectedDate,
        calcFactorTitle: calcFactorTitle,
        selectedCpes: selectedCpe,
        selectedLegalEntities: selectedLegalEntity,
        selectedAgreementTypes: selectedAgreementType,
        selectedCurrencies: selectedCurrency,
        selectedKnobValue: selectedRow.knobValue,
        selectedKnobTypeId: knobTypeId,
        selectedComment: "",
        showModal: true,
        dialogView: dialogViewName,
        addEditCalculationRuleData: {},
        selectedYear: ''
      };
  }

  onCloseModal() {
    delete this.props.addEditCalculationRuleData.addEditStatus;
    delete this.props.simulatedReturnOnAssetsData.isSimulationComplete;
    delete this.props.simulatedReturnOnAssetsData.data;
    this.setState(this.getDefaultFilters());
  }

  isTopSidedAdjustmentKnob() {
    return (this.state != undefined && this.state != null) &&
    ((this.state.calcFactorTitle != undefined && this.state.calcFactorTitle != null && this.state.calcFactorTitle == knobType.TOP_SIDED_ADJUSTMENTS_NOTIONAL)
    || (this.state.selectedKnobType != undefined && this.state.selectedKnobType != null && this.state.selectedKnobType.value == knobType.TOP_SIDED_ADJUSTMENTS_NOTIONAL));
  }

  onAddEditCalcRules = args => {
    if(args.colId == "actions") {
      var selectedRow = {};
      selectedRow = this.updateDialogFilters(args.item, "editCalcRules");
      this.setState(selectedRow);
    }
  }

  updateAddEditRuleStatus(addEditMessage, addEditMessageClass, addEditMessageHidden) {
    let newState = Object.assign({}, this.state);
    var data = {
      'addEditMessage' : addEditMessage,
      'addEditMessageClass': addEditMessageClass,
      'addEditMessageHidden': addEditMessageHidden
    };
    newState['addEditCalculationRuleData'] = data;
    this.setState(newState);
  }

  onCreateSave() {
    this.updateAddEditRuleStatus('', '', true);
    if(this.state.dialogView === "addCalcRules" && (this.state.selectedKnobType === undefined || this.state.selectedKnobType == null)) {
      this.updateAddEditRuleStatus('Calculation Factor must be selected.', 'message--warning no-icon', false);
      return;
    }

    var isTopSidedAdjustment = this.isTopSidedAdjustmentKnob();

    if(!isTopSidedAdjustment && (this.state.selectedYear === undefined || this.state.selectedYear === null || this.state.selectedYear == "")) {
      this.updateAddEditRuleStatus('Effective year cannot be empty.', 'message--warning no-icon', false);
      return;
    }

    if(validateInputEmpty(this.state.selectedKnobValue)) {
      this.updateAddEditRuleStatus('Knob value cannot be empty.', 'message--warning no-icon', false);
      return;
    }

    var valid = validateNumericInput(this.state.selectedKnobValue);
    if(!valid) {
      this.updateAddEditRuleStatus('Knob value must be numeric.', 'message--warning no-icon', false);
      return;
    }

    if(!isTopSidedAdjustment && (this.state.selectedKnobValue < 0 || this.state.selectedKnobValue > 1)) {
      this.updateAddEditRuleStatus('Knob value must be between 0 and 1.', 'message--warning no-icon', false);
      return;
    }

    if(this.state.calcFactorTitle === 'TOTAL FINANCING FLAG' && !(this.state.selectedKnobValue == 0 || this.state.selectedKnobValue == 1)) {
      this.updateAddEditRuleStatus('Knob value for Total Financing Flag must be either 0 or 1.', 'message--warning no-icon', false);
      return;
    }

    let payloadDate;
    if(isTopSidedAdjustment) {
        payloadDate = this.state.selectedEffectiveDate;
    } else {
        payloadDate = this.state.selectedYear.key + '-01-01';
    }

    var payload = {
      date: payloadDate,
      cpeId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedCpes),
      legalEntityId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedLegalEntities),
      agreementTypeId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedAgreementTypes),
      currencyId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedCurrencies),
      knobTypeId: (this.state.dialogView === "addCalcRules") ? getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedKnobType) : this.state.selectedKnobTypeId,
      knobValue: this.state.selectedKnobValue
    };
    this.updateAddEditRuleStatus('', '', true);
    this.props.addEditCalculationRule(payload);
  }

  onSimulate() {
    this.updateAddEditRuleStatus('', '', true);

    if(this.state.dialogView === "addCalcRules" && (this.state.selectedKnobType === undefined || this.state.selectedKnobType == null)) {
      this.updateAddEditRuleStatus('Calculation Factor must be selected.', 'message--warning no-icon', false);
      return;
    }

    if(this.state.selectedDate === undefined || this.state.selectedDate === null || this.state.selectedDate == "") {
      this.updateAddEditRuleStatus('Date cannot be empty.', 'message--warning no-icon', false);
      return;
    }

    if(validateInputEmpty(this.state.selectedKnobValue)) {
      this.updateAddEditRuleStatus('Knob value cannot be empty.', 'message--warning no-icon', false);
      return;
    }

    var valid = validateNumericInput(this.state.selectedKnobValue);
    if(!valid) {
      this.updateAddEditRuleStatus('Knob value must be numeric.', 'message--warning no-icon', false);
      return;
    }

    if(!this.isTopSidedAdjustmentKnob() && (this.state.selectedKnobValue < 0 || this.state.selectedKnobValue > 1)) {
      this.updateAddEditRuleStatus('Knob value must be between 0 and 1.', 'message--warning no-icon', false);
      return;
    }

    if(this.state.calcFactorTitle === 'TOTAL FINANCING FLAG' && !(this.state.selectedKnobValue == 0 || this.state.selectedKnobValue == 1)) {
      this.updateAddEditRuleStatus('Knob value for Total Financing Flag must be either 0 or 1.', 'message--warning no-icon', false);
      return;
    }

    var payload = {
      date: this.state.selectedDate,
      cpeId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedCpes),
      legalEntityId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedLegalEntities),
      agreementTypeId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedAgreementTypes),
      currencyId: getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedCurrencies),
      knobTypeId: (this.state.dialogView === "addCalcRules") ? getCommaSeparatedValuesOrEmptyForSingleSelect(this.state.selectedKnobType) : this.state.selectedKnobTypeId,
      knobValue: this.state.selectedKnobValue
    };
    this.updateAddEditRuleStatus('', '', true);
    this.props.simulateReturnOnAssetsCalculation(payload);

  }

  loadActionMessageDialogs() {
    var isDialogOpen = false;
    var messageTitle = '';
    var message = '';
    if('addEditStatus' in this.props.addEditCalculationRuleData) {
      isDialogOpen = true;
      if(this.props.addEditCalculationRuleData.addEditStatus) {
        message = 'Calculation rule added/updated successfully. Please reload search to view latest results.';
        messageTitle = 'SUCCESS';
      } else {
        message = 'Error occured while adding/updating calculation rule.';
        messageTitle = 'ERROR';
      }
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={isDialogOpen}
          title={messageTitle}
          onClose={this.onCloseModal} >
          <Message error={!this.props.addEditCalculationRuleData.addEditStatus} success={this.props.addEditCalculationRuleData.addEditStatus}
            messageData={message} />
          <FilterButton onClick={this.onCloseModal} reset={false} label='Ok' />
        </Dialog>
      </React.Fragment>
    );
  }

  render() {
    if(this.props.calculationRulesData === null || this.props.calculationRulesData === undefined || this.props.calculationRulesData.length <= 0)
      return getMessage(null, null);
    var message = getMessage(this.props.calculationRulesData.selectedRowProps, this.props.calculationRulesData);

    var addEditMessage = (this.state.addEditCalculationRuleData.addEditMessageHidden) ? '' : (<div className={this.state.addEditCalculationRuleData.addEditMessageClass}>
          {this.state.addEditCalculationRuleData.addEditMessage} </div>);

    var simulatedReturnOnAssets = '';
    if ('message' in this.props.simulatedReturnOnAssetsData)
        simulatedReturnOnAssets = (<Message messageData={this.props.simulatedReturnOnAssetsData.message} />);
    else if ('data' in this.props.simulatedReturnOnAssetsData && this.props.simulatedReturnOnAssetsData.data.length > 0){
      var knobTypeId = (this.state.dialogView === "addCalcRules") ? this.state.selectedKnobType.key : this.state.selectedKnobTypeId;
      simulatedReturnOnAssets = (<SimulatedRoaGrid
        data={this.props.simulatedReturnOnAssetsData.data}
        gridId="SimulatedRoaData"
        label="Agreements Impacted"
        gridColumns={simulatedReturnOnAssetsGridColumns(knobTypeId)}
        gridOptions={simulatedReturnOnAssetsGridOptions()}
        />);
    }

    var calculationFactorFilter = '';
    if(this.state.showModal) {
      if(this.state.dialogView === "editCalcRules") {
        calculationFactorFilter = (<InputFilter
          data={this.state.calcFactorTitle}
          stateKey="knobType"
          label="Calculation Factor"
          disable={true}
          readonly={true}
        />);
      } else {
        calculationFactorFilter = (<RoaCalculationFactorsFilter
          onSelect={this.onSelect}
          selectedData={this.state.selectedKnobType}
        />);
      }
    }

    let grid = (
      <React.Fragment>
            {message}
            <FilterButton
              onClick={this.updateDialogFiltersForAdd}
              reset={false}
              label="Add new Calculation Factor mapping"
            />
            <CalcRulesGrid
              data={this.props.calculationRulesData}
              gridId="CalcRulesData"
              onCellClick={this.onAddEditCalcRules}
              gridColumns={calcRulesGridColumns()}
              gridOptions={calcRulesGridOptions()}
              heightValue={630}
            />

            <Dialog isOpen={this.state.showModal} title={this.state.dialogView === "editCalcRules" ? "Edit Rule" : "Add Rule"} onClose={this.onCloseModal}
              style={{ width: "150px", height: "650px" }}
            >
            {addEditMessage}
            {calculationFactorFilter}
            <Layout>
              <Layout.Child size="content" style={{ padding:'5px' }}>
                <Layout isColumnType={true}>
                  <Layout.Child>
                    <AgreementTypeFilter
                      onSelect={this.onSelect}
                      multiSelect={false}
                      selectedData={this.state.selectedAgreementTypes}
                      readonly={this.state.dialogView === "editCalcRules"}
                    />
                  </Layout.Child>
                  <Layout.Child>
                    <LegalEntityFilter
                      onSelect={this.onSelect}
                      multiSelect={false}
                      selectedData={this.state.selectedLegalEntities}
                      readonly={this.state.dialogView === "editCalcRules"}
                    />
                  </Layout.Child>
                </Layout>
              </Layout.Child>
              <Layout.Child size="content" style={{ padding:'5px' }}>
                <Layout isColumnType={true}>
                  <Layout.Child>
                    <CpeFilter
                      onSelect={this.onSelect}
                      multiSelect={false}
                      selectedData={this.state.selectedCpes}
                      readonly={this.state.dialogView === "editCalcRules"}
                    />
                  </Layout.Child>
                  <Layout.Child>
                    <CurrencyFilter
                      onSelect={this.onSelect}
                      multiSelect={false}
                      selectedData={this.state.selectedCurrencies}
                      readonly={this.state.dialogView === "editCalcRules"}
                    />
                  </Layout.Child>
                </Layout>
              </Layout.Child>
              <Layout.Child size="content" style={{ padding:'5px' }}>
                <Layout isColumnType={true}>
                  <Layout.Child size={1} style={{ paddingLeft:'4px' }}>
                    <DateFilter
                      onSelect={this.onSelect}
                      stateKey="selectedDate"
                      data={this.state.selectedDate}
                      label="Simulate calculations for date"
                    />
                  </Layout.Child>
                </Layout>
              </Layout.Child>
              <br/>
              <Layout.Child size="content" style={{ padding:'5px' }}>
                <Layout isColumnType={true}>
                  <Layout.Child size={1}>
                    <InputNumberFilter
                      stateKey="selectedKnobValue"
                      label= {this.isTopSidedAdjustmentKnob() ? "Value ($)" : "Value"}
                      data={this.state.selectedKnobValue}
                      onSelect={this.onSelect}
                    />
                  </Layout.Child>
                  <Layout.Child size={1} style={{ paddingLeft:'4px' }}>
                  {this.isTopSidedAdjustmentKnob() ?
                  (<DateFilter
                      onSelect={this.onSelect}
                      stateKey="selectedEffectiveDate"
                      data={this.state.selectedEffectiveDate}
                      label="Effective Date"
                    /> ) : (
                    <RoaCalculationFactorYearFilter
                      onSelect={this.onSelect}
                      selectedData={this.state.selectedYear}
                    />)}
                  </Layout.Child>
                </Layout>
              </Layout.Child>
              <br/>
              <Layout.Child size="content" style={{ padding:'5px' }}>
              <InputFilter
                stateKey="selectedComment"
                label="Comment"
                data={this.state.selectedComment}
                onSelect={this.onSelect}
              />
              </Layout.Child>
              <br/>
              <Layout.Child size="content" style={{ padding:'5px' }}>
                <ColumnLayout>
                    <FilterButton
                      onClick={this.onCreateSave}
                      reset={false}
                      label={this.state.dialogView === "editCalcRules" ? "Save" : "Create"}
                      disabled={('addEditStatus' in this.props.addEditCalculationRuleData) || !this.props.simulatedReturnOnAssetsData.isSimulationComplete ? true : false}
                    />
                    <FilterButton
                      onClick={this.onSimulate}
                      reset={false}
                      label="Simulate"
                      disabled={('addEditStatus' in this.props.addEditCalculationRuleData) ? true : false}
                    />
                    <FilterButton reset={true} onClick={this.onCloseModal} label="Cancel" />
                </ColumnLayout>
              </Layout.Child>
              <Layout.Child size="content" style={{ padding:'5px' }}>
                {simulatedReturnOnAssets}
              </Layout.Child>
            </Layout>
            </Dialog>
            {this.loadActionMessageDialogs()}
           </React.Fragment>
          );
          return grid;
  }
}

function getMessage(selectedRowProps, calculationRulesData) {

  if(selectedRowProps == null && calculationRulesData == null) {
    return <Message messageData="Select calculation rules from Return on Assets tab or perform search." className="size--grow" />
  }
  if(selectedRowProps == null) {
    return '';
  } else {
    var message = 'Calculation Rules applied for ';
    message = message.concat(selectedRowProps.agreementDisplayName).concat('-').concat(selectedRowProps.currency);
    return <Message messageData={message} className="size--grow" />;
  }
}

MainContainer.propTypes = {
  handleViewToggle: PropTypes.func,
  fetchCalculationRulesData: PropTypes.func,
  destroyCalculationRulesData: PropTypes.func,
  addEditCalculationRule: PropTypes.func,
  calculationRulesData: PropTypes.object,
  addEditCalculationRuleData: PropTypes.object,
  simulatedReturnOnAssetsData: PropTypes.object,
  updateDrillDownSelectedFilters: PropTypes.func,
  simulateReturnOnAssetsCalculation: PropTypes.func,
  selectedFilters: PropTypes.array,
  Child: PropTypes.element,
  isOpen: PropTypes.bool
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchCalculationRulesData,
      destroyCalculationRulesData,
      addEditCalculationRule,
      simulateReturnOnAssetsCalculation
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    calculationRulesData: state.returnOnAssetsData.calculationRulesData,
    selectedFilters: state.returnOnAssetsData.drillDownFilters,
    addEditCalculationRuleData: state.returnOnAssetsData.addEditCalculationRuleData,
    simulatedReturnOnAssetsData: state.returnOnAssetsData.simulatedReturnOnAssetsData
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainContainer);
