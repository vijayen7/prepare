import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { bindActionCreators } from "redux";
import DateFilter from "commons/container/DateFilter";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton"
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import {
  fetchCalculationRulesData,
  destroyCalculationRulesData,
  updateDrillDownSelectedFilters
} from "../../actions";
import {
  getPreviousDate,
  isAllKeySelected,
  getCommaSeparatedValuesOrEmpty
} from "commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.state = {};
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  componentDidMount() {
    this.setState(this.getDefaultFilters(this.props.selectedFilters));
  }

  getDefaultFilters(selectedRowProps) {
    if(selectedRowProps == null) {
      return {
        dialogView: "",
        addEditRowDetails: "",
        showModal: false,
        selectedDate: getPreviousDate(),
        selectedAgreementTypes: [{
          key: 2,
          value: 'PB'
        }],
        selectedCpes: [],
        selectedLegalEntities: [],
        selectedCurrencies: []
      };
    } else {
      var newState = {
        dialogView: "",
        addEditRowDetails: "",
        showModal: false,
        selectedCpes: selectedRowProps.selectedCpes,
        selectedLegalEntities: selectedRowProps.selectedLegalEntities,
        selectedAgreementTypes: selectedRowProps.selectedAgreementTypes,
        selectedCurrencies: selectedRowProps.selectedCurrencies,
        selectedDate: selectedRowProps.selectedDate === undefined ? getPreviousDate() : selectedRowProps.selectedDate
      };
      return newState;
    }
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
   this.props.updateDrillDownSelectedFilters(this.state);
   this.props.destroyCalculationRulesData();
   var payload = {
     date: this.state.selectedDate,
     cpeIds: (this.state.selectedCpes !== undefined && this.state.selectedCpes.length > 0 && !isAllKeySelected(this.state.selectedCpes)
                ? getCommaSeparatedValuesOrEmpty(this.state.selectedCpes) : ''),
     agreementTypeIds: (this.state.selectedAgreementTypes !== undefined && this.state.selectedAgreementTypes.length > 0 && !isAllKeySelected(this.state.selectedAgreementTypes)
                ? getCommaSeparatedValuesOrEmpty(this.state.selectedAgreementTypes) : ''),
     legalEntityIds: (this.state.selectedLegalEntities !== undefined && this.state.selectedLegalEntities.length > 0 && !isAllKeySelected(this.state.selectedLegalEntities)
                ? getCommaSeparatedValuesOrEmpty(this.state.selectedLegalEntities) : ''),
     currencyIds: (this.state.selectedCurrencies !== undefined && this.state.selectedCurrencies.length > 0 && !isAllKeySelected(this.state.selectedCurrencies)
                ? getCommaSeparatedValuesOrEmpty(this.state.selectedCurrencies) : '')
   };
   this.props.fetchCalculationRulesData(payload);
 }

  render() {

    var selectedDate = ('selectedDate' in this.props.selectedFilters && (this.props.selectedFilters.selectedDate !== "" && this.props.selectedFilters.selectedDate !== undefined))
      ? this.props.selectedFilters.selectedDate : this.state.selectedDate;
    return (<Sidebar>
      <AgreementTypeFilter
        onSelect={this.onSelect}
        selectedData={this.state.selectedAgreementTypes}
      />
      <LegalEntityFilter
        onSelect={this.onSelect}
        selectedData={this.state.selectedLegalEntities}
      />
      <CpeFilter
        onSelect={this.onSelect}
        selectedData={this.state.selectedCpes}
      />
      <CurrencyFilter
        onSelect={this.onSelect}
        selectedData={this.state.selectedCurrencies}
      />
      <DateFilter
        onSelect={this.onSelect}
        stateKey="selectedDate"
        data={selectedDate}
      />
      <ColumnLayout>
        <FilterButton
          onClick={this.handleClick}
          reset={false}
          label="Search"
        />
        <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
      </ColumnLayout>
    </Sidebar>);
  }
}

SideBar.propTypes = {
  fetchCalculationRulesData: PropTypes.func,
  destroyCalculationRulesData: PropTypes.func,
  calculationRulesData: PropTypes.object,
  updateDrillDownSelectedFilters: PropTypes.func,
}

function mapStateToProps(state) {
  return {
    calculationRulesData: state.returnOnAssetsData.calculationRulesData,
    selectedFilters: state.returnOnAssetsData.drillDownFilters
  };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
      {
        fetchCalculationRulesData,
        destroyCalculationRulesData,
        updateDrillDownSelectedFilters
      },
      dispatch
    );
}

  export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
