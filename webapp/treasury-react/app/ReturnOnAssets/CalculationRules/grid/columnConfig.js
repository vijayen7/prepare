import {
  linkFormatter
} from "commons/grid/formatters";

import {
  knobType
} from "../../commons/util";

export function calcRulesGridColumns() {
  var columns = [{
      id: "calculationFactor",
      name: "Calculation Factor",
      field: "knobName",
      toolTip: "Calculation Factor",
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        if( value != undefined && value != null && value == knobType.TOP_SIDED_ADJUSTMENTS_NOTIONAL ) {
            return value + " ($)"
        } else {
          return value;
        }
    },
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 90
    },
    {
      id: "actions",
      name: "Actions",
      field: "actions",
      type: "text",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
          return linkFormatter(
            row,
            cell,
            "<i class='icon-edit--block' />",
            columnDef,
            dataContext
          );
      },
      headerCssClass: "b",
      width: 10,
    }, {
          id: "agreementType",
          name: "Agreement Type",
          field: "agreementType",
          toolTip: "agreementType",
          type: "text",
          filter: true,
          sortable: true,
          headerCssClass: "aln-rt b",
          formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
              if(value === null || value === undefined || value === '') {
                return 'ALL';
              } else {
                return value;
              }
          },
          width: 20
        },  {
          id: "cpe",
          name: "Counterparty",
          field: "cpeName",
          toolTip: "Counterparty",
          type: "text",
          filter: true,
          sortable: true,
          headerCssClass: "aln-rt b",
          formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
              if(value === null || value === undefined || value === '') {
                return 'ALL';
              } else {
                return value;
              }
          },
          width: 50
        }, {
          id: "legalEntity",
          name: "Legal Entity",
          field: "legalEntityName",
          toolTip: "Legal Entity",
          type: "text",
          filter: true,
          sortable: true,
          headerCssClass: "aln-rt b",
          formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
              if(value === null || value === undefined || value === '') {
                return 'ALL';
              } else {
                return value;
              }
          },
          width: 50
        }, {
          id: "currency",
          name: "Currency",
          field: "currency",
          toolTip: "Currency",
          type: "text",
          filter: true,
          sortable: true,
          headerCssClass: "aln-rt b",
          formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
              if(value === null || value === undefined || value === '') {
                return 'ALL';
              } else {
                return value;
              }
          },
          width: 30
        }, {
          id: "knobValue",
          name: "Factor Value",
          field: "knobValue",
          toolTip: "Value",
          type: "text",
          filter: true,
          sortable: true,
          formatter: dpGrid.Formatters.Float,
          headerCssClass: "b",
          width: 30
    },
  ];

  return columns;
}

export function simulatedReturnOnAssetsGridColumns(knobTypeId) {
  var columns = [{
        id: "agreementDisplayName",
        name: "Agreement",
        field: "agreementDisplayName",
        toolTip: "agreementDisplayName",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 70
      },
      {
        id: "businessUnitGroup",
        name: "BU Group",
        field: "businessUnitGroup",
        toolTip: "BU Group",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 30
      }, {
        id: "currency",
        name: "Currency",
        field: "currency",
        toolTip: "Currency",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 20
      }];

      if(knobTypeId > 7) {
        columns.push({
          id: "existingBrokerReturns",
          name: "Existing Broker Returns Value",
          field: "existingBrokerReturnsUsd",
          toolTip: "Existing Broker Returns value",
          type: "number",
          filter: true,
          sortable: true,
          headerCssClass: "aln-rt b",
          formatter: dpGrid.Formatters.Number,
          aggregator: dpGrid.Aggregators.sum,
          width: 50
        });
        columns.push({
          id: "simulatedBrokerReturns",
          name: "Simulated Broker Returns Value",
          field: "simulatedBrokerReturnsUsd",
          toolTip: "Simulated Broker Returns Value",
          type: "number",
          filter: true,
          sortable: true,
          headerCssClass: "aln-rt b",
          formatter: dpGrid.Formatters.Number,
          aggregator: dpGrid.Aggregators.sum,
          width: 50
        });
      }  else {
        columns.push({
            id: "existingAssetsValue",
            name: "Existing Assets Value",
            field: "existingAssetsValueUsd",
            toolTip: "Existing Assets value",
            type: "number",
            filter: true,
            sortable: true,
            headerCssClass: "aln-rt b",
            formatter: dpGrid.Formatters.Number,
            aggregator: dpGrid.Aggregators.sum,
            width: 50
        });
        columns.push({
            id: "simulatedAssetsValue",
            name: "Simulated Assets Value",
            field: "simulatedAssetsValueUsd",
            toolTip: "Simulated Assets Value",
            type: "number",
            filter: true,
            sortable: true,
            headerCssClass: "aln-rt b",
            formatter: dpGrid.Formatters.Number,
            aggregator: dpGrid.Aggregators.sum,
            width: 50
        });
    }
    return columns;
}
