export function calcRulesGridOptions() {
  var groupColumns = ['calculationFactor'];
  var options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [{ columnId: "calculationFactor", sortAsc: true }],
    highlightRowOnClick: true,
    expandCollapseAll: true,
    useAvailableScreenSpace : true
  }

return options;
}

export function simulatedReturnOnAssetsGridOptions() {
      var options = {
        summaryRow: true,
        displaySummaryRow: true,
        summaryRowText: "Total Impact",
        autoHorizontalScrollBar: true,
        applyFilteringOnGrid: true,
        showHeaderRow: true,
        exportToExcel: false,
        sortList: [{ columnId: "agreementDisplayName", sortAsc: true }],
        highlightRowOnClick: true,
        useAvailableScreenSpace : true
      }
    return options;
  }
