import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import SideBar from "./containers/SideBar";
import MainContainer from "./containers/MainContainer";
export default class CalculationRules extends Component {

  render() {
    return (
      <React.Fragment>
        <div className="layout--flex">
          <div className="size--1">
            <SideBar />
          </div>
          <div className="size--5 padding--horizontal--double">
            <MainContainer handleViewToggle = {this.props.handleViewToggle} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

CalculationRules.propTypes = {
  handleViewToggle: PropTypes.func,
  view: PropTypes.string,
  calculationRulesData: PropTypes.object,
  addEditCalculationRuleData: PropTypes.object,
  selectedFilters: PropTypes.array
};
