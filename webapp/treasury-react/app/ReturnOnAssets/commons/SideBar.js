import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
import DateFilter from "commons/container/DateFilter";
import { getCommaSeparatedValuesOrEmpty } from "commons/util";
import Sidebar from "commons/components/Sidebar";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import CpeFamilyFilter from "commons/container/CpeFamilyFilter";
import RoaAgreementTypeFilter from "commons/container/RoaAgreementTypeFilter";
import {
  fetchReturnOnAssetsData,
  destroyReturnOnAssetsData,
  updateDrillDownSelectedFilters
} from "../actions";
import {
  getPreviousDate
} from "commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    //this.resizeCanvas = this.resizeCanvas.bind(this);
    this.state = this.getDefaultFilters();
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  getDefaultFilters() {
    return {
      selectedDate: getPreviousDate(),
      selectedCpeFamilies: [],
      selectedAgreementTypes: []
    };
  }

  componentDidMount() {
    if(this.props.selectedFilters != undefined && this.props.selectedFilters != null) {
      this.setState({
        selectedDate : (this.props.selectedFilters.selectedDate != undefined) ? this.props.selectedFilters.selectedDate : getPreviousDate(),
        selectedCpeFamilies : (this.props.selectedFilters.selectedCpeFamilies != undefined) ? this.props.selectedFilters.selectedCpeFamilies : [],
        selectedAgreementTypes : (this.props.selectedFilters.selectedAgreementTypes != undefined) ? this.props.selectedFilters.selectedAgreementTypes : []
      });
    } else {
      this.setState(this.getDefaultFilters());
    }
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleClick() {
   this.props.updateDrillDownSelectedFilters(this.state);
   this.props.destroyReturnOnAssetsData();
   var payload = {
     date: this.state.selectedDate,
     cpeFamilyIds: getCommaSeparatedValuesOrEmpty(this.state.selectedCpeFamilies),
     agreementTypeIds: getCommaSeparatedValuesOrEmpty(this.state.selectedAgreementTypes)
   };
   this.props.fetchReturnOnAssetsData(payload);
 }

  render() {
    var selectedDate = ('selectedDate' in this.props.selectedFilters && (this.props.selectedFilters.selectedDate !== "" && this.props.selectedFilters.selectedDate !== undefined))
      ? this.props.selectedFilters.selectedDate : this.state.selectedDate;
    let sidebar = (<Sidebar>
      <DateFilter
        onSelect={this.onSelect}
        stateKey="selectedDate"
        data={selectedDate}
      />
      <CpeFamilyFilter
        onSelect={this.onSelect}
        selectedData={this.state.selectedCpeFamilies}
      />
      <RoaAgreementTypeFilter
        onSelect={this.onSelect}
        selectedData={this.state.selectedAgreementTypes}
      />
      <ColumnLayout>
        <FilterButton
          onClick={this.handleClick}
          reset={false}
          label="Search"
          disabled={this.props.disabled}
        />
        <FilterButton reset={true} onClick={this.handleReset} label="Reset" />
      </ColumnLayout>
    </Sidebar>);
    return sidebar;
  }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
      {
        fetchReturnOnAssetsData,
        destroyReturnOnAssetsData,
        updateDrillDownSelectedFilters
      },
      dispatch
    );
}

function mapStateToProps(state) {
  return {
    selectedFilters: state.returnOnAssetsData.drillDownFilters
  };
}

SideBar.propTypes = {
  fetchReturnOnAssetsData: PropTypes.func,
  destroyReturnOnAssetsData: PropTypes.func,
  updateDrillDownSelectedFilters: PropTypes.func,
  selectedFilters: PropTypes.array
};

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
