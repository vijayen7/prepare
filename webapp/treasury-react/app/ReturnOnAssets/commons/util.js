import React, { Component } from "react";
import { Layout } from 'arc-react-components';
import GridContainer from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";

const knobTypeValues = {
  ARRANGING_LMV_BALANCE_PCT: "ARRANGING LMV BALANCE PCT",
  ONSHORE_LMV_INTERN_PCT: "ONSHORE LMV INTERN PCT",
    ONSHORE_SMV_INTERN_PCT: "ONSHORE SMV INTERN PCT",
    ONSHORE_LMV_BENEFIT_PCT: "ONSHORE LMV BENEFIT PCT",
    OFFSHORE_LMV_INTERN_PCT: "OFFSHORE LMV INTERN PCT",
    OFFSHORE_SMV_INTERN_PCT: "OFFSHORE SMV INTERN PCT",
    OFFSHORE_LMV_BENEFIT_PCT: "OFFSHORE LMV BENEFIT PCT",
    TOTAL_FINANCING_FLAG: "TOTAL FINANCING FLAG",
    HTB_BORROW_SPREAD_INTERN_PCT: "HTB BORROW SPREAD INTERN PCT",
    GC_BORROW_SPREAD_INTERN_PCT: "GC BORROW SPREAD INTERN PCT",
    TOP_SIDED_ADJUSTMENTS_NOTIONAL: "TOP SIDED ADJUSTMENTS NOTIONAL",
    SMV_INTERNALIZATION_FACTOR: "SMV INTERNALIZATION FACTOR",
    SWAP_OUTPERFORMANCE_NOTIONAL: "SWAP OUTPERFORMANCE NOTIONAL",
    SWAP_OUTPERFORMANCE_BENEFIT_PCT: "SWAP OUTPERFORMANCE BENEFIT PCT"
}

export const knobType = Object.freeze(knobTypeValues);

export function getMessage(title) {
  var message = "Select CPE Family ";
  message = message.concat(title).concat(" detail from Return On Assets tab.");
  return <Message messageData = {message} className="size--grow" />;
}

export function getStatusMessage(statusMessageClass, statusMessage) {
  var message = '';
  message = (<div className={statusMessageClass}> {statusMessage} </div>);
    return message;
}

export function getDrillDownGrid(gridData, gridId, gridColumns, gridOptions, cpeFamilyName, title, onCellClickHandler) {
  var gridInfo = cpeFamilyName.concat(" - ").concat(title);
  var note = gridId === "ReturnsDetailData" ? (<Layout.Child><div className="legend__item">
                <span className="margin--left--small margin--right--double">
                  <b>Note : </b>Returns calculated from counterparty perspective.
                </span>
              </div></Layout.Child>) : null;
  return (
    <Layout>
      <Layout.Child>
       <Message messageData={gridInfo} />
      </Layout.Child>
        {note}
      <Layout.Child size={27}>
       <GridContainer
          data={gridData}
          gridId={gridId}
          gridColumns={gridColumns}
          gridOptions={gridOptions}
          onCellClick={onCellClickHandler}
       />
      </Layout.Child>
     </Layout>
  );
}

export function validateNumericInput(value) {
  var regex = /^-?\d*(\.\d+)?$/;
  if(value !== undefined && value !== null && value !== "") {
    return (regex.test(value));
  }
  return true;
};

export function validateInputEmpty(value) {
  if(value === undefined || value === null || value === '') {
    return true;
  }
  return false;
}

export function decimalToPercentFormatterWithColorCode(
  row,
  cell,
  value,
  columnDef,
  dataContext,
  className
) {
  if (value == null || value === "") {
    return '<div class="aln-rt"> <span class="message"> n/a </span> </div>';
  } else {
    if (className != null && className != undefined)
      return (
        '<div class="aln-rt ' +
        className +
        '">' +
        Number(value * 100).toFixed(1) +
        "%</div>"
      );
    else if(value >= 0) {
      return (
        '<div class="aln-rt">' + Number(value * 100).toFixed(2) + "%</div>"
      );
    }
    else {
      return(
        '<div class="aln-rt"> <span class="red5">' + Number(value * 100).toFixed(2) + "%*</span></div>"
        );
      }
  }
}
