import { put, takeEvery, call, all } from "redux-saga/effects";

import {
  FETCH_RETURN_ON_ASSETS_DATA,
  FETCH_ROA_CALCULATION_RULES_DATA,
  ADD_EDIT_ROA_CALCULATION_RULE,
  ADD_ROA_ADJUSTMENTS,
  SIMULATE_RETURN_ON_ASSETS_CALCULATION
} from "./commons/constants";

import {
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
  UPDATE_DRILL_DOWN_SELECTED_FILTERS,
} from "commons/constants";;

import {
  getReturnOnAssetsData,
  getCalculationRulesData,
  doAddEditCalculationRule,
  addOrUpdateCpeFamilyAdjustments,
  getSimulatedReturnOnAssetsData
} from "./api";

function* fetchReturnOnAssetsData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getReturnOnAssetsData, action.payload);
    yield put({ type: `${FETCH_RETURN_ON_ASSETS_DATA}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_RETURN_ON_ASSETS_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* returnOnAssetsData() {
  yield [
    takeEvery(FETCH_RETURN_ON_ASSETS_DATA, fetchReturnOnAssetsData)
  ];
}
function* fetchCalculationRulesData(action) {
  try {
    yield put({type: START_LOADING});
    const data = yield call(getCalculationRulesData, action.payload);
    data.selectedRowProps = action.payload.selectedRowProps;
    yield put({type: `${FETCH_ROA_CALCULATION_RULES_DATA}_SUCCESS`, data });
  } catch(e) {
    yield put({type: HANDLE_EXCEPTION});
    yield put({type: `${FETCH_ROA_CALCULATION_RULES_DATA}_FAILURE` });
  } finally {
    yield put({type: END_LOADING });
  }
}

export function* calculationRulesData() {
  yield [
    takeEvery(FETCH_ROA_CALCULATION_RULES_DATA, fetchCalculationRulesData)
  ];
}

function* updateDrillDownSelectedFilters(action) {
  try {
    const selectedFilters = action.payload;
    yield put({
      type: `${UPDATE_DRILL_DOWN_SELECTED_FILTERS}_SUCCESS`,
      selectedFilters
    });
  } catch (e) {}
}

function* drillDownSelectedFiltersRootSaga() {
  yield [
    takeEvery(
      UPDATE_DRILL_DOWN_SELECTED_FILTERS,
      updateDrillDownSelectedFilters
    )
  ];
}

function* addEditCalculationRule(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(doAddEditCalculationRule, action.payload);
    yield put({ type: `${ADD_EDIT_ROA_CALCULATION_RULE}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${ADD_EDIT_ROA_CALCULATION_RULE}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* addEditCalculationRuleData() {
  yield [
    takeEvery(ADD_EDIT_ROA_CALCULATION_RULE, addEditCalculationRule)
  ];
}

function* addCpeFamilyAdjustments(action) {
  try {
    yield put({type: START_LOADING });
    const data = yield call(addOrUpdateCpeFamilyAdjustments, action.payload);
    yield put({ type: `${ADD_ROA_ADJUSTMENTS}_SUCCESS`, data});
  } catch(e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${ADD_ROA_ADJUSTMENTS}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* cpeFamilyAdjustmentsData() {
  yield [
    takeEvery(ADD_ROA_ADJUSTMENTS, addCpeFamilyAdjustments)
  ];
}

function* simulateReturnOnAssetsCalculation(action) {
  try {
    yield put({type: START_LOADING });
    const data = yield call(getSimulatedReturnOnAssetsData, action.payload);
    yield put({ type: `${SIMULATE_RETURN_ON_ASSETS_CALCULATION}_SUCCESS`, data});
  } catch(e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${SIMULATE_RETURN_ON_ASSETS_CALCULATION}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

export function* simulatedReturnOnAssetsData() {
    yield [
      takeEvery(SIMULATE_RETURN_ON_ASSETS_CALCULATION, simulateReturnOnAssetsCalculation)
    ];
}

function* returnOnAssetsSaga() {
  yield all([returnOnAssetsData(), calculationRulesData(), drillDownSelectedFiltersRootSaga(), addEditCalculationRuleData(), cpeFamilyAdjustmentsData(), simulatedReturnOnAssetsData()]);
}

export default returnOnAssetsSaga;
