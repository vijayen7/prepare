import {
  linkFormatter,
  cashReportSumGroupFormatter,
  groupFloatFormatter
} from "commons/grid/formatters";

import { roaAggregator } from "commons/grid/aggregators";

export function returnsGridColumns() {
  var columns = [{
        id: "agreementDisplayName",
        name: "Agreement",
        field: "agreementDisplayName",
        toolTip: "agreementDisplayName",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 110
      }, {
        id: "agreementTypeName",
        name: "Agreement Type",
        field: "agreementTypeName",
        toolTip: "Agreement Type",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 110
      }, {
        id: "currency",
        name: "Currency",
        field: "currency",
        toolTip: "Currency",
        type: "text",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 50
      }, {
        id: "viewCalcRules",
        name: "View Calc Rules",
        field: "viewCalcRules",
        toolTip: "View calculation rules",
        type: "text",
        filter: true,
        sortable: true,
        formatter: function(row, cell, value, columnDef, dataContext) {
            return linkFormatter(
              row,
              cell,
              "View Calc Rules",
              columnDef,
              dataContext
            );
        },
        headerCssClass: "aln-rt b",
        width: 70
      }, {
        id: "dailyFinanceIncomeCostUsd",
        name: "Effective Financing Income",
        field: "dailyFinanceIncomeCostUsd",
        toolTip: "Effective Finance Income",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },  {
        id: "borrowChargeUsd",
        name: "Borrow Income",
        field: "borrowChargeUsd",
        toolTip: "Borrow Cost",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },  {
        id: "htbborrowSpreadInternalizationPct",
        name: "HTB Borrow Spread Internalization Factor",
        field: "brokerReturnsKnob",
        toolTip: "HTB Borrow Spread Internalization Factor",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.brokerReturnsKnob.htbBorrowSpreadInternalizationPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.htbBorrowSpreadInternalizationPct;
        },
        width: 90
      },  {
        id: "gcborrowSpreadInternalizationPct",
        name: "GC Borrow Spread Internalization Factor",
        field: "brokerReturnsKnob",
        toolTip: "GC Borrow Spread Internalization Factor",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return dpGrid.Formatters.Float(row, cell, dataContext.brokerReturnsKnob.gcBorrowSpreadInternalizationPct, columnDef, dataContext);
        },
        excelDataFormatter: function(itemValue, dataItem, currentGridObject, exportCurrentView, columnDef) {
	         return itemValue.gcBorrowSpreadInternalizationPct;
        },
        width: 90
      }, {
        id: "topSidedAdjustmentsUsd",
        name: "Aggregated Top Sided Adjustment (USD)",
        field: "topSidedAdjustmentsUsd",
        toolTip: "Aggregated Top Sided Adjustment (USD)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Float,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: groupFloatFormatter,
        width: 90
      }, {
        id: "totalCommissionUsd",
        name: "Commissions Income",
        field: "totalCommissionUsd",
        toolTip: "Total Commission",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },
      {
        id: "outperformanceIncomeUsd",
        name: "Outperformance Expense",
        field: "outperformanceIncomeUsd",
        toolTip: "Outperformance Expense",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }, {
        id: "lendIncomeUsd",
        name: "Lend Expense",
        field: "lendIncomeUsd",
        toolTip: "Lend Income",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },  {
        id: "longSpreadOnlyFinancingUsd",
        name: "Long Swap Financing Income",
        field: "longSpreadOnlyFinancingUsd",
        toolTip: "Long Swap Financing Income",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }, {
        id: "shortSpreadOnlyFinancingUsd",
        name: "Short Swap Financing Income",
        field: "shortSpreadOnlyFinancingUsd",
        toolTip: "Short Swap Financing Income",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      }, {
        id: "brokerReturnsUsd",
        name: "Total Returns",
        field: "brokerReturnsUsd",
        toolTip: "Total Returns",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },{
        id: "htbMarketValueUsd",
        name: "HTB Value (USD)",
        field: "htbMarketValueUsd",
        toolTip: "HTB Value (USD)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },{
        id: "gcMarketValueUsd",
        name: "GC Value (USD)",
        field: "gcMarketValueUsd",
        toolTip: "GC Value (USD)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },{
        id: "lmvUsd",
        name: "LMV (USD)",
        field: "lmvUsd",
        toolTip: "LMV (USD)",
        type: "number",
        filter: true,
        sortable: true,
        headerCssClass: "aln-rt b",
        formatter: dpGrid.Formatters.Number,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: roaAggregator,
        groupTotalsFormatter: cashReportSumGroupFormatter,
        width: 90
      },

  ];
  return columns;
}
