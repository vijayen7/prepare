import {
  returnsGridColumns
  } from "./columnConfig";

  export function getDefaultColumns() {
    var defaultColumns = [
      "agreementDisplayName",
      "agreementTypeName",
      "currency",
      "viewCalcRules",
      "dailyFinanceIncomeCostUsd",
      "borrowChargeUsd",
      "topSidedAdjustmentsUsd",
      "totalCommissionUsd",
      "outperformanceIncomeUsd",
      "lendIncomeUsd",
      "longSpreadOnlyFinancingUsd",
      "shortSpreadOnlyFinancingUsd",
      "brokerReturnsUsd"
    ];

    return defaultColumns;
  }

  export function getTotalColumns() {
        return returnsGridColumns().map(column => [column.id, column.name]);
  }
