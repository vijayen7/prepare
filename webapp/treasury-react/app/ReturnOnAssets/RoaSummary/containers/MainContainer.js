import React, { Component } from "react";
import { Layout } from 'arc-react-components';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Dialog from "commons/components/Dialog";
import ReturnOnAssetsDataGrid from "commons/components/GridWithCellClick";
import Message from "commons/components/Message";
import InputFilter from "commons/components/InputFilter";
import DateFilter from "commons/container/DateFilter";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import {
  roaGridColumns
} from "../grid/columnConfig";
import {
  roaGridOptions
} from "../grid/gridOptions";
import {
  getCommaSeparatedValuesOrNull,
  getCommaSeparatedValuesOrEmpty
} from "commons/util";
import {
  validateNumericInput,
  validateInputEmpty,
  getStatusMessage
} from "../../commons/util";
import {
  fetchReturnsDetailData,
  fetchAssetsDetailData,
  addCpeFamilyAdjustments,
  destroyReturnOnAssetsData ,
  fetchReturnOnAssetsData
} from "../../actions";

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.onCellClickHandler = this.onCellClickHandler.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.onAddAdjustment = this.onAddAdjustment.bind(this);
    this.updateAdjustmentStatus = this.updateAdjustmentStatus.bind(this);
    this.loadActionMessageDialogs = this.loadActionMessageDialogs.bind(this);
    this.state = {};
  }

  componentWillMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      showModal: false,
      selectedCpeFamily: "",
      selectedCpeFamilyId: "",
      selectedAdjustedReturns: "",
      selectedAdjustedAssets: "",
      selectedCalculatedReturns: "",
      selectedCalculatedAssets: "",
      selectedComment: "",
      cpeFamilyAdjustmentsData: {}
    };
  }

  onSelect(params) {
    const { key, value } = params;
    let newState = Object.assign({}, this.state);
    newState[key] = value;
    this.setState(newState);
  }

  onCellClickHandler = args => {
    var payload = {
      cpeFamilyId: args.item.cpeFamilyId,
      cpeFamilyName: args.item.cpeFamilyName,
      drillDownData: args.item.returnOnAssetsDataList
    };
    if (args.colId === "brokerReturnsUsd" ) {
      this.props.fetchReturnsDetailData(payload);
      this.props.handleViewToggle("returnsDetail");
    } else if(args.colId === "assetsValueUsd") {
      this.props.fetchAssetsDetailData(payload);
      this.props.handleViewToggle("assetsDetail");
    } else if(args.colId === "actions") {
      this.setState({
        showModal: true,
        selectedDate: this.props.selectedFilters.selectedDate,
        selectedCpeFamilyId: args.item.cpeFamilyId,
        selectedCpeFamily: args.item.cpeFamilyName,
        selectedCalculatedReturns: Math.round(args.item.brokerReturnsUsd),
        selectedCalculatedAssets: Math.round(args.item.assetsValueUsd),
        selectedAdjustedReturns: Math.round(args.item.cpeFamilyReturnOnAssetsAdjustment.adjustedReturns),
        selectedAdjustedAssets: Math.round(args.item.cpeFamilyReturnOnAssetsAdjustment.adjustedAssets),
        cpeFamilyAdjustmentsData: {}
      });
    }
  };

  updateAdjustmentStatus(adjustmentMessage, adjustmentMessageClass, adjustmentMessageHidden) {
    let newState = Object.assign({}, this.state);
    var data = {
      'adjustmentMessage' : adjustmentMessage,
      'adjustmentMessageClass': adjustmentMessageClass,
      'adjustmentMessageHidden': adjustmentMessageHidden
    };
    newState['cpeFamilyAdjustmentsData'] = data;
    this.setState(newState);
  }

  onAddAdjustment() {
    this.updateAdjustmentStatus('', '', true);

    if(this.state.selectedDate === undefined || this.state.selectedDate === null || this.state.selectedDate == "") {
      this.updateAdjustmentStatus('Date cannot be empty.', 'message--warning no-icon', false);
      return;
    }

    var returnsEmpty = validateInputEmpty(this.state.selectedAdjustedReturns);
    var assetsEmpty = validateInputEmpty(this.state.selectedAdjustedAssets)
    if(returnsEmpty && assetsEmpty) {
      this.updateAdjustmentStatus('Both adjusted returns and assets are empty. Nothing to update.', 'message--warning no-icon', false);
      return;
    }

    var valid = validateNumericInput(this.state.selectedAdjustedReturns);
    var valid2 = validateNumericInput(this.state.selectedAdjustedAssets);
    if(!valid || !valid2) {
      this.updateAdjustmentStatus('Adjusted Returns/Assets value has to a numeric value.', 'message--warning no-icon', false);
      return;
    }

    var payload = {
      date: this.state.selectedDate,
      cpeFamilyId: this.state.selectedCpeFamilyId,
      adjustedReturns: returnsEmpty ? "__null__" : this.state.selectedAdjustedReturns,
      adjustedAssets: assetsEmpty ? "__null__" : this.state.selectedAdjustedAssets,
      comment: this.state.selectedComment
    }
    this.updateAdjustmentStatus('', '', true);
    this.props.addCpeFamilyAdjustments(payload);

    this.props.destroyReturnOnAssetsData();
    var roaPayload = {
      date: this.state.selectedDate,
      cpeFamilyIds: getCommaSeparatedValuesOrEmpty([]),
      agreementTypeIds: getCommaSeparatedValuesOrEmpty(this.props.selectedFilters.selectedAgreementTypes)
    };
    this.props.fetchReturnOnAssetsData(roaPayload);
  }

  onCloseModal() {
    delete this.props.cpeFamilyAdjustmentsData.addAdjustmentStatus;
    this.setState(this.getDefaultFilters());
  }

  loadActionMessageDialogs() {
    var isDialogOpen = false;
    var messageTitle = '';
    var message = '';
    if('addAdjustmentStatus' in this.props.cpeFamilyAdjustmentsData) {
      isDialogOpen = true;
      if(this.props.cpeFamilyAdjustmentsData.addAdjustmentStatus) {
        message = 'Adjustment(s) added successfully. Return on Assets summary updated.';
        messageTitle = 'SUCCESS';
      } else {
        message = 'Error while adding adjustment(s).';
        messageTitle = 'ERROR';
      }
    }
    return (
      <React.Fragment>
        <Dialog
          isOpen={isDialogOpen}
          title={messageTitle}
          onClose={this.onCloseModal} >
          <Message error={!this.props.cpeFamilyAdjustmentsData.addAdjustmentStatus} success={this.props.cpeFamilyAdjustmentsData.addAdjustmentStatus}
            messageData={message} />
          <FilterButton onClick={this.onCloseModal} reset={false} label='Ok' />
        </Dialog>
      </React.Fragment>
    );
  }

  render() {
    if ('message' in this.props.returnOnAssetsData)
        return <Message messageData={this.props.returnOnAssetsData.message} />;

    if(this.props.returnOnAssetsData.length <= 0)
        return <Message messageData="Search to load data" />;

    var message = (this.state.cpeFamilyAdjustmentsData.adjustmentMessageHidden) ? '' : (<div className={this.state.cpeFamilyAdjustmentsData.adjustmentMessageClass} >
            {this.state.cpeFamilyAdjustmentsData.adjustmentMessage}  </div>);

    let grid = (
        <React.Fragment>
          <Layout>
            <Layout.Child>
                <ReturnOnAssetsDataGrid
                  data={this.props.returnOnAssetsData.data}
                  gridId="ReturnOnAssetsData"
                  onCellClick={this.onCellClickHandler}
                  gridColumns={roaGridColumns()}
                  gridOptions={roaGridOptions()}
                  resizeCanvas={this.props.resizeCanvas}
                />
              </Layout.Child>
                  <Layout.Child>
                  <div className="legend__item">
                  <span className="margin--left--small margin--right--double">
                    <b>Note : </b><br/>
                    All figure in USD.<br/>
                    The returns displayed are annualized numbers whereas the assets are based on 30 day rolling average. <br/>
                    <span class="red7"> * Negative ROA means client is creating liquidity </span>
                  </span>
                </div>
                  </Layout.Child>
                </Layout>
                <Dialog isOpen={this.state.showModal} title="Add Adjustments" onClose={this.onCloseModal}
                  style={{ width: "80px", height: "400px" }}
                >
                {message}
                <InputFilter
                  data={this.state.selectedCpeFamily}
                  stateKey="selectedCpeFamily"
                  label="CPE Family"
                  disable={true}
                />
                <DateFilter
                  onSelect={this.onSelect}
                  stateKey="selectedDate"
                  data={this.state.selectedDate}
                  disable={true}
                />
                <InputFilter
                  stateKey="selectedCalculatedReturns"
                  label="Calculated Returns"
                  data={this.state.selectedCalculatedReturns}
                  disable={true}
                />
                <InputFilter
                  onSelect={this.onSelect}
                  stateKey="selectedAdjustedReturns"
                  label="Adjusted Returns"
                  data={this.state.selectedAdjustedReturns}
                />
                <InputFilter
                  stateKey="selectedCalculatedAssets"
                  label="Calculated Assets"
                  data={this.state.selectedCalculatedAssets}
                  disable={true}
                />
                <InputFilter
                  onSelect={this.onSelect}
                  stateKey="selectedAdjustedAssets"
                  label="Adjusted Assets"
                  data={this.state.selectedAdjustedAssets}
                />
                <InputFilter
                  onSelect={this.onSelect}
                  stateKey="selectedComment"
                  label="Comment"
                  data={this.state.selectedComment}
                />
                <ColumnLayout>
                  <FilterButton
                    onClick={this.onAddAdjustment}
                    reset={false}
                    label="Save"
                    disabled={('addAdjustmentStatus' in this.props.cpeFamilyAdjustmentsData) ? true : false}
                  />
                  <FilterButton reset={true} onClick={this.onCloseModal} label="Cancel" />
                </ColumnLayout>
                </Dialog>
                {this.loadActionMessageDialogs()}
        </React.Fragment>
      );
      return grid;
  }
}

MainContainer.propTypes = {
  returnOnAssetsData: PropTypes.object,
  selectedFilters: PropTypes.array,
  fetchReturnsDetailData: PropTypes.func,
  fetchAssetsDetailData: PropTypes.func,
  handleViewToggle: PropTypes.func,
  fetchReturnOnAssetsData: PropTypes.func,
  destroyReturnOnAssetsData: PropTypes.func,
  updateDrillDownSelectedFilters: PropTypes.func,
  addCpeFamilyAdjustments: PropTypes.func,
  cpeFamilyAdjustmentsData: PropTypes.object
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchReturnsDetailData,
      fetchAssetsDetailData,
      addCpeFamilyAdjustments,
      fetchReturnOnAssetsData,
      destroyReturnOnAssetsData
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    returnOnAssetsData: state.returnOnAssetsData.returnOnAssetsData,
    selectedFilters: state.returnOnAssetsData.drillDownFilters,
    cpeFamilyAdjustmentsData: state.returnOnAssetsData.cpeFamilyAdjustmentsData
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainContainer);
