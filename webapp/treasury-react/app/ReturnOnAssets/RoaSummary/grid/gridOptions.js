export function roaGridOptions(paging) {
    var options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    showHeaderRow: true,
    exportToExcel: true,
    sortList: [{ columnId: "cpeFamilyName", sortAsc: true }],
    highlightRowOnClick: true,
    useAvailableScreenSpace : true,
  };
  return options;
}
