import {
  linkFormatter,
  decimalToPercentFormatter
} from "commons/grid/formatters";
import {decimalToPercentFormatterWithColorCode} from "../../commons/util";

export function roaGridColumns() {
  var columns = [{
      id: "cpeFamilyName",
      name: "Counterparty Family",
      field: "cpeFamilyName",
      toolTip: "cpeFamilyName",
      type: "text",
      filter: true,
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 90
    },
    //  TODO: To be removed once UAT approvals done.
    // {
    //   id: "actions",
    //   name: "Add Adjustments",
    //   field: "actions",
    //   type: "text",
    //   filter: true,
    //   sortable: true,
    //   formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
    //       return linkFormatter(
    //         row,
    //         cell,
    //         "<i class='icon-add' />",
    //         columnDef,
    //         dataContext
    //       );
    //   },
    //   headerCssClass: "b",
    //   width: 20
    // },
    {
      id: "brokerReturnsUsd",
      name: "Returns",
      field: "brokerReturnsUsd",
      toolTip: "Returns",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        var numValue = dpGrid.Formatters.Number(null, null, value, columnDef, null);
          return isExportToExcel ? numValue : linkFormatter(
            row,
            cell,
            numValue,
            columnDef,
            dataContext
          );
      },
      headerCssClass: "b",
      width: 50
    },
    {
      id: "assetsValueUsd",
      name: "Assets",
      field: "assetsValueUsd",
      toolTip: "Assets",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext, isExportToExcel) {
        var numValue = dpGrid.Formatters.Number(null, null, value, columnDef, null);
          return isExportToExcel ? numValue : linkFormatter(
            row,
            cell,
            numValue,
            columnDef,
            dataContext
          );
      },
      headerCssClass: "b",
      width: 50
    },
    {
      id: "adjustedReturns",
      name: "Adjusted Returns",
      field: "cpeFamilyReturnOnAssetsAdjustment",
      toolTip: "Adjusted Returns",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return dpGrid.Formatters.Number(row, cell, dataContext.cpeFamilyReturnOnAssetsAdjustment.adjustedReturns, columnDef, dataContext);
      },
      headerCssClass: "b",
      width: 50
    },
    {
      id: "adjustedAssets",
      name: "Adjusted Assets",
      field: "cpeFamilyReturnOnAssetsAdjustment",
      toolTip: "Adjusted Assets",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return dpGrid.Formatters.Number(row, cell, dataContext.cpeFamilyReturnOnAssetsAdjustment.adjustedAssets, columnDef, dataContext);
      },
      headerCssClass: "b",
      width: 50
    },
    {
      id: "returnOnAssets",
      name: "Return On Assets",
      field: "returnOnAssets",
      toolTip: "Return On Assets",
      type: "number",
      filter: true,
      sortable: true,
      formatter: decimalToPercentFormatterWithColorCode,
      headerCssClass: "b",
      width: 50
    }
  ];

  return columns;
}
