import React from "react";
import { observer } from "mobx-react";
import { toJS } from "mobx";
import Message from "commons/components/Message";
import GridWithCellClick from "commons/components/GridWithCellClick";
import _ from "lodash";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import { Layout } from "arc-react-components";
import { gridOptions, drillDownGridOptions } from "../grid/DetailGridConfig";
import { recommendationDetailColumns, getDrillDownColumns } from "../grid/DetailGridColumns";
import { Panel } from "arc-react-components";

const RecommendationDetailGrid: React.FC<IndebtednessProps> = (
  props: IndebtednessProps
) => {

  const onCellClickHandler = (args) => {
    let ele = args.event.target;
    let { role } = ele.dataset;

    if (role === "drillDown") {
      props.store.isRecommendationDetailDrillDownShown = true;
      props.store.selectedRowDescription = args.item.description;

      let drillData = _.cloneDeep(args.item.axeBrokerData);
      if (typeof drillData !== 'undefined') {
        for (var j = 0; j < drillData.length; j++) {
          drillData[j].id = j + 1;
        }
        props.store.recommendationDetailDrillDownData = drillData;
      } else {
        props.store.recommendationDetailDrillDownData = [];
      }
      props.store.resizeCanvas = !props.store.resizeCanvas;
    }

  };

  function loadDrillDownBottomGrid() {
    var title = `Drill Down For [${props.store.selectedRowDescription}]`;
    var content = props.store.recommendationDetailDrillDownData;
    let drillDown, grid;
    if (!content.length) {
      let noDataMessage = "No drill down data available.";
      grid = (<Message messageData={noDataMessage} />);
    } else {
      grid = (<GridWithCellClick
        gridColumns={getDrillDownColumns()}
        onCellClick={() => {}}
        gridId="recommendationDetailDrillDownGrid"
        gridOptions={drillDownGridOptions()}
        data={content}
        heightValue={200}
      />);
    }

    drillDown = (
      <Panel
        className={`position--relative margin--top size--1`}
        dismissible
        onClose={() => {
          props.store.isRecommendationDetailDrillDownShown = false;
          props.store.resizeCanvas = !props.store.resizeCanvas;
        }}
        title={title}
      >
        <br />
        {grid}
      </Panel>
    );
    return drillDown;
  }

  const renderGridData = () => {
    let recommendationDetailData = props.store.recommendationDetailData;

    if (props.store.sideBarStore.searchStatus.error) {
      return <Message messageData="Error occurred while loading data." />;
    }

    if (!recommendationDetailData.length) {
      let noDataMessage = "Please search to load data";
      if (props.store.sideBarStore.searchStatus.firstSearch == false) {
        noDataMessage = "No data available for search criteria";
      }
      return <Message messageData={noDataMessage} />;
    }
    let flattenedData = props.store.getFlattenedRecommendationDetailData(recommendationDetailData)
    let RC = flattenedData[0].reportingCurrency
    let message = (
      <small><ul className="bullet">
        <li>{`Reporting Currency (RC) corresponds to ${RC}`}</li>
      </ul></small>
    );
    if (RC == undefined) {
      message = (<ul className="bullet">
        <li className="critical">{`Fx Rate unavailable for selected Reporting Currency`}</li>
      </ul>);
    }
    let grid = (
      <div className="layout--flex--row">
        <div className="size--2">
            <GridWithCellClick
              data={flattenedData}
              gridId="recommendationDetailGrid"
              gridColumns={recommendationDetailColumns()}
              gridOptions={gridOptions()}
              onCellClick={onCellClickHandler}
              fill
              resizeCanvas={props.store.resizeCanvas}
            />
        </div>
        <div className="size--content">{message}</div>
        {props.store.isRecommendationDetailDrillDownShown && loadDrillDownBottomGrid()}
      </div>
    );

    return (
      <React.Fragment>
        {grid}
      </React.Fragment>
    );

  };

  return (
    <React.Fragment>
      {renderGridData()}
    </React.Fragment>
  );
};

export default observer(RecommendationDetailGrid);
