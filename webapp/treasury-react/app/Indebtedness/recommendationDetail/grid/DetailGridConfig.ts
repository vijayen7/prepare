import { getSaveSettingsUrl } from "commons/util";
import { recommendationDetailColumns } from "./DetailGridColumns";
import { rehypothecationDetailUrl } from "../../api";

export function gridOptions() {
  let options = {
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    exportToExcel: true,
    autoHorizontalScrollBar: true,
    frozenColumn: 5,
    summaryRow: true,
    displaySummaryRow: true,
    highlightRowOnClick: true,
    highlightRow: true,
    page: true,
    configureReport: {
      urlCallback: function() {
        return window.location.origin + rehypothecationDetailUrl;
      }
    },
    sortList: [
      {
        columnId: "rehypoRank",
        sortAsc: true,
      },
    ],
    sheetName: "Rehypothecation_Recommendation_Detail_Data",
    saveSettings: {
      applicationId: 3,
      applicationCategory: "RecommendationData",
      serviceURL: getSaveSettingsUrl(),
      onBeforeLoadedCallback: function (settingsData) {},
      loadedCallback: function (gridObject) {},
    },
    customColumnSelection: {
      defaultcolumns: getDefaultColumns(),
      totalColumns: getTotalColumns(),
    }
  };

  return options;
}

function getDefaultColumns() {
  return [
    "date",
    "spn",
    "legalEntity",
    "agreementType",
    "cpe",
    "description",
    "eligibleQuantity",
    "eligibleMarketValueRC",
    "price",
    "currentRehypoQuantity",
    "currentRehypoMarketValueRC",
    "availableQuantity",
    "availableMarketValueRC",
    "fxRate",
    "rehypoRank",
    "custodianAccount",
    "currency",
    "index",
    "drillDown",
    "axeQuantity",
    "axeMarketValueRC",
    "axeBorrowRate",
    "hotStockQuantity",
    "hotStockRate",
    "knowledgeStartDate",
    "isin",
    "cusip",
    "sedol",
  ];
}

function getTotalColumns() {
  return recommendationDetailColumns().map((column) => [column.id, column.name]);
}

export function drillDownGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    highlightRowOnClick: true,
    applyFilteringOnGrid: true,
    exportToExcel: true
  };

  return options;
}
