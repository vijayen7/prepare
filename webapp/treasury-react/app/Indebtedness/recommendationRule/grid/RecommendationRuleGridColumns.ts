import { convertJavaDateDashed, formatterFourDecimal } from "commons/util";
import { numberFormatterDefaultZero } from "commons/grid/formatters";

export function getCommonColumns() {
  return [
    {
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreementType",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 80,
      excelFormatter: "#,##0",
    },
    {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legalEntity",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 100,
      excelFormatter: "#,##0",
    },
    {
      id: "cpe",
      type: "text",
      name: "Counterparty",
      field: "cpe",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    },
    {
      id: "custodianAccountName",
      type: "text",
      name: "Custodian Account",
      field: "custodianAccountName",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    },
    {
      id: "validFrom",
      type: "text",
      name: "Valid From",
      field: "effective_start_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 80,
      excelFormatter: "#,##0",
    },
    {
      id: "validTill",
      type: "text",
      name: "Valid Till",
      field: "effective_end_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter(row, cell, value, columnDef, dataContext) {
        if (value === "20380101") {
          return "-";
        }
        return value;
      },
      width: 60,
      excelFormatter: "#,##0",
    },
  ];
}

function getActionColumn() {
  return [
    {
      id: "actions",
      name: "Actions",
      exportToExcel: false,
      sortable: false,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return (
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        );
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true,
    },
  ];
}

export function getRecommendationRuleColumns() {
  let columns = getActionColumn();
  columns = (columns as any[]).concat(getCommonColumns());
  return columns;
}

export function getRecommendationRuleSimulatedColumns() {
  const columns = getCommonColumns();
  return columns;
}

export function getRecommendationSubRuleColumns(editable) {
  return [
    {
      id: "rank",
      type: "text",
      name: "rank",
      field: "rank",
      sortable: false,
      headerCssClass: "aln-rt b",
      width: 80,
      excelFormatter: "#,##0",
      isEditable: editable,
      editor: dpGrid.Editors.Text,
      editorOptions: {
        showEditIcon: editable,
      },
    },
    {
      id: "attributeName",
      type: "text",
      name: "Attribute",
      field: "attributeName",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 100,
      excelFormatter: "#,##0",
    },
    {
      id: "type",
      type: "text",
      name: "type",
      field: "type",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    },
    {
      id: "attributeValueDisplay",
      type: "text",
      name: "Value",
      field: "attributeValueDisplay",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    },
  ];
}

export function getSubRulesWithActionColumn() {
  let columns = [
    {
      id: "actions",
      name: "Actions",
      exportToExcel: false,
      sortable: false,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return (
          "<a title='Increase Priority'><i data-role='up' class='icon-up' /></a>  " +
          "  <a title='Decrease Priority'><i data-role='down' class='icon-down margin--right' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        );
      },
      width: 70,
      minWidth: 70,
      maxWidth: 90,
      fixed: true,
    },
  ];
  columns = (columns as any[]).concat(getRecommendationSubRuleColumns(true));
  return columns;
}
