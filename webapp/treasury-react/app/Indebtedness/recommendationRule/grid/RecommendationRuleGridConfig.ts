export function getRecommendationRuleGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    highlightRowOnClick: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "",
    sortList: [
      { columnId: "agreementType", sortAsc: true },
      { columnId: "cpe", sortAsc: true },
    ],
  };
  return options;
}

export function getRecommendationSubRuleGridOptions(editable) {
  var options = {
    autoHorizontalScrollBar: true,
    highlightRowOnClick: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: !editable,
    exportToExcel: true,
    editable: editable
      ? {
          changeCellStyleOnEdit: true,
        }
      : false,
  };
  return options;
}
