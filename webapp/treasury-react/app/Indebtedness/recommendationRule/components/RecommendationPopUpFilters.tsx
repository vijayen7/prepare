import React, { Component } from "react";
import _ from "lodash";
import { Layout } from "arc-react-components";
import DateFilter from "commons/container/DateFilter";
import InputNumberFilter from "commons/components/InputNumberFilter";
import { observer } from "mobx-react";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import { getDisabledFilter, getCommaSeparatedValuesDisplay } from "../../util";
import { getCommaSeparatedValues } from "commons/util";
import CustodianAccountFilter from "commons/container/CustodianAccountFilter";
import FilterButton from "commons/components/FilterButton";
import RehypothecationRuleAttributeFilter from "commons/container/RehypothecationRuleAttributeFilter";
import RehypothecationRuleTypeFilter from "commons/container/RehypothecationRuleTypeFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import IndebtednessAgreementTypeFilter from "commons/container/IndebtednessAgreementTypeFilter";
import CountryFilter from "commons/container/CountryFilter";
import InputFilter from "commons/components/InputFilter";

const RecommendationPopUpFilters: React.FC<IndebtednessProps> = (props: IndebtednessProps) => {
  function handleAdd() {
    var subRules: any = _.cloneDeep(props.store.recommendationRulePopUpStore.subRuleGridContent);
    var filters = props.store.recommendationRulePopUpStore.filters;
    var id = subRules.length == 0 ? 1 : subRules.slice(-1)[0].id + 1;
    var newRule: any = {
      id: id,
      rank: id,
      attributeName: filters.selectedRehypothecationRuleAttributes.value,
      type: filters.selectedRehypothecationRuleTypes.value,
    };

    switch (props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleAttributes.value) {
      case "Average Borrow Cost":
      case "Index Description":
      case "Hot Stock Fee":
        newRule.attributeValue = filters.selectedAttributeValue;
        newRule.attributeValueDisplay = filters.selectedAttributeValue;
        break;
      case "Country of Security":
      case "Axe List":
        newRule.attributeValue = getCommaSeparatedValues([filters.selectedAttributeValue]);
        newRule.attributeValueDisplay = getCommaSeparatedValuesDisplay([filters.selectedAttributeValue]);
        break;
    }
    subRules.push(newRule);
    props.store.recommendationRulePopUpStore.subRuleGridContent = subRules;
  }

  const loadAttributeValueFilter = () => {
    let filter = <React.Fragment />;
    if (
      typeof props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleAttributes !== "undefined" &&
      props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleAttributes != null
    ) {
      switch (props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleAttributes.value) {
        case "Average Borrow Cost":
          filter = (
            <InputNumberFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              data={props.store.recommendationRulePopUpStore.filters.selectedAttributeValue}
              stateKey="selectedAttributeValue"
              label="Attribute Value"
              layoutStyle="standard"
            />
          );
          break;
        case "Country of Security":
          filter = (
            <CountryFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              selectedData={props.store.recommendationRulePopUpStore.filters.selectedAttributeValue}
              stateKey={"selectedAttributeValue"}
              multiSelect={false}
              horizontalLayout={true}
              label={"Country"}
            />
          );
          break;
        case "Axe List":
          filter = (
            <CpeFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              selectedData={props.store.recommendationRulePopUpStore.filters.selectedAttributeValue}
              stateKey={"selectedAttributeValue"}
              multiSelect={false}
              horizontalLayout={true}
              label={"Attribute Value"}
            />
          );
          break;
        case "Index Description":
          filter = (
            <InputNumberFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              data={props.store.recommendationRulePopUpStore.filters.selectedAttributeValue}
              stateKey={"selectedAttributeValue"}
              label={"Index"}
            />
          );
          break;
        case "Hot Stock Fee":
          filter = (
            <InputNumberFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              data={props.store.recommendationRulePopUpStore.filters.selectedAttributeValue}
              stateKey="selectedAttributeValue"
              label="Attribute Value"
            />
          );
          break;
      }
    }
    return (
      <React.Fragment>
        {filter}
        <FilterButton
          onClick={handleAdd}
          disabled={
            _.isEmpty(props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleAttributes) ||
            _.isEmpty(props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleTypes) ||
            _.isEmpty(props.store.recommendationRulePopUpStore.filters.selectedAttributeValue)
          }
          label="Add"
        />
      </React.Fragment>
    );
  };

  const renderGridData = () => {
    let selectedCAForEdit;
    if (props.store.recommendationRulePopUpStore.filters.selectedCustodianAccounts.length != 0) {
      selectedCAForEdit = props.store.recommendationRulePopUpStore.filters.selectedCustodianAccounts[0].value
    }
    let filters = (
      <Layout isRowType>
        <Layout.Child childId="recommendationPopup1">
          {props.store.recommendationRulePopUpStore.isEditClicked ? (
            getDisabledFilter("Legal Entity:", props.store.recommendationRulePopUpStore.filters.selectedLegalEntities.value)
          ) : (
            <LegalEntityFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              selectedData={props.store.recommendationRulePopUpStore.filters.selectedLegalEntities}
              multiSelect={false}
              horizontalLayout
            />
          )}
          {props.store.recommendationRulePopUpStore.isEditClicked ? (
            getDisabledFilter("CPEs:", props.store.recommendationRulePopUpStore.filters.selectedCpes.value)
          ) : (
            <CpeFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              selectedData={props.store.recommendationRulePopUpStore.filters.selectedCpes}
              multiSelect={false}
              horizontalLayout
            />
          )}
          {props.store.recommendationRulePopUpStore.isEditClicked ? (
            getDisabledFilter(
              "Agreement Type:",
              props.store.recommendationRulePopUpStore.filters.selectedAgreementTypes.value
            )
          ) : (
            <IndebtednessAgreementTypeFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              selectedData={props.store.recommendationRulePopUpStore.filters.selectedAgreementTypes}
              multiSelect={false}
              horizontalLayout
            />
          )}
          {props.store.recommendationRulePopUpStore.isEditClicked ? (
            getDisabledFilter(
              "Custodian Account:",
              selectedCAForEdit
            )
          ) : (
            <CustodianAccountFilter
              onSelect={props.store.recommendationRulePopUpStore.onSelect}
              selectedData={props.store.recommendationRulePopUpStore.filters.selectedCustodianAccounts}
              multiSelect={true}
              horizontalLayout
            />
          )}
          <DateFilter
            onSelect={props.store.recommendationRulePopUpStore.onSelect}
            stateKey="date"
            data={props.store.recommendationRulePopUpStore.filters.date}
            label={props.store.recommendationRulePopUpStore.isDeleteClicked ? "Effective Till" : "Date"}
          />
          <br />
          <hr />
          {props.store.recommendationRulePopUpStore.isDeleteClicked ? (
            <React.Fragment />
          ) : (
            <React.Fragment>
              <RehypothecationRuleAttributeFilter
                onSelect={props.store.recommendationRulePopUpStore.onSelect}
                selectedData={props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleAttributes}
                multiSelect={false}
                horizontalLayout
              />
              <RehypothecationRuleTypeFilter
                onSelect={props.store.recommendationRulePopUpStore.onSelect}
                selectedData={props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleTypes}
                multiSelect={false}
                attribute={props.store.recommendationRulePopUpStore.filters.selectedRehypothecationRuleAttributes.key}
                horizontalLayout
              />
              <br />
              {loadAttributeValueFilter()}
            </React.Fragment>
          )}
        </Layout.Child>
      </Layout>
    );

    return filters;
  };

  return <>{renderGridData()}</>;
};

export default observer(RecommendationPopUpFilters);
