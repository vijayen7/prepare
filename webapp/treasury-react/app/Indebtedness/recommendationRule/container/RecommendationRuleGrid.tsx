import React from "react";
import { observer } from "mobx-react";
import Message from "commons/components/Message";
import GridWithCellClick from "commons/components/GridWithCellClick";
import _ from "lodash";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import RecommendationRulePopUp from "./RecommendationRulePopUp";
import { getRecommendationRuleGridOptions, getRecommendationSubRuleGridOptions } from "../grid/RecommendationRuleGridConfig";
import { getRecommendationRuleColumns, getRecommendationSubRuleColumns } from "../grid/RecommendationRuleGridColumns";
import { Panel } from "arc-react-components";

const RecommendationRuleGrid: React.FC<IndebtednessProps> = (props: IndebtednessProps) => {
  const onCellClickHandler = (args) => {
    let ele = args.event.target;
    let { role } = ele.dataset;
    let subRules = _.cloneDeep(args.item.output);
    for (var j = 0; j < subRules.length; j++) {
      subRules[j].id = j + 1;
    }
    if (role === "edit" || role === "delete") {
      props.store.recommendationRulePopUpStore.isEditClicked = true;
      props.store.selectedGridRow = args.item;
      populatePopUpFilters();
      if (role === "delete") {
        props.store.recommendationRulePopUpStore.isDeleteClicked = true;
      }
      if (role === "edit") {
        props.store.recommendationRulePopUpStore.subRuleGridContent = subRules;
      }
      props.store.recommendationRulePopUpStore.isPopUpOpen = true;
    }

    let title = getSubRulesTitle(args.item);
    props.store.subRuleGridContent = subRules;
    props.store.subRuleGridTitle = title;
    props.store.isSubRuleGridShown = true;
  };

  function getSubRulesTitle(item) {
    let title = "Rule Configuration ";
    if (typeof item.agreementType !== "undefined" && item.agreementType !== "") {
      title += `|  Agreement Type [${item.agreementType}] `;
    }
    if (typeof item.cpe !== "undefined" && item.cpe !== "") {
      title += `|  Counterparty [${item.cpe}] `;
    }
    if (typeof item.legalEntity !== "undefined" && item.legalEntity !== "") {
      title += `|  Legal Entity [${item.legalEntity}] `;
    }
    if (typeof item.custodianAccountName !== "undefined" && item.custodianAccountName !== "") {
      title += `|  Custodian Account [${item.custodianAccountName}] `;
    }
    return title;
  }

  const populatePopUpFilters = () => {
    const rowData: any = props.store.selectedGridRow;
    let populateValues = {};
    if (typeof rowData.legal_entity_id !== "undefined" && rowData.legal_entity_id !== "") {
      props.store.recommendationRulePopUpStore.filters.selectedLegalEntities = {
        key: rowData.legal_entity_id,
        value: `${rowData.legalEntity} [${rowData.legal_entity_id}]`,
      };
    }
    if (typeof rowData.cpe_id !== "undefined" && rowData.cpe_id !== "") {
      props.store.recommendationRulePopUpStore.filters.selectedCpes = {
        key: rowData.cpe_id,
        value: `${rowData.cpe} [${rowData.cpe_id}]`,
      };
    }
    if (typeof rowData.agreement_type_id !== "undefined" && rowData.agreement_type_id !== "") {
      props.store.recommendationRulePopUpStore.filters.selectedAgreementTypes = {
        key: rowData.agreement_type_id,
        value: `${rowData.agreementType} [${rowData.agreement_type_id}]`,
      };
    }
    if (typeof rowData.custodian_account_id !== "undefined" && rowData.custodian_account_id !== "") {
      props.store.recommendationRulePopUpStore.filters.selectedCustodianAccounts = [{
        key: rowData.custodian_account_id,
        value: `${rowData.custodianAccountName} [${rowData.custodian_account_id}]`,
      }];
    }
  };

  function loadSubRulesBottomGrid() {
    var title = props.store.subRuleGridTitle;
    var content = props.store.subRuleGridContent;
    let drillDown;
    drillDown = (
      <Panel
        className={`position--relative margin--top size--1`}
        dismissible
        onClose={() => {
          props.store.isSubRuleGridShown = false;
        }}
        title={title}
      >
        <br />
        <GridWithCellClick
          gridColumns={getRecommendationSubRuleColumns(false)}
          onCellClick={() => {}}
          gridId="recommendationSubRuleGrid"
          gridOptions={getRecommendationSubRuleGridOptions(false)}
          data={content}
          heightValue={200}
        />
      </Panel>
    );
    return drillDown;
  }

  const renderGridData = () => {
    let recommendationRuleData = props.store.recommendationRuleDataExistingList;

    if (props.store.sideBarStore.searchStatus.error) {
      return <Message messageData="Error occurred while loading data." />;
    }

    if (!recommendationRuleData.length) {
      let noDataMessage = "Please search to load data";
      if (props.store.sideBarStore.searchStatus.firstSearch == false) {
        noDataMessage = "No data available for search criteria";
      }
      return <Message messageData={noDataMessage} />;
    }

    let grid = (
      <div className="layout--flex--row">
        <div className="size--2">
            <GridWithCellClick
              gridColumns={getRecommendationRuleColumns()}
              onCellClick={onCellClickHandler}
              gridId="recommendationRuleGrid"
              gridOptions={getRecommendationRuleGridOptions()}
              data={recommendationRuleData}
              fill={true}
              label = "Please click on a row to view the rules configured"
            />
        </div>
        {props.store.isSubRuleGridShown && loadSubRulesBottomGrid()}
      </div>
    );

    return <React.Fragment>{grid}</React.Fragment>;
  };

  return (
    <React.Fragment>
      {renderGridData()}
      <RecommendationRulePopUp store={props.store} />
    </React.Fragment>
  );
};

export default observer(RecommendationRuleGrid);
