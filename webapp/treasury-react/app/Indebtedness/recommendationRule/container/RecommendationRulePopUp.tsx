import React from "react";
import { observer } from "mobx-react";
import FilterButton from "commons/components/FilterButton";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import RecommendationPopUpFilters from "../components/RecommendationPopUpFilters.tsx";
import { Card } from "arc-react-components";
import Dialog from "commons/components/Dialog";
import { ReactArcGrid } from "arc-grid";
import { getRecommendationSubRuleGridOptions } from "../grid/RecommendationRuleGridConfig";
import { getSubRulesWithActionColumn } from "../grid/RecommendationRuleGridColumns";
import _ from "lodash";

const RecommendationRulePopUp: React.FC<IndebtednessProps> = (props: IndebtednessProps) => {
  const handleClosePopUp = () => {
    props.store.recommendationRulePopUpStore.isPopUpOpen = false;
    props.store.recommendationRulePopUpStore.handleReset();
  };

  const onCellClickHandler = (args) => {
    let ele = args.event.target;
    let { role } = ele.dataset;
    let subRules: any = _.cloneDeep(props.store.recommendationRulePopUpStore.subRuleGridContent);
    if (role === "up" || role === "down" || role === "delete") {
      if (role === "up") {
        swapPositions(subRules, args.row, args.row - 1);
      }
      if (role === "down") {
        swapPositions(subRules, args.row + 1, args.row);
      }
      if (role === "delete") {
        subRules.splice(args.row, 1);
      }
      for (var j = 0; j < subRules.length; j++) {
        subRules[j].id = j + 1;
        subRules[j].rank = j + 1;
      }
      props.store.recommendationRulePopUpStore.subRuleGridContent = subRules;
    }
  };

  const onDblClickHandler = (args) => {
    props.store.recommendationRulePopUpStore.dblClickRowId = args.rank
  }

  const swapPositions = (array, a, b) => {
    if (b != -1 && a < array.length) [array[a], array[b]] = [array[b], array[a]];
  };

  const handleDelete = () => {
    let columnValues = props.store.selectedGridRow.columnValues;
    let dateArray = columnValues[0].split("-");
    dateArray[1] = props.store.recommendationRulePopUpStore.filters.date.replace(/-/g, "");
    columnValues[0] = dateArray.join("-");
    props.store.handleDeleteRecommendationRule();
  };

  const renderGridData = () => {
    let subRules = props.store.recommendationRulePopUpStore.subRuleGridContent;
    let subRulesGrid = <React.Fragment />;
    if (!_.isEmpty(subRules)) {
      subRulesGrid = (
        <div style={{ height: "200px" }}>
          <ReactArcGrid
            columns={getSubRulesWithActionColumn()}
            onCellClick={onCellClickHandler}
            onDblClick={onDblClickHandler}
            gridId="recommendationSubRulePopupGrid"
            options={getRecommendationSubRuleGridOptions(true)}
            data={subRules}
          />
        </div>
      );
    }
    let popup = (
      <React.Fragment>
        <Dialog
          isOpen={props.store.recommendationRulePopUpStore.isPopUpOpen}
          onClose={handleClosePopUp}
          style={{
            width: "970px",
          }}
          footer={
            props.store.recommendationRulePopUpStore.isDeleteClicked ? (
              <FilterButton onClick={handleDelete} label={"Delete Rule"} />
            ) : (
              <React.Fragment>
                <FilterButton
                  onClick={props.store.handleSaveRecommendationRule}
                  disabled={_.isEmpty(props.store.recommendationRulePopUpStore.subRuleGridContent)}
                  label="Save Rule"
                />
              </React.Fragment>
            )
          }
        >
          <React.Fragment>
            <Card title="Rule Configuration">
              <hr />
              <div style={{ width: "550px" }}>
                <RecommendationPopUpFilters store={props.store} />
              </div>
            </Card>
            <br />
            {subRulesGrid}
          </React.Fragment>
        </Dialog>
      </React.Fragment>
    );

    return <React.Fragment>{popup}</React.Fragment>;
  };
  return <>{renderGridData()}</>;
};

export default observer(RecommendationRulePopUp);
