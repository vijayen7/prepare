import IndebtednessStore from "./store";
import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ReactLoader } from "../commons/components/ReactLoader";
import Loader from "../commons/container/Loader";
import AppHeader, { whenHeaderRef } from "arc-header";
import IndebtednessRuleGrid from "./indebtednessRule/container/RuleGrid";
import RecommendationRuleGrid from "./recommendationRule/container/RecommendationRuleGrid";
import IndebtednessDetailGrid from "./indebtednessDetail/container/DetailGrid";
import RecommendationDetailGrid from "./recommendationDetail/container/DetailGrid";
import IndebtednessSideBar from "./container/SideBar";
import { Layout, Breadcrumbs } from "arc-react-components";
import { extendObservable } from "mobx";
import { useLocalStore } from "mobx-react-lite";

const dummyObservable = Symbol("dummy-observable");

const updateBreadCrumbs = async () => {
  const headerRef = await whenHeaderRef();
  headerRef.setBreadcrumbs(
    <>
      <Breadcrumbs.Item link="/" key="home">
        <i className="icon-home" />
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link={"/treasury"} key="treasury">
        Treasury
      </Breadcrumbs.Item>
      <Breadcrumbs.Item link="/treasury/indebtedness.html" key="indebtedness">
        Indebtedness
      </Breadcrumbs.Item>
    </>
  );
};

const Indebtedness: React.FC = () => {
  useEffect(() => {
    updateBreadCrumbs();
  }, []);
  const indebtednessStore = useLocalStore(() =>
    extendObservable(new IndebtednessStore(), {
      [dummyObservable]: true,
    })
  );

  const applicationMenu = (
    <div className="application-menu-toggle-view">
      <a
        id="indebtednessRule"
        onClick={() => indebtednessStore.changeView("rule")}
        className={indebtednessStore.view == "rule" ? "active" : ""}
      >
        Indebtedness Rule
      </a>
      <a
        id="indebtednessDetail"
        onClick={() => indebtednessStore.changeView("detail")}
        className={indebtednessStore.view == "detail" ? "active" : ""}
      >
        Indebtedness Detail
      </a>
      <a
        id="recommendationRule"
        onClick={() => indebtednessStore.changeView("recommendationRule")}
        className={indebtednessStore.view == "recommendationRule" ? "active" : ""}
      >
        Rehypothecation Recommendation Rule
      </a>
      <a
        id="recommendationDetail"
        onClick={() => indebtednessStore.changeView("recommendationDetail")}
        className={indebtednessStore.view == "recommendationDetail" ? "active" : ""}
      >
        Rehypothecation Recommendation Detail
      </a>
    </div>
  );
  const openBulkInsert = () => {
    window.open("/cocoa/#/?parserNames=treasury_indebtedness_rules_update");
  };

  let mainGrid = <React.Fragment />;

  if (indebtednessStore.view == "detail") {
    mainGrid = <IndebtednessDetailGrid store={indebtednessStore} />;
  } else if (indebtednessStore.view == "rule") {
    mainGrid = (
      <Layout>
        <Layout.Child size="fit" childId="indebtednessRuleButton">
          <div style={{ textAlign: "right" }}>
            <button onClick={() => (indebtednessStore.isPopUpOpen = true)}>Add Rules</button>
            <button onClick={openBulkInsert} className="margin--left">
              Bulk Insert
            </button>
          </div>
        </Layout.Child>
        <Layout.Child childId="indebtednessRuleGrid">
          <IndebtednessRuleGrid store={indebtednessStore} />
        </Layout.Child>
      </Layout>
    );
  } else if (indebtednessStore.view == "recommendationRule") {
    mainGrid = (
      <Layout>
        <Layout.Child size="fit" childId="recommendationRuleButton">
          <div style={{ textAlign: "right" }}>
            <button onClick={() => (indebtednessStore.recommendationRulePopUpStore.isPopUpOpen = true)}>Add Rules</button>
          </div>
        </Layout.Child>
        <Layout.Child childId="recommendationRuleGrid">
          <RecommendationRuleGrid store={indebtednessStore} />
        </Layout.Child>
      </Layout>
    );
  } else if (indebtednessStore.view == "recommendationDetail") {
    mainGrid = <RecommendationDetailGrid store={indebtednessStore} />;
  }

  return (
    <>
      <Loader />
      <ReactLoader inProgress={indebtednessStore.isSearchInProgress} />
      <div className="layout--flex--row">
        <AppHeader className="size--content" globalFilters={false} applicationMenu={applicationMenu} />
        <Layout isColumnType={true} style={{ height: "100%" }}>
          <Layout.Child size={2} key="child1" title="Search Criteria" childId="sideBar" collapsible showHeader>
            <IndebtednessSideBar store={indebtednessStore} />
          </Layout.Child>
          <Layout.Divider isResizable key="child2" childId="divider" />
          <Layout.Child size={8} key="child3" childId="mainGrid">
            {mainGrid}
          </Layout.Child>
        </Layout>
      </div>
    </>
  );
};

export default observer(Indebtedness);
