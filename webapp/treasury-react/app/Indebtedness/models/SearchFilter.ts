import ReferenceData from "./../../commons/models/ReferenceData";

export interface SearchFilter {
  date: string;
  selectedLegalEntities?: Array<ReferenceData>;
  selectedAgreementTypes?: Array<ReferenceData>;
  selectedCustodianAccounts?: Array<ReferenceData>;
  selectedCpes?: Array<ReferenceData>;
  selectedReportingCurrency?: object;
}

export interface SearchStatus {
  firstSearch: boolean;
  inProgress: boolean;
  error: boolean;
}
