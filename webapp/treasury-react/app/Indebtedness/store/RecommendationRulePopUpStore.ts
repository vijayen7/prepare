import { observable, action, observe } from "mobx";
import { getFirstDateOfCurrentMonth } from "../../commons/util";
import { deepObserve } from "mobx-utils";
import _ from "lodash";

export const INITIAL_FILTERS = {
  date: getFirstDateOfCurrentMonth(),
  selectedLegalEntities: [] as any,
  selectedAgreementTypes: [] as any,
  selectedCustodianAccounts: [] as any,
  selectedCpes: [] as any,
  selectedRehypothecationRuleAttributes: [] as any,
  selectedRehypothecationRuleTypes: [] as any,
  selectedAttributeValue: [] as any,
  selectedRowData: [] as any,
};

export class RecommendationRulePopUpStore {
  // Observale Variables
  @observable filters = INITIAL_FILTERS;
  @observable isEditClicked: Boolean = false;
  @observable isDeleteClicked: Boolean = false;
  @observable isSimulated: Boolean = false;
  @observable isSaveButtonDisabled: Boolean = true;
  @observable isAddDisabled: Boolean = true;
  @observable existingRuleList = [];
  @observable simulatedRuleList = [];
  @observable isPopUpOpen: Boolean = false;
  @observable subRuleGridContent = [];
  @observable dblClickRowId = -1;

  // Action Setters

  @action setFilters(newSearchFilters) {
    this.filters = newSearchFilters;
  }

  constructor() {
    deepObserve(this, (change: any) => {
      if (change.name === "rank" && this.dblClickRowId != -1) {
        let subRules: any = _.cloneDeep(this.subRuleGridContent);
        let rule = subRules.splice(change.oldValue - 1, 1)[0];
        subRules.splice(change.newValue - 1, 0, rule);
        for (var j = 0; j < subRules.length; j++) {
          subRules[j].id = j + 1;
          subRules[j].rank = j + 1;
        }
        this.dblClickRowId = -1;
        this.subRuleGridContent = subRules;
      }
    });
  }

  // Action Methods
  @action.bound handleReset() {
    this.resetFilters();
    this.isEditClicked = false;
    this.isDeleteClicked = false;
    this.isSimulated = false;
    this.isSaveButtonDisabled = true;
    this.existingRuleList = [];
    this.simulatedRuleList = [];
    this.subRuleGridContent = [];
    this.dblClickRowId = -1;
  }

  @action resetFilters() {
    this.filters = INITIAL_FILTERS;
  }

  @action.bound onSelect({ key, value }: any) {
    this.resetDependentFilters(key, value.value);
    const newFilters = Object.assign({}, this.filters);
    newFilters[key] = value;
    this.setFilters(newFilters);
  }

  resetDependentFilters(key, value) {
    if (key === "selectedRehypothecationRuleAttributes") {
      this.filters.selectedAttributeValue = [];
      if (value == "Average Borrow Cost" || value == "Hot Stock Fee") {
        this.filters.selectedRehypothecationRuleTypes = {
          key: "MIN",
          value: "MIN",
        };
      } else {
        this.filters.selectedRehypothecationRuleTypes = {
          key: "INCLUDE",
          value: "INCLUDE",
        };
      }
    }
  }
}

export default RecommendationRulePopUpStore;
