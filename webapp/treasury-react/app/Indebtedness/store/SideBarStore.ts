import { observable, action } from "mobx";
import { SearchFilter, SearchStatus } from "../models/SearchFilter";
import { getCurrentDate } from "../../commons/util";

export const INITIAL_SEARCH_STATUS = {
  firstSearch: true,
  inProgress: false,
  error: false,
};

export const INITIAL_FILTERS = {
  date: getCurrentDate(),
  selectedLegalEntities: [],
  selectedAgreementTypes: [],
  selectedCustodianAccounts: [],
  selectedCpes: [] as any,
  selectedReportingCurrency: { key: 1760000, value: `USD [1760000]`},
};

export class SideBarStore {
  // Observale Variables
  @observable toggleSidebar: boolean = false;
  @observable searchStatus: SearchStatus = INITIAL_SEARCH_STATUS;
  @observable filters: SearchFilter = INITIAL_FILTERS;

  // Action Setters

  @action setFilters(newSearchFilters: SearchFilter) {
    this.filters = newSearchFilters;
  }

  @action setSearchStatus(searchStatus: SearchStatus) {
    this.searchStatus = searchStatus;
  }

  @action setToggleSidebar() {
    this.toggleSidebar = true;
  }

  @action.bound onSelect({ key, value }: any) {
    const newFilters = Object.assign({}, this.filters);
    newFilters[key] = value;
    this.setFilters(newFilters);
  }

  // Action Methods

  @action.bound handleReset() {
    this.searchStatus.firstSearch = true;
    this.resetFilters();
    this.resetSearchStatus();
  }

  @action resetFilters() {
    this.filters = INITIAL_FILTERS;
  }

  @action resetSearchStatus() {
    this.searchStatus = INITIAL_SEARCH_STATUS;
  }

  @action.bound applySavedFilters(savedFilters: SearchFilter) {
    this.filters = savedFilters;
  }
}

export default SideBarStore;
