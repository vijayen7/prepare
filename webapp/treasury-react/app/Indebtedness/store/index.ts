import { observable, action, flow } from "mobx";
import {
  getIndebtednessRuleDataList,
  simulateRule,
  saveIndebtednessRule,
  saveRecommendationRule,
  simulateAndSaveRecommendationRule,
  getIndebtednessDetailDataList,
  getRecommendationDetailDataList,
  getRecommendationRuleDataList,
} from "../api";
import SideBarStore from "./SideBarStore";
import RulePopUpStore from "./RulePopUpStore";
import RecommendationRulePopUpStore from "./RecommendationRulePopUpStore";
import { showToastService } from "../util";
import { ToastService } from "arc-react-components";
import { recommendationDetailColumns } from "../recommendationDetail/grid/DetailGridColumns";

export class IndebtednessStore {
  // Store initializations
  sideBarStore: SideBarStore = new SideBarStore();
  rulePopUpStore: RulePopUpStore = new RulePopUpStore();
  recommendationRulePopUpStore: RecommendationRulePopUpStore = new RecommendationRulePopUpStore();

  // Indebtedness tabs
  @observable indebtednessRuleDataExistingList = [];
  @observable indebtednessRuleDataSimulatedList = [];
  @observable indebtednessDetailData = [];
  @observable isPopUpOpen: Boolean = false;
  @observable isSearchInProgress: Boolean = false;
  @observable view: String = "detail";
  @observable selectedGridRow: any = [];
  @observable resizeCanvas = false;

  // Recommendation tabs
  @observable recommendationRuleDataExistingList = [];
  @observable recommendationRuleDataSimulatedList = [];
  @observable recommendationDetailData = [];
  @observable subRuleGridTitle = "";
  @observable subRuleGridContent = [];
  @observable isSubRuleGridShown: Boolean = false;
  @observable isRecommendationDetailDrillDownShown: Boolean = false;
  @observable recommendationDetailDrillDownData = [];
  @observable selectedRowDescription = "";

  @action.bound handleSearch() {
    let searchStatus = { firstSearch: false, inProgress: true, error: false };
    this.sideBarStore.setSearchStatus(searchStatus);
    if (this.view == "detail") {
      this.getIndebtednessDetailData();
    } else if (this.view == "rule") {
      this.getIndebtednessRuleData();
    } else if (this.view == "recommendationRule") {
      this.getRecommendationRuleData();
    } else if (this.view == "recommendationDetail") {
      this.getRecommendationDetailData();
    }
  }

  @action.bound changeView(view: String) {
    this.sideBarStore.resetSearchStatus();
    this.view = view;
    this.selectedGridRow = [];
    this.isPopUpOpen = false;
  }

  // ------------------------------------------------------------------------------------------
  // -------------------------------Indebtedness Detail----------------------------------------
  getIndebtednessDetailData = flow(
    function* getIndebtednessDetailData(this: IndebtednessStore) {
      try {
        this.isSearchInProgress = true;
        let data = yield getIndebtednessDetailDataList(this.sideBarStore.filters);
        this.indebtednessDetailData = data;
        this.isSearchInProgress = false;
      } catch (e) {
        alert("Error occurred while fetching detail data");
        this.isSearchInProgress = false;
        this.sideBarStore.searchStatus = {
          firstSearch: false,
          inProgress: false,
          error: true,
        };
      }
    }.bind(this)
  );

  // ------------------------------------------------------------------------------------------
  // --------------------------------------Indebtedness Rule-----------------------------------
  getIndebtednessRuleData = flow(
    function* getIndebtednessRuleData(this: IndebtednessStore) {
      try {
        this.isSearchInProgress = true;
        let data = yield getIndebtednessRuleDataList(this.sideBarStore.filters);
        this.indebtednessRuleDataExistingList = data.existingList;
        this.isSearchInProgress = false;
      } catch (e) {
        alert("Error occurred while fetching rules");
        this.isSearchInProgress = false;
        this.sideBarStore.searchStatus = {
          firstSearch: false,
          inProgress: false,
          error: true,
        };
      }
    }.bind(this)
  );

  handleSaveIndebtednessRule = flow(
    function* handleSaveIndebtednessRule(this) {
      try {
        this.isSearchInProgress = true;
        let payload = this.rulePopUpStore.filters.selectedRowData.map((row) => row.columnValues);
        let data = yield saveIndebtednessRule(payload);
        this.isSearchInProgress = false;
        if (data.includes("errorMessage")) {
          showToastService("Error occurred while saving rules", ToastService.ToastType.CRITICAL);
        } else {
          showToastService("Rules have been saved", ToastService.ToastType.SUCCESS);
        }
        this.isPopUpOpen = false;
        this.rulePopUpStore.handleReset();
      } catch (e) {
        this.isSearchInProgress = false;
        alert("Error occurred while saving rule");
      }
    }.bind(this)
  );

  handleSimulate = flow(
    function* handleSimulate(this) {
      try {
        this.isSearchInProgress = true;
        let data = yield simulateRule(this.rulePopUpStore.filters);
        this.rulePopUpStore.existingRuleList = data.existingList;
        this.rulePopUpStore.simulatedRuleList = data.simulatedList;
        this.rulePopUpStore.isSimulated = true;
        this.rulePopUpStore.isSaveButtonDisabled = false;
        this.isSearchInProgress = false;
      } catch (e) {
        this.isSearchInProgress = false;
        alert("Error occurred while Simulating Rule");
      }
    }.bind(this)
  );

  handleDeleteIndebtednessRule = flow(
    function* handleDeleteIndebtednessRule(this) {
      try {
        this.isSearchInProgress = true;
        let payload = [this.selectedGridRow.columnValues];
        let data = yield saveIndebtednessRule(payload);
        this.isSearchInProgress = false;
        if (data.includes("errorMessage")) {
          showToastService("Error occurred while deleting rules", ToastService.ToastType.CRITICAL);
        } else {
          showToastService("Rules have been deleted", ToastService.ToastType.SUCCESS);
        }
        this.isPopUpOpen = false;
        this.selectedGridRow = [];
        this.rulePopUpStore.handleReset();
      } catch (e) {
        this.isSearchInProgress = false;
        alert("Error occurred while deleting rule");
      }
    }.bind(this)
  );

  // --------------------------------------------------------------------------------------------
  // ------------------------------Rehypo Recommendation Rules-----------------------------------
  getRecommendationRuleData = flow(
    function* getRecommendationRuleData(this: IndebtednessStore) {
      try {
        this.isSearchInProgress = true;
        let data = yield getRecommendationRuleDataList(this.sideBarStore.filters);
        this.recommendationRuleDataExistingList = data.existingList;
        this.isSearchInProgress = false;
        this.isSubRuleGridShown = false;
        this.resizeCanvas = !this.resizeCanvas
      } catch (e) {
        alert("Error occurred while fetching rules");
        this.isSearchInProgress = false;
        this.sideBarStore.searchStatus = {
          firstSearch: false,
          inProgress: false,
          error: true,
        };
      }
    }.bind(this)
  );

  handleSaveRecommendationRule = flow(
    function* handleSaveRecommendationRule(this) {
      try {
        this.isSearchInProgress = true;
        let payload = this.recommendationRulePopUpStore.filters;
        let data = yield simulateAndSaveRecommendationRule(payload, this.recommendationRulePopUpStore.subRuleGridContent);
        this.isSearchInProgress = false;
        if (data.includes("errorMessage")) {
          showToastService("Error occurred while saving rules", ToastService.ToastType.CRITICAL);
        } else {
          showToastService("Rules have been saved", ToastService.ToastType.SUCCESS);
        }
        this.recommendationRulePopUpStore.isPopUpOpen = false;
        this.recommendationRulePopUpStore.handleReset();
        this.handleSearch();
      } catch (e) {
        this.isSearchInProgress = false;
        alert("Error occurred while saving rule");
      }
    }.bind(this)
  );

  handleDeleteRecommendationRule = flow(
    function* handleDeleteRecommendationRule(this) {
      try {
        this.isSearchInProgress = true;
        let payload = [this.selectedGridRow.columnValues];
        let data = yield saveRecommendationRule(payload);
        this.isSearchInProgress = false;
        if (data.includes("errorMessage")) {
          showToastService("Error occurred while deleting rules", ToastService.ToastType.CRITICAL);
        } else {
          showToastService("Rules have been deleted", ToastService.ToastType.SUCCESS);
        }
        this.recommendationRulePopUpStore.isPopUpOpen = false;
        this.selectedGridRow = [];
        this.recommendationRulePopUpStore.handleReset();
        this.handleSearch();
      } catch (e) {
        this.isSearchInProgress = false;
        alert("Error occurred while deleting rule");
      }
    }.bind(this)
  );

    // ------------------------------------------------------------------------------------------
  // -------------------------------Recommendation Detail----------------------------------------
  getRecommendationDetailData = flow(
    function* getRecommendationDetailData(this: IndebtednessStore) {
      try {
        this.isSearchInProgress = true;
        let data = yield getRecommendationDetailDataList(this.sideBarStore.filters);
        this.recommendationDetailData = data;
        this.isSearchInProgress = false;
        this.isRecommendationDetailDrillDownShown = false;
        this.resizeCanvas = !this.resizeCanvas
      } catch (e) {
        alert("Error occurred while fetching detail data");
        this.isSearchInProgress = false;
        this.sideBarStore.searchStatus = {
          firstSearch: false,
          inProgress: false,
          error: true,
        };
      }
    }.bind(this)
  );

  @action
  getFlattenedRecommendationDetailData(data) {
    let flattenedData: any = [];
    let fields = recommendationDetailColumns().map(function (column) {return column.field;});
    if (data.length !== 0) {
      data.forEach((item) => {
        let detail: any = [];
        fields.forEach((field) => {
          detail[field] = item[field];
        });
        detail.id = item.id;
        detail.axeBrokerData = item.axeBrokerData;
        flattenedData.push(detail);
      });
    }
    return flattenedData;
  }

  // ------------------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------------------

}

export default IndebtednessStore;
