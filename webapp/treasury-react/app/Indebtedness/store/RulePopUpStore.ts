import { observable, action, flow } from "mobx";
import { SearchFilter } from "../models/SearchFilter";
import { getCurrentDate, getFirstDateOfCurrentMonth } from "../../commons/util";

export const INITIAL_FILTERS = {
  date: getFirstDateOfCurrentMonth(),
  selectedLegalEntities: [] as any,
  selectedAgreementTypes: [] as any,
  selectedCustodianAccounts: [] as any,
  selectedCpes: [] as any,
  selectedDebitBalanceCurrencies: [{ key: -1, value: `All Currencies [-1]`}],
  selectedCreditOffsetCurrencies: [{ key: -1, value: `All Currencies [-1]`}],
  smvConversionFactor: 1,
  rehypothecationFactor: 1.4,
  selectedRowData: [] as any,
  includeIsdaMargin: true,
  includeIsdaVariationMargin: true
};

export class RulePopUpStore {
  // Observale Variables
  @observable filters = INITIAL_FILTERS;
  @observable isEditClicked: Boolean = false;
  @observable isDeleteClicked: Boolean = false;
  @observable isSimulated: Boolean = false;
  @observable isSaveButtonDisabled: Boolean = true;
  @observable existingRuleList = [];
  @observable simulatedRuleList = [];

  // Action Setters

  @action setFilters(newSearchFilters) {
    this.filters = newSearchFilters;
  }

  // Action Methods

  @action.bound handleReset() {
    this.resetFilters();
    this.isEditClicked = false;
    this.isDeleteClicked = false;
    this.isSimulated = false;
    this.isSaveButtonDisabled = true;
    this.existingRuleList = [];
    this.simulatedRuleList = [];
    this.isEditClicked = false;
  }

  @action resetFilters() {
    this.filters = INITIAL_FILTERS;
  }

  @action.bound onSelect({ key, value }: any) {
    const newFilters = Object.assign({}, this.filters);
    newFilters[key] = value;
    this.setFilters(newFilters);
  }

}

export default RulePopUpStore;
