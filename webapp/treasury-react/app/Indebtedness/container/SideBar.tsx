import React from "react";
import { Sidebar } from "arc-react-components";
import { observer } from "mobx-react";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import { IndebtednessProps } from "../models/IndebtednessProps";
import SaveSettingsManager from "commons/components/SaveSettingsManager";
import { Card } from "arc-react-components";
import CustodianAccountFilter from "commons/container/CustodianAccountFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import IndebtednessAgreementTypeFilter from "commons/container/IndebtednessAgreementTypeFilter";
import DateFilter from "commons/container/DateFilter";
import ReportingCurrencyFilter from "commons/container/CurrencyFilter";

const IndebtednessRuleSideBar: React.FC<IndebtednessProps> = (props: IndebtednessProps) => {
  let filters = (
    <React.Fragment>
      <Card>
        <DateFilter
          onSelect={props.store.sideBarStore.onSelect}
          data={props.store.sideBarStore.filters.date}
          label="Date"
          stateKey="date"
        />
        <br />
        <IndebtednessAgreementTypeFilter
          onSelect={props.store.sideBarStore.onSelect}
          selectedData={props.store.sideBarStore.filters.selectedAgreementTypes}
          multiSelect={true}
        />
        <LegalEntityFilter
          onSelect={props.store.sideBarStore.onSelect}
          selectedData={props.store.sideBarStore.filters.selectedLegalEntities}
          multiSelect={true}
        />
        <CpeFilter
          onSelect={props.store.sideBarStore.onSelect}
          selectedData={props.store.sideBarStore.filters.selectedCpes}
          multiSelect={true}
        />
        <CustodianAccountFilter
          onSelect={props.store.sideBarStore.onSelect}
          selectedData={props.store.sideBarStore.filters.selectedCustodianAccounts}
          multiSelect={true}
        />
      </Card>
      <Card>
        <ReportingCurrencyFilter
          label="Reporting Currency"
          onSelect={props.store.sideBarStore.onSelect}
          selectedData={props.store.sideBarStore.filters.selectedReportingCurrency}
          multiSelect={false}
          stateKey="selectedReportingCurrency"
        />
      </Card>
    </React.Fragment>
  );

  let searchButtons = (
    <ColumnLayout>
      <FilterButton onClick={props.store.handleSearch} label="Search" />
      <FilterButton onClick={props.store.sideBarStore.handleReset} reset label="Reset" />
    </ColumnLayout>
  );

  let saveSettings = (
    <Card>
      <SaveSettingsManager
        selectedFilters={props.store.sideBarStore.filters}
        applySavedFilters={props.store.sideBarStore.applySavedFilters}
        applicationName="Indebtedness"
      />
    </Card>
  );

  return (
    <Sidebar header={false} footer={searchButtons}>
      {saveSettings}
      {filters}
    </Sidebar>
  );
};

export default observer(IndebtednessRuleSideBar);
