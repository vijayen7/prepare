import React from "react";
import { ToastService } from "arc-react-components";

export function getValueForSingleSelectOrUndefined(data) {
  if (typeof data === "undefined" || data === null
    || data.length === 0 || data.key === -1)
    return undefined;
  const returnValue = data.key;
  return returnValue;
}

export function getZeroForDefault(data) {
  if (typeof data !== "undefined" && data !== null && data.length === 0)
    return "0";
  return data;
}

export function getDisabledFilter(label, value) {
  return (
    <div className="form-field--split">
      <div className="padding--right--large">{label}</div>
      <div className="text-align--left">{value}</div>
    </div>
  );
}

export function showToastService(content, type) {
  ToastService.append({
    content: content,
    type: type,
    placement: ToastService.Placement.BOTTOM_RIGHT,
    dismissTime: 7000,
  });
};


export function getCommaSeparatedValuesDisplay(data) {
  let returnValue = _.join(
    _.map(data, (e) => e.value),
    ","
  );
  return returnValue;
}
