import React from "react";
import { observer } from "mobx-react";
import Message from "commons/components/Message";
import IndebtednessGrid from "commons/components/GridWithCellClick";
import _ from "lodash";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import { Layout } from "arc-react-components";
import { gridOptions } from "../grid/DetailGridConfig";
import { columns } from "../grid/DetailGridColumns";
import { Panel } from "arc-react-components";

const IndebtednessDetailGrid: React.FC<IndebtednessProps> = (
  props: IndebtednessProps
) => {
  const renderGridData = () => {
    let indebtednessDetailData = props.store.indebtednessDetailData;

    if (props.store.sideBarStore.searchStatus.error) {
      return <Message messageData="Error occurred while loading data." />;
    }

    if (!indebtednessDetailData.length) {
      let noDataMessage = "Please search to load data";
      if (props.store.sideBarStore.searchStatus.firstSearch == false) {
        noDataMessage = "No data available for search criteria";
      }
      return <Message messageData={noDataMessage} />;
    }
    let flattenedData: any = _.cloneDeep(indebtednessDetailData)
    let RC = flattenedData[0].reportingCurrency
    let message = (
      <small><ul className="bullet">
        <li>{`Reporting Currency (RC) corresponds to ${RC}`}</li>
      </ul></small>
    );
    if (RC == undefined) {
      message = (<ul className="bullet">
        <li className="critical">{`Fx Rate unavailable for selected Reporting Currency`}</li>
      </ul>);
    }
    let grid = (
      <Layout
        isColumnType={false}
        className="border"
      >
        <Layout.Child
          childId="detailGrid"
          size={1}
          title="Indebtedness Detail"
          key="gridChild1"
          collapsible
        >
            <IndebtednessGrid
              data={flattenedData}
              gridId="indebtednessDetailGrid"
              gridColumns={columns()}
              gridOptions={gridOptions()}
              onCellClick={() => {}}
              fill
            />
        </Layout.Child>
        <Layout.Child size="Fit" childId="rcGridChild5">
            {message}
        </Layout.Child>
      </Layout>
    );

    return (
      <React.Fragment>
        {grid}
      </React.Fragment>
    );

  };

  return (
    <React.Fragment>
      {renderGridData()}
    </React.Fragment>
  );
};

export default observer(IndebtednessDetailGrid);
