import { getSaveSettingsUrl } from "commons/util";
import { columns } from "./DetailGridColumns";
import { indebtednessDetailUrl } from "../../api";

export function gridOptions() {
  let options = {
    applyFilteringOnGrid: true,
    exportToExcel: true,
    autoHorizontalScrollBar: true,
    page: true,
    sortList: [
      {
        columnId: "date",
        sortAsc: true,
      },
    ],
    sheetName: "Indebtedness_Detail_Data_View",
    frozenColumn: 2,
    highlightRowOnClick: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "Total",
    configureReport: {
      urlCallback: function () {
        return window.location.origin + indebtednessDetailUrl;
      }
    },
    saveSettings: {
      applicationId: 3,
      applicationCategory: "IndebtednessData",
      serviceURL: getSaveSettingsUrl(),
      onBeforeLoadedCallback: function (settingsData) { },
      loadedCallback: function (gridObject) { },
    },
    enableMultilevelGrouping: {
      hideGroupingHeader: false,
      showGroupingKeyInColumn: false,
      showGroupingTotals: true,
      customGroupingRendering: true,
      initialGrouping: ["agreementType", "legalEntity", "cpe"]
    },
    customColumnSelection: {
      defaultcolumns: getDefaultColumns(),
      totalColumns: getTotalColumns(),
    },
  };

  return options;
}

function getDefaultColumns() {
  return [
    "date",
    "agreementType",
    "cpe",
    "legalEntity",
    "ca",
    "smvRC",
    "lmvRC",
    "debitCashRC",
    "creditCashRC",
    "marginRC",
    "variationMarginRC",
    "smvFactor",
    "rehypothecationFactor",
    "indebtednessRC",
    "rehypothecationEligibilityRC",
    "currentRehypothecationEligibilityRC",
    "balanceRehypothecationEligibilityRC"
  ];
}

function getTotalColumns() {
  return columns().map((column) => [column.id, column.name]);
}
