import { BASE_URL } from "commons/constants";
import { ArcFetch } from "arc-commons";
import _ from "lodash";
import { getCommaSeparatedValuesOrUndefined, getCommaSeparatedValuesOrNullForAll, getCommaSeparatedValuesOrNullForSingleSelect } from "commons/util";
import { SearchFilter } from "./models/SearchFilter";
import { getValueForSingleSelectOrUndefined, getZeroForDefault } from "./util";
const queryString = require("query-string");

const QUERY_ARGS = {
  credentials: "include",
  method: "GET",
};
export let indebtednessDetailUrl = "";
export let rehypothecationDetailUrl = "";

// ------------------------------------------------------------------------------------------
// -------------------------------Indebtedness Detail----------------------------------------

export const getIndebtednessDetailDataList = (filters: SearchFilter) => {
  indebtednessDetailUrl = getIndebtednessDetailURL(filters);
  return ArcFetch.getJSON(indebtednessDetailUrl, QUERY_ARGS);
};

const getIndebtednessDetailURL = (filters: SearchFilter) => {
  let payload = {
    date: filters.date,
    legalEntityIds: getCommaSeparatedValuesOrNullForAll(filters.selectedLegalEntities),
    cpeIds: getCommaSeparatedValuesOrNullForAll(filters.selectedCpes),
    agreementTypeIds: getCommaSeparatedValuesOrNullForAll(filters.selectedAgreementTypes),
    caIds: getCommaSeparatedValuesOrNullForAll(filters.selectedCustodianAccounts),
    reportingCurrencyId: getCommaSeparatedValuesOrNullForSingleSelect(filters.selectedReportingCurrency),
  };

  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}/service/indebtednessDetailService/getDetailIndebtednessData?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return url;
};

// ------------------------------------------------------------------------------------------
// -------------------------------Indebtedness Rule----------------------------------------

export const getIndebtednessRuleDataList = (filters: SearchFilter) => {
  const url = getIndebtednessRuleURL(filters);
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

const getIndebtednessRuleURL = (filters: SearchFilter) => {
  let payload = {
    "ruleSystemFilter.legalEntityIds": getCommaSeparatedValuesOrUndefined(filters.selectedLegalEntities),
    "ruleSystemFilter.cpeIds": getCommaSeparatedValuesOrUndefined(filters.selectedCpes),
    "ruleSystemFilter.agreementTypeIds": getCommaSeparatedValuesOrUndefined(filters.selectedAgreementTypes),
    "ruleSystemFilter.custodianAccountIds": getCommaSeparatedValuesOrUndefined(
      filters.selectedCustodianAccounts
    ),
    "ruleSystemFilter.startDate": filters.date,
  };

  payload["ruleSystemFilter.startDate"] = payload["ruleSystemFilter.startDate"].replace(/-/g, "");

  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}/service/indebtednessService/getIndebtednessRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return url;
};

export const simulateRule = (filters) => {
  const url = getSimulateRuleURL(filters);
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

const getSimulateRuleURL = (filters) => {
  let payload = {
    "ruleSystemFilter.legalEntityIds": getValueForSingleSelectOrUndefined(filters.selectedLegalEntities),
    "ruleSystemFilter.cpeIds": getValueForSingleSelectOrUndefined(filters.selectedCpes),
    "ruleSystemFilter.agreementTypeIds": getValueForSingleSelectOrUndefined(filters.selectedAgreementTypes),
    "ruleSystemFilter.custodianAccountIds": getValueForSingleSelectOrUndefined(
      filters.selectedCustodianAccounts
    ),
    "ruleSystemFilter.startDate": filters.date,
    "ruleSystemFilter.debitBalanceCurrencyIds": getCommaSeparatedValuesOrUndefined(
      filters.selectedDebitBalanceCurrencies
    ),
    "ruleSystemFilter.creditOffsetCurrencyIds": getCommaSeparatedValuesOrUndefined(
      filters.selectedCreditOffsetCurrencies
    ),
    "ruleSystemFilter.smvConversionFactor": filters.smvConversionFactor,
    "ruleSystemFilter.rehypothecationFactor": filters.rehypothecationFactor,
    "ruleSystemFilter.includeIsdaMargin": filters.includeIsdaMargin,
    "ruleSystemFilter.includeIsdaVariationMargin": filters.includeIsdaVariationMargin,
  };

  payload["ruleSystemFilter.startDate"] = payload["ruleSystemFilter.startDate"].replace(/-/g, "");

  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}/service/indebtednessService/getSimulatedResults?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return url;
};

export const saveIndebtednessRule = (payload) => {
  let parameters = {
    rulesToSave: JSON.stringify(payload),
  };
  const paramString = queryString.stringify(parameters);
  const url = `${BASE_URL}/service/indebtednessService/saveSimuatedRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

// ------------------------------------------------------------------------------------------
// -------------------------------Recommendation Rule----------------------------------------

export const getRecommendationRuleDataList = (filters: SearchFilter) => {
  let payload = {
    "ruleSystemFilter.legalEntityIds": getCommaSeparatedValuesOrUndefined(filters.selectedLegalEntities),
    "ruleSystemFilter.cpeIds": getCommaSeparatedValuesOrUndefined(filters.selectedCpes),
    "ruleSystemFilter.agreementTypeIds": getCommaSeparatedValuesOrUndefined(filters.selectedAgreementTypes),
    "ruleSystemFilter.custodianAccountIds": getCommaSeparatedValuesOrUndefined(
      filters.selectedCustodianAccounts
    ),
    "ruleSystemFilter.startDate": filters.date,
  };

  payload["ruleSystemFilter.startDate"] = payload["ruleSystemFilter.startDate"].replace(/-/g, "");

  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}/service/indebtednessService/getRehypothecationRecommendationRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;

  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const saveRecommendationRule = (payload) => {
  let parameters = {
    rulesToSave: JSON.stringify(payload),
  };
  const paramString = queryString.stringify(parameters);
  const url = `${BASE_URL}/service/indebtednessService/saveRehypothecationRecommendationRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const simulateAndSaveRecommendationRule = (filters, subRules) => {
  var output = _.cloneDeep(subRules);
  for (var j = 0; j < output.length; j++) {
    delete output[j]["id"];
    delete output[j]["@CLASS"];
  }

  var ruleOutputStr = JSON.stringify(output);
  let payload = {
    "ruleSystemFilter.legalEntityIds": getValueForSingleSelectOrUndefined(filters.selectedLegalEntities),
    "ruleSystemFilter.cpeIds": getValueForSingleSelectOrUndefined(filters.selectedCpes),
    "ruleSystemFilter.agreementTypeIds": getValueForSingleSelectOrUndefined(filters.selectedAgreementTypes),
    "ruleSystemFilter.custodianAccountIds": getCommaSeparatedValuesOrNullForAll(
      filters.selectedCustodianAccounts
    ),
    "ruleSystemFilter.startDate": filters.date,
    "ruleSystemFilter.ruleOutputString": ruleOutputStr,
  };

  payload["ruleSystemFilter.startDate"] = payload["ruleSystemFilter.startDate"].replace(/-/g, "");

  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}/service/indebtednessService/simulateAndsaveRehypothecationRecommendationRules?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return ArcFetch.getJSON(url, QUERY_ARGS);
};

// ------------------------------------------------------------------------------------------
// -------------------------------Recommendation Detail----------------------------------------

export const getRecommendationDetailDataList = (filters: SearchFilter) => {
  rehypothecationDetailUrl = getRecommendationDetailURL(filters);
  return ArcFetch.getJSON(rehypothecationDetailUrl, QUERY_ARGS);
};

const getRecommendationDetailURL = (filters: SearchFilter) => {
  let payload = {
    date: filters.date,
    legalEntityIds: getCommaSeparatedValuesOrNullForAll(filters.selectedLegalEntities),
    cpeIds: getCommaSeparatedValuesOrNullForAll(filters.selectedCpes),
    agreementTypeIds: getCommaSeparatedValuesOrNullForAll(filters.selectedAgreementTypes),
    caIds: getCommaSeparatedValuesOrNullForAll(filters.selectedCustodianAccounts),
    reportingCurrencyId: getCommaSeparatedValuesOrNullForSingleSelect(filters.selectedReportingCurrency),
  };

  const paramString = queryString.stringify(payload);
  const url = `${BASE_URL}/service/rehypothecationDetailService/getRehypothecationDetailData?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  return url;
};


// ------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
