import React, { Component } from 'react';
import _ from 'lodash';
import { Layout } from 'arc-react-components';
import { observer } from "mobx-react";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import SimulatedGrid from 'commons/components/Grid';
import CheckboxGrid from 'commons/components/GridWithCheckbox';
import { getExistingGridOptions, getSimulatedGridOptions } from "../grid/RuleGridConfig";
import { getIndebtednessRuleSimulatedColumns } from "../grid/RuleGridColumns";
import { Card } from "arc-react-components";

const PopUpSimulatedGrids: React.FC<IndebtednessProps> = (
  props: IndebtednessProps
) => {

  const renderGridData = () => {
    if (!props.store.rulePopUpStore.isSimulated) {
      return null;
    }
    let existingRules = <React.Fragment />;
    let simulatedRules = <React.Fragment />;
    const existingList = props.store.rulePopUpStore.existingRuleList;
    const simulatedList = props.store.rulePopUpStore.simulatedRuleList;

    if (typeof existingList === 'undefined' || typeof simulatedList === 'undefined') {
      return <React.Fragment />;
    }
    if (existingList.length > 0) {
      existingRules = (
        <SimulatedGrid
          data={existingList}
          gridId="ExistingRules"
          gridColumns={getIndebtednessRuleSimulatedColumns()}
          gridOptions={getExistingGridOptions()}
          label={<div className="text-align--left">Existing Rules</div>}
        />
      );
    }
    if (simulatedList.length > 0) {
      simulatedRules = (
        <CheckboxGrid
          data={simulatedList}
          gridId="SimulatedRules"
          gridColumns={getIndebtednessRuleSimulatedColumns()}
          gridOptions={getSimulatedGridOptions()}
          label={<div className="text-align--left">Simulated Rules : Please select one or more rules to save.</div>}
          onSelect={props.store.rulePopUpStore.onSelect}
          preSelectAllRows={false}
          heightValue={-1}
        />
      );
    }
    return (
      <Card>
        <Layout isRowType>
          <Layout.Child childId="popupSimulatedGrid1">
            {existingRules}
            <hr />
            <br />
          </Layout.Child>
          <Layout.Child childId="popupSimulatedGrid2">
            {simulatedRules}
          </Layout.Child>
        </Layout>
      </Card>
    );
  }

  return <>{renderGridData()}</>;
};

export default observer(PopUpSimulatedGrids);
