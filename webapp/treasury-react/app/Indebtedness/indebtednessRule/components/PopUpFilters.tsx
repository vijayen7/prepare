import React, { Component } from 'react';
import _ from 'lodash';
import { Layout } from 'arc-react-components';
import DateFilter from 'commons/container/DateFilter';
import InputNumberFilter from 'commons/components/InputNumberFilter';
import { observer } from "mobx-react";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import { getDisabledFilter } from "../../util";
import CustodianAccountFilter from "commons/container/CustodianAccountFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CpeFilter from "commons/container/CpeFilter";
import AgreementTypeFilter from "commons/container/AgreementTypeFilter";
import IndebtednessAgreementTypeFilter from "commons/container/IndebtednessAgreementTypeFilter";
import CurrencyFilter from 'commons/container/CurrencyFilter';
import CheckboxFilter from 'commons/components/CheckboxFilter';

const PopUpFilters: React.FC<IndebtednessProps> = (
  props: IndebtednessProps
) => {
  const renderGridData = () => {
    let filters = (
      <Layout isRowType>
        <Layout.Child childId="indebtednessPopup1">
          {props.store.rulePopUpStore.isEditClicked ? (
            getDisabledFilter('Legal Entity:', props.store.rulePopUpStore.filters.selectedLegalEntities.value)
          ) : (
              <LegalEntityFilter
                onSelect={props.store.rulePopUpStore.onSelect}
                selectedData={props.store.rulePopUpStore.filters.selectedLegalEntities}
                multiSelect={false}
                horizontalLayout
              />
            )}
          {props.store.rulePopUpStore.isEditClicked ? (
            getDisabledFilter('CPEs:', props.store.rulePopUpStore.filters.selectedCpes.value)
          ) : (
              <CpeFilter
                onSelect={props.store.rulePopUpStore.onSelect}
                selectedData={props.store.rulePopUpStore.filters.selectedCpes}
                multiSelect={false}
                horizontalLayout
              />
            )}
          {props.store.rulePopUpStore.isEditClicked ? (
            getDisabledFilter('Agreement Type:', props.store.rulePopUpStore.filters.selectedAgreementTypes.value)
          ) : (
              <IndebtednessAgreementTypeFilter
                onSelect={props.store.rulePopUpStore.onSelect}
                selectedData={props.store.rulePopUpStore.filters.selectedAgreementTypes}
                multiSelect={false}
                horizontalLayout
              />
            )}
          {props.store.rulePopUpStore.isEditClicked ? (
            getDisabledFilter('Custodian Account:', props.store.rulePopUpStore.filters.selectedCustodianAccounts.value)
          ) : (
              <CustodianAccountFilter
                onSelect={props.store.rulePopUpStore.onSelect}
                selectedData={props.store.rulePopUpStore.filters.selectedCustodianAccounts}
                multiSelect={false}
                horizontalLayout
              />
            )}
          {props.store.rulePopUpStore.isDeleteClicked ? (
            <React.Fragment />
          ) : (
              <React.Fragment>
                <CurrencyFilter
                  onSelect={props.store.rulePopUpStore.onSelect}
                  selectedData={props.store.rulePopUpStore.filters.selectedDebitBalanceCurrencies}
                  stateKey="selectedDebitBalanceCurrencies"
                  label="Debit Balance Currencies"
                  horizontalLayout
                />
                <CurrencyFilter
                  onSelect={props.store.rulePopUpStore.onSelect}
                  selectedData={props.store.rulePopUpStore.filters.selectedCreditOffsetCurrencies}
                  stateKey="selectedCreditOffsetCurrencies"
                  label="Credit offset currencies"
                  horizontalLayout
                />
                <InputNumberFilter
                  onSelect={props.store.rulePopUpStore.onSelect}
                  data={props.store.rulePopUpStore.filters.rehypothecationFactor}
                  label="Rehypothecation Factor "
                  stateKey="rehypothecationFactor"
                />
                <InputNumberFilter
                  onSelect={props.store.rulePopUpStore.onSelect}
                  data={props.store.rulePopUpStore.filters.smvConversionFactor}
                  label="SMV Conversion Factor "
                  stateKey="smvConversionFactor"
                />
                <br />
                <CheckboxFilter
                  onSelect={props.store.rulePopUpStore.onSelect}
                  stateKey="includeIsdaMargin"
                  label="Include ISDA Initial Margin"
                  defaultChecked={props.store.rulePopUpStore.filters.includeIsdaMargin}
                />
                <CheckboxFilter
                  onSelect={props.store.rulePopUpStore.onSelect}
                  stateKey="includeIsdaVariationMargin"
                  label="Include ISDA Variation Margin"
                  defaultChecked={props.store.rulePopUpStore.filters.includeIsdaVariationMargin}
                />
              </React.Fragment>
            )}
          <DateFilter
            onSelect={props.store.rulePopUpStore.onSelect}
            stateKey="date"
            data={props.store.rulePopUpStore.filters.date}
            label={props.store.rulePopUpStore.isDeleteClicked ? 'Effective Till' : 'Date'}
          />
        </Layout.Child>
      </Layout>
    );

    return filters;
  }

  return <>{renderGridData()}</>;
};

export default observer(PopUpFilters);
