declare global {
  interface Window {
    CODEX_PROPERTIES: any;
  }
}

const CODEX_PROPERTIES = window.CODEX_PROPERTIES || {};

export function getRuleGridOptions() {
  var options = {
    autoHorizontalScrollBar: true,
    frozenColumn: 4,
    highlightRowOnClick: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    exportToExcel: true,
    summaryRow: true,
    displaySummaryRow: true,
    summaryRowText: "",
    sortList: [
      { columnId: "agreementType", sortAsc: true },
      { columnId: "cpe", sortAsc: true }
    ]
  };
  return options;
}


export function getExistingGridOptions() {
  return {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    highlightRowOnClick: true,
    sortList: [
      {
        columnId: "agreementType",
        sortAsc: true
      }
    ],
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    exportToExcel: true
  };
}

export function getSimulatedGridOptions() {
  return {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    highlightRowOnClick: true,
    sortList: [
      {
        columnId: "agreementType",
        sortAsc: true
      }
    ],
    editable: true,
    asyncEditorLoading: false,
    autoEdit: false,
    applyFilteringOnGrid: true,
    isFilterOnFormattedValue: true,
    exportToExcel: true,
    checkboxHeader: [
      {
        controlChildSelection: false,
        isSelectAllCheckboxRequired: true
      }
    ],
    addCheckboxHeaderAsLastColumn: false
  };
}
