import { convertJavaDateDashed, formatterFourDecimal } from "commons/util";
import { groupFloatFormatter } from "commons/grid/formatters";

export function getCommonColumns() {
  return [
    {
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreementType",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 80,
      excelFormatter: "#,##0",
    },
    {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legalEntity",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 100,
      excelFormatter: "#,##0",
    },
    {
      id: "cpe",
      type: "text",
      name: "Counterparty",
      field: "cpe",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    },
    {
      id: "custodianAccountName",
      type: "text",
      name: "Custodian Account",
      field: "custodianAccountName",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    },
    {
      id: "debitBalanceCurrenciesAbbrev",
      type: "text",
      name: "Debit Balance Currencies",
      field: "debitBalanceCurrenciesAbbrev",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    },
    {
      id: "creditOffsetCurrenciesAbbrev",
      type: "text",
      name: "Credit Offset Currencies",
      field: "creditOffsetCurrenciesAbbrev",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    },
    {
      id: "includeIsdaMargin",
      type: "text",
      name: "Include ISDA Initial Margin",
      field: "includeIsdaMargin",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 120,
    },
    {
      id: "includeIsdaVariationMargin",
      type: "text",
      name: "Include ISDA Variation Margin",
      field: "includeIsdaVariationMargin",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 120,
    },
    {
      id: "rehypothecationFactor",
      name: "Rehypothecation Factor",
      field: "rehypothecationFactor",
      toolTip: "Rehypothecation Factor",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(row, cell, value, columnDef, dataContext);
        else return null;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 140,
    },
    {
      id: "smvConversionFactor",
      name: "SMV Conversion Factor",
      field: "smvConversionFactor",
      toolTip: "SMV Conversion Factor",
      type: "number",
      filter: true,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        if (value !== undefined && value !== null)
          return formatterFourDecimal(row, cell, value, columnDef, dataContext);
        else return null;
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0.0",
      width: 140,
    },
    {
      id: "validFrom",
      type: "text",
      name: "Valid From",
      field: "effective_start_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 80,
      excelFormatter: "#,##0",
    },
    {
      id: "validTill",
      type: "text",
      name: "Valid Till",
      field: "effective_end_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      formatter(row, cell, value, columnDef, dataContext) {
        if (value === "20380101") {
          return "-";
        }
        return value;
      },
      width: 60,
      excelFormatter: "#,##0",
    },
  ];
}

function getActionColumn() {
  return [
    {
      id: "actions",
      name: "Actions",
      exportToExcel: false,
      sortable: false,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return (
          "<a title='Edit'><i data-role='edit' class='icon-edit--block' /></a>  " +
          "  <a title='Delete'><i data-role='delete' class='icon-delete margin--right' /></a>  "
        );
      },
      width: 60,
      minWidth: 60,
      maxWidth: 60,
      fixed: true,
    },
  ];
}

export function getIndebtednessRuleColumns() {
  let columns = getActionColumn();
  columns = (columns as any[]).concat(getCommonColumns());
  return columns;
}

export function getIndebtednessRuleSimulatedColumns() {
  const columns = getCommonColumns();
  return columns;
}
