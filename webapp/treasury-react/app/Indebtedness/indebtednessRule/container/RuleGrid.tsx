import React from "react";
import { observer } from "mobx-react";
import Message from "commons/components/Message";
import GridWithCellClick from "commons/components/GridWithCellClick";
import _ from "lodash";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import RulePopUp from "./RulePopUp";
import { Layout } from "arc-react-components";
import { getRuleGridOptions } from "../grid/RuleGridConfig";
import { getIndebtednessRuleColumns } from "../grid/RuleGridColumns";
import { Panel } from "arc-react-components";

const IndebtednessRuleGrid: React.FC<IndebtednessProps> = (
  props: IndebtednessProps
) => {

  const onCellClickHandler = (args) => {
    let ele = args.event.target;
    let { role } = ele.dataset;
    if (role === 'edit' || role === 'delete') {
      props.store.rulePopUpStore.isEditClicked = true
      props.store.selectedGridRow = args.item
      populatePopUpFilters()
      if (role === 'delete') {
        props.store.rulePopUpStore.isDeleteClicked = true
      }
      props.store.isPopUpOpen = true
    }
  }

  const populatePopUpFilters = () => {
    const rowData: any = props.store.selectedGridRow;
    let populateValues = {};
    if (
      typeof rowData.legal_entity_id !== "undefined" &&
      rowData.legal_entity_id !== ""
    ) {
      props.store.rulePopUpStore.filters.selectedLegalEntities = {
        key: rowData.legal_entity_id,
        value: `${rowData.legalEntity} [${rowData.legal_entity_id}]`
      };
    }
    if (typeof rowData.cpe_id !== "undefined" && rowData.cpe_id !== "") {
      props.store.rulePopUpStore.filters.selectedCpes = {
        key: rowData.cpe_id,
        value: `${rowData.cpe} [${rowData.cpe_id}]`
      };
    }
    if (
      typeof rowData.agreement_type_id !== "undefined" &&
      rowData.agreement_type_id !== ""
    ) {
      props.store.rulePopUpStore.filters.selectedAgreementTypes = {
        key: rowData.agreement_type_id,
        value: `${rowData.agreementType} [${rowData.agreement_type_id}]`
      };
    }
    if (
      typeof rowData.custodian_account_id !== "undefined" &&
      rowData.custodian_account_id !== ""
    ) {
      props.store.rulePopUpStore.filters.selectedCustodianAccounts = {
        key: rowData.custodian_account_id,
        value: `${rowData.custodianAccountName} [${rowData.custodian_account_id}]`
      };
    }
    if (
      typeof rowData.includeIsdaMargin !== "undefined" &&
      rowData.includeIsdaMargin !== null
    ) {
      props.store.rulePopUpStore.filters.includeIsdaMargin = rowData.includeIsdaMargin;
    }
    if (
      typeof rowData.includeIsdaVariationMargin !== "undefined" &&
      rowData.includeIsdaVariationMargin !== null
    ) {
      props.store.rulePopUpStore.filters.includeIsdaVariationMargin = rowData.includeIsdaVariationMargin;
    }
    if (
      typeof rowData.smvConversionFactor !== "undefined" &&
      rowData.smvConversionFactor !== null
    ) {
      props.store.rulePopUpStore.filters.smvConversionFactor = rowData.smvConversionFactor;
    }
    if (
      typeof rowData.rehypothecationFactor !== "undefined" &&
      rowData.rehypothecationFactor !== null
    ) {
      props.store.rulePopUpStore.filters.rehypothecationFactor = rowData.rehypothecationFactor;
    }
    if (
      typeof rowData.debitBalanceCurrenciesMap !== "undefined" &&
      rowData.debitBalanceCurrenciesMap !== null
    ) {
      let filterValue = [] as any;
      let obj = new Map(Object.entries(rowData.debitBalanceCurrenciesMap))
      obj.forEach(((values, keys) => {
        filterValue.push({
          key: keys,
          value: values
        })
      }));
      props.store.rulePopUpStore.filters.selectedDebitBalanceCurrencies = filterValue;
    }
    if (
      typeof rowData.creditOffsetCurrenciesMap !== "undefined" &&
      rowData.creditOffsetCurrenciesMap !== null
    ) {
      let filterValue = [] as any;
      let obj = new Map(Object.entries(rowData.creditOffsetCurrenciesMap))
      obj.forEach(((values, keys) => {
        filterValue.push({
          key: keys,
          value: values
        })
      }));
      props.store.rulePopUpStore.filters.selectedCreditOffsetCurrencies = filterValue;
    }
  }

  const renderGridData = () => {
    let indebtednessRuleData = props.store.indebtednessRuleDataExistingList;

    if (props.store.sideBarStore.searchStatus.error) {
      return <Message messageData="Error occurred while loading data." />;
    }

    if (!indebtednessRuleData.length) {
      let noDataMessage = "Please search to load data";
      if (props.store.sideBarStore.searchStatus.firstSearch == false) {
        noDataMessage = "No data available for search criteria";
      }
      return <Message messageData={noDataMessage} />;
    }

    let grid = (
      <Layout
        isColumnType={false}
        className="border"
        style={{ height: "100%" }}
      >
        <Layout.Child
          childId="indebtednessRules"
          size={1}
          title="Indebtedness Rules"
          key="gridChild1"
          collapsible
        >
          <Panel title="Indebtedness Rules">
            <GridWithCellClick
              gridColumns={getIndebtednessRuleColumns()}
              onCellClick={onCellClickHandler}
              gridId="indebtednessRuleGrid"
              gridOptions={getRuleGridOptions()}
              data={indebtednessRuleData}
              fill={true}
            />
          </Panel>
        </Layout.Child>
      </Layout>
    );

    return (
      <React.Fragment>
        {grid}
      </React.Fragment>
    );

  };

  return (
    <React.Fragment>
      {renderGridData()}
      <RulePopUp store={props.store} />
    </React.Fragment>
  );
};

export default observer(IndebtednessRuleGrid);
