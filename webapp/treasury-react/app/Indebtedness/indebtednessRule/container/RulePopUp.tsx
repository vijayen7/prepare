import React from "react";
import { observer } from "mobx-react";
import FilterButton from "commons/components/FilterButton";
import { IndebtednessProps } from "../../models/IndebtednessProps";
import PopUpFilters from "../components/PopUpFilters.tsx";
import PopUpSimulatedGrids from "../components/PopUpSimulatedGrids.tsx";
import { Card } from "arc-react-components";
import Dialog from "commons/components/Dialog";

const RulePopUp: React.FC<IndebtednessProps> = (props: IndebtednessProps) => {
  const handleClosePopUp = () => {
    props.store.isPopUpOpen = false;
    props.store.rulePopUpStore.handleReset();
  };

  const handleDelete = () => {
    let columnValues = props.store.selectedGridRow.columnValues;
    let dateArray = columnValues[0].split("-");
    dateArray[1] = props.store.rulePopUpStore.filters.date.replace(/-/g, "");
    columnValues[0] = dateArray.join("-");
    props.store.handleDeleteIndebtednessRule();
  };

  const renderGridData = () => {
    let indebtednessRuleData = props.store.indebtednessRuleDataExistingList;

    let popup = (
      <React.Fragment>
        <Dialog
          isOpen={props.store.isPopUpOpen}
          onClose={handleClosePopUp}
          style={{
            width: "970px",
          }}
          footer={
            props.store.rulePopUpStore.isDeleteClicked ? (
              <FilterButton onClick={handleDelete} label={"Delete Rule"} />
            ) : (
              <React.Fragment>
                <FilterButton
                  onClick={props.store.handleSaveIndebtednessRule}
                  disabled={
                    props.store.rulePopUpStore.isSaveButtonDisabled ||
                    props.store.rulePopUpStore.filters.selectedRowData.length === 0
                  }
                  label="Save Indebtedness Rule"
                />
                <FilterButton onClick={props.store.handleSimulate} label={"Simulate Rule"} />
              </React.Fragment>
            )
          }
        >
          <React.Fragment>
            <Card title="Rule Configuration">
              <hr />
              <div style={{ width: "550px" }}>
                <PopUpFilters store={props.store} />
              </div>
            </Card>
            <PopUpSimulatedGrids store={props.store} />
          </React.Fragment>
        </Dialog>
      </React.Fragment>
    );

    return <React.Fragment>{popup}</React.Fragment>;
  };
  return <>{renderGridData()}</>;
};

export default observer(RulePopUp);
