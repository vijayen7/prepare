import React from "react";
import ColumnLayout from "../../../commons/components/ColumnLayout";
import FilterButton from "../../../commons/components/FilterButton";
import SaveSettingsManager from "../../../commons/components/SaveSettingsManager";
import Sidebar from "../../../commons/components/Sidebar";
import DateFilter from "../../../commons/container/DateFilter";
import PropTypes from "prop-types";
import AanaRuleNameFilter from "../../../commons/container/AanaRuleNameFilter";
import Message from "../../../commons/components/Message";
import { RULECONFIGTYPES } from "../constants";
import SingleSelectFilter from "../../../commons/components/SingleSelectFilter";

export const CollapsableSidebar = ({
  resizeCanvas,
  toggleSidebar,
  getCurrentFilterState,
  applySavedFilters,
  onSelect,
  selectedStartDate,
  selectedEndDate,
  selectedRule,
  isStartDateEndDateINOrder,
  handleSearch,
  handleReset,
  selectedRuleConfigType
}) => {
  return (
    <Sidebar
      size="400px"
      header="Rule Filter"
      resizeCanvas={resizeCanvas}
      toggleSidebar={toggleSidebar}
    >
      {isStartDateEndDateINOrder === false && (
        <Message
          error
          messageData="Start Date cannot be greater than End Date"
        />
      )}
      <SaveSettingsManager
        selectedFilters={getCurrentFilterState()}
        applySavedFilters={applySavedFilters}
        applicationName="AanaFilter"
      />
      <SingleSelectFilter
        onSelect={onSelect}
        stateKey="selectedRuleConfigType"
        selectedData={selectedRuleConfigType}
        data={RULECONFIGTYPES}
        horizontalLayout={true}
        multiSelect={false}
        label="Rule Config Type*"
      />
      <DateFilter
        onSelect={onSelect}
        stateKey="selectedStartDate"
        data={selectedStartDate}
        label="Start Date"
      />
      <DateFilter
        onSelect={onSelect}
        stateKey="selectedEndDate"
        data={selectedEndDate}
        label="End Date"
      />

      {selectedRuleConfigType && <AanaRuleNameFilter
        onSelect={onSelect}
        selectedData={selectedRule}
        multiSelect={true}
        horizontalLayout
        selectedRuleConfigType={selectedRuleConfigType}
      />
      }

      <ColumnLayout>
        <FilterButton onClick={handleSearch} reset={false} disabled={!selectedRuleConfigType} label="Search" />
        <FilterButton reset onClick={handleReset} label="Reset" />
      </ColumnLayout>
    </Sidebar>
  );
};
CollapsableSidebar.propTypes = {
  resizeCanvas: PropTypes.any,
  toggleSidebar: PropTypes.bool.isRequired,
  applySavedFilters: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  selectedStartDate: PropTypes.any,
  selectedEndDate: PropTypes.any,
  selectedRule: PropTypes.array.isRequired,
  handleSearch: PropTypes.func.isRequired,
  handleReset: PropTypes.func.isRequired,
  isStartDateEndDateINOrder: PropTypes.bool,
  selectedRuleConfigType: PropTypes.any
};
