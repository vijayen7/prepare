import React from "react";
import PropTypes from "prop-types";
import { Dialog } from "arc-react-components";

export const SearchRulesFailureDialog = ({
  searchRulesFailureDialogStatus,
  handleSearchRulesFailureDialogClose
}) => {
  return (
    <React.Fragment>
      <Dialog
        title="Search Status"
        isOpen={
          searchRulesFailureDialogStatus !== null &&
          searchRulesFailureDialogStatus.show === true
        }
        onClose={handleSearchRulesFailureDialogClose}
      >
        <div>{searchRulesFailureDialogStatus.message}</div>
      </Dialog>
    </React.Fragment>
  );
};
SearchRulesFailureDialog.prototype = {
  handleSearchRulesFailureDialogClose: PropTypes.func.isRequired
};
