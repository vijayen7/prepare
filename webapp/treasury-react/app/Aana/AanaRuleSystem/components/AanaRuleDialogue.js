import React from "react";
import { Dialog } from "arc-react-components";
import Message from "../../../commons/components/Message";
import CountryFilter from "../../../commons/container/CountryFilter";
import AgreementTypeFilter from "../../../commons/container/AgreementTypeFilter";
import InputFilter from "../../../commons/components/InputFilter";
import DateFilter from "../../../commons/container/DateFilter";
import { PropTypes } from "prop-types";
import SingleSelectFilter from "../../../commons/components/SingleSelectFilter";
import MarketFilter from "../../../commons/container/MarketFilter";
import CurrencyFilter from "../../../commons/container/CurrencyFilter";
import LayoutWithSpace from "../../../commons/components/LayoutWithSpace.js";
import GBOTypeFilter from "../../../commons/container/GBOTypeFilter";
import OptionTypeFilter from "../../../commons/container/OptionTypeFilter";
import { INCLUDE, EXCLUDE, TRUE, FALSE } from "../../../commons/constants";
import AanaRuleNameInputFilter from "../../../commons/container/AanaRuleNameInputFilter";
import { SIMM_RULE_CONFIG_TYPE_ID } from "../constants";

export const AanaRuleDialogue = ({
  isAanaRuleFormDialogOpen,
  handleCloseModal,
  OnSaveClick,
  handleReset,
  onSelect,
  addRuleSuccessStatus,
  selectedSecurityGBOType,
  selectedUnderlyingGBOType,
  selectedStartDate,
  selectedEndDate,
  selectedRule,
  selectedRuleType,
  selectedAgreementTypes,
  selectedCountry,
  selectedMarket,
  isPhysical,
  isNDF,
  isMandatoryFieldFilled,
  warningMessage,
  selectedOptionType,
  selectedCurrency,
  bloomberg,
  local,
  selectedRuleConfigType
}) => {
  const ruleTypeOptions = [INCLUDE, EXCLUDE];
  const isPhysicalOptions = [TRUE, FALSE];
  const isNDFOptions = [TRUE, FALSE];

  return (
    <Dialog
      isOpen={isAanaRuleFormDialogOpen}
      title="Add Rule"
      onClose={handleCloseModal}
      footer={
        <React.Fragment>
          <button onClick={OnSaveClick}>Save</button>
          <button onClick={handleReset}>Reset</button>
        </React.Fragment>
      }
    >
      <LayoutWithSpace>
        {isMandatoryFieldFilled === false &&
          addRuleSuccessStatus.show === false && (
            <Message error messageData={warningMessage} />
          )}
        <DateFilter
          onSelect={onSelect}
          stateKey="selectedStartDate"
          data={selectedStartDate}
          label="Start Date"
          horizontalLayout
        />
        <DateFilter
          onSelect={onSelect}
          stateKey="selectedEndDate"
          data={selectedEndDate}
          label="End Date"
          horizontalLayout
        />
        {selectedRuleConfigType && <AanaRuleNameInputFilter
          onSelect={onSelect}
          selectedData={selectedRule}
          label="Rule Name*"
          multiSelect={false}
          horizontalLayout
          readonly={false}
          selectedRuleConfigType={selectedRuleConfigType}
        />
        }
        {selectedRuleConfigType && <SingleSelectFilter
          data={ruleTypeOptions}
          selectedData={selectedRuleType}
          stateKey="selectedRuleType"
          label="Rule Type*"
          onSelect={onSelect}
          horizontalLayout
        />
        }
        {selectedRuleConfigType && <SingleSelectFilter
          data={isPhysicalOptions}
          selectedData={isPhysical}
          stateKey="isPhysical"
          onSelect={onSelect}
          label="Is Physical"
          horizontalLayout
        />
        }
        {selectedRuleConfigType && <SingleSelectFilter
          data={isNDFOptions}
          selectedData={isNDF}
          stateKey="isNDF"
          onSelect={onSelect}
          label="Is NDF"
          horizontalLayout
        />
        }

        {selectedRuleConfigType && <GBOTypeFilter
          onSelect={onSelect}
          selectedData={selectedSecurityGBOType}
          label="Security GBO Type"
          stateKey="selectedSecurityGBOType"
          multiSelect={false}
          horizontalLayout
        />
        }

        {selectedRuleConfigType && <GBOTypeFilter
          onSelect={onSelect}
          selectedData={selectedUnderlyingGBOType}
          label="Underlying GBO Type"
          stateKey="selectedUnderlyingGBOType"
          multiSelect={false}
          horizontalLayout
        />
        }

        {selectedRuleConfigType && <AgreementTypeFilter
          onSelect={onSelect}
          selectedData={selectedAgreementTypes}
          multiSelect={false}
          horizontalLayout
        />
        }

        {selectedRuleConfigType && <CountryFilter
          onSelect={onSelect}
          selectedData={selectedCountry}
          label="Country"
          stateKey="selectedCountry"
          multiSelect={false}
          horizontalLayout
        />
        }
        {selectedRuleConfigType && <MarketFilter
          onSelect={onSelect}
          selectedData={selectedMarket}
          multiSelect={false}
          horizontalLayout
        />
        }

        {selectedRuleConfigType && selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID && <OptionTypeFilter
          selectedData={selectedOptionType}
          stateKey="selectedOptionType"
          label="Option Type"
          onSelect={onSelect}
          multiSelect={false}
          horizontalLayout
        />
        }

        {selectedRuleConfigType && selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID && <CurrencyFilter
          onSelect={onSelect}
          label="Currency"
          stateKey="selectedCurrency"
          selectedData={selectedCurrency}
          multiSelect={false}
          horizontalLayout
        />
        }

        {selectedRuleConfigType && selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID && <InputFilter
          onSelect={onSelect}
          label="Bloomberg"
          stateKey="bloomberg"
          data={bloomberg ? bloomberg : ''}
          onSelect={onSelect}
        />
        }

        {selectedRuleConfigType && selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID && <InputFilter
          onSelect={onSelect}
          label="Local"
          stateKey="local"
          data={local ? local : ''}
          onSelect={onSelect}
        />
        }

      </LayoutWithSpace>
    </Dialog>
  );
};
AanaRuleDialogue.propTypes = {
  isAanaRuleFormDialogOpen: PropTypes.any,
  handleCloseModal: PropTypes.func.isRequired,
  OnSaveClick: PropTypes.func.isRequired,
  handleReset: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  addRuleSuccessStatus: PropTypes.any,
  selectedSecurityGBOType: PropTypes.any,
  selectedUnderlyingGBOType: PropTypes.any,
  selectedStartDate: PropTypes.any,
  selectedEndDate: PropTypes.any,
  selectedRule: PropTypes.any,
  selectedRuleType: PropTypes.any,
  selectedAgreementTypes: PropTypes.any,
  selectedCountry: PropTypes.any,
  selectedMarket: PropTypes.any,
  isPhysical: PropTypes.any,
  isNDF: PropTypes.any,
  isMandatoryFieldFilled: PropTypes.bool,
  warningMessage: PropTypes.string,
  selectedOptionType: PropTypes.any,
  selectedCurrency: PropTypes.any,
  bloomberg: PropTypes.any,
  local: PropTypes.any,
  selectedRuleConfigType: PropTypes.any
};
