import React from "react";
import ArcDataGrid from "arc-data-grid";
import { PropTypes } from "prop-types";

export const AanaRuleSystemGrid = ({
  getRuleSystemGridColumns,
  ruleSystemData
}) => {
  return (
    <React.Fragment>
      <div>
        <ArcDataGrid
          rows={ruleSystemData.data}
          columns={getRuleSystemGridColumns}
          pinnedColumns={{ actions: true }}
        />
      </div>
    </React.Fragment>
  );
};

AanaRuleSystemGrid.propTypes = {
  ruleSystemData: PropTypes.any,
  getRuleSystemGridColumns: PropTypes.any
};
