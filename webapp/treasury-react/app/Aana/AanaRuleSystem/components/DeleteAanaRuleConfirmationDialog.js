import React from "react";
import PropTypes from "prop-types";
import { Button, Dialog } from "arc-react-components";

export const DeleteAanaRuleConfirmationDialog = ({
  isDeleteButtonClicked,
  onClose,
  onConfirmation
}) => {
  return (
    <Dialog
      title="Delete Confirmation"
      isOpen={isDeleteButtonClicked}
      onClose={onClose}
    >
      <div class="layout--flex size--2 gutter">
        <div>Are you sure you want to delete the rule?</div>
        <div class="form size--content clearfix">
          <div class="row">
            <Button
              className="button--primary size--content"
              onClick={onConfirmation}
            >
              Yes
            </Button>
            <Button className="button--primary size--content" onClick={onClose}>
              No
            </Button>
          </div>
        </div>
      </div>
    </Dialog>
  );
};

DeleteAanaRuleConfirmationDialog.prototype = {
  isDeleteButtonClicked: PropTypes.bool,
  handleDeleteAanaRuleClick: PropTypes.func.isRequired
};
