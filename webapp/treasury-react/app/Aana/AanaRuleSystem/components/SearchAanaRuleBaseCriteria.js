import React from "react";
import PropTypes from "prop-types";
import Message from "../../../commons/components/Message";

export function searchAanaRuleBaseCriteria(ruleSystemData) {
  if (ruleSystemData === null) {
    return <Message messageData="Search to load data." />;
  }

  if (ruleSystemData.data !== undefined && ruleSystemData.data.length === 0) {
    return (
      <Message
        error={true}
        messageData="Data is not available for given search criteria."
      />
    );
  }
  return true;
}

searchAanaRuleBaseCriteria.prototype = {
  ruleSystemData: PropTypes.any
};
