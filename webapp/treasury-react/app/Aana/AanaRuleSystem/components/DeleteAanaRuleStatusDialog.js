import React from "react";
import PropTypes from "prop-types";
import { Dialog } from "arc-react-components";

export const DeleteAanaRuleStatusDialog = ({
  deleteRuleFailureDialogStatus,
  deleteRuleSuccessDialogStatus,
  handleDeleteRuleSuccessDialogClose,
  handleDeleteRuleFailureDialogClose
}) => {
  return (
    <React.Fragment>
      <Dialog
        title="Delete Rule Status"
        isOpen={
          deleteRuleFailureDialogStatus !== null &&
          deleteRuleFailureDialogStatus.show === true
        }
        onClose={handleDeleteRuleFailureDialogClose}
      >
        <div>{deleteRuleFailureDialogStatus.message}</div>
      </Dialog>
      <Dialog
        title="Delete Rule Status"
        isOpen={
          deleteRuleSuccessDialogStatus !== null &&
          deleteRuleSuccessDialogStatus.show === true
        }
        onClose={handleDeleteRuleSuccessDialogClose}
      >
        <div>{deleteRuleSuccessDialogStatus.message}</div>
      </Dialog>
    </React.Fragment>
  );
};
DeleteAanaRuleStatusDialog.prototype = {
  handleDeleteRuleFailureDialogClose: PropTypes.func.isRequired,
  handleDeleteRuleSuccessDialogClose: PropTypes.func.isRequired
};
