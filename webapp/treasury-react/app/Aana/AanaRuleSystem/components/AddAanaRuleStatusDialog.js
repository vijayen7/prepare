import React from "react";
import PropTypes from "prop-types";
import { Dialog } from "arc-react-components";

export const AddAanaRuleStatusDialog = ({
  addRuleFailureDialogStatus,
  addRuleSuccessDialogStatus,
  handleAddRuleSuccessDialogClose,
  handleAddRuleFailureDialogClose
}) => {
  return (
    <React.Fragment>
      <Dialog
        title="Add Rule Status"
        isOpen={
          addRuleSuccessDialogStatus &&
          addRuleSuccessDialogStatus.show === true
        }
        onClose={handleAddRuleSuccessDialogClose}
      >
        <div>{addRuleSuccessDialogStatus.message}</div>
      </Dialog>
      <Dialog
        title="Add Rule Status"
        isOpen={
          addRuleFailureDialogStatus &&
          addRuleFailureDialogStatus.show === true
        }
        onClose={handleAddRuleFailureDialogClose}
      >
        <div>{addRuleFailureDialogStatus.message}</div>
      </Dialog>
    </React.Fragment>
  );
};
AddAanaRuleStatusDialog.prototype = {
  handleAddRuleSuccessDialogClose: PropTypes.func.isRequired,
  handleAddRuleFailureDialogClose: PropTypes.func.isRequired
};


