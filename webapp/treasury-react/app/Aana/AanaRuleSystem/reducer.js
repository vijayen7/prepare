import { combineReducers } from "redux";
import {
  FETCH_RULE_SYSTEM_DATA,
  RESIZE_CANVAS,
  ADD_AANA_RULE_DIALOG,
  ADD_RULE_SUCCESS_DIALOG,
  DELETE_RULE_SUCCESS_DIALOG,
  DELETE_RULE_FAILURE_DIALOG,
  NOT_AVAILABLE
} from "../../commons/constants";
import {
  UPDATE_RULE_CONFIG_TYPE,
  SIMM_RULE_CONFIG_TYPE_ID,
  ADD_RULE_FAILURE_DIALOG,
  SEARCH_RULES_FAILURE_DIALOG,
  SIDEBAR_RULE_CONFIG_TYPE
} from "./constants";
import { getNameFromRefData, getAbbrevFromRefData } from "../../commons/TreasuryUtils";
import { parseDJSrvString } from "../../commons/util";
import _ from "lodash";

let defaultDialogState = { show: false, message: "" };

function normalizeRuleDataForUI(enrichedData) {
  let data = enrichedData.data;
  let ruleConfigTypeId = enrichedData.ruleConfigTypeId;
  if (!data) {
    return data;
  }
  for (let i = 0; i < data.length; i++) {
    data[i].id = i;
    var rule = data[i];
    rule.securityGBOType = rule.securityGBOType ?
      getNameFromRefData(rule.securityGBOType)
      : NOT_AVAILABLE;
    rule.underlyingGBOType = rule.underlyingGBOType ?
      getNameFromRefData(rule.underlyingGBOType)
      : NOT_AVAILABLE;
    rule.agreementType = rule.agreementType ?
      getAbbrevFromRefData(rule.agreementType)
      : NOT_AVAILABLE;
    rule.country = rule.country ?
      getNameFromRefData(rule.country)
      : NOT_AVAILABLE;
    rule.market = rule.market ?
      getNameFromRefData(rule.market)
      : NOT_AVAILABLE;
    rule.startDate = dateFormatter(rule.startDate);
    rule.endDate = dateFormatter(rule.endDate);
    if (rule.isPhysical === undefined) {
      rule.isPhysical = NOT_AVAILABLE;
    }
    if (rule.isNDF === undefined) {
      rule.isNDF = NOT_AVAILABLE;
    }
    if (rule.ruleType !== undefined) {
      rule.ruleType = rule.ruleType === 1 ? "Include" : "Exclude";
    }
    if (ruleConfigTypeId === SIMM_RULE_CONFIG_TYPE_ID && rule.optionType === undefined) {
      rule.optionType = NOT_AVAILABLE;
    }
    if (ruleConfigTypeId === SIMM_RULE_CONFIG_TYPE_ID) {
      rule.currency = rule.currency ?
        getNameFromRefData(rule.currency)
        : NOT_AVAILABLE;
    }
    if (ruleConfigTypeId === SIMM_RULE_CONFIG_TYPE_ID && rule.bloomberg === undefined) {
      rule.bloomberg = NOT_AVAILABLE;
    }
    if (ruleConfigTypeId === SIMM_RULE_CONFIG_TYPE_ID && rule.local === undefined) {
      rule.local = NOT_AVAILABLE;
    }
  }
  return enrichedData;
}

function dateFormatter(value) {
  const date = parseDJSrvString(value);
  if (date) {
    return date.format('YYYY-MM-DD');
  }
  return NOT_AVAILABLE;
}

function fetchRuleDataReducer(state = null, action) {
  switch (action.type) {
    case `${FETCH_RULE_SYSTEM_DATA}_SUCCESS`:
      return normalizeRuleDataForUI(action.enrichedData) || state;
    case `${SEARCH_RULES_FAILURE_DIALOG}_SHOW`:
      return null;
    default:
      return state;
  }
}

function resizeCanvasReducer(state = true, action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state === true ? false : true;
    default:
      return state;
  }
}

function searchRulesFailureDialogReducer(state = defaultDialogState, action) {
  switch (action.type) {
    case `${SEARCH_RULES_FAILURE_DIALOG}_SHOW`:
      return { show: true, message: action.payload };
    case `${SEARCH_RULES_FAILURE_DIALOG}_HIDE`:
      return defaultDialogState;
    default:
      return state;
  }
}

function addRuleSuccessDialogReducer(
  state = defaultDialogState,
  action
) {
  switch (action.type) {
    case `${ADD_RULE_SUCCESS_DIALOG}_SHOW`:
      return { show: true, message: action.payload };
    case `${ADD_RULE_SUCCESS_DIALOG}_HIDE`:
      return defaultDialogState;
    default:
      return state;
  }
}

function addRuleFailureDialogReducer(state = defaultDialogState, action) {
  switch (action.type) {
    case `${ADD_RULE_FAILURE_DIALOG}_SHOW`:
      return { show: true, message: action.payload };
    case `${ADD_RULE_FAILURE_DIALOG}_HIDE`:
      return defaultDialogState;
    default:
      return state;
  }
}

function deleteRuleSuccessDialogReducer(state = defaultDialogState,action) {
  switch (action.type) {
    case `${DELETE_RULE_SUCCESS_DIALOG}_SHOW`:
      return { show: true, message: action.payload };
    case `${DELETE_RULE_SUCCESS_DIALOG}_HIDE`:
      return defaultDialogState;
    default:
      return state;
  }
}

function deleteRuleFailureDialogReducer(state = defaultDialogState, action) {
  switch (action.type) {
    case `${DELETE_RULE_FAILURE_DIALOG}_SHOW`:
      return { show: true, message: action.payload };
    case `${DELETE_RULE_FAILURE_DIALOG}_HIDE`:
      return defaultDialogState;
    default:
      return state;
  }
}

function addAanaRuleFormReducer(state = false, action) {
  switch (action.type) {
    case `${ADD_AANA_RULE_DIALOG}_OPEN`:
      return true;
    case `${ADD_AANA_RULE_DIALOG}_CLOSE`:
      return false;
    default:
      return state;
  }
}

function ruleConfigTypeReducer(state = null, action) {
  switch (action.type) {
    case UPDATE_RULE_CONFIG_TYPE:
      return  action.payload ;
    default:
      return state;
  }
}

function sideBarRuleConfigTypeReducer(state = null, action) {
  switch (action.type) {
    case SIDEBAR_RULE_CONFIG_TYPE:
      return  action.payload ;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  ruleSystemData: fetchRuleDataReducer,
  resizeCanvas: resizeCanvasReducer,
  addRuleSuccessDialog: addRuleSuccessDialogReducer,
  addRuleFailureDialog: addRuleFailureDialogReducer,
  isAanaRuleFormDialogOpen: addAanaRuleFormReducer,
  deleteRuleSuccessDialog: deleteRuleSuccessDialogReducer,
  deleteRuleFailureDialog: deleteRuleFailureDialogReducer,
  ruleConfigType: ruleConfigTypeReducer,
  searchRulesFailureDialog: searchRulesFailureDialogReducer,
  sideBarRuleConfigType: sideBarRuleConfigTypeReducer
});

export default rootReducer;
