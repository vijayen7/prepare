import MapData from "../../commons/models/MapData";

export const RULECONFIGTYPES: MapData[] = [
  { key: 1, value: "AANA Rules" },
  { key: 2, value: "SIMM Regime Rules" },
];

export const UPDATE_RULE_CONFIG_TYPE = "UPDATE_RULE_CONFIG_TYPE";
export const SIMM_RULE_CONFIG_TYPE_ID = 2;
export const ADD_RULE_FAILURE_DIALOG = "ADD_RULE_FAILURE_DIALOG";
export const SEARCH_RULES_FAILURE_DIALOG = "SEARCH_RULES_FAILURE_DIALOG";
export const SIDEBAR_RULE_CONFIG_TYPE = "SIDEBAR_RULE_CONFIG_TYPE";
