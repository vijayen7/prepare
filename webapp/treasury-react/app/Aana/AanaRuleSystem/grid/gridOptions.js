/* eslint-disable no-restricted-globals */

export function gridOptions() {
  return {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid: true,
    page: true,
    highlightRow: true,
    highlightRowOnClick: true,
    exportToExcel: true,
    multiColumnSort: true,
    maxHeight: 600,
    width: {
      forceFitColumns: true,
      forceFitColumnsProportionately: true,
      isAutoWidth: true
    },
    checkboxHeader: {
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    }
  };
}

export function getRuleSystemGridOptions() {
  const options = gridOptions();
  options.sheetName = "Rule System";
  return options;
}
