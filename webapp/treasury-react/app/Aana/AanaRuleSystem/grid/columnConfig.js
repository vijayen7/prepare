/* eslint-disable no-undef */
import React from 'react';
import { textFormatter, dateFormatter } from "../../../commons/grid/formatters";
import { decamelize } from "../../../commons/util";

let columns = [];

const columnIds = [
  "startDate",
  "endDate",
  "ruleType",
  "ruleName",
  "securityGBOType",
  "underlyingGBOType",
  "agreementType",
  "isPhysical",
  "isNDF",
  "country",
  "market"
];

const simmColumnIds = [
  "optionType",
  "currency",
  "bloomberg",
  "local"
];

columnIds.forEach(col => {
  columns.push(col);
});

export function aanaRuleSystemGridColumns(grid) {

  let actionColumn = getActionColumn(grid);
  return [actionColumn].concat(columns);
}

let simmColumns = [...columns];
simmColumnIds.forEach(col => {
  simmColumns.push(col);
});

export function simmRuleSystemGridColumns(grid) {

  let actionColumn = getActionColumn(grid);
  return [actionColumn].concat(simmColumns);
}

export function getActionColumn(grid) {
  const actionColumn = {

    identity: "actions",
    maxWidth: 60,
    disableFilters: true,
    disableSearches: true,
    disableSortBy: true,
    disableExport: true,
    disableResizing: true,
    disableGroupBy: true,
    disableAggregation: true,

    Cell: function(value, context){
      return (
        <>
          <i
            title="Delete"
            className="icon-delete"
            onClick={(e) => {

              let ruleIdsString = context.row.ruleId;
              grid.onSelect(ruleIdsString);
              grid.toggleIsDeleteButtonClicked();

            }}>
          </i>
        </>); }
  };

  return actionColumn;
}

