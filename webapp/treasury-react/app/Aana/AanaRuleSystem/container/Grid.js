import { Button, Panel, Layout } from "arc-react-components";
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { AanaRuleSystemGrid } from "../components/AanaRuleSystemGrid";
import { aanaRuleSystemGridColumns, simmRuleSystemGridColumns } from "../grid/columnConfig";
import { getRuleSystemGridOptions } from "../grid/gridOptions";
import { searchAanaRuleBaseCriteria } from "../components/SearchAanaRuleBaseCriteria";
import { DeleteAanaRuleStatusDialog } from "../components/DeleteAanaRuleStatusDialog";
import { SearchRulesFailureDialog } from "../components/SearchRulesFailureDialog";
import AddRuleForm from "./AddRuleForm";
import {
  deleteAanaRule,
  hideDeleteRuleSuccessDialog,
  OpenAddAanaRuleForm,
  hideDeleteRuleFailureDialog,
  hideSearchRulesFailureDialog
} from "../actions";
import { DeleteAanaRuleConfirmationDialog } from "../components/DeleteAanaRuleConfirmationDialog";
import { SIMM_RULE_CONFIG_TYPE_ID } from "../constants";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.handleDeleteAanaRule = this.handleDeleteAanaRule.bind(this);
    this.toggleIsDeleteButtonClicked = this.toggleIsDeleteButtonClicked.bind(
      this
    );
    this.handleDeleteAanaRuleConfirmed = this.handleDeleteAanaRuleConfirmed.bind(
      this
    );
    this.handleOpenAddAanaRuleForm = this.handleOpenAddAanaRuleForm.bind(this);
    this.renderGridData = this.renderGridData.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.updateState = this.updateState.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.state = this.getDefaultFilters();
  }

  getDefaultFilters() {
    return {
      isRowClicked: false,
      isDeleteButtonClicked: false,
      ruleIds: []
    };
  }

  updateState(params) {
    const { key, value } = params;
    this.setState(previousState => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.ruleSystemData !== this.props.ruleSystemData) {
      this.setState(this.getDefaultFilters());
    }
  }

  handleOpenAddAanaRuleForm() {
    this.props.OpenAddAanaRuleForm();
  }

  toggleIsDeleteButtonClicked() {
    this.updateState({
      key: "isDeleteButtonClicked",
      value: !this.state.isDeleteButtonClicked
    });
  }

  handleDeleteAanaRuleConfirmed() {
    this.handleDeleteAanaRule();
    this.setState(this.getDefaultFilters());
  }

  handleDeleteAanaRule() {
    let ruleIdsString = this.state.ruleIds.join();
    let deleteAanaRulePayload = {
      ruleIds: ruleIdsString,
      ruleConfigTypeId: this.props.selectedRuleConfigType.key
    };

    let payload = {
      deleteAanaRulePayload: deleteAanaRulePayload,
      fetchRuleSystemDataPayload: this.props.ruleSystemData
    };
    this.props.deleteAanaRule(payload);
  }

  onSelect(ruleId) {

    if (ruleId !== null) {

      let ruleIds = [ruleId];

      this.setState({
        ...this.state,
        isRowClicked: true,
        ruleIds: ruleIds
      });
    } else {
      this.setState({
        ...this.state,
        isRowClicked: false
      });
    }
  }

  renderGridData() {
    if (searchAanaRuleBaseCriteria(this.props.ruleSystemData) === true) {
      let gridColumns;
      if (this.props.selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID) {
        gridColumns = simmRuleSystemGridColumns(this);
      } else {
        gridColumns = aanaRuleSystemGridColumns(this);
      }
      return (
        <Panel title={this.props.selectedRuleConfigType.value}>
          <AanaRuleSystemGrid
            ruleSystemData={this.props.ruleSystemData}
            getRuleSystemGridColumns={gridColumns}
          />
        </Panel>
      );
    }
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <Layout.Child childId="aanaRulesGridChild1">
            <div class="row" style={{marginLeft:'10px'}}>
              {this.props.selectedRuleConfigType &&
                <span
                   className="button--primary size--content"
                >
                  {"Rule Configuration Type: "} {this.props.selectedRuleConfigType.value}
                </span>
              }
              {this.props.selectedRuleConfigType &&
                <Button
                  className="button--primary size--content"
                  style={{marginLeft:'5px'}}
                  onClick={this.handleOpenAddAanaRuleForm}
                >
                  Add {this.props.selectedRuleConfigType.value}
                </Button>
              }
              <AddRuleForm
                isAanaRuleFormDialogOpen={this.props.isAanaRuleFormDialogOpen}
              />
              <SearchRulesFailureDialog
                searchRulesFailureDialogStatus={
                  this.props.searchRulesFailureDialogStatus
                }
                handleSearchRulesFailureDialogClose={
                  this.props.hideSearchRulesFailureDialog
                }
              />
              <DeleteAanaRuleStatusDialog
                deleteRuleFailureDialogStatus={
                  this.props.deleteRuleFailureDialogStatus
                }
                deleteRuleSuccessDialogStatus={
                  this.props.deleteRuleSuccessDialogStatus
                }
                handleDeleteRuleSuccessDialogClose={
                  this.props.hideDeleteRuleSuccessDialog
                }
                handleDeleteRuleFailureDialogClose={
                  this.props.hideDeleteRuleFailureDialog
                }
              />
              <DeleteAanaRuleConfirmationDialog
                isDeleteButtonClicked={this.state.isDeleteButtonClicked}
                onClose={this.toggleIsDeleteButtonClicked}
                onConfirmation={this.handleDeleteAanaRuleConfirmed}
              />
            </div>
            <div className="padding">{this.renderGridData()}</div>
          </Layout.Child>
        </Layout>
      </React.Fragment>
    );
  }
}

Grid.propTypes = {
  isAanaRuleFormDialogOpen: PropTypes.bool,
  deleteAanaRule: PropTypes.func.isRequired,
  hideDeleteRuleSuccessDialog: PropTypes.func.isRequired,
  OpenAddAanaRuleForm: PropTypes.func.isRequired,
  hideDeleteRuleFailureDialog: PropTypes.func.isRequired,
  hideSearchRulesFailureDialog: PropTypes.func.isRequired,
  selectedRuleConfigType: PropTypes.any
};

const mapDispatchToProps = {
  deleteAanaRule,
  hideDeleteRuleSuccessDialog,
  OpenAddAanaRuleForm,
  hideDeleteRuleFailureDialog,
  hideSearchRulesFailureDialog
};

function mapStateToProps(state) {
  return {
    deleteRuleSuccessDialogStatus:
      state.aanaRuleSystemData.deleteRuleSuccessDialog,
    isAanaRuleFormDialogOpen: state.aanaRuleSystemData.isAanaRuleFormDialogOpen,
    ruleSystemData: state.aanaRuleSystemData.ruleSystemData,
    resizeCanvas: state.aanaRuleSystemData.resizeCanvas,
    deleteRuleFailureDialogStatus:
      state.aanaRuleSystemData.deleteRuleFailureDialog,
    selectedRuleConfigType: state.aanaRuleSystemData.ruleConfigType,
    searchRulesFailureDialogStatus: state.aanaRuleSystemData.searchRulesFailureDialog
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
