import { CollapsableSidebar } from "../components/CollapsableSidebar";
import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import {
  fetchRuleSystemData, resizeCanvas,
  setSelectedRuleConfigType, saveSideBarRuleConfigType
} from "../actions";
import { getCommaSeparatedValuesOrNull } from "../../../commons/util";

class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
    this.getCurrentFilterState = this.getCurrentFilterState.bind(this);
    this.state = this.getDefaultFilters();
  }

  onSelect(params) {
    const { key, value } = params;
    if (params.key === 'selectedRuleConfigType'
      && (!this.state.selectedRuleConfigType || this.state.selectedRuleConfigType.key !== params.value.key)) {
      this.setState(previousState => {
        return {
          ...previousState,
          [key]: value,
          selectedRule: []
        };
      });
      this.props.saveSideBarRuleConfigType(this.state.selectedRuleConfigType);
    } else {
      this.setState(previousState => {
        return {
          ...previousState,
          [key]: value
        };
      });

    }
  }

  componentDidMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      selectedRule: [],
      toggleSidebar: false,
      selectedStartDate: "",
      selectedEndDate: "",
      isStartDateEndDateINOrder: true,
      selectedRuleConfigType: ""
    };
  }

  getCurrentFilterState() {
    const filterState = {
      selectedStartDate: this.state.selectedStartDate,
      selectedEndDate: this.state.selectedEndDate,
      selectedRule: this.state.selectedRule,
      toggleSidebar: this.state.toggleSidebar,
      selectedRuleConfigType: this.state.selectedRuleConfigType
    };
    return filterState;
  }

  resizeCanvas() {
    this.props.resizeCanvas();
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  handleReset() {
    this.setState({ ...this.getDefaultFilters() });
  }

  handleSearch() {
    let payload = {
      "aanaRuleFilter.validFromDate": this.state.selectedStartDate,
      "aanaRuleFilter.validToDate": this.state.selectedEndDate
    };

    payload["aanaRuleFilter.ruleConfigTypeId"] = this.state.selectedRuleConfigType.key;

    if (
      this.state.selectedRule !== undefined &&
      this.state.selectedRule.length > 0
    ) {
      payload["aanaRuleFilter.ruleNameIds"] = getCommaSeparatedValuesOrNull(
        this.state.selectedRule
      );
    }
    if (this.state.selectedStartDate > this.state.selectedEndDate) {
      this.onSelect({ key: "isStartDateEndDateINOrder", value: false });
    } else {
      this.props.fetchRuleSystemData(payload);
      this.onSelect({ key: "isStartDateEndDateINOrder", value: true });
    }
    this.props.setSelectedRuleConfigType(this.state.selectedRuleConfigType);
  }

  render() {
    return (
      <CollapsableSidebar
        resizeCanvas={this.resizeCanvas}
        toggleSidebar={this.state.toggleSidebar}
        getCurrentFilterState={this.getCurrentFilterState}
        applySavedFilters={this.applySavedFilters}
        onSelect={this.onSelect}
        selectedStartDate={this.state.selectedStartDate}
        selectedEndDate={this.state.selectedEndDate}
        selectedRule={this.state.selectedRule}
        isStartDateEndDateINOrder={this.state.isStartDateEndDateINOrder}
        handleSearch={this.handleSearch}
        handleReset={this.handleReset}
        selectedRuleConfigType={this.state.selectedRuleConfigType}
      />
    );
  }
}
SideBar.propTypes = {
  fetchRuleSystemData: PropTypes.func.isRequired,
  resizeCanvas: PropTypes.func.isRequired,
  setSelectedRuleConfigType: PropTypes.func.isRequired,
  saveSideBarRuleConfigType: PropTypes.func.isRequired
};
const mapDispatchToProps = {
  fetchRuleSystemData,
  resizeCanvas,
  setSelectedRuleConfigType,
  saveSideBarRuleConfigType
};

export default connect(
  null,
  mapDispatchToProps
)(SideBar);
