import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  addAanaRule,
  CloseAddAanaRuleForm,
  OpenAddAanaRuleForm,
  hideAddRuleSuccessDialog,
  hideAddRuleFailureDialog
} from "../actions";
import { AanaRuleDialogue } from "../components/AanaRuleDialogue";
import { AddAanaRuleStatusDialog } from "../components/AddAanaRuleStatusDialog";
import { INCLUDE, TRUE } from "../../../commons/constants";
import { SIMM_RULE_CONFIG_TYPE_ID } from "../constants";

class AddRuleForm extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.OnSaveClick = this.OnSaveClick.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.state = this.getDefaultFilters();
  }

  onSelect(params) {
    const { key, value } = params;
    this.setState(previousState => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  getDefaultFilters() {
    return {
      ...this.props.data,
      selectedStartDate: "1900-01-01",
      selectedEndDate: "2038-01-01",
      selectedRule: null,
      selectedRuleType: null,
      selectedSecurityGBOType: null,
      selectedUnderlyingGBOType: null,
      selectedAgreementTypes: null,
      isPhysical: null,
      isNDF: null,
      selectedCountry: null,
      selectedMarket: null,
      selectedOptionType: null,
      selectedCurrency: null,
      bloomberg: null,
      local: null,
      toggleSideBar: false,
      isMandatoryFieldFilled: true,
      warningMessage: ""
    };
  }
  getToggleSidebar() {
    return this.state.toggleSidebar;
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  isNotDefined(obj) {
    return obj !== undefined && obj !== null;
  }

  handleReset() {
    this.setState(this.getDefaultFilters());
  }

  handleOpenModal() {
    this.props.OpenAddAanaRuleForm();
  }

  handleCloseModal() {
    this.props.CloseAddAanaRuleForm();
    this.setState(this.getDefaultFilters());
  }

  OnSaveClick() {
    let payload = {};
    const addAanaRulePayload = {};


    if (this.isNotDefined(this.props.selectedRuleConfigType)) {
      addAanaRulePayload["aanaRuleInput.ruleConfigTypeId"] =
        this.props.selectedRuleConfigType.key;
      this.onSelect({ key: "isMandatoryFieldFilled", value: true });
    } else {
      this.onSelect({ key: "isMandatoryFieldFilled", value: false });
      this.onSelect({
        key: "warningMessage",
        value: "Rule Config Type cannot be empty!"
      });
      return;
    }

    if (this.isNotDefined(this.state.selectedRuleType)) {
      addAanaRulePayload["aanaRuleInput.ruleType"] =
        this.state.selectedRuleType.key === INCLUDE ? 1 : 0;
      this.onSelect({ key: "isMandatoryFieldFilled", value: true });
      this.onSelect({ key: "warningMessage", value: "" });
    } else {
      this.onSelect({ key: "isMandatoryFieldFilled", value: false });
      this.onSelect({
        key: "warningMessage",
        value: "Rule Type cannot be empty!"
      });
      return;
    }

    if (this.isNotDefined(this.state.selectedEndDate)) {
      addAanaRulePayload[
        "aanaRuleInput.validToDate"
      ] = this.state.selectedEndDate;
    }

    if (this.isNotDefined(this.state.selectedStartDate)) {
      addAanaRulePayload[
        "aanaRuleInput.validFromDate"
      ] = this.state.selectedStartDate;
    }

    if (
      this.isNotDefined(this.state.selectedRule) &&
      this.state.selectedRule.value.length > 0
    ) {
      addAanaRulePayload[
        "aanaRuleInput.ruleName"
      ] = this.state.selectedRule.value;
      this.onSelect({ key: "isMandatoryFieldFilled", value: true });
    } else {
      this.onSelect({ key: "isMandatoryFieldFilled", value: false });
      this.onSelect({
        key: "warningMessage",
        value: "Rule Name cannot be empty!"
      });
      return;
    }

    if (
      this.isNotDefined(this.state.selectedSecurityGBOType) &&
      this.state.selectedSecurityGBOType.key !== -1
    ) {
      addAanaRulePayload[
        "aanaRuleInput.securityGBOType"
      ] = this.state.selectedSecurityGBOType.key;
    }

    if (
      this.isNotDefined(this.state.selectedUnderlyingGBOType) &&
      this.state.selectedUnderlyingGBOType.key !== -1
    ) {
      addAanaRulePayload[
        "aanaRuleInput.underlyingGBOType"
      ] = this.state.selectedUnderlyingGBOType.key;
    }

    if (
      this.isNotDefined(this.state.selectedAgreementTypes) &&
      this.state.selectedAgreementTypes.key !== -1
    ) {
      addAanaRulePayload[
        "aanaRuleInput.agreementType"
      ] = this.state.selectedAgreementTypes.key;
    }

    if (this.isNotDefined(this.state.selectedCountry)) {
      addAanaRulePayload[
        "aanaRuleInput.countryId"
      ] = this.state.selectedCountry.key;
    }

    if (this.isNotDefined(this.state.selectedMarket)) {
      addAanaRulePayload[
        "aanaRuleInput.marketId"
      ] = this.state.selectedMarket.key;
    }

    if (this.isNotDefined(this.state.isPhysical)) {
      addAanaRulePayload["aanaRuleInput.isPhysical"] =
        this.state.isPhysical.key === TRUE ? true : false;
    }

    if (this.isNotDefined(this.state.isNDF)) {
      addAanaRulePayload["aanaRuleInput.isNDF"] =
        this.state.isNDF.key === TRUE ? true : false;
    }

    if (this.props.selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID &&
      this.isNotDefined(this.state.selectedOptionType)) {
      addAanaRulePayload["aanaRuleInput.optionType"] = this.state.selectedOptionType.key;
    }

    if (this.props.selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID &&
      this.isNotDefined(this.state.selectedCurrency) && this.state.selectedCurrency.key !== -1) {
      addAanaRulePayload["aanaRuleInput.currencySpn"] = this.state.selectedCurrency.key;
    }

    if (this.props.selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID &&
      this.isNotDefined(this.state.bloomberg)) {
      addAanaRulePayload["aanaRuleInput.bloomberg"] = this.state.bloomberg.trim();
    }

    if (this.props.selectedRuleConfigType.key === SIMM_RULE_CONFIG_TYPE_ID &&
      this.isNotDefined(this.state.local)) {
      addAanaRulePayload["aanaRuleInput.local"] = this.state.local.trim();
    }

    payload["addAanaRulePayload"] = addAanaRulePayload;
    payload["sideBarRuleConfigType"] = this.props.sideBarRuleConfigType;

    if (this.props.ruleSystemData <= 0) {
      this.props.addAanaRule(payload);
    } else {
      payload["fetchRuleSystemDataPayload"] = this.props.ruleSystemData;
      this.props.addAanaRule(payload);
    }
    this.handleReset();
  }
  render() {
    return (
      <div>
        <AddAanaRuleStatusDialog
          addRuleFailureDialogStatus={
            this.props.addRuleFailureDialog
          }
          addRuleSuccessDialogStatus={this.props.addRuleSuccessDialog}
          handleAddRuleSuccessDialogClose={
            this.props.hideAddRuleSuccessDialog
          }
          handleAddRuleFailureDialogClose={
            this.props.hideAddRuleFailureDialog
          }
        />

        <AanaRuleDialogue
          isAanaRuleFormDialogOpen={this.props.isAanaRuleFormDialogOpen}
          handleCloseModal={this.handleCloseModal}
          OnSaveClick={this.OnSaveClick}
          handleReset={this.handleReset}
          onSelect={this.onSelect}
          addRuleSuccessStatus={this.props.addRuleSuccessDialog}
          selectedGboTypes={this.state.selectedGboTypes}
          selectedSecurityGBOType={this.state.selectedSecurityGBOType}
          selectedUnderlyingGBOType={this.state.selectedUnderlyingGBOType}
          selectedStartDate={this.state.selectedStartDate}
          selectedEndDate={this.state.selectedEndDate}
          selectedRule={this.state.selectedRule}
          selectedRuleType={this.state.selectedRuleType}
          selectedAgreementTypes={this.state.selectedAgreementTypes}
          selectedCountry={this.state.selectedCountry}
          selectedMarket={this.state.selectedMarket}
          isPhysical={this.state.isPhysical}
          isNDF={this.state.isNDF}
          isMandatoryFieldFilled={this.state.isMandatoryFieldFilled}
          warningMessage={this.state.warningMessage}
          selectedOptionType={this.state.selectedOptionType}
          selectedCurrency={this.state.selectedCurrency}
          bloomberg={this.state.bloomberg}
          local={this.state.local}
          selectedRuleConfigType={this.props.selectedRuleConfigType}
        />
      </div>
    );
  }
}

AddRuleForm.propTypes = {
  addAanaRule: PropTypes.any,
  data: PropTypes.any,
  OpenAddAanaRuleForm: PropTypes.func.isRequired,
  CloseAddAanaRuleForm: PropTypes.func.isRequired,
  hideAddRuleSuccessDialog: PropTypes.func.isRequired,
  hideAddRuleFailureDialog: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addAanaRule,
      OpenAddAanaRuleForm,
      CloseAddAanaRuleForm,
      hideAddRuleSuccessDialog,
      hideAddRuleFailureDialog
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    ruleSystemData: state.aanaRuleSystemData.ruleSystemData,
    addRuleSuccessDialog: state.aanaRuleSystemData.addRuleSuccessDialog,
    addRuleFailureDialog: state.aanaRuleSystemData.addRuleFailureDialog,
    selectedRuleConfigType: state.aanaRuleSystemData.ruleConfigType,
    sideBarRuleConfigType: state.aanaRuleSystemData.sideBarRuleConfigType
  };
}

export default connect(mapStateToProps, mapDispatchToProps, null)(AddRuleForm);
