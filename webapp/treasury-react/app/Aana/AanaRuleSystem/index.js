import React, { Component } from "react";
import Loader from "../../commons/container/Loader";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";
import { hot } from "react-hot-loader/root";
import { Layout } from "arc-react-components";

class AanaRuleSystem extends Component {
  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div
              slot="application-menu"
              className="application-menu-toggle-view"
            />
          </arc-header>
          <Layout isColumnType={true} className="border">
            <Layout.Child size={2} childId="gridChild1" collapsible>
              <SideBar />
            </Layout.Child>

            <Layout.Divider isResizable key="sideBarDivider" childId="divider" />

            <Layout.Child size={8} childId="gridChild2" >
              <Grid />
            </Layout.Child>
          </Layout>
        </div>

      </React.Fragment>
    );
  }
}

export default hot(AanaRuleSystem);
