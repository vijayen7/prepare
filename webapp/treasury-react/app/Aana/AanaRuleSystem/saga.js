import { put, takeEvery, call } from "redux-saga/effects";
import { getAanaRuleData, addAanaRule, deleteAanaRule } from "./api";
import {
  FETCH_RULE_SYSTEM_DATA,
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
  ADD_AANA_RULE,
  DELETE_AANA_RULE,
  RULE_NAME_FILTER
} from "../../commons/constants";
import { checkForException } from "../../commons/util";
import {
  CloseAddAanaRuleForm,
  showAddRuleSuccessDialog,
  showAddRuleFailureDialog,
  showDeleteRuleSuccessDialog,
  fetchRuleSystemData,
  showDeleteRuleFailureDialog,
  showSearchRulesFailureDialog
} from "./actions.js";
import { fetchFilter } from "../../commons/actions";

function enrichingDataWithFetchAanaRuleCriteria(data, payload) {
  let enrichedData = {
    data: data,
    startDate: payload["aanaRuleFilter.validFromDate"],
    endDate: payload["aanaRuleFilter.validToDate"],
    ruleConfigTypeId: payload["aanaRuleFilter.ruleConfigTypeId"]
  };
  if (payload["aanaRuleFilter.ruleNameIds"] !== undefined) {
    enrichedData["ruleNameIds"] = payload["aanaRuleFilter.ruleNameIds"];
  }
  return enrichedData;
}

function preparePayloadForFetchRuleSystem(data) {
  let fetchRuleSystemDataPayload = {
    "aanaRuleFilter.validFromDate": data.startDate,
    "aanaRuleFilter.validToDate": data.endDate,
    "aanaRuleFilter.ruleConfigTypeId": data.ruleConfigTypeId
  };
  if (data.ruleNameIds !== undefined) {
    fetchRuleSystemDataPayload["aanaRuleFilter.ruleNameIds"] = data.ruleNameIds;
  }
  return fetchRuleSystemDataPayload;
}

function* getAanaRuleSystemData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getAanaRuleData, action.payload);
    checkForException(data);
    let enrichedData = enrichingDataWithFetchAanaRuleCriteria(
      data,
      action.payload
    );
    yield put({
      type: `${FETCH_RULE_SYSTEM_DATA}_SUCCESS`,
      enrichedData,
      payload: action.payload
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put(
      showSearchRulesFailureDialog("Exception Caught while fetching rules : " + e)
    );
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* addedAanaRule(action) {
  try {
    yield put({ type: START_LOADING });
    yield put(CloseAddAanaRuleForm());
    const data = yield call(addAanaRule, action.payload.addAanaRulePayload);
    checkForException(data);
    if (data) {
      if (action.payload.sideBarRuleConfigType && action.payload.addAanaRulePayload &&
        action.payload.sideBarRuleConfigType.key ===
        action.payload.addAanaRulePayload['aanaRuleInput.ruleConfigTypeId']) {
        let filter = {
          key: RULE_NAME_FILTER,
          filterData: { ruleConfigTypeId: action.payload.sideBarRuleConfigType.key }
        };
        yield put(fetchFilter(filter));
      }
      yield put(showAddRuleSuccessDialog("Rule added successfully"));
    } else {
      yield put(showAddRuleFailureDialog("Unable to add rule."));
    }

    // refresh grid if the added rule falls under search criteria
    if (
      data === true &&
      action.payload.fetchRuleSystemDataPayload !== undefined
    ) {
      let fetchRuleSystemDataPayload = preparePayloadForFetchRuleSystem(
        action.payload.fetchRuleSystemDataPayload
      );
      yield put(fetchRuleSystemData(fetchRuleSystemDataPayload));
    }
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put(
      showAddRuleFailureDialog("Exception Caught while adding rule:" + e)
    );
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* deletedAanaRule(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(
      deleteAanaRule,
      action.payload.deleteAanaRulePayload
    );
    checkForException(data);
    if (data === true) {
      yield put(showDeleteRuleSuccessDialog("Delete is done successfully"));
    } else if (data === false) {
      yield put(showDeleteRuleFailureDialog("Unable to delete existing rule."));
    }

    if (
      data === true &&
      action.payload.fetchRuleSystemDataPayload !== undefined
    ) {
      let fetchRuleSystemDataPayload = preparePayloadForFetchRuleSystem(
        action.payload.fetchRuleSystemDataPayload
      );
      yield put(fetchRuleSystemData(fetchRuleSystemDataPayload));
    }
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put(
      showDeleteRuleFailureDialog(
        "Exception caught while deleting existing rule: " + e
      )
    );
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* aanaRuleSystemSaga() {
  yield [
    takeEvery(FETCH_RULE_SYSTEM_DATA, getAanaRuleSystemData),
    takeEvery(ADD_AANA_RULE, addedAanaRule),
    takeEvery(DELETE_AANA_RULE, deletedAanaRule)
  ];
}

export default aanaRuleSystemSaga;
