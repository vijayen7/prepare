import {
  RESIZE_CANVAS,
  FETCH_RULE_SYSTEM_DATA,
  ADD_AANA_RULE,
  ADD_AANA_RULE_DIALOG,
  ADD_RULE_SUCCESS_DIALOG,
  DELETE_AANA_RULE,
  DELETE_RULE_SUCCESS_DIALOG,
  DELETE_RULE_FAILURE_DIALOG
} from "../../commons/constants";
import {
  UPDATE_RULE_CONFIG_TYPE,
  ADD_RULE_FAILURE_DIALOG,
  SEARCH_RULES_FAILURE_DIALOG,
  SIDEBAR_RULE_CONFIG_TYPE
} from "./constants";

export function fetchRuleSystemData(payload) {
  return {
    type: FETCH_RULE_SYSTEM_DATA,
    payload
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}
export function deleteAanaRule(payload) {
  return {
    type: DELETE_AANA_RULE,
    payload
  };
}
export function addAanaRule(payload) {
  return {
    type: ADD_AANA_RULE,
    payload
  };
}

export function showAddRuleSuccessDialog(payload) {
  return {
    type: `${ADD_RULE_SUCCESS_DIALOG}_SHOW`,
    payload
  };
}

export function hideAddRuleSuccessDialog() {
  return {
    type: `${ADD_RULE_SUCCESS_DIALOG}_HIDE`
  };
}

export function showAddRuleFailureDialog(payload) {
  return {
    type: `${ADD_RULE_FAILURE_DIALOG}_SHOW`,
    payload
  };
}

export function hideAddRuleFailureDialog(payload) {
  return {
    type: `${ADD_RULE_FAILURE_DIALOG}_HIDE`,
    payload
  };
}

export function showDeleteRuleSuccessDialog(payload) {
  return {
    type: `${DELETE_RULE_SUCCESS_DIALOG}_SHOW`,
    payload
  };
}

export function hideDeleteRuleSuccessDialog() {
  return {
    type: `${DELETE_RULE_SUCCESS_DIALOG}_HIDE`
  };
}

export function OpenAddAanaRuleForm() {
  return {
    type: `${ADD_AANA_RULE_DIALOG}_OPEN`
  };
}

export function CloseAddAanaRuleForm() {
  return {
    type: `${ADD_AANA_RULE_DIALOG}_CLOSE`
  };
}

export function showDeleteRuleFailureDialog(payload) {
  return {
    type: `${DELETE_RULE_FAILURE_DIALOG}_SHOW`,
    payload
  };
}

export function hideDeleteRuleFailureDialog() {
  return {
    type: `${DELETE_RULE_FAILURE_DIALOG}_HIDE`
  };
}

export function showSearchRulesFailureDialog(payload) {
  return {
    type: `${SEARCH_RULES_FAILURE_DIALOG}_SHOW`,
    payload
  };
}

export function hideSearchRulesFailureDialog() {
  return {
    type: `${SEARCH_RULES_FAILURE_DIALOG}_HIDE`
  };
}

export function setSelectedRuleConfigType(payload) {
  return {
    type: UPDATE_RULE_CONFIG_TYPE,
    payload
  };
}

export function saveSideBarRuleConfigType(payload) {
  return {
    type: SIDEBAR_RULE_CONFIG_TYPE,
    payload
  };
}
