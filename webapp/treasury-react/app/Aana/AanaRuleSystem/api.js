import { BASE_URL } from "../../commons/constants";
const queryString = require("query-string");
export let url = "";

export function getAanaRuleData(payload) {
  const paramString = queryString.stringify(payload);
  const saveAsRuleExt =
    "service/aanaRuleService/getAanaRules?inputFormat=PROPERTIES";
  const url = `${BASE_URL}${saveAsRuleExt}&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function addAanaRule(payload) {
  const paramString = queryString.stringify(payload);
  const saveAsRuleExt =
    "service/aanaRuleService/addAanaRule?inputFormat=PROPERTIES";
  const url = `${BASE_URL}${saveAsRuleExt}&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}

export function deleteAanaRule(payload) {
  const paramString = queryString.stringify(payload);
  const saveAsRuleExt =
    "service/aanaRuleService/invalidateAanaRules?inputFormat=PROPERTIES";
  const url = `${BASE_URL}${saveAsRuleExt}&${paramString}&format=JSON`;
  return fetch(url, {
    credentials: "include"
  }).then(data => data.json());
}
