/* eslint-disable no-undef */
import {
  textFormatter,
  percentFormatter
} from "../../../commons/grid/formatters";
import { decamelize } from "../../../commons/util";
import { SummationAggregator } from "../../../commons/grid/aggregators";
import {getFormattedFinancialNumericColumn} from "../../../commons/TreasuryUtils";

const notionalColumns = [
  getFormattedFinancialNumericColumn("grossNotional"),
  getFormattedFinancialNumericColumn("grossAverageNotional"),
  getFormattedFinancialNumericColumn("minimumNotional"),
  getFormattedFinancialNumericColumn("medianNotional"),
  getFormattedFinancialNumericColumn("maximumNotional")
];

export function gridGrossNotionalColumns() {
  let columns = [
    {
      header: "Legal Entity",
      identity: "legalEntityName"
    },
    {
      header: "Reporting Currency of LE",
      identity: "currencyName"
    } ];

    notionalColumns.forEach(col => {
      columns.push(col);
    });

    return columns;
}

export function gridNotionalByFilterColumns() {
  let columns = [];
  const gboTypeColumn = {
    header: "GBO Type",
    identity: "gboTypeName"
  };

  columns.push(gboTypeColumn);

  notionalColumns.forEach(col => {
    columns.push(col);
  });

  const percentLeColumn = getFormattedFinancialNumericColumn(
    "percentageNotionalOnLegalEntity", "%", "% Notional");
  columns.push(percentLeColumn);

  return columns;
}

export function gridNotionalByEcpeFilterColumns() {
  var columns = [];
  const ecpeColumn = {
    header: "Exposure Counterparty",
    identity: "ecpeName"
  };

  columns.push(ecpeColumn);

  notionalColumns.forEach(col => {

    columns.push(col);
  });

  const percentLeColumn = getFormattedFinancialNumericColumn(
    "percentageNotionalOnLegalEntity", "%", "% Notional");

  columns.push(percentLeColumn);

  return columns;

}
