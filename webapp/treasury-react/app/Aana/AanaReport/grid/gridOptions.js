/* eslint-disable no-restricted-globals */
import cloneDeep from "lodash/cloneDeep";
import { url, reportManagerUrl } from "../api";

const const_options = {
  autoHorizontalScrollBar: true,
  applyFilteringOnGrid: true,
  page: true,
  highlightRow: true,
  onRowClick: function() {
    // highlight on row works if this callback is defined
  },
  highlightClass: "highlight-strong",
  highlightRowOnClick: true,
  exportToExcel: true,
  multiColumnSort: true,

  summaryRow: true,
  displaySummaryRow: true,
  width: {
    forceFitColumns: true,
    forceFitColumnsProportionately: true,
    isAutoWidth: true
  }
};

export function getGrossNotionalGridOptions() {
  let options = cloneDeep(const_options);
  options.sheetName = "Total Gross Notionals";
  options.displaySummaryRow = true;
  options.dynamicSummaryRow = {
    isSummaryRowvisible: true
  };
  options.maxHeight=320;
  options.summaryRowText = "Grand Total";
  options.rowSelection = {
    highlightRowOnClick: true
  };
  options.headerMessage = "Legal Entity Details";
  options.configureReport = {
    urlCallback: function() {
      return window.location.origin + url;
    }
  };
  return options;
}

export function getNotionalByFilterGridOptions(legalEntity , ecpe) {
  let options = cloneDeep(const_options);

  options.sheetName = "Total Gross Notional by GBO Type";
  options.displaySummaryRow = false;
  options.configureReport = {
    urlCallback: function() {
      return window.location.origin + reportManagerUrl;
    }
  };
  options.maxHeight = 120;
  if (ecpe !=""){
    options.headerMessage =
      "Asset Class Details for Legal Entity: " + legalEntity +" and Exposure Counterparty: "+ ecpe;}
  else {
    options.headerMessage =
      "Asset Class Details for Legal Entity: " + legalEntity;}

  return options;
}

export function getNotionalByEcpeFilterGridOptions(legalEntity) {
  let options = cloneDeep(const_options);

  options.sheetName = "Total Gross Notional by Exposure Counterparty";
   options.rowSelection = {
     highlightRowOnClick: true
  };
  options.displaySummaryRow = false;
  options.maxHeight = 120;
  options.configureReport = {
    urlCallback: function() {
      return window.location.origin + reportManagerUrl;
    }
  };
  options.headerMessage =
    "Exposure Counterparty Details for Legal Entity: "+ legalEntity;

  return options;
}
