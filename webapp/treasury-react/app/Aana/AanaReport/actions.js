import {
  RESIZE_CANVAS,
  FETCH_GROSS_NOTIONAL_DATA,
  FETCH_NOTIONAL_DATA_BY_FILTER,
  FETCH_NOTIONAL_DATA_BY_LE_ECPE_FILTER,
  FETCH_NOTIONAL_DATA_BY_ECPE_FILTER,
  FETCH_EMPTY_GRID_AT_BOTTOM,
  ERROR_MSG_SELECT_ONLY_ONE_FIELD
} from "commons/constants";

export function fetchGrossNotionalData(payload) {
  return {
    type: FETCH_GROSS_NOTIONAL_DATA,
    payload
  };
}

export function showForOrMessage() {
  return {
    type: ERROR_MSG_SELECT_ONLY_ONE_FIELD
  };
}

export function fetchNotionalDataByFilter(payload) {
  return {
    type: FETCH_NOTIONAL_DATA_BY_ECPE_FILTER,
    type: FETCH_NOTIONAL_DATA_BY_FILTER,
    payload
  };
}

export function fetchNotionalDataByLeEcpeFilter(ecpe) {
  return {
    type: FETCH_NOTIONAL_DATA_BY_LE_ECPE_FILTER,
    ecpe
  };
}

export function fetchBottomEmptyGrid() {
  return {
    type: FETCH_EMPTY_GRID_AT_BOTTOM
  };
}

export function resizeCanvas() {
  return {
    type: RESIZE_CANVAS
  };
}

/**
 * Returns the entire row corresponding to rowId and data
 */
export function fetchDataWithInputRowId(rowId, data) {
  return data.find(element => element.id === rowId)
}
