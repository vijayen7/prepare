import React, { Component } from "react";
import Loader from "../../commons/container/Loader";
import SideBar from "./container/SideBar";
import Grid from "./container/Grid";
import { hot } from "react-hot-loader/root";
import { LINK_TO_ADD_AANA_RULES } from "../../commons/constants";
import { Layout } from "arc-react-components";

class AanaReport extends Component {
  render() {
    return (
      <React.Fragment>
        <Loader />
        <div className="layout--flex--row">
          <arc-header
            className="size--content"
            user={USER}
            modern-themes-enabled
          >
            <div
              slot="application-menu"
              className="application-menu-toggle-view"
            >
              <a
                href={LINK_TO_ADD_AANA_RULES}
                target="_blank"
                rel="noopener noreferrer"
              >
                Rule Set Details
              </a>
            </div>
          </arc-header>
          <Layout isColumnType={true} className="border">
            <Layout.Child size={2} childId="gridChild1" collapsible>
              <SideBar />
            </Layout.Child>

            <Layout.Divider isResizable key="sideBarDivider" childId="divider" />

            <Layout.Child size={8} childId="gridChild2" >
              <Grid />
            </Layout.Child>
          </Layout>
        </div>
      </React.Fragment>
    );
  }
}

export default hot(AanaReport);
