import { AanaReportGrid } from "../components/AanaReportGrid";
import React, { Component } from "react";
import {
  gridGrossNotionalColumns,
  gridNotionalByEcpeFilterColumns,
  gridNotionalByFilterColumns
} from "../grid/columnConfig";
import {
  getGrossNotionalGridOptions,
  getNotionalByEcpeFilterGridOptions,
  getNotionalByFilterGridOptions
} from "../grid/gridOptions";
import Message from "commons/components/Message";
import { connect } from "react-redux";

import { fetchNotionalDataByFilter,
        fetchNotionalDataByLeEcpeFilter,
        fetchDataWithInputRowId
        }
        from "../actions";

class Grid extends Component {
  constructor(props) {
    super(props);
    this.renderGridData = this.renderGridData.bind(this);
    this.onRowClickHandler = this.onRowClickHandler.bind(this);
    this.onEcpeRowClickHandler = this.onEcpeRowClickHandler.bind(this);
  }

  onRowClickHandler(args) {

    if (args !== "") {

      let legalEntityData = fetchDataWithInputRowId(
        args, this.props.grossNotionalData.data);

      let payload = {
        "marginReportingFilter.startDate": this.props.grossNotionalData
          .startDate,
        "marginReportingFilter.endDate": this.props.grossNotionalData.endDate,
        "marginReportingFilter.gboTypeIds": this.props.grossNotionalData
          .gboTypeIds,
        "marginReportingFilter.legalEntityIds": legalEntityData.legalEntityId,
        "marginReportingFilter.ruleId": this.props.grossNotionalData.ruleId
      };
      this.legalEntity = legalEntityData.legalEntityName;
      this.ecpe = "";
      this.props.fetchNotionalDataByFilter(payload);
    }
  }

    onEcpeRowClickHandler(args) {

        if(args !== "") {

        let ecpeData = fetchDataWithInputRowId(args, this.props.notionalDataByEcpeFilter);

        this.ecpe = ecpeData.ecpeName;
        this.props.fetchNotionalDataByLeEcpeFilter(ecpeData);
      }
    }

  renderGridData() {
    if (this.props.grossNotionalData <= 0)
      return <Message messageData="Search to load data." />;

    if (this.props.grossNotionalData === "Wrong Selection") {
      return (
        <Message
          error={true}
          messageData="Please select only one of 'GBO Type' or 'Rule Name'."
        />
      );
    }
    if (this.props.grossNotionalData.data.length === 0)
      return (
        <Message
          warning={true}
          messageData="Data is not available for given search criteria."
        />
      );

    return (
      <AanaReportGrid
        gridGrossNotionalColumns={gridGrossNotionalColumns}
        getGrossNotionalGridOptions={getGrossNotionalGridOptions}
        gridNotionalByEcpeFilterColumns={gridNotionalByEcpeFilterColumns}
        getNotionalByEcpeFilterGridOptions={getNotionalByEcpeFilterGridOptions}
        gridNotionalByFilterColumns={gridNotionalByFilterColumns}
        getNotionalByFilterGridOptions={getNotionalByFilterGridOptions}
        grossNotionalData={this.props.grossNotionalData}
        onRowClickHandler={this.onRowClickHandler}
        onEcpeRowClickHandler={this.onEcpeRowClickHandler}
        legalEntity={this.legalEntity}
        ecpe={this.ecpe}
        resizeCanvas={this.props.resizeCanvas}
        notionalDataByFilter={this.props.notionalDataByFilter}
        notionalDataByEcpeFilter={this.props.notionalDataByEcpeFilter}
      />
    );
  }

  render() {
    return this.renderGridData();
  }
}

const mapDispatchToProps = {
  fetchNotionalDataByFilter,
  fetchNotionalDataByLeEcpeFilter
};

function mapStateToProps(state) {
  return {
    grossNotionalData: state.aanaData.grossNotionalData,
    notionalDataByEcpeFilter: state.aanaData.notionalDataByEcpeFilter,
    notionalDataByFilter: state.aanaData.notionalDataByFilter,
    resizeCanvas: state.aanaData.resizeCanvas
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
