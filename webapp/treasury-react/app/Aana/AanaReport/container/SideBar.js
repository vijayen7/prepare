import { CollapsableSidebar } from "../components/CollapsableSidebar";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchGrossNotionalData,
  fetchNotionalDataByFilter,
  fetchBottomEmptyGrid,
  showForOrMessage,
  resizeCanvas
} from "../actions";
import { getCommaSeparatedValuesOrNull } from "../../../commons/util";

const USD_KEY = 1760000;
class SideBar extends Component {
  constructor(props) {
    super(props);

    this.onSelect = this.onSelect.bind(this);
    this.onGboSelect = this.onGboSelect.bind(this);
    this.onRuleSelect = this.onRuleSelect.bind(this);
    this.onToggleButtonChange = this.onToggleButtonChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.applySavedFilters = this.applySavedFilters.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDefaultFilters = this.getDefaultFilters.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
    this.getCurrentFilterState = this.getCurrentFilterState.bind(this);
    this.state = this.getDefaultFilters();
  }

  onSelect(params) {
    const { key, value } = params;
    this.setState(previousState => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  onGboSelect(params) {
    const { key, value } = params;
    this.state.selectedRule = "";
    this.setState(previousState => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  onRuleSelect(params) {
    const { key, value } = params;
    this.state.selectedGboTypes= [];
    this.setState(previousState => {
      return {
        ...previousState,
        [key]: value
      };
    });
  }

  onToggleButtonChange(value) {
    if (value === "Toggle 1"){
      this.setState(previousState => {
        return {
          ...previousState,
          showToggle1: true,
          showToggle2: false
        };
      });
    }
    else if (value === "Toggle 2"){
      this.setState(previousState => {
        return {
          ...previousState,
          showToggle2: true,
          showToggle1: false
        };
      });
    }
  }

  componentDidMount() {
    this.setState(this.getDefaultFilters());
  }

  getDefaultFilters() {
    return {
      selectedLegalEntities: [],
      selectedGboTypes: [],
      selectedCurrencies: USD_KEY,
      selectedRule: "",
      toggleSidebar: false,
      selectedStartDate: "",
      selectedEndDate: "",
      isStartDateEndDateINOrder: true
    };
  }

  getCurrentFilterState() {
    const filterState = {
      selectedLegalEntities: this.state.selectedLegalEntities,
      selectedStartDate: this.state.selectedStartDate,
      selectedEndDate: this.state.selectedEndDate,
      selectedGboTypes: this.state.selectedGboTypes,
      selectedCurrencies: this.state.selectedCurrencies,
      selectedRule: this.state.selectedRule,
      toggleSidebar: this.state.toggleSidebar,
      showToggle1: this.state.showToggle1,
      showToggle2: this.state.showToggle2
    };
    return filterState;
  }

  resizeCanvas() {
    this.props.resizeCanvas();
  }

  applySavedFilters(selectedFilters) {
    this.setState(selectedFilters);
  }

  handleReset() {
    this.setState({ ...this.getDefaultFilters() });
  }

  handleSearch() {
    const startDate = this.state.selectedStartDate;
    const endDate = this.state.selectedEndDate;
    if (startDate > endDate) {
      this.onSelect({ key: "isStartDateEndDateINOrder", value: false });
      return;
    }
    this.onSelect({ key: "isStartDateEndDateINOrder", value: true });
    let currencySpn = "";
    // Resets the currency to USD if not selected anything
    if (
      this.state.selectedCurrencies === null ||
      this.state.selectedCurrencies === undefined
    ) {
      this.onSelect({
        key: "selectedCurrencies",
        value: {
          key: 1760000,
          value: ""
        }
      });
      currencySpn = USD_KEY;
    } else {
      currencySpn = this.state.selectedCurrencies.key;
    }

    let ruleId =
      this.state.selectedRule && this.state.selectedRule.key
        ? this.state.selectedRule.key
        : undefined;
    let payload = {
      "marginReportingFilter.startDate": this.state.selectedStartDate,
      "marginReportingFilter.endDate": this.state.selectedEndDate,
      "marginReportingFilter.legalEntityIds": getCommaSeparatedValuesOrNull(
        this.state.selectedLegalEntities
      ),
      "marginReportingFilter.gboTypeIds": getCommaSeparatedValuesOrNull(
        this.state.selectedGboTypes
      ),
      "marginReportingFilter.ruleId": ruleId,
      "marginReportingFilter.currencySpn": currencySpn
    };
    if (
      this.state.selectedRule !== null &&
      this.state.selectedRule !== "" &&
      this.state.selectedGboTypes.length !== 0
    ) {
      this.props.showForOrMessage();
      return;
    }
    this.props.fetchGrossNotionalData(payload);
    this.props.fetchBottomEmptyGrid();
  }

  render() {
    return (
      <CollapsableSidebar
        resizeCanvas={this.resizeCanvas}
        toggleSidebar={this.state.toggleSidebar}
        getCurrentFilterState={this.getCurrentFilterState}
        applySavedFilters={this.applySavedFilters}
        onSelect={this.onSelect}
        onGboSelect={this.onGboSelect}
        onRuleSelect={this.onRuleSelect}
        showToggle1={this.state.showToggle1}
        showToggle2={this.state.showToggle2}
        onToggleButtonChange={this.onToggleButtonChange}
        isStartDateEndDateINOrder={this.state.isStartDateEndDateINOrder}
        selectedStartDate={this.state.selectedStartDate}
        selectedEndDate={this.state.selectedEndDate}
        selectedLegalEntities={this.state.selectedLegalEntities}
        selectedCurrency={this.state.selectedCurrencies}
        selectedRule={this.state.selectedRule}
        selectedGboTypes={this.state.selectedGboTypes}
        handleSearch={this.handleSearch}
        handleReset={this.handleReset}
      />
    );
  }
}

const mapDispatchToProps = {
  fetchGrossNotionalData,
  fetchNotionalDataByFilter,
  fetchBottomEmptyGrid,
  showForOrMessage,
  resizeCanvas
};

export default connect(null, mapDispatchToProps)(SideBar);
