import { BASE_URL } from "commons/constants";
import { fetchPostURL } from "../../commons/util"
import { CLASS_NAMESPACE } from "../../commons/ClassConfigs"

const queryString = require("query-string");

const modelconfig = {
  marginReportingFilter: {
    legalEntityIds: "marginReportingFilter.legalEntityIds",
    gboTypeIds: "marginReportingFilter.gboTypeIds",
    startDate: "marginReportingFilter.startDate",
    endDate: "marginReportingFilter.endDate",
    ruleId: "marginReportingFilter.ruleId",
    currencySpn: "marginReportingFilter.currencySpn"
  }
}

export let url = "";
export let reportManagerUrl = "";

export function getGrossNotionalData(payloadForGrossNotionalData) {

  let legalEntityPayLoad = payloadForGrossNotionalData[modelconfig.marginReportingFilter.legalEntityIds];

  let legalEntityIds = legalEntityPayLoad != "__null__" ? legalEntityPayLoad.split(",").map(
    function(strVale){return Number(strVale);}).filter(n => n != -1): null;

  let gboTypePayLoad = payloadForGrossNotionalData[modelconfig.marginReportingFilter.gboTypeIds];

  let gboTypeIds = gboTypePayLoad != "__null__" ?
      gboTypePayLoad.split(",").map(function(strVale){return Number(strVale);}).
      filter(n => n != -1) : null;

  let payload = {
    "@CLASS": CLASS_NAMESPACE.deshaw.treasury.common.model.margin.MarginReportingFilter,
    "startDate": payloadForGrossNotionalData[modelconfig.marginReportingFilter.startDate],
    "endDate": payloadForGrossNotionalData[modelconfig.marginReportingFilter.endDate],
    "legalEntityIds": legalEntityIds,
    "gboTypeIds": gboTypeIds,
    "ruleId": payloadForGrossNotionalData[modelconfig.marginReportingFilter.ruleId],
    "currencySpn": payloadForGrossNotionalData[modelconfig.marginReportingFilter.currencySpn]
    };

  const params = `marginReportingFilter=${encodeURIComponent(
    JSON.stringify(payload))}&inputFormat=json&format=json`;
  url = `${BASE_URL}service/notionalSynopsisService/getNotionalSynopsis`;

  return fetchPostURL(url, params);
}

export function getNotionalDataByFilter(payloadForNotionalByFilter) {
  const paramString = queryString.stringify(payloadForNotionalByFilter);
  const filteredUrl = `${BASE_URL}service/notionalSynopsisService/getLegalEntityNotionalSynopsis?inputFormat=PROPERTIES&${paramString}&format=JSON`;
  reportManagerUrl = filteredUrl.replace("?", "List?");
  var dataOb = fetch(filteredUrl, {
    credentials: "include"
  }).then(data => data.json());
  return dataOb;
}
