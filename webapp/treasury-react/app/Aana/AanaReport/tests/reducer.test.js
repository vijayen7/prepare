import { notionalDataByFilterReducer } from "../reducer";
import { FETCH_NOTIONAL_DATA_BY_FILTER } from "commons/constants";

describe("testing reducer", () => {
  it("reducer test for grossNotionalData", () => {
    let payload = {
      "AanaFlter.test": "Test"
    };
    const testAction = {
      type: FETCH_NOTIONAL_DATA_BY_FILTER,
      payload
    };
    expect(notionalDataByFilterReducer([], testAction)).toMatchSnapshot();
  });
});
