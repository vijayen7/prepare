import { FETCH_GROSS_NOTIONAL_DATA } from "commons/constants";
import { fetchGrossNotionalData } from "../actions";

describe("actions", () => {
  it("should create an action for grossNotionalData", () => {
    let payload = {
      "marginRportingFilter.test": "Test"
    };
    const expectedAction = {
      type: FETCH_GROSS_NOTIONAL_DATA,
      payload
    };
    expect(fetchGrossNotionalData(payload)).toEqual(expectedAction);
  });
});
