import { put, takeEvery, call } from "redux-saga/effects";
import { getGrossNotionalData, getNotionalDataByFilter } from "./api";
import {
  FETCH_GROSS_NOTIONAL_DATA,
  FETCH_NOTIONAL_DATA_BY_ECPE_FILTER,
  FETCH_NOTIONAL_DATA_BY_LE_ECPE_FILTER,
  FETCH_NOTIONAL_DATA_BY_FILTER,
  FETCH_EMPTY_GRID_AT_BOTTOM,
  START_LOADING,
  END_LOADING,
  HANDLE_EXCEPTION,
  ERROR_MSG_SELECT_ONLY_ONE_FIELD,
  RESIZE_CANVAS
} from "../../commons/constants";
import { checkForException } from "../../commons/util";
var filterdata;

function* fetchGrossNotionalData(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getGrossNotionalData, action.payload);
    let enrichedData = {
      data: data,
      startDate: action.payload["marginReportingFilter.startDate"],
      endDate: action.payload["marginReportingFilter.endDate"],
      gboTypeIds: action.payload["marginReportingFilter.gboTypeIds"],
      ruleId: action.payload["marginReportingFilter.ruleId"]
    };

    yield put({
      type: `${FETCH_GROSS_NOTIONAL_DATA}_SUCCESS`,
      enrichedData,
      payload: action.payload
    });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_GROSS_NOTIONAL_DATA}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchNotionalDataByFilter(action) {
  try {
    yield put({ type: START_LOADING });
    const data = yield call(getNotionalDataByFilter, action.payload);
    yield put({ type: RESIZE_CANVAS });
    checkForException(data);
    filterdata = data;
    yield put({ type: `${FETCH_NOTIONAL_DATA_BY_ECPE_FILTER}_SUCCESS`, data });
    yield put({ type: `${FETCH_NOTIONAL_DATA_BY_FILTER}_SUCCESS`, data });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_NOTIONAL_DATA_BY_ECPE_FILTER}_FAILURE` });
    yield put({ type: `${FETCH_NOTIONAL_DATA_BY_FILTER}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* fetchNotionalDataByLeEcpeFilter(action) {
  try {
    yield put({ type: START_LOADING });
    yield put({ type: RESIZE_CANVAS });
    checkForException(filterdata);
    yield put({ type: `${FETCH_NOTIONAL_DATA_BY_LE_ECPE_FILTER}_SUCCESS`, filterdata, action });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_NOTIONAL_DATA_BY_LE_ECPE_FILTER}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }

}

function* fetchBottomEmptyGrid(action) {
  try {
    yield put({ type: START_LOADING });
    yield put({ type: `${FETCH_EMPTY_GRID_AT_BOTTOM}_SUCCESS` });
  } catch (e) {
    yield put({ type: HANDLE_EXCEPTION });
    yield put({ type: `${FETCH_EMPTY_GRID_AT_BOTTOM}_FAILURE` });
  } finally {
    yield put({ type: END_LOADING });
  }
}

function* showForOrMessage() {
  yield put({ type: `${ERROR_MSG_SELECT_ONLY_ONE_FIELD}_SUCCESS` });
}

function* aanaReportSaga() {
  yield [
    takeEvery(FETCH_GROSS_NOTIONAL_DATA, fetchGrossNotionalData),
    takeEvery(FETCH_NOTIONAL_DATA_BY_FILTER, fetchNotionalDataByFilter),
    takeEvery(FETCH_NOTIONAL_DATA_BY_ECPE_FILTER, fetchNotionalDataByFilter),
    takeEvery(FETCH_NOTIONAL_DATA_BY_LE_ECPE_FILTER, fetchNotionalDataByLeEcpeFilter),
    takeEvery(FETCH_EMPTY_GRID_AT_BOTTOM, fetchBottomEmptyGrid),
    takeEvery(ERROR_MSG_SELECT_ONLY_ONE_FIELD, showForOrMessage)
  ];
}

export default aanaReportSaga;
