import { combineReducers } from "redux";
import {
  FETCH_GROSS_NOTIONAL_DATA,
  FETCH_NOTIONAL_DATA_BY_ECPE_FILTER,
  FETCH_NOTIONAL_DATA_BY_LE_ECPE_FILTER,
  FETCH_NOTIONAL_DATA_BY_FILTER,
  RESIZE_CANVAS,
  FETCH_EMPTY_GRID_AT_BOTTOM,
  ERROR_MSG_SELECT_ONLY_ONE_FIELD
} from "../../commons/constants";
import _ from "lodash";

function flattenNotionalData(enrichedData) {
  let data = enrichedData.data;

  if (!data) {
    return data;
  }

  const notionalSynopsisDataList = data;
  let flattendata = [];
  for (let i = 0; i < notionalSynopsisDataList.length; i++) {
    let notionalSynopsis = notionalSynopsisDataList[i];
    flattendata.push({
      currencyName: notionalSynopsis.currencyName,
      legalEntityName: notionalSynopsis.legalEntity.name,
      legalEntityId: notionalSynopsis.legalEntity.id,
      gboTypeId: notionalSynopsis.gboTypeId,
      grossNotional: notionalSynopsis.grossNotional,
      grossAverageNotional: notionalSynopsis.grossAverageNotional,
      minimumNotional: notionalSynopsis.minimumNotional,
      maximumNotional: notionalSynopsis.maximumNotional,
      medianNotional: notionalSynopsis.medianNotional,
      id: i.toString()
    });
  }
  flattendata = _.orderBy(flattendata, ['legalEntityName'], ['asc']);
  enrichedData.data = flattendata;
  return enrichedData;
}

function notionalDataReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_GROSS_NOTIONAL_DATA}_SUCCESS`:
      return flattenNotionalData(action.enrichedData) || [];
    case `${ERROR_MSG_SELECT_ONLY_ONE_FIELD}_SUCCESS`:
      return "Wrong Selection";
    default:
      return state;
  }
}

function flattenNotionalDataByFilter(data) {
  if (!data) {
    return data;
  }
  var id = 1;
  var flag = 0;
  let flattendata = [];
  for (var legalEntityId in data) {
    if (data.hasOwnProperty(legalEntityId)) {
      let legalEntitySynopsis = data[legalEntityId];
      for (var ecpeId in legalEntitySynopsis) {
        if (legalEntitySynopsis.hasOwnProperty(ecpeId)) {
          let ecpenotionalSynopsis = legalEntitySynopsis[ecpeId];
          for (var gboTypeId in ecpenotionalSynopsis) {
            if (ecpenotionalSynopsis.hasOwnProperty(gboTypeId)) {
              let notionalSynopsis = ecpenotionalSynopsis[gboTypeId];
              for (var i = 0; i < id; i++) {
                if (flattendata[i]) {
                  var obj = flattendata[i];
                  if (obj.gboTypeId == notionalSynopsis.gboType.id) {
                    accumulateNotionalValues(obj, notionalSynopsis);
                    flag = 1;
                    break;
                  }
                }
              }
              if (!(flag)) {
                flattendata.push(getNotionalDataValues(notionalSynopsis, id, legalEntityId));
              };
              id++; flag = 0;
            }
          }
        }
      }
    }
  }
  flattendata = _.orderBy(flattendata, ['gboTypeName'], ['asc']);
  return flattendata;
}

function flattenNotionalDataByEcpeFilter(data) {
  if (!data) {
    return data;
  }
  var id = 1;
  var flag = 0;
  let flattendata = [];
  for (var legalEntityId in data) {
    if (data.hasOwnProperty(legalEntityId)) {
      let legalEntitySynopsis = data[legalEntityId];
      for (var ecpeId in legalEntitySynopsis) {
        if (legalEntitySynopsis.hasOwnProperty(ecpeId)) {
          let ecpenotionalSynopsis = legalEntitySynopsis[ecpeId];
          for (var gboTypeId in ecpenotionalSynopsis) {
            if (ecpenotionalSynopsis.hasOwnProperty(gboTypeId)) {
              let notionalSynopsis = ecpenotionalSynopsis[gboTypeId];
              for (var i = 0; i < id; i++) {
                if (flattendata[i]) {
                  var obj = flattendata[i];
                  if (obj.ecpeId == notionalSynopsis.ecpeId.id) {
                    accumulateNotionalValues(obj, notionalSynopsis);
                    flag = 1;
                    break;
                  }
                }
              }
              if (!(flag)) {
                flattendata.push(getNotionalDataValues(notionalSynopsis, id, legalEntityId));
              };
              id++; flag = 0;
            }
          }
        }
      }
    }
  }
  flattendata = _.orderBy(flattendata, ['ecpeName'], ['asc']);
  return flattendata;
}

function flattenNotionalDataByLeEcpeFilter(data, ecpe) {
  if (!data) {
    return data;
  }
  var id = 1;
  let flattendata = [];
  for (var legalEntityId in data) {
    if (data.hasOwnProperty(legalEntityId)) {
      let legalEntitySynopsis = data[legalEntityId];
      for (var ecpeId in legalEntitySynopsis) {
        if (legalEntitySynopsis.hasOwnProperty(ecpeId)) {
          let ecpenotionalSynopsis = legalEntitySynopsis[ecpeId];
          for (var gboTypeId in ecpenotionalSynopsis) {
            if (ecpenotionalSynopsis.hasOwnProperty(gboTypeId)) {
              let notionalSynopsis = ecpenotionalSynopsis[gboTypeId];
              if (notionalSynopsis.ecpeId.name === ecpe.ecpeName) {
                flattendata.push(getLeEcpeNotionalDataValues(notionalSynopsis, id, legalEntityId , ecpe));
                }
              id++;
            }
          }
        }
      }
    }
  }
  flattendata = _.orderBy(flattendata, ['gboTypeName'], ['asc']);
  return flattendata;
}

function accumulateNotionalValues(obj, notionalSynopsis) {
  obj.percentageNotionalOnLegalEntity =
    (obj.percentageNotionalOnLegalEntity + notionalSynopsis.percentageNotionalOnLegalEntity);
  obj.grossNotional = (obj.grossNotional + notionalSynopsis.grossNotional);
  obj.grossAverageNotional = (obj.grossAverageNotional + notionalSynopsis.grossAverageNotional);
  obj.minimumNotional = (obj.minimumNotional + notionalSynopsis.minimumNotional);
  obj.maximumNotional = (obj.maximumNotional + notionalSynopsis.maximumNotional);
  obj.medianNotional = (obj.medianNotional + notionalSynopsis.medianNotional);
}

function getNotionalDataValues(notionalSynopsis, id, legalEntityId) {
  const values = {
    currencyName: notionalSynopsis.currencyName,
    legalEntityName: "",
    gboTypeName: notionalSynopsis.gboType.name,
    legalEntityId: legalEntityId,
    gboTypeId: notionalSynopsis.gboType.id,
    ecpeName: notionalSynopsis.ecpeId.name,
    ecpeId: notionalSynopsis.ecpeId.id,
    percentageNotionalOnLegalEntity:
      notionalSynopsis.percentageNotionalOnLegalEntity,
    grossNotional: notionalSynopsis.grossNotional,
    grossAverageNotional: notionalSynopsis.grossAverageNotional,
    minimumNotional: notionalSynopsis.minimumNotional,
    maximumNotional: notionalSynopsis.maximumNotional,
    medianNotional: notionalSynopsis.medianNotional,
    id: id.toString()
  };
  return values;
}

function getLeEcpeNotionalDataValues(notionalSynopsis, id, legalEntityId, ecpe) {
  const values = {
    currencyName: notionalSynopsis.currencyName,
    legalEntityName: "",
    gboTypeName: notionalSynopsis.gboType.name,
    legalEntityId: legalEntityId,
    gboTypeId: notionalSynopsis.gboType.id,
    ecpeName: notionalSynopsis.ecpeId.name,
    ecpeId: notionalSynopsis.ecpeId.id,
    percentageNotionalOnLegalEntity:
      ((notionalSynopsis.grossNotional/(ecpe.grossNotional || 1))*100),
    grossNotional: notionalSynopsis.grossNotional,
    grossAverageNotional: notionalSynopsis.grossAverageNotional,
    minimumNotional: notionalSynopsis.minimumNotional,
    maximumNotional: notionalSynopsis.maximumNotional,
    medianNotional: notionalSynopsis.medianNotional,
    id: id.toString()
  };
  return values;
}

function notionalDataByFilterReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_NOTIONAL_DATA_BY_FILTER}_SUCCESS`:
      return flattenNotionalDataByFilter(action.data) || [];
    case `${FETCH_NOTIONAL_DATA_BY_LE_ECPE_FILTER}_SUCCESS`:
      return flattenNotionalDataByLeEcpeFilter(action.filterdata, action.action.ecpe) || [];
    case `${FETCH_EMPTY_GRID_AT_BOTTOM}_SUCCESS`:
      return [];
    default:
      return state;
  }
}

function notionalDataByEcpeFilterReducer(state = [], action) {
  switch (action.type) {
    case `${FETCH_NOTIONAL_DATA_BY_ECPE_FILTER}_SUCCESS`:
      return flattenNotionalDataByEcpeFilter(action.data) || [];
    case `${FETCH_EMPTY_GRID_AT_BOTTOM}_SUCCESS`:
      return [];
    default:
      return state;
  }
}

function resizeCanvasReducer(state = [], action) {
  switch (action.type) {
    case RESIZE_CANVAS:
      return state == true ? false : true;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  grossNotionalData: notionalDataReducer,
  notionalDataByEcpeFilter: notionalDataByEcpeFilterReducer,
  notionalDataByFilter: notionalDataByFilterReducer,
  resizeCanvas: resizeCanvasReducer
});

export default rootReducer;
