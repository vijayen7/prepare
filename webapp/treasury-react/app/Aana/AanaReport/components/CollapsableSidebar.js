import React from "react";
import ColumnLayout from "commons/components/ColumnLayout";
import FilterButton from "commons/components/FilterButton";
import SaveSettingsManager from "commons/components/SaveSettingsManager";
import Sidebar from "commons/components/Sidebar";
import DateFilter from "commons/container/DateFilter";
import LegalEntityFilter from "commons/container/LegalEntityFilter";
import CurrencyFilter from "commons/container/CurrencyFilter";
import GBOTypeFilter from "commons/container/GBOTypeFilter";
import Panel from "commons/components/Panel";
import PropTypes from "prop-types";
import AanaRuleNameFilter from "commons/container/AanaRuleNameFilter";
import Message from "commons/components/Message";
import { ToggleGroup } from 'arc-react-components';

export const CollapsableSidebar = ({
  resizeCanvas,
  toggleSidebar,
  getCurrentFilterState,
  applySavedFilters,
  onSelect,
  onGboSelect,
  onRuleSelect,
  showToggle1,
  showToggle2,
  onToggleButtonChange,
  isStartDateEndDateINOrder,
  selectedStartDate,
  selectedEndDate,
  selectedLegalEntities,
  selectedCurrency,
  selectedGboTypes,
  selectedRule,
  handleSearch,
  handleReset
}) => {
  return (
    <Sidebar
      size="400px"
      resizeCanvas={resizeCanvas}
      toggleSidebar={toggleSidebar}
      header="Aana Reporting Tool"
    >
      {isStartDateEndDateINOrder === false && (
        <Message
          error
          messageData="Start Date cannot be greater than End Date"
        />
      )}
      <SaveSettingsManager
        selectedFilters={getCurrentFilterState()}
        applySavedFilters={applySavedFilters}
        applicationName="AanaFilter"
      />
      <DateFilter
        onSelect={onSelect}
        stateKey="selectedStartDate"
        data={selectedStartDate}
        label="Start Date"
      />
      <DateFilter
        onSelect={onSelect}
        stateKey="selectedEndDate"
        data={selectedEndDate}
        label="End Date"
      />
      <Panel>
        <LegalEntityFilter
          onSelect={onSelect}
          selectedData={selectedLegalEntities}
          multiSelect={true}
        />
      </Panel>
      <Panel>
        Search By:
        <br/>
      <ToggleGroup
          value={"value"}
          onChange={(value) => {
            onToggleButtonChange(value);
          }}
          >
          <ToggleGroup.Button key="1" data = "Toggle 1">
            GBO Types
          </ToggleGroup.Button>
          <ToggleGroup.Button key="2" data = "Toggle 2">
            Rule Name
          </ToggleGroup.Button>
          </ToggleGroup>
          {showToggle1 && <GBOTypeFilter onSelect={onGboSelect} selectedData={selectedGboTypes} />}
            <br/>
        {showToggle2 && <AanaRuleNameFilter
          onSelect={onRuleSelect}
          selectedData={selectedRule}
          multiSelect={false}
          singleSelect={true}
          selectedRuleConfigType={{ key: 1, value: "AANA Rules" }}
        />
        }
      </Panel>
      <Panel>
        <CurrencyFilter
          onSelect={onSelect}
          selectedData={selectedCurrency}
          multiSelect={false}
          singleSelect
          readonly={true}
          label="Currency"
        />
      </Panel>

      <ColumnLayout>
        <FilterButton onClick={handleSearch} reset={false} label="Search" />
        <FilterButton reset onClick={handleReset} label="Reset" />
      </ColumnLayout>
    </Sidebar>
  );
};

CollapsableSidebar.propTypes = {
  resizeCanvas: PropTypes.any,
  toggleSidebar: PropTypes.bool.isRequired,
  getCurrentFilterState: PropTypes.func.isRequired,
  applySavedFilters: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  onGboSelect: PropTypes.func.isRequired,
  onRuleSelect: PropTypes.func.isRequired,
  showToggle1: PropTypes.any,
  showToggle2: PropTypes.any,
  onToggleButtonChange: PropTypes.func.isRequired,
  selectedStartDate: PropTypes.any,
  selectedEndDate: PropTypes.any,
  selectedLegalEntities: PropTypes.array.isRequired,
  selectedRule: PropTypes.any,
  selectedCurrency: PropTypes.any,
  selectedGBOTypes: PropTypes.array.isRequired,
  handleSearch: PropTypes.func.isRequired,
  handleReset: PropTypes.func.isRequired
};
