import React from "react";
import { Layout } from "arc-react-components";
import { Panel } from 'arc-react-components';
import { PropTypes } from "prop-types";
import Message from "commons/components/Message";
import ArcDataGrid from "arc-data-grid";

export const gridConfig = {
  clickableRows: true
};

export const AanaReportGrid = ({
  gridGrossNotionalColumns,
  getGrossNotionalGridOptions,
  gridNotionalByEcpeFilterColumns,
  getNotionalByEcpeFilterGridOptions,
  gridNotionalByFilterColumns,
  getNotionalByFilterGridOptions,
  onRowClickHandler,
  onEcpeRowClickHandler,
  legalEntity,
  ecpe,
  grossNotionalData,
  resizeCanvas,
  notionalDataByFilter,
  notionalDataByEcpeFilter
}) => {
  return (
    <React.Fragment>
    <Layout className="border">
      <Layout.Child childId="aanaReportGridChild1" size={4} title="Legal Entity View" collapsible>
        <Panel title = {"Min/Median/Max Notionals are computed for each level, and therefore not " +
            "intended to tie to the aggregates in the different panels."}>
            <ArcDataGrid
              rows={grossNotionalData.data}
              columns={gridGrossNotionalColumns()}
              onClickedRowChange={onRowClickHandler}
              configurations={gridConfig}
              getRowId={(row, _) => row.id}
            />
        </Panel>
      </Layout.Child>
      <Layout.Divider key="child2" isResizable />
      <Layout.Child childId="aanaReportGridChild2" size = {3} collapsible title="Exposure Counterparty View">
          {notionalDataByEcpeFilter.length == 0 && (
             <Panel title="Exposure Counterparty Details" >
                <Message messageData="Select a Legal Entity to view the details." />
             </Panel>)}
          {notionalDataByEcpeFilter.length !== 0 && (

            <Panel title={"Exposure Counterparty Details for Legal Entity: " + legalEntity}>

              <ArcDataGrid
                rows={notionalDataByEcpeFilter}
                columns={gridNotionalByEcpeFilterColumns()}
                onClickedRowChange={onEcpeRowClickHandler}
                configurations={gridConfig}
                getRowId={(row, _) => row.id}
              />

            </Panel>)}
      </Layout.Child>
      <Layout.Divider key="child3" isResizable />

      <Layout.Child childId="aanaReportGridChild3" size = {3} collapsible title="Asset Class View">
          {notionalDataByFilter.length == 0 && (
             <Panel title="Asset Class Details" >
                <Message messageData="Select a Legal Entity or Exposure Counterparty to view the details." />
             </Panel>)}

          {notionalDataByFilter.length !== 0 &&
             <Panel title = {(ecpe === "") ?
                "Asset Class Details for Legal Entity: " + legalEntity :
                "Asset Class Details for Legal Entity: " + legalEntity + " and Exposure Counterparty: "
                + ecpe} >

             <ArcDataGrid
                rows={notionalDataByFilter}
                columns={gridNotionalByFilterColumns()}
                getRowId={(row, _) => row.id}
                configurations={gridConfig}
             />
            </Panel>}
      </Layout.Child>

    </Layout>
    </React.Fragment>
  );
};

AanaReportGrid.propTypes = {
  gridGrossNotionalColumns: PropTypes.func.isRequired,
  getGrossNotionalGridOptions: PropTypes.func.isRequired,
  gridNotionalByFilterColumns: PropTypes.func.isRequired,
  getNotionalByFilterGridOptions: PropTypes.func.isRequired,
  gridNotionalByEcpeFilterColumns: PropTypes.func.isRequired,
  getNotionalByEcpeFilterGridOptions: PropTypes.func.isRequired,
  onRowClickHandler: PropTypes.func.isRequired,
  onEcpeRowClickHandler: PropTypes.func.isRequired,
  legalEntity: PropTypes.any,
  ecpe: PropTypes.any,
  grossNotionalData: PropTypes.any,
  resizeCanvas: PropTypes.any,
  notionalDataByFilter: PropTypes.any,
  notionalDataByEcpeFilter: PropTypes.any
};
