export function getGridOptions(onSelectedRowsChanged) {
  return {
    autoHorizontalScrollBar: true,
    nestedTable: false,
    highlightRowOnClick: true,
    editable: true,
    autoEdit: false,
    isFilterOnFormattedValue: true,
    applyFilteringOnGrid: true,
    enableMultilevelGrouping: true,
    checkboxHeader: {
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    },
    onSelectedRowsChanged: onSelectedRowsChanged,
    frozenColumn: 1
  };
}
