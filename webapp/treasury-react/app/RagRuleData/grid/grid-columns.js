import { decamelize } from "../../commons/util";

const columnIds = [
  "ruleId",
  "legalEntity",
  "legalEntityFamily",
  "cpe",
  "agreementType",
  "accountType",
  "custodianAccount",
  "custodianAccountCurrency",
  "absoluteExposureInternalBrokerDiff",
  "absoluteExposureInternalPrevdayDiff",
  "percentExposureInternalBrokerDiff",
  "percentExposureInternalPrevdayDiff",
  "absoluteCollateralInternalBrokerDiff",
  "absoluteCollateralInternalPrevdayDiff",
  "percentCollateralInternalBrokerDiff",
  "percentCollateralInternalPrevdayDiff",
  "absoluteRequirementInternalBrokerDiff",
  "absoluteRequirementInternalPrevdayDiff",
  "percentRequirementInternalBrokerDiff",
  "percentRequirementInternalPrevdayDiff",
  "internalEdRange",
  "brokerEdRange",
  "edDirection",
  "absoluteEdDiffBrokerData",
  "percentEdDiffBrokerData",
  "brokerEdDirection",
  "ragCode"
];

const columns = [];

columnIds.forEach(col => {
  const columnConfig = {
    id: col,
    name: decamelize(col, " "),
    field: col,
    toolTip: col,
    type: "text",
    autoWidth: true
  };
  columns.push(columnConfig);
});

const ragStatusColorColumn = {
  id: "ragCategoryCode",
  name: "",
  field: "ragCategoryCode",
  toolTip: "ragCategoryCode",
  type: "text",
  formatter: ragStatusIconFormatter,
  width: 25
};
columns.push(ragStatusColorColumn);

export const getColumnConfig = () => {
  return columns;
};

export function ragStatusIconFormatter(
  row,
  cell,
  value,
  columnDef,
  dataContext
) {
  let ragCategoryCode;

  if (dataContext.ragCategoryCode === "CRITICAL") {
    ragCategoryCode =
      '<div class="highlight-row--critical" style="height:100%;background-color:#8E4C4C"></div>';
  } else if (dataContext.ragCategoryCode === "WARNING") {
    ragCategoryCode =
      '<div class="highlight-row--warning" style="height:100%;background-color:#B79556"></div>';
  } else if (dataContext.ragCategoryCode === "CLEAR") {
    ragCategoryCode =
      '<div class="highlight-row--clear" style="height:100%;background-color:#5F876D"></div>';
  } else if (dataContext.ragCategoryCode === "BROKER_DATA_MISSING") {
    ragCategoryCode =
      '<div class="highlight-row--missing" style="height:100%;background-color:#2189b5"></div>';
  }
  return ragCategoryCode;
}
