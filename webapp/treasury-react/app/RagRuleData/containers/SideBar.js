import React, { Component } from 'react';
import Sidebar from 'commons/components/Sidebar';
import LegalEntityFilter from '../../commons/container/LegalEntityFilter';
import CpeFilter from '../../commons/container/CpeFilter';
import AgreementTypeFilter from '../../commons/container/AgreementTypeFilter';
import CurrencyFilter from '../../commons/container/CurrencyFilter';
import LegalEntityFamilyFilter from '../../commons/container/LegalEntityFamilyFilter';
import CustodianAccountFilter from '../../commons/container/CustodianAccountFilter';
import FilterButton from '../../commons/components/FilterButton';
import ColumnLayout from '../../commons/components/ColumnLayout';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import RagRuleDataStore from '../RagRuleDataStore';
import SaveSettingsManager from '../../commons/components/SaveSettingsManager';

@inject('ragRuleDataStore')
@observer
class SideBar extends Component {
  static propTypes = {
    ragRuleDataStore: PropTypes.instanceOf(RagRuleDataStore).isRequired
  };

  handleReset = () => {
    this.props.ragRuleDataStore.resetFilters();
    this.resizeCanvas();
  };

  onSelect = (params) => {
    this.props.ragRuleDataStore.onSelect(params);
    this.resizeCanvas();
  };

  handleSearch = () => {
    this.props.ragRuleDataStore.loadRuleData();
  };

  handleCopySearch = () => {
      this.props.ragRuleDataStore.copySearchUrl(window.location.origin);
  };

  resizeCanvas = () => {
    this.props.ragRuleDataStore.resizeCanvas();
  };

  applySavedFilters = (savedFilters) => {
    this.props.ragRuleDataStore.setFilters(savedFilters);
  };

  render() {
    return (
      <Sidebar collapsible resizeCanvas={this.resizeCanvas}>
        <SaveSettingsManager
          selectedFilters={this.props.ragRuleDataStore.filters}
          applySavedFilters={this.applySavedFilters}
          applicationName="ragRuleDataFilter"
        />
        <LegalEntityFilter
          multiSelect
          onSelect={this.onSelect}
          selectedData={this.props.ragRuleDataStore.filters.selectedLegalEntities}
        />
        <LegalEntityFamilyFilter
          multiSelect
          onSelect={this.onSelect}
          selectedData={this.props.ragRuleDataStore.filters.selectedLegalEntityFamilies}
        />
        <CpeFilter
          multiSelect
          onSelect={this.onSelect}
          selectedData={this.props.ragRuleDataStore.filters.selectedCpes}
        />
        <AgreementTypeFilter
          multiSelect
          onSelect={this.onSelect}
          selectedData={this.props.ragRuleDataStore.filters.selectedAgreementTypes}
        />
        <CustodianAccountFilter
          multiSelect
          onSelect={this.onSelect}
          selectedData={this.props.ragRuleDataStore.filters.selectedCustodianAccounts}
        />
        <CurrencyFilter
          multiSelect
          label="Custodian Account Currencies"
          onSelect={this.onSelect}
          selectedData={this.props.ragRuleDataStore.filters.selectedCurrencies}
        />
        <br />
        <ColumnLayout>
          <FilterButton onClick={this.handleSearch} label="Search" />
          <FilterButton className="button--tertiary " reset={true} onClick={this.handleReset} label="Reset" />
          <FilterButton className="button--tertiary margin--horizontal--double" onClick={this.handleCopySearch} label="Copy Search URL" />
        </ColumnLayout>
      </Sidebar>
    );
  }
}

export default SideBar;
