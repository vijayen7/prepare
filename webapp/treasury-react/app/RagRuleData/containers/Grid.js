import React, { Component } from 'react';
import { ReactArcGrid } from 'arc-grid';
import { Layout } from 'arc-react-components';
import Message from '../../commons/components/Message';
import { getColumnConfig } from '../grid/grid-columns';
import { getGridOptions } from '../grid/grid-options';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';
import RagRuleDataStore from '../RagRuleDataStore';
import { Dialog } from "arc-react-components";
import InputFilter from "../../commons/components/InputFilter";

@inject('ragRuleDataStore')
@observer
class Grid extends Component {
  static propTypes = {
    ragRuleDataStore: PropTypes.instanceOf(RagRuleDataStore).isRequired
  };

  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.onDeleteRuleClick = this.onDeleteRuleClick.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  data = [];

  loadGridData() {
    let data = [];
    data = this.props.ragRuleDataStore.ragRuleDataList;
    let deleteRulesStatus = this.props.ragRuleDataStore.ruleDataStatus.deleteRulesStatus

    if (
      this.props.ragRuleDataStore.ruleDataStatus.errorOccured ||
      data == null ||
      data.length == null
    ) {
      return errorMessage();
    } else if (this.props.ragRuleDataStore.ruleDataStatus.noDataFound) {
      return noDataFound();
    } else if (data.length === 0) {
      return initialMessage();
    } else if (deleteRulesStatus && deleteRulesStatus.length != 0) {
      this.data = [];
      return deleteRuleMessage(deleteRulesStatus);
    }
    else {
      this.data = data;
    }
  }

  onSelectedRowsChanged = (selectedRowIds, selectedRowItems) => {
    if (selectedRowIds !== null && selectedRowIds.length > 0) {
      this.props.ragRuleDataStore.filters.toBeDeletedRuleIds = selectedRowIds;
    } else {
      this.props.ragRuleDataStore.filters.toBeDeletedRuleIds = [];
    }
  };

  renderGridData() {
    let grid = null;

    grid = (
      <ReactArcGrid
        data={this.data}
        gridId="ragRulesUI"
        columns={getColumnConfig()}
        options={getGridOptions(this.onSelectedRowsChanged)}
        resizeCanvas={this.props.ragRuleDataStore.resizeCanvasFlag}
      />
    );
    return grid;
  }

  onSelect(params) {
    const { value } = params;
    this.props.ragRuleDataStore.setDeleteRulesComment(value);
  }

  onDeleteRuleClick() {
    this.props.ragRuleDataStore.setIsDeleteDialogOpen(true);
  }

  handleCloseModal() {
    this.props.ragRuleDataStore.setIsDeleteDialogOpen(false);
  }

  handleDelete() {
    this.props.ragRuleDataStore.setIsDeleteDialogOpen(false);
    this.props.ragRuleDataStore.deleteRagRules();
  }


  render() {
    const gridMessage = this.loadGridData();
    let isDeleteDialogOpen = this.props.ragRuleDataStore.isDeleteDialogOpen;
    let isDeleteButtonDisabled = true;
    if (this.props.ragRuleDataStore.filters.toBeDeletedRuleIds.length > 0) {
      isDeleteButtonDisabled = false;
    }
    if (this.data.length === 0) {
      return gridMessage;
    } else {
      return (
        <React.Fragment>
          <Layout>
            <Layout.Child childId="child1">
              <button
                onClick={this.onDeleteRuleClick}
                disabled={isDeleteButtonDisabled}
              >Delete Selected Rules</button>
              <Dialog
                isOpen={isDeleteDialogOpen}
                title="Delete Rules"
                onClose={this.handleCloseModal}
                footer={
                  <React.Fragment>
                    <button onClick={this.handleDelete}>Delete</button>
                    <button onClick={this.handleCloseModal}>Cancel</button>
                  </React.Fragment>
                }
              >
                <InputFilter
                  data={this.deleteComment}
                  onSelect={this.onSelect}
                  stateKey="comment"
                  label="Comment"
                />
              </Dialog>
            </Layout.Child>
            <Layout.Child childId="child2">{this.renderGridData()}</Layout.Child>
          </Layout>
        </React.Fragment>
      );
    }
  }
}

const initialMessage = () => {
  return (
    <div>
      <Message messageData="Search to load data" />
    </div>
  );
};

const noDataFound = () => {
  return (
    <div>
      <Message messageData="No Data Found" />
    </div>
  );
};

const errorMessage = () => {
  return (
    <div>
      <Message messageData="Failed to load data" />
    </div>
  );
};

const deleteRuleMessage = (status) => {
  return (
    <div>
      <Dialog
        isOpen={true}
        title="Delete Rules Status"
      >
        {status}
      </Dialog>
    </div>
  );
};

export default Grid;
