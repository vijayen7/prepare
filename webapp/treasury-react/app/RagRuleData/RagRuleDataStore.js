import { action, flow, observable } from 'mobx';
import * as RagRuleDataAPI from './api';
import { getSelectedIdList, getCommaSeparatedKeysFromFilterList } from '../commons/util';
import { invalidateRagRules } from './api';

const copy = require('clipboard-copy')

export const INITIAL_FILTERS = {
  selectedLegalEntities: [],
  selectedLegalEntityFamilies: [],
  selectedCpes: [],
  selectedAgreementTypes: [],
  selectedCustodianAccounts: [],
  selectedCurrencies: [],
  toBeDeletedRuleIds: []
};

const DATA_STATUS = {
  searchInProgress: false,
  errorOccured: false,
  noDataFound: false,
  deleteRulesStatus: ''
};

const INITIAL_RULE_DATA = [];

export default class RagRuleDataStore {
  @observable
  ragRuleDataList = INITIAL_RULE_DATA;

  @observable
  filters = INITIAL_FILTERS;

  @observable
  ruleDataStatus = DATA_STATUS;

  @observable
  resizeCanvasFlag = false;

  @observable
  isDeleteDialogOpen = false;

  @observable
  deleteComment = "";

  @action
  resetFilters() {
    this.filters = INITIAL_FILTERS;
    this.ruleDataStatus = DATA_STATUS;
  }

  @action
  setFilters(newFilters) {
    this.filters = newFilters;
  }

  @action.bound
  resizeCanvas() {
    setTimeout(() => {
      this.resizeCanvasFlag = !this.resizeCanvasFlag;
    }, 0);
  }

  onSelect = (params) => {
      const { key, value } = params;
      const selectedFilters = { ...this.filters };
      selectedFilters[key] = value;
      this.setFilters(selectedFilters);
  }

  copySearchUrl(location) {
      let url = location + `/treasury/comet/ragrules.html?
      legalEntityIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedLegalEntities)}
      &legalEntityFamilyIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedLegalEntityFamilies)}
      &cpeIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedCpes)}
      &agreementTypeIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedAgreementTypes)}
      &custodianAccountIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedCustodianAccounts)}
      &custodianAccountCurrenciesIds=${getCommaSeparatedKeysFromFilterList(this.filters.selectedCurrencies)}`;
      copy(url);
  }

  @action
  updateDeleteRuleStatus(status) {
    this.ruleDataStatus.deleteRulesStatus = status;
  }

  @action.bound
  setIsDeleteDialogOpen(value) {
    this.isDeleteDialogOpen = value;
  }

  @action.bound
  setDeleteRulesComment(value) {
    this.deleteComment = value;
  }



  deleteRagRules = flow(
    function* deleteRagRules() {
      try {
        let apiResponse = yield invalidateRagRules(this.filters.toBeDeletedRuleIds, this.deleteComment);
        if (apiResponse.successStatus === true) {
          this.updateDeleteRuleStatus('Rules Deleted Successfully');
          this.loadRuleData(this.filters);
        } else {
          this.updateDeleteRuleStatus('Unable to Delete Rules');
        }
      } catch (e) {
        this.updateDeleteRuleStatus('Error occurred while deleting rules');
      }
    }.bind(this)
  );

  loadRuleData = flow(
    function* loadRuleData() {

      loadSideBarFilters(this.filters);
      this.ruleDataStatus.searchInProgress = true;

      try {
        let ragRuleDataFilter = getRagRuleDataFilter(this.filters);
        ragRuleDataFilter['@CLASS'] = 'deshaw.treasury.common.model.comet.LcmPositionDataParam';

        const ragRuleDataList = yield RagRuleDataAPI.getRagRuleDataList(ragRuleDataFilter);

        if (ragRuleDataList == null || ragRuleDataList.length == null) {
          this.ruleDataStatus.errorOccured = true;
        }
        ragRuleDataList.forEach((item) => (item.id = item.ruleId));
        this.ragRuleDataList = ragRuleDataList;

        this.ruleDataStatus = {
          searchInProgress: false,
          errorOccured: false,
          noDataFound: ragRuleDataList.length === 0 ? true : false
        };
      } catch (e) {
        this.ruleDataStatus = {
          searchInProgress: false,
          errorOccured: true
        };
      }
    }.bind(this)
  );
}

function loadSideBarFilters(filters) {
    if ((!filters.selectedLegalEntities || !filters.selectedLegalEntities.length) &&
        (!filters.selectedLegalEntityFamilies || !filters.selectedLegalEntityFamilies.length) &&
        (!filters.selectedCpes || !filters.selectedCpes.length) &&
        (!filters.selectedAgreementTypes || !filters.selectedAgreementTypes.length) &&
        (!filters.selectedCurrencies || !filters.selectedCurrencies.length) &&
        (!filters.selectedCustodianAccounts || !filters.selectedCustodianAccounts.length)) {
          filters.selectedLegalEntities = [{ key: "-1", value:"All [-1]"}];
       }
}

const getRagRuleDataFilter = (filters) => {
  return {
    legalEntityIds: getSelectedIdList(filters.selectedLegalEntities),
    legalEntityFamilyIds: getSelectedIdList(filters.selectedLegalEntityFamilies),
    cpeIds: getSelectedIdList(filters.selectedCpes),
    agreementTypeIds: getSelectedIdList(filters.selectedAgreementTypes),
    custodianAccountIds: getSelectedIdList(filters.selectedCustodianAccounts),
    custodianAccountCurrencyIds: getSelectedIdList(filters.selectedCurrencies)
  };
};
