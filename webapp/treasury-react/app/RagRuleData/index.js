import React, { Component } from "react";
import RagRuleApp from "./RagRuleApp";

export default class RagRuleData extends Component {
  render() {
    return (
      <div>
        <RagRuleApp
        location={this.props.location}
        />
      </div>
    );
  }
}
