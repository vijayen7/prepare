import { ArcFetch } from "arc-commons";
import { BASE_URL } from "commons/constants";
import { fetchPostURL } from "../commons/util";

const QUERY_ARGS = {
  credentials: "include",
  method: "GET"
};

export const getRagRuleDataList = ragRuleDataFilter => {
  const url = `${BASE_URL}service/ragRuleService/getRagRules?ragRuleDataFilter=${encodeURIComponent(
    JSON.stringify(ragRuleDataFilter)
  )}&inputFormat=JSON&format=JSON`;

  return ArcFetch.getJSON(url, QUERY_ARGS);
};

export const invalidateRagRules = (input, comment) => {
  const url = `${BASE_URL}service/ragRuleService/invalidateRagRules`;
  let param = encodeURIComponent(
    JSON.stringify(input));
   comment = encodeURIComponent(
    JSON.stringify(comment));
  let encodedParam = 'ruleIds=' + param + '&comment=' + comment + '&inputFormat=JSON&format=JSON';
  return fetchPostURL(url, encodedParam);
};

