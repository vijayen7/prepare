import React, { Component } from "react";
import SideBar from "./containers/SideBar";
import Grid from "./containers/Grid";
import { inject, observer } from "mobx-react";
import { ReactLoader } from "../commons/components/ReactLoader";
import queryString from "query-string";
import { getFilterListFromCommaSeparatedString } from "../commons/util";

@inject("ragRuleDataStore")
@observer
class RagRuleApp extends Component {

   constructor(...args) {
        super(...args);
        this.childKey = 0;
   }

  componentDidMount() {
    const values = queryString.parse(this.props.location.search);
    let filters = {
      selectedLegalEntities: getFilterListFromCommaSeparatedString(values.legalEntityIds),
      selectedLegalEntityFamilies: getFilterListFromCommaSeparatedString(values.legalEntityFamilyIds),
      selectedCpes: getFilterListFromCommaSeparatedString(values.cpeIds),
      selectedAgreementTypes: getFilterListFromCommaSeparatedString(values.agreementTypeIds),
      selectedCustodianAccounts: getFilterListFromCommaSeparatedString(values.custodianAccountIds),
      selectedCurrencies: getFilterListFromCommaSeparatedString(values.custodianAccountCurrenciesIds),
      toBeDeletedRuleIds: [],
    };
    if ((filters.selectedLegalEntities && filters.selectedLegalEntities.length) ||
      (filters.selectedLegalEntityFamilies && filters.selectedLegalEntityFamilies.length) ||
      (filters.selectedCpes && filters.selectedCpes.length) ||
      (filters.selectedAgreementTypes && filters.selectedAgreementTypes.length) ||
      (filters.selectedCurrencies && filters.selectedCurrencies.length) ||
      (filters.selectedCustodianAccounts && filters.selectedCustodianAccounts.length)) {
        this.props.ragRuleDataStore.setFilters(filters);
        this.props.ragRuleDataStore.loadRuleData();
    }
  }

  render() {
    this.childKey++;
    return (
      <React.Fragment>
        <ReactLoader
          inProgress={
            this.props.ragRuleDataStore.ruleDataStatus.searchInProgress
          }
        />
        <div className="layout--flex--row">
          <arc-header
            ref={header => {
              this.header = header;
            }}
            className="size--content"
            user={USER}
            modern-themes-enabled
          />
          <div className="layout--flex--row">
            <div className="layout--flex padding--top">
              <div className="size--content padding--right">
                <SideBar location={this.props.location} />
              </div>
              <Grid key = {this.childKey}/>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default RagRuleApp;
