"use strict";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.calculatorGroupAction = {
    loadResults: _loadResults,
    loadSimulationResults: _loadSimulationResults,
    saveSelectedRules: _saveSelectedRules,
  };

  var _simulatedRuleGrid;

  function _saveSelectedRules() {
    treasury.interest.common.actions.saveSelectedRules("/treasury/financing/update-simulated-rules",
      _simulatedRuleGrid,
      function() {
        document.getElementById('calculatorGroupDialog').visible = false;
      });
  }


  function _loadSimulationResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/simulate-rules",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    treasury.interest.common.actions.loadSimulationGrids(serviceCall, "existingCalculatorGroupRuleGrid",
      "simulatedCalculatorGroupRuleGrid", "calculatorGroupExistingRuleLabel",
      "calculatorGroupSimulatedRuleLabel", treasury.interest.calculatorGroupColumnsConfig
      .getSimulatedColumns, window.treasury.interest.calculatorGroupGridOptions.getGridOptions,
      window.treasury.interest.calculatorGroupGridOptions.getSimulatedGridOptions,
      function() {
        document.getElementById("saveCalculatorGroupRules").style.visibility = "visible";
      },
      function(gridVar) {
        _simulatedRuleGrid = gridVar;
      })
  }

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/calculator-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.interest.common.actions.loadSearchGrid(serviceCall, "calculatorGroupGrid",
      treasury.interest.calculatorGroupColumnsConfig.getColumns, window.treasury.interest.calculatorGroupGridOptions.getGridOptions);
  }
})();
