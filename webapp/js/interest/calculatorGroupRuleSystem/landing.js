"use strict";
var simulationFilterGroup;
(function() {

	var myOnClickEvent = {
  		ajax: true
	}

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.calculatorGroup = {
    loadSearchResults: _loadSearchResults,
    loadAddDialog: _loadAddDialog,
    registerHandlers: _registerHandlers,
    pageDestructor: _pageDestructor,
    editDialog: _editDialog,
  };

  function _registerHandlers() {
    document.getElementById("simulateCalculatorGroupRule").onclick = _loadSimulationResult;
    document.getElementById("saveCalculatorGroupRules").onclick = _saveSelectedRules;
      document.getElementById("agreementTypeFilterAdd").onchange = _loadAgreementTypeSpecificNettingGroupAndCalculatorGroupFilter;
      document.getElementById("nettingGroupFilterAdd").onchange = _loadNettingGroupeSpecificCalculatorGroupFilter;
    document.getElementById('calculatorListSidebar').addEventListener("stateChange", _resizeGridCanvas);
  }

  function _pageDestructor() {
  }

  function _saveSelectedRules() {
    window.treasury.interest.calculatorGroupAction.saveSelectedRules();
  }

  function _loadAgreementTypeSpecificNettingGroupAndCalculatorGroupFilter() {
      if(myOnClickEvent.ajax == false){ return;}
	  var calculatorGroupFilter = new window.treasury.filter.SingleSelect("calculatorGroupFilterIds", "calculatorGroupFilterAdd",[], []);
	  simulationFilterGroup.singleSelectFilterArray[5] = calculatorGroupFilter;

	  _loadAgreementTypeSpecificNettingGroupFilter();
	  if(document.getElementById('agreementTypeFilterAdd').value != null &&
		document.getElementById('agreementTypeFilterAdd').value.key != 20)
		  _loadAgreementTypeSpecificCalculatorGroupFilter();
  }

  function _loadAgreementTypeSpecificNettingGroupFilter() {
    document.getElementById('nettingGroupFilterAdd').value = null;
    if (document.getElementById('agreementTypeFilterAdd').value != null) {
      showLoading("mainLoader");
        var parameters = simulationFilterGroup.getSerializedParameterMap();
      $.when(treasury.loadfilter.loadAgreementTypeSpecificNettingGroupFilter(parameters))
        .done(
          function(nettingGroupsData) {

            var nettingGroupFilter = new window.treasury.filter.SingleSelect(
              "nettingGroupFilterIds", "nettingGroupFilterAdd",
              nettingGroupsData.nettingGroups, []);

            simulationFilterGroup.singleSelectFilterArray[4] = nettingGroupFilter;
            hideLoading("mainLoader");
          }
        );
    }
  }

  function _loadNettingGroupeSpecificCalculatorGroupFilter() {
  	if(myOnClickEvent.ajax == false){ return;}
	  if(document.getElementById('agreementTypeFilterAdd').value != null &&
			  document.getElementById('agreementTypeFilterAdd').value.key == 20 &&
			  		document.getElementById('nettingGroupFilterAdd').value != null)
		  _loadAgreementTypeSpecificCalculatorGroupFilter();
  }

  function _loadAgreementTypeSpecificCalculatorGroupFilter(selectedCalculatorGroupKey) {

	  showLoading("mainLoader");
	  var parameters = simulationFilterGroup.getSerializedParameterMap();
	  if(document.getElementById('nettingGroupFilterAdd').value != null)
		  parameters.nettingGroupFilterIds = document.getElementById('nettingGroupFilterAdd').value.key;

	  $.when(treasury.loadfilter.interestCalculatorGroups(parameters))
	        .done(function(calculatorGroupsData) {
	            var calculatorGroupFilter = new window.treasury.filter.SingleSelect(
	              "calculatorGroupFilterIds", "calculatorGroupFilterAdd",
	              calculatorGroupsData.calculatorGroups, []);

	            simulationFilterGroup.singleSelectFilterArray[5] = calculatorGroupFilter;
	            simulationFilterGroup.singleSelectFilterArray[5].setSelectedNode(selectedCalculatorGroupKey);
	            hideLoading("mainLoader");
	          }
	        );
	}

  function _loadSimulationResult() {
    var parameters = simulationFilterGroup.getSerializedParameterMap();
    if(! _validateParameters(parameters))
    {
      return;
    }
    var formattedDate = getFormattedDate('datePickerAdd');
    parameters.dateString = formattedDate;
    treasury.interest.calculatorGroupAction.loadSimulationResults(parameters);
  }

  function _validateParameters(params)
  {
    if(params['agreementTypeFilterIds'] == -1 && params['nettingGroupFilterIds'] == -1 )
    {
      $("#agreementTypeFilterAdd").addClass("error");
      return false;
    } else {
    	$("#agreementTypeFilterAdd").removeClass();
    }
    if(params['agreementTypeFilterIds'] == 20 && params['nettingGroupFilterIds'] == -1 )
    {
      $("#nettingGroupFilterAdd").addClass("error");
      return false;
    } else {
    	$("#nettingGroupFilterAdd").removeClass();
    }
    if(params['calculatorGroupFilterId'] == -1)
    {
      $("#calculatorGroupFilterAdd").addClass("error");
      return false;
    } else {
    	$("#calculatorGroupFilterAdd").removeClass();
    }
    if(document.getElementById("datePickerErrorAdd").innerHTML != "")
    {
      $("#datePickerAdd").addClass("error");
      return false;
    } else {
      $("#datePickerAdd").removeClass("error");
    }
    return true;
  }

  function _loadSearchResults() {
    if (document.getElementById("datePickerError").innerHTML != "") {
      $("#datePickerAdd").addClass("error");
      return;
    } else {
      $("#datePickerAdd").removeClass("error");
    }

    document.getElementById('calculatorList').innerHTML = '';
    document.getElementById('calculatorListSidebar').setAttribute("hidden", true);
    if (document.getElementById('calculatorListSidebar').state == 'expanded') {
      document.getElementById('calculatorListSidebar').toggle();
    }

    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    parameters.dateString = formattedDate;

    treasury.interest.calculatorGroupAction.loadResults(parameters);
  }

  function _loadAddDialog() {
    _clearAddDialogGrids();
    document.getElementById('calculatorGroupDialog').visible = true;
    _loadAddDialogFilters();
  }

  function _editDialog(row) {
	  _clearAddDialogGrids();
	  document.getElementById('calculatorGroupDialog').visible = true;
	  _loadAddDialogFilters(_editRuleFields.bind(null, searchGrid.data[row]));
  }

  function _editRuleFields(param){		//funding type is not defined even if AT is present.
    myOnClickEvent.ajax = false;

    simulationFilterGroup.singleSelectFilterArray[0].setSelectedNode(param.shaw_entity_id);
    simulationFilterGroup.singleSelectFilterArray[1].setSelectedNode(param.cpe_id);
    simulationFilterGroup.singleSelectFilterArray[2].setSelectedNode(param.funding_type_id);
    simulationFilterGroup.singleSelectFilterArray[3].setSelectedNode(param.ccy_spn);
    simulationFilterGroup.singleSelectFilterArray[4].setSelectedNode(param.netting_group_id);
    simulationFilterGroup.singleSelectFilterArray[5].setSelectedNode(param.calculator_group);
    _loadAgreementTypeSpecificCalculatorGroupFilter(param.calculator_group);

		setFilterDateToMonthStart("datePickerAdd", "datePickerErrorAdd");

    $('#nettingGroupFilterAdd').attr("readOnly","true");
    $('#cpeEntityFilterAdd').attr("readOnly","true");
    $('#agreementTypeFilterAdd').attr("readOnly","true");
    $('#currencyFilterAdd').attr("readOnly","true");
    $('#legalEntityFilterAdd').attr("readOnly","true");

	myOnClickEvent.ajax = true;
  }

  function _clearAddDialogGrids() {
    document.getElementById("saveCalculatorGroupRules").style.visibility = "hidden";
    document.getElementById('calculatorGroupExistingRuleLabel').style.visibility = "hidden";
    document.getElementById('calculatorGroupSimulatedRuleLabel').style.visibility = "hidden";
    window.treasury.common.grid.clearGrid("existingCalculatorGroupRuleGrid");
    window.treasury.common.grid.clearGrid("simulatedCalculatorGroupRuleGrid");
    document.getElementById("datePickerErrorAdd").innerHTML = "";
    $("#datePickerAdd").removeClass("error");

    $('#nettingGroupFilterAdd').removeAttr("readOnly");
    $('#cpeEntityFilterAdd').removeAttr("readOnly");
    $('#agreementTypeFilterAdd').removeAttr("readOnly");
    $('#currencyFilterAdd').removeAttr("readOnly");
    $('#legalEntityFilterAdd').removeAttr("readOnly");
  }

  function _loadAddDialogFilters(callback) {
    showLoading("mainLoader");
    $.when(treasury.loadfilter.legalEntities(),
        treasury.loadfilter.cpes(),
        treasury.loadfilter.loadAgreementTypes(),
        treasury.loadfilter.currencies(),
        treasury.loadfilter.nettingGroups(),
        treasury.loadfilter.interestCalculatorGroups({agreementTypeFilterIds: -1})
      )
      .done(
        function(legalEntitiesData, cpesData, agreementTypesData, currenciesData, nettingGroupsData,
          calculatorGroupData) {

          var legalEntityFilter = new window.treasury.filter.SingleSelect(
            "legalEntityFilterIds", "legalEntityFilterAdd",
            legalEntitiesData[0].descoEntities, []);

          var cpeEntityFilter = new window.treasury.filter.SingleSelect(
            "cpeFilterIds", "cpeEntityFilterAdd",
            cpesData[0].cpes, []);

          var agreementTypeFilter = new window.treasury.filter.SingleSelect(
            "agreementTypeFilterIds", "agreementTypeFilterAdd",
            agreementTypesData[0].agreementTypes, []);

          var currencyFilter = new window.treasury.filter.SingleSelect(
            "currencyFilterIds", "currencyFilterAdd",
            currenciesData[0].currency, []);

          var nettingGroupFilter = new window.treasury.filter.SingleSelect(
            "nettingGroupFilterIds", "nettingGroupFilterAdd",
            nettingGroupsData[0].nettingGroups, []);

          var calculatorGroupFilter = new window.treasury.filter.SingleSelect(
            "calculatorGroupFilterId", "calculatorGroupFilterAdd",
            calculatorGroupData[0].calculatorGroups, []);

          setStartOfMonthDateFilter();
          simulationFilterGroup = new window.treasury.filter.FilterGroup(null, [
            legalEntityFilter, cpeEntityFilter,
            agreementTypeFilter, currencyFilter,
            nettingGroupFilter, calculatorGroupFilter
          ], $("#datePickerAdd"), null);
          if (typeof callback === 'function')
            callback();
          hideLoading("mainLoader");
        }
      );

  }

  function _resizeGridCanvas() {
	  if (typeof searchGrid !== 'undefined')
		  searchGrid.resizeCanvas();
  }

})();
