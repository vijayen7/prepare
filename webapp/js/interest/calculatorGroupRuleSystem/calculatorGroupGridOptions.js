"use strict";
var simulationFilterGroup;
(function() {
  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.calculatorGroup = window.treasury.interest.calculatorGroup || {};
  window.treasury.interest.calculatorGroupGridOptions = {
    getGridOptions: _getGridOptions,
    getSimulatedGridOptions: _getSimulatedGridOptions,
  };

  function _getGridOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      highlightRowOnClick: true,
      sortList: [{
        columnId: "agreementType",
        sortAsc: true
      }],
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      useAvailableScreenSpace: true,
      applyFilteringOnGrid: true,
      exportToExcel: true,

      onCellClick: function(args, isNotToggle) {
              if (args.colId == 'calculatorGroup')
                _loadCalculatorList(args.item);
            }
    };
    
  }

  function _loadCalculatorList(item){
	  var parameters = {};
      parameters.calculatorGroupId = item.calculator_group;
      document.getElementById("calculatorListSidebar").header = item.calculatorGroup;
      var serviceCall = $.ajax({
           url: "/treasury/financing/get-calculators-from-calculator-group",
           data: parameters,
           type: "POST",
           dataType: "json"
         });
      serviceCall.done(function(data) {
    	  document.getElementById("calculatorList").innerHTML = _getTableString(data);
    	  if (document.getElementById("calculatorListSidebar").state == 'collapsed') {
              document.getElementById("calculatorListSidebar").toggle();
              document.getElementById("calculatorListSidebar").removeAttribute("hidden");
    	  }
          searchGrid.resizeCanvas();
      });
	  
  }
  
  function _getTableString(data) {
	  var tableString = '<tr><th>Name</th><th>Level</th></tr>';
	  for (var i = 0; i < data.length; i++)
    	  tableString += "<tr><td>" + data[i].name.bold() + "</td><td>" + data[i].objectType + "</td></tr>";
      return tableString;
  }
  
  function _getSimulatedGridOptions() {
    var gridOptions = _getGridOptions();
    gridOptions['checkboxHeader'] = [{
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    }];
    gridOptions['addCheckboxHeaderAsLastColumn'] = false;
    return gridOptions;
  }
})();