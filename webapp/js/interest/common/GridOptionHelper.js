"use strict";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.common = window.treasury.interest.common || {};
  window.treasury.interest.common.grid = {
    getGridOptions: _getGridOptions,
    getSimulatedGridOptions: _getSimulatedGridOptions,
  };

  function _getGridOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      highlightRowOnClick: true,
      sortList: [{
        columnId: "agreementType",
        sortAsc: true
      }],
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      useAvailableScreenSpace: true,
      applyFilteringOnGrid: true,
      isFilterOnFormattedValue: true,
      exportToExcel: true
    };
  }

  function _getSimulatedGridOptions() {
    var gridOptions = _getGridOptions();
    gridOptions['checkboxHeader'] = [{
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    }];
    gridOptions['addCheckboxHeaderAsLastColumn'] = false;
    return gridOptions;
  }
})();
