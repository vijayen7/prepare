"use strict";
var searchGrid;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.common = window.treasury.interest.common || {};
  window.treasury.interest.common.actions = {
    loadSearchGrid: _loadSearchGrid,
    loadSimulationGrids: _loadSimulationGrids,
    saveSelectedRules: _saveSelectedRules,
    loadSearchGridForData: _loadSearchGridForData,
  };

  function _loadSearchGridForData(data, gridId, columnsFunction, gridOptionFunction){
    if (data && data.resultList && data.resultList.length) {
      hideLoading("mainLoader");
      searchGrid = new dportal.grid.createGrid(
        $("#" + gridId),
        data["resultList"],
        columnsFunction(),
        gridOptionFunction());
        searchGrid.resizeCanvas();
    }
    else if (data && data.errorMessage) {

      hideLoading("mainLoader");
      raiseErrorToast("Error occured while searching for results");
    } else if (data) {

      hideLoading("mainLoader");
      raiseInfoToastWithTimeOut("No data found");
    }
  }


  function _loadSearchGrid(ajaxCall, gridId, columnsFunction, gridOptionFunction,divToClear) {
     clearGrid(gridId);
    showLoading("mainLoader");
    if( divToClear != undefined){
      document.getElementById(divToClear).style.display = "block";
    }
    ajaxCall
      .done(function(data) {
        _loadSearchGridForData(data,gridId, columnsFunction, gridOptionFunction)
      });
    ajaxCall
      .fail(function(xhr, text, errorThrown) {
        raiseErrorToast("Error occured while searching for results");
      });
  }


  function _loadSimulationGrids(ajaxCall, existingGridId, simulatedGridId,
    existingLabelId, simulatedLabelId, columnsFunction, gridOptionFunction,
    simulatedGridOptionFunction, enableSaveButtonFunction, gridVarCallBack) {
    ajaxCall
      .done(function(data) {
        if (data && data.ruleList.existingList && data.ruleList.existingList.length) {
          document.getElementById(existingLabelId).style.visibility = "visible";
          var existingGrid = new dportal.grid.createGrid(
            $("#" + existingGridId),
            data.ruleList["existingList"],
            columnsFunction(),
            gridOptionFunction());
        }
        if (data && data.ruleList.simulatedList && data.ruleList.simulatedList.length) {
          document.getElementById(simulatedLabelId).style.visibility = "visible";
          var _simulatedGrid = new dportal.grid.createGrid(
            $("#" + simulatedGridId),
            data.ruleList["simulatedList"],
            columnsFunction(),
            simulatedGridOptionFunction());
          gridVarCallBack(_simulatedGrid);
          enableSaveButtonFunction();
        } else if (data && data.errorMessage) {
          console
            .log("Error in fetching matching rules.");
        } else if (data && data.validationFailure) {
          document.getElementById("errorMessage").style.display = "block";
        }
      });
    ajaxCall
      .fail(function(xhr, text, errorThrown) {
        raiseErrorToast("Error occurred while retrieving rules.");
      });
  }

  function _saveSelectedRules(saveUrl, gridVar, onSuccessFunction) {
    var selectedData = [];
    var _grid = gridVar.grid;
    var selectedIndexes = _grid.getSelectedRows();
    jQuery.each(selectedIndexes, function(index, value) {
      selectedData.push(_grid.getDataItem(value));
    });
    if(selectedData.length == 0)
    {
      raiseErrorToast("Please select one or more rules to save.");
      return;
    }
    var toast = raiseInfoToast("Saving selected rules ...");

    var parameters = {
      "rulesToSave": JSON.stringify(selectedData)
    };
    var serviceCall = $.ajax({
      url: saveUrl,
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        toast.remove();
        if(data && data.errorMessage)
        {
          raiseErrorToast("Error occured while saving rules");
          return;
        }
        raiseSuccessToast("Rules have been saved");
        onSuccessFunction();
      });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        toast.remove();
        raiseErrorToast("Error occured while saving rules");
      });
  }


function _saveRules(saveUrl, parameters , onSuccessFunction) {

  var toast = raiseInfoToast("Saving values ...");

  var serviceCall = $.ajax({
    url: saveUrl,
    data: parameters,
    type: "POST",
    dataType: "json"
  });
  serviceCall
    .done(function(data) {
      toast.remove();
      raiseSuccessToast("Rules have been saved");
      onSuccessFunction();
    });
  serviceCall
    .fail(function(xhr, text, errorThrown) {
      toast.remove();
      raiseErrorToast("Error occured while saving rules");
    });
}
})();
