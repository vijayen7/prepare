"use strict";
var invalidRows = {};
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.assetClassColumnsConfig = {
    getAssetClassColumns: _getAssetClassColumns,
    getSearchColumns: _getSearchColumns,
    getExistingAssetClassColumns: _getExistingAssetClassColumns
  };

  function _getAssetClassColumns() {
    return [{
      id: "assetClassId",
      type: "text",
      name: "All Asset Classes",
      field: "asset_class_name",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }];
  }

  function _getExistingAssetClassColumns() {
    return [{
      id: "assetClassId",
      type: "text",
      name: "Existing Asset Classes",
      field: "asset_class_name",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }];
  }

  function _getSearchColumns() {
    return [{
        id: "assetClassGroup",
        type: "text",
        name: "Asset Class Group",
        field: "displayName",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      },
      {
        id: "editButton",
        type: "text",
        name: "",
        field: "",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return '<a><i class="fa fa-pencil-square-o fa-lg"></i></a>';
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        width: 5,
        minWidth: 5
      }
    ];
  }
})();
