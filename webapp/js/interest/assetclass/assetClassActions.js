"use strict";
var addedAssetClassIds = new Array();
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.assetClassActions = {
    loadResults: _loadResults,
    loadAssetClasses: _loadAssetClasses,
    saveAssetClass: _saveAssetClass,
    saveAssetClassGroup: _saveAssetClassGroup,
    loadAssetClassGroupById: _loadAssetClassGroupById,
    loadAssetClassGroupByIdToDisplay: _loadAssetClassGroupByIdToDisplay,
    editAssetClassGroup: _editAssetClassGroup,
    updateCurrentAssetClassGrid: _updateCurrentAssetClassGrid

  };


  function _getTableString(data) {
    var tableString = '<tr><th>Name</th><th>Level</th></tr>';
    for (var i = 0; i < data.length; i++)
      tableString += "<tr><td>" + data[i].name.bold() + "</td><td>" + data[i].objectType + "</td></tr>";
    return tableString;
  }

  function _updateCurrentAssetClassGrid(toBeSelected) {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-asset-classes",
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        hideLoading("mainLoader");
        if (data && data.resultList && data.resultList.length) {
          var assetClassData = data["resultList"];
          var currentAssetClassesList = currentAssetClassesAddGrid.data;
          for (var i in toBeSelected) {
            var assetClass = assetClassData.find(function(element) {
              return element.id == toBeSelected[i];
            });
            currentAssetClassesList.push(assetClass);
          }

          clearGrid("currentAssetClassesAddGrid");
          currentAssetClassesAddGrid = new dportal.grid.createGrid(
            $("#currentAssetClassesAddGrid"),
            currentAssetClassesList,
            treasury.interest.assetClassColumnsConfig.getExistingAssetClassColumns(),
            treasury.interest.assetClassGridOptions.getAssetClassOptions(
              "attributesGrid"
            )
          );
        }
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      // clearGrid("allAssetClassesEditGrid");
      hideLoading("mainLoader");
      console.log("Error occurred while updating asset class list.");
    });
  }

  function _loadAssetClassGroupById(parameters) {
    showLoading("editAssetClassGroupLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-asset-class-group-by-id",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    serviceCall
      .done(function(data) {

        clearGrid('currentAssetClassesGrid');
        var assetClassData = data["resultList"];
        if (assetClassData == null)
          assetClassData = [];
        currentAssetClassesGrid = new dportal.grid.createGrid(
          $("#currentAssetClassesGrid"), assetClassData,
          treasury.interest.assetClassColumnsConfig
          .getExistingAssetClassColumns(),
          treasury.interest.assetClassGridOptions
          .getAssetClassOptions("attributesEditGrid"));


        _loadUpdatedAssetClasses();

      });
  }

  function _loadUpdatedAssetClasses() {
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-asset-classes",
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        clearGrid("allAssetClassesEditGrid");
        if (data && data.resultList && data.resultList.length) {
          var assetClassData = data["resultList"];
          allAssetClassesEditGrid = new dportal.grid.createGrid(
            $("#allAssetClassesEditGrid"), assetClassData,
            treasury.interest.assetClassColumnsConfig
            .getAssetClassColumns(),
            treasury.interest.assetClassGridOptions
            .getAssetClassOptions("attributesEditGrid"));

          var updatedDataInGrid = allAssetClassesEditGrid.data;
          var existingAssetClassesIndexIdMap = {};
          for (var i in allAssetClassesEditGrid.data) {
            existingAssetClassesIndexIdMap[allAssetClassesEditGrid.data[i].id] = allAssetClassesEditGrid.data[i];
          }

          for (var i in currentAssetClassesGrid.data) {
            var index = allAssetClassesEditGrid.data.indexOf(existingAssetClassesIndexIdMap[currentAssetClassesGrid.data[i].id]);
            updatedDataInGrid.splice(index, 1);
          }

          clearGrid("allAssetClassesEditGrid");

          allAssetClassesEditGrid = new dportal.grid.createGrid(
            $("#allAssetClassesEditGrid"), updatedDataInGrid,
            treasury.interest.assetClassColumnsConfig
            .getAssetClassColumns(),
            treasury.interest.assetClassGridOptions
            .getAssetClassOptions("attributesEditGrid"));
        } else if (data && data.errorMessage) {
          // showErrorMessage(data.errorMessage);
          console.log("Error in fetching asset classes.");
        } else if (data) {
          document.getElementById("allAssetClassesEditGrid").innerHTML = getFormattedMessage(
            'No asset classes found.', 'primary');
        }
          hideLoading("editAssetClassGroupLoader");
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      clearGrid("allAssetClassesEditGrid");
      hideLoading("editAssetClassGroupLoader");
      console.log("Error occurred while retrieving asset classes.");
    });
  }

  function _loadAssetClassGroupByIdToDisplay(parameters, assetClassName) {
    showLoading("mainLoader");
    var assetClassData;
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-asset-class-group-by-id",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    serviceCall
      .done(function(data) {
        hideLoading("mainLoader");
        clearGrid('currentAssetClassesGrid');
        assetClassData = data["resultList"];
        $('#assetClassList').empty();

        if (assetClassData == null || assetClassData.length == 0) {
          $('#assetClassList').append('<center><p>No Asset Classes found for ' + assetClassName + '</p></center>');
        } else {
          $('#assetClassList').append('<div class="message no-icon text-align--center">Asset Classes for ' + assetClassName + '</div><br />');
          for (var i in assetClassData) {
            treasury.interest.assetClassGridOptions.createGridTables(assetClassData[i]);
            treasury.interest.assetClassGridOptions.createAttributeTable(assetClassData[i].attributes, assetClassData[i].id);
          }
        }
      });

  }

  function _loadResults(parameters) {
    $('#assetClassList').empty();
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-asset-class-group-search-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.interest.common.actions
      .loadSearchGrid(
        serviceCall,
        "assetClassGroupSearchGrid",
        treasury.interest.assetClassColumnsConfig.getSearchColumns,
        treasury.interest.assetClassGridOptions.getAssetClassGroupGridOptions, "assetClassGroupSearch");
  }

  function _loadAssetClasses() {
    showLoading("addAssetClassGroupLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-asset-classes",
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        clearGrid("allAssetClassesGrid");
        if (data && data.resultList && data.resultList.length) {
          var assetClassData = data["resultList"];
          allAssetClassesGrid = new dportal.grid.createGrid(
            $("#allAssetClassesGrid"), assetClassData,
            treasury.interest.assetClassColumnsConfig
            .getAssetClassColumns(),
            treasury.interest.assetClassGridOptions
            .getAssetClassOptions("attributesGrid"));

        } else if (data && data.errorMessage) {
          document.getElementById("addAssetClassGroupDialog").visible = false;
          // showErrorMessage(data.errorMessage);
          console.log("Error in fetching asset classes.");
        } else if (data) {
          document.getElementById("allAssetClassesGrid").innerHTML = getFormattedMessage(
            'No asset classes found.', '');
        }
          hideLoading("addAssetClassGroupLoader");
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      clearGrid("allAssetClassesGrid");
      hideLoading("addAssetClassGroupLoader");
      console.log("Error occurred while retrieving asset classes.");
    });
  }

  function _editAssetClassGroup(parameters) {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/edit-asset-class-group",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        hideLoading("mainLoader");
        treasury.interest.assetClass.resetAssetClassGroupValues();
        document.getElementById("editAssetClassGroupDialog").visible = false;
        if (data && data.success) {
          document.getElementById("editAssetClassGroupDialog").visible = false;
          showMessage("Asset Class Group updated.", 'success');
        } else if (data && data.errorMessage) {
          document.getElementById("addAssetClassGroupDialog").visible = false;
          showMessage("Asset classe group already exists", 'critical');
        } else if (data) {
          console.log('Error in updating asset class groups.');
        }
        addedAssetClassIds = new Array();
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      hideLoading("mainLoader");
      console.log("Error occurred while updating asset class group.");
    });
  }

  function _saveAssetClassGroup(parameters, onSuccessFunction) {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/save-asset-class-group",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        hideLoading("mainLoader");
        treasury.interest.assetClass.resetAssetClassGroupValues();
        if (data && data.success) {
          showMessage("Asset Class Group added.", 'success');
          document.getElementById("addAssetClassGroupDialog").visible = false;
          document.getElementById("submitAddAssetClassGroup").disabled = true;
        } else if (data && data.errorMessage) {
          showMessage("Asset Class Group with this name or asset classes already exists", 'critical');
        } else if (data) {
          console.log("Error in adding Asset class group.")
        }
        addedAssetClassIds = new Array();
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      hideLoading("mainLoader");
      console.log("Error occurred while saving asset class group.");
    });
  }

  function _saveAssetClass() {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/save-asset-class",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        hideLoading("mainLoader");
        if (data && data.assetClassId) {
          addedAssetClassIds.push(data.assetClassId);
          if (!$("#addAssetClassDiv").prev().is("p")) {
            $(
                "<div class='message--success'><p>Asset class was added. Further asset classes may be " +
                "created. Click Done, to proceed to adding asset class group.</p></div>")
              .insertAfter("#assetClassError");
          }
          treasury.interest.assetClass.resetAssetClassValues();
          document.getElementById("assetClassDone").disabled = false;
        } else if (data && data.errorMessage) {
          showMessage("Asset Class with this name or properties already exists.", 'critical');
        } else if (data) {
          $(
            getFormattedMessage(
              'Error in saving asset class.',
              'warning') +
            "<br/>").insertBefore(
            "#addAssetClassDiv");
        }
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      clearGrid("allAssetClassesEditGrid");
      hideLoading("mainLoader");
      console.log("Error occurred while retrieving asset classes.");
    });
  }
})();
