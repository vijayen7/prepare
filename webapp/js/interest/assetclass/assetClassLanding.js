"use strict";
var selectedAssetClassIds;
var parameters;
var addedAcIds;
var attributeSet = new Array();
var attrNameSelect;
var foTypeSelect;
var countryFilter;
var currencyFilter;
var gradingSelect;
var foSubtypeAssociation = new Array();
var selectedDate;
var allAssetClassesEditGrid;
var allAssetClassesGrid;
var currentAssetClassesGrid;
var currentAssetClassesAddGrid;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.assetClass = {
    registerHandlers: _registerHandlers,
    loadSearchResults: _loadSearchResults,
    addAssetClass: _addAssetClass,
    loadAddDialog: _loadAddDialog,
    addAssetClassGroup: _addAssetClassGroup,
    resetAssetClassGroupValues: _resetAssetClassGroupValues,
    resetAssetClassValues: _resetAssetClassValues,
    assetClassDone: _assetClassDone,
    addAttribute: _addAttribute,
    removeAttribute: _removeAttribute,
    saveAssetClass: _saveAssetClass,
    pageDestructor: _pageDestructor,
    loadAgreementNotificationDialog: _loadAgreementNotificationDialog,
    editAssetClassGroup: _editAssetClassGroup,
    leftShiftAssetClass: _leftShiftAssetClass,
    rightShiftAssetClass: _rightShiftAssetClass
  };

  function _registerHandlers() {
    _showAdditionalCheckbox();

    document.getElementById(
      "submitEditAssetClassGroup"
    ).onclick = _loadDialogForAgreementsAffected;

    document.getElementById("rightShift").onclick = function() {
      treasury.interest.assetClass.rightShiftAssetClass(
        "allAssetClassesEditGrid",
        "currentAssetClassesGrid",
        "allAssetClassesEditGrid",
        "currentAssetClassesGrid",
        "attributesEditGrid"
      );
    };
    document.getElementById("leftShift").onclick = function() {
      treasury.interest.assetClass.leftShiftAssetClass(
        "allAssetClassesEditGrid",
        "currentAssetClassesGrid",
        "allAssetClassesEditGrid",
        "currentAssetClassesGrid",
        "attributesEditGrid"
      );
    }

    document.getElementById("rightShiftInAdd").onclick = function() {
      treasury.interest.assetClass.rightShiftAssetClass(
        "allAssetClassesGrid",
        "currentAssetClassesAddGrid",
        "allAssetClassesGrid",
        "currentAssetClassesAddGrid",
        "attributesGrid"
      );
    };
    document.getElementById("leftShiftInAdd").onclick = function() {
      treasury.interest.assetClass.leftShiftAssetClass(
        "allAssetClassesGrid",
        "currentAssetClassesAddGrid",
        "allAssetClassesGrid",
        "currentAssetClassesAddGrid",
        "attributesGrid"
      );
    }
  }

  function _showAdditionalCheckbox() {
    document.getElementById("assetClassGroupCheckbox").style.visibility =
      "visible";
  }

  function _loadDialogForAgreementsAffected() {
    document.getElementById("editAssetClassGroupError").style.display =
          "none";
    var config = {
      title: "Confirmation",
      dismissible: true,
      width: "470px",
      height: "380px"
    };

    document.querySelector("#agreementsAffectedDialog").reveal(config);
    document.getElementById("performAssetClassGroupSave").onclick =
      treasury.interest.assetClass.editAssetClassGroup;
    var parameters = {
      selectedAssetClassGroupId: assetClassGroupId.toString()
    };
    treasury.interest.assetClassGridOptions.loadAgreementsAffected(parameters);
  }

    function _loadAssetClassGroupErrorDialog(parameters, action) {
      var config = {
        title: "Warning",
        dismissible: true,
        width: "470px",
        height: "150px"
      };

      document.querySelector("#assetClassErrorDialog").reveal(config);
      document.getElementById("negativeConfirmation").onclick = function() {
            _loadAssetClassGroupErrorMessage(action);
      }
      document.getElementById("positiveConfirmation").onclick = function() {
            if(action === "edit") {
                _editAssetClassGroupAfterConfirmation(parameters);
            }
            else if(action === "add") {
                _addAssetClassGroupAfterConfirmation(parameters);
            }
        };
       return;
    }

    function _loadAssetClassGroupErrorMessage(action) {
        document.getElementById("assetClassErrorDialog").visible = false;
        document.getElementById("agreementsAffectedDialog").visible = false;
        if(action === "edit")
        {
                document.getElementById("editAssetClassGroupError").innerHTML =
                        "Asset Class Group must have at least one asset class. Please add one or more asset class and try again.";
                document.getElementById("editAssetClassGroupError").style.display =
                        "block";
        }
        else if (action === "add")
        {
                document.getElementById("assetClassGroupError").innerHTML =
                        "Asset Class Group must have at least one asset class. Please add one or more asset class and try again.";
                document.getElementById("assetClassGroupError").style.display =
                        "block";
        }
        return;
    }

  function _rightShiftAssetClass(leftGrid, rightGrid, leftGridId, rightGridId, gridId) {
    var existingAssetClassesMap = window[leftGrid].data.reduce(function(map, obj) {
      map[obj.id] = obj;
      return map;
    }, {});
    var selectedRows = window[leftGrid].selectedRowIds();
    var updatedDataInCurrentGrid = window[rightGrid].data;
    var updatedDataInExistingGrid = window[leftGrid].data;

    for (var i in selectedRows) {
      updatedDataInCurrentGrid.push(existingAssetClassesMap[selectedRows[i]]);
      var index = window[leftGrid].data.indexOf(
        existingAssetClassesMap[selectedRows[i]]
      );
      updatedDataInExistingGrid.splice(index, 1);
    }

    clearGrid(leftGridId);
    clearGrid(rightGridId);
    window[rightGrid] = new dportal.grid.createGrid(
      $("#" + rightGridId),
      updatedDataInCurrentGrid,
      treasury.interest.assetClassColumnsConfig.getExistingAssetClassColumns(),
      treasury.interest.assetClassGridOptions.getAssetClassOptions(gridId)
    );
    window[leftGrid] = new dportal.grid.createGrid(
      $("#" + leftGridId),
      updatedDataInExistingGrid,
      treasury.interest.assetClassColumnsConfig.getAssetClassColumns(),
      treasury.interest.assetClassGridOptions.getAssetClassOptions(gridId)
    );

    window[leftGrid].setSelectedRowIds([]);
  }

  function _leftShiftAssetClass(leftGrid, rightGrid, leftGridId, rightGridId, gridId) {
    var currentAssetClassesMap = window[rightGrid].data.reduce(function(map, obj) {
      map[obj.id] = obj;
      return map;
    }, {});
    var selectedRows = window[rightGrid].selectedRowIds();
    var updatedDataInCurrentGrid = window[rightGrid].data;
    var updatedDataInExistingGrid = window[leftGrid].data;

    for (var i in selectedRows) {
      updatedDataInExistingGrid.push(currentAssetClassesMap[selectedRows[i]]);
      var index = window[rightGrid].data.indexOf(
        currentAssetClassesMap[selectedRows[i]]
      );
      updatedDataInCurrentGrid.splice(index, 1);
    }

    clearGrid(leftGridId);
    clearGrid(rightGridId);
    window[rightGrid] = new dportal.grid.createGrid(
      $("#" + rightGridId),
      updatedDataInCurrentGrid,
      treasury.interest.assetClassColumnsConfig.getExistingAssetClassColumns(),
      treasury.interest.assetClassGridOptions.getAssetClassOptions(gridId)
    );
    window[leftGrid] = new dportal.grid.createGrid(
      $("#" + leftGridId),
      updatedDataInExistingGrid,
      treasury.interest.assetClassColumnsConfig.getAssetClassColumns(),
      treasury.interest.assetClassGridOptions.getAssetClassOptions(gridId)
    );

    window[rightGrid].setSelectedRowIds([]);
  }

  function _editAssetClassGroup() {
    var selectedIds = [];
    for (var i in currentAssetClassesGrid.data) {
      selectedIds.push(currentAssetClassesGrid.data[i].id);
    }

    var parameters = {
      selectedAcIds: selectedIds.toString(),
      selectedAssetClassGroupId: assetClassGroupId.toString(),
      dateString: getFormattedDate("assetClassEditDatePicker")
    };
    if (!("selectedAcIds" in parameters) ||
      parameters["selectedAcIds"] === ""
    ) {
        _loadAssetClassGroupErrorDialog(parameters, "edit");
      return;
    }
    _editAssetClassGroupAfterConfirmation(parameters);
  }

  function _editAssetClassGroupAfterConfirmation(parameters) {
        document.getElementById("assetClassErrorDialog").visible = false;
        document.getElementById("editAssetClassGroupError").style.display =
          "none";
        treasury.interest.assetClassActions.editAssetClassGroup(parameters);
        document.getElementById("agreementsAffectedDialog").visible = false;
        document.getElementById("editAssetClassGroupDialog").visible = false;
        parameters = {};
  }

  function _loadSearchResults() {
    if (document.getElementById("datePickerError").innerHTML != "") {
      $("#datePickerAdd").addClass("error");
      return;
    } else {
      $("#datePickerAdd").removeClass();
    }

    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate("datePicker");
    parameters.dateString = formattedDate;
    parameters.allAssetClassGroups = loadAllAssetClassGroupsCheck.checked.toString();
    selectedDate = formattedDate;

    treasury.interest.assetClassActions.loadResults(parameters);
  }

  function _pageDestructor() {
    document.getElementById("assetClassGroupCheckbox").style.visibility =
      "hidden";
  }

  function _loadAddDialog() {
    document.getElementById("editAssetClassGroupError").style.display = "none";
    document.getElementById("assetClassGroupError").style.display = "none";
    document.getElementById("assetClassGroupNameError").style.display = "none";
    document.getElementById("addAssetClassGroupDialog").visible = true;
    document.getElementById("assetClassGroupName").value = "";
    document.getElementById("assetClassGroupDescription").value = "";
    clearGrid("allAssetClassesGrid");
    if (document.getElementById("attributesGrid").innerHTML != "") {
      clearGrid("attributesGrid");
    }
    $("#attrMessage").show();
    var dateFilterValue = Date.today();
    dateFilterValue.setDate(1);
    var minDate = Date.parse('1970-01-01');
    var maxDate = Date.parse('2038-01-01');
    var dateFormat = 'yy-mm-dd';
    setupDateFilter($("#assetClassAddDatePicker"), $("#assetAddClassDatePickerError"), dateFilterValue, minDate, maxDate,
      dateFormat);
    treasury.interest.assetClassActions.loadAssetClasses();
    currentAssetClassesAddGrid = new dportal.grid.createGrid(
      $("#currentAssetClassesAddGrid"), [],
      treasury.interest.assetClassColumnsConfig.getExistingAssetClassColumns(),
      treasury.interest.assetClassGridOptions.getAssetClassOptions(
        "attributesEditGrid"
      )
    );
    document.getElementById("submitAddAssetClassGroup").disabled = false;
    document.getElementById("submitAddAssetClassGroup").onclick =
      treasury.interest.assetClass.addAssetClassGroup;

    document.getElementById("addNewAssetClass").onclick =
      treasury.interest.assetClass.addAssetClass;
  }

  function _loadAgreementNotificationDialog() {
    document.getElementById("agreementNotification").visible = true;
    $.when(treasury.asset);
  }

  function _addAssetClass() {
    if (
      $("#assetClassError")
      .next()
      .is("p")
    ) {
      $("#assetClassError")
        .next()
        .remove();
    }
    document.getElementById("addAssetClassGroupDialog").visible = false;
    treasury.interest.assetClass.resetAssetClassValues();
    document.getElementById("assetClassError").style.visibility = "hidden";
    document.getElementById("addAssetClassDialog").visible = true;
    $.when(treasury.loadfilter.assetClassAttrSourceTypes()).done(function(
      assetClassAttrSourceTypesData
    ) {
      var sourceTypeSelect = new window.treasury.filter.SingleSelect(
        "sourceTypeId",
        "sourceTypeSelect",
        assetClassAttrSourceTypesData["attrSourceTypes"], []
      );
      document
        .querySelector("#sourceTypeSelect")
        .addEventListener("change", _sourceTypeOnChange);
      document.getElementById("submitAssetClass").onlick =
        treasury.interest.assetClassActions.saveAssetClass;
      document.getElementById("assetClassDone").onclick =
        treasury.interest.assetClass.assetClassDone;
    });
  }

  function _addAssetClassGroup() {
    treasury.interest.assetClass.resetAssetClassGroupValues();
    var acgName = document.getElementById("assetClassGroupName").value.trim();
    var acgDescription = document
      .getElementById("assetClassGroupDescription")
      .value.trim();

    if (!/\S/.test(acgName) || !/\S/.test(acgDescription)) {
      document.getElementById("assetClassGroupNameError").innerHTML =
        "Asset Class Group must have a name and description.";
      document.getElementById("assetClassGroupNameError").style.display =
        "block";
      return;
    }
    document.getElementById("assetClassGroupNameError").style.display =
            "none";

    var selectedIds = [];
    for (var i in currentAssetClassesAddGrid.data) {
      selectedIds.push(currentAssetClassesAddGrid.data[i].id);
    }

    parameters = {
      acgName: document.getElementById("assetClassGroupName").value,
      acgDescription: document.getElementById("assetClassGroupDescription")
        .value,
      selectedAcIds: selectedIds.toString(),
      dateString: getFormattedDate("assetClassAddDatePicker")
    };
    if (!("selectedAcIds" in parameters) ||
      parameters["selectedAcIds"] === ""
    ) {
      _loadAssetClassGroupErrorDialog(parameters, "add");
      return;
    }
    _addAssetClassGroupAfterConfirmation(parameters);
  }

  function _addAssetClassGroupAfterConfirmation(parameters) {
    clearGrid("allAssetClassesEditGrid");
    document.getElementById("assetClassErrorDialog").visible = false;
    document.getElementById("addAssetClassGroupDialog").visible = false;
    document.getElementById("assetClassGroupError").style.display = "none";
    treasury.interest.assetClassActions.saveAssetClassGroup(parameters);
    parameters = {};
  }

  function _resetAssetClassGroupValues() {
    selectedAssetClassIds = new Array();
    parameters = {};
    addedAcIds = "";
  }

  function _resetAssetClassValues() {
    parameters = {};
    attributeSet = new Array();
    foSubtypeAssociation = new Array();
    document.getElementById("submitAssetClass").disabled = true;
    document.getElementById("addAssetClassAttribute").disabled = true;
    document.getElementById("assetClassName").value = "";
    document.getElementById("sourceTypeSelect").value = null;
    document.getElementById("attrNameSelect").value = null;
    document.getElementById("attrValueSelect").value = null;
    document.getElementById("attrValue").value = null;
    document.getElementById("countryCurrencyFilter").value = null;
    document.getElementById("subtypeSelect").value = null;
    document.getElementById("attrValue").value = "";
    document.getElementById("addedAttributes").innerHTML = "";
  }

  function _assetClassDone() {
    document.getElementById("addAssetClassDialog").visible = false;
    document.getElementById("addAssetClassGroupDialog").visible = true;
    treasury.interest.assetClassActions.updateCurrentAssetClassGrid(addedAssetClassIds);
    addedAssetClassIds = new Array();
  }

  function _sourceTypeOnChange() {
    var sourceType = document.getElementById("sourceTypeSelect").value;
    document.getElementById("attrValueEditDiv").style.display = "none";
    document.getElementById("countryCurrencyFilterDiv").style.display = "none";
    document.getElementById("subtypeSelect").value = null;
    document.getElementById("subtypeEditDiv").style.display = "none";
    document.getElementById("attrValueDiv").style.display = "none";
    document.getElementById("attrValue").value = "";
    $.when(treasury.loadfilter.assetClassAttributes(sourceType.key)).done(
      function(assetClassAttributesData) {
        attrNameSelect = new window.treasury.filter.SingleSelect(
          "attrName",
          "attrNameSelect",
          assetClassAttributesData["attrNames"], []
        );
        document
          .querySelector("#attrNameSelect")
          .addEventListener("change", _attrNameOnChange);
      }
    );
    document.getElementById("addAssetClassAttribute").disabled = false;
    document.getElementById("addAssetClassAttribute").onclick =
      treasury.interest.assetClass.addAttribute;
  }

  function _attrNameOnChange() {
    var attrName = document.getElementById("attrNameSelect");
    if (attrName.value.key == "type_id") {
      document.getElementById("attrValue").value = "";
      document.getElementById("attrValueDiv").style.display = "none";
      document.getElementById("countryCurrencyFilterDiv").style.display =
        "none";
      document.getElementById("attrValueEditDiv").style.display = "block";
      document.getElementById("subtypeSelect").value = null;
      document.getElementById("subtypeSelect").style.diaply = "block";
      $.when(treasury.loadfilter.fotypes()).done(function(fotypesData) {
        foTypeSelect = new window.treasury.filter.SingleSelect(
          "foTypeId",
          "attrValueSelect",
          fotypesData["attrFOTypes"], []
        );
        document
          .querySelector("#attrValueSelect")
          .addEventListener("change", _foTypeOnChange);
      });
    } else if (attrName.value.key == "spn") {
      document.getElementById("attrValueEditDiv").style.display = "none";
      document.getElementById("subtypeEditDiv").style.display = "none";
      document.getElementById("countryCurrencyFilterDiv").style.display =
        "none";
      document.getElementById("attrValueDiv").style.display = "block";
      document.getElementById("attrValueDiv").value = "";
    }
    else if (attrName.value.key == "isin") {
      document.getElementById("attrValueEditDiv").style.display = "none";
      document.getElementById("subtypeEditDiv").style.display = "none";
      document.getElementById("countryCurrencyFilterDiv").style.display =
        "none";
      document.getElementById("attrValueDiv").style.display = "block";
      document.getElementById("attrValueDiv").value = "";
    }
     else if (attrName.value.key == "grading") {
      document.getElementById("subtypeEditDiv").style.display = "none";
      document.getElementById("countryCurrencyFilterDiv").style.display =
        "none";
      document.getElementById("attrValueDiv").style.display = "none";
      document.getElementById("attrValue").value = "";
      document.getElementById("attrValueEditDiv").style.display = "block";
      $.when(treasury.loadfilter.gradings()).done(function(gradingsData) {
        gradingSelect = new window.treasury.filter.SingleSelect(
          "grading",
          "attrValueSelect",
          gradingsData["grading"], []
        );
      });
    } else if (attrName.value.key == "subtype_id") {
      document.getElementById("attrValue").value = "";
      document.getElementById("attrValueDiv").style.display = "none";
      document.getElementById("countryCurrencyFilterDiv").style.display =
        "none";
      document.getElementById("attrValueEditDiv").style.display = "block";
      $.when(treasury.loadfilter.fotypes()).done(function(fotypesData) {
        foTypeSelect = new window.treasury.filter.SingleSelect(
          "foTypeId",
          "attrValueSelect",
          fotypesData["attrFOTypes"], []
        );
        document
          .querySelector("#attrValueSelect")
          .addEventListener("change", _foTypeOnChange);
        document.getElementById("subtypeEditDiv").style.display = "block";
      });
    } else if (attrName.value.key == "country_id") {
      document.getElementById("attrValue").value = "";
      document.getElementById("attrValueEditDiv").style.display = "none";
      document.getElementById("attrValueDiv").style.display = "none";
      document.getElementById("subtypeEditDiv").style.display = "none";
      document.getElementById("countryCurrencyFilterDiv").style.display =
        "block";
      $.when(treasury.loadfilter.countries()).done(function(countriesData) {
        countryFilter = new window.treasury.filter.SingleSelect(
          "countryId",
          "countryCurrencyFilter",
          countriesData["countries"], []
        );
      });
    } else if (attrName.value.key == "currency_id") {
      document.getElementById("attrValue").value = "";
      document.getElementById("attrValueEditDiv").style.display = "none";
      document.getElementById("attrValueDiv").style.display = "none";
      document.getElementById("subtypeEditDiv").style.display = "none";
      document.getElementById("countryCurrencyFilterDiv").style.display =
        "block";
      $.when(treasury.loadfilter.currencies()).done(function(currenciesData) {
        currencyFilter = new window.treasury.filter.SingleSelect(
          "currencyId",
          "countryCurrencyFilter",
          currenciesData["currency"], []
        );
      });
    }
  }

  function _foTypeOnChange() {
    var attrValue = document.getElementById("attrValueSelect").value;
    if (document.getElementById("attrNameSelect").value.key == "grading") {
      return;
    }
    $.when(treasury.loadfilter.foSubtypes(attrValue.key)).done(function(
      foSubtypesData
    ) {
      document.getElementById("subtypeEditDiv").style.display = "block";
      var foSubtypeSelect = new window.treasury.filter.SingleSelect(
        "foSubtypeId",
        "subtypeSelect",
        foSubtypesData["foSubtypes"], []
      );
    });
  }

  function _addAttribute() {
    if (
      $("#assetClassError")
      .next()
      .is("p")
    ) {
      $("#assetClassError")
        .next()
        .remove();
    }

    var id = "",
      value = "",
      attributes = "";
    var sourceTypeValue = document.getElementById("sourceTypeSelect").value;
    if (
      sourceTypeValue == null ||
      (document.getElementById("attrValueSelect").value == null &&
        document.getElementById("attrValue").value == null &&
        document.getElementById("countryCurrencyFilter").value == null)
    ) {
      document.getElementById("assetClassError").innerHTML =
        "No attribute added.";
      document.getElementById("assetClassError").style.visibility = "visible";
      return;
    }
    id = id.concat(sourceTypeValue.key, "-");
    value = value.concat("sourceType:", sourceTypeValue.key, ",");
    attributes = attributes.concat(
      "Source Type : ",
      sourceTypeValue.value,
      ","
    );

    var attrNameValue = document.getElementById("attrNameSelect").value;

    if (attrNameValue.key == "subtype_id") {
      id = id.concat("type_id-");
      value = value.concat("attrName:type_id,");
      attributes = attributes.concat(" Attribute : FOType,");
    } else {
      id = id.concat(attrNameValue.key, "-");
      value = value.concat("attrName:", attrNameValue.key, ",");
      attributes = attributes.concat(" Attribute : ", attrNameValue.value, ",");
    }
    var attrValue;
    var typeId, subtypeId;
    switch (attrNameValue.key) {
      case "grading":
        attrValue = document.getElementById("attrValueSelect").value.value;
        id = id.concat(attrValue);
        value = value.concat("attrValue:", attrValue, ",underlyingCheck:;");
        attributes = attributes.concat("Value : ", attrValue);
        attributeSet[id] = value;
        $(
          "<div id='" +
          id +
          "'>" +
          attributes +
          " &nbsp; <a onclick=\"treasury.interest.assetClass.removeAttribute('" +
          id +
          "')\">X</a></div>"
        ).appendTo("#addedAttributes");
        break;
        break;
      case "isin" :
        attrValue = document.getElementById("attrValue").value;
        id = id.concat(attrValue);
        value = value.concat("attrValue:", attrValue, ",underlyingCheck:;");
        attributes = attributes.concat(" Value : ", attrValue);
        attributeSet[id] = value;
        $(
          "<div id='" +
          id +
          "'>" +
          attributes +
          " &nbsp; <a onclick=\"treasury.interest.assetClass.removeAttribute('" +
          id +
          "')\">X</a></div>"
        ).appendTo("#addedAttributes");
      break;
      case "spn":
        attrValue = document.getElementById("attrValue").value;
        var numbers = /^[0-9]+$/;
        if (attrNameValue.value == "PNL SPN" && !attrValue.match(numbers)) {
          alert("Only numeric values allowed.");
          return;
        }
        id = id.concat(attrValue);
        value = value.concat("attrValue:", attrValue, ",underlyingCheck:;");
        attributes = attributes.concat(" Value : ", attrValue);
        attributeSet[id] = value;
        $(
          "<div id='" +
          id +
          "'>" +
          attributes +
          " &nbsp; <a onclick=\"treasury.interest.assetClass.removeAttribute('" +
          id +
          "')\">X</a></div>"
        ).appendTo("#addedAttributes");
        break;
      case "type_id":
      case "subtype_id":
        typeId = document.getElementById("attrValueSelect").value;
        id = id.concat(typeId.key);
        value = value.concat("attrValue:", typeId.key, ",underlyingCheck:;");
        attributes = attributes.concat(" Value : ", typeId.value);
        if (!(id in attributeSet)) {
          $(
            "<div id='" +
            id +
            "'>" +
            attributes +
            " &nbsp; <a onclick=\"treasury.interest.assetClass.removeAttribute('" +
            id +
            "')\">X</a></div>"
          ).appendTo("#addedAttributes");
        }
        attributeSet[id] = value;

        subtypeId = document.getElementById("subtypeSelect").value;
        if (subtypeId != null) {
          var foTypeIdString = id;

          id = "".concat(sourceTypeValue.value, "-");
          id = id.concat("subtype_id", "-");
          id = id.concat(subtypeId.key);
          foSubtypeAssociation[foTypeIdString] = id;
          value = "".concat("sourceType:", sourceTypeValue.key, ",");
          value = value.concat("attrName:", "subtype_id", ",");
          value = value.concat(
            "attrValue:",
            subtypeId.key,
            ",underlyingCheck:;"
          );
          attributes = "".concat("Source Type : ", sourceTypeValue.value, ",");
          attributes = attributes.concat(" Attribute : ", "Subtype,");
          attributes = attributes.concat(" Value : ", subtypeId.value);
          attributeSet[id] = value;
          $(
            "<div id='" +
            id +
            "'>" +
            attributes +
            " &nbsp; <a onclick=\"treasury.interest.assetClass.removeAttribute('" +
            id +
            "')\">X</a></div>"
          ).appendTo("#addedAttributes");
        }

        break;
      case "currency_id":
      case "country_id":
        attrValue = document.getElementById("countryCurrencyFilter").value;
        id = id.concat(attrValue.key);
        value = value.concat("attrValue:", attrValue.key, ",underlyingCheck:;");
        attributes = attributes.concat("Value : ", attrValue.value);
        attributeSet[id] = value;
        $(
          "<div id='" +
          id +
          "'>" +
          attributes +
          " &nbsp; <a onclick=\"treasury.interest.assetClass.removeAttribute('" +
          id +
          "')\">X</a></div>"
        ).appendTo("#addedAttributes");
        break;
    }
    document.getElementById("assetClassError").style.visibility = "hidden";
    document.getElementById("submitAssetClass").disabled = false;
    document.getElementById("submitAssetClass").onclick =
      treasury.interest.assetClass.saveAssetClass;

    //Set all input fields to empty.
    document.getElementById("sourceTypeSelect").value = null;
    document.getElementById("attrNameSelect").value = null;
    document.getElementById("attrValueSelect").value = null;
    document.getElementById("attrValue").value = null;
    document.getElementById("countryCurrencyFilter").value = null;
    document.getElementById("subtypeSelect").value = null;
    document.getElementById("attrValue").value = "";
  }

  function _removeAttribute(currentDivId) {
    if (
      currentDivId.includes("type_id") &&
      currentDivId in foSubtypeAssociation
    ) {
      delete attributeSet[foSubtypeAssociation[currentDivId]];
      $("#" + foSubtypeAssociation[currentDivId]).remove();
      delete foSubtypeAssociation[currentDivId];
    }
    delete attributeSet[currentDivId];
    $("#" + currentDivId).remove();

    //Set all input fields to empty
    document.getElementById("sourceTypeSelect").value = null;
    document.getElementById("attrNameSelect").value = null;
    document.getElementById("attrValueSelect").value = null;
    document.getElementById("attrValue").value = null;
    document.getElementById("countryCurrencyFilter").value = null;
    document.getElementById("subtypeSelect").value = null;
    document.getElementById("attrValue").value = "";
  }

  function _saveAssetClass() {
    var assetClassName = document.getElementById("assetClassName").value;
    if (!/\S/.test(assetClassName)) {
      document.getElementById("assetClassError").innerHTML =
        "Asset Class must have a name.";
      document.getElementById("assetClassError").style.visibility = "visible";
      return;
    }
    var attributeString = "";
    for (var key in attributeSet) {
      attributeString = attributeString.concat(attributeSet[key]);
    }
    parameters = {
      assetClassName: assetClassName,
      attributes: attributeString
    };
    document.getElementById("assetClassError").style.visibility = "hidden";
    treasury.interest.assetClassActions.saveAssetClass(parameters);
  }
})();
