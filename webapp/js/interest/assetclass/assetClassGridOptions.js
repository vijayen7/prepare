"use strict";
var attributesGrid;
var assetClassGroupId;

(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.assetClassGridOptions = {
    getAssetClassOptions: _getAssetClassOptions,
    getAttributeColumns: _getAttributeColumns,
    getAttributeGridOptions: _getAttributeGridOptions,
    getAssetClassGroupGridOptions: _getAssetClassGroupGridOptions,
    createGridTables: _createGridTables,
    createAttributeTable: _createAttributeTable,
    loadAgreementsAffected: _loadAgreementsAffected
  };

  function _getAssetClassGroupGridOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      highlightRowOnClick: true,
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      useAvailableScreenSpace: true,
      applyFilteringOnGrid: true,
      exportToExcel: true,
      onRowClick: function(item, rowObject$, isNotToggle) {
        _showAssetClassList(item);
      },
      onCellClick: function(args, isNotToggle) {
        if (args.colId == 'editButton')
          _editAssetClassGroupDialog(args.item);
      }
    };

  }

  function _editAssetClassGroupDialog(item) {
    assetClassGroupId = item.assetClassGroupId;
    document.getElementById("editAssetClassGroupError").style.display = "none";
    document.getElementById("editAssetClassGroupDialog").visible = true;
    document.getElementById("editAssetClassGroupDialog").title = "Edit Asset Class Group : " + item.displayName
    if (document.getElementById('attributesEditGrid').innerHTML != '') {
      clearGrid('attributesEditGrid');
      document.getElementById('attributesEditGrid').style.visibility = "hidden";
    }
    $("#attrMessage").show();
    var dateFilterValue = Date.today();
    dateFilterValue.setDate(1);
    var minDate = Date.parse('1970-01-01');
    var maxDate = Date.parse('2038-01-01');
    var dateFormat = 'yy-mm-dd';
    setupDateFilter($("#assetClassEditDatePicker"), $("#assetClassEditDatePickerError"), dateFilterValue, minDate, maxDate,
      dateFormat);
    _loadExistingAssetClassesById(item);
  }

  function _loadExistingAssetClassesById(item) {
    parameters = {
      'clickedAssetClassGroupId': item.assetClassGroupId.toString(),
      'selectedDate': selectedDate.toString()
    };
    treasury.interest.assetClassActions.loadAssetClassGroupById(parameters);
  }



  function _loadExistingAssetClasses(item) {
    currentAssetClassesGrid = new dportal.grid.createGrid(
      $("#currentAssetClassesGrid"), item.assetClasses,
      treasury.interest.assetClassColumnsConfig
      .getExistingAssetClassColumns(),
      treasury.interest.assetClassGridOptions
      .getAssetClassOptions());
    var rowIds = new Array();
    for (var i in currentAssetClassesGrid.data) {
      rowIds.push(currentAssetClassesGrid.data[i].id);
    }
    currentAssetClassesGrid.setSelectedRowIds(rowIds);

  }

  function _showAssetClassList(item) {
    var parameters = {
      'clickedAssetClassGroupId': item.assetClassGroupId.toString(),
      'selectedDate': selectedDate.toString(),

    };
    treasury.interest.assetClassActions.loadAssetClassGroupByIdToDisplay(parameters, item.displayName);
  }

  function _createGridTables(assetClass) {
    $('#assetClassList').append('<arc-layout type="column" size="1" style="overflow:visible;"><div size = "1">' + assetClass.asset_class_name +
      '</div><div size = "2" style="width: 100%; display: block; height:75px" class = "arc-grid__table" id = ' + assetClass.id + '></div></arc-layout><hr>');
  }

  function _getAssetClassOptions(attrGrid) {
    return {
      autoHorizontalScrollBar: true,
      expandTillLevel: -1,
      highlightRowOnClick: true,
      onDblClick: function(item, rowObject$, isNotToggle) {
        document.getElementById(attrGrid).style.visibility = 'visible';
        _createAttributeTable(item.attributes, attrGrid);
      },
      addCheckboxHeaderAsLastColumn: false,
      rowSelection: [{
        highlightClass: "highlight-strong",
        highlightRowOnClick: false
      }],
      checkboxHeader: [{
        controlChildSelection: false,
        isSelectAllCheckboxRequired: true
      }],
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      exportToExcel: false
    };
  }


  function _loadAgreementsAffected(params) {

    var serviceCall = $.ajax({
      url: "/treasury/financing/get-agreements-for-asset-class-group",
      data: params,
      type: "POST",
      dataType: "json"
    });
    serviceCall.done(function(data) {
      document.getElementById("agreementList").innerHTML = _getTableString(data.resultList);

    });

  }


  function _getTableString(data) {
    var tableString = '<tr><th>Agreements</th></tr>';
    for (var i = 0; i < data.length; i++)
      tableString += "<tr><td>" + data[i].agreementType + "-" + data[i].legalEntity + "-" + data[i].cpe + "</td></tr>";
    return tableString;
  }


  function _createAttributeTable(attributes, divId) {

    var attributeData = new Array();
    for (var attrIndex in attributes) {
      attributes[attrIndex]['id'] = attrIndex;
      attributeData.push(attributes[attrIndex]);
    }

    $('#attrMessage').hide();
    attributesGrid = new dportal.grid.createGrid(
      $("#" + divId), attributeData,
      treasury.interest.assetClassGridOptions
      .getAttributeColumns(),
      treasury.interest.assetClassGridOptions
      .getAttributeGridOptions());
    //resizeCanvasOnGridChange(attributesGrid, '');
  }



  function _getAttributeGridOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      asyncEditorLoading: false,
      editable: true,
      autoEdit: false,
      exportToExcel: false,
    };
  }

  function _getAttributeColumns() {
    return [{
      id: "sourceType",
      type: "text",
      name: "Source Type",
      field: "source_type_id",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 35
    }, {
      id: "attrName",
      type: "text",
      name: "Attribute",
      field: "attr_name",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 45
    }, {
      id: "value",
      type: "text",
      name: "Value",
      field: "value",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 90
    }, {
      id: "underlyingCheck",
      type: "text",
      name: "Underlying Check",
      field: "underlying_check",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 50
    }];
  }
})();
