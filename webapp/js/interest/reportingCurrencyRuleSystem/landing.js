"use strict";
var simulationFilterGroup;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.reportingCurrency = {
    loadSearchResults: _loadSearchResults,
    loadAddDialog: _loadAddDialog,
    registerHandlers: _registerHandlers,
    pageDestructor: _pageDestructor,
    editReportingCurrency: _editReportingCurrency,
  };

  function _registerHandlers() {
    document.getElementById("simulateReportingCurrencyRule").onclick = _loadSimulationResult;
    document.getElementById("saveReportingCurrencyRules").onclick = _saveSelectedRules;
  }

  function _pageDestructor() {
  }

  function _saveSelectedRules() {
    window.treasury.interest.reportingCurrencyAction.saveSelectedRules();
  }

  function _loadSimulationResult() {
    var parameters = simulationFilterGroup.getSerializedParameterMap();
    if(! _validateParameters(parameters))
    {
      return;
    }
    var formattedDate = getFormattedDate('datePickerAdd');
    parameters.dateString = formattedDate;
    treasury.interest.reportingCurrencyAction.loadSimulationResults(parameters);
  }

  function _validateParameters(params)
  {
    if(params['legalEntityFilterIds'] == -1){
      $("#legalEntityFilterAdd").addClass("error");
      return false;
    } else {
      $("#legalEntityFilterAdd").removeClass();
    }
    if(params['reportingCurrencyFilterId'] == -1){
      $("#reportingCurrencyFilterAdd").addClass("error");
      return false;
    } else {
      $("#reportingCurrencyFilterAdd").removeClass();
    }
    if (document.getElementById("datePickerError").innerHTML != "") {
        $("#datePickerAdd").addClass("error");
        return false;
      } else {
        $("#datePickerAdd").removeClass("error");
      }
    return true;
  }

  function _loadSearchResults() {
	  if (document.getElementById("datePickerError").innerHTML != "") {
	      $("#datePickerAdd").addClass("error");
	      return;
	    } else {
	      $("#datePickerAdd").removeClass("error");
	    }

    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    parameters.dateString = formattedDate;

    treasury.interest.reportingCurrencyAction.loadResults(parameters);
  }

  function _loadAddDialog() {
    _clearAddDialogGrids();
    document.getElementById('reportingCurrencyDialog').visible = true;
    _loadAddDialogFilters();
  }

  function _clearAddDialogGrids() {
    document.getElementById("saveReportingCurrencyRules").style.visibility = "hidden";
    document.getElementById('reportingCurrencyExistingRuleLabel').style.visibility = "hidden";
    document.getElementById('reportingCurrencySimulatedRuleLabel').style.visibility = "hidden";
    window.treasury.common.grid.clearGrid("existingReportingCurrencyRuleGrid");
    window.treasury.common.grid.clearGrid("simulatedReportingCurrencyRuleGrid");
    document.getElementById("datePickerError").innerHTML = "";
    $("#datePickerAdd").removeClass("error");
    
    $('#nettingGroupFilterAdd').removeAttr("readOnly");
    $('#cpeEntityFilterAdd').removeAttr("readOnly");
    $('#agreementTypeFilterAdd').removeAttr("readOnly");
    $('#currencyFilterAdd').removeAttr("readOnly");
    $('#legalEntityFilterAdd').removeAttr("readOnly");
  }

  function _setRowValueToFilters(rowData) {
	  simulationFilterGroup.singleSelectFilterArray[0].setSelectedNode(rowData.legal_entity_id);
    simulationFilterGroup.singleSelectFilterArray[1].setSelectedNode(rowData.cpe_id);
    simulationFilterGroup.singleSelectFilterArray[2].setSelectedNode(rowData.agreement_type_id);
    simulationFilterGroup.singleSelectFilterArray[3].setSelectedNode(rowData.currency_id);
    simulationFilterGroup.singleSelectFilterArray[4].setSelectedNode(rowData.netting_group_id);
    simulationFilterGroup.singleSelectFilterArray[5].setSelectedNode(rowData.reporting_currency_id);

    $('#nettingGroupFilterAdd').attr("readOnly","true");
    $('#cpeEntityFilterAdd').attr("readOnly","true");
    $('#agreementTypeFilterAdd').attr("readOnly","true");
    $('#currencyFilterAdd').attr("readOnly","true");
    $('#legalEntityFilterAdd').attr("readOnly","true");

    var dateString = rowData.effective_start_date;
    var dateFilterValue = new Date(dateString.substr(0,4) + "-" + dateString.substr(4,2) + "-" + dateString.substr(6,2));
    var minDate = dateFilterValue;
    var maxDate = Date.parse('2038-01-01');
    var dateFormat = 'yy-mm-dd';
    setupDateFilter($("#datePickerAdd"), $("#datePickerErrorAdd"), dateFilterValue, minDate, maxDate,
      dateFormat);
  }

  function _editReportingCurrency(row) {
    _clearAddDialogGrids();
    var rowData = searchGrid.data[row];
   _loadAddDialogFilters(_setRowValueToFilters.bind(null, rowData));
    document.getElementById('reportingCurrencyDialog').visible = true;
  }

  function _loadAddDialogFilters(callback) {
    showLoading("mainLoader");
    $.when(treasury.loadfilter.legalEntities(),
        treasury.loadfilter.cpes(),
        treasury.loadfilter.loadAgreementTypes(),
        treasury.loadfilter.currencies(),
        treasury.loadfilter.nettingGroups()
      )
      .done(
        function(legalEntitiesData, cpesData, agreementTypesData, currenciesData, nettingGroupsData
          ) {

          var legalEntityFilter = new window.treasury.filter.SingleSelect(
            "legalEntityFilterIds", "legalEntityFilterAdd",
            legalEntitiesData[0].descoEntities, []);

          var cpeEntityFilter = new window.treasury.filter.SingleSelect(
            "cpeFilterIds", "cpeEntityFilterAdd",
            cpesData[0].cpes, []);

          var agreementTypeFilter = new window.treasury.filter.SingleSelect(
            "agreementTypeFilterIds", "agreementTypeFilterAdd",
            agreementTypesData[0].agreementTypes, []);

          var currencyFilter = new window.treasury.filter.SingleSelect(
            "currencyFilterIds", "currencyFilterAdd",
            currenciesData[0].currency, []);

          var nettingGroupFilter = new window.treasury.filter.SingleSelect(
            "nettingGroupFilterIds", "nettingGroupFilterAdd",
            nettingGroupsData[0].nettingGroups, []);

          var reportingCurrencyFilter = new window.treasury.filter.SingleSelect(
            "reportingCurrencyFilterId", "reportingCurrencyFilterAdd",
            currenciesData[0].currency, []);

          setStartOfMonthDateFilter();
          simulationFilterGroup = new window.treasury.filter.FilterGroup(null, [
            legalEntityFilter, cpeEntityFilter,
            agreementTypeFilter, currencyFilter,
            nettingGroupFilter, reportingCurrencyFilter
          ], $("#datePickerAdd"), null);
          if (typeof callback==='function')
            callback();
          hideLoading("mainLoader");
        }
      );
  }

})();
