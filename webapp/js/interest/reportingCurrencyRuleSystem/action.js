"use strict";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.reportingCurrencyAction = {
    loadResults: _loadResults,
    loadSimulationResults: _loadSimulationResults,
    saveSelectedRules: _saveSelectedRules,
  };

  var _simulatedRuleGrid;

  function _saveSelectedRules() {
    treasury.interest.common.actions.saveSelectedRules("/treasury/financing/update-currency-simulated-rules",
      _simulatedRuleGrid, function (){
        document.getElementById('reportingCurrencyDialog').visible = false;
      });
  }

  function _loadSimulationResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/currency-simulate-rules",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    treasury.interest.common.actions.loadSimulationGrids(serviceCall, "existingReportingCurrencyRuleGrid",
      "simulatedReportingCurrencyRuleGrid", "reportingCurrencyExistingRuleLabel",
      "reportingCurrencySimulatedRuleLabel", treasury.interest.reportingCurrencyColumnsConfig
      .getSimulatedColumns, treasury.interest.common.grid.getGridOptions,
      treasury.interest.common.grid.getSimulatedGridOptions,
      function() {
        document.getElementById("saveReportingCurrencyRules").style.visibility = "visible";
      },
      function(gridVar) {
        _simulatedRuleGrid = gridVar;
      })
  }

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-reporting-currency-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.interest.common.actions.loadSearchGrid(serviceCall, "reportingCurrencyGrid",
      treasury.interest.reportingCurrencyColumnsConfig.getColumns,
      treasury.interest.common.grid.getGridOptions);
  }
})();
