"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.reportingCurrencyColumnsConfig = {
    getColumns: _getColumns,
    getActionColumn: _getActionColumn,
    getSimulatedColumns: _getSimulatedColumns
  };

  function _getActionColumn() {
    return [{
      id: "actions",
      type: "text",
      name: "Actions",
      field: "",
      formatter: function(row, cell, value, columnDef,
        dataContext) {
        return '<a href="#" title="Edit"  onclick= event.stopPropagation();treasury.interest.reportingCurrency.editReportingCurrency(' +
          row +
          '); ><i class="icon-edit--block "></i></a>'
      },
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 40
    }];
  }

  function _getCommonColumns() {
    return [{
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreementType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legalEntity",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "counterPartyEntity",
      type: "text",
      name: "Counterparty",
      field: "cpe",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "currency",
      type: "text",
      name: "Currency",
      field: "ccy",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "nettingGroup",
      type: "text",
      name: "Netting Group",
      field: "nettingGroup",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "reportingCurrency",
      type: "text",
      name: "Reporting Currency",
      field: "reportingCurrency",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "validFrom",
      type: "text",
      name: "Valid From",
      field: "effective_start_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "validTill",
      type: "text",
      name: "Valid Till",
      field: "effective_end_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }];
  }


  function _getColumns() {
    var columns = _getActionColumn();
    columns = columns.concat(_getCommonColumns());
    return columns;
  }

  function _getSimulatedColumns() {
    var columns = _getCommonColumns();
    return columns;
  }
})();
