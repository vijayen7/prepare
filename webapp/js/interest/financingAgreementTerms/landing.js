"use strict";
var simulationFilterGroup;
var nettingGroupFilter;
var baseRateEditFilter;
var selectedRow;

var actionType;
var agreementTermsShovelUrl = "/shovel/template/Agreement Terms";
(function() {
  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.financingAgreementTerms = {
    loadSearchResults: _loadSearchResults,
    loadAddDialog: _loadAddDialog,
    loadFullFinancingAgreementTerms: _loadFullFinancingAgreementTerms,
    registerHandlers: _registerHandlers,
    pageDestructor: _pageDestructor,
    editAgreementTerm: _editAgreementTerm,
    deleteAgreementTerm: _deleteAgreementTerm
  };

  function _editAgreementTerm(row) {
    _loadEditDialog(row);
  }

  function _editDialogDestruct() {
    document.getElementById("nettingGroupFilterAdd").readonly = false;
    document.getElementById("agreementTermsBaseRateInput").readonly = false;
    document.getElementById(
      "agreementTermsBaseRateCurrencyFilterAdd"
    ).readonly = false;
    document.getElementById("agreementFilterAdd").readonly = false;
    document.getElementById("agreementTermsCurrencyFilterAdd").readonly = false;
    document.getElementById(
      "agreementTermsAssetClassGroupFilterAdd"
    ).readonly = false;
    document.getElementById("bandwidthGroupFilterAdd").readonly = false;
    document.getElementById(
      "agreementTermsAccConventionInput"
    ).readonly = false;

    document.getElementById("agreementTermsCreditSpreadInput").value = "";
    document.getElementById("agreementTermsCreditSpreadInput").readOnly = false;

    document.getElementById("agreementTermsDebitSpreadInput").value = "";
    document.getElementById("agreementTermsDebitSpreadInput").readOnly = false;

    document.getElementById("agreementTermsGCSpreadInput").value = "";
    document.getElementById("agreementTermsGCSpreadInput").readOnly = false;
    document.getElementById(
      "agreementTermsCreditIRateFilterAdd"
    ).readonly = false;
    document.getElementById(
      "agreementTermsDebitIRateFilterAdd"
    ).readonly = false;
    document.getElementById(
      "agreementTermsFinancingStyleFilterAdd"
    ).readonly = false;
    document.getElementById("agreementTermsGCShortSpreadInput").value = "";
    document.getElementById(
      "agreementTermsGCShortSpreadInput"
    ).readOnly = false;
    document.getElementById("agreementTermsSmvInput").value = "";
    document.getElementById("agreementTermsSmvInput").readOnly = false;
    document.getElementById("agreementTermsHaircutInput").value = "";
    document.getElementById("agreementTermsHaircutInput").readOnly = false;
    document.getElementById("SficIRateCheckbox").checked = false;
    document.getElementById("agreementTermsComment").value = "";

    document.getElementById("baseRateToggle").checked = false;
    _toggleBaseRateFilters();

    document.getElementById("accToggle").checked = false;
    _toggleAccConventionFilters();
    $("#SficIRateCheckbox").removeAttr("disabled");
  }

  function _loadEditDialog(row) {
    document.getElementById('effectiveDate').innerHTML = 'Effective From';
    _clearAddDialogGrids();
    document.getElementById("financingAgreementTermsDialog").visible = true;
    _editDialogDestruct();
    document.getElementById("deleteTermButton").style.visibility = "hidden";
    document.getElementById(
        "simulateFinancingAgreementTermEdit"
      ).style.visibility =
      "visible";
    _loadEditDialogFilters(row);
  }

  function _loadEditDialogFilters(id) {
    selectedRow = id;
    actionType = "disable";
    var toast = raiseInfoToast("Initializing filters, please wait...");
    var rowData = searchGrid.dataView.getItemById(id);
    $.when(
      treasury.loadfilter.agreementIds(),
      treasury.loadfilter.nettingGroups(),
      treasury.loadfilter.currencies(),
      treasury.loadfilter.assetClassGroups(),
      treasury.loadfilter.bandwidthGroups(),
      treasury.loadfilter.accrualConventions(),
      treasury.loadfilter.baseRates(rowData.financingTerm.baseRateCurrency),
      treasury.loadfilter.loadApplyNegativeRate(),
      treasury.loadfilter.allFinancingStyles(),
      treasury.loadfilter.agmtTermsAttributes()
    ).done(function(
      agreementData,
      nettingGroupsData,
      currenciesData,
      assetClassGroupsData,
      bandwidthGroupsData,
      accrualConventionsData,
      baseRatesData,
      applyNegativeRateData,
      financingStylesData,
      financingTermAttributesData
    ) {
      var selectedColumnData = new Array();

      nettingGroupFilter = new window.treasury.filter.SingleSelect(
        "nettingGroupFilterIds",
        "nettingGroupFilterAdd",
        nettingGroupsData[0].nettingGroups, [rowData.financingMethodology.nettingGroup.myNettingGroupId]
      );
      document.getElementById("nettingGroupFilterAdd").readonly = true;

      if (baseRatesData[0]["baseRates"].length == 0) {
        baseRatesData[0]["baseRates"].push({
          0: 0,
          1: "0 - 0 - 0"
        });
      }

      baseRateEditFilter = new window.treasury.filter.SingleSelect(
        "baseRateFilterIds",
        "agreementTermsBaseRateInput",
        baseRatesData[0]["baseRates"], [rowData.financingTerm.creditBaseRateTsId]
      );

      var creditBaseRateEditFilter = new window.treasury.filter.SingleSelect(
        "creditBaseRateFilterIds",
        "agreementTermsCreditBaseRateInput",
        baseRatesData[0]["baseRates"], [rowData.financingTerm.creditBaseRateTsId]
      );

      var debitBaseRateEditFilter = new window.treasury.filter.SingleSelect(
        "debitBaseRateFilterIds",
        "agreementTermsDebitBaseRateInput",
        baseRatesData[0]["baseRates"], [rowData.financingTerm.debitBaseRateTsId]
      );

      var rebateBaseRateEditFilter = new window.treasury.filter.SingleSelect(
        "rebateBaseRateFilterIds",
        "agreementTermsRebateBaseRateInput",
        baseRatesData[0]["baseRates"], [rowData.financingTerm.rebateBaseRateTsId]
      );

      if (
        rowData.financingTerm.creditBaseRateTsId !=
        rowData.financingTerm.debitBaseRateTsId ||
        rowData.financingTerm.debitBaseRateTsId !=
        rowData.financingTerm.rebateBaseRateTsId ||
        rowData.financingTerm.rebateBaseRateTsId !=
        rowData.financingTerm.creditBaseRateTsId
      ) {
        document.getElementById("baseRateToggle").checked = true;
        _toggleBaseRateFilters();
      }
      if (rowData.financingTerm.baseRateCurrency != null)
        selectedColumnData.push(rowData.financingTerm.baseRateCurrency);
      var baseRateCurrencyFilter = new window.treasury.filter.SingleSelect(
        "baseRateCurrencyFilterIds",
        "agreementTermsBaseRateCurrencyFilterAdd",
        currenciesData[0].currency,
        selectedColumnData
      );

      var agreementFilter = new window.treasury.filter.SingleSelect(
        "agreementFilterIds",
        "agreementFilterAdd",
        agreementData[0].agreementIds, [rowData.financingMethodology.agreementId]
      );
      document.getElementById("agreementFilterAdd").readonly = true;

      currenciesData[0]["currency"].unshift([0, "Select"]);

      var currencyFilter = new window.treasury.filter.SingleSelect(
        "currencyFilterIds",
        "agreementTermsCurrencyFilterAdd",
        currenciesData[0].currency, [rowData.financingMethodology.currency.mySpn]
      );
      document.getElementById(
        "agreementTermsCurrencyFilterAdd"
      ).readonly = true;

      selectedColumnData = [];
      if (rowData.assetClassGroup != null)
        selectedColumnData.push(rowData.assetClassGroup.assetClassGroupId);

      var assetClassGroupFilter = new window.treasury.filter.SingleSelect(
        "assetClassGroupFilterId",
        "agreementTermsAssetClassGroupFilterAdd",
        assetClassGroupsData[0].assetClassGroups,
        selectedColumnData
      );
      document.getElementById(
        "agreementTermsAssetClassGroupFilterAdd"
      ).readonly = true;

      selectedColumnData = [];

      if (rowData.bandwidthGroup != null)
        selectedColumnData.push(rowData.bandwidthGroup.bandwidthGroupId);
      var bandwidthGroupFilter = new window.treasury.filter.SingleSelect(
        "bandwidthGroupFilterId",
        "bandwidthGroupFilterAdd",
        bandwidthGroupsData[0].bandwidthGroup,
        selectedColumnData
      );

      document.getElementById("bandwidthGroupFilterAdd").readonly = true;

      var accrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "accrualConventionInput",
        "agreementTermsAccConventionInput",
        accrualConventionsData[0].accrualConvention, [rowData.financingTerm.creditAccrualConvention]
      );
      var creditAccrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "creditAccrualConvention",
        "agreementTermsCreditAccConventionInput",
        accrualConventionsData[0].accrualConvention, [rowData.financingTerm.creditAccrualConvention]
      );
      var debitAccrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "debitAccrualConvention",
        "agreementTermsDebitAccConventionInput",
        accrualConventionsData[0].accrualConvention, [rowData.financingTerm.debitAccrualConvention]
      );

      if (
        rowData.financingTerm.debitAccrualConvention !=
        rowData.financingTerm.creditAccrualConvention
      ) {
        document.getElementById("accToggle").checked = true;
        _toggleAccConventionFilters();
      }
      document.getElementById("agreementTermsCreditSpreadInput").value =
        rowData.financingTerm.creditSpread * 100;
      document.getElementById("agreementTermsDebitSpreadInput").value =
        rowData.financingTerm.debitSpread * 100;
      document.getElementById("agreementTermsGCSpreadInput").value =
        rowData.financingTerm.gcRate * 100;
      //hardcoding attribute array ids for now
      if (
        rowData.financingTerm.attributeList[0] != undefined &&
        rowData.financingTerm.attributeList[0].ivalue != null
      )
        selectedColumnData.push(rowData.financingTerm.attributeList[0].ivalue);

      var creditIRateFilter = new window.treasury.filter.SingleSelect(
        "creditIRateFilterId",
        "agreementTermsCreditIRateFilterAdd",
        applyNegativeRateData[0].applyNegativeIrates,
        selectedColumnData
      );

      selectedColumnData = [];
      if (
        rowData.financingTerm.attributeList[2] != undefined &&
        rowData.financingTerm.attributeList[2].ivalue != null
      )
        selectedColumnData.push(rowData.financingTerm.attributeList[2].ivalue);
      var debitIRateFilter = new window.treasury.filter.SingleSelect(
        "debitIRateFilterId",
        "agreementTermsDebitIRateFilterAdd",
        applyNegativeRateData[0].applyNegativeIrates,
        selectedColumnData
      );

      selectedColumnData = [];
      if (
        rowData.financingTerm.attributeList[5] != undefined &&
        rowData.financingTerm.attributeList[5].ivalue != null
      )
        selectedColumnData.push(rowData.financingTerm.attributeList[5].ivalue);
      var financingStyleFilter = new window.treasury.filter.SingleSelect(
        "financingStyleFilterId",
        "agreementTermsFinancingStyleFilterAdd",
        financingStylesData[0].financingStyles,
        selectedColumnData
      );

      if (
        rowData.financingTerm.attributeList[6] != undefined &&
        rowData.financingTerm.attributeList[6].rvalue != null
      )
        document.getElementById("agreementTermsGCShortSpreadInput").value =
        rowData.financingTerm.attributeList[6].rvalue * 100;

      if (
        rowData.financingTerm.attributeList[1] != undefined &&
        rowData.financingTerm.attributeList[1].rvalue != null
      )
        document.getElementById("agreementTermsSmvInput").value =
        rowData.financingTerm.attributeList[1].rvalue;

      if (
        rowData.financingTerm.attributeList[4] != undefined &&
        rowData.financingTerm.attributeList[4].rvalue != null
      )
        document.getElementById("agreementTermsHaircutInput").value =
        rowData.financingTerm.attributeList[4].rvalue;

      if (
        rowData.financingTerm.attributeList[3] != undefined &&
        rowData.financingTerm.attributeList[3].ivalue == 1
      )
        document.getElementById("SficIRateCheckbox").checked = true;

      setFilterDateToMonthStart("datePickerAdd", "datePickerErrorAdd");
      simulationFilterGroup = new window.treasury.filter.FilterGroup(
        null, [
          agreementFilter,
          currencyFilter,
          baseRateCurrencyFilter,
          assetClassGroupFilter,
          bandwidthGroupFilter,
          accrualConventionEditSelect,
          creditAccrualConventionEditSelect,
          debitAccrualConventionEditSelect,
          financingStyleFilter,
          debitIRateFilter,
          creditIRateFilter,
          baseRateEditFilter,
          creditBaseRateEditFilter,
          debitBaseRateEditFilter,
          rebateBaseRateEditFilter,
          nettingGroupFilter
        ],
        $("#datePickerAdd"),
        null
      );
      actionType = "enable";
      toast.remove();
    });
  }

  function _deleteAgreementTerm(id) {
    _clearAddDialogGrids();
    selectedRow = id;
    actionType = "disable";
    document.getElementById("deleteTermButton").style.visibility = "visible";
    document.getElementById(
        "simulateFinancingAgreementTermEdit"
      ).style.visibility =
      "hidden";
    document.getElementById("saveFinancingAgreementTerms").style.visibility =
      "hidden";
    document.getElementById("financingAgreementTermsDialog").visible = true;
    _editDialogDestruct();
    var toast = raiseInfoToast("Initializing filters, please wait...");
    var rowData = searchGrid.dataView.getItemById(id);
    $.when(
      treasury.loadfilter.agreementIds(),
      treasury.loadfilter.nettingGroups(),
      treasury.loadfilter.currencies(),
      treasury.loadfilter.assetClassGroups(),
      treasury.loadfilter.bandwidthGroups(),
      treasury.loadfilter.accrualConventions(),
      treasury.loadfilter.baseRates(rowData.financingTerm.baseRateCurrency),
      treasury.loadfilter.loadApplyNegativeRate(),
      treasury.loadfilter.allFinancingStyles(),
      treasury.loadfilter.agmtTermsAttributes()
    ).done(function(
      agreementData,
      nettingGroupsData,
      currenciesData,
      assetClassGroupsData,
      bandwidthGroupsData,
      accrualConventionsData,
      baseRatesData,
      applyNegativeRateData,
      financingStylesData,
      financingTermAttributesData
    ) {
      var selectedColumnData = new Array();
      nettingGroupFilter = new window.treasury.filter.SingleSelect(
        "nettingGroupFilterIds",
        "nettingGroupFilterAdd",
        nettingGroupsData[0].nettingGroups, [rowData.financingMethodology.nettingGroup.myNettingGroupId]
      );
      document.getElementById("nettingGroupFilterAdd").readonly = true;

      if (baseRatesData[0]["baseRates"].length == 0) {
        baseRatesData[0]["baseRates"].push({
          0: 0,
          1: "0 - 0 - 0"
        });
      }

      baseRateEditFilter = new window.treasury.filter.SingleSelect(
        "baseRateFilterIds",
        "agreementTermsBaseRateInput",
        baseRatesData[0]["baseRates"], [rowData.financingTerm.creditBaseRateTsId]
      );

      document.getElementById("agreementTermsBaseRateInput").readonly = true;

      if (rowData.financingTerm.baseRateCurrency != null)
        selectedColumnData.push(rowData.financingTerm.baseRateCurrency);
      var baseRateCurrencyFilter = new window.treasury.filter.SingleSelect(
        "baseRateCurrencyFilterIds",
        "agreementTermsBaseRateCurrencyFilterAdd",
        currenciesData[0].currency,
        selectedColumnData
      );

      document.getElementById(
        "agreementTermsBaseRateCurrencyFilterAdd"
      ).readonly = true;

      var agreementFilter = new window.treasury.filter.SingleSelect(
        "agreementFilterIds",
        "agreementFilterAdd",
        agreementData[0].agreementIds, [rowData.financingMethodology.agreementId]
      );
      document.getElementById("agreementFilterAdd").readonly = true;

      currenciesData[0]["currency"].unshift([0, "Select"]);

      var currencyFilter = new window.treasury.filter.SingleSelect(
        "currencyFilterIds",
        "agreementTermsCurrencyFilterAdd",
        currenciesData[0].currency, [rowData.financingMethodology.currency.mySpn]
      );
      document.getElementById(
        "agreementTermsCurrencyFilterAdd"
      ).readonly = true;

      selectedColumnData = [];
      if (rowData.assetClassGroup != null)
        selectedColumnData.push(rowData.assetClassGroup.assetClassGroupId);

      var assetClassGroupFilter = new window.treasury.filter.SingleSelect(
        "assetClassGroupFilterId",
        "agreementTermsAssetClassGroupFilterAdd",
        assetClassGroupsData[0].assetClassGroups,
        selectedColumnData
      );
      document.getElementById(
        "agreementTermsAssetClassGroupFilterAdd"
      ).readonly = true;

      selectedColumnData = [];

      if (rowData.bandwidthGroup != null)
        selectedColumnData.push(rowData.bandwidthGroup.bandwidthGroupId);
      var bandwidthGroupFilter = new window.treasury.filter.SingleSelect(
        "bandwidthGroupFilterId",
        "bandwidthGroupFilterAdd",
        bandwidthGroupsData[0].bandwidthGroup,
        selectedColumnData
      );

      document.getElementById("bandwidthGroupFilterAdd").readonly = true;

      var accrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "accrualConventionInput",
        "agreementTermsAccConventionInput",
        accrualConventionsData[0].accrualConvention, [rowData.financingTerm.creditAccrualConvention]
      );
      var creditAccrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "creditAccrualConvention",
        "agreementTermsCreditAccConventionInput",
        accrualConventionsData[0].accrualConvention, []
      );
      var debitAccrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "debitAccrualConvention",
        "agreementTermsDebitAccConventionInput",
        accrualConventionsData[0].accrualConvention, []
      );
      document.getElementById(
        "agreementTermsAccConventionInput"
      ).readonly = true;

      document.getElementById("agreementTermsCreditSpreadInput").value =
        rowData.financingTerm.creditSpread * 100;
      document.getElementById(
        "agreementTermsCreditSpreadInput"
      ).readOnly = true;

      document.getElementById("agreementTermsDebitSpreadInput").value =
        rowData.financingTerm.debitSpread * 100;
      document.getElementById("agreementTermsDebitSpreadInput").readOnly = true;

      document.getElementById("agreementTermsGCSpreadInput").value =
        rowData.financingTerm.gcRate * 100;
      document.getElementById("agreementTermsGCSpreadInput").readOnly = true;

      //hardcoding attribute array ids for now
      if (
        rowData.financingTerm.attributeList[0] != undefined &&
        rowData.financingTerm.attributeList[0].ivalue != null
      )
        selectedColumnData.push(rowData.financingTerm.attributeList[0].ivalue);

      var creditIRateFilter = new window.treasury.filter.SingleSelect(
        "creditIRateFilterId",
        "agreementTermsCreditIRateFilterAdd",
        applyNegativeRateData[0].applyNegativeIrates,
        selectedColumnData
      );

      document.getElementById(
        "agreementTermsCreditIRateFilterAdd"
      ).readonly = true;

      selectedColumnData = [];
      if (
        rowData.financingTerm.attributeList[2] != undefined &&
        rowData.financingTerm.attributeList[2].ivalue != null
      )
        selectedColumnData.push(rowData.financingTerm.attributeList[2].ivalue);
      var debitIRateFilter = new window.treasury.filter.SingleSelect(
        "debitIRateFilterId",
        "agreementTermsDebitIRateFilterAdd",
        applyNegativeRateData[0].applyNegativeIrates,
        selectedColumnData
      );

      document.getElementById(
        "agreementTermsDebitIRateFilterAdd"
      ).readonly = true;

      selectedColumnData = [];
      if (
        rowData.financingTerm.attributeList[5] != undefined &&
        rowData.financingTerm.attributeList[5].ivalue != null
      )
        selectedColumnData.push(rowData.financingTerm.attributeList[5].ivalue);
      var financingStyleFilter = new window.treasury.filter.SingleSelect(
        "financingStyleFilterId",
        "agreementTermsFinancingStyleFilterAdd",
        financingStylesData[0].financingStyles,
        selectedColumnData
      );

      document.getElementById(
        "agreementTermsFinancingStyleFilterAdd"
      ).readonly = true;

      if (
        rowData.financingTerm.attributeList[6] != undefined &&
        rowData.financingTerm.attributeList[6].rvalue != null
      )
        document.getElementById("agreementTermsGCShortSpreadInput").value =
        rowData.financingTerm.attributeList[6].rvalue * 100;
      document.getElementById(
        "agreementTermsGCShortSpreadInput"
      ).readOnly = true;

      if (
        rowData.financingTerm.attributeList[1] != undefined &&
        rowData.financingTerm.attributeList[1].rvalue != null
      )
        document.getElementById("agreementTermsSmvInput").value =
        rowData.financingTerm.attributeList[1].rvalue;
      document.getElementById("agreementTermsSmvInput").readOnly = true;

      if (
        rowData.financingTerm.attributeList[4] != undefined &&
        rowData.financingTerm.attributeList[4].rvalue != null
      )
        document.getElementById("agreementTermsHaircutInput").value =
        rowData.financingTerm.attributeList[4].rvalue;
      document.getElementById("agreementTermsHaircutInput").readOnly = true;

      document.getElementById('effectiveDate').innerHTML = 'Effective Till';

      if (
        rowData.financingTerm.attributeList[3] != undefined &&
        rowData.financingTerm.attributeList[3].ivalue == 1
      )
        document.getElementById("SficIRateCheckbox").checked = true;
      document.getElementById("SficIRateCheckbox").disabled = "disabled";

      setFilterDateToMonthStart("datePickerAdd", "datePickerErrorAdd");
      actionType = "enable";
      toast.remove();
    });
  }

  function _deleteSelectedTerm() {
    var rowData = searchGrid.dataView.getItemById(selectedRow);
    rowData.validStart = null;
    var comment = document.getElementById("agreementTermsComment").value;
    var formattedDate = getFormattedDate("datePickerAdd");
    var parameters = {
      comment: comment,
      dateString: formattedDate,
      rulesToSave: "[" + JSON.stringify(rowData) + "]"
    };

    treasury.interest.financingAgreementTermsAction.deleteAgreementTerm(
      parameters
    );
  }

  function _registerHandlers() {
    _showAdditionalOptionButtons();
    document
      .getElementById("agreementTermsBaseRateCurrencyFilterAdd")
      .addEventListener("change", _loadBaseRatesOnCurrency);
    document
      .getElementById("agreementFilterAdd")
      .addEventListener("optionSelected", _loadNettingGroupOnAgreement);
    document.getElementById(
      "simulateFinancingAgreementTermEdit"
    ).onclick = _loadSimulationResult;
    document.getElementById("deleteTermButton").onclick = _deleteSelectedTerm;
    document.getElementById("saveFinancingAgreementTerms").onclick =
      window.treasury.interest.financingAgreementTermsAction.saveSelectedTerms;
    document.getElementById("addBandwidthGroupButton").onclick = function() {
      _loadAddBandwidthGroupDialog();
    };
    document.getElementById("bulkInsertAgreementTermsButton").onclick = function() {
      window.open(agreementTermsShovelUrl);
    };
    document.getElementById("submitBandwidthGroup").onclick =
      treasury.interest.bandwidthGroupActions.saveBandwidthGroup;
    document.getElementById("accToggle").onclick = _toggleAccConventionFilters;
    document.getElementById("baseRateToggle").onclick = _toggleBaseRateFilters;
  }

  function _toggleAccConventionFilters() {
    var list = document.getElementsByClassName("accConventionBlock");
    var displayProperty;
    if (accToggle.checked) {
      displayProperty = "block";
      document.getElementsByClassName("accConvention")[0].style.visibility =
        "hidden";
    } else {
      displayProperty = "none";
      document.getElementsByClassName("accConvention")[0].style.visibility =
        "visible";
    }

    for (var i = 0; i < list.length; ++i) {
      list[i].style.display = displayProperty;
    }

    $("#agreementTermsAccConventionInput").removeClass();
    $("#agreementTermsCreditAccConventionInput").removeClass();
  }

  function _toggleBaseRateFilters() {
    var list = document.getElementsByClassName("baseRateBlock");
    var displayProperty;
    if (baseRateToggle.checked) {
      displayProperty = "block";
      document.getElementsByClassName("baseRate")[0].style.visibility =
        "hidden";
    } else {
      displayProperty = "none";
      document.getElementsByClassName("baseRate")[0].style.visibility =
        "visible";
    }
    for (var i = 0; i < list.length; ++i) {
      list[i].style.display = displayProperty;
    }
  }

  function _addAttributes(event) {
    if (event.type == "add") {
      document.getElementById("attributeList").innerHTML +=
        "<div id='attr" +
        event.items[0].id +
        "'><label>" +
        event.items[0].name +
        "</label><input style = 'float:right;display:inline' type='text' /></div>";
    } else if (event.type == "remove") {
      document.getElementById("attr" + event.item.id).remove();
    } else {}
  }

  function _pageDestructor() {
    document.getElementById("addBandwidthGroupButton").style.display = "none";
    document.getElementById("bulkInsertAgreementTermsButton").style.display = "none";
  }

  function _showAdditionalOptionButtons() {
    document.getElementById("addBandwidthGroupButton").style.display = "block";
    document.getElementById("bulkInsertAgreementTermsButton").style.display = "block";
  }

  function _loadSearchResults() {

    if (document.getElementById("datePickerError").innerHTML != "") {
      raiseErrorToast("Please select a valid Date");
      return;
    }

    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate("datePicker");
    parameters.dateString = formattedDate;

    treasury.interest.financingAgreementTermsAction.loadResults(parameters);
  }

  function _validateParameters(parameters) {
    if (parameters["agreementFilterIds"] == -1) {
      $("#agreementFilterAdd").addClass("error");
      return false;
    } else {
      $("#agreementFilterAdd").removeClass();
    }

    if (parameters["nettingGroupFilterIds"] == -1) {
      $("#nettingGroupFilterAdd").addClass("error");
      return false;
    } else {
      $("#nettingGroupFilterAdd").removeClass();
    }

    if (parameters["currencyFilterIds"] == -1) {
      $("#agreementTermsCurrencyFilterAdd").addClass("error");
      return false;
    } else {
      $("#agreementTermsCurrencyFilterAdd").removeClass();
    }

    if (document.getElementById("accToggle").checked == false) {
      if (parameters["accrualConventionInput"] == -1) {
        $("#agreementTermsAccConventionInput").addClass("error");
        return false;
      } else {
        $("#agreementTermsAccConventionInput").removeClass();
      }
    } else {
      if (parameters["creditAccrualConvention"] == -1) {
        $("#agreementTermsCreditAccConventionInput").addClass("error");
        return false;
      } else {
        $("#agreementTermsCreditAccConventionInput").removeClass();
      }
      if (parameters["debitAccrualConvention"] == -1) {
        $("#agreementTermsDebitAccConventionInput").addClass("error");
        return false;
      } else {
        $("#agreementTermsDebitAccConventionInput").removeClass();
      }
    }
    if (parameters["baseRateCurrencyFilterIds"] == -1) {
      $("#agreementTermsBaseRateCurrencyFilterAdd").addClass("error");
      return false;
    } else {
      $("#agreementTermsBaseRateCurrencyFilterAdd").removeClass();
    }

    if (document.getElementById("baseRateToggle").checked == false) {
      if (parameters["baseRateFilterIds"] == -1) {
        $("#agreementTermsBaseRateInput").addClass("error");
        return false;
      } else {
        $("#agreementTermsBaseRateInput").removeClass();
      }
    } else {
      if (parameters["creditBaseRateFilterIds"] == -1) {
        $("#agreementTermsCreditBaseRateInput").addClass("error");
        return false;
      } else {
        $("#agreementTermsCreditBaseRateInput").removeClass();
      }
      if (parameters["debitBaseRateFilterIds"] == -1) {
        $("#agreementTermsDebitBaseRateInput").addClass("error");
        return false;
      } else {
        $("#agreementTermsDebitBaseRateInput").removeClass();
      }
    }
    var agreementType = document
      .getElementById("agreementFilterAdd")
      .value.value.split("-")[2];
    if (agreementType === "PB") {
      if (document.getElementById("agreementTermsGCSpreadInput").value == "") {
        $("#agreementTermsGCSpreadInput").addClass("error");
        return false;
      } else {
        $("#agreementTermsGCSpreadInput").removeClass();
      }
    } else {
      $("#agreementTermsGCSpreadInput").removeClass();
    }

    return true;
  }

  function _loadSimulationResult() {
    var parameters = simulationFilterGroup.getSerializedParameterMap();
    if (!_validateParameters(parameters)) {;
      return;
    }

    document.getElementById("agreementTermsCreditSpreadInput").value == "" ?
      parameters["creditSpread"] = 0 :
      parameters["creditSpread"] = -Math.abs(
        document.getElementById("agreementTermsCreditSpreadInput").value / 100
      );

    document.getElementById("agreementTermsDebitSpreadInput").value == "" ?
      parameters["debitSpread"] = 0 :
      parameters["debitSpread"] = document.getElementById("agreementTermsDebitSpreadInput").value / 100;

    parameters["gcRate"] = document.getElementById(
      "agreementTermsGCSpreadInput"
    ).value / 100;

    if (document.getElementById("accToggle").checked == false) {
      parameters["creditAccrualConvention"] =
        parameters["accrualConventionInput"];
      parameters["debitAccrualConvention"] =
        parameters["accrualConventionInput"];
    }

    if (document.getElementById("baseRateToggle").checked == false) {
      if (
        parameters["baseRateFilterIds"] == undefined ||
        parameters["baseRateFilterIds"] == null
      ) {
        parameters["baseRateFilterIds"] = "0-0-0";
      }

      parameters["creditBaseRateFilterIds"] = parameters["baseRateFilterIds"];
      parameters["debitBaseRateFilterIds"] = parameters["baseRateFilterIds"];
      parameters["rebateBaseRateFilterIds"] = parameters["baseRateFilterIds"];
    } else {
      if (
        parameters["creditBaseRateFilterIds"] == undefined ||
        parameters["creditBaseRateFilterIds"] == null
      ) {
        parameters["creditBaseRateFilterIds"] = "0-0-0";
      }
      if (
        parameters["debitBaseRateFilterIds"] == undefined ||
        parameters["debitBaseRateFilterIds"] == null
      ) {
        parameters["debitBaseRateFilterIds"] = "0-0-0";
      }
      if (
        parameters["rebateBaseRateFilterIds"] == undefined ||
        parameters["rebateBaseRateFilterIds"] == null
      ) {
        parameters["rebateBaseRateFilterIds"] = "0-0-0";
      }
    }

    parameters["ApplicableNegativeCreditIrate"] =
      parameters["creditIRateFilterId"];

    parameters["ApplicableNegativeDebitIrate"] =
      parameters["debitIRateFilterId"];

    var sficValue = 0;
    if (document.getElementById("SficIRateCheckbox").checked == true)
      sficValue = 1;
    parameters["ApplicableNegativeIrateSfic"] = sficValue;

    if (parameters["financingStyleFilterId"] == -1)
      parameters["FinancingStyleId"] = "null";
    else parameters["FinancingStyleId"] = parameters["financingStyleFilterId"];

    if (document.getElementById("agreementTermsGCShortSpreadInput").value == "")
      parameters["GcShortSpread"] = "null";
    else
      parameters["GcShortSpread"] = document.getElementById(
        "agreementTermsGCShortSpreadInput"
      ).value / 100;

    if (document.getElementById("agreementTermsHaircutInput").value == "")
      parameters["Haircut"] = "null";
    else
      parameters["Haircut"] = document.getElementById(
        "agreementTermsHaircutInput"
      ).value;

    if (document.getElementById("agreementTermsSmvInput").value == "")
      parameters["SmvConversionFactor"] = "null";
    else
      parameters["SmvConversionFactor"] = document.getElementById(
        "agreementTermsSmvInput"
      ).value;

    parameters["comment"] = document.getElementById(
      "agreementTermsComment"
    ).value;

    var formattedDate = getFormattedDate("datePickerAdd");
    parameters.validFrom = formattedDate;
    treasury.interest.financingAgreementTermsAction.loadSimulationResults(
      parameters
    );
  }

  function _loadAddDialog() {
    document.getElementById('effectiveDate').innerHTML = 'Effective From';
    _clearAddDialogGrids();
    document.getElementById("financingAgreementTermsDialog").visible = true;
    _editDialogDestruct();
    document.getElementById("deleteTermButton").style.visibility = "hidden";
    document.getElementById(
        "simulateFinancingAgreementTermEdit"
      ).style.visibility =
      "visible";
    _loadAddDialogFilters();
  }

  function _loadAddBandwidthGroupDialog() {
    _clearAddDialogGrids();
    document.getElementById("addBandwidthGroupDialog").visible = true;
  }

  function _loadAssetClassGroupDialog() {
    _clearAddDialogGrids();
    document.getElementById("addAssetClassGroupDialog").visible = true;
  }

  function _clearAddDialogGrids() {
    document.getElementById("saveFinancingAgreementTerms").style.visibility =
      "hidden";
    document.getElementById(
        "financingAgreementTermExistingLabel"
      ).style.visibility =
      "hidden";
    document.getElementById(
        "financingAgreementTermSimulatedLabel"
      ).style.visibility =
      "hidden";
    window.treasury.common.grid.clearGrid("financingAgreementTermExistingGrid");
    window.treasury.common.grid.clearGrid(
      "financingAgreementTermSimulatedGrid"
    );
  }

  function _loadFullFinancingAgreementTerms() {
    var toast = raiseInfoToast("Fetching Term Details");
  }

  function _loadAddDialogFilters() {
    actionType = "enable";
    var toast = raiseInfoToast("Initializing filters, please wait...");
    $.when(
      treasury.loadfilter.agreementIds(),
      treasury.loadfilter.currencies(),
      treasury.loadfilter.assetClassGroups(),
      treasury.loadfilter.bandwidthGroups(),
      treasury.loadfilter.accrualConventions(),
      treasury.loadfilter.loadApplyNegativeRate(),
      treasury.loadfilter.allFinancingStyles(),
      treasury.loadfilter.agmtTermsAttributes()
    ).done(function(
      agreementData,
      currenciesData,
      assetClassGroupsData,
      bandwidthGroupsData,
      accrualConventionsData,
      applyNegativeRateData,
      financingStylesData,
      financingTermAttributesData
    ) {
      nettingGroupFilter = null;
      baseRateEditFilter = null;
      var agreementFilter = new window.treasury.filter.SingleSelect(
        "agreementFilterIds",
        "agreementFilterAdd",
        agreementData[0].agreementIds, []
      );

      var currencyFilter = new window.treasury.filter.SingleSelect(
        "currencyFilterIds",
        "agreementTermsCurrencyFilterAdd",
        currenciesData[0].currency, []
      );

      var baseRateCurrencyFilter = new window.treasury.filter.SingleSelect(
        "baseRateCurrencyFilterIds",
        "agreementTermsBaseRateCurrencyFilterAdd",
        currenciesData[0].currency, []
      );

      var assetClassGroupFilter = new window.treasury.filter.SingleSelect(
        "assetClassGroupFilterId",
        "agreementTermsAssetClassGroupFilterAdd",
        assetClassGroupsData[0].assetClassGroups, []
      );

      var bandwidthGroupFilter = new window.treasury.filter.SingleSelect(
        "bandwidthGroupFilterId",
        "bandwidthGroupFilterAdd",
        bandwidthGroupsData[0].bandwidthGroup, []
      );

      var accrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "accrualConventionInput",
        "agreementTermsAccConventionInput",
        accrualConventionsData[0].accrualConvention, []
      );
      var creditAccrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "creditAccrualConvention",
        "agreementTermsCreditAccConventionInput",
        accrualConventionsData[0].accrualConvention, []
      );
      var debitAccrualConventionEditSelect = new window.treasury.filter.SingleSelect(
        "debitAccrualConvention",
        "agreementTermsDebitAccConventionInput",
        accrualConventionsData[0].accrualConvention, []
      );

      var creditIRateFilter = new window.treasury.filter.SingleSelect(
        "creditIRateFilterId",
        "agreementTermsCreditIRateFilterAdd",
        applyNegativeRateData[0].applyNegativeIrates, []
      );

      var debitIRateFilter = new window.treasury.filter.SingleSelect(
        "debitIRateFilterId",
        "agreementTermsDebitIRateFilterAdd",
        applyNegativeRateData[0].applyNegativeIrates, []
      );

      var financingStyleFilter = new window.treasury.filter.SingleSelect(
        "financingStyleFilterId",
        "agreementTermsFinancingStyleFilterAdd",
        financingStylesData[0].financingStyles, []
      );

      setStartOfMonthDateFilter();
      simulationFilterGroup = new window.treasury.filter.FilterGroup(
        null, [
          agreementFilter,
          currencyFilter,
          baseRateCurrencyFilter,
          assetClassGroupFilter,
          bandwidthGroupFilter,
          accrualConventionEditSelect,
          debitAccrualConventionEditSelect,
          creditAccrualConventionEditSelect,
          financingStyleFilter,
          debitIRateFilter,
          creditIRateFilter
        ],
        $("#datePickerAdd"),
        null
      );

      toast.remove();
    });
  }

  function _loadNettingGroupOnAgreement() {
    if (actionType == "enable") {
      var toast = raiseInfoToast(
        "Loading Netting Groups applicable for Agreement, please wait..."
      );
      var parameters = simulationFilterGroup.getSerializedParameterMap();
      var agreementId;
      document.getElementById("nettingGroupFilterAdd").placeholder = "";
      if (document.getElementById("agreementFilterAdd") != null) {
        agreementId = parameters["agreementFilterIds"];
      }

      $.when(treasury.loadfilter.nettingGroupsForAgreementId(agreementId)).done(
        function(nettingGroupsData) {
          nettingGroupFilter = new window.treasury.filter.SingleSelect(
            "nettingGroupFilterIds",
            "nettingGroupFilterAdd",
            nettingGroupsData.nettingGroups, []
          );

          simulationFilterGroup.singleSelectFilterArray.push(
            nettingGroupFilter
          );

          toast.remove();
        }
      );
    }
  }

  function _loadBaseRatesOnCurrency() {
    if (actionType == "enable") {
      var toast = raiseInfoToast(
        "Loading base rates applicable for currency, please wait..."
      );
      var currencyId;
      var parameters = simulationFilterGroup.getSerializedParameterMap();
      if (
        document.getElementById("agreementTermsBaseRateCurrencyFilterAdd") !=
        null
      ) {
        currencyId = parameters["baseRateCurrencyFilterIds"];
      }
      if (currencyId <= 0) {
        currencyId = parameters["currencyFilterIds"];
      }
      $.when(treasury.loadfilter.baseRates(currencyId)).done(function(
        baseRatesData
      ) {
        if (baseRatesData["baseRates"].length == 0) {
          baseRatesData["baseRates"].push({
            0: 0,
            1: "0 - 0 - 0"
          });
        }
        baseRateEditFilter = new window.treasury.filter.SingleSelect(
          "baseRateFilterIds",
          "agreementTermsBaseRateInput",
          baseRatesData["baseRates"], []
        );
        simulationFilterGroup.singleSelectFilterArray.push(baseRateEditFilter);
        var creditBaseRateEditFilter = new window.treasury.filter.SingleSelect(
          "creditBaseRateFilterIds",
          "agreementTermsCreditBaseRateInput",
          baseRatesData["baseRates"], []
        );
        simulationFilterGroup.singleSelectFilterArray.push(
          creditBaseRateEditFilter
        );
        var debitBaseRateEditFilter = new window.treasury.filter.SingleSelect(
          "debitBaseRateFilterIds",
          "agreementTermsDebitBaseRateInput",
          baseRatesData["baseRates"], []
        );
        simulationFilterGroup.singleSelectFilterArray.push(
          debitBaseRateEditFilter
        );
        var rebateBaseRateEditFilter = new window.treasury.filter.SingleSelect(
          "rebateBaseRateFilterIds",
          "agreementTermsRebateBaseRateInput",
          baseRatesData["baseRates"], []
        );
        simulationFilterGroup.singleSelectFilterArray.push(
          rebateBaseRateEditFilter
        );

        toast.remove();
      });
    }
  }
})();
