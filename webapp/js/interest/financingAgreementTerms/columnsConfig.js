"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.financingAgreementTermsColumnsConfig = {
    getColumns: _getColumns,
    getCommonColumns: _getCommonColumns
  };

  function _getCommonColumns() {
    return [
      {
        id: "nettingGroup",
        type: "text",
        name: "Netting Group",
        field: "financingMethodology",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          if (
            typeof value !== "undefined" &&
            typeof value.nettingGroup !== "undefined"
          ) {
            returnValue = value.nettingGroup.myDescription;
          }
          return returnValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingMethodology !== "undefined" &&
            typeof a.financingMethodology.nettingGroup !== "undefined"
          )
          {
            aValue = a.financingMethodology.nettingGroup.myDescription;
          }
          if (
            typeof b.financingMethodology !== "undefined" &&
            typeof b.financingMethodology.nettingGroup !== "undefined"
          )
          {
            bValue = b.financingMethodology.nettingGroup.myDescription;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var returnValue = "";
          if (
            typeof value !== "undefined" &&
            typeof value.nettingGroup !== "undefined"
          ) {
            returnValue = value.nettingGroup.myDescription;
          }
          return returnValue;
        },
        width: 250
      },
      {
        id: "creditBaseRate",
        type: "text",
        name: "Credit Base Rate",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          if (
            typeof value.creditBaseRateTs !== "undefined" &&
            value.creditBaseRateTs !== null
          ) {
            returnValue = value.creditBaseRateTs.myName;
          }
          return returnValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined" &&
            typeof a.financingTerm.creditBaseRateTs !== "undefined"
          )
          {
            aValue = a.financingTerm.creditBaseRateTs.myName;
          }
          if (
            typeof b.financingTerm !== "undefined" &&
            typeof b.financingTerm.creditBaseRateTs !== "undefined"
          )
          {
            bValue = b.financingTerm.creditBaseRateTs.myName;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var returnValue = "";
          if (
            typeof value.creditBaseRateTs !== "undefined" &&
            value.creditBaseRateTs !== null
          ) {
            returnValue = value.creditBaseRateTs.myName;
          }
          return returnValue;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "debitBaseRate",
        type: "text",
        name: "Debit Base Rate",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          if (
            typeof value.debitBaseRateTs !== "undefined" &&
            value.debitBaseRateTs !== null
          ) {
            returnValue = value.debitBaseRateTs.myName;
          }
          return returnValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined" &&
            typeof a.financingTerm.debitBaseRateTs !== "undefined"
          )
          {
            aValue = a.financingTerm.debitBaseRateTs.myName;
          }
          if (
            typeof b.financingTerm !== "undefined" &&
            typeof b.financingTerm.debitBaseRateTs !== "undefined"
          )
          {
            bValue = b.financingTerm.debitBaseRateTs.myName;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var returnValue = "";
          if (
            typeof value.debitBaseRateTs !== "undefined" &&
            value.debitBaseRateTs !== null
          ) {
            returnValue = value.debitBaseRateTs.myName;
          }
          return returnValue;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "rebateBaseRate",
        type: "text",
        name: "Rebate Base Rate",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          if (
            typeof value.rebateBaseRateTs !== "undefined" &&
            value.rebateBaseRateTs !== null
          ) {
            returnValue = value.rebateBaseRateTs.myName;
          }
          return returnValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined" &&
            typeof a.financingTerm.rebateBaseRateTs !== "undefined" && a.financingTerm.rebateBaseRateTs !== null
          )
          {
            aValue = a.financingTerm.rebateBaseRateTs.myName;
          }
          if (
            typeof b.financingTerm !== "undefined" &&
            typeof b.financingTerm.rebateBaseRateTs !== "undefined" && b.financingTerm.rebateBaseRateTs !== null
          )
          {
            bValue = b.financingTerm.rebateBaseRateTs.myName;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var returnValue = "";
          if (
            typeof value.rebateBaseRateTs !== "undefined" &&
            value.rebateBaseRateTs !== null
          ) {
            returnValue = value.rebateBaseRateTs.myName;
          }
          return returnValue;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "creditSpread",
        type: "text",
        name: "Credit Spread(bps)",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return value.creditSpread * 100;
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined" &&
            typeof a.financingTerm.creditSpread !== "undefined"
          )
          {
            aValue = a.financingTerm.creditSpread * 100;
          }
          if (
            typeof b.financingTerm !== "undefined" &&
            typeof b.financingTerm.creditSpread !== "undefined"
          )
          {
            bValue = b.financingTerm.creditSpread * 100;
          }
            return treasury.comparators.number(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          return value.creditSpread;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        excelFormatter: "#,##0",
        width: 100
      },
      {
        id: "debitSpread",
        type: "text",
        name: "Debit Spread(bps)",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return value.debitSpread * 100;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined" &&
            typeof a.financingTerm.debitSpread !== "undefined"
          )
          {
            aValue = a.financingTerm.debitSpread * 100;
          }
          if (
            typeof b.financingTerm !== "undefined" &&
            typeof b.financingTerm.debitSpread !== "undefined"
          )
          {
            bValue = b.financingTerm.debitSpread * 100;
          }
            return treasury.comparators.number(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          return value.debitSpread;
        },
        width: 100
      },
      {
        id: "assetClassGroup",
        type: "text",
        name: "Asset Class Group",
        field: "assetClassGroup",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          if (typeof value !== "undefined" && value !== null) {
            returnValue = value.displayName;
          }
          return returnValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.assetClassGroup !== "undefined" && a.assetClassGroup !== null
          )
          {
            aValue = a.assetClassGroup.displayName;
          }
          if (
            typeof b.assetClassGroup !== "undefined" && b.assetClassGroup !== null
          )
          {
            bValue = b.assetClassGroup.displayName;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var returnValue = "";
          if (typeof value !== "undefined" && value !== null) {
            returnValue = value.displayName;
          }
          return returnValue;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "bandwidthGroup",
        type: "text",
        name: "Bandwidth Group",
        field: "bandwidthGroup",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          if (typeof value !== "undefined" && value !== null) {
            returnValue = value.displayName;
          }
          return returnValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.bandwidthGroup !== "undefined" && a.bandwidthGroup !== null
          )
          {
            aValue = a.bandwidthGroup.displayName;
          }
          if (
            typeof b.bandwidthGroup !== "undefined" && b.bandwidthGroup !== null
          )
          {
            bValue = b.bandwidthGroup.displayName;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var returnValue = "";
          if (typeof value !== "undefined" && value !== null) {
            returnValue = value.displayName;
          }
          return returnValue;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "gcRate",
        type: "text",
        name: "GC Rate(bps)",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return value.gcRate * 100;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined"
          )
          {
            aValue = a.financingTerm.gcRate * 100;
          }
          if (
            typeof b.financingTerm !== "undefined"
          )
          {
            bValue = b.financingTerm.gcRate * 100;
          }
            return treasury.comparators.number(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          return value.gcRate;
        },
        width: 100
      },
      {
        id: "creditAccrualConvention",
        type: "text",
        name: "Credit Accrual Convention",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return value.creditAccrualConvention;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined"
          )
          {
            aValue = a.financingTerm.creditAccrualConvention;
          }
          if (
            typeof b.financingTerm !== "undefined"
          )
          {
            bValue = b.financingTerm.creditAccrualConvention;
          }
            return treasury.comparators.number(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          return value.creditAccrualConvention;
        },
        width: 100
      },
      {
        id: "debitAccrualConvention",
        type: "text",
        name: "Debit Accrual Convention",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return value.debitAccrualConvention;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined"
          )
          {
            aValue = a.financingTerm.debitAccrualConvention;
          }
          if (
            typeof b.financingTerm !== "undefined"
          )
          {
            bValue = b.financingTerm.debitAccrualConvention;
          }
            return treasury.comparators.number(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          return value.debitAccrualConvention;
        },
        width: 100
      },
      {
        id: "ApplicableNegativeCreditIrate",
        type: "text",
        name: "Applicable Negative Credit Irate",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Applicable Negative Credit Irate") {
                attributeValue = entry.attributeValueName;
              }
            });
          }
          return attributeValue;
        },
        comparator : function(a, b) {
          var aValue,bValue;
          if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
          else{
           aValue = _getAttr(a, "Applicable Negative Credit Irate", "attributeValueName");
           bValue = _getAttr(b, "Applicable Negative Credit Irate", "attributeValueName");
          }
          return treasury.comparators.text(aValue, bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        filter: {
          isFilterOnFormattedValue: true
        },
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Applicable Negative Credit Irate") {
                attributeValue = entry.attributeValueName;
              }
            });
          }
          return attributeValue;
        },
        width: 160
      },
      {
        id: "SmvConversionFactor",
        type: "text",
        name: "SMV Conversion Factor",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "SMV Conversion Factor") {
                attributeValue = entry.rvalue;
              }
            });
          }
          return attributeValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
        comparator : function(a, b) {
          var aValue,bValue;
          if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
          else{
           aValue = _getAttr(a, "SMV Conversion Factor", "rvalue");
           bValue = _getAttr(b, "SMV Conversion Factor", "rvalue");
          }
          return treasury.comparators.number(aValue, bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "SMV Conversion Factor") {
                attributeValue = entry.rvalue;
              }
            });
          }
          return attributeValue;
        },
        width: 100
      },
      {
        id: "ApplicableNegativeDebitIrate",
        type: "text",
        name: "Applicable Negative Debit Irate",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Applicable Negative Debit Irate") {
                attributeValue = entry.attributeValueName;
              }
            });
          }
          return attributeValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
       comparator : function(a, b) {
        var aValue,bValue;
        if(typeof a === "string" && typeof b === "string") {
         aValue = a
         bValue = b
        }
        else{
           aValue = _getAttr(a, "Applicable Negative Debit Irate", "attributeValueName");
           bValue = _getAttr(b, "Applicable Negative Debit Irate", "attributeValueName");
        }
          return treasury.comparators.text(aValue, bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Applicable Negative Debit Irate") {
                attributeValue = entry.attributeValueName;
              }
            });
          }
          return attributeValue;
        },
        width: 160
      },
      {
        id: "ApplicableNegativeRateSfic",
        type: "text",
        name: "Applicable Negative Irate Sfic",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Applicable Negative Irate Sfic") {
                attributeValue = entry.ivalue;
              }
            });
          }
          return attributeValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
       comparator : function(a, b) {
        var aValue,bValue;
        if(typeof a === "string" && typeof b === "string") {
         aValue = a
         bValue = b
        }
        else{
           aValue = _getAttr(a, "Applicable Negative Irate Sfic", "ivalue");
           bValue = _getAttr(b, "Applicable Negative Irate Sfic", "ivalue");
        }
          return treasury.comparators.number(aValue, bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Applicable Negative Irate Sfic") {
                attributeValue = entry.ivalue;
              }
            });
          }
          return attributeValue;
        },
        width: 100
      },
      {
        id: "Haircut",
        type: "text",
        name: "Haircut",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Haircut") {
                attributeValue = entry.ivalue;
              }
            });
          }
          return attributeValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
       comparator : function(a, b) {
        var aValue,bValue;
        if(typeof a === "string" && typeof b === "string") {
         aValue = a
         bValue = b
        }
        else{
           aValue = _getAttr(a, "Haircut", "ivalue");
           bValue = _getAttr(b, "Haircut", "ivalue");
        }
          return treasury.comparators.number(aValue, bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Haircut") {
                attributeValue = entry.ivalue;
              }
            });
          }
          return attributeValue;
        }
      },
      {
        id: "financingStyle",
        type: "text",
        name: "Financing Style",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Financing Style Id") {
                attributeValue = entry.attributeValueName;
              }
            });
          }
          return attributeValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
       comparator : function(a, b) {
        var aValue,bValue;
         if(typeof a === "string" && typeof b === "string") {
          aValue = a
          bValue = b
         }
         else{
          aValue = _getAttr(a, "Financing Style Id", "attributeValueName");
          bValue = _getAttr(b, "Financing Style Id", "attributeValueName");
         } 
          return treasury.comparators.text(aValue, bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "Financing Style Id") {
                attributeValue = entry.attributeValueName;
              }
            });
          }
          return attributeValue;
        }
      },
      {
        id: "GCShortSpread",
        type: "text",
        name: "GC Short Spread(bps)",
        field: "financingTerm",
        formatter: function(row, cell, value, columnDef, dataContext) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "GC Short Spread") {
                attributeValue = entry.rvalue * 100;
              }
            });
          }
          return attributeValue;
        },
        filter: {
          isFilterOnFormattedValue: true
        },
       comparator : function(a, b) {
          var aValue,bValue;
          if(typeof a === "string" && typeof b === "string") {
           aValue = a
           bValue = b
          }
          else{
             aValue = _getAttr(a, "GC Short Spread", "rvalue") * 100;
             bValue = _getAttr(b, "GC Short Spread", "rvalue") * 100;
           }
           return treasury.comparators.number(aValue, bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var attributeValue = "";
          if (
            typeof value.attributeList !== "undefined" &&
            value.attributeList !== null
          ) {
            value.attributeList.forEach(function(entry) {
              if (entry.name === "GC Short Spread") {
                attributeValue = entry.rvalue;
              }
            });
          }
          return attributeValue;
        },
        width: 100
      }
    ];
  }

  function _getKeyColumns() {
    return [
      {
        id: "actions",
        type: "text",
        name: "Actions",
        field: "",
        formatter: function(row, cell, value, columnDef, dataContext) {
          return (
            '<a href="#" title="Edit Term"  onclick= event.stopPropagation();treasury.interest.financingAgreementTerms.editAgreementTerm(' +
            "'" +
            dataContext.id +
            "'" +
            '); ><i class="icon-edit--block "></i></a>' +
            '<a href="#" title="Delete Term"  onclick= event.stopPropagation();treasury.interest.financingAgreementTerms.deleteAgreementTerm(' +
            "'" +
            dataContext.id +
            "'" +
            '); ><i class="icon-delete margin--left "></i></a>'
          );
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        width: 55
      },
      {
        id: "agreementType",
        type: "text",
        name: "Agreement Type",
        field: "financingMethodology",
        filter: {
          isFilterOnFormattedValue: true
        },
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          returnValue = value.agreementType;
          return returnValue;
        },
        sortable: true,
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingMethodology !== "undefined"
          )
          {
            aValue = a.financingMethodology.agreementType;
          }
          if (
            typeof b.financingMethodology !== "undefined"
          )
          {
            bValue = b.financingMethodology.agreementType;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          return value.agreementType;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "legalEntity",
        type: "text",
        name: "Legal Entity",
        field: "financingMethodology",
        filter: {
          isFilterOnFormattedValue: true
        },
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          returnValue = value.legalEntity;
          return returnValue;
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingMethodology !== "undefined"
          )
          {
            aValue = a.financingMethodology.legalEntity;
          }
          if (
            typeof b.financingMethodology !== "undefined"
          )
          {
            bValue = b.financingMethodology.legalEntity;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          return value.legalEntity;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "cpe",
        type: "text",
        name: "Counter Party Entity",
        field: "financingMethodology",
        filter: {
          isFilterOnFormattedValue: true
        },
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          returnValue = value.cpe;
          return returnValue;
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingMethodology !== "undefined"
          )
          {
            aValue = a.financingMethodology.cpe;
          }
          if (
            typeof b.financingMethodology !== "undefined"
          )
          {
            bValue = b.financingMethodology.cpe;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          return value.cpe;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "currency",
        type: "text",
        name: "Currency",
        field: "financingMethodology",
        filter: {
          isFilterOnFormattedValue: true
        },
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          if (
            typeof value !== "undefined" &&
            typeof value.currency !== "undefined"
          ) {
            returnValue = value.currency.myAbbreviation;
          }
          return returnValue;
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingMethodology !== "undefined" &&
            typeof a.financingMethodology.currency !== "undefined"
          )
          {
            aValue = a.financingMethodology.currency.myAbbreviation;
          }
          if (
            typeof b.financingMethodology !== "undefined" &&
            typeof b.financingMethodology.currency !== "undefined"
          )
          {
            bValue = b.financingMethodology.currency.myAbbreviation;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelDataFormatter: function(value) {
          var returnValue = "";
          if (
            typeof value !== "undefined" &&
            typeof value.currency !== "undefined"
          ) {
            returnValue = value.currency.myAbbreviation;
          }
          return returnValue;
        },
        excelFormatter: "#,##0"
      },
      {
        id: "baseRateCurrency",
        type: "text",
        name: "Base Rate Currency",
        field: "financingTerm",
        filter: {
          isFilterOnFormattedValue: true
        },
        formatter: function(row, cell, value, columnDef, dataContext) {
          var returnValue = "";
          if (
            typeof value !== "undefined" &&
            typeof value.baseRateCurrencyName !== "undefined"
          ) {
            returnValue = value.baseRateCurrencyName;
          }
          return returnValue;
        },
        comparator : function(a, b) {
          var aValue = "";
          var bValue = "";
          if (
            typeof a.financingTerm !== "undefined" &&
            typeof a.financingTerm.baseRateCurrencyName !== "undefined"
          )
          {
            aValue = a.financingTerm.baseRateCurrencyName.myAbbreviation;
          }
          if (
            typeof b.financingTerm !== "undefined" &&
            typeof b.financingTerm.baseRateCurrencyName !== "undefined"
          )
          {
            bValue = b.financingTerm.baseRateCurrencyName.myAbbreviation;
          }
            return treasury.comparators.text(aValue,bValue);
        },
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        },
        excelDataFormatter: function(value) {
          var returnValue = "";
          if (
            typeof value !== "undefined" &&
            typeof value.baseRateCurrencyName !== "undefined"
          ) {
            returnValue = value.baseRateCurrencyName;
          }
          return returnValue;
        },
        excelFormatter: "#,##0"
      }
    ];
  }

  function _getEffectiveDateColumns() {
    var columns = [
      {
        id: "validFrom",
        type: "text",
        name: "Valid Start",
        field: "validStart",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        }
      },
      {
        id: "validEnd",
        type: "text",
        name: "Valid End",
        field: "validEnd",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelHeaderFormatter: function(row, cell) {
          return cell.id;
        }
      }
    ];

    return columns;
  }

  function _getColumns() {
    var columns = _getKeyColumns();
    columns = columns.concat(_getCommonColumns());
    columns = columns.concat(_getEffectiveDateColumns());
    return columns;
  }

  function _getAttr(obj, attrName, fieldName)
  {
    var value = "";
    if (typeof obj.financingTerm.attributeList !== "undefined" &&
        obj.financingTerm.attributeList !== null) {
        obj.financingTerm.attributeList.forEach(function(entry) {
            if (entry.name === attrName) {
                if (fieldName === "ivalue") {
                    value = entry.ivalue;
                }
                else if (fieldName === "rvalue") {
                    value = entry.rvalue;
                }
                else {
                    value = entry.attributeValueName;
                }
            }
        });
    }
    return value;
  }
})();
