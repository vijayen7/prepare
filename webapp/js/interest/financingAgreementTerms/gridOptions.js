"use strict";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.common = window.treasury.interest.common || {};
  window.treasury.interest.financingAgreementTermsGridOptions = {
    getGridOptions: _getGridOptions,
    getSimulatedGridOptions: _getSimulatedGridOptions
  };

  function _getGridOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      highlightRowOnClick: true,
      sortList: [{
        columnId: "agreementType",
        sortAsc: true
      }],
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      useAvailableScreenSpace: true,
      applyFilteringOnGrid: true,
      exportToExcel: true,
      page: true,
      onRowClick: function(item, rowObject, isNotToggle) {
        document.getElementById('fullTermDetailTable').innerHTML = getDetailTable(item);
      }
    };
  }

  function _getSimulatedGridOptions() {
    var gridOptions = _getGridOptions();
    gridOptions['checkboxHeader'] = [{
      controlChildSelection: false,
      isSelectAllCheckboxRequired: true
    }];
    gridOptions['addCheckboxHeaderAsLastColumn'] = false;
    return gridOptions;
  }

  function getDetailTable(item) {
    var value;
    var tableString = '<tr><th>Attribute</th><th>Value</th></tr>';
    item.financingMethodology.attributeList.forEach(function(entry) {
      value = entry[entry['type'].slice(0, 1) + entry['type'].charAt(1).toUpperCase() + entry['type'].slice(2)];
      if (value != null){
        tableString = tableString.concat('<tr><td>' + entry.name + '</td>');
        tableString = tableString.concat('<td>' + value + '</td></tr>');
      }

    })
    return tableString;
  }

  function _getFormattedData(value, precision) {
    if (value == null || value == "") {
      return "";
    } else {
      return parseFloat(value).toFixed(precision);
    }
  }
})();
