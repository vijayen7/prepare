"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.bandwidthGroupActions = {
    saveBandwidthGroup: _saveBandwidthGroup,
    addBandwidthGroupDialog: _addBandwidthGroupDialog
  };

  function _addBandwidthGroupDialog() {
    document.getElementById("addBandwidthGroupDialog").visible = true;
    document.getElementById("submitBandwidthGroup").onclick = treasury.interest.bandwidthGroupActions.saveBandwidthGroup;
  }

  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  function _saveBandwidthGroup() {
    var parameters = {};
    document.getElementById("bandwidthGroupMessage").style.visibility = "hidden";
    var bandwidthGroupName = document.getElementById("bandwidthGroupName");
    var startingThresthold = document.getElementById("startingThreshold");
    var endingThresthold = document.getElementById("endingThreshold");
    var abbreviation = document.getElementById("bandwidthGroupAbbrev");

    if (!(/\S/.test(bandwidthGroupName.value)) || !(/\S/.test(startingThresthold.value)) || !(/\S/.test(endingThresthold.value)) || !(/\S/.test(abbreviation.value))) {
      document.getElementById("bandwidthGroupError").innerHTML = 'All values are required.';
      document.getElementById("bandwidthGroupError").style.visibility = "visible";
      return;
    }

    if (!isNumeric(startingThresthold.value) || !isNumeric(endingThresthold.value)) {
      document.getElementById("bandwidthGroupError").innerHTML = 'Starting and ending threashold have to be numeric values.';
      document.getElementById("bandwidthGroupError").style.visibility = "visible";
      return;
    }
    document.getElementById("bandwidthGroupError").style.visibility = "hidden";

    parameters[bandwidthGroupName.id] = bandwidthGroupName.value;
    parameters[startingThresthold.id] = startingThresthold.value;
    parameters[endingThresthold.id] = endingThresthold.value;
    parameters[abbreviation.id] = abbreviation.value;
    console.log(parameters);
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/save-bandwidth-group",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        hideLoading("mainLoader");
        if (data && data.bandwidthGroupId) {
          document.getElementById("bandwidthGroupSuccess").innerHTML = 'Bandwidth Group was added.';
          document.getElementById("bandwidthGroupSuccess").style.visibility = "visible";
          document.getElementById("bandwidthGroupSuccess").style.display = "block";
          document.getElementById("bandwidthGroupName").value = '';
          document.getElementById("startingThreshold").value = '';
          document.getElementById("endingThreshold").value = '';
          document.getElementById("bandwidthGroupAbbrev").value = '';
        } else if (data && data.errorMessage) {
          document.getElementById("addBandwidthGroupDialog").visible = false;
          showErrorMessage(data.errorMessage);
          console.log("Error in saving bandwidth group.");
        } else if (data) {
          document.getElementById("bandwidthGroupError").innerHTML = 'Error in saving bandwidth group.';
          document.getElementById("bandwidthGroupError").style.visibility = "visible";
        }
      });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        hideLoading("mainLoader");
        document.getElementById("bandwidthGroupError").innerHTML = 'Error in saving bandwidth group.';
        document.getElementById("bandwidthGroupError").style.visibility = "visible";
      });
  }

  $("#endingThreshold").on("focusin", function() {
    $("#bandwidthGroupMessage").show();
  });

  $("#endingThreshold").focusout(function() {
    $("#bandwidthGroupMessage").hide();
  });
})();