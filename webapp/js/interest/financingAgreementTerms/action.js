"use strict";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.financingAgreementTermsAction = {
    loadResults: _loadResults,
    loadFullTerm: _loadFullTerm,
    loadSimulationResults: _loadSimulationResults,
    saveSelectedTerms: _saveSelectedTerms,
    deleteAgreementTerm: _deleteAgreementTerm
  };

  var _agreementTermsSimulatedTermsGrid;

  function _saveSelectedTerms() {

    if (!$("#agreementTermsComment").val()) {
      $("#agreementTermsComment").addClass("error");
      return false;
    } else {
      $("#agreementTermsComment").removeClass();
      treasury.interest.common.actions.saveSelectedRules("/treasury/financing/update-agreement-terms",
        _agreementTermsSimulatedTermsGrid,
        function() {
          document.getElementById('financingAgreementTermsDialog').visible = false;
        });
    }
  }

  function _deleteAgreementTerm(parameters) {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/delete-agreement-term",
      data: parameters,
      type: "POST",
      dataType: "json"
    });


    serviceCall
      .done(function(data) {
        hideLoading("mainLoader");
        if (data && data.errorMessage) {
          raiseErrorToast("Error deleting agreement term");
          return;
        }
        raiseSuccessToast("Agreement Term deleted");
        document.getElementById('financingAgreementTermsDialog').visible = false;
      });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        hideLoading("mainLoader");
        raiseErrorToast("Error deleting agreement term");
      });

  }

  function _loadFullTerm() {
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-full-financing-term",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.interest.common.actions.loadSearchGrid(serviceCall, "financingAgreementTermsGrid",
      treasury.interest.financingAgreementTermsColumnsConfig.getColumns,
      treasury.interest.common.grid.getGridOptions);
  }

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-financing-agreement-terms-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    clearGrid("financingAgreementTermsGrid");
    showLoading("mainLoader");
    serviceCall
      .done(function(data) {
        treasury.interest.common.actions.loadSearchGridForData(data, "financingAgreementTermsGrid",
          treasury.interest.financingAgreementTermsColumnsConfig.getColumns,
          treasury.interest.financingAgreementTermsGridOptions.getGridOptions);
      });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        raiseErrorToast("Error occured while searching for results");
      });

  }

  function _loadSimulationResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/agreement-terms-simulate-rules",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    treasury.interest.common.actions.loadSimulationGrids(serviceCall, "financingAgreementTermExistingGrid",
      "financingAgreementTermSimulatedGrid", "financingAgreementTermExistingLabel",
      "financingAgreementTermSimulatedLabel", treasury.interest.financingAgreementTermsColumnsConfig
      .getCommonColumns, treasury.interest.common.grid.getGridOptions,
      treasury.interest.common.grid.getSimulatedGridOptions,
      function() {
        document.getElementById("saveFinancingAgreementTerms").style.visibility = "visible";
      },
      function(gridVar) {
        _agreementTermsSimulatedTermsGrid = gridVar;
      })
  }
})();
