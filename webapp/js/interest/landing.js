"use strict";
var searchFilterGroup;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.landing = {
    loadLandingView: _loadLandingView,
    loadSearchResults: _loadSearchResults,
    loadAddDialog: _loadAddDialog,

  };

  window.addEventListener("WebComponentsReady", _initialize);

  function _getPageToLoad(type) {
    return {
      'assetClassGroup': '/treasury/jsp/interest/asset-class-group.jspf',
      'financingAgreementTerms': '/treasury/jsp/interest/financing-agreement-terms.jspf',
      'calculatorGroup': '/treasury/jsp/interest/calculator-group-rule-system.jspf',
      'accrualPosting': '/treasury/jsp/interest/methodology-accrual-posting-rule-system.jspf',
      'reportingCurrency': '/treasury/jsp/interest/reporting-currency-rule-system.jspf',
      'nettingGroup': '/treasury/jsp/interest/netting-group-ca-mapping.jspf',
    }[type];
  }

  function _getHandler(type) {
    return {
      'assetClassGroup': window.treasury.interest.assetClass.registerHandlers,
      'financingAgreementTerms': window.treasury.interest.financingAgreementTerms.registerHandlers,
      'calculatorGroup': window.treasury.interest.calculatorGroup.registerHandlers,
      'accrualPosting': window.treasury.interest.methodologyAccrualPosting.registerHandlers,
      'reportingCurrency': window.treasury.interest.reportingCurrency.registerHandlers,
      'nettingGroup': window.treasury.interest.nettingGroup.registerHandlers,
    }[type];
  }

  function _getSearchToLoad(type) {
    return {
      'assetClassGroup': window.treasury.interest.assetClass.loadSearchResults,
      'financingAgreementTerms': window.treasury.interest.financingAgreementTerms.loadSearchResults,
      'calculatorGroup': window.treasury.interest.calculatorGroup.loadSearchResults,
      'accrualPosting': window.treasury.interest.methodologyAccrualPosting.loadSearchResults,
      'reportingCurrency': window.treasury.interest.reportingCurrency.loadSearchResults,
      'nettingGroup': window.treasury.interest.nettingGroup.loadSearchResults,
    }[type];
  }

  function _getAddToLoad(type) {
    return {
      'financingAgreementTerms': window.treasury.interest.financingAgreementTerms.loadAddDialog,
      'calculatorGroup': window.treasury.interest.calculatorGroup.loadAddDialog,
      'accrualPosting': window.treasury.interest.methodologyAccrualPosting.loadAddDialog,
      'reportingCurrency': window.treasury.interest.reportingCurrency.loadAddDialog,
      'nettingGroup': window.treasury.interest.nettingGroup.loadAddDialog,
      'assetClassGroup': window.treasury.interest.assetClass.loadAddDialog,
    }[type];
  }

  function _getpageDestructor(type) {
    return {
      'assetClassGroup': window.treasury.interest.assetClass.pageDestructor,
      'financingAgreementTerms': window.treasury.interest.financingAgreementTerms.pageDestructor,
      'calculatorGroup': window.treasury.interest.calculatorGroup.pageDestructor,
      'accrualPosting': window.treasury.interest.methodologyAccrualPosting.pageDestructor,
      'reportingCurrency': window.treasury.interest.reportingCurrency.pageDestructor,
      'nettingGroup': window.treasury.interest.nettingGroup.pageDestructor,
    }[type];
  }

  function _registerHandlers() {
    document.getElementById('financingAgreementTerms').onclick = function() {
        _resetView();
        _loadViewForId('financingAgreementTerms');
    };
    document.getElementById('assetClassGroup').onclick = function() {
        _resetView();
        _loadViewForId('assetClassGroup');
    };
    document.getElementById('accrualPosting').onclick = function() {
      _resetView();
      _loadViewForId('accrualPosting');
    };
    document.getElementById('calculatorGroup').onclick = function() {
      _resetView();
      _loadViewForId('calculatorGroup');
    };
    document.getElementById('reportingCurrency').onclick = function() {
      _resetView();
      _loadViewForId('reportingCurrency');
    };
    document.getElementById('nettingGroup').onclick = function() {
      _resetView();
      _loadViewForId('nettingGroup');
    };
    document.getElementById("search").onclick = treasury.interest.landing.loadSearchResults;
    document.getElementById("commonAddButton").onclick = treasury.interest.landing.loadAddDialog;
  }

  function _resetView() {
    var id = _getActiveMenuId();
    _getpageDestructor(id)();
    _removeCurrentActiveMenu(id);
  }

  function _loadViewForId(id) {
    document.getElementById(id).classList.add('active');
    $('#mainArea').load(_getPageToLoad(id), function() {
      _getHandler(id)();
    });
  }

  function _initialize() {
    _registerHandlers();
    treasury.interest.landing.loadLandingView();
  }

  function _loadLandingView() {
    _loadLandingViewFilters();
    _loadViewForId('calculatorGroup');
  }

  function _loadLandingViewFilters() {
    showLoading("mainLoader");
    $.when(treasury.loadfilter.legalEntities(),
        treasury.loadfilter.cpes(),
        treasury.loadfilter.loadAgreementTypes(),
        treasury.loadfilter.currencies(),
        treasury.loadfilter.nettingGroups())
      .done(
        function(legalEntitiesData, cpesData, agreementTypesData, currenciesData, nettingGroupsData) {

          var legalEntityFilter = new window.treasury.filter.MultiSelect(
            "legalEntityFilterIds", "legalEntityFilter",
            legalEntitiesData[0].descoEntities, []);

          var cpeEntityFilter = new window.treasury.filter.MultiSelect(
            "cpeFilterIds", "cpeEntityFilter",
            cpesData[0].cpes, []);

          var agreementTypeFilter = new window.treasury.filter.MultiSelect(
            "agreementTypeFilterIds", "agreementTypeFilter",
            agreementTypesData[0].agreementTypes, []);

          var currencyFilter = new window.treasury.filter.MultiSelect(
            "currencyFilterIds", "currencyFilter",
            currenciesData[0].currency, []);

          var nettingGroupFilter = new window.treasury.filter.MultiSelect(
            "nettingGroupFilterIds", "nettingGroupFilter",
            nettingGroupsData[0].nettingGroups, []);

          var dateFilterValue = treasury.defaults.date || Date.today();
          var minDate = Date.parse('1970-01-01');
          var maxDate = Date.parse('2038-01-01');
          var dateFormat = 'yy-mm-dd';
          setupDateFilter($("#datePicker"), $("#datePickerError"), dateFilterValue, minDate, maxDate,
            dateFormat);
          searchFilterGroup = new window.treasury.filter.FilterGroup([
            legalEntityFilter, cpeEntityFilter,
            agreementTypeFilter, currencyFilter,
            nettingGroupFilter
          ], null, $("#datePicker"), null);
          hideLoading("mainLoader");
        }
      );
  }

  function _loadSearchResults() {
    var searchFunction = _getSearchToLoad(_getActiveMenuId());
    searchFunction();

  }

  function _loadAddDialog() {
    var addFunction = _getAddToLoad(_getActiveMenuId())
    addFunction();
  }

  function _getActiveMenuId() {
    return document.getElementById('interestMenu').getElementsByClassName("active")[0].id;
  }

  function _removeCurrentActiveMenu(id) {
    document.getElementById(id).classList.remove('active');
  }

})();
