"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.interestOps = window.treasury.interestOps || {};
  window.treasury.interestOps.action = {
    loadResults : _loadResults,
  };

  function _loadResults(parameters) {
      clearGrid("interestOpsGrid");
      showLoading("mainLoader");
      var serviceCall = $.ajax({
        url: "/treasury/financing/get-interest-jobs",
        data: parameters,
        type: "POST",
        dataType: "json"
      });
      serviceCall.done(function(data) {
        if (data && data.tableData && data.tableData.length) {
            var interestOpGrid = new dportal.grid.createGrid(
                $("#interestOpsGrid"), data["tableData"],
                window.treasury.interestOps.columnConfig.getSearchResultColumns(),
                window.treasury.interestOps.gridOptions.getResultOptions());

          } else if (data && data.errorMessage) {
            document.getElementById("interestOpsGrid").innerHTML = 'Error retrieving data';
          } else if (data) {
            document.getElementById("interestOpsGrid").innerHTML = 'No data Found';
          }
        hideLoading("mainLoader");
      });
      serviceCall.fail(function(xhr, text, errorThrown) {
        hideLoading("mainLoader");
        document.getElementById("interestOpsGrid").innerHTML = 'Error retrieving data';
      });

  }
})();
