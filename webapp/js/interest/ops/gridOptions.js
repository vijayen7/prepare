/*jshint jquery:true*/
/*global treasury, ArcMessageHelper, validateDate*/
"use strict";

(function() {
	window.treasury = window.treasury || {};
	window.treasury.interestOps = window.treasury.interestOps || {};
	window.treasury.interestOps.gridOptions = {
    getResultOptions : _getResultOptions
	};

function _getResultOptions() {
  return {
    autoHorizontalScrollBar : true,
    nestedTable : false,
    expandTillLevel : -1,
    highlightRowOnClick : true,
    editable : true,
    asyncEditorLoading : false,
    autoEdit : false,
    useAvailableScreenSpace : true,
    applyFilteringOnGrid : true,
    exportToExcel : true,
    frozenColumn : 2
  };
}
})();
