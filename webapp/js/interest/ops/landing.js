"use strict";
(function() {

    window.treasury = window.treasury || {};
    window.treasury.interestOps = window.treasury.interestOps || {};
    window.treasury.interestOps.landing = {
        initialize: _initialize,
        loadSearchResults: _loadSearchResults
    };

    function _registerHandlers() {
      document.getElementById("searchInterestJobs").onclick = window.treasury.interestOps.landing.loadSearchResults;
    }

    function _validateInput(numberOfJobs, date) {
        if (document.getElementById("datePickerError").innerHTML != "") {
            return false;
        }
        if (date.value == "") {
            var dateFilterValue = treasury.defaults.date || Date.today();
            date.value = dateFilterValue;
        }

        if (numberOfJobs <= 0 || numberOfJobs > 1000000) {
            return false;
        }
        return true;
    }

    function _loadSearchResults() {
        var numberOfJobs = document.getElementById("interestOpsJobs").value;
        var date = document.getElementById("interestOpsDate");
        if (!_validateInput(numberOfJobs, date)) {
            return;
        }
        var formattedDate = getFormattedDate('interestOpsDate');

        var parameters = {numberOfJobs : numberOfJobs, dateString : formattedDate}

        window.treasury.interestOps.action.loadResults(parameters);
    }

    function _setDefaultNumberOfJobs(defaultNumberOfJobs) {
        document.getElementById("interestOpsJobs").value = defaultNumberOfJobs;
    }

    function _initialize() {
      _registerHandlers();
      _initializeDatePicker(document.getElementById("interestOpsDate"));
      _setDefaultNumberOfJobs(20);
    }
})();
