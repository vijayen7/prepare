"use strict";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.nettingGroupAction = {
    loadResults: _loadResults,
    loadSimulationResults: _loadSimulationResults,
    saveSelectedRules: _saveSelectedRules,
    saveNewNettingGroup: _saveNewNettingGroup,
    loadNettingGroupFilters:_loadNettingGroupsFilters,
    loadFinancingStyleFilters:_loadFinancingStyleFilters
  };

  var _simulatedRuleGrid;

  function _saveSelectedRules() {
    treasury.interest.common.actions.saveSelectedRules("/treasury/financing/update-netting-group-ca-mapping",
      _simulatedRuleGrid, function (){
        document.getElementById('nettingGroupCAMapDialog').visible = false;
      });
  }

  function _loadNettingGroupsFilters(custodianAccountFilterIds) {

      var param = {};
      param.nettingGroupFilterIds  = -1;
      param.custodianAccountFilterIds = custodianAccountFilterIds;
      return $.ajax({
        url: "/treasury/data/load-custodian-account-specific-netting-groups-filter",
        data: param,
        type: "POST",
        dataType: "json"
      });


  }
  function _loadFinancingStyleFilters() {
    var param = addNettingFilterGroup.getParameterMap();
    return $.ajax({
      url: "/treasury/data/load-agreement-type-specific-financial-style-filter",
      data: param,
      type: "POST",
      dataType: "json"
    });
  }



  function _saveNewNettingGroup(parameters) {
      showLoading("mainLoader");

      var serviceCall = $.ajax({
        url: "/treasury/financing/add-new-netting-group",
        data: parameters,
        type: "POST",
        dataType: "json"
      });
      serviceCall
        .done(function(data) {
          hideLoading("mainLoader");
          if(data && data.errorMessage)
          {
            raiseErrorToast("Error occured while saving rules");
            return;
          }
          raiseSuccessToast("Netting Group has been saved");
          document.getElementById('nettingGroupAddDialog').visible = false;

        });
      serviceCall
        .fail(function(xhr, text, errorThrown) {
          hideLoading("mainLoader");
          raiseErrorToast("Error occured while saving rules");
        });


  }

  function _loadSimulationResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/netting-group-ca-simulate-rules",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    treasury.interest.common.actions.loadSimulationGrids(serviceCall, "existingNettingGroupCAMapRuleGrid",
      "simulatedNettingGroupCaMapRuleGrid", "nettingGroupCAMapExistingRuleLabel",
      "nettingGroupCAMapSimulatedRuleLabel", treasury.interest.nettingGroupColumnsConfig
      .getSimulatedColumns, treasury.interest.common.grid.getGridOptions,
      treasury.interest.common.grid.getSimulatedGridOptions,
      function() {
        document.getElementById("saveNettingGroupCAMapRules").style.visibility = "visible";
      },
      function(gridVar) {
        _simulatedRuleGrid = gridVar;
      })
  }

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/get-netting-group-custodian-account-mappings",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.interest.common.actions.loadSearchGrid(serviceCall, "nettingGroupGrid",
      treasury.interest.nettingGroupColumnsConfig.getColumns,
      treasury.interest.common.grid.getGridOptions);
  }
})();
