"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.nettingGroupColumnsConfig = {
    getColumns: _getColumns,
    getActionColumn: _getActionColumn,
    getSimulatedColumns: _getSimulatedColumns
  };

  function _getActionColumn() {
    return [{
      id: "actions",
      type: "text",
      name: "Actions",
      field: "",
      formatter: function(row, cell, value, columnDef,
        dataContext) {
        return '<a href="#" title="Edit Term"  onclick= event.stopPropagation();treasury.interest.nettingGroup.editNettingGroupCAMap(' +
          '\'' +
          row +
          '\'' +
          '); ><i class="icon-edit--block "></i></a>'

      },
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 5
    }];
  }

  function _getCommonColumns() {
    return [{
      id: "custodianAccountName",
      type: "text",
      name: "Custodian Account",
      field: "custodianAccountName",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "nettingGroupName",
      type: "text",
      name: "Netting Group",
      field: "nettingGroupName",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }];
  }

  function _getColumns() {
    var columns = _getActionColumn();
    columns = columns.concat(_getCommonColumns());
    return columns;
  }

  function _getSimulatedColumns() {
    var columns = _getCommonColumns();
    return columns;
  }
})();
