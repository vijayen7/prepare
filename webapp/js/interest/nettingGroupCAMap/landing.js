"use strict";
var simulationFilterGroup;
var addNettingFilterGroup;

var bulkInsertNettingGroupShovelUrl = "/shovel/template/Netting Group";
var bulkUpdateNettingGroupMappingShovelUrl = "/shovel/template/Custodian Account by Netting Group";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.nettingGroup = {
    loadSearchResults: _loadSearchResults,
    loadAddDialog: _loadAddDialog,
    registerHandlers: _registerHandlers,
    pageDestructor: _pageDestructor,
    editNettingGroupCAMap: _editNettingGroupCAMap,
    deleteNettingGroupCAMap: _deleteNettingGroupCAMap,
  };

  function _registerHandlers() {
    _showAddButton();
    document.getElementById("addNettingGroupButton").onclick = function() {
      _loadAddNettingGroupDialog();
    };
    document.getElementById("simultedNettingGroupCAMaprule").onclick = _loadSimulationResult;
    document.getElementById("saveNewNettingGroup").onclick = _saveNewNettingGroup;
    document.getElementById("nettingGroupDescription").value = "";
    document.getElementById("nettingGroupComment").value = "";
    document.getElementById("saveNettingGroupCAMapRules").onclick = _saveSelectedRules;
    document.getElementById("addAgreementId").onchange = _loadFinancingStyleFilters;
     document.getElementById("custodianAccountFilterAdd").onchange = _loadNettingGroupFilters;
    document.getElementById("bulkInsertNettingGroupButton").onclick = function() {
      window.open(bulkInsertNettingGroupShovelUrl);
    };
    document.getElementById("bulkUpdateNettingGroupMappingButton").onclick = function() {
      window.open(bulkUpdateNettingGroupMappingShovelUrl);
    };
  }



  function _editNettingGroupCAMap(row){
    _clearAddDialogGrids();
    document.getElementById('nettingGroupCAMapDialog').visible = true;
    var rowData = searchGrid.data[row];
    document.getElementById('custodianAccountFilterAdd').value= {key: rowData.custodianAccountId,value:rowData.custodianAccountName + ' ' + '['+ rowData.custodianAccountId + ']'};
        $('#custodianAccountFilterAdd').attr("readOnly","true");


  }


  function _deleteNettingGroupCAMap(row){
    _clearAddDialogGrids();
    document.getElementById('nettingGroupCAMapDialog').visible = true;
    _loadAddDialogFilters(function(){});
  }

  function _saveSelectedRules() {
    window.treasury.interest.nettingGroupAction.saveSelectedRules();
  }

  function _saveNewNettingGroup() {
    var parameters = addNettingFilterGroup.getSerializedParameterMap();
    parameters['nettingGroupDescription'] = document.getElementById("nettingGroupDescription").value;
    parameters['nettingGroupComment'] = document.getElementById("nettingGroupComment").value;

    if (_validateSaveNettingGroupParameters(parameters)) {
      window.treasury.interest.nettingGroupAction.saveNewNettingGroup(parameters);
    }
  }

  function _validateSaveNettingGroupParameters(parameters) {
    var ret = true;

    if (parameters['financingTypeId'] == -1) {
      ret = false;
      $("#addFinancingTypeId").addClass("error");
    } else {
      $("#addFinancingTypeId").removeClass();
    }

    if (!parameters['nettingGroupDescription']) {
      ret = false;
      $("#nettingGroupDescription").addClass("error");
    } else {
      $("#nettingGroupDescription").removeClass();
    }

    if (parameters['agreementId'] == -1) {
      ret = false;
      $("#addAgreementId").addClass("error");
    } else {
      $("#addAgreementId").removeClass();
    }

    return ret;
  }

  function _loadSimulationResult() {
      _clearAddDialogGrids();
      var params = {};
      if(simulationFilterGroup.singleSelectFilterArray[1] == null){
        params.nettingGroupFilterIds  = nettingGroupFilterAdd.value.key;
        params.custodianAccountFilterIds = custodianAccountFilterAdd.value.key;

      }else{
        params = simulationFilterGroup.getSerializedParameterMap();
      }

      if (!_validateParameters(params)) {
        return;
      }
      treasury.interest.nettingGroupAction.loadSimulationResults(params);
    }

  function _validateParameters(params) {
    if (params['custodianAccountFilterIds'] == -1) {
      $("#custodianAccountFilterAdd").addClass("error");
      return false;
    } else {
      $("#custodianAccountFilterAdd").removeClass();
    }
    if (params['nettingGroupFilterIds'] == -1) {
      $("#nettingGroupFilterAdd").addClass("error");
      return false;
    } else {
    	$("#nettingGroupFilterAdd").removeClass();
    }

    return true;
  }

  function _loadSearchResults() {

    var parameters = searchFilterGroup.getSerializedParameterMap();
    treasury.interest.nettingGroupAction.loadResults(parameters);
  }

  function _showAddButton() {
    document.getElementById("addNettingGroupButton").style.display = "block";
    document.getElementById("bulkInsertNettingGroupButton").style.display = "block";
    document.getElementById("bulkUpdateNettingGroupMappingButton").style.display = "block";
  }


  function _pageDestructor() {
    document.getElementById("addNettingGroupButton").style.display = "none";
    document.getElementById("bulkInsertNettingGroupButton").style.display = "none";
    document.getElementById("bulkUpdateNettingGroupMappingButton").style.display = "none";
  }

  function _loadAddDialog() {
    _clearAddDialogGrids();
    document.getElementById('nettingGroupCAMapDialog').visible = true;
    _loadAddDialogFilters(function(){});
     $('#custodianAccountFilterAdd').removeAttr("readOnly");
  }

  function _loadAddNettingGroupDialog() {
    _clearAddDialogGrids();
    document.getElementById('nettingGroupAddDialog').visible = true;
    _loadAddNettingGroupDialogFilters();
  }

  function _clearAddDialogGrids() {
    document.getElementById("saveNettingGroupCAMapRules").style.visibility = "hidden";
    document.getElementById('nettingGroupCAMapExistingRuleLabel').style.visibility = "hidden";
    document.getElementById('nettingGroupCAMapSimulatedRuleLabel').style.visibility = "hidden";
    document.getElementById("errorMessage").style.display = "none";
    window.treasury.common.grid.clearGrid("existingNettingGroupCAMapRuleGrid");
    window.treasury.common.grid.clearGrid("simulatedNettingGroupCaMapRuleGrid");
  }

  function _loadAddDialogFilters(callbackfunction) {
    showLoading("editNettingGroupCAMapLoader");

    $.when(treasury.loadfilter.loadNettingGroupSpecificCustodianAccounts(),
        treasury.loadfilter.nettingGroups()
      )
      .done(
        function(caData, nettingGroupsData) {

          var custodianAccountFilter = new window.treasury.filter.SingleSelect(
            "custodianAccountFilterIds", "custodianAccountFilterAdd",
            caData[0].custodianAccounts, []);

          var nettingGroupFilter = new window.treasury.filter.SingleSelect(
            "nettingGroupFilterIds", "nettingGroupFilterAdd",
            nettingGroupsData[0].nettingGroups, []);


          simulationFilterGroup = new window.treasury.filter.FilterGroup(null, [
            nettingGroupFilter, custodianAccountFilter
          ], null, null);
          callbackfunction();
          hideLoading("editNettingGroupCAMapLoader");
        }
      );

  }

  function _loadNettingGroupFilters(callbackfunction) {
    if (document.getElementById('custodianAccountFilterAdd').value != null) {
      var custodianAccountIdToChange = document.getElementById('custodianAccountFilterAdd').value;
      showLoading("editNettingGroupCAMapLoader");
      $.when(treasury.interest.nettingGroupAction.loadNettingGroupFilters(custodianAccountIdToChange.key))
        .done(
          function(nettingGroupsData) {

            var nettingGroupFilter = new window.treasury.filter.SingleSelect(
              "nettingGroupFilterIds", "nettingGroupFilterAdd",
              nettingGroupsData.nettingGroups, []);

              if(simulationFilterGroup!=  undefined){
                simulationFilterGroup.singleSelectFilterArray[0] = nettingGroupFilter;
              }else{
                simulationFilterGroup = new window.treasury.filter.FilterGroup(null, [
                  nettingGroupFilter, null
                ], null, null);
              }


            hideLoading("editNettingGroupCAMapLoader");
          }
        );

    }

  }

  function _loadFinancingStyleFilters() {
    if (document.getElementById('addAgreementId').value != null) {
      showLoading("mainLoader");

      $.when(treasury.interest.nettingGroupAction.loadFinancingStyleFilters())
        .done(
          function(FinancingStyleData) {

            var financingStyleIdFilter = new window.treasury.filter.SingleSelect(
              "financingStyleId", "addFinancingStyleId",
              FinancingStyleData.financingStyles, []);

            addNettingFilterGroup.singleSelectFilterArray[2] = financingStyleIdFilter;
            hideLoading("mainLoader");
          }
        );
    }
  }

  function _loadAddNettingGroupDialogFilters() {
    showLoading("mainLoader");

    $.when(treasury.loadfilter.agreementIds(),
        treasury.loadfilter.financingTypes(),
        treasury.loadfilter.financingStyles()
      )
      .done(
        function(agreementIdsData, financingTypesData, financingStylesData) {
          var agreementFilter = new window.treasury.filter.SingleSelect(
            "agreementId", "addAgreementId",
            agreementIdsData[0].agreementIds, []);
          var financingTypeIdFilter = new window.treasury.filter.SingleSelect(
            "financingTypeId", "addFinancingTypeId",
            financingTypesData[0].financingTypes, []);
          var financingStyleIdFilter = new window.treasury.filter.SingleSelect(
            "financingStyleId", "addFinancingStyleId",
            financingStylesData[0].financingStyles, []);

          addNettingFilterGroup = new window.treasury.filter.FilterGroup(null, [
              agreementFilter, financingTypeIdFilter, financingStyleIdFilter
            ],
            null, null);
          hideLoading("mainLoader");
        }
      );
  }

})();
