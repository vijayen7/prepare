"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.methodologyAccrualPostingColumnsConfig = {
    getColumns: _getColumns,
  };

  function _getCommonColumns() {
    return [{
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreementType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legalEntity",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "counterPartyEntity",
      type: "text",
      name: "Counterparty",
      field: "cpe",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "currency",
      type: "text",
      name: "Currency",
      field: "ccy",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "nettingGroup",
      type: "text",
      name: "Netting Group",
      field: "nettingGroup",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "chargeType",
      type: "text",
      name: "Charge Type Id",
      field: "chargeType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },{
      id: "bundle",
      type: "text",
      name: "Bundle",
      field: "bundle",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "business_unit",
      type: "text",
      name: "Business Unit",
      field: "business_unit",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },{
      id: "custodian_account",
      type: "text",
      name: "Custodian Account",
      field: "custodian_account",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },{
      id: "validFrom",
      type: "text",
      name: "Valid From",
      field: "effective_start_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "validTill",
      type: "text",
      name: "Valid Till",
      field: "effective_end_date",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }];
  }

  function _getActionColumn() {
    return [{
      id: "actions",
      type: "text",
      name: "Actions",
      field: "",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return '<a href="#" title="Edit Term"  onclick= event.stopPropagation();treasury.interest.methodologyAccrualPosting.loadEditDialog(' +
          '\'' +
          row +
          '\'' +
          '); ><i class="icon-edit--block "></i></a>';
      },
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 55
    }, ];

  }

  function _getColumns() {
    var columns = _getActionColumn();
    columns = columns.concat(_getCommonColumns());
    return columns;
  }
})();
