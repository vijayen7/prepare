"use strict";
var simulationFilterGroup;
(function() {

	var myOnClickEvent = {
  		ajax: true
	}

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.methodologyAccrualPosting = {
    loadSearchResults: _loadSearchResults,
    loadAddDialog: _loadAddDialog,
    registerHandlers: _registerHandlers,
    pageDestructor: _pageDestructor,
    loadEditDialog: _loadEditDialog,
  };

  function _registerHandlers() {
    document.getElementById("simulateAccrualPostingRule").onclick = _loadSimulationResult;
    document.getElementById("saveAccrualPostingRules").onclick = _saveSelectedRules;
    document.getElementById("businessUnitFilterAdd").onchange = _loadBusinessUnitSpecificBundleFilter;
    document.getElementById("bundleFilterAdd").onchange = _loadBundleSpecificCustodianAccountFilter;
    document.getElementById("chargeTypeFilterAdd").onchange = _loadChargeTypeSpecificFilters;
  }

   

   function _removeFromList(list,elementToBeRemoved)
   {
        for (var i = 0; i < list.length; i++) {
            if(list[i][0] === elementToBeRemoved)
            {
                list.splice(i,1);
                break;
            }
        }
        return list;
   }

   function _loadBusinessUnitSpecificBundleFilter() {
    if(typeof simulationFilterGroup === 'undefined') {
            return null;
    }
    document.getElementById('bundleFilterAdd').value = null;
        if (document.getElementById('businessUnitFilterAdd').value != null) {
            showLoading("mainLoader");
            var parameters = simulationFilterGroup.getSerializedParameterMap();
            $.when(treasury.loadfilter.loadBundles(),
                    treasury.loadfilter.loadAccrualPostingSpecialBundles())
              .done(
                function(bundleData,specialBundleData) {
                for (var i = specialBundleData[0].specialBundles.length - 1; i >= 0; i--) {
                        bundleData[0].bundles.unshift(specialBundleData[0].specialBundles[i]);
                }
                   if (parameters.chargeTypeFilterIds === '6' || parameters.chargeTypeFilterIds === '7')
                   {
                        bundleData[0].bundles = _removeFromList(bundleData[0].bundles,'-1');
                   }
                   if (parameters.businessUnitFilterIds === '-3')
                   {
                        bundleData[0].bundles = _removeFromList(bundleData[0].bundles,'-2');
                   }
                   var bundleFilter = new window.treasury.filter.SingleSelect(
                                       "bundleFilterIds", "bundleFilterAdd",
                                       bundleData[0].bundles, []);

                   simulationFilterGroup.singleSelectFilterArray[7] = bundleFilter;
                   hideLoading("mainLoader");
                }
                
              );
        }
    }

    function _loadBundleSpecificCustodianAccountFilter() {
        if(typeof simulationFilterGroup === 'undefined') {
              return null;
        }
        document.getElementById('custodianAccountFilterAdd').value = null;
        if (document.getElementById('bundleFilterAdd').value != null) {
            showLoading("mainLoader");
            var parameters = simulationFilterGroup.getSerializedParameterMap();
              $.when(treasury.loadfilter.loadCustodianAccounts(),
                      treasury.loadfilter.loadAccrualPostingSpecialCa())
                .done(
                  function(caData,specialCaData) {
                  for (var i = specialCaData[0].specialCustodianAccount.length - 1; i >= 0; i--) {
                    caData[0].custodianAccounts.unshift(specialCaData[0].specialCustodianAccount[i]);
                  }
                  if(parameters.chargeTypeFilterIds === '6' || parameters.chargeTypeFilterIds === '7')
                  {
                    caData[0].custodianAccounts = _removeFromList(caData[0].custodianAccounts,'-1');
                  }
                  if(parameters.bundleFilterIds === '-4')
                  {
                    caData[0].custodianAccounts = _removeFromList(caData[0].custodianAccounts,'-2');
                  }
                  var caFilter = new window.treasury.filter.SingleSelect(
                        "custodianAccountFilterIds", "custodianAccountFilterAdd",
                        caData[0].custodianAccounts, []);

                      simulationFilterGroup.singleSelectFilterArray[8] = caFilter;
                      hideLoading("mainLoader");
                }
              );
        }
    }

    function _loadChargeTypeSpecificFilters() {
        if(typeof simulationFilterGroup === 'undefined') {
            return null;
         }
         document.getElementById('businessUnitFilterAdd').value = null;
         document.getElementById('bundleFilterAdd').value = null;
         document.getElementById('custodianAccountFilterAdd').value = null;
         if (document.getElementById('chargeTypeFilterAdd').value != null) {
            showLoading("mainLoader");
            var parameters = simulationFilterGroup.getSerializedParameterMap();
            $.when(treasury.loadfilter.loadBusinessUnits(),
                treasury.loadfilter.loadAccrualPostingSpecialBu(),
                treasury.loadfilter.loadBundles(),
                treasury.loadfilter.loadAccrualPostingSpecialBundles(),
                treasury.loadfilter.loadCustodianAccounts(),
                treasury.loadfilter.loadAccrualPostingSpecialCa())
            .done(
                function(businessUnitData,specialBusinessUnitData,bundleData,specialBundleData,
                         caData,specialCaData) {
                    for (var i = specialCaData[0].specialCustodianAccount.length - 1; i >= 0; i--) {
                        caData[0].custodianAccounts.unshift(specialCaData[0].specialCustodianAccount[i]);
                    }
                    if (parameters.chargeTypeFilterIds === '6' || parameters.chargeTypeFilterIds === '7')
                    {
                        caData[0].custodianAccounts = _removeFromList(caData[0].custodianAccounts,'-1');
                    }

                    var caFilter = new window.treasury.filter.SingleSelect(
                            "custodianAccountFilterIds", "custodianAccountFilterAdd",
                            caData[0].custodianAccounts, []);

                    simulationFilterGroup.singleSelectFilterArray[8] = caFilter;

                    for (var i = specialBundleData[0].specialBundles.length - 1; i >= 0; i--) {
                        bundleData[0].bundles.unshift(specialBundleData[0].specialBundles[i]);
                    }
                    if (parameters.chargeTypeFilterIds === '6' || parameters.chargeTypeFilterIds === '7')
                    {
                        bundleData[0].bundles = _removeFromList(bundleData[0].bundles,'-1');
                    }

                    var bundleFilter = new window.treasury.filter.SingleSelect(
                                            "bundleFilterIds", "bundleFilterAdd",
                                            bundleData[0].bundles, []);

                    simulationFilterGroup.singleSelectFilterArray[7] = bundleFilter;

                    for (var i = specialBusinessUnitData[0].specialBusinessUnits.length - 1; i >= 0; i--) {
                        businessUnitData[0].businessUnits.unshift(specialBusinessUnitData[0].specialBusinessUnits[i]);
                    }
                    if (parameters.chargeTypeFilterIds === '6' || parameters.chargeTypeFilterIds === '7')
                    {
                        businessUnitData[0].businessUnits = _removeFromList(businessUnitData[0].businessUnits,'-1');
                    }

                    var businessUnitFilter = new window.treasury.filter.SingleSelect(
                                                                     "businessUnitFilterIds", "businessUnitFilterAdd",
                                                                     businessUnitData[0].businessUnits, []);

                    simulationFilterGroup.singleSelectFilterArray[6] = businessUnitFilter;
                    hideLoading("mainLoader");


            }
          );
        }
   }

  function _pageDestructor() {
  }

  function _saveSelectedRules() {
    window.treasury.interest.methodologyAccrualPostingAction.saveSelectedRules();
  }

  function _loadSimulationResult() {
    var parameters = simulationFilterGroup.getSerializedParameterMap();
    if(! _validateParameters(parameters))
    {
      return;
    }
    var formattedDate = getFormattedDate('datePickerAdd');
    parameters.dateString = formattedDate;
    treasury.interest.methodologyAccrualPostingAction.loadSimulationResults(parameters);
  }

  function _validateParameters(params)
  {
	  if (document.getElementById("datePickerError").innerHTML != "") {
	      $("#datePickerAdd").addClass("error");
	      return false;
	    } else {
	      $("#datePickerAdd").removeClass();
	    }
    return true;
  }

  function _loadSearchResults() {
	  if (document.getElementById("datePickerError").innerHTML != "") {
	      $("#datePickerAdd").addClass("error");
	      return;
	    } else {
	      $("#datePickerAdd").removeClass();
	    }

    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    parameters.dateString = formattedDate;

    treasury.interest.methodologyAccrualPostingAction.loadResults(parameters);
  }

  function _loadAddDialog() {
    _clearAddDialogGrids();
    document.getElementById('accrualPostingDialog').visible = true;
    _loadAddDialogFilters();
  }

  function _loadEditDialog(row) {
    _clearAddDialogGrids();
    document.getElementById('accrualPostingDialog').visible = true;
    _loadAddDialogFilters(_editRuleFields.bind(null, searchGrid.data[row]));
  }

  function _clearAddDialogGrids() {
    document.getElementById("saveAccrualPostingRules").style.visibility = "hidden";
    document.getElementById('accrualPostingExistingRuleLabel').style.visibility = "hidden";
    document.getElementById('accrualPostingSimulatedRuleLabel').style.visibility = "hidden";
    window.treasury.common.grid.clearGrid("existingAccrualPostingRuleGrid");
    window.treasury.common.grid.clearGrid("simulatedAccrualPostingRuleGrid");
    document.getElementById("datePickerErrorAdd").innerHTML = "";
    $("#datePickerAdd").removeClass("error");

    $('#nettingGroupFilterAdd').removeAttr("readOnly");
    $('#cpeEntityFilterAdd').removeAttr("readOnly");
    $('#agreementTypeFilterAdd').removeAttr("readOnly");
    $('#currencyFilterAdd').removeAttr("readOnly");
    $('#legalEntityFilterAdd').removeAttr("readOnly");
    $('#chargeTypeFilterAdd').removeAttr("readOnly");
  }

  function _editRuleFields(param){
    myOnClickEvent.ajax = false;

    simulationFilterGroup.singleSelectFilterArray[0].setSelectedNode(param.legal_entity_id);
    simulationFilterGroup.singleSelectFilterArray[1].setSelectedNode(param.cpe_id);
    simulationFilterGroup.singleSelectFilterArray[2].setSelectedNode(param.agreement_type_id);
    simulationFilterGroup.singleSelectFilterArray[4].setSelectedNode(param.netting_group_id);
    simulationFilterGroup.singleSelectFilterArray[3].setSelectedNode(param.currency_id);
    simulationFilterGroup.singleSelectFilterArray[5].setSelectedNode(param.charge_type_id);
    simulationFilterGroup.singleSelectFilterArray[6].setSelectedNode(param.business_unit_id);
    simulationFilterGroup.singleSelectFilterArray[7].setSelectedNode(param.bundle_id);
    simulationFilterGroup.singleSelectFilterArray[8].setSelectedNode(param.custodian_account_id);
    _loadChargeTypeSpecificFilters();

    setFilterDateToMonthStart("datePickerAdd", "datePickerErrorAdd");

    $('#legalEntityFilterAdd').attr("readOnly","true");
    $('#cpeEntityFilterAdd').attr("readOnly","true");
    $('#agreementTypeFilterAdd').attr("readOnly","true");
    $('#nettingGroupFilterAdd').attr("readOnly","true");
    $('#currencyFilterAdd').attr("readOnly","true");
    $('#chargeTypeFilterAdd').attr("readOnly","true");

	myOnClickEvent.ajax = true;
  }

  function _loadAddDialogFilters(callback) {
    showLoading("mainLoader");
    $.when(treasury.loadfilter.legalEntities(),
        treasury.loadfilter.cpes(),
        treasury.loadfilter.loadAgreementTypes(),
        treasury.loadfilter.currencies(),
        treasury.loadfilter.nettingGroups(),
        treasury.loadfilter.chargeTypes(),
        treasury.loadfilter.loadBusinessUnits(),
        treasury.loadfilter.loadBundles(),
        treasury.loadfilter.loadCustodianAccounts(),
        treasury.loadfilter.loadAccrualPostingSpecialBu(),
        treasury.loadfilter.loadAccrualPostingSpecialBundles(),
        treasury.loadfilter.loadAccrualPostingSpecialCa()
      )
      .done(
        function(legalEntitiesData, cpesData, agreementTypesData, currenciesData, nettingGroupsData, chargeTypesData,
          buData, bundleData, caData, specialBuData, specialBundleData, specialCaData) {

          var legalEntityFilter = new window.treasury.filter.SingleSelect(
            "legalEntityFilterIds", "legalEntityFilterAdd",
            legalEntitiesData[0].descoEntities, []);

          var cpeEntityFilter = new window.treasury.filter.SingleSelect(
            "cpeFilterIds", "cpeEntityFilterAdd",
            cpesData[0].cpes, []);

          var agreementTypeFilter = new window.treasury.filter.SingleSelect(
            "agreementTypeFilterIds", "agreementTypeFilterAdd",
            agreementTypesData[0].agreementTypes, []);

          var currencyFilter = new window.treasury.filter.SingleSelect(
            "currencyFilterIds", "currencyFilterAdd",
            currenciesData[0].currency, []);

          var nettingGroupFilter = new window.treasury.filter.SingleSelect(
            "nettingGroupFilterIds", "nettingGroupFilterAdd",
            nettingGroupsData[0].nettingGroups, []);

          var chargeTypeFilter = new window.treasury.filter.SingleSelect(
            "chargeTypeFilterIds", "chargeTypeFilterAdd",
            chargeTypesData[0].chargeTypes, []);

          for (var i = specialBuData[0].specialBusinessUnits.length - 1; i >= 0; i--) {
            buData[0].businessUnits.unshift(specialBuData[0].specialBusinessUnits[i]);
          }
          var businessUnitFilter = new window.treasury.filter.SingleSelect(
            "businessUnitFilterIds", "businessUnitFilterAdd",
            buData[0].businessUnits, []);

          for (var i = specialBundleData[0].specialBundles.length - 1; i >= 0; i--) {
            bundleData[0].bundles.unshift(specialBundleData[0].specialBundles[i]);
          }
          var bundleFilter = new window.treasury.filter.SingleSelect(
            "bundleFilterIds", "bundleFilterAdd",
            bundleData[0].bundles, []);

          for (var i = specialCaData[0].specialCustodianAccount.length - 1; i >= 0; i--) {
            caData[0].custodianAccounts.unshift(specialCaData[0].specialCustodianAccount[i]);
          }
          var custodianAccountFilter = new window.treasury.filter.SingleSelect(
            "custodianAccountFilterIds", "custodianAccountFilterAdd",
            caData[0].custodianAccounts, []);

          setStartOfMonthDateFilter();
          simulationFilterGroup = new window.treasury.filter.FilterGroup(null, [
            legalEntityFilter, cpeEntityFilter,
            agreementTypeFilter, currencyFilter,
            nettingGroupFilter, chargeTypeFilter, businessUnitFilter, bundleFilter, custodianAccountFilter
          ], $("#datePickerAdd"), null);
          if (typeof callback === 'function')
            callback();
          hideLoading("mainLoader");
        }
      );
  }

})();
