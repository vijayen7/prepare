"use strict";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.methodologyAccrualPostingAction = {
    loadResults: _loadResults,
    loadSimulationResults: _loadSimulationResults,
    saveSelectedRules: _saveSelectedRules,
  };

  var _simulatedAccrualPostingRuleGrid;

  function _saveSelectedRules() {
    treasury.interest.common.actions.saveSelectedRules("/treasury/financing/save-methodology-accrual-posting-rules",
      _simulatedAccrualPostingRuleGrid, function (){
        document.getElementById('accrualPostingDialog').visible = false;
      });
  }

  function _loadSimulationResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-methodology-accrual-posting-simulation",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    treasury.interest.common.actions.loadSimulationGrids(serviceCall, "existingAccrualPostingRuleGrid",
      "simulatedAccrualPostingRuleGrid", "accrualPostingExistingRuleLabel",
      "accrualPostingSimulatedRuleLabel", treasury.interest.methodologyAccrualPostingColumnsConfig
      .getColumns, treasury.interest.common.grid.getGridOptions,
      treasury.interest.common.grid.getSimulatedGridOptions,
      function() {
        document.getElementById("saveAccrualPostingRules").style.visibility = "visible";
      },
      function(gridVar) {
        _simulatedAccrualPostingRuleGrid = gridVar;
      })
  }

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-methodology-accrual-posting-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.interest.common.actions.loadSearchGrid(serviceCall, "methodologyAccrualPostingGrid",
      treasury.interest.methodologyAccrualPostingColumnsConfig.getColumns,
      treasury.interest.common.grid.getGridOptions);
  }
})();
