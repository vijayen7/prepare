"use strict";
var financingTermInRow;
var financingRowId;
(function() {
  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.agreementTermsGridOptions = {
    getResultOptions: _getResultOptions,
    getSimulatedResultOptions: _getSimulatedResultOptions
  };

  function _getResultOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRowOnClick: true,
      sortList: [{
        columnId: "agreementType",
        sortAsc: true
      }],
      onDblClick: function(item, rowObject$, isNotToggle) {
        showLoading("mainLoader");
        financingTermInRow = item.financing_term;
        financingRowId = item.id;
        var parameters = {};
        parameters["financingTermId"] = financingTermInRow;

        var serviceCall = $.ajax({
          url: "/treasury/financing/load-full-financing-term",
          data: parameters,
          type: "POST",
          dataType: "json"
        });
        serviceCall
          .done(function(data) {
            if (data && data.result) {
              if (document
                .getElementById("financingTermSidebar").state == 'collapsed') {
                document.getElementById(
                  "financingTermSidebar").toggle();
              }
              if(document.getElementById("calculatorFilterSidebar").state == 'expanded') {
                document.getElementById("calculatorFilterSidebar").toggle();
              }
              var parentArcLayout = document
                .getElementById('parentArcLayout');
              parentArcLayout.setLayout();
              resizeAllGrids();
              document.getElementById("financingTermsFixed").innerHTML = getFinancingTermsString(data.result);
              document.getElementById("financingTermsAttr").innerHTML = getFinancingAttributesString(data.attrResult);
              hideLoading("mainLoader");
            }

          });
        serviceCall.fail(function(xhr, text, errorThrown) {
          showErrorMessage("Unable to load full financing term.");
          hideLoading("mainLoader");
        });

      },
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      useAvailableScreenSpace: true,
      applyFilteringOnGrid: true,
      exportToExcel: true
    };

  }

  function _getSimulatedResultOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRowOnClick: true,
      sortList: [{
        columnId: "agreementType",
        sortAsc: true
      }],
      addCheckboxHeaderAsLastColumn: false,
      onDblClick: function(item, rowObject$, isNotToggle) {
        if (document.getElementById("financingTermSidebar").state == 'collapsed') {
          document.getElementById("financingTermSidebar").toggle();
        }
        var parentArcLayout = document
          .getElementById('parentArcLayout');
        parentArcLayout.setLayout();
        if (agreementTermsGrid != null) {
          agreementTermsGrid.resizeCanvas();
        }
        agreementTermsSimulatedRulesGrid.resizeCanvas();
        agreementTermsExistingRulesGrid.resizeCanvas();
        finAttrString = item["fin_attr_string"];

        document.getElementById("financingTermsFixed").innerHTML = getFinancingTermsString(item);
        document.getElementById("financingTermsAttr").innerHTML = getFinancingAttributesString(finAttrString);

      },
      rowSelection: [{
        highlightClass: "highlight-strong",
        highlightRowOnClick: false
      }],
      checkboxHeader: [{
        controlChildSelection: false,
        isSelectAllCheckboxRequired: true
      }],
      exportToExcel: true
    };
  }

  function resizeAllGrids() {
    if (agreementTermsGrid != null) {
      agreementTermsGrid.resizeCanvas();
    }
    if (agreementTermsExistingRulesGrid != null) {
      agreementTermsExistingRulesGrid.resizeCanvas();
    }
    if (agreementTermsSimulatedRulesGrid != null) {
      agreementTermsSimulatedRulesGrid.resizeCanvas();
    }
  }

  function getFinancingTermsString(item) {
    var financingTerms = "<table callspacing='5'><tr><td>"
    + "<strong>Credit Base Rate</strong></td><td>"
    + (item.credit_base_rate == 'null' ? "N/A" : item.credit_base_rate)
    + "</td></tr>" + "<tr><td><strong>Debit Base Rate</strong></td><td>"
    + (item.debit_base_rate == 'null' ? "N/A" : item.debit_base_rate)
    + "</td></tr>" + "<tr><td><strong>Rebate Base Rate</strong></td><td>"
    + (item.rebate_base_rate == 'null' ? "N/A" : item.rebate_base_rate)
    + "</td></tr>" + "<tr><td><strong>Credit Spread</strong></td><td>"
    + item.credit_spread + "</td></tr>"
    + "<tr><td><strong>Debit Spread</strong></td><td>"
    + item.debit_spread + "</td></tr>"
    + "<tr><td><strong>GC Rate</strong></td><td>"
    + (item.gc_rate == 'null' ? "N/A" : item.gc_rate)
    + "</td></tr>" + "<tr><td><strong>Credit Accrual</strong></td><td>"
    + item.credit_accrual + "</td></tr>"
    + "<tr><td><strong>Debit Accrual</strong></td><td>"
    + item.debit_accrual + "</td></tr></table>";
    return financingTerms;
  }

  function getFinancingAttributesString(finAttrString) {
    var strHtml = "";
    if (finAttrString == '' || finAttrString == null) {
      strHtml = "No attributes associated with this term.";
    } else {
      var attrs = finAttrString.split("|");
      strHtml = "<table cellspacing='5'>";
      for (var i = 0; i < attrs.length; i++) {
        if (attrs[i].split("-")[2] != 'null') {
          strHtml += "<tr><td><strong>" + attrs[i].split("-")[1] + "</strong></td><td>" + (attrs[i].split("-")[2] == 'null' ? "N/A" : attrs[i].split("-")[2])

          + "</td></tr>";
        }
      }
      strHtml += "</table>";
    }
    return strHtml;
  }
})();
