"use strict";
var agreementTermsGrid;
var agreementTermsSimulatedRulesGrid;
var agreementTermsExistingRulesGrid;
var agreementTermsToBeAddedRulesGrid;
var finAttrString = "";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.agreementTermsActions = {
    loadAgreementTermsResults: _loadAgreementTermsResults,
    loadSimulatedRules: _loadSimulatedRules,
    loadBaseRates: _loadBaseRates,
    updateAgreeementTerms: _updateAgreeementTerms
  };

  function _updateAgreeementTerms(parameters) {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/update-agreement-terms",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        hideLoading("mainLoader");
        clearGrid("agreementTermsSimulatedRulesGrid");
        clearGrid("agreementTermsExistingRulesGrid");
        clearGrid("agreementTermsGrid");
        clearGrid("agreementTermsToBeAddedRulesGrid");
        document.getElementById("agreementTermsSimulatedRulesGrid").innerHTML = "";
        document.getElementById("agreementTermsExistingRulesGrid").innerHTML = "";
        document.getElementById("agreementTermsGrid").innerHTML = "";
        agreementTermsRuleIds = {};
        agreementTermsSelectedRules = [];
        selectedAttrIds = [];
        document.getElementById("agreement-terms-confirm-panel").style.display = "block";
        if (data && data.resultList && data.resultList.length) {
          if (!$("#agreement-terms-search").prev().is("center")) {
            $(getFormattedMessage('Terms have been saved.', 'success'))
              .insertBefore("#agreementTermsToBeAddedRulesGrid");
          }
          agreementTermsToBeAddedRulesGrid = new dportal.grid.createGrid(
            $("#agreementTermsToBeAddedRulesGrid"),
            data["resultList"],
            treasury.interest.agreementTermsColumnsConfig
            .getResultColumns(),
            treasury.interest.agreementTermsGridOptions
            .getResultOptions());
          resizeCanvasOnGridChange(agreementTermsToBeAddedRulesGrid,
            'agreementTermsToBeAddedRulesGrid');

          document.getElementById("currency-add-edit-panel").style.display = "block";
          document.getElementById("agreementTermsConfirmSave").disabled = true;
          document.getElementById("agreementTermsDeleteSelected").disabled = true;
        } else if (data && data.errorMessage) {
          showErrorMessage(data.errorMessage);
          console
            .log("Error in saving agreement terms.");
        } else if (data) {
          document.getElementById("agreementTermsToBeAddedRulesGrid").innerHTML = getFormattedMessage('No rules found.', '');
        }
      });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        hideLoading("mainLoader");
        clearGrid("agreementTermsSimulatedRulesGrid");
        clearGrid("agreementTermsExistingRulesGrid");
        clearGrid("agreementTermsGrid");
        clearGrid("agreementTermsToBeAddedRulesGrid");
        document.getElementById("agreementTermsSimulatedRulesGrid").innerHTML = "";
        document.getElementById("agreementTermsExistingRulesGrid").innerHTML = "";
        document.getElementById("agreementTermsGrid").innerHTML = "";
        agreementTermsRuleIds = {};
        agreementTermsSelectedRules = [];
        selectedAttrIds = [];
        $(getFormattedMessage('Error saving Terms.', 'critical')).insertBefore(
          "#agreementTermsToBeAddedRulesGrid");
        document.getElementById("currency-add-edit-panel").style.display = "block";
        document.getElementById("agreementTermsConfirmSave").disabled = true;
        document.getElementById("agreementTermsDeleteSelected").disabled = true;
      });

  }

  function _loadBaseRates(currencyId) {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-base-rate-filter-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall.done(function(data) {
      hideLoading("mainLoader");
    });
    serviceCall.fail(function(xhr, text, errorThrown) {
      hideLoading("mainLoader");
      console.log("Error occurred while retrieving base rate filters.");
    });

  }

  function _loadAgreementTermsResults(parameters) {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/load-agreement-terms-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        clearGrid("agreementTermsGrid");
        hideLoading("mainLoader");
        if (document.getElementById("financingTermSidebar").state == 'expanded') {
        	document.getElementById("financingTermsFixed").innerHTML = '';
        	document.getElementById("financingTermsAttr").innerHTML = '';
            document.getElementById("financingTermSidebar").toggle();
        }
        if (data && data.resultList && data.resultList.length) {
          agreementTermsGrid = new dportal.grid.createGrid(
            $("#agreementTermsGrid"), data["resultList"],
            treasury.interest.agreementTermsColumnsConfig
            .getSearchResultColumns(),
            treasury.interest.agreementTermsGridOptions
            .getResultOptions());
          resizeCanvasOnGridChange(agreementTermsGrid,
            'agreementTermsGrid');
          if (!$("#agreement-terms-search").prev().is("center")) {
            $(getFormattedMessage('Double click on row to view associated terms.', '')).insertBefore(
              "#agreement-terms-search");
          }
        } else if (data && data.errorMessage) {
            if ($("#agreement-terms-search").prev().is("center")) {
                $("#agreement-terms-search").prev().remove();
            }
          showErrorMessage(data.errorMessage);
          console
            .log("Error in fetching matching agreement terms.");
        } else if (data) {
            if ($("#agreement-terms-search").prev().is("center")) {
                $("#agreement-terms-search").prev().remove();
            }
          document.getElementById("agreementTermsGrid").innerHTML = getFormattedMessage('No rules found.', '');
        }
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      clearGrid("agreementTermsGrid");
      hideLoading("mainLoader");
      console.log("Error occurred while retrieving agreement terms.");
    });
  }

  function _loadSimulatedRules(parameters) {
    showLoading("mainLoader");
    var serviceCall = $.ajax({
      url: "/treasury/financing/agreement-terms-simulate-rules",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        clearGrid("agreementTermsSimulatedRulesGrid");
        clearGrid("agreementTermsExistingRulesGrid");
        hideLoading("mainLoader");
        if (data && data.simulatedList && data.simulatedList.length) {
          agreementTermsSimulatedRulesGrid = new dportal.grid.createGrid(
            $("#agreementTermsSimulatedRulesGrid"),
            data["simulatedList"],
            treasury.interest.agreementTermsColumnsConfig
            .getResultColumns(),
            treasury.interest.agreementTermsGridOptions
            .getSimulatedResultOptions());
          resizeCanvasOnGridChange(agreementTermsSimulatedRulesGrid,
            'agreementTermsSimulatedRulesGrid');
          finAttrString = (data.simulatedList[0].id.split(",")[data.simulatedList[0].id
            .split(",").length - 1]).split(":")[1];
        } else if (data && data.errorMessage) {
          showErrorMessage(data.errorMessage);
          console.log("No terms were simulated.");
        } else {
          document
            .getElementById("agreementTermsSimulatedRulesGrid").innerHTML = getFormattedMessage('No Financing Methodology Term with given parameters.', '');
        }
        if (data && data.existingList && data.existingList.length) {
          $(getFormattedMessage('Existing Terms.', ''))
            .insertBefore(
              "#agreementTermsExistingRulesGrid");
          agreementTermsExistingRulesGrid = new dportal.grid.createGrid(
            $("#agreementTermsExistingRulesGrid"),
            data["existingList"],
            treasury.interest.agreementTermsColumnsConfig
            .getRuleColumns(),
            treasury.interest.agreementTermsGridOptions
            .getResultOptions());
          resizeCanvasOnGridChange(agreementTermsExistingRulesGrid,
            'agreementTermsExistingRulesGrid');
        } else if (data && data.errorMessage) {
          showErrorMessage(data.errorMessage);
          console.log("No existing Agreement Terms.");
        }
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      clearGrid("agreementTermsSimulatedRulesGrid");
      hideLoading("mainLoader");
      console.log("Error in fetching Agreement Terms.");
    });
  }

})();
