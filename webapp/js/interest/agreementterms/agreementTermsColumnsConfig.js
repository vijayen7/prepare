"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.agreementTermsColumnsConfig = {
        getSearchResultColumns: _getSearchResultColumns,
        getResultColumns: _getResultColumns
  };

  function _getSearchResultColumns() {
    return [{
        id: "agreementType",
        type: "text",
        name: "Agreement Type",
        field: "agreement_type_id",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }, {
        id: "legalEntity",
        type: "text",
        name: "Legal Entity",
        field: "legal_entity_id",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }, {
        id: "cpe",
        type: "text",
        name: "Counter Party Entity",
        field: "cpe_id",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }, {
        id: "currency",
        type: "text",
        name: "Currency",
        field: "currency",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }, {
        id: "nettingGroup",
        type: "text",
        name: "Netting Group",
        field: "netting_group",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      },

      {
        id: "assetClassGroup",
        type: "text",
        name: "Asset Class Group",
        field: "asset_class_group_id",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }, {
        id: "bandwidthGroup",
        type: "text",
        name: "Bandwidth Group",
        field: "bandwidth_group",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }, {
        id: "creditBaseRate",
        type: "text",
        name: "Credit Base Rate",
        field: "credit_base_rate",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }, {
          id: "debitBaseRate",
          type: "text",
          name: "Debit Base Rate",
          field: "debit_base_rate",
          sortable: true,
          headerCssClass: "aln-rt b",
          excelFormatter: "#,##0"
        }, {
        id: "creditSpread",
        type: "text",
        name: "Credit Spread",
        field: "credit_spread",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }, {
          id: "debitSpread",
          type: "text",
          name: "Debit Spread",
          field: "debit_spread",
          sortable: true,
          headerCssClass: "aln-rt b",
          excelFormatter: "#,##0"
        }
    ];

  }
  
  function _getResultColumns() {
	  return [{
	        id: "agreementType",
	        type: "text",
	        name: "Agreement Type",
	        field: "agreement_type_id",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      }, {
	        id: "legalEntity",
	        type: "text",
	        name: "Legal Entity",
	        field: "legal_entity_id",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      }, {
	        id: "cpe",
	        type: "text",
	        name: "Counter Party Entity",
	        field: "cpe_id",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      }, {
	        id: "currency",
	        type: "text",
	        name: "Currency",
	        field: "currency",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      }, {
	        id: "nettingGroup",
	        type: "text",
	        name: "Netting Group",
	        field: "netting_group",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      },

	      {
	        id: "assetClassGroup",
	        type: "text",
	        name: "Asset Class Group",
	        field: "asset_class_group_id",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      }, {
	        id: "bandwidthGroup",
	        type: "text",
	        name: "Bandwidth Group",
	        field: "bandwidth_group",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      }, {
	        id: "baseRate",
	        type: "text",
	        name: "Base Rate",
	        field: "credit_base_rate",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      }, {
	        id: "spread",
	        type: "text",
	        name: "Spread",
	        field: "credit_spread",
	        sortable: true,
	        headerCssClass: "aln-rt b",
	        excelFormatter: "#,##0"
	      }
	    ];
  }

})();
