"use strict";

var agreementTermsRuleIds = {};
var agreementTermsSelectedRules = new Array();
var agreementTermsToBeAddedGrid;
var agreementTermsFilterGroup;
var currencyEditFilter;
var baseRateCurrencyEditFilter;
var baseRateEditFilter;
var creditBaseRateEditFilter;
var debitBaseRateEditFilter;
var rebateBaseRateEditFilter;
var legalEntityEditFilter;
var cpeEntityEditFilter;
var agreementTypeEditFilter;
var nettingGroupEditFilter;
var assetClassGroupEditFilter;
var accrualConventionEditSelect;
var accrualConventionCreditEditSelect;
var accrualConventionDebitEditSelect;
var bandwidthGroupEditSelect;
var attrEditFilter;
var selectedAttrIds = [];
var selectedAttrNames = [];
var validFrom;
var baseRateToggle = false;
var accrualConvToggle = false;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.agreementTerms = {
    registerHandlers: _registerHandlers,
    loadAgreementTermsSearchResults: _loadAgreementTermsSearchResults,
    loadEditRulePanel: _loadEditRulePanel,
    loadBaseRateOptions: _loadBaseRateOptions,
    loadAccrualConventionOptions: _loadAccrualConventionOptions,
    simulateRule: _simulateRule,
    saveRulesForUpdate: _saveRulesForUpdate,
    cancelSimulation: _cancelSimulation
  };

  function _registerHandlers() {
    document.getElementById("agreementTermsEditRule").onclick = treasury.interest.agreementTerms.loadEditRulePanel;
    document.getElementById("baseRateOptionButton").onclick = treasury.interest.agreementTerms.loadBaseRateOptions;
    document.getElementById("accrualConventionOptionButton").onclick = treasury.interest.agreementTerms.loadAccrualConventionOptions;
    document.getElementById("agreementTermsSimulateRule").onclick = treasury.interest.agreementTerms.simulateRule;
    document.getElementById("agreementTermsSaveRules").onclick = treasury.interest.agreementTerms.saveRulesForUpdate;
    document.getElementById("agreementTermsCurrencyEditFilter")
      .addEventListener("change", loadBaseRatesOnCurrency);
    document.getElementById("agreementTermsBaseRateCurrencyEditFilter")
      .addEventListener("change", loadBaseRatesOnCurrency);
    document.getElementById("agreementTermsAttributeEditFilter")
      .addEventListener("change", addNewInputAttrField);
    document.getElementById("addBandwidthGroup").onclick = treasury.interest.bandwidthGroupActions.addBandwidthGroupDialog
    document.getElementById("agreementTermsCancelSimulation").onclick = treasury.interest.agreementTerms.cancelSimulation;
  }

  function addNewInputAttrField() {
    var selectedAttr = document
      .getElementById("agreementTermsAttributeEditFilter");
    var attrId = attrEditFilter.getSelectedId();
    if (selectedAttrIds.indexOf(attrId) <= -1) {
      selectedAttrIds.push(attrId);
      selectedAttrNames.push(selectedAttr.value.value);
      var br = document.createElement('br');
      document.getElementById('attributeInputFields').appendChild(br);
      var htmlObject = document.createElement('div');
      htmlObject.className = "row";
      var textNode = "<label>" + selectedAttr.value.value + "</label><input style = 'float:right;display:inline' id='attr" + attrId + "' type='text' />";
      htmlObject.innerHTML = textNode;
      document.getElementById('attributeInputFields').appendChild(
        htmlObject);

    }
  }

  function loadBaseRatesOnCurrency() {
    var currencyId;
    if (document.getElementById("agreementTermsBaseRateCurrencyEditFilter") != null) {
      currencyId = baseRateCurrencyEditFilter.getSelectedId();
    }
    if (currencyId <= 0) {
      currencyId = currencyEditFilter.getSelectedId();
    }
    showLoading("mainLoader");
    $
      .when(treasury.loadfilter.baseRates(currencyId))
      .done(
        function(baseRatesData) {
          if (baseRatesData['baseRates'].length == 0) {
            baseRatesData['baseRates'].push({
              0: 0,
              1: '0 - 0 - 0'
            })
          }
          baseRateEditFilter = new window.treasury.filter.SingleSelect(
            "baseRateFilterIds", "agreementTermsBaseRateEditFilter",
            baseRatesData["baseRates"], []);

          creditBaseRateEditFilter = new window.treasury.filter.SingleSelect(
            "creditBaseRateFilterIds",
            "agreementTermsCreditBaseRateEditFilter",
            baseRatesData["baseRates"], []);

          debitBaseRateEditFilter = new window.treasury.filter.SingleSelect(
            "debitBaseRateFilterIds",
            "agreementTermsDebitBaseRateEditFilter",
            baseRatesData["baseRates"], []);

          rebateBaseRateEditFilter = new window.treasury.filter.SingleSelect(
            "rebateBaseRateFilterIds",
            "agreementTermsRebateBaseRateEditFilter",
            baseRatesData["baseRates"], []);
        });

    hideLoading("mainLoader");

  }

  function _loadBaseRateOptions() {
    if (document.getElementById("agreementTermsBaseRateOption").style.display == 'block') {
      baseRateToggle = false;
      document.getElementById("agreementTermsBaseRateOption").style.display = 'none';
    } else {
      baseRateToggle = true;
      document.getElementById("agreementTermsBaseRateOption").style.display = 'block';
    }
  }

  function _loadAccrualConventionOptions() {
    if (document.getElementById("agreementTermsAccConventionOption").style.display == 'block') {
      accrualConvToggle = false;
      document.getElementById("agreementTermsAccConventionOption").style.display = 'none';
    } else {
      accrualConvToggle = true;
      document.getElementById("agreementTermsAccConventionOption").style.display = 'block';
    }
  }

  function _loadAgreementTermsSearchResults() {

    clearGrid("agreementTermsToBeAddedRulesGrid");
    clearGrid("agreementTermsSimulatedRulesGrid");
    clearGrid("agreementTermsExistingRulesGrid");
    var parameters = searchFilterGroup.getSerializedParameterMap();

    document.getElementById("agreement-terms-add-edit-panel").style.display = 'none';
    document.getElementById("agreement-terms-confirm-panel").style.display = 'none';

    document.getElementById("agreement-terms-confirm-panel").style.display = "none";

    if (document.getElementById("calculatorFilterSidebar").state == 'expanded') {
      document.getElementById("calculatorFilterSidebar").toggle();
    }
    if (document.getElementById("financingTermSidebar").style.display == 'none') {
      document.getElementById("financingTermSidebar").style.display = 'block';
    }

    changeStateOfPanel("agreementTermsSearchResultPanel", "collapsed");

    var formattedDate = getFormattedDate('datePicker');
    parameters.dateString = formattedDate;

    treasury.interest.agreementTermsActions
      .loadAgreementTermsResults(parameters);

  }

  function _loadEditRulePanel() {
    clearGrid("agreementTermsSimulatedRulesGrid")
    clearGrid("agreementTermsExistingRulesGrid");
    clearGrid("agreementTermsToBeAddedRulesGrid");
    document.getElementById("agreement-terms-confirm-panel").style.display = 'none';
    if (document.getElementById("calculatorFilterSidebar").state == 'expanded') {
      document.getElementById("calculatorFilterSidebar").toggle();
    }

    if (document.getElementById("financingTermSidebar").state == 'expanded') {
      document.getElementById("financingTermSidebar").toggle();
    }

    if (document.getElementById("financingTermSidebar").style.display == 'none') {
      document.getElementById("financingTermSidebar").style.display = 'block';
    }

    document.getElementById("agreement-terms-add-edit-panel").style.display = "block";
    baseRateToggle = false;
    accrualConvToggle = false;
    document.getElementById("agreementTermsCreditSpreadInput").value = "";
    document.getElementById("agreementTermsDebitSpreadInput").value = "";
    document.getElementById("agreementTermsCreditAccConventionInput").value = "";
    document.getElementById("agreementTermsAccConventionInput").value = "";
    document.getElementById("agreementTermsDebitAccConventionInput").value = "";
    document.getElementById("agreementTermsGcRateInput").value = "";
    document.getElementById('attributeInputFields').innerHTML = "";

    selectedAttrIds = [];
    selectedAttrNames = [];

    if ($("#agreement-terms-search").prev().is("center")) {
      $("#agreement-terms-search").prev().remove();
    }
    changeStateOfPanel('agreementTermsSearchResultPanel', 'expanded');
    changeStateOfPanel('agreementTermsEditRulesPanel', 'collapsed');
    changeStateOfPanel('agreementTermsSimulatedRulesPanel', 'expanded');

    $
      .when(treasury.loadfilter.legalEntities(),
        treasury.loadfilter.cpes(),
        treasury.loadfilter.loadAgreementTypes(),
        treasury.loadfilter.currencies(),
        treasury.loadfilter.nettingGroups(),
        treasury.loadfilter.assetClassGroups(),
        treasury.loadfilter.agmtTermsAttributes(),
        treasury.loadfilter.accrualConventions(),
        treasury.loadfilter.bandwidthGroups())
      .done(
        function(legalEntitiesData, cpesData,
          agreementTypesData, currenciesData,
          nettingGroupsData, assetClassGroupsData,
          agmtTermsAttributesData,
          accrualConventionsData, bandwidthGroupData) {
          hideLoading("mainLoader");
          legalEntityEditFilter = new window.treasury.filter.MultiSelect(
            "legalEntityFilterIds",
            "agreementTermsLegalEntityEditFilter",
            legalEntitiesData[0]["descoEntities"], []);

          cpeEntityEditFilter = new window.treasury.filter.SingleSelect(
            "cpeFilterIds",
            "agreementTermsCpeEntityEditFilter",
            cpesData[0]["cpes"], []);

          agreementTypeEditFilter = new window.treasury.filter.SingleSelect(
            "agreementTypeFilterIds",
            "agreementTermsAgreementTypeEditFilter",
            agreementTypesData[0]["agreementTypes"], []);
          currenciesData[0]["currency"]
            .unshift([0, 'Select']);
          currencyEditFilter = new window.treasury.filter.SingleSelect(
            "currencyFilterIds",
            "agreementTermsCurrencyEditFilter",
            currenciesData[0]["currency"], []);

          nettingGroupEditFilter = new window.treasury.filter.MultiSelect(
            "nettingGroupFilterIds",
            "agreementTermsNettingGroupEditFilter",
            nettingGroupsData[0]["nettingGroups"], []);

          assetClassGroupEditFilter = new window.treasury.filter.SingleSelect(
            "assetClassGroupFilterId",
            "agreementTermsAssetClassGroupEditFilter",
            assetClassGroupsData[0]["assetClassGroups"], []);
          attrEditFilter = new window.treasury.filter.SingleSelect(
            "attributeIds",
            "agreementTermsAttributeEditFilter",
            agmtTermsAttributesData[0]["attributes"], []);

          accrualConventionEditSelect = new window.treasury.filter.SingleSelect(
            "",
            "agreementTermsAccConventionInput",
            accrualConventionsData[0]["accrualConvention"], []);
          accrualConventionCreditEditSelect = new window.treasury.filter.SingleSelect(
            "",
            "agreementTermsCreditAccConventionInput",
            accrualConventionsData[0]["accrualConvention"], []);
          accrualConventionDebitEditSelect = new window.treasury.filter.SingleSelect(
            "",
            "agreementTermsDebitAccConventionInput",
            accrualConventionsData[0]["accrualConvention"], []);

          baseRateCurrencyEditFilter = new window.treasury.filter.SingleSelect(
            "",
            "agreementTermsBaseRateCurrencyEditFilter",
            currenciesData[0]["currency"], []);

          bandwidthGroupEditSelect = new window.treasury.filter.SingleSelect(
            "", "agreementTermsBandwidthGroupInput",
            bandwidthGroupData[0]["bandwidthGroup"], []);
          var fromDate = new Date();
          fromDate;
          fromDate.setDate(1);

          var maxDate = new Date();
          maxDate.setYear(2038);
          maxDate.setMonth(0);
          maxDate.setDate(1);

          var minDate = new Date();
          minDate.setYear(1900);
          minDate.setMonth(0);
          minDate.setDate(1);
          var options = {
            datepickerOptions: {}
          };

          options.maxDate = maxDate;
          options.minDate = minDate;
          options.showOn = "both";
          options.buttonText = null;
          options.icon = $("#calendarIconIdFromDate");
          options.buttonImage = null;
          options.defaultDate = fromDate;

          // Following is a hack as setupDaterangepicker is
          // from dportal library
          // and expects dportal.app is defined.
          dportal = dportal || {};
          dportal.app = dportal.app || {};

          setupDatepicker({
            input: $("#agreementTermsFromDatePicker"),
            errorDiv: $("#datePickerA"),
            options: options
          });
        });
  }

  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  function _simulateRule() {

    agreementTermsFilterGroup = new window.treasury.filter.FilterGroup([
      legalEntityEditFilter, nettingGroupEditFilter
    ], [
      agreementTypeEditFilter, cpeEntityEditFilter,
      assetClassGroupEditFilter, currencyEditFilter
    ], null, null);
    var parameters = agreementTermsFilterGroup.getSerializedParameterMap();
    if (parameters.agreementTypeFilterIds == "-1") {
      document.getElementById("agreementTermsError").innerHTML = 'Agreement Type cannot be empty.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (parameters.legalEntityFilterIds == "-1") {
      document.getElementById("agreementTermsError").innerHTML = 'Legal Entity cannot be empty.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (parameters.cpeFilterIds == "-1") {
      document.getElementById("agreementTermsError").innerHTML = 'Counter Party Entity cannot be empty.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (parameters.nettingGroupFilterIds == "-1") {
      document.getElementById("agreementTermsError").innerHTML = 'Netting Group cannot be empty.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (parameters.currencyFilterIds == "-1") {
      document.getElementById("agreementTermsError").innerHTML = 'Currency cannot be empty.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (document.getElementById("agreementTermsFromDatePicker").innerHTML != "") {
      document.getElementById("agreementTermsError").innerHTML = 'Please select a valid date.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (document.getElementById("agreementTermsCreditSpreadInput").value
      .trim() == "") {
      document.getElementById("agreementTermsError").innerHTML = 'Credit Spread cannot be empty.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (!isNumeric(document
        .getElementById("agreementTermsCreditSpreadInput").value.trim())) {
      document.getElementById("agreementTermsError").innerHTML = 'Credit Spread input is not valid.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (document.getElementById("agreementTermsDebitSpreadInput").value
      .trim() == "") {
      document.getElementById("agreementTermsError").innerHTML = 'Debit Spread cannot be empty.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (!isNumeric(document
        .getElementById("agreementTermsDebitSpreadInput").value.trim())) {
      document.getElementById("agreementTermsError").innerHTML = 'Debit Spread input is not valid.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (document.getElementById("agreementTermsGcRateInput").value.trim() == "") {
      document.getElementById("agreementTermsError").innerHTML = 'GC Rate cannot be empty.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (!isNumeric(document.getElementById("agreementTermsGcRateInput").value
        .trim())) {
      document.getElementById("agreementTermsError").innerHTML = 'GC Rate input not valid.';
      document.getElementById("agreementTermsError").style.visibility = "visible";
      return;
    }

    if (accrualConvToggle) {
      if (document
        .getElementById("agreementTermsCreditAccConventionInput").value == null || document
        .getElementById("agreementTermsDebitAccConventionInput").value == null) {
        document.getElementById("agreementTermsError").innerHTML = 'Credit/Debit Accrual Convention cannot be empty.';
        document.getElementById("agreementTermsError").style.visibility = "visible";
        return;
      }
    } else {
      if (document.getElementById("agreementTermsAccConventionInput").value == null) {
        document.getElementById("agreementTermsError").innerHTML = 'Accrual Convention cannot be empty.';
        document.getElementById("agreementTermsError").style.visibility = "visible";
        return;
      }
    }

    if (!baseRateToggle) {
      agreementTermsFilterGroup = new window.treasury.filter.FilterGroup(
        [legalEntityEditFilter, nettingGroupEditFilter], [
          agreementTypeEditFilter, cpeEntityEditFilter,
          assetClassGroupEditFilter, currencyEditFilter,
          baseRateEditFilter
        ], null, null);
    } else {
      agreementTermsFilterGroup = new window.treasury.filter.FilterGroup(
        [legalEntityEditFilter, nettingGroupEditFilter], [
          agreementTypeEditFilter, cpeEntityEditFilter,
          assetClassGroupEditFilter, currencyEditFilter,
          creditBaseRateEditFilter, debitBaseRateEditFilter,
          rebateBaseRateEditFilter
        ], null, null);

    }

    parameters = agreementTermsFilterGroup.getSerializedParameterMap();

    if (baseRateToggle) {
      if (parameters.creditBaseRateFilterIds == "-1") {
        document.getElementById("agreementTermsError").innerHTML = 'Credit Base Rate cannot be empty.';
        document.getElementById("agreementTermsError").style.visibility = "visible";
        return;
      }
      if (parameters.debitBaseRateFilterIds == "-1") {
        document.getElementById("agreementTermsError").innerHTML = 'Debit Base Rate cannot be empty.';
        document.getElementById("agreementTermsError").style.visibility = "visible";
        return;
      }
      if (parameters.rebateBaseRateFilterIds == "-1") {
        document.getElementById("agreementTermsError").innerHTML = 'Rebate Base Rate cannot be empty.';
        document.getElementById("agreementTermsError").style.visibility = "visible";
        return;
      }
    } else {
      if (parameters.baseRateFilterIds == "-1") {
        document.getElementById("agreementTermsError").innerHTML = 'Base Rate cannot be empty.';
        document.getElementById("agreementTermsError").style.visibility = "visible";
        return;
      }
    }

    var legalEntityAll = parameters.legalEntityFilterIds.split(",");
    var nettingGroupAll = parameters.nettingGroupFilterIds.split(",");

    var legalEntityLength = document
      .getElementById("agreementTermsLegalEntityEditFilter").data.length;
    var nettingGroupLength = document
      .getElementById("agreementTermsNettingGroupEditFilter").data.length;
    if (legalEntityAll.length == legalEntityLength) {
      parameters.legalEntityFilterIds = null;
    }

    if (nettingGroupAll.length == nettingGroupLength) {
      parameters.nettingGroupFilterIds = null;
    }

    validFrom = getFormattedDate('agreementTermsFromDatePicker');
    parameters.validFrom = validFrom;
    parameters.creditSpread = document
      .getElementById("agreementTermsCreditSpreadInput").value;
    parameters.debitSpread = document
      .getElementById("agreementTermsDebitSpreadInput").value;
    if (accrualConvToggle) {
      parameters.creditAccrualConvention = document
        .getElementById("agreementTermsCreditAccConventionInput").value.key;
      parameters.debitAccrualConvention = document
        .getElementById("agreementTermsDebitAccConventionInput").value.key;
    } else {
      parameters.creditAccrualConvention = document
        .getElementById("agreementTermsAccConventionInput").value.key;
      parameters.debitAccrualConvention = document
        .getElementById("agreementTermsAccConventionInput").value.key;
    }
    parameters.gcRate = document
      .getElementById("agreementTermsGcRateInput").value;
    if (baseRateToggle) {
      parameters.creditBaseRateFilterIds = parameters.creditBaseRateFilterIds;
      parameters.debitBaseRateFilterIds = parameters.debitBaseRateFilterIds;
      parameters.rebateBaseRateFilterIds = parameters.rebateBaseRateFilterIds;
    } else {
      parameters.creditBaseRateFilterIds = parameters.baseRateFilterIds;
      parameters.debitBaseRateFilterIds = parameters.baseRateFilterIds;
      parameters.rebateBaseRateFilterIds = parameters.baseRateFilterIds;
    }

    if (document.getElementById("agreementTermsBandwidthGroupInput").value) {
      parameters.bandwidthGroupFilterId = document
        .getElementById("agreementTermsBandwidthGroupInput").value.key;
      parameters.bandwidthGroupAbbrev = document
        .getElementById("agreementTermsBandwidthGroupInput").value.value;
    }

    var finAttrs = {};
    var i;
    var j;
    for (j = 0; j < selectedAttrIds.length; j++) {
      if (!isNumeric(document.getElementById('attr' + selectedAttrIds[j]).value)) {
        document.getElementById("agreementTermsError").innerHTML = 'Attribute values have to be numeric.';
        document.getElementById("agreementTermsError").style.visibility = "visible";
        return;
      }
    }

    var attrList = document
      .getElementById("agreementTermsAttributeEditFilter").data;
    for (i = 0; i < attrList.length; i++) {
      var data;
      var str = attrList[i].value;

      if (document.getElementById('attr' + attrList[i].key) != null) {
        data = document.getElementById('attr' + attrList[i].key).value;
        if (!str.includes('SMV') && !str.includes('GC') && !isInteger(data)) {
          document.getElementById("agreementTermsError").innerHTML = selectedAttrNames[i] + ' has to be an integer value.';
          document.getElementById("agreementTermsError").style.visibility = "visible";
          return;
        }
      } else {
        data = 'null';
      }
      str = str.replace('SMV', 'smv');
      str = str.replace('GC', 'gc');
      str = str.replace(/ /g, '');
      str = str.charAt(0).toLowerCase() + str.substr(1);
      parameters[str] = attrList[i].key + "-" + attrList[i].value + "-" + data;
    }

    document.getElementById("agreementTermsSaveRules").disabled = false;
    document.getElementById("agreementTermsError").style.visibility = "hidden";
    clearGrid("agreementTermsToBeAddedRulesGrid");
    clearGrid("agreementTermsSimulatedRulesGrid");
    clearGrid("agreementTermsExistingRulesGrid");
    changeStateOfPanel('agreementTermsEditRulesPanel', '');
    changeStateOfPanel('agreementTermsSimulatedRulesPanel', 'collapsed');
    treasury.interest.agreementTermsActions.loadSimulatedRules(parameters);
  }

  function _saveRulesForUpdate() {
    var rules = "";
    var parameters = {};
    var selectedRows = agreementTermsSimulatedRulesGrid.selectedRowIds();
    if (selectedRows.length < 1) {
      showErrorMessage("No simulation has been selected.");
      return;
    }

    for (var row in selectedRows) {
      rules = rules + selectedRows[row] + ";";
    }

    parameters["rowIds"] = rules.substring(0, rules.length - 1);
    changeStateOfPanel("agreementTermsSimulatedRulesPanel", "");
    treasury.interest.agreementTermsActions
      .updateAgreeementTerms(parameters);
    currencyRuleIds = {};
    currencySelectedRules = [];
  }

  function isInteger(data) {
    return (data == parseInt(data, 10));
  }

  function _cancelSimulation() {
    document.getElementById("agreementTypeEditFilter").value = "";
    document.getElementById("legalEntityEditFilter").value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById("cpeEntityEditFilter").value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById("nettingGroupEditFilter").value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById("currencyEditFilter").value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById("calculatorGroupEditFilter").value = null;

    document.getElementById("agreementTermsSaveRules").innerHTML = "Save Agreement Terms";
    clearGrid("agreementTermsSimulatedRulesGrid");
    clearGrid("agreementTermsExistingRulesGrid");
    clearGrid("agreementTermsToBeAddedRulesGrid");
    document.getElementById("agreementTermsSimulatedRulesGrid").innerHTML = "";
    document.getElementById("agreementTermsExistingRulesGrid").innerHTML = "";
    document.getElementById("agreementTermsToBeAddedRulesGrid").innerHTML = "";

    changeStateOfPanel('agreementTermsEditRulesPanel', 'expanded');
    changeStateOfPanel('agreementTermsSimulatedRulesPanel', 'expanded');

    document.getElementById("agreement-terms-confirm-panel").style.display = 'none';
    document.getElementById("agreement-terms-add-edit-panel").style.display = "none";

    changeStateOfPanel('agreementTermsSearchResultPanel', 'collapsed');
    document.getElementById("calculatorFilterSidebar").toggle();
    resizeCanvasOnGridChange(agreementTermsGrid, 'agreementTermsGrid');
    ruleIds = {};
    selectedRules = [];
    parameters["ruleIds"] = null;
  }
})();
