"use strict";
var searchFilterGroup;

(function() {

  window.treasury = window.treasury || {};
  window.treasury.interest = window.treasury.interest || {};
  window.treasury.interest.landing = {
    loadLandingView: _loadLandingViewFilters,
    loadSearchResults: _loadSearchResults,
    resetSearch: _resetSearch
  };
  window.addEventListener("WebComponentsReady", _initialize);

  function _registerHandlers() {

    document.getElementById("calculatorSearch").onclick = treasury.interest.landing.loadSearchResults;
    document.getElementById("resetSearch").onclick = treasury.interest.landing.resetSearch;
    document.getElementById("interestTabs").addEventListener("stateChange", checkAgreementTabActive);
    document.getElementById("calculatorFilterSidebar").addEventListener("stateChange", resizeAllCanvas);
    document.getElementById("financingTermSidebar").addEventListener("stateChange", resizeAllCanvas);

    treasury.interest.nettingGroup.registerHandlers();
    treasury.interest.agreementTerms.registerHandlers();
    treasury.interest.assetClass.registerHandlers();
  }

  function checkAgreementTabActive() {
    if (document.getElementById("interestTabs").selectedTab != "Agreement Terms") {
      document.getElementById("financingTermSidebar").style.display = "none";
      if (document.getElementById("financingTermSidebar").state == 'expanded') {
        document.getElementById(
          "financingTermSidebar").toggle();
      }
    } else {
      document.getElementById("financingTermSidebar").style.display = "block";
      if (document.getElementById("financingTermSidebar").state == 'expanded') {
        document.getElementById(
          "financingTermSidebar").toggle();
      }

    }

    resizeAllCanvas();
  }

  function resizeAllCanvas() {

    document.getElementById('parentArcLayout').setLayout();

    if (agreementTermsGrid != null) {
      agreementTermsGrid.resizeCanvas();
    }

    if (agreementTermsSimulatedRulesGrid != null) {
      agreementTermsSimulatedRulesGrid.resizeCanvas();
    }

    if (agreementTermsExistingRulesGrid != null) {
      agreementTermsExistingRulesGrid.resizeCanvas();
    }

    if (agreementTermsToBeAddedGrid != null) {
      agreementTermsToBeAddedGrid.resizeCanvas();
    }

    if (nettingGrpCustAccMappingGrid != null) {
      nettingGrpCustAccMappingGrid.resizeCanvas();
    }
  }

  function _initialize() {

    $(document).on({
      ajaxStart: function() {
        document.getElementById("interestLoading").show();
      },
      ajaxStop: function() {
        document.getElementById("interestLoading").hide();
      }
    });

    if (document.getElementById("financingTermSidebar").state == 'expanded') {
      document.getElementById("financingTermSidebar").toggle();
    }

    _registerHandlers();
    treasury.interest.landing.loadLandingView();
  }

  function _loadLandingViewFilters() {
    var errorDialog = document.getElementById("errorMessage");
    errorDialog.visible = false;
    $.when(treasury.loadfilter.legalEntities(),
        treasury.loadfilter.cpes(),
        treasury.loadfilter.loadAgreementTypes(),
        treasury.loadfilter.currencies(),
        treasury.loadfilter.nettingGroups())
      .done(
        function(legalEntitiesData, cpesData, agreementTypesData, currenciesData, nettingGroupsData) {

          var legalEntityCalculatorFilter = new window.treasury.filter.MultiSelect(
            "legalEntityFilterIds", "legalEntityCalculatorFilter",
            legalEntitiesData[0].descoEntities, []);

          var cpeEntityCalculatorFilter = new window.treasury.filter.MultiSelect(
            "cpeFilterIds", "cpeEntityCalculatorFilter",
            cpesData[0].cpes, []);

          var agreementTypeCalculatorFilter = new window.treasury.filter.MultiSelect(
            "agreementTypeFilterIds", "agreementTypeCalculatorFilter",
            agreementTypesData[0].agreementTypes, []);

          var currencyCalculatorFilter = new window.treasury.filter.MultiSelect(
            "currencyFilterIds", "currencyCalculatorFilter",
            currenciesData[0].currency, []);

          var nettingGroupCalculatorFilter = new window.treasury.filter.MultiSelect(
            "nettingGroupFilterIds", "nettingGroupCalculatorFilter",
            nettingGroupsData[0].nettingGroups, []);

          var dateFilterValue = treasury.defaults.date || Date.today();
          var minDate = Date.parse('1970-01-01');
          var maxDate = Date.parse('2038-01-01');
          var dateFormat = 'yy-mm-dd';
          setupDateFilter($("#datePicker"), $("#datePickerError"), dateFilterValue, minDate, maxDate,
            dateFormat);
          searchFilterGroup = new window.treasury.filter.FilterGroup([
            legalEntityCalculatorFilter, cpeEntityCalculatorFilter,
            agreementTypeCalculatorFilter, currencyCalculatorFilter,
            nettingGroupCalculatorFilter
          ], null, $("#datePicker"), null);

        }
      );
  }

  function _loadSearchResults() {

    invalidRows = {};
    var arcTabPanel = document.getElementById("interestTabs");
    var tab = arcTabPanel.selectedTab;
    if (tab == 'Netting Groups') {
      treasury.interest.nettingGroup.fetchMappings();
    } else if (tab == 'Agreement Terms') {
      treasury.interest.agreementTerms
        .loadAgreementTermsSearchResults();
    }

  }

  function _resetSearch() {
    document.getElementById("agreementTypeCalculatorFilter").value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById("legalEntityCalculatorFilter").value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById("cpeEntityCalculatorFilter").value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById("nettingGroupCalculatorFilter").value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById("currencyCalculatorFilter").value = {
      status: 'none',
      selectedNodes: []
    };

    var date = new Date();
    date;
    date.setDate(date.getDate());

    $("#datePicker").datepicker("setDate", date);
  }
})();
