"use strict";

function clearGrid(gridId) {
  document.getElementById(gridId).style.display = "block";
  document.getElementById(gridId).style.height = "";
  document.getElementById(gridId).innerHTML = "";

  if (gridId !== 'attributesGrid') {
    if ($("#" + gridId).prev().prev().is("p") ||
      $("#" + gridId).prev().prev().is("center")) {
      $("#" + gridId).prev().prev().remove();
    } else if ($("#" + gridId).prev().is("p") ||
      $("#" + gridId).prev().is("center")) {
      $("#" + gridId).prev().remove();
    }
  }
  var header = document.getElementById(gridId + "-header");
  if (header != null) {
    header.innerHTML = "";
  }
}

function showLoading(id) {
  $("#" + id).removeAttr("hidden");
  $("#" + id + "Overlay").removeAttr("hidden");
}

function hideLoading(id) {
  $("#" + id).attr("hidden", "true");
  $("#" + id + "Overlay").attr("hidden", "true");
}

function showErrorMessage(message) {
  var errorDialog = document.getElementById("errorMessage");

  errorDialog.innerHTML = "<p class='arc-message--critical'>" + message +
    "</p>";
  errorDialog.reveal({
    'title': 'Error',
    'content': errorDialog.innerHTML,
    'modal': true,
    'buttons': [{
      'html': 'OK',

      'callback': function() {
        this.visible = false
      },
      'position': 'right'
    }]
  });
}

function showMessage(message, type) {
  var dialog = document.getElementById("messageContainer");

  dialog.innerHTML = "<p class='arc-message--" + type + "'>" + message +
    "</p>";
  dialog.reveal({
    'title': 'Message',
    'content': dialog.innerHTML,
    'modal': true,
    'buttons': [{
      'html': 'OK',

      'callback': function() {
        this.visible = false
      },
      'position': 'right'
    }]
  });
}

function changeStateOfPanel(panelId, state) {
  if (document.getElementById(panelId).state == state || state == '') {
    document.getElementById(panelId).toggleState();
  }
}

function getFormattedDate(elementId) {
  var datepicker = document.getElementById(elementId);
  var date = new Date(datepicker.value);
  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;
  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  return year + month + day;
}

function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

function resizeCanvasOnGridChange(gridName, gridId) {
  var leftPanel = document.getElementById('calculatorFilterSidebar');
  var rightPanel = document.getElementById('financingTermSidebar');
  leftPanel.addEventListener('stateChange', function() {
    var parentArcLayout = document.getElementById('parentArcLayout');
    parentArcLayout.setLayout();
    gridName.resizeCanvas();
  });
  rightPanel.addEventListener('stateChange', function() {
    var parentArcLayout = document.getElementById('parentArcLayout');
    parentArcLayout.setLayout();
    gridName.resizeCanvas();
  });

  if (gridId != undefined) {
    $('#' + gridId).height($('#' + gridId).height() + 11);
  }
}

function setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate, dateFormat) {
  dateDiv.datepicker({
    dateFormat: dateFormat,
    changeMonth: true,
    changeYear: true,
    minDate: minDate,
    maxDate: maxDate,
    showOn: "both",
    buttonText: "<i class='fa fa-calendar'></i>",
  }).datepicker("setDate", date);
  dateDiv.change(function() {
    validateDate(dateDiv.val(), {
      errorDiv: errorDiv,
      minDate: minDate,
      maxDate: maxDate,
      dateFormat: dateFormat,
    });
  });
}

function isSupportedFileFormat(file) {
  var ext = file.name.match(/\.([^\.]+)$/)[1];
  switch (ext) {
    case 'xls':
    case 'xlsx':
      return true;
    default:
      return false;
  }
}

function getFormattedMessage(message, className) {
  return "<div class=\"message" + (className == '' ? className : '--' + className) +
    "\" style=\"width: 30%;\"><p>" + message + "</p></div>";
}

function raiseInfoToast(message) {
  toastr.options.timeOut = "0";
  toastr.options.positionClass = "toast-bottom-right";
  toastr.options.extendedTimeOut = "0";
  return toastr.info(message);
}

function raiseInfoToastWithTimeOut(message) {
  toastr.options.timeOut = "5000";
  toastr.options.positionClass = "toast-bottom-right";
  toastr.options.extendedTimeOut = "1000";
  return toastr.info(message);
}

function raiseSuccessToast(message) {
  toastr.options.timeOut = "5000";
  toastr.options.positionClass = "toast-bottom-right";
  toastr.options.extendedTimeOut = "1000";
  return toastr.success(message);
}

function raiseErrorToast(message) {
  toastr.options.timeOut = "0";
  toastr.options.positionClass = "toast-bottom-right";
  toastr.options.extendedTimeOut = "0";
  toastr.options.closeButton = "true";
  return toastr.error(message);
}

function setStartOfMonthDateFilter() {
  var dateFilterValue = Date.today();
  if (treasury.defaults.date !== null && treasury.defaults.date !== undefined && treasury.defaults.date !== "") {
    dateFilterValue = new Date(treasury.defaults.date);
  }
  dateFilterValue.setDate(1);
  var minDate = Date.parse('1970-01-01');
  var maxDate = Date.parse('2038-01-01');
  var dateFormat = 'yy-mm-dd';
  setupDateFilter($("#datePickerAdd"), $("#datePickerErrorAdd"), dateFilterValue, minDate, maxDate,
    dateFormat);
}

function setFilterDateToMonthStart(dateId, dateErrorId) {
  var dateFilterValue = Date.today();
  if (treasury.defaults.date !== null && treasury.defaults.date !== undefined && treasury.defaults.date !== "") {
    dateFilterValue = new Date(treasury.defaults.date);
  }
  dateFilterValue.setDate(1);
  var minDate = Date.parse('1970-01-01');
  var maxDate = Date.parse('2038-01-01');
  var dateFormat = 'yy-mm-dd';
  setupDateFilter($("#" + dateId), $("#" + dateErrorId), dateFilterValue, minDate, maxDate,
    dateFormat);
}