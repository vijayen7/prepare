/**
 * Formatters : treasury.formatters.price, treasury.formatters.drillThrough
 */
(function($) {
  $.extend(true, window, {
    "treasury": {
      "formatters": {
        "price": priceFormatter,
        "number":_numberFormatter,
        "formatNumberUsingCommas":_formatNumberUsingCommas,
        "drillThrough": drillThroughFormatter,
        "percent": percentFormatter,
        "percentTwoDecimals": twoDecimalsPercentFormatter,
        "million": millionFormatter,
        "toolTip": toolTipFormatter,
        "status": statusFormatter,
        "decimalToPercent": decimalToPercentFormatter,
        "monthToName": monthToNameFormatter,
        "rate": rateFormatter,
        "spread": spreadFormatter,
        "dateDDMMMYYYYDashed": dateDDMMMYYYYDashedFormatter,
        "roaClickableAmount": roaClickableAmount,
        "clickableAmount" : _clickableAmount,
        "clickablePercent" : _clickablePercentFormatter,
        "clickableText" : _clickableTextFormatter,
        "decamelize": _decamelize
      }
    }
  });

  function _clickableAmount(row, cell, value, columnDef, dataContext) {
	    var numberFormatValue;

	      numberFormatValue = treasury.formatters.number(row, cell, value, columnDef, dataContext);
	      if(row == null || cell == null) {
	        return numberFormatValue;
	      } else {
	        return "<a>" + numberFormatValue + "</a>";
	      }
	    }

 function _clickablePercentFormatter(row, cell, value, columnDef, dataContext) {
    	  var result = treasury.formatters.percent(row, cell, value, columnDef, dataContext);
        return "<a>" + result + "</a>";

  }
  function _clickableTextFormatter(row, cell, value, columnDef, dataContext) {

          return "<a>" + value + "</a>";

    }

  function _numberFormatter(row, cell, value, columnDef, dataContext) {
      if(value != undefined && value !== "" && (typeof value == "string" || !isNaN(value))) {
          value += "";
          var num = Math.round(value.replace(/,/g, "").replace(/x/g, ""));
          value = _formatNumberUsingCommas(num);
          if (num < 0) {
              return "<div style=\"text-align:right\"><span>(" +
                      value.replace(/-/, '') + ")</span></div>";
          }
          else {
              return "<div style=\"text-align: right;\"><span>" + value + "</span></div>";
          }
      }
      else {
          return '<div style=\"text-align: right;\"><span class="message">n/a</span></div>';
      }
  }

  function _formatNumberUsingCommas(nStr)
  {
      if(!isNumeric(nStr)) {
          return nStr;
      }
      nStr = String(nStr);
       var x = nStr.split('.'),
          formattedNumberString = _numberFormat(nStr, x.length > 1 ? x[1].length : 0);
      return formattedNumberString;
  }

  function _numberFormat(number, decimals, decPoint, thousandsSep) {
      var n = number,
          c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals,
          d = decPoint === undefined ? '.' : decPoint,
          t = thousandsSep === undefined ? ',' : thousandsSep,
          s = n < 0 ? "-" : "",
          i = String(parseInt(n = ((n < 0 ? -n : +n) || 0).toFixed(Math.min(c, 20)), 10)),
          j = i.length > 3 ? i.length % 3 : 0;

      return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(Math.min(c, 20)).slice(2) : "");
  }
    function priceFormatter(row, cell, value, columnDef, dataContext) {
    return "<div class='aln-rt'>" + (value == null ? "<span class='message'>n/a</span>" : Number(value).toFixed(2)) + "</div>";
  }

  function rateFormatter(row, cell, value, columnDef, dataContext) {
    // Convert value to bps
    var rate = (value != null ? value * 100 : null);
    return "<div class='aln-rt'>" + (rate == null ? "<span class='message'>-</span>" : Number(rate).toFixed(0)) + "</div>";
  }

  function spreadFormatter(row, cell, value, columnDef, dataContext) {
    return "<div class='aln-rt'>" + (value == null ? "<span class='message'>n/a</span>" : Number(value).toFixed(6)) + "</div>";
  }

  function percentFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return '<div class="aln-rt"> <span class="message"> n/a </span> </div>';
    } else {
      return '<div class="aln-rt">' + Number(value).toFixed(1) + '%</div>';
    }
  }

  function twoDecimalsPercentFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return '<div class="aln-rt"> <span class="message"> n/a </span> </div>';
    } else {
      return '<div class="aln-rt">' + Number(value).toFixed(2) + '%</div>';
    }
  }

  function decimalToPercentFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return '<div class="aln-rt"> <span class="message"> n/a </span> </div>';
    } else {
      return '<div class="aln-rt">' + Number(value * 100).toFixed(1) + '%</div>';
    }
  }

  function monthToNameFormatter(row, cell, value, columnDef, dataContext) {
    var months = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November',
      'December'
    ];

    if (value == null || value === "") {
      return '<div> <span class="message"> n/a </span> </div>';
    } else {
      return '<div>' + months[value - 1] + '</div>';
    }
  }

  function statusFormatter(row, cell, value, columnDef, dataContext) {
    if (value === "OK") {
      return "<img src='/static/images/slick-grid/tick.png'>";
    } else {
      return "<img style='width:11px;height:11px' src='/static/images/close.png'>";
    }
  }

  function millionFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null) {
      return dpGrid.Formatters.Float(row, cell, value, columnDef, dataContext);
    }
    return dpGrid.Formatters.Float(row, cell, Number(value / 1000000).toFixed(2), columnDef, dataContext);
  }

  /*
   * This function wraps the name of the column with the tooltip data to be shown for that cell
   *
   * row - row id
   * cell - column id
   * value - cell value (value is a hash => {value : "123", params : "cpeId=1750208&buId=10"})
   * columnDef - column definition
   * dataContext - row hash (row data)
   * toolTip - tooltip text to be shown on that cell
   */
  function toolTipFormatter(row, cell, value, columnDef, dataContext, toolTip) {
    return "<div style=\"display:inline\" title=\"" + toolTip + "\">" + value + "</div>";
  }

  /*
   * This function wraps the result of customFormatter in a link if predicate applied returns true.
   *
   * row - row id
   * cell - column id
   * value - cell value (value is a hash => {value : "123", params : "cpeId=1750208&buId=10"})
   * columnDef - column definition
   * dataContext - row hash (row data)
   * customFormatter - a function like dpGrid.Formatters.Number or any other without the link.
   * predicate - a function which should return true or false to determine if link formatting should be applied or not.
   * params - params to be published for drill down.
   */
  function drillThroughFormatter(row, cell, value, columnDef, dataContext, customFormatter, predicate, params, moduleId) {
    function formattedValue() {
      if (typeof(customFormatter) !== "undefined") {
        return customFormatter(row, cell, value, columnDef, dataContext);
      }
      return value;
    }

    if (params !== undefined && (typeof(predicate) === "undefined" || predicate(row, cell, value, columnDef, dataContext))) {
      return "<a class='treasury-drill-through" + moduleId + "' data-params='" + params + "'>" + formattedValue() + "</a>";
    } else {
      return formattedValue();
    }
  }

  /*
  This function takes string value of date and return in YYYY-MMM-DD format.
  */
  function dateDDMMMYYYYDashedFormatter(row, cell, value, columnDef, dataContext) {
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

    if (value != null && value != '') {
      var d = new Date(value);
      return (d.getDate() + "-" + month[d.getMonth()].substring(0, 3) + "-" + d.getFullYear());
    } else {
      return '';
    }
  }

  /*
  This function takes amount in USD, returns it as cickable with Numbers formatting
  */
  function roaClickableAmount(row, cell, value, columnDef, dataContext) {
    var numberFormatValue;

    if (value == null || value == "" || value === undefined) {
      return "<div class='aln-rt'>n/a</div>";
    } else {
      numberFormatValue = dpGrid.Formatters.Number(row, cell, value, columnDef, dataContext);
      if(row == null || cell == null) {
        return numberFormatValue;
      } else {
        return "<a>" + numberFormatValue + "</a>";
      }
    }
  }
})(jQuery);

/**
 * Comparators : treasury.comparators.percent, treasury.comparators.number
 */
(function($) {
  $.extend(true, window, {
    "treasury": {
      "comparators": {
        "percent": percentComparator,
        "number": numberComparator,
        "text": textComparator
      }
    }
  });

      function textComparator(a, b) {
        // pushing null's and N/As to the bottom.
        if (a == null || a === "N/A") {
          return isSortAsc ? 1 : -1;
        }
        if (b == null || b === "N/A") {
          return isSortAsc ? -1 : 1;
        }

        return a.toString().localeCompare(b.toString());
      }


  /*
   * Parameters a and b are two %s for comparison.
   * This method is to handle undefined / "N/A" cases while sorting.
   * Ascending / Descending order sorting should result in %'s first, followed by undefined, N/As.
   */
  function percentComparator(a, b) {
    // pushing null's and N/As to the bottom.
    if (a == null || a === "N/A") {
      return isSortAsc ? 1 : -1;
    }
    if (b == null || b === "N/A") {
      return isSortAsc ? -1 : 1;
    }

    // a and b are %s. removing % and comparing the numbers.
    a = a.substring(0, a.length - 1);
    b = b.substring(0, b.length - 1);

    return a - b;
  }

  /*
   * Handles 'null' cases while performing sorting.
   */
  function numberComparator(a, b) {
    //pushing nulls to the bottom of the grid.
    if (a == null) {
      return isSortAsc ? 1 : -1;
    }
    if (b == null) {
      return isSortAsc ? -1 : 1;
    }
    return a - b;
  }
})(jQuery);

/**
 * Aggregators : treasury.aggregators.sum
 */
(function($) {
  $.extend(true, window, {
    "treasury": {
      "aggregators": {
        "linkSum": linkSumAggregator,
        "sumIgnoringUndefined": sumIgnoringUndefinedAggregator
      }
    }
  });

  function linkSumAggregator(items, field) {
    var agg = 0;
    for (var i = 0; i < items.length; i++) {
      if (items[i].gridParent === undefined || items[i].gridParent === "root") {
        var val = items[i][field]["value"];
        if (val != null && val != "" && val != NaN) {
          agg += parseFloat(val);
        }
      }
    }
    return {
      "value": agg
    }; // params is undefined.
  }

  function sumIgnoringUndefinedAggregator(items, field) {
    var agg = 0;
    for (var i = 0; i < items.length; i++) {
      if (items[i].gridParent === undefined || items[i].gridParent === "root") {
        var val = items[i][field];
        if (val !== null && val !== "" && !isNaN(val) && val !== "-") {
          agg += parseFloat(val);
        }
      }
    }

    return agg;
  }
}(jQuery));

/**
 * Other utils : treasury.util_name
 */
(function($){
  $.extend(true, window, {
    "treasury": {
      "getCursorPosition": _getCursorPosition
    }
  });
  /** 
   * This returns the new position of the cursor after the inputValue has been
   * formatted using `_formatNumberUsingCommas`.
   * inputValue : The unformatted value from the user.
   * formattedValue : The return value of the formatter.
   * cursorPosition : The current cursor position.
   */
  function _getCursorPosition(inputValue, formattedValue, cursorPosition) {
    if(inputValue.length === cursorPosition) {
        // If the cursor was at the end of the input, set the cursor to new value's end.
        cursorPosition = formattedValue.length;
    } else if(inputValue !== formattedValue) {
        // If the value has changed due to formatting, change the cursor position to match
        // the number of commas added or removed.
        var integerValue = inputValue.split(".")[0],
            commaCount = (integerValue.match(/,/g) || []).length,
            digitCount = (integerValue.match(/\d/g) || []).length,
            requiredCommas = Math.max(0, Math.floor((digitCount-1)/3));
        cursorPosition += requiredCommas - commaCount;
    }
    return cursorPosition;
  }
})(jQuery);

/**
 * Reads the URL (location.search) and returns the URL Param value for the given key.
 */
var getURLParameter = function(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

/**
 * Updates Grid footer for Broker Revenue gadgets with appropriate notes.
 */
var gridFooterUpdateForBrokerRevenueGadgets = function(moduleId, year, asOfDate, isAnnualized, predicateForDateIncompleteMessage) {

  if (treasury.defaults.disableFootnote == 'true') {
    return;
  }

  $("#grid-footer-" + moduleId).show().html("* All figures in $M USD.");

  // Footer with a message saying 2013 data is incomplete.
  if (predicateForDateIncompleteMessage()) {
    $("#grid-footer-" + moduleId).append("<br />* 2013 data is incomplete.");
  }

  if (asOfDate) {
    $("#grid-footer-" + moduleId).append(
      "<br />* The numbers are updated and verified up until " + asOfDate + ".");
  }

  if (isAnnualized) {
    $("#grid-footer-" + moduleId).append(
      "<br />* The numbers are annualized based on year to date data and may be materially higher or lower than prior year if " +
      "there are expense fluctuations in the earlier months of the year.");
  }
};

/*
 * This will take the union of search parameters (from filters of search panel) and drill through parameters.
 * If there is a collision on any parameter, then it's overridden by drill through parameter.
 */
var mergeSearchParamsAndDrillThroughParams = function(searchParams, drillThroughParams, optionalInfo) {
  var preProcessSearchParams = function(params) {
    var result = new Object();
    for (var key in params) {
      if (params.hasOwnProperty(key) && typeof(params[key]) === "string" && key.match("Ids?$") != null) {
        if (params[key] !== "-1") {
          result[key] = params[key].split(",");
        }
      } else {
        result[key] = params[key];
      }
    }
    return result;
  };

  // Deep clone searchParams and pre-process them to convert csv idstring to id array.
  var resultParams = preProcessSearchParams($.extend(true, {}, searchParams));

  for (var key in drillThroughParams) {
    // Not inherited.
    if (drillThroughParams.hasOwnProperty(key)) {
      resultParams[key] = drillThroughParams[key];
    }
  }

  if (optionalInfo !== "undefined") {
    resultParams["optionalInfo"] = optionalInfo;
  }

  return resultParams;
};

function getYearSingleSelectFilter(endYear, startYear) {
  var yearFilter = [];
  for (var yearIndex = endYear; yearIndex >= startYear; yearIndex--) {
    yearFilter.push([yearIndex, '' + yearIndex]);
  }

  return yearFilter;
}

// Validate the top N filter. The top N value should be an integer and should
// lie between the prescribed min and max values.
function validateTopN(topN, min_value, max_value) {
  if (isInteger(topN)) {
    if (min_value == null || topN >= min_value) {
      if (max_value == null || topN <= max_value) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  return false;
}

// Validating an integer
// Parameter : a string
// checks after trimming white spaces if it's an integer
// returns true/false
function isInteger(value) {
  // truncate value from both sides
  if (value == null) {
    return false;
  }

  var truncated_value = trim(value);

  if (truncated_value.toString().match(/^\d+$/)) {
    return true;
  }

  return false;
}

//
//Formatting related methods.
//

//This is a wrapper over the jquery api jquery.formatNumber
//This fixes some of the bugs which occur in IE
//if the number is "0.2345", the api returns ".23" in IE while we expect "0.23"
//if the number is "0", the api returns "" in IE while we expect "0"
//
//num : number (int or float)
//num_format : optional, if not defined default #,##0.00 is assumed

function formatNumberWithLeadingZero(num, num_format) {
  if (num_format == null) {
    num_format = "#,##0.00";
  }

  var formatted_str = $.formatNumber(num, {
    format: num_format
  });
  if (formatted_str == "") {
    formatted_str = "0";
  } else if (formatted_str.charAt(0) == ".") {
    formatted_str = "0" + formatted_str;
  }

  return formatted_str;
}

function _decamelize(str, separator) {
  separator = typeof separator === 'undefined' ? '_' : separator;

  return str
    .replace(/([a-z\d])([A-Z])/g, '$1' + separator + '$2')
    .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1' + separator + '$2')
    .replace(/(^[a-zA-Z]{1})/, (s) => {
      return s.toUpperCase();
    });
}
