// Controllers of position-data-availability gadget
dportal.ops.opsProgramApp.controller('positionDataAvailabilityCtrl', [
        '$scope',
        '$resource',
        function($scope, $resource) {
            $scope.isLoaded = false;
            $scope.showChart = true;
            $scope.startDate = (Date.parse(new Date().toString("yyyy-MM-dd")))
                    .addDays(-30).toString("MM/dd/yyyy");
            $scope.endDate = (Date.parse(new Date().toString("yyyy-MM-dd")))
                    .toString("MM/dd/yyyy");

            var options = {
                autoHorizontalScrollBar : true,
                applyFilteringOnGrid : true,
                showHeaderRow : true,
                maxHeight : 500,
                forceFitColumns : true,
                summaryRow : false,
                displaySummaryRow : false,
                exportToExcel : true,
                sheetName : "Position Data Availability",
                sortList : [ {
                    columnId : "date",
                    sortAsc : false
                } ]
            // to confirm
            };

            var columns = [ {
                id : "date",
                name : "Date",
                field : "date",
                toolTip : "Date",
                type : "text",
                headerCssClass : "b",
                filter : true,
                sortable : false
            }, {
                id : "agreementDataCount",
                name : "Agreement Data Count",
                field : "agreementDataCount",
                toolTip : "Agreement Data Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "enrichedCommissionCount",
                name : "Enriched Commission Count",
                field : "enrichedCommissionCount",
                toolTip : "Enriched Commission Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true,
                minWidth : 100
            }, {
                id : "enrichedMarginPlusExposureCount",
                name : "Enriched Margin Plus Exposure Count",
                field : "enrichedMarginPlusExposureCount",
                toolTip : "Enriched Margin Plus Exposure Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true,
                minWidth : 70
            }, {
                id : "enrichedMarginDataCount",
                name : "Enriched Margin Data Count",
                field : "enrichedMarginDataCount",
                toolTip : "Enriched Margin Data Count",
                type : "text",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "enrichedExposureCount",
                name : "Enriched Exposure Count",
                field : "enrichedExposureCount",
                toolTip : "Sync Start Time",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "enrichedImpactCount",
                name : "Enriched Impact Count",
                field : "enrichedImpactCount",
                toolTip : "Enriched Impact Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "enrichedRiskCount",
                name : "Enriched Risk Count",
                field : "enrichedRiskCount",
                toolTip : "Enriched Risk Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "enrichedCollateralCount",
                name : "Enriched Collateral Count",
                field : "enrichedCollateralCount",
                toolTip : "Enriched Collateral Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "enrichedCollateralPlusExposureCount",
                name : "Enriched Collateral Plus Exposure Count",
                field : "enrichedCollateralPlusExposureCount",
                toolTip : "Enriched Collateral Plus Exposure Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }];

            $scope.positionDataAvailabilityOptions = {
                data : [],
                options : options,
                columns : columns,
                emptyMessage : "No data is available"
            };

            var getParams = function() {

                var dateParts = $scope.startDate.split("/");
                var date1 = dateParts[2] + "-" + dateParts[0] + "-"
                        + dateParts[1];

                dateParts = $scope.endDate.split("/");
                var date2 = dateParts[2] + "-" + dateParts[0] + "-"
                        + dateParts[1];

                var params = new Object();
                params.startDateString = date1;
                params.endDateString = date2;

                return params;
            };

            $scope.getDate = function(dateString)
            {
                var year        = dateString.substring(0,4);
                var month       = dateString.substring(5,7);
                var day         = dateString.substring(8,10);
                return Date.UTC(year,month - 1,day);
            };

            $scope.dateError = function(){
                var dateParts = $scope.startDate.split("/");
                var date1 = Date.UTC(dateParts[2],dateParts[0],dateParts[1]);
                dateParts = $scope.endDate.split("/");
                var date2 = Date.UTC(dateParts[2],dateParts[0],dateParts[1]);
                if(date1<date2) return false;
                return true;
            };

            $scope.fetchData = function() {
                $scope.isLoaded = false;
                var positionDataAvailabilityREST = $resource(
                        "/treasury/ops/search-enrichment-detail", getParams(),
                        {
                            'query' : {
                                method : 'get',
                                isArray : false
                            }
                        });
                var operation = positionDataAvailabilityREST.query();
                $scope.operation = operation;
                $scope.operation.$promise.then(function(result) {
                    data = result["resultList"];
                    $scope.positionDataAvailabilityOptions.data = data;
                    $scope.isLoaded = true;
                    
                    // Configuration of HighChart
                    var series = [];
                    var lines = ["enrichedCommissionCount", "enrichedMarginPlusExposureCount",  "agreementDataCount",  "enrichedMarginDataCount",  "enrichedExposureCount",  "enrichedImpactCount",  "enrichedRiskCount", "enrichedCollateralCount", "enrichedCollateralPlusExposureCount"];
                    //var lines = ["enrichedCommissionCount", "enrichedMarginPlusExposureCount"];
                    
                    if(!data || data.length==0){
                        series.push({data:[]});
                    }
                    else
                    {
                        for (var j = 0; j < lines.length; j++)
                        {
                            var line = lines[j];
                            var chartData = [];
                            var rows = data;
                            for(i=0; i< rows.length; i++){
                                chartData.push([$scope.getDate(rows[i]["date"]), rows[i][line]]);
                            }
                            series.push({
                                data : chartData,
                                name : line
                            });
                        }
                    }

                    $scope.positionDataAvailabilityConfig = {
                            options: {
                                chart: {
                                    type: 'line'
                                }
                            },
                            title: {
                                text: '',
                                style:{
                                    display: 'none'
                                }
                            },
                            series: series,
                            xAxis: {
                                  type: 'datetime',
                                    dateTimeLabelFormats: {
                                        day: '%b %e, %Y'
                                    }
                            },
                            yAxis: {
                                min: 0,
                            },
                        };
                });
            };
            $scope.fetchData();
        } ])
        
        .directive('ngTabs', function(){
            return {
                restrict: 'A',
                link: function(scope, elm, attrs) {
                    var jqueryElm = $(elm[0]);
                    $(jqueryElm).tabs();
                }
         };
        });