// Controllers of log-viewer gadget
var logViewerGadgetId;
dportal.ops.opsProgramApp.controller('logsViewerCtrl', [
        '$scope',
        '$resource',
        '$interval',
        '$timeout',
        function($scope, $resource, $interval, $timeout) {
        	
        	$scope.possTime = [
        	    	           {'time':30, 'value':'30 seconds'},
        	    	           {'time':60, 'value':'1 minute'},
        	    	           {'time':120, 'value':'2 minutes'},
        	    	           {'time':300, 'value':'5 minutes'},
        	    	           {'time':600, 'value':'10 minutes'},
        	    	           {'time':-1, 'value':'Disabled'}
        	               ];
        	
        	$scope.myTime = $scope.possTime[0];
        	$scope.lastTime = $scope.myTime.time;
        	
        	$scope.logType = "treasury";

            $scope.isLoaded = false;
            $scope.lines = 200;
            
            $scope.isRefreshDisabled = function() {
            	if ( $scope.myTime.time == -1 ) return false;
            	return true;
            };
            
            $scope.isRefreshTimeChangeDisabled = function() {
            	if ( $scope.refreshChangeStatus == "Edit" ) return true;
            	return false;
            };
            
            $scope.refreshChangeStatus = "Edit";
            
            $scope.refreshChangeStatusChanged = function() {
            	if ( $scope.refreshChangeStatus == "Edit" ) {
            		$scope.refreshChangeStatus = "Save";
            	} else {
            		$scope.refreshChangeStatus = "Edit";
            		$scope.refreshTimeChanged();
            	}
            };
            
            $scope.fetchDataFromFileTail = function() {
            	
            	$scope.isLoaded = false;
            	
            	var params = new Object();
            	params.numberOfLines = $scope.lines;
            	params.logType = $scope.logType;

                var SyncJobMonitorREST = $resource(
                        "/treasury/ops/view-logs", params, {
                            'query' : {
                                method : 'get',
                                isArray : false
                            }
                        });
                var operation = SyncJobMonitorREST.query();
                $scope.operation = operation;

                $scope.operation.$promise.then(function(result) {
                    data = result["result"];
                    var objDiv = document.getElementById('containerToShowLogs');
                    objDiv.innerHTML = data;
                    $timeout(function(){
		                objDiv.scrollTop = objDiv.scrollHeight;
                    }, 1000);
                    $scope.isLoaded = true;
                });

            };

            $scope.fetchDataFromFileTail();
            
            var refreshIntervalId = $interval($scope.fetchDataFromFileTail, ($scope.myTime.time)*1000);
            
            $scope.refreshTimeChanged = function() {
        		if ( $scope.lastTime != -1 )
        			$interval.cancel(refreshIntervalId);	
        		$scope.lastTime = $scope.myTime.time;
        		if ( $scope.myTime.time != -1 )
        			refreshIntervalId = $interval($scope.fetchDataFromFileTail, ($scope.myTime.time)*1000);
            };

            gadgets.dppubsub.subscribe(logViewerGadgetId,"treasury-techops-syncjob-executed", function(data) {
            	$scope.fetchDataFromFileTail();
            },1);
            

        } ]);