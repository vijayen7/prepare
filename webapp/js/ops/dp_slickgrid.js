/**
 * Angular JS directive for slickgrid. This needs to be part of a
 * "dportal.angular" module so that it can be shared across teams, but since we
 * dont have any such module as of now, we define it as part of the reins module
 */
(function(modules) {
    for ( var id in modules) {
        var module = modules[id];
        module.directive("dpSlickgrid", function factory() {
            return {
                restrict : "E",
                scope : {
                    options : "=",
                    isLoaded : "=",
                    gridId : "="
                },
                templateUrl : '/treasury/htm/slickGridTemplate.html',
                link : function postLink(scope, iElement, iAttrs) {
                    scope.myId = iAttrs.gridId;
                    scope.$watch(iAttrs.isLoaded, function(value) {
                        new dportal.grid.createGrid($("#" + scope.myId),
                                scope.options.data, scope.options.columns,
                                scope.options.options);
                    });
                }
            };
        });
    }
})([ dportal.ops.opsProgramApp ]);