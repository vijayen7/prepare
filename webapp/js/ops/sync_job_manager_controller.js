// Controllers of sync-job-manager gadget
var syncJobManagerGadgetId;
dportal.ops.opsProgramApp.controller('syncJobManagerCtrl', [
        '$scope',
        '$resource',
        '$timeout',
        function($scope, $resource, $timeout) {

        	$scope.executingJob = [];
        	for ( var i = 0; i < 7; ++i ) $scope.executingJob.push(false);

        	$scope.getMessageOnButton = function(executing) {
        		if ( executing == true ) return "Submitting..";
        		return "Execute";
        	}
            $scope.performCallBackOperations = function(operation) {
                operation.$promise.then(function(result) {
                	// do something if needed callback on sync job completion
                }, function(error) {
    				if ( error.status == 500 ) {
    					if ( error.data.indexOf("UnauthorizedException") > -1 )
    						alert("You don't have the authorization to execute this job.");
    				}
    			});

                $timeout(function(){
                	gadgets.dppubsub.publish(syncJobManagerGadgetId,"treasury-techops-syncjob-executed","new sync job");
                },3000);

            };


            $scope.getStartDate = function(dateRange) {
            	return dateRange.substr(0,10);
            };
            $scope.getEndDate = function(dateRange) {
            	if( dateRange.length > 11 )
            		return dateRange.substr(13,23);
            	return dateRange.substr(0,10);
            };

            $scope.executePositionSyncIncludeASA = "true";

			$scope.executeReportingDataSyncCaller = function() {

	            	var getParamsReportingDataSync = function() {

	                    var date1 = $scope.getStartDate($scope.executeReportingDataSyncDateRangeValue);
	                    var date2 = $scope.getEndDate($scope.executeReportingDataSyncDateRangeValue);

	                    var params = new Object();
	                    params.startDate = date1;
	                    params.endDate = date2;

	                    return params;
	                };

	            	var executeReportingDataSyncCallerREST = $resource(
	                        "/treasury/service/syncManager/executeReportingDataSync", getParamsReportingDataSync(),
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[1] = true;
	            	$timeout( function() {
	            		$scope.executingJob[1] = false;
	            	}, 2000);
	                var operation = executeReportingDataSyncCallerREST.query();
	                $scope.performCallBackOperations(operation);

            };

            $scope.executeMarginSyncAsync = "true";

            $scope.executeMarginSyncCaller = function() {

	            	var getParamsMarginSync = function() {

	                    var date1 = $scope.getStartDate($scope.executeMarginSyncDateRangeValue);
	                    var date2 = $scope.getEndDate($scope.executeMarginSyncDateRangeValue);

	                    var params = new Object();
	                    params.startDate = date1;
	                    params.endDate = date2;
	                    if ( $scope.executeMarginSyncPositionSyncId != "" ) params.positionSyncId = $scope.executeMarginSyncPositionSyncId;
	                    if ( $scope.executeMarginSyncAgreementIds != "" ) params.agreementIds = $scope.executeMarginSyncAgreementIds;
	                    if ( $scope.executeMarginSyncAsync != "" ) params.async = $scope.executeMarginSyncAsync;

	                    return params;
	                };

	            	var executeMarginSyncCallerREST = $resource(
	                        "/treasury/service/syncManager/executeMarginSync", getParamsMarginSync(),
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[2] = true;
	            	$timeout( function() {
	            		$scope.executingJob[2] = false;
	            	}, 2000);
	                var operation = executeMarginSyncCallerREST.query();
	                $scope.performCallBackOperations(operation);
            };


            $scope.executeCommissionSyncCaller = function() {

	            	var getParamsCommissionSync = function() {

	                    var date1 = $scope.getStartDate($scope.executeCommissionSyncDateRangeValue);
	                    var date2 = $scope.getEndDate($scope.executeCommissionSyncDateRangeValue);

	                    var params = new Object();
	                    params.startDate = date1;
	                    params.endDate = date2;

	                    return params;
	                };

	            	var executeCommissionSyncCallerREST = $resource(
	                        "/treasury/service/syncManager/executeCommissionSync", getParamsCommissionSync(),
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[3] = true;
	            	$timeout( function() {
	            		$scope.executingJob[3] = false;
	            	}, 2000);
	                var operation = executeCommissionSyncCallerREST.query();
	                $scope.performCallBackOperations(operation);
            };

            $scope.executeExposureEnricherAsync = "true";

            $scope.executeExposureEnricherCaller = function() {

	            	var getParamsExposureEnricher = function() {

	                    var date1 = $scope.getStartDate($scope.executeExposureEnricherDateRangeValue);
	                    var date2 = $scope.getEndDate($scope.executeExposureEnricherDateRangeValue);

	                    var params = new Object();
	                    params.startDate = date1;
	                    params.endDate = date2;
	                    if ( $scope.executeExposureEnricherPositionSyncId != "" ) params.positionSyncId = $scope.executeExposureEnricherPositionSyncId;
	                    if ( $scope.executeExposureEnricherAgreementIds != "" ) params.agreementIds = $scope.executeExposureEnricherAgreementIds;
	                    if ( $scope.executeExposureEnricherAsync != "" ) params.async = $scope.executeExposureEnricherAsync;

	                    return params;
	                };

	            	var executeExposureEnricherCallerREST = $resource(
	                        "/treasury/service/syncManager/executeExposureEnricher", getParamsExposureEnricher(),
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[4] = true;
	            	$timeout( function() {
	            		$scope.executingJob[4] = false;
	            	}, 2000);
	                var operation = executeExposureEnricherCallerREST.query();
	                $scope.performCallBackOperations(operation);
            };

            $scope.executeCollateralEnricherAsync = "true";

			$scope.executeCollateralEnricherCaller = function() {

	            	var getParamsCollateralEnricher = function() {

	                    var date1 = $scope.getStartDate($scope.executeCollateralEnricherDateRangeValue);
	                    var date2 = $scope.getEndDate($scope.executeCollateralEnricherDateRangeValue);

	                    var params = new Object();
	                    params.startDate = date1;
	                    params.endDate = date2;
	                    if ( $scope.executeCollateralEnricherPositionSyncId != "" ) params.positionSyncId = $scope.executeCollateralEnricherPositionSyncId;
	                    if ( $scope.executeCollateralEnricherAgreementIds != "" ) params.agreementIds = $scope.executeCollateralEnricherAgreementIds;
	                    if ( $scope.executeCollateralEnricherAsync != "" ) params.async = $scope.executeCollateralEnricherAsync;

	                    return params;
	                };

	            	var executeCollateralEnricherCallerREST = $resource(
	                        "/treasury/service/syncManager/executeCollateralEnricher", getParamsCollateralEnricher(),
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[5] = true;
	            	$timeout( function() {
	            		$scope.executingJob[5] = false;
	            	}, 2000);
	                var operation = executeCollateralEnricherCallerREST.query();
	                $scope.performCallBackOperations(operation);
            };


            $scope.executePositionSyncWithAllEnrichmentsCaller = function() {

	            	var executePositionSyncWithAllEnrichmentsCallerREST = $resource(
	                        "/treasury/service/syncManager/executePositionSyncWithAllEnrichments", null,
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[6] = true;
	            	$timeout( function() {
	            		$scope.executingJob[6] = false;
	            	}, 2000);
	                var operation = executePositionSyncWithAllEnrichmentsCallerREST.query();
	                $scope.performCallBackOperations(operation);
            };


            $scope.executeBrokerRevenueDataSyncCaller = function() {

	            	var getParamsBrokerRevenueDataSync = function() {


	                    var params = new Object();
	                    if ( $scope.executeBrokerRevenueDataSyncYear != "" ) params.positionSyncId = $scope.executeBrokerRevenueDataSyncYear;

	                    return params;
	                };

	            	var executeBrokerRevenueSyncCallerREST = $resource(
	                        "/treasury/service/syncManager/executeBrokerRevenueDataSync", getParamsBrokerRevenueDataSync(),
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[7] = true;
	            	$timeout( function() {
	            		$scope.executingJob[7] = false;
	            	}, 2000);
	                var operation = executeBrokerRevenueSyncCallerREST.query();
	                $scope.performCallBackOperations(operation);
            };


            $scope.executeMarginAgeCalculatorJobCaller = function() {

	            	var getParamsMarginAgeCalculatorSync = function() {

	                    var date1 = $scope.getStartDate($scope.executeMarginAgeCalculatorJobDateRangeValue);
	                    var date2 = $scope.getEndDate($scope.executeMarginAgeCalculatorJobDateRangeValue);

	                    var params = new Object();
	                    params.startDate = date1;
	                    params.endDate = date2;

	                    return params;
	                };

	            	var executeMarginAgeCalculatorJobCallerREST = $resource(
	                        "/treasury/service/syncManager/executeMarginAgeCalculatorJob", getParamsMarginAgeCalculatorSync(),
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[10] = true;
	            	$timeout( function() {
	            		$scope.executingJob[10] = false;
	            	}, 2000);
	                var operation = executeMarginAgeCalculatorJobCallerREST.query();
	                $scope.performCallBackOperations(operation);

            };



			$scope.executeBoxPositionAgeCalculatorJobCaller = function() {

	            	var getParamsBoxPositionAgeCalculatorJob = function() {

	                    var date1 = $scope.getStartDate($scope.executeBoxPositionAgeCalculatorJobDateRangeValue);
	                    var date2 = $scope.getEndDate($scope.executeBoxPositionAgeCalculatorJobDateRangeValue);

	                    var params = new Object();
	                    params.startDate = date1;
	                    params.endDate = date2;

	                    return params;
	                };

	            	var executeBoxPositionAgeCalculatorJobCallerREST = $resource(
	                        "/treasury/service/syncManager/executeBoxPositionAgeCalculatorJob", getParamsBoxPositionAgeCalculatorJob(),
	                        {
	                            'query' : {
	                                method : 'get',
	                                isArray : false
	                            }
	                        });
	            	$scope.executingJob[8] = true;
	            	$timeout( function() {
	            		$scope.executingJob[8] = false;
	            	}, 2000);
	                var operation = executeBoxPositionAgeCalculatorJobCallerREST.query();
	                $scope.performCallBackOperations(operation);

            };

			$scope.executeBoxPositionIdentifierCaller = function() {

            	var getParamsBoxPositionIdentifier = function() {

                    var date1 = $scope.getStartDate($scope.executeBoxPositionIdentifierDateRangeValue);
                    var date2 = $scope.getEndDate($scope.executeBoxPositionIdentifierDateRangeValue);

                    var params = new Object();
                    params.startDateInput = date1;
                    params.endDateInput = date2;

                    return params;
                };

            	var executeBoxPositionIdentifierCallerREST = $resource(
                        "/treasury/service/syncManager/executeBoxPositionIdentifier", getParamsBoxPositionIdentifier(),
                        {
                            'query' : {
                                method : 'get',
                                isArray : false
                            }
                        });
            	$scope.executingJob[9] = true;
            	$timeout( function() {
            		$scope.executingJob[9] = false;
            	}, 2000);
                var operation = executeBoxPositionIdentifierCallerREST.query();
                $scope.performCallBackOperations(operation);
        };

        } ]);