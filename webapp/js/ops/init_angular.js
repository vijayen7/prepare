/*
 * Code required to initialize angularjs code in dashboard, and then to manually compile angularjs code of any gadget using following triggetHandler
 * 
 * Calling tirggerHandler (from any gadget): 
 * $(document).triggerHandler("gadgetLoad",["content__MODULE_ID__"]);
 */
opsApp = angular.module('opsApp',["ngResource","highcharts-ng"]);
dportal.namespace("ops");
dportal.ops.opsProgramApp = opsApp;

opsApp.controller("opsCtrl", ["$scope" ,"$compile", "$document",
    function($scope,$compile,$document) {
        $document.off("gadgetLoad").on("gadgetLoad", function(e,elementId) {
                $scope.$apply(function() {
                    var dom = angular.element("#" + elementId);
                    $compile(dom)($scope);
                });
        });
}]);