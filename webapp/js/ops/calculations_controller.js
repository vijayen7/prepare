dportal.ops.opsProgramApp.controller('calculationsController', [
        '$scope',
        '$resource',
        function($scope, $resource) {

            $scope.options = [];

            $scope.options_list = [];

            $scope.flags = [];
            $scope.arguments = "";

            $scope.changeDateFormat = function(date, str) {
                return date.split(str)[2] + date.split(str)[0]
                        + date.split(str)[1];
            };
            $scope.changeDateRangeFormat = function(date, str) {
                return date.split(str)[0] + date.split(str)[1]
                        + date.split(str)[2];
            };
            

            $scope.getStartDate = function(dateRange) {
                return dateRange.substr(0, 10);
            };

            $scope.getEndDate = function(dateRange) {
                if (dateRange.length > 11)
                    return dateRange.substr(13, 23);
                return dateRange.substr(0, 10);
            };

            $scope.getDateArguments = function() {
            };

            $scope.getArguments = function() {

                var checkNull = function(str) {
                    if (str == "__null__" || str == "")
                        return true;
                    else
                        return false;
                };

                $scope.arguments = $scope.getBaseArguments();
                var dateArguments = $scope.getDateArguments();

                for ( var dateArg in dateArguments) {
                    if (!checkNull(dateArguments[dateArg])) {
                        $scope.arguments += dateArg + "="
                                + dateArguments[dateArg] + "&";
                    }
                }

                angular.forEach($scope.options,
                        function(option) {
                            if (!checkNull(option.value)) {
                                $scope.arguments += (option.field + "="
                                        + option.value + "&");
                            }
                        });

                angular.forEach($scope.options_list, function(options_item) {
                    var list = options_item.value.split(" ");
                    for ( var item in list) {
                        if (!checkNull(list[item])) {
                            $scope.arguments += (options_item.field + "="
                                    + list[item] + "&");
                        }
                    }

                });

                angular.forEach($scope.flags, function(flag) {
                    $scope.arguments += (flag.field + "=" + flag.value + "&");
                });
                $scope.arguments = $scope.arguments.substring(0,
                        $scope.arguments.length - 1);
            };

            $scope.runCalculations = function() {
                $scope.getArguments();
                var params = new Object();
                params.arguments = $scope.arguments;
                var JobRunREST = $resource("/treasury/ops/run-calculation",
                        params, {
                            'query' : {
                                method : 'get',
                                isArray : false
                            }
                        });
                var operation = JobRunREST.query();
            };
        } ]);

dportal.ops.opsProgramApp.controller('marginController', function($controller,
        $scope) {
    marginCtrl = $controller('calculationsController', {
        $scope : $scope
    });

    $scope.options = [ {
        name : "Portfolio Group",
        field : "portfolio_group",
        value : "__null__"
    }, {
        name : "CPE",
        field : "cpe",
        value : "__null__"
    }, {
        name : "Desco Entity",
        field : "desco_entity",
        value : "__null__"
    }, {
        name : "Agreement Type",
        field : "agreement_type",
        value : "__null__"
    }, {
        name : "Outdir",
        field : "outdir",
        value : "__null__"
    } ];

    $scope.flags = [ {
        name : "Save Report",
        field : "save_report",
        value : "false"
    }, {
        name : "Span Staging",
        field : "span_staging",
        value : "false"
    }, {
        name : "DB Write",
        field : "db_write",
        value : "false"
    }, {
        name : "Persist Intermediate Data",
        field : "persist_intermediate_data",
        value : "false"
    }, {
        name : "Ust Allocation Run",
        field : "ust_allocation_run",
        value : "false"
    }, ];

    $scope.dateValue = (Date.parse(new Date().toString("yyyy-MM-dd"))).addDays(
            -1).toString("MM/dd/yyyy");
    $scope.getBaseArguments = function () {
        return "runMarginCalculations?";
    }
    $scope.getDateArguments = function() {
        var dateArguments = new Object();
        dateArguments.date = $scope.changeDateFormat($scope.dateValue, "/");
        return dateArguments;
    };

});

dportal.ops.opsProgramApp
        .controller(
                'financingController',
                function($controller, $scope) {
                    financingCtrl = $controller('calculationsController', {
                        $scope : $scope
                    });

                    $scope.options = [ {
                        name : "Batch",
                        field : "batch",
                        value : "__null__"
                    } ];

                    $scope.options_list = [ {
                        name : "Mail To",
                        field : "mail_to",
                        value : "__null__"
                    }, {
                        name : "CPE",
                        field : "cpe",
                        value : "__null__"
                    }, {
                        name : "Agreement Type",
                        field : "agreement_type",
                        value : "__null__"
                    }, {
                        name : "Desco Entity",
                        field : "desco_entity",
                        value : "__null__"
                    }, {
                        name : "Netting Group",
                        field : "netting_group",
                        value : "__null__"
                    }, {
                        name : "Currency",
                        field : "currency",
                        value : "__null__"
                    } ];

                    $scope.flags = [ {
                        name : "Cofi Run",
                        field : "cofi_run",
                        value : "false"
                    }, {
                        name : "Test",
                        field : "test",
                        value : "false"
                    }, {
                        name : "Update Cofi TRS",
                        field : "update_cofi_trs",
                        value : "false"
                    }, {
                        name : "Persist Pnl Outage",
                        field : "persist_pnl_outage",
                        value : "false"
                    }, ];

                    $scope.getBaseArguments = function () {
                        return "runFinancingCalculations?";
                    }

                    $scope.getDateArguments = function() {
                        var dateArguments = new Object();
                        dateArguments.start_date = $scope
                                .changeDateRangeFormat(
                                        $scope
                                                .getStartDate($scope.financingCalculationsDateRangeValue),
                                        "-");
                        dateArguments.end_date = $scope
                                .changeDateRangeFormat(
                                        $scope
                                                .getEndDate($scope.financingCalculationsDateRangeValue),
                                        "-");
                        return dateArguments;
                    };
                });