// Controllers of sync-job-status-monitor gadget
dportal.ops.opsProgramApp.controller('syncJobStatusMonitorCtrl', [
        '$scope',
        '$resource',
        function($scope, $resource) {

            $scope.isLoaded = false;
            $scope.jobs = 200;
            $scope.dateValue = (Date.parse(new Date().toString("yyyy-MM-dd")))
                    .addDays(-7).toString("MM/dd/yyyy");

            var options = {
                autoHorizontalScrollBar : true,
                applyFilteringOnGrid : true,
                showHeaderRow : true,
                maxHeight : 500,
                forceFitColumns : true,
                summaryRow : false,
                displaySummaryRow : false,
                exportToExcel : true,
                sheetName : "Sync Status",
                sortList : [ {
                    columnId : "startTime",
                    sortAsc : false
                } ]
            };

            var columns = [ {
                id : "syncType",
                name : "Sync Type",
                field : "syncType",
                toolTip : "Sync Type",
                type : "text",
                filter : true,
                sortable : true,
                headerCssClass : "b",
                minWidth : 100
            }, {
                id : "syncSource",
                name : "Sync Source",
                field : "syncSource",
                toolTip : "Sync Source",
                type : "text",
                filter : true,
                sortable : true,
                headerCssClass : "b",
                minWidth : 70
            }, {
                id : "syncId",
                name : "Sync ID",
                field : "syncId",
                toolTip : "Sync ID",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "parentSyncId",
                name : "Parent Sync Id",
                field : "parentSyncId",
                toolTip : "Parent Sync Id",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "syncStatus",
                name : "Sync Status",
                field : "syncStatus",
                toolTip : "Sync Status",
                type : "text",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "startTime",
                name : "Sync Start Time (GMT)",
                field : "startTime",
                toolTip : "Sync Start Time",
                type : "text",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "endTime",
                name : "Sync End Time (GMT)",
                field : "endTime",
                toolTip : "Sync End Time",
                type : "text",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "duration",
                name : "Duration (in minutes)",
                field : "duration",
                toolTip : "Duration",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "syncRunDate",
                name : "Sync Run Date",
                field : "syncRunDate",
                toolTip : "Sync Run Date",
                headerCssClass : "b",
                type : "text",
                filter : true,
                sortable : true
            }, {
                id : "syncParamString",
                name : "Sync Parameters",
                field : "syncParamString",
                toolTip : "Sync Parameters",
                headerCssClass : "b",
                type : "text",
                filter : true,
                sortable : true
            } ];

            $scope.syncJobStatusMonitorOptions = {
                data : [],
                options : options,
                columns : columns,
                emptyMessage : "No data is available"
            };

            var getParams = function() {
                var dateParts = $scope.dateValue.split("/");
                var date = dateParts[2] + "-" + dateParts[0] + "-"
                        + dateParts[1];

                var params = new Object();
                params.startDateString = date;
                params.lastN = $scope.jobs;

                return params;
            };

            $scope.fetchData = function() {
                $scope.isLoaded = false;
                // if we call request json using $.ajax (as following), there
                // are some issues related to rendering.
                /*
                 * $.ajax({ url : '/treasury/ops/search-sync-job-detail', data :
                 * getParams(), type : "GET", dataType : "json", success :
                 * function (result){ data = result["resultList"]; if(! data ||
                 * data.length == 0){ var emptyMessageHtml = '<span
                 * class="message">'+scope.options.emptyMessage+'</span>';
                 * iElement.replaceWith(emptyMessageHtml); return ; }
                 *
                 * $scope.syncJobStatusMonitorOptions.data = data;
                 * $scope.isLoaded=true; } });
                 */

                var SyncJobMonitorREST = $resource(
                        "/treasury/ops/search-sync-job-detail", getParams(), {
                            'query' : {
                                method : 'get',
                                isArray : false
                            }
                        });
                var operation = SyncJobMonitorREST.query();
                $scope.operation = operation;

                $scope.operation.$promise.then(function(result) {
                    data = result["resultList"];
                    $scope.syncJobStatusMonitorOptions.data = data;
                    $scope.isLoaded = true;
                });

            };

            $scope.fetchData();

            $scope.doAutoUpdate = true;
            
        } ]);