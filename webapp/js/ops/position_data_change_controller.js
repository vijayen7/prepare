// Controllers of position-data-change gadget
dportal.ops.opsProgramApp.controller('positionDataChangeCtrl', [
        '$scope',
        '$resource',
        function($scope, $resource) {

            $scope.isLoaded = false;
            $scope.startDate = (Date.parse(new Date().toString("yyyy-MM-dd")))
                    .addDays(-7).toString("MM/dd/yyyy");
            $scope.endDate = (Date.parse(new Date().toString("yyyy-MM-dd")))
                    .toString("MM/dd/yyyy");

            var options = {
                autoHorizontalScrollBar : true,
                applyFilteringOnGrid : true,
                showHeaderRow : true,
                maxHeight : 500,
                forceFitColumns : true,
                summaryRow : false,
                displaySummaryRow : false,
                exportToExcel : true,
                sheetName : "position data change",
                sortList : [ {
                    columnId : "date",
                    sortAsc : true
                } ]
            };

            var columns = [
              {
                id : "date",
                name : "Date",
                field : "date",
                toolTip : "Date",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            },{
                id : "positionLevelCount",
                name : "Position Level Count",
                field : "positionLevelCount",
                toolTip : "Position Level Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "agreementLevelCount",
                name : "Agreement Level Count",
                field : "agreementLevelCount",
                toolTip : "Agreement Level Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true,
                minWidth : 100
            }, {
                id : "bundlePositionLevelCount",
                name : "Bundle Position Level Count",
                field : "bundlePositionLevelCount",
                toolTip : "Bundle Position Level Count",
                type : "number",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }];

            $scope.positionDataChangeOptions = {
                data : [],
                options : options,
                columns : columns,
                emptyMessage : "No data is available"
            };

            var getParams = function() {

                var dateParts = $scope.startDate.split("/");
                var date1 = dateParts[2] + "-" + dateParts[0] + "-"
                        + dateParts[1];

                dateParts = $scope.endDate.split("/");
                var date2 = dateParts[2] + "-" + dateParts[0] + "-"
                        + dateParts[1];

                var params = new Object();
                params.startDateString = date1;
                params.endDateString = date2;

                return params;
            };
            
            $scope.getDate = function(dateString)
            {
                var year        = dateString.substring(0,4);
                var month       = dateString.substring(5,7);
                var day         = dateString.substring(8,10);
                return Date.UTC(year,month - 1,day);
            };

            $scope.dateError = function(){
                var dateParts = $scope.startDate.split("/");
                var date1 = Date.UTC(dateParts[2],dateParts[0],dateParts[1]);
                dateParts = $scope.endDate.split("/");
                var date2 = Date.UTC(dateParts[2],dateParts[0],dateParts[1]);
                if(date1<date2) return false;
                return true;
            };

            $scope.fetchData = function() {
                $scope.isLoaded = false;
                var positionDataChangeREST = $resource(
                        "/treasury/ops/search-position-data-change",
                        getParams(), {
                            'query' : {
                                method : 'get',
                                isArray : false
                            }
                        });
                var operation = positionDataChangeREST.query();
                $scope.operation = operation;
                $scope.operation.$promise.then(function(result) {
                    data = result["resultList"];
                    $scope.positionDataChangeOptions.data = data;
                    $scope.isLoaded = true;

                    // Configuration of HighChart
                       var series = [];
                       var lines = ["agreementLevelCount","positionLevelCount", "bundlePositionLevelCount"];
                       
                       if(!data || data.length==0) {
                           series.push({data:[]});
                       } else {
                       		data.sort(function(a,b) {
                       		  return a.id - b.id;
                      		});
                       	    data.reverse();
                       	    $scope.positionDataChangeOptions.data = data;
                       	
                       	    for (var j = 0; j < lines.length; j++)
                       	    {
	                               var line = lines[j];
	                               var chartData = [];
	                               var rows = data;
	                               for(var i=0; i< rows.length; i++){
	                                   chartData.push([$scope.getDate(rows[i]["date"]), rows[i][line]]);
	                               }
	                               series.push({
	                                   data : chartData,
	                                   name : line
	                               });
                       	    }
                       }

                       $scope.positionDataChangeConfig = {
                               options: {
                                   chart: {
                                       type: 'line'
                                   }
                               },
                               title: {
                                   text: '',
                                   style:{
                                       display: 'none'
                                   }
                               },
                               series: series,
                               xAxis: {
                                     type: 'datetime',
                                       dateTimeLabelFormats: {
                                           day: '%b %e, %Y'
                                       }
                               },
                               yAxis: {
                               	type: 'logarithmic',
                                   minorTickInterval: 0.1
                               },
                           };
                });
            };
            $scope.fetchData();
        } ])
        .directive('ngTabs', function(){
            return {
                restrict: 'A',
                link: function(scope, elm, attrs) {
                    var jqueryElm = $(elm[0]);
                    $(jqueryElm).tabs();
                }
         };
        });