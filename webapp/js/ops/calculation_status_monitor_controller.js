// Controllers of sync-job-status-monitor gadget
dportal.ops.opsProgramApp.controller('calculationMonitorController', [
        '$scope',
        '$resource',
        function($scope, $resource) {

            $scope.isLoaded = false;
            $scope.jobs = 20;
            $scope.dateValue = (Date.parse(new Date().toString("yyyy-MM-dd")))
                    .addDays(-2).toString("MM/dd/yyyy");

            var options = {
                autoHorizontalScrollBar : true,
                applyFilteringOnGrid : true,
                showHeaderRow : true,
                maxHeight : 500,
                forceFitColumns : true,
                summaryRow : false,
                displaySummaryRow : false,
                exportToExcel : true,
                sheetName : "Job Run Status",
                sortList : [ {
                    columnId : "date",
                    sortAsc : false
                } ]
            };

            var columns = [ {
                id : "id",
                name : "ID",
                field : "id",
                toolTip : "ID",
                type : "number",
                filter : true,
                sortable : true,
                headerCssClass : "b",
                minWidth : 100
            }, {
                id : "date",
                name : "Date Time",
                field : "date",
                toolTip : "Date Time",
                type : "text",
                filter : true,
                sortable : true,
                headerCssClass : "b",
                minWidth : 100
            }, {
                id : "status",
                name : "Status",
                field : "status",
                toolTip : "Status",
                type : "text",
                headerCssClass : "b",
                filter : true,
                sortable : true
            }, {
                id : "command",
                name : "Command",
                field : "command",
                toolTip : "Command",
                type : "text",
                filter : true,
                sortable : true,
                headerCssClass : "b",
                minWidth : 70
            } ];

            $scope.jobRunStatusOptions = {
                data : [],
                options : options,
                columns : columns,
                emptyMessage : "No data is available"
            };

            var getParams = function() {
                var dateParts = $scope.dateValue.split("/");
                var date = dateParts[2] + "-" + dateParts[0] + "-"
                        + dateParts[1];

                var params = new Object();
                params.startDateString = date;
                params.lastN = $scope.jobs;
                return params;
            };

            $scope.fetchData = function() {
                $scope.isLoaded = false;

                var jobRunREST = $resource(
                        "/treasury/ops/search-calculation-status-detail",
                        getParams(), {
                            'query' : {
                                method : 'get',
                                isArray : false
                            }
                        });
                var operation = jobRunREST.query();
                $scope.operation = operation;

                $scope.operation.$promise.then(function(result) {
                    data = result["resultList"];
                    $scope.jobRunStatusOptions.data = data;
                    $scope.isLoaded = true;
                });

            };

            $scope.fetchData();

        } ]);