// Controllers of sync-job-performance gadget
dportal.ops.opsProgramApp.controller('syncJobPerformanceCtrl', [
        '$scope',
        '$resource',
        function($scope, $resource) {
            $scope.isLoaded = false;
            $scope.showChart = true;
            $scope.startDate = (Date.parse(new Date().toString("yyyy-MM-dd")))
                    .addDays(-30).toString("MM/dd/yyyy");
            $scope.endDate = (Date.parse(new Date().toString("yyyy-MM-dd")))
                    .toString("MM/dd/yyyy");

            $scope.getDate = function(dateString)
            {
                var year        = dateString.substring(0,4);
                var month       = dateString.substring(5,7);
                var day         = dateString.substring(8,10);
                return Date.UTC(year,month - 1,day);
            };

            $scope.dateError = function(){
                var dateParts = $scope.startDate.split("/");
                var date1 = Date.UTC(dateParts[2],dateParts[0],dateParts[1]);
                dateParts = $scope.endDate.split("/");
                var date2 = Date.UTC(dateParts[2],dateParts[0],dateParts[1]);
                if(date1>date2) return true;
                return false;
            };

            $scope.fetchData = function() {
            	
            	var sDate = new Date($scope.startDate);
            	var eDate = new Date($scope.endDate);
            	
            	var timeDiff = Math.abs(sDate.getTime() - eDate.getTime());
            	$scope.diffDays = Math.ceil(0.5 + (timeDiff / (1000*3600*24)));
            	
                $scope.isLoaded = false;
                
                var nameOfJobs = ['PositionSync', 'MarginSync','CommissionSync','ExposureSync','CollateralSync','NULL','RiskSync','ReportingDataSync','ImpactDataSync','ImpactDetailPositionDataMapSync','ExcessDeficitSync','BrokerRevenueDataSync', 'BoxPositionIdentifier', 'MarginAgeCalculatorSync', 'BoxPositionAgeCalculatorSync'];
                	
            	var dateParts = $scope.startDate.split("/");
                var date1 = dateParts[2] + "-" + dateParts[0] + "-"
                        + dateParts[1];

                dateParts = $scope.endDate.split("/");
                var date2 = dateParts[2] + "-" + dateParts[0] + "-"
                        + dateParts[1];

                var params = new Object();
                params.startDateString = date1;
                params.endDateString = date2;
                params.startHour = 0;
            	
                var syncJobPerformanceREST = $resource(
                        "/treasury/ops/search-hourwise-sync-job-detail", params,
                        {
                            'query' : {
                                method : 'get',
                                isArray : false
                            }
                        });
                var syncJobPerformanceRESTOperation = syncJobPerformanceREST.query();
                $scope.syncJobPerformanceRESTOperation = syncJobPerformanceRESTOperation;
                $scope.syncJobPerformanceRESTOperation.$promise.then(function(result) {
                    data = result["resultList"];
                    
                    series = [];
                    for ( var i = 0; i < 15; ++i ) {
                    	emptyData = [];
                    	for ( var j = 0; j < 24; ++j ) {
                    		emptyData.push(0);
                    	}
                        if( i != 5 ) series.push({name:nameOfJobs[i],data:emptyData});
                    }
                    
                    var hcat = [];
                    for ( var i = 0; i < 24; ++i ) {
                        hcat.push(""+i);
                    }
                    
                    var i = 0;
                    
                    while ( data !== undefined && i < data.length ) {
                    	
                    	var syncTypeId = data[i]["syncTypeId"]-1;
                    	if ( syncTypeId > 5 ) syncTypeId = syncTypeId - 1;
                    	
                    	var hourType = data[i]["startHour"];
                    	
                    	series[syncTypeId]["data"][hourType] += data[i]["totalTimeSpent"];
                    	
                    	i += 1;
                    	
                    }
                    
                    for ( var i = 0; i < 14; ++i ) {
                    	for ( var j = 0; j < 24; ++j ) {
                    		series[i]["data"][j] = parseInt(series[i]["data"][j] / 60);
                    		series[i]["data"][j] = series[i]["data"][j] / $scope.diffDays;
                    	}
                    }
                    
                	$scope.hourwiseSyncJobPerformanceConfig = {
                            options: {
                                chart: {
                                    type: 'column',
                                    zoomType: 'xy'
                                }
                            },
                            title: {
                                text: ''
                            },
                            xAxis: {
                            	title: {
                            		text: 'Hour Period'
                            	},
                                categories: hcat
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Total time (in minutes/day)'
                                },
                                stackLabels: {
                                    enabled: true
                                }
                            },
                            series: series
                        };
                	
                	$scope.isLoaded = true;
                });
                
                
            };
            $scope.fetchData();
            
            
        } ]);