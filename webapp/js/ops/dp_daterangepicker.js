/**
 *  dpDaterangepicker directive used for showing the dportal's daterangepicker.
 *  Format ==>
 *  <input type="text" send-module-id="__MODULE_ID__" dp-daterangepicker="dateRangePickerConfig" ng-model="dateRangeValue">
 *  <img src="/static/images/calendar.gif" style="margin-left:-39px;pointer-events:none;" class="dmiddle"/>
 *  
 *  @author: gargd
 */
(function(modules) {
    for (var id in modules) {
        var module = modules[id];
            module.directive("dpDaterangepicker", function factory() {

    return {
            restrict : "A",
            require  : "?ngModel",
            link     : function postLink (scope, iElement, iAttrs,ngModel) {
                if (!ngModel) {
                    return;
                }
                
                // Specify how UI should be updated
                ngModel.$render = function() {
                    if(ngModel.$modelValue == "" || ngModel.$modelValue == "-"){
                        iElement.val(null);
                        ngModel.$setViewValue(null);
                    }else{
                        iElement.val(ngModel.$viewValue);
                    }
                }; 

                console.log(iElement.attr('send-module-id'));

                setupDaterangepicker({
                    input    : iElement,
                    errorDiv : $("#dateError-"+iElement.attr('send-module-id')),
                    options  : {
                        autoupdateField: true,
                        extendPresetRanges: false,
                        presetRanges: [{text: 'Today', dateStart: 'today', dateEnd: 'today' },{text: 'Yesterday', dateStart: 'today-1days', dateEnd: 'today-1days' }]
                    }
                });
                
        		iElement.css('padding-right','3em');
        		iElement.css('width','12em');
        		iElement.css('margin-right','0px');
        		iElement.css('cursor','pointer');
        		
		        iElement.daterangepicker("setDate", Date.parse('today'), Date.parse('today'));
		
				iElement.attr('readonly','readonly');
				iElement.bind('input keydown change', function(event) {
					iElement.attr('readonly','readonly');
					scope.$apply( function() { console.log(ngModel.$viewValue);  });
				});

            }
        };
    });
    }
})([dportal.ops.opsProgramApp]);