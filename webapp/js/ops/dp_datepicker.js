/**
 *  dpDatepicker directive used for showing the dportal's datepicker.
 *  Format ==>  <input  type="text" dp-datepicker="datePickerConfig" ng-model="dateValue">
 */
(function(modules) {
    for (var id in modules) {
        var module = modules[id];
            module.directive("dpDatepicker", function factory() {
    var defaultOptions = {
            dateFormat      : 'mm/dd/yy',
            changeMonth     : true,
            changeYear      : true,
            minDate         : Date.parse('1970-01-01'),
            maxDate         : Date.parse('2100-12-31'),
            showOn          : "button",
            buttonText      : "",
            buttonImage     : "/static/images/calendar.gif",
            buttonImageOnly : true
          };

    var isValidDate = function (date)
    {
        var regex = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
        if(!date){
            return 0;
        }
        if (!date.match(regex)) {
            return -2;
        }
        var dateParts = date.split("-");
        var dPart=dateParts[2],
            mPart=dateParts[1],
            yPart=dateParts[0];

        if (mPart < 1 || mPart > 12) {
            return -4;
        }
        if (dPart < 1 || dPart > 31) {
            return -5;
        }
        if ((mPart == 2 || mPart == 4 || mPart == 6 || mPart == 9 || mPart == 11) && dPart > 30) {
            return -5;
        }
        if (mPart == 2) {
            if (dPart > 29) {
                return -5;
            }
            if (!(yPart % 400 === 0 || (yPart % 4 === 0 && yPart % 100 !== 0)) && dPart > 28) {
                return -5;
            }
        }
        return 0;
    };

    return {
            restrict : "A",
            require  : "?ngModel",
            link     : function postLink (scope, iElement, iAttrs,ngModel) {
                if (!ngModel) {
                    return;
                }

                // Specify how UI should be updated
                ngModel.$render = function() {
                    if(ngModel.$modelValue == "" || ngModel.$modelValue == "-"){
                        iElement.val(null);
                        ngModel.$setViewValue(null);
                    }else{
                        iElement.val(ngModel.$viewValue);
                    }
                };

                defaultOptions.defaultDate = iElement.val();
                iElement.datepicker(defaultOptions);

                /*
                 * input : input is required to capture the datepicker text box as
                 * and when use inputs into it.
                 * keydown : this is mainly to capture the backspace in ie.
                 * change :  this will capture the text box changes and calls the callback
                 * if the value is changed
                 */
                iElement.bind('input keydown change',function(event){
                    var value = iElement.val();
                    /*
                     * IE doesn't take backspace and delete as input.
                     * so taking key-down  and checking if it is
                     * backspace, delete or any other key value.
                     */
                    if(event.keyCode === 8 || event.keyCode === 46){
                        if(value != "" || value != null){
                            value = value.substring(0,value.length-1);
                        }
                    }
                    scope.$apply(function(){
                        if(value.trim() == ""  || value == null){
                            ngModel.$setViewValue(null);
                            /*
                             * As this is a custom directive we need to
                             * explicitly say it is either valid or not.
                             */
                            ngModel.$setValidity("badInput",true);
                        }else{
                            if(isValidDate(value) != 0 ){
                                ngModel.$setValidity("badInput",false);
                            }else{
                                ngModel.$setValidity("badInput",true);
                                ngModel.$setViewValue(value);
                            }
                        }
                    });
                });
            }
        };
    });
    }
})([dportal.ops.opsProgramApp]);