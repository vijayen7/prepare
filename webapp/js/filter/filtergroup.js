"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.filter = window.treasury.filter || {};
    window.treasury.filter.FilterGroup = _FilterGroup;

    /**
     * MultiSelectFilterArray is list of Multi Select Filter array
     * singleSelectArray is list of Single Select Filter array
     * dateFilter jQuery object
     * dateRangeFilter jQuery object
     */
    function _FilterGroup(multiSelectFilterArray, singleSelectFilterArray, dateFilter, dateRangeFilter) {
        this.multiSelectFilterArray = multiSelectFilterArray;
        this.singleSelectFilterArray = singleSelectFilterArray;
        this.dateFilter = dateFilter;
        this.dateRangeFilter = dateRangeFilter;
    }

    /**
     *
     * @returns {parameter map with selected ids} The paramter map contains ids
     *          of single select and multi select both
     */
    _FilterGroup.prototype.getParameterMap = function() {
        var parameterMap = {};
        if (this.multiSelectFilterArray) {
            for (var i = 0; i < this.multiSelectFilterArray.length; i++) {
                var multiSelect = this.multiSelectFilterArray[i];
                parameterMap[multiSelect.paramKey] = multiSelect
                        .getSelectedIds();
            }
        }

        if (this.singleSelectFilterArray) {
            for (var j = 0; j < this.singleSelectFilterArray.length; j++) {
                var singleSelect = this.singleSelectFilterArray[j];
                parameterMap[singleSelect.paramKey] = singleSelect
                        .getSelectedId();
            }
        }

        if (this.dateFilter) {
            parameterMap.dateString = this.dateFilter.val();
        }

        if (this.dateRangeFilter) {
            var dates = this.dateRangeFilter.daterangepicker("getDate");
            parameterMap.startDateString = dates[0];
            parameterMap.endDateString = dates[1];
        }

        return parameterMap;
    };

    /**
     *
     * @returns {parameter map with selected ids} The paramter map contains ids
     *          of single select and multi select both. Multi-select ids would
     *          be a comma-separated string.
     */
    _FilterGroup.prototype.getSerializedParameterMap = function() {
        var serializedParameterMap = this.getParameterMap();

        if (this.multiSelectFilterArray) {
            for (var i = 0; i < this.multiSelectFilterArray.length; i++) {
                var multiSelect = this.multiSelectFilterArray[i];
                serializedParameterMap[multiSelect.paramKey] = multiSelect
                        .getSerializedSelectedIds();
            }
        }

        return serializedParameterMap;
    };

    /**
     *
     * @param params(
     *            is a map of filter and elements to select) selects the
     *            elements passed through the params map
     */
    _FilterGroup.prototype.setParameterMap = function(parameterMap) {
        if (parameterMap) {
            if (this.multiSelectFilterArray) {
                for (var i = 0; i < this.multiSelectFilterArray.length; i++) {
                    var multiSelect = this.multiSelectFilterArray[i];
                    if (parameterMap.hasOwnProperty(multiSelect.paramKey)) {
                        multiSelect
                                .setSelectedNodes(parameterMap[multiSelect.paramKey]);
                    }
                }
            }

            if (this.singleSelectFilterArray) {
                for (var j = 0; j < this.singleSelectFilterArray.length; j++) {
                    var singleSelect = this.singleSelectFilterArray[j];
                    if (parameterMap.hasOwnProperty(singleSelect.paramKey)) {
                        singleSelect
                                .setSelectedNode(parameterMap[singleSelect.paramKey]);
                    }
                }
            }

            if (this.dateFilter && parameterMap.dateString) {
                this.dateFilter.val(parameterMap.dateString);
            }

            if (this.dateRangeFilter && parameterMap.startDateString && parameterMap.endDateString) {
                this.dateRangeFilter.daterangepicker("setDate", parameterMap.startDateString, parameterMap.endDateString);
            }
        }
    };

})();
