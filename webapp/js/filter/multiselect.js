"use strict";

(function() {

    /*
     * paramKey - used in the query params during ajax requests. eg:
     * "cpeFamilyIds" componentId - filter component id. msbList - [[-1, "All"],
     * [2, "Liquid"], ...] selectedIds - [-1] or [1,2] format.
     */

    window.treasury = window.treasury || {};
    window.treasury.filter = window.treasury.filter || {};
    window.treasury.filter.MultiSelect = _MultiSelect;

    function _MultiSelect(paramKey, componentId, inputFilterList, selectedIds) {
        this.paramKey = paramKey;
        this.componentId = componentId;
        this.inputFilterList = inputFilterList;
        this.selectedIds = selectedIds;

        var filterMap = [];
        for (var i = 0; i < this.inputFilterList.length; i++) {
            filterMap.push({
                key : this.inputFilterList[i][0],
                value : this.inputFilterList[i][1]
            });
        }
        this.filterMap = filterMap;

        // Calling initialize method.
        this.initialize();
        this.setSelectedNodes(selectedIds);

    }
    ;

    /**
     * Initialize will add data to the filter component
     */
    _MultiSelect.prototype.initialize = function() {
               // if dome elem passed in undefined
        if (!this.componentId || !this.inputFilterList) {
            return;
        }

        // set the data element of the multi-select
        var data = [];
        for (var i = 0; i < this.inputFilterList.length; i++) {
              if (this.inputFilterList[i][0] != -1) {
                data.push({
                    'key' : this.inputFilterList[i][0],
                    'value' : this.inputFilterList[i][1]
                });
            }
        }
        var component = document.getElementById(this.componentId);
        component.data = data;
    };

    /**
     * returns selected list of Nodes for this filter
     * 
     * @returns
     */
    _MultiSelect.prototype.getSelectedIds = function() {
        var filterComponent = document.getElementById(this.componentId);
        var selectedNodeIds = [];
        if (filterComponent.value !== undefined && filterComponent.value.selectedNodes !== undefined) {
            for (var i = 0; i < filterComponent.value.selectedNodes.length; i++) {
                selectedNodeIds
                        .push(filterComponent.value.selectedNodes[i].key);
            }
        } else {
        	for (var i = 0; i < filterComponent.value.length; i++) {
                selectedNodeIds
                        .push(filterComponent.value[i].key);
            }
        }
        if (selectedNodeIds.length == 0) {
            selectedNodeIds.push(-1);
        }
        return selectedNodeIds;
    };

    /**
     * 
     * @param selectedNodes
     *            selects the filters values identified by the selectedNodes
     *            param
     */
    _MultiSelect.prototype.setSelectedNodes = function(selectedNodeIds) {
        var component = document.getElementById(this.componentId);
        var isAllSelected = false;
        var doReset = false;
        var filterNodes = component.data;
        var filterNodesMap = {};
        for (var j = 0; j < filterNodes.length; j++) {
            if (filterNodes[j] != null) {
                filterNodesMap[filterNodes[j].key] = filterNodes[j];
            }
        }

        var nodesToSelect = [];

        for (var i = 0; i < selectedNodeIds.length; i++) {
            if (selectedNodeIds[i] in filterNodesMap) {
                filterNodesMap[selectedNodeIds[i]].selected = true;
                nodesToSelect.push(filterNodesMap[selectedNodeIds[i]]);
            }
            
            if (selectedNodeIds[i] == -1) {
                doReset = true;
                break;
            }
        }
        
        if(nodesToSelect.length === filterNodes.length) {
            isAllSelected = true;
        }

        if (isAllSelected === true) {
            component.value = {
                status : "all"
            };
        } else if (nodesToSelect.length > 0 && doReset === false) {
            component.value = {
                status : "partial",
                selectedNodes : nodesToSelect
            };
        } else {
            component.value = {
                status : "none"
            };
        }
    };
    
    /*
     * returns a csv formatted string of selected ids.
     */
    _MultiSelect.prototype.getSerializedSelectedIds = function() {
        return this.getSelectedIds() === undefined ? "-1" : this.getSelectedIds().join(",");
    };
})();