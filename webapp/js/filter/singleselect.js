"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.filter = window.treasury.filter || {};
    window.treasury.filter.SingleSelect = _SingleSelect;

    /*
     * paramKey - used in the query params during ajax requests. eg:
     * "cpeFamilyIds" componentId - filter component id. inputFilterList - [[-1,
     * "All"], [2, "Liquid"], ...] selectedIds - [-1] or [1,2] format.
     */
    function _SingleSelect(paramKey, componentId, inputFilterList, selectedId) {
        this.paramKey = paramKey;
        this.componentId = componentId;
        this.inputFilterList = inputFilterList;
        this.selectedId = selectedId;

        this.inputFilterMap = [];
        for (var i = 0; i < this.inputFilterList.length; i++) {
            this.inputFilterMap.push({
                key : this.inputFilterList[i][0],
                value : this.inputFilterList[i][1]
            });
        }

        // Calling initialize method to populate all nodes in the singleselect.
        this.initialize();
        this.setSelectedNode(selectedId);

    };

    /**
     * Initialize will add all nodes data to the filter component
     */
    _SingleSelect.prototype.initialize = function() {
        var component = document.getElementById(this.componentId);
        
        // if some elem passed in undefined
        if (!this.componentId || !this.inputFilterList) {
            return;
        }

        // set the data element of the multi-select
        var data = [];
        for (var i = 0; i < this.inputFilterList.length; i++) {
           
                data.push({
                    'key' : this.inputFilterList[i][0],
                    'value' : this.inputFilterList[i][1]
                });
        }

        component.data = data;
    };

    /**
     * returns selected list of NodeIds for this filter
     * 
     * @returns
     */
    _SingleSelect.prototype.getSelectedId = function() {
        var component = document.getElementById(this.componentId);
        if (component.value !== null && component.value !== undefined) {
            return component.value.key;		
        }
        return -1;
    };

    /**
     * 
     * @param selectedNodes
     *            selects the filters values identified by the selectedNodeIds
     *            param
     */
    _SingleSelect.prototype.setSelectedNode = function(selectedNodeId) {
        var component = document.getElementById(this.componentId);
        var filterNodes = component.data;
        var filterNodesMap = {};
        for (var j = 0; j < filterNodes.length; j++) {
            if (filterNodes[j] != null) {
                filterNodesMap[filterNodes[j].key] = filterNodes[j];
            }
        }

        if (selectedNodeId in filterNodesMap) {
            component.value = filterNodesMap[selectedNodeId];
        }

    };

})();