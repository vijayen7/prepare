/*jshint jquery:true*/
"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.loadfilter = window.treasury.loadfilter || {};
  window.treasury.loadfilter = {
    sourceType: _loadSourceType,
    reconciliationSyncType:_loadReconciliationSyncType,
    legalEntities: _loadLegalEntities,
    cpeFamilies: _loadCpeFamilies,
    defaultDates: _loadDefaultDates,
    loadSecurityTypes: _loadSecurityTypes,
    loadEntityFamilies: _loadEntityFamilies,
    loadAgreementTypes: _loadAgreementTypes,
    currencies: _loadCurrencies,
    nettingGroups: _loadNettingGroups,
    nettingGroupsForAgreementId : _loadNettingGroupsForAgreementId,
    interestCalculatorGroups: _loadInterestCalculatorGroups,
    cpes: _loadCpes,
    agreementIds: _loadAgreementIds,
    financingTypes: _loadFinancingTypes,
    financingStyles: _loadFinancingStyles,
    assetClassGroups: _loadAssetClassGroups,
    agmtTermsAttributes: _loadAtrributesFilter,
    accrualConventions: _loadAccrualConventions,
    bandwidthGroups: _loadBandwidthGroups,
    countries: _loadCountries,
    fotypes: _loadFOTypes,
    foSubtypes: _loadSubtypes,
    assetClassAttributes: _loadAttributeNames,
    assetClassAttrSourceTypes: _loadAttrSourceTypes,
    gradings: _loadGrading,
    baseRates: _loadBaseRates,
    chargeTypes: _loadChargeTypes,
    loadBundles: _loadBundles,
    loadBusinessUnits: _loadBusinessUnits,
    loadCustodianAccounts: _loadCustodianAccounts,
    loadNettingGroupSpecificCustodianAccounts: _loadNettingGroupSpecificCustodianAccounts,
    loadAccrualPostingSpecialBundles: _loadAccrualPostingSpecialBundles,
    loadAccrualPostingSpecialBu: _loadAccrualPostingSpecialBu,
    loadAccrualPostingSpecialCa: _loadAccrualPostingSpecialCa,
    negotiationTypes: _loadNegotiationTypes,
    loadOwnershipEntities: _loadOwnershipEntities,
    loadOwnershipEntitiesFromFuma: _loadOwnershipEntitiesFromFuma,
    loadGboTypes: _loadGboTypes,
    loadMarkets: _loadMarkets,
    loadBooks: _loadBooks,
    loadSeclendDataTypes: _loadSeclendDataTypes,
    loadAgreementTypeSpecificNettingGroupFilter: _loadAgreementTypeSpecificNettingGroupFilter,
    loadApplyNegativeRate:_loadApplyNegativeRate,
    allFinancingStyles: _loadAllFinancingStyles,
    loadMarginCalculators: _loadMarginCalculators,
    loadMarginConfigs: _loadMarginConfigs,
    loadMarginTypes: _loadMarginTypes,
    loadOptionTypes: _loadOptionTypes,
  };

  function _loadLegalEntities() {
    return $.ajax({
      url: "/treasury/data/load-legal-entity-filter",
      dataType: "json"
    });
  }

  function _loadDefaultDates() {
    return $.ajax({
      url: "/treasury/data/load-date-filter",
      dataType: "json"
    });
  }

  function _loadCpeFamilies() {
    return $.ajax({
      url: "/treasury/data/load-cpe-family-filter",
      dataType: "json"
    });
  }

  function _loadSecurityTypes() {
    return $.ajax({
      url: "/treasury/data/load-security-types",
      dataType: "json"
    });
  }

  function _loadEntityFamilies() {
    return $.ajax({
      url: "/treasury/data/load-legal-entity-family-filter",
      dataType: "json"
    });
  }

  function _loadAgreementTypes() {
    return $.ajax({
      url: "/treasury/data/load-agreemen-type-filter",
      dataType: "json"
    });
  }

  function _loadCurrencies() {
    return $.ajax({
      url: "/treasury/data/load-currencies-filter",
      dataType: "json"
    });
  }

  function _loadNettingGroups() {
    return $.ajax({
      url: "/treasury/data/load-nettingGroups-filter",
      dataType: "json"
    });
  }

	function _loadNettingGroupsForAgreementId(agreementId) {
		return $.ajax({
			url : "/treasury/data/load-netting-group-by-agreement-id",
			data : {
				'agreementId' : agreementId
			},
			type : "POST",
			dataType : "json"
		});
	}

  function _loadInterestCalculatorGroups(parameters) {
    return $.ajax({
      url: "/treasury/data/load-calculatorGroups-filter",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
  }

  function _loadCpes() {
    return $.ajax({
      url: "/treasury/data/load-cpe-filter",
      dataType: "json"
    });
  }

  function _loadAgreementIds() {
    return $.ajax({
      url: "/treasury/data/load-agreemen-id-filter",
      dataType: "json"
    });
  }

  function _loadFinancingTypes() {
    return $.ajax({
      url: "/treasury/data/load-financing-type-filter",
      dataType: "json"
    });
  }

  function _loadApplyNegativeRate() {
	    return $.ajax({
	      url: "/treasury/data/load-apply-negative-irate-filter",
	      dataType: "json"
	    });
	  }



  function _loadFinancingStyles() {
    return $.ajax({
      url: "/treasury/data/load-financing-style-filter",
      dataType: "json"
    });
  }

  function _loadAllFinancingStyles() {
	    return $.ajax({
	      url: "/treasury/data/load-all-financing-style-filter",
	      dataType: "json"
	    });
	  }

  function _loadChargeTypes() {
    return $.ajax({
      url: "/treasury/data/load-charge-type-filter",
      dataType: "json"
    });
  }

  function _loadAccrualConventions() {
    return $.ajax({
      url: "/treasury/data/load-accrual-convention",
      dataType: "json"
    });
  }

  function _loadBandwidthGroups() {
    return $.ajax({
      url: "/treasury/data/load-bandwidth-group",
      dataType: "json"
    });
  };

  function _loadAssetClassGroups() {
    return $.ajax({
      url: "/treasury/data/load-asset-class-group-filter",
      dataType: "json"
    });
  }

  function _loadAtrributesFilter() {
    return $.ajax({
      url: "/treasury/data/load-attributes-filter",
      dataType: "json"
    });

  }

  function _loadGrading() {
    return $.ajax({
      url: "/treasury/data/load-grading",
      dataType: "json"
    });
  }

  function _loadAttrSourceTypes() {
    return $.ajax({
      url: "/treasury/data/load-attrSourceTypes",
      dataType: "json"
    });
  }

  function _loadAttributeNames(sourceType) {
    return $.ajax({
      url: "/treasury/data/load-attrNames",
      data: {
        'sourceTypeId': sourceType
      },
      type: "POST",
      dataType: "json"
    });
  }

  function _loadFOTypes() {
    return $.ajax({
      url: "/treasury/data/load-FOTypes",
      dataType: "json"
    });
  }

  function _loadSubtypes(foType) {
    return $.ajax({
      url: "/treasury/data/load-subtypes",
      data: {
        'foTypeId': foType
      },
      type: "POST",
      dataType: "json"
    });
  }

  function _loadCountries() {
    return $.ajax({
      url: "/treasury/data/load-countries",
      dataType: "json",
    });
  }

  function _loadBaseRates(currencyId) {
    return $.ajax({
      url: "/treasury/data/load-base-rate-filter",
      data: {
      'currencyId': currencyId
      },
      type: "POST",
      dataType: "json"
    });
  }

  function _loadBundles() {
    return $.ajax({
      url: "/treasury/data/load-bundles",
      dataType: "json"
    });
  }

  function _loadCustodianAccounts() {
    return $.ajax({
      url: "/treasury/data/load-custodian-accounts-filter",
      dataType: "json"
    });
  }

  function _loadNettingGroupSpecificCustodianAccounts() {
    return $.ajax({
      url: "/treasury/data/load-netting-group-specific-custodian-accounts-filter",
      dataType: "json"
    });
  }

  function _loadAgreementTypeSpecificNettingGroupFilter(parameters) {
    return $.ajax({
      url: "/treasury/data/load-agreement-type-specific-netting-group-filter",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
  }

  function _loadBusinessUnits() {
    return $.ajax({
      url: "/treasury/data/load-all-business-unit",
      dataType: "json"
    });
  }

  function _loadAccrualPostingSpecialBu() {
    return $.ajax({
      url: "/treasury/data/load-accrual-posting-special-business-units",
      dataType: "json"
    });
  }

  function _loadAccrualPostingSpecialBundles() {
    return $.ajax({
      url: "/treasury/data/load-accrual-posting-special-bundles",
      dataType: "json"
    });
  }

  function _loadAccrualPostingSpecialCa() {
    return $.ajax({
      url: "/treasury/data/load-accrual-posting-special-custodian-accounts",
      dataType: "json"
    });
  }

  function _loadNegotiationTypes(){
      return $.ajax({
          url: "/treasury/data/load-negotiation-type-filter",
          dataType: "json"
        });
  }

  function _loadOwnershipEntities() {
    return $.ajax({
      url: "/treasury/data/load-ownership-entity-filter",
      dataType: "json"
    });
  }

  function _loadFinancialAccount() {
	  return $.ajax({
		  url:"/treasury/data/load-financial-account-filter",
		  dataType: "json"
	  });
  }
  function _loadSourceType() {
	  return $.ajax({
		  url:"/treasury/data/load-source-type-filter",
		  dataType: "json"
	  });
  }

  function _loadReconciliationSyncType() {
    return $.ajax({
        url:"/treasury/data/load-reconciliation-sync-type-filter",
        dataType: "json"
    });
  }

  function _loadOwnershipEntitiesFromFuma() {
      return $.ajax({
        url: "/treasury/data/load-ownership-entities-from-fuma",
        dataType: "json"
      });
  }

  function _loadGboTypes() {
      return $.ajax({
        url: "/treasury/data/load-gbo-type-filter",
        dataType: "json"
      });
    }

  function _loadMarkets() {
      return $.ajax({
        url: "/treasury/data/load-market-filter",
        dataType: "json"
      });
    }

  function _loadBooks() {
    return $.ajax({
      url: "/treasury/data/load-book-filter",
      dataType: "json"
    });
  }

  function _loadSeclendDataTypes() {
    return $.ajax({
      url: "/treasury/data/load-seclend-data-type-filter",
      dataType: "json"
    });
  }

  function _loadMarginCalculators(){
    return $.ajax({
      url: "/treasury/data/load-margin-calculator-filter",
      dataType: "json"
    });
  }

  function _loadMarginConfigs(){
    return $.ajax({
      url: "/treasury/data/load-margin-config-filter",
      dataType: "json"
    });
  }

  function _loadMarginTypes() {
    return $.ajax({
      url: "/treasury/data/load-margin-type-filter",
      dataType: "json"
    });
  }

  function _loadOptionTypes() {
    return $.ajax({
      url: "/treasury/data/load-option-type-filter",
      dataType: "json"
    });
  }

})();
