"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.filter = window.treasury.filter || {};
    window.treasury.filter.SecuritySearch = loadSecurityData;

    /*
     * searchString - used in the query params during ajax requests.
     */
    function loadSecurityData(searchString, domElementId) {
    	//remove everything upto last occurence of comma to get the data to search for
        searchString = searchString.substring(searchString.lastIndexOf(",") + 1);
    	this.searchString = searchString;
        this.domElementId = domElementId;

        $('#' + domElementId)
                .autocomplete(
                        {
                            source : function(request, response) {
                                var url = "/treasury/load-security-search-filter";
                                var pars = {
                                    'searchString' : searchString,
                                    'limit' : 100
                                };
                                $.ajax({
                                    url : url,
                                    data : pars,
                                    type : "POST",
                                    dataType : "text",
                                    success : function(data) {
                                        var dataArray = data.split("\n");
                                        response($.map(dataArray,
                                                function(item) {
                                                    if (item.trim()) {
                                                        return item.trim();
                                                    }
                                                }));
                                    }
                                });
                            },
                            search : function() {
                                $(this).addClass("sec_loading");
                            },
                            open : function() {
                                $(this).addClass("sec_input").removeClass(
                                        "sec_loading");
                            },
                            close : function() {
                                $(this).removeClass("sec_input").removeClass(
                                        "sec_loading");
                            },
                            minLength : 3,
                            select : function(e, ui) {
                                var types = ui.item.label.split('~');
                                $('#' + domElementId)[0].value = $('#'
                                        + domElementId)[0].value.substring(0,
                                        $('#' + domElementId)[0].value
                                                .lastIndexOf(',') + 1)
                                        + types[0] + ',';
                                return false;
                            }
                        }).data('ui-autocomplete')._renderItem = function(ul,
                item) {
            var types = item.label.split('~');
            var newLabel = "<div>"
                    + "<div class='aln-lft aln-top ac-spn ac-b'>"
                    + types[0].trim() + "</div>"
                    + "<div class='aln-lft aln-top ac-name ac-b'>" + types[1]
                    + "</div>"
                    + "<div class='aln-lft aln-top ac-identifier ac-b'>"
                    + types[2] + "</div>"
                    + "<div class='aln-lft aln-top ac-type'>"
                    + types[3].trim() + "</div>" + "</div>";

            var term = "";
            if (this.term.indexOf(':') !== -1) {
                var terms = this.term.split(':');
                term = terms[1];
            }
            item.value = types[1];
            newLabel = newLabel.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("
                    + $.ui.autocomplete.escapeRegex(term)
                    + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
            return $('<li></li>').data('item.autocomplete', item).append(
                    newLabel).appendTo(ul);
        };
    }
    ;

})();