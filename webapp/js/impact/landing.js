"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.impact = window.treasury.impact || {};
    window.treasury.impact.landing = {
        initialize : _initialize,
        loadSummaryView : _loadSummaryView,
        loadDetailView : _loadDetailView,
        showLoading : _showLoading,
        hideLoading : _hideLoading,
        getDrillDownParameters : _getDrillDownParameter,
        autoCompleteSecuritySearch : _autoCompleteSecuritySearch
    };

    var _impactDetailFilterGroup;
    // Default search should be performed only once i.e on load
    var _defaultDetailSearchPerformed = false;
    var _summarySearchPerformed = false;

    var _impactSummaryFilterGroup;
    var _drillDownParameters;

    // Loading component is singleton. There are cases where we perform
    // multiple searches and end up calling showLoading multiple times.
    // Thereafter if we call hideLoading at the end of first search, 
    // it hides the loading while the subsequent searches are still in 
    // progress. Hence maintaining the count to apply the check before hiding.
    var _showLoadingCount = 0;

    function _getDrillDownParameter() {
        return _drillDownParameters;
    }

    function _showLoading() {
        _showLoadingCount ++;
        document.getElementById("impactGridLoading").show();
    }

    function _hideLoading() {
        _showLoadingCount --;
        if (_showLoadingCount === 0) {
            document.getElementById("impactGridLoading").hide();
        }
    }

    function _autoCompleteSecuritySearch(event) {
        var searchString = event.target.value;
        searchString = searchString
                .substring(searchString.lastIndexOf(',') + 1);
        window.treasury.filter.SecuritySearch(searchString, "pnlSpns");
    }

    function _loadSummaryViewFilters() {
        document.getElementById("impactDetailFilters").style.display = "none";
        document.getElementById("impactSummaryFilters").style.display = "block";

        document.getElementById("impactFilterSidebar").header = "Impact Summary Filters";
    }

    function _loadDetailViewFilters() {
        document.getElementById("impactSummaryFilters").style.display = "none";
        document.getElementById("impactDetailFilters").style.display = "block";

        document.getElementById("impactFilterSidebar").header = "Impact Detail Filters";
    }

    function _loadDetailView() {

        document.getElementById("showImpactSummary").classList.remove("active");
        document.getElementById("showImpactDetail").classList.add("active");

        // step 1 : Hide Summary view and load Detail view
        var impactSummaryView = document.getElementById("impactSummaryView");
        var impactDetailView = document.getElementById("impactDetailView");

        impactSummaryView.style.display = "none";
        impactSummaryView.setAttribute("size", "0");
        impactDetailView.style.display = "block";
        impactDetailView.setAttribute("size", "1");

        // step 2 : Load Detail view filters
        _loadDetailViewFilters();

        if (!_defaultDetailSearchPerformed) {
            // step 3 : Perform default search
            treasury.loadfilter.defaultDates().done(function(defaultDatesData) {
                var defaultParameters = {
                    "startDateString" : defaultDatesData.tMinusOneFilterDate,
                    "endDateString" : defaultDatesData.tMinusOneFilterDate,
                    "reportingCurrencyId" : 1760000
                };

                treasury.impact.impactGridActions
                        .loadPositionImpactDetailGrid(defaultParameters);

                _defaultDetailSearchPerformed = true;
            });
        }
    }

    function _initializeFilters(callback) {

        var start = 2013;
        var end = new Date().getFullYear();
        var yearList = [];
        for (var year = start; year <= end; year++) {
            yearList.push([year, year]);
        }

        var selectedYear = end;
        // Populate year select box with last 10 years
        if (treasury.defaults.date) {
            selectedYear = Date.parse(treasury.defaults.date).getFullYear();
        }

        // Init year filter
        var yearFilter = new window.treasury.filter.SingleSelect("year",
                "yearFilter", yearList, [ selectedYear ]);

        // Initializing Date Range filter
        var options = {
            datepickerOptions : {}
        };
        options.icon = $("#calendarIconId");

        // Following is a hack as setupDaterangepicker is from dportal library
        // and expects dportal.app is defined.
        dportal = dportal || {};
        dportal.app = dportal.app || {};

        setupDaterangepicker({
            input : $("#dateRangePicker"),
            errorDiv : $("#dateRangePickerError"),
            options : options
        });

         $.when(treasury.loadfilter.legalEntities(),
               treasury.loadfilter.cpeFamilies(),
               treasury.loadfilter.defaultDates(),
               treasury.loadfilter.negotiationTypes(),
               treasury.loadfilter.currencies())
        .done(function(legalEntitiesData, cpeFamiliesData, defaultDatesData, negotiationTypes, currenciesData) {
            // Init Legal Entity Filter for Summary view
            var legalEntitySummaryFilter = new window.treasury.filter.MultiSelect(
                    "legalEntityIds", "legalEntityImpactSummaryFilter",
                    legalEntitiesData[0].descoEntities, []);

            // Init Cpe Family Filter for Summary view
            var cpeFamilySummaryFilter = new window.treasury.filter.MultiSelect(
                    "cpeFamilyIds", "cpeImpactSummaryFilter",
                    cpeFamiliesData[0].counterPartyEntityFamilies, []);

            var reportingCurrencySummaryFilter = new window.treasury.filter.SingleSelect(
                    "reportingCurrencyId", "reportingCurrencySummaryFilter",
                    currenciesData[0].currency, 1760000);

            _impactSummaryFilterGroup = new window.treasury.filter.FilterGroup([
                    legalEntitySummaryFilter, cpeFamilySummaryFilter],
                    [ yearFilter, reportingCurrencySummaryFilter ]);

            // Init Legal Entity Filter for Detail view
            var legalEntityDetailFilter = new window.treasury.filter.MultiSelect(
                    "legalEntityIds", "legalEntityImpactDetailFilter",
                    legalEntitiesData[0].descoEntities, []);

            // Init Cpe Family Filter for Detail view
            var cpeFamilyDetailFilter = new window.treasury.filter.MultiSelect(
                    "cpeFamilyIds", "cpeImpactDetailFilter",
                    cpeFamiliesData[0].counterPartyEntityFamilies, []);

            // Init Negotiation Type Filter for Detail view
            var negotiationTypeFilter = new window.treasury.filter.MultiSelect(
                    "negotiationTypeIds", "negotiationTypeFilter",
                    negotiationTypes[0].negotiationTypes, []);
      
            var reportingCurrencyFilter = new window.treasury.filter.SingleSelect(
                    "reportingCurrencyId", "reportingCurrencyImpactDetailFilter",
                    currenciesData[0].currency, 1760000);

            var startDate = defaultDatesData[0].tMinusOneFilterDate;
            var endDate = defaultDatesData[0].tMinusOneFilterDate;

            $("#dateRangePicker").daterangepicker("setDate", startDate, endDate);

            // Create Filter Group
            _impactDetailFilterGroup = new treasury.filter.FilterGroup([
                    legalEntityDetailFilter, cpeFamilyDetailFilter, negotiationTypeFilter],
                    [reportingCurrencyFilter ], null, $("#dateRangePicker"));

            if (callback) {
                callback();
            }
        });
    }

    function _loadSummaryView() {

        document.getElementById("showImpactDetail").classList.remove("active");
        document.getElementById("showImpactSummary").classList.add("active");

        // step 2 : Hide Detail view and load Summary view
        var impactSummaryView = document.getElementById("impactSummaryView");
        var impactDetailView = document.getElementById("impactDetailView");

        impactDetailView.style.display = "none";
        impactDetailView.setAttribute("size", "0");
        impactSummaryView.style.display = "block";
        impactSummaryView.setAttribute("size", "1");

        // step 3 : Load Summary view filters if not already loaded
        _loadSummaryViewFilters();

        // step 4 : Perform default search
        if (!_summarySearchPerformed) {
            _loadSummaryGrids();
            _summarySearchPerformed = true;
        }

    }

    function _loadSummaryGrids() {

        var parameters = _impactSummaryFilterGroup.getSerializedParameterMap();
        parameters.cpeFamilyId = -1;
        parameters.legalEntityId = -1;
        _drillDownParameters = _impactSummaryFilterGroup.getParameterMap();
        treasury.impact.impactGridActions.loadYTDImpactGrid(parameters);
        treasury.impact.impactGridActions.loadMonthlyImpactGrid(parameters);

        // reset the data in daily impact report
        treasury.impact.impactGridActions.resetDailyImpactGrid();
    }

    function _registerHandlers() {
        // Switch view handler based on which we would invoke one of
        // _loadSummaryView, _loadDetailView
        document.getElementById("showImpactSummary").onclick = _loadSummaryView;

        document.getElementById("showImpactDetail").onclick = function() {
            _loadDetailView(true);
        };

        // Search handlers
        document.getElementById("searchImpactDetailId").onclick = function() {
            var isValid = treasury.impact.validator.validate();

            if (isValid) {
                var parameters = _impactDetailFilterGroup.getParameterMap();
                parameters.pnlSpns = document.getElementById("pnlSpns").value;
                treasury.impact.impactGridActions
                        .loadPositionImpactDetailGrid(parameters);
            }
        };

        document.getElementById("searchImpactSummaryId").onclick = _loadSummaryGrids;

        document.addEventListener("byYearSummaryEvent", function(event) {
            treasury.impact.impactGridActions
                    .loadMonthlyImpactGrid(event.detail);
        }, false);

        document.addEventListener("byMonthSummaryEvent",
                function(event) {
                    treasury.impact.impactGridActions
                            .loadDailyImpactGrid(event.detail);
                }, false);

        // event listener for security search
        document.getElementById("pnlSpns").oninput = _autoCompleteSecuritySearch;

        document.addEventListener("byYearAndMonthSummaryEvent",
                function(event) {
                    // Wrapping it in setTimeout so that the current UI thread
                    // could first highlight the selected row in daily data
                    // grid.
                    setTimeout(function() {
                        _defaultDetailSearchPerformed = true;
                        _loadDetailView();
                        _impactDetailFilterGroup.setParameterMap(event.detail);
                        treasury.impact.impactGridActions
                                .loadPositionImpactDetailGrid(event.detail);
                    }, 0);
                }, false);

    }

    // _initialize is called on page load.
    function _initialize(showDetail) {

        // To be able to show warnings / errors.
        ArcMessageHelper.initialize();

        // Register event handlers.
        _registerHandlers();

        // Initialize saved setting component
        treasury.impact.saveGridSettings.initialize();

        // Retrieve filters from server and initialize components
        // Callback will have action to be taken after initializing the filters
        _initializeFilters(function() {
            // Initialize Impact Detail report filter validator
            treasury.impact.validator.initialize();

            // By default, load summary view
            if (showDetail) {
                _loadDetailView();
            } else {
                _loadSummaryView();
            }

            // All elements are hidden initially. Once webcomponents are ready
            // show the view.
            document.getElementById("landingPageDiv").style.display = "block";
            document.getElementById("landingPageDiv").setLayout();
        });
    }

})();
