/*jshint jquery:true*/
"use strict";

(function() {

window.treasury = window.treasury || {};
window.treasury.impact = window.treasury.impact || {};
window.treasury.impact.validator = {
    initialize : _initialize,
    validate : _validate
};

var dateRangePickerId = "dateRangePicker";
var securitySearchId = "pnlSpns";
var negotiationTypeFilterElementId = "negotiationTypeFilter";
var errorDivId = "impactFilterValidation";
var dateRangeErrorDiv = "dateRangePickerError";

function _initialize() {
    var dateRangePicker = document.getElementById(dateRangePickerId);
    var securitySearch = document.getElementById(securitySearchId);
    var negotiationTypeFilter = document.getElementById(negotiationTypeFilterElementId);

    dateRangePicker.onchange = function() {
        _resetValidation();
        _dateRangePickerUpdate();
    };

    securitySearch.onchange = function() {
        _resetValidation();
        _dateRangePickerUpdate();
    };
    
    negotiationTypeFilter.onchange = function() {
        _resetValidation();
        _dateRangePickerUpdate();
    };
    
    // Reset the max end date when clicked on Specific Date
    document.querySelector(".ui-daterangepicker-specificDate").onclick = _resetMaxEndDate;
    document.querySelector("#calendarIconId").onclick = _resetMaxEndDate;
}

function _resetMaxEndDate() {
    $("#" + dateRangePickerId).daterangepicker("setMaxDate", Date.today());
}

/*
 * Based on the start date chosen, this updates the maxEndDate in the daterangepicker
 * For Rate Negotiation, YTD data give 2M+ records
 * If PNL SPN is set for Rate Negotiation, then allow 365 days search else restrict it to 30 days search.
 */
function _dateRangePickerUpdate() {
    var maxEndDate = _getMaxEndDate();
    // maxEndDate will be null if the input text of date was changed rendering it invalid.
    if (maxEndDate) {
        var dateRangePicker = $("#" + dateRangePickerId);
        dateRangePicker.daterangepicker("setMaxDate", maxEndDate);
    }
}

function _getMaxEndDate() {
    var dateRangePicker = $("#" + dateRangePickerId);
    var securitySearch = document.getElementById(securitySearchId);

    var dates = dateRangePicker.daterangepicker("getDate");
    var startDate = dates[0];

    var offset = 365;

    if (_isRateNegotiationSelected()) {
        
        offset = 30;
        
        //there is additional filter on security
        if(securitySearch.value && _validatePnlSpns(securitySearch.value))
        {
            offset = 365;
        }
    }

    var maxEndDate = Date.parse(startDate);
    // maxEndDate will be null if the input text of date was changed rendering it invalid.
    if (maxEndDate) {
        maxEndDate.setDate(maxEndDate.getDate() + offset);
    }
    return maxEndDate;
}

// Clear the errorDiv
function _resetValidation() {
    document.getElementById(errorDivId).style.display = "none";
    document.getElementById(errorDivId).innerHTML = "";
}

// Validate security
// Validate dateRange etc.
// show error and disable search
function _validate() {
    _resetValidation();

    var errorMessageArr = [];
    if (!_validateDateRange()) {
        errorMessageArr.push("Date Range");
    }
    if (!_validatePnlSpns(document.getElementById(securitySearchId).value)) {
        errorMessageArr.push("Security filter");
    }

    if (errorMessageArr.length) {
        document.getElementById(errorDivId).style.display = "block";
        document.getElementById(errorDivId).innerHTML = 
            (errorMessageArr.join(", ") + 
            (errorMessageArr.length > 1 ? " are " : " is ") +
            "invalid");
    }

    return errorMessageArr.length === 0;
}

function _validateDateRange() {
    var maxEndDate = _getMaxEndDate();

    var dates = $("#" + dateRangePickerId).daterangepicker("getDate");
    var endDate = dates[1];

    return (document.getElementById(dateRangeErrorDiv).textContent.trim() ? false : true) &&
        (Date.parse(endDate) <= maxEndDate);
}

function _validatePnlSpns(pnlSpns) {
    // undef/empty is fine
    if (!pnlSpns || !pnlSpns.trim()) {
        return true;
    }

    var pnlSpnArr = pnlSpns.split(",");
    var isValid = false;
    for (var i = 0; i < pnlSpnArr.length; i++) {
        isValid = (isValid || isInteger(pnlSpnArr[i].trim()));
    }
    return isValid;
}

function _isRateNegotiationSelected()
{
    var negotiationTypeFilter = document.getElementById(negotiationTypeFilterElementId);
        
    //All or Empty means its selected
    if (negotiationTypeFilter.value.selectedNodes.length == 0) {
        return true;
    }
    
    var rateNegotiationSelected = false;
    
    for (var i = 0; i < negotiationTypeFilter.value.selectedNodes.length; i++) {
        if(negotiationTypeFilter.value.selectedNodes[i].value == "BORROW_RATE")
        {
            rateNegotiationSelected = true;
        }
    }
    return rateNegotiationSelected;
}

})();
