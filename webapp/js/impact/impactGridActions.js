/*jshint jquery:true*/
"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.impact = window.treasury.impact || {};
    window.treasury.impact.impactGridActions = {
        loadYTDImpactGrid : _loadYTDImpactGrid,
        loadMonthlyImpactGrid : _loadMonthlyImpactGrid,
        loadDailyImpactGrid : _loadDailyImpactGrid,
        loadPositionImpactDetailGrid : _loadPositionImpactDetailGrid,
        extractImpactDetailGridColumns : _extractImpactDetailGridColumns,
        setImpactDetailGridColumns : _setImpactDetailGridColumns,
        resetDailyImpactGrid : _resetDailyImpactGrid
    };

    var _impactDetailGrid;

    function _loadYTDImpactGrid(parameters) {
        treasury.impact.landing.showLoading();
        var promise = $.ajax({
            url : "/treasury/seclend/search-impact-summary-data-year",
            data : parameters,
            type : "POST",
            dataType : "json"
        });

        promise
                .done(function(data) {
                    _clearGrid("ytdImpactSummaryGrid");
                    treasury.impact.landing.hideLoading();
                    if (data && data.resultList && data.resultList.length) {
                        new dportal.grid.createGrid($("#ytdImpactSummaryGrid"),
                                data.resultList,
                                treasury.impact.impactColumnsConfig.getByYearColumns(),
                                treasury.impact.gridoptions.ytdGridOptions());
                    } else {
                        _showGridInfo("ytdImpactSummaryGrid", "No YTD Impact summary found.");
                    }
                });
        promise
                .fail(function(xhr, text, errorThrown) {
                    _clearGrid("ytdImpactSummaryGrid");
                    treasury.impact.landing.hideLoading();

                    ArcMessageHelper.showMessage("WARNING", "Error occurred while loading YTD Impact summary.");
                });

    }

    function _loadMonthlyImpactGrid(parameters) {
        treasury.impact.landing.showLoading();
        var promise = $.ajax({
            url : "/treasury/seclend/search-impact-summary-data-month",
            data : parameters,
            type : "POST",
            dataType : "json"
        });

        promise.done(function(data) {
            _clearGrid("monthlyImpactSummaryGrid");
            _clearGrid("dailyImpactSummaryGrid");
            _showGridInfo("dailyImpactSummaryGrid", "Choose a month above to view its daily summary");
            treasury.impact.landing.hideLoading();
            if (data && data.resultList && data.resultList.length) {
                new dportal.grid.createGrid($("#monthlyImpactSummaryGrid"),
                        data.resultList,
                        treasury.impact.impactColumnsConfig.getByMonthColumns(),
                        treasury.impact.gridoptions.monthlyGridOptions());
                if(data.resultList[0]["reportingFxRate"]) {
                    _showReportingCurrencyMessage("impactSummary", true, data.resultList[0]["reportingCurrency"]);
                }
                else {
                    _showReportingCurrencyMessage("impactSummary", false);
                }
            } else {
                _showGridInfo("monthlyImpactSummaryGrid", "No Monthly Impact summary found.");
                _hideReportingCurrencyMessage("impactSummary");
            }
        });

        promise
                .fail(function(xhr, text, errorThrown) {
                    _clearGrid("monthlyImpactSummaryGrid");
                    _hideReportingCurrencyMessage("impactSummary");
                    treasury.impact.landing.hideLoading();

                    ArcMessageHelper.showMessage("WARNING", "Error occurred while loading Monthly Impact summary.");
                });
    }

    function _loadDailyImpactGrid(parameters) {
        treasury.impact.landing.showLoading();
        var promise = $.ajax({
            url : "/treasury/seclend/search-impact-summary-data-month-year",
            data : parameters,
            type : "POST",
            dataType : "json"
        });

        promise.done(function(data) {
            _clearGrid("dailyImpactSummaryGrid");
            treasury.impact.landing.hideLoading();
            if (data && data.resultList && data.resultList.length) {
                new dportal.grid.createGrid($("#dailyImpactSummaryGrid"),
                        data.resultList,
                        treasury.impact.impactColumnsConfig.getByYearAndMonthColumns(),
                        treasury.impact.gridoptions.dailyGridOptions());
            } else {
                _showGridInfo("dailyImpactSummaryGrid", "No Daily Impact summary found.");
            }
        });

        promise
                .fail(function(xhr, text, errorThrown) {
                    _clearGrid("dailyImpactSummaryGrid");
                    treasury.impact.landing.hideLoading();

                    ArcMessageHelper.showMessage("WARNING", "Error occurred while loading Daily Impact summary.");
                });
    }

    function _loadPositionImpactDetailGrid(parameters) {

        treasury.impact.landing.showLoading();

        var promise = $.ajax({
            url : "/treasury/seclend/search-position-impact-data",
            data : parameters,
            dataType : "json",
            type : "POST"
        });

        promise
                .done(function(data) {
                    _clearGrid("positionImpactDetailGrid");

                    treasury.impact.landing.hideLoading();

                    if (data && data.resultList && data.resultList.length) {
                        document.getElementById("positionImpactDetailGrid").style.display = "block";
                        _impactDetailGrid = new dportal.grid.createGrid(
                                $("#positionImpactDetailGrid"),
                                data.resultList,
                                treasury.impact.positionImpactColumnsConfig.getColumnConfig(parameters),
                                treasury.impact.gridoptions.positionImpactDetailGridOptions());

                        _moveSaveSettingsToGrid();

                        treasury.impact.saveGridSettings
                                .loadSettingsListAndUpdateGridColumns();
                        if(data.resultList[0]["reportingFxRate"]) {
                            _showReportingCurrencyMessage("impactDetail", true, data.resultList[0]["reportingCurrency"]);
                        }
                        else {
                            _showReportingCurrencyMessage("impactDetail", false);
                        }
                    } else {
                        _showGridInfo("positionImpactDetailGrid", "No Impact detail records obtained.");
                        _hideReportingCurrencyMessage("impactDetail");
                    }
                });
        promise
                .fail(function(xhr, text, errorThrown) {
                    _clearGrid("positionImpactDetailGrid");
                    _hideReportingCurrencyMessage("impactDetail");
                    treasury.impact.landing.hideLoading();

                    ArcMessageHelper.showMessage("WARNING", "Error occurred while loading Position Impact detail.");
                });

    }

    function _moveSaveSettingsToGrid() {
        // Wrapping within setTimeout to make it work in IE
        setTimeout(function() {
            var impactDetailHeader = document.querySelector("#cascadeFilterContainer");

            var saveSettingsSelect = document.querySelector("arc-save-settings");
            saveSettingsSelect.style.display = "inline-block";
            var saveSettingsButton = document.getElementById("saveGridView");
            saveSettingsButton.style.display = "inline-block";

            impactDetailHeader.appendChild(saveSettingsSelect);
            impactDetailHeader.appendChild(saveSettingsButton);

            // The following is to be executed after a few milliseconds or 
            // in chrome, the cascading container expands and then collapses
            setTimeout(function() {
                impactDetailHeader.style.display = "block";
            }, 100);
        }, 0);
    }

    function _moveSaveSettingsToFilter() {
        var saveSettingsSelect = document.querySelector("arc-save-settings");
        saveSettingsSelect.style.display = "none";
        var saveSettingsButton = document.getElementById("saveGridView");
        saveSettingsButton.style.display = "none";

        var impactDetailFilters = document.getElementById("impactDetailFilters");
        impactDetailFilters.appendChild(saveSettingsButton);
        impactDetailFilters.appendChild(saveSettingsSelect);
    }

    function _resetDailyImpactGrid() {
        _clearGrid("dailyImpactSummaryGrid");
        _showGridInfo("dailyImpactSummaryGrid", "Choose a month above to view its daily summary");
    }

    function _showGridInfo(gridId, message) {
        document.getElementById(gridId).style.display = "none";

        document.getElementById(gridId + "Info").style.display = "block";
        document.getElementById(gridId + "Info").innerHTML = message;
    }

    function _clearGrid(gridId) {
        _moveSaveSettingsToFilter();

        document.getElementById(gridId + "Info").innerHTML = "";
        document.getElementById(gridId + "Info").style.display = "none";

        document.getElementById(gridId).style.display = "block";
        document.getElementById(gridId).innerHTML = "";
        if (document.getElementById(gridId + "-header")) {
            document.getElementById(gridId + "-header").innerHTML = "";
        }
        if (document.querySelector("#cascadeFilterContainer")) {
            document.querySelector("#cascadeFilterContainer").style.display = "none";
        }
    }

    function _extractImpactDetailGridColumns() {
        var gridColumnIds = [];
        if (_impactDetailGrid) {
            var selectedColumns = _impactDetailGrid.grid.getColumns();
            for (var i = 0; i < selectedColumns.length; i++) {
                gridColumnIds.push(selectedColumns[i].id);
            }
        }
        return gridColumnIds;
    }

    function _setImpactDetailGridColumns(gridColumns) {
        treasury.impact.landing.showLoading();

        // Set timeout is added as the below invocation completes
        // instantaneously
        // So, loading doesn't appear to give an effect that columns have
        // changed.
        // Hence added half a second of timeout
        setTimeout(function() {
            _impactDetailGrid.setColumns(gridColumns);
            // HACK - gridObj.setColumns doesn't update the column chooser's
            // selected columns.
            // hence we have to call the below to update column chooser's
            // selected columns
			
			//HACK FIX: Below was causing js error in msb.
			//Once UIUX come up with final fix, will re-enable this.
            //setSelectedItems("positionImpactDetailGrid-columnselection", gridColumns);

            treasury.impact.landing.hideLoading();
        }, 300);
    }

    function _showReportingCurrencyMessage(viewPrefix, isFxRatePresent, cur) {
        var viewId = viewPrefix + "ReportingCurrencyMessage";
        var rcMessage = document.querySelector(`#${viewId}`);
        rcMessage.style.display = "block";
        if(isFxRatePresent) {
            rcMessage.className = "arc-message";
            rcMessage.innerHTML = `Reporting Currency (RC) corresponds to ${cur}`;
        }
        else {
            rcMessage.className = "arc-message--critical";
            rcMessage.innerHTML = "Fx Rate unavailable for selected Reporting Currency";
        }
    }

    function _hideReportingCurrencyMessage(viewPrefix) {
        var viewId = viewPrefix + "ReportingCurrencyMessage";
        var rcMessage = document.querySelector(`#${viewId}`);
        rcMessage.style.display = "none";
    }

})();
