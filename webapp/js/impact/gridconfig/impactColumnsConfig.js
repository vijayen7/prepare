"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.impact = window.treasury.impact || {};
    window.treasury.impact.impactColumnsConfig = {
        getByYearColumns : _getByYearColumns,
        getByMonthColumns : _getByMonthColumns,
        getByYearAndMonthColumns : _getByYearAndMonthColumns
    };

    function _getCommonColumns() {
        return [ {
            id : "rateNegotiations",
            type : "number",
            formatter : dpGrid.Formatters.Number,
            name : "Borrow Rate Impact (RC)",
            field : "rateNegotiationsRC",
            sortable : true,
            headerCssClass : "aln-rt b",
            aggregator : dpGrid.Aggregators.sum,
            excelFormatter : "#,##0"
        }, {
            id : "dividendNegotiations",
            type : "number",
            formatter : dpGrid.Formatters.Number,
            name : "Dividend Impact (RC)",
            field : "dividendNegotiationsRC",
            sortable : true,
            headerCssClass : "aln-rt b",
            aggregator : dpGrid.Aggregators.sum,
            excelFormatter : "#,##0"
        }, {
            id : "lendIncome",
            type : "number",
            formatter : dpGrid.Formatters.Number,
            name : "Lend Income Impact (RC)",
            field : "lendIncomeRC",
            sortable : true,
            headerCssClass : "aln-rt b",
            aggregator : dpGrid.Aggregators.sum,
            excelFormatter : "#,##0"
        }, {
            id : "excessBorrowReturns",
            type : "number",
            formatter : dpGrid.Formatters.Number,
            name : "Excess Borrow Returns Impact (RC)",
            field : "excessBorrowReturnsRC",
            sortable : true,
            headerCssClass : "aln-rt b",
            aggregator : dpGrid.Aggregators.sum,
            excelFormatter : "#,##0"
        }, {
            id : "totalImpact",
            type : "number",
            formatter : dpGrid.Formatters.Number,
            name : "Total Impact (RC)",
            field : "totalImpactRC",
            sortable : true,
            headerCssClass : "aln-rt b",
            aggregator : dpGrid.Aggregators.sum,
            excelFormatter : "#,##0"
        } ];
    }

    function _getByYearColumns() {
        var columns = _getCommonColumns();
        columns.unshift({
            id : "legalEntity",
            name : "Legal Entity",
            field : "org",
            type : "text",
            sortable : true,
            formatter : function(row, cell, value, columnDef, dataContext) {
                return treasury.formatters.drillThrough(row, cell,
                        value["value"], columnDef, dataContext, undefined,
                        undefined, JSON.stringify(value["param"]), 'byYear');
            },
            excelFormatter : "#,##0",
            minWidth : 200
        });
        return columns;
    }

    function _getByMonthColumns() {
        var columns = _getCommonColumns();
        columns.unshift({
            id : "month",
            name : "Month",
            field : "month",
            type : "text",
            sortable : true,
            formatter : function(row, cell, value, columnDef, dataContext) {
            	var formattedMonthName = treasury.formatters.monthToName(row, cell, value, columnDef, dataContext);
            	return "<a>" + formattedMonthName + "</a>";
            },
            excelFormatter : "#,##0"
        });
        return columns;
    }

    function _getByYearAndMonthColumns() {
        var columns = _getCommonColumns();
        columns.unshift({
            id : "date",
            name : "Date",
            field : "date",
            sortable : true,
            excelFormatter : "#,##0"
        });
        columns.push({
            id : "action",
            name : "Action",
            field : "date",
            exportToExcel : false,
            sortable : true,
            headerCssClass : "aln-rt b",
            formatter : function(row, cell, value, columnDef, dataContext) {
                return treasury.formatters.drillThrough(row, cell,
                        'view-detail', columnDef, dataContext, undefined,
                        undefined, value, 'detail');
            },
        });
        return columns;
    }

})();
