"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.impact = window.treasury.impact || {};
    window.treasury.impact.positionImpactColumnsConfig = {
        getColumnConfig: _getColumnConfig
    };

    function _getImpactDetailColumns(searchParameters) {
        var impactDetailColumns = [];
        var negotiationTypeIds = searchParameters.negotiationTypeIds;

        //From NegotiationType 
        //1, "BORROW_RATE"
        //2, "DIVIDEND"
        //3, "LEND_INCOME"
        //4, "EXCESS_BORROW_RETURNS"

        for (var i = 0; i < negotiationTypeIds.length; i++) {

            if (negotiationTypeIds[i] == "-1" || negotiationTypeIds[i] == "1") {
                impactDetailColumns.push({
                    id: "rateNegotiationClassification",
                    name: "Classification",
                    field: "rateNegotiationClassification",
                    toolTip: "Classification",
                    type: "text",
                    filter: true,
                    sortable: true,
                    headerCssClass: "b",
                    minWidth: 50
                },{
                    id: "rateNegotiationRate",
                    name: "Borrow Rate (bps)",
                    field: "rateNegotiationRate",
                    type: "number",
                    toolTip: "Borrow Rate (bps)",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Float,
                    headerCssClass: "aln-rt b",
                    minWidth: 50
                },{
                    id: "rateNegotiationMaxRate",
                    name: "Peak Borrow Rate (bps)",
                    field: "rateNegotiationMaxRate",
                    type: "number",
                    toolTip: "Peak Borrow Rate (bps) for Borrow Rate Impact",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Float,
                    headerCssClass: "aln-rt b",
                    minWidth: 50
                });
            }

            if (negotiationTypeIds[i] == "-1" || negotiationTypeIds[i] == "2") {
                impactDetailColumns.push({
                    id: "negotiatedQuantity",
                    name: "Negotiated Quantity (Dividend)",
                    field: "negotiatedQuantity",
                    toolTip: "Negotiated Quantity (Dividend)",
                    type: "number",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Number,
                    aggregator: dpGrid.Aggregators.sum,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0",
                    minWidth: 80
                },{
                    id: "dividendPerShareRC",
                    name: "Dividend Per Share (RC)",
                    field: "dividendPerShareRC",
                    toolTip: "Dividend Per Share (RC)",
                    type: "number",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Number,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0",
                    minWidth: 80
                },{
                    id: "expectedDividendRate",
                    name: "Expected Dividend Rate",
                    field: "expectedDividendRate",
                    type: "number",
                    toolTip: "Expected Dividend Rate",
                    filter: true,
                    sortable: true,
                    formatter: treasury.formatters.price,
                    headerCssClass: "aln-rt b",
                    minWidth: 50
                },{
                    id: "negotiatedDividendRate",
                    name: "Negotiated Dividend Rate",
                    field: "negotiatedDividendRate",
                    type: "number",
                    toolTip: "Negotiated Dividend Rate",
                    filter: true,
                    sortable: true,
                    formatter: treasury.formatters.price,
                    headerCssClass: "aln-rt b",
                    minWidth: 50
                },{
                    id: "dividendExDate",
                    name: "Ex Date",
                    field: "dividendExDate",
                    type: "text",
                    filter: true,
                    sortable: true,
                    headerCssClass: "b",
                    minWidth: 80
                },{
                    id: "dividendRecordDate",
                    name: "Record Date",
                    field: "dividendRecordDate",
                    type: "text",
                    filter: true,
                    sortable: true,
                    headerCssClass: "b",
                    minWidth: 80
                });
            }

            if (negotiationTypeIds[i] == "-1" || negotiationTypeIds[i] == "3") {
                impactDetailColumns.push({
                    id: "lendQuantity",
                    name: "Lend Quantity",
                    field: "lendQuantity",
                    toolTip: "Lend Quantity",
                    type: "number",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Number,
                    aggregator: dpGrid.Aggregators.sum,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0",
                    minWidth: 80
                },{
                    id: "lendMarketValueRC",
                    name: "Lend Market Value (RC)",
                    field: "lendMarketValueRC",
                    toolTip: "Lend Market Value (RC)",
                    type: "number",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Number,
                    aggregator: dpGrid.Aggregators.sum,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0",
                    minWidth: 80
                },{
                    id: "stockLoanFeeRate",
                    name: "Stock Loan Fee Rate (bps)",
                    field: "stockLoanFeeRate",
                    type: "number",
                    toolTip: "Stock Loan Fee Rate (bps)",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Float,
                    headerCssClass: "aln-rt b",
                    minWidth: 50
                });

            }

            if (negotiationTypeIds[i] == "-1" || negotiationTypeIds[i] == "4") {
                impactDetailColumns.push({
                    id: "excessBorrowQuantity",
                    name: "Returned Quantity (For Excess Borrow)",
                    field: "excessBorrowQuantity",
                    toolTip: "Returned Quantity (For Excess Borrow)",
                    type: "number",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Number,
                    aggregator: dpGrid.Aggregators.sum,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0",
                    minWidth: 80
                },{
                    id: "excessBorrowMarketValueRC",
                    name: "Market Value (RC) (Returned Quantity)",
                    field: "excessBorrowMarketValueUsd",
                    toolTip: "Market Value (RC) (Returned Quantity)",
                    type: "number",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Number,
                    aggregator: dpGrid.Aggregators.sum,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0",
                    minWidth: 80
                },{
                    id: "excessBorrowRate",
                    name: "Excess Borrow Rate (bps)",
                    field: "excessBorrowRate",
                    type: "number",
                    toolTip: "Excess Borrow Rate (bps)",
                    filter: true,
                    sortable: true,
                    formatter: dpGrid.Formatters.Float,
                    headerCssClass: "aln-rt b",
                    minWidth: 50
                });
            }
        }

        return impactDetailColumns;
    }

    function _getImpactColumns(searchParameters) {
        var impactColumns = [];
        var negotiationTypeIds = searchParameters.negotiationTypeIds;

        //From NegotiationType 
        //1, "BORROW_RATE"
        //2, "DIVIDEND"
        //3, "LEND_INCOME"
        //4, "EXCESS_BORROW_RETURNS"

        for (var i = 0; i < negotiationTypeIds.length; i++) {

            if (negotiationTypeIds[i] == "-1" || negotiationTypeIds[i] == "1") {
                impactColumns.push({
                    id: "rateNegotiations",
                    type: "number",
                    formatter: dpGrid.Formatters.Float,
                    aggregator: dpGrid.Aggregators.sum,
                    name: "Borrow Rate Impact (RC)",
                    field: "rateNegotiationImpactRC",
                    sortable: true,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0.0"
                });
            }

            if (negotiationTypeIds[i] == "-1" || negotiationTypeIds[i] == "2") {
                impactColumns.push({
                    id: "dividendNegotiations",
                    type: "number",
                    formatter: dpGrid.Formatters.Float,
                    aggregator: dpGrid.Aggregators.sum,
                    name: "Dividend Impact (RC)",
                    field: "dividendImpactRC",
                    sortable: true,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0.0"
                },{
                    id: "dividendNegotiationComment",
                    type: "text",
                    name: "Dividend Negotiations Comment",
                    field: "dividendNegotiationComment",
                    sortable: true,
                    headerCssClass: "b"
                });
            }

            if (negotiationTypeIds[i] == "-1" || negotiationTypeIds[i] == "3") {
                impactColumns.push({
                    id: "lendIncome",
                    type: "number",
                    formatter: dpGrid.Formatters.Float,
                    aggregator: dpGrid.Aggregators.sum,
                    name: "Lend Income Impact (RC)",
                    field: "lendImpactRC",
                    sortable: true,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0.0"
                });
            }

            if (negotiationTypeIds[i] == "-1" || negotiationTypeIds[i] == "4") {
                impactColumns.push({
                    id: "excessBorrowReturns",
                    type: "number",
                    formatter: dpGrid.Formatters.Float,
                    aggregator: dpGrid.Aggregators.sum,
                    name: "Excess Borrow Returns Impact (RC)",
                    field: "excessBorrowImpactRC",
                    sortable: true,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0.0"
                });
            }
        }

        impactColumns.push({
            id: "totalImpact",
            type: "number",
            formatter: dpGrid.Formatters.Float,
            aggregator: dpGrid.Aggregators.sum,
            name: "Total Impact (RC)",
            field: "totalImpactRC",
            sortable: true,
            headerCssClass: "aln-rt b",
            excelFormatter: "#,##0.0"
        });

        return impactColumns;
    }

    function _getAdditionalSecurityColumns() {

        return [
            {
                id: "cusip",
                name: "CUSIP",
                field: "cusip",
                type: "text",
                filter: true,
                sortable: true,
                headerCssClass: "b",
                minWidth: 60
            }, {
                id: "isin",
                name: "ISIN",
                field: "isin",
                type: "text",
                filter: true,
                sortable: true,
                headerCssClass: "b",
                minWidth: 60
            }, {
                id: "sedol",
                name: "SEDOL",
                field: "sedol",
                type: "text",
                filter: true,
                sortable: true,
                headerCssClass: "b",
                minWidth: 60
            }, {
                id: "ticker",
                name: "Ticker",
                field: "ticker",
                type: "text",
                filter: true,
                sortable: true,
                headerCssClass: "b",
                minWidth: 60
            }, {
                id: "ric",
                name: "RIC",
                field: "ric",
                type: "text",
                filter: true,
                sortable: true,
                headerCssClass: "b",
                minWidth: 60
            }, {
                id: "securityShortDesc",
                name: "Short Description",
                field: "securityShortDesc",
                type: "text",
                filter: true,
                sortable: true,
                headerCssClass: "b",
                minWidth: 60
            }
        ];
    }

    function _getColumnConfig(searchParameters) {
        var columns = [{
            id: "date",
            name: "Date",
            field: "date",
            type: "text",
            filter: true,
            sortable: true,
            headerCssClass: "b",
            minWidth: 80
        }, {
            id: "spn",
            name: "SPN",
            field: "spn",
            toolTip: "SPN",
            type: "number",
            filter: true,
            sortable: true,
            headerCssClass: "b",
            minWidth: 60
        }, {
            id: "pnlSpn",
            name: "PNL SPN",
            field: "pnlSpn",
            toolTip: "PNL SPN",
            type: "number",
            filter: true,
            sortable: true,
            headerCssClass: "b",
            minWidth: 60
        }, {
            id: "securityDescription",
            name: "Security Name",
            field: "securityDescription",
            toolTip: "Security Name",
            type: "text",
            filter: true,
            sortable: true,
            headerCssClass: "b",
            minWidth: 120
        }, {
            id: "cpeFamily",
            name: "Counteparty Family",
            field: "cpeFamily",
            toolTip: "Counteparty Family",
            type: "text",
            filter: true,
            sortable: true,
            headerCssClass: "b",
            minWidth: 120
        }, {
            id: "legalEntity",
            name: "Legal Entity",
            field: "legalEntity",
            toolTip: "Legal Entity",
            type: "text",
            filter: true,
            sortable: true,
            headerCssClass: "b",
            minWidth: 120
        }, {
            id: "custodianAccount",
            name: "Custodian Account",
            field: "custodianAccount",
            toolTip: "Custodian Account",
            type: "text",
            filter: true,
            sortable: true,
            headerCssClass: "b",
            minWidth: 110
        }, {
            id: "price",
            name: "Price (RC)",
            field: "priceRC",
            type: "number",
            toolTip: "Price (RC)",
            filter: true,
            sortable: true,
            formatter: treasury.formatters.price,
            headerCssClass: "aln-rt b",
            minWidth: 60
        }, {
            id: "quantity",
            name: "Total Quantity",
            field: "quantity",
            toolTip: "Total Quantity",
            type: "number",
            filter: true,
            sortable: true,
            formatter: dpGrid.Formatters.Number,
            aggregator: dpGrid.Aggregators.sum,
            headerCssClass: "aln-rt b",
            excelFormatter: "#,##0",
            minWidth: 80
        }];

        //incase no negotiation type is passed, set it to -1
        if(searchParameters.negotiationTypeIds == undefined){
            searchParameters.negotiationTypeIds = [-1];
        }
        
        columns = columns.concat(_getImpactColumns(searchParameters));

        columns = columns.concat(_getImpactDetailColumns(searchParameters));

        columns = columns.concat(_getAdditionalSecurityColumns());

        return columns;
    }
})();