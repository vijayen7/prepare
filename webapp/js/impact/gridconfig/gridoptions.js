/*jshint jquery:true*/
"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.impact = window.treasury.impact || {};
    window.treasury.impact.gridoptions = {
        ytdGridOptions : _getYtdGridOptions,
        monthlyGridOptions : _getMonthlyGridOptions,
        dailyGridOptions : _getDailyGridOptions,
        positionImpactDetailGridOptions : _getPositionImpactDetailGridOptions
    };

    function _getYtdGridOptions() {
        return {
            nestedTable : true,
            nestedField : window.treasury.impact.impactColumnsConfig
                    .getByYearColumns()[0].field,
            expandCollapseAll : true,
            expandTillLevel : -1,
            forceFitColumns : true,
            applyFilteringOnGrid : true,
            showHeaderRow : true,
            summaryRow : true,
            highlightRowOnClick : true,
            displaySummaryRow : true,
            summaryRowText : "Summary",
            sortList : [ {
                columnId : "totalImpact",
                sortAsc : false
            } ],
            onRowClick : function() {
                // highlight on row works if this callback is defined
            },
            onCellClick : function(args, isNotToggle) {
            	if(args.colId == "legalEntity" && args.row != null) {
            		var item = args.item;
            		var parameterMap = treasury.impact.landing
                    .getDrillDownParameters();
            		parameterMap.cpeFamilyId = item['org']['param']['counterPartyFamilyId'];
            		parameterMap.legalEntityId = item['org']['param']['legalEntityId'];
            		var byYearEvent = new CustomEvent(
                          "byYearSummaryEvent", {
                             'detail' : parameterMap
                         });
                  document.dispatchEvent(byYearEvent);
            	}
            },
            exportToExcel : true
        };
    }

    function _getMonthlyGridOptions() {
        return {
            forceFitColumns : true,
            applyFilteringOnGrid : true,
            showHeaderRow : true,
            summaryRow : true,
            displaySummaryRow : true,
            highlightRowOnClick : true,
            summaryRowText : "Summary",
            onCellClick : function(args, isNotToggle) {
            	if(args.colId == "month" && args.row != null) {
            		var item = args.item;
            		var colId = args.colId.toString();
            		var parameterMap = treasury.impact.landing
                    .getDrillDownParameters();
            		parameterMap.month = item[colId];
            		if (!('cpeFamilyId' in parameterMap)) {
            			parameterMap.cpeFamilyId = -1;
            		}
            		if (!('legalEntityId' in parameterMap)) {
            			parameterMap.legalEntityId = -1;
            		}
            		var byMonthEvent = new CustomEvent(
                            "byMonthSummaryEvent", {
                                'detail' : parameterMap
                            });
                    document.dispatchEvent(byMonthEvent);
            	}
            },
            exportToExcel : true
        };
    }

    function _getDailyGridOptions() {
        return {
            forceFitColumns : true,
            applyFilteringOnGrid : true,
            showHeaderRow : true,
            summaryRow : true,
            displaySummaryRow : true,
            highlightRowOnClick : true,
            summaryRowText : "Summary",
            onRowClick : function() {
                // highlight on row works if this callback is defined
            },
            onCellClick : function(args, isNotToggle) {
            	if(args.colId == "action" && args.row != null) {
            		var date = args.item.date;
                    var parameterMap = treasury.impact.landing
                            .getDrillDownParameters();
                    parameterMap.startDateString = date;
                    parameterMap.endDateString = date;
                    var byYearAndMonthEvent = new CustomEvent(
                            "byYearAndMonthSummaryEvent", {
                                'detail' : parameterMap
                            });
                  document.dispatchEvent(byYearAndMonthEvent);
            	}
            },
            exportToExcel : true
        };
    }

    function _getPositionImpactDetailGridOptions() {
        return {
            // maxHeight: 600,
            enableCellNavigation : true,
            exportToExcel : true,
            autoHorizontalScrollBar : true,
            useAvailableScreenSpace : true,
            customColumnSelection : true,
            displaySummaryRow : true,
            summaryRowText : "Total",
            sortList : [ {
                columnId : "date",
                sortAsc : true
            }, {
                columnId : "securityDescription",
                sortAsc : true
            }, {
                columnId : "cpeFamily",
                sortAsc : true
            }, {
                columnId : "legalEntity",
                sortAsc : true
            } ],
            // applyFilteringOnGrid : true
            cascadingFilter : {
                containerId : "cascadeFilterContainer",
                filterColumns : ["date", "cpeFamily", "legalEntity", "custodianAccount", "securityDescription", "spn", "pnlSpn"]
            }
        };
    }

})();
