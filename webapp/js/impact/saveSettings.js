"use strict";

(function(){
    window.treasury = window.treasury || {};
    window.treasury.impact = window.treasury.impact || {};
    window.treasury.impact.saveGridSettings = {
        initialize : _initialize,
        loadSettingsListAndUpdateGridColumns : _loadSettingsListAndUpdateGridColumns
    };

    function _initialize() {
    	treasury.impact.landing.showLoading();
        var saveSettings = document.querySelector('arc-save-settings');

        var saveSettingsHost = treasury.defaults.saveSettingsHost || "";

        saveSettings.serviceURL = saveSettingsHost + "/service/SettingsService";
        saveSettings.applicationId = 3;
        saveSettings.applicationCategory = 'ImpactDetailReport';

        saveSettings.saveSettingsCallback = function(){
            // Extract the settingsObj
            var gridColumns = treasury.impact.impactGridActions.extractImpactDetailGridColumns();

            return gridColumns;
        };

        saveSettings.applySettingsCallback = function(gridColumns){
            treasury.impact.impactGridActions.setImpactDetailGridColumns(gridColumns);
        };

        document.getElementById('saveGridView').onclick = function(){
            saveSettings.openSaveDialog();
        };
        treasury.impact.landing.hideLoading();
    }

    function _loadSettingsListAndUpdateGridColumns() {
        // To set the user setting list
        // this internally calls setDefaultSetting which calls applySavedSettings
        // leading to grid with default columns set by the user
        document.querySelector('arc-save-settings').retrieveSettings();
    }

})();