"use strict";

var lureSearchFilterGroup;
var legalEntityFamiliesLureFilter;
var businessUnitsLureFilter;
var ownershipEntitiesLureFilter;
var usageSummaryBySelect;
var usageSummaryByData;
var defaultDates;
(function() {
  window.treasury = window.treasury || {};
  window.treasury.lure = window.treasury.lure || {};
  window.treasury.lure.landing = {
    loadLureLandingView: _loadLureLandingViewFilters,
    loadSearchResults: _loadSearchResults,
    resetSearch: _resetSearchFilters
  };

  window.addEventListener("WebComponentsReady", _initializeLure);

  function _initializeLure() {
    $(document).on({
      ajaxStart: function() {
        treasury.lure.util.showLoading();
      },
      ajaxStop: function() {
        treasury.lure.util.hideLoading();
      }
    });

    _registerHandlers();
    treasury.lure.landing.loadLureLandingView();
  }

  function _registerHandlers() {
    document.getElementById('usageSummarySearch').onclick = treasury.lure.landing.loadSearchResults;
    document.getElementById('resetSearch').onclick = treasury.lure.landing.resetSearch;
    document.getElementById('usageSummaryFilterSidebar').addEventListener("stateChange", resizeAllCanvas);
    document.getElementById('copySearchURL').addEventListener('click', copySearchURL);
  }

  function resizeAllCanvas() {
    if (usageSummaryGrid != null) {
      usageSummaryGrid.resizeCanvas();
    }
  }

  function _resetSearchFilters() {
    document.getElementById('usageSummaryBySelect').value = {
      'key': usageSummaryByData[0][0],
      'value': usageSummaryByData[0][1]
    };
    document.getElementById('ownershipEntityFilter').value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById('legalEntityFamilyFilter').value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById('businessUnitFilter').value = {
      status: 'none',
      selectedNodes: []
    };

    $('#datePicker').datepicker('setDate', defaultDates.tMinusOneFilterDate);
    document.getElementById('showAllBuffers').checked = false;
    document.getElementById('showInMillions').checked = true;
  }

  function _loadLureLandingViewFilters() {
    $.when(treasury.loadfilter.loadEntityFamilies(),
      treasury.loadfilter.loadBusinessUnits(),
      treasury.loadfilter.loadOwnershipEntitiesFromFuma(),
      treasury.loadfilter.defaultDates()).done(
      function(legalEntityFamiliesData, businessUnitsData, ownershipEntitiesData,
        defaultDatesData) {

        legalEntityFamiliesLureFilter = new window.treasury.filter.MultiSelect(
          'legalEntityFamilyFilterIds', 'legalEntityFamilyFilter',
          legalEntityFamiliesData[0].descoEntityFamilies, []);

        businessUnitsLureFilter = new window.treasury.filter.MultiSelect(
          'businessUnitFilterIds', 'businessUnitFilter',
          businessUnitsData[0].businessUnits, []);

        ownershipEntitiesLureFilter = new window.treasury.filter.MultiSelect(
          'ownershipEntityFilterIds', 'ownershipEntityFilter',
          ownershipEntitiesData[0].ownershipEntities, []);

        usageSummaryByData = [
          [1, 'Entity Family By BU'],
          [2, 'Entity Family'],
          [3, 'Ownership Entity By BU']
        ];

        usageSummaryBySelect = new window.treasury.filter.SingleSelect(
          'usageSummaryFilterId', 'usageSummaryBySelect',
          usageSummaryByData, [1]);
        document.querySelector('#usageSummaryBySelect')
          .addEventListener('change', _usageSummaryBySelectOnChange);
        _usageSummaryBySelectOnChange();
        var dateFilterValue = treasury.defaults.date || defaultDatesData[0].tMinusOneFilterDate;
        defaultDates = defaultDatesData[0];
        var minDate = Date.parse('1970-01-01');
        var maxDate = Date.parse('2038-01-01');
        var dateFormat = 'yy-mm-dd';
        treasury.lure.util.setupDateFilter($("#datePicker"),
          $("#datePickerError"), dateFilterValue, minDate, maxDate, dateFormat);

        lureSearchFilterGroup = new treasury.filter.FilterGroup([legalEntityFamiliesLureFilter, businessUnitsLureFilter, ownershipEntitiesLureFilter], [usageSummaryBySelect], $("#datePicker"), null);
        _loadDataFromUrlParams();
      }
    );
  }

  function _usageSummaryBySelectOnChange() {
    var usageSummaryOption = document.getElementById("usageSummaryBySelect").value;
    if (usageSummaryOption != null) {
      if (usageSummaryOption.key == 1) { //Entity Family By BU
        document.getElementById('ownershipEntityDiv').style.display = 'none';
        document.getElementById('legalEntityFamilyDiv').style.display = 'block';
        document.getElementById('businessUnitDiv').style.display = 'block';
      } else if (usageSummaryOption.key == 2) { //Entity Family
        document.getElementById('ownershipEntityDiv').style.display = 'none';
        document.getElementById('legalEntityFamilyDiv').style.display = 'block';
        document.getElementById('businessUnitDiv').style.display = 'none';
      } else if (usageSummaryOption.key == 3) { //Ownership Entity By BU
        document.getElementById('ownershipEntityDiv').style.display = 'block';
        document.getElementById('legalEntityFamilyDiv').style.display = 'none';
        document.getElementById('businessUnitDiv').style.display = 'block';
      }
      document.getElementById('ownershipEntityFilter').value = {
        status: 'none',
        selectedNodes: []
      };
      document.getElementById('legalEntityFamilyFilter').value = {
        status: 'none',
        selectedNodes: []
      };
      document.getElementById('businessUnitFilter').value = {
        status: 'none',
        selectedNodes: []
      };
    }
  }

  function _loadSearchResults() {
    if (document.getElementById('datePickerError').innerHTML != '') {
      ArcMessageHelper.showMessage('error', 'Please select a valid date.', null, null);
      return;
    }
    if (new Date(document.getElementById('datePicker').value) == '') {
      ArcMessageHelper.showMessage('warning', 'Please select date.', null, null);
      return;
    }

    treasury.lure.util.clearGrid('usageSummaryGrid');
    var parameters = lureSearchFilterGroup.getSerializedParameterMap();
    if (document.getElementById('usageSummaryFilterSidebar').state == 'expanded') {
      document.getElementById('usageSummaryFilterSidebar').toggle();
    }
    parameters.showInMillionsOption = document.getElementById('showInMillions').checked;
    parameters.showAllBuffersOption = document.getElementById('showAllBuffers').checked;
    treasury.lure.usageSummaryActions.loadUsageSummaryReport(parameters);
  }

  function _loadDataFromUrlParams() {
    if(window.location.href.indexOf("isCopyUrl=true") >= 0) {
      var paramString =  window.location.href.split("?")[1];
      var paramList = [], parameters = {};

      if(paramString !== undefined) {
        paramList = paramString.split("&");
      }
      
      for(var i=0; i<paramList.length; i++) {
        var param = paramList[i].split("=");
        if(param[0] !== "isCopyUrl") {
          parameters[param[0]] = param[1];
        }
      }
      
      _setFilters(parameters);
      treasury.lure.landing.loadSearchResults();
    }
  }

  function copySearchURL() {
    var url = "/treasury/lure/usageSummaryReport";
    treasury.lure.util.showLoading();
    var parameters = lureSearchFilterGroup.getSerializedParameterMap();
    parameters.showInMillionsOption = document.getElementById('showInMillions').checked;
    parameters.showAllBuffersOption = document.getElementById('showAllBuffers').checked;
    parameters.isCopyUrl = true;
    generateReportURL(url, parameters, "resultList");
    treasury.lure.util.hideLoading();
  }

  function _setFilters(parameters) {
    var showInMillions = parameters.showInMillionsOption === 'true' ? true : false;
    var showAllBuffers = parameters.showAllBuffersOption === 'true' ? true : false;
    var legalEntityFamilies = convertCSVToArray(parameters.legalEntityFamilyFilterIds);
    var businessUnits = convertCSVToArray(parameters.businessUnitFilterIds);
    var ownershipEntities = convertCSVToArray(parameters.ownershipEntityFilterIds);

    usageSummaryBySelect.setSelectedNode(parameters.usageSummaryFilterId)

    document.getElementById('showInMillions').checked = showInMillions;
    document.getElementById('showAllBuffers').checked = showAllBuffers;


    legalEntityFamiliesLureFilter.setSelectedNodes(legalEntityFamilies);
    businessUnitsLureFilter.setSelectedNodes(businessUnits)
    ownershipEntitiesLureFilter.setSelectedNodes(ownershipEntities);

    $("#datePicker").val(parameters.dateString);
  }

  function convertCSVToArray(csvString) {
    return (csvString === "-1") ? [] : csvString.split(",")
  }
})();
