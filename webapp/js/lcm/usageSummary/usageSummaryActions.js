"use strict";

var usageSummaryGrid;


(function() {
  window.treasury = window.treasury || {};
  window.treasury.lure = window.treasury.lure || {};
  window.treasury.lure.usageSummaryActions = {
    loadUsageSummaryReport: _loadUsageSummaryReport,
    loadUsageSummaryGrid: _loadUsageSummaryGrid
  };


  function _loadUsageSummaryReport(parameters) {
    treasury.lure.util.showLoading();
    var serviceCall = $.ajax({
      url: "/treasury/lure/get-usage-summary-report",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        treasury.lure.usageSummaryActions.loadUsageSummaryGrid(data, parameters, false);
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      treasury.lure.util.clearGrid("usageSummaryGrid");
      treasury.lure.util.hideLoading();
      console.log("Error occurred while retrieving results: " + errorThrown);
    });
  }

  function _loadUsageSummaryGrid(data, parameters, forWorkflow) {
    treasury.lure.util.clearGrid("usageSummaryGrid");
    treasury.lure.util.hideLoading();
    if (data && data.resultList && data.resultList.length) {
      if(document.getElementById('lureMessage') != null) {
        document.getElementById('lureMessage').setAttribute("hidden", true);
      }
      document.getElementById('usageSummaryGrid').removeAttribute("hidden");
      var gridOptions = treasury.lure.usageSummaryGridOptions
        .getResultOptions();
      var columnConfigs = treasury.lure.usageSummaryColumnsConfig
        .getResultColumns(parameters.usageSummaryFilterId, parameters.showInMillionsOption);

      if (parameters.usageSummaryFilterId != 1) {
        gridOptions.enableMultilevelGrouping = false;
      }
      if(parameters.showInMillionsOption) {
        $('#usageSummaryGrid').next().removeAttr("hidden");
      } else {
        $('#usageSummaryGrid').next().attr("hidden", true);
      }
      usageSummaryGrid = new dportal.grid.createGrid(
        $("#usageSummaryGrid"), data.resultList,
        columnConfigs, gridOptions);
      treasury.lure.util.resizeCanvasOnGridChange(usageSummaryGrid,
       'usageSummaryGrid', 'usageSummaryFilterSidebar');
     } else if (data && data.errorMessage) {
      if(data.errorMessage.includes('UncheckedTimeoutException')){
        treasury.lure.util.showErrorMessage('Error', 'Timeout Error occurred while fetching MtdPnl data from Manr database. Please contact Archelp.', false);
      }
      else{
        treasury.lure.util.showErrorMessage('Error', data.errorMessage, false);
      }  
      console.log("Error in fetching Usage summary results results");
     } else if (data && (data.resultList == null || data.resultList.length == 0)) {
       document.getElementById('lureMessage').removeAttribute("hidden");
      document
        .getElementById('lureMessage').innerHTML = treasury.lure.util.getFormattedMessage(
              'No results found matching selected criteria.', '');
     }
  }
})();
