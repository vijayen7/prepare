/*jshint jquery:true*/
"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.lure = window.treasury.lure || {};
  window.treasury.lure.usageSummaryGridOptions = {
    getResultOptions: _getResultOptions
  };

  function _getResultOptions() {
    var groupColumns = getGroupColumns();
    return {
      enableMultilevelGrouping: {
        hideGroupingHeader: false,
        showGroupingKeyInColumn: false,
        initialGrouping: groupColumns,
      },
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRow: false,
      sortList: [{
        columnId: "entityFamily",
        sortAsc: true
      }],
      summaryRow: true,
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      useAvailableScreenSpace: true,
      exportToExcel: true,
      sheetName: "Usage Summary Report",
      
    };
  }

  function getGroupColumns() {
    var groupColumns = ['entityFamily'];
    return groupColumns;
  }
})();
