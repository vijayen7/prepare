"use strict";

var showInMillions;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.lure = window.treasury.lure || {};
  window.treasury.lure.usageSummaryColumnsConfig = {
    getResultColumns: _getResultColumns,
    getCommonColumns: _getCommonColumns,
    getEntityFamilyColumns: _getEntityFamilyColumns,
    getEntityFamilyByBUColumns: _getEntityFamilyByBUColumns,
    adjustmentFormatter : _adjustmentFormatter,
    getOwnershipEntityByBUColumns: _getOwnershipEntityByBUColumns
  };

  function _getResultColumns(usageSummaryBy, showInMillionsOption) {
    var i;
    showInMillions = showInMillionsOption;
    for(i in usageSummaryByData) {
      if(usageSummaryByData[i][0] == usageSummaryBy) {
        break;
      }
    }
    if(usageSummaryByData[i][1] == 'Entity Family By BU') {
      return _getEntityFamilyByBUColumns();
    } else if(usageSummaryByData[i][1] == 'Entity Family') {
      return _getEntityFamilyColumns();
    } else if(usageSummaryByData[i][1] == 'Ownership Entity By BU') {
      return _getOwnershipEntityByBUColumns();
    }
  }
  function _getCommonColumns() {
    return [{
      id: "entityFamily",
      type: "text",
      name: " Entity Family",
      field: "entity_family",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "businessUnit",
      type: "text",
      name: "Business Unit",
      field: "business_unit",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "capitalAllocation",
      type: "number",
      name: "Capital Allocation",
      field: "capital_allocation",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "totalBuffer",
      type: "number",
      name: "Total Buffer",
      field: "total_buffer",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "marginCapital",
      type: "number",
      name: "Margin Capital",
      field: "margin_capital",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "marginUsage",
      type: "number",
      name: "Margin Usage",
      field: "margin_usage",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "overUnderAllocation",
      type: "number",
      name: "Over/Under Allocation",
      field: "over_under_allocation",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "marginUsagePercentage",
      type: "number",
      name: "Margin Usage (%)",
      field: "margin_usage_percentage",
      formatter: treasury.formatters.percent,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "grossMonthToDatePnl",
      type: "number",
      name: "Gross Month-to-Date P&L",
      field: "gross_month_to_date_pnl",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "yesterdayMarginUsage",
      type: "number",
      name: "Yesterday's Margin Usage",
      field: "yesterday_margin_usage",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "dailyChange",
      type: "number",
      name: "Daily Change",
      field: "daily_change",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "previousWeekMarginUsage",
      type: "number",
      name: "Previous Week's Margin Usage",
      field: "previous_week_margin_usage",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }, {
      id: "weeklyChange",
      type: "number",
      name: "Weekly Change",
      field: "weekly_change",
      formatter: millionsFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelDataFormatter : function(value){return millionsExcelFormatter(value);}
    }];
  }

  function _getEntityFamilyByBUColumns() {

	   var commonColumns = treasury.lure.usageSummaryColumnsConfig.getCommonColumns();

	    var ownershipEntityColumn = _getOwnershipEntityColumn();
	    commonColumns.splice(2, 0, ownershipEntityColumn);

	    commonColumns.splice(4, 1);
	    commonColumns.splice(7, 1);
	    return commonColumns.splice(0, 8);
  }

  function _getEntityFamilyColumns() {
    var commonColumns = treasury.lure.usageSummaryColumnsConfig.getCommonColumns();
    commonColumns.splice(1, 1);
    return commonColumns;
  }

  function _getOwnershipEntityByBUColumns() {
    var commonColumns = treasury.lure.usageSummaryColumnsConfig.getCommonColumns();
    var ownershipEntityColumn = _getOwnershipEntityColumn();
    commonColumns.splice(0, 1, ownershipEntityColumn);
    return commonColumns;
  }

  function _getOwnershipEntityColumn() {
    var ownershipEntityColumn = {
      id: "ownershipEntity",
      type: "text",
      name: "Ownership Entity",
      field: "ownership_entity",
      sortable: true,
      headerCssClass: "aln-rt b"
    };
    return ownershipEntityColumn;
  }
  function summationGroupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
    var value = "";
    if (totals && totals.sum) {
        value = totals.sum[columnDef.field];
    }
    if(value == null || value == "" || isNaN(value) || value == undefined) {
      return value;
    } else {
      var valueToFormat = showInMillions ? Number(value/1000000).toFixed(2) : value;
      return isExportToExcel ? valueToFormat : dpGrid.Formatters.Float(null, null, valueToFormat, columnDef, null);
    }
  }

  function SummationAggregator(field) {
    this.field_ = field;

    this.init = function() {
      this.sum_ = null;
    };
    this.accumulate = function(item) {
      var val = item[this.field_];
      if (val != null && val !== "" && !isNaN(val) && val != undefined) {
        //this.sum_ += Math.round(val);
        this.sum_ += val;
      }
    };

    this.storeResult = function(groupTotals) {
      if (!groupTotals.sum) {
        groupTotals.sum = {};
      }
      groupTotals.sum[this.field_] = this.sum_;
    };
  }

  function _adjustmentFormatter(row, cell, value, columnDef, dataContext) {

    var adjustment = '<a href="#" title="Add Adjustment"   ><i class="icon-add"></i></a>';
    return adjustment;
  }


  function millionsFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value == "" || isNaN(value)) {
      return value;
    } else {
      var valueToFormat = showInMillions ? Number(value / 1000000).toFixed(2) : value;
      return dpGrid.Formatters.Float(row, cell, valueToFormat, columnDef, dataContext);
    }
  }

  function millionsExcelFormatter(value) {
	  if (value == null || value == "" || isNaN(value)) {
    	  return value;
      }
      else{
    	  return showInMillions ? Number(value / 1000000).toFixed(2): value ;
      }
      }
})();
