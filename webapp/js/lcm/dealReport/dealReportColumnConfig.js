"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.dealReport = window.treasury.dealReport || {};
    window.treasury.dealReport.dealReportColumnConfig = {
        getTopPanelColumns : _getTopPanelColumns,
        getRequirementExposureColumns : _getRequirementExposureColumns,
        getCollateralColumns : _getCollateralColumns
    };
    function _getTopPanelColumns() {
    	return [{
    	      id: "dealName",
    	      type: "text",
            formatter : _textFormatter,
    	      name: "Deal Name",
    	      field: "dealName",
    	      sortable: true,
    	      headerCssClass: "aln-rt b",
            width:60
    	    },{
                id : "gmv",
                type : "number",
                formatter : treasury.formatters.clickableAmount,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "GMV USD",
                field : "gmvUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "lmv",
                type : "number",
                formatter : treasury.formatters.clickableAmount,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "LMV USD",
                field : "lmvUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "smv",
                type : "number",
                formatter : treasury.formatters.clickableAmount,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "SMV USD",
                field : "smvUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "exposure",
                type : "number",
                formatter : treasury.formatters.clickableAmount,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "Exposure USD",
                field : "exposureUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "collateral",
                type : "number",
                formatter : treasury.formatters.clickableAmount,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "Collateral USD",
                field : "collateralUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "equity",
                type : "number",
                formatter : treasury.formatters.number,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "Equity USD",
                field : "equityUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "requirement",
                type : "number",
                formatter : treasury.formatters.clickableAmount,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "Requirement USD",
                field : "marginUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "marginPercentage",
                type : "number",
                formatter : treasury.formatters.percent,
                name : "Requirement %",
                field : "marginPercentage",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:80
            },{
                id : "excessDeficit",
                type : "number",
                formatter : treasury.formatters.number,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "Excess Deficit USD",
                field : "excessDeficitUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "tradeDateCash",
                type : "number",
                formatter : treasury.formatters.clickableAmount,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "Trade Date Cash USD",
                field : "tradeDateCashUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            },{
                id : "repoLoanValueUsd",
                type : "number",
                formatter : treasury.formatters.clickableAmount,
                aggregator : dpGrid.Aggregators.sum,
                groupingAggregator: SummationAggregator,
                groupTotalsFormatter: _groupFormatter,
                name : "Repo Loan Value USD",
                field : "repoLoanValueUsd",
                sortable : true,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0",
                width:90
            }];
    }
    function _getRequirementExposureColumns() {
      return [{
	      id: "pnlSpn",
	      type: "text",
        formatter : _textFormatter,
	      name: "PNL SPN",
	      field: "pnlSpn",
	      sortable: true,
	      headerCssClass: "aln-rt b",
        width:40
	    },{
  	      id: "ca",
	      type: "text",
        formatter : _nestedValueFormatter,
	      name: "ECA",
	      field: "exposureCustodianAccount",
	      sortable: true,
	      headerCssClass: "aln-rt b",
        width:80,
        excelDataFormatter : _nestedValueExcelFormatter,
        filter: {
            isFilterOnFormattedValue: true
          }
	    },{
  	      id: "legalEntity",
	      type: "text",
        formatter : _nestedValueFormatter,
	      name: "Legal Entity",
	      field: "legalEntity",
	      sortable: true,
	      headerCssClass: "aln-rt b",
        width:70,
        excelDataFormatter : _nestedValueExcelFormatter,
        filter: {
            isFilterOnFormattedValue: true
          }
	    },{
    	      id: "cpe",
    	      type: "text",
            formatter : _nestedValueFormatter,
    	      name: "CPE",
    	      field: "cpe",
    	      sortable: true,
    	      headerCssClass: "aln-rt b",
            width:50,
            excelDataFormatter : _nestedValueExcelFormatter,
            filter: {
                isFilterOnFormattedValue: true
              }
    	    },{
        	      id: "agreementType",
        	      type: "text",
                formatter : _nestedValueFormatter,
        	      name: "Agreement Type",
        	      field: "agreementType",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:60,
                excelDataFormatter : _nestedValueExcelFormatter,
                filter: {
                    isFilterOnFormattedValue: true
                  }
        	    },{
          	      id: "field",
        	      type: "text",
                formatter : _textFormatter,
        	      name: "Field",
        	      field: "field",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:30
        	    },{
          	      id: "gboType",
        	      type: "text",
                formatter : _textFormatter,
        	      name: "GBO Type",
        	      field: "gboType",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:40
        	    },{
          	      id: "book",
        	      type: "text",
                formatter : _textFormatter,
        	      name: "Book",
        	      field: "book",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:40
        	    },{
        	      id: "bundle",
        	      type: "text",
                formatter : _nestedValueFormatter,
        	      name: "Bundle",
        	      field: "bundle",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:40,
                excelDataFormatter : _nestedValueExcelFormatter,
                filter: {
                    isFilterOnFormattedValue: true
                  }
        	    },{
    	      id: "dealName",
    	      type: "text",
            formatter : _textFormatter,
    	      name: "Deal Name",
    	      field: "dealName",
    	      sortable: true,
    	      headerCssClass: "aln-rt b",
            width:60
    	    },{
        	      id: "currency",
        	      type: "text",
                formatter : _nestedValueCurrencyFormatter,
        	      name: "Currency",
        	      field: "currency",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:60,
                excelDataFormatter : _nestedValueCurrencyExcelFormatter,
                filter: {
                    isFilterOnFormattedValue: true
                  }
        	    },{
            id : "mv",
            type : "number",
            formatter : treasury.formatters.number,
            aggregator : dpGrid.Aggregators.sum,
            groupingAggregator: SummationAggregator,
            groupTotalsFormatter: _groupFormatter,
            name : "Market Value USD",
            field : "marketValueUsd",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0",
            width:90
        },{
            id: "marginCallType",
            type: "text",
            formatter : _textFormatter,
            name: "Margin Call Type",
            field: "marginCallType",
            sortable: true,
            headerCssClass: "aln-rt b",
            width:60
          },{
            id : "requirement",
            type : "number",
            formatter : treasury.formatters.number,
            aggregator : dpGrid.Aggregators.sum,
            groupingAggregator: SummationAggregator,
            groupTotalsFormatter: _groupFormatter,
            name : "Requirement USD",
            field : "marginUsd",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0",
            width:90
        },{
            id : "exposure",
            type : "number",
            formatter : treasury.formatters.number,
            aggregator : dpGrid.Aggregators.sum,
            groupingAggregator: SummationAggregator,
            groupTotalsFormatter: _groupFormatter,
            name : "Exposure USD",
            field : "exposureUsd",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0",
            width:90
        },{
            id : "repoLoanValueUsd",
            type : "number",
            formatter : treasury.formatters.number,
            aggregator : dpGrid.Aggregators.sum,
            groupingAggregator: SummationAggregator,
            groupTotalsFormatter: _groupFormatter,
            name : "Repo Loan Value USD",
            field : "repoLoanValueUsd",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0",
            width:90
        }];

    }
    function _getCollateralColumns() {
      return [{
  	      id: "ca",
	      type: "text",
        formatter : _nestedValueFormatter,
	      name: "ECA",
	      field: "exposureCustodianAccount",
	      sortable: true,
	      headerCssClass: "aln-rt b",
        width:80,
        excelDataFormatter : _nestedValueExcelFormatter,
        filter: {
            isFilterOnFormattedValue: true
          }
	    },{
  	      id: "legalEntity",
	      type: "text",
        formatter : _nestedValueFormatter,
	      name: "Legal Entity",
	      field: "legalEntity",
	      sortable: true,
	      headerCssClass: "aln-rt b",
        width:60,
        excelDataFormatter : _nestedValueExcelFormatter,
        filter: {
            isFilterOnFormattedValue: true
          }
	    },{
    	      id: "cpe",
    	      type: "text",
            formatter : _nestedValueFormatter,
    	      name: "CPE",
    	      field: "cpe",
    	      sortable: true,
    	      headerCssClass: "aln-rt b",
            width:60,
            excelDataFormatter : _nestedValueExcelFormatter,
            filter: {
                isFilterOnFormattedValue: true
              }
    	    },{
        	      id: "agreementType",
        	      type: "text",
                formatter : _nestedValueFormatter,
        	      name: "Agreement Type",
        	      field: "agreementType",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:60,
                excelDataFormatter : _nestedValueExcelFormatter,
                filter: {
                    isFilterOnFormattedValue: true
                  }
        	    },{
        	      id: "bundle",
        	      type: "text",
                formatter :_nestedValueFormatter,
        	      name: "Bundle",
        	      field: "bundle",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:60,
                excelDataFormatter : _nestedValueExcelFormatter,
                filter: {
                    isFilterOnFormattedValue: true
                  }
        	    },{
    	      id: "dealName",
    	      type: "text",
            formatter : _textFormatter,
    	      name: "Deal Name",
    	      field: "dealName",
    	      sortable: true,
    	      headerCssClass: "aln-rt b",
            width:60
    	    },{
        	      id: "currency",
        	      type: "text",
                formatter : _nestedValueCurrencyFormatter,
        	      name: "Currency",
        	      field: "currency",
        	      sortable: true,
        	      headerCssClass: "aln-rt b",
                width:60,
                excelDataFormatter : _nestedValueCurrencyExcelFormatter,
                filter: {
                    isFilterOnFormattedValue: true
                  }
        	    },{
                              id : "collateral",
                              type : "number",
                              formatter : treasury.formatters.number,
                              aggregator : dpGrid.Aggregators.sum,
                              groupingAggregator: SummationAggregator,
                              groupTotalsFormatter: _groupFormatter,
                              name : "Collateral USD",
                              field : "collateralUsd",
                              sortable : true,
                              headerCssClass : "aln-rt b",
                              excelFormatter : "#,##0",
                              width:90
                          },{
                              id : "tradeDateCash",
                              type : "number",
                              formatter : treasury.formatters.number,
                              aggregator : dpGrid.Aggregators.sum,
                              groupingAggregator: SummationAggregator,
                              groupTotalsFormatter: _groupFormatter,
                              name : "Trade Date Cash USD",
                              field : "tradeCashUsd",
                              sortable : true,
                              headerCssClass : "aln-rt b",
                              excelFormatter : "#,##0",
                              width:90
                          }
                        ];

    }

    function _nestedValueCurrencyExcelFormatter (value) {
          var returnValue = "";
          if (value != null && typeof value !== 'undefined' &&
            value.name !== null) {
            returnValue = value.abbrev;
          };
          return returnValue;
        }

    function _nestedValueCurrencyFormatter(row, cell, value, columnDef, dataContext){
      var result = null;
      if (value != 'undefined' && typeof value !== 'undefined' &&
             value.name != 'undefined') {
            result = value.abbrev;
          }
          return _textFormatter(row, cell, result, columnDef, dataContext)
    }

    function _nestedValueExcelFormatter (value) {
          var returnValue = "";
          if (value != null && typeof value !== 'undefined' &&
            value.name !== null) {
            returnValue = value.name;
          };
          return returnValue;
        }

    function _nestedValueFormatter(row, cell, value, columnDef, dataContext){
      var result = null;
      if (value != 'undefined' && typeof value !== 'undefined' &&
             value.name != 'undefined') {
            result = value.name;
          }
          return _textFormatter(row, cell, result, columnDef, dataContext)
    }
    function _textFormatter(row, cell, value, columnDef, dataContext) {
        return "<div>" + (value == null ? "<span class='message'>n/a</span>" : value) + "</div>"
      }
    function _groupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
        var value = "";
        if (totals && totals.sum) {
            value = totals.sum[columnDef.field];
          }
        return isExportToExcel ? value : treasury.formatters.number(null, null, value, columnDef, null);
    }
    function SummationAggregator(field) {
        this.field_ = field;

        this.init = function() {
          this.sum_ = null;
        };
        this.accumulate = function(item) {
          var val = item[this.field_];
          if (val != null && val !== "" && !isNaN(val)) {
            this.sum_ += parseFloat(val);
          }
        };

        this.storeResult = function(groupTotals) {
          if (!groupTotals.sum) {
            groupTotals.sum = {};
          }
          groupTotals.sum[this.field_] = this.sum_;
        };
      }
})();
