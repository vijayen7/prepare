"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.dealReport = window.treasury.dealReport || {};
  window.treasury.dealReport.dealReportGridOptions = {
    getResultOptions: _getResultOptions,
getTopResultOptions: _getTopResultOptions
  };

  function _getResultOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRow: true,
      highlightRowOnClick : true,
      sortList: [{
        columnId: "pnlSpn",
        sortAsc: true
      },{
        columnId: "ca",
        sortAsc: true
      }],
      displaySummaryRow : true,
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      applyFilteringOnGrid : true,
      exportToExcel: true,
      sheetName: "Deal Report",
      cellRangeSelection: {
      	showAggregations: true,
      	multipleRangesEnabled: true
      },
      isFilterOnFormattedValue: true,

     copyCellSelection : true,
     formatAggregateSummary : function(formattedText, AggregationsObj){return(
      `<div class="size--content layout--flex" style="float: right; width: fit-content;">
        <div class="size--content margin--left--small margin--right--small">Count:`+Math.round(isNaN(AggregationsObj.count) ? 0: AggregationsObj.count)+`</div>
        <div class="size--content margin--left--small margin--right--small">Sum:`+Math.round(isNaN(AggregationsObj.sum) ? 0 : AggregationsObj.sum)+`</div>
        <div class="size--content margin--left--small margin--right--small">Average:`+Math.round(isNaN(AggregationsObj.avg) ? 0 : AggregationsObj.avg)+`</div>
      </div>`
     )}
    };
  }
function _getTopResultOptions() {
var options = _getResultOptions();
options.isFilterOnFormattedValue= false;
options.sortList = [{
  columnId: "dealName",
  sortAsc: true
}]

 options.onCellClick= function(args, isNotToggle){
        var colId = args.colId;
    	 if(colId == "lmv" || colId == "gmv" || colId == "smv" || colId == "requirement" || colId == "exposure" || colId == "repoLoanValueUsd") {
         window.treasury.dealReport.dealReportActions.loadBundleMarginData(args);
       }
       else if (colId == "collateral" || colId == "tradeDateCash"){
         window.treasury.dealReport.dealReportActions.loadBundleCollateralData(args);
       }
       else{
    	   window.treasury.dealReport.dealReportActions.loadBundleCollateralData([]);
       }
      }
return options;
}
})();
