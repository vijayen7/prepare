"use strict";

var dealReportGrid;
var dealReportChildGrid;
var url;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.dealReport = window.treasury.dealReport || {};
  window.treasury.dealReport.dealReportActions = {
    loadDealReportData: _loadDealReportData,
    loadBundleMarginData :_loadBundleMarginData,
    loadBundleCollateralData : _loadBundleCollateralData
  };
function _updateId(data){
  for(var i=0;i<data.length;i++) {
    data[i].id=i;
  }
}
function _loadBundleMarginData(args) {

  if(args.item && args.item.bundlePositionExposureMarginDataList && args.item.bundlePositionExposureMarginDataList.length){
    var data = args.item.bundlePositionExposureMarginDataList;
    treasury.dealReport.util.showLoading();
    _updateId(data);
    document.getElementById('dealReportChildGrid').removeAttribute("hidden");
    document.getElementById('dealReportChildGridPanel').removeAttribute("hidden");
    document.getElementById('childSearchMessage').setAttribute("hidden",true);
    var childOptions = treasury.dealReport.dealReportGridOptions
    .getResultOptions();
    document.getElementById('dealReportChildGridPanel').setAttribute("label",args.item.dealName+ " : Underlying Exposure & Requirement Data");
    dealReportChildGrid = new dportal.grid.createGrid(
              $("#dealReportChildGrid"), data,
              treasury.dealReport.dealReportColumnConfig
              .getRequirementExposureColumns(),
              childOptions);
            treasury.dealReport.util.resizeCanvasOnGridChange(dealReportChildGrid,
              'dealReportChildGrid');
            window.treasury.dealReport.util.resizeGrids();
              treasury.dealReport.util.hideLoading();
  }
  else {
    treasury.dealReport.util.clearGrid("dealReportChildGrid");
    document.getElementById('dealReportChildGridPanel').setAttribute("hidden",true);
    document.getElementById('childSearchMessage').removeAttribute("hidden");
  }
}

function _loadBundleCollateralData(args) {
  if(args.item && args.item.dealCaCcyBundleCollateralDataList && args.item.dealCaCcyBundleCollateralDataList.length){
    treasury.dealReport.util.showLoading();
    var data = args.item.dealCaCcyBundleCollateralDataList;
    _updateId(data);
    document.getElementById('dealReportChildGrid').removeAttribute("hidden");
    document.getElementById('dealReportChildGridPanel').removeAttribute("hidden");
    document.getElementById('childSearchMessage').setAttribute("hidden",true);
    var childOptions = treasury.dealReport.dealReportGridOptions
    .getResultOptions();
    document.getElementById('dealReportChildGridPanel').setAttribute("label",args.item.dealName+ " : Underlying Exposure & Requirement Data")
    dealReportChildGrid = new dportal.grid.createGrid(
              $("#dealReportChildGrid"), data,
              treasury.dealReport.dealReportColumnConfig
              .getCollateralColumns(),
              childOptions);
            treasury.dealReport.util.resizeCanvasOnGridChange(dealReportChildGrid,
              'dealReportChildGrid');
            window.treasury.dealReport.util.resizeGrids();
              treasury.dealReport.util.hideLoading();
  }
  else {
    treasury.dealReport.util.clearGrid("dealReportChildGrid");
    document.getElementById('dealReportChildGridPanel').setAttribute("hidden",true);
    document.getElementById('childSearchMessage').removeAttribute("hidden");
  }
}
function _loadDealReportData(parameters) {
  treasury.dealReport.util.showLoading();
  url = "/treasury/service/dealExcessDeficitDataService/getDealExcessDeficitData?inputFormat=PROPERTIES&" +
  "dealExcessDeficitDataFilter.date=" + parameters.dateString +
  "&dealExcessDeficitDataFilter.legalEntityIds=" + parameters.legalEntityIds +
  "&dealExcessDeficitDataFilter.legalEntityFamilyIds=" + parameters.entityFamilyIds +
	"&format=json" ;


  var serviceCall = $.ajax({
    url: url,

    type: "POST"
  });
  document.getElementById('searchMessage').setAttribute("hidden", true);
  document.getElementById('childSearchMessage').setAttribute("hidden",true);
  document.getElementById('quickLinks').setAttribute("hidden", true);
  treasury.dealReport.util.clearGrid('dealReportGrid');
  treasury.dealReport.util.clearGrid('dealReportChildGrid');
  document.getElementById('dealReportChildGridPanel').setAttribute("hidden",true);
  document.getElementById('dealReportGridPanel').setAttribute("hidden",true);
    $.when(serviceCall)
      .done(
        function(data) {
          treasury.dealReport.util.showLoading();
          window.treasury.dealReport.dealReportLanding.setFilters(parameters);
          var dateFilterValue = Date.parse(parameters["dateString"]);
			var minDate = Date.parse('1970-01-01');
			var maxDate = Date.parse('2038-01-01');
			var dateFormat = 'yy-mm-dd';
			treasury.dealReport.util.setupDateFilter(
					$("#datePicker"),  $("#datePickerError"),
					dateFilterValue, minDate, maxDate,
					dateFormat);


                    if (data && data.length) {
                    	if (document.getElementById('dealReportFilterSidebar').state == 'expanded') {
                			document.getElementById('dealReportFilterSidebar').toggle();
                	}
            _updateId(data);
        	  document.getElementById('dealReportGrid').removeAttribute("hidden");
            document.getElementById('dealReportGridPanel').removeAttribute("hidden");
            var searchDate = document.getElementById("searchDate")
                        searchDate.innerHTML = parameters.dateString

                        var noOfRecords =document.getElementById("noOfRecords")
                        noOfRecords.innerHTML = data.length

                        document.getElementById('quickLinks').removeAttribute("hidden");
            var options = treasury.dealReport.dealReportGridOptions
            .getTopResultOptions();
            options.configureReport = { urlCallback : function(){return (window.location.origin + url)}};
        	  dealReportGrid = new dportal.grid.createGrid(
                      $("#dealReportGrid"), data,
                      treasury.dealReport.dealReportColumnConfig
                      .getTopPanelColumns(),
                      options);
                    treasury.dealReport.util.resizeCanvasOnGridChange(dealReportGrid,
                      'dealReportGrid');
                        window.treasury.dealReport.util.resizeGrid();
                      treasury.dealReport.util.hideLoading();
          } else {
            treasury.dealReport.util.hideLoading();
            treasury.dealReport.util.clearGrid("dealReportChildGrid");
            treasury.dealReport.util.clearGrid("dealReportGrid");
            document.getElementById('dealReportChildGridPanel').setAttribute("hidden",true);
            document.getElementById('dealReportGridPanel').setAttribute("hidden",true);
            document.getElementById('searchMessage').removeAttribute("hidden");
            document.getElementById('quickLinks').setAttribute("hidden",true);
            document.getElementById('searchMessage').innerHTML = 'No records found for search criteria';
          }
      });

      serviceCall
        .fail(function(xhr, text, errorThrown) {
          treasury.dealReport.util.hideLoading();
          treasury.dealReport.util
            .showErrorMessage('Error occurred while retrieving data.');
        });
    }
})();
