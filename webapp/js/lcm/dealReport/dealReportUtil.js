"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.dealReport = window.treasury.dealReport || {};
  window.treasury.dealReport.util = {
    showLoading: _showLoading,
    hideLoading: _hideLoading,
    showErrorMessage: _showErrorMessage,
    setupDateFilter: _setupDateFilter,
    clearGrid: _clearGrid,
    resizeCanvasOnGridChange: _resizeCanvasOnGridChange,
    resizeAllCanvas: _resizeAllCanvas,
    getFormattedMessage: _getFormattedMessage,
    resizeGrids : _resizeGrids,
    resizeGrid : _resizeGrid
  };

  function _resizeGrid() {
    var dealReportGridDiv = document.getElementById('dealReportGridDiv');
    var totalHeight = dealReportGridDiv.offsetHeight;
    dealReportGrid.options.maxHeight = totalHeight - 110;
    dealReportGrid.resizeCanvas();
  }
function _resizeGrids() {
  var dealReportGridDiv = document.getElementById('dealReportGridDiv');
  var totalHeight = dealReportGridDiv.offsetHeight;
  dealReportGrid.options.maxHeight = totalHeight/2 - 100;
  dealReportChildGrid.options.maxHeight = totalHeight/2 - 100;
  $('#dealReportGrid-pager').addClass('margin--bottom');
  dealReportGrid.resizeCanvas();
  dealReportChildGrid.resizeCanvas();

}
  function _clearGrid(gridId) {
    treasury.common.grid.clearGrid(gridId);
    if ($("#" + gridId).prev().prev().is("p") ||
      $("#" + gridId).prev().prev().is("center")) {
      $("#" + gridId).prev().prev().remove();
    } else if ($("#" + gridId).prev().is("p") ||
      $("#" + gridId).prev().is("center")) {
      $("#" + gridId).prev().remove();
    }
    var groupingHeader = document.getElementById(gridId + '-drop-element-holder');
    if (groupingHeader != null) {
      groupingHeader.innerHTML = "";
    }

    var header = document.getElementById(gridId + "-header");
    if (header != null) {
      header.innerHTML = "";
    }

    var pager = document.getElementById(gridId + "-pager");
    if (pager != null) {
      pager.innerHTML = "";
    }
  }

  function _setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
    dateFormat) {
    dateDiv.datepicker({
      dateFormat: dateFormat,
      changeMonth: true,
      changeYear: true,
      minDate: minDate,
      maxDate: maxDate,
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
    }).datepicker("setDate", date);
    dateDiv.change(function() {
      validateDate(dateDiv.val(), {
        errorDiv: errorDiv,
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: dateFormat,
      });
    });
  }

  function _showLoading() {
    $(".loader").removeAttr("hidden");
    $(".overlay").removeAttr("hidden");
  }

  function _hideLoading() {
    $(".loader").attr("hidden", "true");
    $(".overlay").attr("hidden", "true");
  }

  function _showErrorMessage(message) {
    var errorDialog = document.getElementById("errorMessage");

    errorDialog.reveal({
      'title': 'Error',
      'content': "<center><strong>" + message +
        "</strong></center>",
      'modal': true,
      'buttons': [{
        'html': 'OK',
        'callback': function() {
          this.visible = false;
        },
        'position': 'right'
      }]
    });
    errorDialog.removeAttribute("hidden");
  }

  function _resizeAllCanvas() {
    if(document.getElementById('dealReportFilterSidebar').state == 'expanded') {
      document.getElementById('dealReportFilterSidebar').style.width = '300px';
    } else {
      document.getElementById('dealReportFilterSidebar').style.width = '20px';
    }
    if(dealReportGrid != null) {
          dealReportGrid.resizeCanvas();
        }
        if(dealReportChildGrid != null) {
          dealReportChildGrid.resizeCanvas();
        }

  }

  function _resizeCanvasOnGridChange(gridName, gridId) {
    if (gridId != undefined) {
        $('#' + gridId).height($('#' + gridId).height() + 11);
    }
    if(gridName != null) {
      gridName.resizeCanvas();
    }

  }

  function _getFormattedMessage(message) {
      return "<center><div class='message no-icon' style='width: 30%;'>" + message + "</div></center>";
  }

  function _getFormattedData(value, precision) {
    if(value == null || value == "") {
      return "";
    } else {
      return parseFloat(value).toFixed(precision);
    }
  }
})();
