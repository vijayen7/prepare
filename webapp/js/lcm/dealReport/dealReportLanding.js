"use strict";
var dealReportFilterGroup;
var entityFamilyMultiSelect;
var legalEntityMultiSelect;

(function() {
	window.treasury = window.treasury || {};
	window.treasury.dealReport = window.treasury.dealReport || {};
	window.treasury.dealReport.dealReportLanding = {
		loadFilters : _loadFilters,
		loadSearchResults : _loadSearchResults,
		resetFilters : _resetFilters,
		copySearchUrl : _copySearchUrl,
		setFilters : _setFilters
	};

	var entityFamilyToEntitiesMap = [];
	var legalEntityAllDataMap = [];

	window.addEventListener("WebComponentsReady", _initialize);

function _setFilters(parameters){
	var entityFamilyIds = parameters["entityFamilyIds"];
	var legalEntityIds = parameters["legalEntityIds"];

	if(entityFamilyIds!=undefined) {
			entityFamilyIds = entityFamilyIds.split(',');
				entityFamilyMultiSelect.setSelectedNodes(entityFamilyIds);
	}
	if(legalEntityIds!=undefined) {
			legalEntityIds = legalEntityIds.split(',');
			legalEntityMultiSelect.setSelectedNodes(legalEntityIds);
	}
}
	function _initialize() {

		$(document).on({
			ajaxStart : function() {
				treasury.dealReport.util.showLoading();
			},
			ajaxStop : function() {
				treasury.dealReport.util.hideLoading();
			}
		});

		var saveSettings = document.querySelector('arc-save-settings');
        var host = '';
        var clientName = document.getElementById("clientName").value;
        var stabilityLevel = document.getElementById("stabilityLevel").value;

        if(clientName === 'desco') {
            if(stabilityLevel == 'prod') {
                host += "http://landing-app.deshaw.c.ia55.net";
            } else if(stabilityLevel == 'uat'){
                host = "http://landing-app.deshawuat.c.ia55.net";
            } else if(stabilityLevel == 'qa'){
                host = "https://mars.arcesium.com";
            } else if(stabilityLevel == 'dev'){
                host = "https://terra.arcesium.com";
            }
        }
        saveSettings.serviceURL = host+'/service/SettingsService';
        saveSettings.applicationId = 3;
        saveSettings.applicationCategory = 'DealReport';

        document.getElementById('summary-search-save').onclick = function(){
          saveSettings.openSaveDialog();
        };

        saveSettings.saveSettingsCallback = function(){

        var parameters = dealReportFilterGroup.getSerializedParameterMap();
        return parameters;

        }
        saveSettings.applySettingsCallback = function(parameters){

					_setFilters(parameters);

        }



		_registerHandlers();
		_loadFilters();


	}

	function _resetFilters() {
		entityFamilyMultiSelect.setSelectedNodes([]);
		legalEntityMultiSelect.setSelectedNodes([]);

	}

	function _registerHandlers() {
		document.getElementById('searchId').onclick = treasury.dealReport.dealReportLanding.loadSearchResults;
		document.getElementById('resetId').onclick = treasury.dealReport.dealReportLanding.resetFilters;
		document.getElementById('refreshId').onclick = treasury.dealReport.dealReportLanding.loadSearchResults;
		document.getElementById('copySearchURL').onclick = treasury.dealReport.dealReportLanding.copySearchUrl;
		document.getElementById('dealReportFilterSidebar').addEventListener("stateChange",
		treasury.dealReport.util.resizeAllCanvas);
	}

	function _loadFilters() {
		var errorDialog = document.getElementById('errorMessage');
		errorDialog.visible = false;


				$.when(treasury.loadfilter.loadEntityFamilies(),
						treasury.loadfilter.legalEntities(),
						treasury.loadfilter.defaultDates())
				.done(
						function(legalEntityFamiliesData, legalEntitiesData,
								defaultDatesData) {

							entityFamilyMultiSelect = new window.treasury.filter.MultiSelectNew(
									"entityFamilyIds",
									"entityFamilyFilter",
									legalEntityFamiliesData[0].descoEntityFamilies,
									[]);

							legalEntityMultiSelect = new window.treasury.filter.MultiSelectNew(
									"legalEntityIds", "legalEntityFilter",
									legalEntitiesData[0].descoEntities, []);

							var dateFilterValue = new Date(
									treasury.defaults.date
											|| defaultDatesData[0].tMinusOneFilterDate);
							var minDate = Date.parse('1970-01-01');
							var maxDate = Date.parse('2038-01-01');
							var dateFormat = 'yy-mm-dd';
							treasury.dealReport.util.setupDateFilter(
									$("#datePicker"), $("#datePickerError"),
									dateFilterValue, minDate, maxDate,
									dateFormat);

							dealReportFilterGroup = new treasury.filter.FilterGroup(
									[ entityFamilyMultiSelect,
											legalEntityMultiSelect ], null,
									$("#datePicker"), null);

							var legalEntityFilter = document
									.getElementById("legalEntityFilter");
							var legalEntityFilterData = legalEntityFilter.data;

							for (var i = 0; i < legalEntityFilterData.length; i++) {
								legalEntityAllDataMap[legalEntityFilterData[i].key] = legalEntityFilterData[i];
							}

							var entityFamilyFilter = document
									.getElementById("entityFamilyFilter");
							var entityFamilyFilterData = entityFamilyFilter.data;

							var dataNodeIds = [];
							if (entityFamilyFilterData !== undefined) {
								for (var i = 0; i < entityFamilyFilterData.length; i++) {
									dataNodeIds
											.push(entityFamilyFilterData[i].key);
								}
							}


							if (legalEntityFamiliesData &&  legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap
								&& legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap.length) {
									var data = legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap
									for (var i = 0; i < data.length; i++) {

											var entityFamilyId = data[i].entityFamilyId;
											entityFamilyToEntitiesMap[entityFamilyId] = data[i].entities;

									}
							}

							entityFamilyFilter.onchange = function() {
								_handleEntityFamilyChange();
							};

							var saveSettings = document.querySelector('arc-save-settings');
					        saveSettings.retrieveSettings();

								_loadCopyUrlData();


						});
	}
	function _handleEntityFamilyChange() {

		var legalEntityFilter = document.getElementById("legalEntityFilter");

		var selectedEntityFamilies = entityFamilyMultiSelect.getSelectedIds();

		var legalEntitiesToShow = [];
		var legalEntitiesToSelect = [];

		if (selectedEntityFamilies != "-1") {
			for (var j = 0; j < selectedEntityFamilies.length; j++) {
				if (selectedEntityFamilies[j] in entityFamilyToEntitiesMap) {
					legalEntitiesToSelect = legalEntitiesToSelect
							.concat(entityFamilyToEntitiesMap[selectedEntityFamilies[j]]);
				}
			}

			for (var i = 0; i < legalEntitiesToSelect.length; i++) {
				if (legalEntitiesToSelect[i] in legalEntityAllDataMap) {
					legalEntitiesToShow
							.push(legalEntityAllDataMap[legalEntitiesToSelect[i]]);
				}

			}
		} else {
			for ( var j in legalEntityAllDataMap) {
				legalEntitiesToShow.push(legalEntityAllDataMap[j]);
			}
		}

		legalEntityFilter.data = legalEntitiesToShow;
	}

	function _loadSearchResults() {



		var parameters = dealReportFilterGroup.getSerializedParameterMap();

		treasury.dealReport.dealReportActions
					.loadDealReportData(parameters);

	}

	function _copySearchUrl() {


			treasury.dealReport.util.showLoading();
			var url = "/treasury/lcm/dealReport";
			var parameters = dealReportFilterGroup.getSerializedParameterMap();
			parameters["isCopyUrl"] = true;


			generateReportURL(url, parameters, "resultList");
			treasury.dealReport.util.hideLoading();
}
function _loadCopyUrlData()
{
	if(window.location.href.indexOf("isCopyUrl=true") >= 0) {
		if (document.getElementById('dealReportFilterSidebar').state == 'expanded') {
			document.getElementById('dealReportFilterSidebar').toggle();
	}

		treasury.dealReport.util.showLoading();

    		var urlParams = window.location.href.split("?");
    		if(urlParams[1] != undefined) {
    			var paramList = urlParams[1].split("&");
    		}
    		var searchData = {};
    		for(var i=0; i < paramList.length ; i++) {
    			var param = paramList[i].split("=");
    			var val = param[1];
    			if(val != null && param[0] != "isCopyUrl") {
    				searchData[param[0]]=val;
    			}
    		}

				treasury.dealReport.dealReportActions
							.loadDealReportData(searchData);




}
else {
	document.getElementById('searchMessage').removeAttribute("hidden");
}

}

})();
