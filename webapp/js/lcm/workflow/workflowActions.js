"use strict";

var workflowChangesGrid;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.workflow = window.treasury.workflow || {};
  window.treasury.workflow.actions = {
    loadWorkflowChanges: _loadWorkflowChanges,
    workflowChangesColumnsConfig: _workflowChangesColumnsConfig,
    workflowChangesGridOptions: _workflowChangesGridOptions
  };

  function _loadWorkflowChanges(parameters) {
    var workflowChangesCall = $.ajax({
      url: "/treasury/lure/search-liquidity-publish",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    workflowChangesCall
      .done(function(data) {
        treasury.lure.util.clearGrid("workflowChangesGrid");
        document.getElementById('workflowChangesGrid').removeAttribute("hidden");
        workflowChangesGrid = new dportal.grid.createGrid(
          $("#workflowChangesGrid"), data.resultList,
          treasury.workflow.actions.workflowChangesColumnsConfig(),
          treasury.workflow.actions.workflowChangesGridOptions());
        treasury.lure.util.resizeCanvasOnGridChange(workflowChangesGrid,
         'workflowChangesGrid', null);
         document.getElementById('workflowChangesGrid').removeAttribute("hidden");
        document.getElementById('workflowChangesGrid-header').style = "width:20%";
      });

    workflowChangesCall.fail(function(xhr, text, errorThrown) {
      treasury.lure.util.clearGrid("workflowChangesGrid");
      treasury.lure.util.hideLoading();
      console.log("Error occurred while retrieving results: " + errorThrown);
    });
  }

  function _workflowChangesGridOptions() {
    return {
      enableMultilevelGrouping: false,
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRow: false,
      sortList: [{
        columnId: "source",
        sortAsc: true
      }],
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      useAvailableScreenSpace: true,
      exportToExcel: false
    };
  }

  function _workflowChangesColumnsConfig() {
      return [{
        id: "source",
        type: "text",
        name: "Data Source",
        field: "source",
        sortable: true
      }, {
        id: "changeStatus",
        type: "text",
        name: "Change Status",
        field: "change_status",
        sortable: false
      }];
  }
})();
