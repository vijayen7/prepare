"use strict";
var usageSummaryByData = [
  [1, 'Entity Family By BU'],
  [2, 'Entity Family'],
  [3, 'Ownership Entity By BU']
];

(function() {
  window.treasury = window.treasury || {};
  window.treasury.workflow = window.treasury.workflow || {};
  window.treasury.workflow.landing = {
    loadWorkflowLandingView: _loadWorkflowLandingView,
    resizeAllCanvas: resizeAllCanvas,
    emailLiquidityReport: _emailLiquidityReport,
    saveWorkflow: _saveWorkflow,
    cancelWorkflow: _cancelWorkflow,
    publishWorkflow: _publishWorkflow
  };

   WebComponents.waitFor(function() {
        var tabsContainer = document.querySelector('#workflowDataTabs');
        if (tabsContainer) {
          tabsContainer.initializeTabs(tabsContainer);
        }
        _initializeWorkflow();
      });

  function _initializeWorkflow() {
    $(document).on({
      ajaxStart: function() {
        treasury.lure.util.showLoading();
      },
      ajaxStop: function() {
        treasury.lure.util.hideLoading();
      }
    });
    var parameters = {};
    parameters.dateString = sessionStorage.getItem('workflowDate');
    treasury.lure.util.showLoading();
    treasury.workflow.landing.loadWorkflowLandingView(parameters);
    _registerHandlers();
  }

  function _registerHandlers() {
    document.getElementById("workflowDataTabs").addEventListener("stateChange", resizeAllCanvas);
    document.getElementById("saveExitWorkflow").onclick = treasury.workflow.landing.saveWorkflow;
    document.getElementById("cancelWorkflow").onclick = treasury.workflow.landing.cancelWorkflow;
    document.getElementById("publishWorkflow").onclick = treasury.workflow.landing.emailLiquidityReport;
  }

  function _loadWorkflowLandingView(parameters) {
    var liquidityReportParams = jQuery.extend({}, parameters);
    liquidityReportParams.teLiquidity = true;
    liquidityReportParams.workflowInstanceId = sessionStorage.getItem('workflowId');
    var liquidityReportCall = $.ajax({
      url: "/treasury/lure/get-liquidity-report",
      data: liquidityReportParams,
      type: "POST",
      dataType: "json"
    });

    var liquiditySummaryParams = jQuery.extend({}, parameters);
    liquiditySummaryParams.fundLiquidity = true;
    var liquiditySummaryCall = $.ajax({
      url: "/treasury/lure/get-liquidity-report",
      data: liquiditySummaryParams,
      type: "POST",
      dataType: "json"
    });

    var usageSummaryParams = jQuery.extend({}, parameters);
    usageSummaryParams.usageSummaryFilterId = 1;
    usageSummaryParams.showInMillionsOption = true;
    usageSummaryParams.workflowId = sessionStorage.getItem('workflowId');
    var usageSummaryCall = $.ajax({
      url: "/treasury/lure/get-usage-summary-report",
      data: usageSummaryParams,
      type: "POST",
      dataType: "json"
    });

    treasury.lure.util.showLoading();
    $.when(liquidityReportCall, liquiditySummaryCall, usageSummaryCall).done(
      function(liquidityReportData, liquiditySummaryData, usageSummaryData) {
        treasury.liquidity.liquidityReportActions.loadLiquidityGrid(liquidityReportData[0], liquidityReportParams);
        treasury.liquidity.liquidityReportActions.loadLiquidityGrid(liquiditySummaryData[0], liquiditySummaryParams);
        treasury.lure.usageSummaryActions.loadUsageSummaryGrid(usageSummaryData[0], usageSummaryParams, true);

        var panelLabel = document.getElementById('workflowPanel').label;
        panelLabel = panelLabel.concat(" for date ").concat(parameters.dateString);
        document.getElementById('workflowPanel').label = panelLabel;
        document.getElementById('liquidityReportGrid').removeAttribute("hidden");
        document.getElementById('liquiditySummaryGrid').removeAttribute("hidden");
        document.getElementById('usageSummaryGrid').removeAttribute("hidden");
        resizeAllCanvas();
        var workflowIdElement = document.createElement("input");
        workflowIdElement.type = "hidden";
        workflowIdElement.value = sessionStorage.getItem('workflowId');
        workflowIdElement.id = "workflowId";
        document.body.appendChild(workflowIdElement);
        treasury.lure.util.hideLoading();
      }
    );
  }

  function resizeAllCanvas() {
    if (document.getElementById('workflowDataTabs').selectedTab == "Publish") {
      if (document.getElementById('workflowId') == null || document.getElementById('workflowId').value == null ||
        document.getElementById('workflowId').value == -1) {
        return;
      } else {
        treasury.lure.util.showLoading();
        var parameters = {};
        parameters.workflowId = document.getElementById('workflowId').value;
        parameters.workflowState = sessionStorage.getItem('workflowState');
        treasury.workflow.actions.loadWorkflowChanges(parameters);
        document.getElementById('publishWorkflow').disabled = false;
      }
    } else {
      if (usageSummaryGrid != null) {
        usageSummaryGrid.resizeCanvas();
      }
      if (liquidityReportGrid != null) {
        liquidityReportGrid.resizeCanvas();
      }
      if (liquiditySummaryGrid != null) {
        liquiditySummaryGrid.resizeCanvas();
      }
    }
  }

  function _saveWorkflow() {
    treasury.lure.util.showLoading();
    treasury.liquidity.workflowUtils.workflowExecute('SAVED', 'Workflow has been saved.', null);
    treasury.lure.util.hideLoading();
    treasury.lure.util.showErrorMessage("Save", "Workflow has been saved.", true);
  }

  function _cancelWorkflow() {
      treasury.lure.util.showLoading();
      var returnVal = treasury.liquidity.workflowUtils.workflowExecute('CANCELLED', 'Workflow has been cancelled.', null);
      treasury.lure.util.hideLoading();
      if(returnVal == 1 || returnVal === undefined) {
        treasury.lure.util.showErrorMessage("Cancel", "Workflow has been cancelled.", true);
        $('#workflowId').remove();
        sessionStorage.removeItem("workflowId");
      } else if (returnVal == 0) {
        treasury.lure.util.showErrorMessage("Error", "Workflow instance is not defined.", true);
      }
  }

  function _emailLiquidityReport() {
    var parameters = {};
    parameters.workflowId = sessionStorage.getItem('workflowId');
    parameters.workflowState = sessionStorage.getItem('workflowState');
    treasury.lure.util.showLoading();
    document.getElementById('publishWorkflow').disabled = true;
    var emailLiquidityReportCall = $.ajax({
      url: "/treasury/lure/email-liquidity-report",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    emailLiquidityReportCall
      .done(function(data) {
        document.getElementById("lureMessage").innerHTML = treasury.lure.util.getFormattedMessage("Liquidity report email has been sent.", "");
        document.getElementById("lureMessage").removeAttribute("hidden");
        treasury.lure.util.hideLoading();
        _publishWorkflow();
      });

    emailLiquidityReportCall.fail(function(xhr, text, errorThrown) {
          if(xhr.status==502){
             if (confirm("Report generation is taking more time than expected. Please contact help@arcesium.com, if report is not delivered in next 5 minutes.\n Continue to Publish?") == true) {
              _publishWorkflow();
             }
             else {
              return;
            }
          }
          else if (confirm("Mail was not sent, do you want to pubilsh workflow?") == false) {
            return;
          }
          else {
            _publishWorkflow();
          }
        });
  }

  function _publishWorkflow() {
    treasury.lure.util.showLoading();
    treasury.liquidity.workflowUtils.workflowExecute('PUBLISHED', 'Workflow has been published.', null);
    document.getElementById('publishWorkflow').disabled = true;
    document.getElementById('publishWorkflow').title = 'Already published.';
    $('#workflowId').remove();
    sessionStorage.removeItem("workflowId");
    treasury.lure.util.hideLoading();
    treasury.lure.util.showErrorMessage("Publish", "Workflow has been published.", true);
  }
})();
