"use strict";

var liquidityReportGrid;
var liquiditySummaryGrid;
var gridDiv;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.liquidity = window.treasury.liquidity || {};
  window.treasury.liquidity.liquidityReportActions = {
    loadLiquidityReport: _loadLiquidityReport,
    loadLiquidityGrid: _loadLiquidityGrid
  };

  function _loadLiquidityReport(parameters) {
    treasury.lure.util.showLoading();
    var serviceCall = $.ajax({
      url: "/treasury/lure/get-liquidity-report",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        treasury.liquidity.liquidityReportActions.loadLiquidityGrid(data, parameters);
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      treasury.lure.util.clearGrid(gridDiv);
      treasury.lure.util.hideLoading();
      console.log("Error occurred while retrieving results: " + errorThrown);
    });
  }

  function _loadLiquidityGrid(data, parameters) {
    treasury.lure.util.hideLoading();
    if (data && data.resultList && data.resultList.length) {
      if(document.getElementById('lureMessage') != null) {
        document.getElementById('lureMessage').setAttribute("hidden", true);
      }
      var columnConfigs = null;
      if (parameters.teLiquidity) {
        columnConfigs = treasury.liquidity.liquidityReportColumnsConfig
          .getResultColumns();
          gridDiv = "liquidityReportGrid";
          document.getElementById('liquiditySummaryGrid').setAttribute("hidden", true);
      } else {
        columnConfigs = treasury.liquidity.liquidityReportColumnsConfig
        .getSummaryResultColumns();
        gridDiv = "liquiditySummaryGrid";
        document.getElementById('liquidityReportGrid').setAttribute("hidden", true);
      }
      document.getElementById(gridDiv).removeAttribute("hidden");
      liquidityReportGrid = new dportal.grid.createGrid(
        $("#" + gridDiv), data.resultList,
        columnConfigs,
        treasury.liquidity.liquidityReportGridOptions
        .getResultOptions());
        if(parameters.teLiquidity) {
          var adjustmentData = new Array();
          for(var i in data.resultList) {
            var adjustments = data.resultList[i].adjustments;
            if(adjustments.length > 0) {
              adjustmentData = adjustmentData.concat(adjustments);
            }
          }
          if(adjustmentData.length > 0) {
            treasury.liquidity.adjustments.loadAdjustmentsReport(adjustmentData);
          }
        }
      treasury.lure.util.resizeCanvasOnGridChange(liquidityReportGrid,
       gridDiv, 'liquidityReportFilterSidebar');
     } else if (data && data.errorMessage) {
          if(data.errorMessage.includes('UncheckedTimeoutException')){
            treasury.lure.util.showErrorMessage('Error', 'Timeout Error occurred while fetching MtdPnl data from Manr database. Please contact Archelp.', false);
          }
          else{
            treasury.lure.util.showErrorMessage('Error', data.errorMessage, false);
          }  
      console.log("Error in fetching liquidity data");
     } else if (data && (data.length == null || data.length == 0)) {
       if(document.getElementById('lureMessage') != null) {
        document.getElementById('lureMessage').removeAttribute("hidden");
        document
          .getElementById('lureMessage').innerHTML = treasury.lure.util.getFormattedMessage(
                'No results found matching selected criteria.', '');
      }
     }
  }
})();
