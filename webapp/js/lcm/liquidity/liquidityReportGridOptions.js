/*jshint jquery:true*/
"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.liquidity = window.treasury.liquidity || {};
  window.treasury.liquidity.liquidityReportGridOptions = {
    getResultOptions: _getResultOptions
  };

  function _getResultOptions() {
    return {
      enableMultilevelGrouping: false,
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRow: false,
      sortList: [{
        columnId: "entity",
        sortAsc: true
      }],
      summaryRow: true,
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      useAvailableScreenSpace: true,
      exportToExcel: true,
      sheetName: "Liquidity Report"
    };
  }
})();
