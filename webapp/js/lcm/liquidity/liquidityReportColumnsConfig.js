"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.liquidity = window.treasury.liquidity || {};
  window.treasury.liquidity.liquidityReportColumnsConfig = {
    getResultColumns: _getResultColumns,
    getSummaryResultColumns: _getSummaryResultColumns
  };

  function _getResultColumns() {
    return [{
      id: "entity",
      type: "text",
      name: "Trading Entity Family",
      field: "trading_entity_family",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "mmfs",
      type: "number",
      name: "MMFs",
      field: "mmfs",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "cashAtBrokers",
      type: "number",
      name: "Cash at Brokers",
      field: "cash_at_brokers",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    // }, {
    //   id: "paymentsExpected",
    //   type: "number",
    //   name: "Payments/Funds Expected In/Out Within the Week",
    //   field: "payments_expected",
    //   sortable: true,
    //   formatter: dpGrid.Formatters.Float,
    //   headerCssClass: "aln-rt b"
    // }, {
    //   id: "valuationDifferences",
    //   type: "number",
    //   name: "Valuation Differences in Entity's Favour",
    //   field: "valuation_differences",
    //   sortable: true,
    //   formatter: dpGrid.Formatters.Float,
    //   headerCssClass: "aln-rt b"
    }, {
      id: "temporaryUsage",
      type: "number",
      name: "Temporary Treasury Usage",
      field: "temporary_usage",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "totalCash",
      type: "number",
      name: "Total Cash",
      field: "total_cash",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }, {
      id: "targetTradingBuffer",
      type: "number",
      name: "Target Trading Buffer",
      field: "target_trading_buffer",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }, {
      id: "targetTreasuryBuffer",
      type: "number",
      name: "Target Treasury Buffer",
      field: "target_treasury_buffer",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "totalBuffer",
      type: "number",
      name: "Total Buffer",
      field: "total_buffer",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }, {
      id: "nav",
      type: "number",
      name: "NAV",
      field: "nav",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }, {
      id: "totalCashNAV",
      type: "number",
      name: "Total Cash (%NAV)",
      field: "total_cash_nav",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "marginUsage",
      type: "number",
      name: "Margin Usage",
      field: "margin_usage",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }, {
      id: "expectedTotalLiquidity",
      type: "number",
      name: "Expected Total Liquidity",
      field: "expected_total_liquidity",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }, {
      id: "actualVsExpected",
      type: "number",
      name: "Diff between Actual & Expected Liquidity",
      field: "actual_vs_expected",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    // }, {
    //   id: "valuationDiff",
    //   type: "number",
    //   name: "Valudation Diff against Entity",
    //   field: "valuation_diff",
    //   sortable: true,
    //   formatter: dpGrid.Formatters.Float,
    //   headerCssClass: "aln-rt b",
    //   excelFormatter: "#,##0",
    }, {
      id: "unsettledBankDebt",
      type: "number",
      name: "Unsettled Bank Debt Buys",
      field: "unsettled_bank_debt",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }, {
      id: "unexplainedDifference",
      type: "number",
      name: "Unexplained Difference",
      field: "unexplained_difference",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
    }];
  }

  function _getSummaryResultColumns() {
    var liquidityResultColumns = _getResultColumns();
    var liquiditySummaryResultColumns = [
      {
        id: "entity",
        type: "text",
        name: "Ownership Entity",
        field: "ownership_entity",
        sortable: true,
        headerCssClass: "aln-rt b"
      },
      {
        id: "cashInFeeders",
        type: "number",
        name: "Cash-In-Feeders",
        field: "cash_in_feeders",
        sortable: true,
        formatter: dpGrid.Formatters.Float,
        headerCssClass: "aln-rt b"
      },
      {
        id: "proRataShare",
        type: "number",
        name: "Pro-rata Share of Cash in Trading Vehicle",
        field: "pro_rata_share",
        sortable: true,
        formatter: dpGrid.Formatters.Float,
        headerCssClass: "aln-rt b"
      }
    ];
    liquiditySummaryResultColumns = liquiditySummaryResultColumns.concat(liquidityResultColumns.slice(4, 9));
    liquiditySummaryResultColumns = liquiditySummaryResultColumns.concat(liquidityResultColumns.slice(10));
    //liquiditySummaryResultColumns = liquiditySummaryResultColumns.concat(liquidityResultColumns.slice(12, 17));
    return liquiditySummaryResultColumns;
  }
})();
