"use strict";

var workflowVersionsFilter;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.liquidity = window.treasury.liquidity || {};
  window.treasury.liquidity.landing = {
    loadLiquidityLandingView: _loadLiquidityLandingViewFilters,
    loadSearchResults: _loadSearchResults,
    loadWorkflowLanding: _loadWorkflowLanding
  };

  window.addEventListener("WebComponentsReady", _initializeLiquidity);

  function _initializeLiquidity() {
    $(document).on({
      ajaxStart: function() {
        treasury.lure.util.showLoading();
      },
      ajaxStop: function() {
        treasury.lure.util.hideLoading();
      }
    });

    _registerHandlers();
    treasury.liquidity.landing.loadLiquidityLandingView();
  }

  function _registerHandlers() {
    document.getElementById('liquidityReportSearch').onclick = treasury.liquidity.landing.loadSearchResults;
    document.getElementById('liquidityReportFilterSidebar').addEventListener("stateChange", resizeAllCanvas);
    document.getElementById('startWorkflow').onclick = treasury.liquidity.landing.loadWorkflowLanding;
    document.getElementById('copySearchURL').addEventListener('click', copySearchURL);
  }

  function resizeAllCanvas() {
    if (liquidityReportGrid != null) {
      liquidityReportGrid.resizeCanvas();
    }

    if(liquiditySummaryGrid != null) {
      liquiditySummaryGrid.resizeCanvas();
    }
  }

  function _loadLiquidityLandingViewFilters() {
    $.when(treasury.loadfilter.defaultDates()).done(
      function(defaultDatesData) {
        var dateFilterValue = treasury.defaults.date || defaultDatesData.tMinusOneFilterDate;

        var minDate = Date.parse('1970-01-01');
        var maxDate = Date.parse('2038-01-01');
        var dateFormat = 'yy-mm-dd';
        treasury.lure.util.setupDateFilter($("#datePicker"),
          $("#datePickerError"), dateFilterValue, minDate, maxDate, dateFormat);
        $('#datePicker').datepicker().on("input change", loadPublishedWorkflowVersions);
        loadPublishedWorkflowVersions();
      }
    );
  }

  function _loadSearchResults() {
    if (document.getElementById('datePickerError').innerHTML != '') {
      ArcMessageHelper.showMessage('error', 'Please select a valid date.', null, null);
      return;
    }
    if (new Date(document.getElementById('datePicker').value) == '') {
      ArcMessageHelper.showMessage('warning', 'Please select date.', null, null);
      return;
    }
    var parameters = {};
    parameters.dateString = document.getElementById('datePicker').value;
    parameters.workflowInstanceId = document.getElementById('workflowVersionsFilter').value.key;
    var liquidityViewOptions = document.getElementsByName('liquidityViewOptions');
    for (var i in liquidityViewOptions) {
      parameters[liquidityViewOptions[i]['id']] = liquidityViewOptions[i]['checked'];
    }
    treasury.lure.util.clearGrid('liquidityReportGrid');
    treasury.lure.util.clearGrid('liquiditySummaryGrid');
    treasury.lure.util.clearGrid("liquidityAdjustmentsGrid");
    if (document.getElementById('liquidityReportFilterSidebar').state == 'expanded') {
      document.getElementById('liquidityReportFilterSidebar').toggle();
    }
    if (parameters.fundLiquidity) {
      document.getElementById('liquidityAdjustmentContent').setAttribute("hidden", true);
      document.getElementById('liquidityAdjustmentsGrid').setAttribute("hidden", true);
      treasury.lure.util.clearGrid("liquidityAdjustmentsGrid");
    }
    treasury.lure.util.clearGrid("liquidityReportGrid");
    treasury.lure.util.clearGrid("liquiditySummaryGrid");
    treasury.liquidity.liquidityReportActions.loadLiquidityReport(parameters);
  }

  function _loadWorkflowLanding() {
    var workflowUnit = "EntityDate";
    var workflowType = "LIQUIDITY";
    var workflowDate = document.getElementById('datePicker').value;
    var entityFamilyId = -1;
    var records = workflowUnit.concat("_").concat(workflowType).concat("_")
      .concat(entityFamilyId).concat("_").concat(workflowDate);
    records = records.replace(/-/g, "");

    var parameters = "";
    parameters = "workflowParam.dateStr=" + workflowDate;
    parameters = parameters + "&workflowParam.workflowTypeName=" + workflowType;
    parameters = parameters + "&workflowParam.workflowUnit=" + workflowUnit;
    parameters = parameters + "&workflowParam.entityFamilyId=" + entityFamilyId;
    parameters = parameters + "&records=" + records;
    sessionStorage.setItem("workflowDate", workflowDate);
    sessionStorage.setItem("records", records);

    treasury.lure.util.showLoading();
    var serviceCall = $.ajax({
      url: "/treasury/comet/workflowBegin",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {

            var workflowIdElement = document.createElement("input");
            workflowIdElement.type = "hidden";
            workflowIdElement.value = data.resultList[0].workflowId;
            workflowIdElement.id = "workflowId";
            document.body.appendChild(workflowIdElement);
            treasury.lure.util.hideLoading();

            // Following is taken from agreementWorkflowAction
            // Append workflowId after begin workflow
            var wfDataParam = "workflowParam.workflowId=" + data.resultList[0].workflowId;

            var workflowState = data.resultList[0].workflowState;

            // Conflict scenario popup subject and message
            var conflictSubject = data.resultList[0].subject;
            var conflictMessage = data.resultList[0].message;

            // For new workflow the state will be BEGIN_WORKFLOW,
            // reload the agreement summary screen and open a new
            // child window
            if (workflowState == 'BEGIN_WORKFLOW') {
              sessionStorage.setItem("workflowId", data.resultList[0]['workflowId']);
              sessionStorage.setItem("workflowState", data.resultList[0]['workflowState']);
              window.open('/treasury/lure/liquidityWorkflow');
            }

            // A workflow already started by the same user
            if (workflowState == 'RESUME_WORKFLOW') {


              // On Cancel workflow clearing the workflow conflict
              // popup, cancelling the workflow and closing dialog
              var cancelWorkflow = document
                .getElementById('cancelWorkflow');
              cancelWorkflow.disabled = false;
              cancelWorkflow.onclick = function() {

                // Cancel the workflow and closing dialog
                treasury.liquidity.workflowUtils.workflowExecute('CANCELLED', 'Workflow has been cancelled.', null);
                treasury.lure.util.showErrorMessage("Cancel", "Workflow has been cancelled.", false);
                $('#workflowId').remove();
                sessionStorage.removeItem("workflowId");
                document.getElementById('resumeWorkflowPopup').visible = false;
              }

              // On resume workflow clearing the workflow conflict
              // popup, resuming the workflow and
              var resumeWorkflow = document
                .getElementById('resumeWorkflow');
              resumeWorkflow.disabled = false;
              resumeWorkflow.onclick = function() {

                document
                  .querySelector('#resumeWorkflowPopup').visible = false;

                // Resume workflow and load workflow screen
                var resumeWorkflowCall = $.ajax({
                  url: "/treasury/comet/resumeWorkflow",
                  data: wfDataParam,
                  type: "POST",
                  dataType: "json"
                });
                resumeWorkflowCall
                  .done(function(resumeData) {
                    sessionStorage.setItem("workflowId", resumeData.resultList[0]['workflowId']);
                    sessionStorage.setItem("workflowState", resumeData.resultList[0]['workflowState']);
                    window.open('/treasury/lure/liquidityWorkflow');
                  });
              }

              // On dismiss popup clear and hide the workflow
              // popup
              var dismissMessage = document
                .getElementById('dismissMessage');
              dismissMessage.onclick = function() {
                _dismissPopup();
              }

              // Set properties of workflow conflict popup and
              // reveal
              var resumeWorkflowMessage = document
                .getElementById("resumeWorkflowMessage");
              var subject = document.getElementById("subject");
              resumeWorkflowMessage.value = conflictMessage;
              subject.value = conflictSubject;

              document.querySelector('#resumeWorkflowPopup')
                .reveal({
                  'title': 'Workflow Conflict',
                  'modal': true,
                });
              // When the workflow is started by a different user
              // and it is IN_PROGRESS for more then 60 mins , it
              // is considered stale.
            } else if (workflowState == 'FAILED_WORKFLOW') {


              // On Go Back clear and hide the workflow popup
              var failedWorkflowGoBack = document
                .getElementById('failedWorkflowGoBack');
              failedWorkflowGoBack.onclick = function() {
                // Used to prevent the refresh of page
                event.preventDefault();
                document
                  .querySelector('#failedWorkflowPopup').visible = false;
              }

              // On Confirmation Cancel the workflow, start new
              // workflow and reload the workflow screen
              var failedConfirmCancel = document
                .getElementById('failedConfirm');
              failedConfirmCancel.disabled = false;
              failedConfirmCancel.onclick = function() {
                document
                  .querySelector('#failedWorkflowPopup').visible = false;
                treasury.liquidity.workflowUtils.workflowExecute('CANCELLED', 'Workflow has been cancelled.', null);
                $('#workflowId').remove();
                sessionStorage.removeItem("workflowId");
                _loadWorkflowLanding();
              }

              // Set different properties of workflow conflict
              // properties and reveal it
              var failedWorkflowMessage = document
                .getElementById("failedWorkflowMessage");
              var subject = document.getElementById("subject");
              failedWorkflowMessage.value = conflictMessage;
              subject.value = conflictSubject;

              document.querySelector('#failedWorkflowPopup')
                .reveal({
                  'title': 'Workflow Conflict',
                  'modal': true,
                });
              // When the workflow is started by different user
              // and its not stale, then no user can cancel the
              // workflow or start a new workflow
            } else if (workflowState == 'LOCKED_WORKFLOW') {


              // On Go Back click clear and hide the workflow
              // conflict popup
              var lockedWorkflowGoBack = document
                .getElementById('lockedWorkflowGoBack');

              lockedWorkflowGoBack.onclick = function() {
                event.preventDefault();
                document
                  .querySelector('#lockedWorkflowPopup').visible = false;
              }

              // Set properties of the workflow popup and reveal
              // it
              var lockedWorkflowMessage = document
                .getElementById("lockedWorkflowMessage");
              var subject = document.getElementById("subject");
              lockedWorkflowMessage.value = conflictMessage;
              subject.value = conflictSubject;
              document.querySelector('#lockedWorkflowPopup')
                .reveal({
                  'title': 'Workflow Conflict',
                  'modal': true,
                });
            }
          });
        }

    /**
     * Helper Method to dismiss conflict popup
     */
    function _dismissPopup() {
      // To avoid reloading the page on clicking the dismiss popup link
      event.preventDefault();
      document.querySelector('#resumeWorkflowPopup').visible = false;
      document.querySelector('#failedWorkflowPopup').visible = false;
      document.querySelector('#lockedWorkflowPop').visible = false;
    }

  function copySearchURL() {
    var url = "/treasury/lure/get-liquidity-report";
    treasury.lure.util.showLoading();
    var parameters = {};
    parameters.dateString = document.getElementById('datePicker').value;
    var liquidityViewOptions = document.getElementsByName('liquidityViewOptions');
    for (var i = 0; i <liquidityViewOptions.length; i++) {
      parameters[liquidityViewOptions[i]['id']] = liquidityViewOptions[i]['checked'];
    }
    generateReportURL(url, parameters, "resultList");
    treasury.lure.util.hideLoading();
  }

  function loadPublishedWorkflowVersions() {
    var dateValue = document.getElementById('datePicker').value;
    var parameters = {};
    parameters.dateString = dateValue;
    parameters.workflowTypeName = "LIQUIDITY";
    var serviceCall = $.ajax({
      url: "/treasury/lure/get-published-workflow-versions",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        workflowVersionsFilter = new window.treasury.filter.SingleSelect(
          'workflowInstanceId', 'workflowVersionsFilter',
          data.workflowVersions, [-1]);
      });

      serviceCall.fail(function(xhr, text, errorThrown) {
        treasury.lure.util.clearGrid(gridDiv);
        treasury.lure.util.hideLoading();
        console.log("Error occurred while retrieving results: " + errorThrown);
      });
  }
})();
