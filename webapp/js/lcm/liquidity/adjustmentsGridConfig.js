"use strict";

var liquidityAdjustmentsGrid;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.liquidity = window.treasury.liquidity || {};
  window.treasury.liquidity.adjustments = {
    loadAdjustmentsReport: _loadAdjustmentsReport
  };

  function _loadAdjustmentsReport(data) {
    document.getElementById('liquidityAdjustmentContent').removeAttribute("hidden");
    document.getElementById('liquidityAdjustmentsGrid').removeAttribute("hidden");
    treasury.lure.util.clearGrid("liquidityAdjustmentsGrid");
    liquidityAdjustmentsGrid = new dportal.grid.createGrid(
      $("#liquidityAdjustmentsGrid"), data,
      adjustmentsColumnConfigs(),
      adjustmentsGridOptions());
  }

  function adjustmentsColumnConfigs() {
    return [{
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legal_entity",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "exposureCounterparty",
      type: "text",
      name: "Exposure Counterparty",
      field: "exposure_counterparty",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "book",
      type: "text",
      name: "Book",
      field: "book",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "businessUnit",
      type: "text",
      name: "Business Unit",
      field: "business_unit",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreement_type",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "adjustmentType",
      type: "text",
      name: "Adjustment Type",
      field: "adjustment_type",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "description",
      type: "text",
      name: "Description",
      field: "description",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "amount",
      type: "number",
      name: "Amount (USD)",
      field: "amount",
      sortable: true,
      formatter: dpGrid.Formatters.Float,
      headerCssClass: "aln-rt b"
    }, {
      id: "startDate",
      type: "text",
      name: "Start Date",
      field: "start_date",
      sortable: true,
      formatter: treasury.formatters.dateDDMMMYYYYDashed,
      headerCssClass: "aln-rt b"
    }, {
      id: "endDate",
      type: "text",
      name: "End Date",
      field: "end_date",
      sortable: true,
      formatter: treasury.formatters.dateDDMMMYYYYDashed,
      headerCssClass: "aln-rt b"
    }, {
      id: "source",
      type: "text",
      name: "Source",
      field: "source",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "publishedTime",
      type: "text",
      name: "Published Time",
      field: "published_time",
      sortable: true,
      formatter: treasury.formatters.dateDDMMMYYYYDashed,
      headerCssClass: "aln-rt b"
    }, {
      id: "comment",
      type: "text",
      name: "Comment",
      field: "comment",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "user",
      type: "text",
      name: "User",
      field: "user",
      sortable: true,
      headerCssClass: "aln-rt b"
    }];
  }

  function adjustmentsGridOptions() {
      return {
        enableMultilevelGrouping: false,
        autoHorizontalScrollBar: true,
        nestedTable: false,
        expandTillLevel: -1,
        highlightRow: false,
        sortList: [{
          columnId: "legal_entity",
          sortAsc: true
        }],
        summaryRow: true,
        editable: true,
        asyncEditorLoading: false,
        autoEdit: false,
        applyFilteringOnGrid: true,
        //useAvailableScreenSpace: true,
        exportToExcel: false,
      };
  }
})();
