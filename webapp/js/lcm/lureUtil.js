"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.lure = window.treasury.lure || {};
  window.treasury.lure.util = {
    showLoading: _showLoading,
    hideLoading: _hideLoading,
    showErrorMessage: _showErrorMessage,
    setupDateFilter: _setupDateFilter,
    clearGrid: _clearGrid,
    getFormattedMessage: _getFormattedMessage,
    resizeCanvasOnGridChange: _resizeCanvasOnGridChange,
    loadBusinessUnits: _loadBusinessUnits,
  };

  function _showLoading() {
    document.getElementById("lureLoading").show();
  }

  function _hideLoading() {
    document.getElementById("lureLoading").hide();
  }

  function _loadBusinessUnits() {
    return $.ajax({
      url: "/lure/data/load-business-unit-filter",
      dataType: "json"
    });
  }

  function _showErrorMessage(title, message, closeWindow) {

    var errorDialog = document.getElementById("errorMessage");

    errorDialog.reveal({
      'title': title,
      'content': "<center><strong>" + message +
        "</strong></center>",
      'modal': true,
      'buttons': [{
        'html': 'OK',
        'callback': function() {
          this.visible = false;
          if(closeWindow) {
              window.close();
          }
        },
        'position': 'right'
      }]
    });
  }

  function _clearGrid(gridId) {
    treasury.common.grid.clearGrid(gridId);
    if ($("#" + gridId).prev().prev().is("p") ||
      $("#" + gridId).prev().prev().is("center")) {
      $("#" + gridId).prev().prev().remove();
    } else if ($("#" + gridId).prev().is("p") ||
      $("#" + gridId).prev().is("center")) {
      $("#" + gridId).prev().remove();
    }
    if($("#" + gridId).next().is("p")) {
      $("#" + gridId).next().remove();
    }
    var groupingHeader = document.getElementById(gridId + '-drop-element-holder');
    if (groupingHeader != null) {
      groupingHeader.innerHTML = "";
    }
  }

  function _setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
    dateFormat) {
    dateDiv.datepicker({
      dateFormat: dateFormat,
      changeMonth: true,
      changeYear: true,
      minDate: minDate,
      maxDate: maxDate,
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
    }).datepicker("setDate", date);
    dateDiv.change(function() {
      validateDate(dateDiv.val(), {
        errorDiv: errorDiv,
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: dateFormat,
      });
    });
  }

  function _getFormattedMessage(message, className) {
	  return "<center><p class='arc-message" +
      (className == '' ? className : '--' + className) +
      "' style='width: 30%;'>" + message + "</p></center>";
  }

  function _resizeCanvasOnGridChange(gridName, gridId, leftPanelId) {
    var leftPanel = document.getElementById(leftPanelId);
    if (leftPanel != null) {
      leftPanel.addEventListener('stateChange', function() {
        var parentArcLayout = document.getElementById('parentArcLayout');
        parentArcLayout.setLayout();
        gridName.resizeCanvas();
      });
    } else {
      gridName.resizeCanvas();
    }
    if (gridId != undefined) {
      $('#' + gridId).height($('#' + gridId).height() + 11);
    }

    if(typeof liquidityAdjustmentsGrid !== 'undefined' && liquidityAdjustmentsGrid != null) {
      liquidityAdjustmentsGrid.resizeCanvas();
    }
  }
})();
