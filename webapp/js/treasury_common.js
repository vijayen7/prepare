/* Common JS being used in Treasury reports built using modern UI themes */
"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.common = window.treasury.common || {};

  window.treasury.common.grid = {
    clearGrid: _clearGrid,
    showGridInfo: _showGridInfo
  };

  function _clearGrid(gridId) {
    if (document.getElementById(gridId + "Info")) {
      document.getElementById(gridId + "Info").innerHTML = "";
      document.getElementById(gridId + "Info").style.display = "none";
    }

    if (document.getElementById(gridId)) {
      document.getElementById(gridId).style.display = "block";
      document.getElementById(gridId).innerHTML = "";
      document.getElementById(gridId).style.height = "0px";
    }
    if (document.getElementById(gridId + "-header")) {
      document.getElementById(gridId + "-header").innerHTML = "";
      document.getElementById(gridId + "-header").style.height = "0px";
      document.getElementById(gridId + "-header").style.display = "none";
    }
  }

  function _showGridInfo(gridId, message) {
    document.getElementById(gridId).style.display = "none";

    document.getElementById(gridId + "Info").style.display = "block";
    document.getElementById(gridId + "Info").innerHTML = message;
  }
})();
