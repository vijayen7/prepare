/**
 * MSB class (prototypal OO).
 **/
(function($){
    $.extend(true, window,
    {
        "treasury" :
        {
            "MSB" : TreasuryMSB
        }
    });

    /**
     * MSB class
     */
    /*
     * paramString - used in the query params during ajax requests. eg: "cpeFamilyIds"
     * divId - msb div id.
     * msbList - [[-1, "All"], [2, "Liquid"], ...]
     * msbMap - { "-1" : "All", "2", "Liquid", ... }
     * selectedIds - [-1] or [1,2] format.
     */
    function TreasuryMSB(paramString, divId, msbList, selectedIds)
    {
        this.paramString = paramString;
        this.divId = divId;
        this.msbList = msbList;
        this.selectedIds = selectedIds;

        this.msb$ = $("#" + divId);
        this.clearMsb$ = $("#spanClear" + divId + " > a");

        /*
         * Temporarily creating filterMap till Prop#97139 is done.
         */
        var filterMap = {};
        for (var i = 0; i < msbList.length; i++) {
            filterMap[msbList[i][0]] = msbList[i][1];
        }
        this.filterMap = filterMap;

        // Calling initialize method.
        this.initialize();
    }

    /*
     * resets selectedIds, and dom's text & tooltip
     */
    TreasuryMSB.prototype.reset = function() {
        this.selectedIds = [-1];

        this.updateMSBText();
    };

    /*
     * Update selectedIds
     * This is currently useful only during drill throughs where selectedIds are
     * to be updated programmatically.
     */
    TreasuryMSB.prototype.updateSelectedIds = function(newSelectedIds) {
        if (newSelectedIds !== undefined) {
            this.selectedIds = newSelectedIds;
        }
    };

    /*
     * This is need till Prop#97139 is done.
     */
    TreasuryMSB.prototype.updateMSBText = function() {
        var resultText = "All"; // default
        var toolTipText = "All"; // default
        if (this.selectedIds.indexOf(-1) < 0) {
            resultText = this.filterMap[this.selectedIds[0]] + (this.selectedIds.length > 1 ? "..." : "");
            toolTipText = "";
            for (var i = 0; i < this.selectedIds.length; i++) {
                toolTipText = toolTipText + this.filterMap[this.selectedIds[i]] + ", ";
            }

            toolTipText = toolTipText.replace(/, $/, "");
        }

        this.msb$.html(resultText);
        this.msb$.attr("title", toolTipText);
        this.msb$.tooltip();
    };

    TreasuryMSB.prototype.onSelectionMSB = function() {
        var selectedValues = this.msb$.html();

        // When user selects a few items and doesn't add it to the selected list
        // then the html of the element is set to "Select..."
        if (selectedValues == "Select...")
        {
            alert("No item was selected.");
            // Resetting the MSB
            this.reset();
            return;
        }

        this.msb$.attr("title", selectedValues);
        this.msb$.tooltip();

        var selectedItems = this.msb$.html();
        if (selectedItems.length > 15) {
            this.msb$.html(selectedItems.substring(0,15) + "...");
        }
        else {
            this.msb$.html(selectedItems);
        }

        this.selectedIds = getMSBSelectedItems(this.divId);
    };

    /*
     * Registers onSelection event of MSB.
     * Updates the MSB with the selected ids.
     */
    TreasuryMSB.prototype.register = function() {
        // 'this' is not available inside functions declared within functions
        // as 'this' would point to inner function objects.
        var that = this;

        registerMSBCollection(
                this.divId,
                this.msbList,
                (this.selectedIds === undefined || this.selectedIds.indexOf(-1) >= 0) ? new Array() : this.selectedIds,
                function() { that.onSelectionMSB(); },
                true);
    };

    /*
     * reset, binds 'Clear' button event to reset, registers msb with on
     * selection callback method.
     */
    TreasuryMSB.prototype.initialize = function() {
        // 'this' is not available inside functions declared within functions
        // as 'this' would point to inner function objects.
        var that = this;

        // Resets the MSB
        // this.reset();

        // Binds 'Clear' button's click event.
        this.clearMsb$.click(
            function()
            {
                onMSBClearClick(that.divId);
                that.reset();
            });

        this.register();
        this.updateMSBText();
    };

    TreasuryMSB.prototype.serializedSelectedValues = function() {
        var selectedValues = this.msb$.html();

        // When user selects a few items and doesn't add it to the selected list
        // then the html of the element is set to "Select..."
        if (selectedValues == "Select...")
        {
            return;
        }
        return selectedValues;
    };

    /*
     * returns a csv formatted string of selected ids.
     */
    TreasuryMSB.prototype.serializedSelectedIds = function() {
        return this.selectedIds === undefined ? "-1" : this.selectedIds.join(",");
    };
})(jQuery);