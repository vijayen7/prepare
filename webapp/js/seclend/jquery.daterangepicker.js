/**
 * --------------------------------------------------------------------
 * jQuery-Plugin "daterangepicker.jQuery.js"
 * by Scott Jehl, scott@filamentgroup.com
 * reference article: http://www.filamentgroup.com/lab/update_date_range_picker_with_jquery_ui/
 * demo page: http://www.filamentgroup.com/examples/daterangepicker/
 *
 * Copyright (c) 2010 Filament Group, Inc
 * Dual licensed under the MIT (filamentgroup.com/examples/mit-license.txt) and GPL (filamentgroup.com/examples/gpl-license.txt) licenses.
 *
 * Dependencies: jquery, jquery UI datepicker, date.js, jQuery UI CSS Framework
 *
 * 12.15.2010 Made some fixes to resolve breaking changes introduced by jQuery UI 1.8.7
 * --------------------------------------------------------------------
 *
 * MODIFICATIONS TO SOURCE
 *
 * MINOR CHANGES/ ADDITIONS
 * # Added functionality to get dates from the input
 * # Added styling for the input elements such that they fit the dates
 * # Added option for configuring if the input field should be automatically updated on each selection
 * # Made 'change' event of the input field to be triggered when using date picker to select dates
 * # Made changes to show the last used picker always
 * # Corrected bug in the logic used to set dates when arrows are used
 * # Added option for triggering date range picker via an icon
 * # Fixed bugs that occur when 'constrainDates' is set to true
 * # Made the datepickers to show dropdowns for choosing month and year by default
 * # Made 'presetRanges' to be extendable
 * # Changed the default value of 'constrainDates' to true
 * # Removed 'All Dates Before' and 'All Dates After' presets
 * # Added default values for 'minDate' and 'maxDate' properties of datepickerOptions
 * # Removed 'earliestDate' and 'latestDate' properties from Options as these are specified by the 'minDate' and 'maxDate' properties of datepickerOptions
 * # Added support to get the last used preset or preset range
 * # Added support to show the preset range text instead of date when using preset ranges
 *
 * MAJOR CHANGES/ ADDITIONS
 * # Added functionality to dynamically update the settings of the daterangepicker such as minDate, maxDate etc.
 * # Modified the daterangepicker plugin so that it can be initialized on the same component multiple times. (The previous initialization is destroyed)
 *
 * @author: kumarse
 */

 /*
  This file with its css was not getting served.
  Copied from /arc-portal/code/modules/webapps/static/src/main/webapp/css/jquery/ui.daterangepicker.css
 */
jQuery.fn.daterangepicker = function(settings){

        var rangeInput = jQuery(this);
        var otherArgs = Array.prototype.slice.call(arguments, 1);
        var KEY_NAME = 'daterangepicker';
        /*
        * Check if daterangepicker has already been successfully initialized on this component.
        * If so destroy it and create a new one (or) updates its settings
        */
        var updaterFunc = rangeInput.data(KEY_NAME);
        if(updaterFunc)
        {
            if(typeof settings === "string") {
                var ret = updaterFunc(Array.prototype.slice.call(arguments));
                if(settings.indexOf("get") === 0) {
                    return ret;
                }
                return this;
            }
            else {
                updaterFunc(["destroy"]);
            }
        }

        //defaults
        var options = jQuery.extend(true, {
                extendPresetRanges: true,
                icon: null, // The icon used to trigger the date range picker
                rangeStartTitle: 'Start Date',
                rangeEndTitle: 'End Date',
                nextLinkText: 'Next',
                prevLinkText: 'Prev',
                doneButtonText: 'Done',
                rangeSplitter: ':',
                constrainDates: true,
                closeOnSelect: true, //if a complete selection is made, close the menu
                arrows: false,
                appendTo: 'body',
                onClose: function(){},
                onOpen: function(){},
                onChange: undefined, // Triggered whenever there is a change in the input element value
                onSelect: undefined, // Triggered when a complete selection has been made using the date range picker
                autoupdateField: true, //if date selection during date range selection should be immediately reflected in the input field or once 'Done' button is clicked
                autoAdjustWidth: false,
                showTextForPresetRanges: false,
                alwaysShowRange: false,
                highlight: {
                    onDate: false,
                    styleForDate: "2px solid #BE8E8E",
                    onDateRange: false,
                    styleForDateRange: "2px solid #FFCD73",
                    onPresetRange: false
                },
                tooltip: {
                    enable: false,
                    formatter: null
                }
        }, settings);

        options.presets = options.presets || {specificDate: 'Specific Date', dateRange: 'Date Range'};

        //presetRanges: array of objects for each menu preset.
        //Each obj must have text, dateStart, dateEnd. dateStart, dateEnd accept date.js string or a function which returns a date object
        var presetRanges = [
            {
                text : 'Latest',
                dateStart : function() {
                    return _maxDate;
                },
                dateEnd : function() {
                    return _maxDate;
                }
            },
            {
                text : 'Latest 7 Days',
                dateStart : function() {
                    return new Date(_maxDate).add(-6).days();
                },
                dateEnd : function() {
                    return _maxDate;
                }
            },
            {
                text : 'Latest 30 Days',
                dateStart : function() {
                    return new Date(_maxDate).add(-29).days();
                },
                dateEnd : function() {
                    return _maxDate;
                }
            },
            {
                text : 'Month to Date',
                dateStart : function() {
                    return Date.parse('today').moveToFirstDayOfMonth();
                },
                dateEnd : 'today'
            },
            {
                text: 'Year to Date',
                dateStart: function() {
                    var x= Date.parse('today'); x.setMonth(0); x.setDate(1); return x;
                },
                dateEnd: 'today'
            }
        ];

        //preset ranges from options
        options.presetRanges = (settings && settings.presetRanges) ? (options.extendPresetRanges ? settings.presetRanges.concat(presetRanges) : settings.presetRanges) : presetRanges;
        // Following options should be allowed only for single input
        options.showTextForPresetRanges = rangeInput.length == 1 && options.showTextForPresetRanges;
        options.autoAdjustWidth = rangeInput.length == 1 && options.autoAdjustWidth;

        //custom datepicker options, extended by options
        var datepickerOptions = {
                onSelect: function(dateText, inst) {
                    //The other picker must be constrained based on the selection in the current picker.
                    //This must be done before everything else.
                    //Otherwise the date in the other picker may not be set correctly due to the minDate and maxDate settings
                    $(this).trigger('constrainOtherPicker');

                    if(rp.find('.ui-daterangepicker-specificDate').is('.ui-state-active')) {
                            lastSelection.type = lastSelection.text = options.presets.specificDate;
                            closeOnSelect = options.closeOnSelect;
                            rp.find('.range-end').datepicker('setDate', rp.find('.range-start').datepicker('getDate') );
                    }
                    else if(rp.find('.ui-daterangepicker-dateRange').is('.ui-state-active')) {
                        closeOnSelect = false;
                        lastSelection.type = lastSelection.text = options.presets.dateRange;
                    }

                    if(!rp.find('li.ui-state-active').is('.ui-daterangepicker-dateRange') || options.autoupdateField) {
                        updateField();
                    }
                    //if closeOnSelect is true
                    if(closeOnSelect){
                        if(rp.find('.ui-daterangepicker-specificDate').is('.ui-state-active')){
                            rangeInput.trigger('done');
                        }
                        if(!rp.is(':animated') ){
                                hideRP();
                        }
                    }
                },
                defaultDate: +0,
                dateFormat: 'yy-mm-dd', // date formatting. Available formats: http://docs.jquery.com/UI/Datepicker/%24.datepicker.formatDate
                changeMonth: true,
                changeYear: true,
                minDate: Date.parseExact('1970-01-01','yyyy-MM-dd'),
                maxDate: +0
        };

         //datepicker options from options
        options.datepickerOptions = (settings) ? jQuery.extend(datepickerOptions, settings.datepickerOptions) : datepickerOptions;
        var _maxDate = parseDatepickerDate(options.datepickerOptions.maxDate, options.datepickerOptions.dateFormat) || Date.today(),
            triggerEvents = true;

        //function to format a date string
        function fDate(date) {
           if(!date.getDate()){return '';}
           var day = date.getDate();
           var month = date.getMonth();
           var year = date.getFullYear();
           month++; // adjust javascript month
           var df = options.datepickerOptions.dateFormat;
           return jQuery.datepicker.formatDate( df, date );
        }

        function updateWidth(state) {
            if(state === 'wide') {
                if(rangeInput.parent().hasClass('ui-daterangepicker-arrows')) {
                    rangeInput.parent().removeClass('narrow').addClass('wide') ;
                }
                else {
                    rangeInput.removeClass('narrow').addClass('wide') ;
                }
            }
            else {
                if(rangeInput.parent().hasClass('ui-daterangepicker-arrows')) {
                    rangeInput.parent().removeClass('wide').addClass('narrow');
                }
                else {
                    rangeInput.removeClass('wide').addClass('narrow') ;
                }
            }
        }

        // Used to indicate if the date range picker can be hid on selection or not
        var closeOnSelect = options.closeOnSelect;
        // Used to highlight (make active) the last used preset each time the date range picker is triggered
        var lastUsedPreset, lastUsedPresetRange;
        // Used to remember the last preset or preset range used by the user
        var lastSelection = {};
        // Convenience flag used to determine if any of the highlight options is turned on
        var highlight = options.highlight.onDate || options.highlight.onDateRange || options.highlight.onPresetRange;

        /*
         * Utility method to highlight the input
         *
         * -- If highlight preset ranges is true and the selection is a preset range,
         * highlights the input using the style in the preset range
         *
         * -- If a date range is selected (either from the pickers or preset ranges)
         * and  highlight on date range is true, highlights the input using the
         * style for date range selection (or) the style specified in the preset range
         *
         * -- If a date is selected and highlight on date is true, highlights
         * the input using the style for date selection (or) the style specified
         * in the preset range
         */
        function highlightInput(startDate, endDate) {
            var borderCss, activePresetRange = {};

            if(lastSelection.type == "PRESET_RANGE") {
                activePresetRange.highlightStyle = lastUsedPresetRange.data('highlightStyle');
            }

            if(lastSelection.type == "PRESET_RANGE" && options.highlight.onPresetRange) {
                borderCss = activePresetRange.highlightStyle;
            }
            else if(startDate != endDate || lastSelection.text == options.presets.dateRange) {
                if(options.highlight.onDateRange) {
                    borderCss = activePresetRange.highlightStyle || options.highlight.styleForDateRange;
                }
            }
            else {
                if(options.highlight.onDate) {
                    borderCss = activePresetRange.highlightStyle || options.highlight.styleForDate;
                }
            }
            rangeInput.css("border", borderCss || "none");
        }

        function updateField() {
            rangeInput.trigger('focus');
            var rangeA = fDate( rp.find('.range-start').datepicker('getDate') );
            var rangeB = fDate( rp.find('.range-end').datepicker('getDate') );

            if (options.showTextForPresetRanges && lastSelection.type == "PRESET_RANGE") {
                rangeInput.val(lastSelection.text);
                rangeInput.attr("disabled", true);
            }
            else {
                //send back to input or inputs
                if(rangeInput.length == 2){
                    rangeInput.eq(0).val(rangeA);
                    rangeInput.eq(1).val(rangeB);
                }
                else{
                    var alwaysShowRange = (lastSelection.text == options.presets.dateRange && options.alwaysShowRange)
                        || (lastSelection.type == "PRESET_RANGE" && lastUsedPresetRange.data("alwaysShowRange"));

                    rangeInput.val((rangeA != rangeB || alwaysShowRange) ? rangeA+' '+ options.rangeSplitter +' '+rangeB : rangeA );
                }
                rangeInput.attr("disabled", false);
            }

            if(highlight) {
                highlightInput(rangeA, rangeB);
            }

            if(options.tooltip.enable) {
                rangeInput.attr("title", options.tooltip.formatter(rangeA, rangeB, dportal.cloneObject(lastSelection)));
            }

            //The jQuery change event is triggered only when the input element loses focus. But when using the date picker to make selection,
            //the input field never gets focus. Thus change event is not triggered. Hence triggering it manually.
            triggerEvents && rangeInput.trigger('change');
        }

        //build picker
        var rp = jQuery('<div class="ui-daterangepicker ui-widget ui-helper-clearfix ui-widget-content ui-corner-all"></div>');
        var rpPresets = (function(){
                var ul = jQuery('<ul class="ui-widget-content"></ul>').appendTo(rp);
                jQuery.each(options.presetRanges,function(){
                        var pr$ = jQuery('<li class="ui-daterangepicker-'+ this.text.replace(/ /g, '') +' ui-corner-all"><a href="#">'+ this.text +'</a></li>')
                        .data('dateStart', this.dateStart)
                        .data('dateEnd', this.dateEnd);
                        if(this.highlightStyle) { pr$.data('highlightStyle', this.highlightStyle); }
                        if(this.alwaysShowRange) { pr$.data('alwaysShowRange', this.alwaysShowRange); }
                        pr$.appendTo(ul);
                });
                var x=0;
                jQuery.each(options.presets, function(key, value) {
                        jQuery('<li class="ui-daterangepicker-'+ key +' preset_'+ x +' ui-helper-clearfix ui-corner-all"><span class="ui-icon ui-icon-triangle-1-e"></span><a href="#">'+ value +'</a></li>')
                        .appendTo(ul);
                        x++;
                });

                ul.find('li').hover(
                                function(){
                                        jQuery(this).addClass('ui-state-hover');
                                },
                                function(){
                                        jQuery(this).removeClass('ui-state-hover');
                                })
                        .click(function(){
                                rp.find('.ui-state-active').removeClass('ui-state-active');
                                jQuery(this).addClass('ui-state-active');
                                clickActions(jQuery(this),rp, rpPickers, doneBtn);
                                return false;
                        });
                return ul;
        })();

        // jQuery datepicker divs
        var rpPickers = jQuery('<div class="ranges ui-widget-header ui-corner-all ui-helper-clearfix"><div class="range-start"><span class="title-start">Start Date</span></div><div class="range-end"><span class="title-end">End Date</span></div></div>').appendTo(rp);
        rpPickers.find('.range-start, .range-end')
                .datepicker(options.datepickerOptions);

        rpPickers.find('.range-start, .range-end')
                .bind('constrainOtherPicker', function(){
                        if(options.constrainDates){
                                //constrain dates
                                if($(this).is('.range-start')){
                                        rp.find('.range-end').datepicker( "option", "minDate", $(this).datepicker('getDate'));
                                }
                                else{
                                        rp.find('.range-start').datepicker( "option", "maxDate", $(this).datepicker('getDate'));
                                }
                        }
                })
                .trigger('constrainOtherPicker');

        var doneBtn = jQuery('<button class="btnDone ui-state-default ui-corner-all">'+ options.doneButtonText +'</button>')
        .click(function(){
                if(!options.autoupdateField) {
                    updateField();
                }
                rangeInput.trigger('done');
                hideRP();
        })
        .hover(
                        function(){
                                jQuery(this).addClass('ui-state-hover');
                        },
                        function(){
                                jQuery(this).removeClass('ui-state-hover');
                        }
        )
        .appendTo(rpPickers);

        function resetMinMax() {
            rp.find('.range-end').datepicker( "option", "minDate", options.datepickerOptions.minDate);
            rp.find('.range-start').datepicker( "option", "maxDate", options.datepickerOptions.maxDate);
        }

        function setDateInPickers(dateStart, dateEnd) {
            rp.find('.range-start').datepicker('setDate', dateStart).find('.ui-datepicker-current-day').trigger('click');
            rp.find('.range-end').datepicker('setDate', dateEnd).find('.ui-datepicker-current-day').trigger('click');
        }

        //preset menu click events
        function clickActions(el, rp, rpPickers, doneBtn){
                resetMinMax();
                if(el.is('.ui-daterangepicker-specificDate')){
                        //Specific Date (show the "start" calendar)
                        doneBtn.hide();
                        lastUsedPreset = el;
                        rpPickers.show();
                        rp.find('.title-start').text( options.presets.specificDate );
                        rp.find('.range-start').css('opacity',1).show(400);
                        rp.find('.range-end').css('opacity',0).hide(400);
                        setTimeout(function(){doneBtn.fadeIn();}, 400);
                }
                else if(el.is('.ui-daterangepicker-dateRange')){
                        //Specific Date range (show both calendars)
                        doneBtn.hide();
                        lastUsedPreset = el;
                        rpPickers.show();
                        rp.find('.title-start').text(options.rangeStartTitle);
                        rp.find('.title-end').text(options.rangeEndTitle);
                        rp.find('.range-start').css('opacity',1).show(400);
                        rp.find('.range-end').css('opacity',1).show(400);
                        setTimeout(function(){doneBtn.fadeIn();}, 400);
                }
                else {
                        //custom date range specified in the options (no calendars shown)
                        closeOnSelect = true;
                        lastUsedPresetRange = el;
                        lastSelection.type = "PRESET_RANGE";
                        lastSelection.text = el.text();

                        var dateStart = (typeof el.data('dateStart') == 'string') ? Date.parse(el.data('dateStart')) : el.data('dateStart')();
                        var dateEnd = (typeof el.data('dateEnd') == 'string') ? Date.parse(el.data('dateEnd')) : el.data('dateEnd')();
                        rp.find('.range-start').datepicker('setDate', dateStart).trigger('constrainOtherPicker');
                        rp.find('.range-end').datepicker('setDate', dateEnd).trigger('constrainOtherPicker');
                        updateField();
                        if(!rp.is(':animated') ){
                            hideRP();
                        }
                        triggerEvents && rangeInput.trigger('done');
                }

                return false;
        }

        // add arrows (only available on one input)
        if(options.arrows && rangeInput.size()==1){
                var prevLink = jQuery('<a href="#" class="ui-daterangepicker-prev ui-corner-all" title="'+ options.prevLinkText +'"><span class="ui-icon ui-icon-circle-triangle-w">'+ options.prevLinkText +'</span></a>');
                var nextLink = jQuery('<a href="#" class="ui-daterangepicker-next ui-corner-all" title="'+ options.nextLinkText +'"><span class="ui-icon ui-icon-circle-triangle-e">'+ options.nextLinkText +'</span></a>');

                var classToSet = (options.autoAdjustWidth) ? "narrow" : "wide";
                jQuery(this)
                .addClass('ui-rangepicker-input ui-widget-content')
                .wrap('<span class="ui-daterangepicker-arrows '+classToSet+' ui-widget ui-widget-header ui-corner-all"></span>')
                .before( prevLink )
                .before( nextLink )
                .parent().find('a').click(function(){
                        var dateA = rpPickers.find('.range-start').datepicker('getDate');
                        var dateB = rpPickers.find('.range-end').datepicker('getDate');
                        var diff = Math.abs( new TimeSpan(dateA - dateB).getTotalMilliseconds() ) + 86400000; //difference plus one day
                        if(jQuery(this).is('.ui-daterangepicker-prev')){ diff = -diff; }

                        closeOnSelect = true;
                        /* In case of prev, the start date must be set before setting the end date
                        * In case of next, the end date must be set before setting the start date.
                        * Otherwise the minDate,maxDate constraints will be violated and the dates will not be set properly
                        */
                        var pickers=rpPickers.find('.range-start, .range-end'),
                            i= jQuery(this).is('.ui-daterangepicker-prev') ? 0 : 1,
                            count = -1;

                        while(++count < 2) {
                            var thisDate = jQuery(pickers[i]).datepicker( "getDate");
                            if(thisDate == null){return false;}
                            jQuery(pickers[i]).datepicker( "setDate", thisDate.add({milliseconds: diff}) ).find('.ui-datepicker-current-day').trigger('click');
                            i = (i+1)%2;
                        }
                        return false;
                })
                .hover(
                        function(){
                                jQuery(this).addClass('ui-state-hover');
                        },
                        function(){
                                jQuery(this).removeClass('ui-state-hover');
                        });

                var riContain = rangeInput.parent();
        }


        //show, hide, or toggle rangepicker
        function showRP(){
                if(rp.data('state') == 'closed'){
                        positionRP();
                        if(lastUsedPreset) {
                            lastUsedPreset.addClass('ui-state-active');
                        }
                        rp.fadeIn(300).data('state', 'open');
                        options.onOpen();
                }
        }

        function hideRP(){
                if(rp.data('state') == 'open'){
                    rp.fadeOut(300).data('state', 'closed');
                    if(lastUsedPreset) {
                        // In case preset ranges are used, they should not be
                        // active (highlighted) when the menu is shown again.
                        // Instead the last used preset should be made active
                        // when showing again
                        rpPresets.find('.ui-state-active').removeClass('ui-state-active');
                    }
                    options.onClose();
                }
        }

        function toggleRP(){
                if( rp.data('state') == 'open' ){ hideRP(); }
                else { showRP(); }
        }

        function positionRP(){
                //var relEl = riContain || rangeInput; //if arrows, use parent for offsets
                var relEl = triggerElement;
                var riOffset = relEl.offset(),
                        side = 'left',
                        val = riOffset.left,
                        offRight = jQuery(window).width() - val - relEl.outerWidth();

                if(val > offRight){
                        side = 'right', val =  offRight;
                }

                rp.parent().css(side, val).css('top', riOffset.top + relEl.outerHeight());
        }

        //toggle rangepicker visibility
        var triggerElement = jQuery(this);
        if(options.icon) {
            triggerElement = options.icon;
        }
        triggerElement.click(function(){
                toggleRP();
                return false;
        });

        //hide em all
        rpPickers.hide().find('.range-start, .range-end, .btnDone').hide();

        rp.data('state', 'closed');

        //Fixed for jQuery UI 1.8.7 - Calendars are hidden otherwise!
        rpPickers.find('.ui-datepicker').css("display","block");

        //inject rp
        jQuery(options.appendTo).append(rp);

        //wrap and position
        rp.wrap('<div class="ui-daterangepickercontain"></div>');

        jQuery(document).click(function(){
                if (rp.is(':visible')) {
                        hideRP();
                }
        });

        jQuery.fn.restoreDateFromData = function(){
                if(jQuery(this).data('saveDate')){
                        jQuery(this).datepicker('setDate', jQuery(this).data('saveDate')).removeData('saveDate');
                }
                return this;
        }

        jQuery.fn.saveDateToData = function(){
                if(!jQuery(this).data('saveDate')){
                        jQuery(this).data('saveDate', jQuery(this).datepicker('getDate') );
                }
                return this;
        }

        // Set up CSS and event listeners for input element
        if(rangeInput.size()==1 && !options.arrows) {
            (!options.autoAdjustWidth)  ? rangeInput.addClass('ui-rangepicker-input daterange wide')
                                        : rangeInput.addClass('ui-rangepicker-input daterange narrow');
        }
        else if(rangeInput.size()==2) {
            rangeInput.addClass('ui-rangepicker-input narrow date');
        }

        //change event fires both when a calendar is updated or a change event on the input is triggered
        rangeInput.bind('change', options.onChange);
        rangeInput.bind('done', options.onSelect);

        if(options.autoAdjustWidth) {
            rangeInput.bind('keyup change', function() {
                        updateWidth((rangeInput.val().trim().length > 10)  ? 'wide' : 'narrow');
                    });
        }

        if(options.tooltip.enable) {
            rangeInput.tooltip(options.tooltip);
        }

        //Capture Dates from input(s) and set it in the pickers
        var inputDateA , inputDateB = Date.parse('today');
        var inputDateAtemp, inputDateBtemp;
        if(rangeInput.size() == 2){
                inputDateAtemp = Date.parse( rangeInput.eq(0).val() );
                inputDateBtemp = Date.parse( rangeInput.eq(1).val() );
                if(inputDateAtemp == null){inputDateAtemp = inputDateBtemp;}
                if(inputDateBtemp == null){inputDateBtemp = inputDateAtemp;}
        }
        else {
                inputDateAtemp = Date.parse( rangeInput.val().split(options.rangeSplitter)[0] );
                inputDateBtemp = Date.parse( rangeInput.val().split(options.rangeSplitter)[1] );
                if(inputDateBtemp == null){inputDateBtemp = inputDateAtemp;} //if one date, set both
        }
        if(inputDateAtemp != null){inputDateA = inputDateAtemp;}
        if(inputDateBtemp != null){inputDateB = inputDateBtemp;}

        rpPickers.find('.range-start').datepicker('setDate', inputDateA);
        rpPickers.find('.range-end').datepicker('setDate', inputDateB);

        var utilityMethods = {
            // Utility functions
            destroy: function() {
                rp.parent().remove();
                /* Remove the onChange and onSelect handlers set by
                the previous initialization of daterangepicker */
                rangeInput.off('change', options.onChange);
                rangeInput.off('done', options.onSelect);
            },

            getDate: function(returnSingleDate) {
                 var date = [];
                 var alwaysShowRange = (lastSelection.text == options.presets.dateRange && options.alwaysShowRange)
                            || (lastSelection.type == "PRESET_RANGE" && lastUsedPresetRange.data("alwaysShowRange"));
                 if(options.showTextForPresetRanges && lastSelection.type == "PRESET_RANGE") {
                     date[0] = $.datepicker.formatDate(
                                     options.datepickerOptions.dateFormat,
                                     rpPickers.find('.range-start').datepicker('getDate'));
                     date[1] = $.datepicker.formatDate(
                                     options.datepickerOptions.dateFormat,
                                     rpPickers.find('.range-end').datepicker('getDate'));

                     return (returnSingleDate && date[0] == date[1] && !alwaysShowRange) ? date.splice(0,1) : date;
                 }
                 else {
                     if(rangeInput.length === 2) {
                         date[0] = rangeInput.eq(0).val();
                         date[1] = rangeInput.eq(1).val();
                     }
                     else if(rangeInput.length === 1) {
                         date = rangeInput.val().split(options.rangeSplitter);
                     }

                     date[0] = date[0].trim();
                     if(!date[1]) {
                         if(!returnSingleDate || alwaysShowRange) {
                             date[1] = date[0];
                         }
                     }
                     else {
                         date[1] = date[1].trim();
                     }
                     return date;
                 }
             },

             getSelection: function() {
                 return lastSelection.text;
             },

            setMinDate: function(minDate) {
                options.datepickerOptions.minDate = parseDatepickerDate(minDate, options.datepickerOptions.dateFormat);
                rp.find('.range-start, .range-end').datepicker('option', 'minDate', options.datepickerOptions.minDate);
            },

            getMinDate: function() {
                return !options.datepickerOptions.minDate ? null : new Date(options.datepickerOptions.minDate);
            },

            setMaxDate: function(maxDate) {
                options.datepickerOptions.maxDate = parseDatepickerDate(maxDate, options.datepickerOptions.dateFormat);
                _maxDate = options.datepickerOptions.maxDate || Date.today();

                rp.find('.range-start, .range-end').datepicker( 'option', 'maxDate', options.datepickerOptions.maxDate);
            },

            getMaxDate: function() {
                return !options.datepickerOptions.maxDate ? null : new Date(options.datepickerOptions.maxDate);
            },

            setDate: function(dateStart, dateEnd, preventEventTriggers) {
                if(typeof dateEnd === "boolean") {
                    preventEventTriggers = dateEnd;
                    dateEnd = dateStart;
                }
                else if(!dateEnd) {
                    dateEnd = dateStart;
                }

                lastSelection.type = lastSelection.text = options.presets.dateRange;
                rpPresets.find('.ui-state-active').removeClass('ui-state-active');

                resetMinMax();
                triggerEvents = !preventEventTriggers;
                setDateInPickers(dateStart, dateEnd);
                if(!triggerEvents && options.autoAdjustWidth) {
                    updateWidth((rangeInput.val().trim().length > 10)  ? 'wide' : 'narrow');
                }
                triggerEvents = true;
                resetMinMax();
            },

            setDatepickerOptions: function(datepickerOptions) {
                $.extend(options.datepickerOptions, datepickerOptions);
                rp.find('.range-start, .range-end').datepicker( "option", datepickerOptions);
            },

            updatePresetRanges: function(presetRanges) {
                var presetRanges$ = rpPresets.children();
                for(var i=0; i<presetRanges.length; i++) {
                    var entry = presetRanges[i];
                    for(var j=0; j<options.presetRanges.length; j++) {
                        if(entry.text == options.presetRanges[j].text) {
                            presetRanges$.eq(j).data('dateStart', entry.dateStart).data('dateEnd', entry.dateEnd);
                            break;
                        }
                    }
                }
            },

            setPresetRange: function(presetRangeText, preventEventTriggers) {
                for(var i=0; i<options.presetRanges.length; i++) {
                    if(presetRangeText == options.presetRanges[i].text) {
                        triggerEvents = !preventEventTriggers;
                        rpPresets.children().eq(i).trigger("click");
                        triggerEvents = true;
                        break;
                    }
                }
            }
        };

        /*
        * A function that enables dynamically updating the settings of the daterangepicker. It is attached as data to the input element.
        * On invoking the daterangepicker plugin on the same component multiple times, this function helps destroy the existing daterangepicker and creates new one
        * It also enables updating only certain settings instead of destroying and creating new picker.
        */
        var updateDatepicker = (function() {
            return function(args) {
                var params = args.slice(1);
                return utilityMethods[args[0]].apply(null, params);
            }
        })();
        rangeInput.data(KEY_NAME, updateDatepicker);

        rp.click(function(){return false;}).hide();
        return this;
}