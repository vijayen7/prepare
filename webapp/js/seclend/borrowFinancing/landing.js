/*jshint jquery:true*/
/*global treasury, ArcMessageHelper, validateDate*/
"use strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.borrowFinancing = window.treasury.borrowFinancing || {};
    window.treasury.borrowFinancing.landing = {
        initialize : _initialize,
        showLoading : _showLoading,
        hideLoading : _hideLoading
    };

    var _borrowFinancingFilterGroup;

    function _showLoading() {
        document.getElementById("borrowFinancingLoading").show();
    }

    function _hideLoading() {
        document.getElementById("borrowFinancingLoading").hide();
    }

    function _initializeFilters(callback) {
        _resetValidation();

        $.when(treasury.loadfilter.legalEntities(),
               treasury.loadfilter.loadSecurityTypes(),
               treasury.loadfilter.defaultDates())
        .done(function(legalEntitiesData, securityTypesData, defaultDatesData) {

            // Init Legal Entity Filter for Detail view
            var legalEntityDetailFilter = new treasury.filter.MultiSelect(
                    "legalEntityIds", "borrowFinancingLegalEntityFilter",
                    legalEntitiesData[0].descoEntities, []);

            var securityTypeIdsFilter = new treasury.filter.MultiSelect(
                    "securityTypeIds", "borrowFinancingSecurityTypeFilter",
                    securityTypesData[0].borrowFinancingSecurityTypes, []);

            var dateFilterValue = treasury.defaults.date || defaultDatesData[0].tMinusOneFilterDate;

            //Init Date filter
            $("#borrowFinancingDate").datepicker({
                dateFormat : 'yy-mm-dd',
                changeMonth : true,
                changeYear : true,
                minDate : Date.parse('1970-01-01'),
                maxDate : Date.today(),
                showOn : "both",
                buttonText : "<i class='fa fa-calendar'></i>"
            }).datepicker( "setDate", dateFilterValue);

            _borrowFinancingFilterGroup = new treasury.filter.FilterGroup([
                    legalEntityDetailFilter, securityTypeIdsFilter ], null, $("#borrowFinancingDate"),
                    null);

            if (callback) {
                callback();
            }
        });
    }

    function _validate() {
        return validateDate($("#borrowFinancingDate").val(), {errorDiv : $("#borrowFinancingDateError")});
    }

    function _resetValidation() {
        document.getElementById("borrowFinancingDateError").style.display = "none";
        document.getElementById("borrowFinancingDateError").innerHTML = "";
    }

    function _registerHandlers() {
        // DatePicker on change
        document.getElementById("borrowFinancingDate").onchange = function() {
            _resetValidation();
        };

        // Search button click handler
        document.getElementById("searchBorrowFinancing").onclick = function() {
            var error = _validate();
            if (error) {
                return;
            }

            treasury.borrowFinancing.gridActions.loadGrid(_borrowFinancingFilterGroup.getParameterMap());
        };
    }

    function copyReportURL(){
     generateReportURL("/treasury/seclend/search-borrow-financing",_borrowFinancingFilterGroup.getParameterMap(),"resultList");
    }

    // Called on page load once all webcomponents are ready
    function _initialize() {
        // To be able to show warnings / errors.
        ArcMessageHelper.initialize();

        // Register event handlers.
        _registerHandlers();

        _initializeFilters(function() {
            // All elements are hidden initially. Once webcomponents are ready
            // show the view.
            document.getElementById("landingPageDiv").style.display = "block";
            document.getElementById("landingPageDiv").setLayout();

            document.getElementById("parentArcLayout").setLayout();

              treasury.borrowFinancing.gridActions.loadGrid(_borrowFinancingFilterGroup.getParameterMap());
              document.querySelector('#copyReportUrlBtn').addEventListener('click', copyReportURL);
        });
    }
})();
