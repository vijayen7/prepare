/*jshint jquery:true*/
/*global sortcol,treasury*/
"use strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.borrowFinancing = window.treasury.borrowFinancing || {};
    window.treasury.borrowFinancing.gridconfig = {
        getGridOptions : _getGridOptions,
        getColumnConfig : _getColumnConfig
    };

    var _defaultColumns = [];

    function _getGridOptions() {
        return {
            //maxHeight: 600,
            enableCellNavigation : true,
            exportToExcel : true,
            autoHorizontalScrollBar : true,
            useAvailableScreenSpace : true,
            frozenColumn : 2,
            forceFitColumns:true,
            headerHeight : 40,
            applyFilteringOnGrid : true,
            sheetName : "Borrow Financing",
            customColumnSelection : {defaultcolumns:_defaultColumns},
            onRender : function() {
                $(".treasury-drill-through-borrowFinancing").off("click");
                $(".treasury-drill-through-borrowFinancing").on(
                "click",
                function() {
                    var params = $(this).data("params");
                    var grid = treasury.borrowFinancing.gridActions.getGrid();

                    if (grid) {
                        var borrowFinancingSummaryForSecurity = grid.dataView.getItemById(params.pnlSpn);
                        var borrowFinancingSummaryForSelectedCell = borrowFinancingSummaryForSecurity[params.cpeFamilyName];
                        
                        var popupTitle = "Borrow Financing Detail for (" + 
                            borrowFinancingSummaryForSecurity.securityName + ") - (" + 
                            decodeURIComponent(borrowFinancingSummaryForSelectedCell.cpeFamilyName) + ")";

                        treasury.borrowFinancing.popup.showDetail(borrowFinancingSummaryForSelectedCell,popupTitle);
                    }
                });

                $(".treasury-drill-through-borrowFinancingWeightedRate").off("click");
                $(".treasury-drill-through-borrowFinancingWeightedRate").on(
                "click",
                function() {
                    var params = $(this).data("params");
                    var grid = treasury.borrowFinancing.gridActions.getGrid();

                    if (grid) {
                        var borrowFinancingSummaryForSecurity = grid.dataView.getItemById(params.pnlSpn);
                        
                        //prepare object with all cpty data
                        var counterpartyData = [];
                        for(var key in borrowFinancingSummaryForSecurity)
                        {
                            if(borrowFinancingSummaryForSecurity[key]['borrowFinancingDetailList'] != undefined)
                            {
                                for (var i = 0; i < borrowFinancingSummaryForSecurity[key]['borrowFinancingDetailList'].length; i++) 
                                {
                                    counterpartyData.push(borrowFinancingSummaryForSecurity[key]['borrowFinancingDetailList'][i]);
                                }
                            }
                        }

                        var summaryObject ={};
                        summaryObject['borrowFinancingDetailList'] = counterpartyData;

                        var popupTitle = "Borrow Financing Detail for (" + 
                            borrowFinancingSummaryForSecurity.securityName + ")";

                        treasury.borrowFinancing.popup.showDetail(summaryObject,popupTitle);
                    }
                });                
            }
        };
    }

    function _customRateFormatter(currValue, prevValue)
    {
        //Converts to rate format and adds the indicator
        var rate = (currValue != null ? currValue * 100 : null);
        var inputData = {"currValue" : currValue,
                         "prevValue":prevValue};

        var indicatorHTML = "";
        var showDataInCell = true;

        if(currValue != null && prevValue != null)
        {
            if(Math.round(currValue * 100) < Math.round(prevValue * 100))
            {
                indicatorHTML = "<i title='Rate went down' inputData=" + JSON.stringify(inputData) + " class='icon-arrow--down green gridcell-icon'></i>";
            }
            else if(Math.round(currValue * 100) > Math.round(prevValue * 100))
            {
                indicatorHTML = "<i title='Rate went up' inputData=" + JSON.stringify(inputData) + " class='icon-arrow--up red gridcell-icon'></i>";
            }
        }
        else if(currValue != null)
        {
            indicatorHTML = "<i title='New position' inputData=" + JSON.stringify(inputData) + " class='new-position gridcell-icon'></i>";
        }
        else if(prevValue != null)
        {
            //if currValue is null but prevValue != null, show X icon
            indicatorHTML = "<i title='Closed position' inputData=" + JSON.stringify(inputData) + " class='icon-interface__cross closed-position gridcell-icon'></i>";
            showDataInCell = false;
        }


        if(showDataInCell)
        {
             return "<div class='aln-rt'>" + indicatorHTML + (rate == null ? "<span>-</span>" : Number(rate).toFixed(0)) + "</div>";
        }

        return "<div class='aln-rt'>" + indicatorHTML + "</div>";
    }

    function _getColumnConfig(counterpartyColumns) {
        var columns = [];
        _defaultColumns = [];

        _defaultColumns.push("securityName","pnlSpn","weightedAvgRate");

        columns.push(
        {
            id : "securityName",
            name : "Security Name",
            field : "securityName",
            toolTip : "Security Name",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true
            //width : 200
        });

        columns.push(
        {
            id : "pnlSpn",
            name : "PNL_SPN",
            field : "pnlSpn",
            toolTip : "PnL Spn",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true
        });

        columns.push({
            id : "weightedAvgRate",
            name : "Weighted Avg Borrow Rate (bps)",
            field : "weightedAvgRate",
            type : "number",            
            sortable : true,
            filter : false,
            formatter : function(row, cell, currValue, columnDef, securitySummary) {    
                var params = {
                    "pnlSpn" : securitySummary.pnlSpn
                };

                return treasury.formatters.drillThrough(
                        row,
                        cell,
                        currValue,
                        columnDef,
                        undefined,
                        function() {
                            return  _customRateFormatter(currValue, securitySummary.weightedAvgPrevRate); 
                        },
                        undefined, // predicate
                        JSON.stringify(params), // params
                        "-borrowFinancingWeightedRate"); // moduleId (key appended to treasury-drill-through-<moduleId> class)
            },
          excelDataFormatter : function(borrowRate) {
                return borrowRate != null ? borrowRate * 100 : "";
            },
            headerCssClass : "aln-rt b",
            autoWidth : true
            //width : 120
        });

        columns.push(
        {
            id : "securityShortDesc",
            name : "Short Description",
            field : "securityShortDesc",
            toolTip : "Short Description",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true
        });

        columns.push(
        {
            id : "securityIsin",
            name : "ISIN",
            field : "securityIsin",
            toolTip : "ISIN",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true
        });

        columns.push(
        {
            id : "securityCusip",
            name : "CUSIP",
            field : "securityCusip",
            toolTip : "CUSIP",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true
        });

        columns.push(
        {
            id : "securitySedol",
            name : "SEDOL",
            field : "securitySedol",
            toolTip : "SEDOL",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true
        });

        columns.push(
        {
            id : "securityTicker",
            name : "Ticker",
            field : "securityTicker",
            toolTip : "Ticker",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true
        });

        columns.push(
        {
            id : "securityRic",
            name : "RIC",
            field : "securityRic",
            toolTip : "RIC",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            autoWidth : true
        });

        if (counterpartyColumns) {
            for (var i = 0; i < counterpartyColumns.length; i++) {

                _defaultColumns.push(encodeURIComponent(counterpartyColumns[i]));

                columns.push({
                    id : encodeURIComponent(counterpartyColumns[i]),
                    name : counterpartyColumns[i] + " Borrow Rate (bps)",
                    field : encodeURIComponent(counterpartyColumns[i]),
                    type : "object",
                    filter : false,
                    sortable : true,
                    headerCssClass : "aln-rt b",
                    autoWidth : true,
                    //width : 120,
                    comparator : function(a, b) { 
                        var borrowRateFirst = (a[sortcol] ? a[sortcol]["borrowRate"] : 999999);
                        var borrowRateSecond = (b[sortcol] ? b[sortcol]["borrowRate"] : 999999);

                        return treasury.comparators.number(borrowRateFirst, borrowRateSecond); 
                    },
                    excelDataFormatter : function(cpeFamilyFinancing) {
                        return cpeFamilyFinancing && cpeFamilyFinancing.borrowRate != null ? cpeFamilyFinancing.borrowRate * 100 : "";
                    },
                    formatter : function(row, cell, cpeFamilyFinancingData, columnDef, borrowFinancingSummary) {
                        if (cpeFamilyFinancingData) {
                            var params = {
                                "pnlSpn" : cpeFamilyFinancingData.pnlSpn,
                                "cpeFamilyName" : cpeFamilyFinancingData.cpeFamilyName
                            };

                            return treasury.formatters.drillThrough(
                                    row,
                                    cell,
                                    cpeFamilyFinancingData.borrowRate,
                                    columnDef,
                                    undefined,
                                    function() {
                                        return  _customRateFormatter(cpeFamilyFinancingData.borrowRate, cpeFamilyFinancingData.prevDayBorrowRate); 
                                    },
                                    undefined, // predicate
                                    JSON.stringify(params), // params
                                    "-borrowFinancing"); // moduleId (key appended to treasury-drill-through-<moduleId> class)
                        }
                        else {
                            return "<div class='aln-rt'><span class='message'>-</span></div>";
                        }
                    }
                });
            }
        }

        return columns;
    }

})();
