/*jshint jquery:true*/
"use strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.borrowFinancing = window.treasury.borrowFinancing || {};
    window.treasury.borrowFinancing.gridActions = {
        loadGrid : _loadGrid,
        getGrid : _getGrid,
        extractGridColumns : _extractGridColumns,
        setGridColumns : _setGridColumns
    };

    var _borrowFinancingGrid;

    function _getGrid() {
        return _borrowFinancingGrid;
    }

    function _loadGrid(parameters) {
        treasury.borrowFinancing.landing.showLoading();

        var promise = $.ajax({
            url : "/treasury/seclend/search-borrow-financing",
            data : parameters,
            type : "POST",
            dataType : "json"
        });

        promise
                .done(function(data) {
                    _clearGrid("borrowFinancingGrid");
                    treasury.borrowFinancing.landing.hideLoading();
                    if (data && data.resultList && data.resultList.length) {

                        _modifyDataForGrid(data);

                        _borrowFinancingGrid = new dportal.grid.createGrid($("#borrowFinancingGrid"),
                                data.resultList,
                                treasury.borrowFinancing.gridconfig.getColumnConfig(data.columns),
                                treasury.borrowFinancing.gridconfig.getGridOptions());
                        _borrowFinancingGrid.resizeCanvas();
                        //_showSaveSettings();
                    } else {
                        _showGridInfo("borrowFinancingGrid", "No Borrow Financing records found.");
                    }
                });
        promise
                .fail(function(xhr, text, errorThrown) {
                    _clearGrid("borrowFinancingGrid");
                    treasury.borrowFinancing.landing.hideLoading();

                    ArcMessageHelper.showMessage("WARNING", "Error occurred while loading Borrow Financing summary.");
                });
    }

    function _modifyDataForGrid(data)
    {
        //Iterate over columns and update data.result with encoded value for CPE family
        //This is done as CPE family name can contain spl characters 
        //and grid doesnt support filters for those columns

        for (var i = 0; i < data.columns.length; i++) {
            var encodedColumnName = encodeURIComponent(data.columns[i]);

            for(var j=0;j<data.resultList.length;j++){

                if(data.resultList[j][data.columns[i]]!=undefined)
                {
                    data.resultList[j][encodedColumnName] = data.resultList[j][data.columns[i]];
                    data.resultList[j][encodedColumnName]["cpeFamilyName"] = encodedColumnName;

                    //delete the old key
                    delete data.resultList[j][data.columns[i]];
                }
            }
        }
    }


    function _clearGrid(gridId) {
        //_hideSaveSettings();

        document.getElementById(gridId + "Info").innerHTML = "";
        document.getElementById(gridId + "Info").style.display = "none";

        document.getElementById(gridId).style.display = "block";
        document.getElementById(gridId).innerHTML = "";
        if (document.getElementById(gridId + "-header")) {
            document.getElementById(gridId + "-header").innerHTML = "";
        }
    }

    function _showGridInfo(gridId, message) {
        document.getElementById(gridId).style.display = "none";

        document.getElementById(gridId + "Info").style.display = "block";
        document.getElementById(gridId + "Info").innerHTML = message;
    }

    function _extractGridColumns() {

        var gridColumnIds = [];
        if (_borrowFinancingGrid) {
            var selectedColumns = _borrowFinancingGrid.grid.getColumns();
            for (var i = 0; i < selectedColumns.length; i++) {
                gridColumnIds.push(selectedColumns[i].id);
            }
        }
        return gridColumnIds;
    }

    function _setGridColumns(gridColumns){
        _borrowFinancingGrid.setColumns(gridColumns);
        setSelectedItems("borrowFinancingGrid-columnselection",gridColumns);
    }

    function _showSaveSettings() {
        var saveSettingsSelect = document.querySelector("arc-save-settings");
        saveSettingsSelect.style.display = "inline-block";
        var saveSettingsButton = document.getElementById("saveGridView");
        saveSettingsButton.style.display = "inline-block";
    }

    function _hideSaveSettings() {
        var saveSettingsSelect = document.querySelector("arc-save-settings");
        saveSettingsSelect.style.display = "none";
        var saveSettingsButton = document.getElementById("saveGridView");
        saveSettingsButton.style.display = "none";
    }

})();
