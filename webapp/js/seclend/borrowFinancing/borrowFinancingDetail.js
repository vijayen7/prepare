"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.borrowFinancing = window.treasury.borrowFinancing || {};
    window.treasury.borrowFinancing.popup = {
        showDetail : _showDetail
    };

    function _showDetail(borrowFinancingSummary,popupTitle) {
        var borrowFinancingDetailContent = 
            _getBorrowFinancingDetailContent(borrowFinancingSummary.borrowFinancingDetailList);

        var arcDialog = document.querySelector("arc-dialog");

        var dialogConfig = {
            title : popupTitle,
            content : borrowFinancingDetailContent,
            modal : true,
            buttons : [
            {
                html : "Close",
                callback : function() { arcDialog.visible = false; }
            }
            ]
        };

        arcDialog.reveal(dialogConfig);
    }

    function _getBorrowFinancingDetailContent(borrowFinancingDetailList) {
        var borrowFinancingDetailHost = document.createElement("div");

        var templateNode = document.getElementById("borrowFinancingDetailTemplate");
        var templateClone = document.importNode(templateNode.content, true);

        var borrowFinancingDetailTable = templateClone.querySelector("table");

        for (var i = 0; i < borrowFinancingDetailList.length; i++) {
            var borrowFinancingDetail = borrowFinancingDetailList[i];
            borrowFinancingDetailTable.appendChild(_getBorrowFinancingDetailRow(borrowFinancingDetail));
        }

        borrowFinancingDetailHost.appendChild(borrowFinancingDetailTable);

        return borrowFinancingDetailHost.innerHTML;
    }

    function _getBorrowFinancingDetailRow(borrowFinancingDetail) {
        var row = document.createElement("tr");

        var legalEntity = document.createElement("td");
        legalEntity.appendChild(document.createTextNode(borrowFinancingDetail.legalEntity));
        var cpe = document.createElement("td");
        cpe.appendChild(document.createTextNode(borrowFinancingDetail.cpe));
        var eca = document.createElement("td");
        eca.appendChild(document.createTextNode(borrowFinancingDetail.eca));
        var borrowRate = document.createElement("td");
        borrowRate.innerHTML = dpGrid.Formatters.Number(null, null, borrowFinancingDetail.borrowRate);
        var prevDayBorrowRate = document.createElement("td");
        prevDayBorrowRate.innerHTML = dpGrid.Formatters.Number(null, null, borrowFinancingDetail.prevDayBorrowRate);
        var notional = document.createElement("td");
        notional.innerHTML = dpGrid.Formatters.Number(null, null, borrowFinancingDetail.notional);

        row.appendChild(legalEntity);
        row.appendChild(cpe);
        row.appendChild(eca);
        row.appendChild(borrowRate);
        row.appendChild(prevDayBorrowRate);
        row.appendChild(notional);

        return row;
    }

})();