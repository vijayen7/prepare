/*jshint jquery:true*/
/*global treasury, ArcMessageHelper, validateDate*/
"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.seclendOps = window.treasury.seclendOps || {};
  window.treasury.seclendOps.gridOptions = {
    getResultOptions: _getResultOptions
  };


  function _getResultOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRowOnClick: true,
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      useAvailableScreenSpace: true,
      applyFilteringOnGrid: true,
      exportToExcel: true,
      sortList: [{ columnId: "jobStatus", sortAsc: true }],
      frozenColumn: 1
    };
  }
})();
