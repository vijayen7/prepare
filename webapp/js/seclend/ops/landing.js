/*jshint jquery:true*/
/*global treasury, ArcMessageHelper, validateDate*/
"use strict";

var seclendOpsSearchFilterGroup;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.seclendOps = window.treasury.seclendOps || {};
  window.treasury.seclendOps.landing = {
    initialize: _initialize,
    loadSearchResults: _loadSearchResults
  };

  function _initialize() {
    _registerHandlers();
    _initializeFilters();
  }

  function _registerHandlers() {
    document.getElementById("search").onclick = window.treasury.seclendOps.landing.loadSearchResults;
  }

  function _initializeFilters() {
    showLoading();
    $.when(treasury.loadfilter.loadSeclendDataTypes())
    .done(function(seclendDataTypesData) {
          var seclendDataTypeFilterValue = new window.treasury.filter.MultiSelect(
            "seclendDataTypeIds",
            "seclendOpsDataTypeFilter",
            seclendDataTypesData["seclendDataTypes"], []);

          _initializeDatePicker(document.getElementById("seclendOpsDate"), true);

          seclendOpsSearchFilterGroup = new window.treasury.filter.FilterGroup(
            [seclendDataTypeFilterValue], null,
            $("#seclendOpsDate"), null);

        });

    hideLoading();
  }

  function _loadSearchResults() {
    clearGrid("seclendOpsGrid");

    var parameters = seclendOpsSearchFilterGroup
      .getSerializedParameterMap();
    var formattedDate = getFormattedDate("seclendOpsDate");
    var isLatest = document.getElementById("isLatestFilter").checked;
    parameters.dateString = formattedDate;
    parameters.isLatest = isLatest;

    window.treasury.seclendOps.action.loadResults(parameters);
  }

})();
