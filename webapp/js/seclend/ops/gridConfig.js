/*jshint jquery:true*/
/*global treasury, ArcMessageHelper, validateDate*/
"use strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.seclendOps = window.treasury.seclendOps || {};
    window.treasury.seclendOps.gridConfig = {
        getSearchResultColumns : _getSearchResultColumns
    };

    function _getSearchResultColumns() {
        return [ {
            id : "jobStatus",
            type : "text",
            name : "Job Status",
            field : "job_status",
            sortable : true,
            formatter : _applicableColumnIdentifier,
            headerCssClass : "b",
            excelFormatter : "#,##0",
        }, {
            id : "etlAlias",
            type : "text",
            name : "ETL Alias",
            field : "etl_alias",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "fileAlias",
            type : "text",
            name : "File Alias",
            field : "file_alias",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "filePatterns",
            type : "text",
            name : "File Patterns",
            field : "file_patterns",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0",
            width : 200
        }, {
            id : "isLatest",
            type : "text",
            name : "Latest",
            field : "is_latest",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "beginTime",
            type : "text",
            name : "Begin Time(UTC)",
            field : "begin_time",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "endTime",
            type : "text",
            name : "End Time(UTC)",
            field : "end_time",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "recordsParsed",
            type : "text",
            name : "Records Parsed",
            field : "records_parsed",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "recordsMapped",
            type : "text",
            name : "Records Mapped",
            field : "records_mapped",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "recordsUnmapped",
            type : "text",
            name : "Records Unmapped",
            field : "records_unmapped",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "recordsMultimapped",
            type : "text",
            name : "Records Multimapped",
            field : "records_multimapped",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "rollupStartTime",
            type : "text",
            name : "Rollup Start Time(UTC)",
            field : "rollup_start_time",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "rollupEndTime",
            type : "text",
            name : "Rollup End Time(UTC)",
            field : "rollup_end_time",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "configFilePath",
            type : "text",
            name : "Config File Path",
            field : "config_file_path",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "lastParsedDate",
            type : "text",
            name : "Last Parsed Date",
            field : "last_parsed_date",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        }, {
            id : "hasFileArrived",
            type : "text",
            name : "Has File Arrived",
            field : "has_file_arrived",
            sortable : true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        },

        ];

    }

    function _applicableColumnIdentifier(row, cell, value, columnnDef,
            dataContext) {
        if (columnnDef.field == "job_status") {
            if (dataContext.job_status == 'SUCCESS') {
                return '<div class="success" style="height:100%;text-align:center;vertical-align: middle;">'
                        + value + '</div>';
            } else if (dataContext.job_status == 'ACTIVE') {
                return '<div class="primary" style="height:100%;text-align:center;vertical-align: middle;">'
                        + value + '</div>';
            } else if (dataContext.job_status == 'FAILED') {
                return '<div class="critical" style="height:100%;text-align:center;vertical-align: middle;">'
                        + value + '</div>';
            } else if (dataContext.job_status == 'KILLED') {
                return '<div class="warning" style="height:100%;text-align:center;vertical-align: middle;">'
                + value + '</div>';
            } else if (dataContext.job_status == 'SCHEDULED') {
                return '<div class="info" style="height:100%;text-align:center;vertical-align: middle;">'
                        + value + '</div>';
            } else
                return '<div class="disabled" style="height:100%;text-align:center;vertical-align: middle;">'
                        + value + '</div>';
        }
    }
})();
