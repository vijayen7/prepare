/*jshint jquery:true*/
/*global treasury, ArcMessageHelper, validateDate*/
"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.seclendOps = window.treasury.seclendOps || {};
  window.treasury.seclendOps.action = {
    loadResults: _loadResults
  };


  function _loadResults(parameters) {
    showLoading();
    var serviceCall = $.ajax({
      url: "/treasury/seclend/load-seclend-ops-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });

    serviceCall
      .done(function(data) {
        hideLoading();
        clearGrid("seclendOpsGrid");

        if (data && data.resultList && data.resultList.length) {
          var seclendOpGrid = new dportal.grid.createGrid(
            $("#seclendOpsGrid"), data["resultList"],
            window.treasury.seclendOps.gridConfig.getSearchResultColumns(),
            window.treasury.seclendOps.gridOptions.getResultOptions());

        } else if (data && data.errorMessage) {
          showErrorMessage(data.errorMessage);
          console.log("Error in fetching seclend jobs list.");

        } else if (data) {
          document.getElementById("seclendOpsGrid").innerHTML = 'No Rules Found';
        }

      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      hideLoading();
      console.log("Error in fetching seclend jobs list.");
    });
  }

})();
