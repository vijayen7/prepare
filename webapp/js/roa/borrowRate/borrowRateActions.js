"use strict";

var borrowRateDetailsGrid;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.borrowRateActions = {
    loadBorrowRateResults: _loadBorrowRateResults,
    getClassificationRules: _getClassificationRules
  };

  function _loadBorrowRateResults(parameters) {
    treasury.roa.util.showLoading();
    var serviceCall = $.ajax({
      url: 'treasury/counterpartyRelationship/borrow-rate-comparison-results',
      data: parameters,
      type: 'POST',
      dateType: 'json'
    });

    var columnHeaderCall = treasury.roa.borrowRateActions
      .getClassificationRules();

    $.when(serviceCall, columnHeaderCall)
      .done(
        function(borrowRateData, columnHeaderData) {
          treasury.roa.util
            .clearGrid('borrowRateComparisonDetailsGrid');
          treasury.roa.util.hideLoading();
          var data = borrowRateData[0];
          document.getElementById('borrowRateComparisonDetailsGrid').removeAttribute("hidden");
          if (data && data.resultList &&
            data.resultList.length) {
            var borrowRateComparisonData = _formatResultData(data['resultList'], columnHeaderData[0]['classificationRules']);
            var borrowRateColumnData = treasury.roa.borrowRateColumnsConfig
              .getBorrowRateComparisonColumns(columnHeaderData[0]['classificationRules']);
            document.getElementById('roaMessage').setAttribute("hidden", true);
            borrowRateDetailsGrid = new dportal.grid.createGrid(
              $('#borrowRateComparisonDetailsGrid'),
              borrowRateComparisonData,
              borrowRateColumnData,
              treasury.roa.borrowRateGridOptions
              .getBorrowRateComparisonGridOptions());
            treasury.roa.util.resizeCanvasOnGridChange(
              borrowRateDetailsGrid,
              'borrowRateComparisonDetailsGrid');
          } else if (data && data.errorMessage) {
            treasury.roa.util
              .showErrorMessage(data.errorMessage);
          } else if (data && (data.length == null || data.length == 0)) {
            document.getElementById('roaMessage').removeAttribute("hidden");
            document
              .getElementById('roaMessage').innerHTML = treasury.roa.util.getFormattedMessage(
                'No results found matching selected criteria.', '');
          }
        });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        treasury.roa.util.clearGrid('borrowRateComparisonDetailsGrid');
        treasury.roa.util.hideLoading();
        treasury.roa.util
          .showErrorMessage('Error occurred while retrieving borrow rate comparisons.');
      });

    columnHeaderCall
      .fail(function(xhr, text, errorThrown) {
        treasury.roa.util.clearGrid('borrowRateComparisonDetailsGrid');
        treasury.roa.util.hideLoading();
        treasury.roa.util
          .showErrorMessage('Error occurred while retrieving column headers.');
      });
  }

  function _getClassificationRules() {
    return $.ajax({
      url: "/treasury/counterpartyRelationship/get-classification-rules",
      dataType: "json"
    });
  }

  function _formatResultData(borrowRateComparisonData, columnData) {
    var columnKeys = [];
    for (var index in columnData) {
      columnKeys[index] = columnData[index][1];
    }
    for (var i in borrowRateComparisonData) {
      for (var j in columnKeys) {
        if (!(columnKeys[j] in borrowRateComparisonData[i])) {
          borrowRateComparisonData[i][columnKeys[j]] = 0;
        }
      }
    }
    return borrowRateComparisonData;
  }
})();
