"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.borrowRateColumnsConfig = {
    getBorrowRateComparisonColumns: _getBorrowRateComparisonColumns
  };

  function _getBorrowRateComparisonColumns(classificationRulesData) {
    var classifications = new Array();
    classifications.push({
      id: 'cpe_entity_family',
      type: 'text',
      name: 'Counterparty Entity Family',
      field: 'cpe_entity_family',
      sortable: true,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0'
    });

    for (var index in classificationRulesData) {
      if (classificationRulesData[index][1] != 'NA') {
          classifications.push({
            id: classificationRulesData[index][1],
            type: 'number',
            name: classificationRulesData[index][1] + ' (USD)',
            field: classificationRulesData[index][1],
            sortable: true,
            headerCssClass: 'aln-rt b',
            formatter: treasury.formatters.roaClickableAmount,
            aggregator: dpGrid.Aggregators.sum,
            defaultValue: "0",
            excelFormatter: '#,##0.0',
            toolTip: classificationRulesData[index][2]
          });
      }
    }
    classifications.push({
      id: 'total',
      type: 'number',
      name: 'Total (USD)',
      field: 'total_amount',
      sortable: true,
      headerCssClass: 'aln-rt b',
      cssClass: 'cell-selection',
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      defaultValue: "0",
      excelFormatter: '#,##0.0',
      toolTip: "Total refers to the total borrow market value."
    });
    return classifications;
  }
})();
