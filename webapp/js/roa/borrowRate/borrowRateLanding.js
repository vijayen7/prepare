"use strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.roa = window.treasury.roa || {};
    window.treasury.roa.borrowRate = {
        loadBorrowRateSearchResults: _loadBorrowRateSearchResults
    };

    function _loadBorrowRateSearchResults() {
        if (document.getElementById('datePickerError').innerHTML != '') {
          ArcMessageHelper.showMessage('error', 'Please select a valid date.', null, null);
            //treasury.roa.util.showErrorMessage('Please select a valid date.');
            return;
        }
        if (new Date(document.getElementById('datePicker').value) == '') {
          ArcMessageHelper.showMessage('warning', 'Please select date.', null, null);
            //treasury.roa.util.showErrorMessage('Please select date.');
            return;
        }
        treasury.roa.util.clearGrid('borrowRateComparisonDetailsGrid');
        document.getElementById('borrowRateDetails').style.display = 'none';
        var parameters = roaSearchFilterGroup.getSerializedParameterMap();
        parameters.excessBorrowOption = document.getElementById('excessBorrow').checked;
        if (document.getElementById('roaFilterSidebar').state == 'expanded') {
            document.getElementById('roaFilterSidebar').toggle();
        }
        treasury.roa.borrowRateActions.loadBorrowRateResults(parameters);
    }
})();
