"use strict";
var selectedCell = {
  row : null,
  cell : null
};

(function() {

  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.borrowRateGridOptions = {
    getBorrowRateComparisonGridOptions: _getBorrowRateComparisonGridOptions
  };

  function _getBorrowRateComparisonGridOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRow: false,
      sortList: [{
        columnId: "cpe_entity_family",
        sortAsc: true
      }],
      summaryRow: true,
      displaySummaryRow: true,
      summaryRowText: "Total",
      onCellClick: function(args, isNotToggle) {
        var details;
        if (args.cell != 0 &&
          args.row != null &&
          args.colId != 'total' &&
          typeof args.item[args.colId] != "undefined" &&
          args.item[args.colId] != "0") {
          borrowRateDetailsGrid.grid.setActiveCell(args.row, args.cell);
          var item = args.item;
          var colId = args.colId.toString();
          var totalRow = borrowRateDetailsGrid.getSummaryRow();

          var itemAmount = parseFloat(item[colId], 10);
          var totalAmountCPE = parseFloat(item['total_amount'], 10);
          var totalAmountInClassification = parseFloat(totalRow[colId],
            10);

          var shareWithinBroker = Math
            .round((itemAmount / totalAmountCPE) * 100);
          var shareAcrossBrokers = Math
            .round((itemAmount / totalAmountInClassification) * 100);

          details = "<span><p><strong>" + shareWithinBroker + "%</strong> of the borrow market value at " + item['cpe_entity_family'] + " is " + colId + ".<br/><strong>" + shareAcrossBrokers + "%</strong> of the " + colId + " borrow market value is at " + item['cpe_entity_family'] + ".</p></span>";
          document.getElementById('borrowRateDetails').style.display = 'block';
          document.getElementById('borrowRateDetails').label = item['cpe_entity_family'] +
            ' : ' + colId;
          document.getElementById('detailsDiv').innerHTML = details;
        } else if (args.row == null && args.colId != 'total' && args.cell != 0) {
          var totalColAmount = parseFloat(args.item[args.colId], 10);
          var totalAmount = parseFloat(args.item['total_amount'], 10);
          var totalShare = Math.round((totalColAmount / totalAmount) * 100);

          document.getElementById('borrowRateDetails').style.display = 'block';
          document.getElementById('borrowRateDetails').label = args.colId;
          details = "<span><p><strong>" + totalShare + "%</strong> of the borrow market value is as <strong>" + args.colId + "</strong></p></span>";
          document.getElementById('detailsDiv').innerHTML = details;
        } else {
          document.getElementById('borrowRateDetails').style.display = 'none';
          document.getElementById('detailsDiv').innerHTML = '';
        }
      },
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      useAvailableScreenSpace: true,
      exportToExcel: true,
      sheetName: "Borrow Rate Comparison Report"
    };
  }
})();
