"use strict";

var roaSearchFilterGroup;
var foTypeRoaSelect;
var subtypeRoaFilter;
var cpeFamiliesRoaFilter;
var legalEntityFamiliesRoaFilter;
var toolDetails;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.landing = {
    loadRoaLandingView: _loadRoaLandingViewFilters,
    loadSearchResults: _loadSearchResults,
    resetSearch: _resetSearchFilters,
    resetViewByTool: _resetViewByTool
  };

  toolDetails = {
    'borrowRateComparison': {
      'name': 'borrow-rate-comparison',
      'sidebarHeader': 'Borrow Rate Comparison Filters'
    },
    'debitToShortRatio': {
      'name': 'debit-to-short-ratio',
      'sidebarHeader': 'Debit To Short Ratio Filters'
    },
    'freeCashTracker': {
      'name': 'free-cash-tracker',
      'sidebarHeader': 'Free Cash Tracker Filters'
    }
  };

  window.addEventListener("WebComponentsReady", _initializeRoa);

  function _initializeRoa() {
    $(document).on({
      ajaxStart: function() {
        treasury.roa.util.showLoading();
      },
      ajaxStop: function() {
        treasury.roa.util.hideLoading();
      }
    });

    _registerHandlers();
    treasury.roa.landing.loadRoaLandingView();
  }

  function _registerHandlers() {
    document.getElementById('roaSearch').onclick = treasury.roa.landing.loadSearchResults;
    document.getElementById('resetSearch').onclick = treasury.roa.landing.resetSearch;
    document.getElementById('roaFilterSidebar').addEventListener("stateChange", resizeAllCanvas);

    document.getElementById('borrowRateComparison').onclick = function() {
      treasury.roa.landing.resetViewByTool('borrowRateComparison');
    };
    document.getElementById('debitToShortRatio').onclick = function() {
      treasury.roa.landing.resetViewByTool('debitToShortRatio');
    };
    document.getElementById('freeCashTracker').onclick = function() {
      treasury.roa.landing.resetViewByTool('freeCashTracker');
    };

    document.getElementById('copySearchURL').addEventListener('click', copySearchURL);
  }

  function resizeAllCanvas() {
    if (borrowRateDetailsGrid != null) {
      borrowRateDetailsGrid.resizeCanvas();
    }
    if (debitToShortRatioDetailsGrid != null) {
      debitToShortRatioDetailsGrid.resizeCanvas();
    }
    if (freeCashTrackerDetailsGrid != null) {
      freeCashTrackerDetailsGrid.resizeCanvas();
    }
  }

  function _resetSearchFilters() {
    document.getElementById('cpeFamilyFilter').value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById('legalEntityFamilyFilter').value = {
      status: 'none',
      selectedNodes: []
    };
    document.getElementById('foTypeSelect').value = "";
    document.getElementById('subtypeFilter').value = {
      status: 'none',
      selectedNodes: []
    };

    $('#datePicker').datepicker('setDate', Date.today());
    document.getElementById('excessBorrow').checked = true;
  }

  function _loadRoaLandingViewFilters() {
    var errorDialog = document.getElementById('errorMessage');
    errorDialog.visible = false;
    $.when(treasury.loadfilter.cpeFamilies(),
      treasury.loadfilter.loadEntityFamilies(),
      treasury.loadfilter.fotypes(), treasury.loadfilter.defaultDates()).done(
      function(cpeFamiliesData, legalEntityFamiliesData, foTypesData, defaultDatesData) {
        cpeFamiliesRoaFilter = new window.treasury.filter.MultiSelect(
          'cpeFamilyFilterIds', 'cpeFamilyFilter',
          cpeFamiliesData[0].counterPartyEntityFamilies, []);

        legalEntityFamiliesRoaFilter = new window.treasury.filter.MultiSelect(
          'legalEntityFamilyFilterIds', 'legalEntityFamilyFilter',
          legalEntityFamiliesData[0].descoEntityFamilies, []);

        foTypeRoaSelect = new window.treasury.filter.SingleSelect(
          'foTypeFilterId', 'foTypeSelect',
          foTypesData[0]['attrFOTypes'], []);
        document.querySelector('#foTypeSelect')
          .addEventListener('change', _foTypeOnChange);

        subtypeRoaFilter = new window.treasury.filter.MultiSelect(
          'subtypeFilterIds', 'subtypeFilter', [], []);

        var dateFilterValue = treasury.defaults.date || defaultDatesData[0].tMinusOneFilterDate;

        var minDate = Date.parse('1970-01-01');
        var maxDate = Date.parse('2038-01-01');
        var dateFormat = 'yy-mm-dd';
        treasury.roa.util.setupDateFilter($("#datePicker"),
          $("#datePickerError"), dateFilterValue, minDate, maxDate, dateFormat);

        roaSearchFilterGroup = new treasury.filter.FilterGroup([cpeFamiliesRoaFilter, legalEntityFamiliesRoaFilter], [foTypeRoaSelect], $("#datePicker"), null);
        var directLink = false;
        for (var i in toolDetails) {
          if (window.location.href.includes(i)) {
            treasury.roa.landing.resetViewByTool(i);
            directLink = true;
            break;
          }
        }
        if (!directLink) {
          treasury.roa.landing.resetViewByTool('borrowRateComparison');
        }
      }
    );
  }

  function _loadSearchResults() {
    if (document.getElementById('datePickerError').innerHTML != '') {
      ArcMessageHelper.showMessage('error', 'Please select a valid date.', null, null);
      return;
    }
    if (new Date(document.getElementById('datePicker').value) == '') {
      ArcMessageHelper.showMessage('warning', 'Please select date.', null, null);
      return;
    }
    var toolLink = document.getElementById('roa-menu').getElementsByClassName("active")[0];

    if (toolLink['id'] == 'borrowRateComparison') {
      treasury.roa.borrowRate.loadBorrowRateSearchResults();
    } else if (toolLink['id'] == 'debitToShortRatio') {
      treasury.roa.debitToShortRatio.loadDebitToShortRatioSearchResults();
    } else if (toolLink['id'] == 'freeCashTracker') {
      treasury.roa.freeCashTracker.loadFreeCashTrackerSearchResults();
    }
  }

  function _foTypeOnChange() {
    var foTypeValue = document.getElementById("foTypeSelect").value;
    if (foTypeValue != null) {
      $.when(treasury.loadfilter.foSubtypes(foTypeValue.key)).done(
        function(foSubtypesData) {
          subtypeRoaFilter = new window.treasury.filter.MultiSelect(
            "subtypeFilterIds", "subtypeFilter",
            foSubtypesData["foSubtypes"], []);
          roaSearchFilterGroup = new window.treasury.filter
            .FilterGroup([cpeFamiliesRoaFilter, legalEntityFamiliesRoaFilter, subtypeRoaFilter], [foTypeRoaSelect], $("#datePicker"), null);
        });
    }
  }

  function _resetViewByTool(toolId) {
    for (var i in toolDetails) {
      document.getElementById(i).classList.remove('active');
    }
    if (toolId == 'borrowRateComparison') {
      document.getElementById('instrumentTypeDiv').style.display = 'block';
      document.getElementById('subtypeDiv').style.display = 'block';
      document.getElementById('excessBorrowDiv').style.display = 'block';
    } else {
      document.getElementById('instrumentTypeDiv').style.display = 'none';
      document.getElementById('subtypeDiv').style.display = 'none';
      document.getElementById('excessBorrowDiv').style.display = 'none';
    }
    document.getElementById(toolId).classList.add('active');
    document.getElementById('roaFilterSidebar').header = toolDetails[toolId]['sidebarHeader'];
    if (document.getElementById('roaFilterSidebar').state == 'collapsed') {
      document.getElementById('roaFilterSidebar').toggle();
    }
    $('#roaContent').load("../jsp/roa/" + toolDetails[toolId]['name'] + ".jspf");
  }

  function copySearchURL() {
    var url = '/treasury/counterpartyRelationship/';
    treasury.roa.util.showLoading();
    for (var i in toolDetails) {
      if (window.location.href.includes(i)) {
        url = url.concat(toolDetails[i]['name']).concat('-results');
        break;
      }
    }
    if (!url.includes('-results')) {
      url = url.concat(toolDetails['borrowRateComparison']['name']).concat('-results');
    }
    generateReportURL(url, roaSearchFilterGroup.getParameterMap(), "resultList");
    treasury.roa.util.hideLoading();
  }
})();
