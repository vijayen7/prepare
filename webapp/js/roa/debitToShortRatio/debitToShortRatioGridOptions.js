/*jshint jquery:true*/
"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.debitToShortRatioGridOptions = {
    getResultOptions: _getResultOptions
  };

  function _getResultOptions() {
    var groupColumns = getGroupColumns();
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      enableMultilevelGrouping: {
        hideGroupingHeader: false,
        showGroupingKeyInColumn: false,
        initialGrouping: groupColumns,
      },
      expandCollapseAll: true,
      highlightRowOnClick: true,
      sortList: [{
        columnId: "cpeFamily",
        sortAsc: true
      }],
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      useAvailableScreenSpace: true,
      exportToExcel: {
        splitNestedFieldIntoMultipleColumns: false,
      },
      sheetName: "Debit To Short Ratio Report"
    };
  }

  function getGroupColumns() {
    var groupColumns = ['cpeFamily', 'cpeEntity', 'legalEntityFamily', 'legalEntity'];
    return groupColumns;
  }
})();
