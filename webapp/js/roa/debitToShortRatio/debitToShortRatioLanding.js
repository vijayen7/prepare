"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.debitToShortRatio = {
    loadDebitToShortRatioSearchResults: _loadDebitToShortRatioSearchResults
  };

  function _loadDebitToShortRatioSearchResults() {
    treasury.roa.util.clearGrid('debitToShortRatioDetailsGrid');

    var parameters = roaSearchFilterGroup.getSerializedParameterMap();
    if (document.getElementById('roaFilterSidebar').state == 'expanded') {
      document.getElementById('roaFilterSidebar').toggle();
    }
    treasury.roa.debitToShortRatioActions.loadDebitToShortRatioSearchResults(parameters);
  }
})();
