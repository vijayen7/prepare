"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.debitToShortRatioColumnsConfig = {
    getResultColumns: _getResultColumns
  };

  function _getResultColumns() {
    return [{

      id: "cpeFamily",
      type: "text",
      name: "Counterparty Entity Family",
      field: "cpe_family",
      sortable: true,
      headerCssClass: "aln-rt b",
      groupId: "cpeFamily",
    }, {
      id: "cpeEntity",
      type: "text",
      name: "Counterparty Entity",
      field: "counterparty_entity",
      sortable: true,
      headerCssClass: "aln-rt b",
      groupId: "cpeEntity",
    }, {
      id: "legalEntityFamily",
      type: "text",
      name: "Legal Entity Family",
      field: "legal_entity_family",
      sortable: true,
      headerCssClass: "aln-rt b",
      groupId: "legalEntityFamily",
    }, {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legal_entity",
      sortable: true,
      headerCssClass: "aln-rt b",
      groupId: "legalEntity",
    }, {
      id: "currency",
      type: "text",
      name: "Currency",
      field: "currency",
      sortable: true,
      headerCssClass: "aln-rt b",
      groupId: "currency",
    }, {
      id: "debitBalance",
      type: "number",
      name: "Debit Balance (USD)",
      field: "debit_balance",
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      toolTip: "Debit Balance refers to the net cash balance after subtracting the short market value."
    }, {
      id: "smv",
      type: "number",
      name: "SMV (USD)",
      field: "smv",
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      toolTip: "SMV refers to the short market value."
    }, {
      id: "debitToShortRatio",
      type: "number",
      name: "Debit To Short Ratio",
      field: "debit_to_short_ratio",
      formatter: dpGrid.Formatters.Float,
      aggregator: dpGrid.Aggregators.sum,
      groupingAgrregator: SummationAggregator,
      groupTotalsFormatter: ratioGroupFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      toolTip: "Debit to Short Ratio refers to the ratio of debit balance to SMV. This will be n/a if the debit balance is positive (credit balance) and/or if SMV is zero. The ratio is calculated at each level of aggregation."
    }, {
      id: "balanceSheetImpact",
      type: "number",
      name: "Balance Sheet Impact (USD)",
      field: "balance_sheet_impact",
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      toolTip: "Balance Sheet Impact refers to the impact of the client portfolio on the counterparty's balance sheet, and is calculated as the sum of debit balance and SMV."
    }];
  }

  function summationFormatter(totals, columnDef, placeHolder, isExportToExcel) {
    var value = totals.sum[columnDef.field];
    return isExportToExcel ? value : dpGrid.Formatters.Number(null, null, value, columnDef, null);
  }

  function ratioGroupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
    if (totals && totals.sum) {
      if (totals.sum["smv"] != 0 && totals.sum['debit_balance'] < 0) {
        var ratio = Math.round(Math.abs(totals.sum["debit_balance"])) / Math.round(totals.sum["smv"]);
        return isExportToExcel ? ratio : dpGrid.Formatters.Float(null, null, ratio, columnDef, null);
      }
    }
  }

  function SummationAggregator(field) {
    this.field_ = field;

    this.init = function() {
      this.sum_ = null;
    };
    this.accumulate = function(item) {
      var val = item[this.field_];
      if (val != null && val !== "" && !isNaN(val)) {
        this.sum_ += Math.round(val);
      }
    };

    this.storeResult = function(groupTotals) {
      if (!groupTotals.sum) {
        groupTotals.sum = {};
      }
      groupTotals.sum[this.field_] = this.sum_;
    };
  }
})();
