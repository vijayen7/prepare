"use strict";
var debitToShortRatioDetailsGrid;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.debitToShortRatioActions = {
    loadDebitToShortRatioSearchResults: _loadDebitToShortRatioSearchResults
  };

  function _loadDebitToShortRatioSearchResults(parameters) {
    treasury.roa.util.showLoading();
    var serviceCall = $.ajax({
      url: "/treasury/counterpartyRelationship/debit-to-short-ratio-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        treasury.roa.util.clearGrid("debitToShortRatioDetailsGrid");
        treasury.roa.util.hideLoading();
        document.getElementById('debitToShortRatioDetailsGrid').removeAttribute("hidden");
        if (data && data.resultList && data.resultList.length) {
          document.getElementById('roaMessage').setAttribute("hidden", true);
          debitToShortRatioDetailsGrid = new dportal.grid.createGrid(
            $("#debitToShortRatioDetailsGrid"), data.resultList,
            treasury.roa.debitToShortRatioColumnsConfig
            .getResultColumns(),
            treasury.roa.debitToShortRatioGridOptions
            .getResultOptions());
          treasury.roa.util.resizeCanvasOnGridChange(debitToShortRatioDetailsGrid,
            'debitToShortRatioDetailsGrid');
           debitToShortRatioDetailsGrid.dataView.collapseAllGroups();
        } else if (data && data.errorMessage) {
          treasury.roa.util.showErrorMessage(data.errorMessage);
          console.log("Error in fetching debit to short ratio results");
        } else if (data && (data.length == null || data.length == 0)) {
          document.getElementById('roaMessage').removeAttribute("hidden");
          document
            .getElementById('roaMessage').innerHTML = treasury.roa.util.getFormattedMessage(
                 'No results found matching selected criteria.', '');
        }
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      treasury.roa.util.clearGrid("debitToShortRatioDetailsGrid");
      treasury.roa.util.hideLoading();
      console.log("Error occurred while retrieving results.");
    });
  }
})();
