"use strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.roa = window.treasury.roa || {};
    window.treasury.roa.util = {
        showLoading: _showLoading,
        hideLoading: _hideLoading,
        showErrorMessage: _showErrorMessage,
        setupDateFilter: _setupDateFilter,
        clearGrid: _clearGrid,
        getFormattedMessage: _getFormattedMessage,
        resizeCanvasOnGridChange: _resizeCanvasOnGridChange
    };

    function _showLoading() {
        document.getElementById("roaLoading").show();
    }

    function _hideLoading() {
        document.getElementById("roaLoading").hide();
    }

    function _showErrorMessage(message) {

        // TODO: Check with Subhasri if we should use arcHelper for this
        var errorDialog = document.getElementById("errorMessage");

        errorDialog.reveal({
            'title': 'Error',
            'content': "<center><strong>" + message +
            "</strong></center>",
            'modal': true,
            'buttons': [{
                'html': 'OK',
                'callback': function() {
                    this.visible = false
                },
                'position': 'right'
            }]
        });
    }

    function _clearGrid(gridId) {
        treasury.common.grid.clearGrid(gridId);
        if ($("#" + gridId).prev().prev().is("p") ||
            $("#" + gridId).prev().prev().is("center")) {
            $("#" + gridId).prev().prev().remove();
        } else if ($("#" + gridId).prev().is("p") ||
            $("#" + gridId).prev().is("center")) {
            $("#" + gridId).prev().remove();
        }
        var groupingHeader = document.getElementById(gridId+'-drop-element-holder');
        if(groupingHeader != null) {
          groupingHeader.innerHTML = "";
        }
    }

    function _setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
        dateFormat) {
        dateDiv.datepicker({
            dateFormat: dateFormat,
            changeMonth: true,
            changeYear: true,
            minDate: minDate,
            maxDate: maxDate,
            showOn: "both",
            buttonText: "<i class='fa fa-calendar'></i>",
        }).datepicker("setDate", date);
        dateDiv.change(function() {
            validateDate(dateDiv.val(), {
                errorDiv: errorDiv,
                minDate: minDate,
                maxDate: maxDate,
                dateFormat: dateFormat,
            });
        });
    }

    function _getFormattedMessage(message, className) {
        return "<center><p class='arc-message" +
            (className == '' ? className : '--' + className) +
            "' style='width: 30%;'>" + message + "</p></center>";
    }

    function _resizeCanvasOnGridChange(gridName, gridId) {
        var leftPanel = document.getElementById('roaFilterSidebar');
        // var rightPanel = document.getElementById('financingTermSidebar');
        leftPanel.addEventListener('stateChange', function() {
            var parentArcLayout = document.getElementById('parentArcLayout');
            parentArcLayout.setLayout();
            gridName.resizeCanvas();
        });
        // rightPanel.addEventListener('stateChange', function() {
        // var parentArcLayout = document.getElementById('parentArcLayout');
        // parentArcLayout.setLayout();
        // gridName.resizeCanvas();
        // });

        if (gridId != undefined) {
            $('#' + gridId).height($('#' + gridId).height() + 11);
        }
    }

})();
