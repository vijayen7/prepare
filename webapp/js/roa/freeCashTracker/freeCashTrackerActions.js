"use strict";
var freeCashTrackerDetailsGrid;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.freeCashTrackerActions = {
    loadFreeCashTrackerSearchResults: _loadFreeCashTrackerSearchResults
  };

  function _loadFreeCashTrackerSearchResults(parameters) {
    treasury.roa.util.showLoading();
    var serviceCall = $.ajax({
      url: "/treasury/counterpartyRelationship/free-cash-tracker-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {
        treasury.roa.util.clearGrid("freeCashTrackerDetailsGrid");
        treasury.roa.util.hideLoading();
        document.getElementById('freeCashTrackerDetailsGrid').removeAttribute("hidden");
        if (data && data.resultList && data.resultList.length) {
          document.getElementById('roaMessage').setAttribute("hidden", true);
          freeCashTrackerDetailsGrid = new dportal.grid.createGrid(
            $("#freeCashTrackerDetailsGrid"), data.resultList,
            treasury.roa.freeCashTrackerColumnsConfig
            .getResultColumns(),
            treasury.roa.freeCashTrackerGridOptions
            .getResultOptions());
          treasury.roa.util.resizeCanvasOnGridChange(freeCashTrackerDetailsGrid,
            'freeCashTrackerDetailsGrid');
        } else if (data && data.errorMessage) {
          treasury.roa.util.showErrorMessage(data.errorMessage);
          console.log("Error in fetching free cash tracker results");
        } else if (data && (data.length == null || data.length == 0)) {
          document.getElementById('roaMessage').removeAttribute("hidden");
          document
            .getElementById('roaMessage').innerHTML = treasury.roa.util.getFormattedMessage(
              'No results found matching selected criteria.', '');
        }
      });
    serviceCall.fail(function(xhr, text, errorThrown) {
      treasury.roa.util.clearGrid("freeCashTrackerDetailsGrid");
      treasury.roa.util.hideLoading();
      console.log("Error occurred while retrieving results.");
    });
  }
})();
