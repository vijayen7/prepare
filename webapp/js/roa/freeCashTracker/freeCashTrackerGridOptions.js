"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.freeCashTrackerGridOptions = {
    getResultOptions: _getResultOptions,
    getGroupColumns: _getGroupColumns
  };

  function _getResultOptions() {
    var groupColumns = _getGroupColumns();
    return {
      nestedTable: true,
      nestedField: "currency",
      expandTillLevel: -1,
      autoHorizontalScrollBar: true,
      showHeaderRow: true,
      enableMultilevelGrouping: {
        hideGroupingHeader: false,
        showGroupingKeyInColumn: false,
        initialGrouping: groupColumns,
      },
      expandCollapseAll: true,
      highlightRowOnClick: true,
      sortList: [{
        columnId: "cpeFamily",
        sortAsc: true
      }],
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      exportToExcel: {
        splitNestedFieldIntoMultipleColumns: false,
      },
      sheetName: "Free Cash Tracker Report",
      useAvailableScreenSpace: true
    };
  }

  function _getGroupColumns() {
    var groupColumns = ['cpeFamily', 'legalEntityFamily', 'agreementType', 'cpeEntity', 'legalEntity'];
    return groupColumns;
  }
})();
