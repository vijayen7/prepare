"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.freeCashTracker = {
    loadFreeCashTrackerSearchResults: _loadFreeCashTrackerSearchResults
  };

  function _loadFreeCashTrackerSearchResults() {
    treasury.roa.util.clearGrid('freeCashTrackerDetailsGrid');

    var parameters = roaSearchFilterGroup.getSerializedParameterMap();
    if (document.getElementById('roaFilterSidebar').state == 'expanded') {
      document.getElementById('roaFilterSidebar').toggle();
    }
    treasury.roa.freeCashTrackerActions.loadFreeCashTrackerSearchResults(parameters);
  }
})();
