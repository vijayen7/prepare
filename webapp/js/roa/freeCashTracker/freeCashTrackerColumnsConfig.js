"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.roa = window.treasury.roa || {};
  window.treasury.roa.freeCashTrackerColumnsConfig = {
    getResultColumns: _getResultColumns
  };

  function _getResultColumns() {
    return [{

      id: "cpeFamily",
      type: "text",
      name: "Counterparty Entity Family",
      field: "cpe_family",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "legalEntityFamily",
      type: "text",
      name: "Legal Entity Family",
      field: "legal_entity_family",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreement_type",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "cpeEntity",
      type: "text",
      name: "Counterparty Entity",
      field: "counterparty_entity",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legal_entity",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "currency",
      type: "text",
      name: "Currency",
      field: "currency",
      sortable: true,
      headerCssClass: "aln-rt b"
    }, {
      id: "marginExcess",
      type: "number",
      name: "Margin Excess (USD)",
      field: "margin_excess",
      formatter: marginExcessFormatter,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: marginExcessAggregator,
      groupTotalsFormatter: summationFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      toolTip: "Margin Excess refers to the excess (or deficit) collateral balance maintained at the counterparty and includes collateral in transit."
    }, {
      id: "debitBalance",
      type: "number",
      name: "Debit Balance (USD)",
      field: "debit_balance",
      formatter: dpGrid.Formatters.Number,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: SummationAggregator,
      groupTotalsFormatter: summationFormatter,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      toolTip: "Debit Balance refers to the net cash balance after subtracting the short market value."
    }];
  }

  function summationFormatter(totals, columnDef, placeHolder, isExportToExcel) {
    var value = totals.sum[columnDef.field];
    return isExportToExcel ? value : dpGrid.Formatters.Number(null, null, value, columnDef, null);
  }

  function marginExcessFormatter(totals, columnDef, placeHolder, isExportToExcel) {
    return isExportToExcel ? "" : "<div class='aln-rt'></div>";
  }

  function marginExcessAggregator(field) {
    this.field_ = field;

    this.init = function() {
      this.sum_ = null;
    };
    this.accumulate = function(item) {
      var val = item["margin_excess"];
      if (val != null && val !== "" && !isNaN(val)) {
        this.sum_ += Math.round(val);
      }
    };

    this.storeResult = function(groupTotals) {
      if (!groupTotals.sum) {
        groupTotals.sum = {};
      }
      groupTotals.sum["margin_excess"] = this.sum_;
    };
  }

  function SummationAggregator(field) {
    this.field_ = field;
    this.init = function() {
      this.sum_ = null;
    };
    this.accumulate = function(item) {
      var val = item[this.field_];
      if (val != null && val !== "" && !isNaN(val) && item.gridParent == "root") {
        this.sum_ += Math.round(val);
      }
    };

    this.storeResult = function(groupTotals) {
      if (!groupTotals.sum) {
        groupTotals.sum = {};
      }
      groupTotals.sum[this.field_] = this.sum_;
    };
  }
})();
