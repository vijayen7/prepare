// treasury.js
// Author : Manish Mulani, 20101111

/*******************************************************************************
 * The common initialization infrastructure included here assumes that you
 * follow certain naming conventions. If this is done, the more common search
 * filters and pre-post search actions will be taken care of automatically.
 *
 * The assumptions are:
 * 1. Use the data-area.xml and bottom-panel.xml for the data and footer
 *    sections of your gadget.
 * 2. The id of the div for showing error msgs : errorMsgDiv-__MODULE_ID__
 * 3. The id of the div for showing loading image : loading-__MODULE_ID__
 * 4. The id of the date filter : date__MODULE_ID__
 * 5. The id of the strategy select list : strategies__MODULE_ID__
 * 6. The id of the entity family select list: shaw_entity_families__MODULE_ID__
*******************************************************************************/

/*
 * All filters across gadgets will be loaded in this global variable. Thereby
 * We will not have to invoke action for loading filters for every gadget.
 */
var allTreasuryFilters = new Object();
var reportURLFunctions = {};

loadDateFilter();

function loadLegalEntityFamilyFilter()
{
    if (allTreasuryFilters.descoEntityFamilies == null) {
        $.ajax({
            url  : "/treasury/data/load-legal-entity-family-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.descoEntityFamilies = data.descoEntityFamilies;
            }).fail(function(xhr, text, errThrown) {
                console.log("Legal Entity Family filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadLegalEntityFilter()
{
    if (allTreasuryFilters.descoEntities == null) {
        $.ajax({
            url  : "/treasury/data/load-legal-entity-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.descoEntities = data.descoEntities;
            }).fail(function(xhr, text, errThrown) {
            	console.log("Legal Entity filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadDateFilter()
{
    if (allTreasuryFilters.tMinusOneFilterDate == null) {
        $.ajax({
            url  : "/treasury/data/load-date-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.tMinusOneFilterDate = data.tMinusOneFilterDate;
                allTreasuryFilters.tMinusTwoFilterDate = data.tMinusTwoFilterDate;
            }).fail(function(xhr, text, errThrown) {
                alert("Date filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadBaseRateFilter(currencyId){
  var parameters = {};
    parameters["currencyId"] = currencyId;
    
    $.ajax({
        url  : "/treasury/data/load-base-rate-filter",
        async : false,
        data : parameters,
        type : "POST"
        }).done(function(data){
            allTreasuryFilters.baseRates = data.baseRates;
        }).fail(function(xhr, text, errThrown) {
            alert("Base Rate filter couldn't be loaded. Please contact Tech Support.");
        });
}

function loadAgreementTypeFilter()
{
    if (allTreasuryFilters.agreementTypes == null) {
        $.ajax({
            url  : "/treasury/data/load-agreemen-type-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.agreementTypes = data.agreementTypes;
            }).fail(function(xhr, text, errThrown) {
                alert("Agreement Type filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}


function loadCpeFamilyFilter()
{
    if (allTreasuryFilters.counterPartyEntityFamilies== null) {
        $.ajax({
            url  : "/treasury/data/load-cpe-family-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.counterPartyEntityFamilies = data.counterPartyEntityFamilies;
            }).fail(function(xhr, text, errThrown) {
                alert("CPE Family filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadCpeFilter()
{
    if (allTreasuryFilters.cpes== null) {
        $.ajax({
            url  : "/treasury/data/load-cpe-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.cpes = data.cpes;
            }).fail(function(xhr, text, errThrown) {
                alert("CPE filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadOwnershipEntityFilter()
{
    if (allTreasuryFilters.ownershipEntities== null) {
        $.ajax({
            url  : "/treasury/data/load-ownership-entity-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.ownershipEntities = data.ownershipEntities;
            }).fail(function(xhr, text, errThrown) {
                alert("Ownership Entity filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadBusinessUnitGroupFilter()
{
    if (allTreasuryFilters.businessUnitGroups == null) {
        $.ajax({
            url  : "/treasury/data/load-business-unit-group-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.businessUnitGroups = data.businessUnitGroups;
            }).fail(function(xhr, text, errThrown) {
                alert("Business Unit Group filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadBusinessUnitFilter()
{
    if (allTreasuryFilters.businessUnits == null) {
        $.ajax({
            url  : "/treasury/data/load-business-unit-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.businessUnits = data.businessUnits;
            }).fail(function(xhr, text, errThrown) {
                alert("Business Unit filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadBrokerRevenueBusinessUnitFilter()
{
    if (allTreasuryFilters.brokerRevenueBusinessUnits == null) {
        $.ajax({
            url  : "/treasury/data/load-broker-revenue-business-unit-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.brokerRevenueBusinessUnits = data.brokerRevenueBusinessUnits;
            }).fail(function(xhr, text, errThrown) {
                alert("Broker Revenue Business Unit filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadExecBrokerFilter()
{
    if (allTreasuryFilters.execBrokerFamilies == null
            || allTreasuryFilters.cpeAndExecBrokerFamilies == null
            || allTreasuryFilters.execBrokers == null) {
        $.ajax({
            url  : "/treasury/data/load-exec-broker-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.execBrokerFamilies = data.execBrokerFamilies;
                allTreasuryFilters.cpeAndExecBrokerFamilies = data.cpeAndExecBrokerFamilies;
                allTreasuryFilters.execBrokers = data.execBrokers;
            }).fail(function(xhr, text, errThrown) {
                alert("Exec Broker filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadGboTypeFilter()
{
    if (allTreasuryFilters.gboTypes == null) {
        $.ajax({
            url  : "/treasury/data/load-gbo-type-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.gboTypes = data.gboTypes;
            }).fail(function(xhr, text, errThrown) {
                alert("GBO Type filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadMarginCalculatorFilter()
{
    if (allTreasuryFilters.marginCalculators == null) {
        $.ajax({
            url  : "/treasury/data/load-margin-calculator-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.marginCalculators = data.marginCalculators;
            }).fail(function(xhr, text, errThrown) {
                alert("Margin Calculator Family filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadWorkflowStatusFilter()
{
    if (allTreasuryFilters.workflowStatus == null) {
        $.ajax({
            url  : "/treasury/jsp/filter/load-workflow-status-filter.jsp",
            async : false
            }).done(function(data){
                allTreasuryFilters.workflowStatus = data.workflowStatus;
            }).fail(function(xhr, text, errThrown) {
                alert("Workflow Status filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadDataAvailabilityStatusFilter()
{
    if (allTreasuryFilters.dataAvailabilityStatus == null) {
        $.ajax({
            url  : "/treasury/jsp/filter/load-data-availability-status-filter.jsp",
            async : false
            }).done(function(data){
                allTreasuryFilters.dataAvailabilityStatus = data.dataAvailabilityStatus;
            }).fail(function(xhr, text, errThrown) {
                alert("Data Availability Status filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadAssetLiquidityFilter()
{
    if (allTreasuryFilters.assetLiquidity == null) {
        $.ajax({
            url  : "/treasury/jsp/filter/load-asset-liquidity-filter.jsp",
            async : false
            }).done(function(data){
                allTreasuryFilters.assetLiquidity = data.assetLiquidity;
            }).fail(function(xhr, text, errThrown) {
                alert("Asset Liquidity filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}

function loadFinancingStatusTypeFilter()
{
    if (allTreasuryFilters.financingStatusType == null) {
        $.ajax({
            url  : "/treasury/jsp/filter/load-financing-status-type-filter.jsp",
            async : false
            }).done(function(data){
                allTreasuryFilters.financingStatusType = data.financingStatusType;
            }).fail(function(xhr, text, errThrown) {
                alert("Financing Status Type filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}


function loadBrokerRevenueSignOffYear()
{
    if (allTreasuryFilters.brokerRevenueSignOffYear == null) {
        $.ajax({
            url  : "/treasury/service/brokerRevenueService/getBrokerRevenueSignOffYear?format=json",
            async : false
            }).done(function(data){
                allTreasuryFilters.brokerRevenueSignOffYear = data;
            }).fail(function(xhr, text, errThrown) {
                alert("Unable to get broker revenue signoff year. Please contact Tech Support.");
            });
    }
}

function loadBundleGroupTypeFilter(moduleId)
{
    if (allTreasuryFilters.bundleGroupTypes== null) {
        $.ajax({
            url  : "/treasury/data/load-bundle-group-type-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.bundleGroupTypes = data.bundleGroupTypes;
            }).fail(function(xhr, text, errThrown) {
                alert("Bundle Group Type filter couldn't be loaded. Please contact Tech Support.");
            });
    }

    if (allTreasuryFilters.bundleGroupTypes!= null) {
	    var bundleGroupTypeId = 'bundleGroupType-' + moduleId;
	    populateSelectList(allTreasuryFilters.bundleGroupTypes, bundleGroupTypeId);
    }
}

function loadBundleGroupFilter()
{
    if (allTreasuryFilters.bundleGroups== null) {
        $.ajax({
            url  : "/treasury/data/load-bundle-group-filter",
            async : false
            }).done(function(data){
                allTreasuryFilters.bundleGroups = data.bundleGroups;
            }).fail(function(xhr, text, errThrown) {
                alert("Bundle Groups filter couldn't be loaded. Please contact Tech Support.");
            });
    }
}
function loadAgreementIdFilter() {
    if (allTreasuryFilters.agreementIds == null) {
        $
                .ajax({
                    url : "/treasury/data/load-agreemen-id-filter",
                    async : false
                })
                .done(function(data) {
                    allTreasuryFilters.agreementIds = data.agreementIds;
                })
                .fail(
                        function(xhr, text, errThrown) {
                            alert("Agreement Id filter couldn't be loaded. Please contact Tech Support.");
                        });
    }
}
function loadFinancingStyleFilter() {
    if (allTreasuryFilters.financingStyles == null) {
        $
                .ajax({
                    url : "/treasury/data/load-financing-style-filter",
                    async : false
                })
                .done(function(data) {
                    allTreasuryFilters.financingStyles = data.financingStyles;
                })
                .fail(
                        function(xhr, text, errThrown) {
                            alert("Financing Style filter couldn't be loaded. Please contact Tech Support.");
                        });
    }
}
function loadFinancingTypeFilter() {
    if (allTreasuryFilters.financingTypes == null) {
        $
                .ajax({
                    url : "/treasury/data/load-financing-type-filter",
                    async : false
                })
                .done(function(data) {
                    allTreasuryFilters.financingTypes = data.financingTypes;
                })
                .fail(
                        function(xhr, text, errThrown) {
                            alert("Financing Type couldn't be loaded. Please contact Tech Support.");
                        });
    }
}

// Method to populate the select filters
//
// Parameters :
// 1. filterData should be of the form :
// [ [ id1 , name1 ], [ id2, name2 ], .... ]
//
// 2. selectId : ID in the DOM to identify the select.
//
function populateSelectList(filterData, selectId) {
    var itemList = filterData;

    if (itemList == null || itemList.length == 0) {
        return;
    }

    if (selectId == null) {
        return;
    }

    var optionsHTML = "";
    for(var i = 0; i < itemList.length; i++)
    {
        if (itemList[i][1] != "") {
            optionsHTML +=
            "<option value=\"" + itemList[i][0] + "\">" + itemList[i][1] + "</option>";
        }
    }

    $('#'+selectId).append(optionsHTML);
}

var stringifyParams = function(params){
    var str = "";
    for (var property in params) {
        if (params.hasOwnProperty(property)) {
            str += "&" + property + "=" + params[property];
        }
    }
    return "?"+str.substr(1,str.lenth);
}

function sleep (time) {
  return new Promise(function(resolve) {setTimeout(resolve, time);});
}

var generateReportURL = function(ajaxURL, params, dataResultKey){
    var reportURL = location.origin + ajaxURL + stringifyParams(params) + "&_outputKey=" + dataResultKey;
    console.log("Copying report url - "+reportURL);

    var feedbackDiv = document.createElement('div');
    feedbackDiv.id = 'report-url-feedback';
    feedbackDiv.className = 'quick-feedback';
    feedbackDiv.innerHTML = '<a href="'+ reportURL +'">Report URL</a> ';

    if(copyToClipboard(reportURL)){
        feedbackDiv.innerHTML += "copied to clipboard";
        document.getElementsByTagName('body')[0].appendChild(feedbackDiv);
    }else{
        feedbackDiv.innerHTML += "could not be copied to clipboard.";
    }
    document.getElementsByTagName('body')[0].appendChild(feedbackDiv);

    sleep(5000).then(function() {
        feedbackDiv.parentNode.removeChild(feedbackDiv);
    })
}

function copyToClipboard(text) {
    if (window.clipboardData && window.clipboardData.setData) {
        // IE specific code path to prevent textarea being shown while dialog is visible.
        return clipboardData.setData("Text", text); 

    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        } catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return false;
        } finally {
            document.body.removeChild(textarea);
        }
    }
    return false;
}

// This is the onload function for the gadget which initializes the common
// filters. Any specific filters have to be initialized in the gadgets.
function initializeGadget(moduleId, date)
{
    if(reportURLFunctions[moduleId]){
        document.querySelector('#m-' + moduleId + ' .g-opt-ref').insertAdjacentHTML('afterend', 
         '<i class="gad-icon fa fa-link hand g-opt-report-link g-opt-report-link-' + moduleId + '" title="Copy Report Link"></i>');
        document.querySelector(".g-opt-report-link-"+moduleId).addEventListener('click', reportURLFunctions[moduleId], false);
    }

    if ($('#loading-' + moduleId) != null) {
        $('#loading-' + moduleId).hide();
    }


    if (treasury.defaults.disableFootnote == 'true') {
        $(".footnote_hideable-" + moduleId).hide();
    }

    // Export To Excel
    if ($("#export-to-excel" + moduleId).length > 0)
    {
        $("#export-to-excel" + moduleId).click(
            function()
            {
                $("#grid-div-" + moduleId + "-header").find(".exportToExcel").click();
            });
    }

    // Show filter row if present.
    if ($("#filter" + moduleId).length > 0)
    {
        $("#filter" + moduleId).click(
            function()
            {
                $("#grid-div-" + moduleId + "-header").find(".filter").click();
            });
    }

    if ($("#errorMsgDiv-" + moduleId) != null) {
        $("#errorMsgDiv-" + moduleId).hide();
    }

    // Calendar setup
    // Assuming default date is T-1.
    if (date == null) {
        date = allTreasuryFilters.tMinusOneFilterDate;
    }

    if ($("#bottom-date" + moduleId).length > 0) {
        $("#bottom-date" + moduleId).text(date);
    }

    if ($("#date" + moduleId).length > 0) {
        if (treasury.defaults.date === undefined) {
            $("#date" + moduleId).val(date);
        } else {
            $("#date" + moduleId).val(treasury.defaults.date);
        }

        var options =
           { dateFormat: 'yy-mm-dd',
             changeMonth: true,
             changeYear: true,
             minDate: Date.parse('1970-01-01'),
             maxDate: Date.today(),
             showOn: "button",
             buttonText: "",
             buttonImage: "/static/images/calendar.gif",
             buttonImageOnly: true };

        $("#date" + moduleId).datepicker(options);
    }
}

// Hides and shows the DOM elements as necessary just before the search
function setupPreSearch(moduleId)
{
    $("#errorMsgDiv-" + moduleId).hide().html('');
    $("#form-" + moduleId + " *:not(table, tr, td, tbody, span)").disabled();    //jQuery Selector $(P C) = $(parent children)
    $('#info-' + moduleId).hide().html('');
    $("#grid-div-" + moduleId).hide();
    $("#chart-" + moduleId).hide();
    $('#loading-' + moduleId).show();
}

// When data is received, hide loading, show main form, initialize dates etc.
function initializeOnData(moduleId, params)
{
    $("#loading-" + moduleId).hide();
    $("#form-" + moduleId + " *:not(table, tr, td, tbody, span)").disabled(false);
    $("#grid-div-" + moduleId + "-header").empty();
    $("#grid-div-" + moduleId).empty();
    $("#chart-" + moduleId).empty();

    if ($("#dateRangePicker-" + moduleId).length > 0) {
        $("#dateRangePicker-" + moduleId).daterangepicker("setDate", params.startDateString, params.endDateString);
    }
    if ($("#date" + moduleId).length > 0) {
        $("#date" + moduleId).val(params.dateParam);
    }
    if ($("#bottom-date" + moduleId).length > 0) {
        $("#bottom-date" + moduleId).text(params.dateParam);
    }

    if ($("#export-to-excel" + moduleId) != null) {
        $("#export-to-excel" + moduleId).hide();
    }

    var toDay = (new Date()).toString("yyyy-MM-dd");

    if ($("#bottom-date-PFNextImg-" + moduleId).length > 0 &&
        $("#date" + moduleId).length > 0)
    {
        if ($("#date" + moduleId).text() === toDay) {
            $("#bottom-date-PFNextImg-" + moduleId).hide();
        }
        else {
            $("#bottom-date-PFNextImg-" + moduleId).show();
        }
    }
}

function freezeGadget(moduleId)
{
    // Disable drag option
    // $("#m-" + moduleId + ".gad .gad-head").unbind('mousedown');

    // Hide move and remove options from setting menu
    $("#m-" + moduleId + ".gad .g-opt-move").hide();
    $("#m-" + moduleId + ".gad .g-opt-rem").hide();

    // Hide maximize
    $("#m-" + moduleId + ".gad img.sprite-intranet-maximize").hide();
}

// Displays the specified message in the specified DOM element
// We should follow a naming convention for error message div element to
// keep it's id as "errorMsgDiv__MODULE_ID__" always.
function displayErrorMsg(moduleId, msg, errorMsgDiv$)
{
    if (!errorMsgDiv$) {
        errorMsgDiv$ = $("#errorMsgDiv-" + moduleId);
    }
    errorMsgDiv$.show().html('<font color="#FF0000">' + msg + '</font>');
}

// this method is to handle the error that occurs when ajax call to search
// the data fails.
function trsySearchErrorHandler(moduleId, dateParameter, text, xhr)
{
    initializeOnData(moduleId, dateParameter);

    var msg = 'Error Occurred!';

    if ( text == 'timeout' ) {
       msg = 'Query has timed out. please modify your filters and try again.';
    }
    ArcMessageHelper.initialize();
	ArcMessageHelper.showMessage("critical", msg, xhr.responseText.split("\n")[1], {exceptionMessage: xhr.responseText});

	displayErrorMsg(moduleId, msg);
}

// Generic implementation to reset a MSB.
// Parameters :
// selectedObject : { ids : [1,2,3], values : "obj1, obj2" }
// msb$ : jQuery object representing MSB
function resetMSB(selectedObject,
                  msb$)
{
    selectedObject.ids = [-1];
    selectedObject.values = "All";

    msb$.attr("title", selectedObject.values);
    msb$.html("All");
    msb$.tooltip();
}

// Generic implementation of onSelectionMSB.
//
//Parameters :
// selectedObject : { ids : [1,2,3], values : "obj1, obj2" }
// msbId : id of the MSB div.
//
// Usage : onSelectionMSB(selectedGBOTypes__MODULE_ID__,
//                        $('#gbo_types_msb__MODULE_ID__')
function onSelectionMSB(selectedObject,
                        msbId)
{
    var msb$ = $("#" + msbId);
    var selectedValues = msb$.html();

    // When user selects a few items and doesn't add it to the selected list
    // then the html of the element is set to "Select..."
    if (selectedValues == "Select...")
    {
        alert("You did not select any element");
        // Resetting the MSB
        resetMSB(selectedObject, msb$);
        return;
    }

    msb$.attr("title", selectedValues);
    msb$.tooltip();

    var selectedItems = msb$.html().split(", ");
    if (selectedItems.length > 1) {
        msb$.html(selectedItems[0] + "...");
    }
    else {
        msb$.html(selectedItems[0]);
    }

    selectedObject.ids = getMSBSelectedItems(msbId);
    selectedObject.values = selectedValues;
}
//Feedback button click event.
if(document.querySelector(".g-opt-feedback-cont")){
$(document).on("click", ".g-opt-feedback-cont", function() {
    $("#mail-dialog").empty().html("<img src='/static/images/indicator.gif' />&nbsp;&nbsp;Loading...");
    $("#mail-dialog").dialog("option", "title", "E-mail Gadget");
    $("#mail-dialog").dialog("open");
    // Passed module Id and Name as HTML5 data attributes.
    $("#mail-dialog").load("/treasury/data/send-feedback", $.param({moduleId: $(this).data("id"), subject: $(this).data("name")}));
});
}
