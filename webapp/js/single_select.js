/**
 * Single Select class (prototypal OO) - wrapper over select2 library.
 * Documentation : http://ivaynberg.github.io/select2/
 **/
(function($){
    $.extend(true, window, 
    {
        "treasury" :
        {
            "SingleSelect" : TreasurySingleSelect
        }
    });

    function TreasurySingleSelect(paramString, selectId, filterList, defaultSelectedId, width, placeholder) {
        this.paramString = paramString;
        this.selectId = selectId;
        this.filterList = filterList;
        this.selectedId = defaultSelectedId;
        this.placeholder = placeholder;

        this.select$ = $("#" + selectId);
        var data = [];
        var groupHeaderMap = {};
        // The following code groups the list under a group if specified
        // so say if the structure is [ [1, BU1], [2, BU2, Header1], [3, BU3, Header1]]
        // the following code would group it as 
        // [ 1, BU1], [ Header1, [[2, BU2],[3, BU3]]]
        // Note that the group header has no id we dont want it to be selectable in 
        // in the select list
        
        for (var i = 0; i < filterList.length; i++) {
            // skipping All
            if (filterList[i][0] != -1) {
                // Assuming if the third element is the defined, we need to
                // group the data under that name of the third element
                if(filterList[i][2]) {
                    if(groupHeaderMap[filterList[i][2]]) {
                        // Add the record to the children
                        groupHeaderMap[filterList[i][2]]["children"].push(
                            {id : filterList[i][0], text : filterList[i][1]});
                    }
                    else {
                        groupHeaderMap[filterList[i][2]] = { 
                            text : filterList[i][2], 
                            children : [{id : filterList[i][0], text : filterList[i][1]}]
                        };
                    }
                } 
                else {
                    data.push({id : filterList[i][0], text : filterList[i][1]});
                }
            }
        }
        // Fetch all the values and append it to the actual structure
        Object.keys(groupHeaderMap).forEach(function(key) {
            data.push(groupHeaderMap[key]);
        });

        defaultSelectedId = getDefaultId(data,defaultSelectedId);

        if (placeholder !== undefined && placeholder != null) {
            this.select$.select2({data : data,
                width : (width ? width : "200px"),
                placeholder : placeholder,
                escapeMarkup: function(m) { return m; }});
        } else {
            this.select$.select2({data : data,
                width : (width ? width : "200px"),
                escapeMarkup: function(m) { return m; }});
            this.select$.select2("val", defaultSelectedId);
        }

        
    }

/*
 *  Function to check whether the defaultSelectedId is there in reference data
 *  If not, then it returns the id of the first object in reference data
 */ 

    function getDefaultId(referenceData,defaultSelectedId)
    {
        var defaultSelectedIdFlag = 0;
        for (var object in referenceData)
        {
            if(referenceData[object]["id"] == defaultSelectedId) {
                defaultSelectedIdFlag = 1;
                break;
            }
        }
        if(!defaultSelectedIdFlag)
        {
            defaultSelectedId = referenceData[0]["id"];
        }
        return defaultSelectedId;
    }

    /*
     * Returns map containing a single entry of <key (paramString), value>.
     */
    TreasurySingleSelect.prototype.getSelectedValue = function() {
        if (this.select$.select2("val")) {
            return this.select$.select2("data").text;
        } else {
          return "";  
        }
    };

    /*
     * Returns map containing a single entry of <key (paramString), id>.
     */
    TreasurySingleSelect.prototype.getSelectedId = function() {
        if (this.select$.select2("val")) {
            return this.select$.select2("val");
        } else {
          return "";  
        }
    };

    /*
     * returns true if at least one item is selected.
     */
    TreasurySingleSelect.prototype.validate = function() {
        return this.select$.select2("val") ? true : false; 
    };

})(jQuery);