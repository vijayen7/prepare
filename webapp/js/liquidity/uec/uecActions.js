"use strict";

var entityUecGrid;
var apfUecGrid;
var masterUecGrid;
var entityUecData;
var apfUecData;
var masterUecData;
var detailsGrid;
var masterUecDetailsGrid;
var apfUecDetailsGrid;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.uec = window.treasury.uec || {};
  window.treasury.uec.actions = {
    loadUecData: _loadUecData,
    loadAllUecGrids: _loadAllUecGrids,
    loadUecGrid: _loadUecGrid,
    signOffWorkflow: _signOffWorkflow
  };

  function _loadUecData(parameters) {
    treasury.uec.util.showLoading();
    var serviceCall = $.ajax({
      url: 'treasury/liquidity/search-uec-data',
      data: parameters,
      type: 'POST',
      dateType: 'json'
    });

      $.when(serviceCall)
        .done(
          function(data) {
            _loadAllUecGrids(data);
            var workflowDetailData = document.getElementById('versionFilter').value.value;
            if (parameters.workflowId > 0) {
              var workflowDetails = workflowDetailData.split('-');
              var workflowMessage = 'Workflow published by '.concat(workflowDetails[1]).concat(' at ').concat(workflowDetails[2]);
              if (workflowDetails.length < 4) {
              treasury.uec.util.isUserAuthorizedToSignOff();
              } else {
                workflowMessage = workflowMessage.concat(' and signed off by ').concat(workflowDetails[3]).concat(' at ').concat(workflowDetails[4]);
                document.getElementById('signOffWorkflow').hidden = true;
              }
              workflowMessage = workflowMessage.concat('.');
              document.getElementById('workflowMessage').innerHTML = workflowMessage;
              document.getElementById('workflowMessage').removeAttribute('hidden');
            } else {
              document.getElementById('workflowMessage').innerHTML = '';
              document.getElementById('workflowMessage').setAttribute('hidden', true);
              document.getElementById('signOffWorkflow').hidden = true;
            }
          });
      serviceCall
        .fail(function(xhr, text, errorThrown) {
          treasury.uec.util.clearAllGrids();
          treasury.uec.util.hideLoading();
          treasury.uec.util
            .showErrorMessage('Error', 'Error occurred while retrieving entity uec data.', false);
        });
    }

  function _loadUecGrid(uecData, uecGridType) {
    var uecColumnData = treasury.uec.uecColumnsConfig.getUecGridColumnsByType(uecGridType);
    var uecGridOptions = treasury.uec.uecGridOptions.getUecGridOptions(uecGridType);
    if(uecData && uecData.length) {
       if(uecGridType != 'entityUecGrid') {

       var columnsToExclude = ['entity', 'ratio', 'comment', 'commentLink', 'netCashUsdDiff', 'capitalAmountUsdDiff', 'ratioDiff', 'prevMonth.ratio'];
       uecGridOptions.onCellClick = function(args, isNotToggle) {
    	  if(args.colId == "commentLink") {
    	  var id = args.item.id;
          var workflowDataId = args.item.workflow_data_id;
          var entityName = args.item.entity;
          var existingComment = args.item.comment != undefined || args.item.comment != null ? args.item.comment : '';
          window.treasury.uec.workflow.loadCommentPopup(id ,workflowDataId,entityName ,existingComment);
    	  }
        var detailGridDiv = document.getElementById("uecTabs").selectedTab == "Master Fund Unencumbered Cash" ?
        'masterUecDetailsGrid' : 'apfUecDetailsGrid';
        treasury.uec.util.clearGrid(detailGridDiv);
        if (args.row != null && columnsToExclude.indexOf(args.colId) < 0
          && typeof args.item[args.colId] != "undefined" &&
          args.item[args.colId] != "0" && args.item[args.colId] != "") {
            var detailGridData;
            var detailGridColumnConfig;
            if(args.colId.includes('prevMonth')) {
              if(args.colId == 'prevMonth.capitalAmountUsd'){
                detailGridData = args.item['prevMonth.underlyingDataForCapitalList'];
                detailGridColumnConfig = treasury.uec.uecColumnsConfig.getUecGridColumnsByType('capitalDetail');
              }
              if(args.colId == 'prevMonth.netCashUsd' ||args.colId == 'prevMonth.usageUsd' ){
                detailGridData = args.item['prevMonth.underlyingDataForNetCashList'];
                detailGridColumnConfig = treasury.uec.uecColumnsConfig.getUecGridColumnsByType('cashDetail');
              }
              if(args.colId == 'prevMonth.grossMtdPnlUsd'){
                detailGridData = args.item['prevMonth.underlyingDataForPNLList'];
                detailGridColumnConfig = treasury.uec.uecColumnsConfig.getUecGridColumnsByType('pnlDetail');
              }
            }

          if(!args.colId.includes('prevMonth')){
              if(args.colId == 'capitalAmountUsd'){
                detailGridData = args.item['underlyingDataForCapitalList'];
                detailGridColumnConfig = treasury.uec.uecColumnsConfig.getUecGridColumnsByType('capitalDetail');
              }
              if(args.colId == 'netCashUsd'||args.colId == 'usageUsd'  ){
                detailGridData = args.item['underlyingDataForNetCashList'];
                detailGridColumnConfig = treasury.uec.uecColumnsConfig.getUecGridColumnsByType('cashDetail');
              }
              if(args.colId == 'grossMtdPnlUsd'){
                detailGridData = args.item['underlyingDataForPNLList'];
                detailGridColumnConfig = treasury.uec.uecColumnsConfig.getUecGridColumnsByType('pnlDetail');
              }
            }

            document.getElementById(detailGridDiv).removeAttribute("hidden");
            var detailGridOptions;
            if(detailGridData && detailGridData.length) {
              if(document.getElementById("uecTabs").selectedTab == "Master Fund Unencumbered Cash") {
                detailGridOptions = treasury.uec.uecGridOptions.getUecGridOptions('detailGrid');
              } else {
                detailGridOptions = treasury.uec.uecGridOptions.getUecGridOptions('detailGrid');
              }
              detailsGrid = new dportal.grid.createGrid
              ($('#' + detailGridDiv), detailGridData, detailGridColumnConfig, detailGridOptions);
              if(document.getElementById("uecTabs").selectedTab == "Master Fund Unencumbered Cash") {
                masterUecDetailsGrid = detailsGrid;
              } else {
                apfUecDetailsGrid = detailsGrid;
              }
            }else{
              treasury.uec.util.clearGrid(detailGridDiv);
              document.getElementById(detailGridDiv).setAttribute("hidden", true);
            }
        } else {
          treasury.uec.util.clearGrid(detailGridDiv);
          document.getElementById(detailGridDiv).setAttribute("hidden", true);
        }
      }
    }
    document.getElementById('uecTabs').removeAttribute("hidden");
    document.getElementById('uecTabs').style.height = '100%';
    document.getElementById(uecGridType).removeAttribute("hidden");
    return new dportal.grid.createGrid(
      $('#' + uecGridType), uecData, uecColumnData, uecGridOptions);
    }else {
        document.getElementById(uecGridType).setAttribute("hidden", true);
        return null;
    }
  }

function _loadAllUecGrids(data) {
  treasury.uec.util.clearAllGrids();
  treasury.uec.util.hideLoading();
  document.getElementById('uecMessage').setAttribute("hidden", true);
  if (data && data.resultList && data.resultList.length) {

    var uecData = data['resultList'];
    masterUecData = uecData[0]['masterOwnershipUnencumberedCashDataList'];
    apfUecData = uecData[0]['apfOwnershipUnencumberedCashDataList'];
    entityUecData = uecData[0]['entityUnencumberedCashDataList'];

    masterUecGrid = _loadUecGrid(masterUecData, 'masterUecGrid');
    apfUecGrid = _loadUecGrid(apfUecData, 'apfUecGrid');
    entityUecGrid = _loadUecGrid(entityUecData, 'entityUecGrid');

    treasury.uec.util.resizeCanvasOnGridChange(masterUecGrid, 'masterUecGrid');

  } else if (data && data.errorMessage) {
    treasury.uec.util.showErrorMessage('Error', data.errorMessage, false);
  } else if (data && (data.length == null || data.length == 0)) {
    document.getElementById('uecMessage').removeAttribute("hidden");
    document.getElementById('uecTabs').setAttribute("hidden", true);
    document
      .getElementById('uecMessage').innerHTML = treasury.uec.util.getFormattedMessage(
        'No results found matching selected criteria.', '');
  }
}

function _signOffWorkflow(parameters) {
  treasury.uec.util.showLoading();
  //treasury.uec.util.clearAllGrids();
  var serviceCall = $.ajax({
    url: 'treasury/liquidity/sign-off-uec-workflow',
    data: parameters,
    type: 'POST',
    dateType: 'json'
  });

  $.when(serviceCall)
    .done(
      function(data) {
        treasury.uec.util.hideLoading();
        document.getElementById('uecTabs').setAttribute("hidden", true);
        document.getElementById('workflowMessage').innerHTML = '';
        document.getElementById('workflowMessage').setAttribute('hidden', true);
        if (data != null) {
          var message = '';
          var messageTitle = '';
          if (data.result == null) {
            messageTitle = 'UEC Workflow';
            message = 'UEC Workflow has been signed off.';
          } else { //Sign-off failed
            messageTitle = 'Error';
            message = data.result;
          }
          treasury.uec.util.showErrorMessage(messageTitle, message, false);
          document.getElementById('uecMessage').innerHTML = 'Perform search to load results';
          document.getElementById('uecMessage').removeAttribute('hidden');
          document.getElementById('signOffWorkflow').hidden = true;
          if (document.getElementById('uecFilterSidebar').state == 'collapsed') {
            document.getElementById('uecFilterSidebar').toggle();
          }
        } else {
          treasury.uec.util.showErrorMessage('Error', 'Error occurred while signing off UEC workflow.', false);
        }
    });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        treasury.uec.util.hideLoading();
        treasury.uec.util.showErrorMessage('Error', 'Error occurred while signing off UEC workflow.', false);
    });
  }

})();
