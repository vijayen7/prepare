"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.uec = window.treasury.uec || {};
  window.treasury.uec.util = {
    showLoading: _showLoading,
    hideLoading: _hideLoading,
    showErrorMessage: _showErrorMessage,
    setupDateFilter: _setupDateFilter,
    clearGrid: _clearGrid,
    getFormattedMessage: _getFormattedMessage,
    resizeCanvasOnGridChange: _resizeCanvasOnGridChange,
    clearAllGrids: _clearAllGrids,
    resizeAllCanvas: _resizeAllCanvas,
    isUserAuthorizedToSignOff: _isUserAuthorizedToSignOff
  };

  function _showLoading() {
    document.getElementById("uecLoading").show();
  }

  function _hideLoading() {
    document.getElementById("uecLoading").hide();
  }

  function _showErrorMessage(title, message, closeWindow) {

    var errorDialog = document.getElementById("errorMessage");

    errorDialog.reveal({
      'title': title,
      'content': "<center><strong>" + message +
        "</strong></center>",
      'modal': true,
      'buttons': [{
        'html': 'OK',
        'callback': function() {
          this.visible = false;
          if(closeWindow) {
            window.close();
          }
        },
        'position': 'right'
      }]
    });
  }

  function _clearGrid(gridId) {
    treasury.common.grid.clearGrid(gridId);
    if ($("#" + gridId).prev().prev().is("p") ||
      $("#" + gridId).prev().prev().is("center")) {
      $("#" + gridId).prev().prev().remove();
    } else if ($("#" + gridId).prev().is("p") ||
      $("#" + gridId).prev().is("center")) {
      $("#" + gridId).prev().remove();
    }
    var groupingHeader = document.getElementById(gridId + '-drop-element-holder');
    if (groupingHeader != null) {
      groupingHeader.innerHTML = "";
    }
  }

  function _setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
    dateFormat) {
    dateDiv.datepicker({
      dateFormat: dateFormat,
      changeMonth: true,
      changeYear: true,
      minDate: minDate,
      maxDate: maxDate,
      showOn: "both",
      onClose: function(dateText, inst) {
       $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
     },
      buttonText: "<i class='fa fa-calendar'></i>",
    }).datepicker("setDate", date);
    dateDiv.change(function() {
      validateDate(dateDiv.val()+'-01', {
        errorDiv: errorDiv,
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: 'yy-mm-dd',
      });
    });
  }

  function _getFormattedMessage(message, className) {
    return "<center><p class='arc-message" +
      (className == '' ? className : '--' + className) +
      "' style='width: 30%;'>" + message + "</p></center>";
  }

  function _resizeCanvasOnGridChange(gridName, gridId) {
    var leftPanel = document.getElementById('uecFilterSidebar');
    if (leftPanel != null) {
      leftPanel.addEventListener('stateChange', function() {
        gridName.resizeCanvas();
      });
    }
    if (gridId != undefined) {
      $('#' + gridId).height($('#' + gridId).height() + 11);
    }
  }

  function _clearAllGrids() {
    treasury.uec.util.clearGrid('masterUecGrid');
    treasury.uec.util.clearGrid('apfUecGrid');
    treasury.uec.util.clearGrid('entityUecGrid');
    treasury.uec.util.clearGrid('masterUecDetailsGrid');
    treasury.uec.util.clearGrid('apfUecDetailsGrid');
  }

  function _resizeAllCanvas() {
     if(document.getElementById('uecFilterSidebar')) {
        if(document.getElementById('uecFilterSidebar').state == 'expanded') {
        document.getElementById('uecFilterSidebar').style.width = '300px';
      } else {
        document.getElementById('uecFilterSidebar').style.width = '20px';
      }
    }

    if (entityUecGrid != null) {
      entityUecGrid.resizeCanvas();
    }
    if (apfUecGrid != null) {
      apfUecGrid.resizeCanvas();
    }
    if (masterUecGrid != null) {
      masterUecGrid.resizeCanvas();
    }
    if (masterUecDetailsGrid != null) {
      masterUecDetailsGrid.resizeCanvas();
    }
    if (apfUecDetailsGrid != null) {
      apfUecDetailsGrid.resizeCanvas();
    }

  }

    function _isUserAuthorizedToSignOff(){
          var serviceCall =  $.ajax({
          url: '/treasury/liquidity/user-auth-to-l2-review',
          type: "GET"
        });

      $.when(serviceCall)
        .done(
          function(data) {
                if(data.result=='true'){
                    document.getElementById('signOffWorkflow').hidden = false;
                }
          });
         serviceCall
                  .fail(function(xhr, text, errorThrown) {
                    treasury.uec.util.clearAllGrids();
                    treasury.uec.util.hideLoading();
                    treasury.uec.util
                      .showErrorMessage('Error', 'Error occurred while getting auth persmissions', false);
                  });
      }
})();
