"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.uec = window.treasury.uec || {};
  window.treasury.uec.workflow = {
    loadUecWorkflowLanding: _loadUecWorkflowLanding,
    loadCommentPopup: _loadCommentPopup,
    saveWorkflow: _saveWorkflow,
    cancelWorkflow: _cancelWorkflow,
    publishWorkflow: _publishWorkflow
  };

    WebComponents.waitFor(function() {
      var tabsContainer = document.querySelector('#uecTabs');
      if (tabsContainer) {
        tabsContainer.initializeTabs(tabsContainer);
      }
      _initializeWorkflow();
    });

  function _initializeWorkflow() {
    $(document).on({
      ajaxStart: function() {
        treasury.uec.util.showLoading();
      },
      ajaxStop: function() {
        treasury.uec.util.hideLoading();
      }
    });
    var parameters = {};
    parameters.dateString = sessionStorage.getItem('workflowDate');
    parameters.workflowId = sessionStorage.getItem('workflowId');
    parameters.workflowState = sessionStorage.getItem('workflowState');
    var workflowIdElement = document.createElement("input");
    workflowIdElement.type = "hidden";
    workflowIdElement.value = sessionStorage.getItem('workflowId');
    workflowIdElement.id = "workflowId";
    document.body.appendChild(workflowIdElement);

    var workflowDateElement = document.createElement("input");
    workflowDateElement.type = "hidden";
    workflowDateElement.value = sessionStorage.getItem('workflowDate');
    workflowDateElement.id = "workflowDate";
    document.body.appendChild(workflowDateElement);

    treasury.uec.util.showLoading();
    treasury.uec.workflow.loadUecWorkflowLanding(parameters);
    _registerHandlers();
    document.getElementById('workflowMessage').innerHTML = 'Workflow started for date : ' + sessionStorage.getItem('workflowDate');
  }

  function _registerHandlers() {
    document.getElementById("uecTabs").addEventListener("stateChange", treasury.uec.util.resizeAllCanvas);
    document.getElementById("saveExitWorkflow").onclick = treasury.uec.workflow.saveWorkflow;
    document.getElementById("cancelWorkflow").onclick = treasury.uec.workflow.cancelWorkflow;
    document.getElementById("publishWorkflow").onclick = treasury.uec.workflow.publishWorkflow;
  }

  function _loadUecWorkflowLanding(parameters) {
    var serviceCall = $.ajax({
      url: 'treasury/liquidity/get-uec-workflow-data',
      data: parameters,
      type: 'POST',
      dateType: 'json'
    });

    $.when(serviceCall)
      .done(
        function(data) {
          treasury.uec.actions.loadAllUecGrids(data);
        });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        treasury.uec.util.clearAllGrids();
        treasury.uec.util.hideLoading();
        treasury.uec.util
          .showErrorMessage('Error', 'Error occurred while retrieving entity uec data.', false);
      });
  }

  function _loadCommentPopup(id, workflowDataId, entityName, existingComment) {

    document.getElementById('comment').value = existingComment;
    document.getElementById('commentPopup').reveal({
      'title': 'Comment for ' + entityName,
      'modal': true,
      'sticky': true,
      'moveable': true,
      'buttons': [{
        'html': 'Save',
        'class': 'button--primary',
        'callback': function() {
          _saveComment(id, workflowDataId, document.getElementById('comment').value);
          document.getElementById('comment').value = '';
          this.visible = false;
        },
        'position': 'center'
      }, {
        'html': 'Cancel',
        'callback': function() {
          document.getElementById('comment').value = '';
          this.visible = false;
        },
        'position': 'center'
      }]
    });
  }

  function _saveComment(id, workflowDataId, comment) {
    var parameters = {};
    var idParts = id.split('_');
    parameters.lureEntityType = idParts[1];
    parameters.comment = comment;
    parameters.user = document.getElementById('uecHeader').getAttribute('user');
    parameters.workflowId = document.getElementById('workflowId').value;
    parameters.dateString = document.getElementById('workflowDate').value;
    parameters.workflowDataId = workflowDataId;

    treasury.uec.util.showLoading();
    var serviceCall = $.ajax({
      url: 'treasury/liquidity/save-uec-workflow-comment',
      data: parameters,
      type: 'POST',
      dateType: 'json'
    });

    $.when(serviceCall)
      .done(
        function(data) {
          var uecData;
          var uecGridType;
          if(data != null && data.result != null) {
            treasury.uec.util
              .showErrorMessage('Error', 'Error occurred while saving comment:' + data.result, false);
          }
          if(document.getElementById("uecTabs").selectedTab == "Master Fund Unencumbered Cash") {
            uecGridType = 'masterUecGrid';
            uecData = masterUecGrid.data;
          } else if(document.getElementById("uecTabs").selectedTab == "APF Unencumbered Cash") {
            uecGridType = 'apfUecGrid';
            uecData = apfUecGrid.data;
          }
          for(var i = 0; i < uecData.length; i++) {
            if(uecData[i].id == id) {
              uecData[i].comment = comment;
              break;
            }
          }

           var reloadedApfGrid = treasury.uec.actions.loadUecGrid(apfUecGrid.data, 'apfUecGrid');
           treasury.uec.util.resizeCanvasOnGridChange(reloadedApfGrid, 'apfUecGrid');
           var reloadedMasterGrid = treasury.uec.actions.loadUecGrid(masterUecGrid.data, 'masterUecGrid');
           treasury.uec.util.resizeCanvasOnGridChange(reloadedMasterGrid, 'masterUecGrid');
           apfUecGrid = reloadedApfGrid;
           masterUecGrid = reloadedMasterGrid;

        });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        treasury.uec.util.hideLoading();
        treasury.uec.util
          .showErrorMessage('Error', 'Error occurred while saving comment.', false);
      });
  }

  //TODO : Following havent been checked
  function _saveWorkflow() {
    treasury.uec.util.showLoading();
    treasury.liquidity.workflowUtils.workflowExecute('SAVED', 'Workflow has been saved.', null);
    treasury.uec.util.hideLoading();
    treasury.uec.util.showErrorMessage("Save", "Workflow has been saved.", true);
  }

  function _publishWorkflow() {
    treasury.uec.util.showLoading();
    treasury.liquidity.workflowUtils.workflowExecute('PUBLISHED', 'Workflow has been published.', null);
    document.getElementById('publishWorkflow').disabled = true;
    document.getElementById('publishWorkflow').title = 'Already published.';
    $('#workflowId').remove();
    sessionStorage.removeItem("workflowId");
    treasury.uec.util.hideLoading();
    treasury.uec.util.showErrorMessage("Publish", "Workflow has been published.", true);
  }

  function _cancelWorkflow() {
    treasury.uec.util.showLoading();
    var returnVal = treasury.liquidity.workflowUtils.workflowExecute('CANCELLED', 'Workflow has been cancelled.', null);
    treasury.uec.util.hideLoading();
    if (returnVal == 1 || returnVal === undefined) {
      treasury.uec.util.showErrorMessage("Cancel", "Workflow has been cancelled.", true);
      $('#workflowId').remove();
      sessionStorage.removeItem("workflowId");
    } else if (returnVal == 0) {
      treasury.uec.util.showErrorMessage("Error", "Workflow instance is not defined.", true);
    }
  }
})();
