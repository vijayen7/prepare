"use strict";
var selectedCell = {
  row: null,
  cell: null
};

(function () {

  window.treasury = window.treasury || {};
  window.treasury.uec = window.treasury.uec || {};
  window.treasury.uec.uecGridOptions = {
    getUecGridOptions: _getUecGridOptions
  };

  function _getUecGridOptions(gridType) {
    var host = '';
    var clientName = document.getElementById("clientName").value;
    var stabilityLevel = document.getElementById("stabilityLevel").value;

    if (clientName === 'desco') {
      if (stabilityLevel == 'prod') {
        host += "http://landing-app.deshaw.c.ia55.net";
      } else if (stabilityLevel == 'uat') {
        host = "http://landing-app.deshawuat.c.ia55.net";
      } else if (stabilityLevel == 'qa') {
        host = "https://mars.arcesium.com";
      } else if (stabilityLevel == 'dev') {
        host = "https://terra.arcesium.com";
      }
    }

    if (gridType != 'detailGrid') {
      return getColumnConfigOptions(gridType);
    }
    else {
      return getOptions();
    }


    function getColumnConfigOptions(gridType) {
      return {
        forceFitColumns: true,
        autoHorizontalScrollBar: true,
        nestedTable: false,
        expandTillLevel: -1,
        highlightRow: false,
        sortList: [{
          columnId: "netCashUsd",
          sortAsc: true
        }],
        summaryRow: true,
        displaySummaryRow: true,
        summaryRowText: "Total",
        editable: true,
        asyncEditorLoading: false,
        autoEdit: false,
        applyFilteringOnGrid: true,
        useAvailableScreenSpace: true,
        exportToExcel: true,
        sheetName: "Uec Report",
        customColumnSelection: true,
        configureColumns: true,
        applyFilteringOnGrid: true,
        enableMultilevelGrouping: {
          groupingControls: true,
          showGroupingTotal: true,
          customGroupingRendering: true,
          isAddGroupingColumn: true
        },
        cellRangeSelection: {
          showAggregations: true,
          multipleRangesEnabled: true
        }
      };


    }


    function getOptions() {
      return {
        forceFitColumns: true,
        autoHorizontalScrollBar: true,
        nestedTable: false,
        expandTillLevel: -1,
        highlightRow: false,
        sortList: [{
          columnId: "netCashUsd",
          sortAsc: true
        }],
        summaryRow: true,
        displaySummaryRow: true,
        summaryRowText: "Total",
        editable: true,
        asyncEditorLoading: false,
        autoEdit: false,
        applyFilteringOnGrid: true,
        useAvailableScreenSpace: true,
        exportToExcel: true,
        sheetName: "Uec Report"
      };
    }
  }
})();
