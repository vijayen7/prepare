"use strict";

var uecSearchFilterGroup;
var uecVersionFilter;
var dateFilterValue;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.uec = window.treasury.uec || {};
  window.treasury.uec.landing = {
    loadUecLandingView: _loadUecLandingViewFilters,
    loadSearchResults: _loadSearchResults,
    resetSearch: _resetSearchFilters,
    loadWorkflowLanding: _loadWorkflowLanding,
    signOffWorkflow: _signOffWorkflow,
    loadPublishedWorkflowVersions: loadPublishedWorkflowVersions
  };

  WebComponents.waitFor(function() {
    var tabsContainer = document.querySelector('#uecTabs');
    if (tabsContainer) {
      tabsContainer.initializeTabs(tabsContainer);
    }
    _initializeUec();
  });

  function _initializeUec() {
    $(document).on({
      ajaxStart: function() {
        treasury.uec.util.showLoading();
      },
      ajaxStop: function() {
        treasury.uec.util.hideLoading();
      }
    });

    _registerHandlers();
    treasury.uec.landing.loadUecLandingView();
  }

  function _registerHandlers() {
    document.getElementById('uecSearch').onclick = treasury.uec.landing.loadSearchResults;
    document.getElementById('resetSearch').onclick = treasury.uec.landing.resetSearch;
    document.getElementById('uecFilterSidebar').addEventListener("stateChange", treasury.uec.util.resizeAllCanvas);
    document.getElementById("uecTabs").addEventListener("stateChange", treasury.uec.util.resizeAllCanvas);
    document.getElementById('startWorkflow').onclick = treasury.uec.landing.loadWorkflowLanding;
    document.getElementById('signOffWorkflow').onclick = treasury.uec.landing.signOffWorkflow;
  }

  function _resetSearchFilters() {
    $('#datePicker').datepicker('setDate', dateFilterValue);
  }

  function _loadUecLandingViewFilters() {
    var errorDialog = document.getElementById('errorMessage');
    errorDialog.visible = false;
    $.when(treasury.loadfilter.defaultDates()).done(
      function(defaultDatesData) {
        dateFilterValue = new Date(treasury.defaults.date || defaultDatesData.tMinusOneFilterDate);
        var minDate = Date.parse('1970-01-01');
        var maxDate = Date.parse('2038-01-01');
        var dateFormat = 'yy-mm';
        treasury.uec.util.setupDateFilter($("#datePicker"),
          $("#datePickerError"), dateFilterValue, minDate, maxDate, dateFormat);
        $('#datePicker').datepicker().on("input change", loadPublishedWorkflowVersions);
        loadPublishedWorkflowVersions(false);
      }
    );
  }

  function _loadSearchResults() {
    if (document.getElementById('datePickerError').innerHTML != '') {
      ArcMessageHelper.showMessage('error', 'Please select a valid date.', null, null);
      return;
    }
    if (new Date(document.getElementById('datePicker').value) == '') {
      ArcMessageHelper.showMessage('warning', 'Please select date.', null, null);
      return;
    }

    treasury.uec.util.clearGrid('entityUecGrid');
    var parameters = {};
    parameters.dateString = document.getElementById('datePicker').value + '-01';
    parameters.workflowId = document.getElementById('versionFilter').value.key;
    if (document.getElementById('uecFilterSidebar').state == 'expanded') {
      document.getElementById('uecFilterSidebar').toggle();
    }
    treasury.uec.actions.loadUecData(parameters);
  }

  function _loadWorkflowLanding() {
    var workflowUnit = "EntityDate";
    var workflowType = "UEC";
    var workflowDate = document.getElementById('datePicker').value + '-01';
    var entityFamilyId = -1;
    var records = workflowUnit.concat("_").concat(workflowType).concat("_")
      .concat(entityFamilyId).concat("_").concat(workflowDate);
    records = records.replace(/-/g, "");

    var parameters = "";
    parameters = "workflowParam.dateStr=" + workflowDate;
    parameters = parameters + "&workflowParam.workflowTypeName=" + workflowType;
    parameters = parameters + "&workflowParam.workflowUnit=" + workflowUnit;
    parameters = parameters + "&workflowParam.entityFamilyId=" + entityFamilyId;
    parameters = parameters + "&records=" + records;
    sessionStorage.setItem("workflowDate", workflowDate);
    sessionStorage.setItem("records", records);

    treasury.uec.util.showLoading();
    var serviceCall = $.ajax({
      url: "/treasury/comet/workflowBegin",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    serviceCall
      .done(function(data) {

        if(document.getElementById('workflowId') != null) {
          $('#workflowId').remove();
        }
        var workflowIdElement = document.createElement("input");
        workflowIdElement.type = "hidden";
        workflowIdElement.value = data.resultList[0].workflowId;
        workflowIdElement.id = "workflowId";
        document.body.appendChild(workflowIdElement);
        treasury.uec.util.hideLoading();

        // Following is taken from agreementWorkflowAction
        // Append workflowId after begin workflow
        var wfDataParam = "workflowParam.workflowId=" + data.resultList[0].workflowId;

        var workflowState = data.resultList[0].workflowState;

        // Conflict scenario popup subject and message
        var conflictSubject = data.resultList[0].subject;
        var conflictMessage = data.resultList[0].message;

        // For new workflow the state will be BEGIN_WORKFLOW,
        // reload the agreement summary screen and open a new
        // child window
        if (workflowState == 'BEGIN_WORKFLOW') {
          sessionStorage.setItem("workflowId", data.resultList[0]['workflowId']);
          sessionStorage.setItem("workflowState", data.resultList[0]['workflowState']);
          window.open('/treasury/liquidity/uecWorkflow');
        }

        // A workflow already started by the same user
        if (workflowState == 'RESUME_WORKFLOW') {


          // On Cancel workflow clearing the workflow conflict
          // popup, cancelling the workflow and closing dialog
          var cancelWorkflow = document
            .getElementById('cancelWorkflow');
          cancelWorkflow.disabled = false;
          cancelWorkflow.onclick = function() {

            document
              .querySelector('#resumeWorkflowPopup').visible = false;

            // Cancel the workflow and closing dialog
            treasury.liquidity.workflowUtils.workflowExecute('CANCELLED', 'Workflow has been cancelled.', null);
            treasury.uec.util.showErrorMessage("Cancel", "Workflow has been cancelled.", false);
            $('#workflowId').remove();
            sessionStorage.removeItem("workflowId");
            document.getElementById('resumeWorkflowPopup').visible = false;
            _loadWorkflowLanding();
          }

          // On resume workflow clearing the workflow conflict
          // popup, resuming the workflow and
          var resumeWorkflow = document
            .getElementById('resumeWorkflow');
          resumeWorkflow.disabled = false;
          resumeWorkflow.onclick = function() {

            document
              .querySelector('#resumeWorkflowPopup').visible = false;

            // Resume workflow and load workflow screen
            var resumeWorkflowCall = $.ajax({
              url: "/treasury/comet/resumeWorkflow",
              data: wfDataParam,
              type: "POST",
              dataType: "json"
            });
            resumeWorkflowCall
              .done(function(resumeData) {
                sessionStorage.setItem("workflowId", resumeData.resultList[0]['workflowId']);
                sessionStorage.setItem("workflowState", resumeData.resultList[0]['workflowState']);
                window.open('/treasury/liquidity/uecWorkflow');
              });
          }

          // On dismiss popup clear and hide the workflow
          // popup
          var dismissMessage = document
            .getElementById('dismissMessage');
          dismissMessage.onclick = function() {
            _dismissPopup();
          }

          // Set properties of workflow conflict popup and
          // reveal
          var resumeWorkflowMessage = document
            .getElementById("resumeWorkflowMessage");
          var subject = document.getElementById("subject");
          resumeWorkflowMessage.value = conflictMessage;
          subject.value = conflictSubject;

          document.querySelector('#resumeWorkflowPopup')
            .reveal({
              'title': 'Workflow Conflict',
              'modal': true,
            });
          // When the workflow is started by a different user
          // and it is IN_PROGRESS for more then 60 mins , it
          // is considered stale.
        } else if (workflowState == 'FAILED_WORKFLOW') {


          // On Go Back clear and hide the workflow popup
          var failedWorkflowGoBack = document
            .getElementById('failedWorkflowGoBack');
          failedWorkflowGoBack.onclick = function() {
            // Used to prevent the refresh of page
            event.preventDefault();

            document
              .querySelector('#failedWorkflowPopup').visible = false;
          }

          // On Confirmation Cancel the workflow, start new
          // workflow and reload the workflow screen
          var failedConfirmCancel = document
            .getElementById('failedConfirm');
          failedConfirmCancel.disabled = false;
          failedConfirmCancel.onclick = function() {
            document
              .querySelector('#failedWorkflowPopup').visible = false;
            treasury.liquidity.workflowUtils.workflowExecute('CANCELLED', 'Workflow has been cancelled.', null);
            $('#workflowId').remove();
            sessionStorage.removeItem("workflowId");
            _loadWorkflowLanding();
          }

          // Set different properties of workflow conflict
          // properties and reveal it
          var failedWorkflowMessage = document
            .getElementById("failedWorkflowMessage");
          var subject = document.getElementById("subject");
          failedWorkflowMessage.value = conflictMessage;
          subject.value = conflictSubject;

          document.querySelector('#failedWorkflowPopup')
            .reveal({
              'title': 'Workflow Conflict',
              'modal': true,
            });
          // When the workflow is started by different user
          // and its not stale, then no user can cancel the
          // workflow or start a new workflow
        } else if (workflowState == 'LOCKED_WORKFLOW') {

          // On Go Back click clear and hide the workflow
          // conflict popup
          var lockedWorkflowGoBack = document
            .getElementById('lockedWorkflowGoBack');

          lockedWorkflowGoBack.onclick = function() {
            event.preventDefault();
            document
              .querySelector('#lockedWorkflowPopup').visible = false;
          }

          // Set properties of the workflow popup and reveal
          // it
          var lockedWorkflowMessage = document
            .getElementById("lockedWorkflowMessage");
          var subject = document.getElementById("subject");
          lockedWorkflowMessage.value = conflictMessage;
          subject.value = conflictSubject;
          document.querySelector('#lockedWorkflowPopup')
            .reveal({
              'title': 'Workflow Conflict',
              'modal': true,
            });
        }
      });
  }

  /**
   * Helper Method to dismiss conflict popup
   */
  function _dismissPopup() {
    // To avoid reloading the page on clicking the dismiss popup link
    event.preventDefault();
    document.querySelector('#resumeWorkflowPopup').visible = false;
    document.querySelector('#failedWorkflowPopup').visible = false;
    document.querySelector('#lockedWorkflowPop').visible = false;
  }

  function loadPublishedWorkflowVersions(showAlert) {
    var dateValue = document.getElementById('datePicker').value.concat('-01');
    var parameters = {};
    parameters.dateString = dateValue;
    parameters.workflowTypeName = "UEC";
    var serviceCall = $.ajax({
      url: "/treasury/liquidity/get-published-uec-workflow-versions",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.uec.util.hideLoading();
    serviceCall
      .done(function(data) {
        uecVersionFilter = new window.treasury.filter.SingleSelect(
          'workflowId', 'versionFilter',
          data.workflowVersions, [-1]);
          if(showAlert){
            alert("For any date that has been selected, data will be populated as of the beginning of the month."
                  +"For Alkali 3 & Alkali 4, data will be  provided as of the end of the previous month.");
                }
          });

    serviceCall.fail(function(xhr, text, errorThrown) {
      treasury.uec.util.hideLoading();
      console.log("Error occurred while retrieving results: " + errorThrown);
    });
  }

  function _signOffWorkflow() {
    var parameters = {};
    parameters.dateString = document.getElementById('datePicker').value + '-01';
    parameters.workflowId = document.getElementById('versionFilter').value.key;
    // TO DO : Validation
    treasury.uec.actions.signOffWorkflow(parameters);
  }
})();
