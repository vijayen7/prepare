"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.uec = window.treasury.uec || {};
  window.treasury.uec.uecColumnsConfig = {
    getUecGridColumnsByType: _getUecGridColumnsByType
  };

//TODO: Comment for logic
  /*
  1. Fetch common columns : Entity name, net cash, capital amount, uec ratioColumn.
  2. For entity level uec grid, append entity-related columns.
  3. For details grid, add cash and ratio columns, in required order.
  4. For master & apf grids, fetch previous month net cash/capital amount/ratio and
    corresponding diff columns. Loop through and append them in required order, only
    for landing screens. Append comment column as well.
  5. For workflow screen, master & apf grids, append comment link column.
  */
  function _getUecGridColumnsByType(type) {
    var columnConfigs = _getCommonUecColumns(type);
    if(type == 'entityUecGrid') {
      var entityUecColumns = _getEntityUecColumns();
      columnConfigs.splice.apply(columnConfigs, [1, 0].concat(entityUecColumns));
    } else if (type.includes('Detail')) {
      columnConfigs =_getDetailTypeColumns(type);
    }
    else{
      var prevAndDiffColumns = _getPreviousAndDiffColumns(type);
      var existingColumns = columnConfigs;
      columnConfigs = new Array();
      columnConfigs.push(existingColumns[0]);
      for(var i = 1; i < existingColumns.length; i++) {
        columnConfigs.push(existingColumns[i]);
        columnConfigs.push(prevAndDiffColumns[i-1]);
        columnConfigs.push(prevAndDiffColumns[i+4]);
      }
    }

    if(type != 'entityUecGrid' && !type.includes('Detail')) {
      columnConfigs.push({
        id: "comment",
        type: "text",
        name: "Comment",
        field: "comment",
        sortable: true,
        headerCssClass: "aln-rt b",
        cssClass: "aln-rt comment-wrap",
        excelFormatter: "#,##0",
        width: 150
      });
    }
    if(window.location.pathname.indexOf("uecWorkflow") >= 0 && type != 'entityUecGrid' && !type.includes('Detail')) {
      columnConfigs.push({
        id: "commentLink",
        type: "text",
        name: "Add/Edit Comment",
        field: "commentLink",
        formatter : _commentLinkFormatter,
        sortable: true,
        headerCssClass: "aln-rt b",
        cssClass: "aln-rt",
        excelFormatter: "#,##0",
        width: 30,
        minWidth: 30
      });
    }
    return columnConfigs;
  }

  function _getDetailTypeColumns(type) {
  var columnConfigs=[];
  columnConfigs.push(_getEntityColumn(type));
  if(type.includes('capital')){
    var capitalColumn = {
    id: "entityCapitalUsd",
    type: "number",
    name: "Entity Capital",
    field: "entityCapitalUsd",
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    toolTip: "Cash",
    width: 80
  };
  columnConfigs.push(capitalColumn);
  columnConfigs.push(_getCapitalColumn(type));
  }
  if(type.includes('cash')||type.includes('usage')){
  var cashColumn = {
    id: "cashUsd",
    type: "number",
    name: "Cash",
    field: "cashUsd",
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    toolTip: "Cash",
    width: 80
  };
  columnConfigs.push(cashColumn);
  _getCashColumns(type,columnConfigs);
  }
  if(type.includes('pnl')){
	  var pnlColumn = {
    id: "entityGrossMtdPnlUsd",
    type: "number",
    name: "Entity Mtd PNL",
    field: "entityGrossMtdPnlUsd",
    formatter: dpGrid.Formatters.Number,
    aggregator: dpGrid.Aggregators.sum,
    sortable: true,
    headerCssClass: "aln-rt b",
    excelFormatter: "#,##0",
    toolTip: "Entity Mtd PNL",
    width: 80
  };
  columnConfigs.push(pnlColumn);
  columnConfigs.push(_getPnlColumn(type));
  }
  columnConfigs.push(_getRatioColumn(type));
  return columnConfigs;
}

  function _getCashColumns(type,columnConfigs) {
	  columnConfigs.push({
          id: "usageUsd",
          type: "number",
          name: "Usage",
          field: "usageUsd",
          formatter:type == 'entityUecGrid' || type.includes('Detail') ? dpGrid.Formatters.Number : treasury.formatters.roaClickableAmount,
          aggregator: dpGrid.Aggregators.sum,
          sortable: true,
          headerCssClass: "aln-rt b",
          excelFormatter: "#,##0",
          toolTip: "Usage",
          width: 80
        });
	  columnConfigs.push({
        id: "netCashUsd",
        type: "number",
        name: "Net Cash",
        field: "netCashUsd",
        formatter: type == 'entityUecGrid' || type.includes('Detail') ? dpGrid.Formatters.Number : treasury.formatters.roaClickableAmount,
        aggregator: dpGrid.Aggregators.sum,
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        toolTip: "Net Cash",
        width: 80
      });
  }
  function _getCapitalColumn(type) {
	  return {
        id: "capitalAmountUsd",
        type: "number",
        name: "Capital Amount",
        field: "capitalAmountUsd",
        formatter: type == 'entityUecGrid' || type.includes('Detail') ? dpGrid.Formatters.Number : treasury.formatters.roaClickableAmount,
        aggregator: dpGrid.Aggregators.sum,
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        toolTip: "Capital Amount",
        width: 80
      };
  }
  function _getPnlColumn(type) {
	  return {
        id: "grossMtdPnlUsd",
        type: "number",
        name: "Mtd PNL",
        field: "grossMtdPnlUsd",
        formatter: type == 'entityUecGrid' || type.includes('Detail') ? dpGrid.Formatters.Number : treasury.formatters.roaClickableAmount,
        aggregator: dpGrid.Aggregators.sum,
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        toolTip: "Mtd PNL",
        width: 80
      };
  }


  function _getEntityColumn(type) {
	  return {id: "entity",
        type: "text",
        name: "Entity",
        field: "entity",
        sortable: true,
        headerCssClass: "aln-rt b",
        width: 120
      };
  }

    function _getRatioColumn(type) {
	  return {
        id: "ratio",
        type: "number",
        name: type.includes("Detail") ? "Ownership Ratio" : "Ratio(%)",
        field: type.includes("Detail") ? "ownershipRatio" : "ratio",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0",
        toolTip: type.includes('Detail') ? "Ownership Ratio" : "Net Cash to Capital ratio",
        width: 80
      };
  }

  function _getCommonUecColumns(type) {
	   var columnConfigs=[];
	   columnConfigs.push(_getEntityColumn(type));
	   _getCashColumns(type,columnConfigs);
	  columnConfigs.push(_getCapitalColumn(type));
	  columnConfigs.push(_getPnlColumn(type));
	  columnConfigs.push(_getRatioColumn(type)) ;
	  return columnConfigs;
  }

  function _getEntityUecColumns() {
        return [{
            id: "stlCashUsd",
            type: "number",
            name: "Stl Cash",
            field: "stlCashUsd",
            formatter: dpGrid.Formatters.Number,
            aggregator: dpGrid.Aggregators.sum,
            sortable: true,
            headerCssClass: "aln-rt b",
            excelFormatter: "#,##0",
            toolTip: "Stl Cash",
            width: 50
          }, {
            id: "brokerCashUsd",
            type: "number",
            name: "Broker Cash",
            field: "brokerCashUsd",
            formatter: dpGrid.Formatters.Number,
            aggregator: dpGrid.Aggregators.sum,
            sortable: true,
            headerCssClass: "aln-rt b",
            excelFormatter: "#,##0",
            toolTip: "Broker Cash",
            width: 50
          }, {
            id: "rebalancingAmountUsd",
            type: "number",
            name: "Rebalancing Amount",
            field: "rebalancingAmountUsd",
            formatter: dpGrid.Formatters.Number,
            aggregator: dpGrid.Aggregators.sum,
            sortable: true,
            headerCssClass: "aln-rt b",
            excelFormatter: "#,##0",
            toolTip: "Rebalancing Amount",
            width: 50
          }, {
            id: "subscriptionAmountUsd",
            type: "number",
            name: "Subscription Amount",
            field: "subscriptionAmountUsd",
            formatter: dpGrid.Formatters.Number,
            aggregator: dpGrid.Aggregators.sum,
            sortable: true,
            headerCssClass: "aln-rt b",
            excelFormatter: "#,##0",
            toolTip: "Subscription Amount",
            width: 50
          }, {
            id: "withdrawalsPayableAmountUsd",
            type: "number",
            name: "WithdrawalsPayable Amount",
            field: "withdrawalsPayableAmountUsd",
            formatter: dpGrid.Formatters.Number,
            aggregator: dpGrid.Aggregators.sum,
            sortable: true,
            headerCssClass: "aln-rt b",
            excelFormatter: "#,##0",
            toolTip: "WithdrawalsPayable Amount",
            width: 50
          }];
        }

        function _getPreviousAndDiffColumns(type) {

            return [{
                id: "prevMonth.usageUsd",
                type: "number",
                name: "Prev Month Usage Usd",
                field: "prevMonth.usageUsd",
                formatter: type == 'entityUecGrid' || type.includes('Detail') ? dpGrid.Formatters.Number : treasury.formatters.roaClickableAmount,
                aggregator: dpGrid.Aggregators.sum,
                sortable: true,
                headerCssClass: "aln-rt b",
                excelFormatter: "#,##0",
                width:80
              }, {
                  id: "prevMonth.netCashUsd",
                  type: "number",
                  name: "Prev Month Net Cash",
                  field: "prevMonth.netCashUsd",
                  formatter: type == 'entityUecGrid' || type.includes('Detail') ? dpGrid.Formatters.Number : treasury.formatters.roaClickableAmount,
                  aggregator: dpGrid.Aggregators.sum,
                  sortable: true,
                  headerCssClass: "aln-rt b",
                  excelFormatter: "#,##0",
                  toolTip: "Net Cash",
                  width:80
                }, {
                id: "prevMonth.capitalAmountUsd",
                type: "number",
                name: "Prev Month Capital Amount",
                field: "prevMonth.capitalAmountUsd",
                formatter: type == 'entityUecGrid' || type.includes('Detail') ? dpGrid.Formatters.Number : treasury.formatters.roaClickableAmount,
                aggregator: dpGrid.Aggregators.sum,
                sortable: true,
                headerCssClass: "aln-rt b",
                excelFormatter: "#,##0",
                toolTip: "Capital Amount",
                width:80
              },
{
                id: "prevMonth.grossMtdPnlUsd",
                type: "number",
                name: "Prev Month Mtd PNL",
                field: "prevMonth.grossMtdPnlUsd",
                formatter: type == 'entityUecGrid' || type.includes('Detail') ? dpGrid.Formatters.Number : treasury.formatters.roaClickableAmount,
                aggregator: dpGrid.Aggregators.sum,
                sortable: true,
                headerCssClass: "aln-rt b",
                excelFormatter: "#,##0",
                toolTip: "Mtd PNL",
                width:80
              }, {
                id: type.includes('Detail') ? "prevMonth.ownershipRatio" : "prevMonth.ratio",
                type: "number",
                name: type.includes('Detail') ? "Ownership Ratio" : "Prev Month Ratio",
                field: type.includes('Detail') ? "prevMonth.ownershipRatio" : "prevMonth.ratio",
                sortable: true,
                headerCssClass: "aln-rt b",
                excelFormatter: "#,##0",
                toolTip: type.includes('Detail') ? "Ownership Ratio" : "Net Cash to Capital ratio",
                width:80
              },{
                  id: "usageUsdDiff",
                  type: "number",
                  name: "Usage Diff",
                  field: "usageUsdDiff",
                  formatter: dpGrid.Formatters.Number,
                  aggregator: dpGrid.Aggregators.sum,
                  sortable: true,
                  headerCssClass: "aln-rt b",
                  excelFormatter: "#,##0",
                  toolTip: "Usage Diff",
                  width:80
                }, {
                    id: "netCashUsdDiff",
                    type: "number",
                    name: "Net Cash Diff",
                    field: "netCashUsdDiff",
                    formatter: dpGrid.Formatters.Number,
                    aggregator: dpGrid.Aggregators.sum,
                    sortable: true,
                    headerCssClass: "aln-rt b",
                    excelFormatter: "#,##0",
                    toolTip: "Net Cash",
                    width:80
                  },{
                  id: "capitalAmountUsdDiff",
                  type: "number",
                  name: "Capital Amount Diff",
                  field: "capitalAmountUsdDiff",
                  formatter:dpGrid.Formatters.Number,
                  aggregator: dpGrid.Aggregators.sum,
                  sortable: true,
                  headerCssClass: "aln-rt b",
                  excelFormatter: "#,##0",
                  toolTip: "Capital Amount",
                  width:80
                },
{
                  id: "grossMonthToDatePnLDiff",
                  type: "number",
                  name: "Mtd PNL Diff",
                  field: "grossMonthToDatePnLDiff",
                  formatter:dpGrid.Formatters.Number,
                  aggregator: dpGrid.Aggregators.sum,
                  sortable: true,
                  headerCssClass: "aln-rt b",
                  excelFormatter: "#,##0",
                  toolTip: "Mtd PNL Diff",
                  width:80
                },{
                  id: "ratioDiff",
                  type: "number",
                  name: type.includes('Detail') ? "Ownership Ratio" : "Ratio Diff",
                  field: type.includes('Detail') ? "ownershipRatio" : "ratioDiff",
                  sortable: true,
                  headerCssClass: "aln-rt b",
                  excelFormatter: "#,##0",
                  toolTip: type.includes('Detail') ? "Ownership Ratio" : "Net Cash to Capital ratio",
                  width:80
                }];
        }
        function _commentLinkFormatter(row,cell,value,columnDef,dataContext) {

          var commentLink = '<a class="margin--left--small" href="#" title="Edit" ><i class="fa fa-pencil-square-o fa-lg"></i></a>';
          return commentLink;
        }
})();
