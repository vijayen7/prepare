/**
 * Select Filter Group class (prototypal OO)
 * Contains array of MSBs and Select2s (SingleSelect)s.
 **/
(function($){
    $.extend(true, window, 
    {
        "treasury" :
        {
            "SelectFilterGroup" : SelectFilterGroup
        }
    });

    /**
     * Select Filter Group class.
     */
    function SelectFilterGroup(msbArray, singleSelectArray) {
        this.msbArray = msbArray;
        this.singleSelectArray = singleSelectArray;
    }

    /*
     * Constructs map containing selected values from msbs and single selects.
     */
    SelectFilterGroup.prototype.getParameterMap = function() {
        var parameterMap = {};
        if (this.msbArray) {
            for (var i=0; i < this.msbArray.length; i++) {
                var msb = this.msbArray[i];
                parameterMap[msb.paramString] = msb.serializedSelectedIds();
            }
        }

        if (this.singleSelectArray)
        {
            for (var i=0; i < this.singleSelectArray.length; i++) {
                var singleSelect = this.singleSelectArray[i];
                parameterMap[singleSelect.paramString] = singleSelect.getSelectedId();
            }
        }

        return parameterMap;
    };

    SelectFilterGroup.prototype.getParameterValuesMap = function() {
        var parameterMap = {};
        if (this.msbArray) {
            for (var i=0; i < this.msbArray.length; i++) {
                var msb = this.msbArray[i];
                parameterMap[msb.paramString] = msb.serializedSelectedValues();
            }
        }

        if (this.singleSelectArray)
        {
            for (var i=0; i < this.singleSelectArray.length; i++) {
                var singleSelect = this.singleSelectArray[i];
                parameterMap[singleSelect.paramString] = singleSelect.getSelectedValue();
            }
        }

        return parameterMap;
    };

    /*
     * Returns true if each single selects has at least one selected item.
     */
    SelectFilterGroup.prototype.validateSingleSelects = function() {
        var valid = true;
        if (this.singleSelectArray)
        {
            for (var i=0; i < this.singleSelectArray.length; i++) {
                valid &= this.singleSelectArray[i].validate();
            }
        }
        return valid;
    };

    /*
     * Following methods are applicable only on msbs and are not required for
     * single selects.
     */
    SelectFilterGroup.prototype.resetAll = function() {
        if (this.msbArray !== undefined) {
            for (var i=0; i < this.msbArray.length; i++) {
                this.msbArray[i].reset();
            }
        }
    };

    SelectFilterGroup.prototype.registerAll = function() {
        if (this.msbArray !== undefined) {
            for (var i=0; i < this.msbArray.length; i++) {
                this.msbArray[i].register();
            }
        }
    };

    SelectFilterGroup.prototype.updateSelectedIds = function(params) {
        if (params !== undefined && this.msbArray !== undefined) {
            for (var i=0; i < this.msbArray.length; i++) {
                var msb = this.msbArray[i];
                if (params.hasOwnProperty(msb.paramString)) {
                    msb.updateSelectedIds(params[msb.paramString]);
                }
            }
        }
    };

    /*
     * Temporary change till Prop#97139 is done.
     */
    SelectFilterGroup.prototype.updateMSBText = function() {
        if (this.msbArray !== undefined) {
            for (var i=0; i < this.msbArray.length; i++) {
                this.msbArray[i].updateMSBText();
            }
        }
    };
})(jQuery);