(function($) {
  var _createApplicationMenu = function() {
    var headerMenu = document.querySelector(".application-menu");
    var existingDiv = document.querySelector(".controlslide");

    var tabOptionBtns = existingDiv.querySelectorAll(".tab-opt-l");

    for (var i = 0; i < tabOptionBtns.length; i++) {
      tabOptionBtns[i].parentNode.style.verticalAlign = "middle";
      tabOptionBtns[i].classList.add("fa");
      tabOptionBtns[i].classList.add("fa-chevron-circle-down");

    }
    headerMenu.appendChild(existingDiv);

    var headerHeight = document.querySelector('arc-header').getBoundingClientRect().height;

    document.querySelector('.topnavigation').style.top = headerHeight + 'px';
  }

  var _injectCSSOverride = function() {
    var template = document.querySelector('#arcux-head-scripts');
    var clone = document.importNode(template.content, true);
    document.head.appendChild(clone);
  }

  var _injectHeader = function() {
    var arcHeaderTemplate = document.querySelector('#arcux-header');
    var arcHeaderClone = document.importNode(arcHeaderTemplate.content, true);
    $(document.body).prepend(arcHeaderClone);
  }

  document.addEventListener('WebComponentsReady', function() {

    if (localStorage.getItem('treasury-theme-preference') === null) {
      localStorage.setItem('treasury-theme-preference', 'true');
      ArcUXTheme.enableModernTheme();
    }
    if (ArcUXTheme && ArcUXTheme.modernThemeSelected &&
      ArcThemeHandler) {
      ArcThemeHandler.enableThemeSelection();
      _injectCSSOverride();
      _injectHeader();
      _createApplicationMenu();
      _setTopTabSideLinkPosition();
    }
  });

  var _setTopTabSideLinkPosition = function() {
    var _setCurrentView = function() {
      try {
        var secNav = document.querySelector('arc-header').shadowRoot.querySelector('.arc-header__secnav');
        var breadcrumb = secNav.querySelector('.arc-header__secnav__breadcrumb');
        var navTab = secNav.querySelector('.arc-header__secnav__tabnav');
        var currView = document.querySelector('.curr-view-all');
        currView.style.left = (breadcrumb.getBoundingClientRect().width - 10) + 'px';
        var totalWidth = secNav.getBoundingClientRect().width;
        var occupiedWidth = breadcrumb.getBoundingClientRect().width + currView.getBoundingClientRect().width;
        navTab.style.width = (totalWidth - occupiedWidth) + 'px';
      } catch(e) {
        console.warn('unable to set left position for top tab side link', e);
      }
    }
    var header = document.querySelector('arc-header');
    if (!header) {
      return;
    }
    if(header.breadcrumbInitialized) {
      _setCurrentView();
    } else {
      header.addEventListener('breadcrumbInitialized', function(e) {
        _setCurrentView();
      });
    }
  }

  window.addEventListener('resize', function() {
    if (ArcUXTheme && ArcUXTheme.modernThemeSelected &&
      ArcThemeHandler) {
        _setTopTabSideLinkPosition();
    }
  });

  window.addEventListener('load', function() {
    $('#cover').remove();
  });
})(jQuery);

(function() {
    window.addEventListener('load', function() {
      var originalGetCompleteWorkSheetData = dportal.clientExportToExcel.getCompleteWorkSheetData;
      dportal.clientExportToExcel.getCompleteWorkSheetData = function() {
        var resp = originalGetCompleteWorkSheetData.apply(this, arguments);
        if (resp.worksheets && resp.worksheets.length) {
          for (var i = 0; i < resp.worksheets.length; i++) {
            if (resp.worksheets[i] && resp.worksheets[i].data && resp.worksheets[i].data.length) {
              var header = resp.worksheets[i].data[0];
              if (header && header.length) {
                for (var j = 0; j < header.length; j++) {
                  header[j].backgroundColor = 'C9DCE9';
                }
              }
            }
          }
        }
        return resp;
      };
    });
})();
