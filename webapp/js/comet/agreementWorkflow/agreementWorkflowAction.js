"user strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementWorkflowAction = {

        displayAgreementWorkflow : _displayAgreementWorkflow,
        quickPublishAgreementWorkflow : _quickPublishAgreementWorkflow,
        quickPublishWorkflowOperation : _quickPublishWorkflowOperation,
        quickGenerateCallReport : _quickGenerateCallReport,
        workflowExecute : _workflowExecute,
        cancelWorkflow : _cancelWorkflow,
        resumeWorkflow : _resumeWorkflow,
        getWorkflowParam : _getWorkflowParam,
        beginWorkflow : _beginWorkflow,
        getFormattedDate : _getFormattedDate,
        quickCallReport : _quickCallReport,
        downloadBrokerfile : _downloadBrokerfile,
        downloadCrifFile: _downloadCrifFile
    };

    var workflowId;

    /**
     * Begins/Resume Workflow and open new Agreement Workflow window
     */
    function _displayAgreementWorkflow(params) {

        // Get Workflow params
        var workflowParams = _getWorkflowParam(params);
        var wfDataUrl = "/treasury/comet/display-agreement-workflow-data";

        var wfDataParam = "configName=" + "yakAgreementWorkflowConfig";
        wfDataParam += '&date=' + _extractDate(params);
        let treasuryAgreementId = _extractTreasuryAgreementId(params);
        wfDataParam += '&actionableAgreementId=' + treasuryAgreementId;

        _displayBeginWorkflow(workflowParams, wfDataUrl, wfDataParam, treasuryAgreementId);
    }

    function _displayBeginWorkflow(wfParams, wfDataUrl, wfDataParam, treasuryAgreementId) {

        // Service to be invoked to begin workflow
        var url = "/treasury/comet/workflowBegin";

        var workflowId = null;

        var promise = $.ajax({
            url : url,
            data : wfParams,
            dataType : "json",
            type : "POST"
        });

        promise
                .done(function(data) {
                    {
                        let agreementDetails = {};
                        let agreementDetail = {
                            "workflowStatus": "IN_PROGRESS"
                        };
                        agreementDetails[treasuryAgreementId] = agreementDetail;
                        // Append workflowId after begin workflow
                        wfDataParam = wfDataParam + "&workflowId="
                                + data.resultList[0].workflowId;
                        var workflowState = data.resultList[0].workflowState;

                        // Conflict scenario popup subject and message
                        var conflictSubject = data.resultList[0].subject;
                        var conflictMessage = data.resultList[0].message;

                        // For new workflow the state will be BEGIN_WORKFLOW,
                        // reload the agreement summary screen and open a new
                        // child window
                        if (workflowState == 'BEGIN_WORKFLOW') {

                            window.treasury.comet.agreementsummary
                                    .reloadAgreementSummary(true, agreementDetails);
                            _launchAgmtWfWindow('Agreement Workflow',
                                    wfDataUrl, wfDataParam, 'Workflow');

                        }

                        // A workflow already started by the same user
                        if (workflowState == 'RESUME_WORKFLOW') {



                            // On Cancel workflow clearing the workflow conflict
                            // popup, cancelling the workflow and reloading the
                            // summary page
                            var cancelWorkflow = document
                                    .getElementById('cancelWorkflow');
                            cancelWorkflow.disabled = false;
                            cancelWorkflow.onclick = function() {



                                document
                                        .querySelector('#resumeWorkflowPopup').visible = false;

                                // Cancel the workflow and reload the Agreement
                                // Summary screen
                                window.treasury.comet.agreementWorkflowAction
                                        .cancelWorkflow('CANCELLED',
                                                'Workflow has been cancelled',
                                                data.resultList[0].workflowId);
                                window.treasury.comet.agreementsummary
                                        .reloadAgreementSummary(false);
                            }

                            // On resume workflow clearing the workflow conflict
                            // popup, resuming the workflow and reload the
                            // agreement summary screen
                            var resumeWorkflow = document
                                    .getElementById('resumeWorkflow');
                            resumeWorkflow.disabled = false;

                            resumeWorkflow.onclick = function() {



                                document
                                        .querySelector('#resumeWorkflowPopup').visible = false;

                                // Resume workflow and reload Agreement Summary
                                // Screen
                                window.treasury.comet.agreementWorkflowAction
                                        .resumeWorkflow(
                                                data.resultList[0].workflowId,
                                                wfDataUrl, wfDataParam);
                                window.treasury.comet.agreementsummary
                                        .reloadAgreementSummary(true, agreementDetails);

                            }

                            // On dismiss popup clear and hide the workflow
                            // popup
                            var dismissMessage = document
                                    .getElementById('dismissMessage');

                            dismissMessage.onclick = function() {

                                _dismissPopup();

                            }

                            // Set properties of workflow conflict popup and
                            // reveal
                            var resumeWorkflowMessage = document
                                    .getElementById("resumeWorkflowMessage");
                            var subject = document.getElementById("subject");
                            resumeWorkflowMessage.value = conflictMessage;
                            subject.value = conflictSubject;

                            document.querySelector('#resumeWorkflowPopup')
                                    .reveal({
                                        'title' : 'Workflow Conflict',
                                        'modal' : true,
                                    });

                            // When the workflow is started by a different user
                            // and it is IN_PROGRESS for more then 60 mins , it
                            // is considered stale.
                        } else if (workflowState == 'FAILED_WORKFLOW') {



                            // On Go Back clear and hide the workflow popup
                            var failedWorkflowGoBack = document
                                    .getElementById('failedWorkflowGoBack');
                            failedWorkflowGoBack.onclick = function() {



                                document
                                        .querySelector('#failedWorkflowPopup').visible = false;
                            }

                            // On Confirmation Cancel the workflow, start new
                            // workflow and reload the agreement summary screen
                            var failedConfirmCancel = document
                                    .getElementById('failedConfirm');
                            failedConfirmCancel.disabled = false;

                            failedConfirmCancel.onclick = function() {

                                document
                                        .querySelector('#failedWorkflowPopup').visible = false;
                                window.treasury.comet.agreementWorkflowAction
                                        .cancelWorkflow('CANCELLED',
                                                'Workflow has been cancelled',
                                                data.resultList[0].workflowId);

                                wfDataParam = _extractWfParam(wfDataParam);

                                _displayBeginWorkflow(wfParams, wfDataUrl,
                                                wfDataParam, treasuryAgreementId);
                                window.treasury.comet.agreementsummary
                                        .reloadAgreementSummary(true, agreementDetails);

                            }

                            // Set different properties of workflow conflict
                            // properties and reveal it
                            var failedWorkflowMessage = document
                                    .getElementById("failedWorkflowMessage");
                            var subject = document.getElementById("subject");
                            failedWorkflowMessage.value = conflictMessage;
                            subject.value = conflictSubject;

                            document.querySelector('#failedWorkflowPopup')
                                    .reveal({
                                        'title' : 'Workflow Conflict',
                                        'modal' : true,
                                    });
                            // When the workflow is started by different user
                            // and its not stale, then no user can cancel the
                            // workflow or start a new workflow
                        } else if (workflowState == 'LOCKED_WORKFLOW') {




                            // On Go Back click clear and hide the workflow
                            // conflict popup
                            var lockedWorkflowGoBack = document
                                    .getElementById('lockedWorkflowGoBack');

                            lockedWorkflowGoBack.onclick = function() {

                                document.querySelector('#lockedWorkflowPopup').visible = false;

                            }

                            // Set properties of the workflow popup and reveal
                            // it
                            var lockedWorkflowMessage = document
                                    .getElementById("lockedWorkflowMessage");
                            var subject = document.getElementById("subject");
                            lockedWorkflowMessage.value = conflictMessage;
                            subject.value = conflictSubject;

                            document.querySelector('#lockedWorkflowPopup')
                                    .reveal({
                                        'title' : 'Workflow Conflict',
                                        'modal' : true,
                                    });

                        }

                    }
                });

        promise.fail(function(xhr, text, errorThrown) {

            alert("Error occurred while initiating workflow");
            console.log("Error occurred while initiating workflow");

        });
    }

    function _extractWfParam(wfParam) {
        var extractedWfParam;
        if(wfParam != undefined) {
            var wfParamArr = wfParam.split('&');
            for(var i = 0; i<wfParamArr.length;i++) {
                if(wfParamArr[i].includes("workflow")) {
                    wfParamArr.splice(i,i);
                    }
                }
        }
        extractedWfParam = wfParamArr.join('&');
        return extractedWfParam;
    }

    /**
     * Helper Method to dismiss conflict popup
     */
    function _dismissPopup() {


        document.querySelector('#resumeWorkflowPopup').visible = false;

        document.querySelector('#failedWorkflowPopup').visible = false;


        document.querySelector('#lockedWorkflowPopup').visible = false;

    }

    /**
     * Helper menthod for cancelling workflow Inputs: workflowActionType
     * ['CANCEL'] statusMag -- Message for cancelling the workflow workflowId
     */
    function _cancelWorkflow(workflowActionTpye, statusMsg, workflowId) {

        if (confirm("Do you really want to cancel the workflow?") == false) {
            return;
        }

        var wfExecuteUrl = "/treasury/comet/workflowExecute";

        var wfCancelParams = "workflowParam.workflowId=" + workflowId;
        wfCancelParams = wfCancelParams + "&status=" + workflowActionTpye;

        var promise = $.ajax({
            url : wfExecuteUrl,
            data : wfCancelParams,
            dataType : "json",
            type : "GET"
        });

        promise.done(function(data) {
            {
                console.log('Cancel workflow complete');
            }
        });

        promise.fail(function(xhr, text, errorThrown) {

            alert("Error occurred while Workflow execution");
            console.log("Error occurred while Workflow execution");

        });
    }

    /**
     * Helper method to resume workflow Inputs: workflowId resumeDataUrl : url
     * for fetching data to be shown in the workflow screen on resume
     * resumeDataParams : params for fetching data
     */
    function _resumeWorkflow(workflowId, wfDataUrl, wfDataParams) {

        var resumeWfUrl = "/treasury/comet/resumeWorkflow";

        var resumeWfParams = "workflowParam.workflowId=" + workflowId;

        var promise = $.ajax({
            url : resumeWfUrl,
            data : resumeWfParams,
            dataType : "json",
            type : "GET"
        });

        promise.done(function(data) {

            {
                console.log('Resume workflow complete '
                        + data.resultList[0].workflowId);
                var childWindow = _launchAgmtWfWindow('Agreement Workflow',
                        wfDataUrl, wfDataParams, 'Workflow');
            }
        });

        promise.fail(function(xhr, text, errorThrown) {
            alert("Error occurred while Workflow execution");
            console.log("Error occurred while Workflow execution");
        });
    }

    /**
     * Gets the workflow child window.
     */
    function _launchAgmtWfWindow(windowName, action, wfDataParams, title) {

        var url = action + '?' + wfDataParams;

        var childWindow = window.open(url, '_blank');

        if (childWindow == null || typeof (childWindow) == "undefined") {
            alert("Please disable the pop-up block option in the browser settings.");
            return;
        }

    }

    /**
     * This method can be used to CANCEL and SAVE the workflow
     */
    function _workflowExecute(workflowActionType, statusMsg,
            closeWorkflowWindow) {

        if (document.getElementById('workflowId') == null
                || document.getElementById('workflowId') == undefined) {
            alert("Workflow instance is not defined.");
            return;
        }

        /**
         * On publish or save button click disabling the both publish and save buttons
         */
        if ((workflowActionType.indexOf("PUBLISHED") != -1) || (workflowActionType.indexOf("SAVED") != -1)) {

            var publishButton = document.getElementById("publishId");
            var saveButton = document.getElementById("saveId");

            if(publishButton != undefined) {
                publishButton.disabled = true;
            }

            if(saveButton != undefined) {
                saveButton.disabled = true;
            }
        }

        if (workflowActionType.indexOf("CANCELLED") != -1) {
            if (confirm("Do you really want to Cancel the workflow?") == false) {
                return;
            }
        }

        window.treasury.comet.agreementworkflow.showLoading();

        var url = "/treasury/comet/workflowExecute";

        var params = "workflowParam.workflowId="
                + document.getElementById('workflowId').value;
        params = params + "&status=" + workflowActionType;

        var promise = $.ajax({
            url : url,
            data : params,
            dataType : "json",
            type : "GET"
        });

        promise.done(function(data) {

            {
                var workflowId = data.resultList[0].workflowId;
                var actionableAgreementId = document.getElementById('actionableAgreementId').value;

                if (workflowActionType == 'CANCELLED') {
                    _closeWorkflowWindow(closeWorkflowWindow,workflowActionType);
                } else if (workflowActionType == 'SAVED') { // Save Workflow
                    // Artifacts
                    _saveAgreementWorkflowArtifacts("SAVE_WORKFLOW_DATA",
                            workflowId, closeWorkflowWindow, false, null, actionableAgreementId, null);
                } else if (workflowActionType == 'PUBLISHED') { // Save Workflow
                    // Artifacts
                    _saveAgreementWorkflowArtifacts("PUBLISH", workflowId,
                            closeWorkflowWindow, false, null, actionableAgreementId, null);
                }
            }
        });

        promise.fail(function(xhr, text, errorThrown) {
            alert("Error occurred while Workflow execution");
        });

    }

    function _closeWorkflowWindow(closeWorkflowWindow,workflowOperation, agreementDetails, checkAndReload) {
        // Close the workflow window
        window.focus();

        if (closeWorkflowWindow && workflowOperation != undefined && workflowOperation.indexOf("QUICK_PUBLISH") == -1 && workflowOperation.indexOf("QUICK_WIRE_PUBLISH") == -1) {
            let treasuryAgreementId = document.getElementById("actionableAgreementId").value;
            if(!window.treasury.comet.Util.isDefined(agreementDetails)) {
                agreementDetails = {};
            }
            let agreementDetail = agreementDetails[treasuryAgreementId];
            if(!window.treasury.comet.Util.isDefined(agreementDetail)) {
                agreementDetail = {};
                agreementDetails[treasuryAgreementId] = agreementDetail;
            }
            if(workflowOperation == 'CANCELLED' )
                checkAndReload = false;
            else if(workflowOperation == 'SAVE_WORKFLOW_DATA' ){

                agreementDetail["workflowStatus"] = "IN_PROGRESS";
            }
            else if(workflowOperation == 'PUBLISH' ){
                agreementDetail["workflowStatus"] = "PUBLISHED";
            }



            window.opener.treasury.comet.agreementsummary.reloadAgreementSummary(checkAndReload, agreementDetails);

            window.close();
        }
    }

    /**
     * Method to Begin Workflow
     */
    function _beginWorkflow(workflowParam, isAync) {

        if (workflowParam === null || workflowParam === undefined) {

            return;

        }

        // URL for Begin Workflow
        var workflowBeginUrl = "/treasury/comet/workflowBegin";

        var promise = $.ajax({
            url : workflowBeginUrl,
            data : workflowParam,
            async : isAync,
            dataType : "json",
            type : "GET"
        });



        return promise;

    }

    function _getWorkflowParam(workflowParam) {

        // Preparing Workflow Params
        var paramArray = workflowParam.split("_");
        var date = paramArray[0];
        var legalEntityId = paramArray[1];
        var cpeEntityId = paramArray[2];
        var agreementTypeId = paramArray[3];
        var agreementId = paramArray[4];

        var workflowUnit = "AgreementDate";
        var workflowType = "EXCESS_DEFICIT";
        var workflowConfig = "yakExpReqTabConfig";
        var records = workflowUnit + "_" + workflowType + "_" + legalEntityId
                + "_" + cpeEntityId + "_" + agreementTypeId + "_" + date;

        records = records.replace(/-/g, "");

        var workflowParam = "workflowParam.dateStr=" + date;
        workflowParam += "&workflowParam.descoEntityId=" + legalEntityId;
        workflowParam += "&workflowParam.counterpartyEntityId=" + cpeEntityId;
        workflowParam += "&workflowParam.agreementTypeId=" + agreementTypeId;
        workflowParam += "&workflowParam.workflowUnit=" + workflowUnit;
        workflowParam += "&workflowParam.workflowTypeName=" + workflowType;
        workflowParam += "&configName=" + workflowConfig;
        workflowParam += "&records=" + records;

        return workflowParam;
    }

    function _extractDate(workflowParam) {
        // extract date
        var paramArray = workflowParam.split("_");
        return paramArray[0];
    }

    function _extractTreasuryAgreementId(workflowParam) {
            var paramArray = workflowParam.split("_");
            return paramArray[4];
    }


    function _getQuickPublishWorkflowOperation(quikPublishParams, workflowOperation) {
        var workflowParams = _getWorkflowParam(quikPublishParams, false);

        var workflowBeginUrl = "/treasury/comet/workflowBegin";

        var promise = $.ajax({
            url: workflowBeginUrl,
            data: workflowParams,
            async: true,
            dataType: "json",
            type: "GET"
        });

        return promise
            .then(function (data, workflowParams) {

                var workflowId = data.resultList[0].workflowId;

                var workflowExecuteUrl = "/treasury/comet/workflowExecute";

                var workflowExecuteParam = workflowParams;
                workflowExecuteParam += "&workflowParam.workflowId="
                    + workflowId;
                workflowExecuteParam += "&status="
                    + "PUBLISHED";

                var promise = $.ajax({
                    url: workflowExecuteUrl,
                    data: workflowExecuteParam,
                    async: true,
                    dataType: "json",
                    type: "GET"
                });

                return promise;


            }).then(function (data) {
                var workflowId = data.resultList[0].workflowId;

                var params = "workflowOperation=" + workflowOperation;
                params += "&workflowId=" + workflowId;
                params += "&actionableAgreementId=" + _extractTreasuryAgreementId(quikPublishParams);
                var url = "/treasury/comet/saveAgreementWorkflowArtifacts";

                var promise = $.ajax({
                    url: url,
                    data: params,
                    async: true,
                    dataType: "json",
                    type: "GET"
                });

                return promise.done(function(data){
                    if(data!=null && data.resultList!=null && data.resultList[0].status=='success'){
                       return promise;
                    }
                    else{
                       return getCancelledWorkflow(workflowParams,workflowId);
                    }
                    })
                    .fail(function(data){
                       return getCancelledWorkflow(workflowParams,workflowId);
                });
  });
 }

       function getCancelledWorkflow(workflowParams,workflowId) {
           var cancelWorkflowUrl = "/treasury/comet/workflowExecute";
                    var workflowExecuteParam = workflowParams;
                workflowExecuteParam += "&workflowParam.workflowId="
                    + workflowId;
                workflowExecuteParam += "&status=CANCELLED";
                var promise = $.ajax({
                    url: cancelWorkflowUrl,
                    data: workflowExecuteParam,
                    async: true,
                    dataType: "json",
                    type: "GET"
                });
                return promise;
       }

    /**
     * Quick Publish will Begin the Workflow, Publish it and Save the Workflow
     * Artifacts. This method provides support for publishing multiple
     * agreements at once.
     */
     function _quickPublishAgreementWorkflow(quickPublishParams) {

        window.treasury.comet.agreementsummary.showLoading();

        var quickPublishParamArray = quickPublishParams.split(':');

        if (quickPublishParamArray.length > 0) {

            var promises = [];

            let agreementIds = [];

            for (var i = 0; i < quickPublishParamArray.length; i++) {
                promises.push(_getQuickPublishWorkflowOperation(quickPublishParamArray[i],"QUICK_PUBLISH"));
                agreementIds.push(_extractTreasuryAgreementId(quickPublishParamArray[i]));
            }

            var promiseResult = Promise.all(promises).then(function(values) {
                var failure = "Publish failed for following agreement ids ";
                var failureAgreements = "";
                var status = "Status: ";
                for(var x=0;x<values.length;x++){
                    if(values[x].resultList != undefined && values[x].resultList.length > 0){
                        if(values[x].resultList[0].status == "failure") {
                          failureAgreements = failureAgreements.concat(values[x].resultList[0].agreementId+", ");
                        }
                    }
                }
                let checkAndReload = false;
                let agreementDetails = {};
                if(failureAgreements != "") {
                    failureAgreements = failureAgreements.slice(0, failureAgreements.length-2);
                    status = status.concat(failure.concat(failureAgreements));
                    alert(status);
                } else {
                    checkAndReload = true;

                    for (var i = 0; i < agreementIds.length; i++) {
                        let agreementDetail = {
                            "workflowStatus": "PUBLISHED",
                            "ecmRC": 0,
                            "asmRC": 0,
                            "segECMRC": 0,
                            "segASMRC": 0,
                            "disputeAmountRC": 0,
                            "segDisputeAmountRC": 0
                        };
                        agreementDetails[agreementIds[i]] = agreementDetail;
                    }

                }
                window.treasury.comet.agreementsummary.hideLoading();



                window.treasury.comet.agreementsummary.reloadAgreementSummary(checkAndReload, agreementDetails);

            });

        }
    }

    function _quickPublishWorkflowOperation(quikPublishParams,workflowOperation, agreementId, wireAmount, marginCallType) {

        // Get Workflow params
        var workflowParams = _getWorkflowParam(quikPublishParams, false);

        // Begin Workflow with async false
        var promise = _beginWorkflow(workflowParams, false);

        promise
                .done(function(data) {
                    {
                        // On completion of Begin Workflow get
                        // WorkflowId
                        var workflowId = data.resultList[0].workflowId;

                        var workflowExecuteUrl = "/treasury/comet/workflowExecute";

                        var workflowExecuteParam = workflowParams;
                        workflowExecuteParam += "&workflowParam.workflowId="
                                + workflowId;
                        workflowExecuteParam += "&status="
                                + "PUBLISHED";

                        // Workflow execute is called to PUBLISH the
                        // workflow
                        var promise = $.ajax({
                            url : workflowExecuteUrl,
                            data : workflowExecuteParam,
                            async : false,
                            dataType : "json",
                            type : "GET"
                        });

                        promise
                                .done(function(data) {
                                    var workflowId = data.resultList[0].workflowId;

                                    // Save Workflow Artifacts
                                    _saveAgreementWorkflowArtifacts(
                                            workflowOperation, workflowId, false, agreementId, wireAmount, agreementId, marginCallType);
                                })
                    }
                });

        promise.fail(function(xhr, text, errorThrown) {

            alert("Error occurred while initiating workflow");
            console.log("Error occurred while initiating workflow");


        });


    }


    /**
     * Used once Workflow artifacts need to be saved. After the Quick Publish,
     * Save and Exit, Publish
     */
    function _saveAgreementWorkflowArtifacts(workflowOperation, workflowId,
            isAsync, agreementId, wireAmount, actionableAgreementId, marginCallType) {

        var params = ''
        if (marginCallType == null || marginCallType.indexOf(",") != -1 ) {
            marginCallType = "HOUSE"
        }
        if(workflowOperation != undefined) {
            if(workflowOperation.indexOf("QUICK_WIRE_PUBLISH") != -1) {
                var  agreementMarginCallTypeAcmDataList = new Array();
                var acmData = {
                    "agreementId" : agreementId,
                    "marginCallType" : marginCallType,
                    "amountString" : wireAmount
                }
                agreementMarginCallTypeAcmDataList.push(acmData)
                params = {
                    "workflowOperation" : workflowOperation,
                    "workflowId" : workflowId,
                    "actionableAgreementId" :  actionableAgreementId,
                    "acms" : agreementMarginCallTypeAcmDataList
                }
            }
            else if(workflowOperation.indexOf("QUICK_PUBLISH") == -1) {
                params = {
                    "workflowOperation" : workflowOperation,
                    "workflowId" : workflowId,
                    "actionableAgreementId" :  actionableAgreementId,
                    "disputeAmounts" : window.treasury.comet.agreementWorkflowDisputeAmount
                    .getAgmtIdToDisputeAmountStr(),
                    "acms" : window.treasury.comet.agreementWorkflowDisputeAmount.getAgmtIdToACMStr(),
                    "asms" : window.treasury.comet.agreementWorkflowDisputeAmount
                    .getAgmtIdToASMStr()
                }
            }
        }



        var url = "/treasury/comet/saveAgreementWorkflowArtifacts";

        var promise = $.ajax({
            url : url,
            data : JSON.stringify(params),
            async : isAsync,
            processData: false,
            contentType: "application/json; charset=UTF-8",
            type: "POST"
        });

        promise.done(function (data) {
            if (data.resultList[0].status === "failure") {
                alert("Error occurred while publishing workflow");
                window.treasury.comet.agreementworkflow.hideLoading();
            } else {

                    if (workflowOperation == "PUBLISH") {
                        let adjustmentDetail = getAdjustmentDetails();
                        let checkAndReload = !adjustmentDetail.isAdjustmentAddedInWorkflow;
                        _closeWorkflowWindow(true, workflowOperation, _getAgreementDetails(adjustmentDetail.maxKnowledgeTime), checkAndReload);
                    } else if (workflowOperation == "SAVE_WORKFLOW_DATA"){
                        _closeWorkflowWindow(true, workflowOperation, {}, true);
                    }


                console.log("Saved Agreement Workflow artifacts Successfully");
            }
        });

        promise.fail(function(xhr, text, errorThrown) {

            if (workflowOperation == 'SAVE_WORKFLOW_DATA') { // Save Workflow
                // Artifacts
                alert("Error occurred while saving workflow");
            } else if (workflowOperation == 'PUBLISH') { // Save Workflow
                // Artifacts
                alert("Error occurred while publishing workflow");
            }

            console.log("Error occurred while initiating workflow");

        });
    }

    function _getAgreementDetails(adjustmentKnowledgeTime) {
        let wireIds = _getWireIdsForCurrentWorkflow();
        let bookingIds = _getBookingIdsForCurrentWorkflow();
        let agmtIdToACMMap = window.treasury.comet.agreementWorkflowDisputeAmount.getAgmtIdToACMMap();
        let agmtIdToASMMap = window.treasury.comet.agreementWorkflowDisputeAmount.getAgmtIdToASMMap();
        let agmtIdToDisputeAmountMap = window.treasury.comet.agreementWorkflowDisputeAmount.getAgmtIdToDisputeAmountMap();
        let segECMRC = window.treasury.comet.agreementWorkflowDisputeAmount.getSegACM();
        let segASMRC = window.treasury.comet.agreementWorkflowDisputeAmount.getSegASM();
        let segDisputeAmountRC = window.treasury.comet.agreementWorkflowDisputeAmount.getSegDisputeAmount();
        let agreementDetails = {};
        let agreementDetail = {};
        let treasuryAgreementId = document.getElementById("actionableAgreementId").value;
        agreementDetails[treasuryAgreementId] = agreementDetail;
        agreementDetail["wireRequest"] = wireIds;
        agreementDetail["bookingReference"] = bookingIds;
        let ecmRC = agmtIdToACMMap[treasuryAgreementId];
        let asmRC = agmtIdToASMMap[treasuryAgreementId];
        let disputeAmountRC = agmtIdToDisputeAmountMap[treasuryAgreementId];
        agreementDetail["ecmRC"] = ecmRC;
        agreementDetail["asmRC"] = asmRC;
        agreementDetail["disputeAmountRC"] = disputeAmountRC;
        agreementDetail["segECMRC"] = segECMRC;
        agreementDetail["segASMRC"] = segASMRC;
        agreementDetail["segDisputeAmountRC"] = segDisputeAmountRC;
        agreementDetail["adjustmentKnowledgeTime"] = adjustmentKnowledgeTime;
        return agreementDetails;

    }

    function _getWireIdsForCurrentWorkflow() {

        let wireIds;
        let agmtWorkflowTopPanelGrid = treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();

        if (agmtWorkflowTopPanelGrid != undefined && agmtWorkflowTopPanelGrid.data != undefined && agmtWorkflowTopPanelGrid.data.length > 0) {
            let wireIdsArray = [];
            for (var i = 0; i < agmtWorkflowTopPanelGrid.data.length; i++) {
                if (agmtWorkflowTopPanelGrid.data[i].wireRequest != "" && window.treasury.comet.Util.isDefined(agmtWorkflowTopPanelGrid.data[i].wireRequest)) {
                    wireIdsArray.push(agmtWorkflowTopPanelGrid.data[i].wireRequest);
                }

            }
            if (wireIdsArray.length > 0) {
                wireIds = wireIdsArray.join(",");
            }
        }

        return wireIds;

    }

    function _getBookingIdsForCurrentWorkflow() {

        let bookingIds;
        let agmtWorkflowTopPanelGrid = treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();

        if (agmtWorkflowTopPanelGrid != undefined && agmtWorkflowTopPanelGrid.data != undefined && agmtWorkflowTopPanelGrid.data.length > 0) {
            let bookingIdsArray = [];
            for (var i = 0; i < agmtWorkflowTopPanelGrid.data.length; i++) {
                if (agmtWorkflowTopPanelGrid.data[i].bookingReference != "" && window.treasury.comet.Util.isDefined(agmtWorkflowTopPanelGrid.data[i].bookingReference)) {
                    bookingIdsArray.push(agmtWorkflowTopPanelGrid.data[i].bookingReference);
                }

            }
            if (bookingIdsArray.length > 0) {
                bookingIds = bookingIdsArray.join(",");
            }
        }

        return bookingIds;
    }

    function getAdjustmentDetails() {

        let isAdjustmentAddedInWorkflow = false;
        let maxKnowledgeTime = null;
        let agreementIdToAdjustmentMap = window.treasury.comet.agreementAdjustmentGridAction.getAgreementIdToAdjustmentMap();
        Object.keys(agreementIdToAdjustmentMap).forEach(function (key) {
            let adjustments = agreementIdToAdjustmentMap[key];
            if (adjustments != undefined && adjustments.length > 0) {
                for (var i = 0; i < adjustments.length; i++) {
                    if (adjustments[i].knowledgeTime == undefined) {
                        isAdjustmentAddedInWorkflow = true;
                        break;
                    } else {
                        let knowledgeTime = new Date(adjustments[i].knowledgeTime);
                        if (maxKnowledgeTime == null || knowledgeTime > maxKnowledgeTime) {
                            maxKnowledgeTime = knowledgeTime;
                        }
                    }
                }
            }
        });

        let adjustmentDetail = {
           "isAdjustmentAddedInWorkflow": isAdjustmentAddedInWorkflow,
            "maxKnowledgeTime": maxKnowledgeTime
        };

        return adjustmentDetail;

    }

    /**
     * Quick Generate Call Report will Begin the Workflow and Generate the call
     * Report. This method provides support for generating multiple call reports
     * at once.
     */
    function _quickGenerateCallReport(quikGenerateCallReportParams) {

        var quickGenerateCallReportParamArray = quikGenerateCallReportParams
                .split(':');

        if (quickGenerateCallReportParamArray.length > 0) {
            let agreementIds = [];

            for (var i = 0; i < quickGenerateCallReportParamArray.length; i++) {

                // Generating Call report for each selected agreement
                beginWorkflowAndGenerateCallReport(quickGenerateCallReportParamArray[i]);
                agreementIds.push(_extractTreasuryAgreementId(quickGenerateCallReportParamArray[i]));

            }

            let agreementDetails = {};

            for(var i=0; i < agreementIds.length; i++) {
                let agreementDetail = {
                    "workflowStatus": "IN_PROGRESS"
                };
                agreementDetails[agreementIds[i]] = agreementDetail;
            }

            if (confirm("Call report generation from the summary screen changes the status of the corresponding agreement workflow. Do you want to reload this screen?") == true) {
                window.treasury.comet.agreementsummary.reloadAgreementSummary(true, agreementDetails);
            }
        }

    }

    function beginWorkflowAndGenerateCallReport(params) {

        // Get Workflow Params
        var workflowParams = _getWorkflowParam(params);

        // Begin Workflow with async false
        var promise = _beginWorkflow(workflowParams, false);

        promise.done(function(data) {

            // After successful begin workflow generate call report
            var workflowId = data.resultList[0].workflowId;
            _quickCallReport(workflowId, _extractTreasuryAgreementId(params));

        });

    }

    /**
     * TODO : Position level data to be send in call report
     */
    function _quickCallReport(workflowId, agreementId, treasuryAgreementId=null) {
        if(treasuryAgreementId == null) {
            treasuryAgreementId = agreementId;
        }
        var saveCallReportUrl = "/treasury/service/callReportService/emailCallReport";
        var saveCallReportParam = "workflowId=" + workflowId;
        saveCallReportParam += "&actionableAgreementId=" + agreementId;
        saveCallReportParam += "&treasuryAgreementId=" + treasuryAgreementId;
        saveCallReportParam += '&inputFormat=PROPERTIES&format=json';

        _generateCallReport(saveCallReportUrl, saveCallReportParam)

    }

    function _generateCallReport(saveCallReportUrl, saveCallReportParam) {
        var promise = $.ajax({
            url : saveCallReportUrl,
            data : saveCallReportParam,
            dataType : "json",
            type : "GET",
            beforeSend: function () { alert("We will mail your requested Call Report shortly."); },
        });

        promise.done(function(data) {

            alert("Call report generated and emailed successfully.");
            console.log("Call report generated and emailed successfully.");

        });
    }

    function _getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return year + '/' + month + '/' + day;
    }

    function _downloadBrokerfile(brokerFileUrl, date) {

      if(brokerFileUrl.indexOf('transfers') != -1) {
        location.href = 'cometFileDownload?fileName=&contentType=&fileUrl='
            + brokerFileUrl;
      }else {
      window.open('/cocoa/api/file/downloadFile?objectUri=&fileId='+brokerFileUrl+'&fileDate='+date);
    }

    }

    function _downloadCrifFile(crifFileUrl) {
        let downloableFileName = 'CRIF_file';
        window.open('downloadSimulationReport?fileName=' + downloableFileName + ".xls&contentType=&fileUrl=" + crifFileUrl, '_blank');
    }

})();
