"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementWorkflowDisputeAmount = {
            updateDisputeAmountMap : _updateDisputeAmountMap,
            updateACMData : _updateACMData,
            updateASMData : _updateASMData,
            getAgmtIdToDisputeAmountMap : _getAgmtIdToDisputeAmountMap,
            getAgmtIdToACMMap : _getAgmtIdToACMMap,
            getAgmtIdToASMMap : _getAgmtIdToASMMap,
            getAgmtIdToDisputeAmountStr : _getAgmtIdToDisputeAmountStr,
            getAgmtIdToACMStr : _getAgmtIdToACMStr,
            getAgmtIdToASMStr : _getAgmtIdToASMStr,
            getSegACM : _getSegACM,
            getSegASM : _getSegASM,
            getSegDisputeAmount : _getSegDisputeAmount
    };

    var _agmtIdToDisputeAmountMap;
    var _agmtIdToACMMap;
    var _agmtIdToASMMap;

    function _updateDisputeAmountMap(agreementId, disputeAmount) {

        if(_agmtIdToDisputeAmountMap == null) {
            _agmtIdToDisputeAmountMap = new Object();
        }

        if(agreementId != undefined && disputeAmount != undefined && disputeAmount != '') {
            _agmtIdToDisputeAmountMap[agreementId] = disputeAmount;
        }
    }

    function _updateACMData(agreementId, expectedCashMovement) {

        if(_agmtIdToACMMap == null) {
            _agmtIdToACMMap = new Object();
        }

        if(agreementId != undefined && expectedCashMovement != undefined && expectedCashMovement != '') {
            _agmtIdToACMMap[agreementId] = expectedCashMovement;
        }
    }

    function _updateASMData(agreementId, actualSecuritiesMovement) {

        if(_agmtIdToASMMap == null) {
            _agmtIdToASMMap = new Object();
        }

        if(agreementId != undefined && actualSecuritiesMovement != undefined && actualSecuritiesMovement != '') {
            _agmtIdToASMMap[agreementId] = actualSecuritiesMovement;
        }
    }

    function _getAgmtIdToDisputeAmountMap() {
        return _agmtIdToDisputeAmountMap;
    }

    function _getAgmtIdToACMMap() {
        return _agmtIdToACMMap;
    }

    function _getAgmtIdToASMMap() {
        return _agmtIdToASMMap;
    }

    function _getSegACM() {

        var agmtWorkflowTopPanelGrid = treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();
        let segACMRC = 0;
        if (agmtWorkflowTopPanelGrid != undefined && agmtWorkflowTopPanelGrid.data.length > 0) {
            for (var i = 0; i < agmtWorkflowTopPanelGrid.data.length; i++) {
                if (agmtWorkflowTopPanelGrid.data[i].isActionableAgreement && agmtWorkflowTopPanelGrid.data[i].tag == "SEG") {
                    segACMRC += isNaN(agmtWorkflowTopPanelGrid.data[i].acmRC) ? 0 : Number(agmtWorkflowTopPanelGrid.data[i].acmRC);
                }
            }
        }
        return segACMRC;
    }

    function _getSegASM() {

        var agmtWorkflowTopPanelGrid = treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();
        let segASMRC = 0;
        if (agmtWorkflowTopPanelGrid != undefined && agmtWorkflowTopPanelGrid.data.length > 0) {
            for (var i = 0; i < agmtWorkflowTopPanelGrid.data.length; i++) {
                if (agmtWorkflowTopPanelGrid.data[i].isActionableAgreement && agmtWorkflowTopPanelGrid.data[i].tag == "SEG") {
                    segASMRC += isNaN(agmtWorkflowTopPanelGrid.data[i].asmRC) ? 0 : Number(agmtWorkflowTopPanelGrid.data[i].asmRC);
                }
            }
        }
        return segASMRC;
    }

    function _getSegDisputeAmount() {

        var agmtWorkflowTopPanelGrid = treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();
        let segDisputeAmountRC = 0;
        if (agmtWorkflowTopPanelGrid != undefined && agmtWorkflowTopPanelGrid.data.length > 0) {
            for (var i = 0; i < agmtWorkflowTopPanelGrid.data.length; i++) {
                if (agmtWorkflowTopPanelGrid.data[i].isActionableAgreement && agmtWorkflowTopPanelGrid.data[i].tag == "SEG") {
                    segDisputeAmountRC += isNaN(agmtWorkflowTopPanelGrid.data[i].disputeAmountRC) ? 0 : Number(agmtWorkflowTopPanelGrid.data[i].disputeAmountRC);
                }
            }
        }
        return segDisputeAmountRC;
    }


    function _getAgmtIdToDisputeAmountStr() {
        var  agreementMarginCallTypeDisputeAmountDataList = new Array();

        if(_agmtIdToDisputeAmountMap == undefined) {
          _agmtIdToDisputeAmountMap = new Object();
}
            var agmtWorkflowTopPanelGrid = treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();


        if (agmtWorkflowTopPanelGrid != undefined && agmtWorkflowTopPanelGrid.data.length > 0) {
            for (var i = 0; i < agmtWorkflowTopPanelGrid.data.length; i++) {
                var agreementId = agmtWorkflowTopPanelGrid.data[i].agreementId;
                var marginCallType = agmtWorkflowTopPanelGrid.data[i].tag;
                if (marginCallType == null || marginCallType == undefined || marginCallType == "") {
                    marginCallType = "HOUSE"
                }
                //To handle cases for actionable ISDA
                if (agreementId < 0) {
                    agreementId = -1 * agreementId
                }
                if (agmtWorkflowTopPanelGrid.data[i].isActionableAgreement) {
                    _agmtIdToDisputeAmountMap[agreementId] = agmtWorkflowTopPanelGrid.data[i].disputeAmountRC;
                    var disputeAmountData = {
                        "agreementId" : agreementId,
                        "marginCallType" : marginCallType,
                        "amountString" : _getZeroIfUndefined( agmtWorkflowTopPanelGrid.data[i].disputeAmountRC).toString()
                    }
                    agreementMarginCallTypeDisputeAmountDataList.push(disputeAmountData)
                }
            }
        }
        return agreementMarginCallTypeDisputeAmountDataList;
    }

    function _getAgmtIdToACMStr() {
        var  agreementMarginCallTypeAcmDataList = new Array();

        if(_agmtIdToACMMap == undefined) {
            _agmtIdToACMMap = new Object();
          }
            var agmtWorkflowTopPanelGrid = treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();



        if (agmtWorkflowTopPanelGrid != undefined && agmtWorkflowTopPanelGrid.data.length > 0) {
            for (var i = 0; i < agmtWorkflowTopPanelGrid.data.length; i++) {
                var agreementId = agmtWorkflowTopPanelGrid.data[i].agreementId;
                var marginCallType = agmtWorkflowTopPanelGrid.data[i].tag;
                if (marginCallType == null || marginCallType == undefined || marginCallType == "") {
                    marginCallType = "HOUSE"
                }
                //To handle cases for actionable ISDA
                if (agreementId < 0) {
                    agreementId = -1 * agreementId
                }
                if (agmtWorkflowTopPanelGrid.data[i].isActionableAgreement) {
                    _agmtIdToACMMap[agreementId] = agmtWorkflowTopPanelGrid.data[i].acmRC;
                    var acmData = {
                        "agreementId" : agreementId,
                        "marginCallType" : marginCallType,
                        "amountString" : _getZeroIfUndefined( agmtWorkflowTopPanelGrid.data[i].acmRC).toString()
                    }
                    agreementMarginCallTypeAcmDataList.push(acmData)
                }
            }
        }
        return agreementMarginCallTypeAcmDataList;
    }

    function _getAgmtIdToASMStr() {
      var  agreementMarginCallTypeAsmDataList = new Array();
      if (_agmtIdToASMMap == undefined) {
        _agmtIdToASMMap = new Object();
      }
      var agmtWorkflowTopPanelGrid = treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();
      if (
        agmtWorkflowTopPanelGrid != undefined &&
        agmtWorkflowTopPanelGrid.data.length > 0
      ) {
        for (var i = 0; i < agmtWorkflowTopPanelGrid.data.length; i++) {
          var agreementId = agmtWorkflowTopPanelGrid.data[i].agreementId;
          var marginCallType = agmtWorkflowTopPanelGrid.data[i].tag;
          if (marginCallType == null || marginCallType == undefined || marginCallType == "") {
             marginCallType = "HOUSE"
          }
          //To handle cases for actionable ISDA
          if (agreementId < 0) {
            agreementId = -1 * agreementId;
          }
          if (agmtWorkflowTopPanelGrid.data[i].isActionableAgreement) {
            _agmtIdToASMMap[agreementId] =
              agmtWorkflowTopPanelGrid.data[i].asmRC;
            var asmData = {
                "agreementId" : agreementId,
                "marginCallType" : marginCallType,
                "amountString" : _getZeroIfUndefined(agmtWorkflowTopPanelGrid.data[i].asmRC).toString()
            }
            agreementMarginCallTypeAsmDataList.push(asmData)
          }
        }
      }
      return agreementMarginCallTypeAsmDataList;
    }
    function _getZeroIfUndefined(inputVal) {
        return Number((inputVal == null || isNaN(inputVal)) ? 0 : inputVal)
    }

})();
