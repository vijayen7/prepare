"user strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.bookMmfTrade = {
            displayBookMmfPopup : _displayBookMmfPopup,
            bookMmfTrade : _bookMmfTrade
    };

    /**
     * Helper function to display book mmf popup
     */
    function _displayBookMmfPopup (displayBookMmfParam) {

        window.treasury.comet.agreementworkflow.showLoading();
        // Service to display book mmf popup
        var displayBookMmfPopupUrl = '/treasury/comet/displayBookMmfPopup';

        var workflowId = displayBookMmfParam.workflowId;
        var actionableAgreementId = displayBookMmfParam.actionableAgreementId;
        var treasuryAgreementId = displayBookMmfParam.treasuryAgreementId;
        var wireRequestNo = displayBookMmfParam.wireRequestNo;
        var mmfAmount = displayBookMmfParam.mmfAmount;
        var reportingCurrencyFxRate = displayBookMmfParam.reportingCurrencyFxRate;

        var displayBookMmfParams;

        if(workflowId != undefined) {
            displayBookMmfParams = "workflowId="+workflowId;
        }
        if(treasuryAgreementId != undefined) {
            displayBookMmfParams += "&treasuryAgreementId="+treasuryAgreementId;
        }
        if(actionableAgreementId != undefined) {
            displayBookMmfParams += "&actionableAgreementId="+actionableAgreementId;
        }
        if(workflowId != undefined) {
            displayBookMmfParams += "&wireRequestNo="+wireRequestNo;
        }
        if(workflowId != undefined) {
            displayBookMmfParams += "&mmfAmount="+mmfAmount;
        }
        if(reportingCurrencyFxRate != undefined) {
            displayBookMmfParams += "&reportingCurrencyFxRate="+reportingCurrencyFxRate;
        }

        var promise = $.ajax({
            url : displayBookMmfPopupUrl,
            data : displayBookMmfParams,
            dataType : "json",
            type : "GET"
        });

        promise.done(function(data) {




            // Setting values to properties and revealing Log Wire popup
            var segCustodianAccountName = document.getElementById("segCustodianAccountName");
            var mmfSpn = document.getElementById("mmfSpn");
            var amount = document.getElementById("amount");
            var description = document.getElementById("description");

            segCustodianAccountName.value = data.resultList[0].segCustodianAccountAbbrev;
            mmfSpn.value = data.resultList[0].mmfSpn;
            amount.value = data.resultList[0].mmfAmount;
            description.value = data.resultList[0].description;
            window.treasury.comet.agreementworkflow.hideLoading();

            var bookMmfParam = data.resultList[0];
            document.getElementById('bookMmfPopup').reveal({
                'title' : 'COMET - Book mmf trades wires',
                'modal' : true,
                'buttons' : [ {
                    'html' : 'Book trades',
                    'callback' : function() {
                        _bookMmfTrade(bookMmfParam);
                    },
                    'position' : 'center'
                }, {
                    'html' : 'Cancel',
                    'callback' : function() {
                        _cancelBookMmfPopup();
                    },
                    'position' : 'center'
                } ]
            });

        });
    }

    function _bookMmfTrade(bookMmfParams) {
        // Service to display book mmf popup
        var bookMmfTradeUrl = '/treasury/comet/bookMmfTrade';

        var bookMmfTradeParam;
        var segCustodianAccountAbbrev = bookMmfParams.segCustodianAccountAbbrev;
        var mmfAmount = bookMmfParams.mmfAmount;
        var actionableAgreementId = bookMmfParams.actionableAgreementId;
        var wireRequestNo = bookMmfParams.wireRequestNo;
        var mmfSpn = document.getElementById("mmfSpn").value;
        var description = bookMmfParams.description;
        var segCustodianAccountId = bookMmfParams.segCustodianAccountId;
        var reportingCurrencyFxRate = bookMmfParams.reportingCurrencyFxRate;

        if(segCustodianAccountAbbrev!= undefined) {
            bookMmfTradeParam = "segCustodianAccountAbbrev="+segCustodianAccountAbbrev;
        }
        if(mmfAmount!= undefined) {
            bookMmfTradeParam += "&mmfAmount="+mmfAmount;
        }
        if(segCustodianAccountAbbrev!= undefined) {
            bookMmfTradeParam += "&actionableAgreementId="+actionableAgreementId;
        }
        if(segCustodianAccountAbbrev!= undefined) {
            bookMmfTradeParam += "&wireRequestNo="+wireRequestNo;
        }
        if(segCustodianAccountAbbrev!= undefined) {
            bookMmfTradeParam += "&mmfSpn="+mmfSpn;
        }
        if(segCustodianAccountAbbrev!= undefined) {
            bookMmfTradeParam += "&description="+description;
        }
        if(segCustodianAccountAbbrev!= undefined) {
            bookMmfTradeParam += "&segCustodianAccountId="+segCustodianAccountId;
        }
        if(reportingCurrencyFxRate!= undefined) {
            bookMmfTradeParam += "&reportingCurrencyFxRate="+reportingCurrencyFxRate;
        }

        var promise = $.ajax({
                url : bookMmfTradeUrl,
                data : bookMmfTradeParam,
                dataType : "json",
                type : "POST"
            });

        promise.done(function(data) {
            var bookMmfObj =data.resultList[0];
            var bookMmfMsg = bookMmfObj.bookMmfTradeMsg;
            var mmfBookingSuccessFlag = bookMmfObj.mmfBookingSuccessFlag;

            if(bookMmfMsg != undefined) {
                alert(bookMmfMsg);
            }

            if(mmfBookingSuccessFlag) {

                document.querySelector('#bookMmfPopup').visible = false;
            }
        })
    }

    /**
     * Helper function to clear the book mmf popup
     */
    function _cancelBookMmfPopup() {
        if(!confirm("Do you really want to close popup?") == true){
                return;
        }


        document.querySelector('#bookMmfPopup').visible = false;
    }
})();