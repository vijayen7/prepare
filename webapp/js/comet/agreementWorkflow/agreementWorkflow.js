"user strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementworkflow = {
            showLoading : _showLoading,
            hideLoading : _hideLoading
    };

    var _rightPanelPreviousState;

    function _showLoading() {
        document.getElementById("agreementWorkflowTopPanelGridLoading").show();
    }

    function _hideLoading() {
        document.getElementById("agreementWorkflowTopPanelGridLoading").hide();
    }

    /**
     * Called on agreement workflow page load
     */
    function _initialize() {

        // Minimize the right panel
//        var workflowRightPanel = document.getElementById("agreementWorkflowRightBar");
//        workflowRightPanel.toggle();
//
//        // Adding call backs to right panel to listen and resize the grid
//        workflowRightPanel.addEventListener('stateChange', function(){
//            var rightSideBarPreviousState = treasury.comet.agreementWorkflowDetail.getWorkflowDetailPreviousState();
//            var rightSideBarCurrentState = document.getElementById('agreementWorkflowRightBar').state;
//            if(rightSideBarPreviousState === undefined || rightSideBarPreviousState != rightSideBarCurrentState) {
//                treasury.comet.agreementworkflow.showLoading();
//                treasury.comet.agreementWorkflowDetail.setWorkflowDetailPreviousState(rightSideBarCurrentState);
//
//                var parentArcLayout = document.getElementById('parentArcLayout');
//                parentArcLayout.setLayout();
//
//                // Workflow screen all grids resizing
//                setTimeout(function() {_resizeAgreementWorkflowGrids();
//                treasury.comet.agreementworkflow.hideLoading();},0);
//            }
//        });

        // Loading agreement workflow top panel, position and adjustment data
        _loadAgreementWorkflowTopPanel();

    }

    /**
     * Helper method to load data in agreement workflow top panel
     */
    function _loadAgreementWorkflowTopPanel() {
        window.treasury.comet.agreementWorkflowGridAction.loadAgreementWorkflowData();
    }

    /**
     * Helper method to resize all the grids on agreement workflow page
     */
    function _resizeAgreementWorkflowGrids() {
        // resizing agreement workflow top panel
        window.treasury.comet.agreementWorkflowGridAction.resizeAgreementWorkflowTopPanelGrid();
        // resizing agreement adjustment grid
        window.treasury.comet.agreementAdjustmentGridAction.resizeAgreementAdjustmentGrid();
        // resizing position grids
        window.treasury.comet.agreementPositionGridAction.resizeAgreementPositionGrid();
    }

     WebComponents.waitFor(function() {
        var tabsContainerEd = document.querySelector('#excessDeficitSummaryPanelId');
        var tabsContainerPd = document.querySelector('#positionDataPanelId');
        if (tabsContainerEd) {
          tabsContainerEd.initializeTabs(tabsContainerEd);
        }
        if (tabsContainerPd) {
          tabsContainerPd.initializeTabs(tabsContainerPd);
        }
        _initialize();
      });



})();