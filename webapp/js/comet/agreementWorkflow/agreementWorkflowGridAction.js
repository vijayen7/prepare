'user strict';

(function () {
  window.treasury = window.treasury || {};
  window.treasury.comet = window.treasury.comet || {};
  window.treasury.comet.agreementWorkflowGridAction = {
    loadAgreementWorkflowData: _loadAgreementWorkflowData,
    resizeAgreementWorkflowTopPanelGrid: _resizeAgreementWorkflowTopPanelGrid,
    getAgreementWorkflowTopPanelGrid: _getAgreementWorkflowTopPanelGrid,
    getAgreementIdToDisputeMap: _getAgreementIdToDisputeMap,
    getAgreementIdToExpectedCashMovement: _getAgreementIdToExpectedCashMovement,
    getAgreementIdToActualSecuritiesMovement: _getAgreementIdToActualSecuritiesMovement,
    getSelectedRow: _getSelectedRow,
    getSelectedRowId: _getSelectedRowId,
    loadPositionData: _loadPositionData,
    loadBundlePositionData: _loadBundlePositionData,
    setUseAgreementWorkflowService: _setUseAgreementWorkflowService
  };

  // Stores the instance of agreement workflow top panel grid
  var _agreementWorkflowTopPanelGrid;
  var _agreementIdToDisputeMap;
  var _agreementIdToExpectedCashMovement;
  var _agreementIdToActualSecuritiesMovement;
  var _selectedRow;
  var _selectedRowId;

  var _useAgreementWorkflowService = false;

  function _setUseAgreementWorkflowService(flag) {
    _useAgreementWorkflowService = flag;
  }

  function _getAgreementIdToDisputeMap() {
    return _agreementIdToDisputeMap;
  }

  function _getAgreementIdToExpectedCashMovement() {
    return _agreementIdToExpectedCashMovement;
  }

  function _getAgreementIdToActualSecuritiesMovement() {
    return _agreementIdToActualSecuritiesMovement;
  }

  function _getAgreementWorkflowTopPanelGrid() {
    return _agreementWorkflowTopPanelGrid;
  }

  function _getSelectedRow() {
    return _selectedRow;
  }

  function _getSelectedRowId() {
    return _selectedRowId;
  }

  /**
   * Called after starting the workflow, gets the agreement workflow data and
   * starts the grid
   */ function _loadPositionData() {
    var workflowId = document.getElementById('workflowId').value;
    var date = document.getElementById('date').value;
    var actionableAgreementId = document.getElementById('actionableAgreementId').value;

    var promise = getPromiseToFetchEDWorkflowDataList(actionableAgreementId, workflowId, date, true, false);

    promise.done(function (data) {

      let excessDeficitWorkflowDataList = parseEDWorkflowData(_useAgreementWorkflowService, data);

      window.treasury.comet.agreementPositionGridAction.setIsPosLoaded(true);
      if (excessDeficitWorkflowDataList && excessDeficitWorkflowDataList.length) {
        var treasuryAgreementData = excessDeficitWorkflowDataList[0];
        treasuryAgreementData.workflowId = workflowId;

        // Setting data in position and adjustment actions to be
        // displayed on agreement workflow page
        window.treasury.comet.agreementPositionGridAction.setExcessDeficitWorkerData(excessDeficitWorkflowDataList);
        window.treasury.comet.agreementAdjustmentGridAction.setExcessDeficitWorkerData(excessDeficitWorkflowDataList);

        // Handling position level data display
        window.treasury.comet.agreementPositionGridAction.handlePositionDataDisplay();

        for (var i = 0; i < excessDeficitWorkflowDataList.length; i++) {
          excessDeficitWorkflowDataList[i].workflowId = workflowId;
          excessDeficitWorkflowDataList[i].internalED = excessDeficitWorkflowDataList[i].adjustedExcessDeficit;
          excessDeficitWorkflowDataList[i].internalEDRC = excessDeficitWorkflowDataList[i].adjustedExcessDeficitRC;
          // Handling agreement id for actionnable ISDA for adjustments/wires/call report calls..
          var agreementId = excessDeficitWorkflowDataList[i].agreementId;
          if (agreementId < 0) {
            agreementId = -1 * agreementId;
          }
        }

        // Handling adjustment data display
        window.treasury.comet.agreementAdjustmentGridAction.handleAdjustmentDataDisplay();
      }
    });

    promise.fail(function (xhr, text, errorThrown) {
      showErrorDialog('Error occured while loading Exposure & Requirement data');
    });

  }

  function _loadBundlePositionData() {
    var workflowId = document.getElementById('workflowId').value;
    var date = document.getElementById('date').value;
    var actionableAgreementId = document.getElementById('actionableAgreementId').value;
    var promise = getPromiseToFetchEDWorkflowDataList(actionableAgreementId, workflowId, date, true, true);

    promise.done(function (data) {

      let excessDeficitWorkflowDataList = parseEDWorkflowData(_useAgreementWorkflowService, data);

      window.treasury.comet.agreementPositionGridAction.setIsBundlePosLoaded(true);
      if (excessDeficitWorkflowDataList && excessDeficitWorkflowDataList.length) {
        var treasuryAgreementData = excessDeficitWorkflowDataList[0];
        treasuryAgreementData.workflowId = workflowId;

        // Setting data in position and adjustment actions to be
        // displayed on agreement workflow page
        window.treasury.comet.agreementPositionGridAction.setExcessDeficitWorkerData(excessDeficitWorkflowDataList);
        window.treasury.comet.agreementAdjustmentGridAction.setExcessDeficitWorkerData(excessDeficitWorkflowDataList);

        // Handling position level data display
        window.treasury.comet.agreementPositionGridAction.handlePositionDataDisplay();

        for (var i = 0; i < excessDeficitWorkflowDataList.length; i++) {
          excessDeficitWorkflowDataList[i].workflowId = workflowId;
          excessDeficitWorkflowDataList[i].internalED = excessDeficitWorkflowDataList[i].adjustedExcessDeficit;
          excessDeficitWorkflowDataList[i].internalEDRC = excessDeficitWorkflowDataList[i].adjustedExcessDeficitRC;
          // Handling agreement id for actionnable ISDA for adjustments/wires/call report calls..
          agreementId = excessDeficitWorkflowDataList[i].agreementId;
          if (agreementId < 0) {
            agreementId = -1 * agreementId;
          }
        }

        // Handling adjustment data display
        window.treasury.comet.agreementAdjustmentGridAction.handleAdjustmentDataDisplay();
      }
    });

    promise.fail(function (xhr, text, errorThrown) {
      showErrorDialog('Error occured while loading Usage data');
    });
  }

  function _loadAgreementWorkflowData() {
    // Showing loading icon on agreement workflow page
    window.treasury.comet.agreementworkflow.showLoading();

    var workflowId = document.getElementById('workflowId').value;
    var date = document.getElementById('date').value;
    var actionableAgreementId = document.getElementById('actionableAgreementId').value;
    let promise = getPromiseToFetchEDWorkflowDataList(actionableAgreementId, workflowId, date, false, false);

    promise.done(function (data) {

      let excessDeficitWorkflowDataList = parseEDWorkflowData(_useAgreementWorkflowService, data);

      // Hiding loading icon
      window.treasury.comet.agreementworkflow.hideLoading();

      var searchDate = document.getElementById('searchDate');
      searchDate.innerHTML = date.substring(0, 4) + '-' + date.substring(4, 6) + '-' + date.substring(6, 8);
      document.getElementById('workflowDateInfo').removeAttribute('hidden');

      if (excessDeficitWorkflowDataList && excessDeficitWorkflowDataList.length) {
        window.treasury.comet.agreementWorkflowDetail.expandExcessDeficitSummaryPanel();
        // Getting the Agreement Workflow Top Panel Data
        var agreementWorkflowTopPanelGrid = document.getElementById('agreementWorkflowTopPanelGrid');
        agreementWorkflowTopPanelGrid.style.display = 'block';

        var treasuryAgreementData = excessDeficitWorkflowDataList[0];
        treasuryAgreementData.workflowId = workflowId;

        // Setting data in position and adjustment actions to be
        // displayed on agreement workflow page
        window.treasury.comet.agreementPositionGridAction.setExcessDeficitWorkerData(excessDeficitWorkflowDataList);
        window.treasury.comet.agreementAdjustmentGridAction.setExcessDeficitWorkerData(excessDeficitWorkflowDataList);

        // Handling position level data display
        window.treasury.comet.agreementPositionGridAction.handlePositionDataDisplay();

        for (var i = 0; i < excessDeficitWorkflowDataList.length; i++) {
          excessDeficitWorkflowDataList[i].workflowId = workflowId;
          excessDeficitWorkflowDataList[i].internalED = excessDeficitWorkflowDataList[i].adjustedExcessDeficit;
          excessDeficitWorkflowDataList[i].internalEDRC = excessDeficitWorkflowDataList[i].adjustedExcessDeficitRC;

          // Handling agreement id for actionnable ISDA for adjustments/wires/call report calls..
          agreementId = excessDeficitWorkflowDataList[i].agreementId;
          if (agreementId < 0) {
            agreementId = -1 * agreementId;
          }
        }

        if (treasuryAgreementData.agreementType == 'MNA') {
          _displayMNAAgreement(excessDeficitWorkflowDataList);
        } else {
          _displayNonMNAAgreement(treasuryAgreementData);
        }

        // Handling adjustment data display
        window.treasury.comet.agreementAdjustmentGridAction.handleAdjustmentDataDisplay();

        var agreementWorkflowDetail = document.getElementById('agreementWorkflowRightBar');
        agreementWorkflowDetail.addEventListener('stateChange', function () {
          _computeAndChangeGridHeight();
        });

        var detailInformation = document.getElementById('detailInformation');
        detailInformation.addEventListener('stateChange', function () {
          _computeAndChangeGridHeight();
        });

        // increasing grid height
        $('#' + 'agreementWorkflowTopPanelGrid').height($('#' + 'agreementWorkflowTopPanelGrid').height() + 11);
      }
    });

    promise.fail(function (xhr, text, errorThrown) {
      showErrorDialog('Error occured while loading workflow data');
    });

    _loadPositionData();
    //_loadBundlePositionData();
  }

  function _computeAndChangeGridHeight() {
    setTimeout(window.treasury.comet.agreementAdjustmentGridAction.resizeAgreementAdjustmentGrid, 0);
  }

  /**
   * Helper method to display MNA agreements
   */
  function _displayMNAAgreement(excessDeficitWorkflowDataList) {
    
    var options = _getAgreementWorkflowTopPanelGridOptions();
    var isPopulateAsm = false;
    if(excessDeficitWorkflowDataList != null && excessDeficitWorkflowDataList.length > 0) {
      isPopulateAsm = excessDeficitWorkflowDataList[0].isPopulateAsm;
    }
    var columns = window.treasury.comet.agreementWorkflowColumnConfig.getAgreementWorkflowTopPanelColumns(isPopulateAsm);
    for (var i = 0; i < excessDeficitWorkflowDataList.length; i++) {
      excessDeficitWorkflowDataList[i].internalED = excessDeficitWorkflowDataList[i].adjustedExcessDeficit;
      if (
        excessDeficitWorkflowDataList[i].children != null &&
        excessDeficitWorkflowDataList[i].children != undefined &&
        excessDeficitWorkflowDataList[i].children.length > 0
      ) {
        for (var j = 0; j < excessDeficitWorkflowDataList[i].children.length; j++) {
          excessDeficitWorkflowDataList[i].children[j].internalED = excessDeficitWorkflowDataList[i].children[j].adjustedExcessDeficit;
          excessDeficitWorkflowDataList[i].children[j].internalEDRC = excessDeficitWorkflowDataList[i].children[j].adjustedExcessDeficitRC;
          excessDeficitWorkflowDataList[i].children[j].workflowId = workflowId;
        }
      }
    }

    // Initializing the agreement workflow top panel
    _agreementWorkflowTopPanelGrid = new dportal.grid.createGrid(
      $('#agreementWorkflowTopPanelGrid'),
      excessDeficitWorkflowDataList,
      columns,
      options
    );
    window.treasury.comet.agreementWorkflowDetail.populateRightSideBar(excessDeficitWorkflowDataList[0]);
    _agreementWorkflowTopPanelGrid.grid.onBeforeEditCell.subscribe(function (e, args) {
      return args.item.isActionableAgreement;
    });
    _agreementWorkflowTopPanelGrid.collapseAll();
  }

  /**
   * Helper method to display non MNA agreements
   */
  function _displayNonMNAAgreement(data) {
    var isPopulateAsm = false;
    if(data.isPopulateAsm != null && data.isPopulateAsm != undefined && data.isPopulateAsm == true) {
      isPopulateAsm = true;
    }
    var columns = window.treasury.comet.agreementWorkflowColumnConfig.getAgreementWorkflowTopPanelColumns(isPopulateAsm);
    var options = _getAgreementWorkflowTopPanelGridOptions();
    data.internalED = data.adjustedExcessDeficit;
    data.internalEDRC = data.adjustedExcessDeficitRC;
    var disputeAmount = data.adjustedExcessDeficit;

    if (data.children != null && data.children != undefined && data.children.length > 0) {
      data.children[0].wireRequest = data.wireRequest;
      data.children[0].bookingReference = data.bookingReference;
      for (var j = 0; j < data.children.length; j++) {
        data.children[j].internalED = data.children[j].adjustedExcessDeficit;
        data.children[j].internalEDRC = data.children[j].adjustedExcessDeficitRC;
      }
    }

    // Initializing the agreement workflow top panel
    _agreementWorkflowTopPanelGrid = new dportal.grid.createGrid(
      $('#agreementWorkflowTopPanelGrid'),
      data.children,
      columns,
      options
    );
    window.treasury.comet.agreementWorkflowDetail.populateRightSideBar(data.children[0]);
    _agreementWorkflowTopPanelGrid.grid.onBeforeEditCell.subscribe(function (e, args) {
      return args.item.isActionableAgreement;
    });
  }

  /**
   *
   * Helper method that rounds off Acm and DisputeAmount
   */
  function roundValue(input, digits) {
    var output = 0;

    if (input > 0) {
      output = input % digits == 0 ? input : input - (input % digits) + digits;
    } else if (input < 0) {
      // when acm is -ve, we can't round up as its a margin pull
      output = input % digits == 0 ? input : input - (input % digits);
    }
    return output;
  }

  /**
   * Agreement workflow top panel grid option
   */
  function _getAgreementWorkflowTopPanelGridOptions() {
    var options = {
      editable: true,
      exportToExcel: false,
      highlightRowOnClick: true,
      enableCellNavigation: true,
      autoHorizontalScrollBar: true,
      useAvailableScreenSpace: true,
      customColumnSelection: false,
      displaySummaryRow: false,
      frozenColumn: 1,
      asyncEditorLoading: false,
      autoEdit: true,
      nestedTable: true,
      nestedField: 'agreementName',
      configureColumns: false,
      // If broker file url is not null set the broker file url in right

      onCellClick: function (args, isNotToggle) {
        if (args.event.target.classList.contains('icon-add')) {
          var agreementId = args.item.agreementId;
          if (agreementId < 0) {
            agreementId = -1 * agreementId;
          }
          args.event.stopPropagation();
          window.treasury.comet.agreementAdjustmentAction.setWorkflowId(workflowId.value);
          window.treasury.comet.agreementAdjustmentAction.setSourceGridConfig('Agreement');
          window.treasury.comet.agreementAdjustmentAction.setSelectedAgreementId(agreementId);
          window.treasury.comet.agreementAdjustmentAction.handleAdjustmentPopupAgreementWorkflowDisplay(
            date.value,
            args.item.legalEntityId,
            args.item.exposureCounterPartyId,
            args.item.agreementTypeId,
            args.item.agreementType,
            args.item.reportingCurrency
          );
        } else if (args.event.target.classList.contains('icon-download')) {
          args.event.stopPropagation();
          var brokerFile = document.getElementById('selectBrokerFile'+args.item.agreementId);
          if(brokerFile != null && brokerFile!=undefined)
          window.treasury.comet.agreementWorkflowAction.downloadBrokerfile(
            brokerFile.value, date.value);

        } else if (args.event.target.classList.contains('icon-cloud--download')) {
          args.event.stopPropagation();    
          var crifFileUrl = args.item.crifFileUrl;
          window.treasury.comet.agreementWorkflowAction.downloadCrifFile(crifFileUrl);

        } else if (args.event.target.classList.contains('icon-transfer')) {
          //   this should display the popup where we can choose post cash or post collateral
          var agreementId = args.item.agreementId;
          if (agreementId < 0) {
            agreementId = -1 * agreementId;
          }
          _agreementWorkflowTopPanelGrid.commitCurrentEdit();
          args.event.stopPropagation();
          window.treasury.comet.agreementWiresAction.setPage('AgreementWorkflow');
          window.treasury.comet.agreementWiresAction.setTag(args.item.tag);
          window.treasury.comet.agreementWiresAction.setAccountType(args.item.accountType);
          window.treasury.comet.agreementWiresAction.setTripartyAgreementId(args.item.tripartyAgreementId);
          window.treasury.comet.agreementWiresAction.setACM(args.item.acmRC);
          window.treasury.comet.agreementWiresAction.setActionableAgreementId(agreementId);
          window.treasury.comet.agreementWiresAction.setActionableDate(date.value);
          window.treasury.comet.agreementWiresAction.setCustodianAccountId(args.item.custodianAccountId);
          window.treasury.comet.agreementWiresAction.setTripartyAgreementId(args.item.tripartyAgreementId);
          window.treasury.comet.agreementWiresAction.setReportingCurrencyIsoCode(args.item.reportingCurrency);
          window.treasury.comet.agreementWiresAction.setReportingCurrencySpn(args.item.reportingCurrencySpn);
          window.treasury.comet.agreementWiresAction.setReportingCurrencyFxRate(args.item.reportingCurrencyFxRate);
          window.treasury.comet.agreementWiresAction.setInternalVsExternalData(
            window.treasury.comet.agreementDetail.getInternalVsExternalData(args.item)
          );
          window.treasury.comet.agreementWiresAction.showPostCollateralPopup(workflowId.value);
        } else if (args.event.target.classList.contains('icon-mail')) {
          var agreementId = args.item.agreementId;
          if (agreementId < 0) {
            agreementId = -1 * agreementId;
          }
          args.event.stopPropagation();
          window.treasury.comet.agreementWorkflowAction.quickCallReport(workflowId.value, agreementId, actionableAgreementId.value);
        } else if (args.colId == 'agreementName') {
          treasury.comet.agreementworkflow.showLoading();
          args.event.stopPropogation;
          window.treasury.comet.agreementPositionGridAction.setSelectedAgreementId(args.item.agreementId);
          window.treasury.comet.agreementPositionGridAction.handleTabChange();

          window.treasury.comet.agreementWorkflowDetail.populateRightSideBar(args.item);

          window.treasury.comet.agreementWorkflowDetail.expandExcessDeficitSummaryPanel();

          treasury.comet.agreementworkflow.hideLoading();

          if (_selectedRow != undefined) {
            _selectedRow[0].classList.remove('highlight-row--selected');
          }

          _selectedRowId = args.item.id;
          _selectedRow = args.node;
          args.node[0].classList.add('highlight-row--selected');
          treasury.comet.agreementworkflow.hideLoading();
        } else if (args.item != null) {
          args.event.stopPropagation();
          treasury.comet.agreementworkflow.showLoading();
          window.treasury.comet.agreementWorkflowDetail.populateRightSideBar(args.item);

          window.treasury.comet.agreementWorkflowDetail.expandExcessDeficitSummaryPanel();

          treasury.comet.agreementworkflow.hideLoading();

          if (_selectedRow != undefined) {
            _selectedRow[0].classList.remove('highlight-row--selected');
          }

          _selectedRowId = args.item.id;
          _selectedRow = args.node;
          args.node[0].classList.add('highlight-row--selected');
          treasury.comet.agreementworkflow.hideLoading();
        }
      },
      // Setting logwire and call report actions
      onCellChange: function (agreementRow, rowObject$, isNotToggle) {
        // Updating dispute amount and actual cash movement for
        // agreements
        window.treasury.comet.agreementWorkflowDisputeAmount.updateDisputeAmountMap(
          agreementRow.item.agreementId,
          agreementRow.item.disputeAmountRC
        );
        window.treasury.comet.agreementWorkflowDisputeAmount.updateACMData(
          agreementRow.item.agreementId,
          agreementRow.item.acmRC
        );
        window.treasury.comet.agreementWorkflowDisputeAmount.updateASMData(
          agreementRow.item.agreementId,
          agreementRow.item.asmRC
        );

        // Initing actions column

        var workflowTopGrid = window.treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();
        var agreementWorkflowData = _agreementWorkflowTopPanelGrid.data;

        for (var i = 0; i < agreementWorkflowData.length; i++) {
          if (agreementWorkflowData[i].agreementId == agreementRow.item.agreementId) {
            agreementWorkflowData[i].disputeAmount = agreementRow.item.disputeAmount;
            agreementWorkflowData[i].disputeAmountRC = agreementRow.item.disputeAmountRC;
            agreementWorkflowData[i].acm = agreementRow.item.acm;
            agreementWorkflowData[i].acmRC = agreementRow.item.acmRC;
            agreementWorkflowData[i].asm = agreementRow.item.asm;
            agreementWorkflowData[i].asmRC = agreementRow.item.asmRC;
          }
        }

        _agreementWorkflowTopPanelGrid.refreshGrid([]);
      }
    };

    return options;
  }

  /**
   * Helper function that returns a promise to get Excess Deficit Workflow Data from Service/Action based on
   * given configuration
   */
  function getPromiseToFetchEDWorkflowDataList(actionableAgreementId, workflowId, date, includePositions, includeBundlePositions) {

    let promise;

    if (_useAgreementWorkflowService) {
      // Url to show data on begin workflow
      let wfDataUrl = '/treasury/service/agreementWorkflowService/getExcessDeficitWorkflowDataList?';
      let excessDeficitWorkflowFilter = {
        "@CLASS": "deshaw.treasury.common.model.lcm.ExcessDeficitWorkflowFilter",
        "actionableAgreementId": actionableAgreementId,
        "workflowId": workflowId,
        "includePositions": includePositions ? true : false,
        "includeBundlePositions": includeBundlePositions ? true : false
      };
      wfDataUrl += 'excessDeficitWorkflowFilter=' + encodeURIComponent(JSON.stringify(excessDeficitWorkflowFilter));
      wfDataUrl += '&inputFormat=JSON&format=JSON';
      promise = $.ajax({
        url: wfDataUrl,
        dataType: 'json',
        type: 'GET'
      });

    } else {
      // Url to show data on begin workflow
      let wfDataUrl = '/treasury/comet/show-agreement-workflow-data';

      let wfDataParam = 'configName=' + 'yakAgreementWorkflowConfig';
      wfDataParam += '&date=' + date;
      wfDataParam += '&actionableAgreementId=' + actionableAgreementId;
      wfDataParam += '&workflowId=' + workflowId;
      wfDataParam += '&tab=1';
      wfDataParam += '&includePositions=' + (includePositions ? 'true' : 'false');
      wfDataParam += '&includeBundlePositions=' + (includeBundlePositions ? 'true' : 'false');

      promise = $.ajax({
        url: wfDataUrl,
        data: wfDataParam,
        dataType: 'json',
        type: 'POST'
      });
    }
    return promise;
  }

  function showErrorDialog(errorMessage) {
    window.treasury.comet.agreementworkflow.hideLoading();
    let errorDialog = document.getElementById('errorMessage');
    let errorContent = '<center><strong>' + errorMessage + '</strong></center>';
    document.getElementById('publishId').disabled = true;
    document.getElementById('saveId').disabled = true;
    errorDialog.reveal({
      title: 'Error',
      content: errorContent,
      modal: true,
      buttons: [
        {
          html: 'OK',
          callback: function () {
            this.visible = false;
          },
          position: 'right'
        }
      ]
    });
    errorDialog.removeAttribute('hidden');
  }

  /**
   * Helper function for setting adjustment related functions
   */
  function _displayAdjustmentData(data) {
    event.stopPropagation();
    window.treasury.comet.agreementAdjustmentAction.setWorkflowId('+workflowId+');
    window.treasury.comet.agreementAdjustmentAction.setSourceGridConfig('Agreement');
    window.treasury.comet.agreementAdjustmentAction.setSelectedAgreementId(data.agreementId);
    window.treasury.comet.agreementAdjustmentAction.handleAdjustmentPopupDisplay(
      data.date,
      data.legalEntityId,
      data.exposureCounterPartyId,
      data.agreementTypeId,
      data.agreementType
    );
  }

  /**
   * Initing position detail link in agreement workflow
   */
  /*
    function initPositionDetailLink(agreementRow) {
        var positionDetailLink = document.getElementById('positionDetailLink');
        var positionDetailParam = "cpeIds=" + agreementRow.cpeIds;
        positionDetailParam += '&legalEntityIds='+agreementRow.legalEntityIds;
        positionDetailParam += '&agreementTypeIds='+agreementRow.agreementTypeIds;
        positionDetailParam += '&dateString='+agreementRow.date;
        positionDetailParam += '&includeLiveAgreements='+true;
        var displayPositionDetailAction = '/treasury/comet/display-position-detail';
        positionDetailLink.onclick = function(){event.preventDefault();_launchPositionDetails("Position Detail", displayPositionDetailAction, positionDetailParam)};
    }*/

  /**
   * Gets the position detail window.
   */
  function _launchPositionDetails(windowName, action, positionDetailParam) {
    var url = action + '?' + positionDetailParam;

    var childWindow = window.open(url, '_blank');

    if (childWindow == null || typeof childWindow == 'undefined') {
      alert('Please disable the pop-up block option in the browser settings.');
      return;
    }
  }

  /**
   * Helper function for setting log wire functions
   */
  function _displayLogWire(data) {
    event.stopPropagation();
    window.treasury.comet.agreementWiresAction.setPage('AgreementWorkflow');
    window.treasury.comet.agreementWiresAction.setTag(data.tag);
    window.treasury.comet.agreementWiresAction.setACM(data.acm);
    window.treasury.comet.agreementWiresAction.setActionableAgreementId(data.agreementId);
    treasury.comet.agreementWiresAction.getEDSummaryAndDisplayWirePopup(data.workflowId);
  }

  /**
   * Helper function for geenrating call report
   */
  function _displayCallReport(data) {
    event.stopPropagation();
    window.treasury.comet.agreementWorkflowAction.quickCallReport(data.workflowId);
  }

  /**
   * Helper function to set agreement name and related functions
   */
  function _displayAgreementName(data) {
    data.agreementName =
      '<a  href="#"   onclick=' +
      function () {
        _getAgreementName(data);
      } +
      ' >' +
      data.agreementName +
      '</a>';
    return data.agreementName;
  }

  /**
   * Helper function to get agreement name and handler for agreement name
   * click
   */
  function _getAgreementName(data) {
    window.treasury.comet.agreementPositionGridAction.setSelectedAgreementId(data.agreementId);
    window.treasury.comet.agreementPositionGridAction.handleTabChange();
  }

  /**
   * Helper to resize all the grids in agreement workflow screen
   */
  function _resizeAgreementWorkflowTopPanelGrid() {
    // Resizing agreement workflow top panel grid on state change of right
    // panel
    _agreementWorkflowTopPanelGrid.resizeCanvas();
    _agreementWorkflowTopPanelGrid.refreshGrid([]);

    if (window.treasury.comet.agreementWorkflowGridAction.getSelectedRowId() != undefined) {
      var rowId = _agreementWorkflowTopPanelGrid.dataView.getRowById(
        window.treasury.comet.agreementWorkflowGridAction.getSelectedRowId()
      );
      var rowNode = _agreementWorkflowTopPanelGrid.grid.rowsCache()[rowId].rowNode;
      _selectedRow = rowNode;
      rowNode[0].classList.add('highlight-row--selected');
    }
    // increasing grid height
    $('#' + 'agreementWorkflowTopPanelGrid').height($('#' + 'agreementWorkflowTopPanelGrid').height() + 11);
  }
})();

function parseEDWorkflowData(_useAgreementWorkflowService, data) {
  let excessDeficitWorkflowDataList;
  if (!_useAgreementWorkflowService && data.resultList) {
    excessDeficitWorkflowDataList = data.resultList;
  }
  else if (data && data.length) {
    excessDeficitWorkflowDataList = data.map(e => JSON.parse(e[1]));
  }
  return excessDeficitWorkflowDataList;
}

