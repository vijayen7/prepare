"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementWorkflowDetail = {
        populateRightSideBar : _populateRightSideBar,
        expandExcessDeficitSummaryPanel : _expandExcessDeficitSummaryPanel

    };

    function _expandExcessDeficitSummaryPanel() {
        var excessDeficitSummaryPanel = document.getElementById('agreementWorkflowRightBar');
        if (excessDeficitSummaryPanel != undefined && excessDeficitSummaryPanel.state == 'collapsed') {
            excessDeficitSummaryPanel.toggleState();
        }
    }

    function _populateRightSideBar(agreementWorkflowData) {
        _populateStatusData(agreementWorkflowData);
        window.treasury.comet.agreementDetail.populateIntVsExtData(agreementWorkflowData);
        window.treasury.comet.agreementDetail.populateIntT1VsT2(agreementWorkflowData);
        window.treasury.comet.agreementDetail.populateExtT1VsT2(agreementWorkflowData);
        window.treasury.comet.agreementDetail.populateIntVsIntAdjusted(agreementWorkflowData);
    }

    function _populateStatusData(agreementWorkflowData) {
        var agreementStatusMsg = document.getElementById("agreementStatusMsg");
        if (agreementWorkflowData.ragStatusMsg != undefined && agreementStatusMsg) {
            agreementStatusMsg.innerText = agreementWorkflowData.ragStatusMsg;
        } else {
            var agreementStatusMsgDiv = document.getElementById("agreementStatusMsgDiv");
            if (agreementStatusMsgDiv) {
                agreementStatusMsgDiv.innerHtml = "";
            }
        }
    }

})();
