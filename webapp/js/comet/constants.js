"use strict";

(function () {

        window.treasury = window.treasury || {};
        window.treasury.comet = window.treasury.comet || {};
        window.treasury.comet.crimsonTables = {
                INTERNAL_VS_MARGIN_FILE: 'internalVsMarginFile',
                TOP_POS_REC_VS_MARGIN_FILE: 'topPosRecVsMarginFile',
                BOTTOM_POS_REC_VS_MARGIN_FILE: 'bottomPosRecVsMarginFile',
                INTERNAL_VS_POS_REC: 'internalVsPosRec',
                INTERNAL_VS_REPO_REC: 'internalVsRepoRec',
                INTERNAL_VS_MARGIN_FILE_SEG: 'internalVsMarginFileSeg',
                DAY_ON_DAY_CHANGES: 'dayOnDayChanges',
                RECON_STATUS_SUMMARY: 'reconStatusSummary'
        };

        let crimsonTables = window.treasury.comet.crimsonTables;

        const EXPOSURE = 'Exposure';
        const REQUIREMENT = 'Requirement';
        const COLLATERAL = 'Collateral';
        const CASH_COLLATERAL = 'Cash Collateral';
        const SECURITIES_COLLATERAL = 'Securities Collateral';
        const EXCESS_DEFICIT = 'Excess (Deficit)';

        const SEG_REQUIREMENT = 'Seg Requirement';
        const SEG_COLLATERAL = 'Seg Collateral';
        const SEG_CASH_COLLATERAL = 'Seg Cash Collateral';
        const SEG_SECURITIES_COLLATERAL = 'Seg Sec Collateral';
        const SEG_ED = 'Seg ' + EXCESS_DEFICIT;

        const INTERNAL_VS_MARGIN_FILE_DATA = 'Internal Data vs Margin File Data';
        const POS_REC_VS_MARGIN_FILE_DATA = 'PosRec Data vs Margin File Data';
        const INTERNAL_VS_POS_REC_DATA = 'Internal Data vs PosRec Data';
        const INTERNAL_VS_REPO_REC_DATA = 'Internal Data vs RepoRec Data';
        const DAY_ON_DAY_CHANGES = 'Day on Day Changes';
        const RECONCILER_STATUS_SUMMARY = 'Reconciler Status Summary';
        const INTERNAL_VS_MARGIN_FILE_SEG = 'Segregated Internal Data vs Margin File Data';

        const EXPOSURE_DIFF_INTERNAL_VS_MARGIN_FILE = 'Exposure Diff (Internal Exposure - Margin File Exposure)'
        const REQUIREMENT_DIFF_INTERNAL_VS_MARGIN_FILE = 'Margin Calc Diff (Internal Calc - Margin File Calc)'
        const CASH_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE = 'Cash Collateral Diff (Internal Cash Collateral - Margin File Cash Collateral)'
        const SECURITIES_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE = 'Securities Collateral Diff (Internal Sec Collateral - Margin File Sec Collateral)'

        const EXPOSURE_DIFF_POS_REC_VS_MARGIN_FILE = 'External Exposure Diff (PosRec Exposure - Margin File Exposure)';
        const REQUIREMENT_DIFF_POS_REC_VS_MARGIN_FILE = 'Margin Calc Diff (PosRec Data/Internal Calc -  Margin File Data/Broker Calc)';

        const EXPOSURE_DIFF_INTERNAL_VS_POS_REC = 'Exposure Diff (Internal Exposure - PosRec Exposure)';
        const EXPOSURE_DIFF_INTERNAL_VS_REPO_REC = 'Exposure Diff (Internal Exposure - RepoRec Exposure)';
        const REQUIREMENT_DIFF_INTERNAL_VS_POS_REC = 'Margin Calc Diff (Internal Calc - PosRec Calc)'
        const REQUIREMENT_DIFF_INTERNAL_VS_REPO_REC = 'Margin Calc Diff (Internal Calc - RepoRec Calc)'

        const SEG_REQUIREMENT_DIFF_INTERNAL_VS_MARGIN_FILE = 'Margin Calc Diff (Segregated Internal Calc - Margin File Calc)'
        const SEG_CASH_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE = 'Cash Collateral Diff (Segregated Internal Cash Collateral - Margin File Cash Collateral)'
        const SEG_SECURITIES_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE = 'Securities Collateral Diff (Segregated Internal Sec Collateral - Margin File Sec Collateral)'

        const DIFF_ROW_NAMES = {
                [INTERNAL_VS_MARGIN_FILE_DATA]: {
                        [EXPOSURE]: EXPOSURE_DIFF_INTERNAL_VS_MARGIN_FILE,
                        [REQUIREMENT]: REQUIREMENT_DIFF_INTERNAL_VS_MARGIN_FILE,
                        [CASH_COLLATERAL]: CASH_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE,
                        [SECURITIES_COLLATERAL]: SECURITIES_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE
                },
                [POS_REC_VS_MARGIN_FILE_DATA]: {
                        [EXPOSURE]: EXPOSURE_DIFF_POS_REC_VS_MARGIN_FILE,
                        [REQUIREMENT]: REQUIREMENT_DIFF_POS_REC_VS_MARGIN_FILE
                },
                [INTERNAL_VS_POS_REC_DATA]: {
                        [EXPOSURE]: EXPOSURE_DIFF_INTERNAL_VS_POS_REC,
                        [REQUIREMENT]: REQUIREMENT_DIFF_INTERNAL_VS_POS_REC
                },
                [INTERNAL_VS_REPO_REC_DATA]: {
                        [EXPOSURE]: EXPOSURE_DIFF_INTERNAL_VS_REPO_REC,
                        [REQUIREMENT]: REQUIREMENT_DIFF_INTERNAL_VS_REPO_REC
                },
                [INTERNAL_VS_MARGIN_FILE_SEG]: {
                        [SEG_REQUIREMENT]: SEG_REQUIREMENT_DIFF_INTERNAL_VS_MARGIN_FILE,
                        [SEG_CASH_COLLATERAL]: SEG_CASH_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE,
                        [SEG_SECURITIES_COLLATERAL]: SEG_SECURITIES_COLLATERAL_DIFF_INTERNAL_VS_MARGIN_FILE
                },
        }

        window.treasury.comet.constants = {
                AGREEMENT_TYPE_ID: {
                        'PB': '2',
                        'FOC': '5',
                        'ISDA': '6',
                        'MRA': '7',
                        'GMRA': '8',
                        'REPO': '15',
                        'MNA': '20',
                        'FCM': '22',
                        'FOC-OTC': '26'
                },
                NA: 'n/a',
                SOURCE: 'Source',
                EXPOSURE: EXPOSURE,
                REQUIREMENT: REQUIREMENT,
                COLLATERAL: COLLATERAL,
                CASH_COLLATERAL: CASH_COLLATERAL,
                SECURITIES_COLLATERAL: SECURITIES_COLLATERAL,
                ED: EXCESS_DEFICIT,
                SEG_REQUIREMENT: SEG_REQUIREMENT,
                SEG_COLLATERAL: SEG_COLLATERAL,
                SEG_CASH_COLLATERAL: SEG_CASH_COLLATERAL,
                SEG_SECURITIES_COLLATERAL: SEG_SECURITIES_COLLATERAL,
                SEG_ED: SEG_ED,
                INTERNAL: 'Internal',
                MARGIN_FILE: 'Margin File',
                POS_REC: 'PosRec',
                REPO_REC: 'RepoRec',
                DIFF: 'Diff',
                PREV_DAY_ADJ: 'Prev Day Adj',
                LMV_DIFF: 'LMV Diff',
                SMV_DIFF: 'SMV Diff',
                NO_OF_RECORDS: 'No. of Records',
                COMPONENT: 'Component',
                BREAK: 'Break',
                MATCH: 'Match',
                INTERNAL_MISS: 'Internal Miss',
                INTERNAL_MISSING: 'Internal Missing',
                CPE_MISSING: 'CPE Missing',
                CPE_MISS: 'CPE Miss',
                TOTAL: 'Total',

                INTERNAL_VS_MARGIN_FILE_DATA: INTERNAL_VS_MARGIN_FILE_DATA,
                POS_REC_VS_MARGIN_FILE_DATA: POS_REC_VS_MARGIN_FILE_DATA,
                INTERNAL_VS_POS_REC_DATA: INTERNAL_VS_POS_REC_DATA,
                INTERNAL_VS_REPO_REC_DATA: INTERNAL_VS_REPO_REC_DATA,
                DAY_ON_DAY_CHANGES: DAY_ON_DAY_CHANGES,
                RECONCILER_STATUS_SUMMARY: RECONCILER_STATUS_SUMMARY,
                INTERNAL_VS_MARGIN_FILE_SEG: INTERNAL_VS_MARGIN_FILE_SEG,

                AGREEMENT_TYPE_TABLES: {
                        'PB': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.TOP_POS_REC_VS_MARGIN_FILE, crimsonTables.INTERNAL_VS_POS_REC,
                        crimsonTables.DAY_ON_DAY_CHANGES, crimsonTables.RECON_STATUS_SUMMARY],
                        'FOC': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.TOP_POS_REC_VS_MARGIN_FILE, crimsonTables.INTERNAL_VS_POS_REC,
                        crimsonTables.DAY_ON_DAY_CHANGES, crimsonTables.RECON_STATUS_SUMMARY],
                        'ISDA': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.INTERNAL_VS_MARGIN_FILE_SEG, crimsonTables.DAY_ON_DAY_CHANGES],
                        'MRA': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.INTERNAL_VS_REPO_REC, crimsonTables.DAY_ON_DAY_CHANGES],
                        'GMRA': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.INTERNAL_VS_REPO_REC, crimsonTables.DAY_ON_DAY_CHANGES],
                        'REPO': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.DAY_ON_DAY_CHANGES],
                        'MNA': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.INTERNAL_VS_MARGIN_FILE_SEG, crimsonTables.INTERNAL_VS_POS_REC,
                        crimsonTables.BOTTOM_POS_REC_VS_MARGIN_FILE, crimsonTables.DAY_ON_DAY_CHANGES, crimsonTables.RECON_STATUS_SUMMARY],
                        'FCM': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.TOP_POS_REC_VS_MARGIN_FILE, crimsonTables.INTERNAL_VS_POS_REC,
                        crimsonTables.DAY_ON_DAY_CHANGES, crimsonTables.RECON_STATUS_SUMMARY],
                        'FOC-OTC': [crimsonTables.INTERNAL_VS_MARGIN_FILE, crimsonTables.TOP_POS_REC_VS_MARGIN_FILE, crimsonTables.INTERNAL_VS_POS_REC,
                        crimsonTables.DAY_ON_DAY_CHANGES, crimsonTables.RECON_STATUS_SUMMARY]
                },
                DIFF_ROW_NAMES: DIFF_ROW_NAMES,
                AGREEMENT_TYPES_WITH_STATIC_CRIMSON_VIEW: ['PB', 'FOC', 'MNA'],
                DIFF_ROW_INDEX: 2,
                EXCESS_DEFICIT_COLUMN_INDEX: 5
        };

})();