"use strict";

(function () {

    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementSummaryFilterAction = {
        handleEntityFamilyChange: _handleEntityFamilyChange,
        loadAgreementSummaryFilters: _loadAgreementSummaryFilters,
        registerFilterChangeEvents: _registerFilterChangeEvents,
        getSelectedCounterParties: _getSelectedCounterParties,
        getSelectedLegalEntities: _getSelectedLegalEntities,
        getSelectedAgreementTypes: _getSelectedAgreementTypes,
        getSelectedEntityFamilies: _getSelectedEntityFamilies,
        getFilterPanelPreviousState: _getFilterPanelPreviousState,
        setFilterPanelPreviousState: _setFilterPanelPreviousState,
        getFilterPanelCurrentState: _getFilterPanelCurrentState,
        getCpeMultiSelect: _getCpeMultiSelect,
        getAgreementTypeMultiSelect: _getAgreementTypeMultiSelect,
        getLegalEntityFamilyMultiSelectFilter: _getLegalEntityFamilyMultiSelectFilter
    };

    // All filter references in the left panel
    var _entityFamilyMultiSelect;
    var _legalEntityFamilyMultiSelectFilter;
    var _cpeMultiSelect;
    var _agreementTypeMultiSelect;
    var _leftSideBarPreviousState;

    function _getCpeMultiSelect() {
        return _cpeMultiSelect;
    }

    function _getAgreementTypeMultiSelect() {
        return _agreementTypeMultiSelect;
    }

    function _getLegalEntityFamilyMultiSelectFilter() {
        return _legalEntityFamilyMultiSelectFilter;
    }

    function _getFilterPanelPreviousState() {
        return _leftSideBarPreviousState;
    }

    function _setFilterPanelPreviousState(filterPanelState) {
        _leftSideBarPreviousState = filterPanelState;
    }

    function _getFilterPanelCurrentState() {
        var filterPanelState = document.getElementById('rightSidebar').state;
        return filterPanelState;
    }

    // Entity Family To Entities Map and Legal Entity Data Map
    var entityFamilyToEntitiesMap = [];
    var legalEntityAllDataMap = [];

    function _getSelectedCounterParties() {
        return _cpeMultiSelect.getSerializedSelectedIds();
    }

    function _getSelectedLegalEntities() {
        if (_getSelectedEntityFamilies() != '-1' && _legalEntityFamilyMultiSelectFilter
            .getSerializedSelectedIds() == '-1') {
            var legalEntityFilterComponent = document
                .getElementById("legalEntityFilter");
            var allNodes = [];
            for (var i = 0; i < legalEntityFilterComponent.data.length; i++) {
                allNodes.push(legalEntityFilterComponent.data[i].key);
            }
            return allNodes.join(",");
        }
        return _legalEntityFamilyMultiSelectFilter
            .getSerializedSelectedIds();
    }

    function _getSelectedAgreementTypes() {
        return _agreementTypeMultiSelect
            .getSerializedSelectedIds();
    }

    function _getSelectedEntityFamilies() {
        return _entityFamilyMultiSelect.getSerializedSelectedIds();
    }

    function isJsonFilterDataValid(data) {
        if (data != null && data.length != 0 && data[0] != null) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * This function used to different filters on the left panel
     */
    function _loadAgreementSummaryFilters() {

        // Loading left panel filters
        treasury.comet.agreementsummary.showLoading();
        $.when(treasury.loadfilter.loadAgreementTypes(), treasury.loadfilter.loadEntityFamilies(), treasury.loadfilter.legalEntities(),
            treasury.loadfilter.cpes(),
            treasury.loadfilter.defaultDates())
            .done(function (agreementTypesData, legalEntityFamiliesData, legalEntitiesData, cpeData, defaultDatesData) {

                var filterList;
                //Initializing Legal Entity Family Filter
                if (isJsonFilterDataValid(legalEntityFamiliesData) &&
                    legalEntityFamiliesData[0].descoEntityFamilies != null) {
                    filterList = legalEntityFamiliesData[0].descoEntityFamilies;
                }
                else {
                    filterList = [];
                    alert("Failed to load Legal Entity Family Filter");
                }
                _entityFamilyMultiSelect = new window.treasury.filter.MultiSelectNew(
                    "entityFamilyFilter", "entityFamilyFilter",
                    filterList, []);

                // Initializing Legal Entity Filter
                if (isJsonFilterDataValid(legalEntitiesData) && legalEntitiesData[0].descoEntities) {
                    filterList = legalEntitiesData[0].descoEntities;
                }
                else {
                    filterList = [];
                    alert("Failed to load Legal Entity Filter");
                }
                _legalEntityFamilyMultiSelectFilter = new window.treasury.filter.MultiSelectNew(
                    "legalEntityFilter", "legalEntityFilter",
                    filterList, []);

                var legalEntityFamilyMultiSelect = document
                    .getElementById("legalEntityFilter");
                var legalEntityFamilyMultiSelectdata = legalEntityFamilyMultiSelect.data;

                for (var i = 0; i < legalEntityFamilyMultiSelectdata.length; i++) {
                    legalEntityAllDataMap[legalEntityFamilyMultiSelectdata[i].key] = legalEntityFamilyMultiSelectdata[i];
                }

                // Initializing CPE Family Filter
                if (isJsonFilterDataValid(cpeData) && cpeData[0].cpes) {
                    filterList = cpeData[0].cpes;
                }
                else {
                   filterList = [];
                    alert("Failed to load Cpe Filter");
                }
                _cpeMultiSelect = new window.treasury.filter.MultiSelectNew("cpeFilter",
                "cpeFilter", filterList, []);

                // Initializing Agreement Type Filter
                if (isJsonFilterDataValid(agreementTypesData) && agreementTypesData[0].agreementTypes) {
                    filterList = agreementTypesData[0].agreementTypes;
                }
                else {
                    filterList = [];
                    alert("Failed to load Agreement Type Filter");
                }
                _agreementTypeMultiSelect = new window.treasury.filter.MultiSelectNew(
                    "agreementTypeFilter", "agreementTypeFilter",
                    filterList, []);

                entityFamilyFilter.multiSelect = true
                legalEntityFilter.multiSelect = true
                cpeFilter.multiSelect = true
                agreementTypeFilter.multiSelect = true

                // Initializing Date Range filter
                /*var settings = {
                    options : {'defaultDate':date,
                        'buttonImage':"<i class='fa fa-calendar'></i>",
                        'buttonText' : "<i class='fa fa-calendar'></i>"}
                };*/
                // settings.icon = $("#calendarIconId");

                // Following is a hack as setupDaterangepicker is from dportal library
                // and expects dportal.app is defined.
                /*dportal = dportal || {};
                dportal.app = dportal.app || {};
        
                setupDatepicker({
                    input : $("#datepicker"),
                    errorDiv : $("#datePickerError"),
                    options : settings
                });*/

                // Initializing Date filter
                $.when(treasury.loadfilter.defaultDates())
                    .done(function (defaultDatesData) {
                        var dateFilterValue = defaultDatesData.tMinusOneFilterDate;

                        $("#datepicker").datepicker({
                            dateFormat: 'yy-mm-dd',
                            changeMonth: true,
                            changeYear: true,
                            minDate: Date.parse('1970-01-01'),
                            maxDate: Date.today(),
                            showOn: "both",
                            buttonText: "<i class='fa fa-calendar'></i>"
                        }).datepicker("setDate", dateFilterValue);
                    });
                var saveSettings = document.querySelector('arc-save-settings');
                saveSettings.retrieveSettings();
                window.treasury.comet.agreementsummary.loadData();





                // Putting Entity Family to Legal Entity data in map to process the Entity Family Filter changes
                _initEntityFamilyToEntities(legalEntityFamiliesData);
                _registerFilterChangeEvents();

            });




    }

    /**
     * This functions will be used to register callbacks for filters
     */
    function _registerFilterChangeEvents() {

        var entityFamilyFilterComponent = document
            .getElementById("entityFamilyFilter");

        entityFamilyFilterComponent.onchange = function () { _handleEntityFamilyChange(); };
    }

    /**
     * This functions Handles the Entity Family Filter change and refreshed the Legal Entity Filter accordingly
     */
    function _handleEntityFamilyChange() {

        var legalEntityFilterComponent = document
            .getElementById("legalEntityFilter");

        var selectedEntityFamilies = [];
        if (_entityFamilyMultiSelect) {
            selectedEntityFamilies = _entityFamilyMultiSelect.getSelectedIds();
        }

        var legalEntitiesToShow = [];
        var legalEntitiesToSelect = [];

        if (selectedEntityFamilies != "-1") {
            for (var j = 0; j < selectedEntityFamilies.length; j++) {
                if (selectedEntityFamilies[j] in entityFamilyToEntitiesMap) {
                    legalEntitiesToSelect = legalEntitiesToSelect
                        .concat(entityFamilyToEntitiesMap[selectedEntityFamilies[j]]);
                }
            }

            for (var i = 0; i < legalEntitiesToSelect.length; i++) {
                if (legalEntitiesToSelect[i] in legalEntityAllDataMap) {
                    legalEntitiesToShow
                        .push(legalEntityAllDataMap[legalEntitiesToSelect[i]]);
                }

            }
        } else {
            for (var j in legalEntityAllDataMap) {
                legalEntitiesToShow.push(legalEntityAllDataMap[j]);
            }
        }

        legalEntityFilterComponent.data = legalEntitiesToShow;
    }

    /**
     * Helper method to initialize entityFamilyToEntitiesMap map
     */
    function _initEntityFamilyToEntities(legalEntityFamiliesData) {

        if (legalEntityFamiliesData && legalEntityFamiliesData[0] && legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap
            && legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap.length) {
            var data = legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap
            for (var i = 0; i < data.length; i++) {

                var entityFamilyId = data[i].entityFamilyId;
                entityFamilyToEntitiesMap[entityFamilyId] = data[i].entities;

            }
        }
    }
})();
