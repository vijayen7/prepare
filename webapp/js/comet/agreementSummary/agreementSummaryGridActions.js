/*jshint jquery:true*/
'use strict';

(function () {
  window.treasury = window.treasury || {};
  window.treasury.comet = window.treasury.comet || {};
  window.treasury.comet.agreementSummaryGridActions = {
    loadAgreementSummaryGrid: _loadAgreementSummaryGrid,
    extractAgreementSummaryGridColumns: _extractAgreementSummaryGridColumns,
    getAgreementSummaryGridAction: _getAgreementSummaryGridAction,
    getSelectedRow: _getSelectedRow,
    getSearchParamsWithRagSyncId: _getSearchParamsWithRagSyncId,
    getSelectedRowId: _getSelectedRowId,
    setSelectedRowId: _setSelectedRowId,
    computeAndChangeGridHeight: _computeAndChangeGridHeight,
    showCrimsonDiffDrilldown: _showCrimsonDiffDrilldown
  };

  var _agreementSummaryGrid;
  var _selectedRow;
  var _selectedAgreementSummaryRow;
  var _selectedRowId;
  var _searchParamsWithRagSyncId = {};

  function _getAgreementSummaryGridAction() {
    return _agreementSummaryGrid;
  }

  function _getSearchParamsWithRagSyncId() {
    return _searchParamsWithRagSyncId;
  }

  function _getSelectedRow() {
    return _selectedRow;
  }

  function _getSelectedRowId() {
    return _selectedRowId;
  }

  function _setSelectedRowId(rowId) {
    _selectedRowId = rowId;
  }

  function _loadAgreementSummaryGrid(parameters) {
    var agreementSummaryView = document.getElementById('agreementSummaryView');

    agreementSummaryView.setAttribute('hidden', true);

    var url =
      '/treasury/comet/search-agreement-summary-data?' +
      'dateString=' +
      parameters.dateString +
      '&legalEntityIds=' +
      parameters.legalEntityIds +
      '&cpeFamilyIds=' +
      parameters.cpeFamilyIds +
      '&agreementTypeIds=' +
      parameters.agreementTypeIds +
      '&workflowStatusFilter=' +
      parameters.workflowStatusFilter +
      '&exceptionStatusFilter=' +
      parameters.exceptionStatusFilter +
      '&includeLiveAgreements=' +
      parameters.includeLiveAgreements +
      '&format=json&_outputKey=resultList';

    treasury.comet.agreementsummary.showLoading();

    var promise = $.ajax({
      url: url,
      type: 'POST'
    });

    promise.done(function (data) {
      treasury.comet.agreementsummary.hideLoading();
      //setting date filter value to the current search date
      //required in cases of Copy Search Urls where the date is set automatically to the current date on page load
      //TODO : Handle date filter setting in the same way as other filters
      var dateFilterValue;

      var date = parameters['dateString'];
      if (date != undefined && date.length != 0) {
        var year = date.substring(0, 4);
        var month = date.substring(5, 7);
        var day = date.substring(8, 10);

        dateFilterValue = new Date(year, month - 1, day);
      }

      if (dateFilterValue != undefined) {
        $('#datepicker')
          .datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            minDate: Date.parse('1970-01-01'),
            maxDate: Date.today(),
            showOn: 'both',
            buttonText: "<i class='fa fa-calendar'></i>"
          })
          .datepicker('setDate', dateFilterValue);
      }

      window.treasury.comet.agreementsummary.setFilters(parameters);

      var columns = window.treasury.comet.agreementSummaryColumnConfig.getAgreementSummaryColumns();
      var options = _getAgreementSummaryGridOptions();

      if (data && data.resultList && data.resultList.length) {
        let maxRagSyncId = 0;
        for (var i = 0; i < data.resultList.length; i++) {
          let ragSyncId = data.resultList[i].treasuryAgreementRAGSyncId;
          if (ragSyncId > maxRagSyncId) {
            maxRagSyncId = ragSyncId;
          }
        }
        _searchParamsWithRagSyncId = {
          date: date,
          ragSyncId: maxRagSyncId,
          legalEntityIds: parameters.legalEntityIds,
          cpeIds: parameters.cpeFamilyIds,
          agreementTypeIds: parameters.agreementTypeIds,
          includeInProgress:
            parameters.workflowStatusFilter == '' || parameters.workflowStatusFilter.indexOf('IN_PROGRESS') != -1,
          includePublished:
            parameters.workflowStatusFilter == '' || parameters.workflowStatusFilter.indexOf('PUBLISHED') != -1,
          includeNotStarted:
            parameters.workflowStatusFilter == '' || parameters.workflowStatusFilter.indexOf('NOT_STARTED') != -1
        };

        for (var i = 0; i < data.resultList.length; i++) {
          // Initing the quick links at the top of workflow grid
          _initQuickLinks(data.resultList[i]);
        }

        var searchDate = document.getElementById('searchDate');
        searchDate.innerHTML = parameters.dateString;

        var noOfRecords = document.getElementById('noOfRecords');
        noOfRecords.innerHTML = data.resultList.length;

        var agreementSummaryView = document.getElementById('agreementSummaryView');
        if (!agreementSummaryView.hasAttribute('hidden')) {
          agreementSummaryView.setAttribute('hidden', true);
        }

        var agreementDetailDiv = document.getElementById('agreementDetailDiv');
        agreementDetailDiv.hidden = true;

        var statusLegend = document.getElementById('statusLegend');
        if (statusLegend.hasAttribute('hidden')) {
          statusLegend.removeAttribute('hidden');
        }

        var agreementSummaryGridDiv = document.getElementById('agreementSummaryGridDiv');
        if (agreementSummaryGridDiv.hasAttribute('hidden')) {
          agreementSummaryGridDiv.removeAttribute('hidden');
        }

        // Initiating the Grid
        options.configureReport = {
          urlCallback: function () {
            return window.location.origin + url;
          }
        };
        _agreementSummaryGrid = new dportal.grid.createGrid(
          $('#agreementSummaryGrid'),
          data.resultList,
          columns,
          options
        );

        _agreementSummaryGrid.getFilterPlugin().showFilterPanel();

        setTimeout(function () {
          _resizeAgreementSummaryGrid();
          _computeAndChangeGridHeight();
        }, 0);

        // Apply Red, Amber and Green based on CRITICAL, WARNING and CLEAR
        _formatApprovalRuleGrid();
        //_agreementSummaryGrid.refreshGrid([]);

        var quickLinks = document.getElementById('quickLinks');
        quickLinks.style.display = 'block';
      } else {
        // When no data found
        var quickLinks = document.getElementById('quickLinks');
        quickLinks.style.display = 'none';

        if (_agreementSummaryGrid != undefined) {
          _agreementSummaryGrid.data = null;
        }

        var agreementSummaryView = document.getElementById('agreementSummaryView');
        agreementSummaryView.innerHTML = 'No Data Found';
        var header = document.getElementById('agreementSummaryGrid-header');
        if (header != null) {
          header.remove();
        }
        if (agreementSummaryView.hasAttribute('hidden')) {
          agreementSummaryView.removeAttribute('hidden');
        }
        var agreementSummaryGridDiv = document.getElementById('agreementSummaryGridDiv');
        if (!agreementSummaryGridDiv.hasAttribute('hidden')) {
          agreementSummaryGridDiv.setAttribute('hidden', true);
        }

        var statusLegendDiv = document.getElementById('statusLegend');
        if (!statusLegendDiv.hasAttribute('hidden')) {
          statusLegendDiv.setAttribute('hidden', true);
        }
      }

      var leftSidebar = document.getElementById('agreementFilterSidebar');
      let searchFilterResizer = document.getElementById('searchFilterResizer');
      if (leftSidebar.state == 'expanded') {
        leftSidebar.toggle();
        searchFilterResizer.remove();
      }
    });

    promise.fail(function (xhr, text, errorThrown) {
      treasury.comet.agreementsummary.hideLoading();

      // When no data found
      var quickLinks = document.getElementById('quickLinks');
      quickLinks.style.display = 'none';

      if (_agreementSummaryGrid != undefined) {
        _agreementSummaryGrid.data = null;
      }

      var agreementSummaryView = document.getElementById('agreementSummaryView');
      agreementSummaryView.innerHTML = 'No Data Found';
      var header = document.getElementById('agreementSummaryGrid-header');
      if (header != null) {
        header.remove();
      }
      if (agreementSummaryView.hasAttribute('hidden')) {
        agreementSummaryView.removeAttribute('hidden');
      }
      var agreementSummaryGridDiv = document.getElementById('agreementSummaryGridDiv');
      if (!agreementSummaryGridDiv.hasAttribute('hidden')) {
        agreementSummaryGridDiv.setAttribute('hidden', true);
      }

      var statusLegendDiv = document.getElementById('statusLegend');
      if (!statusLegendDiv.hasAttribute('hidden')) {
        statusLegendDiv.setAttribute('hidden', true);
      }

      console.log('Error occurred while retrieving Agreement Summary');
    });
  }

  /**
   * Helper method to resize the grid on right panel collapse or expand
   */
  function _resizeAgreementSummaryGrid() {
    // Adding call backs to left panel to listen and resize the grids
    var leftPanel = document.getElementById('agreementFilterSidebar');
    leftPanel.addEventListener('stateChange', function () {
      setTimeout(function () {
        var filterPanelPreviousState = treasury.comet.agreementSummaryFilterAction.getFilterPanelPreviousState();
        var agreementFilterSidebar = document.getElementById('agreementFilterSidebar');
        var filterPanelCurrentState = agreementFilterSidebar.state;
        if (filterPanelPreviousState === undefined || filterPanelPreviousState != filterPanelCurrentState) {
          treasury.comet.agreementSummaryFilterAction.setFilterPanelPreviousState(filterPanelCurrentState);

          //                        if(filterPanelCurrentState == "expanded") {
          //                            if(agreementFilterSidebar.classList.contains("size--content")) {
          //                                agreementFilterSidebar.classList.remove('size--content')
          //                            }
          //                        } else {
          //                            if(!agreementFilterSidebar.classList.contains("size--content")) {
          //                                agreementFilterSidebar.classList.add('size--content')
          //                            }
          //                        }

          // Apply Red, Amber and Green based on CRITICAL, WARNING and CLEAR
          //_formatApprovalRuleGrid();

          if (filterPanelCurrentState == "expanded") {
            agreementFilterSidebar.insertAdjacentHTML('afterend', '<arc-resizer id="searchFilterResizer"/>');
            let searchFilterResizer = document.getElementById('searchFilterResizer');
            if (searchFilterResizer) {
              searchFilterResizer.addEventListener('resize', function () {
                setTimeout(function () {
                  _agreementSummaryGrid.resizeCanvas();
                }, 0)
              });
            }
          } else if (filterPanelCurrentState == 'collapsed') {
            let resizer = document.getElementById('searchFilterResizer');
            resizer.remove();
          }

          _agreementSummaryGrid.resizeCanvas();

          if (window.treasury.comet.agreementSummaryGridActions.getSelectedRowId() != undefined) {
            var rowId = _agreementSummaryGrid.dataView.getRowById(
              window.treasury.comet.agreementSummaryGridActions.getSelectedRowId()
            );
            var rowNode = _agreementSummaryGrid.grid.rowsCache()[rowId].rowNode;
            _selectedRow = rowNode;
            rowNode[0].classList.add('highlight-row--selected');
          }
        }
      }, 0);
    });

    // Adding call backs to right panel to listen and resize the grid
    /*var rightPanel= document.getElementById('rightSidebar');
        rightPanel.addEventListener('stateChange', function () {
            setTimeout(function(){
                var rightSideBarPreviousState = treasury.comet.agreementDetail.getAgreementDetailPreviousState();
                var rightSideBarCurrentState = document.getElementById('rightSidebar').state;
                if(rightSideBarPreviousState === undefined || rightSideBarPreviousState != rightSideBarCurrentState) {
                    treasury.comet.agreementDetail.setAgreementDetailPreviousState(rightSideBarCurrentState);
                    var parentArcLayout = document.getElementById('parentArcLayout');
                    parentArcLayout.setLayout();
                    /*
                    if(agreementSummaryData!= null && agreementSummaryData.length) {
                        // Initiating the Grid
                           _agreementSummaryGrid = new dportal.grid.createGrid(
                                   $("#agreementSummaryGrid"), agreementSummaryData,
                                   columns, options);
                       }*/

    // Apply Red, Amber and Green based on CRITICAL, WARNING and CLEAR
    //_formatApprovalRuleGrid();
    /*_agreementSummaryGrid.refreshGrid([]);

        if(window.treasury.comet.agreementSummaryGridActions.getSelectedRowId() != undefined) {
            var rowId = _agreementSummaryGrid.dataView.getRowById(window.treasury.comet.agreementSummaryGridActions.getSelectedRowId());
            var rowNode = _agreementSummaryGrid.grid.rowsCache()[rowId].rowNode;
            _selectedRow = rowNode;
            rowNode[0].classList.add("highlight-row--selected");
            /*
            window.treasury.comet.agreementSummaryGridActions.getSelectedRow()[0].classList.add("highlight-row--critical");
            window.treasury.comet.agreementSummaryGridActions.getSelectedRow()[0].classList.remove("highlight-onmove");*/
    /* }
     }
},0);
});*/
  }

  /**
   * Method to init the quick links at top
   */
  function _initQuickLinks(agreementSummaryRow) {
    var param = _generateParam(agreementSummaryRow);
    var agreementId = agreementSummaryRow.agreementId;

    // Quick links on grid top
    var quickPublishButton = document.getElementById('quickPublishId');

    // All quick links disabled at loading
    quickPublishButton.disabled = true;

    quickPublishButton.onclick = agreementSummaryRow.quickPublish;

    if (
      agreementSummaryRow.workflowStatus == 'IN_PROGRESS' ||
      agreementSummaryRow.workflowStatus == 'SAVED' ||
      agreementSummaryRow.workflowStatus == 'PUBLISHED' ||
      (agreementSummaryRow.ragStatusCode != 'CLEAR' && agreementSummaryRow.ragStatusCode != 'CLEAR/CLEAR')
    ) {
      quickPublishButton.disabled = true;
    } else {
      quickPublishButton.disabled = false;
    }

    // Enabling the quick link display
    var quickLinks = document.getElementById('quickLinks');
    quickLinks.removeAttribute('hidden');
    //quickLinks.setAttribute("size", "1");
  }

  /**
   * Helper methods for assigning begin workflow, quick publish, log wire and publish, call report
   */
  function _beginWorkflow(param) {
    if (!window.CustomShadowDOMPolyfill) {
      event.stopPropagation();
    }
    window.treasury.comet.agreementWorkflowAction.displayAgreementWorkflow(param);
  }

  function _quickPublish(param) {
    if (!window.CustomShadowDOMPolyfill) {
      event.stopPropagation();
    }
    window.treasury.comet.agreementWorkflowAction.quickPublishAgreementWorkflow(param);
  }

  function _logWireAndPublish(param, agreementId) {
    if (!window.CustomShadowDOMPolyfill) {
      event.stopPropagation();
    }
    window.treasury.comet.agreementWiresAction.setPage('AgreementSummary');
    window.treasury.comet.agreementWiresAction.setAgreementWorkflowParam(param);
    window.treasury.comet.agreementWiresAction.setActionableAgreementId(agreementId);
    window.treasury.comet.agreementWiresAction.quickLogWire(param);
  }

  function _callReport(param) {
    if (!window.CustomShadowDOMPolyfill) {
      event.stopPropagation();
    }
    window.treasury.comet.agreementWorkflowAction.quickGenerateCallReport(param);
  }

  /**
   * Generate params for begin workflow, quick publish, log wire and publish, call report
   */
  function _generateParam(agreementSummaryObj) {
    var date = agreementSummaryObj.date;
    var legalEntityId = agreementSummaryObj.legalEntityId;
    var expCounterPartyId = agreementSummaryObj.exposureCounterPartyId;
    var agreementTypeId = agreementSummaryObj.agreementTypeId;
    var agreementId = agreementSummaryObj.agreementId;
    var param = date + '_' + legalEntityId + '_' + expCounterPartyId + '_' + agreementTypeId + '_' + agreementId;

    return param;
  }

  /**
   * Method to mark the agreement summary rows red, green and amber
   */
  function _formatApprovalRuleGrid() {
    _agreementSummaryGrid.dataView.oldItemMetadata = _agreementSummaryGrid.dataView.getItemMetadata;
    _agreementSummaryGrid.dataView.getItemMetadata = metaData();

    /*_agreementSummaryGrid.dataView.getItemMetadata = function(row) {
            var dataItem = this.getItem(row);

            var rowProperties = _agreementSummaryGrid.dataView.oldItemMetadata(row) || {};

            // Based on RAG status assigning different class to grid rows
            if(dataItem.workflowStatus != 'PUBLISHED' && dataItem.workflowStatus != 'IN_PROGRESS' && dataItem.workflowStatus != 'SAVED') {
                if (dataItem.ragCategory == 'CRITICAL') {
                    rowProperties["cssClasses"] = "highlight-row--critical";
                } else if (dataItem.ragCategory == 'WARNING') {
                    rowProperties["cssClasses"] = "highlight-row--warning";
                } else if (dataItem.ragCategory == 'CLEAR') {
                    rowProperties["cssClasses"] = "highlight-row--clear";
                } else if (dataItem.ragCategory == 'BROKER_DATA_MISSING') {
                    rowProperties["cssClasses"] = "highlight-row--missing";
                }
            }

            return rowProperties;
        };*/
  }

  function metaData() {
    return function (row) {
      var dataItem = _agreementSummaryGrid.dataView.getItem(row);
      var meta = _agreementSummaryGrid.dataView.oldItemMetadata(row) || {};

      // Based on RAG status assigning different class to grid rows
      if (
        dataItem.workflowStatus != undefined &&
        dataItem.workflowStatus != 'IN_PROGRESS' &&
        dataItem.workflowStatus != 'SAVED'
      ) {
        if (dataItem.workflowStatus == 'PUBLISHED') {
        }
        /*
                else if (dataItem.ragCategory == 'CRITICAL') {
                    meta["cssClasses"] = "highlight-row--critical";
                } else if (dataItem.ragCategory == 'WARNING') {
                    meta["cssClasses"] = "highlight-row--warning";
                } else if (dataItem.ragCategory == 'CLEAR') {
                    meta["cssClasses"] = "highlight-row--clear";
                } else if (dataItem.ragCategory == 'BROKER_DATA_MISSING') {
                    meta["cssClasses"] = "highlight-row--missing";
                }*/
      }

      return meta;
    };
  }

  function _getAgreementSummaryGridOptions() {
    var host = '';
    var clientName = document.getElementById('clientName').value;
    var stabilityLevel = document.getElementById('stabilityLevel').value;

    if (clientName === 'desco') {
      if (stabilityLevel == 'prod') {
        host += 'http://landing-app.deshaw.c.ia55.net';
      } else if (stabilityLevel == 'uat') {
        host = 'http://landing-app.deshawuat.c.ia55.net';
      } else if (stabilityLevel == 'qa') {
        host = 'https://mars.arcesium.com';
      } else if (stabilityLevel == 'dev') {
        host = 'https://terra.arcesium.com';
      }
    }

    var options = {
      // maxHeight: 600,
      editable: true,
      enableCellNavigation: true,
      exportToExcel: true,
      highlightRowOnClick: true,
      //autoHorizontalScrollBar : true,
      //useAvailableScreenSpace : true,
      displaySummaryRow: true,
      asyncEditorLoading: false,
      autoEdit: true,
      configureColumns: true,
      frozenColumn: 2,
      checkboxHeader: true,
      applyFilteringOnGrid: {
        showFilterPanel: true
      },
      dynamicSummaryRow: true,
      enableMultilevelGrouping: {
        groupingControls: true,
        showGroupingTotal: true,
        customGroupingRendering: true,
        isAddGroupingColumn: true,
        groupingColumnPosition: 2,
        initialGrouping: ['agreementType']
      },
      cellRangeSelection: {
        showAggregations: true,
        multipleRangesEnabled: true
      },
      copyCellSelection: true,
      saveSettings: {
        applicationId: 3,
        applicationCategory: 'cometAgreementSummaryGrid',
        serviceURL: host + '/service/SettingsService'
      },
      customColumnSelection: {
        //totalcolumns:window.treasury.comet.agreementSummaryColumnConfig.getTotalColumns(),
        defaultcolumns: window.treasury.comet.agreementSummaryColumnConfig.getDefaultColumns()
      },
      //summaryRowText : "Total",
      //enableMultilevelGrouping: {isAddGroupingColumn: false},

      onCellClick: function (args, isNotToggle) {
        if (args.event.target.classList.contains('icon-edit--block')) {
          var param = _generateParam(args.item);
          args.event.stopPropagation();
          window.treasury.comet.agreementWorkflowAction.displayAgreementWorkflow(param);
        } else if (args.event.target.classList.contains('icon-quick')) {
          var param = _generateParam(args.item);
          args.event.stopPropagation();
          window.treasury.comet.agreementWorkflowAction.quickPublishAgreementWorkflow(param);
        } else if (args.event.target.classList.contains('icon-transfer')) {
          var param = _generateParam(args.item);
          args.event.stopPropagation();
          window.treasury.comet.agreementWiresAction.setPage('AgreementSummary');
          window.treasury.comet.agreementWiresAction.setTag(args.item.tag);
          window.treasury.comet.agreementWiresAction.setAccountType(args.item.accountType);
          window.treasury.comet.agreementWiresAction.setTripartyAgreementId(args.item.tripartyAgreementId);
          var acmvalue = window.treasury.comet.agreementWiresAction.roundValue(
            Math.round(args.item.ecmRC.replace(/,/g, '').replace(/x/g, '')),
            100000
          );
          window.treasury.comet.agreementWiresAction.setACM(acmvalue);
          window.treasury.comet.agreementWiresAction.setCustodianAccountId(args.item.custodianAccountId);
          window.treasury.comet.agreementWiresAction.setAgreementWorkflowParam(param);
          window.treasury.comet.agreementWiresAction.setActionableAgreementId(args.item.agreementId);
          window.treasury.comet.agreementWiresAction.setActionableDate(args.item.date);
          window.treasury.comet.agreementWiresAction.setReportingCurrencyIsoCode(args.item.reportingCurrency);
          window.treasury.comet.agreementWiresAction.setReportingCurrencySpn(args.item.reportingCurrencySpn);
          window.treasury.comet.agreementWiresAction.setReportingCurrencyFxRate(args.item.reportingCurrencyFxRate);
          window.treasury.comet.agreementWiresAction.setInternalVsExternalData(
            window.treasury.comet.agreementDetail.getInternalVsExternalData(args.item)
          );
          window.treasury.comet.agreementWiresAction.quickLogWire(param);
        } else if (args.event.target.classList.contains('icon-mail')) {
          var param = _generateParam(args.item);
          args.event.stopPropagation();
          window.treasury.comet.agreementWorkflowAction.quickGenerateCallReport(param);
        } else if (args.colId == 'runBrokerAndRagSync') {
          args.event.stopPropagation();
          window.treasury.comet.agreementDetail.executeBrokerAndRagSync(args.item.date, args.item.agreementId);
        } else if (args.item.__group == undefined) {
          var agreementDetailTemplate = document.querySelector('#agreementDetailTemplate');

          var cloneAgreementDetailTemplate = document.importNode(agreementDetailTemplate.content, true);
          var agreementDetailDiv = document.querySelector('#agreementDetailDiv');

          agreementDetailDiv.innerHTML = '';

          agreementDetailDiv.appendChild(cloneAgreementDetailTemplate);
          agreementDetailDiv.hidden = false;

          agreementDetail.addEventListener('panelDismiss', function () {
            var agreementDetailDiv = document.getElementById('agreementDetailDiv');
            agreementDetailDiv.hidden = true;
            _computeAndChangeGridHeight();
          });

          treasury.comet.agreementsummary.showLoading();
          // Initing the position detail links in right sidebar
          initPositionDetailLink(args.item);

          if (treasury.defaults.isCrimsonEnabled) {
            document.getElementById('crimsonPositionDetailLink').style.display = 'inline';
            initCrimsonPositionDetailLink(args.item);
            let isInternalDataMissing = args.item.isInternalDataMissing;
            if(!isInternalDataMissing) {
              document.getElementById('viewAdjustmentsLink').style.display = 'inline';
              initCrimsonViewAdjustmentsLink(args.item);
            }
          }

          // Toggling right panel
          $.when(
            window.treasury.comet.agreementDetail.toggleLeftPanel(),
            window.treasury.comet.agreementDetail.initQuickLinksInRightPanel(args.item),
            window.treasury.comet.agreementDetail.populateRightSideBar(args.item)
          ).done(function () {
            treasury.comet.agreementsummary.hideLoading();
          });

          setTimeout(function () {
            _computeAndChangeGridHeight();
          }, 100);
          _selectedRowId = args.item.id;
          _selectedRow = args.node;
          _selectedAgreementSummaryRow = args.item;
        } else {
          var agreementDetailDiv = document.getElementById('agreementDetailDiv');
          if (agreementDetailDiv) {
            agreementDetailDiv.hidden = true;
          }
          _computeAndChangeGridHeight();
        }
      },

      // Handle for row check-box selection
      onSelectedRowsChanged: function (agreementSummary, rowObject$, isNotToggle) {
        // When single check box is selected
        if (rowObject$.length == 1) {
          _initQuickLinks(rowObject$[0]);

          // When multiple check boxes are selected
        } else if (rowObject$.length > 1) {
          var quickPublishButton = document.getElementById('quickPublishId');

          var disableQuicknCallReportPublish = false;
          for (var i = 0; i < rowObject$.length; i++) {
              if(!treasury.defaults.isRagEnabled) {
                   break;
              }
            if (
              (rowObject$[i].workflowStatus != undefined && rowObject$[i].workflowStatus != 'NOT_STARTED') ||
              (rowObject$[i].ragStatusCode != 'CLEAR' && rowObject$[i].ragStatusCode != 'CLEAR/CLEAR')
            ) {
              disableQuicknCallReportPublish = true;
            }
          }

          quickPublishButton.disabled = disableQuicknCallReportPublish;

          if (!disableQuicknCallReportPublish) {
            var selectedRowParams;

            for (var i = 0; i < rowObject$.length; i++) {
              var param = _getParam(rowObject$[i]);
              // Preparing the quickpublish arguments
              if (selectedRowParams === undefined) {
                selectedRowParams = param;
              } else {
                selectedRowParams += ':' + param;
              }
            }

            quickPublishButton.onclick = function () {
              window.treasury.comet.agreementWorkflowAction.quickPublishAgreementWorkflow(selectedRowParams);
            };
          }
          // On no selection
        } else {
          // Disabling quick links
          _disableQuickLinks();
        }
      },
      sortList: [
        {
          columnId: 'agreementType',
          sortAsc: true
        },
        {
          columnId: 'workflowStatusIcon',
          sortAsc: true
        },
        {
          columnId: 'ragCategory',
          sortAsc: true
        },
        {
          columnId: 'ragStatusDisplayText',
          sortAsc: true
        }
      ],
      applyFilteringOnGrid: true
    };
    return options;
  }

  function _computeAndChangeGridHeight() {
    var agreementSummaryGridDiv = document.getElementById('agreementSummaryGridDiv');
    var agreementSummaryGridHeader = document.getElementById('agreementSummaryGrid-header');
    var agreementSummaryGridContainer = document.getElementById('agreementSummaryGrid--save-settings-container');
    var computedHeight =
      agreementSummaryGridDiv.offsetHeight -
      agreementSummaryGridHeader.offsetHeight -
      agreementSummaryGridContainer.offsetHeight -
      5 +
      'px';

    if (_agreementSummaryGrid) {
      var agreementSummaryGrid = document.getElementById('agreementSummaryGrid');
      agreementSummaryGrid.style.height = computedHeight;
      _agreementSummaryGrid.options.maxHeight = computedHeight;
      _agreementSummaryGrid.resizeCanvas();
    }
  }

  /**
   * Initing position detail link in agreement summary
   */
  function initPositionDetailLink(agreementSummaryRow) {
    var agreementPositionDetailLink = document.getElementById('agreementPositionDetailLink');
   

    let url = "/treasury/service/lcmFeatureToggle/isRedirectToNewPdrScreen?format=json";
    isRedirectToNewPdrScreen(url)
    .then((resp) => {
      let positionDetailParam = _createPositionDetailParam(agreementSummaryRow, resp);
      positionDetailParam += '&isCrimsonEnabled=' + false;
      let displayPositionDetailAction;

      if (!resp) {
        displayPositionDetailAction = '/treasury/comet/display-position-detail';
      } else {
        displayPositionDetailAction = '/treasury/comet/positiondetailreport.html';
      }
      agreementPositionDetailLink.onclick = function () {
        if (!window.CustomShadowDOMPolyfill) {
          event.preventDefault();
        }
        _launchWindow('Position Detail', displayPositionDetailAction, positionDetailParam);
      };
    })
  };

  function initCrimsonPositionDetailLink(agreementSummaryRow) {
    let crimsonPositionDetailLink = document.getElementById('crimsonPositionDetailLink');

    let url = "/treasury/service/lcmFeatureToggle/isRedirectToNewPdrScreen?format=json";
   isRedirectToNewPdrScreen(url)
   .then((resp) => {
    let positionDetailParam = _createPositionDetailParam(agreementSummaryRow, resp);
    positionDetailParam += '&isCrimsonEnabled=' + true;
    let displayPositionDetailAction;

    if (!resp) {
      displayPositionDetailAction = '/treasury/comet/display-position-detail';
    } else {
      displayPositionDetailAction = '/treasury/comet/positiondetailreport.html';
    }
    crimsonPositionDetailLink.onclick = function () {
      if (!window.CustomShadowDOMPolyfill) {
        event.preventDefault();
      }
      _launchWindow('Position Detail', displayPositionDetailAction, positionDetailParam);
    };
   });
  }

  function initCrimsonViewAdjustmentsLink(agreementSummaryRow) {
    let viewAdjustmentsLink = document.getElementById('viewAdjustmentsLink');
    let viewAdjustmentsParam = _createViewAdjustmentsParam(agreementSummaryRow);
    let displayViewAdjustments = '/treasury/lcm/Adjustments.html';
    viewAdjustmentsLink.onclick = function () {
      if (!window.CustomShadowDOMPolyfill) {
        event.preventDefault();
      }
      _launchWindow('View Adjustments', displayViewAdjustments, viewAdjustmentsParam);
    };
  }

  function _showCrimsonDiffDrilldown(agreementSummaryRow, fromExcessDeficitDataSource, toExcessDeficitDataSource,
    excessDeficitDataType, showSegDrilldown) {
    let displayPositionDetailAction = '/treasury/comet/display-position-detail';
    let crimsonDiffDrilldownParam = _createCrimsonDiffDrilldownParam(agreementSummaryRow,
      fromExcessDeficitDataSource, toExcessDeficitDataSource, excessDeficitDataType, showSegDrilldown);
    if (!window.CustomShadowDOMPolyfill) {
      event.preventDefault();
    }
    _launchWindow('Position Detail', displayPositionDetailAction, crimsonDiffDrilldownParam);
  }

  function _createPositionDetailParam(agreementSummaryRow, isRedirectToNewPdrScreenFlag) {
    let positionDetailParam = 'cpeIds=' + agreementSummaryRow.cpeIds;
    positionDetailParam += '&legalEntityIds=' + agreementSummaryRow.legalEntityIds;
    positionDetailParam += '&agreementTypeIds=' + agreementSummaryRow.agreementTypeIds;

    let isPublished = agreementSummaryRow.workflowStatus === "PUBLISHED"
    let dateFilterValue;
    let date = agreementSummaryRow.date;
    if (date != undefined && date.length != 0) {
      let year = date.substring(0, 4);
      let month = date.substring(4, 6);
      let day = date.substring(6, 8);
      dateFilterValue = year + '-' + month + '-' + day;
    }

    if (isRedirectToNewPdrScreenFlag) {
      positionDetailParam += '&startDate=' + date;
      positionDetailParam += '&endDate=' + date;
      positionDetailParam += '&includeUnmappedBrokerRecord=' + true;
      positionDetailParam += '&bookIds=' + -1;
      positionDetailParam += '&gboTypeIds=' + -1;
      positionDetailParam += '&dataSet=' + 3;
      positionDetailParam += '&isPublished=' + isPublished
    } else {
      positionDetailParam += '&dateString=' + dateFilterValue;
      positionDetailParam += '&includeLiveAgreements=' + true;
      positionDetailParam += '&loadPositionDetail=' + true;
      positionDetailParam += '&entityFamilyIds=' + -1;
      positionDetailParam += '&includeUnmappedBrokerRecords=' + true;
      positionDetailParam += '&bookIds=';
      positionDetailParam += '&gboTypeIds=';
    }
    
  
    positionDetailParam += '&isCopyUrl=' + true;
    return positionDetailParam;
  }

  function _createViewAdjustmentsParam(agreementSummaryRow) {
    let viewAdjustmentsParam = 'cpeIds=' + agreementSummaryRow.cpeIds;
    viewAdjustmentsParam += '&tradingEntityIds=' + agreementSummaryRow.legalEntityIds;
    viewAdjustmentsParam += '&agreementTypeIds=' + agreementSummaryRow.agreementTypeIds;
    let dateFilterValue;
    let date = agreementSummaryRow.date;
    if (date != undefined && date.length != 0) {
      let year = date.substring(0, 4);
      let month = date.substring(4, 6);
      let day = date.substring(6, 8);
      dateFilterValue = year + '-' + month + '-' + day;
    }
    viewAdjustmentsParam += '&dateString=' + dateFilterValue;
    return viewAdjustmentsParam;
  }

  function _createCrimsonDiffDrilldownParam(agreementSummaryRow, fromExcessDeficitDataSource,
    toExcessDeficitDataSource, excessDeficitDataType, showSegDrilldown) {
    let positionDetailParam = 'legalEntityIds=' + agreementSummaryRow.legalEntityIds;
    positionDetailParam += '&cpeIds=' + agreementSummaryRow.cpeIds;
    positionDetailParam += '&agreementTypeIds=' + agreementSummaryRow.agreementTypeIds;
    positionDetailParam += '&dateString=' + agreementSummaryRow.date;
    positionDetailParam += '&includeLiveAgreements=' + false;
    positionDetailParam += '&includeUnmappedBrokerRecords=' + true;
    positionDetailParam += '&fromExcessDeficitDataSource=' + fromExcessDeficitDataSource;
    positionDetailParam += '&toExcessDeficitDataSource=' + toExcessDeficitDataSource;
    positionDetailParam += '&excessDeficitDataType=' + excessDeficitDataType;
    positionDetailParam += '&showSegDrilldown=' + showSegDrilldown;
    positionDetailParam += '&isCrimsonEnabled=' + true;
    positionDetailParam += '&isCopyUrl=' + true;
    return positionDetailParam;
  }

  /**
   * Gets the position detail window.
   */
  function _launchWindow(windowName, action, params) {
    var url = action + '?' + params;

    var childWindow = window.open(url, '_blank');

    if (childWindow == null || typeof childWindow == 'undefined') {
      alert('Please disable the pop-up block option in the browser settings.');
      return;
    }
  }

  /**
   * Helper method to disable quick links
   */
  function _disableQuickLinks() {
    var quickPublishButton = document.getElementById('quickPublishId');

    quickPublishButton.disabled = true;
    quickPublishButton.onclick = null;
  }

  /**
   * Generates the params need for all the quicklinks
   */
  function _getParam(agreementSummary) {
    return (
      agreementSummary.date +
      '_' +
      agreementSummary.legalEntityId +
      '_' +
      agreementSummary.exposureCounterPartyId +
      '_' +
      agreementSummary.agreementTypeId +
      '_' +
      agreementSummary.agreementId
    );
  }

  function isRedirectToNewPdrScreen(url) {
    return fetch(url).then((response) => response.json()).catch((err) => console.log("Error: ", err));
  }

  function _extractAgreementSummaryGridColumns() {
    var gridColumnIds = [];
    if (_agreementSummaryGrid) {
      var selectedColumns = _agreementSummaryGrid.grid.getColumns();
      for (var i = 0; i < selectedColumns.length; i++) {
        gridColumnIds.push(selectedColumns[i].id);
      }
    }
    return gridColumnIds;
  }
})();
