"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementsummary = {
        showLoading : _showLoading,
        hideLoading : _hideLoading,
        loadAgreementSummaryData : _loadAgreementSummaryData,
        reloadAgreementSummary : _reloadAgreementSummary,
        copySearchUrl : _copySearchUrl,
        loadData : _loadData,
        setFilters : _setFilters
    };

    function _showLoading() {
    	$(".loader").removeAttr("hidden");
        $(".overlay").removeAttr("hidden");
    }

    function _hideLoading() {
    	$(".loader").attr("hidden", "true");
        $(".overlay").attr("hidden", "true");
    }

    function _isUndefinedOrAll(value){
        return value == undefined || value == "-1";
    }

    function _reloadAgreementSummary(checkAndReload, agreementDetails) {
        _showLoading();
        let agreementSummaryGrid = window.treasury.comet.agreementSummaryGridActions.getAgreementSummaryGridAction();
        let gridData = agreementSummaryGrid.data;
        if (checkAndReload && gridData && gridData.length > 0) {
            for (var i = 0; i < gridData.length; i++) {
                let agreementDetail = agreementDetails[gridData[i].agreementId];
                if (agreementDetail != null && agreementDetail != undefined) {


                        let adjustmentKnowledgeTime = agreementDetail["adjustmentKnowledgeTime"];

                        if (gridData[i].knowledgeStartDate != "" && window.treasury.comet.Util.isDefined(gridData[i].knowledgeStartDate)
                            && window.treasury.comet.Util.isDefined(adjustmentKnowledgeTime)) {
                            let knowledgeStartDate = new Date(gridData[i].knowledgeStartDate);
                            if (knowledgeStartDate < adjustmentKnowledgeTime) {
                                checkAndReload = false;
                            }
                        }

                }
            }
        }

        if (checkAndReload) {

            let searchParamsWithRagSyncId = window.treasury.comet.agreementSummaryGridActions.getSearchParamsWithRagSyncId();
            let legalEntityIds = _isUndefinedOrAll(searchParamsWithRagSyncId.legalEntityIds) ?
                    "__null__" : searchParamsWithRagSyncId.legalEntityIds;
            let cpeIds = _isUndefinedOrAll(searchParamsWithRagSyncId.cpeFamilyIds) ?
            "__null__" : searchParamsWithRagSyncId.cpeFamilyIds;
            let agreementTypeIds = _isUndefinedOrAll(searchParamsWithRagSyncId.agreementTypeIds) ?
            "__null__" : searchParamsWithRagSyncId.agreementTypeIds;

            var url = "/treasury/service/lcmExcessDeficitService/isCometDataLatest?" +
                "date=" + searchParamsWithRagSyncId.date +
                "&legalEntityIds=" + legalEntityIds +
                "&cpeIds=" + cpeIds +
                "&agreementTypeIds=" + agreementTypeIds +
                "&ragSyncId=" + searchParamsWithRagSyncId.ragSyncId +
                "&format=json";


            var promise = $.ajax({
                url: url,
                type: "POST"
            });

            promise.done(function (latest) {
                if (!latest) {
                    _reloadAgreementSummaryData();
                } else {

                    let updatedGridData = [];

                    agreementSummaryGrid.setSelectedRowIds([]);

                    if (gridData && gridData.length > 0) {
                        for (var i = 0; i < gridData.length; i++) {
                            let agreementId = gridData[i].agreementId;
                            let agreementDetail = agreementDetails[agreementId];

                            if (window.treasury.comet.Util.isDefined(agreementDetail)) {
                                let workflowStatus = agreementDetail["workflowStatus"];
                                let wireRequest = agreementDetail["wireRequest"];
                                let bookingReference = agreementDetail["bookingReference"];
                                let ecmRC = agreementDetail["ecmRC"];
                                let asmRC = agreementDetail["asmRC"];
                                let disputeAmountRC = agreementDetail["disputeAmountRC"];
                                let segDisputeAmountRC = agreementDetail["segDisputeAmountRC"];
                                let segECMRC = agreementDetail["segECMRC"];
                                let segASMRC = agreementDetail["segASMRC"];

                                if (window.treasury.comet.Util.isDefined(workflowStatus)) {
                                    gridData[i].workflowStatus = workflowStatus;
                                }

                                if (window.treasury.comet.Util.isDefined(ecmRC)) {
                                    gridData[i].ecmRC = ecmRC;
                                    gridData[i].ecm = gridData[i].ecmRC * gridData[i].reportingCurrencyFxRate;
                                }

                                if (window.treasury.comet.Util.isDefined(asmRC)) {
                                    gridData[i].asmRC = asmRC;
                                    gridData[i].asm = gridData[i].asmRC * gridData[i].reportingCurrencyFxRate;
                                }

                                if (window.treasury.comet.Util.isDefined(disputeAmountRC)) {
                                    gridData[i].disputeAmountRC = disputeAmountRC;
                                    gridData[i].disputeAmount = gridData[i].disputeAmountRC * gridData[i].reportingCurrencyFxRate;
                                }

                                if (window.treasury.comet.Util.isDefined(segDisputeAmountRC)) {
                                    gridData[i].segDisputeAmountRC = segDisputeAmountRC;
                                    gridData[i].segDisputeAmount = gridData[i].segDisputeAmountRC * gridData[i].reportingCurrencyFxRate;
                                }

                                if (window.treasury.comet.Util.isDefined(segECMRC)) {
                                    gridData[i].segECMRC = segECMRC;
                                    gridData[i].segECM = gridData[i].segECMRC * gridData[i].reportingCurrencyFxRate;
                                }

                                if (window.treasury.comet.Util.isDefined(segASMRC)) {
                                    gridData[i].segASMRC = segASMRC;
                                    gridData[i].segASM = gridData[i].segASMRC * gridData[i].reportingCurrencyFxRate;
                                }

                                if (window.treasury.comet.Util.isDefined(wireRequest)) {
                                    gridData[i].wireRequest = wireRequest;
                                }

                                if (window.treasury.comet.Util.isDefined(bookingReference)) {
                                    gridData[i].bookingReference = bookingReference;
                                }

                            }
                            if ((gridData[i].workflowStatus == "IN_PROGRESS" && searchParamsWithRagSyncId.includeInProgress) ||
                                (gridData[i].workflowStatus == "NOT_STARTED" && searchParamsWithRagSyncId.includeNotStarted) ||
                                (gridData[i].workflowStatus == "PUBLISHED" && searchParamsWithRagSyncId.includePublished)) {
                                updatedGridData.push(gridData[i]);
                            }
                        }
                    }
                    if (updatedGridData.length > 0) {
                        agreementSummaryGrid.data = updatedGridData;
                        agreementSummaryGrid.setDataItems(updatedGridData);
                        agreementSummaryGrid.refreshGrid([]);

                        var agreementDetailDiv = document.getElementById("agreementDetailDiv");
                        if (agreementDetailDiv) {
                            agreementDetailDiv.hidden = true;
                        }
                        window.treasury.comet.agreementSummaryGridActions.computeAndChangeGridHeight();

                    } else {
                        var quickLinks = document.getElementById("quickLinks");
                        quickLinks.style.display = "none";

                        agreementSummaryGrid.data = null;


                        var agreementSummaryView = document.getElementById('agreementSummaryView');
                        agreementSummaryView.innerHTML = "No Data Found";
                        var header = document.getElementById("agreementSummaryGrid-header");
                        if (header != null) {
                            header.remove();
                        }
                        if (agreementSummaryView.hasAttribute("hidden")) {

                            agreementSummaryView.removeAttribute("hidden");
                        }
                        var agreementSummaryGridDiv = document.getElementById('agreementSummaryGridDiv');
                        if (!agreementSummaryGridDiv.hasAttribute("hidden")) {
                            agreementSummaryGridDiv.setAttribute("hidden", true);
                        }

                        var statusLegendDiv = document.getElementById('statusLegend');
                        if (!statusLegendDiv.hasAttribute("hidden")) {
                            statusLegendDiv.setAttribute("hidden", true);
                        }

                        var agreementDetailDiv = document.getElementById("agreementDetailDiv");
                        if (agreementDetailDiv) {
                            agreementDetailDiv.hidden = true;
                        }


                        var leftSidebar = document.getElementById('agreementFilterSidebar');
                        if (leftSidebar.state == "expanded") {
                            leftSidebar.toggle();
                        }


                    }
                    _hideLoading();

                }

            });

            promise.fail(function (data) {
                _reloadAgreementSummaryData();

            });
        } else {
            _reloadAgreementSummaryData();
        }


    }

    function _reloadAgreementSummaryData() {
        var showWorkflowStatus = _getWorkflowStatusFilter();

        var showExceptionStatus = _getExceptionStatusFilter();

        var datepicker = document.getElementById('datepicker');
        //var date = new Date(datepicker.value);
        //var formattedDate = getFormattedDate(date);
        var defaultParameters = {
            "cpeFamilyIds": window.treasury.comet.agreementSummaryFilterAction
                .getSelectedCounterParties(),
            "legalEntityIds": window.treasury.comet.agreementSummaryFilterAction
                .getSelectedLegalEntities(),
            "agreementTypeIds": window.treasury.comet.agreementSummaryFilterAction
                .getSelectedAgreementTypes(),
            "includeLiveAgreements": true,
            "dateString": datepicker.value,
            "workflowStatusFilter": showWorkflowStatus,
            "exceptionStatusFilter": showExceptionStatus

        };

        window.treasury.comet.agreementSummaryGridActions
            .loadAgreementSummaryGrid(defaultParameters);

    }

function _setFilters(searchData)
 {

               var cpeFamilyIds = searchData["cpeFamilyIds"];
               var legalEntityIds = searchData["legalEntityIds"];
               var agreementTypeIds = searchData["agreementTypeIds"];

               if(agreementTypeIds!=undefined) {
               	agreementTypeIds = agreementTypeIds.split(',');
               	window.treasury.comet.agreementSummaryFilterAction.getAgreementTypeMultiSelect().setSelectedNodes(agreementTypeIds);
               }

               if(legalEntityIds!=undefined) {
                   legalEntityIds = legalEntityIds.split(',');
                     window.treasury.comet.agreementSummaryFilterAction.getLegalEntityFamilyMultiSelectFilter().setSelectedNodes(legalEntityIds);
               }

               if(cpeFamilyIds!=undefined) {
                   cpeFamilyIds = cpeFamilyIds.split(',');
                   window.treasury.comet.agreementSummaryFilterAction.getCpeMultiSelect().setSelectedNodes(cpeFamilyIds);
               }

               if(searchData["workflowStatusFilter"].includes("NOT_STARTED"))
               {
                 document.getElementById("showNotStartedCB").checked=true;
               }
               if(searchData["workflowStatusFilter"].includes("IN_PROGRESS"))
               {
                 document.getElementById("showInProgressCB").checked=true;
               }
               if(searchData["workflowStatusFilter"].includes("PUBLISHED"))
               {
                 document.getElementById("showPublishedCB").checked=true;
               }
               if(searchData["exceptionStatusFilter"].includes("CRITICAL"))
               {
                 document.getElementById("showCritical").checked=true;
               }
               if(searchData["exceptionStatusFilter"].includes("WARNING"))
               {
                 document.getElementById("showWarning").checked=true;
               }
               if(searchData["exceptionStatusFilter"].includes("PASSED"))
               {
                 document.getElementById("showPassed").checked=true;
               }


 }
    function _loadData()
    {
    	if(window.location.href.indexOf("isCopyUrl=true") >= 0) {
    		var urlParams = window.location.href.split("?");
    		if(urlParams[1] != undefined) {
    			var paramList = urlParams[1].split("&");
    		}
    		var searchData = {};
    		for(var i=0; i < paramList.length ; i++) {
    			var param = paramList[i].split("=");
    			var val = param[1];
    			if(val != null && param[0] != "isCopyUrl") {
    				searchData[param[0]]=val;
    			}
    		}
        var leftSidebar = document.getElementById('agreementFilterSidebar');
        if(leftSidebar.state == "expanded") {
            leftSidebar.toggle();
        }
    		searchData["includeLiveAgreements"]=true;
            window.treasury.comet.agreementSummaryGridActions.loadAgreementSummaryGrid(searchData);

      }else{
    	  var agreementSummaryView = document.getElementById('agreementSummaryView');

          if(agreementSummaryView.hasAttribute("hidden")) {
        	  agreementSummaryView.removeAttribute("hidden");
          }
        treasury.comet.agreementsummary.hideLoading();

      }

    }



    // _initialize is called on page load.
    function _initialize() {

        // step 2 : Load Summary view filters if not already loaded
        // TODO : check if filters are loaded already
    	treasury.comet.agreementsummary.showLoading();
        window.treasury.comet.agreementSummaryFilterAction.loadAgreementSummaryFilters();
        var saveSettings = document.querySelector('arc-save-settings');
        var host = '';
        var clientName = document.getElementById("clientName").value;
        var stabilityLevel = document.getElementById("stabilityLevel").value;

        if(clientName === 'desco') {
            if(stabilityLevel == 'prod') {
                host += "http://landing-app.deshaw.c.ia55.net";
            } else if(stabilityLevel == 'uat'){
                host = "http://landing-app.deshawuat.c.ia55.net";
            } else if(stabilityLevel == 'qa'){
                host = "https://mars.arcesium.com";
            } else if(stabilityLevel == 'dev'){
                host = "https://terra.arcesium.com";
            }
        }

        saveSettings.serviceURL = host+'/service/SettingsService';
        saveSettings.applicationId = 3;
        saveSettings.applicationCategory = 'AgreementSummary';

        document.getElementById('summary-search-save').onclick = function(){
          saveSettings.openSaveDialog();
        };

        saveSettings.saveSettingsCallback = function(){


        var showWorkflowStatus = _getWorkflowStatusFilter();

        var showExceptionStatus = _getExceptionStatusFilter();

        var defaultParameters = {
              "cpeFamilyIds" : window.treasury.comet.agreementSummaryFilterAction
                      .getSelectedCounterParties(),
              "legalEntityIds" : window.treasury.comet.agreementSummaryFilterAction
                      .getSelectedLegalEntities(),
              "agreementTypeIds" : window.treasury.comet.agreementSummaryFilterAction
                      .getSelectedAgreementTypes(),
              "includeLiveAgreements" : true,
              "workflowStatusFilter" : showWorkflowStatus,
              "exceptionStatusFilter" : showExceptionStatus

          };
        return defaultParameters;

      }

      saveSettings.applySettingsCallback = function(searchData){



      var datepicker = document.getElementById('datepicker');
      searchData["dateString"] = datepicker.value;

        _setFilters(searchData);

        }


        // The right panel should not appear on load. Minimizing on page load
        //window.treasury.comet.agreementDetail.collapseAgreementDetailPanel();

    }

    function _loadAgreementSummaryData() {

        // Additional Filter Input data
        var showWorkflowStatus = _getWorkflowStatusFilter();

        var showExceptionStatus = _getExceptionStatusFilter();

        var datepicker = document.getElementById('datepicker');
        //var date = new Date(datepicker.value);
        //var formattedDate = getFormattedDate(date);
        var defaultParameters = {
            "cpeFamilyIds" : window.treasury.comet.agreementSummaryFilterAction.getSelectedCounterParties(),
            "legalEntityIds" : window.treasury.comet.agreementSummaryFilterAction.getSelectedLegalEntities(),
            "agreementTypeIds" : window.treasury.comet.agreementSummaryFilterAction.getSelectedAgreementTypes(),
            "includeLiveAgreements" : true,
            "dateString" : datepicker.value,
            "workflowStatusFilter" : showWorkflowStatus,
            "exceptionStatusFilter" : showExceptionStatus
        };

        window.treasury.comet.agreementSummaryGridActions
                .loadAgreementSummaryGrid(defaultParameters);
    }

    function _copySearchUrl() {

        var url = "/treasury/comet/agreementSummary";
        treasury.comet.agreementsummary.showLoading();
        var showWorkflowStatus = _getWorkflowStatusFilter();

        var showExceptionStatus = _getExceptionStatusFilter();

        var datepicker = document.getElementById('datepicker');

        var defaultParameters = {
            "cpeFamilyIds" : window.treasury.comet.agreementSummaryFilterAction.getSelectedCounterParties(),
            "legalEntityIds" : window.treasury.comet.agreementSummaryFilterAction.getSelectedLegalEntities(),
            "agreementTypeIds" : window.treasury.comet.agreementSummaryFilterAction.getSelectedAgreementTypes(),
            "includeLiveAgreements" : true,
            "dateString" : datepicker.value,
            "workflowStatusFilter" : showWorkflowStatus,
            "exceptionStatusFilter" : showExceptionStatus,
            "isCopyUrl" : true
        };

        generateReportURL(url, defaultParameters, "resultList");
        treasury.comet.agreementsummary.hideLoading();

    }

    /**
     * Helper method to get workflow status
     */
    function _getWorkflowStatusFilter() {

        var showNotStarted = document.getElementById('showNotStartedCB').checked == true ? "NOT_STARTED"
                : null;
        var showPublished = document.getElementById('showInProgressCB').checked == true ? "IN_PROGRESS"
                : null;
        var showInProgress = document.getElementById('showPublishedCB').checked == true ? "PUBLISHED"
                : null;
        var showWorkflowStatus = '';
        if (showNotStarted != null) {
            showWorkflowStatus += showNotStarted;
        }
        if (showPublished != null) {
            showWorkflowStatus += ',' + showPublished;
        }
        if (showInProgress != null) {
            showWorkflowStatus += ',' + showInProgress;
        }

        return showWorkflowStatus;
    }

    /**
     * Helper method to get exception status
     */
    function _getExceptionStatusFilter() {

        var showCritical = document.getElementById('showCritical').checked == true ? "CRITICAL"
                : null;
        var showWarning = document.getElementById('showWarning').checked == true ? "WARNING"
                : null;
        var showPassed = document.getElementById('showPassed').checked == true ? "CLEAR"
                : null;
        var showExceptionStatus = '';
        if (showCritical != null) {
            showExceptionStatus += showCritical;
        }
        if (showWarning != null) {
            showExceptionStatus += ',' + showWarning;
        }
        if (showPassed != null) {
            showExceptionStatus += ',' + showPassed;
        }

        return showExceptionStatus;
    }

    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return year + '/' + month + '/' + day;
    }

    WebComponents.waitFor(function() {

            _initialize();
          });


})();
