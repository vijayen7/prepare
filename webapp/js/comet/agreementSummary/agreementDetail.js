"use strict";

(function () {

    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementDetail = {
        //collapseAgreementDetailPanel : _collapseAgreementDetailPanel,
        //expandAgreementDetailPanel : _expandAgreementDetailPanel,
        populateRightSideBar: _populateRightSideBar,
        //toggleRightSidebarPanels : _toggleRightSidebarPanels,
        initQuickLinksInRightPanel: _initQuickLinksInRightPanel,
        //toggleRightPanel : _toggleRightPanel,
        toggleLeftPanel: _toggleLeftPanel,
        getAgreementDetailPreviousState: _getAgreementDetailPreviousState,
        setAgreementDetailPreviousState: _setAgreementDetailPreviousState,
        getAgreementDetailCurrentState: _getAgreementDetailCurrentState,
        executeBrokerAndRagSync: _executeBrokerAndRagSync,
        launchAttributeDetails: _launchAttributeDetails,
        populateIntVsExtData: _populateIntVsExtData,
        populateIntT1VsT2: _populateIntT1VsT2,
        populateExtT1VsT2: _populateExtT1VsT2,
        populateIntVsIntAdjusted: _populateIntVsIntAdjusted,
        populateCollateralMgmtVsReconTab: _populateCollateralMgmtVsReconTab,
        getInternalVsExternalData: _getInternalVsExternalData
    };

    var _rightSideBarPreviousState;
    let CONSTANTS;
    let CRIMSON_TABLES;
    let AGREEMENT_TYPE_TABLES;

    function _initializeConstants() {
        if (window.treasury.comet.constants) {
            CONSTANTS = window.treasury.comet.constants;
            CRIMSON_TABLES = window.treasury.comet.crimsonTables;
            AGREEMENT_TYPE_TABLES = CONSTANTS.AGREEMENT_TYPE_TABLES;
        }
    }

    function _getAgreementDetailPreviousState() {
        return _rightSideBarPreviousState;
    }

    function _setAgreementDetailPreviousState(rightPanelState) {
        _rightSideBarPreviousState = rightPanelState;
    }

    function _getAgreementDetailCurrentState() {
        var rightPanelState = document.getElementById('rightSidebar').state;
        return rightPanelState;
    }


    function _populateCollateralMgmtVsReconTab(agreementSummary) {
        let agreementType = agreementSummary.agreementType;
        let tag = agreementSummary.tag;
        _initializeConstants();
        let crimsonTables = AGREEMENT_TYPE_TABLES[agreementType];
        let internalVsMarginFileData = getInternalVsMarginFileData(agreementSummary, crimsonTables);
        let posRecVsMarginFileData = getPosRecVsMarginFileData(agreementSummary, crimsonTables);
        let internalVsPosRecData = getInternalVsPosRecData(agreementSummary, crimsonTables);
        let internalVsRepoRecData = getInternalVsRepoRecData(agreementSummary, crimsonTables);
        let reconStatusSummaryData = getReconStatusSummaryData(agreementSummary.date, agreementSummary.agreementId, crimsonTables);
        let dayOnDayChangesData = getDayOnDayChanges(agreementSummary, crimsonTables);
        let internalVsMarginFileSegData = getInternalVsMarginFileSegData(agreementSummary, crimsonTables);

        let crimsonDiffData = {
            internalVsMarginFileData: internalVsMarginFileData,
            posRecVsMarginFileData: posRecVsMarginFileData,
            internalVsPosRecData: internalVsPosRecData,
            internalVsRepoRecData: internalVsRepoRecData,
            reconStatusSummaryData: reconStatusSummaryData,
            dayOnDayChangesData: dayOnDayChangesData,
            internalVsMarginFileSegData: internalVsMarginFileSegData,
            agreementType: agreementType,
            tag: tag
        }
        crimsonDiffData.todaySummaryDetail = getTodaysSummaryDetails(crimsonDiffData);
        renderCollateralManagementVsReconTab(crimsonDiffData, agreementSummary);
    }

    function getTodaysSummaryDetails(crimsonDiffData) {
        if (CONSTANTS.AGREEMENT_TYPES_WITH_STATIC_CRIMSON_VIEW.includes(crimsonDiffData.agreementType)) {
            return getStaticTodaysSummaryView(crimsonDiffData);
        } else {
            return computeTopThreeDiffs(crimsonDiffData);
        }
    }

    function onClickCrimsonDiff(agreementSummary, fromExcessDeficitDataSource, toExcessDeficitDataSource,
        excessDeficitDataType, showSegDrilldown) {
        window.treasury.comet.agreementSummaryGridActions.showCrimsonDiffDrilldown(agreementSummary,
            fromExcessDeficitDataSource, toExcessDeficitDataSource, excessDeficitDataType, showSegDrilldown);
    }

    function renderCollateralManagementVsReconTab(data, agreementSummary) {
        ReactDOM.render(
            React.createElement(TreasuryComponents.AgmtSummaryVsReconcilerSummaryDetails, {
                data: data,
                onClick: (fromExcessDeficitDataSource, toExcessDeficitDataSource, excessDeficitDataType, showSegDrilldown) => onClickCrimsonDiff(agreementSummary,
                    fromExcessDeficitDataSource, toExcessDeficitDataSource, excessDeficitDataType, showSegDrilldown)
            }),
            document.querySelector("#CollateralMgmtVsReconTab")
        );
    }

    function validateAndComputeDiff(value1, value2, formatResult) {
        if (formatResult) {
            return (!checkUndefinedOrEmpty(value1) && !checkUndefinedOrEmpty(value2) &&
                !checkUndefinedOrEmpty(value1 - value2) && !isNaN(value1 - value2)) ? validateAndFormatNumber(value1 - value2) : CONSTANTS.NA;
        } else {
            return (!checkUndefinedOrEmpty(value1) && !checkUndefinedOrEmpty(value2) &&
                !checkUndefinedOrEmpty(value1 - value2) && !isNaN(value1 - value2)) ? value1 - value2 : CONSTANTS.NA;
        }
    }

    function validateAndAdd() {
        let sum = 0;
        let allValuesNaN = true;
        for (let i = 0; i < arguments.length; i++) {
            let value = arguments[i];
            if (!isNaN(value)) {
                sum += arguments[i];
                allValuesNaN = false;
            }
        }
        return allValuesNaN ? CONSTANTS.NA : sum
    }

    function validateAndFormatNumber(value) {
        return !isNaN(value) && !checkUndefinedOrEmpty(value) ? window.treasury.comet.Util.plainNumberFormatter(null, null, value, null) : CONSTANTS.NA;
    }

    function validateAndFormatRecStatusNumber(value) {
        return !isNaN(value) && !checkUndefinedOrEmpty(value) ? window.treasury.comet.Util.plainNumberFormatter(null, null, value, null) : 0;
    }


    function createObjectForHover(value, column, table) {
        return {
            value: value,
            hoverOnColumn: column,
            hoverOnTable: table
        }
    }

    function computeTopThreeDiffs(crimsonDiffData) {
        let diffList = [];
        diffList = diffList.concat(getAllDiffsForTableData(crimsonDiffData.internalVsMarginFileData, CONSTANTS.INTERNAL_VS_MARGIN_FILE_DATA));
        diffList = diffList.concat(getAllDiffsForTableData(crimsonDiffData.posRecVsMarginFileData, CONSTANTS.POS_REC_VS_MARGIN_FILE_DATA));
        diffList = diffList.concat(getAllDiffsForTableData(crimsonDiffData.internalVsPosRecData, CONSTANTS.INTERNAL_VS_POS_REC_DATA));
        diffList = diffList.concat(getAllDiffsForTableData(crimsonDiffData.internalVsRepoRecData, CONSTANTS.INTERNAL_VS_REPO_REC_DATA));
        let segDiffData = crimsonDiffData.internalVsMarginFileSegData.unformattedDiffDataRow
        if (segDiffData !== undefined) {
            diffList = diffList.concat([
                createObjectForHover(segDiffData[0], CONSTANTS.SEG_REQUIREMENT, CONSTANTS.INTERNAL_VS_MARGIN_FILE_SEG),
                createObjectForHover(segDiffData[1], CONSTANTS.SEG_CASH_COLLATERAL, CONSTANTS.INTERNAL_VS_MARGIN_FILE_SEG),
                createObjectForHover(segDiffData[2], CONSTANTS.SEG_SECURITIES_COLLATERAL, CONSTANTS.INTERNAL_VS_MARGIN_FILE_SEG)
            ]);
        }

        let tableRows = topThreeDiffs(diffList)
        return { tableRows: tableRows, excessDeficitDiff: getExcessDeficitDiff(crimsonDiffData) };

    }

    function topThreeDiffs(diffList) {
        let validDiffList = [];

        for (let i = 0; i < diffList.length; i++) {
            if (diffList[i].value && diffList[i].value != CONSTANTS.NA && Math.abs(diffList[i].value) >= 0.5) {
                validDiffList.push(diffList[i]);
            }
        }
        validDiffList.sort(function (a, b) {
            return Math.abs(Number(b.value)) - Math.abs(Number(a.value));
        });

        // Since we only need remove duplicates from collateral diffs , only one diff at max will be removed
        // hence we are taking 4 as max value.
        if (validDiffList.length > 0) {
            validDiffList = validDiffList.slice(0, Math.min(4, validDiffList.length));
        }

        if (validDiffList.length >= 2 && isDuplicateCollateralPresent(validDiffList[0], validDiffList[1])) {
            validDiffList = validDiffList.slice(1);
        }
        else if (validDiffList.length >= 3 && isDuplicateCollateralPresent(validDiffList[1], validDiffList[2])) {
            validDiffList.splice(2, 1);
        }

        let tableRows = getTodaySummaryDiffTableRows(validDiffList);

        if (tableRows.length == 0) {
            tableRows.push(["No Diffs to highlight", "", ""]);
        }

        return tableRows;
    }

    function isDuplicateCollateralPresent(e1, e2) {
        if (e1.value == e2.value && e1.hoverOnColumn == e2.hoverOnColumn && e1.hoverOnColumn == CONSTANTS.COLLATERAL) {
            return true;
        }
        return false;
    }

    // This will add all ( E, C, M) diffs from given tableData and create hover objects for them.
    function getAllDiffsForTableData(tableData, tableName) {
        if (tableData != null && tableData.unformattedDiffDataRow !== undefined) {
            let diffData = tableData.unformattedDiffDataRow;
            return createHoverRowsForData(diffData, tableName);
        }
        return [];
    }

    // Here data is instance of TableDiff class , use accordingly.
    function createHoverRowsForData(data, tableName) {
        return [
            createObjectForHover(data.exposure, CONSTANTS.EXPOSURE, tableName),
            createObjectForHover(data.requirement, CONSTANTS.REQUIREMENT, tableName),
            createObjectForHover(data.cashCollateral, CONSTANTS.CASH_COLLATERAL, tableName),
            createObjectForHover(data.physicalCollateral, CONSTANTS.SECURITIES_COLLATERAL, tableName)
        ]
    }

    function getStaticTodaysSummaryView(crimsonDiffData) {

        let excessDeficitDiff = getExcessDeficitDiff(crimsonDiffData);

        let internalVsPosRecExposureDiffRow = getDiffRowForHover(crimsonDiffData.internalVsPosRecData.unformattedDiffDataRow, CONSTANTS.EXPOSURE, CONSTANTS.INTERNAL_VS_POS_REC_DATA);
        let internalVsRepoRecExposureDiffRow = getDiffRowForHover(crimsonDiffData.internalVsRepoRecData.unformattedDiffDataRow, CONSTANTS.EXPOSURE, CONSTANTS.INTERNAL_VS_REPO_REC_DATA);
        let posRecVsMarginFileRequirementDiff = getDiffRowForHover(crimsonDiffData.posRecVsMarginFileData.unformattedDiffDataRow, CONSTANTS.REQUIREMENT, CONSTANTS.POS_REC_VS_MARGIN_FILE_DATA);
        let posRecVsMarginFileExposureDiff = getDiffRowForHover(crimsonDiffData.posRecVsMarginFileData.unformattedDiffDataRow, CONSTANTS.EXPOSURE, CONSTANTS.POS_REC_VS_MARGIN_FILE_DATA);

        let diffList = [...internalVsPosRecExposureDiffRow, ...internalVsRepoRecExposureDiffRow, ...posRecVsMarginFileRequirementDiff, ...posRecVsMarginFileExposureDiff];

        let tableRows = getTodaySummaryDiffTableRows(diffList);

        return {
            excessDeficitDiff: excessDeficitDiff,
            tableRows: tableRows
        };
    }

    function getExcessDeficitDiff(crimsonDiffData) {
        return crimsonDiffData.internalVsMarginFileData.tableRows[CONSTANTS.DIFF_ROW_INDEX][CONSTANTS.EXCESS_DEFICIT_COLUMN_INDEX];
    }

    function getDiffRowForHover(unformattedDiffDataRow, excessDeficitDataType, tableName) {
        let diffRow = [];
        if (unformattedDiffDataRow && typeof excessDeficitDataType == "string") {
            diffRow.push(createObjectForHover(unformattedDiffDataRow[excessDeficitDataType.toLowerCase()],
                excessDeficitDataType, tableName));
        }
        return diffRow;
    }

    function getTodaySummaryDiffTableRows(diffList) {
        let tableRows = [];

        for (let i = 1; i <= Math.min(diffList.length, 3); i++) {
            diffList[i - 1].value = validateAndFormatNumber(diffList[i - 1].value);
            tableRows.push([i + ")  ", diffList[i - 1], CONSTANTS.DIFF_ROW_NAMES[diffList[i - 1].hoverOnTable][diffList[i - 1].hoverOnColumn]]);
        }

        return tableRows;
    }

    function getInternalVsMarginFileData(agreementSummary, tables) {
        let data = {};
        if (!tables.includes(CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE)) {
            return data;
        }
        data.tableName = CONSTANTS.INTERNAL_VS_MARGIN_FILE_DATA;
        data.numberOfColumns = 6;
        data.headerRow = [CONSTANTS.SOURCE, CONSTANTS.EXPOSURE, CONSTANTS.REQUIREMENT, CONSTANTS.CASH_COLLATERAL, CONSTANTS.SECURITIES_COLLATERAL, CONSTANTS.ED];
        let internalDataRow = [
            CONSTANTS.INTERNAL,
            validateAndFormatNumber(agreementSummary.adjustedExposureRC),
            validateAndFormatNumber(agreementSummary.adjustedRequirementRC),
            validateAndFormatNumber(agreementSummary.adjustedCashCollateralRC),
            validateAndFormatNumber(agreementSummary.adjustedPhysicalCollateralRC),
            validateAndFormatNumber(agreementSummary.adjustedExcessDeficitRC)
        ];
        let externalDataRow = [
            CONSTANTS.MARGIN_FILE,
            validateAndFormatNumber(agreementSummary.brokerExposureRC),
            validateAndFormatNumber(agreementSummary.brokerRequirementRC),
            validateAndFormatNumber(agreementSummary.brokerCashCollateralRC),
            validateAndFormatNumber(agreementSummary.brokerPhysicalCollateralRC),
            validateAndFormatNumber(agreementSummary.brokerExcessDeficitRC)
        ]

        let diffDataRow = [
            CONSTANTS.DIFF,
            validateAndComputeDiff(agreementSummary.adjustedExposureRC, agreementSummary.brokerExposureRC, true),
            validateAndComputeDiff(agreementSummary.adjustedRequirementRC, agreementSummary.brokerRequirementRC, true),
            validateAndComputeDiff(agreementSummary.adjustedCashCollateralRC, agreementSummary.brokerCashCollateralRC, true),
            validateAndComputeDiff(agreementSummary.adjustedPhysicalCollateralRC, agreementSummary.brokerPhysicalCollateralRC, true),
            validateAndComputeDiff(agreementSummary.adjustedExcessDeficitRC, agreementSummary.brokerExcessDeficitRC, true)
        ]
        let tableRows = [];
        tableRows.push(internalDataRow);
        tableRows.push(externalDataRow);
        tableRows.push(diffDataRow);
        let unformattedDiffDataRow = new TableDiff(
            validateAndComputeDiff(agreementSummary.adjustedExposureRC, agreementSummary.brokerExposureRC, false),
            validateAndComputeDiff(agreementSummary.adjustedRequirementRC, agreementSummary.brokerRequirementRC, false),
            validateAndComputeDiff(agreementSummary.adjustedCashCollateralRC, agreementSummary.brokerCashCollateralRC, false),
            validateAndComputeDiff(agreementSummary.adjustedPhysicalCollateralRC, agreementSummary.brokerPhysicalCollateralRC, false)
        );
        data.tableRows = tableRows;
        data.unformattedDiffDataRow = unformattedDiffDataRow;
        return data;
    }

    function getPosRecVsMarginFileData(agreementSummary, tables) {
        let data = {};
        if (!tables.includes(CRIMSON_TABLES.TOP_POS_REC_VS_MARGIN_FILE)
            && !tables.includes(CRIMSON_TABLES.BOTTOM_POS_REC_VS_MARGIN_FILE)) {
            return data;
        }
        data.tableName = CONSTANTS.POS_REC_VS_MARGIN_FILE_DATA;
        data.numberOfColumns = 3;
        data.headerRow = [CONSTANTS.SOURCE, CONSTANTS.EXPOSURE, CONSTANTS.REQUIREMENT];

        let externalReconDataRow = [
            CONSTANTS.POS_REC,
            validateAndFormatNumber(agreementSummary.reconExposureRC),
            validateAndFormatNumber(agreementSummary.reconMarginRC)
        ]

        let externalLCMDataRow = [
            CONSTANTS.MARGIN_FILE,
            validateAndFormatNumber(agreementSummary.brokerExposureRC),
            validateAndFormatNumber(agreementSummary.brokerRequirementRC)
        ]

        let diffDataRow = [
            CONSTANTS.DIFF,
            validateAndComputeDiff(agreementSummary.reconExposureRC, agreementSummary.brokerExposureRC, true),
            validateAndComputeDiff(agreementSummary.reconMarginRC, agreementSummary.brokerRequirementRC, true)
        ]

        let tableRows = [];
        tableRows.push(externalReconDataRow);
        tableRows.push(externalLCMDataRow);
        tableRows.push(diffDataRow);

        let unformattedDiffDataRow = new TableDiff(
            validateAndComputeDiff(agreementSummary.reconExposureRC, agreementSummary.brokerExposureRC, false),
            validateAndComputeDiff(agreementSummary.reconMarginRC, agreementSummary.brokerRequirementRC, false)
        )
        data.tableRows = tableRows;
        data.unformattedDiffDataRow = unformattedDiffDataRow;
        return data;
    }

    function getInternalVsPosRecData(agreementSummary, tables) {
        let data = {};
        if (!tables.includes(CRIMSON_TABLES.INTERNAL_VS_POS_REC)) {
            return data;
        }
        data.tableName = CONSTANTS.INTERNAL_VS_POS_REC_DATA;
        data.numberOfColumns = 3;
        data.headerRow = [CONSTANTS.SOURCE, CONSTANTS.EXPOSURE, CONSTANTS.REQUIREMENT];

        let internalDataRow = [
            CONSTANTS.INTERNAL,
            validateAndFormatNumber(agreementSummary.adjustedExposureRC),
            validateAndFormatNumber(agreementSummary.adjustedRequirementRC)
        ]

        let externalReconDataRow = [
            CONSTANTS.POS_REC,
            validateAndFormatNumber(agreementSummary.reconExposureRC),
            validateAndFormatNumber(agreementSummary.reconMarginRC)
        ]

        let diffDataRow = [
            CONSTANTS.DIFF,
            validateAndComputeDiff(agreementSummary.adjustedExposureRC, agreementSummary.reconExposureRC, true),
            validateAndComputeDiff(agreementSummary.adjustedRequirementRC, agreementSummary.reconMarginRC, true)
        ]

        let tableRows = [];
        tableRows.push(internalDataRow);
        tableRows.push(externalReconDataRow);
        tableRows.push(diffDataRow);
        let unformattedDiffDataRow = new TableDiff(
            validateAndComputeDiff(agreementSummary.adjustedExposureRC, agreementSummary.reconExposureRC, false),
            validateAndComputeDiff(agreementSummary.adjustedRequirementRC, agreementSummary.reconMarginRC, false)
        );
        data.tableRows = tableRows;
        data.unformattedDiffDataRow = unformattedDiffDataRow;
        return data;
    }

    function getInternalVsRepoRecData(agreementSummary, tables) {
        let data = {};
        if (!tables.includes(CRIMSON_TABLES.INTERNAL_VS_REPO_REC)) {
            return data;
        }
        data.tableName = CONSTANTS.INTERNAL_VS_REPO_REC_DATA;
        data.numberOfColumns = 3;
        data.headerRow = [CONSTANTS.SOURCE, CONSTANTS.EXPOSURE, CONSTANTS.REQUIREMENT];

        let internalDataRow = [
            CONSTANTS.INTERNAL,
            validateAndFormatNumber(agreementSummary.adjustedExposureRC),
            validateAndFormatNumber(agreementSummary.adjustedRequirementRC)
        ]

        let externalReconDataRow = [
            CONSTANTS.REPO_REC,
            validateAndFormatNumber(agreementSummary.reconExposureRC),
            validateAndFormatNumber(agreementSummary.reconMarginRC)
        ]

        let diffDataRow = [
            CONSTANTS.DIFF,
            validateAndComputeDiff(agreementSummary.adjustedExposureRC, agreementSummary.reconExposureRC, true),
            validateAndComputeDiff(agreementSummary.adjustedRequirementRC, agreementSummary.reconMarginRC, true)
        ]

        let tableRows = [];
        tableRows.push(internalDataRow);
        tableRows.push(externalReconDataRow);
        tableRows.push(diffDataRow);
        let unformattedDiffDataRow = new TableDiff(
            validateAndComputeDiff(agreementSummary.adjustedExposureRC, agreementSummary.reconExposureRC, false),
            validateAndComputeDiff(agreementSummary.adjustedRequirementRC, agreementSummary.reconMarginRC, false)
        );
        data.tableRows = tableRows;
        data.unformattedDiffDataRow = unformattedDiffDataRow;
        return data;
    }

    function getReconStatusSummaryData(date, agreementId, tables) {
        if (!tables.includes(CRIMSON_TABLES.RECON_STATUS_SUMMARY)) {
            return {};
        }
        var url = "/treasury/service/reconDataService/getReconAgreementStatusData?lcmPositionDataFilter.date="
            + date + "&lcmPositionDataFilter.agreementIds=" + agreementId + "&format=Json&inputFormat=PROPERTIES";
        var promise = $.ajax({
            url: url,
            type: "GET",
            async: false
        });
        return createReconStatusDataFromResponse(promise);
    }

    function createReconStatusDataFromResponse(promise) {
        let reconData = JSON.parse(promise.responseText);
        let reconStatusData = {};
        reconStatusData.numberOfColumns = 6;
        reconStatusData.tableName = CONSTANTS.RECONCILER_STATUS_SUMMARY;
        reconStatusData.headerRow = [CONSTANTS.COMPONENT, CONSTANTS.BREAK, CONSTANTS.INTERNAL_MISS, CONSTANTS.CPE_MISS, CONSTANTS.MATCH, CONSTANTS.TOTAL];
        let lmvMap = new Map();
        let smvMap = new Map();
        let noOfRecordsMap = new Map();

        for (let i in reconData) {
            lmvMap.set(reconData[i].recStatus, reconData[i].lmvDiff);
            smvMap.set(reconData[i].recStatus, reconData[i].smvDiff);
            noOfRecordsMap.set(reconData[i].recStatus, reconData[i].dataCount);
        }

        let lmvDiffRow = [
            CONSTANTS.LMV_DIFF,
            validateAndFormatRecStatusNumber(lmvMap.get(CONSTANTS.BREAK)),
            validateAndFormatRecStatusNumber(lmvMap.get(CONSTANTS.INTERNAL_MISSING)),
            validateAndFormatRecStatusNumber(lmvMap.get(CONSTANTS.CPE_MISSING)),
            validateAndFormatRecStatusNumber(lmvMap.get(CONSTANTS.MATCH))
        ];
        let smvDiffRow = [
            CONSTANTS.SMV_DIFF,
            validateAndFormatRecStatusNumber(smvMap.get(CONSTANTS.BREAK)),
            validateAndFormatRecStatusNumber(smvMap.get(CONSTANTS.INTERNAL_MISSING)),
            validateAndFormatRecStatusNumber(smvMap.get(CONSTANTS.CPE_MISSING)),
            validateAndFormatRecStatusNumber(smvMap.get(CONSTANTS.MATCH))
        ];
        let noOfRecordsRow = [
            CONSTANTS.NO_OF_RECORDS,
            validateAndFormatRecStatusNumber(noOfRecordsMap.get(CONSTANTS.BREAK)),
            validateAndFormatRecStatusNumber(noOfRecordsMap.get(CONSTANTS.INTERNAL_MISSING)),
            validateAndFormatRecStatusNumber(noOfRecordsMap.get(CONSTANTS.CPE_MISSING)),
            validateAndFormatRecStatusNumber(noOfRecordsMap.get(CONSTANTS.MATCH))
        ];

        let lmvDiffSum = validateAndAdd(lmvMap.get(CONSTANTS.BREAK), lmvMap.get(CONSTANTS.INTERNAL_MISSING), lmvMap.get(CONSTANTS.CPE_MISSING), lmvMap.get(CONSTANTS.MATCH));
        lmvDiffRow.push(validateAndFormatRecStatusNumber(lmvDiffSum));

        let smvDiffSum = validateAndAdd(smvMap.get(CONSTANTS.BREAK), smvMap.get(CONSTANTS.INTERNAL_MISSING), smvMap.get(CONSTANTS.CPE_MISSING), smvMap.get(CONSTANTS.MATCH));
        smvDiffRow.push(validateAndFormatRecStatusNumber(smvDiffSum));

        let recordCountSum = 0;
        recordCountSum = validateAndAdd(noOfRecordsMap.get(CONSTANTS.BREAK), noOfRecordsMap.get(CONSTANTS.INTERNAL_MISSING), noOfRecordsMap.get(CONSTANTS.CPE_MISSING), noOfRecordsMap.get(CONSTANTS.MATCH));
        noOfRecordsRow.push(validateAndFormatRecStatusNumber(recordCountSum));

        let tableRows = [];
        tableRows.push(lmvDiffRow);
        tableRows.push(smvDiffRow);
        tableRows.push(noOfRecordsRow);
        reconStatusData.tableRows = tableRows;
        return reconStatusData;
    }

    function getDayOnDayChanges(agreementSummary, tables) {
        let data = {};
        if (!tables.includes(CRIMSON_TABLES.DAY_ON_DAY_CHANGES)) {
            return data;
        }
        data.tableName = CONSTANTS.DAY_ON_DAY_CHANGES;
        data.numberOfColumns = 4;
        data.headerRow = [CONSTANTS.COMPONENT, CONSTANTS.INTERNAL, CONSTANTS.MARGIN_FILE, CONSTANTS.PREV_DAY_ADJ];

        let exposureDataRow = [
            CONSTANTS.EXPOSURE,
            validateAndComputeDiff(agreementSummary.adjustedExposureRC, agreementSummary.prevDayAdjustedExposureRC, true),
            validateAndComputeDiff(agreementSummary.brokerExposureRC, agreementSummary.prevDayBrokerExposureRC, true),
            validateAndComputeDiff(agreementSummary.prevDayAdjustedExposureRC, agreementSummary.prevDayExposureRC, true)
        ]

        let requirementDataRow = [
            CONSTANTS.REQUIREMENT,
            validateAndComputeDiff(agreementSummary.adjustedRequirementRC, agreementSummary.prevDayAdjustedRequirementRC, true),
            validateAndComputeDiff(agreementSummary.brokerRequirementRC, agreementSummary.prevDayBrokerRequirementRC, true),
            validateAndComputeDiff(agreementSummary.prevDayAdjustedRequirementRC, agreementSummary.prevDayRequirementRC, true)
        ]

        let collateralDataRow = [
            CONSTANTS.COLLATERAL,
            validateAndComputeDiff(agreementSummary.adjustedCollateralRC, agreementSummary.prevDayAdjustedCollateralRC, true),
            validateAndComputeDiff(agreementSummary.brokerCollateralRC, agreementSummary.prevDayBrokerCollateralRC, true),
            validateAndComputeDiff(agreementSummary.prevDayAdjustedCollateralRC, agreementSummary.prevDayCollateralRC, true)
        ]

        let tableRows = [];
        tableRows.push(exposureDataRow);
        tableRows.push(requirementDataRow);
        tableRows.push(collateralDataRow);
        data.tableRows = tableRows;
        return data;
    }

    function getInternalVsMarginFileSegData(agreementSummary, tables) {
        let data = {};
        if (!tables.includes(CRIMSON_TABLES.INTERNAL_VS_MARGIN_FILE_SEG)) {
            return data;
        }
        data.tableName = CONSTANTS.INTERNAL_VS_MARGIN_FILE_SEG;
        data.numberOfColumns = 5;
        data.headerRow = [CONSTANTS.SOURCE, CONSTANTS.SEG_REQUIREMENT, CONSTANTS.SEG_CASH_COLLATERAL, CONSTANTS.SEG_SECURITIES_COLLATERAL, CONSTANTS.SEG_ED];

        let internalSegDataRow = [
            CONSTANTS.INTERNAL,
            validateAndFormatNumber(agreementSummary.adjustedSegMargin),
            validateAndFormatNumber(agreementSummary.adjustedSegCashCollateral),
            validateAndFormatNumber(agreementSummary.adjustedSegPhysicalCollateral),
            validateAndFormatNumber(agreementSummary.adjustedSegExcessDeficit)
        ];

        let externalSegDataRow = [
            CONSTANTS.MARGIN_FILE,
            validateAndFormatNumber(agreementSummary.brokerSegMargin),
            validateAndFormatNumber(agreementSummary.brokerSegCashCollateralRC),
            validateAndFormatNumber(agreementSummary.brokerSegPhysicalCollateralRC),
            validateAndFormatNumber(agreementSummary.brokerSegExcessDeficit)
        ];

        let segDiffDataRow = [
            CONSTANTS.DIFF,
            validateAndComputeDiff(agreementSummary.adjustedSegMargin, agreementSummary.brokerSegMargin, true),
            validateAndComputeDiff(agreementSummary.adjustedSegCashCollateral, agreementSummary.brokerSegCashCollateralRC, true),
            validateAndComputeDiff(agreementSummary.adjustedSegPhysicalCollateral, agreementSummary.brokerSegPhysicalCollateralRC, true),
            validateAndComputeDiff(agreementSummary.adjustedSegExcessDeficit, agreementSummary.brokerSegExcessDeficit, true)
        ];

        let tableRows = [];
        tableRows.push(internalSegDataRow);
        tableRows.push(externalSegDataRow);
        tableRows.push(segDiffDataRow);
        let unformattedDiffDataRow = [
            validateAndComputeDiff(agreementSummary.adjustedSegMargin, agreementSummary.brokerSegMargin, false),
            validateAndComputeDiff(agreementSummary.adjustedSegCashCollateral, agreementSummary.brokerSegCashCollateralRC, false),
            validateAndComputeDiff(agreementSummary.adjustedSegPhysicalCollateral, agreementSummary.brokerSegPhysicalCollateralRC, false)
        ]
        data.tableRows = tableRows;
        data.unformattedDiffDataRow = unformattedDiffDataRow;
        return data;
    }

    /**
     * Function to minimize the Right Agreement Detail Panel
     */
    /*
    function _collapseAgreementDetailPanel() {
        var rightSidebar = document.getElementById('rightSidebar');
        rightSidebar.style.width = '0%';
        if(rightSidebar.state == "expanded") {
            rightSidebar.toggle();
        }
    }

    function _expandAgreementDetailPanel() {
        var rightSidebar = document.getElementById('rightSidebar');
        if(rightSidebar.state == "collapsed") {
            rightSidebar.toggle();
        }
    }

    /**
     * Helper method to toggle right side bar
     *
     */
    /*
    function _toggleRightPanel(agreementSummary) {
        // Takes right side bar and toggles it
        var rightSideBar = document.getElementById('rightSidebar');
        rightSideBar.header = "Excess Deficit summary for "+ agreementSummary.legalEntityAbbrev+"-"+agreementSummary.exposureCounterPartyAbbrev+"-"+agreementSummary.agreementType;
        if(rightSideBar.state != 'expanded') {
            rightSideBar.style.width = '100%';
            rightSideBar.style.display = 'block';
            rightSideBar.toggle();
        }
    }*/

    function _toggleLeftPanel(agreementSummary) {
        // Takes right side bar and toggles it
        var agreementFilterSidebar = document.getElementById('agreementFilterSidebar');
        if (agreementFilterSidebar.state == 'expanded') {
            agreementFilterSidebar.toggle();
        }
    }

    /**
     * Helper method to assign quick links in right sidebar
     */
    function _initQuickLinksInRightPanel(agreementSummary) {

        //Assign button links in right side bar
        var detailCallReportId = document.getElementById('detailCallReportId');
        var detailQuickPublishId = document.getElementById('detailQuickPublishId');
        var postCollateralAndPublishButton = document.getElementById('postCollateralAndPublishButton');
        var detailBeginWorkflowId = document.getElementById('detailBeginWorkflowId');
        var detailDownloadBrokerFileId = document.getElementById('detailDownloadBrokerFileId');
        var downloadBrokerSelectId = document.getElementById('downloadBrokerSelectId');
        setBrokerFileDetails(agreementSummary, detailDownloadBrokerFileId,downloadBrokerSelectId);

        if (agreementSummary.workflowStatus == 'IN_PROGRESS' || (agreementSummary.workflowStatus == 'SAVED') || (agreementSummary.workflowStatus == 'PUBLISHED')) {
            detailQuickPublishId.disabled = true;
            postCollateralAndPublishButton.disabled = true;
            detailCallReportId.disabled = true;
            detailBeginWorkflowId.disabled = false;
        } else {
            detailQuickPublishId.disabled = false;
            postCollateralAndPublishButton.disabled = false;
            detailCallReportId.disabled = false;
            detailBeginWorkflowId.disabled = false;
        }

        var param = _getParam(agreementSummary);

        // Initiating Quick Publish action in right side bar
        detailQuickPublishId.onclick = function () { window.treasury.comet.agreementWorkflowAction.quickPublishAgreementWorkflow(param) };
        // Initiating Log Wire and Publish action in right side bar
        postCollateralAndPublishButton.onclick = function () {
            window.treasury.comet.agreementWiresAction.setPage("AgreementSummary");
            window.treasury.comet.agreementWiresAction.setTag(agreementSummary.tag);
            window.treasury.comet.agreementWiresAction.setAccountType(agreementSummary.accountType);
            window.treasury.comet.agreementWiresAction.setTripartyAgreementId(agreementSummary.tripartyAgreementId);
            var acmvalue = window.treasury.comet.agreementWiresAction.roundValue(Math.round(agreementSummary.ecmRC.replace(/,/g, "").replace(/x/g, "")), 100000);
            window.treasury.comet.agreementWiresAction.setACM(acmvalue);
            window.treasury.comet.agreementWiresAction.setCustodianAccountId(agreementSummary.custodianAccountId);
            window.treasury.comet.agreementWiresAction.setAgreementWorkflowParam(param);
            window.treasury.comet.agreementWiresAction.setActionableAgreementId(agreementSummary.agreementId);
            window.treasury.comet.agreementWiresAction.setReportingCurrencyIsoCode(agreementSummary.reportingCurrency);
            window.treasury.comet.agreementWiresAction.setReportingCurrencySpn(agreementSummary.reportingCurrencySpn);
            window.treasury.comet.agreementWiresAction.setReportingCurrencyFxRate(agreementSummary.reportingCurrencyFxRate);
            window.treasury.comet.agreementWiresAction.setActionableDate(agreementSummary.date);
            window.treasury.comet.agreementWiresAction.setInternalVsExternalData(_getInternalVsExternalData(agreementSummary));
            window.treasury.comet.agreementWiresAction.quickLogWire(param);
        };
        // Initiating  Call Report action in right side bar
        detailCallReportId.onclick = function () { window.treasury.comet.agreementWorkflowAction.quickGenerateCallReport(param) };
        // Initiating BeginWorkflow action in right side bar
        detailBeginWorkflowId.onclick = function () { window.treasury.comet.agreementWorkflowAction.displayAgreementWorkflow(param) };
    }

    /**
     * Generates the params need for all the quicklinks
     */
    function _getParam(agreementSummary) {

        return agreementSummary.date
            + '_'
            + agreementSummary.legalEntityId
            + '_'
            + agreementSummary.exposureCounterPartyId
            + '_'
            + agreementSummary.agreementTypeId
            + '_'
            + agreementSummary.agreementId
    }

    /**
     * Helper method to toggle all the elements of right sidebar if they are expanded
     */
    function _toggleRightSidebarPanels() {

        var segInternalExternal = document.getElementById('segInternalExternal');
        if (segInternalExternal.state == 'expanded') {
            segInternalExternal.toggleState();
        }
        var internalT1T2 = document.getElementById('internalT1T2');
        if (internalT1T2.state == 'expanded') {
            internalT1T2.toggleState();
        }
        var externalT1T2 = document.getElementById('externalT1T2');
        if (externalT1T2.state == 'expanded') {
            externalT1T2.toggleState();
        }
        var internalVsInternalAdj = document.getElementById('internalVsInternalAdj');
        if (internalVsInternalAdj.state == 'expanded') {
            internalVsInternalAdj.toggleState();
        }
        var regInternalVsExternalId = document.getElementById('regInternalVsExternalId');
        if (regInternalVsExternalId != null && regInternalVsExternalId.state == 'expanded') {
            regInternalVsExternalId.toggleState();
        }
        var houseInternalVsExternalId = document.getElementById('houseInternalVsExternalId');
        if (houseInternalVsExternalId != null && houseInternalVsExternalId.state == 'expanded') {
            houseInternalVsExternalId.toggleState();
        }
        var attributeDetail = document.getElementById('attributeDetail');
        if (attributeDetail.state == 'expanded') {
            attributeDetail.toggleState();
        }
    }

    function isCrimsonEnabledForAgmtTypeId(agreementTypeId) {
        let crimsonEnabledAgmtTypeIds = treasury.defaults.crimsonEnabledAgmtTypeIds;
        let isCrimsonEnabledForAgmtType = false;
        if (!crimsonEnabledAgmtTypeIds) {
            return isCrimsonEnabledForAgmtType;
        }
        for (let ind = 0; ind < crimsonEnabledAgmtTypeIds.length; ind++) {
            if (crimsonEnabledAgmtTypeIds[ind] == Number(agreementTypeId)) {
                isCrimsonEnabledForAgmtType = true;
            }
        }
        return isCrimsonEnabledForAgmtType;
    }

    /**
     * Populates all the panels of right sidebar
     */
    function _populateRightSideBar(agreementSummary) {

        var agreementDetail = document.getElementById('agreementDetail');
        agreementDetail.label = "Excess Deficit summary for " + agreementSummary.legalEntityAbbrev + "-" + agreementSummary.exposureCounterPartyAbbrev + "-" + agreementSummary.agreementType;

        _populateStatusData(agreementSummary);
        _populateIntVsExtData(agreementSummary);
        _populateIntT1VsT2(agreementSummary);
        _populateExtT1VsT2(agreementSummary);
        _populateIntVsIntAdjusted(agreementSummary);
        _populateSegData(agreementSummary);

        if (treasury.defaults.isCrimsonEnabled && isCrimsonEnabledForAgmtTypeId(agreementSummary.agreementTypeId)) {
            _populateCollateralMgmtVsReconTab(agreementSummary);
            _populateAttributeDetails(agreementSummary);
        } else {
            document.querySelector('arc-tab-panel').removeTab('Collateral Mgmt. vs Reconciler');
        }

        // Showing house numbers in panel if reg is applicable
        if (agreementSummary.isRegApplicable == 'true') {
            var houseInternalVsExternalId = document.getElementById("houseInternalVsExternalId");
            if (houseInternalVsExternalId != null) {
                houseInternalVsExternalId.style.display = "block";
            }
            _populateHouseIntVsExtData(agreementSummary);
        } else {
            var houseInternalVsExternalId = document.getElementById("houseInternalVsExternalId");
            if (houseInternalVsExternalId != null) {
                houseInternalVsExternalId.style.display = "none";
            }
        }
        // Showing reg numbers in panel if house is applicable
        if (agreementSummary.isRegApplicable == 'false' && agreementSummary.isRegMarginAvailable == 'true') {
            var regInternalVsExternalId = document.getElementById("regInternalVsExternalId");
            if (regInternalVsExternalId != null) {
                regInternalVsExternalId.style.display = "block";
            }
            _populateRegIntVsExtData(agreementSummary);
        } else {
            var regInternalVsExternalId = document.getElementById("regInternalVsExternalId");
            if (regInternalVsExternalId != null) {
                regInternalVsExternalId.style.display = "none";
            }
        }

        if (agreementSummary.tag != undefined && agreementSummary.tag.indexOf("SEG") == -1) {
            var segInternalExternal = document.getElementById("segInternalExternal");
            if (segInternalExternal != undefined) {
                segInternalExternal.style.display = "none";
            }
        }

        _populateAttributeDetails(agreementSummary);
    }

    /**
     * Method to populate attribute details for isda agreements
     */
    function _populateAttributeDetails(agreementSummary) {
        // Populating attribute details only in case of isda agreements
        if (agreementSummary.agreementType == 'ISDA') {

            var attributeDetail = document.getElementById("attributeDetail");
            if (attributeDetail != null) {
                attributeDetail.style.display = "block";
            }

            var attributeDetailsPopoutLink = document.getElementById("attributeDetailsPopoutLink");

            var reportingCurrency = agreementSummary.reportingCurrency;
            var reportingCurrencySpanList = document.getElementsByClassName("reportingCurrency");

            for (var i = 0; i < reportingCurrencySpanList.length; i++) {
                reportingCurrencySpanList[i].innerHTML = " (" + reportingCurrency + ")";
            }

            var legalEntityAbbrev = document.getElementById("legalEntityAbbrev");
            legalEntityAbbrev.innerHTML = agreementSummary.legalEntityAbbrev;
            var counterpartyAbbrev = document.getElementById("counterpartyAbbrev");
            counterpartyAbbrev.innerHTML = agreementSummary.exposureCounterPartyAbbrev;
            var legalEntityName = document.getElementById("legalEntityName");
            legalEntityName.innerHTML = agreementSummary.legalEntityName;
            var counterPartyName = document.getElementById("counterPartyName");
            counterPartyName.innerHTML = agreementSummary.exposureCounterPartyName;
            var internalExposure = document.getElementById("adInternalExposure");
            internalExposure.innerHTML = checkUndefinedOrEmpty(agreementSummary.adjustedExposureRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.adjustedExposureRC, null);
            var externalExposure = document.getElementById("adExternalExposure");
            externalExposure.innerHTML = checkUndefinedOrEmpty(agreementSummary.brokerExposureRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.brokerExposureRC, null);
            var internalRequirement = document.getElementById("internalIA");
            internalRequirement.innerHTML = checkUndefinedOrEmpty(agreementSummary.adjustedRequirementRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.adjustedRequirementRC, null);
            var externalRequirement = document.getElementById("externalIA");
            externalRequirement.innerHTML = checkUndefinedOrEmpty(agreementSummary.brokerRequirementRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.brokerRequirementRC, null);
            var internalCollateral = document.getElementById("adInternalCollateral");
            internalCollateral.innerHTML = checkUndefinedOrEmpty(agreementSummary.adjustedCollateralRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.adjustedCollateralRC, null);
            var externalCollateral = document.getElementById("adExternalCollateral");
            externalCollateral.innerHTML = checkUndefinedOrEmpty(agreementSummary.brokerCollateralRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.brokerCollateralRC, null);
            var internalPostedCollateral = document.getElementById("internalPostedCollateral");
            internalPostedCollateral.innerHTML = checkUndefinedOrEmpty(agreementSummary.adjustedCollateralRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.adjustedCollateralRC, null);
            var externalPostedCollateral = document.getElementById("externalPostedCollateral");
            externalPostedCollateral.innerHTML = checkUndefinedOrEmpty(agreementSummary.brokerCollateralRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.brokerCollateralRC, null);
            var internalCreditSupport = document.getElementById("internalCreditSupport");
            internalCreditSupport.innerHTML = checkUndefinedOrEmpty(agreementSummary.seCreditSupportAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.seCreditSupportAmount, null);
            var externalCreditSupport = document.getElementById("externalCreditSupport");
            externalCreditSupport.innerHTML = checkUndefinedOrEmpty(agreementSummary.cpeCreditSupportAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.cpeCreditSupportAmount, null);;
            var seCategory = document.getElementById("seCategory");
            seCategory.innerHTML = checkUndefinedOrEmpty(agreementSummary.seCategory) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : agreementSummary.seCategory;
            var cpeCategory = document.getElementById("cpeCategory");
            cpeCategory.innerHTML = checkUndefinedOrEmpty(agreementSummary.cpeCategory) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : agreementSummary.cpeCategory;

            var internalThreshold = document.getElementById("internalThresholdAmount");
            internalThreshold.innerHTML = checkUndefinedOrEmpty(agreementSummary.legalEntityThreshold) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.legalEntityThreshold, null);;
            var externalThreshold = document.getElementById("externalThresholdAmount");
            externalThreshold.innerHTML = checkUndefinedOrEmpty(agreementSummary.cpeThreshold) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.cpeThreshold, null);;
            var internalTransferAmt = document.getElementById("internalMinimumTransferAmount");
            internalTransferAmt.innerHTML = checkUndefinedOrEmpty(agreementSummary.legalEntityMinTransferAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.legalEntityMinTransferAmount, null);;
            var externalTransferAmt = document.getElementById("externalMinimumTransferAmount");
            externalTransferAmt.innerHTML = checkUndefinedOrEmpty(agreementSummary.cpeMinTransferAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.cpeMinTransferAmount, null);;
            var internalRoundingAmount = document.getElementById("internalRoundingAmount");
            internalRoundingAmount.innerHTML = checkUndefinedOrEmpty(agreementSummary.legalEntityRoundingAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.legalEntityRoundingAmount, null);;
            var externalRoundingAmount = document.getElementById("externalRoundingAmount");
            externalRoundingAmount.innerHTML = checkUndefinedOrEmpty(agreementSummary.cpeRoundingAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.cpeRoundingAmount, null);;
            var roundingConvention = document.getElementById("roundingConvention");
            roundingConvention.innerHTML = checkUndefinedOrEmpty(agreementSummary.roundingConvention) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : agreementSummary.roundingConvention;



            var applicableDeliveryAmount = document.getElementById("applicableDeliveryAmount");
            applicableDeliveryAmount.innerHTML = checkUndefinedOrEmpty(agreementSummary.applicableDeliveryAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.applicableDeliveryAmount);;;
            var applicableReturnAmount = document.getElementById("applicableReturnAmount");
            applicableReturnAmount.innerHTML = checkUndefinedOrEmpty(agreementSummary.deliveryAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.deliveryAmount, null);;;
            var returnAmount = document.getElementById("applicableReturnAmount");
            returnAmount.innerHTML = checkUndefinedOrEmpty(agreementSummary.returnAmount) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.returnAmount, null);;;
            var internalRawExcessDeficit = document.getElementById("rawInternalED");
            internalRawExcessDeficit.innerHTML = checkUndefinedOrEmpty(agreementSummary.adjustedExcessDeficitRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.adjustedExcessDeficitRC, null);;;
            var externalRawExcessDeficit = document.getElementById("rawExternalED");
            externalRawExcessDeficit.innerHTML = checkUndefinedOrEmpty(agreementSummary.brokerExcessDeficitRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.brokerExcessDeficitRC, null);;;
            var internalExcessDeficit = document.getElementById("adInternalED");
            internalExcessDeficit.innerHTML = checkUndefinedOrEmpty(agreementSummary.adjustedExcessDeficitRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.adjustedExcessDeficitRC, null);;;
            var externalExcessDeficit = document.getElementById("adExternalED");
            externalExcessDeficit.innerHTML = checkUndefinedOrEmpty(agreementSummary.brokerExcessDeficitRC) ? '<div style=\"text-align: right;\"><span >n/a</span></div>' : treasury.formatters.number(null, null, agreementSummary.brokerExcessDeficitRC, null);;;

        } else {
            var attributeDetailsPopoutLink = document.getElementById("attributeDetailsPopoutLink");
            if (attributeDetailsPopoutLink != null) {
                attributeDetailsPopoutLink.style.display = "none";
            }
        }
    }

    function _launchAttributeDetails() {

        if (!window.CustomShadowDOMPolyfill) {
            event.preventDefault();
        }

        document.querySelector('#attributeDetail')
            .reveal({
                'title': 'Attribute Details',
                'modal': true,
                'buttons': [{
                    'html': 'Close',
                    'callback': function () {
                        _closeAttrbuteDetailsPopup();
                    },
                    'position': 'center'
                }]
            });
    }

    /**
     * On Canceling the popup, the workflow is cancelled and contents of the wire popup are cleared
     */
    function _closeAttrbuteDetailsPopup() {
        document.querySelector('#attributeDetail').visible = false;
    }

    function _populateHouseIntVsExtData(agreementSummary) {
        // Populating internal data
        var houseInternalExposure = document.getElementById('houseInternalExposure');
        houseInternalExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedHouseExposureRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedHouseExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseInternalCashCollateral = document.getElementById('houseInternalCashCollateral');
        houseInternalCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedHouseCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedHouseCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseInternalSecuritiesCollateral = document.getElementById('houseInternalSecuritiesCollateral');
        houseInternalSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedHousePhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedHousePhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseInternalRequirement = document.getElementById('houseInternalRequirement');
        houseInternalRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedHouseMarginRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedHouseMarginRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseInternalED = document.getElementById('houseInternalED');
        houseInternalED.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedHouseExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedHouseExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populating external data
        var houseExternalExposure = document.getElementById('houseExternalExposure');
        houseExternalExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerHouseExposureRC) ? treasury.formatters.number(null, null, agreementSummary.brokerHouseExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseExternalCashCollateral = document.getElementById('houseExternalCashCollateral');
        houseExternalCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerHouseCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.brokerHouseCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseExternalSecuritiesCollateral = document.getElementById('houseExternalSecuritiesCollateral');
        houseExternalSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerHousePhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.brokerHousePhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseExternalRequirement = document.getElementById('houseExternalRequirement');
        houseExternalRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerHouseRequirementRC) ? treasury.formatters.number(null, null, agreementSummary.brokerHouseRequirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseExternalED = document.getElementById('houseExternalED');
        houseExternalED.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerHouseExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.brokerHouseExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populating difference
        var houseDiffExposure = document.getElementById('houseDiffExposure');
        houseDiffExposure.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedHouseExposureRC) && !checkUndefinedOrEmpty(agreementSummary.brokerHouseExposureRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedHouseExposureRC - agreementSummary.brokerHouseExposureRC) && !isNaN(agreementSummary.adjustedHouseExposureRC - agreementSummary.brokerHouseExposureRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedHouseExposureRC - agreementSummary.brokerHouseExposureRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseDiffCashCollateral = document.getElementById('houseDiffCashCollateral');
        houseDiffCashCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedHouseCashCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.brokerHouseCashCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedHouseCashCollateralRC - agreementSummary.brokerHouseCashCollateralRC) && !isNaN(agreementSummary.adjustedHouseCashCollateralRC - agreementSummary.brokerHouseCashCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedHouseCashCollateralRC - agreementSummary.brokerHouseCashCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseDiffSecuritiesCollateral = document.getElementById('houseDiffSecuritiesCollateral');
        houseDiffSecuritiesCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedHousePhysicalCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.brokerHousePhysicalCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedHousePhysicalCollateralRC - agreementSummary.brokerHousePhysicalCollateralRC) && !isNaN(agreementSummary.adjustedHousePhysicalCollateralRC - agreementSummary.brokerHousePhysicalCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedHousePhysicalCollateralRC - agreementSummary.brokerHousePhysicalCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseDiffRequirement = document.getElementById('houseDiffRequirement');
        houseDiffRequirement.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedHouseMarginRC) && !checkUndefinedOrEmpty(agreementSummary.brokerHouseRequirementRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedHouseMarginRC - agreementSummary.brokerHouseRequirementRC) && !isNaN(agreementSummary.adjustedHouseMarginRC - agreementSummary.brokerHouseRequirementRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedHouseMarginRC - agreementSummary.brokerHouseRequirementRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var houseDiffED = document.getElementById('houseDiffED');
        houseDiffED.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedHouseExcessDeficitRC) && !checkUndefinedOrEmpty(agreementSummary.brokerHouseExcessDeficitRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedHouseExcessDeficitRC - agreementSummary.brokerHouseExcessDeficitRC) && !isNaN(agreementSummary.adjustedHouseExcessDeficitRC - agreementSummary.brokerHouseExcessDeficitRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedHouseExcessDeficitRC - agreementSummary.brokerHouseExcessDeficitRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

    }

    function _populateRegIntVsExtData(agreementSummary) {
        // Populating internal data
        var regInternalExposure = document.getElementById('regInternalExposure');
        regInternalExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedRegExposureRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedRegExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regInternalCashCollateral = document.getElementById('regInternalCashCollateral');
        regInternalCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedRegCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedRegCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regInternalSecuritiesCollateral = document.getElementById('regInternalSecuritiesCollateral');
        regInternalSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedRegPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedRegPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regInternalRequirement = document.getElementById('regInternalRequirement');
        regInternalRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedRegMarginRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedRegMarginRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regInternalED = document.getElementById('regInternalED');
        regInternalED.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedRegExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedRegExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populating external data
        var regExternalExposure = document.getElementById('regExternalExposure');
        regExternalExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerRegExposureRC) ? treasury.formatters.number(null, null, agreementSummary.brokerRegExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regExternalCashCollateral = document.getElementById('regExternalCashCollateral');
        regExternalCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerRegCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.brokerRegCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regExternalSecuritiesCollateral = document.getElementById('regExternalSecuritiesCollateral');
        regExternalSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerRegPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.brokerRegPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regExternalRequirement = document.getElementById('regExternalRequirement');
        regExternalRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerRegRequirementRC) ? treasury.formatters.number(null, null, agreementSummary.brokerRegRequirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regExternalED = document.getElementById('regExternalED');
        regExternalED.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerRegExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.brokerRegExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populating difference
        var regDiffExposure = document.getElementById('regDiffExposure');
        regDiffExposure.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedRegExposureRC) && !checkUndefinedOrEmpty(agreementSummary.brokerRegExposureRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedRegExposureRC - agreementSummary.brokerRegExposureRC) && !isNaN(agreementSummary.adjustedRegExposureRC - agreementSummary.brokerRegExposureRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedRegExposureRC - agreementSummary.brokerRegExposureRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regDiffCashCollateral = document.getElementById('regDiffCashCollateral');
        regDiffCashCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedRegCashCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.brokerRegCashCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedRegCashCollateralRC - agreementSummary.brokerRegCashCollateralRC) && !isNaN(agreementSummary.adjustedRegCashCollateralRC - agreementSummary.brokerRegCashCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedRegCashCollateralRC - agreementSummary.brokerRegCashCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regDiffSecuritiesCollateral = document.getElementById('regDiffSecuritiesCollateral');
        regDiffSecuritiesCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedRegPhysicalCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.brokerRegPhysicalCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedRegPhysicalCollateralRC - agreementSummary.brokerRegPhysicalCollateralRC) && !isNaN(agreementSummary.adjustedRegPhysicalCollateralRC - agreementSummary.brokerRegPhysicalCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedRegPhysicalCollateralRC - agreementSummary.brokerRegPhysicalCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regDiffRequirement = document.getElementById('regDiffRequirement');
        regDiffRequirement.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedRegMarginRC) && !checkUndefinedOrEmpty(agreementSummary.brokerRegRequirementRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedRegMarginRC - agreementSummary.brokerRegRequirementRC) && !isNaN(agreementSummary.adjustedRegMarginRC - agreementSummary.brokerRegRequirementRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedRegMarginRC - agreementSummary.brokerRegRequirementRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var regDiffED = document.getElementById('regDiffED');
        regDiffED.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedRegExcessDeficitRC) && !checkUndefinedOrEmpty(agreementSummary.brokerRegExcessDeficitRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedRegExcessDeficitRC - agreementSummary.brokerRegExcessDeficitRC) && !isNaN(agreementSummary.adjustedRegExcessDeficitRC - agreementSummary.brokerRegExcessDeficitRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedRegExcessDeficitRC - agreementSummary.brokerRegExcessDeficitRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

    }

    function _populateStatusData(agreementSummary) {
        var agreementRagStatusMsg = document.getElementById("agreementRagStatusMsg");
        var agreementStatusMsg = document.getElementById("agreementStatusMsg");
        if (agreementSummary.ragStatusDisplayText != undefined) {
            agreementRagStatusMsg.hidden = false;
            agreementStatusMsg.innerText = agreementSummary.ragStatusDisplayText;
            if (agreementSummary.ragStatusMsg) {
                agreementStatusMsg.innerText += ' | ' + agreementSummary.ragStatusMsg;
            }
        } else {
            agreementRagStatusMsg.hidden = true;
            var agreementStatusMsgDiv = document.getElementById("agreementRagStatusMsg");
            if (agreementStatusMsgDiv) {
                agreementStatusMsgDiv.innerHtml = "";
            }
        }
    }

    /**
     * Populates internal vs external data
     */
    function _populateIntVsExtData(agreementSummary) {

        let intVsExtData = _getInternalVsExternalData(agreementSummary);

        // Populating internal data
        var internalCashCollateral = document.getElementById('internalCashCollateral');
        internalCashCollateral.innerHTML = validateAndGetDivElement(intVsExtData.internalCashCollateral);
        var internalSecuritiesCollateral = document.getElementById('internalSecuritiesCollateral');
        internalSecuritiesCollateral.innerHTML = validateAndGetDivElement(intVsExtData.internalSecuritiesCollateral);
        var internalExposure = document.getElementById('internalExposure');
        internalExposure.innerHTML = validateAndGetDivElement(intVsExtData.internalExposure);
        var internalRequirement = document.getElementById('internalRequirement');
        internalRequirement.innerHTML = validateAndGetDivElement(intVsExtData.internalRequirement);
        var internalED = document.getElementById('internalED');
        internalED.innerHTML = validateAndGetDivElement(intVsExtData.internalED);

        // Populating external data
        var externalCashCollateral = document.getElementById('externalCashCollateral');
        externalCashCollateral.innerHTML = validateAndGetDivElement(intVsExtData.externalCashCollateral);
        var externalSecuritiesCollateral = document.getElementById('externalSecuritiesCollateral');
        externalSecuritiesCollateral.innerHTML = validateAndGetDivElement(intVsExtData.externalSecuritiesCollateral);
        var externalExposure = document.getElementById('externalExposure');
        externalExposure.innerHTML = validateAndGetDivElement(intVsExtData.externalExposure);
        var externalRequirement = document.getElementById('externalRequirement');
        externalRequirement.innerHTML = validateAndGetDivElement(intVsExtData.externalRequirement);
        var externalED = document.getElementById('externalED');
        externalED.innerHTML = validateAndGetDivElement(intVsExtData.externalED);

        // Populating difference
        var diffCashCollateral = document.getElementById('diffCashCollateral');
        diffCashCollateral.innerHTML = validateAndGetDivElement(intVsExtData.diffCashCollateral);
        var diffSecuritiesCollateral = document.getElementById('diffSecuritiesCollateral');
        diffSecuritiesCollateral.innerHTML = validateAndGetDivElement(intVsExtData.diffSecuritiesCollateral);
        var diffExposure = document.getElementById('diffExposure');
        diffExposure.innerHTML = validateAndGetDivElement(intVsExtData.diffExposure);
        var diffRequirement = document.getElementById('diffRequirement');
        diffRequirement.innerHTML = validateAndGetDivElement(intVsExtData.diffRequirement);
        var diffED = document.getElementById('diffED');
        diffED.innerHTML = validateAndGetDivElement(intVsExtData.diffED);
    }

    /**
     * Populate T-1,T-2 internal data
     */
    function _populateIntT1VsT2(agreementSummary) {

        // Populate T-1 internal data
        var todayInternalExposure = document.getElementById('todayInternalExposure');
        todayInternalExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedExposureRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var todayInternalCashCollateral = document.getElementById('todayInternalCashCollateral');
        todayInternalCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var todayInternalSecuritiesCollateral = document.getElementById('todayInternalSecuritiesCollateral');
        todayInternalSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var todayInternalRequirement = document.getElementById('todayInternalRequirement');
        todayInternalRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedRequirementRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedRequirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var todayInternalExcess = document.getElementById('todayInternalExcess');
        todayInternalExcess.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populate T-2 internal data
        var yesterdayInternalExposure = document.getElementById('yesterdayInternalExposure');
        yesterdayInternalExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedExposureRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayAdjustedExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var yesterdayInternalCashCollateral = document.getElementById('yesterdayInternalCashCollateral');
        yesterdayInternalCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayAdjustedCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var yesterdayInternalSecuritiesCollateral = document.getElementById('yesterdayInternalSecuritiesCollateral');
        yesterdayInternalSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayAdjustedPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var yesterdayInternalRequirement = document.getElementById('yesterdayInternalRequirement');
        yesterdayInternalRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedRequirementRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayAdjustedRequirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var yesterdayInternalExcess = document.getElementById('yesterdayInternalExcess');
        yesterdayInternalExcess.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayAdjustedExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populate difference between T-1 and T-2
        var internalDiffExposure = document.getElementById('internalDiffExposure');
        internalDiffExposure.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedExposureRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedExposureRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedExposureRC - agreementSummary.prevDayAdjustedExposureRC) && !isNaN(agreementSummary.adjustedExposureRC - agreementSummary.prevDayAdjustedExposureRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedExposureRC - agreementSummary.prevDayAdjustedExposureRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var internalDiffCashCollateral = document.getElementById('internalDiffCashCollateral');
        internalDiffCashCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedCashCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedCashCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedCashCollateralRC - agreementSummary.prevDayAdjustedCashCollateralRC) && !isNaN(agreementSummary.adjustedCashCollateralRC - agreementSummary.prevDayAdjustedCashCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedCashCollateralRC - agreementSummary.prevDayAdjustedCashCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var internalDiffSecuritiesCollateral = document.getElementById('internalDiffSecuritiesCollateral');
        internalDiffSecuritiesCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedPhysicalCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedPhysicalCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedPhysicalCollateralRC - agreementSummary.prevDayAdjustedPhysicalCollateralRC) && !isNaN(agreementSummary.adjustedPhysicalCollateralRC - agreementSummary.prevDayAdjustedPhysicalCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedPhysicalCollateralRC - agreementSummary.prevDayAdjustedPhysicalCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var internalDiffRequirement = document.getElementById('internalDiffRequirement');
        internalDiffRequirement.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedRequirementRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedRequirementRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedRequirementRC - agreementSummary.prevDayAdjustedRequirementRC) && !isNaN(agreementSummary.adjustedRequirementRC - agreementSummary.prevDayAdjustedRequirementRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedRequirementRC - agreementSummary.prevDayAdjustedRequirementRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var internalDiffExcess = document.getElementById('internalDiffExcess');
        internalDiffExcess.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedExcessDeficitRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayAdjustedExcessDeficitRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedExcessDeficitRC - agreementSummary.prevDayAdjustedExcessDeficitRC) && !isNaN(agreementSummary.adjustedExcessDeficitRC - agreementSummary.prevDayAdjustedExcessDeficitRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedExcessDeficitRC - agreementSummary.prevDayAdjustedExcessDeficitRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
    }

    /**
     * Helper method to populate T-1, T-2 external data
     */
    function _populateExtT1VsT2(agreementSummary) {

        // Populate external data T-1 data
        var todayExternalExposure = document.getElementById('todayExternalExposure');
        todayExternalExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerExposureRC) ? treasury.formatters.number(null, null, agreementSummary.brokerExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var todayExternalCashCollateral = document.getElementById('todayExternalCashCollateral');
        todayExternalCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.brokerCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var todayExternalSecuritiesCollateral = document.getElementById('todayExternalSecuritiesCollateral');
        todayExternalSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.brokerPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var todayExternalRequirement = document.getElementById('todayExternalRequirement');
        todayExternalRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerRequirementRC) ? treasury.formatters.number(null, null, agreementSummary.brokerRequirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var todayExternalExcess = document.getElementById('todayExternalExcess');
        todayExternalExcess.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.brokerExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populate external data T-2 data
        var yesterdayExternalExposure = document.getElementById('yesterdayExternalExposure');
        yesterdayExternalExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerExposureRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayBrokerExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var yesterdayExternalCashCollateral = document.getElementById('yesterdayExternalCashCollateral');
        yesterdayExternalCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerCashCollateral) ? treasury.formatters.number(null, null, agreementSummary.prevDayBrokerCashCollateral, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var yesterdayExternalSecuritiesCollateral = document.getElementById('yesterdayExternalSecuritiesCollateral');
        yesterdayExternalSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayBrokerPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var yesterdayExternalRequirement = document.getElementById('yesterdayExternalRequirement');
        yesterdayExternalRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerRequirementRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayBrokerRequirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var yesterdayExternalExcess = document.getElementById('yesterdayExternalExcess');
        yesterdayExternalExcess.innerHTML = !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.prevDayBrokerExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populate external difference between T-1 and T-2
        var externalDiffExposure = document.getElementById('externalDiffExposure');
        externalDiffExposure.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.brokerExposureRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerExposureRC) &&
            !checkUndefinedOrEmpty(agreementSummary.brokerExposureRC - agreementSummary.prevDayBrokerExposureRC) && !isNaN(agreementSummary.brokerExposureRC - agreementSummary.prevDayBrokerExposureRC)) ? treasury.formatters.number(null, null, (agreementSummary.brokerExposureRC - agreementSummary.prevDayBrokerExposureRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var externalDiffCashCollateral = document.getElementById('externalDiffCashCollateral');
        externalDiffCashCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.brokerCashCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerCashCollateral) &&
            !checkUndefinedOrEmpty(agreementSummary.brokerCashCollateralRC - agreementSummary.prevDayBrokerCashCollateral) && !isNaN(agreementSummary.brokerCashCollateralRC - agreementSummary.prevDayBrokerCashCollateral)) ? treasury.formatters.number(null, null, (agreementSummary.brokerCashCollateralRC - agreementSummary.prevDayBrokerCashCollateral), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var externalDiffSecuritiesCollateral = document.getElementById('externalDiffSecuritiesCollateral');
        externalDiffSecuritiesCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.brokerPhysicalCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerPhysicalCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.brokerPhysicalCollateralRC - agreementSummary.prevDayBrokerPhysicalCollateralRC) && !isNaN(agreementSummary.brokerPhysicalCollateralRC - agreementSummary.prevDayBrokerPhysicalCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.brokerPhysicalCollateralRC - agreementSummary.prevDayBrokerPhysicalCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var externalDiffRequirement = document.getElementById('externalDiffRequirement');
        externalDiffRequirement.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.brokerRequirementRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerRequirementRC) &&
            !checkUndefinedOrEmpty(agreementSummary.brokerRequirementRC - agreementSummary.prevDayBrokerRequirementRC) && !isNaN(agreementSummary.brokerRequirementRC - agreementSummary.prevDayBrokerRequirementRC)) ? treasury.formatters.number(null, null, (agreementSummary.brokerRequirementRC - agreementSummary.prevDayBrokerRequirementRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var externalDiffExcess = document.getElementById('externalDiffExcess');
        externalDiffExcess.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.brokerExcessDeficitRC) && !checkUndefinedOrEmpty(agreementSummary.prevDayBrokerExcessDeficitRC) &&
            !checkUndefinedOrEmpty(agreementSummary.brokerExcessDeficitRC - agreementSummary.prevDayBrokerExcessDeficitRC) && !isNaN(agreementSummary.brokerExcessDeficitRC - agreementSummary.prevDayBrokerExcessDeficitRC)) ? treasury.formatters.number(null, null, (agreementSummary.brokerExcessDeficitRC - agreementSummary.prevDayBrokerExcessDeficitRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
    }

    /**
     * Helper method to poulate internal and internal adjusted data
     */
    function _populateIntVsIntAdjusted(agreementSummary) {

        // Populate internal adjusted data
        var adjustedExposure = document.getElementById('adjustedExposure');
        adjustedExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedExposureRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedExposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var adjustedCashCollateral = document.getElementById('adjustedCashCollateral');
        adjustedCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var adjustedSecuritiesCollateral = document.getElementById('adjustedSecuritiesCollateral');
        adjustedSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var adjustedRequirement = document.getElementById('adjustedRequirement');
        adjustedRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedRequirementRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedRequirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var adjustedExcess = document.getElementById('adjustedExcess');
        adjustedExcess.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populate internal data
        var unadjustedExposure = document.getElementById('unadjustedExposure');
        unadjustedExposure.innerHTML = !checkUndefinedOrEmpty(agreementSummary.exposureRC) ? treasury.formatters.number(null, null, agreementSummary.exposureRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var unadjustedCashCollateral = document.getElementById('unadjustedCashCollateral');
        unadjustedCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.cashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.cashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var unadjustedSecuritiesCollateral = document.getElementById('unadjustedSecuritiesCollateral');
        unadjustedSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.physicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.physicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var unadjustedRequirement = document.getElementById('unadjustedRequirement');
        unadjustedRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.requirementRC) ? treasury.formatters.number(null, null, agreementSummary.requirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var unadjustedExcess = document.getElementById('unadjustedExcess');
        unadjustedExcess.innerHTML = !checkUndefinedOrEmpty(agreementSummary.excessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.excessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

        // Populate difference between internal and internal adjusted data
        var adjustedDiffExposure = document.getElementById('adjustedDiffExposure');
        adjustedDiffExposure.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedExposureRC) && !checkUndefinedOrEmpty(agreementSummary.exposureRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedExposureRC - agreementSummary.exposureRC) && !isNaN(agreementSummary.adjustedExposureRC - agreementSummary.exposureRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedExposureRC - agreementSummary.exposureRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var adjustedDiffCashCollateral = document.getElementById('adjustedDiffCashCollateral');
        adjustedDiffCashCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedCashCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.cashCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedCashCollateralRC - agreementSummary.cashCollateralRC) && !isNaN(agreementSummary.adjustedCashCollateralRC - agreementSummary.cashCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedCashCollateralRC - agreementSummary.cashCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var adjustedDiffSecuritiesCollateral = document.getElementById('adjustedDiffSecuritiesCollateral');
        adjustedDiffSecuritiesCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedPhysicalCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.physicalCollateralRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedPhysicalCollateralRC - agreementSummary.physicalCollateralRC) && !isNaN(agreementSummary.adjustedPhysicalCollateralRC - agreementSummary.physicalCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedPhysicalCollateralRC - agreementSummary.physicalCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var adjustedDiffRequirement = document.getElementById('adjustedDiffRequirement');
        adjustedDiffRequirement.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedRequirementRC) && !checkUndefinedOrEmpty(agreementSummary.requirementRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedRequirementRC - agreementSummary.requirementRC) && !isNaN(agreementSummary.adjustedRequirementRC - agreementSummary.requirementRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedRequirementRC - agreementSummary.requirementRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
        var adjustedDiffExcess = document.getElementById('adjustedDiffExcess');
        adjustedDiffExcess.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedExcessDeficitRC) && !checkUndefinedOrEmpty(agreementSummary.excessDeficitRC) &&
            !checkUndefinedOrEmpty(agreementSummary.adjustedExcessDeficitRC - agreementSummary.excessDeficitRC) && !isNaN(agreementSummary.adjustedExcessDeficitRC - agreementSummary.excessDeficitRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedExcessDeficitRC - agreementSummary.excessDeficitRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
    }

    /**
     * Helper method to populate seg columns
     */
    function _populateSegData(agreementSummary) {

        if (agreementSummary != undefined && agreementSummary.childExcessDeficitList != undefined && agreementSummary.childExcessDeficitList.length) {

            var callTypeDetails = document.getElementById('callTypeDetails');
            if (callTypeDetails != undefined) {
                for (var i = 0; i < agreementSummary.childExcessDeficitList.length; i++) {

                    var segIntVsExtTemp = document.querySelector("#segIntVsExtTemp");

                    var segIntVsExtTemplate = document.importNode(segIntVsExtTemp.content, true);

                    var segChildData = agreementSummary.childExcessDeficitList[i];

                    // Populate seg columns
                    var internalSegRequirement = segIntVsExtTemplate.getElementById('internalSegRequirement');
                    internalSegRequirement.innerHTML = !checkUndefinedOrEmpty(segChildData.adjustedRequirementRC) ? treasury.formatters.number(null, null, segChildData.adjustedRequirementRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                    var internalSegCashCollateral = segIntVsExtTemplate.getElementById('internalSegCashCollateral');
                    internalSegCashCollateral.innerHTML = !checkUndefinedOrEmpty(segChildData.adjustedSegCashCollateralRC) ? treasury.formatters.number(null, null, segChildData.adjustedSegCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                    var internalSegSecuritiesCollateral = segIntVsExtTemplate.getElementById('internalSegSecuritiesCollateral');
                    internalSegSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(segChildData.adjustedSegPhysicalCollateralRC) ? treasury.formatters.number(null, null, segChildData.adjustedSegPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                    var internalSegED = segIntVsExtTemplate.getElementById('internalSegED');
                    internalSegED.innerHTML = !checkUndefinedOrEmpty(segChildData.adjustedExcessDeficitRC) ? treasury.formatters.number(null, null, segChildData.adjustedExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

                    // Populate external seg columns
                    var internalSegRequirement = segIntVsExtTemplate.getElementById('externalSegRequirement');
                    internalSegRequirement.innerHTML = !checkUndefinedOrEmpty(segChildData.brokerSegMarginRC) ? treasury.formatters.number(null, null, segChildData.brokerSegMarginRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                    var externalSegCashCollateral = segIntVsExtTemplate.getElementById('externalSegCashCollateral');
                    externalSegCashCollateral.innerHTML = !checkUndefinedOrEmpty(segChildData.brokerSegCashCollateralRC) ? treasury.formatters.number(null, null, segChildData.brokerSegCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                    var externalSegSecuritiesCollateral = segIntVsExtTemplate.getElementById('externalSegSecuritiesCollateral');
                    externalSegSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(segChildData.brokerSegPhysicalCollateralRC) ? treasury.formatters.number(null, null, segChildData.brokerSegPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                    var internalSegED = segIntVsExtTemplate.getElementById('externalSegED');
                    internalSegED.innerHTML = !checkUndefinedOrEmpty(segChildData.brokerSegExcessDeficitRC) ? treasury.formatters.number(null, null, segChildData.brokerSegExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

                    var diffRequirement = segIntVsExtTemplate.getElementById('diffSegRequirement');
                    var defaultField = '<div style=\"text-align: right;\"><span >n/a</span></div>'
                    var requirementDiff = defaultField;
                    if (!checkUndefinedOrEmpty(segChildData.adjustedSegMarginRC)) {
                        requirementDiff = segChildData.adjustedSegMarginRC;
                    }

                    if (!checkUndefinedOrEmpty(segChildData.brokerSegMarginRC)) {
                        if(requirementDiff==defaultField) requirementDiff = 0;
                        requirementDiff -= segChildData.brokerSegMarginRC;
                    }
                    if (requirementDiff != '<div style=\"text-align: right;\"><span >n/a</span></div>') {
                        requirementDiff = treasury.formatters.number(null, null, requirementDiff, null);
                    }
                    diffRequirement.innerHTML = requirementDiff;

                    var diffSegCashCollateral = segIntVsExtTemplate.getElementById('diffSegCashCollateral');
                    var cashCollateralDiff = defaultField;
                    if (!checkUndefinedOrEmpty(segChildData.adjustedSegCashCollateralRC)) {
                        cashCollateralDiff = segChildData.adjustedSegCashCollateralRC;
                    }

                    if (!checkUndefinedOrEmpty(segChildData.brokerSegCashCollateralRC)) {
                        if(cashCollateralDiff==defaultField) cashCollateralDiff = 0;
                        cashCollateralDiff -= segChildData.brokerSegCashCollateralRC;
                    }
                    if (cashCollateralDiff != '<div style=\"text-align: right;\"><span >n/a</span></div>') {
                        cashCollateralDiff = treasury.formatters.number(null, null, cashCollateralDiff, null);
                    }
                    diffSegCashCollateral.innerHTML = cashCollateralDiff;

                    var diffSegSecuritiesCollateral = segIntVsExtTemplate.getElementById('diffSegSecuritiesCollateral');
                    var securitiesCollateralDiff = defaultField;
                    if (!checkUndefinedOrEmpty(segChildData.adjustedSegPhysicalCollateralRC)) {
                        securitiesCollateralDiff = segChildData.adjustedSegPhysicalCollateralRC;
                    }

                    if (!checkUndefinedOrEmpty(segChildData.brokerSegPhysicalCollateralRC)) {
                        if(securitiesCollateralDiff==defaultField) securitiesCollateralDiff = 0;
                        securitiesCollateralDiff -= segChildData.brokerSegPhysicalCollateralRC;
                    }
                    if (securitiesCollateralDiff != '<div style=\"text-align: right;\"><span >n/a</span></div>') {
                        securitiesCollateralDiff = treasury.formatters.number(null, null, securitiesCollateralDiff, null);
                    }
                    diffSegSecuritiesCollateral.innerHTML = securitiesCollateralDiff;

                    var diffED = segIntVsExtTemplate.getElementById('diffSegED');
                    var excessDeficitDiff = defaultField;
                    if (!checkUndefinedOrEmpty(segChildData.adjustedSegExcessDeficitRC)) {
                        excessDeficitDiff = segChildData.adjustedSegExcessDeficitRC;
                    }

                    if (!checkUndefinedOrEmpty(segChildData.brokerSegExcessDeficitRC)) {
                        if(excessDeficitDiff==defaultField) excessDeficitDiff = 0;
                        excessDeficitDiff -= segChildData.brokerSegExcessDeficitRC;
                    }
                    if (excessDeficitDiff != '<div style=\"text-align: right;\"><span >n/a</span></div>') {
                        excessDeficitDiff = treasury.formatters.number(null, null, excessDeficitDiff, null);
                    }
                    diffED.innerHTML = excessDeficitDiff;

                    callTypeDetails.appendChild(segIntVsExtTemplate);
                }
            }
        } else {

            var callTypeDetails = document.getElementById('callTypeDetails');
            if (callTypeDetails != undefined) {

                var segIntVsExtTemp = document.querySelector("#segIntVsExtTemp");

                var segIntVsExtTemplate = document.importNode(segIntVsExtTemp.content, true);

                // Populate seg columns
                var internalSegRequirement = segIntVsExtTemplate.getElementById('internalSegRequirement');
                internalSegRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedSegMarginRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedSegMarginRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var internalSegCashCollateral = segIntVsExtTemplate.getElementById('internalSegCashCollateral');
                internalSegCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedSegCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedSegCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var internalSegSecuritiesCollateral = segIntVsExtTemplate.getElementById('internalSegSecuritiesCollateral');
                internalSegSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedSegPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedSegPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var internalSegED = segIntVsExtTemplate.getElementById('internalSegED');
                internalSegED.innerHTML = !checkUndefinedOrEmpty(agreementSummary.adjustedSegExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.adjustedSegExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

                // Populate external seg columns
                var externalSegRequirement = segIntVsExtTemplate.getElementById('externalSegRequirement');
                externalSegRequirement.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerSegMarginRC) ? treasury.formatters.number(null, null, agreementSummary.brokerSegMarginRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var externalSegCashCollateral = segIntVsExtTemplate.getElementById('externalSegCashCollateral');
                externalSegCashCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerSegCashCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.brokerSegCashCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var externalSegSecuritiesCollateral = segIntVsExtTemplate.getElementById('externalSegSecuritiesCollateral');
                externalSegSecuritiesCollateral.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerSegPhysicalCollateralRC) ? treasury.formatters.number(null, null, agreementSummary.brokerSegPhysicalCollateralRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var externalSegED = segIntVsExtTemplate.getElementById('externalSegED');
                externalSegED.innerHTML = !checkUndefinedOrEmpty(agreementSummary.brokerSegExcessDeficitRC) ? treasury.formatters.number(null, null, agreementSummary.brokerSegExcessDeficitRC, null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

                var diffRequirement = segIntVsExtTemplate.getElementById('diffSegRequirement');
                diffRequirement.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedSegMarginRC) && !checkUndefinedOrEmpty(agreementSummary.brokerSegMarginRC) &&
                    !checkUndefinedOrEmpty(agreementSummary.adjustedSegMarginRC - agreementSummary.brokerSegMarginRC) && !isNaN(agreementSummary.adjustedSegMarginRC - agreementSummary.brokerSegMarginRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedSegMarginRC - agreementSummary.brokerSegMarginRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var diffSegCashCollateral = segIntVsExtTemplate.getElementById('diffSegCashCollateral');
                diffSegCashCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedSegCashCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.brokerSegCashCollateralRC) &&
                    !checkUndefinedOrEmpty(agreementSummary.adjustedSegCashCollateralRC - agreementSummary.brokerSegCashCollateralRC) && !isNaN(agreementSummary.adjustedSegCashCollateralRC - agreementSummary.brokerSegCashCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedSegCashCollateralRC - agreementSummary.brokerSegCashCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var diffSegSecuritiesCollateral = segIntVsExtTemplate.getElementById('diffSegSecuritiesCollateral');
                diffSegSecuritiesCollateral.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedSegPhysicalCollateralRC) && !checkUndefinedOrEmpty(agreementSummary.brokerSegPhysicalCollateralRC) &&
                    !checkUndefinedOrEmpty(agreementSummary.adjustedSegPhysicalCollateralRC - agreementSummary.brokerSegPhysicalCollateralRC) && !isNaN(agreementSummary.adjustedSegPhysicalCollateralRC - agreementSummary.brokerSegPhysicalCollateralRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedSegPhysicalCollateralRC - agreementSummary.brokerSegPhysicalCollateralRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';
                var diffED = segIntVsExtTemplate.getElementById('diffSegED');
                diffED.innerHTML = (!checkUndefinedOrEmpty(agreementSummary.adjustedSegExcessDeficitRC) && !checkUndefinedOrEmpty(agreementSummary.brokerSegExcessDeficitRC) &&
                    !checkUndefinedOrEmpty(agreementSummary.adjustedSegExcessDeficitRC - agreementSummary.brokerSegExcessDeficitRC) && !isNaN(agreementSummary.adjustedSegExcessDeficitRC - agreementSummary.brokerSegExcessDeficitRC)) ? treasury.formatters.number(null, null, (agreementSummary.adjustedSegExcessDeficitRC - agreementSummary.brokerSegExcessDeficitRC), null) : '<div style=\"text-align: right;\"><span >n/a</span></div>';

                callTypeDetails.appendChild(segIntVsExtTemplate);
            }

        }
    }

    function _executeBrokerAndRagSync(date, agreementId) {
        if (!window.CustomShadowDOMPolyfill) {
            event.stopPropagation();
        }
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + '/treasury/executeTreasuryEngineServiceCall?serviceUrl=' +
            encodeURIComponent('syncManager/executeBrokerAndRagSync?a=' + date + '&b=' + date + '&c=' + agreementId + '&d=true');
        window.open(baseUrl, '_blank');
    }

    function _getInternalVsExternalData(agreementSummary) {
        return {
            internalCashCollateral: validateAndGetDisplayText(agreementSummary.adjustedCashCollateralRC),
            internalSecuritiesCollateral: validateAndGetDisplayText(agreementSummary.adjustedPhysicalCollateralRC),
            internalExposure: validateAndGetDisplayText(agreementSummary.adjustedExposureRC),
            internalRequirement: validateAndGetDisplayText(agreementSummary.adjustedRequirementRC),
            internalED: validateAndGetDisplayText(agreementSummary.adjustedExcessDeficitRC),

            externalCashCollateral: validateAndGetDisplayText(agreementSummary.brokerCashCollateralRC),
            externalSecuritiesCollateral: validateAndGetDisplayText(agreementSummary.brokerPhysicalCollateralRC),
            externalExposure: validateAndGetDisplayText(agreementSummary.brokerExposureRC),
            externalRequirement: validateAndGetDisplayText(agreementSummary.brokerRequirementRC),
            externalED: validateAndGetDisplayText(agreementSummary.brokerExcessDeficitRC),

            diffCashCollateral: subtractWithNullChecksAndGetDisplayText(agreementSummary.adjustedCashCollateralRC, agreementSummary.brokerCashCollateralRC),
            diffSecuritiesCollateral: subtractWithNullChecksAndGetDisplayText(agreementSummary.adjustedPhysicalCollateralRC, agreementSummary.brokerPhysicalCollateralRC),
            diffExposure: subtractWithNullChecksAndGetDisplayText(agreementSummary.adjustedExposureRC, agreementSummary.brokerExposureRC),
            diffRequirement: subtractWithNullChecksAndGetDisplayText(agreementSummary.adjustedRequirementRC, agreementSummary.brokerRequirementRC),
            diffED: subtractWithNullChecksAndGetDisplayText(agreementSummary.adjustedExcessDeficitRC, agreementSummary.brokerExcessDeficitRC)
        };
    }

    function subtractWithNullChecksAndGetDisplayText(value1, value2) {
        return (!checkUndefinedOrEmpty(value1) && !checkUndefinedOrEmpty(value2)
            && !checkUndefinedOrEmpty(value1 - value2) && !isNaN(value1 - value2)) ? (value1 - value2) : 'n/a';
    }

    function validateAndGetDisplayText(value) {
        return !checkUndefinedOrEmpty(value) ? value : 'n/a';
    }

    function checkUndefinedOrEmpty(value) {
        if (value == undefined || value === "") {
            return true;
        }
        return false;
    }

    function validateAndGetDivElement(value) {
        let noDataAvailableDiv = '<div style=\"text-align: right;\"><span >n/a</span></div>';
        return value != 'n/a' ? treasury.formatters.number(null, null, value, null) : noDataAvailableDiv;
    }

    class TableDiff {
        constructor(exposure, requirement, cashCollateral, physicalCollateral) {
            this.exposure = exposure;
            this.requirement = requirement;
            this.cashCollateral = cashCollateral;
            this.physicalCollateral = physicalCollateral;
        }
    }

})();

function isValdObject(object){
    return object!=null && object !=undefined;
}
function setBrokerFileDetails(agreementSummary, detailDownloadBrokerFileId, downloadBrokerSelectId) {
    if (isValdObject(agreementSummary.brokerFileUrlsByLevel) && agreementSummary.brokerFileUrlsByLevel.length > 0) {
      detailDownloadBrokerFileId.disabled = false;
      downloadBrokerSelectId.disabled = false;
  
      for (var i = 0; i < agreementSummary.brokerFileUrlsByLevel.length; i++) {
        var brokerFile = agreementSummary.brokerFileUrlsByLevel[i];
        var brokerFileOption = document.createElement("option");
        brokerFileOption.textContent = brokerFile[1];
        brokerFileOption.value = brokerFile[0];
        downloadBrokerSelectId.appendChild(brokerFileOption);
      }

      detailDownloadBrokerFileId.onclick = function (event) {
        event.stopPropagation();
        var brokerFileUrl = document.getElementById('downloadBrokerSelectId').value;
        if (isValdObject(brokerFileUrl)) {
          if (brokerFileUrl.indexOf('transfers') != -1) {
            window.open('cometFileDownload?fileName=&contentType=&fileUrl=' + brokerFileUrl, '_blank');
          }
          else {
            window.open('/cocoa/api/file/downloadFile?objectUri=&fileId=' + brokerFileUrl + '&fileDate=' + agreementSummary.date);
          }
        }
      };
    }
    else {
      detailDownloadBrokerFileId.disabled = true;
      downloadBrokerSelectId.style.disabled = true;
    }
}
