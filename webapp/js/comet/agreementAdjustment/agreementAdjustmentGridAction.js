"user strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementAdjustmentGridAction = {
            handleAdjustmentDataDisplay : _handleAdjustmentDataDisplay,
            setExcessDeficitWorkerData : _setExcessDeficitWorkerData,
            resizeAgreementAdjustmentGrid : _resizeAgreementAdjustmentGrid,
            displayAdjustmentForAgreement : _displayAdjustmentForAgreement,
            getAgreementIdToAdjustmentMap : _getAgreementIdToAdjustmentMap
    };

   var _agmtIdToAdjustmentMap;
   var _adjustmentGrid;
   var _selectedAgreementId;
   var _excessDeficitWorkerData;

   function _setExcessDeficitWorkerData(excessDeficitWorkerData) {
       _excessDeficitWorkerData = excessDeficitWorkerData;
   }

   function _getAgreementIdToAdjustmentMap() {
          return _agmtIdToAdjustmentMap;
   }

   function _handleAdjustmentDataDisplay() {

       var treasuryAgreementData = _excessDeficitWorkerData[0];

       if(_agmtIdToAdjustmentMap === null || _agmtIdToAdjustmentMap === undefined) {
           _agmtIdToAdjustmentMap = new Object();
       }

       if(treasuryAgreementData != null  && treasuryAgreementData.children != undefined) {
           var treasuryAgreementAjustment = [];
           let treasuryAgreementAdjustmentCount = 0;
           for(var j = 0; j<treasuryAgreementData.children.length; j++) {

               var agreementData = treasuryAgreementData.children[j];

               var agreementAjustment = []; agreementAdjustmentCount=0;
               if(agreementData.positionDataList != undefined && agreementData.positionDataList.length > 0) {
                   for(var i = 0; i < agreementData.positionDataList.length; i++) {
                       agreementData.positionDataList[i].id = i;

                       if(agreementData.positionDataList[i].collateralAdjustments != null && agreementData.positionDataList[i].collateralAdjustments != undefined) {
                           for(var m=0;m < agreementData.positionDataList[i].collateralAdjustments.length; m++) {
                               agreementAjustment[agreementAdjustmentCount] = agreementData.positionDataList[i].collateralAdjustments[m];
                               agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
                               agreementAdjustmentCount++;
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] = agreementData.positionDataList[i].collateralAdjustments[m];
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount].id = treasuryAgreementAdjustmentCount;
                               treasuryAgreementAdjustmentCount++;
                           }
                       }

                       if(agreementData.positionDataList[i].exposureAdjustments != null && agreementData.positionDataList[i].exposureAdjustments != undefined) {
                           for(var m=0;m < agreementData.positionDataList[i].exposureAdjustments.length; m++) {
                               agreementAjustment[agreementAdjustmentCount] = agreementData.positionDataList[i].exposureAdjustments[m];
                               agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
                               agreementAdjustmentCount++;
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] = agreementData.positionDataList[i].exposureAdjustments[m];
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount].id = treasuryAgreementAdjustmentCount;
                               treasuryAgreementAdjustmentCount++;
                           }
                       }

                       if(agreementData.positionDataList[i].requirementAdjustments != null && agreementData.positionDataList[i].requirementAdjustments != undefined) {
                           for(var m=0;m < agreementData.positionDataList[i].requirementAdjustments.length; m++) {
                               agreementAjustment[agreementAdjustmentCount] = agreementData.positionDataList[i].requirementAdjustments[m];
                               agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
                               agreementAdjustmentCount++;
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] = agreementData.positionDataList[i].requirementAdjustments[m];
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount].id = treasuryAgreementAdjustmentCount;
                               treasuryAgreementAdjustmentCount++;
                           }
                       }
                   }
               } else {
                       if(agreementData.collateralAdjustments != null && agreementData.collateralAdjustments != undefined) {
                           for(var m=0;m < agreementData.collateralAdjustments.length; m++) {
                               agreementAjustment[agreementAdjustmentCount] = agreementData.collateralAdjustments[m];
                               agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
                               agreementAdjustmentCount++;
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] = agreementData.collateralAdjustments[m];
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount].id = treasuryAgreementAdjustmentCount;
                               treasuryAgreementAdjustmentCount++;
                           }
                       }

                       if(agreementData.exposureAdjustments != null && agreementData.exposureAdjustments != undefined) {
                           for(var m=0;m < agreementData.exposureAdjustments.length; m++) {
                               agreementAjustment[agreementAdjustmentCount] = agreementData.exposureAdjustments[m];
                               agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
                               agreementAdjustmentCount++;
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] = agreementData.exposureAdjustments[m];
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount].id = treasuryAgreementAdjustmentCount;
                               treasuryAgreementAdjustmentCount++;
                           }
                       }

                       if(agreementData.requirementAdjustments != null && agreementData.requirementAdjustments != undefined) {
                           for(var m=0;m < agreementData.requirementAdjustments.length; m++) {
                               agreementAjustment[agreementAdjustmentCount] = agreementData.requirementAdjustments[m];
                               agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
                               agreementAdjustmentCount++;
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] = agreementData.requirementAdjustments[m];
                               treasuryAgreementAjustment[treasuryAgreementAdjustmentCount].id = treasuryAgreementAdjustmentCount;
                               treasuryAgreementAdjustmentCount++;
                           }
                       }
               }

                if(agreementData.lcmPhysicalCollateralPositionDataList != undefined && agreementData.lcmPhysicalCollateralPositionDataList.length > 0) {
                    for(var i = 0; i < agreementData.lcmPhysicalCollateralPositionDataList.length; i++) {
                        agreementData.lcmPhysicalCollateralPositionDataList[i].id = i;
                        if(agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments != null && agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments != undefined) {
                            for(var m=0;m < agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments.length; m++) {
                                agreementAjustment[agreementAdjustmentCount] = agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments[m];
                                agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
                                agreementAdjustmentCount++;
                                treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] = agreementData.lcmPhysicalCollateralPositionDataList[i].physicalCollateralAdjustments[m];
                                treasuryAgreementAjustment[treasuryAgreementAdjustmentCount].id = treasuryAgreementAdjustmentCount;
                                treasuryAgreementAdjustmentCount++;
                            }
                        }
                    }
                }
                else {
                    if(agreementData.physicalCollateralAdjustments != null && agreementData.physicalCollateralAdjustments != undefined) {
                        for(var m=0;m < agreementData.physicalCollateralAdjustments.length; m++) {
                            agreementAjustment[agreementAdjustmentCount] = agreementData.physicalCollateralAdjustments[m];
                            agreementAjustment[agreementAdjustmentCount].id = agreementAdjustmentCount;
                            agreementAdjustmentCount++;
                            treasuryAgreementAjustment[treasuryAgreementAdjustmentCount] = agreementData.physicalCollateralAdjustments[m];
                            treasuryAgreementAjustment[treasuryAgreementAdjustmentCount].id = treasuryAgreementAdjustmentCount;
                            treasuryAgreementAdjustmentCount++;
                        }
                    }
                }
               
               if(agreementAjustment != undefined) {
                   _agmtIdToAdjustmentMap[agreementData.agreementId] = agreementAjustment;
               }

           }
           if(treasuryAgreementAjustment != undefined) {
               _agmtIdToAdjustmentMap[treasuryAgreementData.agreementId] = treasuryAgreementAjustment;
           }
       }

       _setSelectedAgreementId(treasuryAgreementData.agreementId);

       var columns = window.treasury.comet.agreementWorkflowColumnConfig.getAdjustmentColumnConfig();
       var options = _getAdjustmentPanelOptions();

       // Initializing the agreement workflow top panel
       if( _agmtIdToAdjustmentMap[_selectedAgreementId] != undefined && _agmtIdToAdjustmentMap[_selectedAgreementId].length >0) {
           _adjustmentGrid = new dportal.grid.createGrid($("#adjustmentGrid"), _agmtIdToAdjustmentMap[_selectedAgreementId] ,columns,options);
       }

   }

   function _displayAdjustmentForAgreement(agreementId) {
       var columns = window.treasury.comet.agreementWorkflowColumnConfig.getAdjustmentColumnConfig();
       var options = _getAdjustmentPanelOptions();

       if(agreementId != undefined && _agmtIdToAdjustmentMap[agreementId] != undefined && _agmtIdToAdjustmentMap[agreementId].length >0) {
           _adjustmentGrid = new dportal.grid.createGrid($("#adjustmentGrid"), _agmtIdToAdjustmentMap[_selectedAgreementId] ,columns,options);
           _adjustmentGrid.resizeCanvas();
       }
   }

   function _setSelectedAgreementId(agreementId) {
       _selectedAgreementId = agreementId;
   }

   function _getAdjustmentPanelOptions() {

       var options = {
           editable : true,
           exportToExcel : true,
           highlightRowOnClick : true,
           enableCellNavigation : true,
           autoHorizontalScrollBar : true,
           customColumnSelection : false,
           displaySummaryRow : true,
           asyncEditorLoading : false,
           applyFilteringOnGrid: true,
           autoEdit : true,
           configureColumns : true
       };
       return options;
   }

   /**
    * Helper method to resize the adjustment grid
    */
   function _resizeAgreementAdjustmentGrid() {
    // Resizing agreement workflow top panel grid on state change of right panel
       if(_adjustmentGrid != null) {
           _adjustmentGrid.resizeCanvas();
           _adjustmentGrid.refreshGrid([]);
       }
   }

})();
