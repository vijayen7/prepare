
"user strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementAdjustmentAction = {
            handleAdjustmentPopupDisplay : _handleAdjustmentPopupDisplay,
            handleAdjustmentPopupAgreementWorkflowDisplay : _handleAdjustmentPopupAgreementWorkflowDisplay,
            setSourceGridConfig : _setSourceGridConfig,
            setSelectedAgreementId : _setSelectedAgreementId,
            setWorkflowId : _setWorkflowId
    };

    var _sourceGridConfig;
    var _selectedAgreementId;
    var _workflowInstanceId;

    function _saveAdjustment() {

        var url = "/treasury/comet/saveAdjustments";

        var selectedAdjustmentType = document.getElementById('adjustmentType').value;
        var selectedLegalEntity = document.getElementById('legalEntity').value;
        var selectedCounterParty = document.getElementById('exposureCounterParty').value;
        var selectedAgreementType = document.getElementById('agreementType').value;
        var selectedBook = document.getElementById('book').value;
        var selectedBusinessUnit = document.getElementById('businessUnit').value;
        if(selectedBook == undefined || selectedBusinessUnit == undefined || selectedBook == null || selectedBusinessUnit == null) {
            alert("Invalid Book/Business Unit");
            document.querySelector('#adjustmentPopup').shadowRoot.querySelector('button[disabled]').disabled=false;
            return;
        }

        if (selectedAdjustmentType.key == 1 && selectedBusinessUnit.key == -1) {
            if (!confirm("Adding Usage Adjustment with Business Unit as ALL. Do you wish to proceed?")) {
                document.querySelector('#adjustmentPopup').shadowRoot.querySelector('button[disabled]').disabled = false;
                return;
            }
        }
        var selectedMarginCallType = document.getElementById('marginCallType').value;
        if(_sourceGridConfig != 'Agreement') {
            var calculator = document.getElementById('calculator').value;
            var custodianAccount = document.getElementById('custodianAccount').value;
            var pnlSpn = document.getElementById('pnlSpn').value;
            var custodianAccountId = document.getElementById('custodianAccountId').value;
        }
        var startDateStr = document.getElementById('startDate').value;
        var endDateStr = document.getElementById('endDate').value;
        var selectedReasonCode = document.getElementById('reasonCode').value;
        var adjustmentAmount = document.getElementById('adjustmentAmount').value;
        if(adjustmentAmount == undefined || adjustmentAmount == "") {
            alert("Specify an amount");
            document.querySelector('#adjustmentPopup').shadowRoot.querySelector('button[disabled]').disabled=false;
            return;
        }
        adjustmentAmount = adjustmentAmount.replace(/,/g, "").replace(/x/g, "");
        var comment = document.getElementById('comment').value;

        var startDate = null;
        var endDate = null;
        if(startDateStr != null && startDateStr != undefined) {
            startDate = new Date(startDateStr);
        }

        if(endDateStr != null && endDateStr != undefined) {
            endDate = new Date(endDateStr);
        }

        var adjustmentData = null;

        // Setting adjustment data
        if(_sourceGridConfig == 'Agreement') {
            // Adjustment added from top grid
            adjustmentData = {
                "adjustmentTypeId":selectedAdjustmentType.key,
                "legalEntityId" : selectedLegalEntity.key,
                "counterPartyEntityId" : selectedCounterParty.key,
                "agreementTypeId" : selectedAgreementType.key,
                "bookId" : selectedBook.key,
                "businessUnitId" : selectedBusinessUnit.key,
                "startDataStr" : window.treasury.comet.agreementWorkflowAction.getFormattedDate(startDate),
                "endDateStr" : window.treasury.comet.agreementWorkflowAction.getFormattedDate(endDate),
                "reasonCodeId" : selectedReasonCode.key,
                "amountUsd" : adjustmentAmount,
                "comment" : comment,
                "sourceGridConfig" : _sourceGridConfig,
                "workflowInstanceId":_workflowInstanceId != null ? _workflowInstanceId : sessionStorage.getItem('workflowId'),
                "marginCallType":selectedMarginCallType.value,
                "marginCallTypeId" : selectedMarginCallType.key
            };
        } else {
            // Adjustment added from exposure/Requirement/Usage/Collateral grids
            adjustmentData = {
                "adjustmentTypeId":selectedAdjustmentType.key,
                "legalEntityId" : selectedLegalEntity.key,
                "counterPartyEntityId" : selectedCounterParty.key,
                "agreementTypeId" : selectedAgreementType.key,
                "bookId" : selectedBook.key,
                "businessUnitId" : selectedBusinessUnit.key,
                "marginCallTypeId" : selectedMarginCallType.key,
                "calculator" : calculator,
                "caId" : custodianAccountId,
                "spn" : pnlSpn,
                "startDataStr" : window.treasury.comet.agreementWorkflowAction.getFormattedDate(startDate),
                "endDateStr" : window.treasury.comet.agreementWorkflowAction.getFormattedDate(endDate),
                "reasonCodeId" : selectedReasonCode.key,
                "amountUsd" : adjustmentAmount,
                "comment" : comment,
                "sourceGridConfig" : _sourceGridConfig,
                "workflowInstanceId":_workflowInstanceId != null ? _workflowInstanceId : sessionStorage.getItem('workflowId')
            };
        }

        var promise = $.ajax({
            url : url,
            data : adjustmentData,
            type : "POST"
        });

        promise.done(function(data) {
            var retAdjustmentData = data.resultList[0];

            if(retAdjustmentData.status == 'success') {
                _clearAdjustmentPopup();
                alert(retAdjustmentData.message);

                //removing isNewlyCreated=true from url to prevent reinserting of NAV Comparison workflow data
                if(_sourceGridConfig != undefined && _sourceGridConfig == 'navComparisonWorklfow') {
                	var newurl = window.location.href.replace("&isNewlyCreated=true", "");
                	newurl = newurl.replace("#", "");
                	window.location.replace(newurl);
                }
                else {
                	window.location.reload();
                }

            } else if (retAdjustmentData.status == 'failure') {
                alert(retAdjustmentData.message);
                _clearAdjustmentPopup();
            }
         });

        promise.fail(function(xhr, text, errorThrown) {
            alert("Error occurred while adding adjustment");
            console.log("Error occurred while adding adjustment");
        });
    }

    function _handleAdjustmentPopupAgreementWorkflowDisplay(startDataStr, legalEntityId,
            counterPartyEntityId, agreementTypeId,actionableType, reportingCurrency){
        _handleAdjustmentPopupDisplay(startDataStr, legalEntityId,
                counterPartyEntityId, agreementTypeId, null,
                null, null, null, null,
                actionableType, null, null, reportingCurrency);
    }

    function _handleAdjustmentPopupDisplay(startDataStr, legalEntityId,
            counterPartyEntityId, agreementTypeId, calculator,
            custodianAccount, pnlSpn, custodianAccountId, entityFamilyId,
            actionableType, businessUnitId, bookId, reportingCurrency) {

        var url = "/treasury/comet/displayAdjustmentPopup";
        var adjustmentDataParam = _setParametersUsingSourceGridConfig(
                startDataStr, legalEntityId, counterPartyEntityId,
                agreementTypeId, calculator, custodianAccount, pnlSpn,
                custodianAccountId, entityFamilyId, actionableType, businessUnitId, bookId);

        var promise = $.ajax({
            url : url,
            data : adjustmentDataParam,
            dataType : "json",
            type : "POST"
        });

       promise.done(function(data) {


           var adjustmentData = data.resultList[0];
           var businessUnits = adjustmentData.businessUnits;
           var books = adjustmentData.books;
           var reasons = adjustmentData.reasons;
           var counterParties = adjustmentData.counterParties;
           var legalEntities = adjustmentData.legalEntities;
           var startDateStr = adjustmentData.startDate;
           var endDateStr = adjustmentData.endDate;
           var adjustmentType = adjustmentData.adjustmentType;
           var agreementType = adjustmentData.agreementType;
           var marginCallType = adjustmentData.marginCallType;
           var startDate = new Date(startDateStr).toLocaleString("en-US", {timeZone: "America/New_York"});
           startDate = new Date(startDate);
           var endDate = new Date(endDateStr).toLocaleString("en-US", {timeZone: "America/New_York"});
           endDate = new Date(endDate);
           var sourceGridConfig = adjustmentData.sourceGridConfig;
           var calculator = adjustmentData.calculator;
           var custodianAccount = adjustmentData.caName;
           var pnlSpn = adjustmentData.pnlSpn;
           var workflowInstanceId = adjustmentData.workflowInstanceId;

           _sourceGridConfig = sourceGridConfig;
           _workflowInstanceId = workflowInstanceId;

           if(sourceGridConfig == 'Agreement') {
               document.getElementById("calculatorDiv").setAttribute("hidden", true);
               document.getElementById("custodianAccountDiv").setAttribute("hidden", true);
               document.getElementById("pnlSpnDiv").setAttribute("hidden", true);
           }else  {
               document.getElementById("calculatorDiv").removeAttribute("hidden");
               document.getElementById("custodianAccountDiv").removeAttribute("hidden");
               document.getElementById("pnlSpnDiv").removeAttribute("hidden");
           }


           var agreementTypeList = [];
           for (var i = 0; i < agreementType.length; i++) {

               agreementTypeList
                       .push({
                           0 : agreementType[i].agreementTypeId,
                           1 : agreementType[i].abbrev
                       });

           }

           agreementTypeSingleSelect = new window.treasury.filter.SingleSelect(
                   "agreementType", "agreementType",
                   agreementTypeList, []);

           // Setting first value by default
           agreementTypeSingleSelect.setSelectedNode(agreementType[0].agreementTypeId);
           var adjustmentTypeList = [];
           for (var i = 0; i < adjustmentType.length; i++) {

               adjustmentTypeList
                       .push({
                           0 : adjustmentType[i].adjustmentTypeId,
                           1 : adjustmentType[i].name
                       });

           }

           adjustmentTypeSingleSelect = new window.treasury.filter.SingleSelect(
                   "adjustmentType", "adjustmentType",
                   adjustmentTypeList, []);

           // Setting first value by default
           adjustmentTypeSingleSelect.setSelectedNode(adjustmentType[0].adjustmentTypeId);

           if(sourceGridConfig != 'Agreement') {

               var calculatorComponent = document.getElementById("calculator");
               var custodianAccountComponent = document.getElementById("custodianAccount");
               var pnlSpnComponent = document.getElementById("pnlSpn");
               var custodianAccountIdComponent = document.getElementById("custodianAccountId");

               calculatorComponent.value = calculator;
               custodianAccountComponent.value = custodianAccount;
               pnlSpnComponent.value = pnlSpn;
               custodianAccountIdComponent.value = adjustmentData.caId;
           }

           // Setting margin call type filters
           var marginCallTypeList = [];
           for (var i = 0; i < marginCallType.length; i++) {

               marginCallTypeList
                       .push({
                           0 : marginCallType[i].marginCallTypeId,
                           1 : marginCallType[i].abbrev
                       });

           }

           marginCallTypeSingleSelect = new window.treasury.filter.SingleSelect(
                   "marginCallType", "marginCallType",
                   marginCallTypeList, []);

           // Setting first value by default
           marginCallTypeSingleSelect.setSelectedNode(marginCallType[0].marginCallTypeId);

           var businessUnitList = [];
           for (var i = 0; i < businessUnits.length; i++) {

               businessUnitList
                       .push({
                           0 : businessUnits[i].buId,
                           1 : businessUnits[i].name
                       });

           }

           if(sourceGridConfig == 'COLLATERAL_GRID' || sourceGridConfig == 'SECURITIES_COLLATERAL_GRID') {
               businessUnitList = [{0: -1, 1: 'ALL'}];
           }

           businessUnitSingleSelect = new window.treasury.filter.SingleSelect(
                   "businessUnit", "businessUnit",
                   businessUnitList, []);

           // Setting first value by default
           businessUnitSingleSelect.setSelectedNode(businessUnits[0].buId);

           var bookList = [];
           for (var i = 0; i < books.length; i++) {

               bookList
                       .push({
                           0 : books[i].bookId,
                           1 : books[i].displayName
                       });

           }

           bookSingleSelect = new window.treasury.filter.SingleSelect(
                   "book", "book",
                   bookList, []);

           // Setting first value by default
           bookSingleSelect.setSelectedNode(books[0].bookId);

           var reasonList = [];
           for (var i = 0; i < reasons.length; i++) {

               reasonList
                       .push({
                           0 : reasons[i].reasonId,
                           1 : reasons[i].name
                       });

           }

           reasonSingleSelect = new window.treasury.filter.SingleSelect(
                   "reasonCode", "reasonCode",
                   reasonList, []);

           // Setting first value by default
           reasonSingleSelect.setSelectedNode(reasons[0].reasonId);

           var counterPartyList = [];
           for (var i = 0; i < counterParties.length; i++) {

               counterPartyList
                       .push({
                           0 : counterParties[i].counterPartyId,
                           1 : counterParties[i].displayName
                       });

           }

           counterPartySingleSelect = new window.treasury.filter.SingleSelect(
                   "exposureCounterParty", "exposureCounterParty",
                   counterPartyList, []);

           // Setting first value by default
           counterPartySingleSelect.setSelectedNode(counterParties[0].counterPartyId);

           var legalEntityList = [];
           for (var i = 0; i < legalEntities.length; i++) {

               legalEntityList
                       .push({
                           0 : legalEntities[i].legalEntityId,
                           1 : legalEntities[i].displayName
                       });

           }

           legalEntitySingleSelect = new window.treasury.filter.SingleSelect(
                   "legalEntity", "legalEntity",
                   legalEntityList, []);

           // Setting first value by default
           legalEntitySingleSelect.setSelectedNode(legalEntities[0].legalEntityId);

           // Initializing wire date and value date to current date

           $("#startDate").datepicker({
               showOn : "both",
               buttonText : "<i class='fa fa-calendar'></i>"
           }).datepicker("setDate", startDate);

           $("#endDate").datepicker({
               showOn : "both",
               buttonText : "<i class='fa fa-calendar'></i>"
           }).datepicker("setDate", endDate);

           var adjustmentReportingCurrency = document.getElementById('adjustmentReportingCurrency');
           adjustmentReportingCurrency.innerHTML = reportingCurrency || "USD";

           var adjustmentAmountInput = document.getElementById('adjustmentAmount');
           if(adjustmentAmountInput) {
        	   adjustmentAmountInput.value = "";
           }

           var commentInput = document.getElementById('comment');
           if(commentInput) {
        	   commentInput.value = "";
           }

           document.addEventListener('keypress', function (e) {
             var key = e.which || e.keyCode;
             if (key === 13) { // 13 is enter
      if( document.querySelector('#adjustmentPopup').visible == true) {
        document.getElementById('adjustmentPopup').shadowRoot.querySelector('.adjustment').disabled = true;
        _saveAdjustment();
      }
    }
});

    document.getElementById('adjustmentAmount').addEventListener('keyup', function (e) {
        var key = e.which || e.keyCode;
        if(key != 109 && key != 110 && key != 189 && key != 190 && key != 17 && !e.ctrlKey) {

            var inputEl = document.getElementById('adjustmentAmount'),
                value = inputEl.value;


            var isNegative = false;
            if(value.indexOf("-") != -1) {
                isNegative = true;
            }
            value = value.replace(/-/g, "");
            value = value.replace(/,/g, "").replace(/x/g, "");
            value = treasury.formatters.formatNumberUsingCommas(value);
            if(isNegative) {
                value = "-".concat(value);
            }
            if(value == 0) {
                value = "";
            }

            var newCursorStart = treasury.getCursorPosition(inputEl.value, value, inputEl.selectionStart);
            var newCursorEnd = treasury.getCursorPosition(inputEl.value, value, inputEl.selectionEnd);

            inputEl.value = value;
            inputEl.selectionStart = newCursorStart;
            inputEl.selectionEnd = newCursorEnd;
        }
    });

           document.getElementById('adjustmentPopup').reveal({
               'title' : 'Add Adjustment',
               'modal' : true,
               'sticky': true,
               'moveable': true,
               'buttons' : [ {
                   'html' : 'Save',
                   'class' : 'adjustment',
                   'callback' : function() {
                     this.shadowRoot.querySelector('.adjustment').disabled = true;
                       _saveAdjustment();
                   },
                   'position' : 'center'
               }, {
                   'html' : 'Cancel',
                   'callback' : function() {

                       _clearAdjustmentPopup();
                   },
                   'position' : 'center'
               } ]
           });

           console.log('back form adjustment action');
       });

   }

   /**
    * On Canceling the Adjustment Popup, contents of the adjustment popup are cleared
    */
   function _clearAdjustmentPopup() {


       document.querySelector('#adjustmentPopup').visible = false;

   }

   function _setSourceGridConfig(sourceGridConfig) {
       _sourceGridConfig = sourceGridConfig;
   }

   function _setSelectedAgreementId(agreementId) {
       _selectedAgreementId = agreementId;
   }

   function _setWorkflowId(workflowId) {
       _workflowInstanceId = workflowId;
   }

   function _setParametersUsingSourceGridConfig(startDataStr,legalEntityId,counterPartyEntityId,agreementTypeId,calculator,custodianAccount,pnlSpn,custodianAccountId,entityFamilyId,actionableType, businessUnitId, bookId) {

       var adjustmentDataParam = "sourceGridConfig=" + _sourceGridConfig;
       adjustmentDataParam += '&workflowInstanceId='+_workflowInstanceId;
       adjustmentDataParam += '&startDataStr='+startDataStr;

       if(_sourceGridConfig != 'navComparisonWorklfow'){
         if(_selectedAgreementId != undefined && _selectedAgreementId != null) {
           adjustmentDataParam += '&selectedAgreementId='+_selectedAgreementId;
         }
       adjustmentDataParam += '&legalEntityId='+legalEntityId;
       adjustmentDataParam += '&counterPartyEntityId='+counterPartyEntityId;
       adjustmentDataParam += '&agreementTypeId='+agreementTypeId;
       }

           if(calculator != undefined && calculator != 'undefined' && calculator != '') {
               adjustmentDataParam += '&calculator='+calculator;
           }
           if(pnlSpn != undefined && pnlSpn != 'undefined' && pnlSpn != '') {
               adjustmentDataParam += '&spn='+pnlSpn;
           }
           if(custodianAccount != undefined && custodianAccount != 'undefined' && custodianAccount != '') {
               adjustmentDataParam += '&caName='+custodianAccount;
           }
           if(custodianAccountId != undefined && custodianAccountId != 'undefined' && custodianAccountId != '') {
               adjustmentDataParam += '&caId='+custodianAccountId;
           }
           if(entityFamilyId != undefined && entityFamilyId != 'undefined' && entityFamilyId != '') {
               adjustmentDataParam += '&entityFamilyId='+entityFamilyId;
           }
           if(actionableType != undefined && actionableType != 'undefined' && actionableType != '') {
               adjustmentDataParam += '&actionableType='+actionableType;
           }
           if(businessUnitId != undefined && businessUnitId != 'undefined' && businessUnitId != '') {
               adjustmentDataParam += '&businessUnitId='+businessUnitId;
           }
           if(bookId != undefined && bookId != 'undefined' && bookId != '') {
               adjustmentDataParam += '&bookId='+bookId;
           }

           return adjustmentDataParam;
       }


})();
