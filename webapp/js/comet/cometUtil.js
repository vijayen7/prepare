"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.Util = {

            plainNumberFormatter : _plainNumberFormatter,
            isDefined : _isDefined
    };

    function _isDefined(value) {
        return (value != null && value != undefined);
    }
    function _plainNumberFormatter(row, cell, value, columnDef, dataContext) {
        if(value != undefined && value !== "" && (typeof value == "string" || !isNaN(value))) {
            value += "";
            var num = Math.round(value.replace(/,/g, "").replace(/x/g, ""));
            value = treasury.formatters.formatNumberUsingCommas(num);
            if (num < 0) {
                return "(" +
                        value.replace(/-/, '') + ")";
            }
            else {
                return  value;
            }
        }
        else {
            return 'n/a';
        }
    }



})();