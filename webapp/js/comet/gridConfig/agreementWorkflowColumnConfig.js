'use strict';

(function() {
  window.treasury = window.treasury || {};
  window.treasury.comet = window.treasury.comet || {};
  window.treasury.comet.agreementWorkflowColumnConfig = {
    getAgreementWorkflowTopPanelColumns: _getAgreementWorkflowTopPanelColumns,
    getAdjustmentColumnConfig: _getAdjustmentColumnConfig
  };

  var _defaultColumns = [];

  function _getAgreementWorkflowTopPanelColumns(isPopulateAsm) {
    var columnsBeforeAsm = [{
      id: 'actions',
      name: 'Actions',
      field: 'actions',
      formatter: _actionsFormatter,
      type: 'text',
      sortable: true,
      width: 85
    },
    {
      id: 'tag',
      name: 'Tags',
      field: 'tag',
      type: 'text',
      formatter: _applicableColumnIdentifier,
      filter: true,
      headerCssClass: 'b',
      width: 60
    },
    {
      id: 'accountType',
      name: 'Account Type',
      field: 'accountType',
      type: 'text',
      filter: true,
      headerCssClass: 'b',
      width: 60
    },
    {
      id: 'agreementName',
      name: 'Agreement Name',
      field: 'agreementName',
      formatter: _agreementNameFormatter,
      type: 'text',
      filter: true,
      headerCssClass: 'b',
      width: 60
    },
    {
      id: 'reportingCurrency',
      name: 'Reporting Currency',
      field: 'reportingCurrency',
      type: 'text',
      filter: true,
      sortable: true,
      headerCssClass: 'b',
      width: 40
    },
    {
      id: 'internalED',
      name: 'Internal E/D',
      field: 'internalEDRC',
      type: 'number',
      toolTip: 'E/D',
      formatter: treasury.formatters.number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 60
    },
    {
      id: 'brokerExcessDeficit',
      name: 'External E/D',
      field: 'brokerExcessDeficitRC',
      type: 'number',
      toolTip: 'Ext E/D',
      formatter: treasury.formatters.number,
      aggregator: dpGrid.Aggregators.sum,
      headerCssClass: 'aln-rt b',
      excelFormatter: '#,##0',
      width: 60
    }];
    
    var asmColumn = [
      {
        id: 'asm',
        name: 'ASM',
        field: 'asmRC',
        type: 'number',
        isEditable: true,      
        editor: dpGrid.Editors.Text,
        toolTip: 'Actual Securities Movement',
        formatter: treasury.formatters.number,
        aggregator: dpGrid.Aggregators.sum,
        headerCssClass: 'aln-rt b',
        excelFormatter: '#,##0'
      }];
    
    
    var columnsAfterAsm = [
      {
        id: 'ecm',
        name: 'ECM',
        field: 'ecmRC',
        type: 'number',
        toolTip: 'Expected Cash Movement',
        formatter: treasury.formatters.number,
        aggregator: dpGrid.Aggregators.sum,
        headerCssClass: 'aln-rt b',
        excelFormatter: '#,##0'
      },
      {
        id: 'acm',
        name: 'ACM',
        field: 'acmRC',
        type: 'textbox',
        toolTip: 'Actual Cash Movement',
        isEditable: true,
        editor: dpGrid.Editors.Text,
        formatter: _acmColumnFormatter,
        headerCssClass: 'aln-rt b',
        excelFormatter: '#,##0',
        width: 60
      },    
      {
        id: 'disputeAmount',
        name: 'Dispute Amount',
        field: 'disputeAmountRC',
        type: 'number',
        toolTip: 'Dispute Amount',
        isEditable: true,
        editor: dpGrid.Editors.Text,
        formatter: _disputeAmountColumnFormatter,
        headerCssClass: 'aln-rt b',
        excelFormatter: '#,##0',
        width: 60
      },
      {
        id: 'wireRequest',
        name: 'Wire Request',
        field: 'wireRequest',
        type: 'text',
        filter: true,
        sortable: true,
        headerCssClass: 'b',
        minWidth: 40,
        width: 60
      },
      {
        id: 'bookingReference',
        name: 'Booking Reference',
        field: 'bookingReference',
        type: 'text',
        formatter: _ptgBookingReferenceFormatter,
        filter: true,
        sortable: true,
        headerCssClass: 'b',
        minWidth: 40,
        width: 60
      }
    ];
    if(isPopulateAsm == true)
    {
      columnsBeforeAsm = columnsBeforeAsm.concat(asmColumn);
    }
    return columnsBeforeAsm.concat(columnsAfterAsm);
  }

  function _getAdjustmentColumnConfig() {
    return [
      {
        id: 'legalEntityName',
        name: 'Legal Entity',
        field: 'legalEntityName',
        toolTip: 'Legal Entity',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'counterpartyName',
        name: 'Exposure Counter Party',
        field: 'counterpartyName',
        toolTip: 'Exposure Counter Party',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'book',
        name: 'Book',
        field: 'book',
        toolTip: 'Book',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'businessUnit',
        name: 'Business Unit',
        field: 'businessUnit',
        toolTip: 'Business Unit',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'agreementType',
        name: 'Agreement Type',
        field: 'agreementType',
        toolTip: 'Agreement Type',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'adjustmentType',
        name: 'Adjustment Type',
        field: 'adjustmentType',
        toolTip: 'Adjustment Type',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'marginCallType',
        name: 'Margin Call Type',
        field: 'marginCallType',
        toolTip: 'Margin Call Type',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'description',
        name: 'Description',
        field: 'description',
        toolTip: 'Description',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'adjustment',
        name: 'Amount',
        field: 'adjustment',
        toolTip: 'Amount',
        type: 'number',
        formatter: treasury.formatters.number,
        aggregator: dpGrid.Aggregators.sum,
        headerCssClass: 'aln-rt b',
        excelFormatter: '#,##0'
      },
      {
        id: 'startDate',
        name: 'Start Date',
        field: 'startDate',
        toolTip: 'Start Date',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'endDate',
        name: 'End Date',
        field: 'endDate',
        toolTip: 'End Date',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'source',
        name: 'Source',
        field: 'source',
        toolTip: 'Source',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'publishTime',
        name: 'Publish Time',
        field: 'publishTime',
        toolTip: 'Publish Time',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'comment',
        name: 'Comment',
        field: 'comment',
        toolTip: 'Comment',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      },
      {
        id: 'login',
        name: 'User',
        field: 'login',
        toolTip: 'User',
        type: 'text',
        filter: true,
        headerCssClass: 'b',
        minWidth: 60
      }
    ];
  }

  function _actionsFormatter(row, cell, value, columnnDef, dataContext) {
    var actions = '';
    let isInternalDataMissing = dataContext.isInternalDataMissing

    if (
      dataContext.agreementType != undefined &&
      dataContext.agreementType != 'MNA' &&
      dataContext.agreementType != 'SEG' && !isInternalDataMissing
    ) {
      actions = actions.concat('<a  href="#" title="Add Adjustment"   ><i class="icon-add"></i></a>');
    }

    if (dataContext.isActionableAgreement) {
      actions = actions.concat('<a   href="#" title="Post Collateral"  ><i class="icon-transfer margin--left"></i></a>');
      if (dataContext.agreementType != 'SEG' && !isInternalDataMissing) {
        actions = actions.concat(' <a href="#" title="Email Call Report"  ><i class="icon-mail margin--left"></i></a>');
      }
    }

    if (dataContext.brokerFileUrlsByLevel != undefined && dataContext.brokerFileUrlsByLevel != '') {
      actions = actions.concat(_brokerFilesFormatter(dataContext));
      actions = actions.concat(' <a href="#" title="Download Broker File"  ><i class="icon-download margin--left"></i></a>');
      }
    
    if (dataContext.crifFileUrl != undefined && dataContext.crifFileUrl != '') {
      actions = actions.concat(' <a href="#" title="Download Crif File"  ><i class="icon-cloud--download margin--left"></i></a>');
    }
    
    return actions;
  }

  function _brokerFilesFormatter(dataContext) {
    var options = ""
    for (var i = 0; i < dataContext.brokerFileUrlsByLevel.length; i++) {
      var brokerFile = dataContext.brokerFileUrlsByLevel[i];
      options += "<OPTION value='" + brokerFile[0] + "'>" + brokerFile[1] + "</OPTION>";
    }
    return '<select id="selectBrokerFile' + dataContext.agreementId + '">' +
      options +
      '</select>';
  }

  function _agreementNameFormatter(row, cell, value, columnnDef, dataContext) {
    var agreementName = '<a  href="#">' + dataContext.agreementName + '</a>';
    return agreementName;
  }

  function _ptgBookingReferenceFormatter(row, cell, value, columnnDef, dataContext) {
    let url = CODEX_PROPERTIES["com.arcesium.treasury.ptg.url"].myValue + "/searchInstructions?instructionGroupId=" + value
    return value? '<a href="' + url + '" target="_blank">' + value + '</a>': '';
  }

  function _applicableColumnIdentifier(row, cell, value, columnnDef, dataContext) {
    if (cell == 1) {
      if (dataContext.tag == 'REG') {
        return '<span class="token red">' + value + '</span>';
      } else {
        return '<span class="token gray2">' + value + '</span>';
      }
    }
  }

  function _acmColumnFormatter(row, cell, value, columnnDef, dataContext) {
    if (dataContext.acmRC === undefined) {
      dataContext.acmRC = '';
    }

    if (dataContext.isActionableAgreement) {
      if (value === undefined || value == '') {
        return '<div style="background-color:white">Enter ACM</div>';
      } else {
        return (
          '<div style="background-color:white">' +
          treasury.formatters.number(row, cell, value, columnnDef, dataContext) +
          '</div>'
        );
      }
    } else {
      dataContext.acmRC = '';
    }    
  }

  function _disputeAmountColumnFormatter(row, cell, value, columnnDef, dataContext) {    
    if (dataContext.isActionableAgreement) {
      if (value === undefined || value == '') {
        return '<div style="background-color:white">Enter dispute amount</div>';
      } else {
        return (
          '<div style="background-color:white">' +
          treasury.formatters.number(row, cell, value, columnnDef, dataContext) +
          '</div>'
        );
      }
    } else {
      dataContext.disputeAmountRC = '';
    }
  }
})();
