"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.agreementSummaryColumnConfig = {
        getAgreementSummaryColumns : _getAgreementSummaryColumns,
        getDefaultColumns : _getDefaultColumns,
        getTotalColumns : _getTotalColumns
    };

    var _defaultColumns = [];
    var _totalColumns = [];

    function _getDefaultColumns() {
        var columns = _getAgreementSummaryColumns();
        _defaultColumns = getColumnsForDefaultColumnChooser(columns);
        return _defaultColumns;
    }

    function _getTotalColumns() {
        var columns = _getAgreementSummaryColumns();
        _totalColumns = getColumnsForColumnChooser(columns);
        return _totalColumns;
    }

    /**
     * Get the list of columns which need to be displayed n column chooser.
     * @ param : array the list of total columns
     * @return : array the list of columns which are enabled for custom column selection
     */
    function getColumnsForColumnChooser(columns) {
      if (!columns)
        return;
      var retColumns = [];
      for (var i = 0; i < columns.length; i++) {
        retColumns.push([
          columns[i].id,
          columns[i].name
        ]);
      }
      return retColumns;
    }

    /**
     * Get the list of columns which need to be displayed n column chooser.
     * @ param : array the list of total columns
     * @return : array the list of columns which are enabled for custom column selection
     */
    function getColumnsForDefaultColumnChooser(columns) {
        if(!columns)
            return;
        var retColumns = [];
        for(var i=0;i<columns.length;i++){
            if(!columns[i].ignoreInColumnSelection){
                retColumns.push(columns[i].id);
            }
        }
        return retColumns;
    }

    /**
     * Get the list of columns which need to be displayed n column chooser.
     * @ param : array the list of total columns
     * @return : array the list of columns which are enabled for custom column selection
     */
    function getColumnsEnabledForColumnChooser(columns) {
      if (!columns)
        return;
      var retColumns = [];
      for (var i = 0; i < columns.length; i++) {
        if (!columns[i].ignoreInColumnSelection) {
          retColumns.push([
            columns[i].id,
            columns[i].name
          ]);
        }
      }
      return retColumns;
    }

    /**
     * Given the field, name ignoreInColumnSelection values
     * return config for both USD and reporting currency columns
     * @return : The list containing two configs one each for usd and RC values
     */

     function getColumnsForUsdAndRcValues(field, name, ignoreInColumnSelection) {
        var usdFieldSuffix = "";
        var rcFieldSuffix = "RC";

        var usdNameSuffix = " (In USD)";
        var rcNameSuffix = " (In RC)";

        var usdConfig = {
            id : field + usdFieldSuffix,
            type : "number",
            toolTip : name + usdNameSuffix,
            formatter : _publishedRowNumberFormatter,
            aggregator : dpGrid.Aggregators.sum,
            name : name + usdNameSuffix,
            field : field + usdFieldSuffix,
            sortable : true,
            ignoreInColumnSelection:ignoreInColumnSelection,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        };

        var rcConfig = {
            id : field + rcFieldSuffix,
            type : "number",
            toolTip : name + rcNameSuffix,
            formatter : _publishedRowNumberFormatter,
            name : name + rcNameSuffix,
            field : field + rcFieldSuffix,
            sortable : true,
            ignoreInColumnSelection:ignoreInColumnSelection,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        };
        return [usdConfig, rcConfig];
     }

    function _getAgreementSummaryColumns() {

        return [ {
            id : 'actions',
            name : 'Actions',
            field : 'actions',
            formatter : _actionsFormatter,
            type : 'text',
            filter : false,
            ignoreInColumnSelection:false,
            excelDataFormatter: _linkExcelFormatter,
            sortable : true
        },{
            id : "agreementTypeId",
            name : "Agreement Type Id",
            field : "agreementTypeId",
            type : "text",
                formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:true,
            headerCssClass : "b",
            minWidth : 30
        },{
            id : "workflowStatus",
            name : "Workflow Status",
            field : "workflowStatus",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            hide:true,
            ignoreInColumnSelection:true,
            excelDataFormatter: _linkExcelFormatter,
            headerCssClass : "b",
            minWidth : 20
        },{
            id : "workflowStatusIcon",
            name : "",
            field : "workflowStatus",
            type : "text",
            formatter : _workflowStatusIconFormatter,
            filter : false,
            sortable : true,
            ignoreInColumnSelection:false,


            //formatter : _applicableColumnIdentifier,
            comparator : function(a, b) {
              var rank1,rank2;
              if(a.workflowStatus == "NOT_STARTED") {
              rank1 = _getRank(a.ragCategory);
            }
            else {
              rank1 = _getRank(a.workflowStatus);
            }
            if(b.workflowStatus == "NOT_STARTED") {
              rank2 =_getRank(b.ragCategory);
            }
            else {
              rank2 = _getRank(b.workflowStatus);
            }



            return (rank2 - rank1);
                              },
            excelDataFormatter: _linkExcelFormatter,
            headerCssClass : "b",
            width : 25
        },{
            id : "ragStatusDisplayText",
            type : "text",
            toolTip : "Rag Status",
            name : "Rag Status",
            formatter:_publishedRowFormatter,
            field : "ragStatusDisplayText",
            sortable : true,
            ignoreInColumnSelection:false,
            headerCssClass : "aln-rt b",
            width : 120
        },{
            id : "tag",
            name : "Tag",
            field : "tag",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:false,
            headerCssClass : "b",
            width : 60
        },{
            id : "agreementType",
            name : "Agreement Type",
            field : "agreementType",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:false,
            headerCssClass : "b",
            minWidth : 120
        }, {
            id : "date",
            name : "Date",
            field : "date",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:true,
            headerCssClass : "b",
            minWidth : 30
        }, {
            id : "legalEntity",
            name : "Legal Entity",
            field : "legalEntity",
            toolTip : "Legal Entity",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:false,
            headerCssClass : "b",
            minWidth : 120
        }, {
            id : "legalEntityId",
            name : "Legal Entity Id",
            field : "legalEntityId",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:true,
            headerCssClass : "b",
            minWidth : 40
        }, {
            id : "exposureCounterParty",
            name : "Exposure Counterparty",
            field : "exposureCounterParty",
            toolTip : "Exposure Counterparty",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:false,
            headerCssClass : "b",
            minWidth : 120
        }, {
            id : "exposureCounterPartyId",
            name : "Exposure Counter Party Id",
            field : "exposureCounterPartyId",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:true,
            headerCssClass : "b",
            minWidth : 40
        }, {
            id : "reportingCurrency",
            name : "Reporting Currency",
            field : "reportingCurrency",
            toolTip : "Reporting Currency",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            ignoreInColumnSelection:false,
            headerCssClass : "b",
            width : 60
        },
        ...getColumnsForUsdAndRcValues("adjustedExposure", "Exposure", true),
        ...getColumnsForUsdAndRcValues("adjustedRequirement", "Margin", true),
        ...getColumnsForUsdAndRcValues("adjustedRegMargin", "Reg Margin", true),
        ...getColumnsForUsdAndRcValues("adjustedHouseMargin", "House Margin", true),
        ...getColumnsForUsdAndRcValues("adjustedCollateral", "Collateral", true),
        ...getColumnsForUsdAndRcValues("adjustedCashCollateral", "Cash Collateral", true),
        ...getColumnsForUsdAndRcValues("adjustedPhysicalCollateral", "Securities Collateral", true),
        ...getColumnsForUsdAndRcValues("adjustedExcessDeficit", "E/D", false),
        ...getColumnsForUsdAndRcValues("adjustedRegExcessDeficit", "Reg E/D", false),
        ...getColumnsForUsdAndRcValues("adjustedHouseExcessDeficit", "House E/D", false),
        ...getColumnsForUsdAndRcValues("brokerExposure", "Broker Exposure", true),
        ...getColumnsForUsdAndRcValues("brokerRequirement", "Broker Margin", true),
        ...getColumnsForUsdAndRcValues("brokerCollateral", "Broker Collateral", false),
        ...getColumnsForUsdAndRcValues("brokerCashCollateral", "Broker Cash Collateral", false),
        ...getColumnsForUsdAndRcValues("brokerPhysicalCollateral", "Broker Securities Collateral", false),
        ...getColumnsForUsdAndRcValues("brokerExcessDeficit", "Broker E/D", true),
        ...getColumnsForUsdAndRcValues("roundedBrokerExcessDeficit", "Rounded Broker E/D", true),
        ...getColumnsForUsdAndRcValues("edDiff", "E/D Diff", false),
        {
            id : "wireRequest",
            name : "Wire Request",
            field : "wireRequest",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            headerCssClass : "b",
            minWidth : 40
        },
        {
            id : "bookingReference",
            name : "Booking Reference",
            field : "bookingReference",
            type : "text",
            formatter:_ptgBookingReferenceFormatter,
            filter : true,
            sortable : true,
            headerCssClass : "b",
            minWidth : 40
        },
        ...getColumnsForUsdAndRcValues("asm", "Actual Securities Movement", true),
        ...getColumnsForUsdAndRcValues("segASM", "Seg Actual Securities Movement", true),
        ...getColumnsForUsdAndRcValues("ecm", "Expected Cash Movement", false),
        ...getColumnsForUsdAndRcValues("segECM", "Seg Expected Cash Movement", false),
        ...getColumnsForUsdAndRcValues("adjustedUsage", "Usage", false),
        {
            id : "comments",
            type : "text",
            formatter:_publishedRowFormatter,
            toolTip : "Comments",
            name : "Comments",
            field : "comments",
            sortable : true,
            ignoreInColumnSelection:false,
            headerCssClass : "aln-rt b",
        },
        ...getColumnsForUsdAndRcValues("collateralDiff", "Colleteral Diff", true),
        {
            id : "dayPnlUsd",
            type : "number",
            toolTip : "Day Pnl USD",
            formatter : _publishedRowNumberFormatter,
            aggregator : dpGrid.Aggregators.sum,
            name : "Day Pnl USD",
            field : "dayPnlUsd",
            sortable : true,
            ignoreInColumnSelection:true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        },
        ...getColumnsForUsdAndRcValues("dayOnDayCollDiff", "Day on Day Coll Diff", true),
        ...getColumnsForUsdAndRcValues("dayOnDayEDDiff", "Day on Day E/D Diff", true),
        ...getColumnsForUsdAndRcValues("dayOnDayExpDiff", "Day on Day Exp Diff", true),
        ...getColumnsForUsdAndRcValues("dayOnDayReqDiff", "Day on Day Req Diff", true),
        ...getColumnsForUsdAndRcValues("disputeAmount", "Dispute Amount", true),
        ...getColumnsForUsdAndRcValues("segDisputeAmount", "Seg Dispute Amount", true),
        ...getColumnsForUsdAndRcValues("prevDayAdjustedCollateral", "Prev Day Collateral", true),
        ...getColumnsForUsdAndRcValues("prevDayAdjustedExcessDeficit", "Prev Day E/D", true),
        ...getColumnsForUsdAndRcValues("prevDayAdjustedExposure", "Prev Day Exp", true),
        ...getColumnsForUsdAndRcValues("prevDayAdjustedRequirement", "Prev Day Req", true),
        ...getColumnsForUsdAndRcValues("brokerRegCollateral", "Reg Broker Collateral", true),
        ...getColumnsForUsdAndRcValues("brokerRegCashCollateral", "Reg Broker Cash Collateral", true),
        ...getColumnsForUsdAndRcValues("brokerRegPhysicalCollateral", "Reg Broker Securities Collateral", true),
        ...getColumnsForUsdAndRcValues("brokerRegExcessDeficit", "Reg Broker E/D", true),
        ...getColumnsForUsdAndRcValues("brokerRegExposure", "Reg Broker Exp", true),
        ...getColumnsForUsdAndRcValues("brokerRegRequirement", "Reg Broker Req", true),
        ...getColumnsForUsdAndRcValues("regEDDiff", "Reg E/D Diff", true),
        ...getColumnsForUsdAndRcValues("brokerSegCollateral", "Seg Broker Coll", true),
        ...getColumnsForUsdAndRcValues("brokerSegCashCollateral", "Seg Broker Cash Coll", true),
        ...getColumnsForUsdAndRcValues("brokerSegPhysicalCollateral", "Seg Broker Securities Coll", true),
        ...getColumnsForUsdAndRcValues("brokerSegExcessDeficit", "Seg Broker E/D", true),
        ...getColumnsForUsdAndRcValues("brokerSegMargin", "Seg Broker Req", true),
        ...getColumnsForUsdAndRcValues("adjustedSegCollateral", "Seg Collateral", true),
        ...getColumnsForUsdAndRcValues("adjustedSegCashCollateral", "Seg Cash Collateral", true),
        ...getColumnsForUsdAndRcValues("adjustedSegPhysicalCollateral", "Seg Securities Collateral", true),
        ...getColumnsForUsdAndRcValues("segCollateralDiff", "Seg Collateral Diff", true),
        ...getColumnsForUsdAndRcValues("adjustedSegExcessDeficit", "Seg E/D", true),
        ...getColumnsForUsdAndRcValues("segExcessDeficitDiff", "Seg E/D Diff", true),
        /*{
            id : "segExpectedCashMovement",
            type : "number",
            toolTip : "Seg Expected Cash Movement",
            formatter : _publishedRowNumberFormatter,
            aggregator : dpGrid.Aggregators.sum,
            name : "Seg Expected Cash Movement",
            field : "segExpectedCashMovement",
            sortable : true,
            ignoreInColumnSelection:true,
            headerCssClass : "aln-rt b",
            excelFormatter : "#,##0"
        },*/
        ...getColumnsForUsdAndRcValues("adjustedSegMargin", "Seg Req", true),
        ...getColumnsForUsdAndRcValues("segReqDiff", "Seg Req Diff", true),
        ...getColumnsForUsdAndRcValues("adjustedRegCollateral", "Reg Collateral", true),
        ...getColumnsForUsdAndRcValues("adjustedRegExposure", "Reg Exposure", true),
        ...getColumnsForUsdAndRcValues("reconMarketValue", "Recon Market Value", true),
        ...getColumnsForUsdAndRcValues("reconLongMarketValue", "Recon Long Market Value", true),
        ...getColumnsForUsdAndRcValues("reconShortMarketValue", "Recon Short Market Value", true),
        ...getColumnsForUsdAndRcValues("reconMargin", "Recon Margin", true),
        ...getColumnsForUsdAndRcValues("reconSegMargin", "Recon Seg Margin", true),
        ...getColumnsForUsdAndRcValues("reconExposure", "Recon Exposure", true),
        {
            id : "ragStatus",
            type : "text",
            formatter:_publishedRowFormatter,
            toolTip : "ragStatus",
            name : "Rag Status Id",
            field : "ragStatus",
            sortable : true,
            ignoreInColumnSelection:true,
            headerCssClass : "aln-rt b",
        }, {
            id : "ragCategory",
            type : "text",
            formatter:_publishedRowFormatter,
            toolTip : "ragCategory",
            name : "Rag Category",
            field : "ragCategory",
            comparator : function(a, b) {
              var rank1,rank2;

              rank1 = _getRank(a.ragCategory);
              rank2 =_getRank(b.ragCategory);

            return (rank2 - rank1);
                              },
            ignoreInColumnSelection:true,
            sortable : true,
            hide : true,
        },{
            id : "ragStatusCode",
            type : "text",
            formatter:_publishedRowFormatter,
            toolTip : "ragStatusCode",
            name : "Rag Status Code",
            field : "ragStatusCode",
            sortable : true,
            ignoreInColumnSelection:true,
            headerCssClass : "aln-rt b",
        },{
            id : 'runBrokerAndRagSync',
            name : 'Run Broker and Rag Sync',
            field : 'runBrokerAndRagSync',
            type : 'text',
            formatter:_runBrokerAndRagSyncFormatter,
            ignoreInColumnSelection:false,
            excelDataFormatter: _linkExcelFormatter,
            sortable : true
        },{
            id : "agreementId",
            name : "Agreement Id",
            field : "agreementId",
            type : "text",
            formatter:_publishedRowFormatter,
            filter : true,
            sortable : true,
            hide : false,
            ignoreInColumnSelection:false,
            headerCssClass : "b",
            minWidth : 60
        } ];
    }

    function _applicableColumnIdentifier(row,cell,value,columnnDef,dataContext) {
        if(columnnDef.field == "workflowStatusIcon") {

            if(dataContext.workflowStatus == "NOT_STARTED") {
                if (dataContext.ragCategory == 'CRITICAL') {
                    return '<div class="highlight-row--critical" style="height:100%">'+value+'</div>';
                    //meta["cssClasses"] = "highlight-row--critical";
                } else if (dataContext.ragCategory == 'WARNING') {
                    return '<div class="highlight-row--warning" style="height:100%">'+value+'</div>';
                    //meta["cssClasses"] = "highlight-row--warning";
                } else if (dataContext.ragCategory == 'CLEAR') {
                    return '<div class="highlight-row--clear" style="height:100%">'+value+'</div>';
                    //meta["cssClasses"] = "highlight-row--clear";
                } else if (dataContext.ragCategory == 'BROKER_DATA_MISSING') {
                    return '<div class="highlight-row--missing" style="height:100%">'+value+'</div>';
                    //meta["cssClasses"] = "highlight-row--missing";
                }
            } else {
                return value;
            }
    }
    }

    function _linkExcelFormatter(row,cell,value,columnnDef,dataContext) {
        cell.actions = "";
    }

    function SummationAggregator(field) {
        this.field_ = field;

        this.init = function() {
          this.sum_ = null;
        };
        this.accumulate = function(item) {
          var val = item[this.field_];
          if (val != null && val !== "" && !isNaN(val)) {
            this.sum_ += parseFloat(val);
          }
        };

        this.storeResult = function(groupTotals) {
          if (!groupTotals.sum) {
            groupTotals.sum = {};
          }
          groupTotals.sum[this.field_] = this.sum_;
        };
      }
    function _publishedRowNumberFormatter(row, cell, value, columnDef, dataContext) {

        var result = treasury.comet.Util.plainNumberFormatter(row, cell, value, columnDef, dataContext);
        if(result != 'n/a'){
              if (dataContext.workflowStatus == "PUBLISHED") {
                  return "<div style=\"text-align:right;font-style:italic;\"><span class='highlight-published'>" +
                          result + "</span></div>";
              }
              else {
                  return "<div style=\"text-align: right;\"><span>" + result + "</span></div>";
              }
              }
              else {
                if (dataContext.workflowStatus == "PUBLISHED") {
              return '<div style=\"text-align: right;font-style:italic;\"><span class="message highlight-published">n/a</span></div>';
            }
            else {
              return '<div style=\"text-align: right;\"><span class="message">n/a</span></div>';
            }
              }
      }

    function _runBrokerAndRagSyncFormatter() {

             return '<a id="runBrokerAndRagSync" title="Run Broker and Rag Sync" href="#" >Run Broker and Rag Sync</a>';

    }

    function _ptgBookingReferenceFormatter(row, cell, value, columnnDef, dataContext) {
      let url = CODEX_PROPERTIES["com.arcesium.treasury.ptg.url"].myValue + "/searchInstructions?instructionGroupId=" + value
      return value? '<a href="' + url + '" target="_blank">' + value + '</a>': '';
    }

    function _publishedRowFormatter(row,cell,result,columnDef,dataContext) {


        if(result != 'n/a' && result != undefined && result != "" && result != null){
              if (dataContext.workflowStatus == "PUBLISHED") {
                  return "<div style=\"text-align:left;font-style:italic;\"><span class='highlight-published'>" +
                          result + "</span></div>";
              }
              else {
                  return "<div style=\"text-align: left;\"><span>" + result + "</span></div>";
              }
              }
              else {
                if (dataContext.workflowStatus == "PUBLISHED") {
              return '<div style=\"text-align: left;font-style:italic;\"><span class="message highlight-published">n/a</span></div>';
            }
            else {
              return '<div style=\"text-align: left;\"><span class="message">n/a</span></div>';
            }
              }
      }

      function _workflowStatusIconFormatter (row, cell, value, columnDef, dataContext) {
        var workflowStatusIcon;
        if(dataContext.workflowStatus != undefined){
             if(dataContext.workflowStatus == "PUBLISHED") {
                 workflowStatusIcon = '<i class="icon-success" aria-hidden="true"></i>'
             } else if(dataContext.workflowStatus == "IN_PROGRESS" || dataContext.workflowStatus == "SAVED") {
                 workflowStatusIcon = '<i class="icon-locked" aria-hidden="true"></i>'
             } else if(dataContext.workflowStatus == "NOT_STARTED") {

                if (dataContext.ragCategory == 'CRITICAL') {
                    workflowStatusIcon =  '<div class="highlight-row--critical" style="height:100%"></div>';

                } else if (dataContext.ragCategory == 'WARNING') {
                    workflowStatusIcon =  '<div class="highlight-row--warning" style="height:100%"></div>';

                } else if (dataContext.ragCategory == 'CLEAR') {
                    workflowStatusIcon =  '<div class="highlight-row--clear" style="height:100%"></div>';

                } else if (dataContext.ragCategory == 'BROKER_DATA_MISSING') {
                    workflowStatusIcon =  '<div class="highlight-row--missing" style="height:100%"></div>';

                }

         }
     }
     return workflowStatusIcon;
      }

      function _actionsFormatter (row, cell, value, columnDef, dataContext) {
        // Initing actions column
        var actions =
        '<a id="beginWorkflow_"  title="Begin Workflow" ><i class="icon-edit--block"></i></a>'+' '
        + ((dataContext.workflowStatus ==  undefined || dataContext.workflowStatus ==  "" ||
        dataContext.workflowStatus == "NOT_STARTED")?
        (' <a href="#" title="Quick Publish"  ><i class="icon-quick margin--left"></i></a>'+' '
        + ' <a href="#" title="Log Wire and Quick Publish"    ><i class="icon-transfer margin--left"></i></a>'+' '
        + ' <a href="#" title="Email Call Report" ><i class="icon-mail margin--left"></i></a>'):'');

        return actions;
      }


    function _groupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
        var value = "";
        if (totals && totals.sum) {
            value = totals.sum[columnDef.field];
          }
        return isExportToExcel ? value : _publishedRowNumberFormatter(null, null, value, columnDef, null);
      }
      function _getRank(code){
        switch(code)
        {

          case "CRITICAL" :return 7;break;
          case "WARNING" :return 6;break;
          case "BROKER_DATA_MISSING" :return 5;break;
          case "NO_RULES_FOUND" :return 4;break;
          case "CLEAR" :return 3;break;
          case "IN_PROGRESS" : return 2;break;
          case "PUBLISHED" :return 1;break;
        }
      }

})();
