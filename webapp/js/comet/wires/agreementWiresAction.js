'user strict';

(function () {
  window.treasury = window.treasury || {};
  window.treasury.comet = window.treasury.comet || {};
  window.treasury.comet.agreementWiresAction = {
    showPostCollateralPopup: _showPostCollateralPopup,
    quickLogWire: _quickLogWire,
    getEDSummaryAndDisplayWirePopup: _getEDSummaryAndDisplayWirePopup,
    setPage: _setPage,
    setTag: _setTag,
    setAccountType: _setAccountType,
    setACM: _setACM,
    setActionableAgreementId: _setActionableAgreementId,
    setTripartyAgreementId: _setTripartyAgreementId,
    setCustodianAccountId: _setCustodianAccountId,
    getCustodianAccountId: _getCustodianAccountId,
    setAgreementWorkflowParam: _setAgreementWorkflowParam,
    getAgreementIdToWireIdsMap: _getAgreementIdToWireIdsMap,
    setReportingCurrencyIsoCode: _setReportingCurrencyIsoCode,
    setReportingCurrencySpn: _setReportingCurrencySpn,
    setReportingCurrencyFxRate: _setReportingCurrencyFxRate,
    roundValue: _roundValue,
    setInternalVsExternalData: _setInternalVsExternalData,
    getInternalVsExternalData: _getInternalVsExternalData,
    setActionableDate: _setActionableDate
  };

  var workflowId;

  // Hold the references to the filters in wire pop-up
  var sourceWiresPortfolioSingleSelect;
  var destinationWiresPortfolioSingleSelect;
  var sourceAccountsSingleSelect;
  var destinationAccountsSingleSelect;
  var actionableAgreementId;
  var tripartyAgreementId;
  var page;
  var tag;
  var accountType;
  var acm;
  var agreementWorkflowParam;
  var agreementIdToWireIdsMap;
  var custodianAccountId;
  var reportingCurrencyIsoCode;
  var reportingCurrencySpn;
  var reportingCurrencyFxRate;
  var intVsExtData;
  var actionableDate;

  /**
   * Quick Log Wire -> Begin the Workflow, Get the ED Summary for the
   * Agreement and launch the Log Wire Popup
   */
  function _quickLogWire(params) {

    // Get Workflow Params
    var workflowParams = window.treasury.comet.agreementWorkflowAction.getWorkflowParam(params);

    // Begin Workflow with async false
    var promise = window.treasury.comet.agreementWorkflowAction.beginWorkflow(workflowParams, false);

    promise.done(function (data) {
      // Get ED summary info to update the wire amount and get the wires portfolio
      var workflowId = data.resultList[0].workflowId;
      _showPostCollateralPopup(workflowId);
    });
  }

  function _showPostCollateralPopup(workflowId) {

    ReactDOM.unmountComponentAtNode(document.querySelector('#postCollateralPopup'));
    ReactDOM.render(
      React.createElement(TreasuryComponents.PostCollateralPopupComponent, {
        onClickPostSecurities: (onClickPostSecurities = () => {
          _displayPhysicalCollateralInventory();

        }),
        onClickPostCash: (onClickPostCash = () => {
          _getEDSummaryAndDisplayWirePopup(workflowId);
        })
      }),
      document.querySelector('#postCollateralPopup')
    );
  }

  function _displayPhysicalCollateralInventory() {
    let agreementId = actionableAgreementId;
    if (!isNaN(tripartyAgreementId) && accountType === 'IM') {
      agreementId = tripartyAgreementId;
    }

    let collateralTermUrl = '/treasury/service/collateralTermService/getCollateralTerms';

    let collateralTermParam = 'lcmPositionDataFilter.agreementIds=' + [agreementId];
    collateralTermParam += '&inputFormat=PROPERTIES&format=json';

    var promise = $.ajax({
      url: collateralTermUrl,
      data: collateralTermParam,
      dataType: 'json',
      type: 'GET'
    });

    promise.done(function (data) {
      let collateralTerm = _getRelevantCollateralTerm(data);
      if (!collateralTerm || !collateralTerm.physicalCollateralTerm) {
        alert("Could not find physical collateral term for selected agreement")
        return;
      }
      let intVsExtData = _getInternalVsExternalData();

      let physicalCollateralInputParam = {
        '@CLASS': 'com.arcesium.treasury.model.lcm.physicalcollateral.PhysicalCollateralInputParam',
        date: actionableDate,
        agreementId: agreementId,
        accountType: accountType,
        autoSelectionInputParam: {
          '@CLASS': 'com.arcesium.treasury.model.lcm.physicalcollateral.autochooser.PhysicalCollateralAutoSelectionInputParam',
          actualSecurityMovement: acm,
          applyOptimizationRules: false
        }
      };

      _fetchInventoryPositionData(physicalCollateralInputParam);
    });
  }

  function validateValue(value) {
    return value === 'n/a' ? null : value;
  }

  function _getRelevantCollateralTerm(collateralTermList) {
    let accountTypeId;
    if (accountType == "VM_PLUS_IM") {
      accountTypeId = 1;
    } else if (accountType == "IM") {
      accountTypeId = 2;
    } else if (accountType == "VM") {
      accountTypeId = 3;
    } else if (accountType == "SIMM_LE") {
      accountTypeId = 4;
    } else if (accountType == "SIMM_CPE") {
      accountTypeId = 5;
    }

    return collateralTermList.find(item => item.accountTypeId === accountTypeId);
  }

  function _fetchInventoryPositionData(physicalCollateralInputParam) {
    let intVsExtData = _getInternalVsExternalData();

    let eligibleInventoryPositionsUrl = '/treasury/service/physicalCollateralWorkflowManager/getPhysicalCollateralAgreementDataView';
    let eligibleInventoryPositionParam = 'physicalCollateralInputParam=' + encodeURIComponent(JSON.stringify(physicalCollateralInputParam));
    eligibleInventoryPositionParam += '&inputFormat=json&format=json';

    document.getElementById('physicalCollateralInventoryLoader').removeAttribute('hidden');

    let promise = $.ajax({
      url: eligibleInventoryPositionsUrl,
      data: eligibleInventoryPositionParam,
      dataType: 'json',
      type: 'GET'
    });

    promise.done(function (data) {
      document.getElementById('physicalCollateralInventoryLoader').setAttribute('hidden', true);
      ReactDOM.unmountComponentAtNode(document.querySelector('#physicalCollateralInventory'));
      ReactDOM.render(
        React.createElement(TreasuryComponents.PhysicalCollateralInventoryGrid, {
          stagedPhysicalCollateralAgreementDataId: data.stagedPhysicalCollateralAgreementDataId,
          physicalCollateralPositionData: data.physicalCollateralPositionDataView,
          inventorySettleDates: data.inventorySettleDates,
          date: actionableDate,
          cometData: intVsExtData,
          autoSelectionResult: data.autoSelectionResult,
          physicalCollateralInputParam: physicalCollateralInputParam,
          updateBookingReferenceId: updatePhysicalCollateralSuccessForBookingReferenceId
        }),
        document.querySelector('#physicalCollateralInventory')
      );
    });

    promise.fail(function (xhr, text, errorThrown) {
      document.getElementById('physicalCollateralInventoryLoader').setAttribute('hidden', true);
      alert("Error occurred while getting inventory positions");
      console.log("Error occurred while getting inventory positions");
    });
  }

  function updatePhysicalCollateralSuccessForBookingReferenceId(bookingReferenceId) {
    if (page != undefined) {
      updateIfAgreementWorkflowPage(bookingReferenceId)
      updateIfAgreementSummaryPage(bookingReferenceId)
    }
  }

  function updateIfAgreementWorkflowPage(bookingReferenceId) {
    if (page == 'AgreementWorkflow') {
      var workflowTopGrid = window.treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();
      var agreementWorkflowData = workflowTopGrid.data;
      if (agreementWorkflowData != null && agreementWorkflowData.length) {
        var actionableKey = actionableAgreementId + tag;
        for (var i = 0; i < agreementWorkflowData.length; i++) {
          var agId = Math.abs(agreementWorkflowData[i].agreementId);
          var tagVal = agreementWorkflowData[i].tag;
          var agKey = agId + tagVal;
          if (actionableKey === agKey) {
            if (agreementWorkflowData[i].bookingReference == undefined || agreementWorkflowData[i].bookingReference  == "") {
              agreementWorkflowData[i].bookingReference  = "".concat(bookingReferenceId);
            } else {
              agreementWorkflowData[i].bookingReference = agreementWorkflowData[i].bookingReference.concat(',').concat(bookingReferenceId)
            }
          }
        }
        workflowTopGrid.refreshGrid(workflowTopGrid.data, null, null, null, false);
      }
    }
  }

  function updateIfAgreementSummaryPage(bookingReferenceId) {
    if (page != 'AgreementWorkflow' && agreementWorkflowParam != undefined) {
      if (confirm("Do you want to reload this screen?") == true) {
        let agreementDetail = {
          "workflowStatus": "PUBLISHED",
          "bookingReference": bookingReferenceId,
          "segECMRC": 0,
          "disputeAmountRC": 0,
          "segDisputeAmountRC": 0
        };
        let agreementDetails = {};
        agreementDetails[actionableAgreementId] = agreementDetail;

        window.treasury.comet.agreementsummary.reloadAgreementSummary(true, agreementDetails);
      }
    };
  }

  function _getEDSummaryAndDisplayWirePopup(workflowId) {

    var edSummaryUrl = "/treasury/comet/getEdSummaryForWire";
    var edSummaryDataParam = "workflowId=" + workflowId;
    edSummaryDataParam += "&actionableAgreementId=" + actionableAgreementId;
    edSummaryDataParam += "&custodianAccountId=" + custodianAccountId;

    var promise = $.ajax({
      url: edSummaryUrl,
      data: edSummaryDataParam,
      dataType: "json",
      type: "POST"
    });

    promise.done(function (data) {

      var wirePortfolios = data.resultList[0];
      _setActionableAgreementId(wirePortfolios.actionableAgreementId);
      _setCustodianAccountId(wirePortfolios.custodianAccountId);

      _displayWirePopup(wirePortfolios.wirePortfolios, workflowId);

      if ($("workflowId") != null) {
        $("workflowId").value = workflowId;
      }
    });
  }

  /**
   * Initialize the and display the log wire popup
   */
  function _displayWirePopup(wiresPortfolioData, workflowId) {

    var date = new Date();

    var displayWireUrl = "/treasury/comet/displayLogWirePopup";

    var displayWireParam = "workflowId=" + workflowId;
    displayWireParam += "&actionableAgreementId=" + actionableAgreementId;
    displayWireParam += "&custodianAccountId=" + custodianAccountId;
    displayWireParam += "&tripartyAgreementId=" + tripartyAgreementId;

    var promise = $.ajax({
      url: displayWireUrl,
      data: displayWireParam,
      dataType: "json",
      type: "POST"
    });

    $.when(promise, treasury.loadfilter.defaultDates()).done(function (dataList, defaultDatesData) {
      var data = dataList[0];
      date = defaultDatesData[0].tFilterDate;



      // Setting values to properties and revealing Log Wire popup
      var subject = document.getElementById("subject");
      var subscribers = document.getElementById("subscribers");
      var cc = document.getElementById("cc");
      var comments = document.getElementById("comments");

      if (acm == undefined) {
        acm = 0;
      }

      var acmVal = Number(acm);
      document.getElementById("finalWireAmount").value = treasury.formatters.formatNumberUsingCommas(Math
        .abs(acmVal));
      document.getElementById("wireAmount").value = treasury.formatters.formatNumberUsingCommas(Math
        .abs(acmVal));

      var reportingCurrencySpanList = document.getElementsByClassName("wireReportingCurrencyIsoCode");
      var reportingCurrency = reportingCurrencyIsoCode || "USD";

      for (var i = 0; i < reportingCurrencySpanList.length; i++) {
        reportingCurrencySpanList[i].innerHTML = " (" + reportingCurrency + ")";
      }

      var subjectVal = data.resultList[0].wireRequest.subject;

      var acmSign = 1;
      if (acmVal < 0) {
        acmSign = -1;
        subjectVal += " - Margin Pull";
      } else {
        acmSign = 1;
        subjectVal += " - Margin Call";
      }
      subject.value = subjectVal;

      subscribers.value = data.resultList[0].wireRequest.subscribers;
      cc.value = data.resultList[0].wireRequest.cc;
      comments.value = data.resultList[0].wireRequest.comments;

      document.addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;
        if (key === 13) { // 13 is enter
          if (document.querySelector('#logWirePopup').visible == true) {
            document.getElementById('logWirePopup').shadowRoot.querySelector('.wire').disabled = true;
            _logWireRequest(workflowId, acmSign);
          }
        }
      });

      document.getElementById('finalWireAmount').addEventListener('keyup', function (e) {
        var key = e.which || e.keyCode;
        if (key != 110 && key != 190 && key != 17 && !e.ctrlKey) {
          var inputEl = document.getElementById('finalWireAmount'),
            value = inputEl.value;

          value = value.replace(/,/g, "").replace(/x/g, "");
          value = treasury.formatters.formatNumberUsingCommas(value);

          var newCursorStart = treasury.getCursorPosition(inputEl.value, value, inputEl.selectionStart)
          var newCursorEnd = treasury.getCursorPosition(inputEl.value, value, inputEl.selectionEnd)

          inputEl.value = value;
          inputEl.selectionStart = newCursorStart;
          inputEl.selectionEnd = newCursorEnd;

        }


      });

      document.getElementById('logWirePopup').reveal({
        'title': 'COMET - Log wires',
        'modal': true,
        'sticky': true,
        'moveable': true,
        'buttons': [{
          'html': 'Log Selected Wires',
          'class': 'wire',
          'callback': function () {
            this.shadowRoot.querySelector('.wire').disabled = true;
            _logWireRequest(workflowId, acmSign);
          },
          'position': 'center'
        }, {
          'html': 'Cancel',
          'callback': function () {
            _cancelWorkflowAndWirepopup(workflowId);
          },
          'position': 'center'
        }]
      });


      $("#wireDate").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        minDate: Date.parse('1970-01-01'),
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>"
      }).datepicker("setDate", date);

      $("#valueDate").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        minDate: Date.parse('1970-01-01'),
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>"
      }).datepicker("setDate", date);


      var wiresPortfolioList = [];
      for (var i = 0; i < wiresPortfolioData.length; i++) {

        wiresPortfolioList
          .push({
            0: wiresPortfolioData[i].myId,
            1: wiresPortfolioData[i].myAbbreviation
          });

      }

      document.getElementById('bookMmf').checked = false;
      document.getElementById('trackOnlyWire').checked = false;

      // Initializing Wires Portfolio filters
      sourceWiresPortfolioSingleSelect = new window.treasury.filter.SingleSelect(
        "sourcePortfolio", "sourcePortfolio",
        wiresPortfolioList, []);

      destinationWiresPortfolioSingleSelect = new window.treasury.filter.SingleSelect(
        "destinationPortfolio", "destinationPortfolio",
        wiresPortfolioList, []);

      // Initializing source accounts and target accounts filters
      var cpeInstructionsList = [];
      if (data.resultList[0].cpeWireInstructions != null
        && data.resultList[0].cpeWireInstructions.length >= 1) {

        for (var i = 0; i < data.resultList[0].cpeWireInstructions.length; i++) {

          cpeInstructionsList
            .push({
              0: data.resultList[0].cpeWireInstructions[i].instructionId,
              1: data.resultList[0].cpeWireInstructions[i].instructionId
                + '-'
                + data.resultList[0].cpeWireInstructions[i].bankDisplayName
            })

        }

      }

      var legalWireInstructionsList = [];
      if (data.resultList[0].legalWireInstructions != null
        && data.resultList[0].legalWireInstructions.length >= 1) {

        for (var i = 0; i < data.resultList[0].legalWireInstructions.length; i++) {

          legalWireInstructionsList
            .push({
              0: data.resultList[0].legalWireInstructions[i].instructionId,
              1: data.resultList[0].legalWireInstructions[i].instructionId
                + '-'
                + data.resultList[0].legalWireInstructions[i].bankDisplayName
            })

        }

      }

      // Initializing Account Filter

      var acmNum = Number(acm);
      if (acmNum < 0) {
        destinationAccounts = new window.treasury.filter.SingleSelect(
          "destinationAccount", "destinationAccount",
          legalWireInstructionsList, []);

        if (legalWireInstructionsList.length == 1) {
          var key = legalWireInstructionsList[0][0];
          destinationAccounts.setSelectedNode(key);
        }

        sourceAccounts = new window.treasury.filter.SingleSelect(
          "sourceAccount", "sourceAccount",
          cpeInstructionsList, []);

        if (cpeInstructionsList.length == 1) {
          var key = cpeInstructionsList[0][0];
          sourceAccounts.setSelectedNode(key);
        }
      } else {
        destinationAccounts = new window.treasury.filter.SingleSelect(
          "destinationAccount", "destinationAccount",
          cpeInstructionsList, []);

        if (cpeInstructionsList.length == 1) {
          var key = cpeInstructionsList[0][0];
          destinationAccounts.setSelectedNode(key);
        }

        sourceAccounts = new window.treasury.filter.SingleSelect(
          "sourceAccount", "sourceAccount",
          legalWireInstructionsList, []);

        if (legalWireInstructionsList.length == 1) {
          var key = legalWireInstructionsList[0][0];
          sourceAccounts.setSelectedNode(key);
        }
      }
    }).fail(function (xhr, text, errorThrown) {

      alert("Error occurred while fetching wire popup details");
      console.log("Error occurred while fetching wire popup details");

    });
  }

  /**
   * Logs the wire request
   */
  function _logWireRequest(workflowId, acmSign) {

    // Service to log wire
    var logWireUrl = "/treasury/data/logWireRequest";

    // Getting attributes from the Log Wire pop-up
    var subject = document.getElementById('subject').value;
    var subscribers = document.getElementById('subscribers').value;
    var cc = document.getElementById('cc').value;
    var comments = document.getElementById('comments').value;
    var sourceWirePortfolioId = sourceWiresPortfolioSingleSelect.getSelectedId();
    var destinationWiresPortfolioId = destinationWiresPortfolioSingleSelect
      .getSelectedId();
    let isPortfolioMandatoryToLogWire = treasury.defaults.isPortfolioMandatoryToLogWire;
    if ((sourceWirePortfolioId == -1 || sourceWirePortfolioId == undefined || destinationWiresPortfolioId == -1 || destinationWiresPortfolioId == undefined) && isPortfolioMandatoryToLogWire) {
      alert("Source and Destination Portfolio are mandatory");
      document.querySelector('#logWirePopup').shadowRoot.querySelector('button[disabled]').disabled = false;
      return;
    }

    var sourceAccountId = sourceAccounts.getSelectedId();
    var destinationAccountId = destinationAccounts.getSelectedId();
    if (sourceAccountId == -1 || sourceAccountId == undefined || destinationAccountId == -1 || destinationAccountId == undefined) {
      alert("Source and Destination Account are mandatory");
      document.querySelector('#logWirePopup').shadowRoot.querySelector('button[disabled]').disabled = false;
      return;
    }

    var sourceAccount = document.getElementById('sourceAccount').value;
    var destinationAccount = document.getElementById('destinationAccount').value;

    var wireDateVal = document.getElementById('wireDate').value;
    var valueDataVal = document.getElementById('valueDate').value;
    if (wireDateVal == undefined || wireDateVal == "" || valueDataVal == undefined || valueDataVal == "") {
      alert("Wire and Value Date are mandatory");
      document.querySelector('#logWirePopup').shadowRoot.querySelector('button[disabled]').disabled = false;
      return;
    }
    var wireDate = document.getElementById('wireDate').value;
    var valueDate = document.getElementById('valueDate').value;

    var wireAmountUSD = document.getElementById('wireAmount').value;
    var finalWireAmount = document.getElementById('finalWireAmount').value;
    finalWireAmount = finalWireAmount.replace(/,/g, "").replace(/x/g, "");
    if (finalWireAmount == undefined || finalWireAmount == "") {
      alert("Specify an amount");
      document.querySelector('#logWirePopup').shadowRoot.querySelector('button[disabled]').disabled = false;
      return;
    }
    wireAmountUSD = finalWireAmount;

    var bookMmf = document.getElementById('bookMmf').checked;
    var isTrackingWire = document.getElementById('trackOnlyWire').checked;

    if (tag != undefined && tag == "SEG" && !bookMmf) {
      if (!confirm("This is a Seg Ia workflow and you have not selected to book mmf. Do you still wish to continue?") == true) {
        document.querySelector('#logWirePopup').shadowRoot.querySelector('button[disabled]').disabled = false;
        return;
      }
    }
    if (tag != undefined && tag != "SEG" && bookMmf) {
      if (!confirm("This is not a Seg Ia workflow and you have selected to book mmf. MMF will not be booked. " +
        "Do you wish to just log wire ?") == true) {
        document.querySelector('#logWirePopup').shadowRoot.querySelector('button[disabled]').disabled = false;
        return;
      }
      else {
        bookMmf = false;
      }
    }

    // Creating Wire Wrapper and Wire Request
    var wireWrapper = {
      "wire": {
        "sourceAccount": {
          "id": sourceAccountId
        },
        "destinationAccount": {
          "id": destinationAccountId
        },
        "amount": wireAmountUSD,
        "sourcePortfolio": {
          "id": sourceWirePortfolioId
        },
        "destinationPortfolio": {
          "id": destinationWiresPortfolioId
        }
      },
      "bookMmf": bookMmf,
      "wireDateStr": wireDate,
      "valueDateStr": valueDate,
      "isTrackingWire": isTrackingWire,
    };

    if (reportingCurrencySpn) {
      wireWrapper.wire.currencyId = reportingCurrencySpn;
    }

    var wireRequest = {
      "subject": subject,
      "subscribers": subscribers,
      "cc": cc,
      "comments": comments
    };

    if (page == 'AgreementWorkflow') {
      window.treasury.comet.agreementworkflow.showLoading()
    } else {
      window.treasury.comet.agreementsummary.showLoading();
    }

    var promise = $.ajax({
      url: logWireUrl,
      data: JSON.stringify({
        "workflowId": workflowId,
        "actionableAgreementId": actionableAgreementId,
        "wireRequest": wireRequest,
        "wireWrapper": wireWrapper
      }),
      processData: false,
      contentType: "application/json; charset=UTF-8",
      type: "POST"
    });

    promise.done(function (data) {

      if (page == 'AgreementWorkflow') {
        window.treasury.comet.agreementworkflow.hideLoading()
      } else {
        window.treasury.comet.agreementsummary.hideLoading();
      }

      var logMsg = data.resultList[0].logMsg;
      var bookMmf = data.resultList[0].bookMmf;
      var wireRequestNo = data.resultList[0].wireRequestNo;
      var wireStatus = data.resultList[0].status;
      var wireAmount = data.resultList[0].wireAmount;
      var agreementId = data.resultList[0].actionableAgreementId;
      var workflowId = data.resultList[0].workflowId;

      if (logMsg != undefined) {
        alert(logMsg);
      }

      if (page != undefined && page == 'AgreementWorkflow' && bookMmf == true) {
        var bookMmfParam = data.resultList[0];
        bookMmfParam.wireAmount *= acmSign;
        bookMmfParam.mmfAmount *= acmSign;
        bookMmfParam.reportingCurrencyFxRate = reportingCurrencyFxRate;
        bookMmfParam.actionableAgreementId = bookMmfParam.actionableAgreementId
        bookMmfParam.treasuryAgreementId = document.getElementById('actionableAgreementId').value
        window.treasury.comet.bookMmfTrade.displayBookMmfPopup(bookMmfParam);
      }

      if (page != undefined && page == 'AgreementWorkflow' && wireStatus) {
        var workflowTopGrid = window.treasury.comet.agreementWorkflowGridAction.getAgreementWorkflowTopPanelGrid();
        var agreementWorkflowData = workflowTopGrid.data;
        if (agreementIdToWireIdsMap == undefined) {
          agreementIdToWireIdsMap = new Object();
          for (var i = 0; i < agreementWorkflowData.length; i++) {

            var agreementId = agreementWorkflowData[i].agreementId;
            var tagVal = agreementWorkflowData[i].tag;
            var key = agreementId + tagVal;
            if (agreementIdToWireIdsMap[key] == undefined || agreementIdToWireIdsMap[key] == "") {
              agreementIdToWireIdsMap[key] = agreementWorkflowData[i].wireRequest;
            }
            else {
              agreementIdToWireIdsMap[key] = agreementIdToWireIdsMap[key].concat(',').concat(agreementWorkflowData[i].wireRequest);
            }
          }
        }

        var actionableKey = actionableAgreementId + tag;

        if (agreementIdToWireIdsMap[actionableKey] == undefined || agreementIdToWireIdsMap[actionableKey] == "") {
          agreementIdToWireIdsMap[actionableKey] = 'WRS#' + wireRequestNo;
        } else {
          agreementIdToWireIdsMap[actionableKey] = agreementIdToWireIdsMap[actionableKey].concat(',WRS#').concat(wireRequestNo);
        }


        var options = workflowTopGrid.options;
        var columns = workflowTopGrid.columns;
        if (agreementWorkflowData != null && agreementWorkflowData.length) {
          for (var i = 0; i < agreementWorkflowData.length; i++) {
            var agId = Math.abs(agreementWorkflowData[i].agreementId);
            var tagVal = agreementWorkflowData[i].tag;
            var agKey = agId + tagVal;
            if (agreementIdToWireIdsMap[agKey] != undefined && agreementIdToWireIdsMap[agKey] != "" && actionableKey == agKey) {

              agreementWorkflowData[i].wireRequest = agreementIdToWireIdsMap[agKey];


              agreementWorkflowData[i].acmRC = acmSign * wireAmount;
            }
          }

          // Initiating the Grid
          workflowTopGrid.refreshGrid(workflowTopGrid.data, null, null, null, false);
        }
      }

      if (page != undefined && page != 'AgreementWorkflow' && wireStatus) {
        window.treasury.comet.agreementWorkflowDisputeAmount.updateACMData(agreementId, wireAmount * acmSign);
        if (agreementWorkflowParam != undefined) {
          $.when(window.treasury.comet.agreementWorkflowAction.quickPublishWorkflowOperation(agreementWorkflowParam, "QUICK_WIRE_PUBLISH", agreementId, wireAmount * acmSign, tag))
            .done(function () {
              if (confirm("Logging Wire from summary screen changes the status of the corresponding agreement workflow. Do you want to reload this screen?") == true) {
                let agreementDetail = {
                  "workflowStatus": "PUBLISHED",
                  "wireRequest": 'WRS#' + wireRequestNo,
                  "ecmRC": wireAmount * acmSign,
                  "segECMRC": 0,
                  "disputeAmountRC": 0,
                  "segDisputeAmountRC": 0
                };
                let agreementDetails = {};
                agreementDetails[agreementId] = agreementDetail;

                window.treasury.comet.agreementsummary.reloadAgreementSummary(true, agreementDetails);
              }
            });
        };
      }

      if (page != undefined && page != 'AgreementWorkflow' && !wireStatus) {
        window.treasury.comet.agreementWorkflowAction.cancelWorkflow('CANCELLED', 'Workflow has been cancelled', workflowId);
      }
    });


    promise.fail(function (xhr, text, errorThrown) {

      if (page == 'AgreementWorkflow') {
        window.treasury.comet.agreementworkflow.hideLoading()
      } else {
        window.treasury.comet.agreementsummary.hideLoading();
      }

      alert("Error occurred while logging wire request");
      console.log("Error occurred while logging wire request");
    });


    document.querySelector('#logWirePopup').visible = false;
  }

  function _loadWorkflowData() {
    var agreementWorkflowGrid = window.treasury.comet.agreementWorkflowAction.getAgreementWorkflowTopPanelGrid();
    var options = agreementWorkflowGrid.options;
    var agreementWorkflowData = agreementWorkflowGrid.data;
    var agreementWorkflowColumns = agreementWorkflowGrid.columns;

    if (agreementIdToWireIdsMap != null) {
      for (var i = 0; i < agreementWorkflowData.length; i++) {
        if (agreementIdToWireIdsMap[agreementWorkflowData.agreementId] != undefined) {
          agreementWorkflowData[i].wireRequest = agreementIdToWireIdsMap[agreementWorkflowData.agreementId];
        }
      }
    }

    if (agreementWorkflowData != null && agreementWorkflowData.length) {
      // Initiating the Grid
      _agreementWorkflowTopPanelGrid = new dportal.grid.createGrid(
        $("#agreementWorkflowTopPanelGrid"), agreementWorkflowData, agreementWorkflowColumns,
        options);
    }
  }

  /**
   * On Canceling the popup, the workflow is cancelled and contents of the wire popup are cleared
   */
  function _cancelWorkflowAndWirepopup(workflowId) {

    if (page != 'AgreementWorkflow') {
      window.treasury.comet.agreementWorkflowAction.cancelWorkflow('CANCELLED', 'Workflow has been cancelled', workflowId);
    } else {
      if (!confirm("Do you really want to close popup?") == true) {
        return;
      }
    }


    document.querySelector('#logWirePopup').visible = false;
  }

  function _getPage() {
    return page;
  }

  function _setPage(pageVal) {
    page = pageVal;
  }

  function _getTag() {
    return tag;
  }

  function _setTag(tagVal) {
    tag = tagVal;
  }

  function _setAccountType(accountTypeVal) {
    accountType = accountTypeVal;
  }

  function _getACM() {
    return acm;
  }

  function _setACM(acmVal) {
    acm = acmVal;
  }

  function _getAgreementWorkflowParam() {
    return agreementWorkflowParam;
  }

  function _setAgreementWorkflowParam(agreementWorkflowParamVal) {
    agreementWorkflowParam = agreementWorkflowParamVal;
  }

  function _setActionableAgreementId(actionableAgreementIdVal) {
    actionableAgreementId = actionableAgreementIdVal;
  }

  function _getActionableAgreementId() {
    return actionableAgreementId;
  }

  function _setTripartyAgreementId(tripartyAgreementIdVal) {
    tripartyAgreementId = tripartyAgreementIdVal;
  }

  function _setCustodianAccountId(custodianAccountIdVal) {
    custodianAccountId = custodianAccountIdVal;
  }

  function _getCustodianAccountId() {
    return custodianAccountId;
  }

  function _getAgreementIdToWireIdsMap() {
    return agreementIdToWireIdsMap;
  }

  function _setReportingCurrencyIsoCode(reportingCurrencyIsoCodeVal) {
    reportingCurrencyIsoCode = reportingCurrencyIsoCodeVal;
  }

  function _setReportingCurrencySpn(reportingCurrencySpnVal) {
    reportingCurrencySpn = reportingCurrencySpnVal;
  }

  function _setReportingCurrencyFxRate(reportingCurrencyFxRateVal) {
    reportingCurrencyFxRate = reportingCurrencyFxRateVal;
  }

  function _setInternalVsExternalData(data) {
    intVsExtData = data;
  }

  function _getInternalVsExternalData() {
    return intVsExtData;
  }

  function _setActionableDate(date) {
    actionableDate = date;
  }

  /**
   *
   * Helper method that rounds off Acm and DisputeAmount
   */
  function _roundValue(input, digits) {
    var output = 0;

    if (input > 0) {
      output = ((input % digits) == 0) ? input : input - (input % digits) + digits;
    }
    else if (input < 0) {
      // when acm is -ve, we can't round up as its a margin pull
      output = ((input % digits) == 0) ? input : input - (input % digits);
    }
    return output;
  }

})();
