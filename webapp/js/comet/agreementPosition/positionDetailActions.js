"use strict";

var positionDetailGrid;
var url;

(function () {
  window.treasury = window.treasury || {};
  window.treasury.positionDetail = window.treasury.positionDetail || {};
  window.treasury.positionDetail.positionDetailActions = {
    loadpositionDetailData: _loadpositionDetailDatas
  };
  function _updateId(data) {
    for (var i = 0; i < data.length; i++) {
      data[i].id = i;
    }
  }

  function convertJavaDateDashed(str) {
    if (str === null || str === undefined || str.trim() === "" || str === "Total") return "";
    var res = str.match(/\((.*)\)/);
    var deserializedDate = parseInt(res[1]);
    var date = new Date(deserializedDate),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  function convertJavaDateTime(str) {
    if (str === null || str === undefined || str.trim() === '' || str === 'Total') return '';
    let res = str.match(/\((.*)\)/);
    let deserializedDate = parseInt(res[1]);
    let date = new Date(deserializedDate),
      year = date.getFullYear(),
      mnth = `0${date.getMonth() + 1}`.slice(-2),
      day = `0${date.getDate()}`.slice(-2),
      hours = `0${date.getHours()}`.slice(-2),
      minutes = `0${date.getMinutes()}`.slice(-2),
      seconds = `0${date.getSeconds()}`.slice(-2);
    return `${year}-${mnth}-${day} ${hours}:${minutes}:${seconds}`;
  }

  function isNullOrUndefined(obj) {
    if (obj == null || obj == undefined) {
      return true;
    }
    return false;
  }

  function getCurrDayDayOnDayDiffColumns() {
    return getCurrDayLcmPositionDataColumns() + "," + getCurrDayExposureColumns() + "," + getCurrDayMarginColumns()
      + "," + getCurrDayLcmPositionDetailColumns() + "," + getCurrDayReconColumns()
      + "," + getCurrDayBrokerColumns() + "," + getCurrDayBrokerSecurityColumns();
  }

  function getCurrDayLcmPositionDataColumns() {
    return "lcmPositionData.security.shortDesc,lcmPositionData.settleDateQuantity,lcmPositionData.cusip,lcmPositionData.sedol,lcmPositionData.bloomberg,lcmPositionData.isin,lcmPositionData.treasuryCpId,lcmPositionData.legalEntity,lcmPositionData.exposureCounterparty,lcmPositionData.agreementType,lcmPositionData.countryName,lcmPositionData.marketName,lcmPositionData.reportingCurrency,lcmPositionData.date,lcmPositionData.death,lcmPositionData.currencyAbbrev,lcmPositionData.quantity,lcmPositionData.tickUnit,lcmPositionData.reqHairCut,lcmPositionData.hairCut,lcmPositionData.underlyingSecurity.spn,lcmPositionData.underlyingSecurity.isin,lcmPositionData.underlyingSecurity.sedol,lcmPositionData.underlyingSecurity.cusip,lcmPositionData.financedQuantity,lcmPositionData.unFinancedQuantity,lcmPositionData.security.ric,lcmPositionData.security.local,lcmPositionData.security.foTypeName,lcmPositionData.security.subtypeName,lcmPositionData.security.desname,lcmPositionData.security.gboTickUnit,lcmPositionData.componentKey.spn,lcmPositionData.componentKey.fieldType,lcmPositionData.componentKey.calculator,lcmPositionData.componentKey.gboType.name,lcmPositionData.componentKey.gboTypeId,lcmPositionData.componentKey.exposureCustodianAccount.displayName,lcmPositionData.componentKey.custodianAccount.displayName,lcmPositionData.componentKey.book.displayName";
  }

  function getCurrDayExposureColumns() {
    return "lcmPositionData.exposureDetail.exposureRC,lcmPositionData.exposureDetail.isdaPositionMtmData.resetDate,lcmPositionData.exposureDetail.isdaPositionMtmData.resetSettleDate,lcmPositionData.exposureDetail.isdaPositionMtmData.tradeReturnFromLastResetRC,lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedAIRC,lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedCommissionsRC,lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedCorpactRC,lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedFinancingRC,lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedPriceReturnRC,lcmPositionData.exposureDetail.isdaPositionMtmData.overdueReceivablePayableRC,lcmPositionData.exposureDetail.isdaPositionMtmData.markToMarketRC,lcmPositionData.exposureDetail.repoPositionExposureData.onDate,lcmPositionData.exposureDetail.repoPositionExposureData.offDate,lcmPositionData.exposureDetail.repoPositionExposureData.cleanPriceRC,lcmPositionData.exposureDetail.repoPositionExposureData.dirtyPriceRC,lcmPositionData.exposureDetail.repoPositionExposureData.loanValueRC,lcmPositionData.exposureDetail.repoPositionExposureData.loanAIRC,lcmPositionData.exposureDetail.repoPositionExposureData.bondValueRC,lcmPositionData.exposureDetail.repoPositionExposureData.bondAIRC,lcmPositionData.exposureDetail.repoPositionExposureData.poolFactor,lcmPositionData.exposureDetail.repoPositionExposureData.repoMarginRC,lcmPositionData.exposureDetail.repoPositionExposureData.repoExposureRC,lcmPositionData.exposureDetail.fcmPositionOteData.oteRC";
  }

  function getCurrDayMarginColumns() {
    return "lcmPositionData.marginDetail.marginRC,lcmPositionData.marginDetail.applicableMarginRC,lcmPositionData.marginDetail.applicableFinancedMarginRC,lcmPositionData.marginDetail.applicableUnFinancedMarginRC,lcmPositionData.marginDetail.exchangeFinancedMarginRC,lcmPositionData.marginDetail.exchangeUnFinancedMarginRC,lcmPositionData.marginDetail.regFinancedMarginRC,lcmPositionData.marginDetail.regUnFinancedMarginRC,lcmPositionData.marginDetail.exchangeMarginRC,lcmPositionData.marginDetail.regMarginRC,lcmPositionData.marginDetail.regCpeMarginRC,lcmPositionData.marginDetail.usageRC";
  }

  function getCurrDayLcmPositionDetailColumns() {
    return "lcmPositionData.lcmPositionDetail.priceDiffPercent,lcmPositionData.lcmPositionDetail.priceDiffRC,lcmPositionData.lcmPositionDetail.quantityDiff,lcmPositionData.lcmPositionDetail.priceRC,lcmPositionData.lcmPositionDetail.mktValRC,lcmPositionData.lcmPositionDetail.settleDateMarketValueRC,lcmPositionData.lcmPositionDetail.exposureAdjustmentRC,lcmPositionData.lcmPositionDetail.adjustedExposureRC,lcmPositionData.lcmPositionDetail.requirementAdjustmentRC,lcmPositionData.lcmPositionDetail.adjustedRequirementRC,lcmPositionData.lcmPositionDetail.regRequirementAdjustmentRC,lcmPositionData.lcmPositionDetail.regAdjustedRequirementRC,lcmPositionData.lcmPositionDetail.usageAdjustmentRC,lcmPositionData.lcmPositionDetail.adjustedUsageRC,lcmPositionData.lcmPositionDetail.regUsageAdjustmentRC,lcmPositionData.lcmPositionDetail.regAdjustedUsageRC,lcmPositionData.lcmPositionDetail.underlyingValueRC,lcmPositionData.lcmPositionDetail.exposureDiffRC,lcmPositionData.lcmPositionDetail.marginDiffRC";
  }

  function getCurrDayReconColumns() {
    return "lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.quantityDiff,lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.priceDiff,lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.priceDiffPercent,lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.quantityDiff,lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.priceDiff,lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.priceDiffPercent,lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.exposureDiff,lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.marginDiff,lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.marginDiff,lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.exposureDiff,lcmPositionData.cpeReconciledPositionsData.recStatus,lcmPositionData.cpeReconciledPositionsData.cpeQuantity,lcmPositionData.cpeReconciledPositionsData.cpePrice,lcmPositionData.cpeReconciledPositionsData.cpeMarketValue,lcmPositionData.cpeReconciledPositionsData.cpeLongMarketValue,lcmPositionData.cpeReconciledPositionsData.cpeShortMarketValue,lcmPositionData.cpeReconciledPositionsData.marginUSD,lcmPositionData.cpeReconciledPositionsData.segMarginUSD,lcmPositionData.cpeReconciledPositionsData.regMarginUSD,lcmPositionData.cpeReconciledPositionsData.exchangeMarginUSD,lcmPositionData.cpeReconciledPositionsData.recCodes,lcmPositionData.cpeReconciledRepoData.loanAmount,lcmPositionData.cpeReconciledRepoData.bondPrice,lcmPositionData.cpeReconciledRepoData.bondMarketValue,lcmPositionData.cpeReconciledRepoData.bondQuantity,lcmPositionData.cpeReconciledRepoData.poolFactor,lcmPositionData.cpeReconciledRepoData.bondAccruedInterest,lcmPositionData.cpeReconciledRepoData.loanAccruedInterest,lcmPositionData.cpeHairCut,lcmPositionData.cpeReconciledRepoData.exposure";
  }

  function getCurrDayBrokerColumns() {
    return "lcmPositionData.brokerDetail.brokerPositionDetails.matchValue,lcmPositionData.brokerDetail.brokerPositionDetails.requirementHaircutPct,lcmPositionData.brokerDetail.brokerPositionDetails.priceNative,lcmPositionData.brokerDetail.brokerPositionDetails.regCurrencyCode,lcmPositionData.brokerDetail.brokerPositionDetails.brokerRegExposureRC,lcmPositionData.brokerDetail.brokerPositionDetails.brokerRegMarginRC,lcmPositionData.brokerDetail.brokerPositionDetails.matchKey,lcmPositionData.brokerDetail.brokerPositionDetails.brokerExposureRC,lcmPositionData.brokerDetail.brokerPositionDetails.brokerMarginRC,lcmPositionData.brokerDetail.brokerPositionDetails.quantity,lcmPositionData.brokerDetail.brokerPositionDetails.brokerMarketValueRC,lcmPositionData.brokerDetail.brokerPositionDetails.marketValueLocal,lcmPositionData.brokerDetail.brokerPositionDetails.marketValueCurrencyId,lcmPositionData.brokerDetail.brokerPositionDetails.marketValueCurrencyCode,lcmPositionData.brokerDetail.brokerPositionDetails.marginCurrency,lcmPositionData.brokerDetail.brokerPositionDetails.marginFxRate,lcmPositionData.brokerDetail.brokerPositionDetails.regFxRate,lcmPositionData.brokerDetail.brokerPositionDetails.securityName,lcmPositionData.brokerDetail.brokerPositionDetails.product,lcmPositionData.brokerDetail.brokerPositionDetails.tradeDate,lcmPositionData.brokerDetail.brokerPositionDetails.maturityDate,lcmPositionData.brokerDetail.brokerPositionDetails.fxRate,lcmPositionData.brokerDetail.brokerPositionDetails.gboType,lcmPositionData.brokerDetail.brokerPositionDetails.country";
  }

  function getCurrDayBrokerSecurityColumns() {
    return "lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCusip,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency1,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency2,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCanonicalSpnRic,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSpotSpnLocal,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIssuerSpnRedCode,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnIsin,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnCusip,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnIsin,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnCusip,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnIsin,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnLocal,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnRic,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnBloomberg,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.putCallInd,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.strikePrice,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.onDate,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.offDate,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.death,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.isin,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.cusip,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.sedol,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.birth,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.local,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.ameriEuroInd,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUniqueKey,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bloomberg,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingDeath,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIsin,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSedol,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingLocal,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingRic,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingBloomberg,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingLocal,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingRic,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardEndDate,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardSettleDate,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.expirationDate,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingPutCallInd,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUnderlyingStrikePriceRC,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingExpirationDate,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapStart,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapLevel,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn1,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn2,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity1,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity2,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.repoRate,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.accruedDate,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerSpreadRC,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.buySell,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.instrumentName,lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.ric";
  }

  function getPrevDayDayOnDayDiffColumns() {
    return getPrevDayLcmPositionDataColumns() + "," + getPrevDayExposureColumns() + "," + getPrevDayMarginColumns() +
      "," + getPrevDayLcmPositionDetailColumns() + "," + getPrevDayReconColumns() +
      "," + getPrevDayBrokerColumns() + "," + getPrevDayBrokerSecurityColumns();
  }

  function getPrevDayLcmPositionDataColumns() {
    return "prevDayLcmPositionData.security.shortDesc,prevDayLcmPositionData.settleDateQuantity,prevDayLcmPositionData.cusip,prevDayLcmPositionData.sedol,prevDayLcmPositionData.bloomberg,prevDayLcmPositionData.isin,prevDayLcmPositionData.treasuryCpId,prevDayLcmPositionData.legalEntity,prevDayLcmPositionData.exposureCounterparty,prevDayLcmPositionData.agreementType,prevDayLcmPositionData.countryName,prevDayLcmPositionData.marketName,prevDayLcmPositionData.reportingCurrency,prevDayLcmPositionData.date,prevDayLcmPositionData.death,prevDayLcmPositionData.currencyAbbrev,prevDayLcmPositionData.quantity,prevDayLcmPositionData.tickUnit,prevDayLcmPositionData.reqHairCut,prevDayLcmPositionData.hairCut,prevDayLcmPositionData.lcmPositionDetail.quantityDiff,prevDayLcmPositionData.underlyingSecurity.spn,prevDayLcmPositionData.underlyingSecurity.isin,prevDayLcmPositionData.underlyingSecurity.sedol,prevDayLcmPositionData.underlyingSecurity.cusip,prevDayLcmPositionData.financedQuantity,prevDayLcmPositionData.unFinancedQuantity,prevDayLcmPositionData.security.ric,prevDayLcmPositionData.security.local,prevDayLcmPositionData.security.foTypeName,prevDayLcmPositionData.security.subtypeName,prevDayLcmPositionData.security.desname,prevDayLcmPositionData.security.gboTickUnit,prevDayLcmPositionData.componentKey.spn,prevDayLcmPositionData.componentKey.fieldType,prevDayLcmPositionData.componentKey.calculator,prevDayLcmPositionData.componentKey.gboType.name,prevDayLcmPositionData.componentKey.gboTypeId,prevDayLcmPositionData.componentKey.exposureCustodianAccount.displayName,prevDayLcmPositionData.componentKey.custodianAccount.displayName,prevDayLcmPositionData.componentKey.book.displayName";
  }

  function getPrevDayExposureColumns() {
    return "prevDayLcmPositionData.exposureDetail.exposureRC,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.resetDate,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.resetSettleDate,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.tradeReturnFromLastResetRC,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedAIRC,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedCommissionsRC,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedCorpactRC,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedFinancingRC,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedPriceReturnRC,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.overdueReceivablePayableRC,prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.markToMarketRC,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.onDate,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.offDate,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.cleanPriceRC,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.dirtyPriceRC,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.loanValueRC,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.loanAIRC,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.bondValueRC,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.bondAIRC,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.poolFactor,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.repoMarginRC,prevDayLcmPositionData.exposureDetail.repoPositionExposureData.repoExposureRC,prevDayLcmPositionData.exposureDetail.fcmPositionOteData.oteRC";
  }

  function getPrevDayMarginColumns() {
    return "prevDayLcmPositionData.marginDetail.marginRC,prevDayLcmPositionData.marginDetail.applicableMarginRC,prevDayLcmPositionData.marginDetail.applicableFinancedMarginRC,prevDayLcmPositionData.marginDetail.applicableUnFinancedMarginRC,prevDayLcmPositionData.marginDetail.exchangeFinancedMarginRC,prevDayLcmPositionData.marginDetail.exchangeUnFinancedMarginRC,prevDayLcmPositionData.marginDetail.regFinancedMarginRC,prevDayLcmPositionData.marginDetail.regUnFinancedMarginRC,prevDayLcmPositionData.marginDetail.exchangeMarginRC,prevDayLcmPositionData.marginDetail.regMarginRC,prevDayLcmPositionData.marginDetail.regCpeMarginRC,prevDayLcmPositionData.marginDetail.usageRC";
  }

  function getPrevDayLcmPositionDetailColumns() {
    return "prevDayLcmPositionData.lcmPositionDetail.priceRC,prevDayLcmPositionData.lcmPositionDetail.mktValRC,prevDayLcmPositionData.lcmPositionDetail.settleDateMarketValueRC,prevDayLcmPositionData.lcmPositionDetail.exposureAdjustmentRC,prevDayLcmPositionData.lcmPositionDetail.adjustedExposureRC,prevDayLcmPositionData.lcmPositionDetail.requirementAdjustmentRC,prevDayLcmPositionData.lcmPositionDetail.adjustedRequirementRC,prevDayLcmPositionData.lcmPositionDetail.regRequirementAdjustmentRC,prevDayLcmPositionData.lcmPositionDetail.regAdjustedRequirementRC,prevDayLcmPositionData.lcmPositionDetail.usageAdjustmentRC,prevDayLcmPositionData.lcmPositionDetail.adjustedUsageRC,prevDayLcmPositionData.lcmPositionDetail.regUsageAdjustmentRC,prevDayLcmPositionData.lcmPositionDetail.regAdjustedUsageRC,prevDayLcmPositionData.lcmPositionDetail.underlyingValueRC,prevDayLcmPositionData.lcmPositionDetail.exposureDiffRC,prevDayLcmPositionData.lcmPositionDetail.marginDiffRC";
  }

  function getPrevDayReconColumns() {
    return "prevDayLcmPositionData.cpeReconciledPositionsData.recStatus,prevDayLcmPositionData.cpeReconciledPositionsData.cpeQuantity,prevDayLcmPositionData.cpeReconciledPositionsData.cpePrice,prevDayLcmPositionData.cpeReconciledPositionsData.cpeMarketValue,prevDayLcmPositionData.cpeReconciledPositionsData.cpeLongMarketValue,prevDayLcmPositionData.cpeReconciledPositionsData.cpeShortMarketValue,prevDayLcmPositionData.cpeReconciledPositionsData.marginUSD,prevDayLcmPositionData.cpeReconciledPositionsData.segMarginUSD,prevDayLcmPositionData.cpeReconciledPositionsData.regMarginUSD,prevDayLcmPositionData.cpeReconciledPositionsData.exchangeMarginUSD,prevDayLcmPositionData.cpeReconciledPositionsData.recCodes,prevDayLcmPositionData.cpeReconciledRepoData.loanAmount,prevDayLcmPositionData.cpeReconciledRepoData.bondPrice,prevDayLcmPositionData.cpeReconciledRepoData.bondMarketValue,prevDayLcmPositionData.cpeReconciledRepoData.bondQuantity,prevDayLcmPositionData.cpeReconciledRepoData.poolFactor,prevDayLcmPositionData.cpeReconciledRepoData.bondAccruedInterest,prevDayLcmPositionData.cpeReconciledRepoData.loanAccruedInterest,prevDayLcmPositionData.cpeHairCut,prevDayLcmPositionData.cpeReconciledRepoData.exposure";
  }

  function getPrevDayBrokerColumns() {
    return "prevDayLcmPositionData.brokerDetail.brokerPositionDetails.matchValue,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.requirementHaircutPct,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.priceNative,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.regCurrencyCode,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerRegExposureRC,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerRegMarginRC,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.matchKey,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerExposureRC,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerMarginRC,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.quantity,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerMarketValueRC,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marketValueLocal,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marketValueCurrencyId,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marketValueCurrencyCode,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marginCurrency,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marginFxRate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.regFxRate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.securityName,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.product,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.tradeDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.maturityDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.fxRate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.gboType,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.country";
  }

  function getPrevDayBrokerSecurityColumns() {
    return "prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCusip,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency1,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency2,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCanonicalSpnRic,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSpotSpnLocal,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIssuerSpnRedCode,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnIsin,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnCusip,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnIsin,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnCusip,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnIsin,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnLocal,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnRic,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnBloomberg,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.putCallInd,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.strikePrice,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.onDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.offDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.death,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.isin,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.cusip,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.sedol,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.birth,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.local,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.ameriEuroInd,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUniqueKey,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bloomberg,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingDeath,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIsin,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSedol,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingLocal,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingRic,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingBloomberg,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingLocal,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingRic,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardEndDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardSettleDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.expirationDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingPutCallInd,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUnderlyingStrikePriceRC,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingExpirationDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapStart,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapLevel,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn1,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn2,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity1,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity2,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.repoRate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.accruedDate,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerSpreadRC,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.buySell,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.instrumentName,prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.ric";
  }

  function _getCurrDayPositionDetailData(row) {
    return {
      'cusip': row["lcmPositionData.cusip"],
      'sedol': row["lcmPositionData.sedol"],
      'bloomberg': row["lcmPositionData.bloomberg"],
      'isin': row["lcmPositionData.isin"],
      'treasuryCpId': row["lcmPositionData.treasuryCpId"],
      'legalEntity': row["lcmPositionData.legalEntity"],
      'exposureCounterparty': row["lcmPositionData.exposureCounterparty"],
      'agreementType': row["lcmPositionData.agreementType"],
      'countryName': row["lcmPositionData.countryName"],
      'marketName': row["lcmPositionData.marketName"],
      'reportingCurrency': row["lcmPositionData.reportingCurrency"],
      'date': convertJavaDateDashed(row["lcmPositionData.date"]),
      'death': convertJavaDateTime(row["lcmPositionData.death"]),
      'currencyAbbrev': row["lcmPositionData.currencyAbbrev"],
      'quantity': row["lcmPositionData.quantity"],
      'tickUnit': row["lcmPositionData.tickUnit"],
      'requirementHairCut': row["lcmPositionData.reqHairCut"],
      'haircut': row["lcmPositionData.hairCut"],
      'settleDateQuantity': row["lcmPositionData.settleDateQuantity"],
      'quantityDiff': row["lcmPositionData.lcmPositionDetail.quantityDiff"],
      'brokerPriceNative': row["lcmPositionData.brokerDetail.brokerPositionDetails.priceNative"],
      'brokerRegCurrencyCode': row["lcmPositionData.brokerDetail.brokerPositionDetails.regCurrencyCode"],
      'brokerRegExposureRC': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerRegExposureRC"],
      'brokerRegMarginRC': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerRegMarginRC"],
      'brokerMatchKey': row["lcmPositionData.brokerDetail.brokerPositionDetails.matchKey"],
      'brokerUnderlyingCanonicalSpnRic': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCanonicalSpnRic"],
      'brokerUnderlyingSpotSpnLocal': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSpotSpnLocal"],
      'brokerUnderlyingIssuerSpnRedCode': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIssuerSpnRedCode"],
      'brokerBondSpnIsin': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnIsin"],
      'brokerBondSpnCusip': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnCusip"],
      'brokerReferenceSpnIsin': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnIsin"],
      'brokerReferenceSpnCusip': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnCusip"],
      'brokerSpotSpnIsin': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnIsin"],
      'brokerSpotSpnLocal': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnLocal"],
      'brokerSpotSpnRic': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnRic"],
      'brokerSpotSpnBloomberg': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnBloomberg"],
      'brokerPutCallInd': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.putCallInd"],
      'brokerStrikePrice': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.strikePrice"],
      'brokerRepoOnDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.onDate"]),
      'brokerRepoOffDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.offDate"]),
      'underlyingSecuritySpn': row["lcmPositionData.underlyingSecurity.spn"],
      'underlyingSecurityIsin': row["lcmPositionData.underlyingSecurity.isin"],
      'underlyingSecuritySedol': row["lcmPositionData.underlyingSecurity.sedol"],
      'underlyingSecurityCusip': row["lcmPositionData.underlyingSecurity.cusip"],
      'financedQuantity': row["lcmPositionData.financedQuantity"],
      'unFinancedQuantity': row["lcmPositionData.unFinancedQuantity"],
      'brokerRic': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.ric"],
      'ric': row["lcmPositionData.security.ric"],
      'local': row["lcmPositionData.security.local"],
      'foType': row["lcmPositionData.security.foTypeName"],
      'subType': row["lcmPositionData.security.subtypeName"],
      'instrumentName': row["lcmPositionData.security.desname"],
      'shortDescription': row["lcmPositionData.security.shortDesc"],
      'tickUnit': row["lcmPositionData.security.gboTickUnit"],
      'pnlSpn': row["lcmPositionData.componentKey.spn"],
      'fieldType': row["lcmPositionData.componentKey.fieldType"],
      'calculator': row["lcmPositionData.componentKey.calculator"],
      'gboType': isNullOrUndefined(row["lcmPositionData.componentKey.gboType.name"]) ? row["lcmPositionData.componentKey.gboTypeId"] : row["lcmPositionData.componentKey.gboType.name"],
      'exposureCustodianAccount': isNullOrUndefined(row["lcmPositionData.componentKey.exposureCustodianAccount.displayName"]) ? null : row["lcmPositionData.componentKey.exposureCustodianAccount.displayName"],
      'custodianAccount': isNullOrUndefined(row["lcmPositionData.componentKey.custodianAccount.displayName"]) ? null : row["lcmPositionData.componentKey.custodianAccount.displayName"],
      'bookAbbrev': isNullOrUndefined(row["lcmPositionData.componentKey.book.displayName"]) ? null : row["lcmPositionData.componentKey.book.displayName"],
      'priceRC': row["lcmPositionData.lcmPositionDetail.priceRC"],
      'mktValRC': row["lcmPositionData.lcmPositionDetail.mktValRC"],
      'settleDateMarketValueRC': row["lcmPositionData.lcmPositionDetail.settleDateMarketValueRC"],
      'exposureAdjustmentRC': row["lcmPositionData.lcmPositionDetail.exposureAdjustmentRC"],
      'adjustedExposureRC': row["lcmPositionData.lcmPositionDetail.adjustedExposureRC"],
      'requirementAdjustmentRC': row["lcmPositionData.lcmPositionDetail.requirementAdjustmentRC"],
      'adjustedRequirementRC': row["lcmPositionData.lcmPositionDetail.adjustedRequirementRC"],
      'regRequirementAdjustmentRC': row["lcmPositionData.lcmPositionDetail.regRequirementAdjustmentRC"],
      'regAdjustedRequirementRC': row["lcmPositionData.lcmPositionDetail.regAdjustedRequirementRC"],
      'usageAdjustmentRC': row["lcmPositionData.lcmPositionDetail.usageAdjustmentRC"],
      'adjustedUsageRC': row["lcmPositionData.lcmPositionDetail.adjustedUsageRC"],
      'regUsageAdjustmentRC': row["lcmPositionData.lcmPositionDetail.regUsageAdjustmentRC"],
      'regAdjustedUsageRC': row["lcmPositionData.lcmPositionDetail.regAdjustedUsageRC"],
      'underLyingValueRC': row["lcmPositionData.lcmPositionDetail.underlyingValueRC"],
      'exposureDiffRC': row["lcmPositionData.lcmPositionDetail.exposureDiffRC"],
      'marginDiffRC': row["lcmPositionData.lcmPositionDetail.marginDiffRC"],
      'exposureRC': row["lcmPositionData.exposureDetail.exposureRC"],
      'resetDate': convertJavaDateTime(row["lcmPositionData.exposureDetail.isdaPositionMtmData.resetDate"]),
      'resetSettleDate': convertJavaDateTime(row["lcmPositionData.exposureDetail.isdaPositionMtmData.resetSettleDate"]),
      'tradeReturnFromLastResetRC': row["lcmPositionData.exposureDetail.isdaPositionMtmData.tradeReturnFromLastResetRC"],
      'unrealizedAIRC': row["lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedAIRC"],
      'unrealizedCommissionsRC': row["lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedCommissionsRC"],
      'unrealizedCorpactRC': row["lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedCorpactRC"],
      'unrealizedFinancingRC': row["lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedFinancingRC"],
      'unrealizedPriceReturnRC': row["lcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedPriceReturnRC"],
      'overdueReceivablePayableRC': row["lcmPositionData.exposureDetail.isdaPositionMtmData.overdueReceivablePayableRC"],
      'markToMarketRC': row["lcmPositionData.exposureDetail.isdaPositionMtmData.markToMarketRC"],
      'onDate': convertJavaDateTime(row["lcmPositionData.exposureDetail.repoPositionExposureData.onDate"]),
      'offDate': convertJavaDateTime(row["lcmPositionData.exposureDetail.repoPositionExposureData.offDate"]),
      'cleanPriceRC': row["lcmPositionData.exposureDetail.repoPositionExposureData.cleanPriceRC"],
      'dirtyPriceRC': row["lcmPositionData.exposureDetail.repoPositionExposureData.dirtyPriceRC"],
      'loanValueRC': row["lcmPositionData.exposureDetail.repoPositionExposureData.loanValueRC"],
      'loanAIRC': row["lcmPositionData.exposureDetail.repoPositionExposureData.loanAIRC"],
      'bondValueRC': row["lcmPositionData.exposureDetail.repoPositionExposureData.bondValueRC"],
      'bondAIRC': row["lcmPositionData.exposureDetail.repoPositionExposureData.bondAIRC"],
      'poolFactor': row["lcmPositionData.exposureDetail.repoPositionExposureData.poolFactor"],
      'repoMarginRC': row["lcmPositionData.exposureDetail.repoPositionExposureData.repoMarginRC"],
      'repoExposureRC': row["lcmPositionData.exposureDetail.repoPositionExposureData.repoExposureRC"],
      'oteRC': isNullOrUndefined(row["lcmPositionData.exposureDetail.fcmPositionOteData.oteRC"]) ? null : row["lcmPositionData.exposureDetail.fcmPositionOteData.oteRC"],
      'marginRC': row["lcmPositionData.marginDetail.marginRC"],
      'applicableMarginRC': row["lcmPositionData.marginDetail.applicableMarginRC"],
      'applicableFinancedMarginRC': row["lcmPositionData.marginDetail.applicableFinancedMarginRC"],
      'applicableUnFinancedMarginRC': row["lcmPositionData.marginDetail.applicableUnFinancedMarginRC"],
      'exchangeFinancedMarginRC': row["lcmPositionData.marginDetail.exchangeFinancedMarginRC"],
      'exchangeUnFinancedMarginRC': row["lcmPositionData.marginDetail.exchangeUnFinancedMarginRC"],
      'regFinancedMarginRC': row["lcmPositionData.marginDetail.regFinancedMarginRC"],
      'regUnFinancedMarginRC': row["lcmPositionData.marginDetail.regUnFinancedMarginRC"],
      'exchangeMarginRC': row["lcmPositionData.marginDetail.exchangeMarginRC"],
      'regMarginRC': row["lcmPositionData.marginDetail.regMarginRC"],
      'regCpeMarginRC': row["lcmPositionData.marginDetail.regCpeMarginRC"],
      'usageRC': row["lcmPositionData.marginDetail.usageRC"],
      'brokerExposureRC': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerExposureRC"],
      'brokerMarginRC': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerMarginRC"],
      'brokerQuantity': row["lcmPositionData.brokerDetail.brokerPositionDetails.quantity"],
      'brokerMarketValueRC': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerMarketValueRC"],
      'brokerMarketValueLocal': row["lcmPositionData.brokerDetail.brokerPositionDetails.marketValueLocal"],
      'brokerMarketValueCurrencyId': row["lcmPositionData.brokerDetail.brokerPositionDetails.marketValueCurrencyId"],
      'brokerMarketValueCurrencyCode': row["lcmPositionData.brokerDetail.brokerPositionDetails.marketValueCurrencyCode"],
      'brokerMarginCurrency': row["lcmPositionData.brokerDetail.brokerPositionDetails.marginCurrency"],
      'brokerMarginFxRate': row["lcmPositionData.brokerDetail.brokerPositionDetails.marginFxRate"],
      'brokerRegFxRate': row["lcmPositionData.brokerDetail.brokerPositionDetails.regFxRate"],
      'brokerSecurityName': row["lcmPositionData.brokerDetail.brokerPositionDetails.securityName"],
      'brokerDeath': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.death"]),
      'brokerIsin': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.isin"],
      'brokerCusip': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.cusip"],
      'brokerSedol': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.sedol"],
      'brokerProduct': row["lcmPositionData.brokerDetail.brokerPositionDetails.product"],
      'brokerTradeDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.tradeDate"]),
      'brokerMaturityDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.maturityDate"]),
      'brokerFxRate': row["lcmPositionData.brokerDetail.brokerPositionDetails.fxRate"],
      'brokerBirth': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.birth"]),
      'brokerLocal': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.local"],
      'brokerGboType': row["lcmPositionData.brokerDetail.brokerPositionDetails.gboType"],
      'brokerCountry': row["lcmPositionData.brokerDetail.brokerPositionDetails.country"],
      'brokerAmeriEuroInd': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.ameriEuroInd"],
      'brokerUniqueKey': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUniqueKey"],
      'brokerBloomberg': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bloomberg"],
      'brokerUnderlyingDeath': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingDeath"]),
      'brokerUnderlyingIsin': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIsin"],
      'brokerUnderlyingCusip': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCusip"],
      'brokerUnderlyingSedol': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSedol"],
      'brokerUnderlyingLocal': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingLocal"],
      'brokerUnderlyingRic': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingRic"],
      'brokerUnderlyingBloomberg': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingBloomberg"],
      'brokerUnderlyingUnderlyingLocal': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingLocal"],
      'brokerUnderlyingUnderlyingRic': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingRic"],
      'brokerUnderlyingForwardEndDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardEndDate"]),
      'brokerUnderlyingForwardSettleDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardSettleDate"]),
      'brokerExpirationDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.expirationDate"]),
      'brokerUnderlyingPutCallInd': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingPutCallInd"],
      'brokerUnderlyingStrikePriceRC': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUnderlyingStrikePriceRC"],
      'brokerUnderlyingExpirationDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingExpirationDate"]),
      'brokerSwapStart': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapStart"]),
      'brokerSwapLevel': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapLevel"],
      'brokerMatchValue': row["lcmPositionData.brokerDetail.brokerPositionDetails.matchValue"],
      'brokerRequirementHaircutPct': row["lcmPositionData.brokerDetail.brokerPositionDetails.requirementHaircutPct"],
      'brokerForwardSpn1': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn1"],
      'brokerForwardSpn2': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn2"],
      'brokerForwardCurrency1': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency1"],
      'brokerForwardCurrency2': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency2"],
      'brokerForwardQuantity1': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity1"],
      'brokerForwardQuantity2': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity2"],
      'brokerRepoRate': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.repoRate"],
      'brokerAccruedDate': convertJavaDateTime(row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.accruedDate"]),
      'brokerSpreadRC': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerSpreadRC"],
      'brokerBuySell': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.buySell"],
      'brokerInstrumentName': row["lcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.instrumentName"],
      'recStatus': row["lcmPositionData.cpeReconciledPositionsData.recStatus"],
      'cpeQuantity': row["lcmPositionData.cpeReconciledPositionsData.cpeQuantity"],
      'cpePrice': row["lcmPositionData.cpeReconciledPositionsData.cpePrice"],
      'cpeMarketValue': row["lcmPositionData.cpeReconciledPositionsData.cpeMarketValue"],
      'cpeLongMarketValue': row["lcmPositionData.cpeReconciledPositionsData.cpeLongMarketValue"],
      'cpeShortMarketValue': row["lcmPositionData.cpeReconciledPositionsData.cpeShortMarketValue"],
      'cpeMarginUSD': row["lcmPositionData.cpeReconciledPositionsData.marginUSD"],
      'cpeSegMarginUSD': row["lcmPositionData.cpeReconciledPositionsData.segMarginUSD"],
      'cpeRegMarginUSD': row["lcmPositionData.cpeReconciledPositionsData.regMarginUSD"],
      'cpeExchangeMarginUSD': row["lcmPositionData.cpeReconciledPositionsData.exchangeMarginUSD"],
      'recCode': row["lcmPositionData.cpeReconciledPositionsData.recCodes"],
      'internalVsReconExposureDiffUsd': row["lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.exposureDiff"],
      'internalVsReconMarginDiffUsd': row["lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.marginDiff"],
      'reconVsBrokerMarginDiffUsd': row["lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.marginDiff"],
      'reconVsBrokerExposureDiffUsd': row["lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.exposureDiff"],
      'internalVsReconQuantityDiff': row["lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.quantityDiff"],
      'reconVsBrokerQuantityDiff': row["lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.quantityDiff"],
      'internalVsReconPriceDiffUsd': row["lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.priceDiff"],
      'reconVsBrokerPriceDiffUsd': row["lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.priceDiff"],
      'internalVsReconPriceDiffPercent': row["lcmPositionData.lcmReconDiffDetail.internalVsReconDiffDetail.priceDiffPercent"],
      'reconVsBrokerPriceDiffPercent': row["lcmPositionData.lcmReconDiffDetail.reconVsBrokerDiffDetail.priceDiffPercent"],
      'internalVsBrokerPriceDiffRC': row["lcmPositionData.lcmPositionDetail.priceDiffRC"],
      'internalVsBrokerPriceDiffPercent': row["lcmPositionData.lcmPositionDetail.priceDiffPercent"],
      'cpeLoanAmount': row["lcmPositionData.cpeReconciledRepoData.loanAmount"],
      'cpeBondPrice': row["lcmPositionData.cpeReconciledRepoData.bondPrice"],
      'cpeBondMarketValue': row["lcmPositionData.cpeReconciledRepoData.bondMarketValue"],
      'cpeBondQuantity': row["lcmPositionData.cpeReconciledRepoData.bondQuantity"],
      'cpePoolFactor': row["lcmPositionData.cpeReconciledRepoData.poolFactor"],
      'cpeBondAccruedInterest': row["lcmPositionData.cpeReconciledRepoData.bondAccruedInterest"],
      'cpeLoanAccruedInterest': row["lcmPositionData.cpeReconciledRepoData.loanAccruedInterest"],
      'cpeHaircut': row["lcmPositionData.cpeHairCut"],
      'cpeExposure': row["lcmPositionData.cpeReconciledRepoData.exposure"]
    };
  }

  function _getPrevDayPositionDetailData(row) {
    return {
      'prevDayCusip': row["prevDayLcmPositionData.cusip"],
      'prevDaySedol': row["prevDayLcmPositionData.sedol"],
      'prevDayBloomberg': row["prevDayLcmPositionData.bloomberg"],
      'prevDayIsin': row["prevDayLcmPositionData.isin"],
      'prevDayTreasuryCpId': row["prevDayLcmPositionData.treasuryCpId"],
      'prevDayLegalEntity': row["prevDayLcmPositionData.legalEntity"],
      'prevDayExposureCounterparty': row["prevDayLcmPositionData.exposureCounterparty"],
      'prevDayAgreementType': row["prevDayLcmPositionData.agreementType"],
      'prevDayCountryName': row["prevDayLcmPositionData.countryName"],
      'prevDayMarketName': row["prevDayLcmPositionData.marketName"],
      'prevDayReportingCurrency': row["prevDayLcmPositionData.reportingCurrency"],
      'prevDayDate': convertJavaDateDashed(row["prevDayLcmPositionData.date"]),
      'prevDayDeath': convertJavaDateTime(row["prevDayLcmPositionData.death"]),
      'prevDayCurrencyAbbrev': row["prevDayLcmPositionData.currencyAbbrev"],
      'prevDayQuantity': row["prevDayLcmPositionData.quantity"],
      'prevDayTickUnit': row["prevDayLcmPositionData.tickUnit"],
      'prevDayRequirementHairCut': row["prevDayLcmPositionData.reqHairCut"],
      'prevDayHaircut': row["prevDayLcmPositionData.hairCut"],
      'prevDaySettleDateQuantity': row["prevDayLcmPositionData.settleDateQuantity"],
      'prevDayQuantityDiff': row["prevDayLcmPositionData.lcmPositionDetail.quantityDiff"],
      'prevDayBrokerPriceNative': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.priceNative"],
      'prevDayBrokerRegCurrencyCode': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.regCurrencyCode"],
      'prevDayBrokerRegExposureRC': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerRegExposureRC"],
      'prevDayBrokerRegMarginRC': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerRegMarginRC"],
      'prevDayBrokerMatchKey': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.matchKey"],
      'prevDayBrokerUnderlyingCanonicalSpnRic': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCanonicalSpnRic"],
      'prevDayBrokerUnderlyingSpotSpnLocal': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSpotSpnLocal"],
      'prevDayBrokerUnderlyingIssuerSpnRedCode': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIssuerSpnRedCode"],
      'prevDayBrokerBondSpnIsin': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnIsin"],
      'prevDayBrokerBondSpnCusip': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bondSpnCusip"],
      'prevDayBrokerReferenceSpnIsin': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnIsin"],
      'prevDayBrokerReferenceSpnCusip': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.referenceSpnCusip"],
      'prevDayBrokerSpotSpnIsin': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnIsin"],
      'prevDayBrokerSpotSpnLocal': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnLocal"],
      'prevDayBrokerSpotSpnRic': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnRic"],
      'prevDayBrokerSpotSpnBloomberg': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.spotSpnBloomberg"],
      'prevDayBrokerPutCallInd': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.putCallInd"],
      'prevDayBrokerStrikePrice': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.strikePrice"],
      'prevDayBrokerRepoOnDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.onDate"]),
      'prevDayBrokerRepoOffDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.offDate"]),
      'prevDayUnderlyingSecuritySpn': row["prevDayLcmPositionData.underlyingSecurity.spn"],
      'prevDayUnderlyingSecurityIsin': row["prevDayLcmPositionData.underlyingSecurity.isin"],
      'prevDayUnderlyingSecuritySedol': row["prevDayLcmPositionData.underlyingSecurity.sedol"],
      'prevDayUnderlyingSecurityCusip': row["prevDayLcmPositionData.underlyingSecurity.cusip"],
      'prevDayFinancedQuantity': row["prevDayLcmPositionData.financedQuantity"],
      'prevDayUnFinancedQuantity': row["prevDayLcmPositionData.unFinancedQuantity"],
      'prevDayBrokerRic': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.ric"],
      'prevDayRic': row["prevDayLcmPositionData.security.ric"],
      'prevDayLocal': row["prevDayLcmPositionData.security.local"],
      'prevDayFoType': row["prevDayLcmPositionData.security.foTypeName"],
      'prevDaySubType': row["prevDayLcmPositionData.security.subtypeName"],
      'prevDayInstrumentName': row["prevDayLcmPositionData.security.desname"],
      'prevDayShortDescription': row["prevDayLcmPositionData.security.shortDesc"],
      'prevDayTickUnit': row["prevDayLcmPositionData.security.gboTickUnit"],
      'prevDayPnlSpn': row["prevDayLcmPositionData.componentKey.spn"],
      'prevDayFieldType': row["prevDayLcmPositionData.componentKey.fieldType"],
      'prevDayCalculator': row["prevDayLcmPositionData.componentKey.calculator"],
      'prevDayGboType': isNullOrUndefined(row["prevDayLcmPositionData.componentKey.gboType.name"]) ? row["prevDayLcmPositionData.componentKey.gboTypeId"] : row["prevDayLcmPositionData.componentKey.gboType.name"],
      'prevDayExposureCustodianAccount': isNullOrUndefined(row["prevDayLcmPositionData.componentKey.exposureCustodianAccount.displayName"]) ? null : row["prevDayLcmPositionData.componentKey.exposureCustodianAccount.displayName"],
      'prevDayCustodianAccount': isNullOrUndefined(row["prevDayLcmPositionData.componentKey.custodianAccount.displayName"]) ? null : row["prevDayLcmPositionData.componentKey.custodianAccount.displayName"],
      'prevDayBookAbbrev': isNullOrUndefined(row["prevDayLcmPositionData.componentKey.book.displayName"]) ? null : row["prevDayLcmPositionData.componentKey.book.displayName"],
      'prevDayPrice': row["prevDayLcmPositionData.lcmPositionDetail.priceRC"],
      'prevDayMktVal': row["prevDayLcmPositionData.lcmPositionDetail.mktValRC"],
      'prevDaySettleDateMarketValue': row["prevDayLcmPositionData.lcmPositionDetail.settleDateMarketValueRC"],
      'prevDayExposureAdjustmentRC': row["prevDayLcmPositionData.lcmPositionDetail.exposureAdjustmentRC"],
      'prevDayAdjustedExposure': row["prevDayLcmPositionData.lcmPositionDetail.adjustedExposureRC"],
      'prevDayRequirementAdjustment': row["prevDayLcmPositionData.lcmPositionDetail.requirementAdjustmentRC"],
      'prevDayAdjustedRequirement': row["prevDayLcmPositionData.lcmPositionDetail.adjustedRequirementRC"],
      'prevDayRegRequirementAdjustmentRC': row["prevDayLcmPositionData.lcmPositionDetail.regRequirementAdjustmentRC"],
      'prevDayRegAdjustedRequirementRC': row["prevDayLcmPositionData.lcmPositionDetail.regAdjustedRequirementRC"],
      'prevDayUsageAdjustment': row["prevDayLcmPositionData.lcmPositionDetail.usageAdjustmentRC"],
      'prevDayAdjustedUsage': row["prevDayLcmPositionData.lcmPositionDetail.adjustedUsageRC"],
      'prevDayRegUsageAdjustmentRC': row["prevDayLcmPositionData.lcmPositionDetail.regUsageAdjustmentRC"],
      'prevDayRegAdjustedUsageRC': row["prevDayLcmPositionData.lcmPositionDetail.regAdjustedUsageRC"],
      'prevDayUnderLyingValue': row["prevDayLcmPositionData.lcmPositionDetail.underlyingValueRC"],
      'prevDayExposureDiff': row["prevDayLcmPositionData.lcmPositionDetail.exposureDiffRC"],
      'prevDayMarginDiff': row["prevDayLcmPositionData.lcmPositionDetail.marginDiffRC"],
      'prevDayExposure': row["prevDayLcmPositionData.exposureDetail.exposureRC"],
      'prevDayResetDate': convertJavaDateTime(row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.resetDate"]),
      'prevDayResetSettleDate': convertJavaDateTime(row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.resetSettleDate"]),
      'prevDayTradeReturnFromLastReset': row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.tradeReturnFromLastResetRC"],
      'prevDayUnrealizedAI': row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedAIRC"],
      'prevDayUnrealizedCommissionsRC': row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedCommissionsRC"],
      'prevDayUnrealizedCorpact': row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedCorpactRC"],
      'prevDayUnrealizedFinancing': row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedFinancingRC"],
      'prevDayUnrealizedPriceReturn': row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.unrealizedPriceReturnRC"],
      'prevDayOverdueReceivablePayable': row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.overdueReceivablePayableRC"],
      'prevDayMarkToMarket': row["prevDayLcmPositionData.exposureDetail.isdaPositionMtmData.markToMarketRC"],
      'prevDayOnDate': convertJavaDateTime(row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.onDate"]),
      'prevDayOffDate': convertJavaDateTime(row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.offDate"]),
      'prevDayCleanPriceRC': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.cleanPriceRC"],
      'prevDayDirtyPriceRC': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.dirtyPriceRC"],
      'prevDayLoanValueRC': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.loanValueRC"],
      'prevDayLoanAIRC': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.loanAIRC"],
      'prevDayBondValueRC': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.bondValueRC"],
      'prevDayBondAIRC': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.bondAIRC"],
      'prevDayPoolFactor': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.poolFactor"],
      'prevDayRepoMarginRC': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.repoMarginRC"],
      'prevDayRepoExposureRC': row["prevDayLcmPositionData.exposureDetail.repoPositionExposureData.repoExposureRC"],
      'prevDayOte': isNullOrUndefined(row["prevDayLcmPositionData.exposureDetail.fcmPositionOteData.oteRC"]) ? null : row["prevDayLcmPositionData.exposureDetail.fcmPositionOteData.oteRC"],
      'prevDayMargin': row["prevDayLcmPositionData.marginDetail.marginRC"],
      'prevDayApplicableMargin': row["prevDayLcmPositionData.marginDetail.applicableMarginRC"],
      'prevDayApplicableFinancedMarginRC': row["prevDayLcmPositionData.marginDetail.applicableFinancedMarginRC"],
      'prevDayApplicableUnFinancedMarginRC': row["prevDayLcmPositionData.marginDetail.applicableUnFinancedMarginRC"],
      'prevDayExchangeFinancedMarginRC': row["prevDayLcmPositionData.marginDetail.exchangeFinancedMarginRC"],
      'prevDayExchangeUnFinancedMarginRC': row["prevDayLcmPositionData.marginDetail.exchangeUnFinancedMarginRC"],
      'prevDayRegFinancedMarginRC': row["prevDayLcmPositionData.marginDetail.regFinancedMarginRC"],
      'prevDayRegUnFinancedMarginRC': row["prevDayLcmPositionData.marginDetail.regUnFinancedMarginRC"],
      'prevDayExchangeMarginRC': row["prevDayLcmPositionData.marginDetail.exchangeMarginRC"],
      'prevDayRegMarginRC': row["prevDayLcmPositionData.marginDetail.regMarginRC"],
      'prevDayRegCpeMarginRC': row["prevDayLcmPositionData.marginDetail.regCpeMarginRC"],
      'prevDayUsage': row["prevDayLcmPositionData.marginDetail.usageRC"],
      'prevDayBrokerExposureRC': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerExposureRC"],
      'prevDayBrokerMarginRC': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerMarginRC"],
      'prevDayBrokerQuantity': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.quantity"],
      'prevDayBrokerMarketValueRC': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerMarketValueRC"],
      'prevDayBrokerMarketValueLocal': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marketValueLocal"],
      'prevDayBrokerMarketValueCurrencyId': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marketValueCurrencyId"],
      'prevDayBrokerMarketValueCurrencyCode': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marketValueCurrencyCode"],
      'prevDayBrokerMarginCurrency': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marginCurrency"],
      'prevDayBrokerMarginFxRate': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.marginFxRate"],
      'prevDayBrokerRegFxRate': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.regFxRate"],
      'prevDayBrokerSecurityName': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.securityName"],
      'prevDayBrokerDeath': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.death"]),
      'prevDayBrokerIsin': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.isin"],
      'prevDayBrokerCusip': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.cusip"],
      'prevDayBrokerSedol': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.sedol"],
      'prevDayBrokerProduct': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.product"],
      'prevDayBrokerTradeDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.tradeDate"]),
      'prevDayBrokerMaturityDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.maturityDate"]),
      'prevDayBrokerFxRate': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.fxRate"],
      'prevDayBrokerBirth': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.birth"]),
      'prevDayBrokerLocal': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.local"],
      'prevDayBrokerGboType': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.gboType"],
      'prevDayBrokerCountry': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.country"],
      'prevDayBrokerAmeriEuroInd': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.ameriEuroInd"],
      'prevDayBrokerUniqueKey': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUniqueKey"],
      'prevDayBrokerBloomberg': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.bloomberg"],
      'prevDayBrokerUnderlyingDeath': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingDeath"]),
      'prevDayBrokerUnderlyingIsin': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingIsin"],
      'prevDayBrokerUnderlyingCusip': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingCusip"],
      'prevDayBrokerUnderlyingSedol': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingSedol"],
      'prevDayBrokerUnderlyingLocal': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingLocal"],
      'prevDayBrokerUnderlyingRic': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingRic"],
      'prevDayBrokerUnderlyingBloomberg': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingBloomberg"],
      'prevDayBrokerUnderlyingUnderlyingLocal': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingLocal"],
      'prevDayBrokerUnderlyingUnderlyingRic': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingUnderlyingRic"],
      'prevDayBrokerUnderlyingForwardEndDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardEndDate"]),
      'prevDayBrokerUnderlyingForwardSettleDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingForwardSettleDate"]),
      'prevDayBrokerExpirationDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.expirationDate"]),
      'prevDayBrokerUnderlyingPutCallInd': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingPutCallInd"],
      'prevDayBrokerUnderlyingStrikePriceRC': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerUnderlyingStrikePriceRC"],
      'prevDayBrokerUnderlyingExpirationDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.underlyingExpirationDate"]),
      'prevDayBrokerSwapStart': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapStart"]),
      'prevDayBrokerSwapLevel': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.swapLevel"],
      'prevDayBrokerMatchValue': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.matchValue"],
      'prevDayBrokerRequirementHaircutPct': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.requirementHaircutPct"],
      'prevDayBrokerForwardSpn1': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn1"],
      'prevDayBrokerForwardSpn2': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardSpn2"],
      'prevDayBrokerForwardCurrency1': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency1"],
      'prevDayBrokerForwardCurrency2': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardCurrency2"],
      'prevDayBrokerForwardQuantity1': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity1"],
      'prevDayBrokerForwardQuantity2': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.forwardQuantity2"],
      'prevDayBrokerRepoRate': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.repoRate"],
      'prevDayBrokerAccruedDate': convertJavaDateTime(row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.accruedDate"]),
      'prevDayBrokerSpreadRC': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.brokerSpreadRC"],
      'prevDayBrokerBuySell': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.buySell"],
      'prevDayBrokerInstrumentName': row["prevDayLcmPositionData.brokerDetail.brokerPositionDetails.brokerSecurityDetail.instrumentName"],
      'prevDayRecStatus': row["prevDayLcmPositionData.cpeReconciledPositionsData.recStatus"],
      'prevDayCpeQuantity': row["prevDayLcmPositionData.cpeReconciledPositionsData.cpeQuantity"],
      'prevDayCpePrice': row["prevDayLcmPositionData.cpeReconciledPositionsData.cpePrice"],
      'prevDayCpeMarketValue': row["prevDayLcmPositionData.cpeReconciledPositionsData.cpeMarketValue"],
      'prevDayCpeLongMarketValue': row["prevDayLcmPositionData.cpeReconciledPositionsData.cpeLongMarketValue"],
      'prevDayCpeShortMarketValue': row["prevDayLcmPositionData.cpeReconciledPositionsData.cpeShortMarketValue"],
      'prevDayCpeMarginUSD': row["prevDayLcmPositionData.cpeReconciledPositionsData.marginUSD"],
      'prevDayCpeSegMarginUSD': row["prevDayLcmPositionData.cpeReconciledPositionsData.segMarginUSD"],
      'prevDayCpeRegMarginUSD': row["prevDayLcmPositionData.cpeReconciledPositionsData.regMarginUSD"],
      'prevDayCpeExchangeMarginUSD': row["prevDayLcmPositionData.cpeReconciledPositionsData.exchangeMarginUSD"],
      'prevDayRecCode': row["prevDayLcmPositionData.cpeReconciledPositionsData.recCodes"],
      'prevDayCpeLoanAmount': row["prevDayLcmPositionData.cpeReconciledRepoData.loanAmount"],
      'prevDayCpeBondPrice': row["prevDayLcmPositionData.cpeReconciledRepoData.bondPrice"],
      'prevDayCpeBondMarketValue': row["prevDayLcmPositionData.cpeReconciledRepoData.bondMarketValue"],
      'prevDayCpeBondQuantity': row["prevDayLcmPositionData.cpeReconciledRepoData.bondQuantity"],
      'prevDayCpePoolFactor': row["prevDayLcmPositionData.cpeReconciledRepoData.poolFactor"],
      'prevDayCpeBondAccruedInterest': row["prevDayLcmPositionData.cpeReconciledRepoData.bondAccruedInterest"],
      'prevDayCpeLoanAccruedInterest': row["prevDayLcmPositionData.cpeReconciledRepoData.loanAccruedInterest"],
      'prevDayCpeHaircut': row["prevDayLcmPositionData.cpeHairCut"],
      'prevDayCpeExposure': row["prevDayLcmPositionData.cpeReconciledRepoData.exposure"]
    };
  }

  function _getPositionDetailDataFromArrayTable(data, enrichWithPrevDayLcmPositionData) {
    if (data && data.data && data.data.length) {
      let fieldsArr = [];
      data.fields.forEach(e => fieldsArr.push(e.name));
      data = data.data.map(value => _.zipObject(fieldsArr, value));
      if (enrichWithPrevDayLcmPositionData) {
        data = data.map((row) => (Object.assign(_getCurrDayPositionDetailData(row), _getPrevDayPositionDetailData(row))));
      } else {
        data = data.map((row) => (_getCurrDayPositionDetailData(row)));
      }
    }
    return data;
  }

  function _getDayOnDayDiffParam(enrichWithPrevDayLcmPositionData) {
    let dayOnDayDiffParam = "_arrayTable=true&_fieldNames=" + getCurrDayDayOnDayDiffColumns();
    if (enrichWithPrevDayLcmPositionData === true) {
      dayOnDayDiffParam += "," + getPrevDayDayOnDayDiffColumns();
    }
    return dayOnDayDiffParam;
  }


  function _loadpositionDetailDatas(parameters) {
    let url = "/treasury/service/lcmFeatureToggle/isUsePdrCache?format=json";
    fetch(url)
      .then((resp) => resp.json())
      .then(function (response) {
        return _loadpositionDetailData(parameters, response);
      })
      .catch((error) => {
        console.error("Error:", error);
        errorInGettingPdrData();
      });
  }
  function _loadpositionDetailData(parameters,usePdrCache) {
    treasury.positionDetail.util.showLoading();
    let url;

    let isCrimsonEnabled = parameters.isCrimsonEnabled === "true" ? true : false;
    if(isCrimsonEnabled){
        console.log("not using pdr cache as crimson is enabled");
        usePdrCache = false;
    }
    let fromExcessDeficitDataSource = parameters.fromExcessDeficitDataSource;
    let toExcessDeficitDataSource = parameters.toExcessDeficitDataSource;
    let excessDeficitDataType = parameters.excessDeficitDataType;
    let showSegDrilldown = parameters.showSegDrilldown;
    let showCrimsonDiffDrilldown = fromExcessDeficitDataSource && toExcessDeficitDataSource
      && excessDeficitDataType && showSegDrilldown != undefined;
    let dayOnDayDiffParam;
    let enrichWithPrevDayLcmPositionData = false;
    let pdrFilterUrl;
    if (isCrimsonEnabled) {
      if (showCrimsonDiffDrilldown) {
        url = "/treasury/service/lcmExcessDeficitService/getCrimsonDiffDrilldownPositions?" +
          "crimsonDrilldownPositionsInputParam.legalEntityIds=" + parameters.legalEntityIds +
          "&crimsonDrilldownPositionsInputParam.cpeIds=" + parameters.cpeIds +
          "&crimsonDrilldownPositionsInputParam.agreementTypeIds=" + parameters.agreementTypeIds +
          "&crimsonDrilldownPositionsInputParam.date=" + parameters.dateString +
          "&crimsonDrilldownPositionsInputParam.liveOnly=" + parameters.includeLiveAgreements +
          "&crimsonDrilldownPositionsInputParam.includeUnmappedBrokerRecord=" + parameters.includeUnmappedBrokerRecords +
          "&crimsonDrilldownPositionsInputParam.excessDeficitDataType=" + excessDeficitDataType +
          "&crimsonDrilldownPositionsInputParam.showSegDrilldown=" + showSegDrilldown +
          "&crimsonDrilldownPositionsInputParam.fromExcessDeficitDataSource=" + fromExcessDeficitDataSource +
          "&crimsonDrilldownPositionsInputParam.toExcessDeficitDataSource=" + toExcessDeficitDataSource +
          "&crimsonDrilldownPositionsInputParam.addCalculatedFields=" + true +
          "&crimsonDrilldownPositionsInputParam.enrichWithPrevDayLcmPositionData=" + enrichWithPrevDayLcmPositionData +
          "&format=JSON&inputFormat=PROPERTIES";

        dayOnDayDiffParam = _getDayOnDayDiffParam(enrichWithPrevDayLcmPositionData);

        pdrFilterUrl = "/treasury/service/positionDetailReportDataReadManager/getPdrFilters?" +
                  "filter.legalEntityIds=" + parameters.legalEntityIds +
                  "&filter.cpeIds=" + parameters.cpeIds +
                  "&filter.agreementTypeIds=" + parameters.agreementTypeIds +
                  "&filter.date=" + parameters.dateString +
                  "&filter.liveOnly=" + parameters.includeLiveAgreements +
                  "&filter.includeUnmappedBrokerRecord=" + parameters.includeUnmappedBrokerRecords +
                  "&filter.addCalculatedFields=" + true +
                  "&filter.enrichWithPrevDayLcmPositionData=" + enrichWithPrevDayLcmPositionData +
                  "&filter.isPublished=true" +
                  "&format=JSON&inputFormat=PROPERTIES";


      }
      else {
        enrichWithPrevDayLcmPositionData = true;
        url = "/treasury/service/lcmExcessDeficitService/getPositionDetail?" +
          "lcmPositionDataInput.legalEntityIds=" + parameters.legalEntityIds +
          "&lcmPositionDataInput.cpeIds=" + parameters.cpeIds +
          "&lcmPositionDataInput.agreementTypeIds=" + parameters.agreementTypeIds +
          "&lcmPositionDataInput.date=" + parameters.dateString +
          "&lcmPositionDataInput.liveOnly=" + true +
          "&lcmPositionDataInput.includeUnmappedBrokerRecord=" + true +
          "&lcmPositionDataInput.enrichWithPrevDayLcmPositionData=" + enrichWithPrevDayLcmPositionData +
          "&lcmPositionDataInput.addCalculatedFields=" + true;
        if (parameters.pnlSpns != undefined && parameters.pnlSpns !== "") {
          url += "&lcmPositionDataInput.pnlSpns=" + parameters.pnlSpns;
        }
        url += "&format=Json&inputFormat=PROPERTIES";
        dayOnDayDiffParam = _getDayOnDayDiffParam(enrichWithPrevDayLcmPositionData);

        pdrFilterUrl = "/treasury/service/positionDetailReportDataReadManager/getPdrFilters?" +
                  "filter.legalEntityIds=" + parameters.legalEntityIds +
                  "&filter.cpeIds=" + parameters.cpeIds +
                  "&filter.agreementTypeIds=" + parameters.agreementTypeIds +
                  "&filter.date=" + parameters.dateString +
                  "&filter.liveOnly=" + true +
                  "&filter.includeUnmappedBrokerRecord=" + true +
                  "&filter.enrichWithPrevDayLcmPositionData=" + enrichWithPrevDayLcmPositionData +
                  "&filter.isPublished=true" +
                  "&filter.addCalculatedFields=" + true;
        if (parameters.pnlSpns != undefined && parameters.pnlSpns !== "") {
             pdrFilterUrl += "&filter.pnlSpns=" + parameters.pnlSpns;
        }
         pdrFilterUrl += "&format=Json&inputFormat=PROPERTIES";
      }
    }
    else {
      url = "/treasury/comet/search-position-detail?" +
        "dateString=" + parameters.dateString +
        "&legalEntityIds=" + parameters.legalEntityIds +
        "&legalEntityFamilyIds=" + parameters.entityFamilyIds +
        "&agreementTypeIds=" + parameters.agreementTypeIds +
        "&bookIds=" + parameters.bookIds +
        "&gboTypeIds=" + parameters.gboTypeIds +
        "&cpeIds=" + parameters.cpeIds +
        "&includeUnmappedBrokerRecords=" + parameters.includeUnmappedBrokerRecords;
      if (parameters.pnlSpns != undefined) {
        url = url + "&pnlSpns=" + parameters.pnlSpns;
      }
      url = url + "&_outputKey=resultList";
      url = url + "&format=json";


       pdrFilterUrl = "/treasury/service/positionDetailReportDataReadManager/getPdrFilters?" +
                                "filter.date=" + parameters.dateString +
                                "&filter.legalEntityIds=" + parameters.legalEntityIds +
                                "&filter.isPublished=true" +
                                "&filter.agreementTypeIds=" + parameters.agreementTypeIds ;
           if(parameters.bookIds && parameters.bookIds!=-1){
           pdrFilterUrl = pdrFilterUrl+ "&filter.bookIds=" + parameters.bookIds
           }
           if(parameters.gboTypeIds && parameters.gboTypeIds!=-1){
           pdrFilterUrl = pdrFilterUrl+ "&filter.gboTypeIds=" + parameters.gboTypeIds
           }

          pdrFilterUrl= pdrFilterUrl+ "&filter.cpeIds=" + parameters.cpeIds +
                                          "&filter.includeUnmappedBrokerRecord=" + parameters.includeUnmappedBrokerRecords;
       if (parameters.pnlSpns&& parameters.pnlSpns!= -1) {
               pdrFilterUrl = pdrFilterUrl + "&filter.pnlSpns=" + parameters.pnlSpns;
       }
       pdrFilterUrl = pdrFilterUrl +  "&format=JSON&inputFormat=PROPERTIES";
    }


        if (usePdrCache) {
          document.getElementById('searchMessage').setAttribute('hidden', true);
          document.getElementById('quickLinks').setAttribute('hidden', true);
          document.getElementById('positionDetailGridDiv').setAttribute('hidden', true);
          treasury.positionDetail.util.clearGrid('positionDetailGrid');
          

          fetch(pdrFilterUrl)
            .then((resp) => resp.json())
            .then(function (response) {
              let filters = response;
              let i;
              let pdrDataUrls = [];
              for (i = 0; i < filters.length; i++) {
                let urlNew = getPdrDataUrl(filters[i]);
                pdrDataUrls.push(urlNew);
              }
              Promise.all(
                pdrDataUrls.map((url) =>
                  fetch(url, {
                    method: "POST",
                    body: dayOnDayDiffParam,
                  }).then((resp) => resp.json())
                )
              )
                .then((dataParts) => {
                  let data = [];
                  let k;
                  for (k = 0; k < dataParts.length; k++) {
                    data = data.concat(dataParts[k].resultList);
                  }
                  extractPdrData(data,usePdrCache)();
                })
                .catch(errorInGettingPdrData());
            })
            .catch((error) => {
              console.error("Error:", error);
              errorInGettingPdrData();
            });
        } else {
          var promise = $.ajax({
            url: url,
            data: dayOnDayDiffParam,
            type: 'POST',
          });
          document.getElementById('searchMessage').setAttribute('hidden', true);
          document.getElementById('quickLinks').setAttribute('hidden', true);
          document.getElementById('positionDetailGridDiv').setAttribute('hidden', true);
          treasury.positionDetail.util.clearGrid('positionDetailGrid');
          $.when(promise).done(function (data) {
            extractPdrData(data,usePdrCache)();
          });

          promise.fail(function (xhr, text, errorThrown) {
            treasury.positionDetail.util.hideLoading();
            treasury.positionDetail.util.showErrorMessage('Error occurred while retrieving data.');
          });
        }


    function extractPdrData(data,usePdrCache) {
      return function () {
        treasury.positionDetail.util.showLoading();
        var dateFilterValue = Date.parse(parameters["dateString"]);
        var minDate = Date.parse('1970-01-01');
        var maxDate = Date.parse('2038-01-01');
        var dateFormat = 'yy-mm-dd';
        treasury.positionDetail.util.setupDateFilter(
          $("#datePicker"), $("#datePickerError"),
          dateFilterValue, minDate, maxDate,
          dateFormat);

        var resData;
        if (isCrimsonEnabled) {
          resData = _getPositionDetailDataFromArrayTable(data, enrichWithPrevDayLcmPositionData);
        } else {
          if(usePdrCache){
            resData = data;
          }else{
            resData = data.resultList;
          }

        }

        if (resData && resData.length) {
          if (document.getElementById('positionDetailFilterSidebar').state == 'expanded') {
            document.getElementById('positionDetailFilterSidebar').toggle();
          }

          _updateId(resData);
          document.getElementById('positionDetailGrid').removeAttribute("hidden");
          document.getElementById('positionDetailGridDiv').removeAttribute("hidden");
          var searchDate = document.getElementById("searchDate");
          searchDate.innerHTML = parameters.dateString;

          var noOfRecords = document.getElementById("noOfRecords");
          noOfRecords.innerHTML = resData.length;

          document.getElementById('quickLinks').removeAttribute("hidden");
          var options = treasury.positionDetail.positionDetailGridOptions
            .getResultOptions();
          options.configureReport = {
            urlCallback: function () {
              return (window.location.origin + url);
            }
          };

          let panelColumns;
          if (isCrimsonEnabled && !showCrimsonDiffDrilldown) {
            panelColumns = treasury.positionDetail.positionDetailColumnConfig.getPanelColumnsEnrichedWithPrevDayColumns();
          } else {
            panelColumns = treasury.positionDetail.positionDetailColumnConfig.getPanelColumns();
          }

          positionDetailGrid = new dportal.grid.createGrid(
            $("#positionDetailGrid"), resData, panelColumns, options);
          treasury.positionDetail.util.resizeCanvasOnGridChange(positionDetailGrid,
            'positionDetailGrid');
          treasury.positionDetail.util.hideLoading();
        } else {
          treasury.positionDetail.util.hideLoading();
          treasury.positionDetail.util.clearGrid("positionDetailGrid");
          document.getElementById('positionDetailGridDiv').setAttribute("hidden", true);
          document.getElementById('searchMessage').removeAttribute("hidden");
          document.getElementById('quickLinks').setAttribute("hidden", true);
          document.getElementById('searchMessage').innerHTML = 'No records found for search criteria';
        }
      };
    }

    function errorInGettingPdrData() {
      return function (xhr, text, errorThrown) {
        treasury.positionDetail.util.hideLoading();
        treasury.positionDetail.util.showErrorMessage('Error occurred while retrieving data.');
      };
    }

    function getPdrDataUrl(value) {
      let urlNew = url + '';
      if (isCrimsonEnabled) {
        if (showCrimsonDiffDrilldown) {
          urlNew = urlNew + '&crimsonDrilldownPositionsInputParam.requestId=' + value.requestId;
          urlNew = urlNew + '&crimsonDrilldownPositionsInputParam.pageId=' + value.pageId;
        } else {
          urlNew = urlNew + '&lcmPositionDataInput.requestId=' + value.requestId;
          urlNew = urlNew + '&lcmPositionDataInput.pageId=' + value.pageId;
        }
      } else {
        urlNew = urlNew + '&requestId=' + value.requestId;
        urlNew = urlNew + '&pageId=' + value.pageId;
      }
      return urlNew;
    }
  }

})();