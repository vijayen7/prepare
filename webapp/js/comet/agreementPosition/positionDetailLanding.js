"use strict";
var positionDetailFilterGroup;
var entityFamilyMultiSelect;
var legalEntityMultiSelect;
var cpeMultiSelect;
var agreementTypeMultiSelect;
var bookMultiSelect;
var gboTypeMultiSelect;
var legalEntityIdsvalue;
let searchData;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.positionDetail = window.treasury.positionDetail || {};
  window.treasury.positionDetail.positionDetailLanding = {
    loadFilters: _loadFilters,
    loadSearchResults: _loadSearchResults,
    resetFilters: _resetFilters,
    copySearchUrl: _copySearchUrl,
    setFilters: _setFilters
  };

  var entityFamilyToEntitiesMap = [];
  var legalEntityAllDataMap = [];

  window.addEventListener("WebComponentsReady", _initialize);

  function _setFilters(parameters) {
    var entityFamilyIds = parameters["entityFamilyIds"];
    var legalEntityIds = parameters["legalEntityIds"];
    var cpeIds = parameters["cpeIds"];
    var agreementTypeIds = parameters["agreementTypeIds"];
    var bookIds = parameters["bookIds"];
    var gboTypeIds = parameters["gboTypeIds"];

    if (entityFamilyIds != undefined) {
      entityFamilyIds = entityFamilyIds.split(',');
      entityFamilyMultiSelect.setSelectedNodes(entityFamilyIds);
    }
    if (legalEntityIds != undefined) {
      legalEntityIds = legalEntityIds.split(',');
      legalEntityMultiSelect.setSelectedNodes(legalEntityIds);
    }

    if (cpeIds != undefined) {
      cpeIds = cpeIds.split(',');
      cpeMultiSelect.setSelectedNodes(cpeIds);
    }

    if (agreementTypeIds != undefined) {
      agreementTypeIds = agreementTypeIds.split(',');
      agreementTypeMultiSelect.setSelectedNodes(agreementTypeIds);
    }

    if (bookIds != undefined) {
      bookIds = bookIds.split(',');
      bookMultiSelect.setSelectedNodes(bookIds);
    }

    if (gboTypeIds != undefined) {
      gboTypeIds = gboTypeIds.split(',');
      gboTypeMultiSelect.setSelectedNodes(gboTypeIds);
    }

    let includeUnmappedPositions = document.getElementById("includeUnmappedPositions");
  if (parameters["includeUnmappedBrokerRecords"] != undefined && parameters["includeUnmappedBrokerRecords"].toString().includes("false")) {
    includeUnmappedPositions.checked = false;
  } else {
    includeUnmappedPositions.checked = true;
  }

    var pnlSpns = parameters["pnlSpns"];
    if (pnlSpns != undefined) {
      document.getElementById("spn").value = pnlSpns;
    }
    else{
        document.getElementById("spn").value = "";
      }
  }

  function _initialize() {

    $(document).on({
      ajaxStart: function() {
        treasury.positionDetail.util.showLoading("mainLoader");
      },
      ajaxStop: function() {
        treasury.positionDetail.util.hideLoading("mainLoader");
      }
    });

    var saveSettings = document.querySelector('arc-save-settings');
    var host = '';
    var clientName = document.getElementById("clientName").value;
    var stabilityLevel = document.getElementById("stabilityLevel").value;

    if(clientName === 'desco') {
        if(stabilityLevel == 'prod') {
            host += "http://landing-app.deshaw.c.ia55.net";
        } else if(stabilityLevel == 'uat'){
            host = "http://landing-app.deshawuat.c.ia55.net";
        } else if(stabilityLevel == 'qa'){
            host = "https://mars.arcesium.com";
        } else if(stabilityLevel == 'dev'){
            host = "https://terra.arcesium.com";
        }
    }
    saveSettings.serviceURL = host + '/service/SettingsService';
    saveSettings.applicationId = 3;
    saveSettings.applicationCategory = 'positionDetail';

    document.getElementById('summary-search-save').onclick = function() {
      saveSettings.openSaveDialog();
    };

    saveSettings.saveSettingsCallback = function() {

      var parameters = positionDetailFilterGroup.getSerializedParameterMap();
      var includeUnmappedPositions = document.getElementById("includeUnmappedPositions");
      if (includeUnmappedPositions != undefined) {
        parameters["includeUnmappedBrokerRecords"] = includeUnmappedPositions.checked;
      }
      parameters["pnlSpns"] = document.getElementById("spn").value;
      return parameters;

    }
    saveSettings.applySettingsCallback = function(parameters) {

      _setFilters(parameters);

    }



    _registerHandlers();
    _loadCopyUrlData();
    _loadFilters();


  }

  function _resetFilters() {
    entityFamilyMultiSelect.setSelectedNodes([]);
    legalEntityMultiSelect.setSelectedNodes([]);
    cpeMultiSelect.setSelectedNodes([]);
    agreementTypeMultiSelect.setSelectedNodes([]);
    bookMultiSelect.setSelectedNodes([]);
    gboTypeMultiSelect.setSelectedNodes([]);

  }

  function _registerHandlers() {
    document.getElementById('searchId').onclick = treasury.positionDetail.positionDetailLanding.loadSearchResults;
    document.getElementById('resetId').onclick = treasury.positionDetail.positionDetailLanding.resetFilters;
    document.getElementById('refreshId').onclick = treasury.positionDetail.positionDetailLanding.loadSearchResults;
    document.getElementById('copySearchURL').onclick = treasury.positionDetail.positionDetailLanding.copySearchUrl;
    document.getElementById('positionDetailFilterSidebar').addEventListener("stateChange",
      treasury.positionDetail.util.resizeAllCanvas);
  }

  function _loadFilters() {
    var errorDialog = document.getElementById('errorMessage');
    errorDialog.visible = false;


    $.when(treasury.loadfilter.loadEntityFamilies(),
        treasury.loadfilter.legalEntities(),
        treasury.loadfilter.defaultDates(),
        treasury.loadfilter.cpes(),
        treasury.loadfilter.loadAgreementTypes(),
        treasury.loadfilter.loadBooks(),
        treasury.loadfilter.loadGboTypes())
      .done(
        function(legalEntityFamiliesData, legalEntitiesData,
          defaultDatesData, cpesData, agreementTypesData, booksData, gboTypesData) {

          entityFamilyMultiSelect = new window.treasury.filter.MultiSelectNew(
            "entityFamilyIds",
            "entityFamilyFilter",
            legalEntityFamiliesData[0].descoEntityFamilies, []);

          legalEntityMultiSelect = new window.treasury.filter.MultiSelectNew(
            "legalEntityIds", "legalEntityFilter",
            legalEntitiesData[0].descoEntities, []);

          cpeMultiSelect = new window.treasury.filter.MultiSelectNew(
            "cpeIds", "cpeFilter",
            cpesData[0].cpes, []);


          agreementTypeMultiSelect = new window.treasury.filter.MultiSelectNew(
            "agreementTypeIds", "agreementTypeFilter",
            agreementTypesData[0].agreementTypes, []);

          bookMultiSelect = new window.treasury.filter.MultiSelectNew(
            "bookIds", "bookFilter",
            booksData[0].books, []);

          gboTypeMultiSelect = new window.treasury.filter.MultiSelectNew(
            "gboTypeIds", "gboTypeFilter",
            gboTypesData[0].gboTypes, []);


          var dateFilterValue = Date.parse(
            treasury.defaults.date ||
            defaultDatesData[0].tMinusOneFilterDate);
          var minDate = Date.parse('1970-01-01');
          var maxDate = Date.parse('2038-01-01');
          var dateFormat = 'yy-mm-dd';
          treasury.positionDetail.util.setupDateFilter(
            $("#datePicker"), $("#datePickerError"),
            dateFilterValue, minDate, maxDate,
            dateFormat);

          positionDetailFilterGroup = new treasury.filter.FilterGroup(
            [entityFamilyMultiSelect,
              legalEntityMultiSelect, cpeMultiSelect, agreementTypeMultiSelect, bookMultiSelect, gboTypeMultiSelect
            ], null,
            $("#datePicker"), null);

          var legalEntityFilter = document
            .getElementById("legalEntityFilter");
          var legalEntityFilterData = legalEntityFilter.data;

          for (var i = 0; i < legalEntityFilterData.length; i++) {
            legalEntityAllDataMap[legalEntityFilterData[i].key] = legalEntityFilterData[i];
          }

          var entityFamilyFilter = document
            .getElementById("entityFamilyFilter");
          var entityFamilyFilterData = entityFamilyFilter.data;

          var dataNodeIds = [];
          if (entityFamilyFilterData !== undefined) {
            for (var i = 0; i < entityFamilyFilterData.length; i++) {
              dataNodeIds
                .push(entityFamilyFilterData[i].key);
            }
          }


          if (legalEntityFamiliesData && legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap &&
            legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap.length) {
            var data = legalEntityFamiliesData[0].legalEntityFamilyToLegalEntityListMap
            for (var i = 0; i < data.length; i++) {

              var entityFamilyId = data[i].entityFamilyId;
              entityFamilyToEntitiesMap[entityFamilyId] = data[i].entities;

            }
          }

          entityFamilyFilter.onchange = function() {
            _handleEntityFamilyChange();
          };

          var saveSettings = document.querySelector('arc-save-settings');
          saveSettings.retrieveSettings();
          if(searchData) {
            _setFilters(searchData);
          }

        });
  }

  function _handleEntityFamilyChange() {

    var legalEntityFilter = document.getElementById("legalEntityFilter");

    var selectedEntityFamilies = entityFamilyMultiSelect.getSelectedIds();

    var legalEntitiesToShow = [];
    var legalEntitiesToSelect = [];

    if (selectedEntityFamilies != "-1") {
      for (var j = 0; j < selectedEntityFamilies.length; j++) {
        if (selectedEntityFamilies[j] in entityFamilyToEntitiesMap) {
          legalEntitiesToSelect = legalEntitiesToSelect
            .concat(entityFamilyToEntitiesMap[selectedEntityFamilies[j]]);
        }
      }

      for (var i = 0; i < legalEntitiesToSelect.length; i++) {
        if (legalEntitiesToSelect[i] in legalEntityAllDataMap) {
          legalEntitiesToShow
            .push(legalEntityAllDataMap[legalEntitiesToSelect[i]]);
        }

      }
    } else {
      for (var j in legalEntityAllDataMap) {
        legalEntitiesToShow.push(legalEntityAllDataMap[j]);
      }
    }

    legalEntityFilter.data = legalEntitiesToShow;
    var legalEntityIdsvalueArray=[];
    for (var j in legalEntitiesToShow) {
      legalEntityIdsvalueArray.push(legalEntitiesToShow[j].key);
    }
    legalEntityIdsvalue=legalEntityIdsvalueArray.join(',');
    treasury.positionDetail.util.resizeAllCanvas();

  }

  function _loadSearchResults() {

    if (document.getElementById('datePickerError').innerHTML != '') {
      ArcMessageHelper.showMessage('error',
        'Please select a valid date.', null, null);
      return;
    }
    if (new Date(document.getElementById('datePicker').value) == '') {
      ArcMessageHelper.showMessage('warning', 'Please select date.',
        null, null);
      return;
    }

    var parameters = positionDetailFilterGroup.getSerializedParameterMap();
    if (document.getElementById("spn").value != undefined) {
      parameters["pnlSpns"] = document.getElementById("spn").value;
    }

    var includeUnmappedPositions = document.getElementById("includeUnmappedPositions");
    if (includeUnmappedPositions != undefined) {
      parameters["includeUnmappedBrokerRecords"] = includeUnmappedPositions.checked;
      //document.getElementById("includeUnmappedBrokerRecords").value=includeUnmappedPositions.checked;
    }

    if(parameters["legalEntityIds"]=="-1" && parameters["entityFamilyIds"]!="-1"){
      parameters["legalEntityIds"]=legalEntityIdsvalue;
    }

    parameters["pnlSpns"] = document.getElementById("spn").value;
    treasury.positionDetail.positionDetailActions
      .loadpositionDetailData(parameters);

  }

  function _copySearchUrl() {


    treasury.positionDetail.util.showLoading("mainLoader");
    var url = "/treasury/comet/display-position-detail";
    var parameters = positionDetailFilterGroup.getSerializedParameterMap();
    parameters["isCopyUrl"] = true;
    if (document.getElementById("spn").value != undefined) {
      parameters["pnlSpns"] = document.getElementById("spn").value;
    }
    var includeUnmappedPositions = document.getElementById("includeUnmappedPositions");
    if (includeUnmappedPositions != undefined) {
      parameters["includeUnmappedBrokerRecords"] = includeUnmappedPositions.checked;
      document.getElementById("includeUnmappedBrokerRecords").value = includeUnmappedPositions.checked;
    }
    if(parameters["legalEntityIds"]=="-1" && parameters["entityFamilyIds"]!="-1"){
      parameters["legalEntityIds"]=legalEntityIdsvalue;
    }
    generateReportURL(url, parameters, "resultList");
    treasury.positionDetail.util.hideLoading("mainLoader");
  }


  function _loadCopyUrlData() {
    if (window.location.href.indexOf("isCopyUrl=true") >= 0) {
      if (document.getElementById('positionDetailFilterSidebar').state == 'expanded') {
        document.getElementById('positionDetailFilterSidebar').toggle();
      }

      treasury.positionDetail.util.showLoading();

      var urlParams = window.location.href.split("?");
      if (urlParams[1] != undefined) {
        var paramList = urlParams[1].split("&");
      }
      searchData = {};
      for (var i = 0; i < paramList.length; i++) {
        var param = paramList[i].split("=");
        var val = param[1];
        if (val != null && param[0] != "isCopyUrl") {
          searchData[param[0]] = val;
        }
      }

      treasury.positionDetail.positionDetailActions
        .loadpositionDetailData(searchData);




    } else {
      document.getElementById('searchMessage').removeAttribute("hidden");
    }

  }

})();
