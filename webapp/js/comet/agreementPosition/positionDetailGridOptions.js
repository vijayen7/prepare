"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.positionDetail = window.treasury.positionDetail || {};
  window.treasury.positionDetail.positionDetailGridOptions = {
    getResultOptions: _getResultOptions
  };

function _getResultOptions() {

	var host = '';
    var clientName = document.getElementById("clientName").value;
    var stabilityLevel = document.getElementById("stabilityLevel").value;

    if(clientName === 'desco') {
        if(stabilityLevel == 'prod') {
            host += "http://landing-app.deshaw.c.ia55.net";
        } else if(stabilityLevel == 'uat'){
            host = "http://landing-app.deshawuat.c.ia55.net";
        } else if(stabilityLevel == 'qa'){
            host = "https://mars.arcesium.com";
        } else if(stabilityLevel == 'dev'){
            host = "https://terra.arcesium.com";
        }
    }
		var options = {
        editable : true,
        formatAggregateSummary : function(formattedText, AggregationsObj){return(
            `<div class="size--content layout--flex" style="float: right; width: fit-content;">
              <div class="size--content margin--left--small margin--right--small">Count:`+Math.round(isNaN(AggregationsObj.count) ? 0: AggregationsObj.count)+`</div>
              <div class="size--content margin--left--small margin--right--small">Sum:`+Math.round(isNaN(AggregationsObj.sum) ? 0 : AggregationsObj.sum)+`</div>
              <div class="size--content margin--left--small margin--right--small">Average:`+Math.round(isNaN(AggregationsObj.avg) ? 0 : AggregationsObj.avg)+`</div>
            </div>`
        )},          
        exportToExcel : true,
        highlightRowOnClick : true,
        enableCellNavigation : true,
        autoHorizontalScrollBar : true,
        useAvailableScreenSpace : true,
        customColumnSelection : true,
        displaySummaryRow : true,
        asyncEditorLoading : false,
        autoEdit : true,
        configureColumns : true,
        applyFilteringOnGrid : true,
        enableMultilevelGrouping: {
            groupingControls: true,
            showGroupingTotal: true,
            customGroupingRendering: true,
            isAddGroupingColumn: true
          },
          saveSettings: {
              applicationId: 3,
              applicationCategory: 'cometPositionDetailGrid',
              serviceURL: host+'/service/SettingsService'
          },
          cellRangeSelection: {
          	showAggregations: true,
          	multipleRangesEnabled: true
          },
         copyCellSelection : true,

    };
    return options;

}

})();
