'user strict';

(function () {
  window.treasury = window.treasury || {};
  window.treasury.comet = window.treasury.comet || {};
  window.treasury.comet.agreementPositionGridAction = {
    handlePositionDataDisplay: _handlePositionDataDisplay,
    handleTabChange: _handleTabChange,
    setSelectedAgreementId: _setSelectedAgreementId,
    setExcessDeficitWorkerData: _setExcessDeficitWorkerData,
    addCollateralAdjustment: _addCollateralAdjustment,
    getIsBundlePosLoaded: _getIsBundlePosLoaded,
    setIsBundlePosLoaded: _setIsBundlePosLoaded,
    getIsPosLoaded: _getIsPosLoaded,
    setIsPosLoaded: _setIsPosLoaded
  };

  var _selectedAgreementId;
  var _agmtIdToExpReqPositionsMap;
  var _agmtIdUsagePositionsMap;
  var _agmtIdToCashCollateralDataMap;
  var _agmtIdToPhysicalCollateralDataMap;
  var _excessDeficitWorkerDataList;
  var _isPosLoaded = false;
  var _isBundlePosLoaded = false;
  var _isInternalDataMissing = false;

  function _getIsBundlePosLoaded() {
    return _isBundlePosLoaded;
  }

  function _getIsPosLoaded() {
    return _isPosLoaded;
  }

  function _setIsBundlePosLoaded(value) {
    _isBundlePosLoaded = value;
  }

  function _setIsPosLoaded(value) {
    _isPosLoaded = value;
  }

  function _setExcessDeficitWorkerData(excessDeficitWorkerData) {
    _excessDeficitWorkerDataList = excessDeficitWorkerData;
  }

  function _handlePositionDataDisplay() {

    let treasuryAgreementData = _excessDeficitWorkerDataList[0];

    if (treasuryAgreementData) {
      _isInternalDataMissing = treasuryAgreementData.isInternalDataMissing;

      if (treasuryAgreementData.legalEntity) {
        var legalEntity = treasuryAgreementData.legalEntity.displayName;
      }

      var topPanel = document.getElementById("agreementTopPanelId");
      topPanel.label = "Agreement Workflow for " + legalEntity + " dated " + treasuryAgreementData.date;

      if (_agmtIdToExpReqPositionsMap === null || _agmtIdToExpReqPositionsMap === undefined) {
        _agmtIdToExpReqPositionsMap = new Object();
      }

      if (_agmtIdUsagePositionsMap === null || _agmtIdUsagePositionsMap === undefined) {
        _agmtIdUsagePositionsMap = new Object();
      }
      var childAgreementIds = [];
      if (_agmtIdToCashCollateralDataMap === null || _agmtIdToCashCollateralDataMap === undefined) {
        _agmtIdToCashCollateralDataMap = new Object();
      }

      if (_agmtIdToPhysicalCollateralDataMap === null || _agmtIdToPhysicalCollateralDataMap === undefined) {
        _agmtIdToPhysicalCollateralDataMap = new Object();
      }

      if (treasuryAgreementData.children != undefined) {
        for (let j = 0; j < treasuryAgreementData.children.length; j++) {

          let agreementData = treasuryAgreementData.children[j];

          if (childAgreementIds.indexOf(agreementData.agreementId) == -1) {
            childAgreementIds = childAgreementIds.concat(agreementData.agreementId);
          }

          if (agreementData.positionDataList != undefined) {
            for (let i = 0; i < agreementData.positionDataList.length; i++) {
              agreementData.positionDataList[i].agreementId = agreementData.agreementId;
              agreementData.positionDataList[i].adjustment = "";
            }
          }

          if (agreementData.bundlePositionDataList != undefined) {
            for (let i = 0; i < agreementData.bundlePositionDataList.length; i++) {
              agreementData.bundlePositionDataList[i].agreementId = agreementData.agreementId;
              agreementData.bundlePositionDataList[i].adjustment = "";

            }
          }

          if (agreementData.lcmPhysicalCollateralPositionDataList != undefined) {
            for (let i = 0; i < agreementData.lcmPhysicalCollateralPositionDataList.length; i++) {
              agreementData.lcmPhysicalCollateralPositionDataList[i].agreementId = agreementData.agreementId;
              agreementData.lcmPhysicalCollateralPositionDataList[i].adjustment = '';
            }
          }

          if (agreementData.custodianAccountDataList != undefined) {
            for (let i = 0; i < agreementData.custodianAccountDataList.length; i++) {
              var custodianAccountData = agreementData.custodianAccountDataList[i];
              agreementData.custodianAccountDataList[i].agreementId = agreementData.agreementId;
              // Replacing space
              if (custodianAccountData.custodianAccount && custodianAccountData.custodianAccount.displayName) {
                custodianAccountData.custodianAccountAbbrev = custodianAccountData.custodianAccount.displayName.replace(/\s/g, '&nbsp;');
              }
              agreementData.custodianAccountDataList[i].adjustment = "";

            }
          }

          if (agreementData.positionDataList != undefined) {
            _agmtIdToExpReqPositionsMap[agreementData.agreementId] = clean(agreementData.positionDataList, undefined);
          }
          if (agreementData.lcmPhysicalCollateralPositionDataList != undefined) {
            _agmtIdToPhysicalCollateralDataMap[agreementData.agreementId] = clean(
              agreementData.lcmPhysicalCollateralPositionDataList,
              undefined
            );
          }
          if (agreementData.bundlePositionDataList != undefined) {
            _agmtIdUsagePositionsMap[agreementData.agreementId] = clean(agreementData.bundlePositionDataList, undefined);
          }
          if (agreementData.custodianAccountDataList != undefined) {
            _agmtIdToCashCollateralDataMap[agreementData.agreementId] = clean(agreementData.custodianAccountDataList, undefined);
          }
        }

      }
    }

    _setSelectedAgreementId(treasuryAgreementData.agreementId);

    if (treasuryAgreementData.agreementType == 'MNA') {
      _agmtIdToExpReqPositionsMap[treasuryAgreementData.agreementId] = [];
      _agmtIdUsagePositionsMap[treasuryAgreementData.agreementId] = [];
      _agmtIdToCashCollateralDataMap[treasuryAgreementData.agreementId] = [];
      _agmtIdToPhysicalCollateralDataMap[treasuryAgreementData.agreementId] = [];

      for (var i = 0; i < childAgreementIds.length; i++) {

        if (_agmtIdToExpReqPositionsMap[childAgreementIds[i]] != undefined) {
          _agmtIdToExpReqPositionsMap[treasuryAgreementData.agreementId] = _agmtIdToExpReqPositionsMap[treasuryAgreementData.agreementId].concat(_agmtIdToExpReqPositionsMap[childAgreementIds[i]]);
        }

        if (_agmtIdUsagePositionsMap[childAgreementIds[i]] != undefined) {
          _agmtIdUsagePositionsMap[treasuryAgreementData.agreementId] = _agmtIdUsagePositionsMap[treasuryAgreementData.agreementId].concat(_agmtIdUsagePositionsMap[childAgreementIds[i]]);
        }

        if (_agmtIdToCashCollateralDataMap[childAgreementIds[i]] != undefined) {
          _agmtIdToCashCollateralDataMap[treasuryAgreementData.agreementId] = _agmtIdToCashCollateralDataMap[
            treasuryAgreementData.agreementId
          ].concat(_agmtIdToCashCollateralDataMap[childAgreementIds[i]]);
        }

        if (_agmtIdToPhysicalCollateralDataMap[childAgreementIds[i]] != undefined) {
          _agmtIdToPhysicalCollateralDataMap[treasuryAgreementData.agreementId] = _agmtIdToPhysicalCollateralDataMap[
            treasuryAgreementData.agreementId
          ].concat(_agmtIdToPhysicalCollateralDataMap[childAgreementIds[i]]);
        }
      }
    }


    handlePositionsForActionableIsda();
    var positionPanelTab = document.getElementById("positionDataPanelId");
    positionPanelTab.addEventListener('stateChange', _handleTabChange);
    _loadDetailedInformationTabs();

  }

  /**
   * Helper method for adding collateral adjustment
   *
   */
  function _addCollateralAdjustment(workflowId, agreementId, legalEntityId, expCounterPartyId, agreementTypeId, date, custodianAccountAbbrev, custodianAccountId, reportingCurrency) {
    event.stopPropagation();
    window.treasury.comet.agreementAdjustmentAction.setWorkflowId(workflowId);
    window.treasury.comet.agreementAdjustmentAction.setSourceGridConfig('COLLATERAL_GRID');
    window.treasury.comet.agreementAdjustmentAction.setSelectedAgreementId(agreementId);
    window.treasury.comet.agreementAdjustmentAction.handleAdjustmentPopupDisplay(date, legalEntityId, expCounterPartyId, agreementTypeId, null, custodianAccountAbbrev, null, custodianAccountId, reportingCurrency);
  }

  function handlePositionsForActionableIsda() {

    for (var i = 1; i < _excessDeficitWorkerDataList.length; i++) {

      if (_excessDeficitWorkerDataList[i].agreementId < 0) {
        var agreementData = _excessDeficitWorkerDataList[i];

        var agreementId = -1 * agreementData.agreementId;

        if (agreementData.positionDataList != undefined) {
          for (let i = 0; i < agreementData.positionDataList.length; i++) {
            agreementData.positionDataList[i].adjustment = "";
            agreementData.positionDataList[i].agreementid = agreementId;

          }
        }

        if (agreementData.lcmPhysicalCollateralPositionDataList != undefined) {
          for (let i = 0; i < agreementData.lcmPhysicalCollateralPositionDataList.length; i++) {
            agreementData.lcmPhysicalCollateralPositionDataList[i].agreementId = agreementData.agreementId;
            agreementData.lcmPhysicalCollateralPositionDataList[i].adjustment = '';
          }
        }

        if (agreementData.bundlePositionDataList != undefined) {
          for (let i = 0; i < agreementData.bundlePositionDataList.length; i++) {
            agreementData.bundlePositionDataList[i].agreementId = agreementId;
            agreementData.bundlePositionDataList[i].adjustment = "";

          }
        }

        if (agreementData.custodianAccountDataList != undefined) {
          for (let i = 0; i < agreementData.custodianAccountDataList.length; i++) {
            var custodianAccountData = agreementData.custodianAccountDataList[i];
            agreementData.custodianAccountDataList[i].agreementId = agreementId;
            // Replacing space
            custodianAccountData.custodianAccountAbbrev = custodianAccountData.custodianAccountAbbrev.replace(/\s/g, '&nbsp;');;
            agreementData.custodianAccountDataList[i].adjustment = "";

          }
        }

        if (agreementData.positionDataList != undefined && _agmtIdToExpReqPositionsMap[agreementData.agreementId] == null) {
          _agmtIdToExpReqPositionsMap[agreementData.agreementId] = clean(agreementData.positionDataList, undefined);
        }
        if (agreementData.bundlePositionDataList != undefined && _agmtIdUsagePositionsMap[agreementData.agreementId] == null) {
          _agmtIdUsagePositionsMap[agreementData.agreementId] = clean(agreementData.bundlePositionDataList, undefined);
        }
        if (agreementData.lcmPhysicalCollateralPositionDataList != undefined && _agmtIdToPhysicalCollateralDataMap[agreementData.agreementId] == null) {
          _agmtIdToPhysicalCollateralDataMap[agreementData.agreementId] = clean(agreementData.lcmPhysicalCollateralPositionDataList, undefined);
        }
        if (agreementData.custodianAccountDataList != undefined && _agmtIdToCashCollateralDataMap[agreementData.agreementId] == null) {
          _agmtIdToCashCollateralDataMap[agreementData.agreementId] = clean(agreementData.custodianAccountDataList, undefined);
        }
      }
    }
  }
  function clean(arr, deleteValue) {
    if (arr != undefined) {
      for (var i = 0; i < arr.length; i++) {
        if (arr[i] == deleteValue) {
          arr.splice(i, 1);
          i--;
        }
      }
      return arr;
    }
    return arr;
  }

  function updateId(positions) {
    if (positions != undefined) {
      if (positions.length > 0) {
        for (var i = 0; i < positions.length; i++) {
          positions[i].id = i;
        }
      }
    }
    return positions;
  }

  function _handleTabChange() {

    var positionPanelTab = document.getElementById("positionDataPanelId");

    if (positionPanelTab.selectedTab === "Exposure &amp; Requirement" || positionPanelTab.selectedTab === "Exposure & Requirement") {
      if (_isPosLoaded == true) {
        _loadDetailedInformationTabs();

      }
    }

    if (positionPanelTab.selectedTab === 'Cash Collateral') {
      _loadDetailedInformationTabs();
    }

    if (positionPanelTab.selectedTab === 'Securities Collateral') {
      if (_isPosLoaded == true) {
        _loadDetailedInformationTabs();
      }
    }

    if (positionPanelTab.selectedTab === "Usage") {
      if (_isBundlePosLoaded == false && !_isInternalDataMissing) {
        window.treasury.comet.agreementWorkflowGridAction.loadBundlePositionData();
        //_isBundlePosLoaded = true;
      } else {
        _loadDetailedInformationTabs();
      }
    }
  }

  function _loadDetailedInformationTabs() {
    window.treasury.comet.agreementworkflow.showLoading();
    var positionPanelTab = document.getElementById("positionDataPanelId");

    if (positionPanelTab.selectedTab === "Exposure &amp; Requirement" || positionPanelTab.selectedTab === "Exposure & Requirement") {
      let exposureRequirementPositionDataList = _agmtIdToExpReqPositionsMap[_selectedAgreementId];
      updateId(exposureRequirementPositionDataList);
      if (exposureRequirementPositionDataList && exposureRequirementPositionDataList.length > 0) {
        document.getElementById('ExposureLoader').setAttribute("hidden", true);
        document.getElementById('ExposureMessage').setAttribute("hidden", true);
        ReactDOM.unmountComponentAtNode(document.querySelector('#exposureRequirementGrid'));
        ReactDOM.render(
          React.createElement(TreasuryComponents.AgreementWorkflowComponents.exposureRequirementPositionsGrid, {
            exposureRequirementPositionDataList: exposureRequirementPositionDataList,
            handleAddAdjustment: _handleAddAdjustment
          }),
          document.querySelector('#exposureRequirementGrid')
        );
      } else {
        window.treasury.positionDetail.util.clearGrid("exposureRequirementGrid");
        if (_isPosLoaded) {
          document.getElementById('ExposureLoader').setAttribute("hidden", true);
          document.getElementById('ExposureMessage').removeAttribute("hidden");
        }
      }
    }

    if (positionPanelTab.selectedTab === 'Cash Collateral') {
      let cashCollateralDataList = _agmtIdToCashCollateralDataMap[_selectedAgreementId];
      updateId(cashCollateralDataList);
      if (cashCollateralDataList && cashCollateralDataList.length > 0) {
        document.getElementById('CashCollateralLoader').setAttribute('hidden', true);
        document.getElementById('CashCollateralMessage').setAttribute('hidden', true);
        ReactDOM.unmountComponentAtNode(document.querySelector('#cashCollateralGrid'));
        ReactDOM.render(
          React.createElement(TreasuryComponents.AgreementWorkflowComponents.cashCollateralGridComponent, {
            cashCollateralDataList: cashCollateralDataList,
            handleAddAdjustment: _handleAddAdjustment
          }),
          document.querySelector('#cashCollateralGrid')
        );
      } else {
        window.treasury.positionDetail.util.clearGrid('cashCollateralGrid');
        document.getElementById('CashCollateralLoader').setAttribute('hidden', true);
        document.getElementById('CashCollateralMessage').removeAttribute('hidden');
      }
    }

    if (positionPanelTab.selectedTab === 'Securities Collateral') {
      let lcmPhysicalCollateralPositionDataList = _agmtIdToPhysicalCollateralDataMap[_selectedAgreementId];
      updateId(lcmPhysicalCollateralPositionDataList);
      if (lcmPhysicalCollateralPositionDataList && lcmPhysicalCollateralPositionDataList.length > 0) {
        document.getElementById('PhysicalCollateralLoader').setAttribute('hidden', true);
        document.getElementById('PhysicalCollateralMessage').setAttribute('hidden', true);
        ReactDOM.unmountComponentAtNode(document.querySelector('#physicalCollateralGrid'));
        ReactDOM.render(
          React.createElement(TreasuryComponents.AgreementWorkflowComponents.securitiesCollateralGridComponent, {
            securitiesCollateralDataList: lcmPhysicalCollateralPositionDataList,
            handleAddAdjustment: _handleAddAdjustment
          }),
          document.querySelector('#physicalCollateralGrid')
        );
      } else {
        window.treasury.positionDetail.util.clearGrid('physicalCollateralGrid');
        if (_isPosLoaded) {
          document.getElementById('PhysicalCollateralLoader').setAttribute('hidden', true);
          document.getElementById('PhysicalCollateralMessage').removeAttribute('hidden');
        }
      }
    }

    if (positionPanelTab.selectedTab === "Usage") {
      let bundlePositionDataList = _agmtIdUsagePositionsMap[_selectedAgreementId];
      updateId(bundlePositionDataList);
      if (bundlePositionDataList && bundlePositionDataList.length > 0) {
        document.getElementById('UsageLoader').setAttribute("hidden", true);
        document.getElementById('UsageMessage').setAttribute("hidden", true);
        ReactDOM.unmountComponentAtNode(document.querySelector('#usageGrid'));
        ReactDOM.render(
          React.createElement(TreasuryComponents.AgreementWorkflowComponents.usageGrid, {
            bundlePositionDataList: bundlePositionDataList,
            handleAddAdjustment: _handleAddAdjustment
          }),
          document.querySelector('#usageGrid')
        );
      } else {
        window.treasury.positionDetail.util.clearGrid("usageGrid");
        if (_isBundlePosLoaded || _isInternalDataMissing) {
          document.getElementById('UsageLoader').setAttribute("hidden", true);
          document.getElementById('UsageMessage').removeAttribute("hidden");
        }
      }
    }
    window.treasury.comet.agreementworkflow.hideLoading();
  }

  function _handleAddAdjustment(sourceGridConfig, agreementId, startDataStr, legalEntityId,
    counterPartyEntityId, agreementTypeId, calculator,
    custodianAccount, pnlSpn, custodianAccountId, entityFamilyId,
    actionableType, businessUnitId, bookId, reportingCurrency) {

    window.treasury.comet.agreementAdjustmentAction.setWorkflowId(workflowId.value);
    window.treasury.comet.agreementAdjustmentAction.setSourceGridConfig(sourceGridConfig);
    window.treasury.comet.agreementAdjustmentAction.setSelectedAgreementId(agreementId);
    window.treasury.comet.agreementAdjustmentAction.handleAdjustmentPopupDisplay(startDataStr, legalEntityId,
      counterPartyEntityId, agreementTypeId, calculator,
      custodianAccount, pnlSpn, custodianAccountId, entityFamilyId,
      actionableType, businessUnitId, bookId, reportingCurrency)
  }

  function _setSelectedAgreementId(agreementId) {
    _selectedAgreementId = agreementId;
  }

})();
