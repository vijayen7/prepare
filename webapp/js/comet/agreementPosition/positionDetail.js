"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.comet = window.treasury.comet || {};
    window.treasury.comet.positionDetail = {
        showLoading : _showLoading,
        hideLoading : _hideLoading,
        loadPositionDetailData : _loadPositionDetailData
    };

    function _showLoading() {
        document.getElementById("positionDetailGridLoading").show();
    }

    function _hideLoading() {
        document.getElementById("positionDetailGridLoading").hide();
    }

    function _loadPositionDetailView() {

        // step 1 : Hide Detail view and load Summary view
        var positionDetailView = document.getElementById("positionDetailView");

        positionDetailView.style.display = "block";
        positionDetailView.setAttribute("size", "1");

    }

    // _initialize is called on page load.
    function _initialize() {

        // Initialize saved setting component
        _loadPositionDetailView();

        // step 2 : Load Summary view filters if not already loaded
        window.treasury.comet.positionDetailFilterAction.loadPositionDetailFilters(function() {_loadPositionDetailData();});

    }

    function _loadPositionDetailData() {

        var includeUnmappedPositions = _getIncludeUnmappedPositionFilter();

        var datepicker = document.getElementById('datepicker');
        var defaultParameters = {
            "cpeIds" : window.treasury.comet.positionDetailFilterAction.getSelectedCounterParties(),
            "legalEntityIds" : window.treasury.comet.positionDetailFilterAction.getSelectedLegalEntities(),
            "agreementTypeIds" : window.treasury.comet.positionDetailFilterAction.getSelectedAgreementTypes(),
            "bookIds" : window.treasury.comet.positionDetailFilterAction.getSelectedBooks(),
            "gboTypeIds" : window.treasury.comet.positionDetailFilterAction.getSelectedGboTypes(),
            "pnlSpns" : window.treasury.comet.positionDetailFilterAction.getInputPnlSpns(),
            "dateString" : datepicker.value,
            "includeUnmappedBrokerRecords" : includeUnmappedPositions
        };

        window.treasury.comet.positionDetailAction.positionDetailDisplay(defaultParameters);
    }

    /**
     * Helper method to get workflow status
     */
    function _getIncludeUnmappedPositionFilter() {
        return document.getElementById('includeUnmappedPositions').checked;
    }

    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return year + '/' + month + '/' + day;
    }

    window.addEventListener("WebComponentsReady", _initialize);

})();