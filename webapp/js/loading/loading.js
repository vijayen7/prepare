class LoaderElement extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    const sr= this.attachShadow({
      mode: "open"
    });

    let template = `
    <slot></slot>`;
    sr.innerHTML = template;
    this.show = function() {
      this.style.display = "block";
    };

    this.hide = function() {
      this.style.display = "none";
    };
  }
}

customElements.define("arc-loading", LoaderElement);
