"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.margin = window.treasury.margin || {};
  window.treasury.margin.util = {
    showErrorMessage: _showErrorMessage,
    setupDateFilter: _setupDateFilter,
    clearGrid: _clearGrid,
    resizeCanvasOnGridChange: _resizeCanvasOnGridChange,
    resizeAllCanvas: _resizeAllCanvas,
    getFormattedMessage: _getFormattedMessage,
    resizeGrids : _resizeGrids,
    resizeGrid : _resizeGrid
  };

  function _resizeGrid() {
    var searchPositionsGridDiv = document.getElementById('searchPositionsGridDiv');
    var totalHeight = searchPositionsGridDiv.offsetHeight;
    searchPositionsGrid.options.maxHeight = totalHeight - 50;
    searchPositionsGrid.resizeCanvas();
  }
function _resizeGrids() {
  var searchPositionsGridDiv = document.getElementById('searchPositionsGridDiv');
  var totalHeight = searchPositionsGridDiv.offsetHeight;
  searchPositionsGrid.options.maxHeight = totalHeight/2 - 70;
  searchPositionsChildGrid.options.maxHeight = totalHeight/2 - 70;
  $('#searchPositionsGrid-pager').addClass('margin--bottom');
  searchPositionsGrid.resizeCanvas();
  searchPositionsChildGrid.resizeCanvas();

}
  function _clearGrid(gridId) {
    treasury.common.grid.clearGrid(gridId);
    if ($("#" + gridId).prev().prev().is("p") ||
      $("#" + gridId).prev().prev().is("center")) {
      $("#" + gridId).prev().prev().remove();
    } else if ($("#" + gridId).prev().is("p") ||
      $("#" + gridId).prev().is("center")) {
      $("#" + gridId).prev().remove();
    }
    var groupingHeader = document.getElementById(gridId + '-drop-element-holder');
    if (groupingHeader != null) {
      groupingHeader.innerHTML = "";
    }

    var header = document.getElementById(gridId + "-header");
    if (header != null) {
      header.innerHTML = "";
    }

    var pager = document.getElementById(gridId + "-pager");
    if (pager != null) {
      pager.innerHTML = "";
    }
  }

  function _setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
    dateFormat) {
    dateDiv.datepicker({
      dateFormat: dateFormat,
      changeMonth: true,
      changeYear: true,
      minDate: minDate,
      maxDate: maxDate,
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
    }).datepicker("setDate", date);
    dateDiv.change(function() {
      validateDate(dateDiv.val(), {
        errorDiv: errorDiv,
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: dateFormat,
      });
    });
  }

  function _showErrorMessage(message) {
    var errorDialog = document.getElementById("errorMessage");

    errorDialog.reveal({
      'title': 'Error',
      'content': "<center><strong>" + message +
        "</strong></center>",
      'modal': true,
      'buttons': [{
        'html': 'OK',
        'callback': function() {
          this.visible = false;
        },
        'position': 'right'
      }]
    });
    errorDialog.removeAttribute("hidden");
  }

  function _resizeAllCanvas() {
    if(document.getElementById('addPositionsFilterSidebar').state == 'expanded') {
      document.getElementById('addPositionsFilterSidebar').style.width = '300px';
    } else {
      document.getElementById('addPositionsFilterSidebar').style.width = '20px';
    }
    if(searchPositionsGrid != null) {
    	searchPositionsGrid.resizeCanvas();
        }
        if(searchPositionsChildGrid != null) {
        	searchPositionsChildGrid.resizeCanvas();
        }

  }

  function _resizeCanvasOnGridChange(gridName, gridId) {
    if (gridId != undefined) {
        $('#' + gridId).height($('#' + gridId).height() + 11);
    }
    if(gridName != null) {
      gridName.resizeCanvas();
    }

  }

  function _getFormattedMessage(message) {
      return "<center><div class='message no-icon' style='width: 30%;'>" + message + "</div></center>";
  }

  function _getFormattedData(value, precision) {
    if(value == null || value == "") {
      return "";
    } else {
      return parseFloat(value).toFixed(precision);
    }
  }
})();
