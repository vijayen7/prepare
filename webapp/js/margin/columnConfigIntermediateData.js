"use strict";
(function() {
  window.treasury = window.treasury || {};
  window.treasury.margin = window.treasury.margin || {};
  window.treasury.margin.intermediateData = window.treasury.margin.intermediateData || {};
  window.treasury.margin.intermediateData.gridColumns = {
    getMethodologySummaryColumns : _getMethodologySummaryColumns,
    createColumnConfigurationforData: _createColumnConfigurationforData,
    getPriorityBasedColumnList : _getPriorityBasedColumnList
  };
  function _getMethodologySummaryColumns(showDataInRC) {
    const RC_SUFFIX = "RC";
    const USD_SUFFIX = "USD";
    const RC_ID_OR_FIELD_SUFFIX = "Rc";
    const EMPTY_STRING = "";

    var columns = [
      {
        id : "name",
        name : "Agreement",
        field : "name",
        toolTip : "Agreement",
        type : "text",
        sortable : true,
        headerCssClass : "b",
        formatter : function(row, cell, value, columnDef, dataContext) {
          return treasury.formatters.drillThrough(row,
            cell,
            value,
            columnDef,
            dataContext,
            undefined,
            function(row, cell, value, columnDef, dataContext) {
              return typeof(dataContext["children"]) === "undefined";
            },
            JSON.stringify(dataContext));
          }
        },
        {
          id : "lmv" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
          name : "LMV " + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
          field : "lmv" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
          toolTip : "LMV " + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
          type : "number",
          sortable : true,
          formatter : dpGrid.Formatters.Number,
          aggregator : dpGrid.Aggregators.sum,
          headerCssClass : "aln-rt b",
          excelFormatter : "#,##0.0"
        },
        {
          id : "smv" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
          name : "SMV " + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
          field : "smv" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
          toolTip : "SMV " + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
          type : "number",
          sortable : true,
          formatter : dpGrid.Formatters.Number,
          aggregator : dpGrid.Aggregators.sum,
          headerCssClass : "aln-rt b",
          excelFormatter : "#,##0.0"
        },
        {
          id : "margin" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
          name : "Margin " + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
          field : "margin" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
          toolTip : "Margin " + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
          type : "number",
          sortable : true,
          formatter : dpGrid.Formatters.Number,
          aggregator : dpGrid.Aggregators.sum,
          headerCssClass : "aln-rt b",
          excelFormatter : "#,##0.0"
        },
        {
          id : "usage" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
          name : "Usage " + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
          field : "usage" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
          toolTip : "Usage " + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
          type : "number",
          sortable : true,
          formatter : dpGrid.Formatters.Number,
          aggregator : dpGrid.Aggregators.sum,
          headerCssClass : "aln-rt b",
          excelFormatter : "#,##0.0"
        }
      ];

      return columns;
    }

    /***************************************************************************
  	 * Creates column configs based on the data available e.g. columns config
  	 * for position attributes
  	 **************************************************************************/
  	function _createColumnConfigurationforData(data) {
      var columnConfigMap = {};

      let customTitles = new Map();
      customTitles.set('download_crif_report', 'Download CRIF Report');

  		for (var i = 0; i < data.length; ++i) {
  			var row = data[i];
  			for ( var property in row) {
  				if (row.hasOwnProperty(property)) {
  					var cellValue = row[property];
  					if (typeof cellValue == "undefined"
  							|| typeof cellValue == "object"
  							|| property.charAt(0) == "_" || property == "id"
  							|| property == "collection_name") {
  						continue;
  					}
  					var gridDataType = _getGridDataType(cellValue);
  					var columnConfig = {};
  					columnConfig['id'] = property;
  					columnConfig['field'] = property;
  					columnConfig['type'] = gridDataType;
  					columnConfig['filter'] = "true";
  					columnConfig['headerCssClass'] = "b";
  					if (gridDataType == "number" || gridDataType == "float") {
  						columnConfig['sortable'] = "true";
            }
            let title;
            if (customTitles.has(property)) {
              title = customTitles.get(property);
            }
            else {
              title = _getTitleCase(property);
            }
  					columnConfig['name'] = title
  					columnConfigMap[title] = columnConfig;
  				}
  			}
  		}

  		var columns = [];
  		for ( var property in columnConfigMap) {
  			if (columnConfigMap.hasOwnProperty(property)) {
  				columns.push(columnConfigMap[property]);
  			}
  		}

  		return columns;
  	}
    /***************************************************************************
     * This function is intended to determine data type supported by grid for a
     * given valu based on js parsing rules
     **************************************************************************/
    function _getGridDataType(value) {
      var floatVal = parseFloat(value).toFixed(2);
      var intVal = parseInt(value);

      // Value starts with a non numeric char
      if (!_isNumericValue(value) || isNaN(floatVal) || isNaN(intVal)) {
        return "text";
      }

      // Hacky Stuff to force float notation for only small numbers.
      if (Math.abs(floatVal) > Math.abs(intVal) && Math.abs(floatVal) < 1000) {
        return "float";
      } else {
        return "number";
      }
    }
    /***************************************************************************
     * Determines if input is numeric
     **************************************************************************/
    function _isNumericValue(value) {
      var pattern = /^-?\d+.?\d*$/;
      if (typeof value != 'undefined' && pattern.test(value)) {
        return true;
      }
      return false;
    }
    /***************************************************************************
     * Converts string to Title case
     **************************************************************************/
    function _getTitleCase(strValue) {
      // Example book_id to Book Id
      var title = strValue.replace(/_/g, ' ');
      return title.replace(/\w\S*/g, function(val) {
        return val.charAt(0).toUpperCase() + val.substr(1).toLowerCase();
      });
    }
    /***************************************************************************
  	 * Assembles columns based on priority defined as below ['bucket_type',
  	 * 'bucket_value', 'rank', 'lmv_usd', 'smv_usd', 'gmv_usd', 'nmv_usd']
  	 **************************************************************************/
  	function _getPriorityBasedColumnList(columnList, priorityList) {
  		if (columnList === undefined || columnList.length == 0) {
  			return [];
  		}

  		var colFieldToColumn = {};
  		for (var i = 0; i < columnList.length; ++i) {
  			var column = columnList[i];
  			var field = column['field'];
  			colFieldToColumn[field] = column;
  		}

  		var newColumnList = [];
  		for (var i = 0; i < priorityList.length; ++i) {
  			var field = priorityList[i];
  			var column = colFieldToColumn[field];
  			if (column !== undefined) {
  				newColumnList.push(column);
  				delete colFieldToColumn[field];
  			}
  		}

  		for ( var field in colFieldToColumn) {
  			if (colFieldToColumn.hasOwnProperty(field)) {
  				newColumnList.push(colFieldToColumn[field]);
  			}
  		}

  		return newColumnList;
  	}
  })();