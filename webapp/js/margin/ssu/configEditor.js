"use strict";
var singleSelectFilter;
var selectedCalcId = 0;
var selectedMethodologyId = 0;
var selectedConfigTypeId = 0;
var selectedConfigId = "";
var selectedConfigName = "";
var editedConfigData = {};
var deletedConfigData = {};
var filterTypesList = ['country', 'currency', 'fo_type', 'gbo_type', 'market'];
var value_type_map = {'gbo_type' : 'gbo_type',
	                    'type' : 'fo_type',
	                    'country' : 'country',
                      'countries' : 'country',
                      'category' : 'gbo_type',
                      'categories' : 'gbo_type',
                      'broker' : 'market',
                      'market' : 'market',
                      'exchange' : 'market',
                      'currency' : 'currency',
                      'currencies' : 'currency'
                      };                   

(function() {
    window.treasury = window.treasury || {};
    window.treasury.margin = window.treasury.margin || {};
    window.treasury.margin.ssu = window.treasury.margin.ssu || {};

    window.treasury.margin.ssu.configEditor = {
        loadConfigEditor: _loadConfigEditor,
        renderDefaultView: _renderDefaultView,
        editConfig: _editConfig,
        acceptEdit: _acceptEdit,
        undoEdit: _undoEdit,
        deleteRow : _deleteRow,
        undoDelete : _undoDelete,
        renderAddRecordView: _renderAddRecordView,
        acceptAdd: _acceptAdd,
        undoAdd: _undoAdd,
        updateAndSaveXML: _updateAndSaveXML,
        resetConfig: _resetConfig,
        showSaveConfirmationDialogBox: _showSaveConfirmationDialogBox,
        showCloneConfirmationDialogBox: _showCloneConfirmationDialogBox,
        renderSelectedConfig: _renderSelectedConfig,
        showConfig : _showConfig,
        renderClonedConfig : _renderClonedConfig,
    };

    /****************************************************************************
     * This function loads config into configData for the selected calculator
     * calcId and renders it on the screen.
     ****************************************************************************/
    function _loadConfigEditor(configName) {
        $('#calcNamePanel').html('');
        _renderSelectedConfig(configData, false);
        _hideEditForUnauthorizedUser();
        document.getElementById('configEditorMainContainer').hidden = false;
        document.getElementById("editorMessage").setAttribute("hidden", true);
        document.getElementById("noCalcMessage").setAttribute("hidden", true);
        document.getElementById('calcNamePanel').hidden = false;
        if(configName)
        $("#calcNamePanel").text(configName + " Configuration");
    }

    /****************************************************************************
     * This function reads selected config from configData and creates HTML DOM
     * structure to render config in the form of tables on screen.
     ****************************************************************************/
    function _renderSelectedConfig(configData, isViewOnly) {
        $('#editorSectionContainer').html('');
        var configEditorHtml = "";
        for (var tableKey in configData) {
            if (configData.hasOwnProperty(tableKey)) {
                var tableId = tableKey.replace(/ /g, "_"); //replacing spaces with underscores
                var tableHtml = "<div class='padding--vertical--double' id='" + (tableId + '-container') + "'>"
                tableHtml += "<div><strong>" + tableKey.toUpperCase() + "</strong></div>";
                tableHtml += "<table class='table config-editor-table margin--bottom--large' id='" + tableId + "'>";
                var noOfRows = configData[tableKey].length;
                for (var rowId in configData[tableKey]) {
                    tableHtml += _createRowHtml(tableKey, configData[tableKey][rowId], isViewOnly);
                }
                if (!isViewOnly) {
                    tableHtml += _createAddRecordHtml(tableId);
                }
                tableHtml += "</table></div>";
                configEditorHtml += tableHtml;
            }
        }
        document.getElementById("editorSectionContainer").innerHTML = configEditorHtml;
        $('.single-col-add').hide();
        treasury.margin.ssu.util.hideListElements(document.querySelectorAll('.config-editor-table-input'));
    }

    /********************************************************************************
     *This method hides all action buttons if the user is unautorized to edit config.
     ********************************************************************************/
    function _hideEditForUnauthorizedUser() {
        var editAuth = treasury.margin.ssu.util.isUserAuthorizedToEditConfig();
        $.when(editAuth).done(function(editAuthData) {
            if (!editAuthData) {
                $('.auth-edit').hide();
                $('.auth-delete').hide();
                $('.config-editor-table').removeClass('margin--bottom--large');
                console.log('userIsNotAuthorized');
            }
        });
    }

    /****************************************************************************
     * This function loads input element based on whether leaf level value is a
     * string, number or reference data and makes CSS changes to render Edit View
     * on screen.
     ****************************************************************************/
    function _editConfig(rowId) {
        var tableKey = _getTableKeyfromRowId(rowId);
        var tableId = tableKey.replace(/ /g, "_");

        var oldRowData = configData[tableKey][rowId];
        var valueIdx = _getNoOfColumns(rowId) - 1;
        var oldValue = oldRowData[valueIdx];

        var type = _getValueType(tableId)
        if ($.inArray(type, filterTypesList) !== -1) {
            treasury.margin.ssu.util.loadSingleSelectFilter(type, "input-" + rowId);
        }

        //rendering edit view
        var row = document.getElementById(rowId);
        var delimByCommas = _isCommaDelimitedValue(oldValue, oldRowData)
        row.querySelector('del').innerText =
            treasury.margin.ssu.util.formatText(oldValue, delimByCommas);
        row.querySelector('.config-editor-table-label').hidden = true;
        row.querySelector('.config-editor-table-input').hidden = false;
        row.querySelector('.config-editor-table-action').hidden = true;
        row.querySelector('.config-editor-table-change').hidden = false;

        document.getElementById('saveConfigBtn').setAttribute('disabled', true);
        document.getElementById('resetConfigBtn').setAttribute('disabled', true);
        document.getElementById('cloneConfigBtn').setAttribute('disabled', true);
        var editConfigCommentElement = document.getElementById('comment');
        if (treasury.margin.util.isDefinedAndValid(editConfigCommentElement)) {
                editConfigCommentElement.setAttribute('disabled', true);
        }
        //hide edit buttons and show accept/reject buttons
        treasury.margin.ssu.util.disableListElements(document.querySelectorAll('.add-record-btn .button--tertiary'));
        treasury.margin.ssu.util.disableListElements(document.querySelectorAll('.config-editor-table-action .button--tertiary'));
    }

    /*****************************************************************************
     * This function creates a new edited row object from old row(rowId)
     * and adds it to editedConfigData.
     *****************************************************************************/
    function _acceptEdit(rowId, newAddedValue) {
        var tableKey = _getTableKeyfromRowId(rowId);
        var tableId = tableKey.replace(/ /g, "_");
        var row = document.getElementById(rowId);
        var textLabel = row.querySelector('.config-editor-table-label');
        var oldRowData = configData[tableKey][rowId];

        var dummyRow = JSON.parse(JSON.stringify(configData[tableKey][rowId]));
        if (!_setAndValidateInput(dummyRow, tableKey, 'input-' + rowId)) return;
        var valueIdx = _getNoOfColumns(rowId) - 1;
        var oldValue = oldRowData[valueIdx];
        var newValue = newAddedValue ? newAddedValue : dummyRow[valueIdx];
        if (oldValue == newValue) {
					  treasury.margin.ssu.util.raiseInfoToast('Updated value is same as original. Please update to a different value or cancel');
            return;
        }

        var delimByCommas = _isCommaDelimitedValue(oldValue, oldRowData);
        textLabel.innerText = treasury.margin.ssu.util.formatText(newValue, delimByCommas);
        var newRowData = {
            'tableKey': tableKey,
            'rowId': rowId,
            'newValue': newValue,
            'oldValue': oldValue,
            'newRow': dummyRow,
            'oldRow': configData[tableKey][rowId]
        };
        editedConfigData[rowId] = newRowData;

        //Update DOM structure to highlight edited row.
        row.classList.add('yellow2');
        textLabel.classList.add('success', 'margin--left');
        if (row.querySelector('.icon-undo') == null) {
            var undoNode = document.createElement('button');
            undoNode.classList.add('tertiary', 'button--tertiary', 'icon-undo');
            undoNode.onclick = function() {
                treasury.margin.ssu.configEditor.undoEdit(rowId)
            };
            row.querySelector('.config-editor-table-action').appendChild(undoNode);
        }
        treasury.margin.ssu.configEditor.renderDefaultView(rowId);
        var delimByCommas = _isCommaDelimitedValue(oldValue,oldRowData);
        row.querySelector('del').innerText =
              treasury.margin.ssu.util.formatText(oldValue, delimByCommas);
    }

    /*******************************************************************************
     * This function reads input from input filters and elements, sets it in rowData
     * and validates the input.
     *******************************************************************************/
    function _setAndValidateInput(rowData, tableKey, inputElementId) {
        var tableId = tableKey.replace(/ /g, "_");
        var type = _getValueType(tableId);
        var rowId = rowData[rowData.length - 1];
        var valueIdx = _getNoOfColumns(rowId) - 1;
        if ($.inArray(type, filterTypesList) !== -1) {
            var id = singleSelectFilter.getSelectedId();
            if (id == -1) {
                treasury.margin.ssu.util.raiseErrorToast('Please select a valid entry in filter');
                return;
            }
            rowData[valueIdx] = treasury.margin.ssu.util.getSingleSelectValue(id, singleSelectFilter);
            rowData[valueIdx + 2] = id;
        } else {
            rowData[valueIdx] = document.getElementById(inputElementId).value;
            if (rowData[valueIdx + 2]) {
                rowData[valueIdx + 2] = rowData[valueIdx];
            }
        }
        if (!_validateInput(tableKey, rowData[valueIdx])) {
            console.log('Invalid input. Returning..')
            return false;
        }
        return true;
    }

    /*****************************************************************************
     * This function removes rowId from editedConfigData and makes CSS changes to
     * simulate Undo operation on row rowId.
     *****************************************************************************/
    function _undoEdit(rowId) {
        var row = document.getElementById(rowId);
        var oldValue = editedConfigData[rowId]['oldValue'];
        var tableId = row.closest('table').id;
        var tableKey = tableId.replace(/_/g, " ");
        row.classList.remove('yellow2');
        row.querySelector('span').classList.remove('success', 'margin--left');
        var delimByCommas = _isCommaDelimitedValue(oldValue,configData[tableKey][rowId])
        row.querySelector('span').innerText =
            treasury.margin.ssu.util.formatText(oldValue, delimByCommas);
        treasury.margin.ssu.configEditor.renderDefaultView(rowId);
        if (row.querySelector('.icon-undo') != null) {
            row.querySelector('.icon-undo').remove();
        }
        delete editedConfigData[rowId];
        _updateSaveResetButtonStatus();
    }

    /*****************************************************************************
     * This function makes CSS changes to render view to add new record to the
     * table tableId.
     *****************************************************************************/
    function _renderAddRecordView(tableId) {
        $('#' + tableId + ' .single-col-add').show();
        var type = _getValueType(tableId);
        if ($.inArray(type, filterTypesList) !== -1) {
            var valueId = "input-" + tableId;
            treasury.margin.ssu.util.loadSingleSelectFilter(type, valueId);
        }
        document.getElementById('saveConfigBtn').setAttribute('disabled', true);
        document.getElementById('resetConfigBtn').setAttribute('disabled', true);
        document.getElementById('cloneConfigBtn').setAttribute('disabled', true);
        var editConfigCommentElement = document.getElementById('comment');
        if (treasury.margin.util.isDefinedAndValid(editConfigCommentElement)) {
                editConfigCommentElement.setAttribute('disabled', true);
        }
        document.getElementById(tableId).querySelector('.add-record-filters').hidden = false;
        treasury.margin.ssu.util.disableListElements(document.querySelectorAll('.add-record-btn .button--tertiary'));
        treasury.margin.ssu.util.disableListElements(document.querySelectorAll('.config-editor-table-action .button--tertiary'));
    }

    /********************************************************************************
     * This function deletes the row with id rowId and makes appropriate CSS changes.
     ********************************************************************************/
    function _deleteRow(rowId){
      var tableKey = _getTableKeyfromRowId(rowId);
      var tableId = tableKey.replace(/ /g, "_");
      var row = document.getElementById(rowId);
      var oldRow = configData[tableKey][rowId];

      if(oldRow == undefined){
        _undoAdd(rowId);
        return;
      }

      var deletedRowData = {
        'tableKey': tableKey,
        'rowId': rowId,
        'oldRow': oldRow,
        'oldValue' : oldRow[0]
      }

      deletedConfigData[rowId] = deletedRowData;
      delete configData[tableKey][rowId];

      //highlight deleted row and show undo button
      row.querySelector('del').innerText=oldRow[0];
      row.querySelector('span').innerText='';

      row.querySelector('.config-editor-table-action').hidden = true;
      row.querySelector('.config-editor-table-change').hidden = false;

      _updateSaveResetButtonStatus();
    }

    /*****************************************************************************
     * This function undoes the delete opertion on row with id rowId.
     *****************************************************************************/
    function _undoDelete(rowId){
      var tableKey = _getTableKeyfromRowId(rowId);
      var tableId = tableKey.replace(/ /g, "_");
      var row = document.getElementById(rowId);

      configData[tableKey][rowId] = deletedConfigData[rowId]['oldRow'];
      delete deletedConfigData[rowId];

      _updateSaveResetButtonStatus();

      row.querySelector('span').innerText=configData[tableKey][rowId][0];
      row.querySelector('del').innerText='';
      row.querySelector('.config-editor-table-action').hidden = false;
      row.querySelector('.config-editor-table-change').hidden = true;
    }

    /*****************************************************************************
     * This function reads data from input elements, validates the data and if
     * validation is succesful, it creates a new row with id with newRowId and data
     * of the added record and adds it  to editedConfigData. It makes CSS Changes
     * to render the newly added record to the table on screen.
     *****************************************************************************/
    function _acceptAdd(tableId) {
        var table = document.getElementById(tableId);
        var tableKey = tableId.replace(/_/g, " ");
        var dummyRow = _getDummyRow(tableId);
        var noOfRows = Object.keys(configData[tableKey]).length;
        var noOfColumns = noOfRows>0 ? _getNoOfColumns(dummyRow[dummyRow.length-1]) : 1;

        if (!_setAndValidateInput(dummyRow, tableKey, "input-" + tableId)) return;
        var valueIdx = noOfColumns-1;
        var existingRowId = _getExistingRowId(tableKey, dummyRow);

        if (existingRowId == -1) {
            var newRowId = _generateNewRowId();
            dummyRow[dummyRow.length - 1] = newRowId;
            editedConfigData[newRowId] = {
                'oldRow': null,
                'newRow': dummyRow,
                'rowId': newRowId,
                'tableKey': tableKey,
                'newValue': dummyRow[valueIdx]
            }
            table.querySelector('.add-record-filters').hidden = true;
            var html = _createRowHtml(tableKey, dummyRow);
            $(html).insertBefore(table.querySelector('.add-record-btn'));
            $('#' + newRowId + ' span').addClass('success');
        } else if (configData[tableKey][existingRowId]) {
            treasury.margin.ssu.util.raiseErrorToast('This record already exists in the config')
        } else if (editedConfigData[existingRowId]) {
						treasury.margin.ssu.util.raiseErrorToast('Cannot add the same record twice')
        } else if (deletedConfigData[existingRowId]) {
          _undoDelete(existingRowId);
					treasury.margin.ssu.util.raiseInfoToast('Undid the delete operation on this row')
        }
        treasury.margin.ssu.configEditor.renderDefaultView(tableId);
        _updateFirstColStatus(tableId);
    }

    /*****************************************************************************
     * This function returns a deep copy of first row of table tableId, if the
     * table has rows. Otherwise, it creates a dummy row object based on value type.
     *****************************************************************************/
    function _getDummyRow(tableId){
      var tableKey = tableId.replace(/_/g, " ");

      if(Object.keys(configData[tableKey]).length > 0){
        var firstRowId = Object.keys(configData[tableKey])[0]
        return JSON.parse(JSON.stringify(configData[tableKey][firstRowId]));
      }
      else{
        var type = _getValueType(tableId);
        return ["dummyName", null, "-1", type, "dummyRowId"]
      }
    }

    /*********************************************************************************
     * This method deletes the added row rowId from editedConfigData and also removes
     * it from HTML dom.
     ********************************************************************************/
    function _undoAdd(rowId) {
        var row = document.getElementById(rowId);
        var tableId = row.closest('table').id;
        row.remove();
        delete(editedConfigData[rowId]);
        _updateSaveResetButtonStatus();
        _updateFirstColStatus(tableId);
    }

    /*****************************************************************************************
     * This method updates unsaved config changes to the XML and pushes the config to database.
     * If successful, it reloads all calculators and configs, otherwise displays error message.
     *****************************************************************************************/
    function _updateAndSaveXML(flag) {
        var updateSuccess = true;
        updateSuccess = treasury.margin.ssu.Actions.updateXML(selectedConfigName, flag);
        treasury.margin.common.showLoading("mainLoader");
        var obj = {
            'author': document.querySelector('arc-header').getAttribute('user'),
            'comment': document.getElementById('comment').value
        }
        if (updateSuccess) {
            treasury.margin.ssu.Actions.postCalculatorConfigDataToDB(obj, flag);
        } else {
				treasury.margin.ssu.util.raiseErrorToast('Error Occured while saving config to database');
        }
    }
    
    function _showConfig(){
        treasury.margin.common.showLoading();
        var methodology = document.getElementById(selectedMethodologyId);
        methodology.querySelector('.calcname-btn').click(); //This refreshes the state of the UI to clean

        //expand the selected methodology
        var agreementId = methodology.closest('.agreement').id;
        treasury.margin.ssu.calcNav.showChildren(agreementId);
        if (methodology.classList.contains('childMethodology')) {
            var parentMethodologyId = methodology.closest('.parent .methodology').id;
            treasury.margin.ssu.calcNav.showChildren(parentMethodologyId);
        }
        console.log('Posted config to DB');

        let calculatorId = treasury.margin.ssu.calcNav.getCalcIdFromAgreement(agreementId,
            selectedMethodologyId, methodology.classList.contains('childMethodology'));
        let configList = treasury.margin.ssu.calcNav.getConfigList(calculatorId);
        let configType, configTypeName;
        for(let i=0; i< configList.productionConfig.length;i++){
            if( configList.productionConfig[i].configName == selectedConfigName){
                configType = "prod";
                configTypeName = "productionConfig";
                break;
            }
        }
        if(configType == undefined){
              for(let i=0; i< configList.simulationConfig.length;i++){
                if( configList.simulationConfig[i].configName == selectedConfigName){
                    configType = "sim";
                    configTypeName = "simulationConfig";
                    break;
                }
            }  
        }

        if(configType != undefined && configTypeName != undefined){
            let configTypeNode = document.getElementById(configType + '-' + selectedMethodologyId);
            let configListButton = configTypeNode.querySelector('button');
            treasury.margin.ssu.calcNav.toggleConfigList(configListButton, configList, configTypeName);  
            let configNode = document.getElementById(selectedConfigName + '-' + configType + '-config');
            let configButton = configNode.querySelector('button');
            treasury.margin.ssu.calcNav.loadConfigAndToggleAttrs(configButton, selectedConfigName);
        }
        
        treasury.margin.common.hideLoading();
    }

    /*********************************************************************************
     * This method displays a dialog box on screen asking user to confirm saving config
     * changes. If user confirms, it saves config to DB and reloads all calculators and config
     * editor. Otherwise, it lets user continue editing.
     ********************************************************************************/
    function _showSaveConfirmationDialogBox() {
        var arcDialog = document.getElementById("saveConfigPopup");
        arcDialog.reveal({
            'title' : 'Save Config',
            'modal' : true,
            'sticky': true,
            'moveable': true,
            'buttons': [
                {
                    html: "Save Changes",
                    callback: function() {
                        if (document.getElementById('comment').value == "") {
                            document.getElementById('commentError').removeAttribute('hidden');
                        } else {
                            document.getElementById('commentError').setAttribute('hidden', true);
                            arcDialog.visible = false;
                            _updateAndSaveXML(SAVE_CLONED_EDITED_CONFIG_ON_CONFIG_UI);
                        }
                    }
                },
                {
                    html: "Continue Editing",
                    callback: function() {
                        arcDialog.visible = false;
                    }
                }
            ]
        });
    }
    
    /*********************************************************************************
     * This method displays a dialog box on screen asking user to enter new Config Name and Suitable comment
     * The name and the comment cannot be empty. Also name should be unique. Once the user clicks on Clone, it 
     * clones the config and reloads all of them displaying the Config that was cloned.
     ********************************************************************************/
    function _showCloneConfirmationDialogBox() {
        document.getElementById("cloneConfigPopup").reveal({
        'title' : 'Clone Config',
        'modal' : true,
        'sticky': true,
        'moveable': true,
        'buttons' : [ {
            'html' : 'Clone',
            'class' : 'clone-config',
            'callback' : function() {
                             var configInputCheck;
                             configInputCheck = treasury.margin.ssu.Actions.isConfigInputEmpty();
                             if(configInputCheck['isEmpty'])
                             {
                                 treasury.margin.ssu.util.raiseInfoToastWithTimeOut(configInputCheck['message'], 2000);
                             }
                             else
                             {
                                 let configName = document.getElementById('clone-config-name').value;
                                 if(treasury.margin.ssu.Actions.validateConfigNameAlreadyExists())
                                 {
                                 	treasury.margin.ssu.util.raiseInfoToastWithTimeOut(
                                 		'Config Name already exists', 2000);
                                 }
                                 else if(treasury.margin.ssu.Actions.validateConfigNameContainsSpecialCharacters())
                                 {
                                 	treasury.margin.ssu.util.raiseInfoToastWithTimeOut(
                                 	    'Config Name should not contain special characters', 2000);
                                 }
                                 else if(treasury.margin.common.validateNameIsUndefined(configName)){
                                    let message = 'Config Name can not be ' + configName;
                                    treasury.margin.ssu.util.raiseInfoToastWithTimeOut(
                                        message, 2000);
                                 }
                                 else
                                 {
                                     this.shadowRoot.querySelector('.clone-config').disabled = true;
                                     _cloneConfig();
                                 }
                             }
                     },
            'position' : 'center'
             }, {
             'html' : 'Cancel',
             'callback' : function() {
                       _clearCloneConfigurationPopup();
                           },
             'position' : 'center'
             } ]
           });
    }
    
    /*********************************************************************************
     * This method clones our Configuration. once the Configuration is cloned, it displays that Configuration
     * on the screen.
     ********************************************************************************/
    
    function _cloneConfig()
    {
        treasury.margin.common.showLoading("mainLoader");
        var obj = {
            'isCloneConfig' : true,   
            'author': document.querySelector('arc-header').getAttribute('user'),
            'comment': document.getElementById('clone-config-comment').value,
            'configName': document.getElementById('clone-config-name').value
        }
        document.getElementById("cloneConfigPopup").visible = false;
        treasury.margin.ssu.Actions.postClonedConfigToDB(obj, SAVE_CLONED_CONFIG_ON_CONFIG_UI);
    }

    function _clearCloneConfigurationPopup()
    {
    	document.getElementById('clone-config-comment').value = '';
        document.getElementById('clone-config-name').value = '';
        document.getElementById("cloneConfigPopup").visible = false;
    }

    function _renderClonedConfig(){

        treasury.margin.common.showLoading();
        selectedConfigName = document.getElementById('clone-config-name').value;
                
        var methodology = document.getElementById(selectedMethodologyId);
        methodology.querySelector('.calcname-btn').click(); //This refreshes the state of the UI to clean
        
        var agreementId = methodology.closest('.agreement').id;
        treasury.margin.ssu.calcNav.showChildren(agreementId);
        
        var calculatorId = treasury.margin.ssu.calcNav.getCalcIdFromAgreement(agreementId,
            selectedMethodologyId, methodology.classList.contains('childMethodology'));
        var configList = treasury.margin.ssu.calcNav.getConfigList(calculatorId);
  
        var configTypeNode = document.getElementById('sim-' + selectedMethodologyId);
        var configListButton = configTypeNode.querySelector('button');
       
        if (methodology.classList.contains('childMethodology')) {
            var parentMethodologyId = methodology.closest('.parent .methodology').id;
            treasury.margin.ssu.calcNav.showChildren(parentMethodologyId);
        }
        treasury.margin.ssu.calcNav.toggleConfigList(configListButton, configList, 'simulationConfig');
        var newConfigName = document.getElementById('clone-config-name').value;
        var configNode = document.getElementById(newConfigName + '-sim-config');
        var configButton = configNode.querySelector('button');
        treasury.margin.ssu.calcNav.loadConfigAndToggleAttrs(configButton, newConfigName);
        
        console.log('Posted config to DB');
        _clearCloneConfigurationPopup();
        treasury.margin.common.hideLoading();
    }
    
    /*********************************************************************************
     * This method performs undo on all edit/add operations done by user and resets
     * config to its original state.
     ********************************************************************************/
    function _resetConfig() {
        for (var rowId in editedConfigData) {
            if (editedConfigData[rowId]['oldRow'] == null) {
                treasury.margin.ssu.configEditor.undoAdd(rowId);
            } else {
                treasury.margin.ssu.configEditor.undoEdit(rowId);
            }
        }
        for (var rowId in deletedConfigData) {
            treasury.margin.ssu.configEditor.undoDelete(rowId);
        }
				$("#saveConfigError").remove();
				treasury.margin.ssu.util.updateFilterErrorMessage();
    }

    /*************************************************************************************
     * This method renders the default view of config editor. elementId can be a  rowId
     * or tableId.
     ************************************************************************************/
    function _renderDefaultView(elementId) {
        var element = document.getElementById(elementId);

        //element is a row
        if($('#'+elementId).is('tr')){
          element.querySelector('del').innerText = "";
          element.querySelector('.config-editor-table-label').hidden = false;
        }

        var actionBtns = element.querySelector('.config-editor-table-action');
        var changeBtns = element.querySelector('.config-editor-table-change');
        if (actionBtns != null) actionBtns.hidden = false;
        if (changeBtns != null) changeBtns.hidden = true;

        treasury.margin.ssu.util.hideListElements(document.querySelectorAll('.add-record-filters'));
        treasury.margin.ssu.util.hideListElements(document.querySelectorAll('.config-editor-table-input'));
        treasury.margin.ssu.util.enableListElements(document.querySelectorAll('.add-record-btn .button--tertiary'));
        treasury.margin.ssu.util.enableListElements(document.querySelectorAll('.config-editor-table-action .button--tertiary'));
        _updateFirstColStatus(element.closest('table').id);
        _updateSaveResetButtonStatus();
    }

    function _updateFirstColStatus(tableId){
      if ($('#' + tableId + ' .add-record-action').length == 0)
        $('#' + tableId + ' .single-col-add').hide();
      else
        $('#' + tableId + ' .single-col-add').show();
    }

    /**************************************************************************************
     * This method enables Save/Reset buttons if there are any changes, disables otherwise.
     **************************************************************************************/
    function _updateSaveResetButtonStatus() {
        var noOfEditedRows = Object.keys(editedConfigData).length;
        var noOfDeletedRows = Object.keys(deletedConfigData).length;
        var editConfigCommentElement = document.getElementById('comment');
        if (noOfEditedRows > 0 || noOfDeletedRows>0) {
            document.getElementById('saveConfigBtn').removeAttribute('disabled');
            document.getElementById('resetConfigBtn').removeAttribute('disabled');
            document.getElementById('cloneConfigBtn').setAttribute('disabled', true);
            if (treasury.margin.util.isDefinedAndValid(editConfigCommentElement)) {
                    editConfigCommentElement.removeAttribute('disabled');
            }
        } else {
            document.getElementById('saveConfigBtn').setAttribute('disabled', true);
            document.getElementById('resetConfigBtn').setAttribute('disabled', true);
            document.getElementById('cloneConfigBtn').removeAttribute('disabled');
            if (treasury.margin.util.isDefinedAndValid(editConfigCommentElement)) {
                    editConfigCommentElement.setAttribute('disabled', true);
            }
        }
    }

    /*********************************************************************************
     * This function creates HTML DOM structure for row rowData.
     ********************************************************************************/
    function _createRowHtml(tableKey, rowData, isViewOnly) {
        var html = "";
        var length = rowData.length;
        var id = rowData[length - 1];
        var type = rowData[length - 2];
        var maxCol = _getMaxNoOfColumns(tableKey);

        for (var col = 0; col < length; col++) {
            if (rowData[col] == null || rowData[col] == 'null') break;
        }
        html += "<tr id ='" + id + "'>"; //last element is unique id

        var type=(col==1)?'add':'edit';

        for (var i = 0; i < col; i++) {
            var delimByCommas = (col>1 && i==col-1) ? _isCommaDelimitedValue(rowData[i],rowData) : false;
            if (rowData[i + 1] == null || rowData[i + 1] == 'null') {
                if (!isViewOnly) {
                    var input;
                    if ($.inArray(type, filterTypesList) !== -1) {
                        input = "<arc-combobox class='config-editor-table-input' id = 'input-" + id + "'></arc-combobox>";
                    } else {
                        var inputType = _getInputType(rowData);
                        var str = (inputType == 'string') ? 'text' : 'number';
                        input = "<input type='" + str + "' class='config-editor-table-input' id = 'input-" + id + "' />";
                    }
                    var del = "<del class='critical'></del>"
                }
                var label = "<span class='config-editor-table-label'>" +
                            treasury.margin.ssu.util.formatText(rowData[i], delimByCommas) + "</span>";
                html += "<td class='padding--horizontal--double'>";
                if (del !=undefined && del!= null) {
                    html += del;
                }
                html += label;
                if (input !=undefined && input!= null) {
                    html += input;
                }
                html += "</td>";
            } else {
                html += "<td class='padding--horizontal--double'>" + treasury.margin.ssu.util.formatText(rowData[i],delimByCommas) + "</td>";
            }
        }

        for (var i = 0; i < maxCol - col; i++) {
            html += "<td></td>";
        }
        if (!isViewOnly) {
            html += _buttonHTML(id, type);
		}
        html += "</tr>";
        return html;
    }

    function _isCommaDelimitedValue(value, rowData) {
        if(!value || !rowData) return false;
        for (var col = 0; col < rowData.length; col++) {
            if (rowData[col] == null || rowData[col] == 'null') break;
        }

        //Non-Key values most likely to be IDs
        if (isNaN(value)) return false;

        //Following patterns are most likely to be IDs
        var idPatterns = ['spn', 'custodian', 'index', 'indices', 'exec_dt', ' id']

        for (var i = 0; i < col - 1; i++) {
            var key = rowData[i].toLowerCase();
            for (var j = 0; j < idPatterns.length; j++) {
                if (key.search(idPatterns[j])>-1) return false;
            }
        }
        return true;
    }

    /*********************************************************************************
     * This function returns HTML required to render view for adding a new record.
     * It creates a new row with single select filers and text and number input
     * boxes based on the type of content of the table tableId.
     ********************************************************************************/
    function _createAddRecordHtml(tableId) {
        var tableKey = tableId.replace(/_/g, " ");
        var noOfColumns = 1;
        var firstRowId = Object.keys(configData[tableKey])[0]

        if(firstRowId){
          var firstRow = configData[tableKey][firstRowId];
          for (var i = 0; i < firstRow.length; i++) {
            if (firstRow[i] == null || firstRow[i] == 'null') break;
          }
          var noOfColumns = i;
        }

        if (noOfColumns > 1) return "";

        var html = "<tr class = 'add-record-btn border--bottom auth-edit' id ='add-" + tableId + "'>";
        //Button to add record
        html += "<td class='padding--horizontal-large text-align--center gray3 auth-edit' colspan='" + noOfColumns + 1 + "' class='text-align--center'>"
        html += "<button class='button--tertiary' onclick=\"treasury.margin.ssu.configEditor.renderAddRecordView('" + tableId + "')\">Add Record</button>";
        html += "</td></tr>";

        //Add record value : based on whether the value is string/number or reference data
        html += "<tr class='add-record-filters auth-edit' hidden>";

        var type = _getValueType(tableId);
        var id = "input-" + tableId;
        if ($.inArray(type, filterTypesList) !== -1) {
            html += "<td><arc-combobox class='add-record-value' id = '" + id + "' ></arc-combobox></td>";
        } else {
            var firstRowId = Object.keys(configData[tableKey])[0];
            var inputType = _getInputType(configData[tableKey][firstRowId]);
            var str = (inputType == 'string') ? 'text' : 'number';
            html += "<td><input type='" + str + "' class='add-record-value' id = '" + id + "' /></td>"
        }

				//Accept/Reject buttons for adding record
        html +=
            "<td><div class='add-record-change'>\
      			<button class='button--tertiary icon-accept padding' onclick=\"treasury.margin.ssu.configEditor.acceptAdd('" + tableId + "')\"></button>\
      			<button class='button--tertiary icon-reject padding' onclick=\"treasury.margin.ssu.configEditor.renderDefaultView('" + tableId + "')\"></button>\
      		  </div></td>";

        html += "</tr>";
        return html;
    }

    /*********************************************************************************
     * This function returns HTML required to render action buttons like Edit, Undo,
     * Accept and Reject.
     ********************************************************************************/
    function _buttonHTML(id, type) {
      if(type=='add'){
        var buttonHTML =
            "<td class='auth-delete'><div class='config-editor-table-change' hidden>\
             <button class='tertiary button--tertiary icon-undo padding' onclick=\"treasury.margin.ssu.configEditor.undoDelete('" + id + "')\"></button>\
             </div>\
             <div class='config-editor-table-action'>\
               <button class='tertiary button--tertiary icon-delete padding' onclick=\"treasury.margin.ssu.configEditor.deleteRow('" + id + "')\"></button>\
             </div></td>";
      }
      else if(type=='edit'){
        var buttonHTML =
            "<td class='auth-edit'><div class='config-editor-table-change' hidden>\
             <button class='tertiary button--tertiary icon-accept padding' onclick=\"treasury.margin.ssu.configEditor.acceptEdit('" + id + "')\"></button>\
             <button class='tertiary button--tertiary icon-reject padding' onclick=\"treasury.margin.ssu.configEditor.renderDefaultView('" + id + "')\"></button>\
             </div>\
             <div class='config-editor-table-action'>\
               <button class='tertiary button--tertiary icon-edit padding' onclick=\"treasury.margin.ssu.configEditor.editConfig('" + id + "')\"></button>\
             </div></td>";
        }
        return buttonHTML;
    }

    /*********************************************************************************
     * This method returns the type of data in the leaf node(value). e.g., 'country',
     * 'currency', 'fo_type', etc. Second last element of row stores value type.
     ********************************************************************************/
    function _getValueType(tableId) {
        var tableKey = tableId.replace(/_/g, " ");
        var rows = Object.keys(configData[tableKey]);
        if(rows.length > 0){
          var row = configData[tableKey][rows[0]];
          if(row[1]=='methodology'){
              row = configData[tableKey][rows[1]];
          }
          return row[row.length - 2];
        }
        else{
          var patterns = Object.keys(value_type_map);
          for(var i=0; i<patterns.length; i++){
            if(tableKey.search(patterns[i]) > -1)
              return value_type_map[patterns[i]];
          }
        }
    }

    /*********************************************************************************
     * This method returns the type of input for the value(leaf) column of row rowData.
     * It is used to display correct input element(number or text) in the DOM.
     ********************************************************************************/
    function _getInputType(rowData) {
        if(rowData==undefined) return 'string';
        var value = rowData[rowData.length - 4];
        if (value == null || value == undefined) return 'string';
        var number = Number(value);
        if (isNaN(number)) return 'string';
        if (number % 1 === 0) return 'integer';
        if (number % 1 !== 0) return 'float';
    }

    /*********************************************************************************
     * This method validates the value entered by user.
     ********************************************************************************/
    function _validateInput(tableKey, value) {
        var firstRowId = Object.keys(configData[tableKey])[0];
        if (value == undefined || value == null || value == "") {
						treasury.margin.ssu.util.raiseErrorToast('Please enter a valid input');
            return false;
        }
        if (!/\S/.test(value)) {
					treasury.margin.ssu.util.raiseErrorToast('Empty value is not allowed');
            return false;
        }
        var expectedType = _getInputType(configData[tableKey][firstRowId]);
        if (expectedType == 'string') return true;

        expectedType = 'integer';
        for (var rowId in configData[tableKey]) {
            expectedType = _getInputType(configData[tableKey][rowId]);
            if (expectedType == 'float') break;
        }

        if (expectedType == 'integer' && value % 1 !== 0) {
						treasury.margin.ssu.util.raiseErrorToast('Please enter a valid integer, not decimal numbers');
            return false;
        }
        console.log('validation succesful');
        return true;
    }

    /*********************************************************************************
     * This method returns the number of data columns in the row rowId. Data
     * columns include all keys columns and the value column.
     ********************************************************************************/
    function _getNoOfColumns(rowId) {
        if(document.getElementById(rowId)==null) return 1;

        var tableId = document.getElementById(rowId).closest('table').id;
        var tableKey = tableId.replace(/_/g, " ");
        var row = configData[tableKey][rowId];
        for (var i = 0; i < row.length; i++) {
            if (row[i] == null || row[i] == 'null') return i;
        }
    }

    /*********************************************************************************
     * This method returns max number of columns across all rows in table tableId.
     ********************************************************************************/
    function _getMaxNoOfColumns(tableKey) {
        var max = 0;
        for (var rowId in configData[tableKey]) {
            var row = configData[tableKey][rowId];
            for (var i = 0; i < row.length; i++) {
                if (row[i] == null || row[i] == 'null') {
                    max = Math.max(i, max);
                    break;
                }
            }
        }
        return max;
    }

    /*********************************************************************************
     * This method returns a unique id for the new record to be added to the config.
     ********************************************************************************/
    function _generateNewRowId() {
        var username = document.querySelector('arc-header').getAttribute('user');
        var date = new Date();
        var id = username + date.valueOf(); //d.valueOf() returns no. of milliseconds elapsed since 1 Jan 1970.
        return id;
    }

    /**************************************************************************************
     * This method returns the tableId of the table to which the row with id rowId belongs.
     **************************************************************************************/
    function _getTableKeyfromRowId(rowId) {
        var tableId = document.getElementById(rowId).closest('table').id;
        return tableId.replace(/_/g, " ");
    }

    /**************************************************************************************
     * This method checks if the record newRowData already exists in either the original
     * config or has already been added by the user and not saved yet. If such a duplicate
     *  record exists, it returns rowId of the duplicate record, else returns -1.
     **************************************************************************************/
    function _getExistingRowId(tableKey, newRowData) {
        var tableData = configData[tableKey];
        //If its a list, single value is itself the key and should be unique
        if (_getNoOfColumns(newRowData[newRowData.length-1]) == 1) {
            for (var rowId in tableData) {
                if (tableData[rowId][0] == newRowData[0]) return rowId;
            }
            for (var rowId in editedConfigData) {
                var metaData = editedConfigData[rowId];
                if (metaData['tableKey'] != tableKey) continue;
                if (metaData['newRow'][0] == newRowData[0]) return rowId;
            }
            for (var rowId in deletedConfigData) {
                var metaData = deletedConfigData[rowId];
                if (metaData['tableKey'] != tableKey) continue;
                if (metaData['oldRow'][0] == newRowData[0]) return rowId;
            }
            return -1;
        }

        //checking in existing records
        for (var rowId in tableData) {
            var flag = true;
            var rowData = tableData[rowId];
            for (var i = 0; rowData[i + 1] != null && rowData[i + 1] != 'null'; i++) {
                if (rowData[i] != newRowData[i]) flag = false;
            }
            if (flag) return rowId;
        }

        //checking in newly added records
        for (var rowId in editedConfigData) {
            var flag = true;
            var metaData = editedConfigData[rowId];
            if (metaData['oldRow'] != null || metaData['tableKey'] != tableKey) continue;
            var rowData = metaData['newRow'];
            for (var i = 0; rowData[i + 1] != null && rowData[i + 1] != 'null'; i++) {
                if (rowData[i] != newRowData[i]) flag = false;
            }
            if (flag) return rowId;
        }
        return -1;
    }
})();