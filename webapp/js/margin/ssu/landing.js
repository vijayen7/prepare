"use strict";
var cpeMultiSelect;
var legalEntityMultiSelect;
var agreementTypeMultiSelect;
var searchPortfolioFilterGroup;

(function() {
    window.treasury = window.treasury || {};
    window.treasury.margin = window.treasury.margin || {};
    window.treasury.margin.ssu = window.treasury.margin.ssu || {};
    window.treasury.margin.ssu.Landing = {
        loadFilters: _loadFilters,
        resetFilters: _resetFilters,
        copySearchURL: _copySearchURL,
        loadCopyUrlData: _loadCopyUrlData
    };

    window.addEventListener("WebComponentsReady", _initialize);
    /****************************************************************************
     * This is the first method after the screen loads.
     ****************************************************************************/
    function _initialize() {
        $(document).on({
            ajaxStart: function() {
                treasury.margin.common.showLoading("mainLoader");
            },
            ajaxStop: function() {
                treasury.margin.common.hideLoading("mainLoader");
            }
        });

        var saveSettings = document.querySelector('arc-save-settings');
        var host = '';
        var clientName = document.getElementById("clientName").value;
        var stabilityLevel = document.getElementById("stabilityLevel").value;

        if (clientName === 'desco') {
            if (stabilityLevel != 'prod') {
                host += "http://arc" + stabilityLevel + '.deshaw.com';
            } else {
                host = "http://arc.deshaw.com";
            }
        }
        saveSettings.serviceURL = host + '/service/SettingsService';
        saveSettings.applicationId = 3;
        saveSettings.applicationCategory = 'MarginSSU';

        document.getElementById('summary-search-save').onclick = function() {
            saveSettings.openSaveDialog();
        };

        saveSettings.saveSettingsCallback = function() {
            var parameters = searchPortfolioFilterGroup.getSerializedParameterMap();
            return parameters;
        }
        saveSettings.applySettingsCallback = function(parameters) {
            _setFilters(parameters);
        }

        _registerHandlers();
        _loadFilters();

    }
    /****************************************************************************
     *This method registers event behavior for all elements
     ****************************************************************************/
    function _registerHandlers() {
        document.getElementById('searchId').onclick = treasury.margin.ssu.calcNav.showNavMenu;
        document.getElementById('saveConfigBtn').onclick =
            treasury.margin.ssu.configEditor.showSaveConfirmationDialogBox;
        document.getElementById('cloneConfigBtn').onclick =
            treasury.margin.ssu.configEditor.showCloneConfirmationDialogBox;
        document.getElementById('resetConfigBtn').onclick = treasury.margin.ssu.configEditor.resetConfig;
        document.getElementById('copySearchURL').onclick = treasury.margin.ssu.Landing.copySearchURL;
        document.getElementById('clearFiltersBtn').onclick = treasury.margin.ssu.Landing.resetFilters;
        document.getElementById('ssuFilterSidebar').addEventListener("stateChange",
            treasury.margin.ssu.util.resizeAllCanvas);
    }
    /****************************************************************************
     * This function loads search filters and creates a FilterGroup object
     ****************************************************************************/
    function _loadFilters() {
      $.when(treasury.loadfilter.legalEntities(),
          treasury.loadfilter.cpes(),
          treasury.loadfilter.loadAgreementTypes(),
					treasury.loadfilter.defaultDates()
        )
      .done(
          function(legalEntitiesData, cpesData,
              agreementTypesData, defaultDatesData) {

            _setupDateFilter(defaultDatesData[0].tMinusOneFilterDate);

            // Initializing Legal Entity Filter
            legalEntityMultiSelect = new window.treasury.filter.MultiSelectNew(
                "descoEntityIds", "legalEntityFilter",
                _decodeString(legalEntitiesData[0].descoEntities), []);

            // Initializing cpe filter
            cpeMultiSelect = new window.treasury.filter.MultiSelectNew(
                "cpeIds", "cpeFilter", _decodeString(cpesData[0].cpes), []);

            // Initializing agreement type filter
            agreementTypeMultiSelect = new window.treasury.filter.MultiSelectNew(
                "agreementTypeIds", "agreementTypeFilter",
                agreementTypesData[0].agreementTypes, []);

            //Getting Parameter Map
            searchPortfolioFilterGroup = new treasury.filter.FilterGroup(
									[ legalEntityMultiSelect, cpeMultiSelect,
											agreementTypeMultiSelect], null, $('#datePicker'), null);
            var saveSettings = document.querySelector('arc-save-settings');
            saveSettings.retrieveSettings();
            _loadCopyUrlData();
        })
      }

    function _setupDateFilter(dateString) {
      var dateFilterValue = new Date(treasury.defaults.date || dateString);
  		var minDate = new Date('1970-01-01');
  		var maxDate =  new Date();
  		var dateFormat = 'yy-mm-dd';
  		treasury.margin.ssu.util.setupDateFilter(
				$("#datePicker"), $("#datePickerError"),
				dateFilterValue, minDate, maxDate, dateFormat);
    }

  /****************************************************************************
   * This function decodes special characters like &amp; &agrave;
   ****************************************************************************/
   function _decodeString(entityList) {
     for(var index = 0; index < entityList.length; index++){
       var temp = jQuery.parseHTML(entityList[index][1]);
       if(temp == null) continue;
       entityList[index][1] = temp[0].data;
     }
     return entityList;
   }

    /****************************************************************************
     *This method sets values in parameters on associated singleSelectFilters
     ****************************************************************************/
     function _setFilters(parameters) {
   		var legalEntityIds = parameters["descoEntityIds"];
   		var cpeIds = parameters["cpeIds"];
   		var agreementTypeIds = parameters["agreementTypeIds"];
      var dateString = parameters["dateString"];

   		if (legalEntityIds != undefined) {
   			legalEntityIds = legalEntityIds.split(',');
   			legalEntityMultiSelect.setSelectedNodes(legalEntityIds);
   		}
   		if (cpeIds != undefined) {
   			cpeIds = cpeIds.split(',');
   			cpeMultiSelect.setSelectedNodes(cpeIds);
   		}
   		if (agreementTypeIds != undefined) {
   			agreementTypeIds = agreementTypeIds.split(',');
   			agreementTypeMultiSelect.setSelectedNodes(agreementTypeIds);
   		}
      if(dateString != undefined){
   	     _setupDateFilter(dateString);
		  }
 	}

    /****************************************************************************
     *This method generates copy search URL for the search performed by user.
     ****************************************************************************/
    function _copySearchURL() {
        treasury.margin.common.showLoading("mainLoader");
        var url = "/treasury/margin/calculator-configurations";
        var parameters = searchPortfolioFilterGroup.getSerializedParameterMap();
        parameters["isCopyUrl"] = true;
        generateReportURL(url, parameters, "resultList");
        treasury.margin.common.hideLoading("mainLoader");
    }

    /****************************************************************************
     *This method resets all filters.
     ****************************************************************************/
    function _resetFilters() {
        legalEntityMultiSelect.setSelectedNodes([]);
    		cpeMultiSelect.setSelectedNodes([]);
    		agreementTypeMultiSelect.setSelectedNodes([]);
        _setupDateFilter();
    }

    /****************************************************************************
     *This method loads calculators based on parameters in copy search URL.
     ****************************************************************************/
    function _loadCopyUrlData() {
        if (window.location.href.indexOf("isCopyUrl=true") >= 0) {
            treasury.margin.common.showLoading("mainLoader");
            var urlParams = window.location.href.split("?");
            if (urlParams[1] != undefined) {
                var paramList = urlParams[1].split("&");
            }
            var searchData = {};
            for (var i = 0; i < paramList.length; i++) {
                var param = paramList[i].split("=");
                var val = param[1];
                if (val != null && param[0] != "isCopyUrl") {
                    searchData[param[0]] = val;
                }
            }
            treasury.margin.common.hideLoading("mainLoader");
             _setFilters(searchData);
            treasury.margin.ssu.calcNav.showNavMenu(event, searchData);
        } else {
            document.getElementById('searchMessage').removeAttribute("hidden");
        }
    }
})();
