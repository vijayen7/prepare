"use strict";
var calcData;
var configData;
var prodConfigNameData = {};
var simConfigNameData = {};
var simulationConfigListSection;

(function() {
    window.treasury = window.treasury || {};
    window.treasury.margin = window.treasury.margin || {};
    window.treasury.margin.ssu = window.treasury.margin.ssu || {};

    window.treasury.margin.ssu.calcNav = {
        showNavMenu: _showNavMenu,
        showChildren: _showChildren,
        hideChildren: _hideChildren,
        toggleConfigTypeList: _toggleConfigTypeList,
        toggleConfigList: _toggleConfigList,
        loadConfigAndToggleAttrs: _loadConfigAndToggleAttrs,
        toggleChildCalculatorsList: _toggleChildCalculatorsList,
        getAttrSectionHTML : _getAttrSectionHTML,
        showConfigList : _showConfigList,
        getCalcIdFromAgreement : _getCalcIdFromAgreement,
        getConfigList : _getConfigList,
        renderNavigationMenuWithCalculatorsData :_renderNavigationMenuWithCalculatorsData
    };

    /****************************************************************************
     * This function loads calculators based on search filters and renders
     * the Navigation Menu with list of all calculators returned.
     ****************************************************************************/
    function _showNavMenu(event, searchData, flag) {
        //Clear the current state of Config Editor Section to empty
        if(document.getElementById('datePickerError').innerHTML != "") return;
        var noOfEditedRows = Object.keys(editedConfigData).length;
        if (noOfEditedRows > 0) {
            treasury.margin.ssu.util.updateFilterErrorMessage("Please save/reset current config changes first", "saveConfigError");
            return;
        }
        _clearNavMenuAndConfigEditorSection();
        //load and render Calculators for search params
        treasury.margin.ssu.Actions.getCalcNavMetadata(searchData, flag);
    }

    function _renderNavigationMenuWithCalculatorsData(calculatorData){

        if(!calculatorData) return;
        calcData = calculatorData;

        var html = '<ul>';
        var methDataIndicesSortedByAgmtName = _sortmethDataIndicesByAgmtName(methodologyMarginDataList);

        $.each(methDataIndicesSortedByAgmtName, function(i, idx) {
            var agreementData = methodologyMarginDataList[idx];
            html += _getCalcNavHTML(agreementData['id'], agreementData['name'], 'agreement');

            var methodologyDataList = agreementData['children'];
            html += "<ul>";
            $.each(methodologyDataList, function(j, methodologyData) {
                html += _getCalcNavHTML(methodologyData['id'], methodologyData['name'], 'methodology');
                html += "<ul>";
                html += _getConfigListHTML(methodologyData['id'], 'prod');
                html += _getConfigListHTML(methodologyData['id'], 'sim');
                html += "</ul>";

                //Methodology has child methodologies
                if (methodologyData['children'] && methodologyData['children'].length > 0) {
                    html += "<ul>";
                    $.each(methodologyData['children'], function(j, childMethodologyData) {
                        html += _getCalcNavHTML(childMethodologyData['id'],
                            childMethodologyData['name'], 'childMethodology');
                        html += "<ul>";
                        html += _getConfigListHTML(childMethodologyData['id'], 'prod');
                        html += _getConfigListHTML(childMethodologyData['id'], 'sim');
                        html += "</ul>";
                        html += "</li>";
                    });
                    html += "</ul>";
                }
                html += "</li>";
            });
            html += "</ul>";
            html += "</li>"
        });
        html += "</ul>";

        var count = methodologyMarginDataList.length;
        document.getElementById('noOfRecords').innerText = count;
        document.getElementById('calcNavList').innerHTML = html;
        document.getElementById('searchMessage').hidden = true;
        document.getElementById('calcNavInfoTab').hidden = false;
        if (count > 0) {
            document.getElementById('editorMessage').hidden = false;
            document.getElementById('configEditorMainContainer').hidden = false;
        }
        _setEventMethodsForCalcNav();
    }

    /****************************************************************************
     * This function clears Navigation Menu and Config Editor sections.
     ****************************************************************************/
    function _clearNavMenuAndConfigEditorSection() {
        $('#calcNavList').html('');
        $('#calcNamePanel').html('');
        $('#editorSectionContainer').html('');
        document.getElementById("noCalcMessage").hidden = true;
        document.getElementById("noCalcMessage").hidden = true;
        document.getElementById("calcNavInfoTab").hidden = true;
        document.getElementById("editorMessage").hidden = true;
        document.getElementById('saveResetSection').hidden = true;
    }

    /****************************************************************************
     * This function sorts agreement Names in methodologyMarginDataList by
     * agreementName and returns array of corresponding indices in the same order
     ****************************************************************************/
    function _sortmethDataIndicesByAgmtName(methodologyMarginDataList) {
        //a map of agreement name and indices of methodologyMarginDataList
        // where this agreementname occurs
        var agmtNameIdxMap = {};
        var agmtNamesList = [];
        var sortedIndicesList = [];

        $.each(methodologyMarginDataList, function(idx, agmt) {
            var agmtName = agmt.name.toLowerCase();
            if (!agmtNameIdxMap[agmtName]) {
                agmtNameIdxMap[agmtName] = [];
            }
            agmtNameIdxMap[agmtName].push(idx);
        });

        agmtNamesList = Object.keys(agmtNameIdxMap);
        var sortedAgmtNames = agmtNamesList.sort(); //sorts the array NOT in-place
        $.each(sortedAgmtNames, function(i, agmtName) {
            sortedIndicesList = sortedIndicesList.concat(agmtNameIdxMap[agmtName]);
        });
        return sortedIndicesList;
    }

    /********************************************************************************
     * This function generates HTML to render agreement data as navigation menu.
     * It modifies HTML based on the type of the element(e.g. agreement/methodology)
     ********************************************************************************/
    function _getCalcNavHTML(id, name, type) {
        var classList = type + ' padding--left--double';
        var calcNavHTML = "<li class='" + classList + "' id = '" + id + "'>" +
            "<div class='calcname'>" +
            "<button class='button--tertiary calcname-btn'>" +
            "<i class='icon-fw' id = 'icon-" + id + "'></i>"+
            name +
            "</button></div>";
        return calcNavHTML;
    }

    function _getConfigListHTML(id, type)
    {
        var configType = (type == 'prod') ? 'Production' : 'Simulation';

        var configListHTML = "<li class='config padding--left--double' id='" + type + "-" + id + "' hidden>" +
            "<div class='config-type'>" +
            "<button class='button--tertiary config-type-btn' id='" + type + "-" + id + "-config-btn'>" +
            "<i class='icon-fw' id='" + type + "-icon-" + id + "'></i>" +
            configType + " Configuration</button></div>" +
            "<div class='" + type + "-config-list margin--left--large' hidden>" +
            "</div>";
        
        configListHTML += "</li>";

        return configListHTML;
    }
    /****************************************************************************
     * This function generates HTML to render attributes section of a methodology.
     ****************************************************************************/
    function _getAttrSectionHTML() {
        var attrSectionHTML = "<div class='attr-section margin--left--large' hidden></div>";
        return attrSectionHTML;
    }

    /****************************************************************************
     * This function sets the event behavior of components in Calc Nav dom.
     ****************************************************************************/
    function _setEventMethodsForCalcNav() {
        $('.agreement').addClass('parent margin--top');
        $('.methodology').addClass('parent child');
        $('.childMethodology').addClass('child');

        treasury.margin.ssu.util.hideListElements(document.getElementsByClassName('methodology'));
        treasury.margin.ssu.util.hideListElements(document.getElementsByClassName('childMethodology'));

        var calcNodes = $('.methodology .calcname-btn');
        //User cannot view another config if the current config has been edited and not saved.
        $.each(calcNodes, function(index, calcNode) {
        	var methodologyId = calcNode.closest('li').id;
        	document.getElementById(methodologyId).querySelector('i').classList.add('icon-expand-row');
            calcNode.onclick = function(event) {
                var sameTabClicked = (event.target.closest('li').id == selectedMethodologyId);
                if (editedConfigData && Object.keys(editedConfigData).length && !sameTabClicked > 0) {
                  treasury.margin.ssu.util.
                      updateFilterErrorMessage("Please save/reset changes to current config to " +
                          "view another config", "saveConfigError");
                }
                else {
                    var methodologyNode = event.target.closest('li');
                    var agreementId = event.target.closest('.agreement').id;
                    var calculatorId = _getCalcIdFromAgreement(agreementId,
                        methodologyNode.id, methodologyNode.classList.contains('childMethodology'));
                    selectedMethodologyId = methodologyNode.id;
                    var icon = document.getElementById(selectedMethodologyId).querySelector('i');
                    if(!(icon.classList.contains('icon-expand-row') || 
                        icon.classList.contains('icon-collapse-row')))
                    {
                        icon.classList.add('icon-expand-row');
                    } 
                    if(calculatorId == -1 || (!(treasury.margin.ssu.util.configExistsForCalculator(calculatorId))))
                    {
                        treasury.margin.ssu.util.
                            raiseInfoToastWithTimeOut('No calculator config available for this agreement', "2000");
                    }
                    else
                    {
                        treasury.margin.ssu.calcNav.toggleConfigTypeList(event, calculatorId);
                    }
                }
            };
        });

        var configTypeNodes = $('.config .config-type-btn');
        $.each(configTypeNodes, function(index, configTypeNode)
        {
            var configTypeId = configTypeNode.closest('li').id;
            document.getElementById(configTypeId).querySelector('i').classList.add('icon-expand-row');
        	configTypeNode.onclick = function(event)
            {
                var sameTabClicked = (event.target.closest('li').id == selectedConfigTypeId);
                if (editedConfigData && Object.keys(editedConfigData).length && !sameTabClicked > 0) {
                    treasury.margin.ssu.util.updateFilterErrorMessage("Please save/reset changes to current config to view another config", "saveConfigError");
                  }
                else {
                    var configTypeId = event.target.closest('li').id;
                    selectedConfigTypeId = configTypeId;
                    var icon = document.getElementById(selectedConfigTypeId).querySelector('i');
                    
                    if(!(icon.classList.contains('icon-expand-row') || 
                        icon.classList.contains('icon-collapse-row')))
                    {
                        icon.classList.add('icon-expand-row');
                    } 
                    var configType = _getConfigType(event);
                    var methodologyId = configTypeId.replace(configType + "-", "");
                    var methodologyNode = document.getElementById(methodologyId);
                    var agreementId = event.target.closest('.agreement').id;
                    var calculatorId = _getCalcIdFromAgreement(agreementId,
                        methodologyNode.id, methodologyNode.classList.contains('childMethodology'));

                    var configList = _getConfigList(calculatorId);
                    var button = event.currentTarget;
                    if(configType == "prod")
                    {
                    	treasury.margin.ssu.calcNav.toggleConfigList(event.currentTarget, configList, 'productionConfig');
                    }
                    else if(configType == "sim")
                    {
                    	treasury.margin.ssu.calcNav.toggleConfigList(event.currentTarget, configList, 'simulationConfig');
                    }
                }
            }
        });

        var parents = document.querySelectorAll('.parent');
        $.each(parents, function(index, parent) {
            var children = document.getElementById(parent['id']).querySelectorAll('.child');
            if (children.length > 0) {
                $('#' + parent['id'] + ' .child').addClass('padding--left--large');
                parent.closest('li').classList.remove('padding--left--double');
                parent.querySelector('i').classList.add('icon-expand-row');
                parent.querySelector('.calcname-btn').onclick =
                    function(event) {
                        treasury.margin.ssu.calcNav.toggleChildCalculatorsList(event)
                    };
                $('#' + parent['id'] + ' .toggle-btn').show();
            } else {
                $('#' + parent['id'] + ' .toggle-btn').hide();
            }
        });
    }
    
    function _getConfigType(event)
    {
        var configTypeId = event.currentTarget.closest('li').id;
        
        return (configTypeId.search("prod") != -1) ? "prod" : "sim";
    }  	
    
    function _toggleConfigTypeList(event, calculatorId)
    {
        var button = event.currentTarget;
        var prodConfig = document.getElementById('prod-' + selectedMethodologyId);
        var simConfig = document.getElementById('sim-' + selectedMethodologyId);
        var icon = document.getElementById(selectedMethodologyId).querySelector('i')
        if (button.querySelector('i').classList.contains('icon-expand-row')) {
            _showConfigType(prodConfig, simConfig, icon);
        } else {
            _hideConfigType(prodConfig, simConfig, icon);
        }
    }

    function _showConfigType(prodConfig, simConfig, icon)
    {
        prodConfig.removeAttribute("hidden");
        simConfig.removeAttribute("hidden");
        icon.classList.remove('icon-expand-row');
        icon.classList.add('icon-collapse-row');
    }

    function _hideConfigType(prodConfig, simConfig, icon)
    {
        prodConfig.setAttribute("hidden", true);
        simConfig.setAttribute("hidden", true);
        icon.classList.add('icon-expand-row');
        icon.classList.remove('icon-collapse-row');
    }
    
    /****************************************************************************
     * This function fetches list of all the Configs for a given Calculator. It then groups them into
     * Simulation and Production Configurations.
     ****************************************************************************/
    
    function _getConfigList(calculatorId)
    {
        var configList = {};
        configList['simulationConfig'] = [];
        configList['productionConfig'] = [];
        var configs = calcData[calculatorId]['configData'];
        var configDataIndex;
        for (configDataIndex = 0; configDataIndex < configs.length; configDataIndex++)
        {
            var configDetails = configs[configDataIndex];
            configDetails['isSimulationConfig'] ? 
               	configList['simulationConfig'].push(configDetails) : 
               		configList['productionConfig'].push(configDetails); 
        }

        return configList;
    }
    
    /****************************************************************************
     * This function toggles the list of Configs. It dynamically puts HTML on each of the Configs.
     ****************************************************************************/
    function _toggleConfigList(button, configList, configType)
    {
    	var configs = configList[configType];
        var configHTML;
        configHTML = "<ul class = 'margin--left--large'>";
        var i; 	
        
        var class_type = (configType == 'productionConfig') ? 'prod' : 'sim';
        var configNameData = (configType == 'productionConfig') ? prodConfigNameData : simConfigNameData;
        for(i = 0; i < configs.length; i++)
        {
            var configData = configs[i];
            configHTML  += "<li class='" + class_type + "-config-name' id='" + configData['configName'] + "-" + class_type + "-config'>" +
                           "<div class='" + class_type + "-config-data'>" +
                           "<button class='button--tertiary vw-" + class_type + "-config-btn'>" +
                           "<i class='icon-fw'></i>" + configData['configName'] + "</button></div>";
            var attrSectionHTML = _getAttrSectionHTML();
            configHTML += attrSectionHTML + "</li>";
            configNameData[configData['configName']] = configData;
        }
        
        configHTML += "</ul>";
        var configListSection = button.closest('li').querySelector('.' + class_type + '-config-list');
        configListSection.innerHTML = configHTML;
        if (button.querySelector('i').classList.contains('icon-expand-row')) {
            _showConfigList(configListSection, button);
        } else {
            _hideConfigList(configListSection, button);
        }
        
        var configNodes = $('.' + class_type + '-config-data .vw-' + class_type + '-config-btn');
        $.each(configNodes, function(index, config)
        {
            if(selectedConfigId == "")
            {
        	    selectedConfigId = config.closest('li').id;
            }
            config.onclick = function(event) {
                let parent = event.target.parentElement;
                while(!parent.classList.contains('config')){
                    parent = parent.parentElement;
                }
                let closestMethodologyId = parent.id;
                let search_pattern_for_methodology_id = '(sim|prod)-';
                let methodologyRegex = new RegExp(search_pattern_for_methodology_id);
                let currentMethodologyId = closestMethodologyId.replace(methodologyRegex, "" );
                if (selectedMethodologyId != currentMethodologyId){
                    selectedMethodologyId = currentMethodologyId;
                }
                if (editedConfigData && Object.keys(editedConfigData).length) {
                treasury.margin.ssu.util.
                        updateFilterErrorMessage("Please save/reset changes to current config to " +
                            "view another config", "saveConfigError");
                }
                else { 
                    var configNameId = event.target.closest('li').id;
                    var search_pattern = '\-' + class_type + '-config';
                    var re = new RegExp(search_pattern);
                    selectedConfigName = configNameId.replace(re, "");
                    treasury.margin.ssu.calcNav.loadConfigAndToggleAttrs(
                        event.currentTarget, selectedConfigName);                    
                }
            }
        });
    }
    function _showConfigList(configListSection, button)
    {
        configListSection.removeAttribute("hidden");
        button.querySelector('i').classList.remove('icon-expand-row');
        button.querySelector('i').classList.add('icon-collapse-row');  
    }
    
    function _hideConfigList(configListSection, button)
    {
        configListSection.setAttribute("hidden", true);
        button.querySelector('i').classList.remove('icon-collapse-row');
        button.querySelector('i').classList.add('icon-expand-row');  
    }

    /****************************************************************************
     * This function shows/hides list of child calculators on clicking
     * expand/collapse button.
     ****************************************************************************/
    function _toggleChildCalculatorsList(event) {
        var button = event.currentTarget;
        let isParentCalcConfigExists = false;

        let parentId = button.closest('li').id;
        if (parentId == undefined || parentId == null) return;
        let parent = document.getElementById(parentId);

        // This check is to decide whether master calculator config exists or not.
        if (!parent.classList.contains('agreement')) {
            let agreementId = button.closest('.agreement').id;
            let calculatorId = _getCalcIdFromAgreement(agreementId,
                parentId, false);
            selectedMethodologyId = parentId;
            if (calculatorId != -1 && treasury.margin.ssu.util.configExistsForCalculator(calculatorId)) {
                isParentCalcConfigExists = true;
            }
        }

        if (button.querySelector('i').classList.contains('icon-expand-row')) {
            if (isParentCalcConfigExists == true) {
                document.getElementById('prod-' + parentId).removeAttribute('hidden');
                document.getElementById('sim-' + parentId).removeAttribute('hidden');
            }
            _showChildren(parentId);
        } else {
            if (isParentCalcConfigExists == true) {
                document.getElementById('prod-' + parentId).setAttribute("hidden", true);
                document.getElementById('sim-' + parentId).setAttribute("hidden", true);
            }
            _hideChildren(parentId);
        }
    }

    function _showChildren(parentId) {
        if (parentId == undefined || parentId == null) return;
        var parent = document.getElementById(parentId);
        var childClass = parent.classList.contains('agreement') ? '.methodology' : '.childMethodology';
        var children = parent.querySelectorAll(childClass);
        if (children.length > 0) {
            treasury.margin.ssu.util.showListElements(children);
            document.getElementById(parentId).querySelector('i').classList.remove('icon-expand-row');
            document.getElementById(parentId).querySelector('i').classList.add('icon-collapse-row');
        }
    }

    function _hideChildren(parentId) {
        if (parentId == undefined || parentId == null) return;
        var parent = document.getElementById(parentId);
        var childClass = parent.classList.contains('agreement') ? '.methodology' : '.childMethodology';
        var children = parent.querySelectorAll(childClass);
        if (children.length > 0) {
            treasury.margin.ssu.util.hideListElements(children);
            document.getElementById(parentId).querySelector('i').classList.add('icon-expand-row');
            document.getElementById(parentId).querySelector('i').classList.remove('icon-collapse-row');
        }
    }





    /****************************************************************************
     * If config not already loaded, load config and expand attrs list,
     * otherwise, collapse attrs list in nav menu.
     ****************************************************************************/
    function _loadConfigAndToggleAttrs(button, configName) {
        var configNode = button.closest('li')
        var attrSection = configNode.querySelector('.attr-section');
        var isAttrSectionCollapsed = attrSection.hidden;

        document.getElementById('editorMessage').hidden = true;
        document.getElementById('saveResetSection').hidden = true;

            //highlight selected tab
        	selectedConfigId = button.closest('li').id;
        	$('#calcNavList div').removeClass('selected');
            configNode.querySelector('div').classList.add('selected');
            configData = treasury.margin.ssu.Actions.getParsedData(configName, CONFIG_UI_SET_CURRENT_CALCULATOR_CONFIG);
            if (configData == null) {
                $('#calcNamePanel').html('');
                $('#editorSectionContainer').html('');
                document.getElementById("noCalcMessage").hidden = false;
                return;
            }
            treasury.margin.ssu.configEditor.loadConfigEditor(configName);
            _expandAttrsList(button);
        if(isAttrSectionCollapsed){
          _expandAttrsList(button);
        }
        else{
          _collapseAttrsList(button);
        }

    //    _hideChildren(button.closest('li').id)
        document.getElementById('saveResetSection').hidden = false;
    }

    /****************************************************************************
     * This function return calculator id of agreement with id agreementId.
     ****************************************************************************/
    function _getCalcIdFromAgreement(agreementId, methodologyId, isChildMethodology) {
        for (var i = 0; i < methodologyMarginDataList.length; i++) {
            if (methodologyMarginDataList[i].id != agreementId) continue;

            var methodologies = methodologyMarginDataList[i]['children'];

            if (isChildMethodology) {
                for (var j = 0; j < methodologies.length; j++) {
                    var childMethodologies = methodologies[j].children || [];
                    for (var k = 0; k < childMethodologies.length; k++) {
                        if (childMethodologies[k].id == methodologyId) {
                            return childMethodologies[k].calculatorId;
                        }
                    }
                }
            } else {
                for (var j = 0; j < methodologies.length; j++) {
                    if (methodologies[j].id == methodologyId) {
                        return methodologies[j].calculatorId;
                    }
                }
            }
            return -1;
        }
    }

    /*********************************************************************************
     * This method collapses(hides) the list of attributes for calculator associated
     * with btn element upon clicking the button btn.
     ********************************************************************************/
    function _collapseAttrsList(btn) {
        var attrSection = btn.closest('li').querySelector('.attr-section');
        attrSection.hidden = true;
    }

    /*********************************************************************************
     * This method expands(shows) the list of attributes for calculator associated
     * with btn element upon clicking the button btn.
     ********************************************************************************/
    function _expandAttrsList(btn) {
        var attrs = Object.keys(configData);
        var html = "<ul class = 'margin--left--large'>";
        for (var i in attrs) {
            if (Object.keys(configData[attrs[i]]).length) {
                var targetId = '#' + attrs[i].replace(/ /g, "_") + '-container';
                html += "<li class='margin--bottom'>" +
                    "<a href='" + targetId + "'>" + treasury.margin.ssu.util.formatText(attrs[i]) + "</a>" +
                    "</li>";
            }
        }
        html += "</ul>";
        var attrSection = btn.closest('li').querySelector('.attr-section');
        attrSection.innerHTML = html;
        attrSection.hidden = false;
    }
})();