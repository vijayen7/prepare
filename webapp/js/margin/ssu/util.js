"use strict";

(function() {
    window.treasury = window.treasury || {};
    window.treasury.margin = window.treasury.margin || {};
    window.treasury.margin.ssu.util = {
        //CSS Utility Methods
        showListElements: _showListElements,
        hideListElements: _hideListElements,
        enableListElements: _enableListElements,
        disableListElements: _disableListElements,
        resizeAllCanvas: _resizeAllCanvas,
        formatText: _formatText,
        configExistsForCalculator: _configExistsForCalculator,
        //Filter related methods
        getSingleSelectValue: _getSingleSelectValue,
        loadSingleSelectFilter: _loadSingleSelectFilter,
        initCountryFilter: _initCountryFilter,
        initCurrencyFilter: _initCurrencyFilter,
        initGboTypeFilter: _initGboTypeFilter,
        initFoTypeFilter: _initFoTypeFilter,
        initMarketFilter: _initMarketFilter,
        setupDateFilter : _setupDateFilter,
        //Auth related methods
        isUserAuthorizedToEditConfig: _isUserAuthorizedToEditConfig,
        //Toast Related methods
        updateFilterErrorMessage : updateFilterErrorMessage,
        raiseInfoToast: raiseInfoToast,
        raiseSuccessToast: raiseSuccessToast,
        raiseInfoToastWithTimeOut: raiseInfoToastWithTimeOut,
        raiseErrorToast: raiseErrorToast,
        raiseErrorToastWithTimeOut: raiseErrorToastWithTimeOut,
    };

    /*********************************************************************************
     * This method returns the value of singleSelect filter corresponding to the id.
     ********************************************************************************/
    function _getSingleSelectValue(id, filter) {
        var inputFilterList = filter.inputFilterList;
        for (var i = 0; i < inputFilterList.length; i++) {
            if (inputFilterList[i][0] == id)
                return inputFilterList[i][1];
        }
        return "";
    }

    /*********************************************************************************
     * This method loads appropriate single select filter based on 'type' given.
     ********************************************************************************/
    function _loadSingleSelectFilter(type, comboboxId) {
        switch (type) {
            case 'country':
                treasury.margin.ssu.util.initCountryFilter(comboboxId);
                break;
            case 'currency':
                treasury.margin.ssu.util.initCurrencyFilter(comboboxId);
                break;
            case 'fo_type':
                treasury.margin.ssu.util.initFoTypeFilter(comboboxId);
                break;
            case 'gbo_type':
                treasury.margin.ssu.util.initGboTypeFilter(comboboxId);
                break;
            case 'market':
                treasury.margin.ssu.util.initMarketFilter(comboboxId);
                break;
        }
    }

    /*********************************************************************************
     * This method loads single select filter with list of all countries
     ********************************************************************************/
    function _initCountryFilter(comboboxId) {
        $.when(treasury.loadfilter.countries()).done(function(countriesData) {
            singleSelectFilter = new window.treasury.filter.SingleSelect(
                "countryId",
                comboboxId,
                countriesData["countries"], []
            );
        });
    }

    /*********************************************************************************
     * This method loads single select filter with list of all currencies
     ********************************************************************************/
    function _initCurrencyFilter(comboboxId) {
        $.when(treasury.loadfilter.loadCurrencies()).done(function(currenciesData) {
            singleSelectFilter = new window.treasury.filter.SingleSelect(
                "currencyId",
                comboboxId,
                currenciesData[0].currency, []
            );
        });
    }

    /*********************************************************************************
     * This method loads single select filter with list of all GBO types
     ********************************************************************************/
    function _initGboTypeFilter(comboboxId) {
        $.when(treasury.loadfilter.loadGboTypes()).done(function(gboTypesData) {
            singleSelectFilter = new window.treasury.filter.SingleSelect(
                "gboTypesId",
                comboboxId,
                gboTypesData.gboTypes, []
            );
        });
    }

    /*********************************************************************************
     * This method loads single select filter with list of all FO types
     ********************************************************************************/
    function _initFoTypeFilter(comboboxId) {
        $.when(treasury.loadfilter.loadFOTypes()).done(function(foTypesData) {
            singleSelectFilter = new window.treasury.filter.SingleSelect(
                "foTypesId",
                comboboxId,
                foTypesData.attrFOTypes, []
            );
        });
    }

    /*********************************************************************************
     * This method loads single select filter with list of all Markets
     ********************************************************************************/
    function _initMarketFilter(comboboxId) {
        $.when(treasury.loadfilter.loadMarkets()).done(function(marketsData) {
            singleSelectFilter = new window.treasury.filter.SingleSelect(
                "marketId",
                comboboxId,
                marketsData.markets, []
            );
        });
    }

    /*********************************************************************************
     * This method shows all DOM elements in the list elems.
     ********************************************************************************/
    function _showListElements(elems) {
        for (var i = 0; i < elems.length; i++) {
            elems[i].removeAttribute('hidden')
        }
    }

    /*********************************************************************************
     * This method hides all DOM elements in the list elems.
     ********************************************************************************/
    function _hideListElements(elems) {
        for (var i = 0; i < elems.length; i++) {
            elems[i].setAttribute('hidden', true)
        }
    }

    /*********************************************************************************
     * This method disables all DOM elements in the list elems.
     ********************************************************************************/
    function _disableListElements(elems) {
        for (var i = 0; i < elems.length; i++) {
            elems[i].setAttribute('disabled', true)
        }
    }

    /*********************************************************************************
     * This method enables all DOM elements in the list elems.
     ********************************************************************************/
    function _enableListElements(elems) {
        for (var i = 0; i < elems.length; i++) {
            elems[i].removeAttribute('disabled')
        }
    }

    /*********************************************************************************
     * This method returns a human readable format of the string 'text'. It replaces
     * underscores with white spaces and capitalizes first letter of each word.
     * If delimByCommas is TRUE, it converts the number to comma separated e.g.
     * 10000 => 10,000 and _sector_concentration => Sector Concentration
     ********************************************************************************/
    function _formatText(text,delimByCommas) {
        text=text.replace(/_/g, " ");
        if(delimByCommas){
          return parseFloat(text).toLocaleString();
        }
        else {
          return text.replace(/\w\S*/g, function(txt){
              return txt.charAt(0).toUpperCase() + txt.substr(1);
          });
        }
    }

    /*********************************************************************************
     * This method resizes the canvas upon expanding/collapsing sidebar.
     ********************************************************************************/
    function _resizeAllCanvas() {
        var ssuFilterSidebar = document.getElementById('ssuFilterSidebar');
        if (ssuFilterSidebar != null && ssuFilterSidebar.state == 'expanded') {
            ssuFilterSidebar.style.width = '300px';
        } else if (ssuFilterSidebar != null) {
            ssuFilterSidebar.style.width = '20px';
        }
    }

    /*********************************************************************************
     * This method returns TRUE if current user is allowed to edit config, returns
     * FALSE otherwise.
     ********************************************************************************/
    function _isUserAuthorizedToEditConfig(){
        return $.ajax({
        url: '/treasury/user-auth-to-edit-config',
        type: "GET"
      });
    }

    /*********************************************************************************
     * This method sets up the date filter as per given parameters.
     ********************************************************************************/
    function _setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
    dateFormat) {
    dateDiv.datepicker({
      dateFormat: dateFormat,
      changeMonth: true,
      changeYear: true,
      minDate: minDate,
      maxDate: maxDate,
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
    }).datepicker("setDate", date);
    dateDiv.change(function() {
      validateDate(dateDiv.val(), {
        errorDiv: errorDiv,
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: dateFormat,
      });
      updateFilterErrorMessage();
    });
  }

  function raiseInfoToast(message) {
    toastr.options.timeOut = "0";
    toastr.options.positionClass =  "toast-top-right";
    toastr.options.extendedTimeOut = "0";
    return toastr.info(message);
  }

  function raiseInfoToastWithTimeOut(message, timeout) {
    toastr.options.timeOut = timeout||"5000";
    toastr.options.positionClass =  "toast-top-right";
    toastr.options.extendedTimeOut = "1000";
    return toastr.info(message);
  }

  function raiseSuccessToast(message)
  {
    toastr.options.timeOut = "5000";
    toastr.options.positionClass =  "toast-top-right";
    toastr.options.extendedTimeOut = "1000";
    return toastr.success(message);
  }

  function raiseErrorToast(message)
  {
    toastr.options.timeOut = "0";
    toastr.options.positionClass =  "toast-top-right";
    toastr.options.extendedTimeOut = "0";
    toastr.options.closeButton = "true";
    return toastr.error(message);
  }
  
  function raiseErrorToastWithTimeOut(message, timeout){
    toastr.options.timeOut = timeout||"5000";
    toastr.options.positionClass =  "toast-top-right";
    toastr.options.extendedTimeOut = "1000";
    return toastr.error(message);
  }

  function updateFilterErrorMessage(message,id){
    var datePickerErrorElement = document.getElementById('datePickerError');
    if(!(treasury.margin.util.isDefinedAndValid(datePickerErrorElement))) return;

    if(document.getElementById('datePickerError').innerHTML!=""){
      if($('#dateError').val()==undefined)
        $('#filterErrorMsg ul').append("<li id='dateError'>Please select a valid date</li>");
    }
    else{
      $('#dateError').remove();
    }

    if(message && message!="")
      $('#filterErrorMsg ul').append("<li" + " id='" +id+ "'>" + message + "</li>");

    if($('#filterErrorMsg ul').children().length > 0)
      document.getElementById('filterErrorMsg').hidden = false;
    else
      document.getElementById('filterErrorMsg').hidden = true;
  }
  function _configExistsForCalculator(calculatorId)
  {
      return calcData[calculatorId] != undefined;
  }
})();
