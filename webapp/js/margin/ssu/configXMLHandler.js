var dataTypeNodes = [];
var arrValueToFind;
var xmlInput = "";
var currentCalculatorConfig;
var currentSearchData;
var currentXmlDom;
var allConfigsData = {};
var allConfigNames = [];
var methodologyMarginDataList = [];
var DUMMY_TABLE_NAME = 'other_parameters';


(function() {
    window.treasury = window.treasury || {};
    window.treasury.margin = window.treasury.margin || {};
    window.treasury.margin.ssu = window.treasury.margin.ssu || {};
    window.treasury.margin.ssu.Actions = {
        getCalcNavMetadata: _getCalcNavMetadata,
        getParsedData: _getParsedData,
        generateTables: _generateTables,
        updateXML: _updateXML,
        fetchConfigForConfigName: _fetchConfigForConfigName,
        fetchConfigsForCalculators: _fetchConfigsForCalculators,
        postCalculatorConfigDataToDB: _postCalculatorConfigDataToDB,
        postClonedConfigToDB: _postClonedConfigToDB,
        getRootElement : _getRootElement,
        parseXmlInput : _parseXmlInput,
        validateConfigNameAlreadyExists : _validateConfigNameAlreadyExists,
        validateConfigNameContainsSpecialCharacters : _validateConfigNameContainsSpecialCharacters,
        isConfigInputEmpty : _isConfigInputEmpty,
        updateConfigData : _updateConfigData
    };

    /*************************************************************************************
     * This function fetches methodology data and config Data. saves corresponding config
     * for each methodology in ConfigDataMap.
     *************************************************************************************/
    function _getCalcNavMetadata(searchData, flag) {
        
        var params = searchData ||
            searchPortfolioFilterGroup.getSerializedParameterMap();
        var errorMsg = _validateInput(params);

        if (!errorMsg) {
            params.dateParam = params.dateString;
            currentSearchData = params;
            _fetchMethodologyMarginDataList(params, flag);
        }else {
            treasury.margin.ssu.util.raiseErrorToast(errorMsg);
            return null;
        }
        
    }

    function _getMethodologyConfigDataMap(){
        var methodologyConfigDataMap = {};
        $.each(methodologyMarginDataList, function(i, agreementData) {
            var methodologyDataList = agreementData['children'];
            $.each(methodologyDataList, function(j, methodologyData) {
                var calcId = methodologyData['calculatorId'];
                _addKeyIfDoesNotExist(calcId, methodologyConfigDataMap);
                if (methodologyData['children'] && methodologyData['children'].length > 0) {
                    $.each(methodologyData['children'], function(j, childMethodologyData) {
                        var calcId = methodologyData['calculatorId'];
                        _addKeyIfDoesNotExist(calcId, methodologyConfigDataMap);
                    });
                }
            });
        });
        return methodologyConfigDataMap;
    }

    function _populateCaldDataAndRenderCalcNavList(){

        let metaData = {};
        treasury.margin.common.showLoading("mainLoader");
        
        for (let key in allConfigsData) {
            let result = allConfigsData[key];
            if(metaData[result['calculator_id']] == undefined)
            {
                metaData[result['calculator_id']] = {};
                var calcMetaData = {
                        'calculatorId': result['calculator_id'],
                        'calculatorName': result['calculator_name'],
                        'parentId': result['parent_calculator_id'],
                        'children': [],
                        'configData' : []
                    }
                    metaData[result['calculator_id']] = calcMetaData;
            }
            var configData = {
                'configId': result['config_id'],
                'isSimulationConfig': result['is_simulation_config'],
                'configName' : result['config_path']
            };
            var configList = metaData[result['calculator_id']]['configData'];
            configList.push(configData);
            allConfigNames.push(result['config_path']);
        }
        if (!metaData) return;
        treasury.margin.ssu.calcNav.renderNavigationMenuWithCalculatorsData(metaData);
        treasury.margin.common.hideLoading("mainLoader");
    }
    /*************************************************************************************
     * This function validates input filter and populates appropriate message in errorMsg
     *************************************************************************************/
    function _validateInput(searchParams) {
        if (searchParams.cpeIds == -1 && searchParams.agreementTypeIds == -1 &&
            searchParams.descoEntityIds == -1 &&
            (searchParams.dateString == undefined || searchParams.dateString == "")) {
            var error = "All-All search is not supported. Please select atleast one filter."
        }
        return error;
    }

    /*************************************************************************************
     * This function adds key to map if it doesn't exist already.
     *************************************************************************************/
    function _addKeyIfDoesNotExist(key, map) {
        if (key != undefined && !map.hasOwnProperty(key)) {
            map[key] = null;
        }
        return map;
    }

    /*************************************************************************************
     * This function fetches methodology data in methodologyMarginDataList for searchParams
     *************************************************************************************/
    function _fetchMethodologyMarginDataList(searchParams,flag) {
        $.ajax({
            url: "/treasury/data/search-methodology-margin",
            data: searchParams,
            type: "GET",
            dataType: "json",
            error: function(response, data) {
                methodologyMarginDataList = null; 
                errorMsg = 'No methodology data available for this search criteria';
                treasury.margin.ssu.util.raiseErrorToastWithTimeOut(errorMsg, 2000);
                treasury.margin.common.hideLoading("mainLoader");
                return;
            },
            success: function(data) {
                // if no data
                if (data == null || data["resultList"] == undefined ||
                    data["resultList"].length == 0) {
                    methodologyMarginDataList = null;
                    errorMsg = 'No methodology data available for this search criteria';
                    treasury.margin.ssu.util.raiseErrorToastWithTimeOut(errorMsg);
                    return;
                }
                methodologyMarginDataList = data['resultList'];
                var methodologyConfigDataMap = _getMethodologyConfigDataMap();
                var calculatorIds = Object.keys(methodologyConfigDataMap);
                var params = {
                    'marginCalculatorIds': calculatorIds.join()
                };
                //initializing allConfigData on every search 
                allConfigsData = {};
                treasury.margin.ssu.Actions.fetchConfigsForCalculators(params, flag);
            },
            timeout: 120000
        });
    }

    /*
     * Returns configData for given configName by parsing
     */
    function _getParsedData(configName, flag) {

        currentCalculatorConfig = _fetchConfigForConfigName(configName, flag);
        if (currentCalculatorConfig == null) {
            return null;
        }
        var xmlInput = currentCalculatorConfig['config_xml'];
        xmlInput = xmlInput.replace(/\n|\r/g, "");
        xmlInput = xmlInput.replace(/>.*?</g, "><");
        return _parseXmlInput(xmlInput);
    }

    /*
     * Fetching config xmls for ALL calculatorIds and stores the result in
     * global variable resultlist
     */
    function _fetchConfigsForCalculators(params, flag) {
        $.ajax({
            url: "/treasury/data/search-calculator-config-data",
            data: params,
            type: "GET",
            dataType: "json",
            error: function(response, data) {
                treasury.margin.common.hideLoading("mainLoader");
                return response;
            },
            success: function(data) {
                if (data == null || data["resultList"] == undefined ||
                    data["resultList"].length == 0) {
                    allConfigsData = {};
                    treasury.margin.ssu.calcNav.renderNavigationMenuWithCalculatorsData(allConfigsData);
                } else {
                    _updateConfigData(data["resultList"]);
                    _populateCaldDataAndRenderCalcNavList();
                    if (flag == RENDER_CLONED_CONFIG){
                        treasury.margin.ssu.configEditor.renderClonedConfig();
                    } else if (flag == RESET_AND_SHOW_CONFIG){
                        treasury.margin.ssu.configEditor.showConfig();
                    }
                }
                treasury.margin.common.hideLoading("mainLoader");
            },
            timeout: 120000
        });
    }

    function _updateConfigData(configData)
    {
        for( let i = 0; i < configData.length; i++)
        {
            if(typeof configData[i]["calculator_id"] !== "undefined" && 
            typeof configData[i]["config_path"] !== "undefined" ){
                let key = configData[i]["calculator_id"] + "_" + configData[i]["config_path"];
                allConfigsData[key] = configData[i];
            }
        }
    }

    /*
     * Returns config xml for given configName
     */
    function _fetchConfigForConfigName(configName, flag) {
        let calculatorId;
        if(flag == CONFIG_UI_SET_CURRENT_CALCULATOR_CONFIG || 
            flag == SAVE_CLONED_EDITED_CONFIG_ON_CONFIG_UI){
            let methodology = document.getElementById(selectedMethodologyId);
            
            let agreementId = methodology.closest('.agreement').id;
            treasury.margin.ssu.calcNav.showChildren(agreementId);
            
            calculatorId = treasury.margin.ssu.calcNav.getCalcIdFromAgreement(agreementId,
            selectedMethodologyId, methodology.classList.contains('childMethodology'));
        } else if (flag == SAVE_CLONED_EDITED_CONFIG_ON_SIM_UI){
            calculatorId = currentCalculatorId; 
        }
        let key = calculatorId + '_' + configName;
        if (typeof calculatorId === 'undefined'){
            for (let key in allConfigsData){
                if(allConfigsData[key]['config_path'] == configName)
                    return allConfigsData[key];
            }
        } else if(typeof key !== 'undefined'){
            return allConfigsData[key];
        }
        return null;
    }

    /*
     * Get the dom root element
     */

    function _getRootElement(xmlFile) {
        var parser = new DOMParser();
        var xmlDoc = parser.parseFromString(xmlFile, "text/xml");

        var rootElement = xmlDoc.documentElement;
        currentXmlDom = xmlDoc;
        return rootElement;
    }
    /*
     * Parse the config xml
     */
    function _parseXmlInput(xmlFile) {
        var rootElement = _getRootElement(xmlFile);
        if(rootElement.childNodes[0].tagName=='parsererror'){
          console.log('parser error : ' + currentCalculatorConfig['config_path']);
          return null;
        }
        _createDummyNode(rootElement);
        return _generateTables(rootElement);
    }

    /*
     * We create a dummy table node to accommodate properties in the config
     * which do not have a corresponding parent(or table node in UI terms)
     */
    function _createDummyNode(rootElement){
      var dummyXmlDoc = document.implementation.createDocument(null, DUMMY_TABLE_NAME);
      var dummyNode = dummyXmlDoc.childNodes[0];
      var listOfTableNodes = rootElement.childNodes;
      for (var i = 0; i < listOfTableNodes.length; i++) {
        var tableNode=listOfTableNodes[i];
        if(tableNode.getAttribute('NodeType')!='List' && tableNode.childNodes.length==0){
          dummyNode.appendChild(tableNode);
          // A node can only have one parent. Therefore it moves it if you appends it to another node.
          // Adds a node to the end of the list of children of a specified parent node.
          // If the node already exists it is removed from current parent node, then added to new parent node.
          // Ref : https://stackoverflow.com/questions/12146888/why-does-appendchild-moves-a-node
          i = i-1;
        }
      }
      if(dummyNode.childNodes.length != 0)
      {
          rootElement.appendChild(dummyNode);
      }
    }

    /*
     * Generate tables for first level tags in xml
     */
    function _generateTables(rootElement) {
        var listOfTableNodes = rootElement.childNodes;
        var listOfTableNames = [];
        var tableData = {};
        for (var i = 0; i < listOfTableNodes.length; i++) {
            var tableNode=listOfTableNodes[i];
            if (tableNode.getAttribute('hidden') != null)
                next;
            if(tableNode.getAttribute('NodeType')=='List' || tableNode.childNodes.length>0){
                var tableName = tableNode.nodeName.replace(/_/g, " ");
                listOfTableNames.push(tableName);
                tableData[tableName] = _generateTableRows(tableNode);
            }
        }
        return tableData;
    }

    /*
     *
     * Parameters : XmlElement Return: data = { columnNames = [col1, col2,..],
     * dataRows = [][]}
     *
     */
    function _generateTableRows(tableNode) {
        var listTableChildren = tableNode.childNodes;
        var dataRowsPrefix = [];
        var dataRowsMatrix = {};

        for (var j = 0; j < listTableChildren.length; j++) {
            var dataRowsMatrixForANode = [];
            var tempMatrix = _traverseTree(dataRowsPrefix,
                listTableChildren[j], dataRowsMatrixForANode);
            for (var rowIdx = 0; rowIdx < tempMatrix.length; rowIdx++) {
                var rowData = tempMatrix[rowIdx];
                var id = rowData[rowData.length - 1];
                dataRowsMatrix[id] = rowData;
            }
        }
        return dataRowsMatrix;
    }
    /*
     * Traverse xml to create data rows
     */
    function _traverseTree(dataRowsPrefix, node, dataRowsMatrix) {

        if (node.childNodes.length == 0) {
            var dataRow = _generateDataRow(dataRowsPrefix, node);
            dataRowsMatrix.push(dataRow);
            return dataRowsMatrix;
        }
        var listTableChildren = node.childNodes;
        for (var j = 0; j < listTableChildren.length; j++) {
            var newColumn = node.nodeName;
            dataRowsPrefix.push(newColumn);
            _traverseTree(dataRowsPrefix, listTableChildren[j], dataRowsMatrix);
            var index = dataRowsPrefix.indexOf(newColumn);
            dataRowsPrefix.splice(index, 1);
        }
        return dataRowsMatrix;
    }
    /*
     * Generate data row using prefix columns and node values
     */
    function _generateDataRow(dataRowsPrefix, node) {
        var dataRow = [];
        for (var i = 0; i < dataRowsPrefix.length; i++) {
            dataRow.push(dataRowsPrefix[i]);
        }
        dataRow.push(node.getAttribute('display_name'));
        dataRow.push(node.getAttribute('value'));
        dataRow.push(node.getAttribute('data'));
        var dataTypeAttribute = node.getAttribute('data_type');
        if (dataTypeAttribute != null) {
            dataRow.push(dataTypeAttribute);
            dataTypeNodes.push(node.nodeName);
        } else {
            dataRow.push(node.getAttribute('value_type'));
        }
        dataRow.push(node.nodeName);
        return dataRow;
    }

    /*
     * Update the xml and send back
     */
    function _updateXML(configName, flag) {
        var calculatorConfig = _fetchConfigForConfigName(configName, flag);
        var calculatorConfigXml = calculatorConfig['config_xml'];
        var rootElement = _getRootElement(calculatorConfigXml);
        for (var rowId in editedConfigData) {
            var newRow = editedConfigData[rowId]['newRow'];
            var tableKey = editedConfigData[rowId]['tableKey'];
            var tableName = tableKey.replace(/ /g, "_");
            if (rootElement.getElementsByTagName(rowId)[0] != null) {
                _replaceNode(rootElement, rowId, newRow);
            } else {
                _addNode(rootElement, newRow, tableName, rowId);
            }
        }
        for (var rowId in deletedConfigData) {
            var deletedRow = deletedConfigData[rowId]['oldRow'];
            var tableKey = deletedConfigData[rowId]['tableKey'];
            var tableName = tableKey.replace(/ /g, "_");
            _deleteNode(rootElement, rowId);
        }
        return true;
    }

    function _checkEqual(name) {
        return name == arrValueToFind;
    }
    /*
     * Replace an existing tag with new one
     */
    function _replaceNode(rootElement, rowId, newRow) {

        var newNode = _createNewNode(rootElement, rowId, newRow);
        var oldNode = rootElement.getElementsByTagName(rowId)[0];

        if (dataTypeNodes.find(_checkEqual)) {
            oldNode.setAttribute("data_type", newNode.getAttribute("data_type"));
        } else {
            oldNode.setAttribute("value_type", newNode.getAttribute("value_type"));
        }

        oldNode.setAttribute("value", newNode.getAttribute("value"));

        oldNode.setAttribute("data", newNode.getAttribute("data"));
        oldNode.setAttribute("display_name", newNode
            .getAttribute("display_name"));
    }
    /*
     * Add a new element tag
     */
    function _addNode(rootElement, newRow, tableName, rowId) {
        var newNode = _createNewNode(rootElement, rowId, newRow);
        var tableNode = rootElement.getElementsByTagName(tableName)[0];
        var parentNode = tableNode;
        for (var i = 0; i < newRow.length; i++) {
            var tempParentNode = rootElement.getElementsByTagName(newRow[i])[0];
            if (tempParentNode != null) {
                parentNode = tempParentNode;
            } else {
                break;
            }
        }
        var siblingNodes = parentNode.childNodes;
        for (var i = 0; i < siblingNodes.length; i++) {

            if (siblingNodes[i].getAttribute('display_name') == newNode.getAttribute('display_name'))
                return false;
        }
        parentNode.appendChild(newNode);
        return true;
    }

    function _createNewNode(rootElement, rowId, newRow) {
        var rowLength = newRow.length;
        var newNode = currentXmlDom.createElement(rowId);
        arrValueToFind = rowId;

        if (dataTypeNodes.find(_checkEqual)) {
            newNode.setAttribute("data_type", newRow[rowLength - 2]);
        } else {
            newNode.setAttribute("value_type", newRow[rowLength - 2]);
        }
        if (newRow[rowLength - 3] != null) {
            newNode.setAttribute("data", newRow[rowLength - 3]);
        }
        if (newRow[rowLength - 4] != null) {
            newNode.setAttribute("value", newRow[rowLength - 4]);
        }
        newNode.setAttribute("display_name", newRow[rowLength - 5]);

        return newNode;
    }

    function _deleteNode(rootElement, rowId){
      var node = currentXmlDom.getElementsByTagName(rowId)[0];
      node.parentNode.removeChild(node);
    }

    /*
     * Serialize the parsed xml dom to xml string
     */
    function _serializeXmlDom(xmlDoc) {
        var xmlSerializer = new XMLSerializer();
        var serializedXml = xmlSerializer
            .serializeToString(xmlDoc.documentElement);
        return serializedXml;
    }

    function _postCalculatorConfigDataToDB(parameters, flag) {
        var params = {};
        params.calculatorConfigData = _createJsonObject(parameters);
        $.ajax({
            url: "/treasury/data/save-calculator-config-data",
            data: params,
            type: "POST",
            error: function(response, data) {
                treasury.margin.common.hideLoading("mainLoader");
                return response;
            },
            success: function(data) {
                treasury.margin.ssu.util.raiseSuccessToast('Update successful by user : ' + parameters.author);
                if (flag == SAVE_CLONED_EDITED_CONFIG_ON_CONFIG_UI){
                    document.getElementById('comment').value = "";
                    _refreshUIpostEditingAndCloningConfig(RESET_AND_SHOW_CONFIG);
                } else if(flag == SAVE_CLONED_EDITED_CONFIG_ON_SIM_UI){
                    treasury.margin.marginTerms.getConfigDataForConfigName(UPDATE_UI_ELEMENTS_ATTRIBUTE);
                }
                return true;
            },
            timeout: 120000
        });

    }

    function _postClonedConfigToDB(parameters, flag) {
        var params = {};
        params.calculatorConfigData = _createJsonObject(parameters);
        $.ajax({
            url: "/treasury/data/save-calculator-config-data",
            data: params,
            type: "POST",
            error: function(response, data) {
                treasury.margin.common.hideLoading("mainLoader");
                return response;
            },
            success: function(data) {
                if (flag == SAVE_AND_SET_CLONED_CONFIG_ON_SIM_UI){
                treasury.margin.ssu.util.raiseSuccessToast('Clone successful by user : ' + parameters.author +
                            " , Cloned config name : " + parameters.configName);
                    treasury.margin.marginTerms.setSelectedFilterAfterPostClonedConfigToDB();
                } else if(flag == SAVE_CLONED_CONFIG_ON_CONFIG_UI){
                    treasury.margin.ssu.util.raiseSuccessToast('Update successful by user : ' + parameters.author);
                    _refreshUIpostEditingAndCloningConfig(RENDER_CLONED_CONFIG);
                }
            },
            timeout: 120000
        });

    }
    function _refreshUIpostEditingAndCloningConfig(flag){
       treasury.margin.ssu.configEditor.resetConfig();
       let params = {
        'marginCalculatorIds': currentCalculatorConfig["calculator_id"]
        };
        treasury.margin.ssu.Actions.fetchConfigsForCalculators(params, flag); 
    }

    function _createJsonObject(params) {
        var jsonObject = {};
        jsonObject["configId"] = currentCalculatorConfig["config_id"];
        jsonObject["calculatorId"] = currentCalculatorConfig["calculator_id"];
        if(params['isCloneConfig'])
        {
            jsonObject["isCloneConfig"] = true;
            jsonObject["isSimulationConfig"] = true;
            jsonObject["configPath"] = params['configName'];
        }
        else
        {
        	jsonObject["isSimulationConfig"] = currentCalculatorConfig["is_simulation_config"];
        	jsonObject["configPath"] = currentCalculatorConfig["config_path"];
        	jsonObject["isCloneConfig"] = false;
        }

        jsonObject["configXml"] = _serializeXmlDom(currentXmlDom);
        jsonObject["comment"] = params.comment;
        jsonObject["knowledgeStartDate"] = new Date().toISOString()
            .slice(0, 19).replace('T', ' ');
        jsonObject["knowledgeEndDate"] = "null";
        jsonObject["author"] = params.author;
        return JSON.stringify(jsonObject);
    }

    function _validateConfigNameAlreadyExists()
    {
        var configName = document.getElementById('clone-config-name').value;

        let methodology = document.getElementById(selectedMethodologyId);
        
        let agreementId = methodology.closest('.agreement').id;
        treasury.margin.ssu.calcNav.showChildren(agreementId);
        
        let calculatorId = treasury.margin.ssu.calcNav.getCalcIdFromAgreement(agreementId,
            selectedMethodologyId, methodology.classList.contains('childMethodology'));
        let configList = treasury.margin.ssu.calcNav.getConfigList(calculatorId);
        for(let configType in configList){ 
            for(let i =0 ; i< configList[configType].length; i++){
                if(configList[configType][i].configName == configName){
                    return true;
                }
            }
        }
        return false;

    }

    function _validateConfigNameContainsSpecialCharacters()
    {
        var configName = document.getElementById('clone-config-name').value;
        var specialCharacters = /[!@#$%^&*()+\-=\[\]{};':"\\|,.<>\/?]+/;
        return specialCharacters.test(configName);
    }

    function _isConfigInputEmpty()
    {
        var configInput = {};
    	var message;

        var configName = document.getElementById('clone-config-name').value;
        var comment = document.getElementById('clone-config-comment').value;

        if(configName.replace(/ /g, '') == '')
        {
            configInput['isEmpty'] = true;
            configInput['message'] = 'Config Name cannot be empty';
        }
        else if(comment.replace(/ /g, '') == '')
        {
        	configInput['isEmpty'] = true;
        	configInput['message'] = 'Please enter some comment';
        }

        return configInput;
    }
})();