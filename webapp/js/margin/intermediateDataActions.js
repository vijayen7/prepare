"use strict";
var methodologySummaryGrid;
var positionMarginGrid;
var intermediateDataGrid;
var intermediateMarginDetails;
var date;
var methodologyName;
(function() {
	window.treasury = window.treasury || {};
	window.treasury.margin = window.treasury.margin || {};
	window.treasury.margin.intermediateData = window.treasury.margin.intermediateData
			|| {};
	window.treasury.margin.intermediateData.Actions = {
		loadIntermediateDataResults : _loadIntermediateDataResults,
		loadExtendedMethodologyMarginDetails : _loadExtendedMethodologyMarginDetails,
		fetchExtendedMethodDetails : _fetchExtendedMethodDetails,
		loadPositionMarginDetails : _loadPositionMarginDetails,
		loadIntermediateDetails : _loadIntermediateDetails,
		resizeAllCanvas : _resizeAllCanvas,
		downloadMarginReport : _downloadMarginReport,
		downloadCrifReport : _downloadCrifReport
	};
	/***************************************************************************
	 * Load search results for given portfolio filters
	 **************************************************************************/
	function _loadIntermediateDataResults(serializedParams) {

		var parameters = {};
		parameters.dateParam = serializedParams.dateString;
		parameters.cpeIds = serializedParams.cpeIds;
		parameters.descoEntityIds = serializedParams.legalEntityIds;
		parameters.agreementTypeIds = serializedParams.agreementTypeIds;
		parameters.reportingCurrencyId = serializedParams.reportingCurrencyId;

		if(parameters.cpeIds == -1 && parameters.agreementTypeIds == -1 &&
			 parameters.descoEntityIds == -1 &&
			 (parameters.dateParam == undefined || parameters.dateParam == ""))
			   {
			       ArcMessageHelper.showMessage('ERROR', "All-All search is not supported. Please select atleast one filter.");
						 $("#searchMessage").show();
						 return;
			   }

		$("#searchMessage").hide();
		_loadMethodologyMargin(parameters);
	}

	/***************************************************************************
	 *
	 **************************************************************************/
	function _formatNumbersToAccountingFormat(n) {
		var n = parseFloat(n).toFixed(2);
		var d = formatNumberUsingCommas(Number(Math.abs(n)).toFixed(0));
		if (n < 0) {
			return "(" + d.replace(/-/, "") + ")";
		} else {
			return d;
		}
	}
	/***************************************************************************
	 * Format number to percentage format
	 **************************************************************************/
	function _formatNumbersToPercentFormat(n) {
		var floatValue = parseFloat(n);
		var percentValue = Number(floatValue * 100).toFixed(1);
		return percentValue + " %";
	}

	/***************************************************************************
	 * This function renders the positions margin details for the selected
	 * methodology.
	 **************************************************************************/
	function _loadPositionMarginDetails(collectionName, collectionIndex, showDataInRC) {

		var data = intermediateMarginDetails
		var positions;
		// if no data
		if (typeof data["data"] == undefined) {
			positions = [];
		}
		// Check for selecting positions from intermediate buckets if required
		else if (collectionName != "position_margin"
				&& typeof collectionIndex != 'undefined') {
			var intermediateBuckets = data["data"]["intermediate_buckets"];
			positions = intermediateBuckets[collectionIndex]["positions"];
		} else {
			positions = data["data"]["positions"];
			$("#intermediateDataArea").hide();
		}
		treasury.margin.common.showLoading();
		document.getElementById('methodologMarginArea').removeAttribute('hidden');
		$("#methodologMarginArea").show();
		document.getElementById('positionMarginArea').removeAttribute('hidden');
		$("#positionMarginArea").show();

		if (positions == null || positions.length == 0) {
			$("#positionMarginGrid-header").hide();
			$("#positionMarginGrid").hide();
			document.getElementById('positionMarginAreaError').removeAttribute('hidden');
			$("#positionMarginAreaError").show();
			treasury.margin.common.hideLoading();
			return;
		}
		$("#positionMarginAreaError").hide();
		if(collectionIndex != undefined)
		{
			document.getElementById('positionMarginArea').label = "Position Margin Details for intermediate bucket - "
				+ data["data"]["intermediate_buckets"][collectionIndex]["bucket_value"];
		}
		$("#positionMarginGrid").show();

		var columns = window.treasury.margin.intermediateData.gridColumns
				.createColumnConfigurationforData(positions);
		var priority_list = [ 'pnl_spn', 'ticker', 'desname', 'short_description', 'legal_entity',
				'exposure_counter_party', 'exposure_agreement_type',
				'exposure_custodian_account', 'instrument_type', 'gbo_type',
				'price', 'quantity', 'market_value', 'fx_rate', 'book', 'ccy',
				'baseline', 'margin_native', 'margin_usd', 'margin_type',
				'underlying_value', 'usage_usd', 'state' ];
		
		if (showDataInRC) {

			let marginIndex = priority_list.indexOf('margin_usd');
			priority_list[marginIndex] = 'margin';

			let usageIndex = priority_list.indexOf('usage_usd');
			priority_list[usageIndex] = 'usage';
		}
		columns = window.treasury.margin.intermediateData.gridColumns
				.getPriorityBasedColumnList(columns, priority_list);

		for (var i = 0; i < columns.length; i++) {
			var col = columns[i];
			// Avoid PNL SPN from getting formatted as a number(comma notation)
			if (col["type"] === "number" && col["field"] !== "pnl_spn") {
				col["formatter"] = dpGrid.Formatters.Number;
				col["sortable"] = true;
			} else if (col["type"] === "float") {
				col["formatter"] = function(row, cell, value, columnDef,
					dataContext) {
						return "<div class='aln-rt'>" + (value == null 
							? "<span class='message'>n/a</span>" : Number(value).toFixed(4)) + "</div>";
					}
				col["sortable"] = true;
			} else if (col["type"] === "percent") {
				col["formatter"] = treasury.formatters.decimalToPercent;
				col["sortable"] = true;
			}
		}

		for (var i = 0; i < positions.length; i++) {
			var pos = positions[i];
			pos["id"] = pos["_id"];
		}

		var options = treasury.margin.intermediateData.gridOptions
				.getPositionMarginGridOptions();
		positionMarginGrid = new dportal.grid.createGrid(
				$("#positionMarginGrid"), positions, columns, options);

		_resizeAllCanvas();
		treasury.margin.common.hideLoading();
	}

	/***************************************************************************
	 * Renders intermediate details involved in margin computation
	 **************************************************************************/
	function _loadIntermediateDetails(methodologyName, collectionName, labelName, showDataInRC) {
		treasury.margin.common.showLoading();
		var data = intermediateMarginDetails;
		document.getElementById('methodologMarginArea').removeAttribute('hidden');
		$("#methodologMarginArea").show();
		$("#positionMarginArea").hide();
		document.getElementById('intermediateDataArea').removeAttribute(
				'hidden');
		$("#intermediateDataArea").show();
		document.getElementById('intermediateDataArea').label = "Intermediate Details for " + labelName;

		// if no data
		if (typeof data == undefined || typeof data["data"] == undefined
				|| typeof data["data"]["intermediate_buckets"] == undefined) {
			document.getElementById('intermediateDataGrid').removeAttribute(
					'hidden');
			$("#intermediateDataGrid")
					.show()
					.html(
							"<br><span class='message'>No Intermediate margin details available for methodology : "
									+ methodologyName + labelName + " .</span>");
			return;
		}
		document.getElementById('intermediateDataGrid').removeAttribute(
				'hidden');
		$("#intermediateDataGrid").show();

		var bucketsToBeRendered = [];
		for (var i = 0; i < data["data"]["intermediate_buckets"].length; i++) {
			var imd = data["data"]["intermediate_buckets"][i];
			imd["id"] = imd["_id"];
			imd["_collection_index"] = i;
			if (imd["collection_name"] == collectionName) {
				bucketsToBeRendered.push(imd);
			}
		}

		var columns = window.treasury.margin.intermediateData.gridColumns
				.createColumnConfigurationforData(bucketsToBeRendered);
		var columnPriority = [ 'bucket_type', 'bucket_value', 'rank',
				'lmv_usd', 'smv_usd', 'gmv_usd', 'nmv_usd' ];

		if (showDataInRC) {
			let lmvIndex = columnPriority.indexOf('lmv_usd');
			columnPriority[lmvIndex] = 'lmv';

			let smvIndex = columnPriority.indexOf('smv_usd');
			columnPriority[smvIndex] = 'smv';

			let gmvIndex = columnPriority.indexOf('gmv_usd');
			columnPriority[gmvIndex] = 'gmv';

			let nmvIndex = columnPriority.indexOf('nmv_usd');
			columnPriority[lmvIndex] = 'nmv';
		}

		columns = window.treasury.margin.intermediateData.gridColumns
				.getPriorityBasedColumnList(columns, columnPriority);
		for (var i = 0; i < columns.length; i++) {
			var col = columns[i];
			if (col["type"] === "number") {
				col["formatter"] = dpGrid.Formatters.Number;
				col["sortable"] = true;
			} else if (col["type"] === "float") {
				col["formatter"] = function(row, cell, value, columnDef,
					dataContext) {
						return "<div class='aln-rt'>" + (value == null 
							? "<span class='message'>n/a</span>" : Number(value).toFixed(4)) + "</div>";
					}
				col["sortable"] = true;
			} else if (col["type"] === "percent") {
				col["formatter"] = treasury.formatters.decimalToPercent;
				col["sortable"] = true;
			}

			if (col["field"] === "bucket_value") {
				col["isLink"] = true;
				col["formatter"] = function(row, cell, value, columnDef,
						dataContext) {
					var formattedCellValue = value;
					if (columnDef["type"] != undefined
							&& columnDef["type"] === "number"
							&& dataContext["bucket_type"] != "ISSUER") {
						formattedCellValue = dpGrid.Formatters.Number(row,
								cell, value, columnDef, dataContext);
					}
					var param = {
						collection : collectionName,
						id : dataContext["_collection_index"]
					};
					var paramString = JSON.stringify(param);

					return "<a class='intermediate-drill-through' onclick='treasury.margin.intermediateData.Actions.loadPositionMarginDetails(\""
							+ collectionName
							+ "\","
							+ dataContext["_collection_index"]
							+ "\","
							+ showDataInRC
							+ "\")' >"
							+ formattedCellValue + "</a>";
				}
			}
		}

		var options = treasury.margin.intermediateData.gridOptions
				.getIntermediateDetailsOptions(showDataInRC);
		intermediateDataGrid = new dportal.grid.createGrid(
				$("#intermediateDataGrid"), bucketsToBeRendered, columns,
				options);

		_resizeAllCanvas();
		 treasury.margin.common.hideLoading();
	}

	/***************************************************************************
	 * Fetches extended methodology data
	 **************************************************************************/
	function _fetchExtendedMethodDetails(params, showDataInRC) {

		var data = params["data"];
		methodologyName = params["methodologyName"];
		var level = params["level"];
		var methodologyDataId = params["methodologyDataId"];
		$("#methodologMarginArea").hide();
	  intermediateMarginDetails = data;

		if (typeof data == "undefined" || data == null
				|| typeof data["data"] == "undefined") {
			$("#methodologyErrorArea").show().html(
					"<span class='message'>No details available for methodology : "
							+ methodologyName + " .</span>");
			return;
		} else {
			$("#methodologyErrorArea").hide();
		}

		var results = data["data"];
		//Reports are downloaded only at the Methodology level (1)
		if(level == 1)
		{
			results["download_margin_report"] = "Click here";
			results["download_crif_report"] = "Click here";
		}
		document.getElementById('methodologyDetailsArea').removeAttribute(
				'hidden');
		$("#methodologyDetailsArea").show();
    document.getElementById('methodologyDetailsArea').header = methodologyName + " Summary Details";

		if (typeof results["meta_data"] !== 'undefined') {
			console.log("Methodology metadata available");
			var meth_desc_html = "" + "<blockquote cite=\""
					+ results["meta_data"]["description_url"] + "\">" + "<p>"
					+ results["meta_data"]["description"] + "</p>" + "<br/>"
					+ "<span style=\"text-align:right;\"><cite>"
					+ results["meta_data"]["name"] + "</cite></span>"
					+ "</blockquote>";
			$("#methodologyInfoArea").show();
			$("#methodologyInfo").show().html(meth_desc_html);
		} else {
			$("#methodologyInfoArea").hide();
		}
		$("#methodologyMarginSummaryArea").show();
		var marginSummaryTableHtml = ""
				+ "<table id='tableMarginSummary"
				+ "' class=\"table table--no-scroll table--text-wrap table--vertical-align--top\""
				+ " style=\"table-layout:fixed;margin-top:5px\" border=0 cellspacing=0 cellpadding=3>"
				+ "<colgroup><col width=\"50%\"><col width=\"*%\"></colgroup><tbody><tr><th colspan=\"2\">"
				+ methodologyName + "</th></tr></tbody></table>";
		$("#methodologyMarginSummary").show().html(marginSummaryTableHtml);

		var methodologyMarginData = _getConsumableMethdologyDetails(results);
		var methodologyMarginColumns = window.treasury.margin.intermediateData.gridColumns
				.createColumnConfigurationforData([ methodologyMarginData ]);
		var methodologyColumnPriority = [ 'lmv', 'smv', 'lov', 'sov', 'margin',
				'usage', 'fx_rate' ];

		methodologyMarginColumns = window.treasury.margin.intermediateData.gridColumns
				.getPriorityBasedColumnList(methodologyMarginColumns,
						methodologyColumnPriority);

		var intermediateBuckets = results["intermediate_buckets"];
		if (typeof intermediateBuckets == 'undefined') {
			intermediateBuckets = [];
		}

		_associateCollectionsToLabels(methodologyMarginColumns,
				intermediateBuckets);
		for (var i = 0; i < methodologyMarginColumns.length; i++) {
			// Get column info
			var col = methodologyMarginColumns[i];

			var row$ = $('<tr/>');
			row$.append($('<td/>').html(col["name"]));

			var key = col["field"];
			var cellValue = results[key];
			var formattedCellValue = cellValue;
			if (col["type"] === "number") {
				formattedCellValue = _formatNumbersToAccountingFormat(cellValue);
			} else if (col["type"] === "percent") {
				formattedCellValue = _formatNumbersToPercentFormat(cellValue);
			} else if (col["type"] === "float") {
				formattedCellValue = parseFloat(cellValue).toFixed(4);
			}
            if(key == "download_margin_report")
            {
				let isCrifFile = false;
				let isReportPresentOnObjectStore = _reportPresentOnObjectStore(methodologyDataId, isCrifFile);
                if(isReportPresentOnObjectStore !== "true")
                {
                    methodologyMarginColumns.splice(i,1);
                    continue;
                }
                else
                {
                    col["isLink"] = true;
                    var params = {};
                    params["methodologyDataId"] = methodologyDataId;
                    params["methodologyName"] = methodologyName;
                    formattedCellValue = '<a href=# id="report_' + methodologyDataId + '" class="icon-download-report" ' +
                        'onclick="treasury.margin.intermediateData.Actions.downloadMarginReport(' +
                         methodologyDataId + ')">Click Here</a>';
                }
			}
			else if (key == "download_crif_report") {
				let isCrifFile = true;
				let crifFilePattern = "CRIF_".concat(methodologyDataId);
				let isReportPresentOnObjectStore = _reportPresentOnObjectStore(methodologyDataId, isCrifFile);
				if (isReportPresentOnObjectStore !== "true") {
					methodologyMarginColumns.splice(i, 1);
					continue;
				}
				else {
					col["isLink"] = true;
					formattedCellValue = '<a href=# id="report_' + crifFilePattern + '" class="icon-download-report" ' +
						'onclick="treasury.margin.intermediateData.Actions.downloadCrifReport(' +
						methodologyDataId + ')">Click Here</a>';
				}
			}
            else if (col["isLink"]) {
				var param = {
					collection : col["collection"],
					methodology : methodologyName
				};
				var paramString = JSON.stringify(param);
				formattedCellValue = "<a class='methodology-drill-through' onclick='treasury.margin.intermediateData.Actions.loadPositionMarginDetails(\""
						+ col["collection"]
						+ "\","
						+ "undefined"
						+ ","
						+ showDataInRC
						+ ")' >"
						+ formattedCellValue
						+ "</a>";
			}
			
			row$.append($('<td/>').html(formattedCellValue));

			$("#tableMarginSummary").append(row$);
		}

		if (typeof results["margin_penalties"] != 'undefined') {
			console.log("Methodology penalities available");
			$("#methodologyPenaltiesArea").show();
			var marginPenaltiesTableHtml = ""
					+ "<table id='tableMarginPenalties'"
					+ " class=\"table table--no-scroll table--text-wrap table--vertical-align--top\""
					+ " style=\"table-layout:fixed;margin-top:5px\" border=0 cellspacing=0 cellpadding=3>"
					+ "<colgroup><col width=\"50%\"><col width=\"*\"></colgroup><tbody></tbody></table>";
			$("#methodologyMarginPenalties").show().html(
					marginPenaltiesTableHtml);

			var methodologyPenaltyColumns = window.treasury.margin.intermediateData.gridColumns
					.createColumnConfigurationforData([ results["margin_penalties"] ]);
			_associateCollectionsToLabels(methodologyPenaltyColumns,
					intermediateBuckets);
			for (var i = 0; i < methodologyPenaltyColumns.length; i++) {
				// Get column info
				var col = methodologyPenaltyColumns[i];

				var row$ = $('<tr/>');
				row$.append($('<td/>').html(col["name"]));

				var key = col["field"];
				var cellValue = results["margin_penalties"][key];
				var formattedCellValue = cellValue;
				if (col["type"] === "number") {
					formattedCellValue = _formatNumbersToAccountingFormat(cellValue);
				} else if (col["type"] === "percent") {
					formattedCellValue = _formatNumbersToPercentFormat(cellValue);
				} else if (col["type"] === "float") {
					formattedCellValue = parseFloat(cellValue).toFixed(4);
				}

				if (col["isLink"]) {

					formattedCellValue = "<a class='methodology-drill-through' onclick='treasury.margin.intermediateData.Actions.loadIntermediateDetails(\""
							+ methodologyName
							+ "\",\""
							+ col["collection"]
							+ "\",\""
							+ col["name"]
							+ "\",\""
							+ showDataInRC
							+ "\")' >" + formattedCellValue + "</a>";
				}
				row$.append($('<td/>').html(formattedCellValue));

				$("#tableMarginPenalties").append(row$);
			}
		} else {
			$("#methodologyPenaltiesArea").hide();
		}

		$("#methodologyMarginTags").html('');
		if (typeof intermediateBuckets != "undefined"
				&& intermediateBuckets.length > 0) {
			var displayContent$ = _getIntermediateTagsDisplayContent(
					intermediateBuckets, methodologyName, showDataInRC);
			$("#methodologyMarginTags").append(displayContent$);
			$("#methodologyMarginTagsArea").show();
		} else {
			$("#methodologyMarginTagsArea").hide();
		}
		_resizeAllCanvas();
	}
	/***************************************************************************
	 * Displays Margin tags in methodology details area
	 **************************************************************************/
	function _getIntermediateTagsDisplayContent(intermediateBuckets,
			methodologyName, showDataInRC) {
		// Added padding to prevent browser CSS from kicking in.
		var displayContent$ = $('<ul style="padding: 5px;" />');
		var collectionTypesMap = {};
		for (var i = 0; i < intermediateBuckets.length; ++i) {
			var ib = intermediateBuckets[i];
			var collectionName = ib['collection_name'];
			if (_isPrimitiveValue(collectionName)) {
				collectionTypesMap[collectionName] = 1;
			}
		}

		for ( var property in collectionTypesMap) {
			if (collectionTypesMap.hasOwnProperty(property)) {
				var param = {
					collection : property,
					methodology : methodologyName
				};
				var paramString = JSON.stringify(param);
				// ?: makes the match group non capturing
				var matchResults = property
						.match(/(?:intermediate_bucket.)([a-z_]+)/);
				var displayName;
				if (matchResults !== undefined) {
					displayName = matchResults[1];
				}
				if (_isPrimitiveValue(displayName)) {
					displayName = displayName.toUpperCase();
					var formattedCellValue = "<a class='methodology-drill-through' onclick='treasury.margin.intermediateData.Actions.loadIntermediateDetails(\""
							+ methodologyName
							+ "\",\""
							+ property
							+ "\",\""
							+ displayName
							+ "\",\""
							+ showDataInRC
							+ "\")' >"
							+ displayName + "</a>"
					displayContent$.append($('<li/>').html(formattedCellValue));
				}
			}
		}
		return displayContent$;
	}

	/***************************************************************************
	 * Get the presentable data from margin data received
	 **************************************************************************/
	function _getConsumableMethdologyDetails(data) {
		var methodologyMarginDetails = {};

		for ( var property in data) {
			var value = data[property];
			if (data.hasOwnProperty(property) && _isPrimitiveValue(value)) {
				methodologyMarginDetails[property] = value;
			}
		}
		return methodologyMarginDetails;
	}
	/***************************************************************************
	 *
	 **************************************************************************/
	function _isPrimitiveValue(value) {
		var dataType = typeof value;
		if (dataType == "undefined" || dataType == "object") {
			return false;
		} else {
			return true;
		}
	}
	/***************************************************************************
	 * Each collection is associated with label e.g. margin_usd with a positions
	 * collection
	 **************************************************************************/
	function _associateCollectionsToLabels(columns, intermediateBuckets) {
		var displayLabelToCollectionName = {};
		for (var i = 0; i < intermediateBuckets.length; ++i) {
			var ib = intermediateBuckets[i];
			var displayLabel = ib['_display_label'];
			var collectionName = ib['collection_name'];
			if (_isPrimitiveValue(displayLabel)
					&& _isPrimitiveValue(collectionName)) {
				displayLabelToCollectionName[displayLabel] = collectionName;
			}
		}

		for (var i = 0; i < columns.length; ++i) {
			var columnField = columns[i]['field'];
			var collectionName = displayLabelToCollectionName[columnField];
			if (_isPrimitiveValue(collectionName)) {
				columns[i]['isLink'] = true;
				columns[i]['collection'] = collectionName;
			}

			if (columnField == 'margin') {
				columns[i]['isLink'] = true;
				columns[i]['collection'] = 'position_margin';
			}
		}
	}
	/***************************************************************************
	 * This function would be called everytime a search is to be performed.
	 * Renders methodology summary area
	 **************************************************************************/
	function _loadMethodologyMargin(allParams) {
		// Hide other area's
		$("#methodologyDetailsArea").hide();
		$("#methodologMarginArea").hide();
		$("#methodologySummaryErrorArea").hide();
		$("#methodologySummaryArea").hide();
		date  = allParams["dateParam"];
		var showDataInRC = allParams["reportingCurrencyId"] == 1 ? true : false;

		treasury.margin.common.showLoading();
		$
				.ajax({
					url : "/treasury/data/search-methodology-margin",
					data : allParams,
					type : "GET",
					dataType : "json",
					error: function(response, data) {
						if (showDataInRC) {
							ArcMessageHelper.showMessage('ERROR', "Fx Rate is not available for the selected reporting currency.");
						} else {
							document.getElementById('methodologySummaryServerErrorArea').removeAttribute('hidden');
							$("#methodologySummaryServerErrorArea").show().html(
								"<div class=\"arc-message--critical \"><br>Search request failed with error<br/>   "
								+response.status +":"+response.statusText + "</div>");
						}
						
						treasury.margin.common.hideLoading();
						return;
					},
					success : function(data) {
						// if no data
						if (data == null || data["resultList"] == undefined
								|| data["resultList"].length == 0) {
							$("#methodologySummaryGrid").hide();
							document.getElementById('methodologySummaryErrorArea').removeAttribute(
										'hidden');
							$("#methodologySummaryErrorArea").show();
							_resizeAllCanvas();
							treasury.margin.common.hideLoading();
							return;
						}
						var i,j;
						var resultList;
						resultList = data["resultList"];

						for(i=0; i < resultList.length; i++)
                        {
                            var methodologies = resultList[i]["children"];


                        }
						document.getElementById('methodologySummaryArea').removeAttribute(
								'hidden');
						$("#methodologySummaryArea").show();
						$("#methodologySummaryGrid").show();

					
						var columns = treasury.margin.intermediateData.gridColumns
								.getMethodologySummaryColumns(showDataInRC);
						var options = treasury.margin.intermediateData.gridOptions
								.getMethodologyMarginSummaryGridOptions(allParams["reportingCurrencyId"],date);

						methodologySummaryGrid = new dportal.grid.createGrid(
								$("#methodologySummaryGrid"),
								data["resultList"], columns, options);
						if (showDataInRC) {
							ArcMessageHelper.showMessage('warning', 'Displayed margin values are in reporting currency ',
							null, null);
						}
					  treasury.margin.common.hideLoading();
					},
					timeout : 120000
				});


	}
	/***************************************************************************
	 * Fetches margin data for selected methodology. Loads methodology details
	 * sidebar
	 **************************************************************************/
	function _loadExtendedMethodologyMarginDetails(methodologyObj, reportingCurrencyId, date) {
		var inputs = new Object();
		inputs.methodologyMarginId = methodologyObj["id"];
		inputs.methodologyName = methodologyObj["name"];
		inputs.level = methodologyObj["level"];
		inputs.reportingCurrencyId = reportingCurrencyId;
		inputs.dateParam = date;
		if(methodologyObj["level"] == 1) //Methodology Level
		{
		    inputs.methodologyDataId = methodologyObj["methodologyDataId"];
		}
		$("#methodologyInfoArea").hide();
		$("#methodologyMarginSummaryArea").hide();
		$("#methodologyPenaltiesArea").hide();
		$("#methodologyMarginTagsArea").hide();
		if(methodologyObj["level"] == 0) //Agreement Level
		{
			_resizeAllCanvas();
			return;
		}
		treasury.margin.common.showLoading();
		$.ajax({
					url : "/treasury/data/search-extended-methodology-details",
					data : inputs,
					type : "GET",
					dataType : "json",
					error: function(response,data){
						treasury.margin.common.hideLoading();
						document.getElementById('methodologyDetailsArea').removeAttribute('hidden');
						$("#methodologyDetailsArea").show();
						document.getElementById('methodologyErrorArea').removeAttribute('hidden');
						$("#methodologyErrorArea").show().html(
							"<div class=\"arc-message--critical \"><br>Search request failed with error<br/>   "
								+response.status +":"+response.statusText + "</div>");
						return;
					},
					success : function(data) {
						// if no data
						if (data == null || data["data"] == undefined
								|| data["data"].length == 0) {
							document.getElementById('methodologyDetailsArea')
									.removeAttribute('hidden');
							$("#methodologyDetailsArea").show();
							document.getElementById('methodologyErrorArea')
									.removeAttribute('hidden');
							$("#methodologyErrorArea")
									.show()
									.html(
											"<div class=\"arc-message-critical \"><br><span class='message'>No data available for this search criteria.</span></div>");
							_resizeAllCanvas();
							treasury.margin.common.hideLoading();
							return;
						}
						inputs.data = data;
						let showDataInRC = reportingCurrencyId === 1 ? true : false;
						_fetchExtendedMethodDetails(inputs, showDataInRC);
						treasury.margin.common.hideLoading();
					},
					timeout : 120000
				});
	}
	/***************************************************************************
	 * When a sidebar changes it's state resize all the grids
	 **************************************************************************/
	 function _resizeAllCanvas() {
 		var methodologyDetailsArea = document.getElementById('methodologyDetailsArea');
 		var searchPortfoliosFilterSidebar = document.getElementById('searchPortfoliosFilterSidebar');

 		if (methodologyDetailsArea != null && methodologyDetailsArea.state == 'expanded') {
 			methodologyDetailsArea.style.width = '400px';
 		} else if (methodologyDetailsArea != null){
 			methodologyDetailsArea.style.width = '20px';
 		}

 		if (searchPortfoliosFilterSidebar != null && searchPortfoliosFilterSidebar.state == 'expanded') {
 			 searchPortfoliosFilterSidebar.style.width = '300px';
 		} else if (searchPortfoliosFilterSidebar != null){
 			 searchPortfoliosFilterSidebar.style.width = '20px';
 		}

		if (methodologySummaryGrid != null) {
			methodologySummaryGrid.resizeCanvas();
		}
		if (positionMarginGrid != null) {
			positionMarginGrid.resizeCanvas();
		}
		if (intermediateDataGrid != null) {
			intermediateDataGrid.resizeCanvas();
		}
	}

	function _downloadMarginReport(methodologyDataId)
    {
        var reportURI;
    	var bucketName = _getBucketName();
    	var stringDate = date.replace(/-/g, '');

    	reportURI = "obs://" + bucketName + "/margin/reports/" + stringDate + "/" + methodologyDataId;
    	window.open('downloadSimulationReport?fileName='+ methodologyName +".xls&contentType=&fileUrl="+reportURI,'_blank');
    }
	
	function _downloadCrifReport(methodologyDataId) {
		let bucketName = _getBucketName();
		let stringDate = date.replace(/-/g, '');
		let downloableFileName = 'CRIF_'.concat(methodologyName, '_', date);

		let reportURI = "obs://" + bucketName + "/treasury/margin/crif/" + stringDate + "/CRIF_" + methodologyDataId;
		window.open('downloadSimulationReport?fileName=' + downloableFileName + ".xls&contentType=&fileUrl=" + reportURI, '_blank');
	}

    function _reportPresentOnObjectStore(methodologyDataId, isCrifFile)
    {
        var bucketName = _getBucketName();
        var stringDate = date.replace(/-/g, '');
        var isReportPresentOnObjectStore;
		var fileName = "margin/reports/" + stringDate + "/" + methodologyDataId;
		if (isCrifFile) {
			fileName = "treasury/margin/crif/" + stringDate + "/CRIF_" + methodologyDataId;
		}
        var allParams = {};
        allParams["bucketName"] = bucketName;
	    allParams["reportName"] = fileName;

        $.ajax({
        		url : "/treasury/data/is-report-present-on-object-store",
        		data : allParams,
        		type : "GET",
        		dataType : "json",
        		async: false,
        		error: function(response, data) {
        			document.getElementById('methodologySummaryServerErrorArea').removeAttribute('hidden');
        				$("#methodologySummaryServerErrorArea").show().html(
        				"<div class=\"arc-message--critical \"><br>Search request failed with error<br/>   "
        					+response.status +":"+response.statusText + "</div>");
        			treasury.margin.common.hideLoading();
        		    return;
        		},
        		success : function(data) {
        			// if no data
        			if (data == null || data["result"] == undefined
        				|| data["result"].length == 0) {
            			$("#methodologySummaryGrid").hide();
        	    			document.getElementById('methodologySummaryErrorArea').removeAttribute(
        		        	'hidden');
        				$("#methodologySummaryErrorArea").show();
        				_resizeAllCanvas();
        				treasury.margin.common.hideLoading();
        				return;
        		    }
					isReportPresentOnObjectStore =  data["result"];
                },
                timeout : 120000
        });
        return isReportPresentOnObjectStore;
    }

    function _getBucketName()
    {
        var bucketName = "ia55-pod-"
        var clientName = document.getElementById("clientName").value;
        var stabilityLevel = document.getElementById("stabilityLevel").value;

        clientName = (clientName == "desco") ? "deshaw" : clientName;
        stabilityLevel = (stabilityLevel == "prod") ? "" : stabilityLevel;
        stabilityLevel = (stabilityLevel == "dev") ? "" : stabilityLevel;

        bucketName = bucketName + clientName + stabilityLevel;

        return bucketName;
    }



})();
