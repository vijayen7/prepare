"use strict";
(function() {
  window.treasury = window.treasury || {};
  window.treasury.margin = window.treasury.margin || {};
  window.treasury.margin.intermediateData = window.treasury.margin.intermediateData || {};
  window.treasury.margin.intermediateData.gridOptions = {
    getMethodologyMarginSummaryGridOptions : _getMethodologyMarginSummaryGridOptions,
    getPositionMarginGridOptions : _getPositionMarginGridOptions,
    getIntermediateDetailsOptions : _getIntermediateDetailsOptions
  };
  /****************************************************************************
   *
   ****************************************************************************/
  function _getMethodologyMarginSummaryGridOptions(reportingCurrencyId, date){
    var options = {
      nestedTable: true,
      nestedField: "name",
      expandTillLevel: -1,
      expandCollapseAll : true,
      applyFilteringOnGrid : true,
      showHeaderRow : true,
      summaryRow : true,
      displaySummaryRow:true,
      summaryRowText : "Total",
      exportToExcel : true,
      sheetName : "Methodology Margin",
      useAvailableScreenSpace: true,
      forceFitColumns  : true,
      sortList : [{columnId:"name", sortAsc:true},
    ],
    onRender : function() {
      $(document).off("click", ".treasury-drill-through");
      console.log("Methodology Summary grid rendering done");
    },
    onCellClick : function(args){
      // Hide existing position area, intermediate area and methodology area
      $("#positionMarginArea").hide();
      $("#intermediateDataArea").hide();
      $("#methodologyDetailsArea").hide();

      var methodologyObj = args.item;
      try{
        treasury.margin.intermediateData.Actions
        .loadExtendedMethodologyMarginDetails(methodologyObj, reportingCurrencyId, date);
      } catch(exception) {
        console.log("Following exception occured while loading methodology margin details ", exception);
        $("#methodologySummaryGrid").show()
        .html("<span class='message'>An error occured while loading methodology margin details "
        + ". Check browser logs for details" + " .</span>");
      }
    }
  };
  return options;
}
/****************************************************************************
 *
 ****************************************************************************/
function _getPositionMarginGridOptions(){
    var options = {
      autoHorizontalScrollBar: true,
      applyFilteringOnGrid : true,
      useAvailableScreenSpace : true,
      showHeaderRow : true,
      forceFitColumns:true,
      summaryRow : false,
      displaySummaryRow:false,
      exportToExcel : true,
      sheetName : "Position Margin Details",
      sortList : [{columnId:"pnl_spn", sortAsc:true},
    ],
    onRender : function() {
      console.log("Position margin grid rendering done");
    }
  };
  return options;
}
/****************************************************************************
 *
 ****************************************************************************/
function _getIntermediateDetailsOptions(showDataInRC){
  var options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid : true,
    showHeaderRow : true,
    forceFitColumns:true,
    useAvailableScreenSpace : true,
    summaryRow : false,
    displaySummaryRow:false,
    exportToExcel : true,
    sheetName : "Intermediate Margin Details",
    onRender : function() {
      $(".intermediate-drill-through").off("click");
      console.log("Intermediate Detail grid rendering done");
    },
    onCellClick:function(args){
        // Hide existing position area
        $("#positionMarginGrid").hide();
        var inputs = args.item;
        if (typeof inputs == undefined){
          $("#positionMarginArea").show();
          $("#positionMarginGrid")
					.show()
					.html(
							"<br><span class='message'>No Position margin details available for this methodology </span>");
        } else {
          treasury.margin.intermediateData.Actions.loadPositionMarginDetails(inputs.collection_name, inputs._collection_index, showDataInRC);
        }
      }
  };
  return options;
}
})();
