"use strict";
var cpeMultiSelect;
var legalEntityMultiSelect;
var agreementTypeMultiSelect;
var searchPortfolioFilterGroup;
var reportingCurrencySingleSelect = null;

(function() {
	window.treasury = window.treasury || {};
	window.treasury.margin = window.treasury.margin || {};
	window.treasury.margin.intermediateData = window.treasury.margin.intermediateData
			|| {};
	window.treasury.margin.intermediateData.Landing = {
		loadFilters : _loadFilters,
		resetFilters : _resetFilters,
		loadSearchResults : _loadSearchResults,
		copySearchURL : _copySearchURL
	};

	var legalEntityAllDataMap = [];
	window.addEventListener("WebComponentsReady", _initialize);
	/****************************************************************************
	 *
	 ****************************************************************************/
	function _initialize() {
		$(document).on({
			ajaxStart : function() {
				treasury.margin.common.showLoading();
			},
			ajaxStop : function() {
				treasury.margin.common.hideLoading();
			}
		});

	var saveSettings = document.querySelector('arc-save-settings');
    var host = '';
    var clientName = document.getElementById("clientName").value;
    var stabilityLevel = document.getElementById("stabilityLevel").value;

    if(clientName === 'desco') {
        if(stabilityLevel == 'prod') {
            host += "http://landing-app.deshaw.c.ia55.net";
        } else if(stabilityLevel == 'uat'){
            host = "http://landing-app.deshawuat.c.ia55.net";
        } else if(stabilityLevel == 'qa'){
            host = "https://mars.arcesium.com";
        } else if(stabilityLevel == 'dev'){
            host = "https://terra.arcesium.com";
        }
    }
    saveSettings.serviceURL = host+'/service/SettingsService';
    saveSettings.applicationId = 3;
    saveSettings.applicationCategory = 'DealReport';

    document.getElementById('summary-search-save').onclick = function(){
              saveSettings.openSaveDialog();
    };

    saveSettings.saveSettingsCallback = function(){

       var parameters = searchPortfolioFilterGroup.getSerializedParameterMap();
       return parameters;
    }
    saveSettings.applySettingsCallback = function(parameters){
		_setFilters(parameters);
    }

	_registerHandlers();
	_loadFilters();

	}
	/****************************************************************************
	 *
	 ****************************************************************************/
	function _registerHandlers() {
		// event listener for security search
		document.getElementById('searchId').onclick = treasury.margin.intermediateData.Landing.loadSearchResults;
		document.getElementById('clearSearchPosBtn').onclick = treasury.margin.intermediateData.Landing.resetFilters;
		document.getElementById('copySearchURL').onclick = treasury.margin.intermediateData.Landing.copySearchURL;
    document.getElementById('methodologyDetailsArea').addEventListener("stateChange", treasury.margin.intermediateData.Actions.resizeAllCanvas);
    document.getElementById('searchPortfoliosFilterSidebar').addEventListener("stateChange", treasury.margin.intermediateData.Actions.resizeAllCanvas);
	}
	/****************************************************************************
	 *
	 ****************************************************************************/
	function _loadFilters() {

		$.when(treasury.loadfilter.legalEntities(),
						treasury.loadfilter.cpes(),
						treasury.loadfilter.loadAgreementTypes(),
						treasury.loadfilter.defaultDates())
				.done(
						function(legalEntitiesData, cpesData,
								agreementTypesData, defaultDatesData) {
							
							var currenciesData = [[0, "USD"], [1, "Reporting Currency"]];

							_setupDateFilter(defaultDatesData[0].tMinusOneFilterDate);

							// Initializing Legal Entity Filter
							legalEntityMultiSelect = new window.treasury.filter.MultiSelectNew(
									"legalEntityIds", "legalEntityFilter",
									_decodeString(legalEntitiesData[0].descoEntities), treasury.defaults.legalEntities);

							var legalEntityFilter = document
									.getElementById("legalEntityFilter");
							var legalEntityFilterData = legalEntityFilter.data;
							for (var i = 0; i < legalEntityFilterData.length; i++) {
								legalEntityAllDataMap[legalEntityFilterData[i].key] = legalEntityFilterData[i];
							}

							// Initializing cpe filter
							cpeMultiSelect = new window.treasury.filter.MultiSelectNew(
									"cpeIds", "cpeFilter", _decodeString(cpesData[0].cpes), treasury.defaults.counterPartyEntities);

							// Initializing agreement type filter
							agreementTypeMultiSelect = new window.treasury.filter.MultiSelectNew(
									"agreementTypeIds", "agreementTypeFilter",
									agreementTypesData[0].agreementTypes, treasury.defaults.agreementTypes);

						    // Initializing currency type filter
							// removing all currencies
							if (treasury.defaults.enableReportingCurrencyFilter) {
								reportingCurrencySingleSelect = new window.treasury.filter.SingleSelect(
								"reportingCurrencyId", "reportingCurrencyFilter",
								currenciesData, treasury.defaults.reportingCurrencies);
							}

							//Getting Parameter Map
							if (reportingCurrencySingleSelect != null) {
								searchPortfolioFilterGroup = new treasury.filter.FilterGroup(
									[ legalEntityMultiSelect, cpeMultiSelect,
											agreementTypeMultiSelect],[reportingCurrencySingleSelect],
									$("#datePicker"), null);
							} else {
								searchPortfolioFilterGroup = new treasury.filter.FilterGroup(
									[ legalEntityMultiSelect, cpeMultiSelect,
											agreementTypeMultiSelect],null,
									$("#datePicker"), null);
							}
						    var saveSettings = document.querySelector('arc-save-settings');
								saveSettings.retrieveSettings();
                _loadCopyUrlData();
						})
	}
	/****************************************************************************
	 *
	 ****************************************************************************/
	 function _loadSearchResults() {
		 if (new Date(document.getElementById('datePicker').value) == '') {
 			ArcMessageHelper.showMessage('warning', 'Please select date.',
 					null, null);
 			return;
 		}

		if (treasury.defaults.enableReportingCurrencyFilter &&
			document.getElementById("reportingCurrencyFilter").value === null) {
			ArcMessageHelper.showMessage('warning', 'Please select all the required fields',
 					null, null);
 			return;
		}

		var serializedParams = searchPortfolioFilterGroup.getSerializedParameterMap();
		treasury.margin.intermediateData.Actions.loadIntermediateDataResults(serializedParams);
	 }
	/****************************************************************************
	 *
	 ****************************************************************************/
	function _validate(param) {
		//should be a whole number
		return ((param == undefined || param == "" || param == null
				|| isNaN(param) || (param % 1) != 0) ? false : true);
	}
	/****************************************************************************
	 * This function decodes special characters like &amp; &agrave;
	 ****************************************************************************/
	 function _decodeString(entityList) {
		 for(var index = 0; index < entityList.length; index++){
			 var temp = jQuery.parseHTML(entityList[index][1]);
			 if(temp == null) continue;
			 entityList[index][1] = temp[0].data;
		 }
		 return entityList;
	 }
	/****************************************************************************
	 *
	 ****************************************************************************/
	function _setFilters(parameters) {
		var legalEntityIds = parameters["legalEntityIds"];
		var cpeIds = parameters["cpeIds"];
		var agreementTypeIds = parameters["agreementTypeIds"];
		var reportingCurrencyId = parameters["reportingCurrencyId"];
		var dateString = parameters["dateString"];

		if (legalEntityIds != undefined) {
			legalEntityIds = legalEntityIds.split(',');
			legalEntityMultiSelect.setSelectedNodes(legalEntityIds);
		}
		if (cpeIds != undefined) {
			cpeIds = cpeIds.split(',');
			cpeMultiSelect.setSelectedNodes(cpeIds);
		}
		if (agreementTypeIds != undefined) {
			agreementTypeIds = agreementTypeIds.split(',');
			agreementTypeMultiSelect.setSelectedNodes(agreementTypeIds);
		}
		if (reportingCurrencyId != undefined) {
			reportingCurrencySingleSelect.setSelectedNode(reportingCurrencyId);
		}
		if(dateString != undefined){
			_setupDateFilter(dateString);
		}
	}
	/****************************************************************************
	 *
	 ****************************************************************************/
	function _copySearchURL() {
        treasury.margin.common.showLoading();
    	var url = "/treasury/margin/intermediate-data";
    	var parameters = searchPortfolioFilterGroup.getSerializedParameterMap();
		  parameters["isCopyUrl"] = true;
    	generateReportURL(url, parameters, "resultList");

    	treasury.margin.common.hideLoading();
    }
	/****************************************************************************
	 *
	 ****************************************************************************/
	 function _setupDateFilter(dateString) {
    	var dateFilterValue = Date.parse(treasury.defaults.date || dateString);
		var minDate = Date.parse('1970-01-01');
		var maxDate =  new Date();
		var dateFormat = 'yy-mm-dd';
		treasury.margin.util.setupDateFilter(
				$("#datePicker"), $("#datePickerError"),
				dateFilterValue, minDate, maxDate, dateFormat);
    }
	/****************************************************************************
	 *
	 ****************************************************************************/
	function _resetFilters() {
		legalEntityMultiSelect.setSelectedNodes([]);
		cpeMultiSelect.setSelectedNodes([]);
		agreementTypeMultiSelect.setSelectedNodes([]);
		reportingCurrencySingleSelect.setSelectedNode();
		_setupDateFilter();

	}
	/****************************************************************************
 *
 ****************************************************************************/
	function _loadCopyUrlData()
    {
    	if(window.location.href.indexOf("isCopyUrl=true") >= 0) {
    		if (document.getElementById('searchPortfoliosFilterSidebar').state == 'expanded') {
    			document.getElementById('searchPortfoliosFilterSidebar').toggle();
    	}

    	treasury.margin.common.showLoading();

      var urlParams = window.location.href.split("?");
      if(urlParams[1] != undefined) {
        			var paramList = urlParams[1].split("&");
      }
      var searchData = {};
      for(var i=0; i < paramList.length ; i++) {
        			var param = paramList[i].split("=");
        			var val = param[1];
        			if(val != null && param[0] != "isCopyUrl") {
        				searchData[param[0]]=val;
        			}
			}
			_setFilters(searchData);
			treasury.margin.intermediateData.Actions.loadIntermediateDataResults(searchData);
      treasury.margin.common.hideLoading();
    }
    else {
    	document.getElementById('searchMessage').removeAttribute("hidden");
    }

    }

})();