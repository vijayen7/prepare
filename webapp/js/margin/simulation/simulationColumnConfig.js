"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.margin = window.treasury.margin || {};
    window.treasury.margin.simulationColumnConfig = {
        getSearchPositionsColumns : _getSearchPositionsColumns,
        getSearchPositionsColumnsForCashMargin : _getSearchPositionsColumnsForCashMargin,
        getSelectedPositionsColumns : _getSelectedPositionsColumns,
        getSelectedPositionsColumnsForCashMargin : _getSelectedPositionsColumnsForCashMargin,
        getMarginTermsColumns : _getMarginTermsColumns,
        getSimResultsColumns : _getSimResultsColumns,
        getSimResultsColumnsForCashMargin : _getSimResultsColumnsForCashMargin,
        getCalculatorLevelBreakUpColumns : _getCalculatorLevelBreakUpColumns
    };

    function _getSearchPositionsColumns(showDataInRC) {
         const RC_ID_OR_FIELD_SUFFIX = "RC";
         const EMPTY_STRING = "";
    	return [{
    		id : "pnl_spn",
            name : "PNL SPN",
            field : "pnl_spn",
            toolTip : "PNL SPN",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            minWidth : 30
	    },
	    {
	        id : "desname",
	        name : "Description",
	        field : "desname",
	        toolTip : "Description",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 120
        },
        {
            id: "shortDescription",
            name: "Short Description",
            field: "shortDescription",
            toolTip: "Short Description",
            type: "text",
            filter: true,
            sortable: true,
            headerCssClass: "b",
            minWidth: 30
        },
	    {
	        id : "gboType",
	        name : "Asset Class",
	        field : "gboType",
	        toolTip : "Asset Class",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 100
	    },
	    {
	        id : "book",
	        name : "Book",
	        field : "book",
	        toolTip : "Book",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 100
	    },
	    {
	        id : "originalQuantity",
	        name : "Quantity",
	        field : "originalQuantity",
	        toolTip : "Quantity",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 70
	    },
	    {
	        id : "originalPrice"+ (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
	        name : "Price",
	        field : "originalPrice"+ (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
	        type : "number",
	        toolTip : "Price",
	        filter : true,
	        sortable : true,
	        formatter : treasury.formatters.price,
	        headerCssClass : "b",
	        minWidth : 30
	    },
	    {
	        id : "marketValue"+ (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
	        name : "Market Value",
	        field : "marketValue"+ (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
	        toolTip : "Market Value",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 60
	    },
	    {
	        id : "margin"+ (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
	        name : "Margin",
	        field : "margin"+ (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
	        toolTip : "Margin",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        comparator : function(a, b) { return treasury.comparators.number(a[sortcol], b[sortcol]); },
	        minWidth : 50
	    },
	    {
	        id : "calculator",
	        name : "Calculator",
	        field : "calculator",
	        toolTip : "Calculator",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 70
	    },
	    {
	        id : "agreement",
	        name : "Agreement",
	        field : "agreement",
	        toolTip : "Agreement",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 150
	    },
	    {
	        id : "quantity",
	        name : "Simulated Quantity",
	        field : "quantity",
	        toolTip : "Simulated Quantity",
	        type : "number",
	        filter : true,
	        sortable : true,
	        editor : dpGrid.Editors.Integer,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 90
	    },
	    {
	        id : "Price",
	        name : "Simulated Price",
	        field : "price"+ (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
	        toolTip : "Simulated Price",
	        type : "number",
	        filter : true,
	        sortable : true,
	        editor : dpGrid.Editors.Float ,
	        formatter : treasury.formatters.price,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 90
	    }];
    }
    function _getSearchPositionsColumnsForCashMargin() {
    	return [{
    		id : "pnl_spn",
            name : "PNL SPN",
            field : "pnl_spn",
            toolTip : "PNL SPN",
            type : "text",
            filter : true,
            sortable : true,
            headerCssClass : "b",
            minWidth : 30
	    },
	    {
	        id : "desname",
	        name : "Description",
	        field : "desname",
	        toolTip : "Description",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 120
	    },
	    {
	        id : "gboType",
	        name : "Asset Class",
	        field : "gboType",
	        toolTip : "Asset Class",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 100
	    },
	    {
	        id : "book",
	        name : "Book",
	        field : "book",
	        toolTip : "Book",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 100
	    },
	    {
	        id : "originalQuantity",
	        name : "Quantity",
	        field : "originalQuantity",
	        toolTip : "Quantity",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 70
	    },
	    {
	        id : "originalPrice",
	        name : "Price",
	        field : "originalPrice",
	        type : "number",
	        toolTip : "Price",
	        filter : true,
	        sortable : true,
	        formatter : treasury.formatters.price,
	        headerCssClass : "b",
	        minWidth : 30
	    },
	    {
	        id : "marketValue",
	        name : "Market Value",
	        field : "marketValue",
	        toolTip : "Market Value",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 60
	    },
	    {
	        id : "margin",
	        name : "Margin",
	        field : "margin",
	        toolTip : "Margin",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        comparator : function(a, b) { return treasury.comparators.number(a[sortcol], b[sortcol]); },
	        minWidth : 50
	    },
	    {
	        id : "cash_quantity",
	        name : "UnFinanced Quantity",
	        field : "cash_quantity",
	        toolTip : "UnFinanced Quantity",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 50
	    },
	    {
	        id : "financed_margin_usd",
	        name : "Financed Margin",
	        field : "financed_margin_usd",
	        toolTip : "Financed Margin",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 50
	    },
	    {
	        id : "cash_margin_usd",
	        name : "UnFinanced Margin",
	        field : "cash_margin_usd",
	        toolTip : "UnFinanced Margin",
	        type : "number",
	        filter : true,
	        sortable : true,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 50
	    },
	    {
	        id : "calculator",
	        name : "Calculator",
	        field : "calculator",
	        toolTip : "Calculator",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 70
	    },
	    {
	        id : "agreement",
	        name : "Agreement",
	        field : "agreement",
	        toolTip : "Agreement",
	        type : "text",
	        filter : true,
	        sortable : true,
	        headerCssClass : "b",
	        minWidth : 150
	    },
	    {
	        id : "financed_quantity",
	        name : "Simulated Quantity",
	        field : "financed_quantity",
	        toolTip : "Simulated Quantity",
	        type : "number",
	        filter : true,
	        sortable : true,
	        editor : dpGrid.Editors.Integer,
	        formatter : dpGrid.Formatters.Number,
	        aggregator : dpGrid.Aggregators.sum,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 90
	    },
	    {
	        id : "Price",
	        name : "Simulated Price",
	        field : "price",
	        toolTip : "Simulated Price",
	        type : "number",
	        filter : true,
	        sortable : true,
	        editor : dpGrid.Editors.Float ,
	        formatter : treasury.formatters.price,
	        headerCssClass : "aln-rt b",
	        excelFormatter : "#,##0",
	        minWidth : 90
	    }];
    }

    function _getSelectedPositionsColumns() {
      return _getSearchPositionsColumns();
    }

    function _getSelectedPositionsColumnsForCashMargin() {
        return _getSearchPositionsColumnsForCashMargin();
      }

    function _getMarginTermsColumns() {
      var columns = [
          {
              id : "legal_entity",
              name : "Legal Entity",
              field : "legal_entity",
              type : "text",
              headerCssClass : "b",
              minWidth : 150
          },
          {
              id : "cpe",
              name : "Counterparty Entity",
              field : "cpe",
              type : "text",
              headerCssClass : "b",
              minWidth : 130
          },
          {
              id : "agreement_type",
              name : "Agreement Type",
              field : "agreement_type",
              type : "text",
              headerCssClass : "b",
              minWidth : 100
          },
          {
            id : "margin_type",
            name : "Margin Type",
            field : "margin_type",
            type : "text",
            headerCssClass : "b",
            minWidth : 100
          },
          {
              id : "calculator",
              name : "Calculator",
              field : "calculator",
              type : "text",
              headerCssClass : "b",
              minWidth : 70
          },
          {
              id : "includeExistingPortfolio",
              name : "Include Existing Portfolio?",
              field : "includeExistingPortfolio",
              type : "text",
              formatter : dpGrid.Formatters.YesNo,
              headerCssClass : "b",
              minWidth : 150
          }
      ];
      return columns;
    }

    function _getSimResultsColumns(showDataInRC){
        const RC_ID_OR_FIELD_SUFFIX = "RC";
        const EMPTY_STRING = "";
        var columns = [
                  {
                      id : "legal_entity",
                      name : "Legal Entity",
                      field : "legal_entity",
                      type : "text",
                      headerCssClass : "b",
                      minWidth : 150
                  },
                  {
                      id : "cpe",
                      name : "Counterparty Entity",
                      field : "cpe",
                      type : "text",
                      headerCssClass : "b",
                      minWidth : 130
                  },
                  {
                      id : "agreement_type",
                      name : "Agreement Type",
                      field : "agreement_type",
                      type : "text",
                      headerCssClass : "b",
                      minWidth : 100
                  },
                  {
                      id : "calculator",
                      name : "Calculator",
                      field : "calculator",
                      type : "text",
                      headerCssClass : "b",
                      minWidth : 70
                  },
                  {
                      id : "config",
                      name : "Configuration",
                      field : "config",
                      type : "text",
                      headerCssClass : "b",
                      minWidth : 100
                  },
                  {
                      id : "includeExistingPortfolio",
                      name : "Include Existing Portfolio?",
                      field : "includeExistingPortfolio",
                      type : "text",
                      formatter : dpGrid.Formatters.YesNo,
                      headerCssClass : "b",
                      minWidth : 150
                  },
                  {
                      id : "margin" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
                      name : "Margin",
                      field : "margin" + (showDataInRC ? RC_ID_OR_FIELD_SUFFIX : EMPTY_STRING),
                      type : "number",
                      formatter : function(row, cell, margin, columnDef,
                              dataContext) {
                          var marginFormattedValue = dpGrid.Formatters.Number(
                                  row, cell, margin, columnDef, dataContext);
                          var param = {
                              index : row,
                              methodology : dataContext.calculator
                          };
                          var paramString = JSON.stringify(param);
                          return "<a class='margin-drill-through' onclick='treasury.margin.runSimulation.loadMarginSummaryDetails("
              							+ paramString
                            + ")'>"
              							+ marginFormattedValue + "</a>";
                      },
                      sortable : true,
                      minWidth : 150,
                      toolTip : "Financed Margin",
                      headerCssClass : "b",
                      excelFormatter : "#,##0"
                  },
                  {
                      id : "successfulPositionCount",
                      name : "Successful Positions",
                      field : "successfulPositionCount",
                      type : "number",
                      formatter : dpGrid.Formatters.Number,
                      sortable : true,
                      toolTip : "Total number of successful positions",
                      headerCssClass : "b",
                      excelFormatter : "#,##0",
                      minWidth : 150
                  }, {
                      id : "skippedPositionCount",
                      name : "Skipped Positions",
                      field : "skippedPositionCount",
                      type : "number",
                      formatter : dpGrid.Formatters.Number,
                      sortable : true,
                      toolTip : "Total number of skipped positions",
                      headerCssClass : "b",
                      excelFormatter : "#,##0",
                      minWidth : 150
                  }, {
                      id : "failedPositionCount",
                      name : "Failed Positions",
                      field : "failedPositionCount",
                      type : "number",
                      formatter : dpGrid.Formatters.Number,
                      sortable : true,
                      toolTip : "Total number of failed positions",
                      headerCssClass : "b",
                      excelFormatter : "#,##0",
                      minWidth : 150
                  }, {
                      id : "error",
                      name : "Status",
                      field : "error",
                      type : "text",
                      formatter : treasury.formatters.status,
                      toolTip : "Calculation status",
                      headerCssClass : "b",
                      excelFormatter : "#,##0",
                      minWidth : 120
                  },
                  {
                      id : "reportPath",
                      name : "Download Margin Report",
                      field : "reportPath",
                      type : "text",
                      formatter : function(row, cell, value, columnDef,
                              dataContext) {
                          return '<a href="#" id="report_' + row + '" class="icon-download-report" ' +
                              'onclick="treasury.margin.runSimulation.downloadMarginReport(' + row + ')">Click Here<\a>';
                      },
                      toolTip : "Download Margin Report",
                      headerCssClass : "b",
                      excelFormatter : "#,##0",
                      minWidth : 150
                  }];

          return columns;
      }

    function _getSimResultsColumnsForCashMargin(){
      var columns = [
                {
                    id : "legal_entity",
                    name : "Legal Entity",
                    field : "legal_entity",
                    type : "text",
                    headerCssClass : "b",
                    minWidth : 150
                },
                {
                    id : "cpe",
                    name : "Counterparty Entity",
                    field : "cpe",
                    type : "text",
                    headerCssClass : "b",
                    minWidth : 130
                },
                {
                    id : "agreement_type",
                    name : "Agreement Type",
                    field : "agreement_type",
                    type : "text",
                    headerCssClass : "b",
                    minWidth : 100
                },
                {
                    id : "calculator",
                    name : "Calculator",
                    field : "calculator",
                    type : "text",
                    headerCssClass : "b",
                    minWidth : 70
                },
                {
                    id : "config",
                    name : "Configuration",
                    field : "config",
                    type : "text",
                    headerCssClass : "b",
                    minWidth : 100
                },
                {
                    id : "includeExistingPortfolio",
                    name : "Include Existing Portfolio?",
                    field : "includeExistingPortfolio",
                    type : "text",
                    formatter : dpGrid.Formatters.YesNo,
                    headerCssClass : "b",
                    minWidth : 150
                },
                {
                    id : "financedMargin",
                    name : "Financed Margin",
                    field : "financedMargin",
                    type : "number",
                    formatter : function(row, cell, margin, columnDef,
                            dataContext) {
                        var marginFormattedValue = dpGrid.Formatters.Number(
                                row, cell, margin, columnDef, dataContext);
                        var param = {
                            index : row,
                            methodology : dataContext.calculator
                        };
                        var paramString = JSON.stringify(param);
                        return "<a class='margin-drill-through' onclick='treasury.margin.runSimulation.loadMarginSummaryDetails("
            							+ paramString
                          + ")'>"
            							+ marginFormattedValue + "</a>";
                    },
                    sortable : true,
                    minWidth : 150,
                    toolTip : "Financed Margin",
                    headerCssClass : "b",
                    excelFormatter : "#,##0"
                },
                {
                	 id : "cashMargin",
                     name : "UnFinanced Margin",
                     field : "cashMargin",
                     type : "number",
                     formatter : dpGrid.Formatters.Number,
                     sortable : true,
                     toolTip : "UnFinanced Margin",
                     headerCssClass : "b",
                     excelFormatter : "#,##0",
                     minWidth : 150
                },{
               	 id : "margin",
                 name : "Total Margin",
                 field : "margin",
                 type : "number",
                 formatter : dpGrid.Formatters.Number,
                 sortable : true,
                 toolTip : "Total Margin",
                 headerCssClass : "b",
                 excelFormatter : "#,##0",
                 minWidth : 150
                }
                ,{
                    id : "successfulPositionCount",
                    name : "Successful Positions",
                    field : "successfulPositionCount",
                    type : "number",
                    formatter : dpGrid.Formatters.Number,
                    sortable : true,
                    toolTip : "Total number of successful positions",
                    headerCssClass : "b",
                    excelFormatter : "#,##0",
                    minWidth : 150
                }, {
                    id : "skippedPositionCount",
                    name : "Skipped Positions",
                    field : "skippedPositionCount",
                    type : "number",
                    formatter : dpGrid.Formatters.Number,
                    sortable : true,
                    toolTip : "Total number of skipped positions",
                    headerCssClass : "b",
                    excelFormatter : "#,##0",
                    minWidth : 150
                }, {
                    id : "failedPositionCount",
                    name : "Failed Positions",
                    field : "failedPositionCount",
                    type : "number",
                    formatter : dpGrid.Formatters.Number,
                    sortable : true,
                    toolTip : "Total number of failed positions",
                    headerCssClass : "b",
                    excelFormatter : "#,##0",
                    minWidth : 150
                }, {
                    id : "error",
                    name : "Status",
                    field : "error",
                    type : "text",
                    formatter : treasury.formatters.status,
                    toolTip : "Calculation status",
                    headerCssClass : "b",
                    excelFormatter : "#,##0",
                    minWidth : 120
                },
                {
                    id : "reportPath",
                    name : "Download Margin Report",
                    field : "reportPath",
                    type : "text",
                    formatter : function(row, cell, value, columnDef,
                            dataContext) {
                        return '<a href="#" id="report_' + row + '" class="icon-download-report" ' +
                            'onclick="treasury.margin.runSimulation.downloadMarginReport(' + row + ')">Click Here<\a>';
                    },
                    toolTip : "Download Margin Report",
                    headerCssClass : "b",
                    excelFormatter : "#,##0",
                    minWidth : 150
                }];

        return columns;
    }

    function _getCalculatorLevelBreakUpColumns(calculatorData, showDataInRC) {
        const RC_ID_OR_FIELD_SUFFIX = "RC";
        const EMPTY_STRING = "";
        const USD_SUFFIX = " USD";
        const USD_ID_SUFFIX = "_usd";
        var columns = [
            {
                id : "name",
                name : "Calculator",
                field : "name",
                toolTip : "Calculator",
                type : "text",
                filter : true,
                sortable : true,
                headerCssClass : "b",
                minWidth : 140,
                formatter : function(row, cell, value, columnDef, dataContext) {
                        return treasury.formatters.drillThrough(row,
                            cell,
                            value,
                            columnDef,
                            dataContext,
                            undefined,
                            function(row, cell, value, columnDef, dataContext) {
                                return typeof(dataContext["children"]) === "undefined";
                            },
                            JSON.stringify(dataContext));
                }
            },
            {
                id : "lmv",
                name : "LMV" + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
                field : "lmv",
                toolTip : "LMV" + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
                type : "number",
                filter : true,
                sortable : true,
                formatter : dpGrid.Formatters.Number,
                aggregator : dpGrid.Aggregators.sum,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0.0",
                minWidth : 90
            },
            {
                id : "smv",
                name : "SMV" + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
                field : "smv",
                toolTip : "SMV" + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
                type : "number",
                filter : true,
                sortable : true,
                formatter : dpGrid.Formatters.Number,
                aggregator : dpGrid.Aggregators.sum,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0.0",
                minWidth : 90
            },
            {
                id : "margin",
                name : "Margin" + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
                field : "margin",
                toolTip : "Margin" + (showDataInRC ? EMPTY_STRING : USD_SUFFIX),
                type : "number",
                filter : true,
                sortable : true,
                formatter : dpGrid.Formatters.Number,
                aggregator : dpGrid.Aggregators.sum,
                headerCssClass : "aln-rt b",
                excelFormatter : "#,##0.0",
                minWidth : 90
            }
        ];

        return columns;
    };
})();
