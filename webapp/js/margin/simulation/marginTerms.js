"use strict";

var mode = "add"; //used to maintain the state of filters. "Add" by default.
var row; //the row which is being edited/deleted saved at any time
var allValidMarginTerms=[];
var filterMarginTermsData = {};
var marginTermsData = {};
var filteredMarginTerms = [];
var addSingleSelectMap = {};
var editSingleSelectMap = {};
var marginTermsAvailableFlag = true;
var dummyFilterGroup;
var addMarginTermsFilterGroup;
var editMarginTermsFilterGroup;
var configData;
var allConfigsData;
var allConfigNames = {};
var currentCalculatorConfig;
var selectedFilterValuesMap = {};
var cloneConfigFiltersMap = {};
var selectedConfigName;
var selectedConfigFromMarginTerm;
var productionConfigForCalculator = {};
var currentCalculatorConfigForAddedMarginTerm;
var isRowEditable;
var isEditMarginTerm = false;
var currentCalculatorId;
var selectedCalculatorFromMarginTerm;
var inputParameters = {};
var success_status = 200;
var status_files_not_present = 210;

(function() {
  window.treasury = window.treasury || {};
	window.treasury.margin = window.treasury.margin || {};
	window.treasury.margin.marginTerms = {
    loadFilters : _loadFilters,
    clearMarginTermsFilter: _clearMarginTermsFilter,
    registerHandlers : _registerHandlers,
    addMarginTerm: _addMarginTerm,
    editMarginTerm : _editMarginTerm,
    acceptEdit : _acceptEdit,
    rejectEdit : _rejectEdit,
    deleteMarginTerm : _deleteMarginTerm,
    setModeAndAdd : _setModeAndAdd,
    marginTermsAvailable : _marginTermsAvailable,
    viewConfig : _viewConfig,
    cloneConfig : _cloneConfig,
    editConfig : _editConfig,
    setSelectedFilterAfterPostClonedConfigToDB : _setSelectedFilterAfterPostClonedConfigToDB,
    getConfigDataForConfigName : _getConfigDataForConfigName
	};

	var legalEntityAllDataMap = [];

  function _registerHandlers() {
		document.getElementById('addMarginTermsBtn').onclick = treasury.margin.marginTerms.setModeAndAdd;
    document.getElementById('refreshMarginTermsBtn').onclick = treasury.margin.marginTerms.clearMarginTermsFilter;
    document.getElementById('legalEntityMarginTermsFilter').onchange =
            function() {_filterMarginTermsFilter('legal_entity_id', "legalEntity")};
    document.getElementById('cpeMarginTermsFilter').onchange =
            function() {_filterMarginTermsFilter('cpe_id', "cpe")};
    document.getElementById('agreementTypeMarginTermsFilter').onchange =
            function() {_filterMarginTermsFilter('agreement_type_id', "agreementType")};
    document.getElementById('marginTypeMarginTermsFilter').onchange =
            function() {_filterMarginTermsFilter('margin_type_id', "marginType")};
    document.getElementById('calculatorMarginTermsFilter').onchange =
            function() {_filterConfigBasedOnCalculator('calculator_id', "calculator")};
    document.getElementById('configMarginTermsFilter').onchange =
            function() {_setSelectedConfigAndGetConfigData('config', "config")};
    document.getElementById('viewConfigBtn').onclick = function(event) {_viewConfig(event)};
    document.getElementById('cloneConfigBtn').onclick = function() {_cloneConfig()};
    document.getElementById('editConfigBtn').onclick = function() {_editConfig()};
	}

  function _loadFilters(){
    var serviceCall = $.ajax({
	    url: '/treasury/fetch-margin-terms',
	    type: "POST",
	  });
    $.when(serviceCall)
       .done(
         function(data) {
           allValidMarginTerms = data["resultList"];
      	   if((typeof allValidMarginTerms != "undefined") && allValidMarginTerms.length > 0){
                 _loadAndInitializeMarginTermsFilters();
      	   }
           else{
             marginTermsAvailableFlag = false;
           }
         });

     serviceCall
         .fail(function(xhr, text, errorThrown) {
           treasury.margin.common.hideLoading("mainLoader");
           treasury.margin.util
             .showErrorMessage( 'Error occurred while retrieving margin terms.');
     });
  }
  /*********************************************************************************
   * This method loads current state of span.
   ********************************************************************************/

  function _loadSpanState(parameters){
    treasury.margin.util.showSpanLoading("Loading state of span");
    var serviceCall = $.ajax({
      url: '/treasury/service/spanService/getSpanState',
      type: 'POST',
      data : {format : 'json'},
      dataType: 'json'
    });

    $.when(serviceCall)
       .done(
          function(data) {
            var spanStateDetails = {};
            if (typeof data['response'] !== 'undefined' && data['response'] === success_status) {
              if (data["message"].includes(parameters.date)) {
                _addCalculatorToTable(inputParameters);
                ArcMessageHelper.showMessage("WARNING", data['message']);
              } else {
                spanStateDetails['message'] = data["message"] +
                ". If you want to reset span for date: " +parameters.date +", please click Reload Span.\n";
                document.getElementById('spanStateMessage').innerHTML = spanStateDetails.message;
                treasury.margin.common.renderDialogBox("viewEditSpanStateDialogBox", "Span Information");
              }
            } else if (typeof data['response'] !== 'undefined' && data['response'] === status_files_not_present) {
              spanStateDetails['message'] = data["message"] +
              ". If you want to reset span for date: " +parameters.date +", please click Reload Span.\n";
              document.getElementById('spanStateMessage').innerHTML = spanStateDetails.message;
              treasury.margin.common.renderDialogBox("viewEditSpanStateDialogBox", "Span Information");
            }
            else {
              ArcMessageHelper.showMessage("ERROR", data['message']);
            }
            
            // Defining on click functions for buttons
            document.getElementById('closeSpanStateMessageBtn').onclick = function() {
                document.getElementById("viewEditSpanStateDialogBox").setAttribute("hidden", true);
              }
            const elem = document.getElementById("viewEditSpanStateDialogBox").shadowRoot.querySelector('.arc-component-dialog');
            const closeDialogButton = elem.querySelector('.arc-dialog__panel__header__control .icon-close');
            closeDialogButton.addEventListener("click", function() {
                document.getElementById("viewEditSpanStateDialogBox").setAttribute("hidden", true);
                });
            treasury.margin.util.hideSpanLoading();
       });

      serviceCall
       .fail(function(xhr, text, errorThrown) {
        treasury.margin.util.hideSpanLoading();
             treasury.margin.util
               .showErrorMessage('Error occurred while retrieving span state.');
       });
  }
  /*********************************************************************************
   * This method reloads span for search date
   ********************************************************************************/
  function _setSpanState(parameters){
    
    document.getElementById("viewEditSpanStateDialogBox").setAttribute("hidden", true);
    treasury.margin.util.showSpanLoading("Re-setting span for date: " + parameters.date);
    parameters['format'] = 'json';
    
    var serviceCall = $.ajax({
      url: '/treasury/service/spanService/bootStrapSpan',
      type: 'POST',
      data: parameters,
      dataType: 'json'
    });
    
    $.when(serviceCall)
      .done(
        function(data) {
            var spanStateResettingDetails = {};
            spanStateResettingDetails['message'] = '';
            if(typeof data['response'] !== 'undefined' && data['response'] === success_status) {
              spanStateResettingDetails['message'] = data['message'];
              _addCalculatorToTable(inputParameters)
              ArcMessageHelper.showMessage("WARNING", spanStateResettingDetails.message);
            } else {
              spanStateResettingDetails.message += data['message'];
              ArcMessageHelper.showMessage("ERROR", spanStateResettingDetails.message);
            }
            treasury.margin.util.hideSpanLoading();
          });
    serviceCall
      .fail(function(xhr, text, errorThrown) {
        treasury.margin.util.hideSpanLoading();
        treasury.margin.util
          .showErrorMessage( ' Error occurred when brining up span server for date : ' + parameters.date);
      });
  }

  function _initSingleSelectMap(){
    var obj =
    {
        "legalEntity" : {
          "obj" : "",
          "id"  : "legalEntityMarginTermsFilter"
        },
        "cpe" : {
          "obj" : "",
          "id"  : "cpeMarginTermsFilter"
        },
        "calculator" : {
          "obj" : "",
          "id"  : "calculatorMarginTermsFilter"
        },
        "agreementType" : {
          "obj" : "",
          "id"  : "agreementTypeMarginTermsFilter"
        },
        "marginType" : {
          "obj" : "",
          "id"  : "marginTypeMarginTermsFilter"
        },
        "config" : {
        "obj" : "",
        "id"  : "configMarginTermsFilter"
        }
    };
    return obj;
  }

  function _initEditSingleSelectMap(rowId){
    var obj =
    {
        "legalEntity" : {
          "obj" : "",
          "id"  : "legalEntity-" + rowId
        },
        "cpe" : {
          "obj" : "",
          "id"  : "cpe-" + rowId
        },
        "calculator" : {
          "obj" : "",
          "id"  : "calculator-" + rowId
        },
        "agreementType" : {
          "obj" : "",
          "id"  : "agreementType-" + rowId
        },
        "marginType" : {
          "obj" : "",
          "id"  : "marginType-" + rowId
        },
        "config" : {
        "obj" : "",
        "id"  : "config-" + rowId
        }
    };
    return obj;
  }

  function _loadAndInitializeMarginTermsFilters() {
      $
      	.when(treasury.loadfilter.legalEntities(),
        	  treasury.loadfilter.cpes(),
      		  treasury.loadfilter.loadAgreementTypes(),
            treasury.loadfilter.loadMarginTypes(),
              treasury.loadfilter.loadMarginCalculators(),
              treasury.loadfilter.loadMarginConfigs())
      		.done(
      				function(legalEntitiesData, cpesData,
      						agreementTypesData, marginTypesData, marginCalculatorsData,
      						marginConfigsData) {

                            filterMarginTermsData["legalEntitiesData"] = legalEntitiesData;
                            filterMarginTermsData["cpesData"] = cpesData;
                            filterMarginTermsData["agreementTypesData"] = agreementTypesData;
                            filterMarginTermsData["marginTypesData"] = marginTypesData;
                            filterMarginTermsData["marginCalculatorsData"] = marginCalculatorsData;
                            filterMarginTermsData["marginConfigsData"] = marginConfigsData;

                            _initializeSingleSelectMap();
      });
  }

    function _initializeSingleSelectMap(){
      if (mode == "add") {
        addSingleSelectMap = _initSingleSelectMap();
      } else {
        editSingleSelectMap = _initEditSingleSelectMap(row.id);
      }
      var singleSelectMap = _setSingleSelectMap();

      // Initializing Legal Entity Filter
      singleSelectMap["legalEntity"]["obj"] = new window.treasury.filter.SingleSelect(
      				"legalEntityFilterIds", singleSelectMap["legalEntity"]["id"],
                    filterMarginTermsData["legalEntitiesData"][0].descoEntities, []);

      // Initializing cpe filter
      singleSelectMap["cpe"]["obj"] = new window.treasury.filter.SingleSelect(
      				"cpeIds", singleSelectMap["cpe"]["id"], filterMarginTermsData["cpesData"][0].cpes, []);

      // Initializing agreement type filter
      singleSelectMap["agreementType"]["obj"] = new window.treasury.filter.SingleSelect(
      				"agreementTypeIds", singleSelectMap["agreementType"]["id"],
      				filterMarginTermsData["agreementTypesData"][0].agreementTypes, []);

      // Initializing agreement type filter
      singleSelectMap["marginType"]["obj"] = new window.treasury.filter.SingleSelect(
        "marginTypeIds", singleSelectMap["marginType"]["id"],
        filterMarginTermsData["marginTypesData"][0].marginTypes, []);

      //Initializing margin calculator filter
      singleSelectMap["calculator"]["obj"] = new window.treasury.filter.SingleSelect(
      			"marginCalculatorIds", singleSelectMap["calculator"]["id"],
      			filterMarginTermsData["marginCalculatorsData"][0].marginCalculators,[]);

      //Initializing margin configuration filter
      singleSelectMap["config"]["obj"] = new window.treasury.filter.SingleSelect(
      				"marginConfigs", singleSelectMap["config"]["id"],
      				filterMarginTermsData["marginConfigsData"][0].marginCalculatorConfigs,[]);

      //Getting Parameter Map
      var marginTermsFilterGroup = new treasury.filter.FilterGroup(null,
                                   [singleSelectMap["legalEntity"]["obj"], singleSelectMap["cpe"]["obj"],
                                   singleSelectMap["agreementType"]["obj"], singleSelectMap["marginType"]["obj"],
                                   singleSelectMap["calculator"]["obj"],
                                   singleSelectMap["config"]["obj"]], null, null);
      if(mode == "add"){
        addMarginTermsFilterGroup = marginTermsFilterGroup;
      }
      else {
        editMarginTermsFilterGroup = marginTermsFilterGroup;
      }

      _loadSimulationReadyCalculators(singleSelectMap);
    }

    function _loadSimulationReadyCalculators(singleSelectMap) {
          var filteredCalculators = [];
          var filteredLegalEntities = [];
          var filteredCpeEntities = [];
          var filteredAgreementTypes = [];
          var filteredMarginTypes = [];

          $.each(allValidMarginTerms, function (i, marginTerm) {
              if (treasury.margin.util.isDefinedAndValid(marginTerm.config)
                  && ! isDuplicateConfig(marginTerm.config, marginTerm.calculator_id, allConfigNames)) {
                allConfigNames[marginTerm.calculator_id].push(marginTerm.config);
              }
              if (! isDuplicate(marginTerm.calculator_id, filteredCalculators)) {
                  filteredCalculators.push({id : marginTerm.calculator_id, text : marginTerm.calculator});
              }
              if (! isDuplicate(marginTerm.legal_entity_id, filteredLegalEntities)) {
                  filteredLegalEntities.push({id : marginTerm.legal_entity_id, text : marginTerm.legal_entity});
              }
              if (! isDuplicate(marginTerm.cpe_id, filteredCpeEntities)) {
                  filteredCpeEntities.push({id : marginTerm.cpe_id, text : marginTerm.cpe});
              }
              if (! isDuplicate(marginTerm.agreement_type_id, filteredAgreementTypes)) {
                  filteredAgreementTypes.push({id : marginTerm.agreement_type_id, text : marginTerm.agreement_type});
              }
              if (! isDuplicate(marginTerm.margin_type_id, filteredMarginTypes)) {
                filteredMarginTypes.push({id : marginTerm.margin_type_id, text : marginTerm.margin_type});
            }
          });

          filteredCalculators.sort(nameComparator);
          filteredLegalEntities.sort(nameComparator);
          filteredCpeEntities.sort(nameComparator);
          filteredAgreementTypes.sort(nameComparator);
          filteredMarginTypes.sort(nameComparator);

          resetSingleSelectFilter(singleSelectMap["legalEntity"]["obj"],filteredLegalEntities, 'Select a Legal Entity');
          resetSelectedValue(singleSelectMap["legalEntity"]["obj"], "");

          resetSingleSelectFilter(singleSelectMap["cpe"]["obj"], filteredCpeEntities, 'Select a Counterparty Entity');
          resetSelectedValue(singleSelectMap["cpe"]["obj"], "");

          resetSingleSelectFilter(singleSelectMap["agreementType"]["obj"],filteredAgreementTypes, 'Select an agreement type');
          resetSelectedValue(singleSelectMap["agreementType"]["obj"], "");

          resetSingleSelectFilter(singleSelectMap["marginType"]["obj"],filteredMarginTypes, 'Select a margin type');
          resetSelectedValue(singleSelectMap["marginType"]["obj"], "");

          resetSingleSelectFilter(singleSelectMap["calculator"]["obj"],filteredCalculators);
          resetSelectedValue(singleSelectMap["calculator"]["obj"], filteredCalculators[0], filteredCalculators[0].value);
      }

  function _editMarginTerm(event,rowId){
    mode = "edit";
    isEditMarginTerm = true;
    row = event.target.closest('tr');
    _initializeSingleSelectMap();
    //hide all buttons
    row.classList.add('editable');
    $(".addMarginTermsFilter").attr("hidden" , "true");
    var table = document.getElementById("marginTermsTable");
    var tableActionContainer = table.querySelectorAll('.margin-terms-table-action');
    for(var i=0; i < tableActionContainer.length; i++){
      tableActionContainer[i].hidden = true;
    }
    row.querySelector('.margin-terms-table-change').hidden = false;
    isRowEditable = true;
    document.getElementById("cloneConfigActionBtn-" + rowId).removeAttribute("disabled");
    if (currentCalculatorConfig["is_simulation_config"] == 0)
    {
        document.getElementById("editConfigActionBtn-" + rowId).setAttribute("disabled", true);
    }
    else
    {
        document.getElementById("editConfigActionBtn-" + rowId).removeAttribute("disabled");
    }
  }

  function _deleteMarginTerm(event,rowId){
    row = event.target.closest('tr');
    _clearMarginTermsFilter();
    var table = document.getElementById("marginTermsTable");
    table.deleteRow(row.rowIndex);
    delete marginTermsData[rowId];
    mode = "add";
    treasury.margin.widget.addMarginTerms();
  }

  function _acceptEdit(rowId){
     isEditMarginTerm = false;
     _addMarginTerm(rowId);
  }

  function _rejectEdit(rowId){
    isEditMarginTerm = false;
    row.classList.remove('editable');
    $(".addMarginTermsFilter").removeAttr("hidden");
    _clearMarginTermsFilter();
    var table = document.getElementById("marginTermsTable");
    var tableActionContainer = table.querySelectorAll('.margin-terms-table-action');
    for(var i=0; i < tableActionContainer.length; i++){
      tableActionContainer[i].hidden = false;
    }
    row.querySelector('.margin-terms-table-change').hidden = true;
    isRowEditable = false;
    document.getElementById("cloneConfigActionBtn-" + rowId).setAttribute("disabled", true);
    document.getElementById("editConfigActionBtn-" + rowId).setAttribute("disabled", true);
    mode = "add";
    _resetSelectedFilters();
    _initializeSingleSelectMap();
  }

    function nameComparator(item1, item2) {
          return item1["text"].localeCompare(item2["text"]);
    }

    function isDuplicate (id, items) {
      var isDuplicate = false;
        $.each(items, function (i, item) {
            if (item["id"] === id) {
                isDuplicate = true;
                return false;
            }
        });
        return isDuplicate;
    }

    function isDuplicateConfig (configName, calculatorId, allConfigNames) {
      var isDuplicate = false;
      let configList = allConfigNames[calculatorId];
      if(!configList || configList.length == 0) {
        allConfigNames[calculatorId] = [];
        return false;
      }
        $.each(configList, function (i, item) {
            if (item === configName) {
                isDuplicate = true;
                // Breaking $.each() loop using return false statement.
                return false;
            }
        });
        return isDuplicate;
    }

    //sets the drop-down data to new items
    function resetSingleSelectFilter (singleSelectObj, newItems, placeholder){
      if( !(treasury.margin.util.isDefinedAndValid(singleSelectObj))) return;
      var filterElement = document.getElementById(singleSelectObj.componentId);

      filterElement.placeholder = placeholder;
      singleSelectObj.inputFilterList = newItems.slice(0);
      singleSelectObj.inputFilterMap.length=0;
      filterElement.data.length = 0;
      $.each(newItems,function(i,newItem){
        var filterItem = {
          key : newItem.id,
          value : newItem.text
        };
        singleSelectObj.inputFilterMap.push(filterItem);
        filterElement.data.push(filterItem);
      });
    }

    //Reset options listed in singleSelectObj dropdown
    function resetSelectedValue (singleSelectObj, selectedVal) {
      if( !(treasury.margin.util.isDefinedAndValid(singleSelectObj))) return;
      var filterElement = document.getElementById(singleSelectObj.componentId);
      if( !(treasury.margin.util.isDefinedAndValid(selectedVal))){
        singleSelectObj.setSelectedNode();
      }
      else{
        filterElement.value = {
          key : selectedVal.id,
          value : selectedVal.text
        }
        singleSelectObj.selectedId = selectedVal.id;
        singleSelectObj.setSelectedNode(selectedVal.id);
      }
    }

  function _setSingleSelectMap() {
    return (mode=="add") ? addSingleSelectMap : editSingleSelectMap;
  }

  function _filterMarginTermsFilter(source, filterName){

      var singleSelectMap = _setSingleSelectMap();
      var singleSelectObj = singleSelectMap[filterName]["obj"];
      if(singleSelectObj == undefined || singleSelectObj == "") return;
      var selectedVal = singleSelectObj.getSelectedId();
      if(selectedVal == undefined || selectedVal == -1) return;
      treasury.margin.common.showLoading("mainLoader");
      var filteredLegalEntities = [];
      var uniqueEntities = [];
      var filteredCpe = [];
      var uniqueCpes = [];
      var filteredAgmtTypes = [];
      var uniqueAgmtTypes = [];
      var filteredCalculators = [];
      var uniqueCalculators = [];
      var filteredMarginTypes = [];
      var uniqueMarginTypes = [];

      var marginTerms;
      if(filteredMarginTerms.length == 0) {
          marginTerms = allValidMarginTerms;
      } else {
          marginTerms = filteredMarginTerms;
      }
      filteredMarginTerms = [];

      $.each(marginTerms, function (i, marginTerm) {
          if (marginTerm[source] == selectedVal) {
              filteredMarginTerms.push(marginTerm);

              switch (source)
              {
                case 'legal_entity_id':
                    selectedFilterValuesMap["selectedLegalEntity"] = {id : marginTerm.legal_entity_id, text : marginTerm.legal_entity};
                    break;
                case 'cpe_id':
                    selectedFilterValuesMap["selectedCPE"] = {id : marginTerm.cpe_id, text : marginTerm.cpe};
                    break;
                case 'agreement_type_id':
                    selectedFilterValuesMap["selectedAgreementType"] = {id : marginTerm.agreement_type_id, text : marginTerm.agreement_type};
                    break;
                case 'margin_type_id':
                    selectedFilterValuesMap["selectedMarginType"] = {id : marginTerm.margin_type_id, text : marginTerm.margin_type};

                    break;
                case 'calculator':
                    selectedFilterValuesMap["selectedCalculator"] = {id : marginTerm.calculator_id, text : marginTerm.calculator};
                    break;
              }

              if ($.inArray(marginTerm.legal_entity_id, uniqueEntities) === -1) {
                  filteredLegalEntities.push({id : marginTerm.legal_entity_id, text : marginTerm.legal_entity});
                  uniqueEntities.push(marginTerm.legal_entity_id);
              }

              if ($.inArray(marginTerm.cpe_id, uniqueCpes) === -1) {
                  filteredCpe.push({id : marginTerm.cpe_id, text : marginTerm.cpe});
                  uniqueCpes.push(marginTerm.cpe_id);
              }

              if ($.inArray(marginTerm.agreement_type_id, uniqueAgmtTypes) === -1) {
                  filteredAgmtTypes.push({id : marginTerm.agreement_type_id, text : marginTerm.agreement_type});
                  uniqueAgmtTypes.push(marginTerm.agreement_type_id);
              }

              if ($.inArray(marginTerm.margin_type_id, uniqueMarginTypes) === -1) {
                  filteredMarginTypes.push({id : marginTerm.margin_type_id, text : marginTerm.margin_type});
                  uniqueMarginTypes.push(marginTerm.margin_type_id);
              }

              if ($.inArray(marginTerm.calculator_id, uniqueCalculators) === -1) {
                  filteredCalculators.push({id : marginTerm.calculator_id, text : marginTerm.calculator});
                  uniqueCalculators.push(marginTerm.calculator_id);
              }
          }
      });

      resetSingleSelectFilter(singleSelectMap["legalEntity"]["obj"],filteredLegalEntities, "");
      resetSingleSelectFilter(singleSelectMap["cpe"]["obj"], filteredCpe, "");
      resetSingleSelectFilter(singleSelectMap["agreementType"]["obj"],filteredAgmtTypes, "");
      resetSingleSelectFilter(singleSelectMap["marginType"]["obj"],filteredMarginTypes, "");
      resetSingleSelectFilter(singleSelectMap["calculator"]["obj"],filteredCalculators, "");
      if(filteredCalculators.length > 0){
          resetSelectedValue(singleSelectMap["calculator"]["obj"], filteredCalculators[0]);
      }
      treasury.margin.common.hideLoading("mainLoader");
  };

  function _filterConfigBasedOnCalculator(source, filterName)
  {
	  treasury.margin.common.showLoading("mainLoader");
	  var singleSelectMap = _setSingleSelectMap();
      var singleSelectObj = singleSelectMap[filterName]["obj"];
	  if(singleSelectObj == undefined || singleSelectObj == "") return;
      var selectedVal = singleSelectObj.getSelectedId();
      if(selectedVal == undefined || selectedVal == -1) return;

      var uniqueConfigurations = [];
      var marginTerms = allValidMarginTerms;
      var filteredConfigurations = [];

      $.each(marginTerms, function (i, marginTerm) {
          if (marginTerm[source] == selectedVal) {
              selectedFilterValuesMap["selectedCalculator"] = {id : marginTerm.calculator_id, text : marginTerm.calculator};
              if ($.inArray(marginTerm.config, uniqueConfigurations) === -1) {
            	  filteredConfigurations.push({id : marginTerm.config, text : marginTerm.config});
                  uniqueConfigurations.push(marginTerm.config);
              }
          }
      });
      filteredConfigurations.sort(nameComparator);
      treasury.margin.common.hideLoading("mainLoader");
      currentCalculatorId = selectedVal;
      editedConfigData = {};
      var params = {
              'marginCalculatorId': selectedVal
          };
      $.ajax({
                    url: "/treasury/data/search-calculator-production-config-name",
                    data: params,
                    type: "GET",
                    dataType: "json",
                    error: function(response, data) {
                        return response;
                    },
                    success: function(data) {
                        if (treasury.margin.util.isDefinedAndValid(data) &&
                            treasury.margin.util.isDefinedAndValid(data.result)) {
                            productionConfigForCalculator.id = data.result;
                            productionConfigForCalculator.text = data.result;
                        } else {
                            productionConfigForCalculator = {};
                        }
                        resetSingleSelectFilter(singleSelectMap["config"]["obj"],filteredConfigurations, "");
                        if(filteredConfigurations.length > 0){
                            if (treasury.margin.util.isDefinedAndValid(productionConfigForCalculator)) {
                              resetSelectedValue(singleSelectMap["config"]["obj"], productionConfigForCalculator);
                            } else {
                              resetSelectedValue(singleSelectMap["config"]["obj"], filteredConfigurations[0]);
                            }
                        }
                    },
                    timeout: 120000
                });
  };

  function _setSelectedConfigAndGetConfigData(source, filterName)
  {
	  var singleSelectMap = _setSingleSelectMap();
      var singleSelectObj = singleSelectMap[filterName]["obj"];
	  if(singleSelectObj == undefined || singleSelectObj == "") return;
	  if (singleSelectObj.getSelectedId() == "") return;
      selectedConfigName = singleSelectObj.getSelectedId();
      _getConfigDataForConfigName(SET_CONFIG_DATA);
  };

  function _getConfigDataForConfigName(flag)
  {
    if(typeof selectedConfigName === "undefined" ){
      if (mode == 'add') {
        document.getElementById('viewConfigBtn').setAttribute("disabled", true);
        document.getElementById('editConfigBtn').setAttribute("disabled", true);
        document.getElementById('cloneConfigBtn').setAttribute("disabled", true);
        } else {
        document.getElementById("viewConfigActionBtn-" + row.id).setAttribute("disabled", true);
        document.getElementById("editConfigActionBtn-" + row.id).setAttribute("disabled", true);
        document.getElementById("cloneConfigActionBtn-" + row.id).setAttribute("disabled", true);
      }
      return;
    }
    var params = {
                  'calculatorConfigName': selectedConfigName
              };
    $.ajax({
         url: "/treasury/data/search-calculator-config-data-from-config-name",
         data: params,
         type: "GET",
         dataType: "json",
         error: function(response, data) {
            treasury.margin.common.hideLoading("mainLoader");
            return response;
         },
         success: function(data) {
            if (data == null || data["resultList"] == undefined || data["resultList"].length == 0) {
              currentCalculatorConfig = [];
            } else {
              currentCalculatorConfig = data["resultList"];
              treasury.margin.ssu.Actions.updateConfigData(currentCalculatorConfig);
              for (let i = 0; i<currentCalculatorConfig.length; i++){
                if(currentCalculatorConfig[i].calculator_id == currentCalculatorId){
                  currentCalculatorConfig = currentCalculatorConfig[i];
                  break;
                }
              }
            }
            configData = _getParsedData();
            if(flag == SET_CONFIG_DATA) {
              _setSelectedConfig();
            }
            else if(flag == UPDATE_UI_ELEMENTS_ATTRIBUTE){
              document.getElementById("viewEditConfigDialogBox").setAttribute("hidden", true);
              document.getElementById('cloneConfigBtn').removeAttribute('disabled');
            }
            treasury.margin.common.hideLoading("mainLoader");
         },
         timeout: 120000
    });
  }

  function _getParsedData(){
    var xmlInput = currentCalculatorConfig['config_xml'];
    if (typeof xmlInput === "undefined"){
      return;
    }
    xmlInput = xmlInput.replace(/\n|\r/g, "");
    xmlInput = xmlInput.replace(/>.*?</g, "><");
    return treasury.margin.ssu.Actions.parseXmlInput(xmlInput);
  }

  function _setSelectedConfig(){
    if (!(treasury.margin.util.isDefinedAndValid(currentCalculatorConfig)))
    {
      if (mode == 'add') {
      document.getElementById('viewConfigBtn').setAttribute("disabled", true);
      document.getElementById('editConfigBtn').setAttribute("disabled", true);
      document.getElementById('cloneConfigBtn').setAttribute("disabled", true);
      } else {
      document.getElementById("viewConfigActionBtn-" + row.id).setAttribute("disabled", true);
      document.getElementById("editConfigActionBtn-" + row.id).setAttribute("disabled", true);
      document.getElementById("cloneConfigActionBtn-" + row.id).setAttribute("disabled", true);
      }
    }
    else if (currentCalculatorConfig["is_simulation_config"] == 0) {
      if (mode == 'add') {
      document.getElementById('viewConfigBtn').removeAttribute("disabled");
      document.getElementById('editConfigBtn').setAttribute("disabled", true);
      document.getElementById('cloneConfigBtn').removeAttribute("disabled");
      } 
      else {
        if(isEditMarginTerm) {
            document.getElementById("viewConfigActionBtn-" + row.id).removeAttribute("disabled");
        }
        else if (row.cells[4].querySelector('.margin-terms-table-label').innerHTML == "none") {
            document.getElementById("viewConfigActionBtn-" + row.id).setAttribute("disabled", true);
        }
        document.getElementById("editConfigActionBtn-" + row.id).setAttribute("disabled", true);
        if (treasury.margin.util.isDefinedAndValid(isRowEditable) && isRowEditable) {
            document.getElementById("cloneConfigActionBtn-" + row.id).removeAttribute("disabled");
        }
      }
    }
    else
    {
      if (mode == 'add') {
      document.getElementById('viewConfigBtn').removeAttribute("disabled");
      document.getElementById('editConfigBtn').removeAttribute("disabled");
      document.getElementById('cloneConfigBtn').removeAttribute("disabled");
      } else {
      if(isEditMarginTerm) {
          document.getElementById("viewConfigActionBtn-" + row.id).removeAttribute("disabled");
      }
      else if (row.cells[4].querySelector('.margin-terms-table-label').innerHTML == "none") {
          document.getElementById("viewConfigActionBtn-" + row.id).setAttribute("disabled", true);
      }
      document.getElementById("editConfigActionBtn-" + row.id).removeAttribute("disabled");
      if (treasury.margin.util.isDefinedAndValid(isRowEditable) && isRowEditable) {
          document.getElementById("cloneConfigActionBtn-" + row.id).removeAttribute("disabled");
      }
      }
    }
  }
  function getMarginTermsRecordKey(marginTermsRecord)
    {
        var marginTermsRecordKey = marginTermsRecord.legal_entity_id   + "|" +
                                   marginTermsRecord.cpe_id            + "|" +
                                   marginTermsRecord.agreement_type_id + "|" +
                                   marginTermsRecord.margin_type_id    + "|" +
                                   marginTermsRecord.calculator_id     + "|" +
                                   marginTermsRecord.config     	   + "|" +
                                   marginTermsRecord.includeExistingPortfolio;
        return marginTermsRecordKey;
    }

    function _getParameterValuesMap(keyArray,params,filterGroup){
      var filterArray = filterGroup.singleSelectFilterArray;
      for (var i=0;i<filterArray.length;i++){
        var singleSelectObj = filterArray[i];
        var selectedId = singleSelectObj.getSelectedId();
        var inputFilterMap = singleSelectObj.inputFilterMap;
        if(selectedId != undefined){
          for(var idx in inputFilterMap) {
              var key = inputFilterMap[idx].key;
              var val = inputFilterMap[idx].value;
              if(key == selectedId) {
                  params[keyArray[i]] = val;
                  break;
              }
          }
        }
      }
      return params;
    }

    function _setModeAndAdd(){
      mode = "add";
      _addMarginTerm();
    }

    function _addMarginTerm(rowId){
      var params = {
        legalEntity : "",
        cpe : "",
        agmtType : "",
        marginType: "",
        calculator : "",
        config : "",
        inclExistingPortfolio : ""
      };

      var marginTermsFilterGroup;
      if(mode == "add"){
        marginTermsFilterGroup = addMarginTermsFilterGroup;
        params["inclExistingPortfolio"] = $("#includeExistingPortfolioChkbox").prop('checked');
      }
      else{
        marginTermsFilterGroup = editMarginTermsFilterGroup;
        params["inclExistingPortfolio"] = document.getElementById("checkbox-" + rowId).checked;
      }

      _getParameterValuesMap(["legalEntity","cpe","agmtType","marginType","calculator","config"], params, marginTermsFilterGroup);
      var paramIds = marginTermsFilterGroup.getParameterMap();

      if (params["inclExistingPortfolio"] &&
          (params.legalEntity == "" || params.cpe == "" || params.agmtType == "" || params.marginType == "" ||
           params.calculator == "")){
            ArcMessageHelper.showMessage("ERROR", "Only valid portfolio can be run with Include existing portfolio\
                                                  option. Please select a valid entry for Legal entity/Counterparty/Agreement type/Margin Type.");
        return false;
      }
      inputParameters = {};
      inputParameters['params'] = params;
      inputParameters['paramIds'] = paramIds;
      inputParameters['rowId'] = rowId;

      if (params.calculator != "" && CODEX_PROPERTIES["treasury.portal.defaults.simulation.span.feature"] != undefined
        && CODEX_PROPERTIES["treasury.portal.defaults.simulation.span.feature"].myValue
        && (params.calculator.toLowerCase().includes('span') ||
        params.calculator.toLowerCase().includes('barclaysgnair')) ) {
          var spanParameter = {};
          spanParameter['date'] =  $("#datePicker").val().replace(/-/g, "");
          _loadSpanState(spanParameter);

          // Defining on click functions for buttons
          document.getElementById('reloadSpanBtn').onclick = function() {
            _setSpanState(spanParameter);
          }
          return true;
      }
      return _addCalculatorToTable(inputParameters);
    }

    function _addCalculatorToTable(inputParameters) {
      var params = inputParameters['params'];
      var paramIds = inputParameters['paramIds'];
      if (params.calculator == "" && params.config != ""){
          ArcMessageHelper.showMessage("ERROR", "Configuration can be selected corresponding to a calculator only.\
                                                 Please select a valid calculator before selecting configuration.");
          return false;
      }
      if (params.calculator == "" && params.config == ""){
          ArcMessageHelper.showMessage("ERROR", "Calculator and Configuration are mandatory fields.\
                                                 Please select a valid calculator and a corresponding configuration.");
          return false;
      }
      if (params.calculator != "" && params.config == ""){
          params.config = (treasury.margin.util.isDefinedAndValid(productionConfigForCalculator.text)) ?
                          productionConfigForCalculator.text : "none";

          if (params.config == "none") {
            ArcMessageHelper.showMessage("WARNING", "No configuration exists for the calculator.  \
                                                     Hence, displaying configuration as \"none\".");
          }
          else {
            ArcMessageHelper.showMessage("WARNING", "You have not selected any configuration for the calculator.  \
                                                     Selecting production configuration by default.");
          }
      }
      var rowId = inputParameters['rowId'];
        var id = rowId ? rowId : ("id_" + Date.now()).replace(/[()/ /]/g, '');
        var dataRow = { id : id,
                calculator : params.calculator,
                calculator_id : paramIds.marginCalculatorIds.toString(),
                config : params.config,
                includeExistingPortfolio : params.inclExistingPortfolio
              };

        if (params.legalEntity != "") {
            dataRow['legal_entity'] = params.legalEntity;
            dataRow['legal_entity_id'] = paramIds.legalEntityFilterIds.toString();
        }
        if (params.cpe != "") {
            dataRow['cpe'] = params.cpe;
            dataRow['cpe_id'] = paramIds.cpeIds.toString();
        }
        if (params.agmtType != "") {
            dataRow['agreement_type'] = params.agmtType;
            dataRow['agreement_type_id'] = paramIds.agreementTypeIds.toString();
        }
        if (params.marginType != "") {
          dataRow['margin_type'] = params.marginType;
          dataRow['margin_type_id'] = paramIds.marginTypeIds.toString();
        }
        var dataRowKey = getMarginTermsRecordKey(dataRow);
        var marginTermsTable = document.getElementById("marginTermsTable");
        if(marginTermsTable != undefined){
            for (var rowId in marginTermsData) {
               var key = getMarginTermsRecordKey(marginTermsData[rowId]);
               if (dataRowKey == key) {
                   if (treasury.margin.util.isDefinedAndValid(row)) {
                   	row.classList.remove('editable');
                   	var table = document.getElementById("marginTermsTable");
                   	var tableActionContainer = table.querySelectorAll('.margin-terms-table-action');
                   	for(var i=0; i < tableActionContainer.length; i++){
                   		tableActionContainer[i].hidden = false;
                   	}
                   	row.querySelector('.margin-terms-table-change').hidden = true;
                   }
                   $(".addMarginTermsFilter").removeAttr("hidden");
                   ArcMessageHelper.showMessage("warning","This term has already been added!");
                   if(mode == 'edit'){
                    document.getElementById("editConfigActionBtn-" + row.id).setAttribute("disabled", true);
                    document.getElementById("cloneConfigActionBtn-" + row.id).setAttribute("disabled", true);
                   }
                   mode = 'add';
                   _clearMarginTermsFilter();
                   return false;
               }
           }
         }
         marginTermsData[dataRow['id']] = dataRow;
         if(mode == "add"){
            _addTableRow(dataRow);
         }
         else{
           _updateTableRow(dataRow);
           mode = 'add';
         }

         document.getElementById("nextId").removeAttribute("disabled");
         document.getElementById("wizard_step3").removeAttribute("disabled");
         _clearMarginTermsFilter();
         _resetSelectedFilters();
         _initializeSingleSelectMap();
         return true;
    }

    function _addTableRow(dataRow){
      var html = "<tr id = '" + dataRow['id'] + "'>";
      for(var i=0;i<6;i++){
        var label = "<span class='margin-terms-table-label'></span>";
        var combobox = "<arc-combobox class='margin-terms-table-input'></arc-combobox>";
        html += "<td>" + label + combobox + "</td>";
      }

      var configButtonHTML =
       "<div class='configuration-action'>\
          <button class='button--tertiary icon-view padding view-config-action' onclick=\"treasury.margin.marginTerms.viewConfig(event)\"></button>\
          <button class='button--tertiary icon-copy padding clone-config-action' disabled onclick=\"treasury.margin.marginTerms.cloneConfig()\"></button>\
          <button class='button--tertiary icon-edit--block padding edit-config-action' disabled onclick=\"treasury.margin.marginTerms.editConfig()\"></button>\
        </div>";
      html += "<td>" + configButtonHTML + "</td>";

      //checkboxHTML
      var label = "<span class='margin-terms-table-label'></span>";
      var checkbox = "<input type='checkbox' class='margin-terms-table-input'>";
      html += "<td>" + label + checkbox + "</td>";

      var buttonHTML =
      "<div class='margin-terms-table-change' hidden>\
        <button class='button--tertiary icon-accept padding' onclick=\"treasury.margin.marginTerms.acceptEdit('" +dataRow['id']+ "')\"></button>\
        <button class='button--tertiary icon-reject padding' onclick=\"treasury.margin.marginTerms.rejectEdit('" +dataRow['id']+ "')\"></button>\
      </div>\
      <div class='margin-terms-table-action'>\
        <button class='button--tertiary icon-edit padding' onclick=\"treasury.margin.marginTerms.editMarginTerm(event,'" +dataRow['id']+ "')\"></button>\
        <button class='button--tertiary icon-delete padding' onclick=\"treasury.margin.marginTerms.deleteMarginTerm(event,'" +dataRow['id']+ "')\"></button>\
      </div>";

      html += "<td>" + buttonHTML + "</td>";
      html += "</tr>";
      $(html).insertBefore(".addMarginTermsFilter");

      var row = document.getElementById(dataRow['id']);
      var cells = row.cells;
      cells[0].querySelector('.margin-terms-table-label').innerHTML = dataRow['legal_entity'] || "All";
      cells[1].querySelector('.margin-terms-table-label').innerHTML = dataRow['cpe'] || "All";
      cells[2].querySelector('.margin-terms-table-label').innerHTML = dataRow['agreement_type'] || "All";
      cells[3].querySelector('.margin-terms-table-label').innerHTML = dataRow['margin_type'] || "All";
      cells[4].querySelector('.margin-terms-table-label').innerHTML = dataRow['calculator'] || "All";
      cells[5].querySelector('.margin-terms-table-label').innerHTML = dataRow['config'];
      cells[7].querySelector('.margin-terms-table-label').innerHTML = dataRow['includeExistingPortfolio'] ? "Yes" : "No";

      cells[0].querySelector('.margin-terms-table-input').id = "legalEntity-" + dataRow['id'];
      cells[1].querySelector('.margin-terms-table-input').id = "cpe-" + dataRow['id'];
      cells[2].querySelector('.margin-terms-table-input').id = "agreementType-" + dataRow['id'];
      cells[3].querySelector('.margin-terms-table-input').id = "marginType-" + dataRow['id'];
      cells[4].querySelector('.margin-terms-table-input').id = "calculator-" + dataRow['id'];
      cells[5].querySelector('.margin-terms-table-input').id = "config-" + dataRow['id'];
      cells[7].querySelector('.margin-terms-table-input').id = "checkbox-" + dataRow['id'];

      row.querySelector('.view-config-action').id = "viewConfigActionBtn-" + dataRow['id'];
      row.querySelector('.clone-config-action').id = "cloneConfigActionBtn-" + dataRow['id'];
      row.querySelector('.edit-config-action').id = "editConfigActionBtn-" + dataRow['id'];

      if (cells[5].querySelector('.margin-terms-table-label').innerHTML == "none")
      {
        document.getElementById("viewConfigActionBtn-" + row.id).setAttribute("disabled", true);
      }

       document.getElementById("checkbox-" + dataRow['id']).checked = dataRow['includeExistingPortfolio'];
       document.getElementById("legalEntity-" + dataRow['id']).onchange =
            function() {_filterMarginTermsFilter('legal_entity_id', "legalEntity")};
       document.getElementById("cpe-" + dataRow['id']).onchange =
           function(){_filterMarginTermsFilter('cpe_id', "cpe")};
       document.getElementById("agreementType-" + dataRow['id']).onchange =
           function(){_filterMarginTermsFilter('agreement_type_id', "agreementType")};
        document.getElementById("marginType-" + dataRow['id']).onchange =
           function(){_filterMarginTermsFilter('margin_type_id', "marginType")};
       document.getElementById("calculator-" + dataRow['id']).onchange =
                  function(){_filterConfigBasedOnCalculator('calculator_id', "calculator")};
       document.getElementById("config-" + dataRow['id']).onchange =
                         function(){_setSelectedConfigAndGetConfigData('config', "config")};
    }

    function _updateTableRow(dataRow){
      row.cells[0].querySelector('.margin-terms-table-label').innerHTML = dataRow['legal_entity'] || "All";
      row.cells[1].querySelector('.margin-terms-table-label').innerHTML = dataRow['cpe'] || "All";
      row.cells[2].querySelector('.margin-terms-table-label').innerHTML = dataRow['agreement_type'] || "All";
      row.cells[3].querySelector('.margin-terms-table-label').innerHTML = dataRow['margin_type'] || "All";
      row.cells[4].querySelector('.margin-terms-table-label').innerHTML = dataRow['calculator'] || "All";
      row.cells[5].querySelector('.margin-terms-table-label').innerHTML = dataRow['config'];
      row.cells[7].querySelector('.margin-terms-table-label').innerHTML = dataRow['includeExistingPortfolio'] ? "Yes" : "No";
      if (row.cells[5].querySelector('.margin-terms-table-label').innerHTML == "none")
      {
        document.getElementById("viewConfigActionBtn-" + row.id).setAttribute("disabled", true);
      }
      var table = document.getElementById("marginTermsTable");
      var tableActionContainer = table.querySelectorAll('.margin-terms-table-action');
      for(var i=0; i < tableActionContainer.length; i++){
        tableActionContainer[i].hidden = false;
      }
      row.querySelector('.margin-terms-table-change').hidden = true;
      isRowEditable = false;
      document.getElementById("cloneConfigActionBtn-" + dataRow['id']).setAttribute("disabled", true);
      document.getElementById("editConfigActionBtn-" + dataRow['id']).setAttribute("disabled", true);
      row.classList.remove('editable');
      $(".addMarginTermsFilter").removeAttr("hidden");
    }

    function _clearMarginTermsFilter(){
      var singleSelectMap = _setSingleSelectMap();
      var checkbox = (mode=="add") ? "includeExistingPortfolioChkbox" : "checkbox-" + row.id;
      $.when(_initializeSingleSelectMap()).done(function(){
        filteredMarginTerms = [];
        resetSelectedValue(singleSelectMap["legalEntity"]["obj"], "");
        resetSelectedValue(singleSelectMap["cpe"]["obj"], "");
        resetSelectedValue(singleSelectMap["agreementType"]["obj"], "");
        resetSelectedValue(singleSelectMap["marginType"]["obj"], "");
        document.getElementById(checkbox).checked = false;
      });
    }

    function _marginTermsAvailable(){
      return marginTermsAvailableFlag;
    }

    function _viewConfig(event)
    {
        // Resetting to original dom structure
    	document.getElementById("saveResetEditorSection").setAttribute("hidden", true);

        // Showing dialog box
        if (event.target.closest('tr').classList.value == "addMarginTermsFilter" ||
            event.target.closest('tr').classList.contains('editable'))
        {
            _renderConfigData(true);
            treasury.margin.util.renderDialogBox("viewEditConfigDialogBox", "View", selectedConfigName);
        }
        else
        {
    	    var row = event.target.closest('tr');
          var configCellId = "config-" + row.id;
          var calculatorCellId = "calculator-" + row.id;
            for (var i = 0; i < row.childNodes.length; i++) {
                if (row.children[i].childNodes[1].id == configCellId) {
                    selectedConfigFromMarginTerm = row.childNodes[i].firstChild.innerText
                    break;
                }
            }
            for (var i = 0; i < row.childNodes.length; i++) {
              if (row.children[i].childNodes[1].id == calculatorCellId) {
                  selectedCalculatorFromMarginTerm = row.childNodes[i].firstChild.innerText
                  break;
              }
            }
            _fetchConfigDataDetailsForAddedMarginTerm(true);
        }

        // Defining on click functions for buttons
        document.getElementById('closeViewEditConfigBtn').onclick = function() {
            document.getElementById("viewEditConfigDialogBox").setAttribute("hidden", true);
          }
        const elem = document.getElementById("viewEditConfigDialogBox").shadowRoot.querySelector('.arc-component-dialog');
        const closeDialogButton = elem.querySelector('.arc-dialog__panel__header__control .icon-close');
        closeDialogButton.addEventListener("click", function() {
            document.getElementById("viewEditConfigDialogBox").setAttribute("hidden", true);
            });
    }

    function _cloneConfig()
    {
    	// Resetting to original dom structure
    	document.getElementById("cloneConfigDialogBox").removeAttribute("hidden");
    	document.getElementById("cloneConfigNameError").setAttribute("hidden", true);
    	document.getElementById("cloneCommentError").setAttribute("hidden", true);
    	document.getElementById("clone-config-name").value = "";
    	document.getElementById("clone-config-comment").value = "";

    	// Showing dialog box
    	treasury.margin.util.renderDialogBox("cloneConfigDialogBox", "Clone", selectedConfigName);

        // Defining on click functions for buttons
        document.getElementById('setCloneConfigBtn').onclick = _setClonedConfigData;
        document.getElementById('closeCloneConfigBtn').onclick = function() {
            document.getElementById("cloneConfigDialogBox").setAttribute("hidden", true);
          }
        const elem = document.getElementById("cloneConfigDialogBox").shadowRoot.querySelector('.arc-component-dialog');
        const closeDialogButton = elem.querySelector('.arc-dialog__panel__header__control .icon-close');
        closeDialogButton.addEventListener("click", function() {
            document.getElementById("cloneConfigDialogBox").setAttribute("hidden", true);
		      });
    }

    function _editConfig()
    {
    	// Resetting to original dom structure
    	document.getElementById("viewEditConfigDialogBox").removeAttribute("hidden");
    	document.getElementById("saveResetEditorSection").removeAttribute("hidden");

        document.getElementById('commentError').setAttribute('hidden', true);
        document.getElementById('comment').value = "";
        document.getElementById('comment').setAttribute('disabled', true);
        document.getElementById('saveConfigBtn').setAttribute('disabled', true);
        document.getElementById('resetConfigBtn').setAttribute('disabled', true);

        // Showing dialog box
        _renderConfigData(false);
        treasury.margin.util.renderDialogBox("viewEditConfigDialogBox", "Edit", selectedConfigName);
        document.getElementById('cloneConfigBtn').removeAttribute('disabled');

        // Defining on click functions for buttons
        document.getElementById('closeViewEditConfigBtn').onclick = function() {
                                            document.getElementById("viewEditConfigDialogBox").setAttribute("hidden", true);
                                          }
        document.getElementById('cloneConfigBtn').removeAttribute('disabled');
    }

    function _renderConfigData(isViewOnly)
    {
        if (!isViewOnly) {
            document.getElementById('saveConfigBtn').onclick = _saveEditedConfig;
            document.getElementById('resetConfigBtn').onclick = _resetEditedConfig;
        }
        treasury.margin.ssu.configEditor.renderSelectedConfig(configData, isViewOnly);
    }

    function _fetchConfigDataDetailsForAddedMarginTerm(isViewOnly) {
          var params = {
                        'calculatorConfigName': selectedConfigFromMarginTerm
                       };
          treasury.margin.common.showLoading();
          $.ajax({
              url: "/treasury/data/search-calculator-config-data-from-config-name",
              data: params,
              type: "GET",
              dataType: "json",
              error: function(response, data) {
                  treasury.margin.common.hideLoading();
                  return response;
              },
              success: function(data) {
                  if (data == null || data["resultList"] == undefined ||
                      data["resultList"].length == 0) {
                      currentCalculatorConfigForAddedMarginTerm = [];
                  } else {
                    for(let i = 0; i<data["resultList"].length; i++){
                      if(data["resultList"][i]["calculator_name"] == selectedCalculatorFromMarginTerm){
                        currentCalculatorConfigForAddedMarginTerm = data["resultList"][i];
                        break;
                      }
                    }
                  }
                  var xmlInput = currentCalculatorConfigForAddedMarginTerm['config_xml'];
                  var configDataForAddedMarginTerm = treasury.margin.ssu.Actions.parseXmlInput(xmlInput);
                  treasury.margin.ssu.configEditor.renderSelectedConfig(configDataForAddedMarginTerm, isViewOnly);
                  treasury.margin.common.hideLoading();
                  treasury.margin.util.renderDialogBox("viewEditConfigDialogBox", "View", selectedConfigFromMarginTerm);
              },
              timeout: 120000
          });
       }

    function _saveEditedConfig()
    {
        if (document.getElementById('comment').value == "") {
            document.getElementById('commentError').removeAttribute('hidden');
        } else {
            document.getElementById('commentError').setAttribute('hidden', true);
            treasury.margin.ssu.configEditor.updateAndSaveXML(SAVE_CLONED_EDITED_CONFIG_ON_SIM_UI);
        }
    }

    function _resetEditedConfig()
    {
        document.getElementById('commentError').setAttribute('hidden', true);
        document.getElementById('comment').value = "";
        document.getElementById('comment').setAttribute('disabled', true);
        document.getElementById('saveConfigBtn').setAttribute('disabled', true);
        document.getElementById('resetConfigBtn').setAttribute('disabled', true);
        treasury.margin.ssu.configEditor.resetConfig();
        document.getElementById('cloneConfigBtn').removeAttribute('disabled');
    }

    function _setClonedConfigData()
    {
        document.getElementById('cloneConfigNameError').setAttribute("hidden", "true");
        document.getElementById('cloneCommentError').setAttribute("hidden", "true");

        var isConfigNameAndCommentPopulated = 1;
        if (document.getElementById('clone-config-name').value == "") {
            document.getElementById('cloneConfigNameError').removeAttribute('hidden');
            isConfigNameAndCommentPopulated = 0;
        }
        if (document.getElementById('clone-config-comment').value == "") {
            document.getElementById('cloneCommentError').removeAttribute('hidden');
            isConfigNameAndCommentPopulated = 0;
        }

        if (isConfigNameAndCommentPopulated)
        {
            let configName = document.getElementById('clone-config-name').value;
            // Validate new config name and post new config to DB
            if(_validateConfigNameAlreadyExists())
            {
            	treasury.margin.ssu.util.raiseInfoToastWithTimeOut(
            		'Config Name already exists', 2000);
            }
            else if(treasury.margin.ssu.Actions.validateConfigNameContainsSpecialCharacters())
            {
            	treasury.margin.ssu.util.raiseInfoToastWithTimeOut(
            	    'Config Name should not contain special characters', 3000);
            }
            else if(treasury.margin.common.validateNameIsUndefined(configName)){
              let message = 'Config Name can not be ' + configName;
              treasury.margin.ssu.util.raiseInfoToastWithTimeOut(
                  message, 2000);
           }
            else {
                var obj = {
                'isCloneConfig' : true,
                'author': document.querySelector('arc-header').getAttribute('user'),
                'comment': document.getElementById('clone-config-comment').value,
                'configName': document.getElementById('clone-config-name').value
                }
                // Close clone config dialog box
                document.getElementById("cloneConfigDialogBox").setAttribute("hidden", true);
                treasury.margin.ssu.Actions.postClonedConfigToDB(obj, SAVE_AND_SET_CLONED_CONFIG_ON_SIM_UI );
            }
        }
    }

    function _validateConfigNameAlreadyExists()
    {
        var configName = document.getElementById('clone-config-name').value;
        return allConfigNames[currentCalculatorConfig["calculator_id"]].includes(configName);
    }

    function _setSelectedFilterAfterPostClonedConfigToDB(){
        // Set selected filters in the drop-down
        selectedConfigName = document.getElementById('clone-config-name').value;
        _setValueOfSelectedFilters();
        _resetSelectedFilters();
        _updateConfigFilter(selectedConfigName);
    }
    function _resetSelectedFilters()
    {
        selectedFilterValuesMap = {};
    }

    function _setValueOfSelectedFilters()
    {
        cloneConfigFiltersMap = selectedFilterValuesMap;
        cloneConfigFiltersMap["selectedConfigName"] = {id : selectedConfigName, text : selectedConfigName};
    }

    function _updateConfigFilter(selectedConfigName){
      if(selectedConfigName != undefined && (allValidMarginTerms != undefined) 
      && allValidMarginTerms.length > 0){
        let newMarginTerm = {};
        let filteredConfigurations = [];
        let uniqueConfigurations = [];
        for(let marginTerm of allValidMarginTerms) {

          if (marginTerm["calculator_id"] == currentCalculatorId && 
            $.inArray(marginTerm.config, uniqueConfigurations) === -1 ){
              filteredConfigurations.push({id : marginTerm.config, text : marginTerm.config});
              uniqueConfigurations.push(marginTerm.config);
              if(newMarginTerm["config"] == undefined){
                for (let key in marginTerm){
                  newMarginTerm[key] = marginTerm[key];
                }
                newMarginTerm["config"] = selectedConfigName;
                uniqueConfigurations.push(marginTerm.config);
                filteredConfigurations.push({id : selectedConfigName, text : selectedConfigName});
              }
          }

        }
        allValidMarginTerms.push(newMarginTerm);
        if (! isDuplicateConfig(selectedConfigName, currentCalculatorId, allConfigNames)) {
            allConfigNames[currentCalculatorId].push(selectedConfigName);
        }
        filteredConfigurations.sort(nameComparator);
        let singleSelectMap = _setSingleSelectMap();
        resetSingleSelectFilter(singleSelectMap["config"]["obj"],filteredConfigurations, "");
        resetSelectedValue(singleSelectMap["config"]["obj"], cloneConfigFiltersMap["selectedConfigName"]);
      }
    }
})();