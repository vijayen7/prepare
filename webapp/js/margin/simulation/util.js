"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.margin = window.treasury.margin || {};
  window.treasury.margin.util = {
    showErrorMessage: _showErrorMessage,
    setupDateFilter: _setupDateFilter,
    clearGrid: _clearGrid,
    resizeCanvasOnGridChange: _resizeCanvasOnGridChange,
    resizeAllCanvas: _resizeAllCanvas,
    getFormattedMessage: _getFormattedMessage,
    resizeGrids : _resizeGrids,
    resizeGrid : _resizeGrid,
    isGridEmpty : _isGridEmpty,
    isMapEmpty : _isMapEmpty,
    setSelectedPosGridHeight : _setSelectedPosGridHeight,
    isDefinedAndValid : _isDefinedAndValid,
    renderDialogBox : _renderDialogBox,
    showSpanLoading : _showSpanLoading,
    hideSpanLoading : _hideSpanLoading
  };

  function _resizeGrid() {
    var searchPositionsGridDiv = document.getElementById('searchPositionsGridDiv');
    var totalHeight = searchPositionsGridDiv.offsetHeight;
    searchPositionsGrid.options.maxHeight = totalHeight - 50;
    searchPositionsGrid.resizeCanvas();
  }

  function _isMapEmpty(obj){
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
  }

function _resizeGrids() {
  var searchPositionsGridDiv = document.getElementById('searchPositionsGridDiv');
  var totalHeight = searchPositionsGridDiv.offsetHeight;
  searchPositionsGrid.options.maxHeight = totalHeight/2 - 70;
  searchPositionsChildGrid.options.maxHeight = totalHeight/2 - 70;
  $('#searchPositionsGrid-pager').addClass('margin--bottom');
  searchPositionsGrid.resizeCanvas();
}
function _setSelectedPosGridHeight(){
  var size = selectedPositionsGrid.data.length;
  var height = (size>10) ? 300 : (50 + size*30);
  if (selectedPositionsGrid != undefined) {
      $('#selectedPositionsGrid').height(height);
  }
  if(selectedPositionsGrid != null) {
    selectedPositionsGrid.resizeCanvas();
  }
}
  function _clearGrid(gridId) {
    treasury.common.grid.clearGrid(gridId);
    if ($("#" + gridId).prev().prev().is("p") ||
      $("#" + gridId).prev().prev().is("center")) {
      $("#" + gridId).prev().prev().remove();
    } else if ($("#" + gridId).prev().is("p") ||
      $("#" + gridId).prev().is("center")) {
      $("#" + gridId).prev().remove();
    }
    var groupingHeader = document.getElementById(gridId + '-drop-element-holder');
    if (groupingHeader != null) {
      groupingHeader.innerHTML = "";
    }

    var header = document.getElementById(gridId + "-header");
    if (header != null) {
      header.innerHTML = "";
    }

    var pager = document.getElementById(gridId + "-pager");
    if (pager != null) {
      pager.innerHTML = "";
    }
  }

  function _isGridEmpty(gridObj){
    return (gridObj==undefined || gridObj.data.length <= 0);
  }

  function _setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
    dateFormat) {
    dateDiv.datepicker({
      dateFormat: dateFormat,
      changeMonth: true,
      changeYear: true,
      minDate: minDate,
      maxDate: maxDate,
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
    }).datepicker("setDate", date);
    dateDiv.change(function() {
      validateDate(dateDiv.val(), {
        errorDiv: errorDiv,
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: dateFormat,
      });
    });
  }

  function _showSpanLoading(message) {
    if(message != null && message.length != 0){
        $("#spanLoader")[0].innerHTML = message;
    }
    $("#spanLoader").removeAttr("hidden");
    $("#spanOverlay").removeAttr("hidden");
    $("#spanLoader")[0].style.display = 'block';
    $("#spanOverlay")[0].style.display = 'flex';
  }

  function _hideSpanLoading() {
    $("#spanLoader").attr("hidden", "true");
    $("#spanOverlay").attr("hidden", "true");
    $("#spanLoader")[0].style.display = 'none';
    $("#spanOverlay")[0].style.display = 'none';
  }

  function _showErrorMessage(message) {
    var errorDialog = document.getElementById("errorMessage");

    errorDialog.reveal({
      'title': 'Error',
      'content': "<center><strong>" + message +
        "</strong></center>",
      'modal': true,
      'buttons': [{
        'html': 'OK',
        'callback': function() {
          this.visible = false;
        },
        'position': 'right'
      }]
    });
    errorDialog.removeAttribute("hidden");
  }

  function _resizeAllCanvas() {
    if(document.getElementById('addPositionsFilterSidebar').state == 'expanded') {
      document.getElementById('addPositionsFilterSidebar').style.width = '300px';
    } else {
      document.getElementById('addPositionsFilterSidebar').style.width = '20px';
    }
    if(searchPositionsGrid != null) {
    	searchPositionsGrid.resizeCanvas();
        }
        if(searchPositionsChildGrid != null) {
        	searchPositionsChildGrid.resizeCanvas();
        }

  }

  function _resizeCanvasOnGridChange(gridName, gridId) {
    if (gridId != undefined) {
        $('#' + gridId).height($('#' + gridId).height() + 10);
    }
    if(gridName != null) {
      gridName.resizeCanvas();
    }
  }

  function _getFormattedMessage(message) {
      return "<center><div class='message no-icon' style='width: 30%;'>" + message + "</div></center>";
  }

  function _getFormattedData(value, precision) {
    if(value == null || value == "") {
      return "";
    } else {
      return parseFloat(value).toFixed(precision);
    }
  }

  function _isDefinedAndValid(data)
  {
      if(data != "" && data != null && data != undefined)
      {
          return true;
      }
      return false;
  }

  function _renderDialogBox(dialogBoxElement, dialogBoxTitle, configName) {
    document.getElementById(dialogBoxElement).removeAttribute("hidden");
    document.getElementById(dialogBoxElement).reveal({
    	title: dialogBoxTitle + " Configuration : " + configName,
        modal: true,
        sticky: true,
        width: '550px',
        dismissible: 'true'
    });
  }
})();
