"use strict";

var searchPositionsGrid;
var selectedPositionsGrid;
var url;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.margin = window.treasury.margin || {};
  window.treasury.margin.simulationActions = {
    loadSearchPositionsData: _loadSearchPositionsData,
    loadSelectedPositionsData :_loadSelectedPositionsData,
    uploadFiles : _uploadFiles,
    addPositionRow : _addPositionRow,
    fetchPositionPrice : _fetchPositionPrice,
    fetchFxRate: _fetchFxRate
  };

  function _fetchFxRate(price, callback){
    let params = {};
    params.reportingCurrencyId = document.getElementById('reportingCurrencyFilter').value.key;
    params.dateParam = $("#datePicker").val();
    treasury.margin.common.showLoading("mainLoader");

    $
				.ajax({
					url : "/treasury/fetch-fx-rate",
					data : params,
					type : "GET",
					dataType : "json",
					error: function(response, data) {
						ArcMessageHelper.showMessage('ERROR', "Fx Rate is not available for the selected reporting currency.");
						treasury.margin.common.hideLoading("mainLoader");
						return;
					},
					success : function(data) {
						// if no data
						if (data == null || data["result"] == undefined || data["result"] === "0") {
							treasury.margin.common.hideLoading("mainLoader");
							return;
						}
						let fxRate;
						fxRate = data["result"];
						treasury.margin.common.hideLoading("mainLoader");
						callback(price,fxRate);
					},
					timeout : 120000
				});
  }

  function _addPositionRow(data){
    if(searchPositionsGrid == undefined){
      treasury.margin.util.clearGrid('searchPositionsGrid');
      document.getElementById('searchPositionsGridHeader').removeAttribute("hidden");
      document.getElementById('searchMessage').setAttribute("hidden", true);
      let columns;
      if (treasury.defaults.enableReportingCurrencyFilter) {
        columns =
          treasury.margin.simulationColumnConfig.getSearchPositionsColumns(
            true
          );
      } else {
        columns =
          treasury.margin.simulationColumnConfig.getSearchPositionsColumns();
      }
      var options = treasury.margin.simulationGridOptions.getSearchPositionsOptions();
      searchPositionsGrid = new dportal.grid.createGrid($("#searchPositionsGrid"), data,columns,options);
      treasury.margin.util.resizeCanvasOnGridChange(searchPositionsGrid,'searchPositionsGrid');
      $("#actionMessage").text('Select atleast one position and click next to add margin term.')
    }
    else{
      searchPositionsGrid.dataView.addItem(data[0]);
      treasury.margin.util.resizeCanvasOnGridChange(searchPositionsGrid,'searchPositionsGrid');
    }
  }

  function _uploadFiles(sourceElementId) {
    // validate input before uploading
	  var inputElement = document.getElementById(sourceElementId);
	  var errorMsg = '';

	  if (typeof FormData === "undefined" && typeof FileReader === "undefined") {
	    ArcMessageHelper.showMessage("WARNING", "Browser does not Support required API to Upload");
	    return;
	  }

	  if(treasury.defaults.enableReportingCurrencyFilter &&
	    document.getElementById('reportingCurrencyFilter').value == null){
        ArcMessageHelper.showMessage('ERROR', "Please select reporting currency.");
        return;
      }

	  var file = files[0];
	  var formData = new FormData();
	  formData.append("uploadedFile", file);
	  formData.append("fileName", file.name);
	  formData.append("dateParam", $("#datePicker").val());
	  if(treasury.defaults.enableReportingCurrencyFilter){
          formData.append("reportingCurrencyId", reportingCurrencySingleSelect.getSelectedId());
      }
      treasury.margin.common.showLoading("mainLoader");
	  var xhr = new XMLHttpRequest();
	  xhr.open("POST", "/treasury/upload-positions", true);
	  xhr.responseType = 'json';
      xhr.send(formData);
	  xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && xhr.status == 200) {
	      var data = xhr.response;
	      var errorDetails;

           if (treasury.defaults.enableReportingCurrencyFilter
           	    && data !== null && data["metaData"] !== undefined
           	    && data["metaData"]["fxRateNotFound"] !== undefined) {
                    var errorMsg = "FxRate is not available for selected reporting currency : "+
                          document.getElementById('reportingCurrencyFilter').value.value.split('[')[0];
                    treasury.margin.ssu.util.raiseErrorToast(errorMsg);
                    treasury.margin.common.hideLoading("mainLoader");
                    return;
           }

	      //Reporting errors in Uploaded file
	      if (data !== null && data["metaData"] !== undefined
	        && data["metaData"]["errorDetails"] !== undefined ) {
	              errorDetails = data["metaData"]["errorDetails"];
	              var errorMsg = "The position file had errors!!\n";
	              for ( var property in errorDetails){
	            	  if(errorDetails.hasOwnProperty(property)){
                          var logList = errorDetails[property];
                          for( var i=0; i<logList.length; i++)
                          {
                              errorMsg += logList[i];
                              errorMsg += "\n";
                          }
                      }
	              }
                  treasury.margin.common.hideLoading("mainLoader");
	              treasury.margin.ssu.util.raiseErrorToast(errorMsg);
	              document.getElementById('fileBrowser').value = "";
	              $('#uploadBtn').attr('disabled' ,true);

	              // return;
	      }

	      //If no uploaded position exists in system
	      if (data == null || data["resultList"] == undefined || data["resultList"].length == 0) {
	        treasury.margin.util.clearGrid('searchPositionsGrid');
	        treasury.margin.common.hideLoading("mainLoader");
	        treasury.margin.ssu.util.raiseErrorToast("No positions available for this search criteria.");
	        document.getElementById('fileBrowser').value = "";
	        $('#uploadBtn').attr('disabled' ,true);
	        return;
	      }

	      //Truncating grid
          if(data["recordsTruncated"] == true){
              var actualRowCountFormatted = data["actualRowCount"];
              var truncatedRowCountFormatted = data["truncatedRowCount"];
              var message = "Search returned too many rows(" + actualRowCountFormatted + "). Truncating it to " + truncatedRowCountFormatted;
              document.getElementById('searchMessage').removeAttribute("hidden");
              document.getElementById('searchMessage').innerHTML = "<span><font color='#FF0000'>" + message + "</font></span>";
          }
          //Displaying info
          showInfoTabs($("#datePicker").val(),data["actualRowCount"], data["truncatedRowCount"]);
	      //Rendering grid
	      treasury.margin.common.hideLoading("mainLoader");
	      treasury.margin.ssu.util.raiseInfoToastWithTimeOut("Position File parsed successfully!!", 1000);

          treasury.margin.util.clearGrid('searchPositionsGrid');
           if(treasury.defaults.enableReportingCurrencyFilter){
                $("#searchMessage").text('All the values are shown in ' +
                document.getElementById('reportingCurrencyFilter').value.value.split('[')[0]);
                document.getElementById('searchMessage').removeAttribute('hidden');
           }else{
                document.getElementById('searchMessage').setAttribute("hidden", true);
           }

	      document.getElementById('searchPositionsGrid').removeAttribute("hidden");
          document.getElementById('searchPositionsGridHeader').removeAttribute("hidden");
	      var columns;
	      if (treasury.defaults.enableReportingCurrencyFilter){
	        columns =  treasury.margin.simulationColumnConfig.getSearchPositionsColumns(true);
	      }else{
	        columns =  treasury.margin.simulationColumnConfig.getSearchPositionsColumns();
	      }
	      var options = treasury.margin.simulationGridOptions.getSearchPositionsOptions();
	      searchPositionsGrid = new dportal.grid.createGrid($("#searchPositionsGrid"), data.resultList,columns,options);
		  treasury.margin.util.resizeCanvasOnGridChange(searchPositionsGrid,'searchPositionsGrid');
          $("#actionMessage").text('Select atleast one position and click next to add margin term.');
          document.getElementById('fileBrowser').value = "";
          $('#uploadBtn').attr('disabled' ,true);
	    }
	    else if(xhr.readyState == 4 && xhr.status != 200)
	    {
	        treasury.margin.util.clearGrid('searchPositionsGrid');
	        treasury.margin.common.hideLoading("mainLoader");
	        treasury.margin.ssu.util.raiseErrorToast("Error occurred while parsing the file");
	        document.getElementById('fileBrowser').value = "";
	        $('#uploadBtn').attr('disabled' ,true);
	    }
	  };
	}


    function showInfoTabs(date, actualRowCount, truncatedRowCount){
      document.getElementById('searchPositionsGridHeader').removeAttribute("hidden");
      var searchDate = document.getElementById("searchDate");
      searchDate.innerHTML = date;
      var noOfRecords =document.getElementById("noOfRecords");
      noOfRecords.innerHTML = actualRowCount;
      document.getElementById('quickLinks').removeAttribute("hidden");
      //Truncating grid
      if(actualRowCount > truncatedRowCount){
          var message = "Search returned too many rows(" + actualRowCount + "). Truncating it to " + truncatedRowCount;
          document.getElementById('searchMessage').removeAttribute("hidden");
          document.getElementById('searchMessage').innerHTML = "<span><font color='#FF0000'>" + message + "</font></span>";
      }
    }

  function _loadSearchPositionsData(params) {
	  //setting spn
	  var spns = $("#searchPositionPnlSpns").val();
	  if (spns != "") {
          spns = spns.trim().replace(/,+$/, "");
          params["spns"] = spns;
    }
	  params["dateParam"] = params.dateString;
	  if(params.cpeIds              == -1 &&
	     params.agreementTypeIds    == -1 &&
	     params.descoEntityIds      == -1 &&
	     params.marginCalculatorIds == -1 &&
	     params.bookIds 			== -1 &&
	     params.spns                == undefined)
	   {
	       ArcMessageHelper.showMessage('ERROR', "All-All search is not supported. Please select atleast one filter.");
	       return;
	   }
	   if(treasury.defaults.enableReportingCurrencyFilter && params.reportingCurrencyId == -1){
       	       ArcMessageHelper.showMessage('ERROR', "Please select reporting currency.");
                  return;
       }

	  var serviceCall = $.ajax({
	    url: '/treasury/search-positions',
	    type: "POST",
	    data: params
	  });
	  treasury.margin.util.clearGrid('searchPositionsGrid');
    treasury.margin.common.showLoading("mainLoader");
	    $.when(serviceCall)
	      .done(
	        function(data) {
	          // window.treasury.margin.simulationLanding.setFilters(params);
	          var dateFilterValue = Date.parse(params["dateString"]);
	          var minDate = Date.parse('1970-01-01');
	          var maxDate = Date.parse('2038-01-01');
	          var dateFormat = 'yy-mm-dd';
			  treasury.margin.util.setupDateFilter(
					$("#datePicker"), $("#datePickerError"),
					dateFilterValue, minDate, maxDate,dateFormat);

	            if (data && data.resultList && data.resultList.length) {
                document.getElementById('searchPositionsGridHeader').removeAttribute("hidden");
                if(data.metaData != null){
                    $("#searchMessage").text('FxRate is not available for given reporting currency : '+
                                document.getElementById('reportingCurrencyFilter').value.value.split('[')[0]);
                    document.getElementById('searchPositionsGridHeader').setAttribute('hidden',true);
                    document.getElementById('searchMessage').removeAttribute('hidden');
                    return;
                }else if(treasury.defaults.enableReportingCurrencyFilter){
                    $("#searchMessage").text('All the values are shown in ' +
                                document.getElementById('reportingCurrencyFilter').value.value.split('[')[0]);
                    document.getElementById('searchMessage').removeAttribute('hidden');
                }else{
                     document.getElementById('searchMessage').setAttribute("hidden", true);
                }
  	        	  var searchDate = document.getElementById("searchDate")
                searchDate.innerHTML = params.dateString;
                var noOfRecords =document.getElementById("noOfRecords")
                noOfRecords.innerHTML = data.length;
                document.getElementById('quickLinks').removeAttribute("hidden");
                showInfoTabs($("#datePicker").val(),data["actualRowCount"], data["truncatedRowCount"]);
                var clientName = document.getElementById("clientName").value;
                var columns = {};
                if(clientName == 'baam')
                {
                    columns =
                        treasury.margin.simulationColumnConfig.getSearchPositionsColumnsForCashMargin();
                }
                else if (treasury.defaults.enableReportingCurrencyFilter){
                    columns =
                        treasury.margin.simulationColumnConfig.getSearchPositionsColumns(true);
                }
                else
                {
                    columns =
                        treasury.margin.simulationColumnConfig.getSearchPositionsColumns();
                }
                var options = treasury.margin.simulationGridOptions.getSearchPositionsOptions();
  	        	  searchPositionsGrid = new dportal.grid.createGrid(
  	                    $("#searchPositionsGrid"), data.resultList,
  	                      columns,options);
	              treasury.margin.util.resizeCanvasOnGridChange(searchPositionsGrid,'searchPositionsGrid');
                $("#actionMessage").text('Select atleast one position and click next to add margin term.')
	              treasury.margin.common.hideLoading("mainLoader");
	          } else {
	            treasury.margin.common.hideLoading("mainLoader");
	            treasury.margin.util.clearGrid("searchPositionsGrid");
              document.getElementById('searchPositionsGridHeader').setAttribute('hidden',true);
              $("#searchMessage").text('No records found for search criteria');
	            document.getElementById('quickLinks').setAttribute("hidden",true);
              document.getElementById('searchMessage').removeAttribute('hidden');
	          }
	      });

      serviceCall
        .fail(function(xhr, text, errorThrown) {
          treasury.margin.common.hideLoading("mainLoader");
          treasury.margin.util
            .showErrorMessage('Error', 'Error occurred while retrieving data.');
        });
    }


   function _fetchPositionPrice(params,callback){
    var price = '';
    var serviceCall = $.ajax({
          	    url: '/treasury/fetch-position-price',
          	    type: "POST",
          	    data: params
          	  });
        $.when(serviceCall)
        	      .done(function(data){
        if (data && data.result){
            price = data.result;
        }
        if(callback){
          	callback(price);
        }
   });
 }


    function isInArray(value, array) {
      return array.indexOf(value) > -1;
    }

    function _getSelectedPositionsData(){
      var selectedPositionsData = [];
      if(searchPositionsGrid != undefined && searchPositionsGrid.data!=undefined){
        var positions = searchPositionsGrid.data;
        for(var i=0; i<positions.length; i++){
          var position = positions[i];
          if(isInArray(position.id, selectedPositions)){
            selectedPositionsData.push(position);
          }
        }
      }
      return selectedPositionsData;
    }

  	//To display the grid of only the selected positions on step 2 (add-margin-terms)
  	function _loadSelectedPositionsData(){
      treasury.margin.util.clearGrid('selectedPositionsGrid');
      document.getElementById('searchMessage').setAttribute("hidden", true);
      document.getElementById('selectedPositionsGrid').removeAttribute("hidden");
      document.getElementById('selectedPositionsGridHeader').removeAttribute("hidden");
      var data = _getSelectedPositionsData();
      var clientName = document.getElementById("clientName").value;
      var columns = {};
      if(clientName === 'baam')
      {
          columns =  treasury.margin.simulationColumnConfig.getSelectedPositionsColumnsForCashMargin();
      }
      else if (treasury.defaults.enableReportingCurrencyFilter){
          columns =   treasury.margin.simulationColumnConfig.getSearchPositionsColumns(true);
      }
      else
      {
          columns =  treasury.margin.simulationColumnConfig.getSelectedPositionsColumns();
      }
      var options = treasury.margin.simulationGridOptions.getSelectedPositionsOptions();
      selectedPositionsGrid = new dportal.grid.createGrid($("#selectedPositionsGrid"), data,columns,options);
      treasury.margin.util.setSelectedPosGridHeight();
  	}
})();
