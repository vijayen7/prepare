"use strict";

(function() {

    window.treasury = window.treasury || {};
    window.treasury.margin = window.treasury.margin || {};
    window.treasury.margin.widget = {
        hideSteps : hideSteps,
        selectPositions: selectPositions,
        addMarginTerms: addMarginTerms,
        viewResult: viewResult,
        goBack: goBack,
        goNext: goNext,
        goToStep : _goToStep,
        renderSimulationResultGrid : _renderSimulationResultGrid

    };

    function hideSteps() {
        //Filter
        $("div#step1").addClass("hidden");
        $("div#step2").addClass("hidden");
        $("div#step3").addClass("hidden");

        //Color of selected widget
        $("#wizard_step1").removeClass("rule-wizard__step--active");
        $("#wizard_step2").removeClass("rule-wizard__step--active");
        $("#wizard_step3").removeClass("rule-wizard__step--active");

        //Info tabs
        document.getElementById('quickLinks').setAttribute("hidden", true);
        document.getElementById('actionMessage').setAttribute("hidden", true);
        document.getElementById('searchMessage').setAttribute("hidden", true);

        //Main area grid/content
        document.getElementById('searchPositionsGridHeader').setAttribute("hidden", true);
        document.getElementById('selectedPositionsGridHeader').setAttribute("hidden", true);
        document.getElementById('simResultsMainGridHeader').setAttribute("hidden", true);

        //widget buttons
        document.getElementById('wizard_step1').removeAttribute("disabled");
        document.getElementById('wizard_step2').removeAttribute("disabled");
        document.getElementById('wizard_step3').removeAttribute("disabled");

        //Action buttons
        document.getElementById('nextId').setAttribute("disabled",true);
        document.getElementById('backId').setAttribute("disabled",true);
        $("#backId").hide();
        $("#nextId").hide();
    }

    function selectPositions() {
        hideSteps();
        $("#nextId").show();
        document.getElementById('wizard_step2').setAttribute("disabled", true);
        document.getElementById('wizard_step3').setAttribute("disabled", true);

        if (!treasury.margin.util.isGridEmpty(searchPositionsGrid)) {
            $("#actionMessage").text('Select atleast one position and click next to add margin term.');
            document.getElementById('searchMessage').removeAttribute("hidden");
            document.getElementById('searchPositionsGridHeader').removeAttribute("hidden");
            document.getElementById('quickLinks').removeAttribute("hidden");
        }
        else{
          $("#actionMessage").text('Search positions using filters or \
                                    Upload a position file (in given template)\
                                    or Manually add a position.');
        }
        document.getElementById('actionMessage').removeAttribute("hidden");
        $("div#step1").removeClass("hidden");
        $("#wizard_step1").addClass("rule-wizard__step--active");

        //enable next button only if some positions have been selected
        if (typeof selectedPositions !== 'undefined' && selectedPositions.length > 0) {
            document.getElementById('nextId').removeAttribute("disabled");
            document.getElementById('wizard_step2').removeAttribute("disabled");
        }
    }

    function addMarginTerms() {
        hideSteps();
        $("#backId").show();
        $("#nextId").show();
        document.getElementById('wizard_step3').setAttribute("disabled", true);

        $("#actionMessage").text('Add atleast one margin term and click Next to run simulation');
        document.getElementById('actionMessage').removeAttribute("hidden");
        treasury.margin.simulationActions.loadSelectedPositionsData();
        $("div#step2").removeClass("hidden");
        $("#wizard_step2").addClass("rule-wizard__step--active");
        if (!treasury.margin.util.isGridEmpty(selectedPositionsGrid)) {
            document.getElementById('selectedPositionsGridHeader').removeAttribute("hidden");
        }
        if (!treasury.margin.util.isMapEmpty(marginTermsData)) {
            document.getElementById('nextId').removeAttribute("disabled");
            document.getElementById('wizard_step3').removeAttribute("disabled");
        }
        document.getElementById('backId').removeAttribute("disabled");
    }

    function viewResult() {
        hideSteps();
        $("#backId").show();
        $("#wizard_step3").addClass("rule-wizard__step--active");
        treasury.margin.runSimulation.loadResult();
    }

    function _renderSimulationResultGrid(){
        $("div#step3").removeClass("hidden");
        if (!treasury.margin.util.isMapEmpty(marginTermsData) &&
            !treasury.margin.util.isGridEmpty(simResultsMainGrid)) {
            document.getElementById('simResultsMainGridHeader').removeAttribute("hidden");
        }
        document.getElementById('backId').removeAttribute("disabled");
    }

    function _goToStep(target){
      var curr = getCurrentStep();
      switch (curr) {
        case 1:
          if(target==2) addMarginTerms();
          if(target==3) console.log('Cant jump from 1 to 3');
          break;
        case 2:
          if(target==1) selectPositions();
          if(target==3) viewResult();
          break;
        case 3:
          if(target==2) addMarginTerms();
          if(target==1) {addMarginTerms(); selectPositions();}
          break;
      }
    }

    function goBack() {
        var curr = getCurrentStep();
        switch (curr) {
            case 1:
                break;
            case 2:
                selectPositions();
                break;
            case 3:
                addMarginTerms();
                break;
        }
    }

    function goNext() {
        var curr = getCurrentStep();
        switch (curr) {
            case 1:
                addMarginTerms();
                break;
            case 2:
                viewResult();
                break;
            case 3:
                break;
        }
    }

    function getCurrentStep() {
        if (!$("div#step1").hasClass("hidden")) {
            return 1;
        }
        if (!$("div#step2").hasClass("hidden")) {
            return 2;
        }
        if (!$("div#step3").hasClass("hidden")) {
            return 3;
        }
    }

})();
