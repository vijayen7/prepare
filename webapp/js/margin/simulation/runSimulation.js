"use strict";
var resultList;
var childDisplayParams;
var simResultsMainGrid;
var methodologySummaryGrid;
var reportURIs = [];
var reportOutputFormats = [];
var calcNames = [];
var isSimulationSuccessful = [];

(function() {
  window.treasury = window.treasury || {};
	window.treasury.margin = window.treasury.margin || {};
	window.treasury.margin.runSimulation = {
    registerHandlers : _registerHandlers,
    loadResult : _loadResult,
    loadMarginSummaryDetails : _loadMarginSummaryDetails,
    resizeAllCanvas : _resizeAllCanvas,
    downloadMarginReport : _downloadMarginReport
	};

	window.addEventListener("WebComponentsReady", _initialize);

  function _initialize() {
		_registerHandlers();
  }

  function _registerHandlers() {
    document.getElementById('methodologyDetailsArea').addEventListener("stateChange",
                                                      treasury.margin.runSimulation.resizeAllCanvas);
	}

  function _loadMarginSummaryDetails(params) {
      var gridDisplayParams;
      $("#positionMarginArea").hide();
      $("#intermediateDataArea").hide();
      $("#methodologyDetailsArea").hide();

      if (resultList != null) {
          var calculatorData = resultList[params['index']]['intermediateJSON'];

          // The id scheme used below is neccessary to support dportal grid framework.
          if(calculatorData != null) {
              gridDisplayParams = getSimulatedDataSummaryParams(calculatorData['data']);
              gridDisplayParams['id'] = params['index'];

              if(calculatorData['children'] != null) {
                  var childCalculatorData = calculatorData['children'];
                  gridDisplayParams['children'] = [];

                  for(var i=0; i < childCalculatorData.length ; ++i) {
                      childDisplayParams = getSimulatedDataSummaryParams(childCalculatorData[i]['data']);
                      childDisplayParams['masterDataIndex'] = params['index'];
                      gridDisplayParams['children'][i]      = childDisplayParams;
                      gridDisplayParams['children'][i]['id']= i+childDisplayParams['masterDataIndex']+1;
                  }
              }
          }

          if(gridDisplayParams !== undefined) {
                loadCalculatorLevelBreakUp(gridDisplayParams,treasury.defaults.enableReportingCurrencyFilter);
          }
      }
  }

  function getSimulatedDataSummaryParams(calculatorData) {
        var gridDisplayParams = {};
        gridDisplayParams['lmv'] = calculatorData['lmv'];
        gridDisplayParams['smv'] = calculatorData['smv'];
        gridDisplayParams['lov'] = calculatorData['lov'];
        gridDisplayParams['sov'] = calculatorData['sov'];
        gridDisplayParams['name'] = calculatorData['calculator_name'];
        gridDisplayParams['margin'] = calculatorData['margin'];
        return gridDisplayParams;
  }

  function loadCalculatorLevelBreakUp(calculatorData, showDataInRC){
    var columns = treasury.margin.simulationColumnConfig.getCalculatorLevelBreakUpColumns(calculatorData, showDataInRC);
    var options = treasury.margin.simulationGridOptions.getCalculatorLevelBreakUpOptions(columns, showDataInRC);

    methodologySummaryGrid = new dportal.grid.createGrid($("#methodologySummaryGrid"), [calculatorData], columns, options);
    document.getElementById('methodologySummaryArea').label =
      'Methodology Summary of ' + calculatorData['name'] + ' [Margin Term ' + (calculatorData['id']+1) + ']';
    document.getElementById("methodologySummaryArea").removeAttribute("hidden");
    $("#methodologySummaryArea").show();
    methodologySummaryGrid.resizeCanvas();
  }

  function _verify(){
    if (selectedPositions.length == 0) {
        ArcMessageHelper.showMessage('error', "Atleast one position should be selected.");
        return 0;
    }
    if(treasury.margin.util.isMapEmpty(marginTermsData)) {
      ArcMessageHelper.showMessage('error', "No terms added to simulate.");
      return 0;
    }
    return 1;
  }

  function _loadResult(){
    if(!_verify()){
      treasury.margin.widget.renderSimulationResultGrid();
      return;
    }
    document.getElementById("simResultsMainGridHeader").setAttribute("hidden",true);
    document.getElementById("positionMarginArea").setAttribute("hidden",true);
    document.getElementById("intermediateDataArea").setAttribute("hidden",true);
    document.getElementById("methodologyDetailsArea").setAttribute("hidden",true);
    var params = {};
    var positionList = [];
    if (searchPositionsGrid.dataView.getItems().length > 0) {
          var searchedPositionsList = searchPositionsGrid.dataView.getItems();
          for (var i = 0; i < searchedPositionsList.length; i++) {
              if (selectedPositions.indexOf(searchedPositionsList[i]["id"]) >= 0) {
                  positionList.push(searchedPositionsList[i]);
              }
          }
          params.positionsList = _getPositionJsonForSimulation(positionList);
      }

      var marginTermsArray = [];
      for(var key in marginTermsData){
        if(marginTermsData.hasOwnProperty(key)){
          marginTermsArray.push(marginTermsData[key]);
        }
      }
      params.marginUnitList = JSON.stringify(marginTermsArray);
      params.dateParam = $("#datePicker").val();
      var clientName = document.getElementById("clientName").value;
      var simResultsColumns = {};

      //We need to display Cash Margin and Financed Margin for BAAM
      if(clientName === 'baam')
      {
          simResultsColumns =  treasury.margin.simulationColumnConfig.getSimResultsColumnsForCashMargin();
      }
      else if(treasury.defaults.enableReportingCurrencyFilter)
      {
          params.reportingCurrencyId = reportingCurrencySingleSelect.getSelectedId();
          simResultsColumns =  treasury.margin.simulationColumnConfig.getSimResultsColumns(true);
      }
      else
      {
          simResultsColumns =  treasury.margin.simulationColumnConfig.getSimResultsColumns();
      }

      var simResultsOptions = treasury.margin.simulationGridOptions.getSimResultsOptions();
      treasury.margin.common.showLoading("mainLoader");
      var serviceCall = $.ajax({
  	    url: '/treasury/simulate-margin',
  	    type: "POST",
  	    data: params
	    });
      $.when(serviceCall)
       .done(
         function(data) {
           resultList = data["resultList"];
           var i;

           document.getElementById('simResultsMainGridHeader').removeAttribute("hidden");
           if(treasury.defaults.enableReportingCurrencyFilter){
                $("#simResultSearchMessage").text('All the values are shown in ' +
                           document.getElementById('reportingCurrencyFilter').value.value.split('[')[0]);
                document.getElementById('simResultSearchMessage').removeAttribute('hidden');
           }else{
                document.getElementById('simResultSearchMessage').setAttribute("hidden",true);
           }

           simResultsMainGrid = new dportal.grid.createGrid($("#simResultsMainGrid"), data["resultList"],simResultsColumns,simResultsOptions);
           $("#methodologySummaryArea").hide();
           simResultsMainGrid.resizeCanvas();
           for(i=0; i < resultList.length; i++)
           {
               var row = resultList[i];
               reportURIs[i] = row["reportURI"];
               calcNames[i] = row["calculator"];
               reportOutputFormats[i] = row["reportOutputFormat"];
               isSimulationSuccessful[i] = false;
               if(("reportURI" in row) && row["reportURI"] !== "" )
               {
                   isSimulationSuccessful[i] = true;
               }
               else
               {
            	   document.getElementById('report_' + i).setAttribute('disabled',true);
               }
           }
           treasury.margin.widget.renderSimulationResultGrid();
           treasury.margin.common.hideLoading("mainLoader");
         });

     serviceCall
         .fail(function(xhr, text, errorThrown) {
           treasury.margin.common.hideLoading("mainLoader");
           treasury.margin.util
             .showErrorMessage('Error', 'Error occurred while running simulation.');
     });
  }

  function _getPositionJsonForSimulation(positionList){
    var positions = [];
    $.each(positionList, function(i, position) {
        var pos = {};
        pos["pnl_spn"] = position.pnl_spn;
        pos["price"] = position.price;
        pos["priceXtick"] = position.priceXtick;
        pos["quantity"] = position.quantity;
        pos["ccy_spn"] = position.ccy_spn;
        pos["book"] = position.book;
        pos["book_id"] = position.book_id;
        pos["cash_quantity"] = position.cash_quantity;
        pos["cash_margin_usd"] = position.cash_margin_usd;
        if(position.price != 0)
        {
            pos["tick"] = position.priceXtick / position.price;
        }
        if(pos['tick']==undefined||pos['tick']==null||isNaN(pos['tick'])){
          pos['tick']=1;
        }
        positions.push(pos);
    });
    return JSON.stringify(positions);
  }

  function _resizeAllCanvas() {
   var methodologyDetailsArea = document.getElementById('methodologyDetailsArea');
   if (methodologyDetailsArea!=null && methodologyDetailsArea.state == 'expanded') {
     methodologyDetailsArea.style.width = '400px';

   } else if (methodologyDetailsArea!=null){
     methodologyDetailsArea.style.width = '20px';
   }

   if(simResultsMainGrid != null){
     simResultsMainGrid.resizeCanvas();
   }

   if (methodologySummaryGrid != null) {
     methodologySummaryGrid.resizeCanvas();
   }
   if (positionMarginGrid != null) {
     positionMarginGrid.resizeCanvas();
   }
   if (intermediateDataGrid != null) {
     intermediateDataGrid.resizeCanvas();
   }
 }

  function _downloadMarginReport(params)
  {
	  var reportURI = reportURIs[params];
	  var reportName = calcNames[params];
	  var reportOutputFormat = reportOutputFormats[params];
	  if(isSimulationSuccessful[params])
	  {
		  window.open('downloadSimulationReport?fileName='+ reportName +'.'+ reportOutputFormat + '&contentType=&fileUrl='+reportURI,'_blank');
	  }
  }


})();