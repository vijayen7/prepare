"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.margin = window.treasury.margin || {};
  window.treasury.margin.simulationGridOptions = {
    getSearchPositionsOptions: _getSearchPositionsOptions,
    getSelectedPositionsOptions: _getSelectedPositionsOptions,
    getMarginTermsOptions: _getMarginTermsOptions,
    getSimResultsOptions : _getSimResultsOptions,
    getCalculatorLevelBreakUpOptions : _getCalculatorLevelBreakUpOptions
  };

  function _getSearchPositionsOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      highlightRow: true,
      highlightRowOnClick : true,
      displaySummaryRow : true,
      maxHeight : 600,
      changeCellStyleOnEdit: true,
      editable: {
        changeCellStyleOnEdit: true
      },
      applyFilteringOnGrid: true,
      exportToExcel: true,
      sheetName: "Search Positions",
      checkboxHeader:true,
      rowSelection:{
    	  highlightRowOnClick: true
      },
      isFilterOnFormattedValue: true,
      onSelectedRowsChanged : function(selectedIds){
        selectedPositions = selectedIds;
        //enable next button if atleast one position is selected
        if (selectedPositions.length > 0) {
            document.getElementById('nextId').removeAttribute("disabled");
            document.getElementById('wizard_step2').removeAttribute("disabled");
        }
        else{
          document.getElementById('nextId').setAttribute("disabled",true);
          document.getElementById('wizard_step2').setAttribute("disabled",true);
        }
      },
    };
  }

function _getSelectedPositionsOptions(){
  var options = _getSearchPositionsOptions();
  options.checkboxHeader = false;
  options.maxHeight = 350;
  return options;
}

function _getMarginTermsOptions(){
  var options = {
    autoHorizontalScrollBar: true,
    applyFilteringOnGrid : true,
    showHeaderRow : true,
    maxHeight:200,
    copyCellSelection : true,
    deleteRow : true,
    onDeleteRow : function(){
      var marginTerms = marginTermsGrid.dataView.getItems();
      //enable next button if atleast one margin term is added
      if (marginTerms.length > 0) {
          document.getElementById('nextId').removeAttribute("disabled");
      }
      else{
        document.getElementById('nextId').setAttribute("disabled",true);
      }
    },
  };
  return options;
}

function _getSimResultsOptions(){
  var options = {
     maxHeight : 500,
     autoWidth : true,
     forceFitColumns : true,
     exportToExcel : true,
     autoHorizontalScrollBar: true,
     sheetName : "Simulation Result data"
  };
  return options;
}

function _getCalculatorLevelBreakUpOptions(columns,gridData,showDataInRC){
  var options = {
      nestedTable: true,
      nestedField: columns[0].field,
      expandTillLevel: -1,
      expandCollapseAll : true,
      applyFilteringOnGrid : true,
      showHeaderRow : true,
      forceFitColumns:true,
      summaryRow : true,
      displaySummaryRow:true,
      summaryRowText : "Total",
      exportToExcel : true,
      sheetName : "Methodology Margin",
      sortList : [{columnId:"name", sortAsc:true},
                 ],
      onRender : function() {
          $(document).off("click", ".treasury-drill-through")
      },
      onCellClick : function(args){
        // Hide existing position area, intermediate area and methodology area
        $("#positionMarginArea").hide();
        $("#intermediateDataArea").hide();
        $("#methodologyDetailsArea").hide();
        var gridData = args.item;
        var calcData;
        var masterDataIndex = gridData["masterDataIndex"];

        // The id scheme below is coupled with the one in .margin-drill-through click listener
        // and hence should be updated with respect to it.
        if( masterDataIndex != null )
        {
            var childId = gridData["id"] - masterDataIndex - 1;
            calcData = resultList[masterDataIndex]["intermediateJSON"]["children"][childId];
        } else {
            calcData = resultList[gridData["id"]]["intermediateJSON"];
        }
        var params = {};
        params["data"] = calcData;
        params["methodologyName"] = gridData["name"];
        treasury.margin.intermediateData.Actions.fetchExtendedMethodDetails(params,showDataInRC);
        simResultsMainGrid.resizeCanvas();
      }

    };
  return options;
}
})();
