"use strict";
var cpeMultiSelect;
var legalEntityMultiSelect;
var agreementTypeMultiSelect;
var marginCalculatorMultiSelect;
var bookMultiSelect;
var bookSingleSelect;
var booksAllData;
var searchPositionsFilterGroup;
var files;
var selectedPositions = [];
var reportingCurrencySingleSelect = null;

(function() {
	window.treasury = window.treasury || {};
	window.treasury.margin = window.treasury.margin || {};
	window.treasury.margin.simulationLanding = {
		copySearchUrl : _copySearchUrl,
		loadFilters : _loadFilters,
		registerHandlers : _registerHandlers,
		resetFilters : _resetFilters,
		setFilters : _setFilters,
		loadSearchResults : _loadSearchResults,
		selectFiles : _selectFiles,
		autoCompleteSecuritySearch : _autoCompleteSecuritySearch,
		addPosition : _addPosition,
		loadUploadedPositions : _loadUploadedPositions,
		searchBookName : _searchBookName
	};

	var legalEntityAllDataMap = [];
	window.addEventListener("WebComponentsReady", _initialize);

	function _initialize() {
		$(document).on({
			ajaxStart : function() {
				treasury.margin.common.showLoading("mainLoader");
			},
			ajaxStop : function() {
				if (!treasury.margin.marginTerms.marginTermsAvailable()) {
					treasury.margin.widget.hideSteps();
					$("div#noMarginTerms").removeClass("hidden");
				}
				treasury.margin.common.hideLoading("mainLoader");
			}
		});
		var clientName = document.getElementById("clientName").value;
		if (clientName === "desco" || clientName === "deshaw") {
			document.getElementById('useReconDiv').removeAttribute("hidden");
		}
		var saveSettings = document.querySelector('arc-save-settings');
		var host = '';
		var stabilityLevel = document.getElementById("stabilityLevel").value;

		if (clientName === 'desco') {
			if (stabilityLevel == 'prod') {
				host += "http://landing-app.deshaw.c.ia55.net";
			} else if (stabilityLevel == 'uat') {
				host = "http://landing-app.deshawuat.c.ia55.net";
			} else if (stabilityLevel == 'qa') {
				host = "https://mars.arcesium.com";
			} else if (stabilityLevel == 'dev') {
				host = "https://terra.arcesium.com";
			}
		}
		saveSettings.serviceURL = host + '/service/SettingsService';
		saveSettings.applicationId = 3;
		saveSettings.applicationCategory = 'MarginSimulation';

		document.getElementById('summary-search-save').onclick = function() {
			saveSettings.openSaveDialog();
		};

		saveSettings.saveSettingsCallback = function() {
			var parameters = searchPositionsFilterGroup
					.getSerializedParameterMap();
			return parameters;
		}
		saveSettings.applySettingsCallback = function(parameters) {
			_setFilters(parameters);
		}
        if( treasury.defaults.enableReportingCurrencyFilter ){
            document.getElementById('reportingCurrency').removeAttribute("hidden");
            document.getElementById("priceXTickLabel").innerHTML = "PriceXTick(RC) :";
        }
		treasury.margin.widget.selectPositions();
		treasury.margin.simulationLanding.registerHandlers();
		treasury.margin.simulationLanding.loadFilters();
		treasury.margin.marginTerms.loadFilters();
		treasury.margin.marginTerms.registerHandlers();
	}

	function _registerHandlers() {
		// event listener for security search
		document.getElementById("searchPositionPnlSpns").oninput = _autoCompleteSecuritySearch;
		document.getElementById("addPosnSpn").oninput = _autoCompleteSecuritySearch;
		document.getElementById('searchId').onclick = treasury.margin.simulationLanding.loadSearchResults;
		document.getElementById('clearSearchPosBtn').onclick = treasury.margin.simulationLanding.resetFilters;

		// save SettingsService
		document.getElementById('copySearchURL').onclick = treasury.margin.simulationLanding.copySearchUrl;

		// file upload
		document.getElementById('fileBrowser').onchange = _selectFiles;
		document.getElementById('uploadBtn').onclick = treasury.margin.simulationLanding.loadUploadedPositions;

		// add position
		document.getElementById('addPosnBtn').onclick = treasury.margin.simulationLanding.addPosition;
		document.getElementById('nextId').onclick = treasury.margin.widget.goNext;
		document.getElementById('backId').onclick = treasury.margin.widget.goBack;
	}

	function _loadFilters() {
		var errorDialog = document.getElementById('errorMessage');
		errorDialog.visible = false;

		$
				.when(treasury.loadfilter.currencies(),
                        treasury.loadfilter.legalEntities(),
						treasury.loadfilter.cpes(),
						treasury.loadfilter.loadAgreementTypes(),
						treasury.loadfilter.loadMarginCalculators(),
						treasury.loadfilter.loadBooks(),
						treasury.loadfilter.defaultDates())
				.done(
						function(currenciesData, legalEntitiesData, cpesData,
								agreementTypesData, marginCalculatorsData,
								booksData, defaultDatesData) {

							booksAllData = booksData;

							var dateFilterValue = Date
									.parse(treasury.defaults.date
											|| defaultDatesData[0].tMinusOneFilterDate);
							var minDate = Date.parse('1970-01-01');
							var maxDate = new Date();
							var dateFormat = 'yy-mm-dd';
							treasury.margin.util.setupDateFilter(
									$("#datePicker"), $("#datePickerError"),
									dateFilterValue, minDate, maxDate,
									dateFormat);

							// Initializing Legal Entity Filter
							legalEntityMultiSelect = new window.treasury.filter.MultiSelectNew(
									"descoEntityIds",
									"legalEntityFilter",
									_decodeString(legalEntitiesData[0].descoEntities),
									treasury.defaults.legalEntities);

							var legalEntityFilter = document
									.getElementById("legalEntityFilter");
							var legalEntityFilterData = legalEntityFilter.data;
							for (var i = 0; i < legalEntityFilterData.length; i++) {
								legalEntityAllDataMap[legalEntityFilterData[i].key] = legalEntityFilterData[i];
							}

							// Initializing cpe filter
							cpeMultiSelect = new window.treasury.filter.MultiSelectNew(
									"cpeIds", "cpeFilter",
									_decodeString(cpesData[0].cpes),
									treasury.defaults.counterPartyEntities);

							// Initializing agreement type filter
							agreementTypeMultiSelect = new window.treasury.filter.MultiSelectNew(
									"agreementTypeIds", "agreementTypeFilter",
									agreementTypesData[0].agreementTypes,
									treasury.defaults.agreementTypes);

							// Initializing margin calculator filter
							marginCalculatorMultiSelect = new window.treasury.filter.MultiSelectNew(
									"marginCalculatorIds", "calculatorFilter",
									marginCalculatorsData[0].marginCalculators,
									[]);

							// Initializing book multi select filter
							bookMultiSelect = new window.treasury.filter.MultiSelectNew(
									"bookIds", "bookFilter",
									_decodeString(booksData[0].books),
									treasury.defaults.books);

							// Initializing book single select filter
							bookSingleSelect = new window.treasury.filter.SingleSelect(
									"bookIds", "addPosnBook",
									_decodeString(booksData[0].books),
									treasury.defaults.books);

                             // Initializing currency type filter
							// removing all currencies
							if (treasury.defaults.enableReportingCurrencyFilter) {
								currenciesData[0].currency.splice(0,1);
								reportingCurrencySingleSelect = new window.treasury.filter.SingleSelect(
								"reportingCurrencyId", "reportingCurrencyFilter",
								currenciesData[0].currency, treasury.defaults.reportingCurrencies);
							}

							// Getting Parameter Map
							if (reportingCurrencySingleSelect != null) {
							    searchPositionsFilterGroup = new treasury.filter.FilterGroup(
                                								[ legalEntityMultiSelect, cpeMultiSelect,
                                									agreementTypeMultiSelect,
                                									marginCalculatorMultiSelect,
                                									bookMultiSelect ],
                                								    [ bookSingleSelect,
                                									 reportingCurrencySingleSelect ],
                                									 $("#datePicker"),
                                									null);
							}else{
							    searchPositionsFilterGroup = new treasury.filter.FilterGroup(
                            									[ legalEntityMultiSelect, cpeMultiSelect,
                            										agreementTypeMultiSelect,
                            										marginCalculatorMultiSelect,
                            										bookMultiSelect ],
                            									    [ bookSingleSelect ],
                            									    $("#datePicker"),
                            									null);
							}


							var saveSettings = document
									.querySelector('arc-save-settings');
							saveSettings.retrieveSettings();
							_loadCopyUrlData();
						});
	}

	function _copySearchUrl() {
		treasury.margin.common.showLoading("mainLoader");
		var url = "/treasury/margin/simulation";
		var parameters = searchPositionsFilterGroup.getSerializedParameterMap();
		parameters["isCopyUrl"] = true;
		generateReportURL(url, parameters, "resultList");
		treasury.margin.common.hideLoading("mainLoader");
	}

	function _loadCopyUrlData() {
		if (window.location.href.indexOf("isCopyUrl=true") >= 0) {
			treasury.margin.common.showLoading("mainLoader");
			var urlParams = window.location.href.split("?");
			if (urlParams[1] != undefined) {
				var paramList = urlParams[1].split("&");
			}
			var searchData = {};
			for (var i = 0; i < paramList.length; i++) {
				var param = paramList[i].split("=");
				var val = param[1];
				if (val != null && param[0] != "isCopyUrl") {
					searchData[param[0]] = val;
				}
			}
			treasury.margin.common.hideLoading("mainLoader");
			_setFilters(searchData);
			treasury.margin.simulationActions
					.loadSearchPositionsData(searchData);
		} else {
			document.getElementById('searchMessage').removeAttribute("hidden");
		}
	}

	function _autoCompleteSecuritySearch(event) {
		var searchString = event.target.value;
		searchString = searchString
				.substring(searchString.lastIndexOf(',') + 1);
		window.treasury.filter.SecuritySearch(searchString, event.target.id);
	}

	function _loadSearchResults() {
		var parameters = searchPositionsFilterGroup.getSerializedParameterMap();
		var useReconPositions = document.getElementById("useReconPositions");
		if (useReconPositions.checked) {
			parameters["useReconPositions"] = true;
		} else {
			parameters["useReconPositions"] = false;
		}
		parameters["marginTypeIds"] = "1,3,4,5,6";
		treasury.margin.simulationActions.loadSearchPositionsData(parameters);
	}
	/* This function decodes special characters like &amp; &agrave; */
	function _decodeString(entityList) {
		for (var index = 0; index < entityList.length; index++) {
			var temp = jQuery.parseHTML(entityList[index][1]);
			if (temp == null)
				continue;
			entityList[index][1] = temp[0].data;
		}
		return entityList;
	}

	function _addPosition() {
	    var errorMsg = "";
	    var spn = $('#addPosnSpn').val();
	    // Remove special characters from spn
        spn = spn.trim().replace(/(,+|;+)$/, "");
        var qty = $('#addPosnQty').val();
        var price = $('#addPosnPrice').val();
        // invalid spn and quantity and user has entered the price but it is not a number.
        if (!_validate(spn) || !_validate(qty) || isNaN(price)) {
            			errorMsg = "Please enter valid spn and qty and price for simulation.";
           }
        if (treasury.defaults.enableReportingCurrencyFilter && !_validateDouble(price)
             ){

            errorMsg = errorMsg + "Please enter price according to required reporting currency."
        }

        if(errorMsg != ""){
            ArcMessageHelper.showMessage('ERROR', errorMsg);
            return;
        }
        // If user has not entered price then price needs to be fetched from backend.
		if(!_validateDouble(price)){
            var parameters = {};
            parameters["dateParam"] = $("#datePicker").val();
            parameters["spns"]  = spn;
            treasury.margin.simulationActions.fetchPositionPrice(parameters,addPositionWithPrice);
        }
		else{
			if (treasury.defaults.enableReportingCurrencyFilter) {
				treasury.margin.simulationActions.fetchFxRate(price, addPositionInReportingCurrency);
			}else {
				addPositionWithPrice(price);
			}
		}
	}

function addPositionWithPrice(price) {
  let positionInfo = _getAddPositionInformation(price);
  let data = [
    {
      id: "id_" + positionInfo["spn"],
      pnl_spn: positionInfo["spn"],
      quantity: positionInfo["qty"],
      price: price,
      originalPrice: price,
      originalQuantity: positionInfo["originalQuantity"],
      marketValue: positionInfo["marketValue"],
      priceXtick: positionInfo["priceXtick"],
      book_id: positionInfo["bookId"],
      book: positionInfo["book"],
      date: $("#datePicker").val(),
    },
  ];
  _resetAddPosition(data);
}

function addPositionInReportingCurrency(price, fxRate) {
  let positionInfo = _getAddPositionInformation(price);
  let priceUSD = price * fxRate;
  let originalPriceUSD = priceUSD;
  let marketValueUSD = positionInfo["marketValue"] * fxRate;
  let priceXtickUSD = priceUSD;
  
  let data = [
    {
	  id : "id_" + positionInfo["spn"],
	  pnl_spn : positionInfo["spn"],
      quantity : positionInfo["qty"],
      priceRC: price,
      price: priceUSD,
      originalPriceRC: price,
      originalPrice: originalPriceUSD,
      originalQuantity: positionInfo["originalQuantity"],
      marketValueRC: positionInfo["marketValue"],
      marketValue: marketValueUSD,
      priceXtickRC: positionInfo["priceXtick"],
      priceXtick: priceXtickUSD,
      book_id: positionInfo["bookId"],
      book: positionInfo["book"],
      date: $("#datePicker").val(),
    },
  ];
  _resetAddPosition(data);
}

function _getAddPositionInformation(price) {
  let positionInfo = {};
  positionInfo["originalQuantity"] = $("#addPosnQty").val();
  positionInfo["priceXtick"] = price;
  positionInfo["marketValue"] = positionInfo["originalQuantity"] * positionInfo["priceXtick"];
  positionInfo["bookId"] = bookSingleSelect.getSelectedId();
  positionInfo["book"] =
    treasury.margin.simulationLanding.searchBookName(positionInfo["bookId"]);
  let spn = $("#addPosnSpn").val();
  positionInfo["spn"] = spn.trim().replace(/(,+|;+)$/, "");
  positionInfo["qty"] = $("#addPosnQty").val();

  return positionInfo;
}

function _resetAddPosition(data) {
  treasury.margin.simulationActions.addPositionRow(data);
  $("#addPosnSpn").val("");
  $("#addPosnQty").val("");
  $("#addPosnPrice").val("");
  $("#addPosnBook").val("");
}

	function _validate(param) {
		// should be a whole number
		var str = param.slice(0, -1); // removing comma for NaN to work
		return ((param == undefined || param == "" || param == null
				|| isNaN(str) || (str % 1) != 0) ? false : true);
	}

	function _validateDouble(param) {
    	return ((param == undefined || param == "" || param == null) ? false : true);
    }

	function _selectFiles(event) {
		var uploadEvent = (event && event.originalEvent ? event.originalEvent
				: window.event)
				|| event;
		var validationMessage = '';
		files = this.files || uploadEvent.target.files;
		if (!_isSupportedFileFormat(files[0].name)) {
			validationMessage += 'Only xls or xlsx files are allowed. ';
			files = [];
			this.value = "";
		}
		if (validationMessage != '') {
			ArcMessageHelper.showMessage("WARNING", validationMessage);
			return;
		}
		$('#uploadBtn').removeAttr('disabled');
	}

	function _isSupportedFileFormat(fileName) {
		// cant use endsWith as its not supported with IE
		return ((fileName.lastIndexOf('.xlsx') === fileName.length - 5) || (fileName
				.lastIndexOf('.xls') === fileName.length - 4));
	}

	function _loadUploadedPositions() {
		treasury.margin.simulationActions.uploadFiles();
	}

	function _setFilters(parameters) {
		var legalEntityIds = parameters["descoEntityIds"];
		var cpeIds = parameters["cpeIds"];
		var agreementTypeIds = parameters["agreementTypeIds"];
		var marginCalculatorIds = parameters["marginCalculatorIds"];
		var bookIds = parameters["bookIds"];
		var dateString = parameters["dateString"];
        var reportingCurrencyId = parameters["reportingCurrencyId"];
		if (legalEntityIds != undefined) {
			legalEntityIds = legalEntityIds.split(',');
			legalEntityMultiSelect.setSelectedNodes(legalEntityIds);
		}
		if (cpeIds != undefined) {
			cpeIds = cpeIds.split(',');
			cpeMultiSelect.setSelectedNodes(cpeIds);
		}
		if (agreementTypeIds != undefined) {
			agreementTypeIds = agreementTypeIds.split(',');
			agreementTypeMultiSelect.setSelectedNodes(agreementTypeIds);
		}
		if (marginCalculatorIds != undefined) {
			marginCalculatorIds = marginCalculatorIds.split(',');
			marginCalculatorMultiSelect.setSelectedNodes(marginCalculatorIds);
		}
		if (bookIds != undefined) {
			bookIds = bookIds.split(',');
			bookMultiSelect.setSelectedNodes(bookIds);
		}
		if (reportingCurrencyId != undefined) {
            reportingCurrencySingleSelect.setSelectedNode(reportingCurrencyId);
        }
		if (dateString != undefined) {
			_setupDateFilter(dateString);
		}
	}
	/***************************************************************************
	 *
	 **************************************************************************/
	function _setupDateFilter(dateString) {
		var dateFilterValue = Date.parse(treasury.defaults.date || dateString);
		var minDate = Date.parse('1970-01-01');
		var maxDate = new Date();
		var dateFormat = 'yy-mm-dd';
		treasury.margin.util.setupDateFilter($("#datePicker"),
				$("#datePickerError"), dateFilterValue, minDate, maxDate,
				dateFormat);
	}
	function _resetFilters() {
		legalEntityMultiSelect.setSelectedNodes([]);
		cpeMultiSelect.setSelectedNodes([]);
		agreementTypeMultiSelect.setSelectedNodes([]);
		marginCalculatorMultiSelect.setSelectedNodes([]);
		bookMultiSelect.setSelectedNodes([]);
		if (reportingCurrencyId != undefined) {
		    reportingCurrencySingleSelect.setSelectedNode();
		}
		_setupDateFilter();
		document.getElementById('searchPositionPnlSpns').value = '';
	}
	function _searchBookName(book_id) {
		var books = booksAllData[0]["books"];
		for (var row = 0; row < books.length; row++) {
		    var index = books[row].indexOf(book_id);
		    if (index > -1) {
		      return books[row][1];
		    }
		  }
	}
})();
