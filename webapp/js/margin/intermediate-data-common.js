// Format number to accounting format
function formatNumbersToAccountingFormat(n) {
    var n = parseFloat(n).toFixed(2);
    var d = formatNumberUsingCommas(Number(Math.abs(n)).toFixed(0));
    if (n < 0) {
        return "(" + d.replace(/-/, "") + ")";
    } else {
        return d;
    }
}

// Format number to percentage format
function formatNumbersToPercentFormat(n) {
    var floatValue = parseFloat(n);
    var percentValue = Number(floatValue * 100).toFixed(1);
    return percentValue + " %";
}

// function which renders the positions margin details for the selected methodology.
function loadPositionMarginDetails(moduleId,pmdInputs, showDataInRC) {

    $("#position_margin_area"+moduleId).show();
    $("#position_margin_area_info"+moduleId).hide();
    var data = treasury.intermediate_margin_details
    var positions;
    // if no data
    if (typeof data["data"] == undefined ) {
        positions = [];
    }
    //Check for selecting positions from intermediate buckets if required
    else if(pmdInputs["collection"] != "position_margin" && typeof pmdInputs["id"] != 'undefined') {
        var intermediateBuckets = data["data"]["intermediate_buckets"];
        positions = intermediateBuckets[pmdInputs["id"]]["positions"];
    }
    else{
       positions = data["data"]["positions"]
    }

    if(positions == null || positions.length == 0) {
        $("#position_margin_area"+moduleId).hide();
        $("#position_margin_area_info"+moduleId).show().html("<br><span class='message'>No Position margin details available for this methodology </span>");
        return;
    }
    $("#position-margin-grid-div-"+moduleId).show();

    var columns = createColumnConfigurationforData(positions);
    var priority_list = 
        ['pnl_spn', 'ticker', 'desname', 'legal_entity', 'exposure_counter_party', 'exposure_agreement_type', 
         'exposure_custodian_account', 'instrument_type', 'gbo_type', 'price', 'quantity', 'market_value', 'fx_rate', 
         'book', 'ccy', 'baseline', 'margin_native', 'margin_usd', 'margin_type', 'underlying_value', 'usage_usd', 'state'];

    if (showDataInRC) {

        let marginIndex = priority_list.indexOf('margin_usd');
        priority_list[marginIndex] = 'margin';

        let usageIndex = priority_list.indexOf('usage_usd');
        priority_list[usageIndex] = 'usage';
    }

    columns = getPriorityBasedColumnList(columns, priority_list);

    for (var i=0; i < columns.length; i++) {
        var col = columns[i];
      //Avoid PNL SPN from getting formatted as a number(comma notation)
        if(col["type"] === "number" && col["field"] !== "pnl_spn" ) {
            col["formatter"] = dpGrid.Formatters.Number;
            col["sortable"]  = true;
        }
        else if (col["type"] === "float") {
            col["formatter"] = dpGrid.Formatters.Float;
            col["sortable"]  = true;
        }
        else if(col["type"] === "percent") {
            col["formatter"] = treasury.formatters.decimalToPercent;
            col["sortable"]  = true;
        }
    }

    for (var i=0;i < positions.length;i++) {
        var pos = positions[i];
        pos["id"] = pos["_id"];
    }

    var options = {
        autoHorizontalScrollBar: true,
        applyFilteringOnGrid : true,
        showHeaderRow : true,
        maxHeight:500,
        forceFitColumns:true,
        summaryRow : false,
        displaySummaryRow:false,
        exportToExcel : true,
        sheetName : "Position Margin Details",
        sortList : [{columnId:"pnl_spn", sortAsc:true},
                   ],
        onRender : function() {
            console.log("Position margin grid rendering done");
        }
    };

    new dportal.grid.createGrid($("#position-margin-grid-div-"+moduleId), positions, columns, options);

}


function createColumnConfigurationforData(data) {
    var columnConfigMap = {};

    for(var i=0; i < data.length; ++i) {
        var row = data[i];
        for(var property in row) {
            if(row.hasOwnProperty(property)) {
                var cellValue = row[property];
                if(typeof cellValue == "undefined" || typeof cellValue == "object"
                    || property.charAt(0) == "_" || property == "id" || property == "collection_name") 
                {
                    continue;
                }
                var gridDataType = getGridDataType(cellValue);
                var columnConfig = {};
                columnConfig['id'] = property;
                columnConfig['field'] = property;
                columnConfig['type']  = gridDataType;
                columnConfig['filter'] = "true";
                columnConfig['headerCssClass'] = "b";
                if(gridDataType == "number" || gridDataType == "float") {
                    columnConfig['sortable'] = "true";
                }
                var title = getTitleCase(property);
                columnConfig['name'] = title
                columnConfigMap[title] = columnConfig;
            }
        }
    }

    var columns = [];
    for(var property in columnConfigMap) {
        if(columnConfigMap.hasOwnProperty(property)) {
            columns.push(columnConfigMap[property]);
        }
    }

    return columns;
}


function getTitleCase(strValue) {
    //Example book_id to Book Id
    var title = strValue.replace(/_/g, ' ');
    return title.replace(/\w\S*/g, 
            function(val) {
                return val.charAt(0).toUpperCase() + val.substr(1).toLowerCase();
            }
    );
}

//This function is intended to determine data type supported by grid for a
//given value based on javascript's parsing rules.
function getGridDataType(value) {
    var floatVal = parseFloat(value).toFixed(2);
    var intVal   = parseInt(value);

     //Value starts with a non numeric char
    if(!isNumericValue(value) || isNaN(floatVal) || isNaN(intVal)){
        return "text";
    }

    // Hacky Stuff to force float notation for only small numbers.
    if(Math.abs(floatVal) > Math.abs(intVal) && Math.abs(floatVal) < 1000) {
        return "float";
    } else {
        return "number";
    }
}


function isNumericValue(value) {
    var pattern = /^-?\d+.?\d*$/;
    if(typeof value != 'undefined' && pattern.test(value)) {
        return true;
    } 

    return false;
}


//renders intermediate details involved in margin computation
function loadIntermediateDetails(moduleId,imdInputs, showDataInRC) {

    var data = treasury.intermediate_margin_details
    // Display the area
    $("#intermediate_data_area"+moduleId).show();

    // if no data
    if (typeof data["data"] == undefined || typeof data["data"]["intermediate_buckets"] == undefined) {
        $("#intermediate-data-grid-div-"+moduleId).show().html("<br><span class='message'>No Intermediate margin details available for methodology : " + imdInputs["methodology"] + " .</span>");
        return;
    }

    $("#intermediate-data-grid-div-"+moduleId).show();

    var bucketsToBeRendered = [];
    for (var i=0;i < data["data"]["intermediate_buckets"].length;i++) {
        var imd = data["data"]["intermediate_buckets"][i];
        imd["id"] = imd["_id"];
        imd["_collection_index"] = i;
        if(imd["collection_name"] == imdInputs["collection"]) {
            bucketsToBeRendered.push(imd);
        }
    }

    var columns  = createColumnConfigurationforData(bucketsToBeRendered);
    var columnPriority = ['bucket_type', 'bucket_value', 'rank', 'lmv_usd', 'smv_usd', 'gmv_usd', 'nmv_usd'];

    if (showDataInRC) {
        let lmvIndex = columnPriority.indexOf('lmv_usd');
        columnPriority[lmvIndex] = 'lmv';

        let smvIndex = columnPriority.indexOf('smv_usd');
        columnPriority[smvIndex] = 'smv';

        let gmvIndex = columnPriority.indexOf('gmv_usd');
        columnPriority[gmvIndex] = 'gmv';

        let nmvIndex = columnPriority.indexOf('nmv_usd');
        columnPriority[nmvIndex] = 'nmv';
    }

    columns = getPriorityBasedColumnList(columns, columnPriority);
    for (var i=0; i < columns.length; i++) {
        var col = columns[i];
        if(col["type"] === "number") {
            col["formatter"] = dpGrid.Formatters.Number;
            col["sortable"]  = true;
        }
        else if (col["type"] === "float") {
            col["formatter"] = dpGrid.Formatters.Float;
            col["sortable"]  = true;
        }
        else if (col["type"] === "percent") {
            col["formatter"] = treasury.formatters.decimalToPercent;
            col["sortable"]  = true;
        }

        if(col["field"] === "bucket_value") {
            col["isLink"] = true;
            col["formatter"] = function(row, cell, value, columnDef, dataContext) {
                var formattedCellValue = value;
                if(columnDef["type"] != undefined && columnDef["type"] === "number") {
                    formattedCellValue = dpGrid.Formatters.Number(row, cell, value, columnDef, dataContext);
                }
                var param = {collection: imdInputs["collection"], id: dataContext["_collection_index"]};
                var paramString = JSON.stringify(param);
                return "<a class='intermediate-drill-through" + moduleId + "'" + " data-params='" +
                                 paramString + "'>" + formattedCellValue + "</a>";
            }
        }
    }

    var options = {
        autoHorizontalScrollBar: true,
        applyFilteringOnGrid : true,
        showHeaderRow : true,
        maxHeight:500,
        forceFitColumns:true,
        summaryRow : false,
        displaySummaryRow:false,
        exportToExcel : true,
        sheetName : "Intermediate Margin Details",
        onRender : function() {
            $(".intermediate-drill-through"+moduleId).off("click");
            $(".intermediate-drill-through"+moduleId).on("click", function() {
                // Hide existing position area
                $("#position_margin_area"+moduleId).hide();
                $("#position_margin_area_info"+moduleId).hide();
                var inputs = $(this).data("params");
                loadPositionMarginDetails(moduleId,inputs, showDataInRC);
            });
        }
    };

    new dportal.grid.createGrid($("#intermediate-data-grid-div-"+moduleId), bucketsToBeRendered, columns, options);

}

function getPriorityBasedColumnList(columnList, priorityList) {
    if(columnList === undefined || columnList.length == 0) {
        return columns;
    }

    var colFieldToColumn = {};
    for(var i=0; i < columnList.length ; ++i) {
        var column = columnList[i];
        var field = column['field'];
        colFieldToColumn[field] = column;
    }

    var newColumnList = [];
    for(var i=0;i < priorityList.length ; ++i) {
        var field = priorityList[i];
        var column = colFieldToColumn[field];
        if( column !== undefined ) {
            newColumnList.push(column);
            delete colFieldToColumn[field];
        }
    }

    for(var field in colFieldToColumn) {
        if(colFieldToColumn.hasOwnProperty(field)) {
            newColumnList.push(colFieldToColumn[field]);
        }
    }

    return newColumnList;
}

function fetchExtendedMethodDetails(moduleId,params, showDataInRC) {

    var data = params["data"];
    var methodologyName = params["methodologyName"];

    //Adding details of this methodology to treasury namespace 
    treasury.intermediate_margin_details = data;

    if (typeof data == "undefined" || data == null || typeof data["data"] == "undefined") {
        $("#methodology_info_area"+moduleId).show().html("<span class='message'>No details available for methodology : " + methodologyName + " .</span>");
        return;
    }
    else {
        $("#methodology_info_area"+moduleId).hide();
    }

    var results = data["data"];

    $("#methodology_margin_details_area"+moduleId).show();

    if (typeof results["meta_data"] !== 'undefined') {
        console.log("Methodology metadata available");
        $("#methodology_description_area"+moduleId).show();
        var meth_desc_html = "" +
            "<blockquote cite=\"" + results["meta_data"]["description_url"] + "\">" +
            "<p>" + results["meta_data"]["description"]  + "</p>" + "<br/><br/>" +
            "<span style=\"text-align:right;\"><cite>" + results["meta_data"]["name"] + "</cite></span>" +
            "</blockquote>";
        $("#methodology_description"+moduleId).show().html(meth_desc_html);
    }
    else {
        $("#methodology_description_area"+moduleId).hide();
    }

    $("#methodology_margin_summary_area"+moduleId).show();
    var margin_summary_tbl_html = "" +
        "<table id='tbl_margin_summary" + moduleId + "'" +
        " style=\"table-layout:fixed;margin-top:5px\" border=0 cellspacing=0 cellpadding=3>" +
        "<colgroup><col width=\"50%\"><col width=\"*%\"></colgroup><tbody><tr><th colspan=\"2\">" +
        methodologyName + "</th></tr></tbody></table>";
    $("#methodology_margin_summary"+moduleId).show().html(margin_summary_tbl_html);

    var methodologyMarginData = getConsumableMethdologyDetails(results);
    var methodologyMarginColumns = 
        createColumnConfigurationforData([methodologyMarginData]);
    var methodologyColumnPriority = ['lmv', 'smv', 'lov', 'sov', 'margin', 'usage', 'fx_rate'];
    
    methodologyMarginColumns = getPriorityBasedColumnList(methodologyMarginColumns, methodologyColumnPriority);

    var intermediateBuckets = results["intermediate_buckets"];
    if (typeof intermediateBuckets == 'undefined') {
        intermediateBuckets = [];
    }

    associateCollectionsToLabels(methodologyMarginColumns, intermediateBuckets);
    for (var i=0; i < methodologyMarginColumns.length; i++) {
        // Get column info
        var col =  methodologyMarginColumns[i];

        var row$ = $('<tr/>');
        row$.append($('<td/>').html(col["name"]));

        var key = col["field"];
        var cellValue = results[key];
        var formattedCellValue = cellValue;
        if(col["type"] === "number") {
            formattedCellValue = formatNumbersToAccountingFormat(cellValue);
        }
        else if(col["type"] === "percent") {
            formattedCellValue = formatNumbersToPercentFormat(cellValue);
        }
        else if(col["type"] === "float") {
            formattedCellValue = parseFloat(cellValue).toFixed(2);
        }
    
        if(col["isLink"]) {
            var param = {collection:col["collection"], methodology:methodologyName};
            var paramString = JSON.stringify(param);
            formattedCellValue = "<a class='methodology-drill-through" + moduleId + "'" + " data-params='" +
                             paramString + "' style='color:#0066FF'>" + formattedCellValue + "</a>"
        }
        row$.append($('<td/>').html(formattedCellValue));

        $("#tbl_margin_summary"+moduleId).append(row$);
    }

    if (typeof results["margin_penalties"] != 'undefined' ) {
        console.log("Methodology penalities available");
        $("#methodology_penalities_area"+moduleId).show();
        var margin_penalties_tbl_html = "" +
            "<table id='tbl_margin_penalties" + moduleId + "'"  +
            " style=\"table-layout:fixed;margin-top:5px\" border=0 cellspacing=0 cellpadding=3>" +
            "<colgroup><col width=\"50%\"><col width=\"*\"></colgroup><tbody></tbody></table>";
        $("#methodology_margin_penalties"+moduleId).show().html(margin_penalties_tbl_html);

        var methodologyPenaltyColumns = 
           createColumnConfigurationforData([results["margin_penalties"]]);
        associateCollectionsToLabels(methodologyPenaltyColumns, intermediateBuckets);
        for (var i=0; i < methodologyPenaltyColumns.length; i++) {
            // Get column info
            var col =  methodologyPenaltyColumns[i];

            var row$ = $('<tr/>');
            row$.append($('<td/>').html(col["name"]));

            var key = col["field"];
            var cellValue = results["margin_penalties"][key];
            var formattedCellValue = cellValue;
            if(col["type"] === "number") {
                formattedCellValue = formatNumbersToAccountingFormat(cellValue);
            }
            else if(col["type"] === "percent") {
                formattedCellValue = formatNumbersToPercentFormat(cellValue);
            }
            else if(col["type"] === "float") {
                formattedCellValue = parseFloat(cellValue).toFixed(2);
            }

            if(col["isLink"]) {
                var param = {collection:col["collection"], methodology:methodologyName};
                var paramString = JSON.stringify(param);
                formattedCellValue = "<a class='methodology-drill-through" + moduleId + "'" + " data-params='" +
                                     paramString + "' style='color:#0066FF'>" + formattedCellValue + "</a>"
            }
            row$.append($('<td/>').html(formattedCellValue));

            $("#tbl_margin_penalties"+moduleId).append(row$);
        }
    }
    else {
        $("#methodology_penalities_area"+moduleId).hide();
    }

    $("#methodology_margin_tags"+moduleId).html('');
    if(typeof intermediateBuckets != "undefined" && intermediateBuckets.length > 0) {
        var displayContent$ = getIntermediateTagsDisplayContent(moduleId,intermediateBuckets, methodologyName,showDataInRC);
        $("#methodology_margin_tags"+moduleId).append(displayContent$);
        $("#methodology_tags_area"+moduleId).show();
    } 
    else {
        $("#methodology_tags_area"+moduleId).hide();
    }
}

function getIntermediateTagsDisplayContent(moduleId,intermediateBuckets, methodologyName,showDataInRC) {
    //Added padding to prevent browser CSS from kicking in.
    var displayContent$ = $('<ul style="padding: 5px;" />');
    var collectionTypesMap = {};
    for(var i=0; i < intermediateBuckets.length ; ++i ) {
        var ib = intermediateBuckets[i];
        var collectionName = ib['collection_name'];
        if(isPrimitiveValue(collectionName)) {
            collectionTypesMap[collectionName] = 1;
        }
    }

    for(var property in collectionTypesMap) {
        if(collectionTypesMap.hasOwnProperty(property)) {
            var param = {collection:property, methodology:methodologyName, showDataInRC:showDataInRC};
            var paramString = JSON.stringify(param);
            // ?: makes the match group non capturing
            var matchResults = property.match(/(?:intermediate_bucket.)([a-z_]+)/);
            var displayName;
            if(matchResults !== undefined) {
                displayName = matchResults[1];
            }
            if(isPrimitiveValue(displayName)) {
                displayName = displayName.toUpperCase();
                var formattedCellValue = "<a class='methodology-drill-through" + moduleId + "'" +" data-params='" +
                                                     paramString + "' style='color:#0066FF'>" + displayName + "</a>"
                displayContent$.append($('<li/>').html(formattedCellValue));
            }
        }
    }

    return displayContent$;
}


function getConsumableMethdologyDetails(data) {
    var methodologyMarginDetails = {};

    for (var property in data) {
        var value = data[property];
        if(data.hasOwnProperty(property) && isPrimitiveValue(value) ) {
            methodologyMarginDetails[property] = value;
        }
    }

    return methodologyMarginDetails;
}

function isPrimitiveValue(value) {
    var dataType = typeof value;
    if (dataType == "undefined" || dataType == "object") {
        return false;
    } else {
        return true;
    }
}

function associateCollectionsToLabels(columns, intermediateBuckets) {
    var displayLabelToCollectionName = {};
    for(var i=0; i < intermediateBuckets.length ; ++i ) {
        var ib = intermediateBuckets[i];
        var displayLabel = ib['_display_label'];
        var collectionName = ib['collection_name'];
        if(isPrimitiveValue(displayLabel) && isPrimitiveValue(collectionName)) {
            displayLabelToCollectionName[displayLabel] = collectionName;
        }
    }

    for (var i=0; i < columns.length ;++i ) {
       var columnField = columns[i]['field'];
       var collectionName = displayLabelToCollectionName[columnField];
       if (isPrimitiveValue(collectionName)) {
            columns[i]['isLink'] =  true;
            columns[i]['collection'] = collectionName;
       }

       if(columnField == 'margin') {
            columns[i]['isLink'] =  true;
            columns[i]['collection'] = 'position_margin';
       }
    }
}
