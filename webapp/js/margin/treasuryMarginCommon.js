"use strict";

const RENDER_CLONED_CONFIG = 1;
const RESET_AND_SHOW_CONFIG = 2;
const SAVE_CLONED_CONFIG_ON_CONFIG_UI = 3
const SAVE_CLONED_EDITED_CONFIG_ON_CONFIG_UI = 4;  
const SAVE_CLONED_EDITED_CONFIG_ON_SIM_UI = 5;
const SAVE_AND_SET_CLONED_CONFIG_ON_SIM_UI = 6;
const CONFIG_UI_SET_CURRENT_CALCULATOR_CONFIG = 7;
const SET_CONFIG_DATA = 8;
const UPDATE_UI_ELEMENTS_ATTRIBUTE = 9;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.margin = window.treasury.margin || {};
  window.treasury.margin.common = {
    showLoading: _showLoading,
    hideLoading: _hideLoading,
    renderDialogBox: _renderDialogBox,
    validateNameIsUndefined : _validateNameIsUndefined
  };

/*********************************************************************************
 * This method shows the loading icon on the screen.
 ********************************************************************************/
  function _showLoading(id) {
    if (typeof id !== 'undefined' && id.length != 0){
        $("#" + id).removeAttr("hidden");
        $("#" + id + "Overlay").removeAttr("hidden");
    } else {
        $(".loader").removeAttr("hidden");
        $(".overlay").removeAttr("hidden");
    }
  }
/*********************************************************************************
 * This method hides loading icon from the screen.
 ********************************************************************************/
  function _hideLoading(id) {
    if (typeof id !== 'undefined' && id.length != 0){
        $("#" + id).attr("hidden", "true");
        $("#" + id + "Overlay").attr("hidden", "true");
    } else {
        $(".loader").attr("hidden", "true");
        $(".overlay").attr("hidden", "true");
    }
  }
  function _renderDialogBox (dialogBoxElement, dialogBoxTitle){
    document.getElementById(dialogBoxElement).removeAttribute("hidden");
    document.getElementById(dialogBoxElement).reveal({
        title: dialogBoxTitle,
        modal: true,
        sticky: true,
        width: '550px',
        dismissible: 'true'
    });
  }
  
  function _validateNameIsUndefined(name){
    let nameInLowerCase = name.toLowerCase();
    let configNameUndefined = /(undefined)/;
    return configNameUndefined.test(nameInLowerCase);
  }

})();