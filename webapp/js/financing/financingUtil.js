"use strict";

(function() {
  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.util = {
    showLoading: _showLoading,
    hideLoading: _hideLoading,
    showErrorMessage: _showErrorMessage,
    setupDateFilter: _setupDateFilter,
    clearGrid: _clearGrid,
    resizeCanvasOnGridChange: _resizeCanvasOnGridChange,
    getDetailTable: _getDetailTable,
    resizeAllCanvas: _resizeAllCanvas,
    getFormattedMessage: _getFormattedMessage
  };

  function _clearGrid(gridId) {
    treasury.common.grid.clearGrid(gridId);
    if ($("#" + gridId).prev().prev().is("p") ||
      $("#" + gridId).prev().prev().is("center")) {
      $("#" + gridId).prev().prev().remove();
    } else if ($("#" + gridId).prev().is("p") ||
      $("#" + gridId).prev().is("center")) {
      $("#" + gridId).prev().remove();
    }
    var groupingHeader = document.getElementById(gridId + '-drop-element-holder');
    if (groupingHeader != null) {
      groupingHeader.innerHTML = "";
    }

    var header = document.getElementById(gridId + "-header");
    if (header != null) {
      header.innerHTML = "";
    }
  }

  function _setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
    dateFormat) {
    dateDiv.datepicker({
      dateFormat: dateFormat,
      changeMonth: true,
      changeYear: true,
      minDate: minDate,
      maxDate: maxDate,
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
    }).datepicker("setDate", date);
    dateDiv.change(function() {
      validateDate(dateDiv.val(), {
        errorDiv: errorDiv,
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: dateFormat,
      });
    });
  }

  function _showLoading() {
    $(".loader").removeAttr("hidden");
    $(".overlay").removeAttr("hidden");
  }

  function _hideLoading() {
    $(".loader").attr("hidden", "true");
    $(".overlay").attr("hidden", "true");
  }

  function _showErrorMessage(message) {
    var errorDialog = document.getElementById("errorMessage");

    errorDialog.reveal({
      'title': 'Error',
      'content': "<center><strong>" + message +
        "</strong></center>",
      'modal': true,
      'buttons': [{
        'html': 'OK',
        'callback': function() {
          this.visible = false;
        },
        'position': 'right'
      }]
    });
    errorDialog.removeAttribute("hidden");
  }

  function _resizeAllCanvas() {
    if(document.getElementById('comparisonFilterSidebar').state == 'expanded') {
      document.getElementById('comparisonFilterSidebar').style.width = '300px';
    } else {
      document.getElementById('comparisonFilterSidebar').style.width = '20px';
    }
    if(interestSeclendComparisonGrid != null) {
      interestSeclendComparisonGrid.resizeCanvas();
    }
  }

  function _resizeCanvasOnGridChange(gridName, gridId) {
    if (gridId != undefined) {
        $('#' + gridId).height($('#' + gridId).height() + 11);
    }
    if(interestSeclendComparisonGrid != null) {
      interestSeclendComparisonGrid.resizeCanvas();
    }
  }

  function _getDetailTable(item) {
    var tableString = '<tr><th>Attribute</th><th>Interest</th><th>Seclend</th></tr>';
    tableString = tableString.concat('<tr><td>Quantity</td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["int_quantity"], 0)).concat('</td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["sec_quantity"], 0)).concat('</td></tr>');

    tableString = tableString.concat('<tr><td>Excess Borrow Quantity</td>');
    tableString = tableString.concat('<td></td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["sec_excess_borrow_quantity"], 0)).concat('</td></tr>');

    tableString = tableString.concat('<tr><td>SLF Rate</td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["int_slf_rate"], 3)).concat('</td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["sec_slf_rate"], 3)).concat('</td></tr>');

    tableString = tableString.concat('<tr><td>Rebate Rate</td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["int_rebate_rate"], 3)).concat('</td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["sec_rebate_rate"], 3)).concat('</td></tr>');

    tableString = tableString.concat('<tr><td>Financing Fee Rate</td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["int_financing_fee_rate"], 3)).concat('</td>');
    tableString = tableString.concat('<td>').concat(_getFormattedData(item["sec_financing_fee_rate"], 3)).concat('</td></tr>');
    return tableString;
  }

  function _getFormattedMessage(message) {
      return "<center><div class='message no-icon' style='width: 30%;'>" + message + "</div></center>";
  }

  function _getFormattedData(value, precision) {
    if(value == null || value == "") {
      return "";
    } else {
      return parseFloat(value).toFixed(precision);
    }
  }
})();
