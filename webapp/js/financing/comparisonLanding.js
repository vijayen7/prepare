"use strict";
var cpesFilter;
var legalEntitiesFilter;
var custodianAccountsFilter;
var financingDataComparisonFilterGroup;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.comparisonLanding = {
    loadComparisonLandingView: _loadComparisonLandingView,
    loadSearchResults: _loadSearchResults,
  };
  window.addEventListener("WebComponentsReady", _initializeFinancingComparison);

  function _initializeFinancingComparison() {
    $(document).on({
      ajaxStart: function() {
        treasury.financing.util.showLoading();
      },
      ajaxStop: function() {
        treasury.financing.util.hideLoading();
      }
    });

    _registerHandlers();
    treasury.financing.comparisonLanding.loadComparisonLandingView();
  }

  function _registerHandlers() {
    document.getElementById('comparisonSearch').onclick = treasury.financing.comparisonLanding.loadSearchResults;
    document.getElementById('comparisonFilterSidebar').addEventListener("stateChange", treasury.financing.util.resizeAllCanvas);
    document.getElementById('comparisonDetailSidebar').addEventListener("stateChange", treasury.financing.util.resizeAllCanvas);
  }

  function _loadComparisonLandingView() {
    var errorDialog = document.getElementById('errorMessage');
    errorDialog.visible = false;
    $.when(treasury.loadfilter.defaultDates(),
      treasury.loadfilter.cpes(),
      treasury.loadfilter.legalEntities(),
      treasury.loadfilter.loadCustodianAccounts()).done(
      function(defaultDatesData, cpesData, legalEntitiesData, custodianAccountsData) {

        cpesFilter = new window.treasury.filter.MultiSelect(
          'cpeFilterIds', 'cpeFilter',
          cpesData[0].cpes, []);

        legalEntitiesFilter = new window.treasury.filter.MultiSelect(
          'legalEntityFilterIds', 'legalEntityFilter',
          legalEntitiesData[0].descoEntities, []);

        custodianAccountsFilter = new window.treasury.filter.MultiSelect(
          'custodianAccountFilterIds', 'custodianAccountFilter',
          custodianAccountsData[0].custodianAccounts, []);

        var dateFilterValue = new Date(treasury.defaults.date || defaultDatesData.tMinusOneFilterDate);
        var minDate = Date.parse('1970-01-01');
        var maxDate = Date.parse('2038-01-01');
        var dateFormat = 'yy-mm-dd';
        treasury.financing.util.setupDateFilter($("#datePicker"),
          $("#datePickerError"), dateFilterValue, minDate, maxDate, dateFormat);

        financingDataComparisonFilterGroup = new treasury.filter.FilterGroup([cpesFilter, legalEntitiesFilter, custodianAccountsFilter], null, $("#datePicker"), null);
      }
    );
  }

  function _loadSearchResults() {
    if (document.getElementById('datePickerError').innerHTML != '') {
      ArcMessageHelper.showMessage('error', 'Please select a valid date.', null, null);
      return;
    }
    if (new Date(document.getElementById('datePicker').value) == '') {
      ArcMessageHelper.showMessage('warning', 'Please select date.', null, null);
      return;
    }

    var parameters = financingDataComparisonFilterGroup.getSerializedParameterMap();
    parameters.dateString = document.getElementById('datePicker').value;
    parameters.spnString = document.getElementById('spnFilter').value;

    parameters.differenceByQuantityFilter = document.getElementById('differenceByQuantityFilter').checked;

    if (document.getElementById('comparisonFilterSidebar').state == 'expanded') {
      document.getElementById('comparisonFilterSidebar').toggle();
      document.getElementById('comparisonFilterSidebar').style.width = '20px';
    }
    document.getElementById('comparisonDetails').innerHTML = '';
    if (document.getElementById('comparisonDetailSidebar').state == 'expanded') {
      document.getElementById('comparisonDetailSidebar').toggle();
    }

    var spnValidation = _validateSpnString(parameters.spnString);
    if (spnValidation) {
      treasury.financing.util.clearGrid('comparisonGrid');
      document.getElementById('comparisonDetails').innerHTML = '';
      document.getElementById('comparisonDetailSidebar').setAttribute("hidden", true);
      treasury.financing.comparisonActions.loadInterestSeclendComparisonData(parameters);
    } else {
      if (document.getElementById('comparisonFilterSidebar').state == 'collapsed') {
        document.getElementById('comparisonFilterSidebar').toggle();
        document.getElementById('comparisonFilterSidebar').style.width = '300px';
      }
      return;
    }

  }

  function _validateSpnString(spnString) {
    if (spnString == null || spnString == "") {
      return true;
    }
    var numbers = /^[0-9]+$/;
    var spnList = spnString.split(",");

    for (var i = 0; i < spnList.length; i++) {
      spnList[i] = spnList[i].trim();
      if (!spnList[i].match(numbers)) {
        treasury.financing.util.showErrorMessage("Only numeric values allowed for SPN.");
        return false;
      }
    }
    return true;
  }

})();
