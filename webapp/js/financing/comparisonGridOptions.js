"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.comparisonGridOptions = {
    getResultOptions: _getResultOptions
  };

  function _getResultOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      expandTillLevel: -1,
      highlightRow: false,
      sortList: [{
        columnId: "legal_entity",
        sortAsc: true
      }],
      summaryRow: false,
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      applyFilteringOnGrid: true,
      useAvailableScreenSpace: true,
      exportToExcel: true,
      sheetName: "Interest-Seclend Data Comparison Report",
      customColumnSelection: {
        defaultcolumns: treasury.financing.comparisonColumnsConfig.getDefaultColumns(),
      },
      onRowClick: function(item, rowObject, isNotToggle) {
        document.getElementById('comparisonDetails').innerHTML = treasury.financing.util.getDetailTable(item);
        if (document.getElementById('comparisonDetailSidebar').state == 'collapsed') {
          document.getElementById('comparisonDetailSidebar').toggle();
        }
      },
      frozenColumn: 4
    };
  }
})();
