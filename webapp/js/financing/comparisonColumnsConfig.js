"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.comparisonColumnsConfig = {
    getResultColumns: _getResultColumns,
    getConfigColumns: _getConfigColumns,
    getTotalColumns: _getTotalColumns,
    getDefaultColumns: _getDefaultColumns
  };

  function _getResultColumns() {
    return [{
      id: "agreement",
      type: "text",
      name: "Agreement",
      field: "agreement",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 30
    }, {
      id: "custodian_account",
      type: "text",
      name: "Custodian Account",
      field: "custodian_account",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 30
    }, {
      id: "book",
      type: "text",
      name: "Book",
      field: "book",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 30
    }, {
      id: "borrow_type",
      type: "text",
      name: "Borrow Type",
      field: "borrow_type",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 15
     }, {
      id: "spn",
      type: "text",
      name: "SPN",
      field: "spn",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 15
    }, {
      id: "applicable_fee_type",
      type: "text",
      name: "Applicable Fee Type",
      field: "applicable_fee_type",
      sortable: true,
      headerCssClass: "aln-rt b",
      width: 20,
      formatter: _applicableColumnIdentifier
    }, {
      id: "int_applicable_fee_rate",
      type: "number",
      name: "Interest Applicable Fee Rate",
      field: "int_applicable_fee_rate",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 30,
      formatter: _applicableColumnIdentifier
    }, {
      id: "sec_applicable_fee_rate",
      type: "number",
      name: "Seclend Applicable Fee Rate",
      field: "sec_applicable_fee_rate",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 30,
      formatter: _applicableColumnIdentifier
    }, {
      id: "applicable_fee_rate_diff",
      type: "number",
      name: "Applicable Fee Rate Diff",
      field: "applicable_fee_rate_diff",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 30,
      formatter: _applicableColumnIdentifier
    }, {
      id: "int_quantity",
      type: "number",
      name: "Interest Quantity",
      field: "int_quantity",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 20,
      formatter: _applicableColumnIdentifier
    }, {
      id: "sec_quantity",
      type: "number",
      name: "Seclend Quantity",
      field: "sec_quantity",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 20,
      formatter: _applicableColumnIdentifier
    }, {
     id: "quantity_diff",
     type: "number",
     name: "Quantity Diff",
     field: "quantity_diff",
     sortable: true,
     headerCssClass: "aln-rt b",
     excelFormatter: "#,##0",
     width:20,
     formatter: _applicableColumnIdentifier
   }];
  }

  function _getConfigColumns() {

    return [{
      id: "int_price",
      type: "number",
      name: "Interest Price",
      field: "int_price",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 70
    }, {
      id: "int_slf_rate",
      type: "number",
      name: "Interest SLF Rate",
      field: "int_slf_rate",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 70,
      formatter: _applicableColumnIdentifier
    }, {
      id: "int_rebate_rate",
      type: "number",
      name: "Interest Rebate Rate",
      field: "int_rebate_rate",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 70,
      formatter: _applicableColumnIdentifier
    }, {
      id: "int_financing_fee_rate",
      type: "number",
      name: "Interest Financing Fee Rate",
      field: "int_financing_fee_rate",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 70,
      formatter: _applicableColumnIdentifier
    }, {
      id: "sec_price",
      type: "number",
      name: "Seclend Price",
      field: "sec_price",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 70,
      formatter: _applicableColumnIdentifier
    }, {
      id: "sec_slf_rate",
      type: "number",
      name: "Seclend SLF Rate",
      field: "sec_slf_rate",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 70,
      formatter: _applicableColumnIdentifier
    }, {
      id: "sec_rebate_rate",
      type: "number",
      name: "Seclend Rebate Rate",
      field: "sec_rebate_rate",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 70,
      formatter: _applicableColumnIdentifier
    }, {
      id: "sec_financing_fee_rate",
      type: "number",
      name: "Seclend Financing Fee Rate",
      field: "sec_financing_fee_rate",
      formatter: dpGrid.Formatters.Float,
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width: 70,
      formatter: _applicableColumnIdentifier
    }, {
     id: "price_diff",
     type: "number",
     name: "Price Diff",
     field: "price_diff",
     formatter: dpGrid.Formatters.Float,
     sortable: true,
     headerCssClass: "aln-rt b",
     excelFormatter: "#,##0",
     width: 70
   }, {
     id: "slf_rate_diff",
     type: "number",
     name: "SLF Rate Diff",
     field: "slf_rate_diff",
     formatter: dpGrid.Formatters.Float,
     sortable: true,
     headerCssClass: "aln-rt b",
     excelFormatter: "#,##0",
     width: 70,
     formatter: _applicableColumnIdentifier
   }, {
     id: "rebate_rate_diff",
     type: "number",
     name: "Rebate Rate Diff",
     field: "rebate_rate_diff",
     formatter: dpGrid.Formatters.Float,
     sortable: true,
     headerCssClass: "aln-rt b",
     excelFormatter: "#,##0",
     width: 70,
     formatter: _applicableColumnIdentifier
   }, {
     id: "financing_fee_rate_diff",
     type: "number",
     name: "Financing Fee Rate Diff",
     field: "financing_fee_rate_diff",
     formatter: dpGrid.Formatters.Float,
     sortable: true,
     headerCssClass: "aln-rt b",
     excelFormatter: "#,##0",
     width: 70,
     formatter: _applicableColumnIdentifier
    }];
  }

  function _getTotalColumns() {
    var allColumns = _getResultColumns();
    var configColumns = _getConfigColumns();
    var totalColumns = allColumns.slice(0, 5).concat(configColumns).concat(allColumns.slice(5));
    return totalColumns;
  }

  function _getDefaultColumns() {
    var resultColumns = _getResultColumns();
      if (!resultColumns)
        return;
      var retColumns = [];
      for (var i = 0; i < resultColumns.length; i++) {
        if(resultColumns[i].id != 'book') {
          retColumns.push([
            resultColumns[i].id
          ]);
        }
      }
      return retColumns;
  }

  function _applicableColumnIdentifier(row, cell, value, columnDef, dataContext) {
    var category = '';
    if (columnDef.field.indexOf('slf') !== -1) {
      category = 'blue7';
    } else if (columnDef.field.indexOf('rebate') !== -1) {
      category = 'blue3';
    } else if (columnDef.field.indexOf('financing') !== -1) {
      category = 'blue7';
    } else if (columnDef.field.indexOf('applicable_fee_rate') !== -1) {
      category = 'blue3';
    } else if(columnDef.field.indexOf('quantity') !== -1) {
      value = dpGrid.Formatters.Number(row, cell, value, columnDef, dataContext);
      category = 'blue7';
    }
    return '<div class="' + category + '" style="height:100%">' + value + '</div>';
  }
})();
