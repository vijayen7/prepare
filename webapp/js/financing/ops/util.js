/*jshint jquery:true*/
/*global treasury, ArcMessageHelper, validateDate*/
"use strict";
  function showLoading() {
      document.getElementById("opsLoading").show();
  }

  function hideLoading() {
    document.getElementById("opsLoading").hide();
  }

  function clearGrid(gridId) {
	  document.getElementById(gridId).style.display = "block";
	  document.getElementById(gridId).style.height = "";
	  document.getElementById(gridId).innerHTML = "";

	  if (gridId !== 'attributesGrid') {
	    if ($("#" + gridId).prev().prev().is("p") ||
	      $("#" + gridId).prev().prev().is("center")) {
	      $("#" + gridId).prev().prev().remove();
	    } else if ($("#" + gridId).prev().is("p") ||
	      $("#" + gridId).prev().is("center")) {
	      $("#" + gridId).prev().remove();
	    }
	  }
	  var header = document.getElementById(gridId + "-header");
	  if (header != null) {
	    header.innerHTML = "";
	  }
  }
  function getFormattedDate(elementId) {
    var datepicker = document.getElementById(elementId);
    var date = new Date(datepicker.value);
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return year + month + day;
  }

  function _initializeDatePicker(id, previousDayFlag) {
     var previousDate = new Date();
     previousDate.setDate(previousDate.getDate() - 1);
      var dateFilterValue = (previousDayFlag == true) ?  previousDate: Date.today();
      dateFilterValue = dateFilterValue ||treasury.defaults.date;
      $(id).datepicker({
          dateFormat : 'yy-mm-dd',
          changeMonth : true,
          changeYear : true,
          minDate : Date.parse('1970-01-01'),
          maxDate : Date.today(),
          showOn : "both",
          buttonText : "<i class='fa fa-calendar'></i>"
      }).datepicker( "setDate", dateFilterValue);
  }

  function showErrorMessage(message) {
    var errorDialog = document.getElementById("message");

    errorDialog.innerHTML = "<p class='arc-message--critical'>" + message +
      "</p>";
    errorDialog.reveal({
      'title': 'Error',
      'content': errorDialog.innerHTML,
      'modal': true,
      'buttons': [{
        'html': 'OK',

        'callback': function() {
          this.visible = false
        },
        'position': 'right'
      }]
    });
  }
