"use strict";
(function() {

    window.treasury = window.treasury || {};
    window.treasury.financingOps = window.treasury.financingOps || {};
    window.treasury.financingOps.landing = {
    	loadPage: _loadPage,
    };

    window.addEventListener("WebComponentsReady", _initialize);

    function _getPageToLoad(id) {
      return {
        'interestOps': '/treasury/jsp/interest/ops/dashboard.jspf',
        'seclendOps': '/treasury/jsp/seclend/ops/seclendops.jspf'
      }[id];
    }

    function _initializePage(id) {
      return {
    		'interestOps': window.treasury.interestOps.landing.initialize,
        'seclendOps': window.treasury.seclendOps.landing.initialize
  		}[id];
    }

    function _registerHandlers() {
    	document.getElementById('interestOps').onclick = function() {
            _loadPage('interestOps');
        };
        document.getElementById('seclendOps').onclick = function() {
            _loadPage('seclendOps');
        };
    }

    function _loadPage(id) {
    	$('#mainArea').load(_getPageToLoad(id), function() {
    		_initializePage(id)();
    	});
    	_resetActiveMenu();
    	_setActiveMenu(id);
    }

    function _setActiveMenu(id) {
    	document.getElementById(id).classList.add('active');
    }

    function _resetActiveMenu(id) {
    	document.getElementById('interestOps').classList.remove('active');
      document.getElementById('seclendOps').classList.remove('active');
    }

    function _initialize() {
      _registerHandlers();
      _loadPage('interestOps');
    }
})();
