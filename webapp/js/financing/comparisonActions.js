"use strict";

var interestSeclendComparisonGrid;

(function() {
  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.comparisonActions = {
    loadInterestSeclendComparisonData: _loadInterestSeclendComparisonData
  };

function _loadInterestSeclendComparisonData(parameters) {
  treasury.financing.util.showLoading();
  var serviceCall = $.ajax({
    url: 'treasury/financing/get-interest-seclend-comparison-data',
    data: parameters,
    type: 'POST',
    dateType: 'json'
  });
  document.getElementById('searchMessage').setAttribute("hidden", true);
    $.when(serviceCall)
      .done(
        function(data) {
          treasury.financing.util.clearGrid('comparisonGrid');
          treasury.financing.util.hideLoading();
          document.getElementById('comparisonGrid').removeAttribute("hidden");
          document.getElementById('comparisonDetailSidebar').removeAttribute("hidden");
          if (document.getElementById('comparisonDetailSidebar').state == 'expanded') {
            document.getElementById('comparisonDetailSidebar').toggle();
          }
          if (data && data.resultList && data.resultList.length) {
            interestSeclendComparisonGrid = new dportal.grid.createGrid(
              $("#comparisonGrid"), data.resultList,
              treasury.financing.comparisonColumnsConfig
              .getTotalColumns(),
              treasury.financing.comparisonGridOptions
              .getResultOptions());
            treasury.financing.util.resizeCanvasOnGridChange(interestSeclendComparisonGrid,
              'comparisonGrid');
          } else {
            document.getElementById('searchMessage').removeAttribute("hidden");
            document.getElementById('searchMessage').innerHTML = treasury.financing.util.getFormattedMessage('No diff records found for search criteria.');
          }
      });

      serviceCall
        .fail(function(xhr, text, errorThrown) {
          treasury.financing.util.clearGrid('comparisonGrid');
          treasury.financing.util.hideLoading();
          treasury.financing.util
            .showErrorMessage('Error', 'Error occurred while retrieving Interest-Seclend comparison data.');
        });
    }
})();
