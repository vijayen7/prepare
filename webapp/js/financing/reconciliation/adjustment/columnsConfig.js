"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.AdjustmentSummaryColumnsConfig = {
    getColumns: _getColumns,
  };

  function _getCommonColumns() {
    return [
      {
      id: "date",
      type: "text",
      name: "Date",
      field: "date",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      hideOnGrouping: true
    },
      {
      id: "agreementKey",
      type: "text",
      name: "Agreement Name",
      field: "agreementKey",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      hideOnGrouping: true
    },
    {
      id: "chargeType",
      type: "text",
      name: "Charge Type",
      field: "chargeType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, 
  {
  id: "currency",
  type: "text",
  name: "Currency",
  field: "currency",
  sortable: true,
  headerCssClass: "aln-rt b",
  excelFormatter: "#,##0",
  hideOnGrouping: true
  }, {
      id: "custodianAccount",
      type: "text",
      name: "Custodian Account",
      field: "custodianAccount",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "book",
      type: "text",
      name: "Book",
      field: "book",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "bundle",
      type: "text",
      name: "Bundle",
      field: "bundle",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },{
      id: "treasury",
      type: "text",
      name: "Adjustment posted in local(Local)",
      field: "blendedTreasury",
      sortable: true,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(parseFloat(value)) + "</div>";
      },
      groupingAggregator: "Sum",
     groupTotalsFormatter: function totalsGroupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
       if( totals && totals.sum && totals.group.field == "currency"){
         return isExportToExcel ? treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])
             : "<div class='aln-rt b'>"+treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])+"</div>";
       } else {
         return "";
       }
     },
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      width: 190,
      excelFormatter: "#,##0"
    },{
      id: "adjustmentPostedInUSD",
      type: "text",
      name: "Adjustment posted in USD",
      field: "adjustmentPostedInUSD",
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      formatter: function(row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(parseFloat(value)) + "</div>";
      },
      groupingAggregator: "Sum",
     groupTotalsFormatter: function totalsGroupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
      if (totals && totals.sum) {
        return isExportToExcel ? treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])
            : "<div class='aln-rt b'>"+treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])+"</div>";
      } else {
        return "";
      }
     },
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      width: 190,
      excelFormatter: "#,##0"
    },
    {
          id: "adjustmentPostedInRC",
          type: "text",
          name: "Adjustment posted in RC",
          field: "adjustmentPostedInRC",
          sortable: true,
          aggregator: dpGrid.Aggregators.sum,
          formatter: function(row, cell, value, columnDef, dataContext) {
            return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(parseFloat(value)) + "</div>";
          },
          groupingAggregator: "Sum",
         groupTotalsFormatter: function totalsGroupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
          if (totals && totals.sum) {
            return isExportToExcel ? treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])
                : "<div class='aln-rt b'>"+treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])+"</div>";
          } else {
            return "";
          }
         },
          headerCssClass: "aln-rt b",
          cssClass: "amount",
          width: 190,
          excelFormatter: "#,##0"
        },
    {
      id: "treasuryUSD",
      type: "text",
      name: "Net Adjustment (USD)",
      field: "treasuryUSD",
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: function totalsGroupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
        if (totals && totals.sum) {
          return isExportToExcel ? treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])
              : "<div class='aln-rt b'>"+treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])+"</div>";
        } else {
          return "";
        }
      },
      formatter: function(row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(parseFloat(value)) + "</div>";
      },
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      width: 160,
      excelFormatter: "#,##0"
    },
    {
          id: "treasuryRC",
          type: "text",
          name: "Net Adjustment (RC)",
          field: "treasuryRC",
          sortable: true,
          aggregator: dpGrid.Aggregators.sum,
          groupingAggregator: "Sum",
          groupTotalsFormatter: function totalsGroupFormatter(totals, columnDef, placeHolder, isExportToExcel) {
            if (totals && totals.sum) {
              return isExportToExcel ? treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])
                  : "<div class='aln-rt b'>"+treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(totals.sum[columnDef.field])+"</div>";
            } else {
              return "";
            }
          },
          formatter: function(row, cell, value, columnDef, dataContext) {
            return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(parseFloat(value)) + "</div>";
          },
          headerCssClass: "aln-rt b",
          cssClass: "amount",
          width: 160,
          excelFormatter: "#,##0"
        },
    {
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreementType",
      sortable: true,
      groupingAggregator:customAgreementTypeAgreegator,
      groupTotalsFormatter: function(totals,columnDef, placeHolder, isExportToExcel) {
        if(totals && totals.agreementType) {
          return isExportToExcel ? totals.agreementType[columnDef.field]  : "<div class='aln-rt b'>"+totals.agreementType[columnDef.field]+"</div>";
        }
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },  {
      id: "comments",
      type: "text",
      name: "Comments",
      field: "comments",
      sortable: false,
      groupingAggregator:customCommentsAgreegator,
      groupTotalsFormatter: function(totals,columnDef, placeHolder, isExportToExcel) {
        if(totals && totals.comment) {
          return isExportToExcel ? totals.comment[columnDef.field] : "<div class='aln-rt b'>"+totals.comment[columnDef.field]+"</div>"
        }
      },
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legalEntity",
      groupingAggregator:customLegalEntityAgreegator,
      groupTotalsFormatter: function(totals,columnDef, placeHolder, isExportToExcel) {
        if(totals && totals.legalEntity) {
          return  isExportToExcel ? totals.legalEntity[columnDef.field]  : "<div class='aln-rt b'>"+totals.legalEntity[columnDef.field]+"</div>";
        }
      },
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "counterPartyEntity",
      type: "text",
      name: "Counterparty",
      field: "cpe",
      groupingAggregator:customCpeAgreegator,
      groupTotalsFormatter: function(totals,columnDef, placeHolder, isExportToExcel) {
        if(totals && totals.cpe) {
          return   isExportToExcel ? totals.cpe[columnDef.field]  : "<div class='aln-rt b'>"+totals.cpe[columnDef.field]+"</div>";
        }
      },
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "currency",
      type: "text",
      name: "Currency",
      field: "currency",
      groupingAggregator:customCurrencyAgreegator,
      groupTotalsFormatter: function(totals,columnDef, placeHolder, isExportToExcel) {
        if(totals && totals.currency) {
          return isExportToExcel ? totals.currency[columnDef.field]  : "<div class='aln-rt b'>"+totals.currency[columnDef.field]+"</div>"; +"</div>";
        }
      },
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "chargeType",
      type: "text",
      name: "Charge Type",
      field: "chargeType",
      groupingAggregator:customChargeTypeAgreegator,
      groupTotalsFormatter: function(totals,columnDef, placeHolder, isExportToExcel) {
        if(totals && totals.chargeType) {
          return isExportToExcel ? totals.chargeType[columnDef.field] : "<div class='aln-rt b'>"+totals.chargeType[columnDef.field]+"</div>"
        }
      },
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, ];
  }


  function _getColumns() {
    var columns = _getCommonColumns();
    return columns;
  }
})();
