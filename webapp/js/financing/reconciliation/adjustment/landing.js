"use strict";
var simulationFilterGroup;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.adjustmentSummary = {
    loadSearchResults: _loadSearchResults,
    pageDestructor: _pageDestructor,
    registerHandlers: _registerHandlers,
    adjustmentApproved: _adjustmentApproved,
    loadLandingViewFilter: _loadLandingViewFilter,
    deleteAdjustment: _deleteAdjustment,
    copySearchURL: _copySearchURL,
    loadPage: _loadPage,
    endAdjustmentWorkflow: _endAdjustmentWorkflow,    
  };

  function _loadPage() {
	  _loadLandingViewFilter();
     
    
	  $("#adjustmentSearch").removeClass("hidden")
    if (document.getElementById('filterSidebar').state == "collapsed") {
      document.getElementById('filterSidebar').toggle();
    }
    if (adjustmentSummarySearchGrid != undefined)
    {
      if (document.getElementById('filterSidebar').state == "expanded") {
      document.getElementById('filterSidebar').toggle();

    }
      adjustmentSummarySearchGrid.resizeCanvas(true);

    }
   
    }

  function _copySearchURL() {
    var url = "/treasury/financing/reconciliation/adjustmentSummary-results";
    var parameters = searchFilterGroup.getSerializedParameterMap();
    parameters.isSignedOff = document.getElementById("show-all-yes").checked;
    var formattedDate = getFormattedDate('datePicker');
    parameters.dateString = formattedDate;
    generateReportURL(url, parameters, "resultList");

  }

  function _pageDestructor() {


    $("#showAllApprovedDiv").addClass("hidden");
    $("#currencyParentDiv").addClass("hidden");
     $("#reportingCurrencyTypeDiv").addClass("hidden");
  
    $("#adjustmentSearch").addClass("hidden");
    $("#startWorkflow").addClass("hidden");
    $("#endWorkflow").addClass("hidden");
  }

  function _loadConfirmationDialogForId(id) {
	  return function() {
		  document.getElementById(id).visible = true
	  }
  }

  function _removeConfirmationDialogForId(id) {
	  return function() {
		  document.getElementById(id).visible = false
	  }
  }


  function _registerHandlers() {

	//Approve Adjustments Handler
    document.getElementById("adjust").onclick = function() {
    	var selectedIndexes = adjustmentSummarySearchGrid.selectedRowIds();
    	if(selectedIndexes.length == 0) {
    		raiseErrorToast("Selected one or more adjustments to approve!")
    	} else {
    	_loadConfirmationDialogForId('approveAdjustmentsConfirmationDialog')();
    	}
    }
    document.getElementById("performApproval").onclick = function() {
    	_adjustmentApproved();
    	_removeConfirmationDialogForId('approveAdjustmentsConfirmationDialog')();
    }
    document.getElementById("notPerformApproval").onclick = _removeConfirmationDialogForId('approveAdjustmentsConfirmationDialog');

    //Delete Adjustments Handlers
    document.getElementById("delete-adjust").onclick = function() {
    	var selectedIndexes = adjustmentSummarySearchGrid.selectedRowIds();
    	if(selectedIndexes.length == 0) {
    		raiseErrorToast("Select one or more adjustments to delete!")
    	} else {
    	_loadConfirmationDialogForId('deleteAdjustmentsConfirmationDialog')();
    	}
    }
    document.getElementById("performDeletion").onclick = function() {
    	_deleteAdjustment();
    	_removeConfirmationDialogForId('deleteAdjustmentsConfirmationDialog')();
    }
    document.getElementById("notPerformDeletion").onclick = _removeConfirmationDialogForId('deleteAdjustmentsConfirmationDialog');
    document.getElementById("startWorkflow").onclick = _beginAdjustmentWorkflow;
    document.getElementById("endWorkflow").onclick = _endAdjustmentWorkflow;

  }

  function _loadLandingViewFilter() {
    $("#showAllApprovedDiv").removeClass("hidden");
    $("#currencyParentDiv").removeClass("hidden");
    $("#reportingCurrencyTypeDiv").removeClass("hidden");

  }

  function _loadSearchResults() {
    if (document.getElementById("datePickerError").innerHTML != "") {
      raiseErrorToast("Please select a valid Date");
      return;
    }
    document.getElementById("adjust").classList.add("hidden");
    document.getElementById("delete-adjust").classList.add("hidden");
    document.getElementById("endWorkflow").classList.add("hidden");
    document.getElementById("startWorkflow").classList.add("hidden");
    
    var parameters = searchFilterGroup.getSerializedParameterMap();
    parameters.isSignedOff = document.getElementById("show-all-yes").checked;

    var formattedDate = getFormattedDate('datePicker');
    parameters.dateString = formattedDate;
    
    treasury.financing.reconciliation.AdjustmentSummaryAction.loadResults(parameters);
  }

  function _adjustmentApproved() {
    treasury.financing.reconciliation.AdjustmentSummaryAction.approveAdjustment();
  }

  function _deleteAdjustment() {
    treasury.financing.reconciliation.AdjustmentSummaryAction.deleteAdjustment();
  }

  function _beginAdjustmentWorkflow() {
    var selectedIndexes = adjustmentSummarySearchGrid.selectedRowIds();
     if (selectedIndexes.length == 0) {
      raiseErrorToast("Please select one or more records to start the workflow.");
      return;
    }
    var ajaxCall = treasury.financing.reconciliation.AdjustmentSummaryAction.beginAdjustmentWorkflow(getFormattedDate('datePicker'));
    ajaxCall.done(function(response) {
      if(response.hasWorkflowStarted === true)
      {
        document.getElementById("adjust").classList.remove("hidden");
        document.getElementById("delete-adjust").classList.remove("hidden");
        document.getElementById("endWorkflow").classList.remove("hidden");
        $("#startWorkflow").addClass("hidden");
        
      }
      else {
        raiseErrorToast(response.errorMessage);
      }
    });

  }

  function _endAdjustmentWorkflow() {
    var ajaxCall = treasury.financing.reconciliation.AdjustmentSummaryAction.endAdjustmentWorkflow(getFormattedDate('datePicker'));
    ajaxCall.done(function(response) {
      document.getElementById("adjust").classList.add("hidden");
      document.getElementById("delete-adjust").classList.add("hidden");
      document.getElementById("endWorkflow").classList.add("hidden");
      if(response.errorMessage) {
        raiseErrorToast(response.errorMessage);
      }
      else{
        _loadSearchResults();
      }
  })
}

})()
