"use strict";
var adjustmentSummarySearchGrid;
var acuralAndBrokerDataForAdjustmentSummarySearchGrid;
(function () {
  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.AdjustmentSummaryAction = {
    loadResults: _loadResults,
    approveAdjustment: _approveAdjustment,
    deleteAdjustment: _deleteAdjustment,
    beginAdjustmentWorkflow: _beginAdjustmentWorkflow,
    endAdjustmentWorkflow: _endAdjustmentWorkflow,
    loadAccuralAndBrokerPostingResults: _loadAccuralAndBrokerPostingResults,
  };
  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/reconciliation/adjustmentSummary-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.financing.reconciliation.common.actions.clearGrid("adjustmentGrid");
    treasury.financing.reconciliation.common.actions.clearGrid("acuralAndBrokerAdjustmentGrid");
    _endAdjustmentWorkflow(getFormattedDate('datePicker'));
    _loadAdjustmentSearchGrid(serviceCall, "adjustmentGrid",
      treasury.financing.reconciliation.AdjustmentSummaryColumnsConfig.getColumns,
      treasury.financing.reconciliation.common.grid.getSummaryAndAdjustmentGridOptions);
     
  }
  function _loadAdjustmentSearchGrid(ajaxCall, gridId, columnsFunction,
    gridOptionFunction) {
    showLoading("mainLoader");
    ajaxCall.done(function(data) {
      hideLoading("mainLoader");
      for (var i = 0; i < data.length; i++) {
        data[i] = treasury.financing.reconciliation.common.actions.flattenJsonResponse(data[i]);
      }
      if (adjustmentSummarySearchGrid != undefined)
        adjustmentSummarySearchGrid.dataView.setItems([]);
      if (data && !data.hasOwnProperty('errorMessage') && data.length != 0) {
        var interestPowerAuth = treasury.financing.reconciliation.common.actions.isUserAuthorizedForInterestPowerEdit();
        $.when(interestPowerAuth).done(function(interestPowerAuthData) {
          if (interestPowerAuthData) {
          document.getElementById("startWorkflow").classList.remove("hidden");
            } else {
            if(!($("#startWorkflow").hasClass("hidden"))){
              document.getElementById("startWorkflow").className+=" hidden";
              }
            }
        });
        adjustmentSummarySearchGrid = new dportal.grid.createGrid($("#" +
            gridId), data, columnsFunction(),
          gridOptionFunction());
        var rows = [];
        for (var i = 0; i < adjustmentSummarySearchGrid.data.length; i++) {
          if (adjustmentSummarySearchGrid.data[i].isSignedOff == 0) {
            rows.push(data[i].id);
          }
        }
        adjustmentSummarySearchGrid.setSelectedRowIds(rows);
        adjustmentSummarySearchGrid.dataView.collapseAllGroups();
        treasury.financing.reconciliation.common.actions.resizeCanvasOnGridChange(
          adjustmentSummarySearchGrid,
          'adjustmentGrid');
        document.getElementById('loadingStateDialog').visible = false;
        if(data[0]["reportingFxRate"]) {
            treasury.financing.reconciliation.common.actions.showReportingCurrencyMessage(gridId, true, data[0]["reportingCurrency"]);
          }
        else {
            treasury.financing.reconciliation.common.actions.showReportingCurrencyMessage(gridId, false);
             }
      } else if (data && data.errorMessage) {
        document.getElementById('loadingStateDialog').visible = false;
        raiseErrorToast("Error occured while searching for results");
        treasury.financing.reconciliation.common.actions.hideReportingCurrencyMessage(gridId);
      } else if (data && data.length == 0) {
        document.getElementById('loadingStateDialog').visible = false;
        raiseInfoToastWithTimeOut("No data found");
        treasury.financing.reconciliation.common.actions.hideReportingCurrencyMessage(gridId);
      }
      if (document.getElementById('filterSidebar').state == 'expanded' && adjustmentSummarySearchGrid != undefined) {
        document.getElementById('filterSidebar').toggle();
      }
    });
    ajaxCall.fail(function(xhr, text, errorThrown) {
      raiseErrorToast("Error occured while searching for results");
      treasury.financing.reconciliation.common.actions.hideReportingCurrencyMessage(gridId);
    });
  }
  function _approveAdjustment(parameters) {
    var approvedAdjustmentRecords = [];
    var selectedIndexes = adjustmentSummarySearchGrid.selectedRowIds();
    jQuery.each(selectedIndexes, function(index, value) {
      approvedAdjustmentRecords.push(adjustmentSummarySearchGrid.dataView.getItemById(value));
    });
    if (approvedAdjustmentRecords.length == 0) {
      raiseErrorToast("Please select one or more records to adjust.");
      return;
    }
    var parameters = {
      "approvedAdjustmentRecords": JSON.stringify(approvedAdjustmentRecords),
    }
    var ajaxCall = $.ajax({
      url: 'treasury/financing/reconciliation/adjustmentSummary-signedOff',
      type: "POST",
      data: parameters,
    });
     
    treasury.financing.reconciliation.common.actions.clearGrid("adjustmentGrid")
    showLoading("mainLoader");
    ajaxCall.done(function(response) {
      hideLoading("mainLoader");
      if (response != undefined &&
        response.hasOwnProperty("errorMessage")) {
        raiseErrorToast(response.errorMessage);
      } else {
        raiseSuccessToast("Records have been successfully approved");
        document.getElementById('loadingStateDialog').visible = true;
        $('.arc-dialog__panel__header__control').addClass("hidden");
        adjustmentSummarySearchGrid.dataView.setItems([]);
        treasury.financing.reconciliation.adjustmentSummary.endAdjustmentWorkflow();
      }
    })
  }
  function _beginAdjustmentWorkflow(date) {
    var params = {'date': date,
                  'format': "json"};
    console.log("Beginnning workflow for dare" + date);
    var ajaxCall = $.ajax({
      url: '/treasury/service/interestReconciliationDataService/beginWorkflow',
      data: params,
      type: "POST",
    });
    return ajaxCall;
  }
  function _endAdjustmentWorkflow(date) {
    var params = {'date': date,
                  'format': "json"};
    var ajaxCall = $.ajax({
      url: '/treasury/service/interestReconciliationDataService/endWorkflow',
      data: params,
      type: "POST",
    });
    return ajaxCall;
  }
  function _deleteAdjustment(parameters) {
    var deleteAdjustmentRecords = [];
    var selectedIndexes = adjustmentSummarySearchGrid.selectedRowIds();
    jQuery.each(selectedIndexes, function(index, value) {
      deleteAdjustmentRecords.push(adjustmentSummarySearchGrid.dataView.getItemById(value));
    });
    if (deleteAdjustmentRecords.length == 0) {
      raiseErrorToast("Please select one or more records to delete.");
      return;
    }
    var parameters = {
      "deleteAdjustmentRecords": JSON.stringify(deleteAdjustmentRecords),
    };
    var ajaxCall = $.ajax({
      url: 'treasury/financing/reconciliation/deleteAdjustments',
      data: parameters,
      type: "POST",
    });
    treasury.financing.reconciliation.common.actions.clearGrid("adjustmentGrid");
    showLoading("mainLoader");
    ajaxCall.done(function(response) {
      hideLoading("mainLoader");
      if (response != undefined &&
        response.hasOwnProperty("errorMessage")) {
        raiseErrorToast(response.errorMessage);
      } else { 
        raiseSuccessToast("Adjustments have been successfully deleted");
        adjustmentSummarySearchGrid.dataView.setItems([]);
        document.getElementById('loadingStateDialog').visible = true;
        $('.arc-dialog__panel__header__control').addClass("hidden");
        treasury.financing.reconciliation.adjustmentSummary.endAdjustmentWorkflow();
      }
    })
  } 
  function _loadAccuralAndBrokerPostingResults(parameters) {
   
    var serviceCall = $.ajax({
      url: "/treasury/financing/reconciliation/adjustmentSummaryAcuralAndBrokerPosting-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    var gridId = "acuralAndBrokerAdjustmentGrid";
    var columnFunction = treasury.financing.reconciliation.SummaryColumnsConfig.getAcuralAndAdjustmentPostingColumns;
    var gridOptionFunction = treasury.financing.reconciliation.common.grid.getAcuralAndBrokerAdjustmentGridOptions;
    treasury.financing.reconciliation.common.actions.clearGrid(gridId);
    showLoading("accrualBrokerAdjustmentLoader");
    serviceCall.done(function (data) {

      hideLoading("accrualBrokerAdjustmentLoader");
      for (var i = 0; i < data.length; i++) {
        data[i] = treasury.financing.reconciliation.common.actions.flattenJsonResponse(data[i]);
      }
      if (data && !data.hasOwnProperty('errorMessage') && data.length != 0) {

        acuralAndBrokerDataForAdjustmentSummarySearchGrid = new dportal.grid.createGrid($("#" +
          gridId), data, columnFunction(),
          gridOptionFunction());
        treasury.financing.reconciliation.common.actions.resizeCanvasOnGridChange(
          acuralAndBrokerDataForAdjustmentSummarySearchGrid,
          'acuralAndBrokerAdjustmentGrid');
        document.getElementById('loadingStateDialog').visible = false;
      }
      else if (data && data.errorMessage) {
        document.getElementById('loadingStateDialog').visible = false;
        raiseErrorToast("Error occured while searching for results");
      } else if (data && data.length == 0) {
        document.getElementById('loadingStateDialog').visible = false;
        raiseInfoToastWithTimeOut("No data found");
      }

    });
    serviceCall.fail(function (xhr, text, errorThrown) {
      hideLoading("mainLoader");
      raiseErrorToast("Error occured while searching for results");
    });
  }
})();