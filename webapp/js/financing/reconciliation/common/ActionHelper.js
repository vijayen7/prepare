"use strict";
var userAuthforPowerEdit;
var userAuthForOspreyEdit;


(function () {
  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.common = window.treasury.financing.reconciliation.common || {};
  window.treasury.financing.reconciliation.common.actions = {
    flattenJsonResponse: _flattenJsonResponse,
    clearAllGrids: _clearAllGrids,
    resizeCanvasOnGridChange: _resizeCanvasOnGridChange,
    resizeAllCanvas: _resizeAllCanvas,
    clearGrid: _clearGrid,
    loadSearchGrid: _loadSearchGrid,
    isUserAuthorizedForInterestPowerEdit: _isUserAuthorizedForInterestPowerEdit,
    isUserAuthorizedForInterestEdit: _isUserAuthorizedForInterestEdit,
    isUserAuthorizedForInterestSync: _isUserAuthorizedForInterestSync,
    showReportingCurrencyMessage: _showReportingCurrencyMessage,
    hideReportingCurrencyMessage: _hideReportingCurrencyMessage
  };

  function _isUserAuthorizedForInterestPowerEdit() {
    return $.ajax({
      url: 'treasury/financing/reconciliation/user-auth-for-interest-power-edit',
      type: "POST",
    });
  }
  function _isUserAuthorizedForInterestEdit() {
    return $.ajax({
      url: 'treasury/financing/reconciliation/user-auth-for-interest-edit',
      type: "POST",
    });
  }
  function _isUserAuthorizedForInterestSync() {
    return $.ajax({
      url: 'treasury/financing/reconciliation/user-auth-for-interest-sync',
      type: "POST",
    });
  }


  function _flattenJsonResponse(jsonResponse) {
    for (var field in jsonResponse) {
      if (!jsonResponse.hasOwnProperty(field))
        continue;
      if ((typeof jsonResponse[field]) == 'object') {
        var flatObject = _flattenJsonResponse(jsonResponse[field]);
        for (var innerField in flatObject) {
          if (!flatObject.hasOwnProperty(innerField))
            continue;
          jsonResponse[innerField] = flatObject[innerField];
        }
      }
    }
    return jsonResponse;
  }

  function _resizeAllCanvas() {
    if (document.getElementById('filterSidebar').state == 'expanded') {
      document.getElementById('filterSidebar').style.width = '300px';
    } else {
      document.getElementById('filterSidebar').style.width = '20px';
    }

  }

  function _resizeCanvasOnGridChange(gridName, gridId) {
    var leftPanel = document.getElementById('filterSidebar');

    leftPanel.addEventListener('stateChange', function () {
      if (summarySearchGrid != undefined && document.getElementById('reconciliationSummary').classList[0] == 'active') {
         summarySearchGrid.resizeCanvas(true); 
        }
      if (adjustmentSummarySearchGrid != undefined && document.getElementById('adjustmentSummary').classList[0] == 'active') {
         adjustmentSummarySearchGrid.resizeCanvas(true); 
        }

      if (gearAdjustmentGrid != undefined && document.getElementById('gearAdjustmentData').classList[0] == 'active') { 
        gearAdjustmentGrid.resizeCanvas(true); 
      }

      if (detailGrid != undefined && document.getElementById('reconciliationDetailData').classList[0] == 'active') { 
        detailGrid.resizeCanvas(true); 
      }

      if (syncStatusGrid != undefined && document.getElementById('syncStatus').classList[0] == 'active') { 
        syncStatusGrid.resizeCanvas(true); 
      }


    });

    if (gridId != undefined) {
      $('#' + gridId).height($('#' + gridId).height() + 11);
    }
  }
  function _clearAllGrids() {
    if (summarySearchGrid != undefined)
      summarySearchGrid.dataView.setItems([]);
    if (simulatedSummaryGrid != undefined)
      simulatedSummaryGrid.dataView.setItems([]);
    if (searchGrid != undefined)
      searchGrid.dataView.setItems([]);
    if (adjustmentSummarySearchGrid != undefined)
      adjustmentSummarySearchGrid.dataView.setItems([]);
  }

  function _clearGrid(gridId) {
    treasury.common.grid.clearGrid(gridId);
    if ($("#" + gridId).prev().prev().is("p") ||
      $("#" + gridId).prev().prev().is("center")) {
      $("#" + gridId).prev().prev().remove();
    } else if ($("#" + gridId).prev().is("p") ||
      $("#" + gridId).prev().is("center")) {
      $("#" + gridId).prev().remove();
    }
    var groupingHeader = document.getElementById(gridId + '-drop-element-holder');
    if (groupingHeader != null) {
      groupingHeader.innerHTML = "";
    }

    var pager = document.getElementById(gridId + '-pager');
    if (pager != null) {
      pager.innerHTML = "";
    }

    var header = document.getElementById(gridId + "-header");
    if (header != null) {
      header.innerHTML = "";
    }
  }

  function _loadSearchGrid(ajaxCall, gridId, columnsFunction,
    gridOptionFunction, gridVarCallBack) {
    showLoading("mainLoader");
    ajaxCall
      .done(function (data) {

        hideLoading("mainLoader");
        for (var i = 0; i < data.length; i++) {
          data[i] = treasury.financing.reconciliation.common.actions.flattenJsonResponse(data[i]);
        }
        if (searchGrid != undefined)
          searchGrid.dataView.setItems([]);
        if (data && !data.hasOwnProperty('errorMessage') && data.length != 0) {

          var searchGrid = new dportal.grid.createGrid(
            $("#" + gridId), data, columnsFunction(),
            gridOptionFunction());
          if (document.getElementById('filterSidebar').state == "expanded") {
            document.getElementById('filterSidebar').toggle();
          }
          treasury.financing.reconciliation.common.actions.resizeCanvasOnGridChange(
            searchGrid, gridId);
          searchGrid.resizeCanvas(true);
          gridVarCallBack(searchGrid);
            if(data[0]["reportingFxRate"]) {
                    _showReportingCurrencyMessage(gridId, true, data[0]["reportingCurrency"]);
                }
            else {
                    _showReportingCurrencyMessage(gridId, false);
                }
        } else if (data && data.errorMessage) {
          hideLoading("mailLoader");
          raiseErrorToast("Error occured while searching for results");
          _hideReportingCurrencyMessage(gridId);
        } else if (data && data.length == 0) {
          hideLoading("mailLoader");
          raiseInfoToastWithTimeOut("No data found");
          _hideReportingCurrencyMessage(gridId);
        }
        if (document.getElementById('filterSidebar').state == "collapsed" && searchGrid == undefined) {
          document.getElementById('filterSidebar').toggle();
          document.getElementById('filterSidebar').style.width = '20px';
        }

      });
    ajaxCall.fail(function (xhr, text, errorThrown) {
      hideLoading("mailLoader");
      raiseErrorToast("Error occured while searching for results");
       _hideReportingCurrencyMessage(gridId);
    });
  }

  function _showReportingCurrencyMessage(viewPrefix, isFxRatePresent, cur) {
          var viewId = viewPrefix + "ReportingCurrencyMessage";
          var rcMessage = document.querySelector(`#${viewId}`);
          rcMessage.style.display = "block";
          if(isFxRatePresent) {
              rcMessage.className = "arc-message";
              rcMessage.innerHTML = `Reporting Currency (RC) corresponds to ${cur}`;
          }
          else {
              rcMessage.className = "arc-message--critical";
              rcMessage.innerHTML = "Fx Rate unavailable for selected Reporting Currency";
          }
      }

      function _hideReportingCurrencyMessage(viewPrefix) {
          var viewId = viewPrefix + "ReportingCurrencyMessage";
          var rcMessage = document.querySelector(`#${viewId}`);
          rcMessage.style.display = "none";
      }


})();
