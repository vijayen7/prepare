"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.commonColumnsConfig = {
    currencyFormatter: _currencyFormatter,
  };
  function _formatterUtil(number)
  {
      number  = Math.round(number);
      var stringnumber = number+"";
      var is_neg = 0;
      if(stringnumber.charAt(0) == '-')
      {
         is_neg = 1;
         stringnumber = stringnumber.substring(1);
      }
      return stringnumber;
  }
  function _currencyFormatter(number)
  {
      if(number == 0)
      {
        return "0.00";
      }
      var is_neg = 0;
      if(number < 0)
        is_neg = 1;
      number = number*100;
      var retnumber = _formatterUtil(number);
      while(retnumber.length < 3)
      retnumber = "0"+retnumber;
      retnumber  = retnumber.substring(0,retnumber.length-2) + "." + retnumber.substring(retnumber.length-2);
      var mod3 = 0;
      var returnval = "";
      for(var i = retnumber.length-4;i>=0;i--)
      {
        returnval = retnumber.charAt(i)+returnval;
        if(mod3 == 2 && i!=0)
         returnval = ","+returnval;
         mod3 = (mod3+1)%3;
      }
      returnval =  returnval + retnumber.substring(retnumber.length-3);
      if(is_neg == 1)
      returnval = "(" +returnval+ ")";
      return returnval;
  }
})();
