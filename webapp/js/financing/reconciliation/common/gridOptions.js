"use strict";
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.common = window.treasury.financing.reconciliation.common || {};
  window.treasury.financing.reconciliation.common.grid = {
    getGridOptions: _getGridOptions,
    getSummaryAndAdjustmentGridOptions: _getSummaryAndAdjustmentGridOptions,
    getSummaryGridOptions: _getSummaryGridOptions,
    getSyncStatusGridOption: _getSyncStatusGridOption,
    getAcuralAndBrokerAdjustmentGridOptions: _getAcuralAndBrokerAdjustmentGridOptions,
  };

  function _getGridOptions() {
    return {
      autoHorizontalScrollBar: true,
      nestedTable: false,
      highlightRowOnClick: true,
      sortList: [{
        columnId: "isAutoApproved",
        sortAsc: true
      }],
      editable: true,
      asyncEditorLoading: false,
      autoEdit: false,
      summaryRow: true,
      displaySummaryRow: true,
      summaryRowText: "Total",
      applyFilteringOnGrid: true,
      page : true,
      exportToExcel: true,
      maxHeight : 650,
      expandCollapseAll: true,
      enableMultilevelGrouping: {
      showGroupingTotals: true,
      customGroupingRendering: true,
      hideGroupingHeader: false,
      },
    };
  }

  function _getSummaryAndAdjustmentGridOptions() {
    var gridOptions = _getGridOptions()
    gridOptions.onDblClick =  function(item, rowObject$, isNotToggle) {
      for ( var tempItem in adjustmentSummarySearchGrid.dataView.getItems()) {
        if( adjustmentSummarySearchGrid.dataView.getItems()[tempItem].agreementKey === item.value ) {         
        var key  =  adjustmentSummarySearchGrid.dataView.getItems()[tempItem];
        var formattedDate = getFormattedDate('datePicker');
        var parameters = {};
        parameters["agreementTypeId"] = key.agreementTypeId;
        parameters["legalEntityId"] = key.legalEntityId;
        parameters["cpeId"] = key.cpeId;
        parameters["dateString"] = formattedDate;
        treasury.financing.reconciliation.AdjustmentSummaryAction.loadAccuralAndBrokerPostingResults(parameters);
         break;
         }
        }
      },
    gridOptions['checkboxHeader'] = true;
    gridOptions['frozenColumn'] = 1;
    gridOptions['maxHeight'] = 480;
    gridOptions['useAvailableScreenSpace'] = false;
    gridOptions.enableMultilevelGrouping.initialGrouping = ['agreementKey','chargeType','currency'];
    return gridOptions;
  
}

function _getAcuralAndBrokerAdjustmentGridOptions() { 
   var gridOptions = _getGridOptions();
   gridOptions['maxHeight'] = 180;
   gridOptions['useAvailableScreenSpace'] = false;
   gridOptions['expandCollapseAll'] = false;
   gridOptions['applyFilteringOnGrid'] = false;
   gridOptions['page'] = false;
   gridOptions.enableMultilevelGrouping= {hideGroupingHeader: true};
   return gridOptions;
}

  function _getSummaryGridOptions() {
    var gridOptions = _getGridOptions()
    gridOptions['checkboxHeader'] = true;
    gridOptions['frozenColumn'] = 4;
    gridOptions['useAvailableScreenSpace'] = false;
    gridOptions['maxHeight'] = 550;
    return gridOptions;
}

  function _getSyncStatusGridOption() {
    return {
      autoHorizontalScrollBar : true,
      nestedTable : false,
      expandTillLevel : -1,
      highlightRowOnClick : true,
      editable : true,
      asyncEditorLoading : false,
      autoEdit : false,
      useAvailableScreenSpace : true,
      applyFilteringOnGrid : true,
      exportToExcel : true,
    }
  }

})();
