"use-strict"

function setupMonthDateFilter(dateDiv, errorDiv, date, minDate, maxDate, dateFormat) {
  dateDiv.monthpicker({
    changeMonth: true,
    changeYear: true,
    dateFormat:dateFormat,
    minDate: minDate,
    maxDate: maxDate,
    showOn: "both",
    onClose: function(inst) {
    treasury.financing.reconciliation.summary.viewLatestSync();
  },
    buttonText: "<i class='fa fa-calendar'></i>"
  });
}

function setupDateFilter(dateDiv, errorDiv, date, minDate, maxDate,
  dateFormat) {
  dateDiv.datepicker({
    dateFormat: dateFormat,
    changeMonth: true,
    changeYear: true,
    minDate: minDate,
    maxDate: maxDate,
    showOn: "both",
    buttonText: "<i class='fa fa-calendar'></i>",
  }).datepicker("setDate", date);
  dateDiv.change(function() {
    validateDate(dateDiv.val(), {
      errorDiv: errorDiv,
      minDate: minDate,
      maxDate: maxDate,
      dateFormat: dateFormat,
    });
  });
}


function getFormattedDate(elementId) {
  var datepicker = document.getElementById(elementId);
  var datepicker_value = datepicker.value;
  var date = new Date (new Date(datepicker_value).toLocaleString("en-US",{ timeZone: 'UTC' }));
  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;
  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  return year + month + day;
}

function showLoading(id) {
    $("#" + id).removeAttr("hidden");
    $("#" + id + "Overlay").removeAttr("hidden");
  }

  function hideLoading(id) {
    $("#" + id).attr("hidden", "true");
    $("#" + id + "Overlay").attr("hidden", "true");
  }

function raiseInfoToast(message) {
  toastr.options.timeOut = "0";
  toastr.options.positionClass = "toast-bottom-right";
  toastr.options.extendedTimeOut = "0";
  return toastr.info(message);
}

function raiseInfoToastWithTimeOut(message) {
  toastr.options.timeOut = "5000";
  toastr.options.positionClass = "toast-bottom-right";
  toastr.options.extendedTimeOut = "1000";
  return toastr.info(message);
}

function raiseSuccessToast(message) {
  toastr.options.timeOut = "5000";
  toastr.options.positionClass = "toast-bottom-right";
  toastr.options.extendedTimeOut = "1000";
  return toastr.success(message);
}

function raiseErrorToast(message) {
  toastr.options.timeOut = "5000";
  toastr.options.positionClass = "toast-bottom-right";
  toastr.options.extendedTimeOut = "0";
  toastr.options.closeButton = "true";
  return toastr.error(message);
}

function customLegalEntityAgreegator(field) {
  this.field_ = field;
  this.init = function() {
    this.legalEntity_ = "";
  };
  this.accumulate = function(item) {
    this.legalEntity_ = item['legalEntity']
  };
  this.storeResult = function(groupTotals) {
    if (!groupTotals.legalEntity) {
      groupTotals.legalEntity = {};
    }
    groupTotals.legalEntity[this.field_] = this.legalEntity_
  };
}

function customCpeAgreegator(field) {
  this.field_ = field;
  this.init = function() {
    this.cpe_ = "";
  };
  this.accumulate = function(item) {
    this.cpe_ = item['cpe']
  };
  this.storeResult = function(groupTotals) {
    if (!groupTotals.cpe) {
      groupTotals.cpe = {};
    }
    groupTotals.cpe[this.field_] = this.cpe_
  };
}



function customAgreementTypeAgreegator(field) {
  this.field_ = field;
  this.init = function() {
    this.agreementType_ = "";
  };
  this.accumulate = function(item) {
    this.agreementType_ = item['agreementType']
  };
  this.storeResult = function(groupTotals) {
    if (!groupTotals.agreementType) {
      groupTotals.agreementType = {};
    }
    groupTotals.agreementType[this.field_] = this.agreementType_
  };
}

function customCommentsAgreegator(field) {
  this.field_ = field;
  this.init = function() {
    this.comment_ = "";
  };
  this.accumulate = function(item) {
    this.comment_ = item['comments']
  };
  this.storeResult = function(groupTotals) {
    if (!groupTotals.comment) {
      groupTotals.comment = {};
    }
    groupTotals.comment[this.field_] = this.comment_
  };
}

function customSimulationStatusAgreegator(field) {
  this.field_ = field;
  this.init = function() {
    this.simulationStatus_ = "SUCCESSFUL";
  };
  this.accumulate = function(item) {
    this.simulationStatus_ = (this.simulationStatus_ == "SUCCESSFUL" &&
        (item['simulationStatus'] == "SUCCESSFUL" || item['simulationStatus'] == "Zero Adjustment")) ? "SUCCESSFUL" : "";

  };
  this.storeResult = function(groupTotals) {
    if (!groupTotals.simulationStatus) {
      groupTotals.simulationStatus = {};
    }
    groupTotals.simulationStatus[this.field_] = this.simulationStatus_;
  };
}

function customBooleanAggregator(field) {
  this.field_ = field;
  this.init = function() {
    this.value_ = true;
  };
  this.accumulate = function(item) {
    this.value_ = (this.value_ == true && item[field] == true);

  };
  this.storeResult = function(groupTotals) {
    if (!groupTotals.booleanSum) {
      groupTotals.booleanSum = {};
    }
    groupTotals.booleanSum[this.field_] = this.value_;
  };
}
function customCurrencyAgreegator(field) {
  this.field_ = field;
  this.init = function() {
    this.currency_ = "";
  };
  this.accumulate = function(item) {
    this.currency_ = item['currency']
  };
  this.storeResult = function(groupTotals) {
    if (!groupTotals.currency) {
      groupTotals.currency = {};
    }
    groupTotals.currency[this.field_] = this.currency_
  };
}

function customChargeTypeAgreegator(field) {
  this.field_ = field;
  this.init = function() {
    this.chargeType = "";
  };
  this.accumulate = function(item) {
    this.chargeType_ = item['chargeType']
  };
  this.storeResult = function(groupTotals) {
    if (!groupTotals.chargeType) {
      groupTotals.chargeType = {};
    }
    groupTotals.chargeType[this.field_] = this.chargeType_
  };
}
