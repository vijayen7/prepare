"use strict";
var searchFilterGroup;
var saveButtonDisplayStatus;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.landing = {
    loadLandingView: _loadLandingView,
    loadSearchResults: _loadSearchResults,
    initializeView: _initialize,
    resetView: _resetView,
    loadViewForId: _loadViewForId,
    removeCurrentActiveMenu: _removeCurrentActiveMenu,
    resetSearchFilters: _resetSearchFilters,
    copySearchURL: _copySearchURL
  };

  window.addEventListener("WebComponentsReady", _initialize);

  function _loadPageForId(type) {
	  return {
		  'reconciliationDetailData':window.treasury.financing.reconciliation.detailData.loadPage,
		  'reconciliationSummary': window.treasury.financing.reconciliation.Summary.loadPage,
		  'adjustmentSummary': window.treasury.financing.reconciliation.adjustmentSummary.loadPage,
		  'simulatedAdjustmentData': window.treasury.financing.reconciliation.Summary.loadSimulatedAdjustmentsPage,
		  'gearAdjustmentData' : window.treasury.financing.reconciliation.gearAdjustmentData.loadPage,
		  'syncStatus' : window.treasury.financing.reconciliation.syncStatus.loadPage,
	  }[type];
  }

  function _getCopyURLToLoad(type) {
	  return {
		  'reconciliationDetailData':window.treasury.financing.reconciliation.detailData.copySearchURL,
		  'reconciliationSummary': window.treasury.financing.reconciliation.Summary.copySearchURL,
		  'adjustmentSummary': window.treasury.financing.reconciliation.adjustmentSummary.copySearchURL,
		  'gearAdjustmentData':window.treasury.financing.reconciliation.gearAdjustmentData.copySearchURL,
	  }[type];
  }

  function _getHandler(type) {
    return {
      'reconciliationSummary': window.treasury.financing.reconciliation.Summary.registerHandlers,
      'reconciliationDetailData': window.treasury.financing.reconciliation.detailData.registerHandlers,
      'adjustmentSummary': window.treasury.financing.reconciliation.adjustmentSummary.registerHandlers,
      'simulatedAdjustmentData': window.treasury.financing.reconciliation.detailData.registerHandlers,
      'gearAdjustmentData': window.treasury.financing.reconciliation.gearAdjustmentData.registerHandlers,
      'syncStatus': window.treasury.financing.reconciliation.syncStatus.registerHandlers,
    }[type];
  }

  function _getSearchToLoad(type) {
    return {
      'reconciliationSummary': window.treasury.financing.reconciliation.Summary.loadSearchResults,
      'reconciliationDetailData': window.treasury.financing.reconciliation.detailData.loadSearchResults,
      'adjustmentSummary': window.treasury.financing.reconciliation.adjustmentSummary.loadSearchResults,
      'gearAdjustmentData': window.treasury.financing.reconciliation.gearAdjustmentData.loadSearchResults,
      'syncStatus': window.treasury.financing.reconciliation.syncStatus.loadSearchResults,
    }[type];
  }


  function _getpageDestructor(type) {
    return {
      'reconciliationSummary': window.treasury.financing.reconciliation.Summary.pageDestructor,
      'reconciliationDetailData': window.treasury.financing.reconciliation.detailData.pageDestructor,
      'adjustmentSummary': window.treasury.financing.reconciliation.adjustmentSummary.pageDestructor,
      "simulatedAdjustmentData": window.treasury.financing.reconciliation.Summary.simulatedAdjustmentsPageDestructor,
      'gearAdjustmentData': window.treasury.financing.reconciliation.gearAdjustmentData.pageDestructor,
      'syncStatus': window.treasury.financing.reconciliation.syncStatus.pageDestructor,
    }[type];
  }

  function _registerHandlers() {

    document.getElementById('reconciliationSummary').onclick = function() {
      _resetView();
      _loadViewForId('reconciliationSummary');
    };

    document.getElementById('reconciliationDetailData').onclick = function() {
      _resetView();
      _loadViewForId('reconciliationDetailData');
    };

    document.getElementById('adjustmentSummary').onclick = function() {
      _resetView();
      _loadViewForId('adjustmentSummary');
    };
    document.getElementById('simulatedAdjustmentData').onclick = function() {
      _resetView();
      _loadViewForId('simulatedAdjustmentData');
    };
    document.getElementById('gearAdjustmentData').onclick = function() {
          _resetView();
          _loadViewForId('gearAdjustmentData');
    };
    document.getElementById('syncStatus').onclick = function() {
          _resetView();
          _loadViewForId('syncStatus');
    };
    document.getElementById("search").onclick = treasury.financing.reconciliation.landing.loadSearchResults;
    document.getElementById("resetSearch").onclick = treasury.financing.reconciliation.landing.resetSearchFilters;
    document.getElementById('copySearchURL').addEventListener('click', treasury.financing.reconciliation.landing.copySearchURL);
  }

  function _resetSearchFilters(){

    document.getElementById('legalEntityFilter').value = [];
    document.getElementById('cpeEntityFilter').value = [];
    document.getElementById('agreementTypeFilter').value = [];
    document.getElementById('currencyFilter').value = [];
    document.getElementById('chargeTypeFilterAdd').value = [];
    document.getElementById('sourceTypeFilter').value =[];
    document.getElementById("showZeroDifference").checked = true;
    document.getElementById('syncTypeFilter').value = ""
    document.getElementById('reportingCurrencyFilter').value = ""

  }

  function _resetView() {
    var id = _getActiveMenuId();
    _getpageDestructor(id)();
    _removeCurrentActiveMenu(id);
  }



  function _loadViewForId(id) {
    document.getElementById(id).classList.add('active');
    _getHandler(id)();

    _loadPageForId(id)();
  }

  function _initialize() {
    _registerHandlers();
    _initializeSaveSettings();
    treasury.financing.reconciliation.landing.loadLandingView();
  }

  function _loadLandingView() {
    if(!treasury.defaults.toastEnabled){
      $("#gearAdjustmentData").removeClass("hidden");
      $("#syncStatus").removeClass("hidden");
      $("#simulatedAdjustmentData").removeClass("hidden");
      $("#adjustmentSummary").removeClass("hidden");
    }
    _loadLandingViewFilters();
    _loadViewForId('reconciliationSummary');
  }

  function _loadLandingViewFilters() {


    $
      .when(treasury.loadfilter.legalEntities(),
        treasury.loadfilter.cpes(),
        treasury.loadfilter.loadAgreementTypes(),
        treasury.loadfilter.currencies(),
        treasury.loadfilter.chargeTypes(),
        treasury.loadfilter.sourceType(),
        treasury.loadfilter.reconciliationSyncType())
      .done(
        function(legalEntitiesData, cpesData,
          agreementTypesData, currenciesData,
          chargeTypesData, sourceTypeData,
          syncTypeData) {

          var legalEntityFilter = new window.treasury.filter.MultiSelectNew(
            "legalEntityFilterIds",
            "legalEntityFilter",
            legalEntitiesData[0].descoEntities, []);

          var cpeEntityFilter = new window.treasury.filter.MultiSelectNew(
            "cpeFilterIds", "cpeEntityFilter",
            cpesData[0].cpes, []);

          var agreementTypeFilter = new window.treasury.filter.MultiSelectNew(
            "agreementTypeFilterIds",
            "agreementTypeFilter",
            agreementTypesData[0].agreementTypes, []);

          var currencyFilter = new window.treasury.filter.MultiSelectNew(
            "currencyFilterIds", "currencyFilter",
            currenciesData[0].currency, []);
            
          chargeTypesData[0].chargeTypes =  chargeTypesData[0].chargeTypes.filter(function(e) {return (e[0] !=3 && e[0] != 6 && e[0] !=7);});

          var chargeTypeFilter = new window.treasury.filter.MultiSelectNew(
              "chargeTypeFilterIds", "chargeTypeFilterAdd",
              chargeTypesData[0].chargeTypes, []);


          var sourceTypeFilter = new window.treasury.filter.MultiSelectNew(
            "sourceTypeFilterIds", "sourceTypeFilter",
            sourceTypeData[0].sourceType, []);

          var syncTypeFilter = new window.treasury.filter.SingleSelect(
            "syncTypeFilterId", "syncTypeFilter",
            syncTypeData[0].reconciliationSyncType, []);

          var reportingCurrencyFilterId = new window.treasury.filter.SingleSelect(
                                  "reportingCurrencyFilterId", "reportingCurrencyFilter",
                                  currenciesData[0].currency, [1760000]);

          var dateValue = treasury.defaults.date ||
            Date.today();
            var dateFilterValue = new Date(dateValue);
            dateFilterValue.setDate(1);
            dateFilterValue.setMonth(dateFilterValue.getMonth()-1);

          var minDate = Date.parse('1970-01-01');
          var maxDate = Date.parse('2038-01-01');
          var dateFormat = 'yy-mm';
          setupMonthDateFilter($("#datePicker"),
            $("#datePickerError"), dateFilterValue,
            minDate, maxDate, dateFormat);
          var month = "0" + (Number(dateFilterValue.getMonth()) + Number(1));

          document.getElementById("datePicker").value = dateFilterValue.getFullYear() + "-" + month.slice(-2);

          var settleDateFilterValue =Date.today();
          settleDateFilterValue.setDate(20);
          var minDate = Date.parse('1970-01-01');
          var maxDate = Date.parse('2038-01-01');
          var dateFormat = 'yy-mm-dd';
          setupDateFilter($("#datePickerSettleDate"),
            $("#datePickerError"), settleDateFilterValue, minDate, maxDate, dateFormat);

          searchFilterGroup = new window.treasury.filter.FilterGroup(
            [legalEntityFilter, cpeEntityFilter,
              agreementTypeFilter,
              currencyFilter,
              chargeTypeFilter,
              sourceTypeFilter,
            ], [syncTypeFilter, reportingCurrencyFilterId],
            $("#datePicker"), null,null);
          treasury.financing.reconciliation.summary.viewLatestSync();

            hideLoading("mainLoader");
          treasury.financing.reconciliation.summary.viewLatestSync();
        });
  }

  function _setFilters(values) {
    searchFilterGroup.setParameterMap(values["filterGroupParameterMap"]);
    if (values["show-all-yes"] == true) {
      document.getElementById("show-all-yes").checked = true;
    } else {
      document.getElementById("show-all-no").checked = true;
    }
    document.getElementById("showZeroDifference").checked = values["showZeroDifference"];
  }

  function _loadSearchResults() {
    var searchFunction = _getSearchToLoad(_getActiveMenuId());
    searchFunction();
  }

  function _copySearchURL() {
    var searchFunction = _getCopyURLToLoad(_getActiveMenuId());
    searchFunction();
  }

  function _getActiveMenuId() {
    return document.getElementById('reconciliationMenu')
      .getElementsByClassName("active")[0].id;
  }

  function _removeCurrentActiveMenu(id) {
    document.getElementById(id).classList.remove('active');
  }

  function _initializeSaveSettings() {
    var saveSettings = document.querySelector('arc-save-settings');
    var host = '';
    var clientName = document.getElementById("clientName").value;
    var stabilityLevel = document.getElementById("stabilityLevel").value;

    if(clientName === 'desco') {
        if(stabilityLevel == 'prod') {
            host += "http://landing-app.deshaw.c.ia55.net";
        } else if(stabilityLevel == 'uat'){
            host = "http://landing-app.deshawuat.c.ia55.net";
        } else if(stabilityLevel == 'qa'){
            host = "https://mars.arcesium.com";
        } else if(stabilityLevel == 'dev'){
            host = "https://terra.arcesium.com";
        }
    }
    saveSettings.serviceURL = host+'/service/SettingsService';
    saveSettings.applicationId = 3;
    saveSettings.applicationCategory = 'FinancingReconciliation';

    document.getElementById('summary-search-save').onclick = function(){
      saveSettings.openSaveDialog();
    };

    saveSettings.saveSettingsCallback = _saveSettings;

    saveSettings.applySettingsCallback = _applySettings;

    saveSettings.retrieveSettings();
  }

  function _saveSettings() {
    var values = {
          "filterGroupParameterMap" :   searchFilterGroup.getParameterMap(),
          "show-all-yes"            :   document.getElementById("show-all-yes").checked,
          "showZeroDifference"      :   document.getElementById("showZeroDifference").checked,
      };
    return values;
  }

  function _applySettings(searchData) {
//    var datepicker = document.getElementById('datepicker');
//    searchData["dateString"] = datepicker.value;
    _setFilters(searchData);
  }
})();
