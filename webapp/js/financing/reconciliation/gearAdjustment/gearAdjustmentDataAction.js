"use strict";
var gearAdjustmentGrid

(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.gearAdjustmentDataAction = {
    loadResults: _loadResults,
  };

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/reconciliation/gearAdjustmentData-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.financing.reconciliation.common.actions.clearGrid("gearAdjustmentDataGrid");
    treasury.financing.reconciliation.common.actions.loadSearchGrid(serviceCall, "gearAdjustmentDataGrid",
    treasury.financing.reconciliation.detailDataColumnsConfig.getColumns,
    treasury.financing.reconciliation.common.grid.getGridOptions,
    function(gridVar) {
      gearAdjustmentGrid = gridVar;
    })
  }

})();
