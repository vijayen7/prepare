"use strict";
var simulationFilterGroup;
var searchFilterGroupDetailData;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.gearAdjustmentData = {
    loadSearchResults: _loadSearchResults,
    registerHandlers: _registerHandlers,
    pageDestructor: _pageDestructor,
    loadLandingViewFilter: _loadLandingViewFilter,
    loadDetailDataScreen: _loadDetailDataScreen,
    loadPage: _loadPage,
    copySearchURL: _copySearchURL
  };

  function _registerHandlers() {
  }

  function _copySearchURL() {
    var url = "/treasury/financing/reconciliation/gearAdjustmentData-results";
    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    var showZeroDifference = document.getElementById("showZeroDifference").checked;
    parameters.dateString = formattedDate;
    generateReportURL(url, parameters, "resultList");
  }


  function _loadPage() {
	  document.getElementById("filterSidebar").setAttribute('size', '300px');
      window.treasury.financing.reconciliation.common.actions.resizeAllCanvas();
      _loadLandingViewFilter();
      $("#gearAdjustmentDataSearch").removeClass("hidden")
      if (document.getElementById('filterSidebar').state == "collapsed") {
        document.getElementById('filterSidebar').toggle();
      }

      if (gearAdjustmentGrid != undefined) {
          document.getElementById('filterSidebar').toggle();
          document.getElementById("gearAdjustmentDataSearch").setAttribute("width", $("gearAdjustmentDataSearch").width());
          gearAdjustmentGrid.resizeCanvas(true);
      }
  }

  function _pageDestructor() {
	  $("#chargeTypeParentDiv").addClass("hidden");
	  $("#currencyParentDiv").addClass("hidden");
	  $("#reportingCurrencyTypeDiv").addClass("hidden");

	  $("#gearAdjustmentDataSearch").addClass("hidden");
  }

  function _loadLandingViewFilter() {


	  $("#chargeTypeParentDiv").removeClass("hidden");
	  $("#currencyParentDiv").removeClass("hidden");
	  $("#reportingCurrencyTypeDiv").removeClass("hidden");
  }

  function _loadDetailDataScreen(legalEntityId, cpeId, agreementTypeId, ccySpn, chargeType, reportingCurrencyFilterId) {

	  var parameters = {};

	  parameters.legalEntityFilterIds = legalEntityId
	  parameters.currencyFilterIds = ccySpn
	  parameters.agreementTypeFilterIds = agreementTypeId
	  parameters.cpeFilterIds = cpeId
	  parameters.chargeTypeFilter = chargeType;
	  parameters.reportingCurrencyFilterId=reportingCurrencyFilterId;
	  parameters.dateString = getFormattedDate('datePicker');

	  window.treasury.financing.reconciliation.landing.resetView();
	  window.treasury.financing.reconciliation.landing.loadViewForId('gearAdjustmentData');

	  treasury.financing.reconciliation.gearAdjustmentData.loadResults(parameters);
  }

  function _loadSearchResults() {

    if (document.getElementById("datePickerError").innerHTML != "") {
      raiseErrorToast("Please select a valid Date");
      return;
    }
    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    parameters.dateString = formattedDate;

    treasury.financing.reconciliation.gearAdjustmentDataAction.loadResults(parameters);
  }
})();
