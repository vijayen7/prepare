"use strict";

var detailGrid;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.detailDataAction = {
    loadResults: _loadResults,
  };

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/reconciliation/detailData-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
  treasury.financing.reconciliation.common.actions.clearGrid("detailDataGrid");
    if(parameters.showConsolidatedDetailData){
      treasury.financing.reconciliation.common.actions.loadSearchGrid(serviceCall, "detailDataGrid",
      treasury.financing.reconciliation.detailDataColumnsConfig.getConsolidatedDetailDataColumns,
      treasury.financing.reconciliation.common.grid.getGridOptions,function(gridVar) {
        detailGrid = gridVar;
      });
    }
    else{
      treasury.financing.reconciliation.common.actions.loadSearchGrid(serviceCall, "detailDataGrid",
      treasury.financing.reconciliation.detailDataColumnsConfig.getColumns,
      treasury.financing.reconciliation.common.grid.getGridOptions,function(gridVar) {
        detailGrid = gridVar;
      });
    }

  }
})();
