"use strict";
var simulationFilterGroup;
var searchFilterGroupDetailData;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.detailData = {
    loadSearchResults: _loadSearchResults,
    registerHandlers: _registerHandlers,
    pageDestructor: _pageDestructor,
    loadLandingViewFilter: _loadLandingViewFilter,
    loadDetailDataScreen: _loadDetailDataScreen,
    loadPage: _loadPage,
    copySearchURL: _copySearchURL
  };

  function _registerHandlers() {
  }

  function _copySearchURL() {
    var url = "/treasury/financing/reconciliation/detailData-results";
    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    var showZeroDifference = document.getElementById("showZeroDifference").checked;
    parameters.dateString = formattedDate;
    generateReportURL(url, parameters, "resultList");

  }


  function _loadPage() {

	  document.getElementById("filterSidebar").setAttribute('size', '300px');
      window.treasury.financing.reconciliation.common.actions.resizeAllCanvas();
      _loadLandingViewFilter();
      $("#detailDataSearch").removeClass("hidden");
      $("#showConsolidatedDetailDataDiv").removeClass("hidden");
      
      if(document.getElementById('filterSidebar').state == "expanded"){
        if(detailGrid !=undefined){
          document.getElementById('filterSidebar').toggle();
          detailGrid.resizeCanvas(true);
        }
      }else{
        if(detailGrid == undefined){
          document.getElementById('filterSidebar').toggle();
        }else{ detailGrid.resizeCanvas(true);}
      }

  }

  function _pageDestructor() {


	  $("#chargeTypeParentDiv").addClass("hidden");
	  $("#sourceTypeParentDiv").addClass("hidden");
	  $("#currencyParentDiv").addClass("hidden");
    $("#showConsolidatedDetailDataDiv").addClass("hidden");
     $("#reportingCurrencyTypeDiv").addClass("hidden");

	  $("#detailDataSearch").addClass("hidden");
  }

  function _loadLandingViewFilter() {



	  $("#chargeTypeParentDiv").removeClass("hidden");
	  $("#sourceTypeParentDiv").removeClass("hidden");
	  $("#currencyParentDiv").removeClass("hidden");
	   $("#reportingCurrencyTypeDiv").removeClass("hidden");
  }

  function _loadDetailDataScreen(legalEntityId, cpeId, agreementTypeId, ccySpn, chargeTypeId, reportingCurrencyFilterId) {

	  var parameters = {};

	  parameters.legalEntityFilterIds = legalEntityId
	  parameters.currencyFilterIds = ccySpn
	  parameters.agreementTypeFilterIds = agreementTypeId
	  parameters.cpeFilterIds = cpeId
	  parameters.chargeTypeFilterIds = chargeTypeId;
	  parameters.reportingCurrencyFilterId=reportingCurrencyFilterId;
	  parameters.dateString = getFormattedDate('datePicker');

	  window.treasury.financing.reconciliation.landing.resetView();
	  window.treasury.financing.reconciliation.landing.loadViewForId('reconciliationDetailData');

	  treasury.financing.reconciliation.detailDataAction.loadResults(parameters);


  }

  function _loadSearchResults() {

    if (document.getElementById("datePickerError").innerHTML != "") {
      raiseErrorToast("Please select a valid Date");
      return;
    }
    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    var showConsolidatedDetailData = document.getElementById("showConsolidatedDetailData").checked;
    parameters.dateString = formattedDate;
    parameters.showConsolidatedDetailData = showConsolidatedDetailData;
    
    treasury.financing.reconciliation.detailDataAction.loadResults(parameters);
  }
})();
