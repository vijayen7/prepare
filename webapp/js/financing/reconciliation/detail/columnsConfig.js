"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.detailDataColumnsConfig = {
    getColumns: _getColumns,
    getConsolidatedDetailDataColumns: _getConsolidatedDetailDataColumns,
  };

  function _getCommonColumns() {
    var columns =   [ {
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legalEntity",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "cpe",
      type: "text",
      name: "Counterparty",
      field: "cpe",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "currency",
      type: "text",
      name: "Currency",
      field: "currency",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "chargeType",
      type: "text",
      name: "Charge Type",
      field: "chargeType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "custodianAccount",
      type: "text",
      name: "Custodian Account",
      field: "custodianAccount",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreementType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"

    }, {
      id: "book",
      type: "text",
      name: "Book",
      field: "book",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "bundle",
      type: "text",
      name: "Bundle",
      field: "bundle",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "sourceTypeId",
      type: "text",
      name: "Source",
      field: "sourceType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },  {
      id: "accountType",
      type: "text",
      name: "Account Type",
      field: "accountType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "balance",
      type: "text",
      name: "Balance (Local)",
      field: "balance",
      sortable: true,
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      excelFormatter: "#,##0",

    }, {
      id: "balanceUSD",
      type: "text",
      name: "Balance (USD)",
      field: "balanceUSD",
      sortable: true,
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      excelFormatter: "#,##0",
      aggregator: dpGrid.Aggregators.sum,

      formatter: function(row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(parseFloat(value)) + "</div>";
      },

    },
      {
            id: "balanceRC",
            type: "text",
               name: "Balance (RC)",
               field: "balanceRC",
               sortable: true,
               headerCssClass: "aln-rt b",
               cssClass: "amount",
               excelFormatter: "#,##0",
               aggregator: dpGrid.Aggregators.sum,

               formatter: function(row, cell, value, columnDef, dataContext) {
                 return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(parseFloat(value)) + "</div>";
               },

      },
     {
      id: "settleDate",
      type: "text",
      name: "Settle Date",
      field: "settleDate",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    } ];

    columns = columns.concat(getColumnsForNonToast());

    return columns;
  }

  function getColumnsForNonToast(){

    if(!treasury.defaults.toastEnabled){
      return [ {
        id: "financialAccount",
        type: "text",
        name: "Financial Account",
        field: "financialAccount",
        sortable: true,
        headerCssClass: "aln-rt b",
        excelFormatter: "#,##0"
      }];
    }
    return [];
  }

  function _getDetailColumns(){
    return [{
      id: "date",
      type: "text",
      name: "Date ",
      field: "date",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },{
      id: "userTransactionId",
      type: "text",
      name: "User transaction ID",
      field: "userTransactionId",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "adjustmentId",
      type: "text",
      name: "Adjustment ID",
      field: "adjustmentId",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    },{
      id: "fxRate",
      type: "text",
      name: "Fx Rate",
      field: "fxRate",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      formatter: function(row, cell, value, columnDef, dataContext) {
        return "<div style=\"text-align:right\">" + value + "</div>";
      }

    },{
      id: "actualSettleDate",
      type: "text",
      name: "Actual Date",
      field: "actualSettleDate",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "knowledgeDate",
      type: "text",
      name: "Knowledge Date",
      field: "knowledgeDate",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "actualBook",
      type: "text",
      name: "Actual Book",
      field: "actualBook",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "eventDate",
      type: "text",
      name: "Event Date",
      field: "eventDate",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, ];
  }

  function _getColumns() {
    var columns = _getCommonColumns();
    columns = columns.concat(_getDetailColumns());
    return columns;
  }

  function _getConsolidatedDetailDataColumns(){
      var columns = _getCommonColumns();
      return columns;
  }
})();
