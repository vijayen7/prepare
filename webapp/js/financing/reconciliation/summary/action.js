"use strict";
var summarySearchGrid;
var simulatedSummaryGrid;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.summary = {
    loadResults: _loadResults,
    simulateSelectedRecords: _simulateSelectedRecords,
    saveAdjustmentRecords: _saveAdjustmentRecords,
    allSync: _allSync,
    brokerAdjustmentSync: _brokerAdjustmentSync,
    viewLatestSync: _viewLatestSync,
  };

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/reconciliation/summary-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.financing.reconciliation.common.actions.clearGrid("summaryGrid");
    showLoading("mainLoader");
    _loadSummarySearchGrid(
      serviceCall,
      "summaryGrid",
      treasury.financing.reconciliation.SummaryColumnsConfig.getColumns,
      treasury.financing.reconciliation.common.grid.getSummaryGridOptions);
  }

  function _simulateSelectedRecords() {
    var dateString = document.getElementById("datePicker").value;
    var selectedData = [];
    var commentsFlag = 0;
    var isAdjustmentGreater = 0;
    var dif;
    var eps = 1e-2;
    var selectedIndexes = summarySearchGrid.selectedRowIds();
    $("#simulatedAdjustmentMessage").addClass("hidden");
    jQuery.each(selectedIndexes, function(index, value) {
      if (summarySearchGrid.dataView.getItemById(value).comments == null || summarySearchGrid.dataView.getItemById(
          value).comments == "")
        commentsFlag = 1;
      dif = summarySearchGrid.dataView.getItemById(value).accrual + summarySearchGrid.dataView.getItemById(value)
        .brokerPosting + summarySearchGrid.dataView.getItemById(value).approvedAdjustment;
      if (dif > 0 && (summarySearchGrid.dataView.getItemById(value).adjustment > Number((dif + eps).toFixed(2)) ||
          summarySearchGrid.dataView.getItemById(value).adjustment < 0))
        isAdjustmentGreater = 1;
      if (dif < 0 && (summarySearchGrid.dataView.getItemById(value).adjustment < Number((dif - eps).toFixed(2)) ||
          summarySearchGrid.dataView.getItemById(value).adjustment > 0))
        isAdjustmentGreater = 1;
      selectedData.push(summarySearchGrid.dataView.getItemById(value));
    });
    if (selectedData.length == 0) {
      raiseErrorToast("Please select one or more records to adjust.");
      return;
    }
    if (isAdjustmentGreater == 1) {
      raiseErrorToast("One or more Records has adjustment beyond limit!");
      return;
    }
    if (commentsFlag == 1) {
      raiseErrorToast("One or more selected records don't have Comments");
      return;
    }
    var parameters = {
      "selectedRecords": JSON.stringify(selectedData),
      "dateString": dateString,
    };

    var ajaxCall = $.ajax({
      url: 'treasury/financing/reconciliation/adjustment',
      data: parameters,
      type: "POST",
      dataType: "json"
    });
    treasury.financing.reconciliation.common.actions.clearGrid("simulatedSummaryGrid");
    _loadSimulatedAdjustmentDataGrid(
      ajaxCall,
      "simulatedSummaryGrid",
      treasury.financing.reconciliation.AdjustmentSummaryColumnsConfig.getColumns,
      treasury.financing.reconciliation.common.grid.getGridOptions);
    treasury.financing.reconciliation.landing.resetView();
    treasury.financing.reconciliation.landing
      .loadViewForId("simulatedAdjustmentData");

  }

  function _saveAdjustmentRecords() {
    var savedData = [];
    savedData = simulatedSummaryGrid.data;
    var parameters = {
      "savedRecords": JSON.stringify(savedData),
    }
    var ajaxCall = $.ajax({
      url: 'treasury/financing/reconciliation/save-adjustments',
      data: parameters,
      type: "POST",
    });
    treasury.financing.reconciliation.common.actions.clearGrid("simulatedSummaryGrid")
    $("#simulatedAdjustmentMessage").removeClass("hidden");
    showLoading("mainLoader");

    ajaxCall.done(function(response) {
      hideLoading("mainLoader");
      if (response != undefined &&
        response.hasOwnProperty("errorMessage")) {
        hideLoading("mainLoader");
        raiseErrorToast("Unable to save records");
      } else {
        hideLoading("mainLoader");
        raiseSuccessToast("Records have been successfully saved");

        summarySearchGrid.dataView.setItems([]);
        simulatedSummaryGrid.dataView.setItems([]);
        treasury.financing.reconciliation.landing.resetView();
        treasury.financing.reconciliation.landing.loadViewForId("reconciliationSummary","simulatedAdjustmentData");
        document.getElementById('loadingStateDialog').visible = true;
        //     document.getElementById('filterSidebar').toggle();
        treasury.financing.reconciliation.Summary.loadSearchResults();
      }
    })

  }

  function _updateSummaryGrid(data) {
   
    var item;
    var failedSimulantionCnt = 0;
    var expandGroupList = [];
    var selectedSummaryDataIndexes = summarySearchGrid.selectedRowIds();
    var newSelectedIndexes = [];
    for (var i = 0; i < selectedSummaryDataIndexes.length; i++) {
      item = summarySearchGrid.dataView.getItemById(selectedSummaryDataIndexes[i]);
      var isZeroAdjustment = data.simulationErrorMessages[item.id] == 'Zero Adjustment';
      if (!data.simulationErrorMessages.hasOwnProperty(String(item.id)) || isZeroAdjustment) {
        if (!isZeroAdjustment) {
          newSelectedIndexes.push(selectedSummaryDataIndexes[i])
        }
        item.simulationStatus = isZeroAdjustment ? "Zero Adjustment" : "SUCCESSFUL"
        if(item.issueWithAgreement){
          item.cssClasses = 'issueWithAgreement successful-simulation'
        }
        else if (item.isAutoApproved == 1) {
          item.cssClasses = 'autoApproved successful-simulation'
        } else if (item.isAutoApproved == 0) {
          item.cssClasses = 'notAutoApproved successful-simulation'
        } else {
          item.cssClasses = 'successful-simulation'
        }
      } else {
        item.simulationStatus = data.simulationErrorMessages[String(item.id)];
        expandGroupList.push(item.agreementKey);
        
        ++failedSimulantionCnt;

        if(item.issueWithAgreement){
          item.cssClasses = 'issueWithAgreement failed-simulation'
        }
        else if (item.isAutoApproved == 1) {
          item.cssClasses = 'autoApproved failed-simulation'
        } else if (item.isAutoApproved == 0) {
          item.cssClasses = 'notAutoApproved failed-simulation'
        } else {
          item.cssClasses = 'failed-simulation'
        }
      }
      summarySearchGrid.dataView.updateItem(item.id, item, false, 0);

    }

    if (jQuery.isEmptyObject(data.simulationErrorMessages) || failedSimulantionCnt == 0) {
        if (data.adjustmentResult.length != 0) {
          $("#saveStatus").addClass("hidden");
          var interestPowerAuth = treasury.financing.reconciliation.common.actions.isUserAuthorizedForInterestPowerEdit();
          var interestEditAuth = treasury.financing.reconciliation.common.actions.isUserAuthorizedForInterestEdit();
          $.when(interestPowerAuth, interestEditAuth).done(function(interestPowerAuthData, interestEditAuthData) {
            if (interestEditAuthData[0] || interestPowerAuthData[0]) {
              $("#saveAdjustmentsButton").removeClass("hidden");
              $("#cancelSimulationButton").removeClass("hidden");
            }
          });

          raiseSuccessToast("Adjustment Successfully Simulated.");
        }
    } else {
      $("#saveAdjustmentsButton").addClass("hidden");
      $("#cancelSimulationButton").addClass("hidden");
      document.getElementById("saveStatus").innerHTML = failedSimulantionCnt + "/" + selectedSummaryDataIndexes.length + " simulations failed, please proceed to Reconciliation Summary tab";
      raiseErrorToast("One or more adjustment simulation failed")
      $("#saveStatus").removeClass("hidden");
    }

    summarySearchGrid.setSelectedRowIds(newSelectedIndexes);
    
     summarySearchGrid.dataView.collapseAllGroups();

  }

  function _loadSummarySearchGrid(ajaxCall, gridId, columnsFunction,
    gridOptionFunction) {

    ajaxCall
      .done(function(data) {
          hideLoading("mainLoader");
        for (var i = 0; i < data.length; i++) {
          data[i] = treasury.financing.reconciliation.common.actions.flattenJsonResponse(data[i]);
        }
        if (summarySearchGrid != undefined)
          summarySearchGrid.dataView.setItems([]);
        if (data && !data.hasOwnProperty('errorMessage') && data.length != 0) {

          $("#legend").removeClass("hidden");
          if(!treasury.defaults.toastEnabled) {
            var interestPowerAuth = treasury.financing.reconciliation.common.actions.isUserAuthorizedForInterestPowerEdit();
            var interestEditAuth = treasury.financing.reconciliation.common.actions.isUserAuthorizedForInterestEdit();
            $.when(interestPowerAuth, interestEditAuth).done(function(interestPowerAuthData, interestEditAuthData) {
              if (interestEditAuthData[0] || interestPowerAuthData[0]) {
                document.getElementById("simulate").classList.remove("hidden");
              }
            });
          }
          

          summarySearchGrid = new dportal.grid.createGrid($("#" +
              gridId), data, columnsFunction(),
            gridOptionFunction());

          var rows = [];
          for (var i = 0; i < summarySearchGrid.data.length; i++) {
            summarySearchGrid.data[i] = treasury.financing.reconciliation.common.actions.flattenJsonResponse(data[i]);
            if(summarySearchGrid.data[i].issueWithAgreement == 1){
              summarySearchGrid.data[i].cssClasses = 'issueWithAgreement';
            }
            else if (summarySearchGrid.data[i].isAutoApproved == 1) {
            
              summarySearchGrid.data[i].cssClasses = 'autoApproved';
              summarySearchGrid.data[i].comments = "Auto Approved";
            } else if (summarySearchGrid.data[i].isAutoApproved == 0) {
              summarySearchGrid.data[i].cssClasses = 'notAutoApproved';
            } 

            if(summarySearchGrid.data[i].adjustmentMonthEndUSD != 0 && summarySearchGrid.data[i].isAutoApproved == 1 
              && !summarySearchGrid.data[i].issueWithAgreement){
              rows.push(data[i].id);
            }
          }

          summarySearchGrid.setSelectedRowIds(rows);
          summarySearchGrid.setDynamicGroupingColumns(['agreementKey']);
          summarySearchGrid.dataView.collapseAllGroups();
          treasury.financing.reconciliation.common.actions.resizeCanvasOnGridChange(
            summarySearchGrid,
            'summaryGrid');
          document.getElementById('loadingStateDialog').visible = false;
           if(data[0]["reportingFxRate"]) {
                treasury.financing.reconciliation.common.actions.showReportingCurrencyMessage(gridId, true, data[0]["reportingCurrency"]);
                  }
           else {
                treasury.financing.reconciliation.common.actions.showReportingCurrencyMessage(gridId, false);
                 }
        } else if (data && data.errorMessage) {
          document.getElementById('loadingStateDialog').visible = false;

          hideLoading("mainLoader");
          raiseErrorToast("Error occured while searching for results");
          treasury.financing.reconciliation.common.actions.hideReportingCurrencyMessage(gridId);

        } else if (data && data.length == 0) {
          document.getElementById('loadingStateDialog').visible = false;
          $("#legend").addClass("hidden");
          hideLoading("mainLoader");
          raiseInfoToastWithTimeOut("No data found");
          treasury.financing.reconciliation.common.actions.hideReportingCurrencyMessage(gridId);

        }
        if (document.getElementById('filterSidebar').state == 'expanded' && summarySearchGrid != undefined) {
          document.getElementById('filterSidebar').toggle();
          document.getElementById('filterSidebar').style.width = '20px';
        }
      });
    ajaxCall.fail(function(xhr, text, errorThrown) {

      hideLoading("mainLoader");
      raiseErrorToast("Error occured while searching for results");
      treasury.financing.reconciliation.common.actions.hideReportingCurrencyMessage(gridId);
    });
  }

  function _loadSimulatedAdjustmentDataGrid(ajaxCall, gridId,
    columnsFunction, gridOptionFunction) {
    showLoading("mainLoader");

    document.getElementById("saveStatus").innerHTML = "";
    document.getElementById('invalidAdjustmentDialog').visible = false;
    ajaxCall
      .done(function(data) {

        hideLoading("mainLoader");
        if (simulatedSummaryGrid != undefined)
          simulatedSummaryGrid.dataView.setItems([]);
        if (data && data.hasOwnProperty('errorMessage') && data.length != 0) {
          if(data.errorMessage.includes("Can not find fx rate")) {
            raiseErrorToast(data.errorMessage)
          } else {
          raiseErrorToast("Error occured while fetching Adjustment Records");
          }
          return;
        } else if(data && (data.hasOwnProperty('inactiveBookMessage') || data.hasOwnProperty('inactiveBundleMessage')) && data.length != 0) {
            raiseErrorToast("The adjustment data simulated have inactive books or bundles.");
            $("#saveAdjustmentsButton").addClass("hidden");
            $("#cancelSimulationButton").addClass("hidden")
            document.getElementById('invalidAdjustmentDialog').visible = true;
            if(data.hasOwnProperty('inactiveBookMessage')) {
                document.getElementById("inactiveBookMessage").innerHTML = "<h2>Inactive agreement - book mappings are:</h2> <br/>" + data.inactiveBookMessage;
            }
            if(data.hasOwnProperty('inactiveBundleMessage')) {
                document.getElementById("inactiveBundleMessage").innerHTML = "<h2>Inactive agreement - bundle mappings are:</h2> <br/>" + data.inactiveBundleMessage;
            }
            $("#saveStatus").removeClass("hidden");
            return;
        }

        if (data.adjustmentResult.length == 0) {
            $("#saveAdjustmentsButton").addClass("hidden");
            $("#cancelSimulationButton").addClass("hidden");
            document.getElementById("saveStatus").innerHTML = "No adjustment data to be shown";
            $("#saveStatus").removeClass("hidden");
        }
        else {
            for (var i = 0; i < data.adjustmentResult.length; i++) {
              data.adjustmentResult[i] = treasury.financing.reconciliation.common.actions.flattenJsonResponse(data.adjustmentResult[i]);
              data.adjustmentResult[i].agreementKey = data.adjustmentResult[i].agreementKey + " _ " + data.adjustmentResult[i].currency + " _ " + data.adjustmentResult[i].chargeType;
            }

            simulatedSummaryGrid = new dportal.grid.createGrid($("#" +
                gridId), data.adjustmentResult,
              columnsFunction(), gridOptionFunction());

            simulatedSummaryGrid.resizeCanvas(true);
        }
        _updateSummaryGrid(data);

      });

    ajaxCall
      .fail(function(xhr, text, errorThrown) {

        hideLoading("mainLoader");
        raiseErrorToast("Error occured while simulating Adjustment Records");
      });
  }

  function _allSync() {
    var dateString = document.getElementById("datePicker").value;
    var parameters = {
      "dateString": dateString,
    };
    var ajaxCall = $.ajax({
      url: 'treasury/financing/reconciliation/all-sync',
      data: parameters,
      type: "POST",
    });

    treasury.financing.reconciliation.summary.viewLatestSync();
  }

  function _brokerAdjustmentSync() {
    var dateString = document.getElementById("datePicker").value;
    var parameters = {
      "dateString": dateString,
    };

    var ajaxCall = $.ajax({
      url: 'treasury/financing/reconciliation/broker-adjustment-sync',
      data: parameters,
      type: "POST",
    });
    treasury.financing.reconciliation.summary.viewLatestSync();
  }

  function _viewLatestSync() {
    var dateString = document.getElementById("datePicker").value;
    var parameters = {
      "dateString": dateString,
    };

    var ajaxCall = $.ajax({
      url: 'treasury/financing/reconciliation/view-latest-sync',
      data: parameters,
      type: "POST",
    });
    ajaxCall.done(function(response) {
      document.getElementById('accrualMessage').innerHTML = "Accrual Sync<br />&emsp;&emsp;&emsp;&emsp;" + response.accrualMessage;
      document.getElementById('brokerMessage').innerHTML = "Broker&AdjustmentSync<br />&emsp;&emsp;&emsp;&emsp;" + response.brokerMessage;
      $('#latestSyncDiv').removeClass("hidden")
    })
  }


})();
