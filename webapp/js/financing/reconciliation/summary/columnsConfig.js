"use strict";

(function () {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.SummaryColumnsConfig = {
    getColumns: _getColumns,
    getAcuralAndAdjustmentPostingColumns: _getAcuralAndAdjustmentPostingColumns
  };

  function _getCommonColumns() {
    return [{
      id: "currency",
      type: "text",
      name: "Currency",
      field: "currency",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      width : 80
    }, {
      id: "chargeType",
      type: "text",
      name: "Charge Type",
      field: "chargeType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "accrual",
      type: "text",
      name: "Accrual (USD)",
      field: "accrualUSD",
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: _getGroupTotalsFormatter,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
          parseFloat(value)) + "</div>";
      },
      comparator: function (a, b) {
        return treasury.comparators.number(a.accrualUSD, b.accrualUSD);
      },
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      width: 140,
      excelFormatter: "#,##0"
    },{
        id: "accrualRC",
        type: "text",
        name: "Accrual (RC)",
        field: "accrualRC",
        sortable: true,
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: "Sum",
        groupTotalsFormatter: _getGroupTotalsFormatter,
        formatter: function (row, cell, value, columnDef, dataContext) {
          return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
            parseFloat(value)) + "</div>";
        },
        comparator: function (a, b) {
          return treasury.comparators.number(a.accrualRC, b.accrualRC);
        },
        headerCssClass: "aln-rt b",
        cssClass: "amount",
        width: 140,
        excelFormatter: "#,##0"
      },
    {
      id: "brokerPosting",
      type: "text",
      name: "BrokerPosting (USD)",
      field: "brokerPostingUSD",
      aggregator: dpGrid.Aggregators.sum,
      sortable: true,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(-
          1.0 * parseFloat(value)) + "</div>";
      },
      groupingAggregator: "Sum",
      groupTotalsFormatter: function (totals, columnDef, placeHolder, isExportToExcel) {
        if (totals && totals.sum) {
          return isExportToExcel ? treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(-1 *
            totals.sum[columnDef.field]) : "<div class='aln-rt b'>" + treasury.financing.reconciliation.commonColumnsConfig
              .currencyFormatter(-1 * totals.sum[columnDef.field]) + "</div>";
        } else {
          return "";
        }
      },
      comparator: function (a, b) {
        return  treasury.comparators.number( a.brokerPostingUSD,  b.brokerPostingUSD);
      },
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      width: 140,
      excelDataFormatter: function (value) {
        return -1 * value;
      }
    }, {
         id: "brokerPostingRC",
         type: "text",
         name: "BrokerPosting (RC)",
         field: "brokerPostingRC",
         aggregator: dpGrid.Aggregators.sum,
         sortable: true,
         formatter: function (row, cell, value, columnDef, dataContext) {
           return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(-
             1.0 * parseFloat(value)) + "</div>";
         },
         groupingAggregator: "Sum",
         groupTotalsFormatter: function (totals, columnDef, placeHolder, isExportToExcel) {
           if (totals && totals.sum) {
             return isExportToExcel ? treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(-1 *
               totals.sum[columnDef.field]) : "<div class='aln-rt b'>" + treasury.financing.reconciliation.commonColumnsConfig
                 .currencyFormatter(-1 * totals.sum[columnDef.field]) + "</div>";
           } else {
             return "";
           }
         },
         comparator: function (a, b) {
           return  treasury.comparators.number( a.brokerPostingRC,  b.brokerPostingRC);
         },
         headerCssClass: "aln-rt b",
         cssClass: "amount",
         width: 140,
         excelDataFormatter: function (value) {
           return -1 * value;
         }
       },
    {
      id: "differenceMonthEndUSD",
      type: "text",
      name: "Difference (USD)",
      field: "differenceMonthEndUSD",
      sortable: true,
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: _getGroupTotalsFormatter,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
          parseFloat(value)) + "</div>";
      },
      comparator: function (a, b) {
        return treasury.comparators.number(a.differenceMonthEndUSD, b.differenceMonthEndUSD);
      },
      width: 150,
      excelFormatter: "#,##0"
    },{
            id: "differenceMonthEndRC",
            type: "text",
            name: "Difference (RC)",
            field: "differenceMonthEndRC",
            sortable: true,
            headerCssClass: "aln-rt b",
            cssClass: "amount",
            aggregator: dpGrid.Aggregators.sum,
            groupingAggregator: "Sum",
            groupTotalsFormatter: _getGroupTotalsFormatter,
            formatter: function (row, cell, value, columnDef, dataContext) {
              return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
                parseFloat(value)) + "</div>";
            },
            comparator: function (a, b) {
              return treasury.comparators.number(a.differenceMonthEndRC, b.differenceMonthEndRC);
            },
            width: 150,
            excelFormatter: "#,##0"
          },
    {
      id: "difference",
      type: "text",
      name: "Difference (Local)",
      field: "difference",
      sortable: true,
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: _getGroupTotalsFormatter,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
          parseFloat(value)) + "</div>";
      },
      comparator: function (a, b) {
        return treasury.comparators.number(a.difference, b.difference);
      },
      width: 200,
      excelFormatter: "#,##0"
    }, {
      id: "approvedAdjustment",
      type: "text",
      name: "ApprovedAdjustment (USD)",
      field: "approvedAdjustmentUSD",
      sortable: true,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: _getGroupTotalsFormatter,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
          parseFloat(value)) + "</div>";
      },
      comparator: function (a, b) {
        return treasury.comparators.number(a.approvedAdjustmentUSD, b.approvedAdjustmentUSD);
      },
      width: 180,
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      excelFormatter: "#,##0"
    },{
            id: "approvedAdjustmentRC",
            type: "text",
            name: "ApprovedAdjustment (RC)",
            field: "approvedAdjustmentRC",
            sortable: true,
            aggregator: dpGrid.Aggregators.sum,
            groupingAggregator: "Sum",
            groupTotalsFormatter: _getGroupTotalsFormatter,
            formatter: function (row, cell, value, columnDef, dataContext) {
              return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
                parseFloat(value)) + "</div>";
            },
            comparator: function (a, b) {
              return treasury.comparators.number(a.approvedAdjustmentRC, b.approvedAdjustmentRC);
            },
            width: 180,
            headerCssClass: "aln-rt b",
            cssClass: "amount",
            excelFormatter: "#,##0"
          },
    {
      id: "adjustmentMonthEndUSD",
      type: "text",
      name: "Difference to approve (USD)",
      field: "adjustmentMonthEndUSD",
      sortable: true,
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: _getGroupTotalsFormatter,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
          parseFloat(value)) + "</div>";
      },
      comparator: function (a, b) {
        return treasury.comparators.number(a.adjustmentMonthEndUSD, b.adjustmentMonthEndUSD);
      },
      width: 180,
      excelFormatter: "#,##0"
    },
    {
          id: "adjustmentMonthEndRC",
          type: "text",
          name: "Difference to approve (RC)",
          field: "adjustmentMonthEndRC",
          sortable: true,
          headerCssClass: "aln-rt b",
          cssClass: "amount",
          aggregator: dpGrid.Aggregators.sum,
          groupingAggregator: "Sum",
          groupTotalsFormatter: _getGroupTotalsFormatter,
          formatter: function (row, cell, value, columnDef, dataContext) {
            return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
              parseFloat(value)) + "</div>";
          },
          comparator: function (a, b) {
            return treasury.comparators.number(a.adjustmentMonthEndRC, b.adjustmentMonthEndRC);
          },
          width: 180,
          excelFormatter: "#,##0"
        },
    {
      id: "adjustment",
      type: "text",
      name: "Difference to approve (Local)",
      field: "adjustment",
      sortable: true,
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      editor: dpGrid.Editors.Float,
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: _getGroupTotalsFormatter,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
          parseFloat(value)) + "</div>";
      },
      comparator: function (a, b) {
        return treasury.comparators.number(a.adjustment, b.adjustment);
      },
      width: 200,
      excelFormatter: "#,##0"
    }, {
      id: "comments",
      type: "text",
      name: "Comments",
      field: "comments",
      sortable: true,
      headerCssClass: "aln-rt b",
      groupingAggregator: customCommentsAgreegator,
      groupTotalsFormatter: function (totals, columnDef, placeHolder, isExportToExcel) {
        if (totals && totals.comment) {
          var rowIdList = [];
          rowIdList.push(totals.group.collapsed);
          for (var i = 0; i < totals.group.rows.length; ++i) {
            rowIdList.push(totals.group.rows[i].id);
          }
          if (totals.comment[columnDef.field]) {
            return isExportToExcel ? totals.comment[columnDef.field] : "<div class='aln-rt b'>" +
              '<a href="#" title="Edit Term"  onclick= event.stopPropagation();treasury.financing.reconciliation.Summary.addComments(' +
              '\'' +
              rowIdList +
              '\'' +
              '); ><i class="icon-edit--block button-edit"></i></a>' + totals.comment[
                columnDef.field]  +"</div>";
          }
          return isExportToExcel ? "" : '<a href="#" title="Edit Term"  onclick= event.stopPropagation();treasury.financing.reconciliation.Summary.addComments(' +
            '\'' +
            rowIdList +
            '\'' +
            '); ><i class="icon-edit--block button-edit"></i></a>'

        }
      },
      cssClass: "comments",
      editor: dpGrid.Editors.Text,
      excelFormatter: "#,##0"
    }];
  }

  function _getColumns() {
    var columns = [{
      id: "isAutoApproved",
      type: "text",
      name: "",
      field: "isAutoApproved",
      sortable: true,
      headerCssClass: "aln-rt b",
      cssClass: "approvalColumn",
      groupingAggregator: "Avg",
      groupTotalsFormatter: function (totals, columnDef, placeHolder, isExportToExcel) {
        var issueWithAgreement = false;
        for (var i = 0; i < totals.group.rows.length; i++) {
          issueWithAgreement = issueWithAgreement || totals.group.rows[i].issueWithAgreement
        }
        if (issueWithAgreement)
          return isExportToExcel ? "" : '<div  class="fullWidthHeight yellow7"></div>';
        else if (totals.avg[columnDef.field] == 1)
          return isExportToExcel ? "" : '<div  class="fullWidthHeight green6"></div>';
        else if (totals.avg[columnDef.field] == 0)
          return isExportToExcel ? "" : '<div  class="fullWidthHeight red7"></div>';
        return isExportToExcel ? "" : '<div class="fullWidthHeight"></div>';
      },
      maxWidth: 8,
      autoWidth: false
    },
    {
      id: "agreementKey",
      type: "text",
      name: "Agreement Name",
      field: "agreementKey",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      hideOnGrouping: true,
      width: 350,
      maxWidth: 800
    }, {
      id: "simulationStatus",
      type: "text",
      name: "Simulation Status",
      field: "simulationStatus",
      headerCssClass: "aln-rt b",
      sortable: true,
      cssClass: "status",
      groupingAggregator: customSimulationStatusAgreegator,
      groupTotalsFormatter: function (totals, columnDef, placeHolder, isExportToExcel) {
        if (totals && totals.simulationStatus) {
          return isExportToExcel ? totals.simulationStatus[columnDef.field] :
            "<div class='text-align--center green6'>" + totals.simulationStatus[
            columnDef.field] + "</div>"
        }
      },
      width: 150,
      excelFormatter: "#,##0"
    }, {
      id: "agreementType",
      type: "text",
      name: "Agreement Type",
      field: "agreementType",
      groupingAggregator: customAgreementTypeAgreegator,
      groupTotalsFormatter: function (totals, columnDef, placeHolder, isExportToExcel) {
        if (totals && totals.agreementType) {
          return isExportToExcel ? totals.agreementType[columnDef.field] : "<div class='aln-rt b'>" + totals.agreementType[
            columnDef.field] + "</div>"
        }
      },
      sortable: true,
      width: 100,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }];
    columns = columns.concat(_getCommonColumns());
    columns.push({
      id: "legalEntity",
      type: "text",
      name: "Legal Entity",
      field: "legalEntity",
      sortable: true,
      headerCssClass: "aln-rt b",
      groupingAggregator: customLegalEntityAgreegator,
      groupTotalsFormatter: function (totals, columnDef, placeHolder, isExportToExcel) {
        if (totals && totals.legalEntity) {
          return isExportToExcel ? totals.legalEntity[columnDef.field] : "<div class='aln-rt b'>" + totals.legalEntity[
            columnDef.field] + "</div>"
        }
      },
      excelFormatter: "#,##0"
    }, {
        id: "counterPartyEntity",
        type: "text",
        name: "Counterparty",
        field: "cpe",
        sortable: true,
        headerCssClass: "aln-rt b",
        groupingAggregator: customCpeAgreegator,
        groupTotalsFormatter: function (totals, columnDef, placeHolder, isExportToExcel) {
          if (totals && totals.cpe) {
            return isExportToExcel ? totals.cpe[columnDef.field] : "<div class='aln-rt b'>" + totals.cpe[
              columnDef.field] + "</div>"
          }
        },
        excelFormatter: "#,##0"
      }, {
        id: "adjustmentUSD",
        type: "text",
        name: "Difference(Daily USD Rates)",
        field: "adjustmentUSD",
        sortable: true,
        headerCssClass: "aln-rt b",
        cssClass: "amount",
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: "Sum",
        groupTotalsFormatter: _getGroupTotalsFormatter,
        formatter: function (row, cell, value, columnDef, dataContext) {
          return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
            parseFloat(value)) + "</div>";
        },
        width: 180,
        excelFormatter: "#,##0"
      },{
        id: "adjustmentRC",
        type: "text",
        name: "Difference(DailyRC Rates)",
        field: "adjustmentRC",
        sortable: true,
        headerCssClass: "aln-rt b",
        cssClass: "amount",
        aggregator: dpGrid.Aggregators.sum,
        groupingAggregator: "Sum",
        groupTotalsFormatter: _getGroupTotalsFormatter,
        formatter: function (row, cell, value, columnDef, dataContext) {
          return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
            parseFloat(value)) + "</div>";
        },
        width: 180,
        excelFormatter: "#,##0"
      },
      {
        id: "detailDataLink",
        type: "text",
        name: "Detail Data",
        field: "key",
        sortable: true,
        formatter: _viewDetailDataLink,
        headerCssClass: "aln-rt b",
        cssClass: "detailDataLink",
        exportToExcel: false
      }

    );
    return columns;
  }

  function _getAcuralAndAdjustmentPostingColumns() {
    var columns = [{
      id: "agreementKey",
      type: "text",
      name: "Agreement Name",
      field: "agreementKey",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0",
      hideOnGrouping: true,
      width: 350,
      maxWidth: 800
    }];
    columns = columns.concat(_getCommonColumns());
    columns.push({
      id: "adjustmentUSD",
      type: "text",
      name: "Difference(Daily USD Rates)",
      field: "adjustmentUSD",
      sortable: true,
      headerCssClass: "aln-rt b",
      cssClass: "amount",
      aggregator: dpGrid.Aggregators.sum,
      groupingAggregator: "Sum",
      groupTotalsFormatter: _getGroupTotalsFormatter,
      formatter: function (row, cell, value, columnDef, dataContext) {
        return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
          parseFloat(value)) + "</div>";
      },
      comparator: function (a, b) {
        return treasury.comparators.number(a.adjustmentUSD, b.adjustmentUSD);
      },
      width: 180,
      excelFormatter: "#,##0"
    }, {
         id: "adjustmentRC",
         type: "text",
         name: "Difference(Daily RC Rates)",
         field: "adjustmentRC",
         sortable: true,
         headerCssClass: "aln-rt b",
         cssClass: "amount",
         aggregator: dpGrid.Aggregators.sum,
         groupingAggregator: "Sum",
         groupTotalsFormatter: _getGroupTotalsFormatter,
         formatter: function (row, cell, value, columnDef, dataContext) {
           return "<div class='text-align--right'>" + treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
             parseFloat(value)) + "</div>";
         },
         comparator: function (a, b) {
           return treasury.comparators.number(a.adjustmentRC, b.adjustmentRC);
         },
         width: 180,
         excelFormatter: "#,##0"
       },
    {
        id: "detailDataLink",
        type: "text",
        name: "Detail Data",
        field: "key",
        sortable: true,
        formatter: _viewDetailDataLink,
        headerCssClass: "aln-rt b",
        cssClass: "detailDataLink",
        exportToExcel: false
      });
    return columns;
  }

  function _getGroupTotalsFormatter(totals, columnDef, placeHolder, isExportToExcel) {
     if (totals && totals.sum) {
       return isExportToExcel ? treasury.financing.reconciliation.commonColumnsConfig.currencyFormatter(
       totals.sum[columnDef.field]) : "<div class='aln-rt b'>" + treasury.financing.reconciliation.commonColumnsConfig
         .currencyFormatter(totals.sum[columnDef.field]) + "</div>";
     } else {
        return "";
     }
 }

  function _viewDetailDataLink(row, cell, value, columnDef, dataContext) {
    return "<a class=\"detailDataLink\" onclick='window.treasury.financing.reconciliation.detailData.loadDetailDataScreen(" +
      value.legalEntityId + "," + value.cpeId + "," + value.agreementTypeId + "," + value.ccySpn + ",\"" + value.chargeTypeId +  "," + value.reportingCurrencyFilterId +
      "\")'>View Detail Data</a>";

  }

})();
