"use strict";
var simulationFilterGroup;
var commentRows = [];
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.Summary = {
    loadSearchResults: _loadSearchResults,
    pageDestructor: _pageDestructor,
    registerHandlers: _registerHandlers,
    simulateSelectedRecords: _simulateSelectedRecords,
    saveAdjustmentRecords: _saveAdjustmentRecords,
    loadSimulatedAdjustmentsPage: _loadSimulatedAdjustmentsPage,
    simulatedAdjustmentsPageDestructor: _simulatedAdjustmentsPageDestructor,
    loadPage: _loadPage,
    addComments: _addComments,
    copySearchURL: _copySearchURL
  };

  function _pageDestructor() {
    $("#summarySearch").addClass("hidden");
    $("#showZeroDifferenceDiv").addClass("hidden");
    $("#legend").addClass("hidden");
    $("#settleDateParentDiv").addClass("hidden");
     $("#reportingCurrencyTypeDiv").addClass("hidden");

  }



  function _loadPage() {
    document.getElementById("filterSidebar").setAttribute('size', '300px');
    window.treasury.financing.reconciliation.common.actions.resizeAllCanvas();
    if(summarySearchGrid!=undefined){
      $("#legend").removeClass("hidden");
    }
    $("#reportingCurrencyTypeDiv").removeClass("hidden");
    setTimeout(function() {
      $("#settleDateParentDiv").removeClass("hidden");
      $("#summarySearch").removeClass("hidden");

      $("#showZeroDifferenceDiv").removeClass("hidden");
      if (!treasury.defaults.toastEnabled) {
        var interestPowerAuth = treasury.financing.reconciliation.common.actions.isUserAuthorizedForInterestPowerEdit();
        var interestEditAuth = treasury.financing.reconciliation.common.actions.isUserAuthorizedForInterestEdit();
        var interestSyncAuth = treasury.financing.reconciliation.common.actions.isUserAuthorizedForInterestSync();
        $.when(interestPowerAuth, interestEditAuth, interestSyncAuth).done(function(interestPowerAuthData, interestEditAuthData, interestSyncAuthData) {
          if (interestEditAuthData[0] || interestPowerAuthData[0] || interestSyncAuthData[0]) {
            $("#syncButtons").removeClass("hidden");
          }
        });
      }
      
      $("#showZeroDifferenceDiv").removeClass("hidden");

      if(document.getElementById('filterSidebar').state == "expanded"){
        if(summarySearchGrid !=undefined){
          document.getElementById('filterSidebar').toggle();
          summarySearchGrid.resizeCanvas(true);
          document.getElementById("summaryGrid").setAttribute("width", $("summarySearch").width());
        }
      }else{
        if(summarySearchGrid == undefined){
          document.getElementById('filterSidebar').toggle();
        }else{  
          document.getElementById("summaryGrid").setAttribute("width", $("summarySearch").width());
        summarySearchGrid.resizeCanvas(true);
        }
      }

 

      if(summarySearchGrid != undefined){
        var expandList = [];
        var summaryDataItems = summarySearchGrid.dataView.getItems();       
        for (var i = 0; i < summaryDataItems.length; i++) {
          var item =summaryDataItems[i];
          if(item.cssClasses.indexOf("failed-simulation") >= 0){
            expandList.push(item.agreementKey);
          }
            
        }
        for (var i = 0; i < expandList.length; i++) {
          summarySearchGrid.dataView.expandGroup(expandList[i])
       
        }
       
    }

    }, 6);

  }

  function _simulatedAdjustmentsPageDestructor() {
    $("#filterSidebar").removeClass("hidden");
    $("#saveAdjustmentsButton").addClass("hidden");
    $("#cancelSimulationButton").addClass("hidden");
    $("#saveStatus").addClass("hidden");
    $("#simulatedAdjustmentRecords").addClass("hidden");
    treasury.financing.reconciliation.common.actions.clearGrid("simulatedSummaryGrid");
    $("#simulatedAdjustmentMessage").removeClass("hidden");


  }

  function _loadSimulatedAdjustmentsPage() {
    document.getElementById("filterSidebar").setAttribute('size', '0');
    document.getElementById("filterSidebar").classList += " hidden";
    $("#simulatedAdjustmentRecords").removeClass("hidden")
  }

  function _loadConfirmationDialogForId(id) {
    return function() {
      document.getElementById(id).visible = true
    }
  }

  function _removeConfirmationDialogForId(id) {
    return function() {
      document.getElementById(id).visible = false
    }
  }

  function _registerHandlers() {

    document.getElementById("simulate").onclick = treasury.financing.reconciliation.Summary.simulateSelectedRecords;
    document.getElementById("addComments").onclick = function() {
      saveComments();
      _removeConfirmationDialogForId('commentPopup')();
    }
    // All Sync Handlers
    document.getElementById("allSyncButton").onclick = function() {
      document.getElementById("allSyncMessage").innerHTML = "Are you sure you want to perform all sync for " +
        $("#datePicker").val() + "? ";
      _loadConfirmationDialogForId('allSyncConfirmationDialog')();
    }
    document.getElementById("performAllSync").onclick = function() {
      _allSync();
      _removeConfirmationDialogForId('allSyncConfirmationDialog')();
    }
    document.getElementById("notPerformAllSync").onclick = _removeConfirmationDialogForId('allSyncConfirmationDialog');

    // Broker And Adjustment Sync Handlers
    document.getElementById("brokerAdjustmentButton").onclick = function() {
      document.getElementById("brokerAdjustmentSyncMessage").innerHTML =
        "Are you sure you want to perform broker and adjustment sync for: " +
        $("#datePicker").val() + "? ";
      _loadConfirmationDialogForId(
        'brokerAdjustmentSyncConfirmationDialog')();
    }
    document.getElementById("performBrokerAndAdjustmentSync").onclick = function() {
      _brokerAdjustmentSync();
      _removeConfirmationDialogForId(
        'brokerAdjustmentSyncConfirmationDialog')();
    }
    document.getElementById("notPerformBrokerAndAdjustmentSync").onclick = _removeConfirmationDialogForId(
      'brokerAdjustmentSyncConfirmationDialog');

    // Save Handlers
    document.getElementById('saveAdjustmentsButton').onclick = _loadConfirmationDialogForId(
      'saveAdjustmentsDialogBox');

      document.getElementById('cancelSimulationButton').onclick = _loadConfirmationDialogForId(
        'cancelSimulationDialogBox');
      

    document.getElementById('saveSimulatedAdjustments').onclick = function() {
      treasury.financing.reconciliation.Summary.saveAdjustmentRecords();
      _removeConfirmationDialogForId('saveAdjustmentsDialogBox')();
    }

    document.getElementById('notSaveSimulatedAdjustments').onclick = _removeConfirmationDialogForId(
      'saveAdjustmentsDialogBox');

    document.getElementById('continueSimulation').onclick = _removeConfirmationDialogForId(
      'cancelSimulationDialogBox');

    document.getElementById('cancelSimulation').onclick = function() {
      document.getElementById('reconciliationSummary').click();
      _removeConfirmationDialogForId('cancelSimulationDialogBox')();
    }

    setInterval(_viewLatestSync, 100000);

    document.getElementById('filterSidebar').addEventListener("stateChange", treasury.financing.reconciliation.common
      .actions.resizeAllCanvas);
  }



  function _addComments(rows) {
    var config = {
      'title': 'Add comment',
                  'dismissible': true,
                  'width': '300px',
                  'height': '160px',
                  'moveable': true,
                  }

    document.querySelector('#commentPopup').reveal(config);
    commentRows = JSON.parse("[" + rows + "]");
    if(  summarySearchGrid.dataView.getItemById(commentRows[1]).comments == null){
      document.getElementById("comment").value = "";
    }else{
      document.getElementById("comment").value = summarySearchGrid.dataView.getItemById(commentRows[1]).comments;
    }
  }

  function saveComments() {
    var comment = document.getElementById("comment").value;
    var collapsed = commentRows[0];
    for (var i = 1; i < commentRows.length; ++i) {
      summarySearchGrid.dataView.getItemById(commentRows[i]).comments = comment;
    }
    _toggleExpandCollapseGroup(collapsed,summarySearchGrid.dataView.getItemById(commentRows[1]).agreementKey);
    _toggleExpandCollapseGroup(!collapsed,summarySearchGrid.dataView.getItemById(commentRows[1]).agreementKey);


  }

  function _toggleExpandCollapseGroup(collapse,agreementKey){
    if(collapse){
        summarySearchGrid.dataView.expandGroup(agreementKey);
    }else {
        summarySearchGrid.dataView.collapseGroup(agreementKey);
    }
  }

  function _copySearchURL() {
    var url = "/treasury/financing/reconciliation/summary-results";
    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    var showZeroDifference = document.getElementById("showZeroDifference").checked;
    parameters.dateString = formattedDate;
    parameters.settleDate = getFormattedDate('datePickerSettleDate');
    parameters.showZeroDifference = showZeroDifference;

    generateReportURL(url, parameters, "resultList");

  }

  function _loadSearchResults() {
    if (document.getElementById("datePickerError").innerHTML != "") {
      raiseErrorToast("Please select a valid Date");
      return;
    }
    
    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    var showZeroDifference = document.getElementById("showZeroDifference").checked;
    parameters.dateString = formattedDate;
    parameters.settleDate = getFormattedDate('datePickerSettleDate');
    parameters.showZeroDifference = showZeroDifference;
    
    treasury.financing.reconciliation.summary.loadResults(parameters);
  }

  function _simulateSelectedRecords() {
    $("#saveAdjustmentsButton").addClass("hidden");
    $("#cancelSimulationButton").addClass("hidden");
    treasury.financing.reconciliation.summary.simulateSelectedRecords();
  }

  function _saveAdjustmentRecords() {

    treasury.financing.reconciliation.summary.saveAdjustmentRecords();
  }

  function _allSync() {
    treasury.financing.reconciliation.summary.allSync();
    treasury.financing.reconciliation.summary.viewLatestSync();
  }

  function _brokerAdjustmentSync() {
    treasury.financing.reconciliation.summary.brokerAdjustmentSync();
    treasury.financing.reconciliation.summary.viewLatestSync();
  }

  function _viewLatestSync() {
    treasury.financing.reconciliation.summary.viewLatestSync();
  }
})()
