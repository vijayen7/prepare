"use strict";
var simulationFilterGroup;
var searchFilterGroupDetailData;
(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.syncStatus = {
    loadSearchResults: _loadSearchResults,
    registerHandlers: _registerHandlers,
    pageDestructor: _pageDestructor,
    loadLandingViewFilter: _loadLandingViewFilter,
    loadPage: _loadPage,
  };

  function _registerHandlers() {
  }

  function _loadPage() {

      document.getElementById("filterSidebar").setAttribute('size', '300px');
      window.treasury.financing.reconciliation.common.actions.resizeAllCanvas();
      _loadLandingViewFilter();
      $("#syncStatusSearch").removeClass("hidden");

      if(document.getElementById('filterSidebar').state == "expanded"){
        if(syncStatusGrid !=undefined){
          document.getElementById('filterSidebar').toggle();
          syncStatusGrid.resizeCanvas(true);
        }
      }else{
        if(syncStatusGrid == undefined){
          document.getElementById('filterSidebar').toggle();
        }else{ syncStatusGrid.resizeCanvas(true);}
      }
 }

  function _pageDestructor() {
    $("#copySearchURL").removeClass("hidden");
    $("#saveSettingsParentDiv").removeClass("hidden");
    $("#legalEntityParentDiv").removeClass("hidden");
    $("#cpeParentDiv").removeClass("hidden");
    $("#agreementParentDiv").removeClass("hidden");
    $("#reportingCurrencyTypeDiv").removeClass("hidden");

    $("#syncStatusSearch").addClass("hidden");
    $("#syncTypeParentDiv").addClass("hidden");
  }

  function _loadLandingViewFilter() {
    $("#syncTypeParentDiv").removeClass("hidden");
    $("#syncStatusSearch").removeClass("hidden");

    $("#copySearchURL").addClass("hidden");
    $("#saveSettingsParentDiv").addClass("hidden");
    $("#legalEntityParentDiv").addClass("hidden");
    $("#cpeParentDiv").addClass("hidden");
    $("#agreementParentDiv").addClass("hidden");
    $("#reportingCurrencyTypeDiv").addClass("hidden");
  }

  function _loadSearchResults() {
    if (document.getElementById("datePickerError").innerHTML != "") {
      raiseErrorToast("Please select a valid Date");
      return;
    }

    var parameters = searchFilterGroup.getSerializedParameterMap();
    var formattedDate = getFormattedDate('datePicker');
    parameters.dateString = formattedDate;

    treasury.financing.reconciliation.syncStatusAction.loadResults(parameters);
  }
})();
