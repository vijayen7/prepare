"use strict";
var syncStatusGrid;

(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.syncStatusAction = {
    loadResults: _loadResults,
  };

  function _loadResults(parameters) {
    var serviceCall = $.ajax({
      url: "/treasury/financing/reconciliation/syncStatus-results",
      data: parameters,
      type: "POST",
      dataType: "json"
    });
      treasury.financing.reconciliation.common.actions.clearGrid("syncStatusGrid");
      treasury.financing.reconciliation.common.actions.loadSearchGrid(serviceCall, "syncStatusGrid",
      treasury.financing.reconciliation.syncStatusColumnsConfig.getColumns,
      treasury.financing.reconciliation.common.grid.getSyncStatusGridOption,
      function(gridVar) {
        syncStatusGrid = gridVar;
      })
  }

})();
