"use strict";

(function() {

  window.treasury = window.treasury || {};
  window.treasury.financing = window.treasury.financing || {};
  window.treasury.financing.reconciliation = window.treasury.financing.reconciliation || {};
  window.treasury.financing.reconciliation.syncStatusColumnsConfig = {
    getColumns: _getColumns
  };

  function _getColumns() {
    return [
    {
      id: "status",
      type: "text",
      name: "Status",
      field: "status",
      sortable: true,
      ignoreInColumnSelection : false,
      formatter : _applicableColumnIdentifier,
      headerCssClass: "aln-rt b",
      excelDataFormatter : _linkExcelFormatter,
    },{
      id: "syncType",
      type: "text",
      name: "Sync Type",
      field: "syncType",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "userName",
      type: "text",
      name: "User Login",
      field: "userName",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "startDate",
      type: "text",
      name: "Start Date (ET)",
      field: "startDate",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "endDate",
      type: "text",
      name: "End Date (ET)",
      field: "endDate",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }, {
      id: "month",
      type: "text",
      name: "Month",
      field: "month",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"

    }, {
      id: "year",
      type: "text",
      name: "Year",
      field: "year",
      sortable: true,
      headerCssClass: "aln-rt b",
      excelFormatter: "#,##0"
    }];
  }

  function _applicableColumnIdentifier(row, cell, value, columnnDef, dataContext) {
      if (dataContext.status == 'FAILED') {
        return _get_div_element(value, 'critical');
      } else if (dataContext.status == 'ACTIVE') {
        return _get_div_element(value, 'primary');
      } else if (dataContext.status == 'SUCCESS') {
        return _get_div_element(value, 'success');
      }
  }

  function _get_div_element(value, element_class) {
      return '<div class="' + element_class + '" style="height:100%; text-align:center;">'
              + value + '</div>';
  }

  function _linkExcelFormatter(row, cell, value, columnnDef, dataContext) {
      cell.actions = "";
      return row;
  }
})();
