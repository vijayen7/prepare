<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">

  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Arcesium::Ops Dashboard</title>
    <jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
    <link rel="stylesheet" href="/treasury/css/interest/onboarding.css"/>
    <link rel="stylesheet" type="text/css" href="/treasury/css/interest/toastr.css"/>
    <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css"/>
      <script>
        function switchJqueryCSS() {
          var href = document.querySelector('link#jquery-ui-css-arcux').href
          if (ArcThemeHandler.getCurrentTheme() == 'dark') {
            if (href.indexOf('light') > -1) {
              document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
            }
          } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
            if (href.indexOf('light') === -1) {
              document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
            }
          }
        }
        switchJqueryCSS();
        ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
      </script>
    </head>

    <body>
      <arc-layout type="row">
        <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
          <div slot="application-menu" class="application-menu-toggle-view" id="opsMenu">
            <a href="#" id="interestOps">Interest</a>
            <a href="#" id="seclendOps">Seclend</a>
          </div>
        </arc-header>
        <arc-layout type="row">
          <div id="mainArea"></div>
        </arc-layout>
      </arc-layout>
      <script type="text/javascript" src="/treasury/js/filter/multiselect.js"></script>
      <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
      <script type="text/javascript" src="/treasury/js/util-min.js"></script>
      <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
      <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
      <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
      <script type="text/javascript" src="/treasury/js/treasury_common.js"></script>

      <script type="text/javascript" src="/treasury/js/seclend/ops/landing.js"></script>
      <script type="text/javascript" src="/treasury/js/seclend/ops/action.js"></script>
      <script type="text/javascript" src="/treasury/js/seclend/ops/gridConfig.js"></script>
      <script type="text/javascript" src="/treasury/js/seclend/ops/gridOptions.js"></script>

      <script type="text/javascript" src="/treasury/js/interest/ops/gridOptions.js"></script>
      <script type="text/javascript" src="/treasury/js/interest/ops/columnConfig.js"></script>
      <script type="text/javascript" src="/treasury/js/interest/ops/action.js"></script>
      <script type="text/javascript" src="/treasury/js/interest/ops/landing.js"></script>

      <script type="text/javascript" src="/treasury/js/financing/ops/landing.js"></script>
      <script type="text/javascript" src="/treasury/js/financing/ops/util.js"></script>

      <script>
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
        window.treasury.defaults = window.treasury.defaults || {};
        if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
          treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
        }
      </script>
    </body>
  </html>
