<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">

  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Arcesium::Interest Reconciliation</title>
    <jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>

    <link rel="stylesheet" href="/treasury/css/interest/onboarding.css"/>
    <link rel="stylesheet" type="text/css" href="/treasury/css/financing/reconciliation/interest-reconciliation.css"/>
    <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css"/>
    <link rel="stylesheet" type="text/css" href="/treasury/css/financing/reconciliation/toastr.css"/>

    <script>
      function switchJqueryCSS() {
        var href = document.querySelector('link#jquery-ui-css-arcux').href
        if (ArcThemeHandler.getCurrentTheme() == 'dark') {
          if (href.indexOf('light') > -1) {
            document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
          }
        } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
          if (href.indexOf('light') === -1) {
            document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
          }
        }
      }
      switchJqueryCSS();
      ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
    </script>
  </head>
  <body>

    <arc-dialog title="Message" id="allSyncConfirmationDialog" sticky="true" dismissible="false" >
      <div class="confirmationMessage" id="allSyncMessage"></div>
      <div class="confirmationDialogBox">
        <div class="dialogButtons">
          <button id="performAllSync" class="confirmtionButtons">Yes</button>
          <button id="notPerformAllSync" class="confirmationButtons">No</button>
        </div>
      </div>
    </arc-dialog>


    <arc-dialog title="Message" id="brokerAdjustmentSyncConfirmationDialog" sticky="true" dismissible="false">
      <div class="confirmationMessage" id="brokerAdjustmentSyncMessage">Are you sure you want to perform broker and adjustment sync?</div>
      <div class="confirmationDialogBox">
        <div class="dialogButtons">
          <button id="performBrokerAndAdjustmentSync" class="confirmtionButtons">Yes</button>
          <button id="notPerformBrokerAndAdjustmentSync" class="confirmationButtons">No</button>
        </div>
      </div>
    </arc-dialog>

    <input type="hidden" id="stabilityLevel" name="stabilityLevel" value="${stability_level}"/>
    <input type="hidden" id="clientName" name="clientName" value="${client_name}"/>

    <div class="layout--flex--row">
    <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
        <div slot="application-menu" class="application-menu-toggle-view"id="reconciliationMenu">
          <a href="#" id="reconciliationSummary">Reconciliation Summary</a>
          <a href="#" id="simulatedAdjustmentData" class="hidden">Simulated Adjustments</a>
          <a href="#" id="adjustmentSummary" class="hidden">Adjustment Summary </a>
          <a href="#" id="reconciliationDetailData">Reconciliation Detail</a>
          <a href="#" id="gearAdjustmentData" class="hidden">Gear Adjustment Data</a>
          <a href="#" id="syncStatus" class="hidden">Sync Status</a>

        </div>
      </arc-header>

      <div class="layout--flex gutter" id="parentArcLayout">
        <%@ include file="/jsp/financing/reconciliation/landing-filter.jspf"%>
        <div class="layout--flex--row" style="width: 100%; padding: 5px 0px;">
          <div>
            <div id="mainArea">
              <div class="overlay" id="mainLoaderOverlay">
                <div class="loader loader--block form-field--center" id="mainLoader" style="width:100px">Loading</div>
              </div>
              <%@ include file="/jsp/financing/reconciliation/reconciliation-summary.jspf"%>
              <%@ include file="/jsp/financing/reconciliation/reconciliation-adjustment.jspf"%>
              <%@ include file="/jsp/financing/reconciliation/reconciliation-detail.jspf"%>
              <%@ include file="/jsp/financing/reconciliation/reconciliation-simulated-adjust.jspf"%>
              <%@ include file="/jsp/financing/reconciliation/reconciliation-invalid-simulated-adjustment.jspf"%>
              <%@ include file="/jsp/financing/reconciliation/reconciliation-loading.jspf"%>
              <%@ include file="/jsp/financing/reconciliation/reconciliation-gear-adjustment.jspf"%>
              <%@ include file="/jsp/financing/reconciliation/reconciliation-sync-status.jspf"%>
            </div>
          </div>
        </div>
      </div>

    </div>

    <!-- Common Scripts -->

    <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/multiselect-new.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/singleselect.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
    <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
    <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
    <script type="text/javascript" src="/treasury/js/util-min.js"></script>
    <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>
    <script type="text/javascript" src="/treasury/js/ops/monthpicker.js"></script>

    <!--  Default Landing -->
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/landing.js"></script>

    <!--Common Scripts from reconciliation application-->
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/common/reconciliationUtil.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/common/ActionHelper.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/common/gridOptions.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/common/toastr.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/common/columnsConfig.js"></script>

    <!-- Summary Data Scripts -->
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/summary/landing.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/summary/action.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/summary/columnsConfig.js"></script>

    <!-- Detail Data Scripts -->
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/detail/detailDataLanding.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/detail/detailDataAction.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/detail/columnsConfig.js"></script>

    <!-- Gear Adjustment Data Scripts -->
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/gearAdjustment/gearAdjustmentDataLanding.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/gearAdjustment/gearAdjustmentDataAction.js"></script>

    <!-- Adjustment Summary Data Scripts -->
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/adjustment/landing.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/adjustment/action.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/adjustment/columnsConfig.js"></script>

    <!-- Sync Status Scripts -->
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/syncStatus/syncStatusLanding.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/syncStatus/syncStatusAction.js"></script>
    <script type="text/javascript" src="/treasury/js/financing/reconciliation/syncStatus/columnsConfig.js"></script>

    <script>
      var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
      window.treasury.defaults = window.treasury.defaults || {};
      if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
        treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
      }
      console.log("before");
       if (CODEX_PROPERTIES['com.arcesium.treasury.toastEnabled'] != undefined) {
        treasury.defaults.toastEnabled = CODEX_PROPERTIES['com.arcesium.treasury.toastEnabled'].myValue;
        console.log("after");
      }
    </script>
  </body>
</html>
