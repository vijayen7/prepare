<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
    <title>Interest vs Seclend Data</title>
    <jsp:include page="../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css"/>
        <script>
          function switchJqueryCSS() {
            var href = document.querySelector('link#jquery-ui-css-arcux').href
            if (ArcThemeHandler.getCurrentTheme() == 'dark') {
              if (href.indexOf('light') > -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
              }
            } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
              if (href.indexOf('light') === -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
              }
            }
          }
          switchJqueryCSS();
          ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
        </script>
        <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css" />
    </head>
    <body>
      <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>
      <div class="layout--flex--row">
        <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
        </arc-header>
        <div class="layout--flex gutter">
          <%@ include file="/jsp/financing/financing-filters.jspf" %>
           <div class="layout--flex--row">
              <div class="message size--content" id="searchMessage">Perform search to load results</div>
              <div class="overlay">
                  <div class="loader loader--block form-field--center" style="width:100px">Loading</div>
              </div>
              <div>
                <div class="arc-grid__table" id="comparisonGrid" style="width: 100%;" hidden>
                </div>
            </div>
           </div>
           <arc-sidebar id="comparisonDetailSidebar" header="Interest vs Seclend Data Details" collapsible="true" position="right" class="size--content" hidden>
             <table class="table" id="comparisonDetails">
             </table>
           </arc-sidebar>
        </div>
      </div>

      <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
      <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
      <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
      <script type="text/javascript" src="/treasury/js/util-min.js"></script>
      <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>
      <script type="text/javascript" src="/treasury/js/filter/multiselect-min.js"></script>
      <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>

      <!-- Financing -->
      <script type="text/javascript" src="/treasury/js/financing/comparisonLanding.js"></script>
      <script type="text/javascript" src="/treasury/js/financing/comparisonActions.js"></script>
      <script type="text/javascript" src="/treasury/js/financing/comparisonColumnsConfig.js"></script>
      <script type="text/javascript" src="/treasury/js/financing/comparisonGridOptions.js"></script>
      <script type="text/javascript" src="/treasury/js/financing/financingUtil.js"></script>

      <script>
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
        window.treasury.defaults = window.treasury.defaults || {};
        // copy system property of default 'date' to treasury.defaults package
        if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
          treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
        }
      </script>
    </body>
  </html>
