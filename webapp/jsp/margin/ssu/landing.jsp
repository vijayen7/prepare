<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Calculator Configurations</title>
    <jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
    <link rel="import" href="/treasury/components/loading/loading.html">
      <link rel="stylesheet" type="text/css" href="/treasury/css/margin/toastr.css"/>
      <link rel="stylesheet" href="/treasury/css/treasury_common.css">
      <link rel="stylesheet" href="/treasury/css/margin/ssu/ssu.css">

            <!-- Function to change JQuery and CSS applied based on theme -->
            <script>
              function switchJqueryCSS() {
                var href = document.querySelector('link#jquery-ui-css-arcux').href
                if (ArcThemeHandler.getCurrentTheme() == 'dark') {
                  if (href.indexOf('light') > -1) {
                    document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
                  }
                } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
                  if (href.indexOf('light') === -1) {
                    document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
                  }
                }
              }
              switchJqueryCSS();
              ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
            </script>
          </head>
          <body>
            <input type="hidden" id="stabilityLevel" name="stabilityLevel" value="${stability_level}"/>
            <input type="hidden" id="clientName" name="clientName" value="${client_name}"/>
            <%@ include file="/jsp/margin/ssu/save-config-popup.jspf" %>
            <%@ include file="/jsp/margin/ssu/clone-config-popup.jspf" %>
            <div class="layout--flex--row">
              <!-- Header -->
              <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
                <div slot="application-menu" class="application-menu-toggle-view" id="margin-menu">
                  <a href="simulation" id="marginSimulation">Margin Simulation</a>
                  <a href="intermediate-data" id="intermediateMargin">
                    Intermediate Margin</a>
                  <a href="calculator-configurations" id="ssu">Calculator Configurations</a>
                </div>
              </arc-header>
              <!--Main Area-->
              <div class="layout--flex">
                <%@ include file="/jsp/margin/ssu/search-filters-sidebar.jspf" %>
                <arc-resizer></arc-resizer>
                <div class="layout--flex size--5">
                  <div id="calcNav">
                    <%@ include file="/jsp/margin/ssu/calculator-navigation-menu.jspf" %>
                  </div>
                  <arc-resizer></arc-resizer>
                  <div id="configEditor" class="size--3 layout--flex">
                    <%@ include file="/jsp/margin/ssu/config-editor.jspf" %>
                  </div>
                </div>
              </div>
            </div>
            <div class="overlay" id="mainLoaderOverlay">
              <div class="loader loader--block form-field--center" id="mainLoader">Loading</div>
            </div>

            <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
            <script type="text/javascript" src="/treasury/js/filter/multiselect-new-min.js"></script>
            <script type="text/javascript" src="/treasury/js/filter/singleselect-min.js"></script>
            <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
            <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
            <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
            <script type="text/javascript" src="/treasury/js/util.js"></script>
            <script type="text/javascript" src="/treasury/js/treasury_common.js"></script>

            <script type="text/javascript" src="/treasury/js/margin/ssu/landing.js"></script>
            <script type="text/javascript" src="/treasury/js/margin/ssu/util.js"></script>
            <script type="text/javascript" src="/treasury/js/margin/ssu/configEditor.js"></script>
            <script type="text/javascript" src="/treasury/js/margin/ssu/configXMLHandler.js"></script>
            <script type="text/javascript" src="/treasury/js/margin/ssu/calculatorNavigationMenu.js"></script>

            <script type="text/javascript" src="/treasury/js/margin/simulation/util.js"></script>

            <script type="text/javascript" src="/treasury/js/margin/toastr.js"></script>
            <script type="text/javascript" src="/treasury/js/margin/treasuryMarginCommon.js"></script>

            <script>
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
        window.treasury.defaults = window.treasury.defaults || {};
        // copy system property of default 'date' to treasury.defaults package
        if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
          treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
        }
        if (CODEX_PROPERTIES["treasury.portal.defaults.ssu.counterPartyEntities"] != undefined) {
          treasury.defaults.counterPartyEntities = CODEX_PROPERTIES["treasury.portal.defaults.ssu.counterPartyEntities"].myValue;
        }
        if (CODEX_PROPERTIES["treasury.portal.defaults.ssu.agreementTypes"] != undefined) {
          treasury.defaults.agreementTypes = CODEX_PROPERTIES["treasury.portal.defaults.ssu.agreementTypes"].myValue;
        }
        if (CODEX_PROPERTIES["treasury.portal.defaults.ssu.legalEntities"] != undefined) {
          treasury.defaults.legalEntities = CODEX_PROPERTIES["treasury.portal.defaults.ssu.legalEntities"].myValue;
        }
     </script>
          </body>
        </html>