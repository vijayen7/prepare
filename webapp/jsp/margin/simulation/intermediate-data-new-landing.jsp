<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Margin Intermediate Data</title>
	<jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
  <script type="text/javascript">
  	ArcThemeHandler.enableThemeSelection();
  </script>

  <!-- link rel="import" href="/treasury/components/loading/loading.html"-->
  <link rel="stylesheet" href="/treasury/css/treasury_common.css">

  <!-- Function to change JQuery and CSS applied based on theme -->
  <script>
  	function switchJqueryCSS() {
  		var href = document.querySelector('link#jquery-ui-css-arcux').href
  		if (ArcThemeHandler.getCurrentTheme() == 'dark') {
  			if (href.indexOf('light') > -1) {
  				document.querySelector('link#jquery-ui-css-arcux').href = href
  						.replace('light/jquery-ui.css', 'jquery-ui.css');
  			}
  		} else if (ArcThemeHandler.getCurrentTheme() == 'light') {
  			if (href.indexOf('light') === -1) {
  				document.querySelector('link#jquery-ui-css-arcux').href = href
  						.replace('jquery-ui.css', 'light/jquery-ui.css');
  			}
  		}
  	}
  	switchJqueryCSS();
  	ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
  </script>
</head>

<body>
    <input type="hidden" id="stabilityLevel" name="stabilityLevel" value="${stability_level}" />
    <input type="hidden" id="clientName" name="clientName" value="${client_name}" />
	<div class="layout--flex--row">
		<!-- Header -->
		<arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
			<div slot="application-menu" class="application-menu-toggle-view"id="margin-menu">
			<a href="simulation" id="marginSimulation">Margin Simulation</a>
            <a href="intermediate-data" id="intermediateMargin"> Intermediate Margin</a>
            <a href="calculator-configurations" id="ssu">Calculator Configurations</a>
		</div>
		</arc-header>
		<!-- Main Area -->
		<div class="layout--flex container border">
			<%@ include file="/jsp/margin/simulation/search-portfolio.jspf"%>

			<!-- Grid Area -->
			<div class="layout--flex--row">
					<div class="arc-message size--content margin--top--double" style="height:50px"
						id="searchMessage" hidden>Perform search to load results</div>
					<!--div class="arc-resizer"-->
						<div class="layout--flex gutter size--1">
               <arc-panel-element id="methodologySummaryArea" label="Methodology Margin Summary"
                        class="size--3" hidden>
                        <div id="methodologySummaryGrid"></div>
                </arc-panel-element>
								<arc-panel-element id="methodologySummaryErrorArea"
									class="margin--top-double arc-message" style="height:50px" hidden>
                         <div> No data present for the search criteria</div>
                 </arc-panel-element>
								 <div id="methodologySummaryServerErrorArea"
									 class="margin--top-double" hidden> </div>
                <!-- Right panel for Intermediate data  -->
                <%@ include	file="/jsp/margin/simulation/methodology-intermediate-details.jspf"%>
            </div>
				    <%@ include	file="/jsp/margin/simulation/methodology-margin-details.jspf"%>
				<!--/div-->
				<div class="overlay">
						<div class="loader loader--block form-field--center"
									style="width: 100px">Loading</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="/treasury/js/filter/loadfilters.js"></script>
	<script type="text/javascript"
		src="/treasury/js/filter/multiselect-new.js"></script>
	<script type="text/javascript"
		src="/treasury/js/filter/singleselect.js"></script>
	<script type="text/javascript" src="/treasury/js/filter/filtergroup.js"></script>
	<script type="text/javascript"
		src="/treasury/js/filter/securitysearch.js"></script>
	<script type="text/javascript" src="/treasury/js/treasury.js"></script>
	<script type="text/javascript" src="/treasury/js/util.js"></script>
	<script type="text/javascript"
		src="/treasury/js/arc-grid/arc-grid.min.js"></script>
	<script type="text/javascript" src="/treasury/js/treasury_common.js"></script>
	<script type="text/javascript" src="/treasury/js/margin/util.js"></script>
	<script type="text/javascript"
		src="/treasury/js/margin/intermediateDataLanding.js"></script>
	<script type="text/javascript"
		src="/treasury/js/margin/intermediateDataActions.js"></script>
	<script type="text/javascript"
		src="/treasury/js/margin/columnConfigIntermediateData.js"></script>
	<script type="text/javascript"
		src="/treasury/js/margin/gridOptionsIntermediateData.js"></script>
	<script type="text/javascript" src="/treasury/js/margin/treasuryMarginCommon.js"></script>


	<script>
		var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
		window.treasury.defaults = window.treasury.defaults || {};
		// copy system property of default 'date' to treasury.defaults package
		if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
			treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
		}
		if(CODEX_PROPERTIES["treasury.portal.defaults.intermediate.legalEntities"] != undefined){
      treasury.defaults.legalEntities = CODEX_PROPERTIES["treasury.portal.defaults.intermediate.legalEntities"].myValue;
		}
    if( CODEX_PROPERTIES["treasury.portal.defaults.intermediate.counterPartyEntities"] != undefined){
      treasury.defaults.counterPartyEntities =
						CODEX_PROPERTIES["treasury.portal.defaults.intermediate.counterPartyEntities"].myValue;
		}
    if(CODEX_PROPERTIES["treasury.portal.defaults.intermediate.agreementTypes"] != undefined){
      treasury.defaults.agreementTypes = CODEX_PROPERTIES["treasury.portal.defaults.intermediate.agreementTypes"].myValue;
		}
		if(CODEX_PROPERTIES["treasury.portal.defaults.intermediate.reportingCurrencies"] != undefined){
            treasury.defaults.reportingCurrencies = CODEX_PROPERTIES["treasury.portal.defaults.intermediate.reportingCurrencies"].myValue;
		}
		if(CODEX_PROPERTIES["treasury.portal.defaults.intermediate.enableReportingCurrencyFilter"] != undefined) {
			treasury.defaults.enableReportingCurrencyFilter = 
			CODEX_PROPERTIES["treasury.portal.defaults.intermediate.enableReportingCurrencyFilter"].myValue;
		}
	</script>
</body>
</html>
