<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">

  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Margin Simulation</title>
    <jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>

        <link rel="stylesheet" href="/treasury/css/margin/simulation/landing.css">
          <link rel="stylesheet" href="/treasury/css/margin/simulation/margin_widget.css"/>
          <link rel="stylesheet" href="/treasury/css/treasury_common.css">
            <link rel="stylesheet" href="/treasury/css/filter/security_search.css">
            <link rel="stylesheet" type="text/css" href="/treasury/css/margin/toastr.css"/>

              <!-- Function to change JQuery and CSS applied based on theme -->
              <script>
                function switchJqueryCSS() {
                  var href = document.querySelector('link#jquery-ui-css-arcux').href
                  if (ArcThemeHandler.getCurrentTheme() == 'dark') {
                    if (href.indexOf('light') > -1) {
                      document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
                    }
                  } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
                    if (href.indexOf('light') === -1) {
                      document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
                    }
                  }
                }
                switchJqueryCSS();
                ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
              </script>
            </head>

            <body style="overflow:hidden">
              <input type="hidden" id="stabilityLevel" name="stabilityLevel" value="${stability_level}"/>
              <input type="hidden" id="clientName" name="clientName" value="${client_name}"/>
              <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>

              <div class="layout--flex--row">
                <!-- Header -->
                <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
                  <div slot="application-menu" class="application-menu-toggle-view" id="margin-menu">
                    <a href="simulation" id="marginSimulation">Margin Simulation</a>
                    <a href="intermediate-data" id="intermediateMargin">
                      Intermediate Margin</a>
                    <a href="calculator-configurations" id="ssu">Calculator Configurations</a>
                  </div>
                </arc-header>
                <!-- Widget -->
                <div class="size--content widget--holder">
                  <div class="layout--flex rule-wizard" id="widgetContainer">
                    <!-- Step 1 -->
                    <div class="rule-wizard__step" id="wizard_step1" onclick="treasury.margin.widget.goToStep(1)">
                      <div class="rule-wizard__step__number">1</div>
                      <strong class="rule-wizard__step__label">Select and Add Positions</strong>
                      <div class="rule-wizard__step__connector"></div>
                    </div>
                    <!-- Step 2 -->
                    <div class="rule-wizard__step" id="wizard_step2" onclick="treasury.margin.widget.goToStep(2)">
                      <div class="rule-wizard__step__number">2</div>
                      <strong class="rule-wizard__step__label">Add Margin Terms</strong>
                      <div class="rule-wizard__step__connector"></div>
                    </div>
                    <!-- Step 3 -->
                    <div class="rule-wizard__step" id="wizard_step3" onclick="treasury.margin.widget.goToStep(3)">
                      <div class="rule-wizard__step__number">3</div>
                      <strong class="rule-wizard__step__label">View Result</strong>
                    </div>
                  </div>
                </div>

                <!--Action Message-->
                <div class="message size--content margin--bottom" id="actionMessage" hidden></div>

                <!--Main Area-->
                <div id="step1">
                  <%@ include file="/jsp/margin/simulation/add-positions.jspf" %>
                </div>
                <div id="step2" class="hidden">
                  <%@ include file="/jsp/margin/simulation/add-margin-terms.jspf" %>
                </div>
                <div id="step3" class="hidden">
                  <%@ include file="/jsp/margin/simulation/run-scenario.jspf" %>
                </div>
                <div id="noMarginTerms" class="hidden">
                  <%@ include file="/jsp/margin/simulation/no-margin-terms.jspf" %>
                </div>
                <%@ include file="/jsp/margin/simulation/clone-config-dialog-box.jspf" %>
                <%@ include file="/jsp/margin/simulation/view-edit-config-dialog-box.jspf" %>
                <%@ include file="/jsp/margin/simulation/view-edit-span-state-dialog-box.jspf" %>

                <!-- Action Buttons -->
                <div class="container--action form size--content margin-top">
                  <div class="row">
                    <button class="button--medium button--tertiary size--content" id="backId" disabled>
                      <i class="icon-go--back margin--right padding--left--double"></i>Go Back
                    </button>

                    <div class="blank"></div>
                    <button class="button--medium button--tertiary size--content" id="nextId">
                      Next
                      <i class="icon-go--forward margin--left padding--right--double"></i>
                    </button>
                  </div>
                </div>
              </div>
              <div class="overlay" id="mainLoaderOverlay">
                <div class="loader loader--block form-field--center" id="mainLoader" style="width: 100px">Loading</div>
              </div>
              <div class="overlay" id="spanOverlay" style="display: none;">
                <div class="loader loader--block form-field--center" id="spanLoader" style="width: fit-content;display:none">Loading</div>
              </div>
              
              <script type="text/javascript" src="/treasury/js/filter/loadfilters.js"></script>
              <script type="text/javascript" src="/treasury/js/filter/multiselect-new.js"></script>
              <script type="text/javascript" src="/treasury/js/filter/singleselect.js"></script>
              <script type="text/javascript" src="/treasury/js/filter/filtergroup.js"></script>
              <script type="text/javascript" src="/treasury/js/filter/securitysearch.js"></script>
              <script type="text/javascript" src="/treasury/js/treasury.js"></script>
              <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
              <script type="text/javascript" src="/treasury/js/util.js"></script>
              <script type="text/javascript" src="/treasury/js/treasury_common.js"></script>

              <script type="text/javascript" src="/treasury/js/margin/simulation/widget.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/simulation/simulationLanding.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/simulation/util.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/simulation/simulationActions.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/simulation/simulationColumnConfig.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/simulation/simulationGridOptions.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/simulation/marginTerms.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/simulation/runSimulation.js"></script>

              <script type="text/javascript" src="/treasury/js/margin/intermediateDataActions.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/gridOptionsIntermediateData.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/columnConfigIntermediateData.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/treasuryMarginCommon.js"></script>
              
              <script type="text/javascript" src="/treasury/js/margin/ssu/configEditor.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/ssu/configXMLHandler.js"></script>
              <script type="text/javascript" src="/treasury/js/margin/ssu/util.js"></script>

              <script type="text/javascript" src="/treasury/js/margin/toastr.js"></script>

              <script>
                var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
                window.treasury.defaults = window.treasury.defaults || {};
                // copy system property of default 'date' to treasury.defaults package
                if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
                  treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
                }
                if (CODEX_PROPERTIES["treasury.portal.defaults.simulation.counterPartyEntities"] != undefined) {
                  treasury.defaults.counterPartyEntities = CODEX_PROPERTIES["treasury.portal.defaults.simulation.counterPartyEntities"].myValue;
                }
                if (CODEX_PROPERTIES["treasury.portal.defaults.simulation.agreementTypes"] != undefined) {
                  treasury.defaults.agreementTypes = CODEX_PROPERTIES["treasury.portal.defaults.simulation.agreementTypes"].myValue;
                }
                if (CODEX_PROPERTIES["treasury.portal.defaults.simulation.legalEntities"] != undefined) {
                  treasury.defaults.legalEntities = CODEX_PROPERTIES["treasury.portal.defaults.simulation.legalEntities"].myValue;
                }
                if (CODEX_PROPERTIES["treasury.portal.defaults.simulation.books"] != undefined) {
                    treasury.defaults.books = CODEX_PROPERTIES["treasury.portal.defaults.simulation.books"].myValue;
                  }
                if(CODEX_PROPERTIES["treasury.portal.defaults.simulation.reportingCurrencies"] != undefined){
                    treasury.defaults.reportingCurrencies = CODEX_PROPERTIES["treasury.portal.defaults.simulation.reportingCurrencies"].myValue;
                }
                if(CODEX_PROPERTIES["treasury.portal.defaults.simulation.enableReportingCurrencyFilter"] != undefined) {
                    treasury.defaults.enableReportingCurrencyFilter =
                 			CODEX_PROPERTIES["treasury.portal.defaults.simulation.enableReportingCurrencyFilter"].myValue;
                }
              </script>
            </body>
          </html>
