<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="feedback-form">
<table class="" cellpadding="3" cellspacing="0" border="0" style="table-layout:fixed;width:100%;">
    <colgroup>
        <col width="25%" />
        <col width="75%" />
    </colgroup>
    <tr id="mailValidation" style="display:none;">
        <td></td>
        <td>
            <div class="validations">Please specify "To" list.</div>
        </td>
    </tr>
    <tr>
        <td class="aln-rt dblue help-only" title="Specify combination of mailing lists and/or users.">
        To:
        </td>
        <td>
            <input size="50" name="mailToAddress" id="mailToAddress" value="treasury-dev-ops@arcesium.com," />
        </td>
    </tr>
    <tr>
        <td class="aln-rt dblue">
        Comment:
        </td>
        <td>
            <textarea rows="3" cols="50" name="mailComment" id="mailComment"></textarea>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><input type="checkbox" name="sendCopy" checked="checked" />&nbsp;&nbsp;Send me a copy</td>
    </tr>
    <tr>
        <td class="aln-rt dblue" >
        Subject:
        </td>
        <td>
            <input size="50" name="mailSubject" id="mailSubject" value="<s:property value="#parameters['subject'][0]" />" />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <input type="submit" value="Send" class="send-btn cmd-btn" style="width:60px" /> &nbsp;
            <input type="button" class="cancel-btn btn" value="Cancel" style="width:60px" />
            &nbsp;<span style="display:none" id="mailSending"><img src="/static/images/indicator.gif" /> Sending...</span>
        </td>
    </tr>
</table>
</div>
<script>
$(function () {
    // attach cancel on click event
    $(".cancel-btn", $("#feedback-form")).click(function() {$("#mail-dialog").dialog("close");})

    var mailTargetElement$ = $(".gad-cont", $("#m-<s:if test="max">max-</s:if><s:property value="#parameters['moduleId'][0]" />"));

    // attach send-email on click event
    $(".send-btn", $("#feedback-form")).click(function() {
        var toAddress = $.trim($("#mailToAddress").val());
        var sendCopy = $("input[name=sendCopy]").is(":checked");
        var subject = $.trim($("#mailSubject").val());
        if (toAddress === "") {
            $("#mailValidation").show();
            return;
        }
        $(".cmd-btn", $("#feedback-form")).disabled(true);
        $("#mailSending").show();

        $("body").append("<div id='tempHtml2Canvas' style='position:absolute;top:"+ (mailTargetElement$.position().top + 30)
                + "px;left:" + ($("body").width() + 30)
                + "px;width:" + mailTargetElement$.width() +"px;'></div>")
        mailTargetElement$.clone().appendTo("#tempHtml2Canvas");
        mailTargetElement$ = $("#tempHtml2Canvas");

        convertFormpanelsToImages(mailTargetElement$);
        convertChartsToImages(mailTargetElement$);

        html2canvas(mailTargetElement$,
            {
                proxy: "/common/proxy",
                useCORS: true,
                onrendered: function(canvas) {
                    $.post(
                    "/common/send-mail", 
                    {    sendMail: true,
                         toAddress: toAddress,
                         sendCopy: sendCopy,
                         header : $("#mailComment").val(),
                         subject: subject,
                         imageData : canvas.toDataURL()
                     },
                     function() {
                         $("#tempHtml2Canvas").remove();
                         $("#mail-dialog").dialog("close");
                     });
                }
            });
    });

    // attach autocomplete for the acl input
    $("#mailToAddress").autocomplete("/common/user-ac", {
        width : "20%",
        multiple : true,
        multipleSeparator : ",",
        cacheLength : 0,
        matchContains : true,
        formatItem : function(row, i, max) {
            return "<table class='tbl-ac' border='0' cellpadding='0' cellspacing='0'><tr><td>"+ row[1] + " &lt;" + row[0] + "&gt;</td></tr></table>";
        },
        formatResult : function (row) {
            return row[0];
        }
    }).focus();

    $("#mailToAddress").focusEnd();
});
</script>
