<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
<title>Deal Report</title>
<jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
<script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
<link rel="stylesheet" type="text/css"
    href="//cloud.typography.com/6708954/800486/css/fonts.css" />
<script>
          function switchJqueryCSS() {
            var href = document.querySelector('link#jquery-ui-css-arcux').href
            if (ArcThemeHandler.getCurrentTheme() == 'dark') {
              if (href.indexOf('light') > -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
              }
            } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
              if (href.indexOf('light') === -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
              }
            }
          }

          switchJqueryCSS();
          ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
</script>
<link rel="stylesheet" type="text/css"
    href="/treasury/css/treasury_common.css" />
</head>
<body>
    <input type="hidden" id="stabilityLevel" name="stabilityLevel"
        value="${stability_level}" />
    <input type="hidden" id="clientName" name="clientName"
        value="${client_name}" />
    <arc-dialog title="Error" id="errorMessage" modal="true"
        style="z-index:9998"></arc-dialog>
    <div class="layout--flex--row">
        <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>"> </arc-header>
    <div class="layout--flex">


            <%@ include file="/jsp/lcm/dealReport/deal_report_filters.jspf"%>



            <div class="layout--flex--row gutter padding--horizontal">
                <div class="message size--content" id="searchMessage" hidden>Perform
                    search to load results</div>
                    <div class="size--content form" id="quickLinks" hidden>
                    <div class="row">
                    <div></div>
                    <span id="info" class="size--content" >Found <strong id="noOfRecords"></strong> records for <strong id="searchDate"></strong></span>
                    <vr class="size--content"></vr>
                    <button id="refreshId"  class="button--text size--content"><i class="icon-refresh"></i> Refresh Data</button>
                    </div>
                </div>
                <div class="overlay">
                    <div class="loader loader--block form-field--center"
                        style="width: 100px">Loading</div>
                </div>

                <div id = "dealReportGridDiv">
                    <arc-panel-element id = "dealReportGridPanel" collapsible="false" showheader="false" dismissible="false" label="Deal Level Data" hidden >
                    <div class="arc-grid__table " id="dealReportGrid"
                        style="width: 100%;height:100%;overflow:hidden;" hidden></div></arc-panel-element>
                        <div class="message size--content" id="childSearchMessage" hidden>No Underyling Data</div>

                  <arc-panel-element id = "dealReportChildGridPanel" collapsible="false" showheader="false" dismissible="false" hidden >
                    <div class="arc-grid__table " id="dealReportChildGrid"
                        style="width: 100%;overflow:hidden;" hidden></div></arc-panel-element>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript"
        src="/treasury/js/filter/loadfilters-min.js"></script>
    <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
    <script type="text/javascript"
        src="/treasury/js/arc-grid/arc-grid.min.js"></script>
    <script type="text/javascript" src="/treasury/js/util.js"></script>
    <script type="text/javascript"
        src="/treasury/js/treasury_common-min.js"></script>
    <script type="text/javascript"
        src="/treasury/js/filter/multiselect-new.js"></script>
    <script type="text/javascript"
        src="/treasury/js/filter/filtergroup-min.js"></script>

    <script type="text/javascript"
        src="/treasury/js/lcm/dealReport/dealReportLanding.js"></script>
    <script type="text/javascript"
        src="/treasury/js/lcm/dealReport/dealReportUtil.js"></script>

    <script type="text/javascript"
        src="/treasury/js/lcm/dealReport/dealReportActions.js"></script>

    <script type="text/javascript"
        src="/treasury/js/lcm/dealReport/dealReportColumnConfig.js"></script>

    <script type="text/javascript"
        src="/treasury/js/lcm/dealReport/dealReportGridOptions.js"></script>

    <script>
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
        window.treasury.defaults = window.treasury.defaults || {};
        // copy system property of default 'date' to treasury.defaults package
        if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
          treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
        }
      </script>
</body>
</html>
