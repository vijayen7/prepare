<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "workflowVersions" : [
<s:if test="workflowList != null && workflowList.size > 0">
  <s:iterator value="workflowList" status="state">
    [<s:property value="workflowInstanceId"/>, "<s:property value="description"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
