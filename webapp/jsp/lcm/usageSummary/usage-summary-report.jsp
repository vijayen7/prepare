<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
    <title>Usage Summary Report</title>
    <jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css"/>
        <script>
          function switchJqueryCSS() {
            var href = document.querySelector('link#jquery-ui-css-arcux').href
            if (ArcThemeHandler.getCurrentTheme() == 'dark') {
              if (href.indexOf('light') > -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
              }
            } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
              if (href.indexOf('light') === -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
              }
            }
          }
          switchJqueryCSS();
          ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
        </script>
        <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css" />
    </head>
    <body>
      <arc-loading id="lureLoading" style="display:none">
        <div id="loading-indicator">
          <i id="loading-indicator__icon" class="fa fa-spinner fa-spin"></i>
          <br />
          <label class="loading-indicator__comment">Loading</label>
      </div>
      </arc-loading>
      <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>
      <arc-layout type="row">
        <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>"> </arc-header>
        <arc-layout type="column" gutter="20px" id="parentArcLayout" size="content" style="overflow:hidden;">
          <arc-sidebar id="usageSummaryFilterSidebar" header="Usage Summary Report Filter" size="300px" collapsible="true">
            <%@ include file="/jsp/lcm/usageSummary/usage-summary-filters.jspf" %>
            <arc-panel-element showheader="false">
              <div style="text-align: center">
                <button class="button--primary" id="usageSummarySearch">Search</button>
                <button class="button--text" id="resetSearch">Reset</button><br>
                <button class="button--text" id="copySearchURL">Copy Search URL</button>
              </div>
            </arc-panel-element>
          </arc-sidebar>
          <%-- <arc-layout type="row" gutter="10px" size="1" style="overflow:hidden;"> --%>
            <div id="usageSummaryContent">
              <div style="height:30px" class="form-field--center" id="lureMessage">Perform search to load results</div>
              <div class="arc-grid__table" id="usageSummaryGrid" style="width: 100%;" hidden>
              </div>
              <div style="height:22px" hidden ><p>Note: All figures in millions of USD unless otherwise indicated.</p></div>
            </div>
          <%-- </arc-layout> --%>
        </arc-layout>
      </arc-layout>

      <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
      <script type="text/javascript" src="/treasury/js/filter/multiselect-min.js"></script>
      <script type="text/javascript" src="/treasury/js/filter/singleselect-min.js"></script>
      <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
      <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
      <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
      <script type="text/javascript" src="/treasury/js/util-min.js"></script>
      <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>
      <!-- LURE -->
      <script type="text/javascript" src="/treasury/js/lcm/usageSummary/usageSummaryLanding.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/usageSummary/usageSummaryActions.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/usageSummary/usageSummaryColumnsConfig.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/usageSummary/usageSummaryGridOptions.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/lureUtil.js"></script>

      <script>
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
        window.treasury.defaults = window.treasury.defaults || {};
        // copy system property of default 'date' to treasury.defaults package
        if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
          treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
        }
      </script>
    </body>
  </html>
