<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
    <title>Liquidity Report</title>
    <jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css"/>
        <script>
          function switchJqueryCSS() {
            var href = document.querySelector('link#jquery-ui-css-arcux').href
            if (ArcThemeHandler.getCurrentTheme() == 'dark') {
              if (href.indexOf('light') > -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
              }
            } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
              if (href.indexOf('light') === -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
              }
            }
          }
          switchJqueryCSS();
          ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
        </script>
        <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css" />
    </head>
    <body>
      <arc-loading id="lureLoading" style="display:none">
        <div id="loading-indicator">
          <i id="loading-indicator__icon" class="fa fa-spinner fa-spin"></i>
          <br />
          <label class="loading-indicator__comment">Loading</label>
      </div>
    </arc-loading>
      <%@ include file="/jsp/common/workflow/failed-workflow-template.jspf" %>
      <%@ include file="/jsp/common/workflow/locked-workflow-template.jspf" %>
      <%@ include file="/jsp/common/workflow/resume-workflow-template.jspf" %>
      <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>
      <arc-layout type="row">
        <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
          <div slot="application-menu" class="application-menu-toggle-view" id="start-workflow">
            <a href="javascript:void(0)" id="startWorkflow" targt="_blank">Start Workflow</a>
            <a href="/treasury/lcm/workflowStatus.html" id="workflowStatus" target="_blank">Workflow Status</a>
          </div>
        </arc-header>
        <arc-layout type="column" gutter="20px" id="parentArcLayout" size="content" style="overflow:hidden;">
          <arc-sidebar id="liquidityReportFilterSidebar" header="Liquidity Report Filter" size="300px" collapsible="true">
            <arc-panel-element showheader="false">
              <div class="form-field" id="liquidityReportOptionsDiv">
                <input type="radio" id="teLiquidity" name="liquidityViewOptions" checked>TE - Liquidity
                <input type="radio" id="fundLiquidity" name="liquidityViewOptions">Fund - Liquidity
              </div>
              <div class="form-field">
                <label>As of</label>
                <div class="form-field__input">
                  <input type="text" id="datePicker" style="display: inline;"/>
                  <div id="datePickerError" class="validation-error"></div>
                </div>
              </div>
              <div class="form-field" style="overflow: visible" id="workflowVersionsDiv">
                <label for="">Version</label>
                <arc-combobox id="workflowVersionsFilter"></arc-combobox>
              </div>
            </arc-panel-element>
            <arc-panel-element showheader="false" type="">
              <div style="text-align: center">
                <button class="button--primary" id="liquidityReportSearch">Search</button>
                <button class="button--text" id="copySearchURL">Copy Search URL</button>
              </div>
            </arc-panel-element>
          </arc-sidebar>
           <arc-layout type="row" gutter="10px">
            <arc-panel-element id="liquidityReportContent" showheader="false" size="content">
              <div style="height:30px" class="form-field--center" id="lureMessage">Perform search to load results</div>
              <div class="arc-grid__table" id="liquidityReportGrid"
                  style="width: 100%;" hidden>
              </div>
              <div class="arc-grid__table" id="liquiditySummaryGrid"
                  style="width: 100%;" hidden>
              </div>
            </arc-panel-element>
            <%@ include file="/jsp/lcm/common/adjustment-details.jspf" %>
           </arc-layout>
        </arc-layout>
      </arc-layout>

      <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
      <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
      <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
      <script type="text/javascript" src="/treasury/js/util-min.js"></script>
      <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>
      <script type="text/javascript" src="/treasury/js/filter/singleselect-min.js"></script>
      <!-- LURE -->
      <script type="text/javascript" src="/treasury/js/lcm/liquidity/liquidityReportLanding.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/liquidity/liquidityReportActions.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/liquidity/liquidityReportColumnsConfig.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/liquidity/liquidityReportGridOptions.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/liquidity/adjustmentsGridConfig.js"></script>
      <script type="text/javascript" src="/treasury/js/lcm/lureUtil.js"></script>

      <script type="text/javascript" src="/treasury/js/liquidity/workflowUtils.js"></script>

      <script>
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
        window.treasury.defaults = window.treasury.defaults || {};
        // copy system property of default 'date' to treasury.defaults package
        if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
          treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
        }
      </script>
    </body>
  </html>
