<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
    <title>Liquidity Workflow</title>
    <jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css"/>
      
        <script>
          function switchJqueryCSS() {
            var href = document.querySelector('link#jquery-ui-css-arcux').href
            if (ArcThemeHandler.getCurrentTheme() == 'dark') {
              if (href.indexOf('light') > -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
              }
            } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
              if (href.indexOf('light') === -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
              }
            }
          }
          switchJqueryCSS();
          ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
        </script>
        <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css"/>
      </head>
      <body>
        <!-- Hidden fields [Skipped records]-->
        <input type="hidden" name="configName" id="configName" value="yakLiquidityDetailsWorkflowConfig"/>
        <input type="hidden" id="prePublishMethod" name="prePublishMethod" value="treasury.workflow.landing.emailLiquidityReport"/>

        <arc-loading id="lureLoading" style="display:none">
          <div id="loading-indicator">
            <i id="loading-indicator__icon" class="fa fa-spinner fa-spin"></i>
            <br />
            <label class="loading-indicator__comment">Loading</label>
        </div>
        </arc-loading>
        <%@ include file="/jsp/comet/display_adjustment_popup.jsp" %>
        <arc-dialog id="adjustmentPopup" width="100" height="100"></arc-dialog>
        <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>
        <arc-layout type="row" gutter="20px" id="parentArcLayout" size="1" style="overflow:hidden;">
          <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>"> </arc-header>
          <div style="height:30px" class="form-field--center" id="lureMessage" hidden></div>
          <arc-panel-element id="workflowOperations" showheader="false" size="content">
            <button class="button--primary" id="saveExitWorkflow">Save &amp; Exit</button>
            <button class="button--primary" id="publishWorkflow" disabled="true">Publish</button>
            <button class="button--primary" id="cancelWorkflow">Cancel</button>
          </arc-panel-element>
          <arc-panel-element id="workflowPanel" showheader="true" label="Liquidity Details Workflow" size="5" style="overflow:visible">
            <arc-tab-panel id="workflowDataTabs" size="4">
              <arc-tab header="Trading Entity - Liquidity">
                <div class="arc-grid__table" id="liquidityReportGrid" style="width: 100%;" hidden></div>
              </arc-tab>
              <arc-tab header="Fund - Liquidity">
                <div class="arc-grid__table" id="liquiditySummaryGrid" style="width: 100%;" hidden></div>
              </arc-tab>
              <arc-tab header="Usage Summary - Trading Entity By BU">
                <div class="arc-grid__table" id="usageSummaryGrid" style="width: 100%;" hidden></div>
              </arc-tab>
              <arc-tab header="Publish">
                <div class="arc-grid__table" id="workflowChangesGrid" style="width: 20%" hidden></div>
              </arc-tab>
            </arc-tab-panel>
          </arc-panel-element>
          <%@ include file="/jsp/lcm/common/adjustment-details.jspf" %>
        </arc-layout>
        <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
        <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
        <script type="text/javascript" src="/treasury/js/util-min.js"></script>
        <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>

        <script type="text/javascript" src="/treasury/js/filter/multiselect-min.js"></script>
        <script type="text/javascript" src="/treasury/js/filter/singleselect-min.js"></script>

        <script type="text/javascript" src="/treasury/js/lcm/liquidity/liquidityReportActions.js"></script>
        <script type="text/javascript" src="/treasury/js/lcm/liquidity/liquidityReportColumnsConfig.js"></script>
        <script type="text/javascript" src="/treasury/js/lcm/liquidity/liquidityReportGridOptions.js"></script>
        <script type="text/javascript" src="/treasury/js/lcm/liquidity/adjustmentsGridConfig.js"></script>

        <script type="text/javascript" src="/treasury/js/lcm/usageSummary/usageSummaryActions.js"></script>
        <script type="text/javascript" src="/treasury/js/lcm/usageSummary/usageSummaryColumnsConfig.js"></script>
        <script type="text/javascript" src="/treasury/js/lcm/usageSummary/usageSummaryGridOptions.js"></script>
        <script type="text/javascript" src="/treasury/js/lcm/lureUtil.js"></script>

        <script type="text/javascript" src="/treasury/js/lcm/workflow/workflowLanding.js"></script>
        <script type="text/javascript" src="/treasury/js/lcm/workflow/workflowActions.js"></script>
        <script type="text/javascript" src="/treasury/js/liquidity/workflowUtils.js"></script>
        <script type="text/javascript" src="/treasury/js/comet/agreementAdjustment/agreementAdjustmentAction.js"></script>
        <script type="text/javascript" src="/treasury/js/comet/agreementWorkflow/agreementWorkflowAction.js"></script>
        <script>
          var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
          window.treasury.defaults = window.treasury.defaults || {};
          // copy system property of default 'date' to treasury.defaults package
          if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
            treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
          }
        </script>
      </body>
    </html>
