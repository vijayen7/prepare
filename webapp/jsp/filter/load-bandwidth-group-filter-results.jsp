<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "bandwidthGroup" : [
<s:if test="bandwidthGroupList != null && bandwidthGroupList.size > 0">
  <s:iterator value="bandwidthGroupList" status="state">
    [<s:property value="bandwidthGroupId"/>, "<s:property escapeHtml="false" value="abbrev"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
