<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
 "chargeTypes" :
    [

            <s:iterator value="@com.arcesium.treasury.model.financing.ChargeType@values()" status="state" >
              ["<s:property value="id"/>","<s:property escapeHtml="false" value="name"/> [<s:property value="id"/>]"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>

    ]

}
