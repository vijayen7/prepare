<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "agreementIds" :
    [
        <s:if test="agreementTypeIdList != null && agreementTypeIdList.size > 0">
            <s:iterator value="agreementTypeIdList" status="state" var="agreement">
              [<s:property value="#agreement.getAgreementId()"/>,"<s:property escapeHtml="false" value="%{#agreement.getDESCOEntity().getCodaCompanyCode() + '-' + #agreement.getCounterpartyEntity().getAbbreviation() + '-' + #agreement.getAgreementType().getAbbrev()}"/>"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}
