<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "financingStyles" :
    [

            <s:iterator value="@com.arcesium.treasury.model.financing.calculator.FinancingStyle@values()" status="state">
              ["<s:property value="financingStyleId"/>","<s:property escapeHtml="false"  value="financingStyleName"/>"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
    ]

}
