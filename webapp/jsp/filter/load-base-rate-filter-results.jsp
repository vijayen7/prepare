<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "baseRates" : [
<s:iterator value="baseRateList" var="baseRate" status="state">
  ["<s:property value="typeId"/>","<s:property escapeHtml="false" value="name"/>"]
  <s:if test="!#state.last">,</s:if>
</s:iterator>
]
}
