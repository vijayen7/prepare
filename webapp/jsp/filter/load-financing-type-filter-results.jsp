<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
 "financingTypes" :
    [
        
            <s:iterator value="@com.arcesium.treasury.model.financing.calculator.FinancingType@values()" status="state" >
              ["<s:property value="financingTypeId"/>","<s:property escapeHtml="false" value="financingTypeName"/>"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>

    ]

}
