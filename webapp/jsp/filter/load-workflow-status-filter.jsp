<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "workflowStatus" :
    [
        [-1, "All"],
        <s:iterator value="@deshaw.treasury.common.model.dqm.WorkflowStatus@values()" status="state">
            [<s:property value="statusId" />, "<s:property value="status" />"]
            <s:if test="!#state.last">,</s:if>
        </s:iterator>
    ]
}