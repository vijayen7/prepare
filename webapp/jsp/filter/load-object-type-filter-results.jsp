<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "objectTypes" :
    [
        <s:if test="objectTypeList != null && objectTypeList.size > 0">
            <s:iterator value="objectTypeList" status="state">
              ["<s:property />", "<s:property />"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}