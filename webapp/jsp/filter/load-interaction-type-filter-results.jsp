<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "interactionTypes" :
    [
        <s:if test="interactionTypeList != null && interactionTypeList.size != 0">
            <s:iterator value="interactionTypeList" status="state">
                ["<s:property />", "<s:property />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}