<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "custodianAccounts" :
    [
        <s:if test="nettingGroupSpecificCustodianAccountList != null && nettingGroupSpecificCustodianAccountList.size > 0">
            <s:iterator value="nettingGroupSpecificCustodianAccountList" status="state">
              [<s:property value="custodianAccountId" />, "<s:property escapeHtml="false" value="displayName" /> [<s:property value="custodianAccountId" />]"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}
