<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "ownershipEntities" : [
<s:if test="ownershipEntitiesFromFuma != null && ownershipEntitiesFromFuma.size > 0">
  <s:iterator value="ownershipEntitiesFromFuma" status="state">
    [<s:property value="id"/>, "<s:property escapeHtml="false" value="displayName"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
