<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "bundles" :
    [
        <s:if test="bundleList != null && bundleList.size != 0">
            <s:iterator value="bundleList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" /> [<s:property value="id" />]"]
                 <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}