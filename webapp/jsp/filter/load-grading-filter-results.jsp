<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "grading" : [
<s:if test="gradingList != null && gradingList.size > 0">
  <s:iterator value="gradingList" status="state">
    [<s:property value="gradingId"/>, "<s:property escapeHtml="false" value="grading"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
