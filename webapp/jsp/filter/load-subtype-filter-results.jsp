<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "foSubtypes" : [
<s:if test="subtypeList != null && subtypeList.size > 0">
  <s:iterator value="subtypeList" status="state">
    [<s:property value="subtypeId"/>, "<s:property escapeHtml="false" value="name"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
