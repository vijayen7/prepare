<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "marginTypes" :
    [
        <s:if test="marginTypes != null && marginTypes.size > 0">
            <s:iterator value="marginTypes" var="marginType" status="state">
              [<s:property value="#marginType.key" />, "<s:property value="#marginType.value" /> [<s:property value="#marginType.key" />]"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}
