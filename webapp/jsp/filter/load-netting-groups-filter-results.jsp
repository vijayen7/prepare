<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "nettingGroups" : [
<s:if test="nettingGroupList != null && nettingGroupList.size > 0">
  <s:iterator value="nettingGroupList" status="state">
    [<s:property value="nettingGroupId"/>, "<s:property escapeHtml="false" value="description"/> [<s:property value="nettingGroupId"/>]"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
