<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "businessUnits" :
    [
        <s:if test="businessUnitList != null && businessUnitList.size != 0">
            <s:iterator value="businessUnitList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="displayName" />" <s:if test="!isActive">, "Others"</s:if>]
                 <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}