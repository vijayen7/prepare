<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "countries" : [
<s:if test="countryList != null && countryList.size > 0">
  <s:iterator value="countryList" status="state">
    [<s:property value="id"/>, "<s:property escapeHtml="false" value="name"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
