<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "financingStatusType" :
    [
        <s:iterator value="@deshaw.treasury.common.model.FinancingStatusType@values()" status="loop">
            [<s:property value="financingStatusId"/>, "<s:property escapeHtml="false" value="displayName"/>"]
            <s:if test="!#loop.last">,</s:if>
        </s:iterator>
    ]
}