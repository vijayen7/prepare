<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "dataAvailabilityStatus" :
    [
        [-1, "All"],
        <s:iterator value="@deshaw.treasury.common.model.dqm.DataAvailabilityStatus@values()" status="state">
            [<s:property value="statusId" />, "<s:property escapeHtml="false" value="status" />"]
            <s:if test="!#state.last">,</s:if>
        </s:iterator>
    ]
}