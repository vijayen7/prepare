<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
 "specialBusinessUnits" :
    [
        
            <s:iterator value="@arcesium.treasury.model.financing.AccrualPostingRuleSpecialBusinessUnit@values()" status="state" >
              ["<s:property value="id"/>","<s:property escapeHtml="false" value="name"/>"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        
    ]

}
