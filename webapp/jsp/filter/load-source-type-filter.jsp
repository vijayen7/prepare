<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "sourceType" :
    [
        <s:if test="sourceTypes != null && sourceTypes.size != 0">
            <s:iterator value="sourceTypes" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="sourceTypeName" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}