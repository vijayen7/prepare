<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "gboTypes" :
    [
        <s:if test="gboTypeList != null && gboTypeList.size != 0">
            <s:iterator value="gboTypeList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}