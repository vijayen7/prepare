<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "accrualConvention" : [
<s:if test="accrualConventionList != null && accrualConventionList.size > 0">
  <s:iterator value="accrualConventionList" status="state">
    [<s:property value="days"/>, "<s:property escapeHtml="false" value="days"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
