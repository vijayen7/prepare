<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "bundleGroupTypes" :
    [
        <s:if test="bundleGroupTypeList != null && bundleGroupTypeList.size > 0">
            <s:iterator value="bundleGroupTypeList" status="state">
              [<s:property value="bundleGroupTypeId" />, "<s:property escapeHtml="false" value="name" />"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}