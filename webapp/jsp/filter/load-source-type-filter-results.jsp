<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "attrSourceTypes" : [
<s:if test="sourceTypeList != null && sourceTypeList.size > 0">
  <s:iterator value="sourceTypeList" status="state">
    [<s:property value="sourceTypeId"/>, "<s:property escapeHtml="false" value="sourceType"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
