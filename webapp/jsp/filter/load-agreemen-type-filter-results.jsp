<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "agreementTypes" :
    [
        <s:if test="agreementTypeList != null && agreementTypeList.size > 0">
            <s:iterator value="agreementTypeList" status="state">
              [<s:property value="agreementTypeId" />, "<s:property escapeHtml="false" value="abbrev" /> [<s:property value="agreementTypeId" />]"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}
