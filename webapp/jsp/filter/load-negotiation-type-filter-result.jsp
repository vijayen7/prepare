<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
 "negotiationTypes" :
    [
        
            <s:iterator value="@com.arcesium.treasury.model.seclend.NegotiationType@values()" status="state" >
              ["<s:property value="id"/>","<s:property escapeHtml="false" value="name"/>"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
    ]

}
