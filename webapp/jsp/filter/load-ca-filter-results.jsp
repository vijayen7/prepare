<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "custodianAccounts" :
    [
        <s:if test="custodianAccountList != null && custodianAccountList.size != 0">
            <s:iterator value="custodianAccountList" status="state">
                [<s:property value="custodianAccountId" />, "<s:property escapeHtml="false" value="displayName" />"]
                 <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}