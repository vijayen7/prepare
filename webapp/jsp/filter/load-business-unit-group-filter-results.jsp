<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "businessUnitGroups" :
    [
        <s:if test="businessUnitGroupList != null && businessUnitGroupList.size != 0">
            <s:iterator value="businessUnitGroupList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="description" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}