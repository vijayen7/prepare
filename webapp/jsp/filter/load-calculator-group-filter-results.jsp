<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "calculatorGroups" : [
<s:if test="calculatorGroupList != null && calculatorGroupList.size > 0">
  <s:iterator value="calculatorGroupList" status="state">
    ["<s:property value="calculatorGroupId"/>", "<s:property escapeHtml="false" value="name"/> [<s:property value="calculatorGroupId"/>]"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
