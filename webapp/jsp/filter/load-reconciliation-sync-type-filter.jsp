<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "reconciliationSyncType" :
    [
        <s:if test="reconciliationSyncTypes != null && reconciliationSyncTypes.size != 0">
            <s:iterator value="reconciliationSyncTypes" status="state">
                [<s:property value="ordinal()" />, "<s:property escapeHtml="false" value="name()" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}