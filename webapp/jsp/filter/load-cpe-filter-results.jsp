<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "cpes" :
    [
        <s:if test="cpeList != null && cpeList.size != 0">
            <s:iterator value="cpeList" status="state">
                  [<s:property value="id" />, "<s:property escapeHtml="false" value="name" /> [<s:property value="id" />]"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}
