<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "nettingGroups" : [
<s:if test="custodianAccountSpecificNettingGroupsList != null && custodianAccountSpecificNettingGroupsList.size > 0">
  <s:iterator value="custodianAccountSpecificNettingGroupsList" status="state">
    [<s:property value="nettingGroupId"/>, "<s:property escapeHtml="false" value="description"/> [<s:property value="nettingGroupId"/>]"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
