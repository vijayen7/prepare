<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "businessUnits" :
    [
        <s:if test="allBusinessUnitList != null && allBusinessUnitList.size != 0">
            <s:iterator value="allBusinessUnitList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" />"]
                 <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}