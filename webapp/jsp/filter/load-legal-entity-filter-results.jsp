<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "descoEntities" :
    [
        <s:if test="descoEntityList != null && descoEntityList.size > 0">
            <s:iterator value="descoEntityList" status="state">
              [<s:property value="id" />, "<s:property escapeHtml="false" value="name" /> [<s:property value="id" />]"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}
