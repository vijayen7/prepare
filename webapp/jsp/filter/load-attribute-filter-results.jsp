<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "attributes" : [
<s:iterator value="allFinancingTermAttrDefs" var="attr" status="state">
  ["<s:property value="#attr.id"/>","<s:property escapeHtml="false" value="#attr.name"/>"]
  <s:if test="!#state.last">,</s:if>
</s:iterator>
]
}
