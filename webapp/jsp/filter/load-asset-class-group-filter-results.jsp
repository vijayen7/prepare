<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "assetClassGroups" : [
<s:if test="assetClassGroupList != null && assetClassGroupList.size > 0">
  <s:iterator value="assetClassGroupList" status="state">
    [<s:property value="assetClassGroupId"/>, "<s:property escapeHtml="false" value="displayName"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
