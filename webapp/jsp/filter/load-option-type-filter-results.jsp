<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "optionTypes" :
    [
        <s:if test="optionTypes != null && optionTypes.size > 0">
            <s:iterator value="optionTypes" var="optionType" status="state">
              ["<s:property value="#optionType.key" />", "<s:property value="#optionType.value" />"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}
