<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "ownershipEntities" :
    [
        <s:if test="ownershipEntityList != null && ownershipEntityList.size != 0">
            <s:iterator value="ownershipEntityList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}