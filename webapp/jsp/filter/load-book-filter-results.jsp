<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "books" :
    [
        <s:if test="bookList != null && bookList.size != 0">
            <s:iterator value="bookList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="displayName" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}