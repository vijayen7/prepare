<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "attrFOTypes" : [
<s:if test="foTypeList != null && foTypeList.size > 0">
  <s:iterator value="foTypeList" status="state">
    [<s:property value="id"/>, "<s:property escapeHtml="false" value="name"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
