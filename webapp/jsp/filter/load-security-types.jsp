<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "borrowFinancingSecurityTypes" :
    [
        <s:if test="borrowFinancingSecurityTypeList != null && borrowFinancingSecurityTypeList.size > 0">
            <s:iterator value="borrowFinancingSecurityTypeList" status="state">
              [<s:property value="id" />, "<s:property escapeHtml="false" value="name" />"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}