<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "financialAccount" :
    [
        <s:if test="financialAccounts!= null && financialAccounts.size > 0">
            <s:iterator value="financialAccounts" status="state">
              ["<s:property/>","<s:property/>"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}