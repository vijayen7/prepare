<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "marginCalculatorConfigs" :
    [
        <s:if test="marginConfigList != null && marginConfigList.size != 0">
            <s:iterator value="marginConfigList" status="state">
                ["<s:property />", "<s:property />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}