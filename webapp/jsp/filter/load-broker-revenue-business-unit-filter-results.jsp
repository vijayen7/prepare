<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "brokerRevenueBusinessUnits" :
    [
        <s:if test="brokerRevenueBusinessUnitList != null && brokerRevenueBusinessUnitList.size != 0">
            <s:iterator value="brokerRevenueBusinessUnitList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="description" />" <s:if test="!isActive">, "Others"</s:if>]
                 <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}