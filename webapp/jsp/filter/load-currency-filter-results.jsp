<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "currency" : [
<s:if test="currencyList != null && currencyList.size > 0">
  <s:iterator value="currencyList" status="state">
    [<s:property value="spn"/>, "<s:property escapeHtml="false" value="abbreviation"/> [<s:property value="spn"/>]"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
