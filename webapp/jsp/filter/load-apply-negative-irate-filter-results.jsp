<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
 "applyNegativeIrates" :
    [

            <s:iterator value="@com.arcesium.treasury.model.financing.agreementTerms.ApplicableNegativeRateType@values()" status="state" >
               ["<s:property value="id"/>","<s:property escapeHtml="false" value="displayName"/>"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>

    ]

}
