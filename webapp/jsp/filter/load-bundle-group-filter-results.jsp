<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "bundleGroups" :
    [
        <s:if test="bundleGroupList != null && bundleGroupList.size > 0">
            <s:iterator value="bundleGroupList" status="state">
              [<s:property value="bundleGroupId" />, "<s:property escapeHtml="false" value="name" />"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]

}