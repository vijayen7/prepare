<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "descoEntityFamilies" :
    [
        <s:if test="treasuryEntityFamilyList != null && treasuryEntityFamilyList.size != 0">
            <s:iterator value="treasuryEntityFamilyList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" /> [<s:property value="id" />]"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ],
    "legalEntityFamilyToLegalEntityListMap" :
    [
    <s:if test="treasuryEntityFamilyList != null && treasuryEntityFamilyList.size != 0">
        <s:iterator value="treasuryEntityFamilyList" status="state">
            {"entityFamilyId" :<s:property value="id" />, "entities" :<s:property escapeHtml="false" value="entities" />}
            <s:if test="!#state.last">,</s:if>
         </s:iterator>
    </s:if>
    ]

}