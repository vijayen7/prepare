<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "tMinusOneFilterDate" : "<s:property value="tMinusOneFilterDate" />" ,
    "tMinusTwoFilterDate" : "<s:property value="tMinusTwoFilterDate" />",
    "tFilterDate" : "<s:property value="tFilterDate" />"
}