<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "counterPartyEntityFamilies" :
    [
        <s:if test="cpeFamilyList != null && cpeFamilyList.size != 0">
            <s:iterator value="cpeFamilyList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}