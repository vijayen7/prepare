<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{ "attrNames" : [
<s:if test="attributeNameList != null && attributeNameList.size > 0">
  <s:iterator value="attributeNameList" status="state">
    ["<s:property value="attrId"/>", "<s:property escapeHtml="false" value="attrName"/>"]
    <s:if test="!#state.last">,</s:if>
  </s:iterator>
</s:if>
]
}
