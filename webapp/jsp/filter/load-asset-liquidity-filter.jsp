<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "assetLiquidity" :
    [
        <s:iterator value="@deshaw.treasury.common.model.AssetLiquidity@values()" status="loop">
            [<s:property value="assetLiquidityId"/>, "<s:property escapeHtml="false" value="displayName"/>"]
            <s:if test="!#loop.last">,</s:if>
        </s:iterator>
    ]
}