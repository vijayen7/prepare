<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "execBrokerFamilies" :
    [
        <s:if test="execBrokerFamilyList != null && execBrokerFamilyList.size != 0">
            <s:iterator value="execBrokerFamilyList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ],

    "cpeAndExecBrokerFamilies" :
    [
        <s:if test="cpeAndExecBrokerFamilyList != null && cpeAndExecBrokerFamilyList.size != 0">
            <s:iterator value="cpeAndExecBrokerFamilyList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ],

    "execBrokers" :
    [
        <s:if test="execBrokerList != null && execBrokerList.size != 0">
            <s:iterator value="execBrokerList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="name" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}