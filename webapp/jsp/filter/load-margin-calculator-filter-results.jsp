<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{

    "marginCalculators" :
    [
        <s:if test="marginCalculatorList != null && marginCalculatorList.size != 0">
            <s:iterator value="marginCalculatorList" status="state">
                [<s:property value="id" />, "<s:property escapeHtml="false" value="alias" />"]
                <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}