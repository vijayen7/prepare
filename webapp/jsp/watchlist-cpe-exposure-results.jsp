<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>
{
    "watchlistCounterpartyExposure" :
    [
    <s:if test="cpeWatchlistExposureDataList != null && cpeWatchlistExposureDataList.size > 0">
        <s:iterator value="cpeWatchlistExposureDataList" status="outerLoop">
        {
         "id" : "<s:property value="name"/>",
         "name" : "<s:property value="name"/>",

         <s:iterator value="ownershipEquityMap" status="mapStatus">
            <s:set var="equity" value= "value[@deshaw.treasury.web.model.OwnershipExposureData$EquitySet@equity]" />
            <s:set var="relativeEquity" value="value[@deshaw.treasury.web.model.OwnershipExposureData$EquitySet@relativeEquity]" />
            "<s:property value="key"/>_equity"  : "<s:property value="#equity"/>",
            "<s:property value="key"/>_relative_equity" : "<s:if test="#relativeEquity != null && #relativeEquity != 0.0"><s:text name="percent"><s:param value="%{(#equity - #relativeEquity)/#relativeEquity}"/></s:text></s:if><s:else>N/A</s:else>",
         </s:iterator>

         "ownership_entities" : [
         <s:iterator value="ownershipEquityMap" status="ownershipsLoop">
             "<s:property value="key"/>"<s:if test="!#ownershipsLoop.last">,</s:if>
         </s:iterator>
         ]
        }
        <s:if test="!#outerLoop.last">,</s:if>
        </s:iterator>
    </s:if>
    ]
}