<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib" %>

<!-- This will fetch properties from landing-app codex and set attributes in the request-->

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Impact reports</title>
    <jsp:include page="../arcux/common-arc-imports.jsp"></jsp:include>
    <link rel="stylesheet" type="text/css" href="/treasury/css/seclend/ui.daterangepicker-min.css" />
    <link rel="stylesheet" href="/treasury/css/impact-min.css">
    <link rel="stylesheet" href="/treasury/css/treasury_common-min.css">
    <link rel="stylesheet" href="/treasury/css/filter/security_search-min.css">

    <script>
        (function(){
            window.treasury = window.treasury || {};
            window.treasury.impact = window.treasury.impact || {};
            window.treasury.impact.user = "<%=request.getRemoteUser()%>";
        })();
    </script>
</head>

<body style="overflow:hidden">
    <arc-banner size="content"></arc-banner>
    <arc-dialog></arc-dialog>
    <arc-loading id="impactGridLoading" style="display:none">
      <div id="loading-indicator">
    <i id="loading-indicator__icon" class="fa fa-spinner fa-spin"></i>
    <br />
    <label class="loading-indicator__comment">Loading</label>
      </div>
    </arc-loading>

    <arc-layout type="row" gutter="0px" id="landingPageDiv" style="display:none">
        <arc-header size="content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
            <div slot="application-menu">
                <div class="impact-toggle-view">
                    <a href="#" id="showImpactSummary">SUMMARY</a>
                    <a href="#" id="showImpactDetail">DETAIL</a>
                </div>
            </div>
        </arc-header>

        <arc-layout type="column" gutter="20px" id="parentArcLayout" size="1" style="overflow:hidden;">
            <arc-sidebar id="impactFilterSidebar" header="Impact Summary Filters" size="300px" collapsible="false" type="fill">
                <arc-panel-element id="impactSummaryFilters" showheader="false">
                    <label>Year :</label>
                    <arc-dropdown id="yearFilter"></arc-dropdown>
                    <br/>

                    <label>Legal Entity:</label>
                    <arc-combobox-multi id="legalEntityImpactSummaryFilter"></arc-combobox-multi>
                    <br/>

                    <label>Counterparty Entitiy Family:</label>
                    <arc-combobox-multi id="cpeImpactSummaryFilter"></arc-combobox-multi>
                    <br/>

                    <label>Reporting Currency:</label>
                        <arc-combobox id="reportingCurrencySummaryFilter"></arc-combobox>
                    <br/>

                    <div style="text-align: center">
                        <button id="searchImpactSummaryId" onclick="">Search</button>
                    </div>
                </arc-panel-element>
                <arc-panel-element id="impactDetailFilters" showheader="false" >
                    <label>Date :</label>
                    <div>
                        <input type="text" id="dateRangePicker" style="display:inline;"/>
                        <i class="fa fa-calendar"  id="calendarIconId" style="margin-right: 5px;"></i>
                        <i class="fa fa-info-circle" id="dateRangeInfo"
                        title="Max date range supported is 30 days for Rate Negotiation. If valid securities are provided in the Security filter below, you can perform YTD search">
                        </i>
                    </div>
                    <div id="dateRangePickerError" class="critical"></div>
                    <br/>

                    <label>Legal Entity:</label> <br />
                    <arc-combobox-multi id="legalEntityImpactDetailFilter"></arc-combobox-multi>
                    <br/>

                    <label>Counterparty Entitiy Family:</label>
                    <arc-combobox-multi id="cpeImpactDetailFilter"></arc-combobox-multi>
                    <br/>

                    <label>Security:</label>
                    <div>
                        <input type="text" id="pnlSpns" placeholder="Enter Security name or SPN here" name="pnlSpns" style="display:inline;width:100%"/>
                    </div>
                    <br/>

                    <label>Negotiation Type:</label>
                        <arc-combobox-multi id="negotiationTypeFilter"></arc-combobox-multi>
                    <br/>

                    <label>Reporting Currency:</label>
                        <arc-combobox id="reportingCurrencyImpactDetailFilter"></arc-combobox>
                    <br/>

                    <div style="text-align: center">
                        <div id="impactFilterValidation" class="arc-message--critical" style="margin-top:10px; display:none"></div>
                        <button id="searchImpactDetailId" style="margin:10px">Search</button>
                    </div>

                    <button id="saveGridView" class="save-settings-btn" style="display:none">Save View</button>
                    <arc-save-settings style="display:none" class="save-settings-select"></arc-save-settings>
                </arc-panel-element>
            </arc-sidebar>

            <!-- Impact Summary View -->
            <div id="impactSummaryView">
                <arc-layout type="row" id="gridLayout" style="width:100%;height:90%">
                    <arc-panel-element size="1" showheader="true" label="YTD Summary" type="fill" collapsible="false">
                        <div id="ytdImpactSummaryGridInfo" class="arc-message" style="display:none">
                        </div>
                        <div class="arc-grid__table" id="ytdImpactSummaryGrid" style="width:100%;height:85%">
                        </div>
                    </arc-panel-element>
                    <arc-panel-element size="1" showheader="true" label="Monthly Summary" type="fill" collapsible="false">
                        <div id="monthlyImpactSummaryGridInfo" class="arc-message" style="display:none">
                        </div>
                        <div class="arc-grid__table" id="monthlyImpactSummaryGrid" style="width:100%;height:85%">
                        </div>
                    </arc-panel-element>
                    <arc-panel-element size="1" showheader="true" label="Daily Summary" type="fill" collapsible="false">
                        <div id="dailyImpactSummaryGridInfo" class="arc-message" style="display:none">
                        </div>
                        <div class="arc-grid__table" id="dailyImpactSummaryGrid" style="width:100%;height:85%">
                        </div>
                    </arc-panel-element>
                </arc-layout>
                <span id="impactSummaryReportingCurrencyMessage" size="fit" style="display:none">
                </span>
            </div>

            <!-- Impact Detail View -->
            <div id="impactDetailView" size="0" style="display:none">
                <div id="positionImpactDetailGridInfo" class="arc-message" style="display:none">
                </div>
                <div id="cascadeFilterContainer">
                </div>
                <div class="arc-grid__table" id="positionImpactDetailGrid" style="width:100%;max-height:85%">
                </div>
                <span id="impactDetailReportingCurrencyMessage" size="fit" style="display:none">
                </span>
            </div>

        </arc-layout>
    </arc-layout>
    <script type="text/javascript" src="/treasury/js/filter/securitysearch-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/multiselect-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/singleselect-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>

     <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>

    <script type="text/javascript" src="/treasury/js/util-min.js"></script>
    <script type="text/javascript" src="/treasury/js/impact/gridconfig/impactColumnsConfig-min.js"></script>
    <script type="text/javascript" src="/treasury/js/impact/gridconfig/positionImpactColumnsConfig-min.js"></script>
    <script type="text/javascript" src="/treasury/js/impact/gridconfig/gridoptions-min.js"></script>
    <script type="text/javascript" src="/treasury/js/impact/saveSettings-min.js"></script>
    <script type="text/javascript" src="/treasury/js/impact/impactGridActions-min.js"></script>
    <script type="text/javascript" src="/treasury/js/impact/validation-min.js"></script>
    <script type="text/javascript" src="/treasury/js/impact/landing-min.js"></script>
    <script type="text/javascript" src="/treasury/js/seclend/jquery.daterangepicker-min.js"></script>
    <script>
        ArcThemeHandler.addThemeChangeListener(function() {
            var href = document.querySelector('link#jquery-ui-css').href;

            if (ArcThemeHandler.getCurrentTheme() == 'dark') {
                document.querySelector('link#jquery-ui-css').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
            } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
                if (href.indexOf('light') === -1) {
                    document.querySelector('link#jquery-ui-css').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
                }
            }
        });

        ArcThemeHandler.enableThemeSelection();

        // Default properties from Codex properties
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal' /> ;
        window.treasury.defaults = window.treasury.defaults || {};

        // copy system property of default 'date' to treasury.defaults package
        if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined && CODEX_PROPERTIES["treasury.portal.defaults.date"].myValue) {
            treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
        }

        if (CODEX_PROPERTIES['arcesium.ux.commons.saveSettingsHost'] != undefined && CODEX_PROPERTIES['arcesium.ux.commons.saveSettingsHost'].myValue) {
            treasury.defaults.saveSettingsHost = CODEX_PROPERTIES['arcesium.ux.commons.saveSettingsHost'].myValue;
        }

        window.addEventListener("WebComponentsReady", function() {
            treasury.impact.landing.initialize(<s:property value="showDetail"/>);
        });
    </script>
</body>

</html>
