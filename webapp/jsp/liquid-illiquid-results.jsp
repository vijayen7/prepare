<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>
{
    <%-- The topNCounterpartyFamilies is a method that performs iteration 
         over list of liquid illiquid objects at different levels. 
         Hence shouldn't be called often. --%>
    <s:set var="topNCpes" value="topNCounterpartyFamilies"/>
    "topNCpes" : [
        <s:if test="#topNCpes != null && #topNCpes.size > 0">
        <s:iterator value="#topNCpes" status="cpeIterator">
         "<s:property />"<s:if test="!#cpeIterator.last">,</s:if>
        </s:iterator>
        </s:if>
    ],

    "total" : [{
        "isSummary" : true,
        "displayName" : "<s:property value="totalLiquidIlliquidData.displayName"/>",
        "buSortOder"  : "<s:property value="totalLiquidIlliquidData.sortOrder"/>",
        <s:set var="totalLevelCPEFamilyLiquidIlliquidMap" value="totalLiquidIlliquidData.cpeFamilyLiquidIlliquidDataMap" />
        <s:if test="#topNCpes != null && #topNCpes.size > 0">
            <s:iterator var="cpeName" value="#topNCpes" status="cpeIterator">
              "<s:property value="#cpeName"/>" : 
              {"value" : "<s:text name="number"><s:param value="#totalLevelCPEFamilyLiquidIlliquidMap[#cpeName].mv"/></s:text>  <s:if test="%{#cpeName != 'Total' && #parameters['gmvOrNmv'][0] != 'NMV'}">(<s:text name="percent"><s:param value="#totalLevelCPEFamilyLiquidIlliquidMap[#cpeName].mv/#totalLevelCPEFamilyLiquidIlliquidMap['Total'].mv" /></s:text>)</s:if>",
               "params" : ""}
              <s:if test="!#cpeIterator.last">,</s:if>
            </s:iterator>
        </s:if>
    }
    ],

    "liquidIlliquidData" : [
    <s:if test="buGroupLiquidIlliquidDataList != null && buGroupLiquidIlliquidDataList.size > 0">
        <s:iterator value="buGroupLiquidIlliquidDataList" status="buGroupLoop">
            {
                "id" : "buGroup_<s:property value="displayName"/>",
                "displayName" : "<s:property value="displayName"/>",
                "buSortOder"  : "<s:property value="sortOrder"/>",
                <s:set var="buGroupLevelCPEFamilyLiquidIlliquidMap" value="cpeFamilyLiquidIlliquidDataMap" />

                <s:if test="#topNCpes != null && #topNCpes.size > 0">
                <s:iterator var="cpeName" value="#topNCpes" status="cpeIterator">
                  "<s:property value="#cpeName"/>" : 
                  { "value" : "<s:property value="#buGroupLevelCPEFamilyLiquidIlliquidMap[#cpeName].mv"/>",
                    "params" : "<s:property value="#buGroupLevelCPEFamilyLiquidIlliquidMap[#cpeName].linkParameterValues"/>"},
                </s:iterator>
                </s:if>

                "children" : [
                    <s:iterator value="childLiquidIlliquidDataList" status="buLoop">
                    {
                        "id" : "bu_<s:property value="%{displayName + sortOrder}"/>",
                        "displayName" : "<s:property value="displayName"/>",
                        "buSortOder"  : "<s:property value="sortOrder"/>",
                        <s:set var="buLevelCPEFamilyLiquidIlliquidMap" value="cpeFamilyLiquidIlliquidDataMap" />

                        <s:if test="#topNCpes != null && #topNCpes.size > 0">
                        <s:iterator var="cpeName" value="#topNCpes" status="cpeIterator">
                          "<s:property value="#cpeName"/>" : 
                          { "value" : "<s:property value="#buLevelCPEFamilyLiquidIlliquidMap[#cpeName].mv"/>",
                            "params" : "<s:property value="#buLevelCPEFamilyLiquidIlliquidMap[#cpeName].linkParameterValues"/>"}
                          <s:if test="!#cpeIterator.last">,</s:if>
                        </s:iterator>
                        </s:if>
                    }
                    <s:if test="!#buLoop.last">,</s:if>
                    </s:iterator>
                ]
            }
            <s:if test="!#buGroupLoop.last">,</s:if>
        </s:iterator>
    </s:if>
    ]
}