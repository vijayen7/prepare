<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
    "classificationRules" :
    [
        <s:if test="classificationRules != null && classificationRules.size > 0">
            <s:iterator value="classificationRules" status="state">
              [<s:property value="classificationTypeId" />, "<s:property value="name" />", "<s:property value="comment" />"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>
        </s:if>
    ]
}
