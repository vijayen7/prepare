<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Agreement Summary</title>
    <jsp:include page="../arcux/common-arc-imports.jsp"></jsp:include>


    <!-- These imports will be removed once arc date components are available -->

    <link rel="stylesheet" href="/treasury/css/comet-min.css">
    <link rel="stylesheet" href="/treasury/components/treasury-components.css">

    <script>
        (function(){
            window.treasury = window.treasury || {};
            window.treasury.comet = window.treasury.comet || {};
            window.treasury.comet.user = "<%=request.getRemoteUser()%>";
        })();
    </script>
</head>

<body style="overflow:hidden">
<input type="hidden" id="stabilityLevel" name="stabilityLevel" value="${stability_level}" />
<input type="hidden" id="clientName" name="clientName" value="${client_name}"/>

    <arc-banner size="content"/></arc-banner>
    <%@ include file="display_log_wire.jsp" %>
     <template  id="agreementDetailTemplate">
     <arc-panel-element class="size--content" id="agreementDetail" label="" dismissible>
                    <div class="layout--flex--row">
                        <arc-panel-element class="size--content margin--bottom">
                            <div class="layout--flex">
                                <div>
                                    <button id="detailQuickPublishId" disabled>
                                        <i class="icon-quick"></i> Quick Publish
                                    </button>
                                    <button id="postCollateralAndPublishButton" disabled>
                                        <i class="icon-transfer"></i> Post Collateral & Publish
                                    </button>
                                    <button id="detailBeginWorkflowId" class="button--primary" disabled>
                                        <i class="icon-edit"></i> Begin Workflow
                                    </button>
                                    <a id="agreementPositionDetailLink" class="margin--small" href="#">Agreement Level Position Detail <i class="icon-external-link"></i></a>
                                
                                    <a id="crimsonPositionDetailLink" class="margin--small" href="#" style="display: none">Position Detail (Day on
                                        Day Diff) <i class="icon-external-link"></i></a>
                                    
                                    <a id="viewAdjustmentsLink" class="margin--small" href="#" style="display: none">View Adjustments <i class="icon-external-link"></i></a>

                                </div>
                                <div class="size--content">
                                    <button id="detailCallReportId" disabled>
                                        <i class="icon-mail"></i> Call Report
                                    </button>
                                    <select id="downloadBrokerSelectId" disabled>
                                    </select>
                                    <button id="detailDownloadBrokerFileId" disabled>
                                        <i class="icon-download"></i> Broker File
                                    </button>
                                    <a href="#" onclick="window.treasury.comet.agreementDetail.launchAttributeDetails()" id="attributeDetailsPopoutLink">Attribute
                                        Details <i class="icon-pop-out"></i></a>
                                </div>
                            </div>

                        </arc-panel-element>
                        <div class="arc-message size--content margin--bottom" id="agreementRagStatusMsg" hidden>
                            <span id="agreementStatusMsg"></span>
                        </div>
                        <arc-tab-panel style="max-height: 300px">
                            <arc-tab label="Collateral Mgmt. vs Reconciler">
                                <div  id="CollateralMgmtVsReconTab"> </div>
                            </arc-tab>
                            <arc-tab label="Data Summary" >
                                <div class="margin--bottom">
                                    <div class="layout--flex fill--width gutter" id="callTypeDetails">
                                        <%@ include file="agreement_internal_vs_external.jspf" %>
                                        <%@ include file="call_type_detail.jspf" %>
                                    </div>
                                </div>
                                <%@ include file="agreement_additional_detail.jspf" %>
                            </arc-tab>
                        </arc-tab-panel>
                    </div>
                </arc-panel-element>
     </template>
     <div id="postCollateralPopup"></div>
     <div id="physicalCollateralInventoryLoader" hidden class="overlay">
             <div class="loader loader--block form-field--center" style="width: 100px">Loading</div>
         </div>
         <div id="physicalCollateralInventory"></div>
     <arc-dialog label="Attribute Details" id="attributeDetail" collapsible>
                     <table style="display:block" class="table">
                        <tbody>
                            <tr>
                                <td>
                                    Abbrev:
                                </td>
                                <td class="text-align--right" id="legalEntityAbbrev">
                                </td>
                                <td class="text-align--right" id="counterpartyAbbrev">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Name
                                </td>
                                <td class="text-align--right"  id="legalEntityName">
                                </td>
                                <td class="text-align--right"  id="counterPartyName">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Exposure <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="adInternalExposure">
                                </td>
                                <td class="text-align--right"  id="adExternalExposure">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Independent Amount <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="internalIA">
                                </td>
                                <td class="text-align--right"  id="externalIA">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Collateral <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="adInternalCollateral">
                                </td>
                                <td class="text-align--right"  id="adExternalCollateral">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Posted Collateral <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="internalPostedCollateral">
                                </td>
                                <td class="text-align--right"  id="externalPostedCollateral">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Credit Support Amount <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="internalCreditSupport">
                                </td>
                                <td class="text-align--right"  id="externalCreditSupport">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Secured Party/Pledgor
                                </td>
                                <td class="text-align--right"  id="seCategory">
                                </td>
                                <td class="text-align--right"  id="cpeCategory">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Threshold Amount <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="internalThresholdAmount">
                                </td>
                                <td class="text-align--right"  id="externalThresholdAmount">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Minimum Transfer Amount <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="internalMinimumTransferAmount">
                                </td>
                                <td class="text-align--right"  id="externalMinimumTransferAmount">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Rounding Amount <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="internalRoundingAmount">
                                </td>
                                <td class="text-align--right"  id="externalRoundingAmount">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Rounding Convention
                                </td>
                                <td class="text-align--right" colspan="2"  id="roundingConvention">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Applicable Delivery Amount <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  colspan="2" id="applicableDeliveryAmount">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Applicable Return Amount <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right" colspan="2"  id="applicableReturnAmount">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Raw Excess/Deficit <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="rawInternalED">
                                </td>
                                <td class="text-align--right"  id="rawExternalED">
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    Excess/Deficit <span class="reportingCurrency"></span>
                                </td>
                                <td class="text-align--right"  id="adInternalED">
                                </td>
                                <td class="text-align--right"  id="adExternalED">
                                </td>
                            </tr>
                        </tbody>
                    </table>
     </arc-dialog>
     <%@ include file="/jsp/common/workflow/failed-workflow-template.jspf" %>
     <%@ include file="/jsp/common/workflow/locked-workflow-template.jspf" %>
     <%@ include file="/jsp/common/workflow/resume-workflow-template.jspf" %>

    <div class="layout--flex--row">

         <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
        </arc-header>

<div class="layout--flex" id="parentArcLayout" >
            <arc-sidebar id="agreementFilterSidebar" header="Search"  collapsible="true" class="size--content">
              <arc-panel-element collapsible="false" showheader="false" dismissible="false">
    <div class="form">
      <div class="row">
        <arc-save-settings></arc-save-settings>
        <button id="summary-search-save" class="button--tertiary size--content">
          <i class="icon-save" id="save-button" size="content"></i>
        </button>
      </div>
    </div>
  </arc-panel-element>
                <arc-panel-element>


                    <div class="form">
                        <div class="row">
                            <label class="size--content">Date:</label>
                            <input  id="datepicker" type="text"/>
                        </div>
                    </div>
                </arc-panel-element>
                <arc-panel-element id="agreementSummaryFilters">
                        <div class="form">
                          <div>
                            <label>Legal Entity Family:</label>
                            <arc-combobox id="entityFamilyFilter" onchange="window.treasury.comet.agreementSummaryFilterAction.handleEntityFamilyChange()" ></arc-combobox>
                          </div>
                          <div>
                              <label>Legal Entities:</label>
                              <arc-combobox id="legalEntityFilter" ></arc-combobox>
                        </div>
                        <div>
                            <label>Counterparty Entities:</label>
                            <arc-combobox id="cpeFilter" ></arc-combobox>
                      </div>
                      <div>
                          <label>Agreement Types:</label>
                          <arc-combobox id="agreementTypeFilter" ></arc-combobox>
                    </div>

                        </div>


                </arc-panel-element>
                <arc-panel-element label="Additional Filters">
                  <label for="">Workflow Status</label>
                    <div class="form margin--bottom--double">
                        <div class="row no-margin--bottom">
                                    <input id="showNotStartedCB" type="checkbox"><label for="" class="size--content">Not Started</label>
                                    <input id="showInProgressCB" type="checkbox" class="margin--left"><label for="" class="size--content">In Progress</label>
                        </div>
                        <div class="row">
                          <input id="showPublishedCB" type="checkbox"><label for="" class="size--content">Published</label>
                        </div>
                      </div>
                      <label for="">Exception Status</label>
                        <div class="form">
                            <div class="row">
                                    <input id="showCritical" type="checkbox"><label for="" class="size--content">Critical</label>
                                    <input id="showWarning" type="checkbox" class="margin--left"><label for="" class="size--content">Warning</label>
                                    <input id="showPassed" type="checkbox" class="margin--left"><label for="" class="size--content">Passed</label>
                                </div>
                        </div>
                </arc-panel-element>
                <div  slot="footer">
                    <button class="button--primary" id="searchId" onclick="window.treasury.comet.agreementsummary.loadAgreementSummaryData()">Search</button>
                    <button class="button--text" id="copySearchURL" onClick="window.treasury.comet.agreementsummary.copySearchUrl()">Copy Search URL</button>
                </div>
            </arc-sidebar>
            <arc-resizer id='searchFilterResizer'></arc-resizer>
            <!-- Agreement Summary View -->
            <div class="layout--flex--row size--3 gutter padding--horizontal fill--height">
            <div class="overlay" hidden="hidden" style="z-index: 9000;">
        <div class="loader loader--block form-field--center" style="width: 100px">
              Loading
        </div>
     </div>
                <div class="message size--content margin--top" hidden id="agreementSummaryView" >Perform Search to view Results</div>
                <div class="size--content form" id="quickLinks" hidden>
                  <div class="row">
                    <button disabled id="quickPublishId" class="size--content"><i class="icon-quick"></i> Quick Publish</button>
                    <div></div>
                    <span class="size--content" >Found <strong id="noOfRecords"></strong> records for <strong id="searchDate"></strong></span>
                    <vr class="size--content"></vr>
                    <button onclick="window.treasury.comet.agreementsummary.reloadAgreementSummary(false);" class="button--text size--content"><i class="icon-refresh"></i> Refresh Data</button>
                  </div>
                </div>
                <div class="size--2" id="agreementSummaryGridDiv" hidden>
                    <div class="arc-grid__table" id="agreementSummaryGrid" style="width:100%">
                    </div>
                </div>
                <div id="statusLegend" class="grid__legend display--flex size--content" hidden>
                    <small class="margin--right"><strong>Legend: </strong></small>
                    <div class="grid__legend__color red"></div>
                    <small class="grid__legend__label">Critical</small>
                    <div class="grid__legend__color orange"></div>
                    <small class="grid__legend__label">Warning</small>
                    <div class="grid__legend__color green10"></div>
                    <small class="grid__legend__label">Clear</small>
                    <div class="grid__legend__color primary"></div>
                    <small class="grid__legend__label">Missing Broker Data</small>
                    <i class="icon-success"></i>
                    <small class="grid__legend__label">Published</small>
                    <i class="icon-locked"></i>
                    <small class="grid__legend__label">In Progress</small>
                </div>
                <div id = "agreementDetailDiv" class="size--content margin--bottom" hidden></div>
            </div>
        </div>

    </div>
<script>
    WebComponents.waitFor(() => {
            let scriptArrayForPromise = [UIUX.loadjscssfile("/treasury/js/filter/multiselect-new.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/filter/filtergroup.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/filter/singleselect.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/filter/loadfilters.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/treasury.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/util.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/cometUtil.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/constants.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/arc-grid/arc-grid.min.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/gridConfig/agreementSummaryColumnConfig.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/gridConfig/agreementWorkflowColumnConfig.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/agreementSummary/agreementSummaryGridActions.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/agreementSummary/agreementSummaryFilterAction.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/agreementWorkflow/agreementWorkflowAction.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/agreementWorkflow/agreementWorkflowDisputeAmount.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/wires/agreementWiresAction.js", "js"),
                  UIUX.loadjscssfile("/treasury/js/comet/agreementSummary/agreementDetail.js", "js"),
                  UIUX.loadjscssfile("${cdn_base_url}${version}/react/react.development.js", "js"),
                  UIUX.loadjscssfile("${cdn_base_url}${version}/react-dom/react-dom.development.js", "js"),
                  UIUX.loadjscssfile("/treasury/components/treasury-components.production.min.js", "js")]
            Promise.all(scriptArrayForPromise).then(() => {
                  UIUX.loadjscssfile("/treasury/js/comet/agreementSummary/agreementSummary.js", "js")
                  });
        });
      </script>
      <script>
        UIUX.loadjscssfile("${cdn_base_url}${version}/js/arc-body-imports.js", "js")
      </script>
    <script>
        ArcThemeHandler.enableThemeSelection();
    </script>
    <script>
          var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
          var TREASURY_CODEX_PROPERTIES = <codex:propertySet category='treasury'/>;
          window.treasury.defaults = window.treasury.defaults || {};
          if (CODEX_PROPERTIES['treasury.lcm.isCrimsonEnabled'] != undefined) {
            treasury.defaults.isCrimsonEnabled = CODEX_PROPERTIES['treasury.lcm.isCrimsonEnabled'].myValue;
          }

          if (CODEX_PROPERTIES['treasury.lcm.crimsonEnabledAgmtTypeIds'] != undefined) {
            treasury.defaults.crimsonEnabledAgmtTypeIds = CODEX_PROPERTIES['treasury.lcm.crimsonEnabledAgmtTypeIds'].myValue;
          }

          if (CODEX_PROPERTIES['com.arcesium.treasury.lcm.isPortfolioMandatoryToLogWire'] != undefined) {
            treasury.defaults.isPortfolioMandatoryToLogWire = CODEX_PROPERTIES['com.arcesium.treasury.lcm.isPortfolioMandatoryToLogWire'].myValue;
          }

          if (TREASURY_CODEX_PROPERTIES['arcesium.treasury.comet.rag.enable'] != undefined) {
            treasury.defaults.isRagEnabled = TREASURY_CODEX_PROPERTIES['arcesium.treasury.comet.rag.enable'].myValue;
          }
    </script>

</body>

</html>