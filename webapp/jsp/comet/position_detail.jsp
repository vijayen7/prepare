<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- saved from url=(0041)http://www/~mulani/slickgrid/landing.html -->
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>::Position Detail::</title>
    <jsp:include page="../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>

      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css"/>

        <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css"/>
        <!-- Following three imports will be removed once arc date components are available -->

        <script>
            function switchJqueryCSS() {
              var href = document.querySelector('link#jquery-ui-css-arcux').href
              if (ArcThemeHandler.getCurrentTheme() == 'dark') {
                if (href.indexOf('light') > -1) {
                  document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
                }
              } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
                if (href.indexOf('light') === -1) {
                  document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
                }
              }
            }
            switchJqueryCSS();
            ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
        </script>
        <script>
          (function () {
            window.treasury = window.treasury || {};
            window.treasury.comet = window.treasury.comet || {};
            window.treasury.comet.user = "<%=request.getRemoteUser()%>";
          })();
        </script>
      </head>

      <body>
        <input type="hidden" id="cpeIds" name="workflowId" value="${cpeIds}"/>
        <input type="hidden" id="legalEntityIds" name="legalEntityIds" value="${legalEntityIds}"/>
        <input type="hidden" id="agreementTypeIds" name="agreementTypeIds" value="${agreementTypeIds}"/>
        <input type="hidden" id="date" name="date" value="${dateString}"/>
        <input type="hidden" id="includeLiveAgreements" name="includeLiveAgreements" value="${includeLiveAgreements}"/>
        <input type="hidden" id="bookIds" name="bookIds" value="${bookIds}"/>
        <input type="hidden" id="gboTypeIds" name="gboTypeIds" value="${gboTypeIds}"/>
        <input type="hidden" id="pnlSpns" name="pnlSpns" value="${pnlSpns}"/>
        <input type="hidden" id="includeUnmappedBrokerRecords" name="includeUnmappedBrokerRecords" value="${includeUnmappedBrokerRecords}"/>
        <input type="hidden" id="loadPositionDetail" name="loadPositionDetail" value="${loadPositionDetail}"/>
        <input type="hidden" id="stabilityLevel" name="stabilityLevel" value="${stability_level}"/>
        <input type="hidden" id="clientName" name="clientName" value="${client_name}"/>

        <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>

        <div class="layout--flex--row">

          <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>"></arc-header>

          <div class="layout--flex">

            <arc-sidebar id="positionDetailFilterSidebar" header="Search" collapsible="true" class="size--content" style="width:300px">
              <arc-panel-element collapsible="false" showheader="false" dismissible="false">
                <div class="form">
                  <div class="row">
                    <arc-save-settings></arc-save-settings>
                    <button id="summary-search-save" class="button--tertiary size--content">
                      <i class="icon-save" id="save-button" size="content"></i>
                    </button>
                  </div>
                </div>
              </arc-panel-element>

              <arc-panel-element>
                <div class="form">
                  <div class="row">
                    <label class="size--content">Date:</label>
                    <input id="datePicker" type="text"/>
                  </div>
                  <div id="datePickerError" class="validation-error"></div>
                </div>
              </arc-panel-element>

              <arc-panel-element>
                <div class="form">
                  <div class="row">
                    <label class="size--content">SPN:</label>
                    <input type="text" id="spn"></input>
                  </div>
                </div>
              </arc-panel-element>

              <arc-panel-element>

                <div class="form">

                  <div>
                    <label>Legal Entity Family::</label>
                    <arc-combobox multi-select="multi-select" show-tokens="show-tokens" placeholder="Select..." id="entityFamilyFilter"/>
                  </div>

                  <div>
                    <label>Legal Entities:</label>
                    <arc-combobox multi-select="multi-select" show-tokens="show-tokens" placeholder="Select..." id="legalEntityFilter"/>
                  </div>

                  <div>
                    <label>Counterparty Entities:</label>
                    <arc-combobox multi-select="multi-select" show-tokens="show-tokens" placeholder="Select..." id="cpeFilter"/>
                  </div>

                  <div>
                    <label>Agreement Types:</label>
                    <arc-combobox multi-select="multi-select" show-tokens="show-tokens" placeholder="Select..." id="agreementTypeFilter"/>
                  </div>

                  <div>
                    <label>Books:</label>
                    <arc-combobox multi-select="multi-select" show-tokens="show-tokens" placeholder="Select..." id="bookFilter"/>
                  </div>

                  <div>
                    <label>GBO Types:</label>
                    <arc-combobox multi-select="multi-select" show-tokens="show-tokens" placeholder="Select..." id="gboTypeFilter"/>
                  </div>

                </div>

              </arc-panel-element>

              <arc-panel-element>
                <div class="form">
                  <div class="row">
                    <span style="display:inline-block">
                      <input id="includeUnmappedPositions" type="checkbox">
                        <label>Include Unmapped Positions</label>
                      </span>
                    </div>
                  </div>
                </arc-panel-element>

                <div slot="footer">
                  <button class="button--primary" id="searchId" onclick="window.treasury.comet.positionDetail.loadPositionDetailData()">Search</button>
                  <button class="button--text" id="resetId">Reset</button>
                  <button class="button--text" id="copySearchURL">Copy Search URL</button>
                </div>

              </arc-sidebar>

              <div class="layout--flex--row gutter padding--horizontal">
                <div class="message size--content" id="searchMessage" hidden="hidden">
                  Perform search to load results
                </div>
                <div class="size--content form" id="quickLinks" hidden="hidden">
                  <div class="row">
                    <div></div>
                    <span id="info" class="size--content">Found
                      <strong id="noOfRecords"></strong>
                      records for
                      <strong id="searchDate"></strong>
                    </span>
                    <vr class="size--content"></vr>
                    <button id="refreshId" class="button--text size--content">
                      <i class="icon-refresh"></i>
                      Refresh Data</button>
                  </div>
                </div>

                <div class="overlay" hidden="hidden">
                  <div class="loader loader--block form-field--center" style="width: 100px">
                    Loading
                  </div>
                </div>

                <div class="overlay" id="mainLoaderOverlay">
                  <div class="loader loader--block form-field--center" id="mainLoader" style="width: 100px">Loading</div>
                </div>

                <div id="positionDetailGridDiv">
                  <div class="arc-grid__table" id="positionDetailGrid" style="width:100%" hidden/>
                </div>
              </div>
            </div>

          </div>
        </div>

        <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
        <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
        <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
        <script type="text/javascript" src="/treasury/js/util.js"></script>
        <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>
        <script type="text/javascript" src="/treasury/js/filter/multiselect-new.js"></script>
        <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>

        <script type="text/javascript" src="/treasury/js/comet/agreementPosition/positionDetailLanding.js"></script>
        <script type="text/javascript" src="/treasury/js/comet/agreementPosition/positionDetailUtil.js"></script>

        <script type="text/javascript" src="/treasury/js/comet/agreementPosition/positionDetailActions.js"></script>

        <script type="text/javascript" src="/treasury/js/comet/agreementPosition/positionDetailColumnConfig.js"></script>

        <script type="text/javascript" src="/treasury/js/comet/agreementPosition/positionDetailGridOptions.js"></script>

        <script>
          var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
          window.treasury.defaults = window.treasury.defaults || {};
          // copy system property of default 'date' to treasury.defaults package
          if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
            treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
          }
          if (CODEX_PROPERTIES['treasury.lcm.isCrimsonEnabled'] != undefined) {
            treasury.defaults.isCrimsonEnabled = CODEX_PROPERTIES['treasury.lcm.isCrimsonEnabled'].myValue;
          }
        </script>
      </body>
    </html>
