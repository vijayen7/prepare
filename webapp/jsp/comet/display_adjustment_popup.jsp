
<arc-dialog id="adjustmentPopup">
          <div id="adjustmentRequestFields" style="width:650px">
          <!-- arc-panel-element id="adjustmentRequestFields" collapsible="false" style="border: 1px solid #3F4846;"-->
            <div class="form-field--split">
                <label for="">Adjustment Type</label>
                <div class="form-field__input">
                    <arc-dropdown id="adjustmentType"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Legal Entity</label>
                <div class="form-field__input">
                    <arc-dropdown id="legalEntity"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Exposure Counterparty</label>
                <div class="form-field__input">
                    <arc-dropdown id="exposureCounterParty"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Agreement Type</label>
                <div class="form-field__input">
                    <arc-dropdown id="agreementType"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Book</label>
                <div class="form-field__input">
                    <arc-combobox id="book" ></arc-combobox>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Business Unit</label>
                <div class="form-field__input">
                  <arc-combobox id="businessUnit" ></arc-combobox>
                </div>
            </div>
            <div id="marginCallTypeDiv" class="form-field--split">
                <label for="">Margin Call Type</label>
                <div class="form-field__input">
                    <arc-dropdown id="marginCallType"></arc-dropdown>
                </div>
            </div>
            <div id="calculatorDiv" class="form-field--split">
                <label for="">Calculator</label>
                <div class="form-field__input">
                    <input type="text"  readonly="true" id="calculator"></input>
                </div>
            </div>
            <div id="custodianAccountDiv" class="form-field--split">
                <label for="">Custodian Account</label>
                <div class="form-field__input">
                    <input type="text" readonly="true" id="custodianAccount"></input>
                </div>
            </div>
            <div  id="pnlSpnDiv" class="form-field--split">
                <label for="">PNL SPN</label>
                <div class="form-field__input">
                    <input type="text"  readonly="true" id="pnlSpn"></input>
                    </div>
            </div>
            <div class="form-field--split">
                <label for="">Start Date</label>
                <div class="form-field__input">
                    <input type="text" id="startDate" style="display:inline"/>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">End Date</label>
                <div class="form-field__input">
                    <input type="text"  id="endDate" style="display:inline"/>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Reason Code</label>
                <div class="form-field__input">
                    <arc-dropdown id="reasonCode"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Adjustment Amount (<span id="adjustmentReportingCurrency"></span>)</label>
                <div class="form-field__input">
                    <input type="text" id="adjustmentAmount"></input>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Comment</label>
                <input type="text" id="comment"></input>
            </div>
            <input style="display:none" type="text" id="custodianAccountId"></input>
            <!--  /arc-panel-element-->
          </div>
    </arc-dialog>
