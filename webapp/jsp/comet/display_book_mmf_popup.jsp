
<arc-dialog id="bookMmfPopup">
          <div id="bookMmfRequestFields" style="width:650px">
          <!-- arc-panel-element id="adjustmentRequestFields" collapsible="false" style="border: 1px solid #3F4846;"-->
            <div class="form-field--split">
                <label for="">Seg IA Custodian Account</label>
                <div class="form-field__input">
                    <input type="text"  readonly="true" id="segCustodianAccountName"></input>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">MMF SPN</label>
               <div class="form-field__input">
                    <input type="text" id="mmfSpn"></input>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Description</label>
                <div class="form-field__input">
                    <input type="text"  readonly="true" id="description"></input>
                </div>
            </div>
            <div class="form-field--split">
                <label for="">Amount</label>
                <div class="form-field__input">
                    <input type="text"  readonly="true" id="amount"></input>
                </div>
            </div>
          </div>
    </arc-dialog>
