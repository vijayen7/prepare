

    <arc-dialog id="logWirePopup">
    <div>
          <div id="yakWireRequestFields" style="width:650px">
            <div class="form-field--split">
                <label for="">Subject</label>
                <textarea id="subject"></textarea>
            </div>
            <div class="form-field--split">
                <label for="">Subscribers</label>
                <textarea id="subscribers"></textarea>
            </div>
            <div class="form-field--split">
                <label for="">CC</label>
                <input type="text" id="cc">
            </div>
            <div class="form-field--split">
                <label for="">Comments</label>
                <textarea id="comments"></textarea>
            </div>
          </div>

          <arc-panel-element label="Wire Details" collapsible="false" style="border: 1px solid #3F4846;">
            <div class="form-field--split">
                <label>Source Account </label>
                <div class="form-field__input">
                    <arc-dropdown id="sourceAccount"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label>Destination Account </label>
                <div class="form-field__input">
                    <arc-dropdown id="destinationAccount"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label>Source Portfolio </label>
                <div class="form-field__input">
                    <arc-dropdown id="sourcePortfolio"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label>Destination Portfolio </label>
                <div class="form-field__input">
                    <arc-dropdown id="destinationPortfolio"></arc-dropdown>
                </div>
            </div>
            <div class="form-field--split">
                <label>Wire Date </label>
                <div class="form-field__input">
                    <input  id="wireDate" type="text" style="display:inline"/>
                </div>
            </div>
            <div class="form-field--split">
                <label>Value Date </label>
                <div class="form-field__input">
                    <input  id="valueDate" type="text" style="display:inline"/>
                </div>
            </div>
            <div class="form-field--split">
                <label>Wire Amount <span class="wireReportingCurrencyIsoCode"></span> </label>
                <input readonly="true" id="wireAmount" type="text"  />
            </div>
            <div class="form-field--split">
                <label> Final Wire Amount <span class="wireReportingCurrencyIsoCode"></span> </label>
                <input  id="finalWireAmount" type="text" id="finalWireAmount"/>
            </div>
            <div class="form-field--split">
            <div class="form-field__label"></div>
            <div class="form-field__input">
            <input type="checkbox" id="bookMmf" name="wireWrapper.bookMmf"
                       title="Select, to book mmf for this amount"  /> <label>Book MMF for this amount</label>
            <input type="checkbox" id="trackOnlyWire" name="wireWrapper.bookMmf"
                       title="Select, to book mmf for this amount"  /> <label>Track only Wire</label>
            </div>
            </div>
        </arc-panel-element>
    </div>
    </arc-dialog>
