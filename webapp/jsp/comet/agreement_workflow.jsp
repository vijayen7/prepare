<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>

<!DOCTYPE html>
<!-- saved from url=(0041)http://www/~mulani/slickgrid/landing.html -->
<html lang="en" class="win chrome chrome4 webkit webkit5">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8"><title>Dispute Workflow</title>
    <jsp:include page="../arcux/common-arc-imports.jsp"></jsp:include>
    <!-- link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css" /> -->

    <link rel="stylesheet" href="/treasury/css/comet-min.css">
    <link rel="stylesheet" href="/treasury/css/treasury_common.css">
    <link rel="stylesheet" href="/treasury/components/treasury-components.css">

 <script>
        (function(){
            window.treasury = window.treasury || {};
            window.treasury.comet = window.treasury.comet || {};
            window.treasury.comet.user = "<%=request.getRemoteUser()%>";
        })();
    </script>
</head>
<body style="overflow:hidden">
<input type="hidden" id="workflowId" name="workflowId" value="${workflowId}" />
<input type="hidden" id="configName" name="configName" value="${configName}" />
<input type="hidden" id="date" name="date" value="${date}" />
<input type="hidden" id="actionableAgreementId" name="actionableAgreementId" value="${actionableAgreementId}" />
<div id="postCollateralPopup"></div>
<div id="physicalCollateralInventoryLoader" hidden class="overlay" style="z-index: 9100;">
        <div class="loader loader--block form-field--center" style="width: 100px">Loading</div>
    </div>
<div id="physicalCollateralInventory"></div>
<arc-dialog title="Error" id="errorMessage" modal="true"
        style="z-index:9998"></arc-dialog>


    <arc-loading id="agreementWorkflowTopPanelGridLoading" style="display:none">
      <div id="loading-indicator">
          <i id="loading-indicator__icon" class="fa fa-spinner fa-spin"></i>
          <br />
          <label class="loading-indicator__comment">Loading</label>
      </div>
    </arc-loading>
    <%@ include file="display_log_wire.jsp" %>
    <%@ include file="display_adjustment_popup.jsp" %>
    <%@ include file="display_book_mmf_popup.jsp" %>
    <div class="layout--flex--row">
        <!-- HEADER START -->
        <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
        </arc-header>
        <!-- HEADER END -->

        <div class="row gutter padding size--content" id="parentArcLayout" >
            <div class="size--content container--action">
              <div class="form">
                <div class="row">
                            <a class="size--content" href="url" id="cancelId" onclick="event.preventDefault();window.treasury.comet.agreementWorkflowAction.workflowExecute('CANCELLED', 'Workflow has been cancelled', true, null);">Cancel & Go Back</a>
                            <div></div>
                            <span  id="workflowDateInfo" class="size--content" hidden>Workflow for <strong id="searchDate"></strong></span>
                            <vr class="size--content"></vr>
                            <button class="size--content" id="publishId" onclick="window.treasury.comet.agreementWorkflowAction.workflowExecute('PUBLISHED', 'Workflow has been published', true, null);">Publish</button>
                            <button class="size--content" id="saveId" onclick="window.treasury.comet.agreementWorkflowAction.workflowExecute('SAVED', 'Workflow has been saved', true, 'postSaveMethod');">Save & Exit</button>

                </div>
              </div>
            </div>
        </div>

            <hr class="size--content">
            <div class="size--content" id="agreementTopPanelId" type="fill" collapsible="false">
                <div id="agreementWorkflowTopPanelGrid" style="display:none"></div>
                <!-- arc-panel-element  collapsible="false" showheader="false" style="border: 1px solid #3F4846; text-align:center; color:blue;" -->
            </div>
            <arc-panel-element class="size--content" id="agreementWorkflowRightBar" collapsible="true" label="Excess Deficit Summary"  state="collapsed">
                <div class="layout--flex--row">
                    <!-- arc-panel-element class="size--content margin--bottom">
                        <div class="layout--flex">
                            <div>
                                <button id="rightSideBarCallReportId">
                                    <i class="icon-download"></i>  Call Report</button>
                                <button id="downloadBrokerFileId">Download Broker File</button>
                            </div>
                        </div>
                    </arc-panel-element-->
                        <arc-tab-panel id="excessDeficitSummaryPanelId">
                            <arc-tab label="Data Summary">
                                    <div class="lauout--flex fill--width gutter">
                                        <%@ include file="agreement_internal_vs_external.jspf" %>
                                    </div>
                            </arc-tab>
                            <arc-tab label="Additional Summary">
                                    <div class="layout--flex fill--width gutter">
                                        <%@ include file="agreement_additional_detail.jspf" %>
                                    </div>
                            </arc-tab>
                    </arc-panel-element>
                </div>
            </arc-panel-element>
            <!-- div class="layout--flex"-->
                <arc-panel-element label="Detailed Information" state="expanded" collapsible class="layout--flex size--2" id="detailInformation" type="fill">
                    <arc-tab-panel id="positionDataPanelId">
                        <arc-tab label="Cash Collateral"  type="fill">
                        <div class="loader loader--block margin--top" id="CashCollateralLoader" >Loading Data</div>
                          <div class="message size--content margin--top" hidden id="CashCollateralMessage" >No data found</div>
                            <div id="cashCollateralGrid"></div>
                        </arc-tab>
                        <arc-tab label="Securities Collateral"  type="fill">
                            <div class="loader loader--block margin--top" id="PhysicalCollateralLoader" >Loading Data</div>
                              <div class="message size--content margin--top" hidden id="PhysicalCollateralMessage" >No data found</div>
                                <div id="physicalCollateralGrid"></div>
                            </arc-tab>
                        <arc-tab label="Exposure &amp; Requirement"  type="fill">
                          <div class="loader loader--block margin--top" id="ExposureLoader" >Loading Data</div>
                          <div class="message size--content margin--top" hidden id="ExposureMessage" >No data found</div>
                                <div id="exposureRequirementGrid"></div>
                        </arc-tab>
                        <arc-tab label="Usage" type="fill">
                          <div class="loader loader--block margin--top" id="UsageLoader" >Loading Data</div>
                          <div class="message size--content margin--top" hidden id="UsageMessage" >No data found</div>
                            <div id="usageGrid"></div>
                        </arc-tab>
                    </arc-tab-panel>
                </arc-panel-element>
            <!-- /div -->
            <arc-panel-element collapsible="false" label="Adjustments Impacting Dispute Workflow" class="size--1">
                                <div id="adjustmentGrid"></div>
            </arc-panel-element>
            <!-- div id = "agreementWorkflowDetailDiv" class="size--content" hidden></div-->
            </div>
    </div>
  <script>
  WebComponents.waitFor(() => {
        let scriptArrayForPromise = [UIUX.loadjscssfile("/treasury/js/filter/singleselect.js", "js"),
              UIUX.loadjscssfile("/treasury/js/filter/loadfilters.js", "js"),
              UIUX.loadjscssfile("/treasury/js/treasury_common.js", "js"),
              UIUX.loadjscssfile("/treasury/js/arc-grid/arc-grid.min.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/gridConfig/agreementSummaryColumnConfig.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/gridConfig/agreementWorkflowColumnConfig.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementPosition/agreementPositionGridAction.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementPosition/positionDetailUtil.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementWorkflow/agreementWorkflowAction.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementWorkflow/agreementWorkflowGridAction.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementWorkflow/agreementWorkflowDetail.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementSummary/agreementDetail.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementWorkflow/agreementWorkflowDisputeAmount.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementWorkflow/bookMmfTrade.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/wires/agreementWiresAction.js", "js"),
              UIUX.loadjscssfile("/treasury/js/util.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/cometUtil.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementAdjustment/agreementAdjustmentAction.js", "js"),
              UIUX.loadjscssfile("/treasury/js/comet/agreementAdjustment/agreementAdjustmentGridAction.js", "js"),
              UIUX.loadjscssfile("${cdn_base_url}${version}/react/react.development.js", "js"),
              UIUX.loadjscssfile("${cdn_base_url}${version}/react-dom/react-dom.development.js", "js"),
              UIUX.loadjscssfile("/treasury/components/treasury-components.production.min.js", "js")]
        Promise.all(scriptArrayForPromise).then(() => {
              UIUX.loadjscssfile("/treasury/js/comet/agreementWorkflow/agreementWorkflow.js", "js")
              });
    });
  </script>
    <script>
        ArcThemeHandler.enableThemeSelection();
    </script>
    <script>
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
        window.treasury.defaults = window.treasury.defaults || {};

        if (CODEX_PROPERTIES['com.arcesium.treasury.lcm.isPortfolioMandatoryToLogWire'] != undefined) {
          treasury.defaults.isPortfolioMandatoryToLogWire = CODEX_PROPERTIES['com.arcesium.treasury.lcm.isPortfolioMandatoryToLogWire'].myValue;
        }
  </script>
</body>
</html>
