<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>
{
"counterpartyExposureCoverageData" : [
        <s:if test="cpeExposureCoverageDataList != null && cpeExposureCoverageDataList.size > 0">
            <s:iterator value="cpeExposureCoverageDataList" status="outerLoop">
                     {
                         "id" : "<s:property value="cpeFamily.displayName"/>_<s:property value="%{#outerLoop.count}"/>",
                         "cpe_family" : "<s:property value="cpeFamily.displayName"/>",
                         "customer_account_margin_usd" : "<s:property value="customerAccountMarginUsd"/>",
                         "non_customer_account_margin_usd" : "<s:property value="nonCustomerAccountMarginUsd"/>",
                         "segregated_ia_usd" : "<s:property  value="segregatedMarginUsd"/>",
                         "total_margin_usd"  : "<s:property  value="totalMarginUsd"/>",
                         "required_cds_usd"  : "<s:property  value="requiredCdsUsd"/>",
                         "actual_cds_usd"    : "<s:property  value="acutalCdsUsd"/>",
                         "cds_shortfall_usd" : "<s:property  value="cdsShortfallExcessUsd"/>",
                         "cds_shortfall_one_month" : "<s:property  value="cdsShortfallExcessOneMonth"/>",
                         "cds_shortfall_three_month" : "<s:property  value="cdsShortfallExcessThreeMonths"/>",
                         "cds_shortfall_six_month" : "<s:property  value="cdsShortfallExcessSixMonths"/>",
                         "one_year_spread"       : "<s:property  value="oneYearSpread"/>",
                         "atm_implied_vol"     : "0"
                    }
                <s:if test="!#outerLoop.last">,</s:if>
            </s:iterator>
        </s:if>
        ]
}