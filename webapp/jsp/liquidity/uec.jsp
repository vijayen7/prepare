<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
    <title>Unencumbered Cash</title>
    <jsp:include page="../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
    <style>
      .comment-wrap {
        overflow-wrap: break-word;
      }
    </style>
    
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css"/>
      
          <script>
            function switchJqueryCSS() {
              var href = document.querySelector('link#jquery-ui-css-arcux').href
              if (ArcThemeHandler.getCurrentTheme() == 'dark') {
                if (href.indexOf('light') > -1) {
                  document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
                }
              } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
                if (href.indexOf('light') === -1) {
                  document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
                }
              }
            }
            switchJqueryCSS();
            ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
          </script>
          <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css" />
        </head>
        <body>
        <input type="hidden" id="stabilityLevel" name="stabilityLevel" value="${stability_level}"/>
        <input type="hidden" id="clientName" name="clientName" value="${client_name}"/>

          <arc-loading id="uecLoading" style="display:none">
            <div id="loading-indicator">
              <i id="loading-indicator__icon" class="fa fa-spinner fa-spin"></i>
              <br />
              <label class="loading-indicator__comment">Loading</label>
          </div>
          </arc-loading>
          <%@ include file="/jsp/common/workflow/failed-workflow-template.jspf" %>
          <%@ include file="/jsp/common/workflow/locked-workflow-template.jspf" %>
          <%@ include file="/jsp/common/workflow/resume-workflow-template.jspf" %>
          <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>
          <div class="layout--flex--row">
            <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
              <div slot="application-menu" class="application-menu-toggle-view" id="start-workflow">
                <text style="height:30px" id="workflowMessage" hidden></text>
                <a id="startWorkflow" >Start Workflow</a>
                <a id="signOffWorkflow" hidden= "true">Sign Off</a>
              </div>
            </arc-header>
            <div id="parentArcLayout" class="layout--flex">
              <arc-sidebar id="uecFilterSidebar" header="Search" collapsible="true" class="size--content" style="width:300px">
                <%@ include file="/jsp/liquidity/uec-filters.jspf" %>
                <arc-panel-element showheader="false" type="">
                  <div style="text-align: center">
                    <button class="button--primary" id="uecSearch">Search</button>
                    <button class="button--text" id="resetSearch">Reset</button><br/>
                  </div>
                </arc-panel-element>
              </arc-sidebar>

              <div id="uecContent" class="layout--flex--row gutter padding--horizontal">
                  <div class="message size--content" id="uecMessage">
                    Perform search to load results
                  </div>
                  <div id="uecGridsDiv">
                  <%@ include file="/jsp/liquidity/uec-tabs.jspf" %>
                  </div>
              </div>


              </div>
            </div>


          <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
          <script type="text/javascript" src="/treasury/js/filter/singleselect-min.js"></script>
          <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
          <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
          <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
          <script type="text/javascript" src="/treasury/js/util-min.js"></script>
          <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>

          <script type="text/javascript" src="/treasury/js/liquidity/workflowUtils.js"></script>
          <!-- Uec -->
          <script type="text/javascript" src="/treasury/js/liquidity/uec/uecUtil.js"></script>
          <script type="text/javascript" src="/treasury/js/liquidity/uec/uecActions.js"></script>
          <script type="text/javascript" src="/treasury/js/liquidity/uec/uecLanding.js"></script>
          <script type="text/javascript" src="/treasury/js/liquidity/uec/uecColumnsConfig.js"></script>
          <script type="text/javascript" src="/treasury/js/liquidity/uec/uecGridOptions.js"></script>

          <script>
            var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
            window.treasury.defaults = window.treasury.defaults || {};
            // copy system property of default 'date' to treasury.defaults package
            if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
              treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
            }
          </script>
        </body>
      </html>
