<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
    <title>UEC Workflow</title>
    <jsp:include page="../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
    <style>
      .comment-wrap {
        overflow-wrap: break-word;
      }
    </style>
    
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css"/>
      
        <script>
          function switchJqueryCSS() {
            var href = document.querySelector('link#jquery-ui-css-arcux').href
            if (ArcThemeHandler.getCurrentTheme() == 'dark') {
              if (href.indexOf('light') > -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
              }
            } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
              if (href.indexOf('light') === -1) {
                document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
              }
            }
          }
          switchJqueryCSS();
          ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
        </script>
        <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css"/>
      </head>
      <body>
        <input type="hidden" id="stabilityLevel" name="stabilityLevel" value="${stability_level}"/>
        <input type="hidden" id="clientName" name="clientName" value="${client_name}"/>
        <arc-loading id="uecLoading" style="display:none">
          <div id="loading-indicator">
            <i id="loading-indicator__icon" class="fa fa-spinner fa-spin"></i>
            <br />
            <label class="loading-indicator__comment">Loading</label>
        </div>
        </arc-loading>
        <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>
        <arc-dialog id="commentPopup" width="100" modal="true" height="100">
          <div id="commentRequestFields" style="width:650px">
            <div class="form-field--split">
              <label for="">Comment</label>
              <div class="form-field__input">
                <textarea id="comment" rows="10" cols="60"></textarea>
              </div>
            </div>
          </div>
        </arc-dialog>
        <arc-layout type="row"  id="parentArcLayout" size="1" style="overflow:hidden;">
          <arc-header class="size--content" modern-themes-enabled id="uecHeader" user="<%=request.getRemoteUser()%>" > </arc-header>
          <div style="height:30px" id="uecMessage" hidden></div>
          <arc-panel-element id="workflowOperations" showheader="false" size="content">
            <button class="button--primary" id="saveExitWorkflow">Save &amp; Exit</button>
            <button class="button--primary" id="publishWorkflow">Publish</button>
            <button class="button--primary" id="cancelWorkflow">Cancel</button>
            <div id="workflowMessage" class="aln-rt"></div>
          </arc-panel-element>
          <%@ include file="/jsp/liquidity/uec-tabs.jspf" %>
        </arc-layout>
        <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
        <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
        <script type="text/javascript" src="/treasury/js/util-min.js"></script>
        <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>

        <script type="text/javascript" src="/treasury/js/filter/multiselect-min.js"></script>
        <script type="text/javascript" src="/treasury/js/filter/singleselect-min.js"></script>

        <script type="text/javascript" src="/treasury/js/liquidity/workflowUtils.js"></script>
        <script type="text/javascript" src="/treasury/js/liquidity/uec/uecWorkflow.js"></script>
        <script type="text/javascript" src="/treasury/js/liquidity/uec/uecActions.js"></script>
        <script type="text/javascript" src="/treasury/js/liquidity/uec/uecUtil.js"></script>
        <script type="text/javascript" src="/treasury/js/liquidity/uec/uecColumnsConfig.js"></script>
        <script type="text/javascript" src="/treasury/js/liquidity/uec/uecGridOptions.js"></script>

        <script type="text/javascript" src="/treasury/js/comet/agreementAdjustment/agreementAdjustmentAction.js"></script>
        <script type="text/javascript" src="/treasury/js/comet/agreementWorkflow/agreementWorkflowAction.js"></script>
        <script>
          var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
          window.treasury.defaults = window.treasury.defaults || {};
          // copy system property of default 'date' to treasury.defaults package
          if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
            treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
          }
        </script>
      </body>
    </html>
