<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib" %>

<!DOCTYPE html>
<!-- saved from url=(0041)http://www/~mulani/slickgrid/landing.html -->
<html lang="en" class="win chrome chrome4 webkit webkit5">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Borrow Financing Report</title>
    <jsp:include page="../../arcux/common-arc-imports.jsp"></jsp:include>
    <script>
         function switchJqueryCSS() {
           var href = document.querySelector('link#jquery-ui-css-arcux').href
           if (ArcThemeHandler.getCurrentTheme() == 'dark') {
            if (href.indexOf('light') > -1) {
              document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
            }
           } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
            if (href.indexOf('light') === -1) {
              document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
            }
          }
        }
        switchJqueryCSS();
        ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
    </script>

    <link rel="stylesheet" href="/treasury/css/treasury_common-min.css">
    <link rel="stylesheet" href="/treasury/css/seclend/borrow_financing-min.css">

    <script>
        (function(){
            window.treasury = window.treasury || {};
            window.treasury.borrowFinancing = window.treasury.borrowFinancing || {};
            window.treasury.borrowFinancing.user = "<%=request.getRemoteUser()%>";
        })();
    </script>
</head>

<body style="overflow:hidden">
    <arc-banner size="content"></arc-banner>
    <arc-dialog></arc-dialog>
    <arc-loading id="borrowFinancingLoading" style="display:none">
      <div id="loading-indicator">
        <i id="loading-indicator__icon" class="fa fa-spinner fa-spin"></i>
        <br />
        <label class="loading-indicator__comment">Loading</label>
      </div>
    </arc-loading>

    <arc-layout type="row" gutter="0px" id="landingPageDiv" style="display:none">
        <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
        </arc-header>

        <arc-layout type="column" gutter="20px" id="parentArcLayout" size="1" style="overflow:hidden;">
            <arc-sidebar id="borrowFinancingSidebar" header="Borrow Financing filters" size="300px" collapsible="false" type="fill">
                <arc-panel-element id="borrowFinancingFilters" showheader="false">
                    <label>Date:</label>
                    <div><input id="borrowFinancingDate" type="text" style="display:inline"/></div>
                    <div id="borrowFinancingDateError" class="critical"></div>
                    <br/>

                    <label>Legal Entity:</label><br/>
                    <arc-combobox-multi id="borrowFinancingLegalEntityFilter"></arc-combobox-multi>
                    <br/>

                    <label>Security Type:</label><br/>
                    <arc-combobox-multi id="borrowFinancingSecurityTypeFilter"></arc-combobox-multi>

                </arc-panel-element>
                <arc-panel-element showheader="false">
                    <div style="text-align: center">
                            <div id="borrowFinancingFilterValidation" class="arc-message--critical" style="display:none"></div>
                            <button id="searchBorrowFinancing">Search</button>
                            <button class="button--text" id="copyReportUrlBtn">Copy Report URL</button>
                    </div>
                </arc-panel-element>
            </arc-sidebar>

            <!-- Borrow Financing Grid -->
            <div id="borrowFinancingGridView" size="1">
                <div id="borrowFinancingGridInfo" class="arc-message" style="display:none">
                </div>
                <!-- <div id="saveSettingsContainer">
                    <button id="saveGridView" class="save-settings-btn" style="display:none">Save View</button>
                    <arc-save-settings style="display:none" class="save-settings-select"></arc-save-settings>
                </div> -->
                <div class="arc-grid__table" id="borrowFinancingGrid" style="width:100%">
                </div>
            </div>

        </arc-layout>
    </arc-layout>

    <template id="borrowFinancingDetailTemplate">
        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Legal Entity</th>
                        <th>Counterparty Entity</th>
                        <th>Custodian Account</th>
                        <th>Borrow Rate (bps)</th>
                        <th>Previous Day's Borrow Rate (bps)</th>
                        <th>Notional (USD)</th>
                    </tr>
                </thead>
            </table>
        </div>
    </template>

    <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
    <script type="text/javascript" src="/treasury/js/util-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/multiselect-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
    <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
    <script type="text/javascript" src="/treasury/js/seclend/borrowFinancing/borrowFinancingDetail-min.js"></script>
    <script type="text/javascript" src="/treasury/js/seclend/borrowFinancing/gridConfig-min.js"></script>
    <script type="text/javascript" src="/treasury/js/seclend/borrowFinancing/gridActions-min.js"></script>
    <script type="text/javascript" src="/treasury/js/seclend/borrowFinancing/landing-min.js"></script>

    <script>
        ArcThemeHandler.addThemeChangeListener(function() {
            var href = document.querySelector('link#jquery-ui-css').href;

            if (ArcThemeHandler.getCurrentTheme() == 'dark') {
                document.querySelector('link#jquery-ui-css').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
            } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
                if (href.indexOf('light') === -1) {
                    document.querySelector('link#jquery-ui-css').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
                }
            }
        });

        ArcThemeHandler.enableThemeSelection();

        // Default properties from codex.
        var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal' /> ;

        window.treasury.defaults = window.treasury.defaults || {};

        // copy system property of default 'date' to treasury.defaults package
        if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined && CODEX_PROPERTIES["treasury.portal.defaults.date"].myValue) {
            treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
        }

        window.addEventListener("WebComponentsReady", function() {
            treasury.borrowFinancing.landing.initialize();
        });
    </script>
</body>

</html>
