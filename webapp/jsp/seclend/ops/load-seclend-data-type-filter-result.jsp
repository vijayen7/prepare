<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>

{
 "seclendDataTypes" :
    [

            <s:iterator value="@com.arcesium.treasury.model.seclend.ops.SeclendDataType@values()" status="state" >
              [<s:property value="id"/>,"<s:property value="name"/> [<s:property value="id"/>]"]
              <s:if test="!#state.last">,</s:if>
            </s:iterator>

    ]

}
