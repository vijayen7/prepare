<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>
{

    "ownershipEntitiesSortedOrder" :
      [
        <s:if test="ownershipEntitiesSortedOrder != null && ownershipEntitiesSortedOrder.size > 0">
            <s:iterator value="ownershipEntitiesSortedOrder" status="outerLoop">
                "<s:property />"
            <s:if test="!#outerLoop.last">,</s:if>
            </s:iterator>
        </s:if>
      ],
    "totalCounterpartyExposure" :
    [
    <s:if test="cpeOwnershipExposureDataList != null && cpeOwnershipExposureDataList.size > 0">
        <s:iterator value="cpeOwnershipExposureDataList" status="outerLoop">
        {
         "id" : "<s:property value="name"/>",
         "name" : "<s:property value="name"/>",

         <s:iterator value="ownershipEquityMap" status="mapStatus">

            "<s:property value="key"/>_equity"  : "<s:property value="value[@deshaw.treasury.web.model.OwnershipExposureData$EquitySet@equity]"/>",
            <s:if test="%{value[@deshaw.treasury.web.model.OwnershipExposureData$EquitySet@equity] / ownershipTotalEquityMap[key] != null}">
                "<s:property value="key"/>_percent" : "<s:text name="percent"><s:param value="%{value[@deshaw.treasury.web.model.OwnershipExposureData$EquitySet@equity] / ownershipTotalEquityMap[key]}"/></s:text>",
            </s:if>
            <s:else>
                "<s:property value="key"/>_percent" : "0.00%",
            </s:else>
         </s:iterator>

         "ownership_entities" : [
         <s:iterator value="ownershipEquityMap" status="ownershipsLoop">
             "<s:property value="key"/>"<s:if test="!#ownershipsLoop.last">,</s:if>
         </s:iterator>
         ]

        <s:if test="childExposureDataList != null && childExposureDataList.size > 0">
        ,
            "children" : [
                <s:iterator value="childExposureDataList " status="childrenLoop">
                {
                 "id" : "<s:property value="name"/>",
                 "name" : "<s:property value="name"/>",

                  <s:iterator value="ownershipEquityMap" status="mapStatus">
                      "<s:property value="key"/>_equity"  : "<s:property value="value[@deshaw.treasury.web.model.OwnershipExposureData$EquitySet@equity]"/>",
                      <s:if test="%{value[@deshaw.treasury.web.model.OwnershipExposureData$EquitySet@equity] / ownershipTotalEquityMap[key] != null}">
                          "<s:property value="key"/>_percent" : "<s:text name="percent"><s:param value="%{value[@deshaw.treasury.web.model.OwnershipExposureData$EquitySet@equity] / ownershipTotalEquityMap[key]}"/></s:text>"
                      </s:if>  <s:else>
                          "<s:property value="key"/>_percent" : "0.00%"
                      </s:else><s:if test="!#mapStatus.last">,</s:if>



                  </s:iterator>
                }
                <s:if test="!#childrenLoop.last">,</s:if>
                </s:iterator>
            ]
        </s:if>
        }
        <s:if test="!#outerLoop.last">,</s:if>
        </s:iterator>
    </s:if>
    ]
}
