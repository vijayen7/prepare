<%@page import="com.arcesium.codex.Codex"%>
<%@page import="arcesium.ux.commons.utils.CdnVersionResolver"%>
<%@page import="deshaw.auth.authentication.AuthenticationUtils"%>

<% request.setAttribute("cdn_base_url", Codex.getLibraryConfiguration("landing-app").getString("cdn_base_url"));
request.setAttribute("version", CdnVersionResolver.resolve(request, response,AuthenticationUtils.getUserLogin(request.getRemoteUser()),"v1"));
request.setAttribute("stability_level", Codex.getLibraryConfiguration("landing-app").getString("codex.stability_level"));
request.setAttribute("client_name", Codex.getLibraryConfiguration("landing-app").getString("codex.client_name"));%>

<script type="text/javascript" arc-components arc-jquery arc-grid src="${cdn_base_url}${version}/components/dist/js/arc-head-imports.js"></script>
<link rel="stylesheet" href="${cdn_base_url}${version}/css/framework.css"/>
<link rel="stylesheet" type="text/css" href="/treasury/css/loading.css"/>
<script src="/treasury/js/loading/loading.js"></script>
