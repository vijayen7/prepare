<%@page import="deshaw.auth.authentication.AuthenticationUtils"%>
<template id="arcux-head-scripts">
	<link id="cssOverride" rel="stylesheet" href="${cdn_base_url}${cdn_version}/css/app_overrides/treasury/treasury.css" />
</template>

<template id="arcux-header">
	<arc-header user="<%=AuthenticationUtils.getUserLogin(request.getRemoteUser()) %>" page-title="<s:property value="dashboard.name" />" has-theme-selector modern-themes-enabled>
		<div slot="application-menu"></div>
	</arc-header>
</template>
