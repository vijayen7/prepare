<%@page import="com.arcesium.codex.Codex"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
    request.setAttribute("cdn_base_url",
					Codex.getLibraryConfiguration("landing-app").getString("cdn_base_url"));
			request.setAttribute("cdn_version", Codex.getLibraryConfiguration("landing-app").getString("version"));
%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">

  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
    <title>Arcesium::Interest Onboarding</title>

    <script src="${cdn_base_url}${cdn_version}/components/dist/js/arc-head-imports.js"></script>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
    <script src="${cdn_base_url}${cdn_version}/vendor/jquery-ui/jquery-ui.js"></script>

    <link rel="import" href="/treasury/components/loading/loading.html">
      <link rel="import" href="${cdn_base_url}${cdn_version}/components/dist/arc-components.html"/>
      <link rel="stylesheet" href="${cdn_base_url}${cdn_version}/css/framework.css"/>
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6708954/800486/css/fonts.css"/>
      <link rel="stylesheet" href="${cdn_base_url}${cdn_version}/css/arc-grid.css "/>
      <link rel="stylesheet" href="${cdn_base_url}${cdn_version}/vendor/jquery-ui/jquery-ui.min.css">
        <link rel="stylesheet" href="${cdn_base_url}${cdn_version}/vendor/font-awesome/css/font-awesome.min.css">
          <!-- Following three imports will be removed once arc date components are available -->
          <script src="/static/compressed/js/lib/jquery/jquery.daterangepicker-min.js"></script>
          <link rel="stylesheet" type="text/css" href="/static/compressed/css/jquery/ui.daterangepicker-min.css"/>

          <style>
            [class^="tob-action-panel"] {
              padding: 5px 0;
            }

            [class^="tob-action-panel"] > * {
              margin: 0 5px;
            }

            .tob-action-panel--top {
              margin-bottom: 10px;
            }

            .tob-action-panel--bottom {
              margin-top: 10px;
            }

            .left {
              float: left;
            }

            .right {
              float: right;
            }

            arc-input-dropdown {
              overflow: visible;
            }

            arc-layout[type="column"] > *[class^="form-field"] {
              margin-top: 0;
            }

            .form-field--inline.left {
              margin-top: 0;
              width: auto;
            }

            .selected {
              background-color: #FFB;
            }

            .row--critical .slick-cell {
              background: #8E4C4C !important;
            }

            .row--clear .slick-cell {
              background: #006600 !important;
            }

          </style>
        </head>

        <body>
          <arc-loading id="interestLoading" style="display:none"></arc-loading>
          <arc-dialog title="Error" id="errorMessage" modal="true" style="z-index:9998"></arc-dialog>
          <arc-layout type="row">
            <arc-header size="content" modern-themes-enabled user="<%=request.getRemoteUser()%>"></arc-header>
            <arc-layout type="column" gutter="20px" id="parentArcLayout" size="1" style="overflow:hidden;">
              <arc-sidebar id="calculatorFilterSidebar" header="Filter &amp; Search" size="300px" collapsible="true">
                <arc-panel-element showheader="false">
                  <div class="form-field">
                    <label for="">Legal Entity</label>
                    <arc-combobox-multi id="legalEntityCalculatorFilter"></arc-combobox-multi>
                  </div>
                  <div class="form-field">
                    <label for="">Counterparty Entity</label>
                    <arc-combobox-multi id="cpeEntityCalculatorFilter"></arc-combobox-multi>
                  </div>
                  <div class="form-field">
                    <label for="">Agreement Type</label>
                    <arc-combobox-multi id="agreementTypeCalculatorFilter"></arc-combobox-multi>
                  </div>
                  <div class="form-field" style="overflow: visible">
                    <label for="">Netting Groups</label>
                    <arc-combobox-multi id="nettingGroupCalculatorFilter"></arc-combobox-multi>
                  </div>
                  <div class="form-field" style="overflow: visible">
                    <label for="">Currencies</label>
                    <arc-combobox-multi id="currencyCalculatorFilter"></arc-combobox-multi>
                  </div>
                  <div class="form-field">
                    <label>Date</label>
                    <div class="form-field__input">
                      <input type="text" id="datePicker" style="display: inline;"/>
                      <div id="datePickerError" class="validation-error"></div>
                    </div>
                  </div>

                </arc-panel-element>
                <arc-panel-element showheader="false" type="">
                  <div style="text-align: right">
                    <button class="button--primary" id="resetSearch">Reset</button>
                    <button class="button--primary" id="calculatorSearch">Search</button>
                  </div>
                </arc-panel-element>
              </arc-sidebar>
              <arc-tab-panel id="interestTabs" size="7">
              <arc-tab header="Netting Groups">
                <%@ include
		file="/jsp/interest/add-netting-group.jspf"%>
              </arc-tab>
              <arc-tab header="Agreement Terms">
                <%@ include
		file="/jsp/interest/agreement-terms.jspf"%>
              </arc-tab>
              <!--    <arc-tab header="Allocators"></arc-tab> -->
              <!--    <arc-tab header="Bundle, BU & Account Mapping"></arc-tab> -->
            </arc-tab-panel>
            <arc-sidebar id="financingTermSidebar" header="Full Term" size="300px" collapsible="true" class="right" position="right" state="collapsed" style="display:none">
              <arc-panel-element id="financingTermsFixed" label="Fixed Financing Terms" collapsible="false"></arc-panel-element>
              <arc-panel-element id="financingTermsAttr" label="Financing Term Attributes" collapsible="false"></arc-panel-element>
            </arc-sidebar>
          </arc-layout>
        </arc-layout>
        <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/interestUtil.js"></script>
        <script type="text/javascript" src="/treasury/js/filter/multiselect-min.js"></script>
        <script type="text/javascript" src="/treasury/js/filter/singleselect.js"></script>
        <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
        <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
         <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>

        <script type="text/javascript" src="/treasury/js/util-min.js"></script>

        <script type="text/javascript" src="/treasury/js/interest/nettinggroup/nettingGroupLanding-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/nettinggroup/nettingGroupActions-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/nettinggroup/nettingGrpCustAccMappingColumnsConfig-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/nettinggroup/nettingGrpCustAccMappingGridOptions-min.js"></script>

				<script type="text/javascript" src="/treasury/js/interest/agreementterms/agreementTermsLanding-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/agreementterms/agreementTermsActions-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/agreementterms/agreementTermsGridOptions-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/agreementterms/agreementTermsColumnsConfig-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/agreementterms/bandwidthGroupActions-min.js"></script>

        <script type="text/javascript" src="/treasury/js/interest/interestLanding-min.js"></script>

        <script type="text/javascript" src="/treasury/js/interest/assetclass/assetClassActions-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/assetclass/assetClassLanding-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/assetclass/assetClassColumnsConfig-min.js"></script>
        <script type="text/javascript" src="/treasury/js/interest/assetclass/assetClassGridOptions-min.js"></script>

        <script>
          var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;

          window.treasury.defaults = window.treasury.defaults || {};

          // copy system property of default 'date' to treasury.defaults package
          if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
            treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
          }
        </script>

      </body>
    </html>
