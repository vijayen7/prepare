<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">

  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Arcesium::Interest Onboarding</title>
    <jsp:include page="../arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
    <link rel="stylesheet" href="/treasury/css/interest/onboarding.css"/>
    <link rel="stylesheet" type="text/css" href="/treasury/css/interest/toastr.css"/>
    <script>
      function switchJqueryCSS() {
        var href = document.querySelector('link#jquery-ui-css-arcux').href
        if (ArcThemeHandler.getCurrentTheme() == 'dark') {
          if (href.indexOf('light') > -1) {
            document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
          }
        } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
          if (href.indexOf('light') === -1) {
            document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
          }
        }
      }
      switchJqueryCSS();
      ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
    </script>
  </head>

  <body>
    <arc-dialog title="Error" id="messageContainer" style="z-index:9998!important"></arc-dialog>
    <arc-layout type="row">
      <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>">
        <div slot="application-menu" class="application-menu-toggle-view" id="interestMenu">
          <a href="#" id="calculatorGroup">Calculator Group</a>
          <a href="#" id="financingAgreementTerms">Financing Agreement Terms</a>
          <a href="#" id="assetClassGroup">Asset Class Group</a>
          <a href="#" id="reportingCurrency">Reporting Currency</a>
          <a href="#" id="accrualPosting">Accrual Posting</a>
          <a href="#" id="nettingGroup">Netting Group Mapping</a>
        </div>
      </arc-header>
      <arc-layout type="column" id="parentArcLayout" size="2" style="overflow:hidden;">
        <%@ include
file="/jsp/interest/common-filter.jspf"%>
        <arc-layout type="row">
          <div>
            <button id="commonAddButton" class="topBarButton ">Add</button>
            <button id="bulkUpdateNettingGroupMappingButton" class="topBarButton" style="display:none">Bulk Update Mapping</button>
            <button id="addNettingGroupButton" class="topBarButton" style="display:none">Add New Netting Group</button>
            <button id="addBandwidthGroupButton" class="topBarButton" style="display:none">Add Bandwidth Group</button>
            <button id="bulkInsertAgreementTermsButton" class="topBarButton" style="display:none">Bulk Insert Agreement Terms</button>
            <button id="bulkInsertNettingGroupButton" class="topBarButton" style="display:none">Bulk Insert Netting Group</button>
            <br/>
            <div id="mainArea"></div>
            <div class="overlay" id="mainLoaderOverlay">
              <div class="loader loader--block form-field--center" id="mainLoader" style="width:100px">Loading</div>
            </div>
          </div>
        </arc-layout>
      </arc-layout>
    </arc-layout>
    <script type="text/javascript" src="/treasury/js/filter/loadfilters-min.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/interestUtil.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/multiselect-min.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/singleselect.js"></script>
    <script type="text/javascript" src="/treasury/js/filter/filtergroup-min.js"></script>
    <script type="text/javascript" src="/treasury/js/treasury-min.js"></script>
    <script type="text/javascript" src="/treasury/js/arc-grid/arc-grid.min.js"></script>
    <script type="text/javascript" src="/treasury/js/util-min.js"></script>
    <script type="text/javascript" src="/treasury/js/treasury_common-min.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/landing.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/common/ActionHelper.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/common/GridOptionHelper.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/financingAgreementTerms/action.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/financingAgreementTerms/columnsConfig.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/financingAgreementTerms/landing.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/financingAgreementTerms/gridOptions.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/financingAgreementTerms/bandwidthGroupActions.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/assetclass/assetClassActions.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/assetclass/assetClassColumnsConfig.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/assetclass/assetClassGridOptions.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/assetclass/assetClassLanding.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/calculatorGroupRuleSystem/action.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/calculatorGroupRuleSystem/columnsConfig.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/calculatorGroupRuleSystem/calculatorGroupGridOptions.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/calculatorGroupRuleSystem/landing.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/reportingCurrencyRuleSystem/action.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/reportingCurrencyRuleSystem/columnsConfig.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/reportingCurrencyRuleSystem/landing.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/methodologyAccrualPostingRuleSystem/action.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/methodologyAccrualPostingRuleSystem/columnsConfig.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/methodologyAccrualPostingRuleSystem/landing.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/nettingGroupCAMap/action.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/nettingGroupCAMap/columnsConfig.js"></script>
    <script type="text/javascript" src="/treasury/js/interest/nettingGroupCAMap/landing.js"></script>

    <script type="text/javascript" src="/treasury/js/interest/toastr.js"></script>

    <script>
      var CODEX_PROPERTIES = <codex:propertySet category='treasury.portal'/>;
      window.treasury.defaults = window.treasury.defaults || {};
      if (CODEX_PROPERTIES['treasury.portal.defaults.date'] != undefined) {
        treasury.defaults.date = CODEX_PROPERTIES['treasury.portal.defaults.date'].myValue;
      }
    </script>
  </body>
</html>
