<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="application/json" %>
{
   "exposureDetailData" : [
   <s:if test="cpeFamilyExposureDetailList != null && cpeFamilyExposureDetailList.size > 0">
     <s:iterator value="cpeFamilyExposureDetailList" status="outerLoop">
     {
         "id" : "<s:property value="cpeFamily.name"/>_<s:property value="%{#outerLoop.count}"/>",
         "cpe_family" : "<s:property value="cpeFamily.name"/> Total",
		 "usage_comet" : "<s:property value="cometUsageUsd"/>",
         "usage_utp" : {"value" : "<s:property value="utpUsageUsd"/>",
                       "params" : "<s:property value="linkParameterValues"/>"},
         "equity" : "<s:property value="equityUsd"/>",
         "excess_deficit" : "<s:property value="excessDeficitUsd"/>",
         "valuation_difference" : "<s:property value="valuationDifferenceUsd"/>",
         "segregated_margin" : "<s:property value="segregatedMarginUsd"/>",
         "total_usage" : "<s:property value = "totalUsage"/> ",
         "reg_cpe_margin" : "<s:property value="regCpeMarginUsd"/>",
         "reg_cpe_equity" : "<s:property value="regCpeEquityUsd"/>",
         "subtotalField" : "<s:property value="%{#parameters.subtotalField}" />",
         "children" : [
         <%-- subtotalField query parameter structure is Map<String, String[]> and not Map<String, String>--%>
         <s:if test='#parameters.subtotalField[0] == "1"'>
             <s:if test="descoEntityExposureDetailList != null && descoEntityExposureDetailList.size > 0">
               <s:iterator value="descoEntityExposureDetailList" status="descoEntityLoop">
               {
                   "id" : "<s:property value="cpeFamily.name"/>_<s:property value="descoEntity.name"/>_<s:property value="%{#outerLoop.count}"/>",
       

                   "desco_entity" : "<s:property value="descoEntity.name"/> Total",
				   "usage_comet" : "<s:property value="cometUsageUsd"/>",
                   "usage_utp" : {"value" : "<s:property value="utpUsageUsd"/>",
                                 "params" : "<s:property value="linkParameterValues"/>"},
                   "equity" : "<s:property value="equityUsd"/>",
                   "excess_deficit" : "<s:property value="excessDeficitUsd"/>",
                   "valuation_difference" : "<s:property value="valuationDifferenceUsd"/>",
                   "segregated_margin" : "<s:property value="segregatedMarginUsd"/>",                   
                   "total_usage" : "<s:property value = "totalUsage"/> ",
                   "reg_cpe_margin" : "<s:property value="regCpeMarginUsd"/>",
                   "reg_cpe_equity" : "<s:property value="regCpeEquityUsd"/>",
                   "children" : [
                   <s:if test="agreementExposureDataList != null && agreementExposureDataList.size > 0">
                     <s:iterator value="agreementExposureDataList" status="innerLoop">
                     {
                         "id" : "<s:property value="agreement.counterpartyEntity.name"/>_<s:property value="agreement.agreementType.abbrev"/>_<s:property value="agreement.DESCOEntity.name"/>_<s:property value="%{#outerLoop.count}"/>",
                         "cpe" : "<s:property value="agreement.counterpartyEntity.name"/>",
                         "agreement_type" : "<s:property value="agreement.agreementType.abbrev"/>",
                         "desco_entity" : "<s:property value="agreement.DESCOEntity.name"/>",
					     "usage_comet" : "<s:property value="cometUsageUsd"/>",
	                     "usage_utp" : {"value" : "<s:property value="utpUsageUsd"/>",
	                                   "params" : "<s:property value="linkParameterValues"/>"},
                         "equity" : "<s:property value="equityUsd"/>",
                         "excess_deficit" : "<s:property value="excessDeficitUsd"/>",
                         "valuation_difference" : "<s:property value="valuationDifferenceUsd"/>",
                         "segregated_margin" : "<s:property value="segregatedMarginUsd"/>",
                         "total_usage" : "<s:property value = "totalUsage"/> ",
                         "reg_cpe_margin" : "<s:property value="regCpeMarginUsd"/>",
                         "reg_cpe_equity" : "<s:property value="regCpeEquityUsd"/>"
                     }<s:if test="!#innerLoop.last">,</s:if>
                     </s:iterator>
                   </s:if>
                   ]
               }<s:if test="!#descoEntityLoop.last">,</s:if>
               </s:iterator>
             </s:if>
         </s:if>
         <s:else>
             <s:if test="agreementTypeExposureDetailList != null && agreementTypeExposureDetailList.size > 0">
               <s:iterator value="agreementTypeExposureDetailList" status="agmtTypeLoop">
               {
                   "id" : "<s:property value="cpeFamily.name"/>_<s:property value="agreementType.abbrev"/>_<s:property value="%{#outerLoop.count}"/>",
                   "agreement_type" : "<s:property value="agreementType.abbrev"/> Total",
				   "usage_comet" : "<s:property value="cometUsageUsd"/>",
                   "usage_utp" : {"value" : "<s:property value="utpUsageUsd"/>",
                                 "params" : "<s:property value="linkParameterValues"/>"},
                   "equity" : "<s:property value="equityUsd"/>",
                   "excess_deficit" : "<s:property value="excessDeficitUsd"/>",
                   "valuation_difference" : "<s:property value="valuationDifferenceUsd"/>",
                   "segregated_margin" : "<s:property value="segregatedMarginUsd"/>",
                   "total_usage" : "<s:property value = "totalUsage"/> ",
                   "reg_cpe_margin" : "<s:property value="regCpeMarginUsd"/>",
                   "reg_cpe_equity" : "<s:property value="regCpeEquityUsd"/>",
                   "children" : [
                   <s:if test="agreementExposureDataList != null && agreementExposureDataList.size > 0">
                     <s:iterator value="agreementExposureDataList" status="innerLoop">
                     {
                         "id" : "<s:property value="agreement.counterpartyEntity.name"/>_<s:property value="agreement.agreementType.abbrev"/>_<s:property value="agreement.DESCOEntity.name"/>_<s:property value="%{#outerLoop.count}"/>",
                         "cpe" : "<s:property value="agreement.counterpartyEntity.name"/>",
                         "agreement_type" : "<s:property value="agreement.agreementType.abbrev"/>",
                         "desco_entity" : "<s:property value="agreement.DESCOEntity.name"/>",
						 "usage_comet" : "<s:property value="cometUsageUsd"/>",
		                 "usage_utp" : {"value" : "<s:property value="utpUsageUsd"/>",
		                               "params" : "<s:property value="linkParameterValues"/>"},
                         "equity" : "<s:property value="equityUsd"/>",
                         "excess_deficit" : "<s:property value="excessDeficitUsd"/>",
                         "valuation_difference" : "<s:property value="valuationDifferenceUsd"/>",
                         "segregated_margin" : "<s:property value="segregatedMarginUsd"/>" ,
                         "total_usage" : "<s:property value = "totalUsage"/> ",
                         "reg_cpe_margin" : "<s:property value="regCpeMarginUsd"/>",
                         "reg_cpe_equity" : "<s:property value="regCpeEquityUsd"/>"
                     }<s:if test="!#innerLoop.last">,</s:if>
                     </s:iterator>
                   </s:if>
                   ]
               }<s:if test="!#agmtTypeLoop.last">,</s:if>
               </s:iterator>
             </s:if>
         </s:else>
       ]
     }<s:if test="!#outerLoop.last">,</s:if>
     </s:iterator>
   </s:if>
   ]
}
