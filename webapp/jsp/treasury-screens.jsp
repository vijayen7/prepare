<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="codex" uri="http://arcesium.com/codex/taglib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" class="win chrome chrome4 webkit webkit5">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> -->
    <title>Treasury Screens</title>
    <jsp:include page="arcux/common-arc-imports.jsp"></jsp:include>
    <script type="text/javascript">
      ArcThemeHandler.enableThemeSelection();
    </script>
    <style>
      .application-menu-toggle-view a {
        font-size: 12px;
        padding-left: 10px;
      }

      .application-menu-toggle-view a.active {
        color: #000;
        cursor: default;
      }

      .application-menu-toggle-view a.active {
        color: #fff;
        cursor: default;
      }

      .light-theme .application-menu-toggle-view a.active {
        color: #000;
      }

      .myDiv {
        width: 500px;
      }

    </style>
      <script>
        function switchJqueryCSS() {
          var href = document.querySelector('link#jquery-ui-css-arcux').href
          if (ArcThemeHandler.getCurrentTheme() == 'dark') {
            if (href.indexOf('light') > -1) {
              document.querySelector('link#jquery-ui-css-arcux').href = href.replace('light/jquery-ui.css', 'jquery-ui.css');
            }
          } else if (ArcThemeHandler.getCurrentTheme() == 'light') {
            if (href.indexOf('light') === -1) {
              document.querySelector('link#jquery-ui-css-arcux').href = href.replace('jquery-ui.css', 'light/jquery-ui.css');
            }
          }
        }
        switchJqueryCSS();
        ArcThemeHandler.addThemeChangeListener(switchJqueryCSS);
      </script>
      <link rel="stylesheet" type="text/css" href="/treasury/css/treasury_common.css"/>
    </head>
    <body>
      <div class="layout--flex--row gutter">
        <arc-header class="size--content" modern-themes-enabled user="<%=request.getRemoteUser()%>"></arc-header>
        <s:if test="treasuryScreens != null && treasuryScreens.size > 0">
          <s:iterator value="treasuryScreens" status="state1">
            <div class="size--content">
              <arc-panel-element showheader="true" label="<s:property value=" groupName" />">
                <div class="clearfix">
                  <s:iterator value="underlyingScreens" status="state">
                    <div style="float: left; padding-left: 10px;" class="myDiv">
                      <p>
                        <a href="<s:property value=" screenLink" />" target="_blank"><s:property value="screenName"/></a><br/>
                        <s:property value="screenDescription" escapeHtml="false"/>
                      </p>
                    </div>
                  </s:iterator>
                </div>
              </arc-panel-element>
            </div>
          </s:iterator>
        </s:if>
      </div>
    </body>
  </html>
