var url = self.location.href;

var host = url.substr(0, url.lastIndexOf('/'));
if (self.importScripts){
  self.importScripts(host + '/xlsx.js');
  self.importScripts(host + '/jszip.js');
}

onmessage = function(e) {
  // Following check will ignore message coming from React-DevTools or any other source
  if(e.data && e.data.type === 'exportToExcel'){
    var worksheets = e.data.worksheets;
    var generateXLSX = xlsx({
        creator: 'Arcesium',
        lastModifiedBy: 'Arcesium',
        worksheets: worksheets,
    });
    postMessage({
    	'blob': generateXLSX.blob
    });
  }
}